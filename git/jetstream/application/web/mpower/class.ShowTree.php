<?php

require_once(BASE_PATH . '/class.PageComponent.php');


class ShowTree extends PageComponent 
{
	public function Render() {
		return $this->DrawPerson($_SESSION['login_id']);
	}

	private function DrawPerson($person, $indent = 0) {
		$rec = $_SESSION['tree_obj']->GetRecord($person);

		$combined = ($_SESSION['tree_obj']->IsSeniorMgr($person)) ? ' <a href="' . SessionManager::CreateUrl('dashboard.php', 'pov=' . $person . '&combined=true') . '">Combined View</a>' : '';
		
		if ($rec['Level'] > 1) {

			$output = $rec['FirstName'] . ' ' . $rec['LastName'];
			if ($_SESSION['tree_obj']->IsDirectMgr($person)) {
				$output = '<a href="' . SessionManager::CreateUrl('dashboard.php', 'pov=' . $person . '&combined=false') . '">' . $output . '</a>';
			}

			$output = '<div src="images/person_sm.png" style="margin-left:' . $indent * 23 . 'px;">' . $output . $combined . '</div>';
		}
		if(isset($rec['children'])) {
			foreach($rec['children'] as $child) {
				$output .= $this->DrawPerson($child, $indent + 1);
			}
		}
		return $output;
	}

	
}
