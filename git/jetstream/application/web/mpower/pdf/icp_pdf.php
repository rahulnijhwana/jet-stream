<?PHP
define('FPDF_FONTPATH','font/');

include('cpdf.php'); // base class
include('tagpdf.php'); //extension to allow formatting in cells


//function hex2dec
//returns an associative array (keys: R,G,B) from
//a hex html code (e.g. #3FE5AA)
function hex2dec($couleur = "#000000"){
    $R = substr($couleur, 1, 2);
    $rouge = hexdec($R);
    $V = substr($couleur, 3, 2);
    $vert = hexdec($V);
    $B = substr($couleur, 5, 2);
    $bleu = hexdec($B);
    $tbl_couleur = array();
    $tbl_couleur['R']=$rouge;
    $tbl_couleur['G']=$vert;
    $tbl_couleur['B']=$bleu;
    return $tbl_couleur;
}

//conversion pixel -> millimeter in 72 dpi
function px2mm($px){
    return $px*25.4/72;
}

function txtentities($html){
    $trans = get_html_translation_table(HTML_ENTITIES);
    $trans = array_flip($trans);
    return strtr($html, $trans);
}


class PDF extends tagPDF
{

	var $AddFooter;			 // keeps the text to show in footer with the default footer
	var $TotalPage;			 // keeps the text to show in footer with the default footer
	var $DispayPage;			 // keeps the text to show in footer with the default footer
	var $pageNumberToShow;	 //keep page number of a particular invoice/entity when multiple entities are printed
	

	//Page header
	function Header()
	{
		//Arial bold 15
		$this->SetFont('Arial','B',14);
		//Title
		//$this->Cell(0,.25,$_SESSION['subscriber_name'],'B',0,'C');
		//Line break
		//$this->Ln(.5);
		
		if ($this->title != ""){
			$this->SetFont('Arial','B',14);
			$this->setY(0);
			$this->Cell(0,.5,$this->title,0,0,'C');
			$this->Ln(.5);		
		}
	}

	function Footer()
	{
		$this->setY(-.5);
		//Arial bold 15
		$this->SetFont('Arial','',9);
		$this->Cell(0, 0.25, "", 0, 0, 'C');
		
		if ($this->AddFooter) {
			$this->setY(-0.5);
			//Arial bold 15
			$this->SetFont('Arial','',7);
			//Footer Text
			$this->Cell(0, 0.25, $this->AddFooter, 0, 0, 'C');
		}
		if(!$this->pageNumberToShow){
			$this->pageNumberToShow = $this->page;
		}
		if ($this->DispayPage) {
			$this->setY(-0.5);
			$this->setX(-2.0);
			//Arial bold 15
			$this->SetFont('Arial','',7);
			//Footer Text
			if($this->TotalPage > 0) {
				$this->Cell(1.8, 0.25, 'Page'.' '.$this->pageNumberToShow.' '.'of'.' '.$this->TotalPage, 1, 0, 'R');
			} else {
				$this->Cell(1.8, 0.25, 'Page'.' '.$this->pageNumberToShow, 0, 0, 'R');
			}
			
		}
	}

    function RoundedRect($x, $y, $w, $h,$r, $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' or $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2f %.2f m',($x+$r)*$k,($hp-$y)*$k ));
        $xc = $x+$w-$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2f %.2f l', $xc*$k,($hp-$y)*$k ));

        $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
        $xc = $x+$w-$r ;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2f %.2f l',($x+$w)*$k,($hp-$yc)*$k));
        $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
        $xc = $x+$r ;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2f %.2f l',$xc*$k,($hp-($y+$h))*$k));
        $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2f %.2f l',($x)*$k,($hp-$yc)*$k ));
        $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }

	//Colored table
	function FancyTable($header, $data, $width = array())
	{
		$this->Ln();
		//Colors, line width and bold font
		$this->SetFillColor(255,255,255);
		$this->SetTextColor(10);
		$this->SetDrawColor(128,0,0);
		$this->SetLineWidth(.005);
		//$this->SetFont('','B');
		//Header
		
		if (count($width) > 0) {
			$w = $width;
		} else {
			$w = array_fill(0, count($header), .925);
		}
		
		
		for ($i=0; $i < count($header); $i++) {
			$this->Cell($w[$i], .3, $header[$i], 1, 0, 'C', true);
		}
		$this->Ln();
		//Color and font restoration
		$this->SetFillColor(255,255,255);
		//$this->SetTextColor(0);
		//$this->SetFont('');
		//Data
		$fill=false;
		foreach ($data as $row)
		{
			foreach($row as $key=>$value) {
				$this->Cell($w[$key], .2, $value, 1,0,'R',$fill);
			}
			$this->Ln();
			$fill=!$fill;
		}
		$this->Cell(array_sum($w),0,'','T');
		$this->Ln();
	}
	
	function BasicTable($header,$data, $width)
	{
		$this->Ln();
		if (count($width) > 0) {
			$w = $width;
		} else {
			$w = array_fill(0, count($header), .925);
		}
	
		//Header
		foreach ($header as $key=>$col) {
			$this->Cell($w[$key], .2, $col, 0);
		}	
		$this->Ln();
		
		//Data
		foreach ($data as $row)
		{
			foreach ($row as $key=>$col) {
				$this->Cell($w[$key], .2, $col, 1);
			}
			$this->Ln();
		}
	}

	

}


?>
