<?php

require_once(BASE_PATH . '/class.PageComponent.php');

class Header extends PageComponent 
{
	protected $title;
	protected $tabs = array();
	protected $subtabs = array();
	protected $current_tab;
	protected $show_tabs = true;
	protected $alert = false;
	
	function __construct() {
		$this->tabs = array(
			'GroupDash' => array('page' => 'dashboard.php', 'qry_str' => 'target=false', 'hidden' => true)
			, 'Dashboard' => 'dashboard.php'
			, 'Calendar' => 'calendar.php'
			, 'Reports' => 'reports.php'
			, 'Trends' => array('page' => '#', 'hidden' => true, 'lightbox' => true)
			, 'Coaching' => array('page' => '#', 'hidden' => true, 'lightbox' => true)
			, 'Alerts' => array('page' => 'alerts.php', 'hidden' => true, 'class'=>'redalert') 
			, 'AddOpp' => '#'
			, 'AddTarget' => '#'
			, 'ImportTargets' => array('page' => 'import_targets.php', 'hidden' => true) 
			, 'Reassign' => array('page' => '#', 'hidden' => true, 'lightbox' => true) 
			, 'Renew' => array('page' => '#') 
			, 'Forecast' => array('page' => 'forecast.php', 'simple' => true)
			, 'Manage' => array('page' =>'manager_admin.php', 'hidden' => true)
			, 'Help' => 'help.php'
			, 'Logout' => array('page' => 'logout.php', 'qry_str' => 'CID='.$_SESSION['company_id'])
			);
		$this->SetAlert();
	}
	
	public function SetTitle($title) {
		$this->title = $title;
	}

	public function SetSubtabs($target) {
		if (is_array($target) && count($target) > 0) {
			$trendssubmenu = array();
			$coachingsubmenu = array();
			$editoppsubmenu = array();
			$addtarget = array();
			$reassign = array();
			$renew = array();
			$forecast = array();

			$target = $_SESSION['tree_obj']->SortPeople($target, 'FirstName');

			$trendssubmenu['All'] = SessionManager::CreateUrl('legacy/board/select_trend.php','target=-1');
			$forecast['My Team'] = SessionManager::CreateUrl('forecast.php', 'target=' . $_SESSION['login_id'] . '&team=1');

			foreach($target as $personid) {
				$name = $_SESSION['tree_obj']->GetInfo($personid, 'FirstName') . ' ' . $_SESSION['tree_obj']->GetInfo($personid, 'LastName');
				$trendssubmenu[$name] = SessionManager::CreateUrl('legacy/board/select_trend.php','target='.$personid);
				$coachingsubmenu[$name] = SessionManager::CreateUrl('legacy/board/analysis.php','target='.$personid);
				$editoppsubmenu[$name] = SessionManager::CreateUrl('legacy/shared/edit_opp2.php','reqOpId=-1&amp;reqPersonId='.$personid.'#-1_'.$personid);
				$addtarget[$name] = SessionManager::CreateUrl('legacy/shared/edit_targ.php','hash=-1_'.$personid);

				$reassign[$name] = SessionManager::CreateUrl('legacy/board/spreadsheet.php','isspreadsheet=1&show=1&hash=-1_'.$personid);
				$renew[$name] = SessionManager::CreateUrl('legacy/board/spreadsheet.php','hash=-2_'.$personid);
				$forecast[$name] = SessionManager::CreateUrl('forecast.php','target='.$personid);
			}
		} elseif (is_int($target) && $target > 0) {
			$personid = $target;
			$trendssubmenu = SessionManager::CreateUrl('legacy/board/select_trend.php','target='.$personid);
			$coachingsubmenu = SessionManager::CreateUrl('legacy/board/analysis.php','target='.$personid);
			$editoppsubmenu = SessionManager::CreateUrl('legacy/shared/edit_opp2.php','reqOpId=-1&amp;reqPersonId='.$personid.'#-1_'.$personid);
			$addtarget = SessionManager::CreateUrl('legacy/shared/edit_targ.php','hash=-1_'.$personid);

			$reassign = SessionManager::CreateUrl('legacy/board/spreadsheet.php','isspreadsheet=1&show=1&hash=-1_'.$personid);
			$renew = SessionManager::CreateUrl('legacy/board/spreadsheet.php','hash=-2_'.$personid);
			$forecast = SessionManager::CreateUrl('forecast.php','target='.$personid);	
		}

		$subtabs = array();
		$subtabs['Trends'] = $trendssubmenu;
		$subtabs['Coaching'] = $coachingsubmenu;
		$subtabs['AddOpp'] = $editoppsubmenu;
		$subtabs['AddTarget'] = $addtarget;
		$subtabs['Reassign'] = $reassign;
		$subtabs['Renew'] = $renew;
		$subtabs['Forecast'] = $forecast;
		$this->subtabs = $subtabs;
	}

	public function AddTab($name, $link, $current = false) {
		$this->tabs[$name] = $link;

		if ($current) {
			$this->current_tab = $name;
		}
		
		return $this;
	}

	public function SetAlert() {
		//$this->alert = true;
		if(isset($_GET['target']) && $_GET['target'] != 'false') {
			$peers = (int)$_GET['target'];
		} else {
			$targets = $_SESSION['tree_obj']->GetTarget(true);
			if (is_array($targets)) {
				$peers = implode(',',array_values($targets));
			} else {
				$peers = (int)$targets;
			}
		}
			
		//$peers = implode(',',array_values($_SESSION['tree_obj']->GetTarget(true)));
		$sql = "SELECT DealID, PersonID, Alert1, Alert2, Alert3, Alert4, Alert5, Alert6  
					FROM opportunities
					WHERE (Alert1 = 1 OR Alert2 = 1 OR Alert3 = 1 OR Alert4 = 1 OR Alert5 = 1 OR Alert6 = 1) 
						  AND PersonID in ($peers) 
						  AND Category NOT IN (9, 6)";
		$opp_rs	= DbConnManager::GetDb('mpower')->Exec($sql);
		//print_r($sql);
		//print '<br><br><br>';
		//print_r($opp_rs);
		if (count($opp_rs) > 0) {
			$this->alert = true;
		} else {
			$this->alert = false;
		}
	}

	public function UnHideTab($name) {
		if (is_array($this->tabs[$name])) {
			$this->tabs[$name]['hidden'] = false;
		}
		return $this;
	}
	
	public function HideTab($name) {
		$val = $this->tabs[$name];
		if (!is_array($this->tabs[$name])) {
			$val = array('page' => $val);
		}
		$val['hidden'] = true;
		$this->tabs[$name] = $val;
		return $this;
	}

	public function NoTabs() {
		$this->show_tabs = false;
	}
	
	public function Current($name) {
		$this->current_tab = $name;
		return $this;
	}
	
	public function GetCssFiles() {
		return array('tabs.css', 'gb_styles.css', 'header.css', array('print', 'header_print.css'));
	}
			
	public function GetJsFiles() {
		return array('AJS.js', 'AJS_fx.js', 'gb_scripts.js', 'windowing.js');
	}

	public function GetJsInit() {
		return;
	}

	
	public function Render() {
		if ($_SESSION['tree_obj']->IsManager($_SESSION['login_id'])) {
			$this->UnHideTab('GroupDash');
			$this->UnHideTab('Trends');
			$this->UnHideTab('Coaching');
			$this->UnHideTab('Alerts');
			$this->UnHideTab('ImportTargets');
			$this->UnHideTab('Reassign');
			$this->UnHideTab('Manage');
			if (!isset($_SESSION['target'])) {
				$this->HideTab('Dashboard');
			}
		}
		
		if(is_null($this->current_tab)) {
			$script_name = basename($_SERVER['SCRIPT_NAME']);
			foreach($this->tabs as $tab_name => $tab) {
				$tab_script_name = (is_array($tab)) ? $tab['page'] : $tab;
				if ($script_name == $tab_script_name && (!is_array($tab) || !isset($tab['hidden']) || $tab['hidden'] == false)) {
					$this->current_tab = $tab_name;
				}
			}
		}

		if (isset($_SESSION['company_obj']['TargetUsed']) && $_SESSION['company_obj']['TargetUsed'] == 0) {
			$this->HideTab('AddTarget');
			$this->HideTab('ImportTargets');
		}
			
		if (!isset($this->title)) {
			$this->SetTitle($_SESSION['tree_obj']->GetPath($_SESSION['pov']));
		}

		$date = Language::__get(date('F')) . date(' j, Y');
		$output = '';
		$output .= "<div id=\"header\"><div>$date<br>$this->title</div></div>";
		if (count($this->tabs) > 0 && $this->show_tabs) {
			$tabs = '<div class="menu2"><ul>';			

			// $output .= '<div id="tabs"><ul id="Step1MenuBar" class="MenuBarHorizontal">';
			if ($this->current_tab == 'Dashboard' && $_SESSION['tree_obj']->IsManager($_SESSION['login_id'])) {
				$this->SetSubtabs($_SESSION['target']);
			} else {
				$this->SetSubtabs($_SESSION['tree_obj']->GetTarget(true));
			}

			reset($this->tabs);
			foreach ($this->tabs as $title => $values) {
				$query_string = false;
				$is_lightbox = false;
				$is_simple = false;
				$anchor_params = '';
				$class = '';
				if (!is_array($values)) {
					$link = $values;
				} else {
					if (isset($values['hidden']) && $values['hidden'] == true) {
						continue;
					}
					$link = $values['page'];
					if (isset($values['qry_str'])) {
						$query_string = $values['qry_str'];
					}
					if (isset($values['lightbox'])) {
						$is_lightbox = $values['lightbox'];
					}
					if (isset($values['simple'])) {
						$is_simple = $values['simple'];
					}
					$class = isset($values['class']) && $this->alert ? $values['class'] : '';
				}

				$height = 630;
				$width = 820;
				if ($title == 'AddTarget') {
					$height = 300;
					$width = 360;
				}

				$class_current = '';
				if ($title == $this->current_tab) {
					$class_current = 'current';
				}
				if ($link != '#') {
					$link = SessionManager::CreateUrl($link, $query_string);
				} elseif (isset($this->subtabs[$title]) && is_string($this->subtabs[$title])) {
					if ($is_lightbox) {
						$link = $this->subtabs[$title];
						$anchor_params = 'title="" rel="gb_pageset[spreadsheets]"';
					}
					else if ($is_simple) {
						$link = SessionManager::CreateUrl($link, $query_string);
					}
					 else {

						$link = "javascript: var win = Windowing.openSizedWindow('{$this->subtabs[$title]}', $height, $width);";
					}
				}

				$submenu = '';
				
				if (isset($this->subtabs[$title]) && is_array($this->subtabs[$title]) && count($this->subtabs[$title]) > 0) {
					$scroller = (count($this->subtabs[$title]) > 15) ? 'class="scroller"' : ''; 
					$submenu = "<li><a class=\"drop $class_current\" href=\"$link\" $anchor_params><nobr>" . Language::__get($title) . "</nobr>
						<!--[if IE 7]><!--></a><!--<![endif]-->
						<!--[if lte IE 6]><table><tr><td><iframe src=\"blank.html\"></iframe><![endif]--><ul $scroller>";

					foreach ($this->subtabs[$title] as $subtitle => $sublink) {
						if($is_lightbox) {
							$submenu .= "<li style=\"width:120px;\"><a href=\"$sublink\" title=\"\" rel=\"gb_pageset[spreadsheets]\"><nobr>$subtitle</nobr></a></li>";
						}
						else if($is_simple) {
							$submenu .= "<li style=\"width:120px;\"><a href=\"$sublink\"><nobr>$subtitle</nobr></a></li>";
						}
						else {
							$submenu .= "<li style=\"width:120px;\"><a href=\"javascript:var win = Windowing.openSizedWindow('$sublink', $height, $width);\"><nobr>$subtitle</nobr></a></li>";
						}
					}
					$submenu .= '</ul><!--[if lte IE 6]></td></tr></table></a><![endif]-->';
				} else {
					$tabs .= "<li><a  class=\"$class $class_current\" href=\"$link\" $anchor_params><nobr>" . Language::__get($title) . "</nobr></a>";
				}

				$tabs .= $submenu . "</li>";
			}
			$tabs .= '</ul></div>';
			// $output .= '</ul></div></<div style="clear:both"></div>';
			$output .= $tabs;
		}
		return $output;
	}
}
