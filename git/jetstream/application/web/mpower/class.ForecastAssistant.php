<?php
require_once(BASE_PATH . '/class.PageComponent.php');
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/pdf/icp_pdf.php';

class ForecastAssistant extends PageComponent 
{
	public $target;
	public $download;
	private $unreported_sales;
	private $unreported_revenue;
	private $reported_sales;
	private $reported_revenue;	
	private $all_unreported_sales;
	private $all_unreported_revenue;		
	private $all_reported_sales;
	private $all_reported_revenue;
	private $fiscal_year_end;
	private $company_goal;
	private $personal_goal;
	private $period;
	private $period_list;
	private $first_meetings;
	private $sales_cycle;
	private $closing_ratio;
	private $sales_per_month;
	private $current_period;
	private $today;
	private $days_left_this_quarter;
	private $days_left_this_month;
	private $days_left_this_year;
	private $current_quarter;
	private $seasonality;
	private $dayofyear;	
	private $calculated_total_sales;
	private $calculated_total_revenues;
	private $unreported_sales_per_month;
	private $unreported_avg_sale;
	public $year;
	public $seasonality_set;
	public $pdf;
	private $salesperson_list = array();
	public $team_view;
	
	
	function __construct() {
		$this->period = 12;		
		$this->year = isset($_GET['year']) ? $_GET['year'] : date('Y');
		$this->today = date('m/d/Y');
		$this->SetFiscalYearEnd();
		$this->dayofyear = ($this->year == date('Y')) ? round((date("z")/365) * 100) : (($this->year < date('Y')) ? 100 : 0);
	}

	public function GetJsInit() {
		return;
	}
	
	function getTotal($array) {	
		$total = 0;
		if(count($array)) {
			foreach($array as $item)  {
				$total += $item;
			}
		}
		return $total;
	}	
	
	public function GetJs() {
		$this->GetCurrentQuarter();		
		$output = "var sn = '{$_GET['SN']}';\n";		
		
		if ((!$_SESSION['tree_obj']->GetInfo($this->target, 'IsSalesperson') || $this->team_view == 1) && ($_SESSION['tree_obj']->IsSeniorMgr($this->target) || $_SESSION['tree_obj']->IsManager($this->target))) {			
			$output .= "var personid = -1;\n";
		} else {
			$this->GetInfo();
			
			$this->seasonality = $this->GetSeasonality($this->target);	
			$this->GetUnreportedSales();
			$this->GetReportedSales();
			
			// calculate the sales per month
			$this->sales_per_month = round($this->sales_per_month, 1);
			$original_monthly_sales = $this->sales_per_month;
			
			// calculate the average sale			
			$this->average_sale = ($this->calculated_total_sales == 0) ? 0 : $this->calculated_total_revenues / $this->calculated_total_sales;
			$this->average_sale = round($this->average_sale, 1);						
			$original_average_sales = $this->average_sale;
						
			$output .= "var current_period = $this->current_period;\n";
			$output .= "var personid = $this->target;\n";
			$output .= "var current_quarter = $this->current_quarter;\n";
			$output .= "var total_period = $this->period;\n";		
			$output .= "var year = $this->year;\n";		
			$output .= "var days_left_this_month = $this->days_left_this_month;\n";
			$output .= "var days_left_this_quarter = $this->days_left_this_quarter;\n";		
			$output .= "var days_left_this_year = '$this->days_left_this_year';\n";
			$output .= "var first_meeting_slider_value = $this->first_meetings;\n";
			$output .= "var sales_cycle_slider_value = $this->sales_cycle;\n";
			$output .= "var closing_ratio_slider_value = $this->closing_ratio;\n";
			$output .= "var average_sales_slider_value = $this->average_sale;\n";
			$output .= "var calculated_total_sales = $this->calculated_total_sales;\n";
			$output .= "var original_unreported_avg_sale = $this->unreported_avg_sale;\n";
			$output .= "var original_unreported_sales_per_month = $this->unreported_sales_per_month;\n";			
			
			if ($this->period == 13) {
				$output .= "var seasonality = new Array({$this->seasonality[1]},{$this->seasonality[2]},{$this->seasonality[3]},{$this->seasonality[4]},{$this->seasonality[5]},{$this->seasonality[6]},{$this->seasonality[7]},{$this->seasonality[8]},{$this->seasonality[9]},{$this->seasonality[10]},{$this->seasonality[11]},{$this->seasonality[12]},{$this->seasonality[13]});\n";		
			} else {
				$output .= "var seasonality = new Array({$this->seasonality[1]},{$this->seasonality[2]},{$this->seasonality[3]},{$this->seasonality[4]},{$this->seasonality[5]},{$this->seasonality[6]},{$this->seasonality[7]},{$this->seasonality[8]},{$this->seasonality[9]},{$this->seasonality[10]},{$this->seasonality[11]},{$this->seasonality[12]});\n";		
			}
		
		}		
		return $output;
	}
	
	public function GetCssFiles() {
		return array('slider.css', 'forecast_assistant.css', array('print', 'forecast_assistant_print.css'));
	}

	public function GetJsFiles() {
		return array('jquery.js', 'ui.base.js', 'ui.slider.js', 'forecast_assistant.js');
	}
	
	public function GetUnreportedSales() {
		
		$sql = "SELECT UnreportedSalesID, PersonID, Year, Period, Sales, Revenue
			    FROM UnreportedSales 
			    WHERE Year = ? AND 
					  PersonID = ? 
			    ORDER BY Period";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->year), array(DTYPE_INT, $this->target));
		
		$sales_rs = DbConnManager::GetDb('mpower')->Execute($sql);

		$this->unreported_sales = array_fill(1, $this->period, 0);
		$this->unreported_revenue = array_fill(1, $this->period, 0);

		foreach($sales_rs as $sales) {
			$this->unreported_sales[$sales->Period] = $sales->Sales;
			$this->unreported_revenue[$sales->Period] = $sales->Revenue;
		} 
	}

	public function GetInfo() {	
		$this->first_meetings = (int) $_SESSION['tree_obj']->GetInfo($this->target, 'FirstMeeting');
		$this->sales_cycle = (int) $_SESSION['tree_obj']->GetInfo($this->target, 'AverageSalesCycle');
		$this->closing_ratio = ($_SESSION['tree_obj']->GetInfo($this->target, 'ClosingRatio')) * 100;
				
		$this->sales_per_month = $_SESSION['tree_obj']->GetInfo($this->target, 'ClosingRatio') * $this->first_meetings;		
		$this->calculated_total_sales = (int) ($_SESSION['tree_obj']->GetInfo($this->target, 'TotalSales'));
		$this->calculated_total_revenues = (int) $_SESSION['tree_obj']->GetInfo($this->target, 'TotalRevenues');				
		$this->unreported_avg_sale = (int) $_SESSION['tree_obj']->GetInfo($this->target, 'UnreportedAvgSale');
		$this->unreported_sales_per_month = (int) $_SESSION['tree_obj']->GetInfo($this->target, 'UnreportedSalesPerMonth');
		$this->average_sale = ($this->calculated_total_sales == 0) ? 0 : $this->calculated_total_revenues / $this->calculated_total_sales;
		
		$this->GetPeriodList();
	}
	
	function GetPeriodList() {
		$sql = "SELECT Period, PeriodEndDate, PeriodLabel
				FROM FiscalYear
				WHERE CompanyID = ? AND Year = ? AND FiscalYearEndDate is NULL AND PeriodEndDate is NOT NULL
				ORDER BY Period";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['mpower_companyid']), array(DTYPE_INT, $this->year));
		$company_rs = DbConnManager::GetDb('mpower')->Execute($sql);
		
		$this->period_list = array();
		
		foreach($company_rs as $period_end) {
			$this->period_list[$period_end->Period]['Date'] = strtotime($period_end->PeriodEndDate);
			$this->period_list[$period_end->Period]['Label'] = $period_end->PeriodLabel;
		}
		
		if (count($this->period_list) > 12) $this->period = count($this->period_list);		
	}

	public function GetSeasonality($person_id = 0) {
		
		$value = ($this->period == 0) ? 0 : round(100 / $this->period, 2);		
		$seasonality_list = array_fill(1, 13, $value);
		
		// check the indivisual seasonlaity
		$seasonality_enabled_sql = "SELECT SeasonalityEnabled
							FROM Seasonality  
							WHERE PersonID = ? AND Year = ? AND Period IS NULL AND Seasonality IS NULL";
		$seasonality_enabled_sql = SqlBuilder()->LoadSql($seasonality_enabled_sql)->BuildSql(array(DTYPE_INT, $person_id), array(DTYPE_INT, $this->year));
		$seasonality = DbConnManager::GetDb('mpower')->GetOne($seasonality_enabled_sql);

		if (!$seasonality instanceof MpRecord) {		
			$this->seasonality_set = false;
		} else {
			$this->seasonality_set = $seasonality->SeasonalityEnabled;
		}	
				
		if ($this->seasonality_set) {
			// check the indivisual seasonlaity
			$seasonality_sql = "SELECT Period, Seasonality
								FROM Seasonality  
								WHERE PersonID = ? AND Year = ? AND Period IS NOT NULL AND Seasonality IS NOT NULL";
			$seasonality_sql = SqlBuilder()->LoadSql($seasonality_sql)->BuildSql(array(DTYPE_INT, $person_id), array(DTYPE_INT, $this->year));			
			$seasonality_rs = DbConnManager::GetDb('mpower')->Execute($seasonality_sql);
			if (count($seasonality_rs)) {				
				foreach ($seasonality_rs as $seasonality) {
					$seasonality_list[$seasonality->Period] = $seasonality->Seasonality;
				}
			} 
		} else {
		
			$sql = "SELECT SeasonalityEnabled
					FROM Seasonality WHERE CompanyID = ? AND PersonID IS NULL AND Year = ? AND PersonID IS NULL AND Seasonality IS NULL";
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['mpower_companyid']), array(DTYPE_INT, $this->year));
			
			
			$seasonality_enabled_rs = DbConnManager::GetDb('mpower')->getOne($sql);
			
			if ($seasonality_enabled_rs['SeasonalityEnabled']) {
				// check the company seasonlaity
				$seasonality_sql = "SELECT S.Period, S.Seasonality
									FROM Seasonality S LEFT JOIN Company C ON S.CompanyID = C.CompanyID AND C.Seasonality = 1
									WHERE S.CompanyID = ? AND S.Year = ? AND C.Seasonality IS NOT NULL AND S.PersonID IS NULL AND S.SeasonalityEnabled IS NULL";
				$seasonality_sql = SqlBuilder()->LoadSql($seasonality_sql)->BuildSql(array(DTYPE_INT, $_SESSION['mpower_companyid']), array(DTYPE_INT, $this->year));
			
				$seasonality_rs = DbConnManager::GetDb('mpower')->Execute($seasonality_sql);
				if (count($seasonality_rs)) {
					$this->seasonality_set = true;
					foreach ($seasonality_rs as $seasonality) {
						$seasonality_list[$seasonality->Period] = $seasonality->Seasonality;
					}
				}
			}
		}		
		return $seasonality_list;
	}

	public function GetCurrentQuarter() {
		$month = (int) date('n');
		
		// This is correct for 13 period
		/*if ($this->period == 13) {
			$current_timestamp = strtotime("now");
			foreach ($this->period_list as $key=>$temp_period) {
				if ($current_timestamp < $temp_period['Date']) {
					$month = $key;
					break;
				}
			}
		}*/
		
		$this->current_period = $month;		
		//$this->current_quarter = ($month % 3 == 0) ? $month / 3 : floor(($month / 3)) + 1;
		
		if ($month >= 1 && $month <= 3 ) $this->current_quarter = 1;
		else if ($month >= 4 && $month <= 6) $this->current_quarter = 2;
		else if ($month >= 7 && $month <= 9) $this->current_quarter = 3;
		else $this->current_quarter = 4;
				
		$date_past_month = date('d');	
		$this->days_left_this_month = 31 - $date_past_month;
		
		$temp_quarter_passed = ($this->current_period - 1) % 3;	
		//if($this->period == 13) $temp_quarter_passed = 3;
		
		$total_days_past_this_quarter = ($temp_quarter_passed * 31) +	$date_past_month;
		
			
		$number_of_months = 3;
		//if($this->period == 13 && $this->current_quarter == 4) $number_of_months = 4;	
		
		$total_days_this_quarter = $number_of_months * 31;	
		//echo $total_days_this_quarter;
		$this->days_left_this_quarter = $total_days_this_quarter - $total_days_past_this_quarter;	
		
		//echo $total_days_past_this_quarter.'<br>';
		
		$this->days_left_this_year = 372 - ((($this->current_period - 1) * 31) +  $date_past_month);
	}

	public function GetReportedSales() {
		 
		$this->reported_sales = array_fill(1, $this->period, 0);
		$this->reported_revenue = array_fill(1, $this->period, 0);
		$date = '01/01/' . $this->year;
		
		if ($this->period == 12) {
			
			$opp_sql = "SELECT sum(ActualDollarAmount) Amount, count(ActualCloseDate) CloseDates, month(ActualCloseDate) M
						FROM opportunities 
						WHERE opportunities.PersonID = ? AND 
							(category = 6 AND ActualCloseDate >= ? AND ActualCloseDate <= ?)
						GROUP BY month(ActualCloseDate)";
						
			$opp_sql = SqlBuilder()->LoadSql($opp_sql)->BuildSql(array(DTYPE_INT, $this->target), array(DTYPE_TIME, $date), array(DTYPE_TIME, $this->fiscal_year_end));
			$closed_opps = DbConnManager::GetDb('mpower')->Execute($opp_sql);			
			
			foreach($closed_opps as $opps) {
				$this->reported_sales[$opps->M] = $opps->CloseDates;
				$this->reported_revenue[$opps->M] = $opps->Amount;
			} 
		} else {
		
			$opp_sql = "SELECT ActualDollarAmount, ActualCloseDate
						FROM opportunities 
						WHERE opportunities.PersonID = ? AND 
							(category = 6 AND ActualCloseDate >= ? AND ActualCloseDate <= ?)";
						
			$opp_sql = SqlBuilder()->LoadSql($opp_sql)->BuildSql(array(DTYPE_INT, $this->target), array(DTYPE_TIME, $date), array(DTYPE_TIME, $this->fiscal_year_end));
			$closed_opps = DbConnManager::GetDb('mpower')->Execute($opp_sql);
			
			foreach($closed_opps as $opps) {
				$temp = strtotime($opps->ActualCloseDate);
				foreach($this->period_list as $key=>$temp_period) {
					if($temp < $temp_period['Date']) {
						$this->reported_sales[$key] += 1;
						$this->reported_revenue[$key] += $opps->ActualDollarAmount;
						break;
					}
				}
			} 		
		}
	}
	
	public function Download() {
		$this->GetPeriodList();
		
		
		if ($this->download == 'Team') {
			$this->pdf = new PDF('L', 'in', 'Letter');

			// Add the PDF page
			$this->pdf->AddPage();
			$this->pdf->SetLeftMargin(0.4);
			$this->pdf->SetRightMargin(0.4);
			$this->pdf->AddFooter = 'M-Power is a trademark of ASA Sales Systems, LLC. Patent Pending';
			//$this->pdf->DispayPage = true;
			//$this->pdf->pageNumberToShow = $this->pdf->page;
			
			$managername = $_SESSION['tree_obj']->GetInfo($this->target, 'FirstName') . ' ' . $_SESSION['tree_obj']->GetInfo($this->target, 'LastName');			
			$this->pdf->SetFont('Arial', '', 12);			
			$this->pdf->Cell(0, 0.2, 'Forecast Assistant', 1, 1, 'C'); 		
			
			$this->pdf->SetFont('Arial', '', 8);	
				
			$this->pdf->Cell(0, 0.2, " $managername's Team", 1, 1, 'C'); 	
			$this->pdf->Cell(0, 0.2, "Report Date : $this->today", 1, 1, 'C');
				
			$this->Rollups();

			echo $this->pdf->Output();			
			

		} else if ($this->download == 'Salesperson') {
			
			$this->GetInfo();
			
			$this->seasonality = $this->GetSeasonality($this->target);	
			$this->GetUnreportedSales();	
			$this->GetReportedSales();		
			$this->GetCurrentQuarter();
		
		
			$this->pdf = new PDF('L', 'in', 'Letter');
			// Add the PDF page
			$this->pdf->AddPage();
			$this->pdf->SetLeftMargin(0.4);
			$this->pdf->SetRightMargin(0.4);
			$this->pdf->AddFooter = 'M-Power is a trademark of ASA Sales Systems, LLC. Patent Pending';
			//$this->pdf->DispayPage = true;
			//$this->pdf->pageNumberToShow = $this->pdf->page;
			
			$managername = $_SESSION['tree_obj']->GetInfo($this->target, 'FirstName') . ' ' . $_SESSION['tree_obj']->GetInfo($this->target, 'LastName');			
			$this->pdf->SetFont('Arial', '', 12);			
			$this->pdf->Cell(0, 0.2, 'Forecast Assistant', 1, 1, 'C');
			$this->pdf->SetFont('Arial', '', 8);	
			
			$this->Info();		
			$this->Goals();			
			$this->GetAllUnreportedSales($this->target);
			$this->GetAllReportedSales($this->target);
			$this->MonthlyReport($this->company_goal);
			
			echo $this->pdf->Output();			
					
		} else {
			echo  'Not implemented!';
		}
	}
	
	public function Render() {
		
		$forecast = SessionManager::CreateUrl('forecast.php');		
		$forcast_body = '<form name="fa_form" id="fa_form">
		<div id="faheader">Forecast Assistant</div>';		

		$this->team_view = isset($_GET['team']) ? $_GET['team'] : 0;
		
		$target_qtr = isset($_GET['target']) ? 'target=' . (int)$_GET['target'] : '';		
		$past_year = date('Y') - 1;
		$future_year = date('Y') + 1;
		
		$forecast_past = SessionManager::CreateUrl('forecast.php', 'year=' . $past_year . '&' . $target_qtr);
		$forecast_current = SessionManager::CreateUrl('forecast.php', $target_qtr);
		$forecast_future = SessionManager::CreateUrl('forecast.php', 'year=' . $future_year . '&' . $target_qtr);		
		$this->target = ($this->target > 0) ? $this->target : $_SESSION['login_id'];
		$body_content = '';
		
		if ((!$_SESSION['tree_obj']->GetInfo($this->target, 'IsSalesperson') || $this->team_view == 1) && ($_SESSION['tree_obj']->IsSeniorMgr($this->target) || $_SESSION['tree_obj']->IsManager($this->target))) {			
			$managername = $_SESSION['tree_obj']->GetInfo($this->target, 'FirstName') . ' ' . $_SESSION['tree_obj']->GetInfo($this->target, 'LastName');			
			$forcast_body .= '<div>' . " $managername's Team" . '</div>';			
			$forcast_body .= '<div style="padding-bottom:20px;">Report Date : ' . $this->today . '</div>';		
			$body_content .= $this->Rollups();			
			$forecast_download = SessionManager::CreateUrl('forecast.php', 'download=Team&' . $target_qtr);	
	
			$forcast_body .= '<div style="float:left" class="download"><a href="' . $forecast_download . '" target="_blank" >
								<img src="images/pdf.png"></img></a></div>';			
			
		} else {
			$body_content .= '<div><a href="' . $forecast . '&team=1" style="float:right">My Team</a></div><br><br>';
			$body_content .= $this->Info();		
			$body_content .= '<br>';
			$body_content .= $this->Goals();
			$body_content .= '<br>';
			$body_content .= $this->MonthlyReport($this->company_goal);			
			$forecast_download = SessionManager::CreateUrl('forecast.php', 'download=Salesperson&' . $target_qtr);	
	
			$forcast_body .= '<div style="float:left" class="download"><a href="' . $forecast_download . '" target="_blank" >
								<img src="images/pdf.png"></img></a></div>';			
						
		}	
		
		$forcast_body .= $body_content;
		$forcast_body = '<div style="padding:30px;">' . $forcast_body . '</div>';	
		
		return $forcast_body;
	}
	
	private function GetPerson($person) {
				
		$rec = $_SESSION['tree_obj']->GetRecord($person);

		if ($rec['Level'] == 1 || $_SESSION['tree_obj']->GetInfo($person, 'IsSalesperson')) {
			$this->salesperson_list[] = $person;
		}
		
		if(isset($rec['children'])) {
			foreach($rec['children'] as $child) {
				$this->GetPerson($child);
			}
		}		
	}		
		
	function Rollups() {
		
		$this->GetPeriodList();		
		$tbody = $target_list = '';
		
		if ($this->download) $tbody_pdf = array();
		
		$total_first_meeting = $total_sales_cycle = $total_closing_ratio = $total_to_goal = $total_goal_to_date = 0;
		$total_average_sale = $total_sales_per_month = $total_sales_to_date = $toatl_annula_goal = $total_percentage_goal_to_date = 0;		
			
		$target = $_SESSION['tree_obj']->GetRecord($this->target);
		$total_salespersons = count($target['children']);		
		$target['children'] = $_SESSION['tree_obj']->SortPeople($target['children'], 'FirstName');		
		
		$this->salesperson_list = array();
		$this->GetPerson($this->target);
		$salesperson_array = $this->salesperson_list;
		$salesperson_array = $_SESSION['tree_obj']->SortPeople($salesperson_array, 'FirstName');		
		

		$targets = implode(',', $salesperson_array);		
		$this->GetAllUnreportedSales($targets);
		$this->GetAllReportedSales($targets);		
		
		foreach ($salesperson_array as $key => $personid) {			
			$annual_goals = 0;			
			$person_annual_goals = $this->GetSalesPersonsGoal($personid);			
			foreach ($person_annual_goals as $person_annual_goal) {
				$annual_goals += $person_annual_goal;	
			}		
			
			$this->salesperson_list = array();
			$this->GetPerson($personid);
			$salesperson_list = $this->salesperson_list;
					
			$sales_cycle = $average_sale = $sales_per_month = $goal_to_date = $first_meeting = $closing_ratio = $sales_cycle = 0;			
			$count = 0;
			$first_meeting_total = $sales_cycle_total = $closing_ratio_total = $average_sale_total = 0;				 

			
			foreach ($salesperson_list as $salespersonid) {				
				$first_meeting_total += (int)$_SESSION['tree_obj']->GetInfo($salespersonid, 'FirstMeeting');
				$sales_cycle_total += $_SESSION['tree_obj']->GetInfo($salespersonid, 'AverageSalesCycle');
				$closing_ratio_total += $_SESSION['tree_obj']->GetInfo($salespersonid, 'ClosingRatio');				
				$calculated_total_sales = (int) ($_SESSION['tree_obj']->GetInfo($salespersonid, 'TotalSales'));		
				$calculated_total_revenues = (int) $_SESSION['tree_obj']->GetInfo($salespersonid, 'TotalRevenues');
				$average_sale_total += ($calculated_total_sales == 0) ? 0 : $calculated_total_revenues / $calculated_total_sales;				 
				$goal_to_date += $this->GoalsToDate($salespersonid);
				$count++;
			}			
			
			$first_meeting = ($count == 0)? 0 : $first_meeting_total / $count;
			$sales_cycle = ($count == 0)? 0 : $sales_cycle_total / $count;
			$closing_ratio = ($count == 0)? 0 : $closing_ratio_total / $count;			
			$average_sale = ($count == 0)? 0 : $average_sale_total / $count;
			
			$total_goal_to_date += $goal_to_date;	
			$sales_per_month = (int) $first_meeting * floatval($closing_ratio);			
			$salesperson_list = (count($salesperson_list) >0) ? implode(',', $salesperson_list) : 0;			
			$name = $_SESSION['tree_obj']->GetInfo($personid, 'FirstName') . ' ' . $_SESSION['tree_obj']->GetInfo($personid, 'LastName');						
			
			$total_first_meeting += $first_meeting;
			$total_sales_cycle += $sales_cycle;
			$total_closing_ratio += $closing_ratio;
			$total_average_sale += $average_sale;
			$total_sales_per_month += $sales_per_month;
			$annula_goal_amount = $annual_goals;			
			$toatl_annula_goal += $annula_goal_amount;
			$sales_to_date = 0;
			
			$start_date = '01/01/'. $this->year;
			$end_date = $this->fiscal_year_end;
			
			$sql = "SELECT sum(ActualDollarAmount) ReportedAmount FROM opportunities
					WHERE PersonID IN ($salesperson_list) AND category = 6 AND ActualCloseDate BETWEEN '$start_date' AND '$end_date'";
			$reported_sales = DbConnManager::GetDb('mpower')->getOne($sql);		
			$sales_to_date += $reported_sales->ReportedAmount;			
			
			$period_check = ($this->current_period < 13) ? ' AND Period < 13' : '';
			
			$sql = "SELECT sum(Revenue) UnreportedAmount FROM UnreportedSales WHERE Year = ? AND PersonID IN ($salesperson_list) " . $period_check;			
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->year));
			$unreported_sales = DbConnManager::GetDb('mpower')->getOne($sql);
			$sales_to_date += $unreported_sales->UnreportedAmount;
			
			$to_goal = ($annula_goal_amount == 0) ? 0 : $sales_to_date / $annula_goal_amount * 100;
			$total_sales_to_date += $sales_to_date;
			$total_to_goal += $to_goal;			
			$forecast_url = SessionManager::CreateUrl('forecast.php','target='.$personid);	
			
			$percentage_goal_to_date = ($goal_to_date == 0) ? 0 : ($sales_to_date / $goal_to_date) * 100;
			$total_percentage_goal_to_date += $percentage_goal_to_date;
			
			if ($this->download) {			
				$tbody_pdf[] = array($name, $first_meeting, $sales_cycle, round($closing_ratio * 100, 1) . '%', $this->FormatCurrency($average_sale), 
								round($sales_per_month, 1), $this->FormatCurrency($sales_to_date), $this->FormatCurrency($goal_to_date), 
								round($percentage_goal_to_date) . '%', $this->FormatCurrency($annula_goal_amount), round($to_goal, 1). '%');
			} else {				
				$tbody .= '<tr>
							<th style="text-align:right"><a href="' . $forecast_url . '">'.$name.'</a></th>
							<td style="text-align:right" id="first_meeting_avg_' . $key . '">' . round($first_meeting) . '</td>
							<td style="text-align:right" id="sales_cycle_avg_' . $key . '">' . round($sales_cycle) . '</td>
							<td style="text-align:right" id="closing_ratio_avg_' . $key . '">' . round($closing_ratio * 100, 1) . '%</td>
							<td style="text-align:right" id="average_sale_avg_' . $key . '">' . $this->FormatCurrency($average_sale) . '</td>
							<td style="text-align:right" id="sales_per_month_avg_' . $key . '">' . round($sales_per_month, 1) . '</td>
							<td style="text-align:right" id="sales_to_date_' . $key . '">' . $this->FormatCurrency($sales_to_date) . '</td>
							<td style="text-align:right" id="goal_to_date_' . $key . '">' . $this->FormatCurrency($goal_to_date) . '</td>
							<td style="text-align:right" id="percentage_goal_to_date_' . $key . '">' . round($percentage_goal_to_date) . '%</td>
							<td style="text-align:right">' . $this->FormatCurrency($annula_goal_amount) . '</td>						
							<td style="text-align:right" id="to_goal_avg_' . $key . '">' . round($to_goal, 1) . '%</td>
						  </tr>';			
			}		  
		}
		
		$avg_first_meeting = ($total_salespersons == 0) ? 0 : round($total_first_meeting / $total_salespersons, 1);
		$avg_sales_cycle = ($total_salespersons == 0) ? 0 : round($total_sales_cycle / $total_salespersons, 1);		
		$avg_closing_ratio = ($total_salespersons == 0) ? 0 : round($total_closing_ratio * 100 / $total_salespersons, 1);		
		$avg_average_sale = ($total_salespersons == 0) ? 0 : round($total_average_sale / $total_salespersons, 1);
		$avg_sales_per_month = ($total_salespersons == 0) ? 0 : round($total_sales_per_month / $total_salespersons, 1);
		$avg_average_sale = $this->FormatCurrency($avg_average_sale);		
		$avg_to_goal = ($total_salespersons == 0) ? 0 : round($total_to_goal / $total_salespersons, 1);
		$avg_goal_to_date = ($total_salespersons == 0) ? 0 : $total_to_goal / $total_salespersons;		
		$total_sales_to_date = $this->FormatCurrency($total_sales_to_date);		
		$toatl_annula_goal_amount = $this->FormatCurrency($toatl_annula_goal);		
		$total_goal_to_date = $this->FormatCurrency($total_goal_to_date);
		$avg_percentage_goal_to_date = ($total_salespersons == 0) ? 0 : round($total_percentage_goal_to_date / $total_salespersons);
		
		
		if ($this->download) {
			$header = array('', 'First Meetings', 'Sales Cycle', 'Closing Ratio', 'Average Sale', 'Sales Per Month', 
							'Sales To date', 'Goal To date', '% Goal To date', 'Goal Annual', '% Goal Annual');		
			
			$tbody_pdf[] = array('Team Totals', round($total_first_meeting), '', '', '', '', $total_sales_to_date, $total_goal_to_date, '', $toatl_annula_goal_amount, '');
			$tbody_pdf[] = array('Team Averages', $avg_first_meeting, $avg_sales_cycle, $avg_closing_ratio . '%', $avg_average_sale, $avg_sales_per_month, 
						'', '', $avg_percentage_goal_to_date . '%', '', $avg_to_goal . '%');
			
			$this->pdf->FancyTable($header, $tbody_pdf);			
			$this->MonthlyReport($toatl_annula_goal);
		} else {
			
			$total_first_meeting = round($total_first_meeting);
			
			$output = <<<OUTPUT
			
			<div id="roll_ups" align="center">
			<table cellpadding="4" cellspacing="2" border="0" class="fasales">
			<thead>
				<th>&nbsp;</th>
				<th>First <br />Meetings</th>
				<th>Sales <br />Cycle</th>
				<th>Closing <br />Ratio</th>
				<th>Average <br />Sale</th>
				<th>Sales <br />Per Month</th>
				<th>Sales <br />To date</th>			
				<th>Goal <br />To date</th>
				<th>% Goal<br />To date</th>			
				<th>Goal <br />Annual</th>			
				<th>% Goal <br /> Annual</th>			
			</thead>
	
			<tbody>		
				$tbody		
			</tbody>
			<tfoot>
				<tr>
					<th>Team Totals</th>
					<td style="text-align:right;border-top:2px solid black;">$total_first_meeting</td>
					<td class="empty_cell" style="border-top:2px solid black;"></td>
					<td class="empty_cell" style="border-top:2px solid black;"></td>
					<td class="empty_cell" style="border-top:2px solid black;"></td>
					<td class="empty_cell" style="border-top:2px solid black;"></td>
					<td style="text-align:right;border-top:2px solid black;">$total_sales_to_date</td>
					<td style="text-align:right;border-top:2px solid black;">$total_goal_to_date</td>
					<td class="empty_cell" style="border-top:2px solid black;"></td>
					<td style="text-align:right;border-top:2px solid black;">$toatl_annula_goal_amount</td>				
					<td class="empty_cell" style="border-top:2px solid black;"></td>
				</tr>		
				<tr>
					<th>Team Averages</th>
					<td style="text-align:right" id="first_meeting_avg">$avg_first_meeting</td>
					<td style="text-align:right" id="sales_cycle_avg">$avg_sales_cycle</td>
					<td style="text-align:right" id="closing_ratio_avg">$avg_closing_ratio%</td>
					<td style="text-align:right" id="average_sale_avg">$avg_average_sale</td>
					<td style="text-align:right" id="sales_per_month_avg">$avg_sales_per_month</td>
					<td class="empty_cell"></td>
					<td class="empty_cell"></td>
					<td style="text-align:right" id="avg_percentage_goal_to_date">$avg_percentage_goal_to_date%</td>
					<td class="empty_cell"></td>
					<td style="text-align:right" id="to_goal_avg">$avg_to_goal %</td>
				</tr>
							
			</tfoot>
			</table>
			</div>
OUTPUT;
			$output .= '<br><br>';
			
			$output .= $this->MonthlyReport($toatl_annula_goal);
			return $output;		
		}	
	}	
	
	public function GetAllUnreportedSales($target) {

		$sql = "SELECT Period, sum(Sales) Sales, sum(Revenue) Revenue
			    FROM UnreportedSales 
			    WHERE Year = $this->year AND PersonID IN ($target) GROUP BY Period";		
		$sales_rs = DbConnManager::GetDb('mpower')->Execute($sql);

		$this->all_unreported_sales = array_fill(1, $this->period, 0);
		$this->all_unreported_revenue = array_fill(1, $this->period, 0);
		
		if (count($sales_rs) > 0) {
			foreach($sales_rs as $sales) {
				$this->all_unreported_sales[$sales->Period] = $sales->Sales;
				$this->all_unreported_revenue[$sales->Period] = $sales->Revenue;
			} 
		}
	}	
	
	public function GetAllReportedSales($target) {
		 
		$this->all_reported_sales = array_fill(1, $this->period, 0);
		$this->all_reported_revenue = array_fill(1, $this->period, 0);
		$date = '01/01/' . $this->year;
		
		if ($this->period == 12) {
			
			$opp_sql = "SELECT sum(ActualDollarAmount) Amount, count(ActualCloseDate) CloseDates, month(ActualCloseDate) M
						FROM opportunities 
						WHERE opportunities.PersonID IN ($target) AND 
							(category = 6 AND ActualCloseDate >= ? AND ActualCloseDate <= ?)
						GROUP BY month(ActualCloseDate)";
						
			$opp_sql = SqlBuilder()->LoadSql($opp_sql)->BuildSql(array(DTYPE_TIME, $date), array(DTYPE_TIME, $this->fiscal_year_end));
			$closed_opps = DbConnManager::GetDb('mpower')->Execute($opp_sql);
			
			foreach($closed_opps as $opps) {
				$this->all_reported_sales[$opps->M] = $opps->CloseDates;
				$this->all_reported_revenue[$opps->M] = $opps->Amount;
			} 
		} else {
		
			$opp_sql = "SELECT ActualDollarAmount, ActualCloseDate
						FROM opportunities 
						WHERE opportunities.PersonID IN ($target) AND 
							(category = 6 AND ActualCloseDate >= ? AND ActualCloseDate <= ?)";
						
			$opp_sql = SqlBuilder()->LoadSql($opp_sql)->BuildSql(array(DTYPE_TIME, $date), array(DTYPE_TIME, $this->fiscal_year_end));
			$closed_opps = DbConnManager::GetDb('mpower')->Execute($opp_sql);
			
			foreach($closed_opps as $opps) {
				$temp = strtotime($opps->ActualCloseDate);
				foreach($this->period_list as $key=>$temp_period) {
					if($temp < $temp_period['Date']) {
						$this->all_reported_sales[$key] += 1;
						$this->all_reported_revenue[$key] += $opps->ActualDollarAmount;
						break;
					}
				}
			} 		
		}
	}

	public function GoalsToDate($person_id) {
		$seasonality = $this->getSeasonality($person_id);		
		
		$sql = "SELECT CompanySetGoal, PersonalGoal FROM Goal WHERE PersonID = ? AND Year = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $person_id), array(DTYPE_INT, $this->year));		
		$goal = DbConnManager::GetDb('mpower')->Execute($sql);
				
		$company_goal = 0;		
		if (count($goal) > 0) $company_goal = $goal[0]->CompanySetGoal;
		
		$goal_to_date = 0;
		
		for ($month = 1; $month <= $this->current_period;  ++$month) {
			if ($month == $this->current_period) {
				$goal_to_date += ($company_goal * $seasonality[$month] / 100) * date('j') / 31;
			} else {
				$goal_to_date += $company_goal * $seasonality[$month] / 100;
			}
		}	
		
		return round($goal_to_date);
	}

	function FormatCurrency($amount) {
		return '$'.number_format($amount);
	}
	
	function CalculateCarryForward($period, $required_amount) {
		$carry_forward = 0;
		
		$temp_required_amount = $required_amount;
		
		for ($id = 1; $id < $period; $id++) {
			$temp_actual_amount = $this->all_reported_revenue[$id] + $this->all_unreported_revenue[$id];
			$temp_amount_to_add = $temp_required_amount - $temp_actual_amount;
			$carry_forward += $temp_amount_to_add;			
		}
		
		$temp_period = $this->period - ($period - 1);
		$carry_forward = ($temp_period == 0) ? 0 : $carry_forward / $temp_period;		
		
		return $carry_forward;
	}
	
	function GetRequiredAmount($goal_amount) {
		$monthly_quota_amount = $this->GetMonthlyQuotaAmount($goal_amount);
		
		$required_amount = array_fill(1, $this->period, 0);
		$monthly_requirement = $goal_amount / $this->period;
		$current_carry_forward = 0;		
		
		for ($id = 1; $id <= $this->period; $id++) {
			
			if ($id > $this->current_period) {
				$carry_forward = $current_carry_forward;
			} else {				
				$carry_forward = $this->CalculateCarryForward($id, $monthly_quota_amount[1]);
				$current_carry_forward = $carry_forward;
			}			
			
			$carry_forward = ($carry_forward < 0)? 0 : $carry_forward;			
			$required_amount[$id] = $monthly_quota_amount[$id] +  $carry_forward;			
		}		
		
		return $required_amount;
	}
	
	function MonthlyReport($toatl_annula_goal) {		
		
		$unreporeted_sales_content = '<td style="background-color:#FFFFFF; text-align:left; " class="facaption falefttd"><nobr>Unreported Sales ##</nobr></td>';
		$unreporeted_revenue_content = '<td style="background-color:#FFFFFF; text-align:left;" class="facaption falefttd"><nobr>Unreported Sales $$</nobr></td>';
		$reported_sales = '<td class="facaption falefttd" style=" text-align:left;border-top: 2px solid black;"><nobr>Reported Sales ##</nobr></td>';
		$reported_revenue = '<td class="facaption falefttd" style=" text-align:left;"><nobr>Reported Sales $$</nobr></td>';
		$actual_sales = '<td class="facaption falefttd" style=" text-align:left;border-top: 2px solid black;"><nobr>Total Sales ##</nobr></td>';
		$actual = '<td style="text-align:left;" class="falefttd">Total Sales $$</td>';
		$required = '<td style="text-align:left;" class="falefttd">Required</td>';
		if ($this->download) $tbody = array();
		$monthly_quota = '<td style="text-align:left;" class="falefttd">Monthly Quota</td>';
		
		$fourth_quarter_colsan = 3;
		
		if ($this->period == 13) {			
			$headerArray = array(1 => 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',  'November', 'December', '');
		} else {			
			$headerArray = array(1 => 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',  'November', 'December');
		}
		
		if (count($this->period_list)) {
			foreach ($this->period_list as $key=>$period) {
				
				if(trim($period['Label']) != '') {				 	
					$headerArray[$key] = $period['Label'];				
				}
			}
			
			if (count($this->period_list) == 12) $fourth_quarter_colsan = 3;
			else $fourth_quarter_colsan = 4;
		}
		
		$required_amount = $this->GetRequiredAmount($toatl_annula_goal);
		
		$header = $percentage_monthly_goal = $percentage_monthly_quota = $percentage_quaterly_goal = $required_quarter = $percentage_quaterly_quota = $revenue_quarter_column = '';
		
		if ($this->download) {
			$pdf_quarterly_total_sales[] = 'Quarterly Total Sales $$';
			$pdf_quarterly_quota[] = 'Quarterly Quota';
			$pdf_percentage_to_quarterly[] = '% to Quarterly Quota';
		}
		
		$monthly_quota_amount_list = $this->GetMonthlyQuotaAmount($toatl_annula_goal);
		
		for ($quarter = 1; $quarter <= 4; ++$quarter) {
	
			$total_sales_amount = $total_revenue_amount = $required_quarter_amount = 0;
			$quarter_end = $quarter * 3;
			$quarter_start = $quarter_end - 2;		
			
			if ($this->period == 13 && $quarter == 4) $quarter_end = 13;			
			
			$count = 0;
			for ($i = $quarter_start; $i <= $quarter_end; ++$i) {				
				$all_unreported_sales = (int)$this->all_unreported_sales[$i];
				$all_unreported_revenue = $this->all_unreported_revenue[$i];			
				$all_reported_sales = $this->all_reported_sales[$i];
				$all_reported_revenue = $this->all_reported_revenue[$i];
			
				$total_sales = $all_reported_sales + $all_unreported_sales;
				$total_revenues = $all_unreported_revenue + $all_reported_revenue;				
				
				$total_sales_amount += $total_sales;		
				$total_revenue_amount += $total_revenues;
				$required_quarter_amount += $monthly_quota_amount_list[$i];				
				$count++;	
			}
			
			$style = ($quarter < 4) ? ' style="border-right: 2px solid black;" ' : ' ';			
			$required_quarter .= '<td colspan="' . $count . '" ' . $style. '><nobr><span class="green" id="required_quater_' . $quarter . '">' . $this->FormatCurrency($required_quarter_amount) . '</span></nobr></td>';

			$quaterly_quota_percentage = ($required_quarter_amount == 0) ? 0 : $total_revenue_amount / $required_quarter_amount * 100;			
			$quaterly_quota_percentage = round($quaterly_quota_percentage);

			$color = ($quaterly_quota_percentage < 100) ? 'class="fared"' : '';
			$percentage_quaterly_quota	 .= '<td colspan="' . $count . '" ' . $style. '><span ' . $color . ' id="quater_quota_' . $quarter . '">' . $quaterly_quota_percentage . '%</span></td>';
			$revenue_quarter_column	 .= '<td colspan="' . $count . '" ' . $style. '><span id="actual_quater_' . $quarter . '">' . $this->FormatCurrency($total_revenue_amount) . '</span></td>';
			
			if ($this->download) {
				
				$pdf_quarterly_total_sales[] = $this->FormatCurrency($total_revenue_amount);
				$pdf_quarterly_quota[] = $this->FormatCurrency($required_quarter_amount);
				$pdf_percentage_to_quarterly[] = $quaterly_quota_percentage . '%';			
			}
		}		
		
		if ($this->download) {
			$body[] = $pdf_quarterly_total_sales;
			$body[] = $pdf_quarterly_quota;		
			$body[] = $pdf_percentage_to_quarterly;		
		}
		
		$total_unreported_revenue = $total_reported_revenue = 0;
				
		for ($id = 1; $id <= $this->period; $id++) {			
			$style = '';
			$style_more = 'style="border-top: 2px solid black;"';
			
			if ($id == 3 or $id == 6 or $id == 9) {				
				$style = ' style="border-right: 2px solid black;" ';
				$style_more = 'style="border-top: 2px solid black;border-right: 2px solid black;"';
			}			
			
			$all_unreported_sales = (int)$this->all_unreported_sales[$id];
			$all_unreported_revenue = $this->all_unreported_revenue[$id];		
			$all_reported_sales = $this->all_reported_sales[$id];
			$all_reported_revenue = $this->all_reported_revenue[$id];			
			$total_unreported_revenue += $all_unreported_revenue;
			$total_sales = $all_reported_sales + $all_unreported_sales;
			$total_revenues = $all_unreported_revenue + $all_reported_revenue;
			$monthly_percentage = ($required_amount[$id] == 0) ? 0 : ($total_revenues / $required_amount[$id]) * 100;

			if ($_SESSION['tree_obj']->IsSeniorMgr($this->target) || $_SESSION['tree_obj']->IsManager($this->target)) {			
				$unreporeted_sales_content .= "<td $style>" . $all_unreported_sales . '</td>';
				$unreporeted_revenue_content .= "<td $style>" . $this->FormatCurrency($all_unreported_revenue) . '</td>';			
						
				$reported_sales .= "<td $style_more>".'<span class="fablue" >' . $all_reported_sales . '</span></nobr></td>';
				$reported_revenue .= "<td $style>".'<span class="fablue">' . $this->FormatCurrency($all_reported_revenue) . '</span></nobr></td>';				
				$actual_sales .= "<td $style_more><span>" . $total_sales . '</span></td>';
				$actual .= "<td $style><span>" . $this->FormatCurrency($total_revenues) . '</span></td>';
			} else {			
				$unreporeted_sales_content .= "<td $style>" . '<div style="text-align:center; width:100%" class="edit" id="unreported_sales_div_' . $id . '" tabindex="' . $id . '" onKeyDown="editField(event,\'sales\',' . $id . ');" onClick="edit(\'sales\',' . $id . ')">' . (int)$this->unreported_sales[$id] . '</div><input style="display:none;font-size:10px; width:90%" type="text" id="unreported_sales_' . $id . '" value="' . (int)$this->unreported_sales[$id] . '" size="5" onKeyDown="commitField(event,\'sales\',' . $id . ');" onBlur="commit(\'sales\',' . $id . ')"></td>';
				$unreporeted_revenue_content .= "<td $style>" . '<div style="text-align:center; width:100%" class="edit" id="unreported_revenue_div_' . $id . '" tabindex="' . $id . '" onKeyDown="editField(event,\'revenue\',' . $id . ');"  onClick="edit(\'revenue\',' . $id . ')">' . $this->FormatCurrency($this->unreported_revenue[$id]) . '</div><input style="display:none; font-size:10px; width:95%" type="text" id="unreported_revenue_' . $id . '" value="' . $this->unreported_revenue[$id] . '" size="5" onKeyDown="commitField(event,\'sales\',' . $id . ');" onBlur="commit(\'revenue\',' . $id . ')"></td>';
				$reported_sales .= "<td $style_more>" . '<span class="fablue" id="reported_sales_' . $id . '">'.$this->reported_sales[$id].'</span></nobr></td>';
				$reported_revenue .= "<td $style>" . '<span class="fablue" id="reported_revenue_' . $id . '">' . $this->FormatCurrency($this->reported_revenue[$id]) . '</span></nobr></td>';
				$actual_sales .=  "<td $style_more>" . '<span id="actual_sales_' . $id . '">0</span></td>';
				$actual .=  "<td $style>" . '<span id="actual_' . $id . '">$0</span></td>';				
			}
			
			$required .= '<td class="monthly_quota"' . $style .'><span id="required_revenue_' . $id . '">' . $this->FormatCurrency($required_amount[$id]) . '</span></td>';
			$monthly_quota_amount = $monthly_quota_amount_list[$id];			
			
			$monthly_quota .= '<td class="monthly_quota"' . $style .'><span id="required_quota_' . $id . '">' . $this->FormatCurrency($monthly_quota_amount) . '</span></td>';
			$show_date = ($this->period == 13) ? '<br>' . date('m/d/Y',$this->period_list[$id]['Date']) : '';
			$header .= '<td class="fathead"' . $style .'>' . $headerArray[$id] . $show_date . '</td>';
			$percentage_to_monthly_quota = ($monthly_quota_amount == 0) ? 0 : ($total_revenues / $monthly_quota_amount) * 100;	
			$mothly_goal_css = ($monthly_percentage < 100) ? ' class="fared" ' : '';
			$mothly_quota_css = ($percentage_to_monthly_quota < 100) ? ' class="fared" ' : '';
			$percentage_monthly_goal .= '<td ' . $style_more . '><span ' . $mothly_goal_css . ' id="percentage_monthly_goal_' . $id . '">' . round($monthly_percentage) . '%</span></td>';			
			$percentage_monthly_quota .= '<td ' . $style_more . '><span ' . $mothly_quota_css . ' id="percentage_monthly_quota_' . $id . '">' . round($percentage_to_monthly_quota) . '%</span></td>';
			$percentage_quaterly_goal .= '<td style="text-align:center"><span>' . round($percentage_quaterly_goal) . '%</span></td>';
			
			if ($this->download) {
				$pdf_unreported_sales[] = $all_unreported_sales;
				$pdf_unreported_revenue[] = $this->FormatCurrency($this->all_unreported_revenue[$id]);
				$pdf_reported_sales[] = $all_reported_sales;
				$pdf_reported_revenue[] = $this->FormatCurrency($all_reported_revenue);			
				$pdf_total_sales[] = $total_sales;
				$pdf_total_revenues[] = $this->FormatCurrency($total_revenues);
				$pdf_required_amount[] = $this->FormatCurrency($required_amount[$id]);			
				$pdf_monthly_quota_amount[] = $this->FormatCurrency($monthly_quota_amount);			
				$pdf_percentage_to_required[] = round($monthly_percentage) . '%';
				$pdf_percentage_to_monthly_quota[] = round($percentage_to_monthly_quota) . '%';
			}
		}
		
		if ($this->download) {
			$tbody[] = array_merge(array('Unreported Sales ##'), $pdf_unreported_sales);
			$tbody[] = array_merge(array('Unreported Sales $$'), $pdf_unreported_revenue);		
			$tbody[] = array_merge(array('Reported Sales ##'), $pdf_reported_sales);
			$tbody[] = array_merge(array('Reported Sales $$'), $pdf_reported_revenue);		
			$tbody[] = array_merge(array('Total Sales ##'), $pdf_total_sales);
			$tbody[] = array_merge(array('Total Sales $$'), $pdf_total_revenues);
			$tbody[] = array_merge(array('% to Required'), $pdf_percentage_to_required);
			$tbody[] = array_merge(array('Required'), $pdf_required_amount);		
			$tbody[] = array_merge(array('% to Monthly Quota'), $pdf_percentage_to_monthly_quota);		
			$tbody[] = array_merge(array('Monthly Quota'), $pdf_monthly_quota_amount);
		}
				
		for ($month = 1; $month <= $this->current_period; ++$month) {
			$total_reported_revenue += $this->all_reported_revenue[$month];
		}
		
		$total_sales_todate = $total_unreported_revenue + $total_reported_revenue;
		$percentage_to_annual_goal = ($toatl_annula_goal == 0) ? 0 : $total_sales_todate/ $toatl_annula_goal * 100;
		$total_sales_todate = $this->FormatCurrency($total_sales_todate);
		$total_goal = $this->FormatCurrency($toatl_annula_goal);
		$percentage_to_annual_goal = round($percentage_to_annual_goal);		
		$year_completed = "$this->dayofyear %";
		$style = '';
		
		if ($this->seasonality_set) { 
			$year_completed = '';
			$style = 'background-color:#CCCCCC';
		}
		
		if ($this->download) {			
			$this->pdf->AddPage();
			$this->pdf->SetLeftMargin(0.4);
			$this->pdf->SetRightMargin(0.4);	
			$this->pdf->Cell(0, 0.2, "Month By Month Sales and Required Sales to Reach $total_goal", 1, 1, 'C'); 	
			
			if ($this->period == 13)
				$width = array_merge(array(1.3), array_fill(1, 13, .7));
			else 
				$width = array_merge(array(1.3), array_fill(1, 12, .74));
			
			$headerArray = array_merge(array(''), $headerArray);
			$this->pdf->FancyTable($headerArray, $tbody, $width);		
			
			
			if ($this->period == 13) 			
				$width = array(1.3, 2.1, 2.1, 2.1, 2.8);
			else 
				$width = array(1.3, 2.22, 2.22, 2.22, 2.22);
				
			$headerArray = array('', 'First Quarter', 'Second Quarter', 'Third Quarter', 'Fourth Quarter');
			$this->pdf->FancyTable($headerArray, $body, $width);
	
			if ($this->period == 13) 			
				$width = array(1.3, 4.2, 2.1, 2.8);
			else 
				$width = array(1.3, 4.44, 2.22, 2.22);			
			
			$header = array('', 'January 1st to Today', '% to Annual Goal', '% Year Completed');
			$tbody_content[] = array('Annual Total Sales $$', $total_sales_todate, $percentage_to_annual_goal . '%', $this->dayofyear . '%');		
			$this->pdf->FancyTable($header, $tbody_content, $width);
		}	else { 
			
			$output = <<<OUTPUT
			<div class="large_title">Month By Month Sales and Required Sales to Reach <span id="goal_amount">$total_goal</span></div>
			
			<div style="text-align:center" id="sales_detail">
			<table cellpadding="4" cellspacing="0" width="95%" border="0" class="fasales">
			
			<tr valign="top">
				<td class="falefttd">&nbsp;</td>
				$header
			</tr>
			
			<tr valign="top" style="background-color:#CCCCCC">$unreporeted_sales_content</tr>
			<tr valign="top" style="background-color:#CCCCCC">$unreporeted_revenue_content</tr>
			
			<tr valign="top">$reported_sales</tr>
			<tr valign="top">$reported_revenue</tr>
			<tr valign="top">$actual_sales</tr>
			<tr valign="top">$actual</tr>
			
			<tr valign="top">
				<td style="text-align:left;border-top: 2px solid black;" class="falefttd"><nobr>% to Required</nobr></td>
				$percentage_monthly_goal
			</tr>
			
			<tr valign="top">$required</tr>
			
			<tr valign="top">
				<td style="text-align:left;border-top: 2px solid black;" class="falefttd"><nobr>% to Monthly Quota</nobr></td>
				$percentage_monthly_quota
			</tr>
					
			<tr valign="top">$monthly_quota</tr>
			
			<tr valign="top">
				<td style="text-align:left;" class="falefttd">&nbsp;</td>
				<td colspan="3" class="fathead" style="border-right: 2px solid black;"><nobr>First Quarter</nobr></td>
				<td colspan="3" class="fathead" style="border-right: 2px solid black;"><nobr>Second Quarter</nobr></td>
				<td colspan="3" class="fathead" style="border-right: 2px solid black;"><nobr>Third Quarter</nobr></td>
				<td colspan="$fourth_quarter_colsan" class="fathead"><nobr>Fourth Quarter</nobr></td>
			</tr>
			
			<tr valign="top">
				<td style="text-align:left;" class="falefttd">Quarterly Total Sales $$</td>
				$revenue_quarter_column	
			</tr>
			
			<tr valign="top">
				<td style="text-align:left;" class="falefttd">Quarterly Quota</td>
				$required_quarter
			</tr>
			
			<tr valign="top">
				<td class="facaption falefttd" style="text-align:left;"><nobr>% to Quarterly Quota</nobr></td>
				$percentage_quaterly_quota
			</tr>		
			
			<tr valign="top">
				<td class="falefttd">&nbsp;</td><td colspan="6" >January 1st to Today</td>
				<td colspan="3">% to Annual Goal</td><td colspan="$fourth_quarter_colsan" >% Year Completed</td>
			</tr>
	
			<tr valign="top">
				<td style="text-align:left;" class="falefttd">Annual Total Sales $$</td>
				<td colspan="6"><nobr><span id="actual_total">$total_sales_todate</span></nobr></td>
				<td colspan="3" style="text-align:center"><span id="annual_goal_completed">$percentage_to_annual_goal%</span></td>
				<td colspan="$fourth_quarter_colsan" style="text-align:center; $style"><span id="year_completed">$year_completed</span></td>
			</tr>		
				
			
			</table>
			</div>
			</form>
OUTPUT;
			return $output;
		}
	}

	function SetFiscalYearEnd() {
		
		$sql = "SELECT convert(varchar, FiscalYearEndDate, 101) FiscalYearEndDate
				FROM FiscalYear 
				WHERE CompanyID = ? AND 
					  Year = ? AND 
					  Period IS NULL";
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['mpower_companyid']), array(DTYPE_INT, $this->year));
		$rs = DbConnManager::GetDb('mpower')->Execute($sql);		

		if (count($rs) > 0) $this->fiscal_year_end = $rs[0]->FiscalYearEndDate;
		else $this->fiscal_year_end = "12/31/" . $this->year;

	}
	
	
	function Info() {
		$today = date('m/d/Y');
		$salesperson_name = $_SESSION['tree_obj']->GetInfo($this->target, 'FirstName').' '.$_SESSION['tree_obj']->GetInfo($this->target, 'LastName');		
		$start_date = date('m/d/Y', strtotime($_SESSION['tree_obj']->GetInfo($this->target, 'StartDate')));
			
		$average_sale = $this->FormatCurrency($this->average_sale);
		
		if ($this->download == 'Salesperson') {
			$header = array('', '', '', '', '');
			$body = array();
			$body[] = array('Salesperson', $salesperson_name, '', 'First Meetings', $this->first_meetings);
			$body[] = array('Fiscal Year End', $this->fiscal_year_end, '', 'Sales Cycle', $this->sales_cycle);
			$body[] = array('Start Date', $start_date, '', 'Closing Ratio', $this->closing_ratio . '%');
			$body[] = array('Report Date', $today, '', 'Average Sale', $average_sale);	
			$body[] = array('', '', '', 'Sales per Month', $this->sales_per_month);	
			
			$this->pdf->BasicTable($header, $body, array(1.4, 1.4, 4.6, 1.4, 1.4));
		} else {				
		
		$output =<<<OUTPUT
			<div style="float:left">
			<table border="0" cellpadding="4" cellspacing="0">
			<tr><td class="facaption">Salesperson</td><td><span class="fablue"><nobr>$salesperson_name</nobr></td></tr>
			<tr><td class="facaption">Fiscal Year End</td><td><span class="fablue">$this->fiscal_year_end</span></td></tr>
			<tr><td class="facaption">Start Date</td><td><span class="fablue">$start_date</span></td></tr>
			<tr><td class="facaption">Report Date</td><td><span class="fablue">$today</span></td></tr>
			</table>
			</div>
			
			<div align="right">
			<table class="fatablert" border="0">
			<tr>
				<th></th>
				<th class="btop bleft">Current</th>
				<th class="btop bleft bright" colspan="2" align="center">Adjusted</th>
			</tr>
			<tr>
				<td class="facaption">First Meetings</td>
				<td class="bleft fablue"><span id="first_meeting_orig_slider">$this->first_meetings</span></td>
				<td class="bleft"><span id="first_meeting_slider">$this->first_meetings</span></td>
				<td class="bright">
					<div id='faslider1' class='ui-slider-1' style="float:left; border:0px solid black">
					<div class='ui-slider-handle'></div>
					</div>
					<div style="border:0px solid black">	
						<img src="images/prev.gif" onClick="shiftFirstMeeting(0)" style="padding:0px;margin-right:-3px; cursor:pointer">
						<img src="images/next.gif" onClick="shiftFirstMeeting(1)" style="padding:0px;margin-left:-3px; cursor:pointer">
					</div>
				</td>
			</tr>
			
			<tr><td class="facaption">Sales Cycle</td><td class="bleft fablue">
			<span id="sales_cycle_original">$this->sales_cycle</span></td>
			<td class="bleft"><span id="sales_cycle_slider">$this->sales_cycle</span></td>
			<td class="bright">
				<div id='faslider2' class='ui-slider-1' style="float:left; border:0px solid black">
				<div class='ui-slider-handle'></div>
				</div>
				<div style="border:0px solid black">
					<img src="images/prev.gif" onClick="shiftSalesCycle(0)" style="padding:0px;margin-right:-3px; cursor:pointer">
					<img src="images/next.gif" onClick="shiftSalesCycle(1)" style="padding:0px;margin-left:-3px; cursor:pointer">
				</div>
				
			</td></tr>
			
			<tr>
			<td class="facaption">Closing Ratio</td><td class="bleft fablue"><span id="closing_ratio_ori_slider">$this->closing_ratio</span>%</td>
			<td class="bleft"><span id="closing_ratio_slider">$this->closing_ratio</span>%</td>
			<td class="bright">
				<div id='faslider3' class='ui-slider-1' style="float:left; border:0px solid black">
				<div class='ui-slider-handle'></div>
				</div>
				<div style="border:0px solid black">
					<img onClick="shiftClosingRatio(0)" src="images/prev.gif" style="padding:0px;margin-right:-3px; cursor:pointer">					
					<img onClick="shiftClosingRatio(1)" src="images/next.gif" style="padding:0px;margin-left:-3px; cursor:pointer">
				</div>			
			</td></tr>
			
			<tr>
			<td class="facaption">Average Sale</td><td class="bleft"><span class="fablue" id="average_sales_ori_slider">$average_sale</span></td>
			<td class="bleft" style="width:90px"><span id="average_sales_slider" style="cursor:pointer" onClick="editAdjustment('average_sales_slider', 'average_sales_slider_input')">$average_sale</span><input type="text" onBlur="commitAdjustment('average_sales_slider', 'average_sales_slider_input')" style="display:none; width:80px; font-size:10px;" name="average_sales_slider_input" id="average_sales_slider_input"> </td>
			<td class="bright" style="width:240px">
			<div id='faslider4' class='ui-slider-1' style="float:left; border:0px solid black">
			<div class='ui-slider-handle'></div>
			</div>
			<div style="border:0px solid black">
				<img src="images/prev.gif" onClick="shiftAvgSales(0)" style="padding:0px;margin-right:-3px; cursor:pointer">
				<img onClick="shiftAvgSales(1)" src="images/next.gif" style="padding:0px;margin-left:-3px; cursor:pointer">
			</div>
			</td></tr>
			
			<tr>
			<td class="facaption">Sales per Month</td>
			<td class="bbottom bleft"><span class="fablue" id="monthly_sales">$this->sales_per_month</span></td>
			<td class="bbottom bleft">	
			<span id="monthly_sales_slider">$this->sales_per_month</span></td>
			<td class="bright bbottom"></td>
			</tr>
			</table>
			</div>
OUTPUT;
			return $output;
		}
		
	}
	
	
	function SetGoal() {
		
		$sql = "SELECT CompanySetGoal, PersonalGoal FROM Goal WHERE PersonID = ? AND Year = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->target), array(DTYPE_INT, $this->year));		
		$goal = DbConnManager::GetDb('mpower')->Execute($sql);
		
		$this->company_goal = 0;
		$this->personal_goal = 0;
		
		if (count($goal) > 0) {
			 
			$this->company_goal = $goal[0]->CompanySetGoal;
			$this->personal_goal = ($goal[0]->PersonalGoal >= $goal[0]->CompanySetGoal) ?$goal[0]->PersonalGoal : $this->company_goal;
		}
	}
	
	
	function GetSalesPersonsGoal($personid) {
		$sql = "SELECT P.PersonID, CompanySetGoal from people P LEFT JOIN Goal G ON P.PersonID = G.PersonID WHERE (P.SupervisorID = ? OR P.PersonID = ?)  AND Year = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $personid), array(DTYPE_INT, $personid), array(DTYPE_INT, $this->year));
		$salespersons_rs = DbConnManager::GetDb('mpower')->Execute($sql);
		
		$annual_goals = array();
		
		foreach ($salespersons_rs as $salesperson) {
			$annual_goals[$salesperson->PersonID] = $salesperson->CompanySetGoal;		
		}
		
		return $annual_goals;
	}
	
	
	function CalculateQuarterAmount($actual_array, $quarter) {
		$quarter_end = $quarter * 3;
		$quarter_start = $quarter_end - 3;
		$quarter_amount = 0.0;

		if ($this->period == 13 && $quarter == 4) $quarter_end = 13;
		
		for ($i = $quarter_start; $i < $quarter_end; ++$i) {
			$quarter_amount += $actual_array[$i + 1];
		}
		
		return $quarter_amount;
	}
	
	
	function GetMonthlyQuotaAmount($goal) {	
		$monthly_quota_amount = array();
		$monthly_quota_amount = array_fill(1, 13, 0);
		
		if ($_SESSION['tree_obj']->IsSeniorMgr($this->target) || $_SESSION['tree_obj']->IsManager($this->target)) {
			$target = $_SESSION['tree_obj']->GetRecord($this->target);
			foreach ($target['children'] as $key=>$personid) {			
				
				if ($_SESSION['tree_obj']->IsSeniorMgr($personid) || $_SESSION['tree_obj']->IsManager($personid)) {
					$salesperson_list = $_SESSION['tree_obj']->GetRecord($personid);
					$salesperson_list = (isset($salesperson_list['children'])) ? $salesperson_list['children'] : array();	
				} else {
					$salesperson_list = array($personid);
				}				
				
				foreach ($salesperson_list as $salespersonid) {				
					$salesperson_seasonality = $this->GetSeasonality($salespersonid);				
					
					$sql = "SELECT CompanySetGoal FROM Goal WHERE PersonID = ? AND Year = ?";
					$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $salespersonid), array(DTYPE_INT, $this->year));		
					$goal = DbConnManager::GetDb('mpower')->Execute($sql);
										
					$company_goal = 0;
					if (count($goal) > 0) {
						$company_goal = $goal[0]->CompanySetGoal;
					}
									
					foreach	($salesperson_seasonality as $key=>$seasonality) {						
						$monthly_quota_amount[$key] += $company_goal * $seasonality / 100;
					}
				}			
			}		
		} else {		
			if (count($this->seasonality)) {				
				foreach ($this->seasonality as $key=>$seasonality) {		
					$monthly_quota_amount[$key] = $goal * $seasonality / 100;
				}
			}
		}		
		return $monthly_quota_amount;	
	}
	
	
	function Goals() {
		$this->SetGoal();

		if ($this->download == 'Salesperson') {
			$monthly_quota = $this->GetMonthlyQuotaAmount($this->company_goal);			
			$personal_monthly_quota = $this->GetMonthlyQuotaAmount($this->personal_goal);
			
			$unreported_sales = $this->getTotal($this->unreported_sales);
			$unreported_revenue = $this->getTotal($this->unreported_revenue);
			
			$reported_sales = $this->getTotal($this->reported_sales);
			$reported_revenue = $this->getTotal($this->reported_revenue);
			
			$total_sales = $unreported_sales + $reported_sales;
			$total_revenue = $unreported_revenue + $reported_revenue;
			
			$unreported_revenue_amount = $this->FormatCurrency($unreported_revenue);
			$reported_revenue_amount = $this->FormatCurrency($reported_revenue);
			$total_revenue_amount = $this->FormatCurrency($total_revenue);
			
			$quarter_unreported_revenue = $this->CalculateQuarterAmount($this->unreported_revenue,  $this->current_quarter);
			$quarter_reported_revenue = $this->CalculateQuarterAmount($this->reported_revenue,  $this->current_quarter);						
			$current_quater_amount = $quarter_unreported_revenue + $quarter_reported_revenue;			
			$current_company_quater_required = $this->CalculateQuarterAmount($monthly_quota, $this->current_quarter);
			$current_person_quater_required = $this->CalculateQuarterAmount($personal_monthly_quota, $this->current_quarter); 
												
			$to_reach_this_quarter_goal = $current_company_quater_required - $current_quater_amount;
			if($to_reach_this_quarter_goal < 0) $to_reach_this_quarter_goal = 0;

			$person_to_reach_this_quarter_goal = $current_person_quater_required - $current_quater_amount;
			if($person_to_reach_this_quarter_goal < 0) $person_to_reach_this_quarter_goal = 0;

			$month_unreported_revenue = $this->unreported_revenue[$this->current_period];
			$month_reported_revenue = $this->reported_revenue[$this->current_period];
			$monthly_amount = $month_unreported_revenue + $month_reported_revenue;	
			$month_required = $monthly_quota[$this->current_period - 1];
			$personal_month_required = $personal_monthly_quota[$this->current_period - 1];
			
			$person_to_reach_this_month_goal = $personal_month_required - $monthly_amount;
			if($person_to_reach_this_month_goal < 0) $person_to_reach_this_month_goal = 0;
			
			$company_to_reach_this_month_goal = $month_required - $monthly_amount;
			
			
			if($company_to_reach_this_month_goal < 0) $company_to_reach_this_month_goal = 0;			
			
			$required_to_reach_year_goal = $this->company_goal - $total_revenue;			
			$person_required_to_reach_year_goal = $this->personal_goal - $total_revenue;	
			
			//echo 		$required_to_reach_year_goal;

			if($required_to_reach_year_goal < 0) $required_to_reach_year_goal = 0;
			if($person_required_to_reach_year_goal < 0) $person_required_to_reach_year_goal = 0;

			$company_to_reach_this_month_goal = $this->FormatCurrency($company_to_reach_this_month_goal);
			$person_required_to_reach_year_goal = $this->FormatCurrency($person_required_to_reach_year_goal);			

			$company_to_reach_this_year_goal = $this->FormatCurrency($required_to_reach_year_goal);
			$person_to_reach_this_month_goal = $this->FormatCurrency($person_to_reach_this_month_goal);			
			$person_to_reach_this_quarter_goal = $this->FormatCurrency($person_to_reach_this_quarter_goal);
			$to_reach_this_quarter_goal = $this->FormatCurrency($to_reach_this_quarter_goal);

			
			$to_annual = ($this->company_goal == 0) ? 0 : $total_revenue / $this->company_goal * 100;
			$above_below_annual = 100 - $to_annual;
		
			if ($above_below_annual <= 0) {				
				$above_below_annual = ($above_below_annual == 0) ? 0 : (-1) * $above_below_annual;
			} else {
				$color = 'red';
			}
			
			$to_annual = round($to_annual);
			$above_below_annual = round($above_below_annual);
						
			$this->pdf->Ln();			
			$this->pdf->Cell(0, 0.2, 'Goal Analysis', 1, 1, 'C');

			$header = array('', 'Company Set Goal', 'Personal Goal');
			$body = array();
			$body[] = array('Annual Goals', $this->FormatCurrency($this->company_goal), $this->FormatCurrency($this->personal_goal));		
			$this->pdf->BasicTable($header, $body, array(1.4, 1.4, 1.6));

			$header = array('', 'Company', 'Personal');
			$body = array();
			$body[] = array('M-Power Reported Sales ##', $reported_sales, $reported_sales);		
			$body[] = array('M-Power Reported Sales $$', $reported_revenue_amount, $reported_revenue_amount);
			
			$body[] = array('', '', '');
				
			$body[] = array('Unreported Sales ##', $unreported_sales, $unreported_sales);	
			$body[] = array('Unreported Sales $$', $unreported_revenue_amount, $unreported_revenue_amount);
			
			$body[] = array('', '', '');
				
			$body[] = array('Total Sales to Date ##', $total_sales, $total_sales);	
			$body[] = array('Total Sales to Date $$', $this->FormatCurrency($total_revenue),$this->FormatCurrency($total_revenue));
			
			$body[] = array('', '', '');
			
			$body[] = array('Required $$ to Reach This Month\'s Goal', $company_to_reach_this_month_goal, $person_to_reach_this_month_goal);	
			$body[] = array('Required $$ to Reach This Quarter\'s Goal', $to_reach_this_quarter_goal, $person_to_reach_this_quarter_goal);
			$body[] = array('Required $$ to Reach This Year\'s Goal', $company_to_reach_this_year_goal, $person_required_to_reach_year_goal);	
			
			$this->pdf->BasicTable($header, $body, array(3, 2, 2));
			//$this->pdf->Ln();
			$this->pdf->AddPage();			
			$this->pdf->Cell(0, 0.2, 'Projections', 1, 1, 'C');

			$header = array('', 'To Annual', 'Below/Above Annual');
			$body = array();
			$body[] = array('Projected Percentage', $to_annual . '%', $above_below_annual . '%');		
			$this->pdf->BasicTable($header, $body, array(1.4, 1.4, 1.6));

			$header = array('', 'History', 'Projected', 'Miss/Over');
			$body = array();
			$fiscal_year_total =  0;			
			
			$quarter_array = array(1 => '1st', '2nd', '3rd', '4th');
			for ($quarter = 1; $quarter<= 4; ++$quarter) {
			

				$quarter_unreported_revenue = $this->CalculateQuarterAmount($this->unreported_revenue, $quarter);
				$quarter_reported_revenue = $this->CalculateQuarterAmount($this->reported_revenue, $quarter);						
				$amount = $quarter_unreported_revenue + $quarter_reported_revenue;	
				
				$required = $this->CalculateQuarterAmount($monthly_quota, $quarter); 
				
				$miss_over = $amount - $required;
				
				if ($miss_over < 0) {
					$miss_over = (-1) * $miss_over;
					$color = 'red';
				}
				
				if ($quarter == $this->current_quarter) {					
					$fiscal_year_total += $amount;
					$month_unreported_revenue = $this->unreported_revenue[$this->current_period];
					$month_reported_revenue = $this->reported_revenue[$this->current_period];
					$monthly_amount = $month_unreported_revenue + $month_reported_revenue;	
					$month_required = $monthly_quota[$this->current_period - 1];
					$today_to_current_month_end = ($this->days_left_this_month / 31) * ($this->sales_per_month * $this->average_sale + $this->unreported_sales_per_month * $this->unreported_avg_sale);
					$month_total = $today_to_current_month_end + $monthly_amount;
					$month_miss_over = $month_total - $month_required;

					$today_to_current_quarter_end = ($this->days_left_this_quarter / 31) * ($this->sales_per_month * $this->average_sale + $this->unreported_sales_per_month * $this->unreported_avg_sale);
					$quarter_total = $amount + $today_to_current_quarter_end;
					$quater_miss_over = $quarter_total - $required;
					
					$monthly_amount = $this->FormatCurrency($monthly_amount);	
					$today_to_current_month_end = $this->FormatCurrency($today_to_current_month_end);	
					$month_total = $this->FormatCurrency($month_total);	
					$month_miss_over = $this->FormatCurrency($month_miss_over);	
					$today_to_current_quarter_end = $this->FormatCurrency($today_to_current_quarter_end);	
					$quarter_total = $this->FormatCurrency($quarter_total);
					$quater_miss_over = $this->FormatCurrency($quater_miss_over);
					$amount = $this->FormatCurrency($amount);
					
					$body[] = array('Total $$ Sold Current Month', $monthly_amount, '', '');		
					$body[] = array('Today to Current Month End', '', $today_to_current_month_end, '');			
					$body[] = array('Sold & Projection Current Month', '', $month_total, $month_miss_over);		

					$body[] = array('', '', '', '');

					$body[] = array('Total $$ Sold Current Quarter', $amount, '', '');		
					$body[] = array('Today to Current Quarter End', '', $today_to_current_quarter_end, '');		
					$body[] = array('Total Projection Current Quarter', '', $quarter_total, $quater_miss_over);		
					
				} else {
					if ($quarter > $this->current_quarter) { 
						$body[] = array($quarter_array[$quarter]. ' Quarter Sales', '$0', '', '');
					} else {
						$fiscal_year_total += $amount;
						$amount = $this->FormatCurrency($amount);
						$miss_over = $this->FormatCurrency($miss_over);	
						$body[] = array($quarter_array[$quarter]. ' Quarter Sales', $amount, '', $miss_over);			
					}							
				}
				
				$body[] = array('', '', '', '');
				
			}

			$today_to_end_fiscal_year = ($this->days_left_this_year / 31) * ($this->sales_per_month * $this->average_sale + $this->unreported_sales_per_month * $this->unreported_avg_sale);
			$fiscal_year_total += $today_to_end_fiscal_year;
			
			$fiscal_year_miss_over = $fiscal_year_total - $this->company_goal;
			
			$today_to_end_fiscal_year = $this->FormatCurrency($today_to_end_fiscal_year);	
			$fiscal_year_total = $this->FormatCurrency($fiscal_year_total);
			$fiscal_year_miss_over = $this->FormatCurrency($fiscal_year_miss_over);
			
			
			$body[] = array('Today to End Fiscal Year', '', $today_to_end_fiscal_year, '');		
			$body[] = array('Fiscal year ' . $this->year, '', $fiscal_year_total, $fiscal_year_miss_over);		
			
			$this->pdf->BasicTable($header, $body, array(3, 2, 2, 2));			
			
						
		} else {				
				
			$goal_top = '<table class="fadata">
					<tr>
						<td rowspan="2" class="large_title">Annual Goals</td>
						<th>Company<br>Set&nbsp;Goal</td>
						<th>Personal<br>Goal</td>
					</tr>
					<tr>
						<td class="fablue"><input type="radio" name="goal" id="goal_company" value="company" checked onClick="calculate()"><span id="company_goal">' . $this->FormatCurrency($this->company_goal) . '</span></nobr></span></td>
						<td><span><nobr><input type="radio" name="goal" id="goal_personal" value="personal" onClick="calculate()"><input type="text" style="font-size:12px; width:100px" maxlength="15" name="personalgoal" id="personal_goal" value="' . $this->FormatCurrency($this->personal_goal) . '" size="10" style="text-align:right" onBlur="save_personal_goal();calculate()"></nobr></span></td>
					</tr>
				</table>';
			
			$projections_top = '<table class="fadata">
					<tr>
						<td rowspan="2" class="large_title">Projected Percentage</td>
						<th>To<br>Annual</td>
						<th>Below/Above<br>Annual</td>
					</tr>
					<tr>
						<td><span id="projected_annual">0%</span></td>
						<td><span class="fared" id="above_below_annual">0%</span></td>
					</tr>
				</table>';
	
			$goal_bottom = '<table class="fadata2">
					<tr>
						<th></th>
						<th>Company</th>
						<th>Personal</th>
					</tr>
					<tr>
						<td class="facaption">M-Power Reported Sales ##</td>
						<td class="fablue"><span id="mp_reported_sales_company">0</span></td>
						<td class="fablue"><span id="mp_reported_sales_personal">0</span></td>
					</tr>
					<tr>
						<td class="facaption fasingle">M-Power Reported Sales $$</td>
						<td class="fablue fasingle"><span id="mp_reported_revenue_company">$0</span></td>
						<td class="fablue fasingle"><span id="mp_reported_revenue_personal">$0</span></td>
					</tr>
					<tr>
						<td class="facaption">Unreported Sales ##</td>
						<td class=""><span id="unreported_sales_company">0</span></td>
						<td><span id="unreported_sales_personal">0</span></td>
					</tr>
					<tr>
						<td class="facaption fasingle">Unreported Sales $$</td>
						<td class="fasingle"><span id="unreported_revenue_company">$0</span></td>
						<td class="fasingle"><span id="unreported_revenue_personal">$0</span></td>
					</tr>
					<tr>
						<td class="facaption">Total Sales to Date ##</td>
						<td><span id="company_sales_todate">0</span></td>
						<td><span id="personal_sales_todate">0</span></td>
					</tr>
					<tr>
						<td class="facaption fadouble">Total Sales to Date $$</td>
						<td class="fadouble"><span id="company_revenue_todate">$0</span></td>
						<td class="fadouble"><span id="personal_revenue_todate">$0</span></td>
					</tr>
					<tr>
						<td class="facaption">Required $$ to Reach This Month\'s Goal</td>
						<td><span id="toreach_month_goal_company" class="green">$0</span></td>
						<td><span id="toreach_month_goal_personal" class="green">$0</span></td>
					</tr>
					<tr>
						<td class="facaption">Required $$ to Reach This Quarter\'s Goal</td>
						<td><span id="toreach_quater_goal_company" class="green">$0</span></td>
						<td><span id="toreach_quarter_goal_personal" class="green">$0</span></td>
					</tr>
					<tr>
						<td class="facaption">Required $$ to Reach This Year\'s Goal</td>
						<td><span id="toreach_year_goal_company" class="green">$0</span></td>
						<td><span id="toreach_year_goal_personal" class="green">$0</span></td>
					</tr>
				</table>';
	
			$current_month = '<tr>
					<td class="facaption">Total $$ Sold Current Month</td>
					<td class="fablue"><span id="current_month_sales">$0</span></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td class="facaption">Today to Current Month End</td>
					<td></td>
					<td><span id="current_month_end">$0</span></td>
					<td></td>
				</tr>
				<tr>
					<td class="facaption fasingle">Sold & Projection Current Month</td>
					<td class="fasingle"></td>
					<td class="fasingle"><span id="monthly_total_projection">$0</span></td>
					<td class="fared fasingle"><span id="monthly_total_projection_missover">$0</span></td>
				</tr>		
				<tr>
					<td class="facaption">Total $$ Sold Current Quarter</td>
					<td class="fablue"><span id="total_sold_current_quarter">$0</span></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td class="facaption">Today to Current Quarter End</td>
					<td></td>
					<td><span id="today_quarter_end">$0</span></td>
					<td></td>
				</tr>
				<tr>
					<td class="facaption fasingle">Total Projection Current Quarter</td>
					<td class="fasingle"></td>
					<td class="fasingle"><span id="projection_current_quarter">$0</span></td>
					<td class="fared fasingle"><span id="miss_over_current_projection_quarter">$0</span></td>
				</tr>';
							
							
			$projection = '';	
			$quarter_array = array(1 => '1st', '2nd', '3rd', '4th');
			
			for ($quarter = 1; $quarter<= 4; ++$quarter) {							
				$quarter_sales = '';
				if ($quarter > $this->current_quarter) { 
					$quarter_sales = '<td class="fasingle"></td>
						<td class="fasingle"><span id="quarter_sales_'.$quarter.'">$0</span></td>';
				} else {					
					$quarter_sales = '<td class="fablue fasingle"><span id="quarter_sales_'.$quarter.'">$0</span></td>
						<td class="fasingle"></td>';							
				}
			
				if ($quarter == $this->current_quarter) {		 
					$projection .= $current_month;
				} else {
					$projection .=  '<tr>									
							<td class="facaption fasingle">'.$quarter_array[$quarter].' Quarter Sales</td>'
							. $quarter_sales .
							'<td class="fared fasingle"><span id="miss_over_'.$quarter.'">$0</span></td>
						</tr>';
				}
			}
			
			$projections_bottom = '<table class="fadata2">
					<tr>
						<th></th>
						<th>History</th>
						<th>Projected</th>
						<th><span class="fared">Miss</span>/Over</th>
					</tr>' 
					 . $projection .
					'<tr>
						<td class="facaption">Today to End Fiscal Year</td>
						<td></td>
						<td><span id="today_to_end_fiscal_year">$0</span></td>
						<td class="fared"><span id="miss_over_end_fiscal_year"></span></td>
					</tr>					
					<tr>
						<td class="facaption">Fiscal year ' . $this->year . '</td>
						<td></td>
						<td><span id="fiscal_year">$0</span></td>
						<td class="fared"><span id="fiscal_year_miss_over">$0</span></td>
					</tr>
				</table>';
	
			
			$output = '<table style="width:100%;border-collapse:collapse;">
					<tr>
						<td style="width:50%;padding:0 70px 10px 50px;vertical-align:top;">
							<div class="large_title">Goal Analysis</div>
							' . $goal_top . '
						</td>
						<td style="width:50%;padding:0 50px 10px 70px;vertical-align:top;">
							<div class="large_title">Projections</div>
							' . $projections_top . '
						</td>
					</tr>
					<tr>
						<td style="padding-right:20px;vertical-align:top;">
							' . $goal_bottom . '
						</td>
						<td style="padding-left:20px;vertical-align:top;">
							' . $projections_bottom . '
						</td>
					</tr>
				</table>';
	
			return $output;
		}	
	}

}