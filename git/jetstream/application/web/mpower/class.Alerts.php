<?php

require_once BASE_PATH . '/class.PageComponent.php';
require_once BASE_PATH . '/class.SessionManager.php';

class Alerts extends PageComponent 
{
	public function Render() { 
		return '<iframe name="myiframe" src ="' . SessionManager::CreateUrl('legacy/board/alerts.php') . '" style="width:100%;height:1000px; border:0px solid black"></iframe>';
	}

}

?>