<?php

require_once BASE_PATH . '/include/mpconstants.php';

session_start();

function SessionSet($var, $val, $time = 0) {
	$_SESSION[$var] = $val;
}

function SessionDel($name) {
	unset($_SESSION[$name]);
}

function SessionGet($name) {
	if (array_key_exists($name, $_SESSION)) {
		return $_SESSION[$name];
	} else {
		return false;
	}
}

function ValidateSession() {
	session_start();
	if (array_key_exists('auth', $_SESSION) && array_key_exists('company', $_SESSION) && array_key_exists('userid', $_SESSION)) {
		list($username, $token) = explode(':', $_SESSION['auth']);
		if (crypt($_SESSION['userid'] . $_SESSION['company'] . $_SERVER['HTTP_USER_AGENT'], $token) == $token) {
			return true;
		}
	}
	return false;
}

?>
