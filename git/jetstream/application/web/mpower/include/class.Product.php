<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.MpRecord.php';

/**
 * The SubAnswer database record
 * @package database
 * @subpackage record
 */
class Product extends MpRecord
{
    public function __toString() {
        return 'Product Record';
    }

//    public function __set($variable, $value) {
//        parent::__set($variable, $value);
//    }

    public function Initialize() {
        $this->db_table = 'products';
        $this->recordset->GetFieldDef('ProductID')->auto_key = TRUE;
        $this->recordset->GetFieldDef('Name')->required = TRUE;
        $this->recordset->GetFieldDef('CompanyID')->required = TRUE;

        parent::Initialize();
    }
}

?>
