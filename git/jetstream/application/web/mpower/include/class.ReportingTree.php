<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.People.php';

Class ReportingTree
{
	protected $tree = array();
	public $paging;
	public $total_sp;
	private $target;
	
	// protected $tree;
	
    public function BuildTree($company) {
		$tree_fields = array('PersonID', 'UserID', 'Color', 'FirstName', 'LastName', 'IsSalesperson', 'SupervisorID', 'GroupName', 'Rating', 'Level', 'StartDate', 'AvgVLevel', 'Email', 'AverageSalesCycle', 'FirstMeeting', 'AverageSale', 'ClosingRatio', 'TotalRevenues', 'TotalSales', 'UnreportedAvgSale', 'UnreportedSalesPerMonth');

		$sql = "SELECT " . implode(',', $tree_fields) . " FROM people left outer join
		(select PersonID as PID, avg(cast (Vlevel as float)) as AvgVLevel from opportunities where companyid = ? and vlevel > 0 and category not in (9,6) group by personid) as Valuations on people.PersonID = Valuations.PID
		WHERE companyid = ? AND deleted = 0";

		//$sql = "SELECT " . implode(',', $tree_fields) . " FROM people WHERE companyid = ? AND deleted = 0";

		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company), array(DTYPE_INT, $company));
		$recordset = DbConnManager::GetDb('mpower')->Execute($sql, 'MpRecord', 'PersonID');
		$parent_index = array();
		$people = array();
		foreach($recordset as $person_id => $record) {
			foreach($tree_fields as $fieldname) {
				$people[$person_id][$fieldname] = $record[$fieldname];
			}
			$people[$record->SupervisorID]['children'][] = $person_id;
		}

		$this->tree = $people;
    }

    public function GetPersonByUsername($username) {
    	foreach($this->tree as $person) {
    		if (isset($person['UserID']) && $person['UserID'] == $username) {
    			return $person['PersonID'];
    		}
    	}
		return false;
    }
    
    public function GetPersonByName($first, $last) {
    	foreach($this->tree as $person) {
    		if (isset($person['FirstName']) && isset($person['LastName']) && $person['FirstName'] == $first && $person['LastName'] == $last) {
    			return $person['PersonID'];
    		}
    	}
		return false;
    }

    public function GetTarget($get_all = false, $update_ratings = false) {
		$this->manager = $this->IsManager($_SESSION['login_id']);
		//if (isset($this->target)) {
		//	return $this->target;
		//}

		if (!$get_all && isset($_GET['target']) && $this->IsViewable($_GET['target'])) {
			$_SESSION['target'] = (int)$_GET['target'];
			$target = (int)$_GET['target'];
		} elseif (!$get_all && isset($_SESSION['target']) && $_SESSION['target'] && $this->IsViewable($_SESSION['target']) && basename($_SERVER['SCRIPT_NAME']) == 'dashboard.php') {
			$target = $_SESSION['target'];
		} elseif (!$this->manager) {
			$target = (int)$_SESSION['pov'];
		} elseif ($_SESSION['combined']) {
			$target = $_SESSION['tree_obj']->GetLimb($_SESSION['pov']);
		} else {
			$target = $_SESSION['tree_obj']->GetDirectSp($_SESSION['pov']);
		}
		$this->total_sp = count($target);
		if ($update_ratings) {
			$this->UpdateRatings($target);
		}
			
		$this->paging = (basename($_SERVER['SCRIPT_NAME']) == 'dashboard.php') ? true : false;
		$reset_paging = false;
		if (isset($_POST['sp_sort'])) {
			$sort = $_POST['sp_sort'];
			if (!isset($_SESSION['sp_sort']) || (isset($_SESSION['sp_sort']) && $_SESSION['sp_sort'] != $sort)) {
				$_SESSION['sp_sort'] = $sort;
				$reset_paging = true;
			}
		}
		
		if ($get_all) {
			return $target;
		}

		if ($this->paging && is_array($target)) {
			$page = $_SESSION['sp_page'];
			if ($page > $this->total_sp) $page = $this->total_sp - 10;
			if ($page < 0 || $reset_paging) $page = 0;
			$_SESSION['sp_page'] = $page;
			$this->target = array_slice($target, $page, 10);
		} else {
			$this->target = $target;
		}		
		return $this->target;
	}
    
    public function GetPath($person, $output = null) {
    	$record = $this->tree[$person];
    	if (is_null($output)) {
			$output = $record['FirstName'] . ' ' . $record['LastName'];
		}
		if ($record['GroupName']) {
			$output = $record['GroupName'] . ': ' . $output;
		}
		if (isset($this->tree[$record['SupervisorID']]) && $record['SupervisorID'] > 0) {
			$output = $this->GetPath($record['SupervisorID'], $output);
		}
        return $output;
    }
    
    public function GetRecord($person) {
		if (isset($this->tree[$person])) {
			return $this->tree[$person];
		}
    	
    }
    
    public function GetInfo($person, $type) {
		if (isset($this->tree[$person][$type])) {
	    	return $this->tree[$person][$type];
		} else {
			//echo $person . ' : ' . $type . ' is undefined<br>';
			return 0;
		}
    }

    public function GetRoots() {
        $output = array();
        foreach($this->tree as $key => $info) {
            if (!array_key_exists('parent', $info)) {
                $output[] = $key;
            }
        }
        return $output;
    }        

	/**
	 * Returns a list of the start and their children that are salespeople
	 * Does not proceed into grandchildren
	 *
	 * @param int $start_id
	 * @return array
	 */
    public function GetDirectSp($start_id) {
		$sort = (isset($_SESSION['sp_sort'])) ? $_SESSION['sp_sort'] : 'FirstName';
		$reverse_sort = false;
		if (substr($sort, -4) == '_asc') {
			$reverse_sort = false;
			$sort = substr($sort, 0, -4);
		}
		if (substr($sort, -4) == '_dsc') {
			$reverse_sort = true;
			$sort = substr($sort, 0, -4);
		}
		
		$start_record = $this->tree[$start_id];
		$output = array();
		$children = (isset($start_record['children'])) ? $start_record['children'] : array();
		$children[] = $start_id;
    	foreach($children as $child) {
    		if($this->tree[$child]['IsSalesperson'] == 1) {
    			$output[] = $child;
            }
        }

		return $this->SortPeople($output, $sort, $reverse_sort);
    }
    
    public function GetLimb($start_id) {
		$sort = (isset($_SESSION['sp_sort'])) ? $_SESSION['sp_sort'] : 'FirstName';
		$reverse_sort = false;
		if (substr($sort, -4) == '_asc') {
			$reverse_sort = false;
			$sort = substr($sort, 0, -4);
		}
		if (substr($sort, -4) == '_dsc') {
			$reverse_sort = true;
			$sort = substr($sort, 0, -4);
		}
		
		$start_record = $this->tree[$start_id];
    	$output = $this->GetDescendants($start_record, $sort);
        if($start_record['IsSalesperson'] == 1) {
        	$output[] = $start_id;
        	// array_unshift($output, $start_id);
        }

		return $this->SortPeople($output, $sort, $reverse_sort);
    }

    public function SortPeople($list, $sort, $reverse = false) {
		foreach($list as $person) {
			$first[$person] = $this->tree[$person]['FirstName'];
			$last[$person] = $this->tree[$person]['LastName'];
			$rating[$person] = $this->tree[$person]['Rating'];
			$start_date[$person] = date('Ymd', strtotime($this->tree[$person]['StartDate']));
		}
		$sort_dir = ($reverse) ? SORT_DESC : SORT_ASC;

		switch ($sort) {
			case 'FirstName':
				array_multisort($first, $sort_dir, $last, $sort_dir, $list);
				break;
			case 'LastName':
				array_multisort($last, $sort_dir, $first, $sort_dir, $list);
				break;
			case 'Rating':
				array_multisort($rating, $sort_dir, $first, SORT_ASC, $last, SORT_ASC, $list);
				break;
			case 'StartDate':
				array_multisort($start_date, $sort_dir, $first, SORT_ASC, $last, SORT_ASC, $list);
				break;
		}

		//print_r($list);
		
		return $list;
    }
    
    public function GetDescendants($start_record, $sort = null) {
    	$output = array();
        if (isset($start_record['children'])) {
        	foreach($start_record['children'] as $child) {
        		if($this->tree[$child]['IsSalesperson'] == 1) {
        			$output[] = $child;
	            }
                $output = array_merge($output, $this->GetDescendants($this->tree[$child], $sort));
	        }
		}
        return $output;
    }        

    public function IsManager($person_id) {
    	return ($this->tree[$person_id]['Level'] > 1) ? true : false;
    }

    public function IsSeniorMgr($start_id) {
		$start_record = $this->tree[$start_id];
		if (isset($this->tree[$start_id]['children'])) {
			foreach($this->tree[$start_id]['children'] as $child) {
				if (isset($this->tree[$child]['children']) && count($this->tree[$child]['children']) > 0) {
					return true;
				}
			}
		}
		return false;
    }

    public function IsDirectMgr($start_id) {
		$start_record = $this->tree[$start_id];
		foreach($this->tree[$start_id]['children'] as $child) {
			if (isset($this->tree[$child]['IsSalesperson']) && $this->tree[$child]['IsSalesperson']) {
				return true;
			}
		}		
		return false;
    }

    public function IsViewable($target) {
    	if ($target == $_SESSION['login_id']) {
    		return true;
    	} elseif (isset($this->tree[$target]) && $this->tree[$target]['SupervisorID'] > 0 && $this->IsViewable($this->tree[$target]['SupervisorID'])) {
    		return true;
    	}
		return false;    	
    }
    
    public function GetCombined() {
        $output = array();
        foreach($this->tree as $key => $info) {
            if (!array_key_exists('parent', $info)) {
                $output[] = $key;
            }
        }
        return $output;
    }

   public function UpdateRatings($people_list) {
    	if (is_int($people_list)) {
    		$people_list = array($people_list);
    	}
    	$people_sql = "SELECT PersonID, Rating, FMValue, IPValue, DPValue, CloseValue, AnalysisID, FMThreshold, IPThreshold, DPThreshold, CloseThreshold, DATEDIFF(month, StartDate, GETDATE()) AS tenure
    		FROM people 
    		WHERE PersonID in (" . ArrayQm($people_list) . ")";

		$people_sql = SqlBuilder()->LoadSql($people_sql)->BuildSql(array(DTYPE_INT, $people_list));

		$people = DbConnManager::GetDb('mpower')
			->Execute($people_sql, 'People', 'PersonID')
			->Initialize();

		$category_total_sql = "SELECT PersonID, Category, count(dealid) as TotNum
			FROM opportunities 
			WHERE PersonID IN (" . ArrayQm($people_list) . ")
			AND (Category IN (1, 2, 4) OR (Category = 6 AND DATEDIFF(d, ActualCloseDate, GETDATE()) < 30))
			GROUP BY PersonID, Category ORDER BY PersonID, Category";
		$category_total_sql = SqlBuilder()->LoadSql($category_total_sql)->BuildSql(array(DTYPE_INT, $people_list));

		$cat_query = mssql_query($category_total_sql);
		
		$results = array();
		$category_trans = array(1 => 'FM', 2 => 'IP', 4 => 'DP', 6 => 'Close');

		while ($category_total = mssql_fetch_assoc($cat_query)) { 
		// foreach($category_totals as $category_total) {
			$this_cat = $category_trans[$category_total['Category']];
			$calc = $category_total['TotNum'] / $people[$category_total['PersonID']][$this_cat . 'Threshold'];

			if ($calc == 0) $result = 0;
			elseif ($calc < .66) $result = 1;
			elseif ($calc < 1.5) $result = 2;
			else $result = 3;

			$results[$category_total['PersonID']][$this_cat] = $result;
		}
		foreach ($people as $person) {
			$analysis_sql = "SELECT AnalysisID, RankExperienced, RankNew FROM analysis
				WHERE FMValue = ? and IPValue = ? and DPValue = ? AND CloseValue = ?";
			$fm_value = (isset($results[$person['PersonID']]['FM'])) ? $results[$person['PersonID']]['FM'] : 0;
			$ip_value = (isset($results[$person['PersonID']]['IP'])) ? $results[$person['PersonID']]['IP'] : 0;
			$dp_value = (isset($results[$person['PersonID']]['DP'])) ? $results[$person['PersonID']]['DP'] : 0;
			$c_value = (isset($results[$person['PersonID']]['Close'])) ? $results[$person['PersonID']]['Close'] : 0;
			$analysis_sql = SqlBuilder()->LoadSql($analysis_sql)->BuildSql(array(DTYPE_INT, $fm_value), array(DTYPE_INT, $ip_value), array(DTYPE_INT, $dp_value), array(DTYPE_INT, $c_value));
			$analysis = DbConnManager::GetDb('mpower')->GetOne($analysis_sql);
			$rating = ($person['tenure'] > $_SESSION['company_obj']['MonthsConsideredNew']) ? $analysis['RankExperienced'] : $analysis['RankNew'];
			$person['Rating'] = $person['Rating'] = (!is_null($rating)) ? $rating : -1;
			$person['FMValue'] = $fm_value;
			$person['IPValue'] = $ip_value;
			$person['DPValue'] = $dp_value;
			$person['CloseValue'] = $c_value;
			$person['AnalysisID'] = (!is_null($analysis['AnalysisID'])) ? $analysis['AnalysisID'] : -2;
		}
		$people->Save();
		
   }
}
    