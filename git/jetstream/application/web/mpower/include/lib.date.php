<?php
/**
 * @package library
 */

date_default_timezone_set('America/Chicago');

/**
 * Converts a timestamp into a string with the M-Power MS Sql format
 * type 'd' converts only the date, 't' converts only the time, and
 * 'b' (or nothing or anything other than 'd' or 't') converts both
 *
 * @param time $timestamp
 * @param char $type
 */
function TimestampToMsSql($timestamp, $type = 'b') {
    if (is_null($timestamp) || $timestamp === 0 || $timestamp == FALSE) {
        return NULL;
    }
    if ($type == 't') {
        return date('g:i A', $timestamp);
    }
    $date = date('M j Y', $timestamp);
    $time = ($type != 'd') ? date('h:iA', $timestamp) : '12:00AM';
    // Seems that this may be a difference between Windows and Unix MS Sql drivers
    if ($time{0} == '0') $time{0} = ' ';
    return $date . ' ' . $time;
}

/**
 * Returns the date of the start of a $quarter quarters ago.
 *
 * @param int $quarter
 * @return date
 */
function QuarterAgoDate($quarter) {
 	return mktime(0, 0, 0, ((ceil(date('m')/3)-1)*3+1-$quarter*3), 1, date('Y'));
}

/**
 * Returns the first day of $month months ago.
 *
 * @param int $month
 * @return date
 */
function MonthAgoDate($month) {
	return mktime(0, 0, 0, date('m') - $month, 1, date('Y'));
}

/**
 * Returns the monthname of the date specified.
 *
 * @param date $date
 * @return string
 */
function MonthName($date) {
	return JDMonthName(unixtojd($date), 1);
}


/**
 * Returns the the name of the quarter based on a date.
 * Defaults to Q2 but the Q can be changed with the $q_term
 *
 * @param date $date
 * @param string $q_term
 * @return string
 */
function QuarterName($date, $q_term = 'Q', $year = true) {
	$year_text = '';
	if ($year) {
		$year_text = ' ' . date('Y');
	}
	return $q_term . ceil(date('m', $date)/3) . $year_text;
}

/**
 * Returns the number of weekdays (M-F) between the two days inclusive.
 *
 * @param date $dateBegin
 * @param date $dateEnd
 * @return int
 */
function WeekdayCount($dateBegin, $dateEnd) {
    $begin_week = date('Y', $dateBegin) * 100 + date('W', $dateBegin);
    $end_week = date('Y', $dateEnd) * 100 + date('W', $dateEnd);

    $begin_offset = (GetWeekdayNum($dateBegin) - 1);
    if ($begin_offset > 5) $begin_offset = 5;
    $end_offset = 5 - GetWeekdayNum($dateEnd);
    if ($end_offset < 0) $end_offset = 0;

    $weekday_count = ($end_week - $begin_week + 1) * 5 - $begin_offset - $end_offset;
    return $weekday_count;
}

/**
 * Returns the total number of days between the two specified dates.
 *
 * @param date $dateBegin
 * @param date $dateEnd
 * @return int
 */
function DaysDiff($dateBegin, $dateEnd) {
    $days_diff = floor(strtotime($dateEnd) - strtotime($dateBegin)) / 86400;
    return $days_diff;
}

/**
 * This returns 1 for Monday & 7 for Sunday (same as the Date('N'...
 *
 * @param date $inDate
 * @return int
 */
function GetWeekdayNum($inDate) {
    $day_of_week = date('w', $inDate);
    if ($day_of_week == 0) $day_of_week = 7;
    return $day_of_week;
}

function MsSqlToTimestamp($date) {
	return strtotime($date);
}

function ReformatDate($date) {
	if (is_null($date) || strlen($date) == 0) return null;
	return(date('m/d/y', MsSqlToTimestamp($date)));
}

?>
