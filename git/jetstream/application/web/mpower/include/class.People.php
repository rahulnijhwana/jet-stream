<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.MpRecord.php';

/**
 * The SubAnswer database record
 * @package database
 * @subpackage record
 */
class People extends MpRecord
{
    public function __toString() {
        return 'Person Record';
    }

//    public function __set($variable, $value) {
//        parent::__set($variable, $value);
//    }

    public function Initialize() {
        $this->db_table = 'people';
        $this->recordset->GetFieldDef('PersonID')->auto_key = TRUE;
        // $this->recordset->GetFieldDef('CompanyID')->required = TRUE;

        parent::Initialize();
    }
}

?>
