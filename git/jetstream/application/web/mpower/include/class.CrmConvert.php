<?php
/**
 * Reconcile Salesforce opps with MPower
 *
 * @package salesforce
 */

if (!defined('BASE_PATH')) define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/lib.string.php';

Class CrmConvert
{
	private $opp_recordset;

	/**
	 * Get all the MPower records that match the CMIDs from the CRM recordset
	 *
	 * @param MpRecordset $crm_recordset
	 */
	public function MergeSaveOpps(MpRecordset $crm_recordset, $user, $company) {
		// Get the opportunities that match the CMIDs from the CRM records
		$mp_sql = 'SELECT *
            FROM opportunities
            WHERE CMID IS NOT NULL AND CompanyID = ?';

		$targets = $crm_recordset->GetField('CMID');

		if (count($targets) > 0) {
			$mp_sql = $mp_sql . ' AND CMID in (' . implode(', ', array_fill(0, count($targets), '?')) .')';
		}

		// Load all the M-Power Opps matching the target index by CMID
		$mp_sql = SqlBuilder()->LoadSql($mp_sql)->BuildSql(array(DTYPE_INT, $company), array(DTYPE_STRING, $targets));

		
		$mp_recordset = DbConnManager::GetDb('mpower')->Execute($mp_sql, 'Opp', 'CMID', TRUE);

		// Get the SubMilestones for the Opps and attach them to their respective Opp		
		if (count($mp_recordset) > 0) {
			$deal_ids = $mp_recordset->GetField('DealID');
			$mp_fielddef = new MpFieldDef();
			$mp_fielddef->link = 'DealID';
			
			$mp_recordset->SetFielddef('SubAnswers', $mp_fielddef);

 			// Get the submilestone definitions to use as a template for the milestones
			$submilestone_sql = 'SELECT SubID, YesNo, AlphaNum, CheckBox, Calendar, DropDown, CAST(NULL as VARCHAR) as Answer, NULL as DealID
				FROM SubMilestones
				WHERE CompanyID = ? AND Enable = 1';
			
			$submilestone_sql = SqlBuilder()->LoadSql($submilestone_sql)->BuildSql(array(DTYPE_INT, $company));
			$submilestone_template = DbConnManager::GetDb('mpower')->Execute($submilestone_sql, 'SubAnswer', 'SubID');

			// Create a new index of the records by DealID to make it easy to
			// attach the SubMilestones
			$deal_id_index = array();
			foreach ($mp_recordset as $record) {
				$record->SubAnswers = clone $submilestone_template;
				$record->SubAnswers->SetAll('DealID', $record->DealID);
				$deal_id_index[$record->DealID] = $record;
			}

			$sub_ids = $submilestone_template->GetField('SubID');
			
			// Get the answers to populate the milestones			
			$subanswer_sql = 'SELECT DealID, SubID, Answer FROM SubAnswers 
				WHERE DealID IN (' . implode(', ', array_fill(0, count($deal_ids), '?')) . ')
				AND SubID in (' . implode(', ', array_fill(0, count($sub_ids), '?')) . ')';

			$subanswer_sql = SqlBuilder()->LoadSql($subanswer_sql)->BuildSql(array(DTYPE_INT, $deal_ids), array(DTYPE_INT, $sub_ids));
			$subanswer_recordset = DbConnManager::GetDb('mpower')->Execute($subanswer_sql);

			foreach($subanswer_recordset as $sub_answer) {
				$deal_id_index[$sub_answer->DealID]->SubAnswers[$sub_answer->SubID]['Answer'] = $sub_answer['Answer'];
				$deal_id_index[$sub_answer->DealID]->SubAnswers[$sub_answer->SubID]->ClearDirty();
			}

		}

		$mp_recordset->ClearDirty();
		foreach ($crm_recordset as $crm_record) {
			if (!$mp_recordset->InRecordset($crm_record->CMID)) {
				$blank_opp = new Opp();
				$mp_recordset[$crm_record->CMID] = $blank_opp;
				$blank_opp->SetDatabase(DbConnManager::GetDb('mpower'));
				$blank_opp['PersonID'] = $user;
				$blank_opp['CompanyID'] = $company;
				$blank_opp['SubAnswers'] = clone $submilestone_template;
			}
		}
		$mp_recordset->Initialize();

		// Finished loading the MPower data.  Now merge the MPower data with the CRM
		// record and save it.
		$mp_recordset->Merge($crm_recordset);

		foreach ($mp_recordset as $mp_record) {
			// Perform the initial save
			$mp_record->Save();
			// I use the database to total up the milestones to reduce the amount of data the script needs to load 
			// so it cannot be done until after all the milestones are updated and inserted
			$mp_record->UpdateMilestoneTotals();
			$mp_record->SelfCategorize();
			// Then it has to be resaved to account for any changes
			$mp_record->Save();
		}
		// Synchronize the Products
		$this->opp_recordset = $mp_recordset;
	}

	public function MergeSaveProducts($crm_product_recordset, $company) {
        if (is_object($crm_product_recordset)) {
    		// Get all of the MPower records for the effected DealIDs
    		$deals = $this->opp_recordset->GetField('DealID');
    		
    		$mp_product_sql = 'SELECT ProductID, DealID, Val, Qty, CompanyID, ID, CMID
                FROM Opp_Product_XRef
                WHERE DealID in (' . implode(', ', array_fill(0, count($deals), '?')) . ')';
    		$mp_product_sql = SqlBuilder()->LoadSql($mp_product_sql)->BuildSql(array(DTYPE_INT, $deals));
    		$mp_product_recordset = DbConnManager::GetDb('mpower')->Execute($mp_product_sql, 'ProductXRef', 'CMID');
    
    		// Initialize the MPower Ops and mark for delete.  We will clear the delete as we
    		// determine that they are valid
    		$mp_product_recordset->Initialize()->MarkAllForDelete();
    
    		foreach ($crm_product_recordset as $crm_product_record) {
    			// Create a blank M-Power Product_XRef if there isn't match
    			if (!$mp_product_recordset->InRecordset($crm_product_record->CMID)) {
    				$xref = new ProductXRef();
    				$mp_product_recordset[$crm_product_record->CMID] = $xref;
    				$xref->CompanyID = $company;
    				$xref->DealID = $this->opp_recordset[$crm_product_record->c_OppCMID]->DealID;
    				$xref->SetDatabase(DbConnManager::GetDb('mpower'));
    			}
    			// Merge the M-Power record with the data from Salesforce
    			$mp_product_recordset[$crm_product_record->CMID]
    				->Initialize()
    				->Merge($crm_product_record)
    				->ClearDelete();
    		}
            		
    		$mp_product_recordset->Save();  // Save em' all
        }
	}

    public function RemoveOpps($company) {
		// This function assumes that every valid Opp for the company is in $this->opp_recordset

		$active_opps = $this->opp_recordset->GetField('DealID');
		
		$sql = 'UPDATE opportunities SET Category = 9 WHERE 
			CompanyID = ? AND Category != 9 AND CMID IS NOT NULL
            AND DealID NOT IN (' . implode(', ', array_fill(0, count($active_opps), '?')) . ')';

        $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company), array(DTYPE_INT, $active_opps));
		echo $sql . "\n";
		DbConnManager::GetDb('mpower')->Execute($sql);
    }
}