<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.DbConn.php';
require_once BASE_PATH . 'sfsoapclient/SforcePartnerClient.php';

/**
 * The Salesforce database connector
 * @package database
 * @subpackage connector
 */
class DbConnSalesforce extends DbConn
{
	// private $query_count;

	public $session, $url, $username, $password;

	public function DoConnect() {
		$this->database = new SforcePartnerClient();
		$this->database->createConnection('sfsoapclient/partner.wsdl.xml');
		$error = FALSE;
		if (isset($this->session) && isset($this->url)) {
			$this->database->setSessionHeader($this->session);
			$this->database->setEndpoint($this->url);
		}
		else if (isset($this->username) && isset($this->password)) {
			$result = $this->database->login($this->username, $this->password);
		}
		else {
			throw new Exception('Not enough information to connect.  Need session ID and URL or username and password.');
		}

		if ($error != FALSE) {  // Database connection failed
			throw new Exception('Failed to connect to Salesforce: ' . $error);
		}
	}

	public function GetUserId() {
		$result = $this->database->getUserInfo();
		return($result->userId);
	}

	public function Execute($sql, $record_type = 'MpRecord', $index = NULL) {
		$result = $this->database->query($sql);

		$recordset = new MpRecordSet();
		
		if (array_key_exists('records', $result)) {
			foreach($result->records as $sfrecord) {
				$sfrecord = new SObject($sfrecord);

				$record = new $record_type;
				$record->SetRecordset($recordset);

				$record->Id = $sfrecord->Id;
				foreach($sfrecord->fields as $sfkey => $sfvalue) {
					$record->{$sfkey} = (string) $sfvalue;
				}

				if (is_null($index)) {
					$recordset[] = $record;
				} else {
					$recordset[$record[$index]] = $record;
				}
			}
		}
		return($recordset);
	}

//	private function LoadRecord(MpRecord $record, array $sfrecord) {
//		$record->Id = $sfrecord['Id'][0];
//		foreach($sfrecord as $sfkey => $sfvalue) {
//			if ($sfkey != 'Id') {
//				$record->{$sfkey} = $sfvalue;
//			}
//		}
//		return $record;
//	}
}

?>
