<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.DbConn.php';
require_once BASE_PATH . '/include/lib.mpower.php';

/**
 * Base Database connection Class also inlcudes the MS Sql connection
 * @package database
 * @subpackage connector
 */
class DbConnMssql extends DbConn
{
    public $server, $user, $password, $database_name;

    public function DoConnect() {
        $max_connect_attempts = 3;
        for ($connect_attempt = 1; $connect_attempt <= $max_connect_attempts; $connect_attempt++) {
            $this->database = @mssql_pconnect($this->server, $this->user, $this->password);
            if(@mssql_select_db($this->database_name, $this->database) != FALSE) {
                return;
            }
        }
        throw new Exception('Failed to connect to the database.');
    }

    function Execute($sql, $record_type = 'MpRecord', $index = NULL) {
		// timehack('before query');
    	$result = mssql_query($sql, $this->database);
		// timehack($sql);
        // result will be TRUE if it is a successful update or insert or
        // FALSE if the update or insert fails
        if (is_bool($result)) {
            return $result;
        }

        $recordset = new MpRecordset();

        while($fieldname = mssql_fetch_field($result)) {
            $field = new MpFieldDef;
            $field->type = GetMpDataType($fieldname->type);
            $recordset->SetFieldDef($fieldname->name, $field);
        }
        while ($row = mssql_fetch_assoc($result)) {
            $record = new $record_type;
            $record->SetDatabase($this);
			$record->SetRecordset($recordset);

			$record->SetDbRow($row);
			// timehack('before row breakout');
			// foreach($row as $fieldname => $value) {
			//	$record[$fieldname] = $value;
            // }
        	// timehack('after row breakout');

            if (is_null($index)) {
                $recordset[] = $record;
            }
            else {
            	$recordset[$record->$index] = $record;
            }
        }
		//timehack('records');
        $recordset->ClearDirty();

        return($recordset);
    }

    /**
     * Execute a SQL statement. 
     * 
     * @param unknown_type $sql
     */
    function Exec($sql) {
    	$result = mssql_query($sql, $this->database);

        if (is_bool($result)) {
            return $result;
        }

		$recordset = array();
        while ($row = mssql_fetch_assoc($result)) {
			$recordset[] = $row;
		}	

        return($recordset);
    }


    /**
     * Execute a SQL statement that should only return a single result.  Will throw exception if 
     *
     * @param unknown_type $sql
     * @param unknown_type $record_type
     * @return MsRecord;
     */
    function GetOne($sql, $record_type = 'MpRecord') {
    	$rs = $this->Execute($sql, $record_type);
		if (!$rs instanceof MpRecordSet || count($rs) == 0) {
			return false;
		} 
		if (count($rs) > 1) {
			throw new Exception('Single result query produced multiple results.');
		}
		return $rs[0];
    }
}


?>
