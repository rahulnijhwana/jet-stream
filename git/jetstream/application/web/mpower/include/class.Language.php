<?php

class Language
{
    /**
     * The single instance of the class
     * @var object
     */
    private static $instance;

    /**
     * Private constructor to dissallow instantiaion
     */
    private function __construct() {}

    /**
     * Returns a single instance of the object
     * @return object
     */
    public static function GetInstance() {
        if(!isset(self::$instance)){
            $object = __CLASS__;
            self::$instance = new $object;
        }
        return self::$instance;
    }
	
	public static $language_terms = array();
	
	public static function __toString() {
		return ("Language Set");
	}

	public static function __get($name) {

		if (count(self::$language_terms) == 0) {
			self::Init();
		}
		if (array_key_exists($name, self::$language_terms)) {
    		$value = self::$language_terms[$name];
    		$regex = "|\{(\w*?)\}|";	// Find words encased in curly braces
    		return preg_replace_callback($regex, array('self', 'LookupRegEx'), $value);
    	} else {
            return NULL;
        }
	}

	private static function LookupRegEx($val) {
		return self::__get($val[1]);  // Go back and get the value of the word
	}

	public static function Term($val) {
		return self::__get($val);  // Go back and get the value of the word
	}
	
	public static function Init($company = null, $lang = 'en') {
		if(isset($_SESSION['language_terms'])) {
			self::$language_terms = $_SESSION['language_terms'];
			return;
		}
		
		require BASE_PATH . '/lang/en_lang.php';
		if ($lang != 'en') {
			$english = $language_terms;
			require BASE_PATH . '/lang/' . $lang . '_lang.php';
			$language_terms = array_merge($english, $language_terms);
		}

		if (!is_null($company)) {
			foreach($company->GetDbRow() as $variable => $value) {
				if (!empty($value) && isset($language_terms[$variable])) {
					$language_terms[$variable] = $value;
				}
			}
		}
		$_SESSION['language_terms'] = self::$language_terms = $language_terms;
	}
}
