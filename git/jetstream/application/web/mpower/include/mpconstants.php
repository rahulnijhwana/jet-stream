<?php

// M-Power Phases: To make the code more decipherable
define('MP_TARGET', 10);
define('MP_FM',      1);
define('MP_IP',      2);
define('MP_IPS',     3);
define('MP_DP',      4);
define('MP_DPS',     5);
define('MP_CLOSED',  6);
define('MP_REMOVED', 9);

// Milestone Types: Because the way the database defines them sucks
define('MSTYPE_UNKNOWN',  0);
define('MSTYPE_YESNO',    1);
define('MSTYPE_ALPHANUM', 2);
define('MSTYPE_CHECKBOX', 3);
define('MSTYPE_CALENDAR', 4);
define('MSTYPE_DROPDOWN', 5);

// Data Types: Important for SQL Queries
define('DTYPE_MIXED',     0);
define('DTYPE_CHAR',      1);
define('DTYPE_TIME',      2);
define('DTYPE_STRING',    3);
define('DTYPE_BOOL',      4);
define('DTYPE_FLOAT',     5);
define('DTYPE_INT',       6);
define('DTYPE_RECORDSET', 7);
define('DTYPE_SUBQUERY',  8);
