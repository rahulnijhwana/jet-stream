<?php
/**
 * @package library
 */

/**
 * Makes a multi-dimensional array into a single dimensional array
 * All keys are dropped and resulting order follows the left-hand branches of the tree
 * @param array $array
 * @return array
 */
function ArrayFlatten(array $array) {
    $output = array();
    foreach ($array as $item)
    {
        if (is_array($item)) {
            $output = array_merge($output, ArrayFlatten($item));
        }
        elseif (!is_null($item)) {
            $output[] = $item;
        }
    }
    return $output;
}

/**
 * Appends text to every element of an array.
 * Only works on one-dimensional arrays containing string data.
 * @param array pieces
 * @param string $append_text
 * @return string
 */
function AppendEach(array $pieces, $append_text) {
    $output = array();
    foreach($pieces as $piece) {
        $output[] = $piece . $append_text;
    }
    return $output;
}

/**
 * Returns an array with the subarry values with keys that match $branch
 * @param array pieces
 * @param string $append_text
 * @return string
 */
function GetBranch(array $array, $branch) {
    $result = array();
    foreach ($array as $key => $element) {
        $result[$key] = $element[$branch];
    }
    return $result;
}

function ImplodeWithKey(array $assoc, $inglue = '>', $outglue = ',')
{
    $return = '';
    foreach ($assoc as $tk => $tv)
    {
        $return .= $outglue . $tk . $inglue . $tv;
    }
    return substr($return,strlen($outglue));
}

function ExplodeWithKey($str, $inglue = ">", $outglue = ',')
{
    $hash = array();
    foreach (explode($outglue, $str) as $pair)
    {
        $k2v = explode($inglue, $pair);
        $hash[$k2v[0]] = $k2v[1];
    }
    return $hash;
}

function HtmlDebug($variable) {
    echo '<pre>';
    print_r($variable);
    echo '</pre>';
}

function GetIfExists($key, $array) {
	if (array_key_exists($key, $array)) {
		return $array[$key];
	}
	return false;
}

?>
