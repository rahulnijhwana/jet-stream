<?php

require_once BASE_PATH . '/include/class.MpFieldDef.php';

class MpRecordset implements IteratorAggregate, ArrayAccess, Countable {
	/**
	 * An array of MpRecord objects
	 *
	 * @var array
	 */
    private $records = array();

	/**
	 * An array of Field Definition objects
	 *
	 * @var array
	 */
	private $fielddefs = array();
    
    public function __set($variable, MpRecord $value) {
		if (is_null($value->GetRecordset())) {
	    	$value->SetRecordset($this);
		}
    	if ($variable) {
    	    $this->records[$variable] = $value;
    	}
    	else {
    	    $this->records[] = $value;
    	}
	}

	public function __get($variable) {
        return $this->records[$variable];
	}

	public function __clone() {
		$clone_records = array();
		foreach($this->records as $record_id => $record) {
			$clone_records[$record_id] = clone($record);
		}
		$this->records = $clone_records;
	}
	
	public function SetFieldDef($fielddef_name, $fielddef) {
		$this->fielddefs[$fielddef_name] = $fielddef;
	}
    
	public function Validate($field_name, $field_val) {
		if (key_exists($field_name, $this->fielddefs)) {
			return $this->fielddefs[$field_name]->Validate($field_val);
		} else {
			return $field_val;
		}
	}
	
	public function GetFieldDef($fielddef_name = NULL) {
		if (is_null($fielddef_name)) {
			return $this->fielddefs;
		}
		if (key_exists($fielddef_name, $this->fielddefs)) {
			return $this->fielddefs[$fielddef_name];
		}
 		return FALSE;
	}
    
    public function __toString() {
        return 'MpRecordSet';
    }
    
    public function Pop() {
    	return array_pop($this->records);
    }

    public function ClearDirty() {
        foreach($this->records as $record) {
            $record->ClearDirty();
        }
		return $this;
    }

    public function Initialize() {
        foreach ($this->records as $record) {
            $record->Initialize();
        }
        return $this;
    }

    // Returns an array containing one field from each record
    public function GetField($fieldname) {
    	$result = array();
        foreach ($this->records as $record) {
            if ($record->InRecord($fieldname, false)) {
        		$result[] = $record[$fieldname];
            }
        }
        return $result;
    }

    public function Merge(MpRecordset $recordset) {
        foreach($recordset as $record_name => $record) {
            if (array_key_exists($record_name, $this->records)) {
                $this->records[$record_name]->Merge($record);
            }
            else {
				debug_print_backtrace();
                throw new Exception('Cannot merge recordset as base record does not exist: ' . $record_name);
            }
        }
		return $this;
    }

    public function Save($simulate = false) {
    	foreach ($this->records as $record) {
            $record->Save($simulate);
        }
    }

    public function Debug($offset = '') {
		foreach($this->fielddefs as $fieldname => $fielddef) {
			echo "{$offset}Field: $fieldname\n";
			$fielddef->Debug($offset);
			echo "\n";
		}

		foreach($this->records as $record_id => $record) {
			echo "{$offset}Record: $record_id\n";
			$record->Debug($offset);
			echo "\n";
		}
    }

    public function MarkAllForDelete() {
        foreach($this->records as $record) {
            $record->SetDelete();
        }
        return $this;
    }

    public function SetAll($variable, $value) {
        foreach($this->records as $record) {
            $record[$variable] = $value;
        }
        return $this;
    }

    public function InRecordset($variable) {
        return array_key_exists($variable, $this->records);
    }

    public function GetKeys() {
        return array_keys($this->records);
    }

    public function count() {
        return count($this->records);
    }
    
    
    // Required for IteratorAggregate
    public function getIterator()
    {
        return new ArrayIterator($this->records);
    }

    // *** Required ArrayAccess Functions

    public function offsetExists($offset)
    {
    	return (isset($this->records[$offset])) ? TRUE : FALSE;
    }

    public function offsetGet($offset)
    {
        return ($this->offsetExists($offset)) ? $this->records[$offset] : FALSE;
    }

    public function offsetSet($offset, $value)
    {
        $this->__set($offset, $value);
    }

    public function offsetUnset($offset)
    {
        unset ($this->records[$offset]);
    }

    // End required ArrayAccess functions ***
}

?>
