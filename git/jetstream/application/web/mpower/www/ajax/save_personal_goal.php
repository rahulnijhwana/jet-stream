<?php
/**
* @package Ajax
*/
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once(BASE_PATH . '/class.SessionManager.php');
require_once(BASE_PATH . '/include/class.ReportingTree.php');

SessionManager::Init();
SessionManager::Validate();

$personid = (int) $_POST['PersonID'];
$personal_goal = (int) $_POST['PersonalGoal'];
$year = (int) $_POST['Year'];


if (!empty($personid) || !empty($personal_goal)) {

	$sql = "IF EXISTS (SELECT * FROM Goal WHERE PersonID = $personid and Year = $year)
				UPDATE Goal
				   SET PersonalGoal = $personal_goal 
				 WHERE PersonID = $personid and Year = $year
			ELSE
				INSERT INTO Goal
					   (PersonID
					   ,Year
					   ,PersonalGoal)
				 VALUES ($personid, $year, $personal_goal)";
	
	//file_put_contents('test.log', $sql);
	//$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $personid), array(DTYPE_INT, $year), array(DTYPE_INT, $personal_goal));
	
	if(DbConnManager::GetDb('mpower')->Exec($sql)) {
		$checksum = $personid + $personal_goal;
		echo $checksum;
	} else {
		echo 'Error in saving';
	}
}
?>

