<?php
/**
 * @package Admin
 */

include('../reports/_report_utils.php');
include('../reports/jim_utils.php');
include('../reports/ci_pdf_utils.php');

define(SHADE_MED, "MED_BLUE");
define(SHADE_LIGHT, "LIGHT_BLUE");


function GetElapsedDays($StartTime, $EndTime)
{
	$Diff = $EndTime - $StartTime;
	return (int)($Diff / (60 * 60 * 24));
}



function GetMonth($date)
{
	$str = date("m", $date);
	return (int)$str;
}

function GetYear($date)
{
	$str = date("Y", $date);
	return (int)$str;
}


function PrintColTitles(&$report)
{
	BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0);

	SetRowColorStandard($report, -1, SHADE_MED);
	SetColColorStandard($report, -1, -1, SHADE_MED);

	AddColumn($report, 'No',	 				"center", 0.3, 0);	//0
	AddColumn($report, 'Status', 				"center", 0.5, 0);	//1
	AddColumn($report, 'Salesperson', 			"center", 1.2, 0);  	//2
	AddColumn($report, 'Company', 				"center", 3.0, 0);	//3
	AddColumn($report, 'Date Assigned', 		"center", 0.7, 0);	//4
	AddColumn($report, 'Offering', 				"center", 2.0, 0);	//5
	AddColumn($report, 'First Meeting', 		"center", 0.7, 0);	//6
	AddColumn($report, 'Forecast Close Date', 	"center", 0.7, 0);	//7
	AddColumn($report, 'Actual Close Date', 	"center", 0.7, 0);	//8
	AddColumn($report, 'Val Level', 			"center", 0.5, 0);
}

function PrintData(&$report, $arrData)
{
	$highlight=1;
	for ($i=0; $i < count($arrData); $i++)
	{
		BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
		if($highlight) SetRowColorStandard($report, -1, SHADE_LIGHT);
		$highlight=!$highlight;

		AddColumnText($report, $i+1,			 				"center", 0, 0);			//0
		AddColumnText($report, $arrData[$i]['Status'], 			"center", 0, 0);			//1
		AddColumnText($report, $arrData[$i]['Salesperson'], 	"left", 0, 0);		//2
		AddColumnText($report, $arrData[$i]['Company'], 		"left", 0, 0);			//3
		AddColumnText($report, $arrData[$i]['DateAssigned'], 	"center", 0, 0);		//4
		AddColumnText($report, $arrData[$i]['Offering'], 		"left", 0, 0);			//5
		AddColumnText($report, $arrData[$i]['FirstMeeting'], 	"center", 0, 0);		//6
		AddColumnText($report, $arrData[$i]['ForecastCloseDate'], "center", 0, 0);//7
		AddColumnText($report, $arrData[$i]['ActualCloseDate'], "center", 0, 0);	//8
		AddColumnText($report, $arrData[$i]['VLevel'], 			"center", 0, 0);
	}

}

function GetProducts($compid, &$arrProds, &$arrProdIDs)
{
	$sql = "select * from products where companyid = $compid";
	$ok = mssql_query($sql);
	while(($row=mssql_fetch_assoc($ok)))
	{
		$arrProds[$row['ProductID']] = $row['Name'];
		"'" . $ptext = $row['Name'] . "'";
		$arrProdIDs[$ptext] = $row['ProductID'];
	}
}

function GetSalesCycles($compid, &$arrCycles)
{
	$sql = "select * from SalesCycles where CompanyID = $compid order by PersonID, ProductID";
	$ok = mssql_query($sql);
	while(($row=mssql_fetch_assoc($ok)))
	{
		$ind = $row['PersonID'] . '|' . $row['ProductID'];
		$arrCycles[$ind] = $row['SalesCycleLength'];
	}
}

function GetCategoryValue($strCat)
{
	switch($strCat)
	{
		case 'First Meeting':
			return 1;
		case 'Information Phase':
			return 2;
		case 'Stalled Information Phase':
			return 3;
		case 'Decision Point':
			return 4;
		case 'Stalled Decision Point':
			return 5;
		case 'Closed':
			return 6;
	}
	return "<<Unknown: $strCat>>";
}

function GetCategoryStr($val)
{
	switch($val)
	{
		case 1:
			return 'FM';
		case 2:
			return 'IP';
		case 3:
			return 'IPS';
		case 4:
			return 'DP';
		case 5:
			return 'DPS';
		case 6:
			return 'CL';
		case 9:
			return 'RM';
		case 10:
			return 'T';
	}
	return $val;
}

function MakeDateStr($field, $dateFrom, $dateTo)
{
	return " AND $field >= '$dateFrom' AND $field <= '$dateTo'";
}

function GetClause($strfilter, &$strWhere, &$strParams, &$fcFrom, &$fcTo)
{
	$strWhere = "";
	$strParams = "";
	$arrTerms = explode('|', $strfilter);

	$type = $arrTerms[0];
	$value = $arrTerms[1];
	$valuetext = $arrTerms[2];
	$date_from = $arrTerms[3];
	$date_to = $arrTerms[4];

	switch($type)
	{
		case 'FM':
			$strWhere = MakeDateStr('FirstMeeting', $date_from, $date_to);
			$strParams = "First Meeting ($date_from to $date_to)";
			break;
		case 'FC':
			$fcFrom = $date_from;
			$fcTo = $date_to;
//			$strWhere = MakeDateStr('ForecastCloseDate', $date_from, $date_to);
			$strParams = "Forecast Close Date ($date_from to $date_to)";
			break;
		case 'AC':
			$strWhere = MakeDateStr('AdjustedCloseDate', $date_from, $date_to);
			$strParams = "Adjusted Close Date ($date_from to $date_to)";
			break;
		case 'SP':
			$names = explode(',', $valuetext);
			$fname = trim($names[1]);
			$lname = trim($names[0]);
			if(strlen($lname))
				$strWhere = " AND LastName='$lname'";
			if(strlen($fname))
				$strWhere .= " AND FirstName='$fname'";
			$strParams = "Salesperson ($valuetext)";
			break;
		case 'COMP':
			$strWhere = " AND Company = '$valuetext'";
			$strParams = "Company ($valuetext)";
			break;
		case 'PRODUCT':
//			$ProdID = $arrProdIDs[$valuetext];
//			$strWhere = " AND ProductID = $ProdID";
			$strWhere = " AND ProductID = $value";
			$strParams = "Offering ($valuetext)";
			break;
		case 'CATEGORY':
			$strWhere = " AND Category = " . GetCategoryValue($valuetext);
			$strParams = "Category ($valuetext)";

			break;
		case 'ACTIVE':
			$strWhere = " AND Category > 0 and Category < 8";
			$strParams = "Active opportunities";

			break;
		case 'REMOVED':
			$strWhere = " AND Category = 9";
			$strParams = "Removed opportunities";

	}
}

/*
function calc_close_date(first_meeting, personID, productID)
{
	if (productID < 0 || first_meeting == '')
		return '';
	var ss = find_salescycle(personID, productID);
	if (!ss)
		return '';
	var fm = new Date(Dates.mssql2us(first_meeting));
	return Dates.formatDate(new Date(fm.getTime() + (ss[g_salescycles.SalesCycleLength] * msPerDay)));
}
*/




function calc_close_date_ts($fm, $personid, $prodid)
{
global $arrSalesCycles;
	if ($personid < 0 || $prodid < 1 || $fm == '')
		return 0;

	$sPerDay = 24 * 60 * 60;
	$ind = "$personid|$prodid";
	$days = $arrSalesCycles[$ind];
	$sCycleLen = $days * $sPerDay;
	return(strtotime($fm) + $sCycleLen);
}


function GetOpportunities($companyid, &$arrResults, $arrProds, $arrProdIDs, &$arrParams, $arrFilters)
{
	$sql =  "SELECT * FROM opportunities";
	$sql .= " INNER JOIN people ON opportunities.PersonID = people.PersonID";
	$sql .= " WHERE Opportunities.CompanyID = $companyid";

	$strWhere = "";
	$strParams = "";
	$forecastFrom = '';
	$forecastTo = '';
	for ($i=0; $i < count($arrFilters); $i++)
	{
		GetClause($arrFilters[$i], $tmpWhere, $tmpParams, $forecastFrom, $forecastTo);
		if (strlen($tmpWhere))
			$strWhere .= $tmpWhere;
		if (strlen($tmpParams))
		{
			array_push($arrParams, $tmpParams);
		//	if(strlen($strParams)) $strParams .= ';';
		//	$strParams .= $tmpParams;
		}
	}

	if (!count($arrParams)) $arrParams[0] = "All Records";

	$sql .= $strWhere . " ORDER BY LastName, Category, Company";

	$ok = mssql_query($sql);
	if(!$ok)
		return 0;
	$tsFCFrom = 0;
	$tsFCTo = 0;
	if(strlen($forecastFrom ) && strlen($forecastTo))
		{
			$tsFCFrom = strtotime($forecastFrom);
			$tsFCTo	= strtotime($forecastTo);
		}

	while(($row = mssql_fetch_assoc($ok)))
	{
//Calc ForecastCloseDate
		$tsClose = calc_close_date_ts($row['FirstMeeting'], $row['PersonID'], $row['ProductID']);

		if($tsFCFrom && ($tsClose < $tsFCFrom || $tsClose > $tsFCTo))
			continue;

		$tmp = array();
		$tmp['ForecastCloseDate'] = $tsClose > 0 ? date("m/d/y", $tsClose) : '';

		$tmp['Status'] = GetCategoryStr($row['Category']);
		$tmp['Salesperson'] = $row['LastName'] . ', ' . $row['FirstName'];
		$tmp['Company']	= $row['Company'];
		$tmp['DateAssigned'] = fmdate($row['TargetDate']);
		$tmp['Offering'] = $arrProds[$row['ProductID']];
		$tmp['FirstMeeting'] = fmdate($row['FirstMeeting']);
//		$tmp['ForecastCloseDate'] = fmdate($row['ForecastCloseDate']);
		$tmp['ActualCloseDate'] = fmdate($row['ActualCloseDate']);
		$tmp['VLevel'] = ($row['VLevel'] > 0) ? $row['VLevel'] : '';
		array_push($arrResults, $tmp);

	}
	return 1;
}

function fmdate($strDate)
{
	if(!$strDate) return '';
	return date("m/d/y", strtotime($strDate));
}

/***************************PDF*************************/

$report = CreateReport();
//SetLandscape($report);
$report['pagewidth'] = 11*72;
$report['pageheight'] = 8.5*72;
$report['left'] = 72/4;
$report['right'] = 10.75 * 72;
$report['bottom'] = 8.25 * 72;


$report['left'] = 72/4;
$report['right'] = 10.75 * 72;

$p = PDF_new();
PDF_set_parameter($p, "license", PDFLIB_LICENSE_KEY);
PDF_open_file($p, "");
StartPDFReport($report, $p, "AAAA", "BBBB", "CCCCC");

SetLogo($report, "report_logo.jpg", 10, 40, 72, 0);


/*
BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, 0.1);
AddFreeFormText($report, $title, -1, "center", 1.5, 5.5);
*/



$left = 1;
$width1 = 1;
$width2 = 3;

$arrResults = array();
$arrProducts = array();
$arrProdIDs = array();
$arrFilters = array();
$arrParams = array();
$arrSalesCycles = array();


$filter = $_REQUEST['filter1'];
$i = 0;
while(1)
{
	$i++;
	$fname = "filter$i";
	$filter = $_REQUEST[$fname];
	if(!strlen($filter)) break;
	array_push($arrFilters, $filter);

}





GetProducts($mpower_companyid, $arrProducts, $arrProdIDs);
GetSalesCycles($mpower_companyid, $arrSalesCycles);


if(!GetOpportunities($mpower_companyid, $arrResults, $arrProducts, $arrProdIDs, $arrParams, $arrFilters))
	{
	BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, 0.1);
	AddFreeFormText($report, "Error retrieving opportunities", -1, "center", 1.5, 5.5);
	PrintPDFReport($report, $p);
	PDF_delete($p);
	close_db();
	}

$countRecs = count($arrResults);

if(count($arrParams) == 1)
	$title = "Opportunities ($countRecs records)";
else
	$title="Filtered Opportunities ($countRecs records)";

$report['footertext'] = $title;

BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, 0.1);
AddFreeFormText($report, $title, -1, "center", 2.75, 5.5);
BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0.1);
AddFreeFormText($report, 'Parameters:', -1, "center", 2.75, 5.5);
for ($i=0; $i < count($arrParams); $i++)
{
	BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0);
	AddFreeFormText($report, $arrParams[$i], -1, "center", 2.75, 5.5);

}

BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, 0.1);
AddFreeFormText($report, ' ', -1, "center", 2.75, 5.5);


PrintColTitles($report);

PrintData($report, $arrResults, $arrProducts);

//end_report();


PrintPDFReport($report, $p);
PDF_delete($p);

close_db();

?>
