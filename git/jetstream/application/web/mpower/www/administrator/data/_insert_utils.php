<?php
/**
 * @package Data
 */

include_once('_query_utils.php');
include_once('../include/log_obj.php');

function writeinslog($message)
{
    //$handle=fopen("/usr/local/apache/htdocs/logs/insert.log","a+");
    //$logtime=date("Y M j D G:i:s");
    //fwrite($handle,"$logtime $message\n");
    //fclose($handle);
    LogFile::WriteLine("$message");
}

function insert_row($table_name, $field_list, $ps_data, $index_field_name, $ignore_field_list)
{
	GLOBAL $currentuser_loginid;
writeinslog("$table_name: Inserting Row");
	$bUseChangedByID=$ignore_field_list=='ChangedByID';

	$ignore_array = explode(',', $ignore_field_list);
	$ignore_count = count($ignore_array);
	$ignore_map = array();
	for ($k = 0; $k < $ignore_count; ++$k)
	{
		$temp_str = trim($ignore_array[$k]);
		$ignore_map[$temp_str] = $temp_str;
	}

	$query_str = "INSERT INTO $table_name (";
	$field_array = explode(',', $field_list);
	// \' don't work here, replace with '', which becomes a single quote in the data base
	$value_array = explode('|', str_replace("'", "''", str_replace("\\\"", "\"", str_replace("\\'","'", $ps_data))));
	$field_count = count($field_array);
	$indexid_index = -1;
	$query_field_list = '';
	$query_value_list = '';
	$first_time = true;
	for ($k = 0; $k < $field_count; ++$k)
	{
		if ($field_array[$k] == $index_field_name)
			$indexid_index = $k;
		else if (!isset($ignore_map[$field_array[$k]]))
		{
			if ($first_time == false)
			{
				$query_field_list .= ', ';
				$query_value_list .= ', ';
			}
			else
				$first_time = false;
			$query_field_list .= $field_array[$k];
			if ($value_array[$k] == 'NULL' || strlen($value_array[$k]) == 0) $query_value_list .= 'NULL';
			else $query_value_list .= "'" . $value_array[$k] . "'";
		}
	}
	if ($bUseChangedByID)
	{
		$query_field_list.=', ChangedByID';
		$query_value_list.=", $currentuser_loginid";
	}
	$query_str .= $query_field_list . ') VALUES (' . $query_value_list . ')';
//	print('<nobr><pre>' . $query_str . '</pre></nobr>');
	$result = mssql_query($query_str);
	if ($result != false)
	{
		writeinslog("$table_name: Success - $query_str");
		return true;
	}
	else
	{
		writeinslog("$table_name: The was an error inserting: $query_str");
		return false;
	}
}
?>
