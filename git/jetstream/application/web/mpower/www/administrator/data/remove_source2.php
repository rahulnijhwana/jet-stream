<?php

print('<script language="JavaScript">');

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_admin_login())
{
	print('</script>');
	close_db();
	exit();
}

$result = mssql_query("UPDATE sources2 SET Deleted = 1 WHERE Source2ID = $mpower_remove_value");

print('window.result = ' . ($result ? 'true' : 'false') . ';</script>');

close_db();

?>