<?php
include_once('_base_utils.php');
include_once('_update_utils.php');

function print_result($result)
{
	print("window.result = '$result';\n");
	print("</script>\n");
	close_db();
	exit();
}

print("<script language=\"JavaScript\">\n");

if (!check_user_login())
	print_result('Invalid login');
if (!isset($mpower_data) || !strlen($mpower_data))
	print_result('No data to save');
if (!isset($mpower_fieldlist) || !strlen($mpower_fieldlist))
	print_result('Field list is missing');

print("</script>\n");
print('<div id="errmsg">');
$ok = update_rows('CloseQuestions', $mpower_fieldlist, $mpower_data, 'QuestionID', '');
//print($UPDATE_ERROR_MSG);
print('</div>');
print("<script language=\"JavaScript\">\n");

if ($ok)
	print_result('ok');
else
	print_result('Update opportunities failed.');

close_db();

?>
