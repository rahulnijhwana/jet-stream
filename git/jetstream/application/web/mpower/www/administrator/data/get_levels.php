<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

print("// ----- get_levels -----\n\n");
dump_sql_as_array('g_levels', "SELECT * FROM levels WHERE CompanyID IN ($companyList) order by LevelNumber");
?>
