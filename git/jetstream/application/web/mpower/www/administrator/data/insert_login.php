<?php
/**
 * @package Data
 */

print('<script language="JavaScript">');

include_once('_base_utils.php');
include_once('_insert_utils.php');

if (!check_admin_login())
{
	print('</script>');
	close_db();
	exit();
}

print('</script><div id="errmsg">');

$result = insert_row('Logins', $mpower_fieldlist, $mpower_data, 'ID', '');
print('</div><script language="JavaScript">');

if ($result != false)
{
	$result = mssql_query("SELECT @@IDENTITY AS 'ID'");
	$temp_assoc = mssql_fetch_assoc($result);
	print('window.result = ' . $temp_assoc['ID'] . ';</script>');
}
else
	print('window.result = false;</script>');

close_db();

?>