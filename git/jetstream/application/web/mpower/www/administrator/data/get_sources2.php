<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

print("// ----- get_sources2 -----\n\n");

$Source2List=array();	// this array associates the string with the ID
$Source2Name=-1;
$Source2ID=-1;

// callback function for dump_result that is processed for every row, used to build CM info
function BuildSource2List($resultset,$row)
{
	GLOBAL $Source2List,$Source2Name,$Source2ID;
	if ($Source2Name == -1)
	{
		$len = mssql_num_fields($resultset);
		for ($i = 0; $i < $len; ++$i)
		{
			$field = mssql_field_name($resultset, $i);
			if ($field=='Source2ID')
			{
				$Source2ID=$i;
			}
			else if ($field=='Name')
			{
				$Source2Name=$i;
			}
		}
	}
	$Source2List[$row[$Source2Name]]=$row[$Source2ID];
}

$result = mssql_query("select * from sources2 where CompanyID = '$mpower_companyid' and Deleted = '0' order by Name");
if ($result)
{
	dump_resultset_as_array('g_sources2', $result, 'BuildSource2List');
}

$result = mssql_query("select * from sources2 where CompanyID = '$mpower_companyid' and Deleted = '1'");
if ($result)
{
	dump_resultset_as_array('g_removed_sources2', $result, 'BuildSource2List');
}

?>
