<?php
/*  change log
5-27-07		LF	Opportunities are no longer retrieved immediately
*/

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_admin_login())
{
	close_db();
	exit();
}

if ($mpower_companyid == 85)
	ini_set('memory_limit', '16M');

$companyList="('$mpower_companyid')";	// set for compatibility with the board side

include('get_company.php');
//include('get_opportunities_people.php');
//include('get_unassigned_opportunities_people.php');
dump_nothing_as_array(array('g_opportunities','g_unassigned_opportunities'));
include('get_products.php');
include('get_people.php');
include('get_removereasons.php');
include('get_levels.php');
include('get_cycles.php');
include('get_close_questions.php');
include('get_milestone_questions.php');
include('get_submilestones.php');
include('get_msanalysisquestions.php');
include('get_valuations.php');
include('get_sources.php');
include('get_sources2.php');
include('get_logins.php');
include('get_milestone_labels.php');
include('get_category_labels.php');
include('get_reports_salespeople.php');
include('get_reports_company_xref.php');
include('get_opp_products_xrefs.php');
include('get_alias_owner_xref.php');
include('get_fiscal_year.php');
include('get_seasonality.php');
include('get_year.php');



$result = mssql_query('SELECT RightNow = GETDATE()');
$result = mssql_fetch_assoc($result);
print("window.g_data_birthdate = '" . $result['RightNow'] . "'\n\n");

close_db();

?>
