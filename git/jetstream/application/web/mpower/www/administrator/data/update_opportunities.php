<?php
/**
 * @package Data
 */

print('<script language="JavaScript">');

include_once('_base_utils.php');
include_once('_update_utils.php');

if (!check_admin_login())
{
	print('</script>');
	close_db();
	exit();
}

$result = @update_rows('opportunities', $mpower_fieldlist, $mpower_data, 'DealID', 'Color,FirstName,LastName');

if ($result == true)
{
	$field_array = explode(',', $mpower_fieldlist);
	$field_count = count($field_array);
	$value_array = explode('|', $mpower_data);
	$value_count = count($value_array);
	$person_id_index = -1;
	for ($k = 0; $k < $field_count; ++$k)
	{
		if ($field_array[$k] == 'PersonID')
		{
			$person_id_index = $k;
			break;
		}
	}
	
	if ($person_id_index != -1)
	{
		$updated_person_ids = array();
		for ($k = $person_id_index; $k < $value_count; $k += $field_count)
			$updated_person_ids[$value_array[$k]] = true;
		$where_list = false;
		foreach ($updated_person_ids as $key => $value)
		{
			if ($where_list == false)
				$where_list = 'PersonID = ' . $key;
			else
				$where_list .= ' OR PersonID = ' . $key;
			$result = mssql_query("SELECT SupervisorID, Level FROM people WHERE PersonID = $key");
			if ($result != false && mssql_num_rows($result) != 0)
			{
				$person_data = mssql_fetch_assoc($result);
				if ($person_data['Level'] == 1)
					$where_list .= ' OR PersonID = ' . $person_data['SupervisorID'];
			}
		}
		mssql_query("UPDATE people SET LastChanged = GETDATE() WHERE $where_list");
	}
}

print('window.result = ' . (($result != false) ? 'true' : 'false') . ';</script>');

close_db();

?>
