<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

print("// ----- get_analysis_curves -----\n\n");
dump_sql_as_array('g_analysisCurves', "select * from analysis");
?>
