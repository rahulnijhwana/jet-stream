<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

print("// ----- get_trendtext -----\n\n");
$result = mssql_query("select TrendID, Name, CAST([Content] AS TEXT) AS [Content], Calc, Ord from trends where TrendID = '$mpower_trendid'");
if ($result && ($row = mssql_fetch_assoc($result)))
{
	$result2 = mssql_query("select * from company where CompanyID = '$mpower_companyid'");
	if ($result2 && ($row2 = mssql_fetch_assoc($result2)))
	{
//First, patch in the userdef milestones	(1 && 6)
		if ($mpower_trendid == 13 || $mpower_trendid == 14)
		{
			$num = $mpower_trendid - 12;
			$fieldname = 'Requirement' . $num . 'trend';
		$row['Content'] = $row2["$fieldname"];
		}

//Now, substitute the user-defined milestone labels
//		if($mpower_trendid > 8)
//		{
			$arrDefaults = array('Person', 'Need', 'Money', 'Time');
			for ($i=0; $i< count($arrDefaults); $i++)
			{
				$ms = $i + 2;
				$fieldname = 'MS' .$ms . 'Label';
				$label = $fieldname . '=' . $row2[$fieldname];
				if (strlen($row2[$fieldname]) > 0)
					$label = $row2[$fieldname];
				else
					$label = $arrDefaults[$i];
				$strContent = $row['Content'];
				$row['Content'] = str_replace('{{' . $arrDefaults[$i] . '}}', $label, $strContent);
//			}
		}
//Substitute the user-defined column labels on the browser side

	}
	dump_assoc_as_array('g_trendtext', $row);
}
?>
