<?php
/**
 * @package Data
 */

print('<script language="JavaScript">');

include_once('_base_utils.php');
include_once('_update_utils.php');

if (!check_admin_login())
{
	print('</script>');
	close_db();
	exit();
}

$result = @update_rows('SubMilestones', $mpower_fieldlist, $mpower_data, 'SubID', '');

print('window.result = ' . (($result != false) ? 'true' : 'false') . ';</script>');

close_db();

?>
