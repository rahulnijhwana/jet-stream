<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_admin_login(true))
{
	close_db();
	exit();
}

$sql_str = "SELECT * FROM opportunities WHERE CompanyID = $mpower_companyid AND Category = 6";
$result = mssql_query($sql_str);

if ($result == false)
{
	close_db();
	exit("Error querying for closed opportunities.\nQuery: $sql_str");
}

if (mssql_num_rows($result) == 0)
{
	close_db();
	exit("This company has no closed opportunities\nQuery: $sql_str");
}

$data_str = '';

$questions_result = mssql_query("SELECT * FROM CloseQuestions WHERE CompanyID = $mpower_companyid AND [Use] = 1 ORDER BY QuestionID");
$questions = array();
while ($q_row = mssql_fetch_assoc($questions_result))
	array_push($questions, $q_row);

while ($opp_row = mssql_fetch_assoc($result))
{
	$sql_str = 'SELECT * FROM CloseAnswers WHERE OpportunityID = ' . $opp_row['DealID'] . ' ORDER BY QuestionID';
	$answer_result = mssql_query($sql_str);
	if ($answer_result == false)
	{
		close_db();
		exit("Error querying for close answers.\nQuery: $sql_str");
	}
	if (mssql_num_rows($answer_result) == 0)
		continue;
	$sql_str = 'SELECT * FROM people WHERE PersonID = ' . $opp_row['PersonID'];
	$person_result = mssql_query($sql_str);
	if ($person_result == false || mssql_num_rows($person_result) == 0)
	{
		close_db();
		exit("Error querying for person.\nQuery: $sql_str");
	}
	$person_row = mssql_fetch_assoc($person_result);
	$data_str .= $person_row['LastName'] . ', ' . $person_row['FirstName'];
	$data_str .= "\t" . $person_row['GroupName'];
	$data_str .= "\t" . $opp_row['Company'];
	$data_str .= "\t" . date('n/j/Y', strtotime($opp_row['ActualCloseDate']));
	$data_str .= "\t";
	$data_str .= $opp_row['ActualDollarAmount'];
	
	for ($k = 0; $k < count($questions); ++$k)
	{
		$single_result = mssql_query('SELECT * FROM CloseAnswers WHERE OpportunityID = ' . $opp_row['DealID'] . ' AND QuestionID = ' . $questions[$k]['QuestionID']);
		if ($answer_row = mssql_fetch_assoc($single_result))
		{
			if ($questions[$k]['YesNo'] == 1)
			{
				if ($answer_row['Answer'] === '1')
					$data_str .= "\tYes";
				else if ($answer_row['Answer'] === '0')
					$data_str .= "\tNo";
				else
					$data_str .= "\t";
			}
			else
			{
				$data_str .= "\t";
				$data_str .= $answer_row['Answer'];
			}
		}
		else
			$data_str .= "\t";
	}
	
	$data_str .= "\r\n";
}

//header('Content-type: text/plain');
header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=close_answers.txt");

print($data_str);

close_db();

?>
