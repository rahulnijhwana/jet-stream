<?php
/**
 * @package Data
 */

print('<script language="JavaScript">');

include_once('_base_utils.php');
include_once('_insert_utils.php');

if (!check_admin_login())
{
	print('</script>');
	close_db();
	exit();
}

print('</script><div id="errmsg">');

$result = insert_row('CompanyReportsXRef', $mpower_fieldlist, $mpower_data, 'ID', '');
print('</div><script language="JavaScript">');

if ($result == false)
	print('window.result = false;</script>');
else
	print('window.result = true;</script>');

close_db();

?>