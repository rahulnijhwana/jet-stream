<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_insert_utils.php');

function print_result($result)
{
	print("window.result = $result;\n");
	print("</script>\n");
	close_db();
	exit();
}

print("<script language=\"JavaScript\">\n");

if (!check_user_login())
	print_result('Invalid login.');
if (!isset($mpower_data) || !strlen($mpower_data))
	print_result('No data to save.');
if (!isset($mpower_fieldlist) || !strlen($mpower_fieldlist))
	print_result('Field list is missing.');

$rows = explode("\n", str_replace("\r\n","\n",$mpower_data));
$ok = true;
$len = count($rows);

print("</script>\n");
print('<div id="errmsg">');
for ($i = 0; $i < $len; ++$i)
{
	$result = insert_row('Opp_Product_Xref', $mpower_fieldlist, $rows[$i], 'ID', '');

	if (!$result)
		$ok = false;
}


print('</div>');
print("<script language=\"JavaScript\">\n");

if (!$ok)
	print_result('"Insert opp_product_xrefs failed."');
else
	print_result('"ok"');

close_db();

?>
