<?php
/**
 * @package Data
 */
include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

print("// ----- g_fiscal_year -----\n\n");

$sql = "SELECT F.Year, convert(varchar, F.FiscalYearEndDate, 1) FiscalYearEndDate,
			   F.PeriodLabel, F.Period, convert(varchar, F.PeriodEndDate, 1) PeriodEndDate, S.Seasonality
				FROM FiscalYear F
				Left join seasonality S on S.period = F.Period AND S.CompanyID = '$mpower_companyid' AND S.Year = F.Year 
				WHERE F.CompanyID = '$mpower_companyid' ORDER BY F.Year";
									
dump_sql_as_array('g_fiscal_year', $sql);
?>