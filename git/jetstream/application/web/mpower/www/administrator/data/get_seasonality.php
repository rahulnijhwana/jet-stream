<?php
/**
 * @package Data
 */
include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

print("// ----- g_fiscal_year -----\n\n");

$sql = "SELECT Year, SeasonalityEnabled
		FROM Seasonality 
		WHERE CompanyID = '$mpower_companyid' AND Period IS NULL AND PersonID IS NULL AND Seasonality IS NULL";
									
dump_sql_as_array('g_seasonality', $sql);
?>