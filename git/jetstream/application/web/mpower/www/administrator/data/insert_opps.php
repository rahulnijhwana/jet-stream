<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_insert_utils.php');

function print_result($result)
{
	print("window.result = $result;\n");
	print("</script>\n");
	close_db();
	exit();
}

writeinslog("Inserting Opps");


print("<script language=\"JavaScript\">\n");

if (!check_user_login())
	print_result('Invalid login.');
if (!isset($mpower_data) || !strlen($mpower_data))
	print_result('No data to save.');
if (!isset($mpower_fieldlist) || !strlen($mpower_fieldlist))
	print_result('Field list is missing.');

$field_array = explode(',', $mpower_fieldlist);
$field_count = count($field_array);
$DealID_index = -1;
for ($k = 0; $k < $field_count; ++$k)
{
	if ($field_array[$k] == 'DealID')
	{
		$DealID_index = $k;
		break;
	}
}

$rows = explode("\n", str_replace("\r\n","\n",$mpower_data));
$ok = true;
$len = count($rows);
$newids = 'new Array(';

print("</script>\n");
print('<div id="errmsg">');
for ($i = 0; $i < $len; ++$i)
{
writeinslog("Inserting One Opp");
	if ($i > 0)
		$newids .= ',';
	$temp_array = explode('|', $rows[$i]);
	$newids .= $temp_array[$DealID_index];
	$result = insert_row('opportunities', $mpower_fieldlist, $rows[$i], 'DealID', 'ChangedByID');
	if ($result != false)
	{
		$id_result = mssql_query("SELECT @@IDENTITY AS 'DealID'");
		$temp_assoc = mssql_fetch_assoc($id_result);
		$newids .= ',';
		$newids .= $temp_assoc['DealID'];
	}

	if (!$result)
	{
		$ok = false;
		writeinslog("Error Inserting Opp--failure to get new id");
	}
}

$newids .= ');';

//include('calc_salescycles.php');
print('</div>');
print("<script language=\"JavaScript\">\n");

if (!$ok)
{
	writeinslog("Error Inserting Opp--bad return code");
	print_result('"Insert opportunities failed."');
}
else
{
	writeinslog("Inserting Opps--Success");
	print_result($newids);
}
close_db();

?>
