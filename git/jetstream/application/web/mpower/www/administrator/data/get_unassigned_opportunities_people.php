<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

print("// ----- get_unassigned_opportunities_people -----\n\n");
dump_sql_as_array('g_unassigned_opportunities', "SELECT opportunities.*, Color = 'white', FirstName = '', LastName = 'Unassigned' FROM opportunities WHERE CompanyID = $mpower_companyid AND PersonID = -1");
?>