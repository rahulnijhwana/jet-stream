<?php
/**
 * @package Security
 */

/*
if (strtotime("October 8, 2010") < time())
{
	exit("document.write('<script language=\"JavaScript\">window.g_overflow_alert = true;</script>');");

}
*/

include_once('_db2.php');

$currentuser_level = -2; // -3 backdoor, -2 is as-yet-unknown, -1 is insufficient info, 0 is bad login, int > 0 is user's level
$currentuser_isadmin = 0;
$currentuser_viewsiblings = 0;
$currentuser_personid = -1;
$currentuser_eula = 0;
$currentuser_data = array();
$currentuser_accesstype = 0; // 0 is no access type, 1 is salesperson, 2 is sales manager

function SetBoardSuper()
{
	global $mpower_companyid, $mpower_userid, $mpower_pwd;
	$sql = "select max([level]) as toplevel from people where companyid=$mpower_companyid and Deleted<>1 group by companyid";
	$ok=mssql_query($sql);
	if($ok)
	{
		$row=mssql_fetch_assoc($ok);
		if($row)
		{
			$toplevel=$row['toplevel'];
			$sql =  "SELECT Logins.UserID as uid, Logins.PasswordZ as pw";
			$sql .= " FROM people INNER JOIN Logins";
			$sql .= " ON people.PersonID = Logins.PersonID";
			$sql .= " WHERE (Logins.CompanyID = $mpower_companyid) AND (people.Deleted = 0) AND (people.[Level] = $toplevel) and (people.SupervisorID <> -3)";
			$ok1=mssql_query($sql);
			if($ok1)
			{
				$row1=mssql_fetch_assoc($ok1);
				if($row1)
				{
					$mpower_userid=$row1['uid'];
					$mpower_pwd=$row1['pw'];
				}
			}
		}
	}
}

/**
 * checks the current login, sets a few global variables indicating the result
 */
function check_login()
{
	global $currentuser_level, $currentuser_isadmin, $currentuser_viewsiblings,
			$currentuser_personid, $currentuser_eula, $currentuser_loginid,
			$mpower_userid, $mpower_pwd, $mpower_companyid, $currentuser_data, $noUpdateLastUsed;

	if ($currentuser_level != -2) return;

	$currentuser_level = -1;

	if (!isset($mpower_userid) || !isset($mpower_pwd) || !isset($mpower_companyid) ||
		(int) $mpower_companyid < 0 || !strlen($mpower_userid)) {
			return;
	}
    // *** Get superuser from DB ***
	// $sql = "select * from su";
	// $ok = mssql_query($sql);
//	if(($su=mssql_fetch_assoc($ok)))
//	{
//		$suID = $su['UID'];
//		$suPW = $su['PW'];
//	}
	$suID = 'supr3m3';
	$suPW = 'rul3r';
// ******************************
	if ($mpower_userid==$suID && $mpower_pwd==$suPW)
	{
		$currentuser_level=-3;
		$currentuser_isadmin=1;
		$currentuser_eula=1;
	} else {
        //Do board superuser ASAP
		if (($mpower_userid=="a2a" && $mpower_pwd=="c2ar") ||
			($mpower_userid=="asahs" && $mpower_pwd=="87621"))  {
			SetBoardSuper();
			$noUpdateLastUsed = true;
		}

		
        //Alter to look at logins table instead

		$sql = "select logins.ID, logins.Eula as LoginEula, logins.PasswordZ as RealPassword, people.*
			from logins left join people on people.PersonID = logins.PersonID
			where convert(varbinary(50),logins.UserID) = convert(varbinary(50),'$mpower_userid') and Logins.CompanyID = '$mpower_companyid'";
        
        //$sql = "select * from logins where convert(varbinary(50),UserID) = convert(varbinary(50),'$mpower_userid') and CompanyID = '$mpower_companyid'";
		$result = mssql_query($sql);
		if ($row = mssql_fetch_assoc($result)) {
			// $realpwd = $row['PasswordZ'];
			$realpwd = $row['RealPassword'];
			if (strcmp($mpower_pwd, $realpwd)) {
				$currentuser_level = 0;
				return;
			}
			$pid = $row['PersonID'];
			$currentuser_loginid = $row['ID'];

            // NB: EULA now stored in Logins table
			$currentuser_eula = $row['LoginEula'];

			$currentuser_level = $row['Level'];
			$currentuser_isadmin = ($row['SupervisorID'] == -3);
			$currentuser_viewsiblings = $row['ViewSiblings'];
			$currentuser_personid = $row['PersonID'];
			$currentuser_data = $row;		// this will allow access to all the current user fields
		}
	}
}

/**
 * check login for regular user
 */
function check_user_login() {
	global $currentuser_level, $currentuser_isadmin, $mpower_userid, $mpower_pwd;
	if ($currentuser_level == -2) {
		check_login();
		print("window.loginResult = $currentuser_level;\n");
		if ($currentuser_isadmin) print("window.isAdmin = true;\n\n");
	}

	return (0 < $currentuser_level || $currentuser_level == -3);
}

function check_user_login_quiet()
{
	global $currentuser_level, $currentuser_isadmin, $mpower_userid, $mpower_pwd;
	if ($currentuser_level == -2)
	{
		check_login();
	}

	return (0 < $currentuser_level || $currentuser_level == -3);
}

/**
 * check admin login
 */
function check_admin_login($supress_js_output = false)
{
	global $currentuser_isadmin, $currentuser_level;
	if ($currentuser_level == -2)
	{
		check_login();
		if ($supress_js_output == false)
			print("window.loginResult = $currentuser_isadmin;\n\n");
	}

	return (0 < $currentuser_isadmin);
}

/**
 * check login to view reports (admin or level >= 2)
 */
function check_report_login()
{
	global $currentuser_isadmin, $currentuser_level;
	check_login();
    // Selected reports now enabled on salesperson's board
	return ($currentuser_isadmin == 1 || 1 <= $currentuser_level);

}

function calc_access_type()
{
	global $currentuser_level, $currentuser_accesstype;
	if ($currentuser_level == 1) $currentuser_accesstype = 1;
	else if (1 < $currentuser_level) $currentuser_accesstype = 2;
}
?>
