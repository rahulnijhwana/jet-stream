<?php

print('<script language="JavaScript">');

include_once('_base_utils.php');
include_once('_insert_utils.php');

if (!check_admin_login())
{
	print('</script>');
	close_db();
	exit();
}

$result = @insert_row('Sources', $mpower_fieldlist, $mpower_data, 'SourceID', '');

print('window.result = ' . ($result ? 'true' : 'false') . ';</script>');

close_db();

?>