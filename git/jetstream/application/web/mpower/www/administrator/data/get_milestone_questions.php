<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

print("// ----- get_person_questions -----\n\n");
dump_sql_as_array('g_person_questions', "select * from CloseQuestions where CompanyID = '$mpower_companyid' and Type = 1 order by QuestionID");

print("// ----- get_need_questions -----\n\n");
dump_sql_as_array('g_need_questions', "select * from CloseQuestions where CompanyID = '$mpower_companyid' and Type = 2 order by QuestionID");

print("// ----- get_money_questions -----\n\n");
dump_sql_as_array('g_money_questions', "select * from CloseQuestions where CompanyID = '$mpower_companyid' and Type = 3 order by QuestionID");

print("// ----- get_time_questions -----\n\n");
dump_sql_as_array('g_time_questions', "select * from CloseQuestions where CompanyID = '$mpower_companyid' and Type = 4 order by QuestionID");

print("// ----- get_req1_questions -----\n\n");
dump_sql_as_array('g_req1_questions', "select * from CloseQuestions where CompanyID = '$mpower_companyid' and Type = 5 order by QuestionID");

print("// ----- get_req2_questions -----\n\n");
dump_sql_as_array('g_req2_questions', "select * from CloseQuestions where CompanyID = '$mpower_companyid' and Type = 6 order by QuestionID");

?>
