<?php

print('<script language="JavaScript">');

include_once('_base_utils.php');
include_once('_insert_utils.php');

if (!check_admin_login())
{
	print('</script>');
	close_db();
	exit();
}

$result = @insert_row('Alias2OwnerXRef', $mpower_fieldlist, $mpower_data, 'ID', '');

print('window.result = ' . ($result ? 'true' : 'false') . ';</script>');

close_db();

?>