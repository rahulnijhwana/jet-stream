<?php

print('<script language="JavaScript">');

include_once('_base_utils.php');
include_once('_insert_utils.php');

if (!check_admin_login())
{
	print('</script>');
	close_db();
	exit();
}

print('</script><div id="errmsg">');
$result = insert_row('Valuations', $mpower_fieldlist, $mpower_data, 'ValID', '');
print('</div><script language="JavaScript">');

if ($result != false)
{
	$idresult = mssql_query("SELECT @@IDENTITY AS 'ValID'");
	$temp_assoc = mssql_fetch_assoc($idresult);
	print('window.result = ' . $temp_assoc['ValID'] . ';</script>');
}
else
	print('window.result = false;</script>');

close_db();

?>