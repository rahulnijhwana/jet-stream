<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

print("// ----- get_grouppath -----\n\n");

// For optimization sake this function should probably be replaced with a
// stored procedure
function get_group_name($id) {
	global $mpower_companyid;

	$fields = 'FirstName, LastName, GroupName, SupervisorID';
	$sql = "select $fields from people where CompanyID = '$mpower_companyid' and PersonID = '$id'";
	$result = mssql_query($sql);
	if ($result && ($row = mssql_fetch_assoc($result)))
	{
		$gname = $row['GroupName'];
		if ($gname == null || strlen($gname) == 0)
			$gname = $row['FirstName'] . ' ' . $row['LastName'];
		$sid = (int) $row['SupervisorID'];
		if (0 < $sid) $gname = get_group_name($sid) . ': ' . $gname;
		return $gname;
	}
	else return "";
}

print('g_grouppath = "' . get_group_name($currentuser_personid) . "\";\n\n");

?>
