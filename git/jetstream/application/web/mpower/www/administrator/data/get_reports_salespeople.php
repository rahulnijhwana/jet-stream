<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

print("// ----- get_reports -----\n\n");
//dump_sql_as_array('g_reports', 'select * from reports order by reports.Name');

dump_sql_as_array('g_reports', 'select * from reports where Salespeople = 1 order by Name');

?>
