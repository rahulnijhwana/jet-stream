function list_valuation_options(val_is_selected)
{
	var ret = '';
	for (var k = 0; k < g_valuations.length; ++k)
	{
		var data = g_valuations[k];
		if (data[g_valuations.Activated] != true)
			continue;
		ret += '<option value="' + data[g_valuations.VLevel] + '"';
		if (data[g_valuations.VLevel] == val_is_selected)
		{
			ret += ' selected';
		}
		ret += '>[' + data[g_valuations.VLevel] + ']&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + data[g_valuations.Label];

		ret += '</option>';
	}
	return ret;
}



function list_source_options(val_is_selected) {
	var ret = '';
	for (var k = 0; k < g_sources.length; ++k) {
		var data = g_sources[k];
		ret += '<option value="' + data[g_sources.SourceID] + '"';
		if (data[g_sources.SourceID] == val_is_selected) {
			ret += ' selected';
		}
		ret += '>' + data[g_sources.Name] + '</option>';
	}
	return ret;
}

function getValArray() {
	var ret = Array();
	var vals = g_valuations;
	for (var k = 0; k < vals.length; ++k) {
		var data = vals[k];
		if (data[g_valuations.Activated] != true)
			continue;
		var tmp = Array();
		tmp[0] = data[g_valuations.VLevel];
		tmp[1] = '[' + data[g_valuations.VLevel] + ']     ' + data[g_valuations.Label];
		ret[ret.length] = tmp;
	}
	return ret;

}

function getPeopleArray() {
	var ret = Array();
	for (var k = 0; k < g_people.length; k++) {
		var data = g_people[k];
		if (data[g_people.IsSalesperson] != '1')
			continue;
		var tmp = Array();
		tmp[0] = data[g_people.PersonID];
		tmp[1] = data[g_people.LastName] + ', ' + data[g_people.FirstName];
		ret[ret.length] = tmp;
	}
	return ret;
}
