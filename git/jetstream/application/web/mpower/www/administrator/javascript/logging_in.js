
function get_cookie(cookieName)
{
	var name = cookieName + '=';
	var dc = document.cookie;
	if (dc.length > 0)
	{
		var begin = dc.indexOf(name);
		if (begin != -1)
		{
			begin += name.length;
			var end = dc.indexOf(';', begin);
			if (end == -1) end = dc.length;
			return unescape(dc.substring(begin, end));
		}
	}

	return null;
}

function set_cookie(cookieName, cookieValue, permanent)
{
	var neverExpires = new Date('01/01/2050');
	if (set_cookie.arguments.length < 2) return false;

	var cookie = cookieName += '=' + escape(cookieValue) + '; path=/';
	if (permanent) cookie += '; expires=' + neverExpires.toUTCString();
	document.cookie = cookie;
	return true;
}

function delete_cookie(cookieName)
{
	document.cookie = cookieName + '=; expires=Thu, 01-Jan-70 00:00:01 GMT; path=/';
	return true;
}

function init(skipSetFocus)
{
	set_cookie('mpower_effective_userid', '');
	set_cookie('sib_mode', '');
	set_cookie('view_mode', '');
	set_cookie('view_previous_mode','');
	if (!skipSetFocus)
		document.forms['form1'].elements.uname.focus();
}

function enter(branch)
{
	if (!branch)
		branch = 'current';
	var f = document.forms['form1'];
	var uname = f.elements.uname.value;
	var pass = f.elements.pass.value;

	if (uname == '')
	{
		alert('Please enter your username to continue.');
		f.elements.uname.focus();
		return;
	}

	if (pass == '')
	{
		alert('Please enter your password to continue.');
		f.elements.pass.focus();
		return;
	}

	set_cookie('mpower_userid', uname, false);
	set_cookie('mpower_pwd', pass, false);
	set_cookie('mpower_companyid', f.elements.company.value, false);
    

//	set_cookie('mpower_userid', uname);
//	set_cookie('mpower_pwd', pass);
//	set_cookie('mpower_companyid', f.elements.company.value);

	var test = get_cookie('mpower_userid');
	if (test != uname)
	{
		alert('You must have cookies enabled to log in.');
		return;
	}

	window.location.href = '/' + branch + '/login.php';
}

function set_cookie(cookieName, cookieValue, permanent)
{
	var neverExpires = new Date('01/01/2050');
	if (set_cookie.arguments.length < 2) return false;

	var cookie = cookieName += '=' + escape(cookieValue) + '; path=/';
	if (permanent) cookie += '; expires=' + neverExpires.toUTCString();

	document.cookie = cookie;
	return true;
}

function get_cookie(cookieName)
{
	var name = cookieName + '=';
	var dc = document.cookie;

	if (dc.length > 0)
	{
		var begin = dc.indexOf(name);
		if (begin != -1)
		{
			begin += name.length;
			var end = dc.indexOf(';', begin);
			if (end == -1) end = dc.length;
			return unescape(dc.substring(begin, end));
		}
	}

	return null;
}
