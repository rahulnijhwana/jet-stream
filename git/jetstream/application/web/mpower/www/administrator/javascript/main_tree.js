
Tree = new Object();

Tree.makeHTML = _Tree_makeHTML;
Tree.navToBoard = _Tree_navToBoard;
Tree.choiceCount = 0;
Tree.lastChoice = null;
Tree.topguy = null;

Tree.m_indent = '&nbsp;&nbsp;&nbsp;&nbsp;';

function xb_refresh()
{
	if ((navigator.appName.indexOf("Netscape") != -1))
		window.location.href = 'main.html';
	else
		window.history.go(0);
}

function navFlatView(personID)
{
	_Tree_setCookie('mpower_effective_userid', personID);
	_Tree_setCookie('flat_view_id', personID);
	xb_refresh();
}

// valid node_types are "view", "sibling", "mine"
function _Tree_makeHTML(top, node_type)
{
	Tree.choiceCount = 0;
	Tree.lastChoice = null;
	var ret = '<p class="text">Please select the person or group whose board you would like to view:</p>';
	ret += '<table align="center" border=0><tr><td align="left">';
	ret += _Tree_makePerson(top, 0, node_type);
	ret += '</td></tr></table><br>';
	return ret;
}

function _Tree_makePerson(person, indent, node_type)
{
	var ret = '';
	for (var j = 0; j < indent; ++j)
		ret += Tree.m_indent;

	ret += '<img src="../images/sp_icon.gif"> ';
	
	var nolink=true;
	if (node_type != "view")
	{
		// all manager-salesmen have themselves on their board
		nolink = person[g_people.IsSalesperson] != 1;
		for (var k = 0; k < g_people.length && nolink; ++k)
		{
			var peo=g_people[k];
			
			if (peo[g_people.SupervisorID] == person[g_people.PersonID] 
				&& peo[g_people.IsSalesperson] == 1
				&& (peo[g_people.Level]==1 || peo[g_people.AppearsOnManager]==1))
			{
				nolink = false;
			}
		}
	}

	if (!nolink)
	{
		Tree.choiceCount++;
		Tree.lastChoice = {PersonID:person[g_people.PersonID], sib:((node_type=="sibling")?'yes':'')};
		ret += '<a href="javascript:Tree.navToBoard(' + person[g_people.PersonID] + ',';
		if (node_type=="sibling")
			ret+="\'yes\'";
		else
			ret+="\'\'";
		ret+=')">';
	}
	ret += person[g_people.FirstName] + ' ' + person[g_people.LastName];
	if (person[g_people.GroupName] != '')
		ret += ' (' + person[g_people.GroupName] + ')';
	if (node_type=="sibling" && !nolink) ret += " [View Only]";	
	if (!nolink)
		ret += '</a>';
	if (person[g_people.Level] > 2)
		ret += ' <a href="javascript:navFlatView('+person[g_people.PersonID]+');" style="font-size:10pt;">Combined View</a>';
	ret += '<br>';

	for (var k = 0; k < g_people.length; ++k)
	{
		if (g_people[k][g_people.SupervisorID] != person[g_people.PersonID] || g_people[k][g_people.Level] == 1)
			continue;
		var next_node=node_type;
		if (node_type == "view")	// see if we need to figure out sibling or mine
		{
			if (g_people[k][g_people.PersonID]==g_user[0][g_user.PersonID])
				next_node="mine";
			else
				next_node="sibling";
		}
		ret += _Tree_makePerson(g_people[k], indent + 1,next_node);
	}

	return ret;
}

function _Tree_navToBoard(personID,sib)
{
	_Tree_setCookie('mpower_effective_userid', personID);
	_Tree_setCookie('sib_mode',sib);
	_Tree_setCookie('view_mode','');

	// this doesn't work in mozilla because it doesn't check the server again..
	// perhaps setting an expires header on the server will solve that?
	xb_refresh();
}

function _Tree_setCookie(cookieName, cookieValue)
{
	var cookie = cookieName += '=' + escape(cookieValue) + '; path=/';
	document.cookie = cookie;
}
