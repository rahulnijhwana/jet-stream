function find_person(id)
{
	for (var i = 0; i < g_people.length; ++i)
		if (g_people[i][g_people.PersonID] == id) return g_people[i];

	return null;
}

function get_salesperson_curve(id)
{
	var ret = new Object();

	var person = find_person(id);
	if (!person) return ret;

	ret.fmValue = person[g_people.FMValue];
	ret.ipValue = person[g_people.IPValue];
	ret.dpValue = person[g_people.DPValue];
	ret.cValue = person[g_people.CloseValue];
	return ret;
}

function get_salesperson_info(id)
{
	var ret = new Object();

	var monthsNew = g_company[0][g_company.MonthsConsideredNew];
	for (var i = 0; i < g_people.length; ++i)
	{
		if (g_people[i][g_people.PersonID] != id)
			continue;
		ret.rating = g_people[i][g_people.Rating];
		ret.color = g_people[i][g_people.Color];
		ret.name = g_people[i][g_people.FirstName] + ' ' + g_people[i][g_people.LastName];
		ret.rawdata = g_people[i];

		var expDate = new Date(Dates.mssql2us(g_people[i][g_people.StartDate]));
		for (; monthsNew>0; --monthsNew)
		{
			var m = expDate.getMonth();
			if (m == 11)
			{
				expDate.setMonth(0);
				expDate.setYear(expDate.getYear() + 1);
			}
			else expDate.setMonth(m + 1);
		}
		if (expDate.getTime() > new Date().getTime())
		{
			ret.tenure = 'New';
			if (g_user[0][g_user.Level] > 1)
				ret.name += '&nbsp;(n)';
		}
		else
			ret.tenure = 'Experienced';

		ret.analysisID = g_people[i][g_people.AnalysisID];
		ret.PersonID = g_people[i][g_people.PersonID];
		ret.CompanyID= g_people[i][g_people.CompanyID];
		break;
	}

	return ret;
}

function getValuationAvg(idPerson)
{
	var vCount=0;
	var vTotal=0;
	var vAvg=0;
	var i;
	for(i=0; i<g_opps.length; i++)
	{
        //Presumably, exclude removed and target opps from average		
		if (g_opps[i][g_opps.PersonID]!=idPerson) continue;
		if (g_opps[i][g_opps.Category]=='6')		
			continue;
		if(g_opps[i][g_opps.VLevel] < '1') continue;
		vCount++;
		vTotal+=Number(g_opps[i][g_opps.VLevel]);

	}

	if(vCount>0)
	{
		vAvg = vTotal/vCount;
	}	
	return vAvg;
}

function make_curve_desc()
{
	if (g_curve.fmValue == null || g_curve.ipValue == null || g_curve.dpValue == null ||
		g_curve.cValue == null)
		return 'Curve Unknown';

	var ret = '<div align="right" style="color: ivory">';
	ret += '</div>';
	return ret;
}

function myOnKeyDown(e)
{
	if (!isNav)
		e = event;

	if (e.keyCode == matches[cur_match])
	{
		++cur_match;
		if (cur_match == matches.length)
		{
			alert("Curve: " + g_curve.fmValue + ' ' + g_curve.ipValue + ' ' + g_curve.dpValue + ' ' + g_curve.cValue);
			cur_match = 0;
		}
	}
	else
		cur_match = 0;
}

function redraw()
{
	document.body.style.cursor = 'wait';
	if (window.opener) document.getElementById('theAlerts').innerHTML = window.opener.Alerts.makeHTML();
	document.body.style.cursor = 'auto';
}

function round_float(val, shft)
{
	val*=shft;
	val=Math.round(val);
	return val/shft;
}


function show()
{
	var els = document.forms['form1'].elements;
	update_visibility('div_analysis', els.chk_analysis.checked);
	update_visibility('div_questions', els.chk_questions.checked);
	update_visibility('div_actions', els.chk_actions.checked);
}

function update_from_edit_window()
{
	redraw();
}

function update_visibility(id, makeVisible)
{
	var el = document.getElementById(id);
	if (!el) return;

	if (makeVisible) el.style.display = 'block';
	else el.style.display = 'none';
}

function write_actions()
{
	document.writeln('<div id="div_actions">');
	document.writeln(ivoryBox.makeTop());
	write_subheader('Actions');
	document.writeln('<ol>');

	var fld = (g_isNew ? g_analysisActions.ActionTextNew : g_analysisActions.ActionTextExperienced);

	for (var i = 0; i < g_analysisActions.length; ++i)
		document.writeln('<li>' + substituteCategoryLabels(g_analysisActions[i][fld]) + '</li>');

	if (g_analysisActions.length == 0)
		document.writeln('<p>No actions available.</p>');

	document.writeln('</ol>');
	document.writeln(ivoryBox.makeBottom());
	document.writeln('</div>');
}

function write_analysis(ivoryBox)
{
	document.writeln('<div id="div_analysis">');
	document.writeln(ivoryBox.makeTop());
	write_subheader('Analysis');
	document.writeln('<ol>');

	for (var i = 0; i < g_analysisReasons.length; ++i)
		document.writeln('<li>' + substituteCategoryLabels(g_analysisReasons[i][g_analysisReasons.ReasonText]) + '</li>');


	if (g_analysisReasons.length == 0)
		document.writeln('<p>No analysis available.</p>');

	document.writeln('</ol>');
	document.writeln(ivoryBox.makeBottom());
	document.writeln('</div>');
}

function write_header()
{
	var bVal = (window.opener.g_company[0][window.opener.g_company.ValuationUsed] == '1');

	var rating = g_info.rating;
	if (rating < 0) rating = 'Unlikely';

	var avgVal=window.opener.getValuationAvg(g_info.PersonID);
	avgVal=round_float(avgVal, 10);

	document.writeln('<br><table border="0" width="98%" class="analysis" bgcolor="black">');

	document.writeln('<tr border="0" bgcolor="' + g_info.color + '">');
	document.writeln('<td border="0" width="50%" nowrap><b>Rating:</b> ' + rating + '&nbsp;&nbsp;&nbsp;<b>Tenure Level:</b> ' + g_info.tenure + '</td>');
	if (bVal)
	{
		document.writeln('<td border="0" width="50%" nowrap align="right"><b>Average '+opener.g_company[0][opener.g_company.ValLabel]+':</b> ' + avgVal + '</td>');
	}


	document.writeln('</tr></table><br>');
}

// displays the user defined questions from the milestones
// for the specified milestone
function write_MSquestions(milestone,showQuestions)
{
	for (var i=0;i<g_msAnalysisQuestions.length; i++)
	{
		if (g_msAnalysisQuestions[i][g_msAnalysisQuestions.Location]==milestone)
		{
			if (!showQuestions) return true;
			document.writeln('<li>' + g_msAnalysisQuestions[i][g_msAnalysisQuestions.Prompt] + '</li>');
		}
	}
	return false;
}

function write_questions()
{
	document.writeln('<div id="div_questions">');
	document.writeln(ivoryBox.makeTop());
	write_subheader('Questions');
	document.writeln('<ol>');

	var bAnyQuestions=false;
	var Milestone= 0;
	var Heading="";
	var bShowHeading=false;
	for (var i = 0; i < g_analysisQuestions.length; ++i)
	{
		if (g_analysisQuestions[i][g_analysisQuestions.IsHeader] > '0')
		{
			if (Milestone != 0)
			{
				// first, write any final user defined questions
				if (bShowHeading==true && write_MSquestions(Milestone,false))
				{
					document.writeln('<br><br>');
					document.writeln('<div class="title" style="height: 23px">');
					document.writeln( Heading+ '</div>');
				}
				write_MSquestions(Milestone,true);
				bAnyQuestions=true;
			}
			Heading = substituteCategoryLabels(g_analysisQuestions[i][g_analysisQuestions.QuestionText]);
			bShowHeading=true;
			Milestone=g_analysisQuestions[i][g_analysisQuestions.IsHeader];
		}
		else
		{
			if (bShowHeading==true)
			{
				document.writeln('<br><br>');
				document.writeln('<div class="title" style="height: 23px">');
				document.writeln( Heading+ '</div>');
				bShowHeading=false;
			}
			document.writeln('<li>' + substituteCategoryLabels(g_analysisQuestions[i][g_analysisQuestions.QuestionText]) + '</li>');

			bAnyQuestions=true;
		}
	}

	if (opener.g_company[0][opener.g_company.Requirement1used]=='1')
	{
		if (write_MSquestions(0,false))
		{
			document.writeln('<br><br>');
			document.writeln('<div class="title" style="height: 23px">');
			document.writeln( "Questions about " + opener.g_company[0][opener.g_company.Requirement1] + '</div>');
			write_MSquestions(0,true);
			bAnyQuestions=true;
		}
	}

	if (opener.g_company[0][opener.g_company.Requirement2used]=='1')
	{
		if (write_MSquestions(5,false))
		{
			document.writeln('<br><br>');
			document.writeln('<div class="title" style="height: 23px">');
			document.writeln( "Questions about " + opener.g_company[0][opener.g_company.Requirement2] + '</div>');
			write_MSquestions(5,true);
			bAnyQuestions=true;
		}
	}

	if (g_analysisQuestions.length == 0)
		document.writeln('<p>No questions available.</p>');

	document.writeln('</ol>');
	document.writeln(ivoryBox.makeBottom());
	document.writeln('</div>');
}

function write_showboxes()
{
	document.writeln('<form name="form1">&nbsp;&nbsp;&nbsp; Show:');
	document.writeln('&nbsp;&nbsp;&nbsp;<input type="checkbox" name="chk_analysis" onclick="show()" checked> Analysis');
	document.writeln('&nbsp;&nbsp;&nbsp;<input type="checkbox" name="chk_questions" onclick="show()" checked> Questions');
	document.writeln('&nbsp;&nbsp;&nbsp;<input type="checkbox" name="chk_actions" onclick="show()" checked> Actions');
	document.writeln('</form>');
}

function write_subheader(val)
{
	document.writeln('<div class="title" align="center">');
	document.writeln(val + '</div>');
}

function write_unlikely_reason(ivoryBox, strReason)
{
	document.writeln('<div id="div_analysis">');
	document.writeln(ivoryBox.makeTop());
	write_subheader('Unlikely Curve');
	document.writeln('<ol>');

	document.writeln('<p>' + strReason + '</p>');

	document.writeln('</ol>');
	document.writeln(ivoryBox.makeBottom());
	document.writeln('</div>');
}

