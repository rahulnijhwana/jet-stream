function findSupervisor(spdata, sp)
{
	for (var k = 0; k < spdata.length; ++k)
	{
		if (sp[spdata.SupervisorID] == spdata[k][spdata.PersonID])
			return spdata[k];
	}
	return null;
}

function linkSalespersons(spdata)
{
	for (var k = 0; k < spdata.length; ++k)
		spdata[k].supervisor = findSupervisor(spdata, salespersons[k]);
}

function generateSPLeafText(sp)
{
	var str = "<CENTER><B>" + sp[this.m_objArray.FirstName] + " " + sp[this.m_objArray.LastName] + "</B></CENTER>";
	return str;
}

function makeSPOrgTree(spdata, indexOfRootNode, levsVisAtOnce, showControls)
{
	linkSalespersons(spdata);
	spOrgTree = new OrgTree("supervisor", spdata, (indexOfRootNode < 0) ? null : salespersons[indexOfRootNode]);
	spOrgTree.getObjTextFunc = generateSPLeafText;
	if (showControls)
		spOrgTree.showNodeControls = showControls;
	if (!levsVisAtOnce)
		levsVisAtOnce = 2;
	spOrgTree.setNumberOfLevelsVisible(levsVisAtOnce);
	return spOrgTree;
}

function updateSPOrgTree(tree, spdata)
{
	linkSalespersons(spdata);
	tree.m_objArray = spdata;
	if (tree.m_baseobj == null)
	{
		var rootupdated = false;
		for (var k = 0; k < spdata.length; ++k)
		{
			if (spdata[k][spdata.id] == tree.m_baseobj[spdata.id])
			{
				tree.m_baseobj = spdata[k];
				rootupdated = true;
			}
		}
		if (!rootupdated)
			tree.m_baseobj = null;
	}
	tree.linkObjs();
	tree.redraw();
}