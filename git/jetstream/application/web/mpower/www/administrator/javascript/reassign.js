function do_reassign()
{
	var personID=ssSalesPerson.getSelectedVal();
	if (personID==-1)
	{
		alert("Select a new salesman.");
		return;
	}

	var i;
	var update_from_mass_op=window.opener.update_from_mass_op;
	var find_person=window.opener.find_person;
	for (i=0;i<g_massOpps.length;i++)
	{
		var opp=window.opener.find_opportunity(g_massOpps[i]);
		var oldPersonID=opp[g_opps.PersonID];
		var oldCat=opp[g_opps.Category];
//If target, accumulate prev days old
//Also if stalled
		if (oldCat == '10' || oldCat == '3' || oldCat == '5')
		{
			var prevdays = opp[g_opps.PrevDaysOld];
			var now = new Date();
			//var newTargetDate = 1*(1+now.getMonth()) + '/' + now.getDate() + '/' + now.getFullYear();
			var newTargetDate = Dates.formatDate(now);
			var days = 0;
			var td = Dates.mssql2us(opp[g_opps.TargetDate]);
            if (td) {
    			days = getDaysOld(td);
            }
			opp[g_opps.TargetDate] = newTargetDate;
			//var lm;
			//if (opp[g_opps.LastMoved] == '')
			//	lm=  Dates.mssql2us(opp[g_opps.FirstMeeting]);
			//else
			//	lm = Dates.mssql2us(opp[g_opps.LastMoved]);
			//days = getDaysOld(lm);
			opp[g_opps.PrevDaysOld] = prevdays*1 + days*1;
		}
		if (personID!=-1)
		{
			opp[g_opps.PersonID]=personID;
			if (update_from_mass_op)	// called from admin
			{
				var person=find_person(personID);
				opp[g_opps.Color]=person[g_people.Color];
				opp[g_opps.Level]=person[g_people.Level];
				opp[g_opps.FirstName]=person[g_people.FirstName];
				opp[g_opps.LastName]=person[g_people.LastName];
			}
		}
		opp.edited = true;
		if (!update_from_mass_op)
			window.opener.update_from_edit_window(opp, oldCat, oldPersonID, 'skip_calcs');
	}
	if (!update_from_mass_op)
		window.opener.do_person_calcs(personID);
	if (update_from_mass_op) update_from_mass_op();
	if (window.opener.callbackMassMove) window.opener.callbackMassMove();
	if(ci_confirm('Would you like to save now?'))
		window.opener.do_save();
	window.close();
}

//This is a copy of a function in reassign.js
//Both should be deleted & moved to utils.js
function arrCopy(arr)
{
	var cpy=new Array();
	var str = arr.join('|');
	var cpy=str.split('|');
	return cpy;
}

var ssSalesPerson = null;

function initSPSelect(sp)
{
	var arrPeople = window.opener.getPeopleArray();
//Strike this guy from people array
	if ((typeof(sp) != 'undefined'))
	{

		for (var i=0; i<arrPeople.length; i++)
		{
			if(arrPeople[i][0] == sp)
			{
				arrPeople.splice(i, 1);
			}
		}
	}

	ssSalesPerson = new SmartSelect(document.getElementById('select_salesperson'), arrPeople, 0, 1/*, g_personID*/);
}

function positionSelects()
{
	if (ssSalesPerson)
		ssSalesPerson.positionControls();
}

window.onresize=positionSelects;
