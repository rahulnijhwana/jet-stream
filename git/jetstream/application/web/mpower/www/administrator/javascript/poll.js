function poll_changes(url, compare_date, company_id, person_id, section, callback)
{
	poll_changes.callback = callback;
	
	var polling_container = window.frames['polling_container'];
	//var polling_container = window.open('', 'debug');

	polling_container.document.writeln('<html><body>');
	polling_container.document.writeln('<form name="form1" method="post" action="' + url + '">');

	polling_container.document.writeln('<script language="JavaScript">');
	polling_container.document.writeln('<!--');
	polling_container.document.writeln('window.result = "waiting";');
	polling_container.document.writeln('// -->');
	polling_container.document.writeln('</script>');

	polling_container.document.writeln('<input type="text" name="mpower_section" value="' + section + '"><br>');
	polling_container.document.writeln('<input type="text" name="mpower_company_id" value="' + company_id + '"><br>');
	if (section == 'board')
		polling_container.document.writeln('<input type="text" name="mpower_person_id" value="' + person_id + '"><br>');
	polling_container.document.writeln('<input type="text" name="mpower_compare_date" value="' + compare_date + '"><br>');
	polling_container.document.writeln('<input type="text" name="mpower_random_id" value="' + Math.random() + '"><br>');

	polling_container.document.writeln('<input type="button" name="Save" value="Save" onclick="document.form1.submit()">');
	polling_container.document.writeln('</form>');

	polling_container.document.writeln('</body></html>');
	polling_container.document.close();

	poll_changes.poll_window = polling_container;
	poll_changes.timer = window.setInterval('_poll_interval()', 500);
	polling_container.document.forms['form1'].submit();
}

function _poll_interval()
{
	if (poll_changes.poll_window.result == 'waiting')
		return;
	window.clearInterval(poll_changes.timer);
	poll_changes.callback(poll_changes.poll_window.need_refresh);
}
