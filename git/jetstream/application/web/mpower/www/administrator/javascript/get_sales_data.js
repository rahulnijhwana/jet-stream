function get_salesperson_info(id)
{
	var ret = new Object();

	var monthsNew = g_company[0][g_company.MonthsConsideredNew];
	for (var i = 0; i < g_people.length; ++i)
	{
		if (g_people[i][g_people.PersonID] != id)
			continue;
		ret.rating = g_people[i][g_people.Rating];
		ret.color = g_people[i][g_people.Color];
		ret.name = g_people[i][g_people.FirstName] + ' ' + g_people[i][g_people.LastName];
		ret.rawdata = g_people[i];

		var expDate = new Date(Dates.mssql2us(g_people[i][g_people.StartDate]));
		for (; monthsNew>0; --monthsNew)
		{
			var m = expDate.getMonth();
			if (m == 11)
			{
				expDate.setMonth(0);
				expDate.setYear(expDate.getYear() + 1);
			}
			else expDate.setMonth(m + 1);
		}
		if (expDate.getTime() > new Date().getTime())
		{
			ret.tenure = 'New';
			if (g_user[0][g_user.Level] > 1)
				ret.name += '&nbsp;(n)';
		}
		else
			ret.tenure = 'Experienced';

		ret.analysisID = g_people[i][g_people.AnalysisID];
		ret.PersonID = g_people[i][g_people.PersonID];
		ret.CompanyID= g_people[i][g_people.CompanyID];
		break;
	}

	return ret;
}

function get_salesperson_curve(id)
{
	var ret = new Object();

	var person = find_person(id);
	if (!person) return ret;

	ret.fmValue = person[g_people.FMValue];
	ret.ipValue = person[g_people.IPValue];
	ret.dpValue = person[g_people.DPValue];
	ret.cValue = person[g_people.CloseValue];
	return ret;
}

function find_person(id)
{
	for (var i = 0; i < g_people.length; ++i)
		if (g_people[i][g_people.PersonID] == id) return g_people[i];

	return null;
}

function getValuationAvg(idPerson)
{
	var vCount=0;
	var vTotal=0;
	var vAvg=0;
	var i;
	for(i=0; i<g_opps.length; i++)
	{
        //Presumably, exclude removed and target opps from average		
		if (g_opps[i][g_opps.PersonID]!=idPerson) continue;
		if (g_opps[i][g_opps.Category]=='6')		
			continue;
		if(g_opps[i][g_opps.VLevel] < '1') continue;
		vCount++;
		vTotal+=Number(g_opps[i][g_opps.VLevel]);
	}

	if(vCount>0)
	{
		vAvg = vTotal/vCount;
	}	
	return vAvg;
}
