
Trends = new Object();

Trends.calc = _Trends_calc; // calcs and stores trend indications in g_people's TrendState
Trends.calcTotals = _Trends_calcTotals;
Trends.list = _Trends_list;
Trends.listAll = _Trends_listAll;
Trends.getThresholds = _Trends_getThresholds;

function TrendTotal(id, name, color)
{
	this.id = id;
	this.name = name;
	this.color = color;

	this.fm = 0;
	this.ip_live = 0;
	this.ip_stalled = 0;
	this.ip_total = 0;
	this.dp_live = 0;
	this.dp_stalled = 0;
	this.dp_total = 0;
	this.closed = 0;
	this.ip_live_person = 0;
	this.ip_live_money = 0;
	this.ip_live_need = 0;
	this.ip_live_time = 0;
	this.ip_live_req1 = 0;
	this.ip_live_req2 = 0;
}

function _Trends_calcTotals()
{
	var trendTotals = new Object();
	for (var i = 0; i < g_people.length; ++i)
	{
		var id = g_people[i][g_people.PersonID];
		var name = g_people[i][g_people.FirstName] + ' ' + g_people[i][g_people.LastName];
		var color = g_people[i][g_people.Color];
		trendTotals['id' + id] = new TrendTotal(id, name, color);
	}

	var tempcut = new Date();
	var cutoff = (tempcut.getTime() - (1000 * 60 * 60 * 24 * 30));

	for (var i = 0; i < g_opps.length; ++i)
	{
		var id = g_opps[i][g_opps.PersonID];
		var ttotal = trendTotals['id' + id];

		var cat = g_opps[i][g_opps.Category];
		if (cat == 1)
			ttotal.fm++;
		else if (cat == 2)
		{
			ttotal.ip_live++;
			ttotal.ip_total++;
			if (g_opps[i][g_opps.Person] == '1')
				ttotal.ip_live_person++;
			if (g_opps[i][g_opps.Need] == '1')
				ttotal.ip_live_need++;
			if (g_opps[i][g_opps.Money] == '1')
				ttotal.ip_live_money++;
			if (g_opps[i][g_opps.Time] == '1')
				ttotal.ip_live_time++;
			if (g_opps[i][g_opps.Requirement1] == '1')
				ttotal.ip_live_req1++;
			if (g_opps[i][g_opps.Requirement2] == '1')
				ttotal.ip_live_req2++;
		}
		else if (cat == 3)
		{
			ttotal.ip_stalled++;
			ttotal.ip_total++;
		}
		else if (cat == 4)
		{
			ttotal.dp_live++;
			ttotal.dp_total++;
		}
		else if (cat == 5)
		{
			ttotal.dp_stalled++;
			ttotal.dp_total++;
		}
		else if (cat == 6)
		{
			var oppDate = new Date(Dates.mssql2us(g_opps[i][g_opps.ActualCloseDate]));
			if (oppDate.getTime() >= cutoff)
				ttotal.closed++;
		}
	}

	return trendTotals;
}

function _Trends_list(id, trendsPerLine)
{
	var ret = '';
	var trendTotals = Trends.calcTotals();
	var headerShown = false;

	var ttotal = trendTotals['id' + id];
	if (!ttotal)
	{
		alert('Trends.list: cannot find trends for id ' + id);
		return; // should never happen
	}

	var trendsThisLine = 0;

	var thresholds = Trends.getThresholds(ttotal.id);
	var fm_threshold = thresholds.FMThreshold;
	var ip_live_threshold = thresholds.IPThreshold;
	var dp_live_threshold = thresholds.DPThreshold;
	var close_threshold = thresholds.CloseThreshold;

	var req1Used = (g_company[0][g_company.Requirement1used] == '1')
	var req2Used = (g_company[0][g_company.Requirement2used] == '1')
	/*
	str = 'ttotal.dp_live = ' + ttotal.dp_live + '\n';
	str += 'dp_live_threshold = ' + dp_live_threshold + '\n';
	str += 'ttotal.closed = ' + ttotal.closed + '\n';
	alert(str);
	*/
	var picSuffix='';
	if (!req1Used && !req2Used) picSuffix='_a';
	else if (req1Used && !req2Used) picSuffix='_b';
	else if (!req1Used && req2Used) picSuffix='_c';
	with (ttotal)
	{
		for (var i = 0; i < g_trends.length; ++i)
		{
			var TrendID=g_trends[i][g_trends.TrendID];
			if (TrendID == 13 && !req1Used) continue;
			if (TrendID == 14 && !req2Used) continue;

			var alg = g_trends[i][g_trends.Calc];
			var applies = eval(alg);

			if (!headerShown)
			{
				ret += '<p style="font-size:10pt; font-family:Arial;">The trends highlighted below apply to ' + name + ':</p>';
				ret += '<table cellspacing=5><tr valign="top">';
				headerShown = true;
			}

			if (trendsThisLine == trendsPerLine)
			{
				ret += '</tr><tr valign="top">';
				trendsThisLine = 1;
			}
			else trendsThisLine++;

			ret += '<td align="center" class="trend">';
			var thisSuffix='';
			if (i>=8) thisSuffix=picSuffix;
			
			if (applies)
			{
				ret_link = '<a href="javascript:open_trend(' + g_trends[i][g_trends.TrendID] + ', ' + id + ')" style="color:red;">';

				// ret += '<a href="javascript:open_trend(' + g_trends[i][g_trends.TrendID] + ', ' + id + ')" style="color:red;">';
//TrendIDs are no longer in numerical order, in order to sort userDef1 before rest of milestones
//				ret += '<span style="background-color: ' + color + '; height: 100px;"><img src="trendpics/trend' + (i + 1) + '_on'+thisSuffix+'.gif" style="border-color:red;" border="2"></span>';
				ret += '<div style="background-color: ' + color + '; width: 85px;">' + ret_link + '<img src="trendpics/trend' + TrendID + '_on'+thisSuffix+'.gif" style="border-color:red;" border="2"></a></div>';
			} //			else ret += '<span style="background-color: silver"><img src="trendpics/trend' + (i + 1) + '_off'+thisSuffix+'.gif" border=0></span>';
			else {
				ret += '<div style="background-color: silver; width: 85px;"><img src="trendpics/trend' + TrendID + '_off'+thisSuffix+'.gif" border=0></div>';
			}
			
			var trendName = makeTrendName(g_trends[i][g_trends.TrendID], g_trends[i][g_trends.Name])
/*			
			var trendName = g_trends[i][g_trends.Name];
			if (g_trends[i][g_trends.TrendID] == 13)
				trendName = trendName.replace(/Requirement1/ig,  g_company[0][g_company.Requirement1]);
			else if (g_trends[i][g_trends.TrendID] == 14)
				trendName = trendName.replace(/Requirement2/ig,  g_company[0][g_company.Requirement2]);
*/				

			if (applies) {
				ret += '<br>' + ret_link + trendName + '</a>';
			} else {
				ret += '<br>' + trendName;
			}
			
			ret += '</td>';
		}
	}

	if (headerShown) ret += '</table>';
	else ret += '<p>No trends apply to ' + name + ' at this time.</p>';

	return ret;
}

function makeTrendName(trendID, trendName)
{
//all this ugliness inservice of config milestones
	switch(trendID)
	{


		case 9:
		case '9':
 			if(0 == g_company[0][g_company.MS2Label].length) return trendName;
			trendName = trendName.replace(/Person/ig,  g_company[0][g_company.MS2Label]);
			break;
		case 10:
		case '10':
			if(0 == g_company[0][g_company.MS3Label].length) return trendName;
			trendName = trendName.replace(/Need/ig,  g_company[0][g_company.MS3Label]);
			break;
		case 11:
		case '11':
			if(0 == g_company[0][g_company.MS4Label].length) return trendName;
			trendName = trendName.replace(/Money/ig,  g_company[0][g_company.MS4Label]);		
			break;
		case 12:
		case '12':
			if(0 == g_company[0][g_company.MS5Label].length) return trendName;
			trendName = trendName.replace(/Time/ig,  g_company[0][g_company.MS5Label]);		
			break;

		case 13:
		case '13':
			trendName = trendName.replace(/Requirement1/ig,  g_company[0][g_company.Requirement1]);
			break;
		case 14:
		case '14':
			trendName = trendName.replace(/Requirement2/ig,  g_company[0][g_company.Requirement2]);
			break;
	}
	return trendName;
}

function _Trends_listAll(trendsPerLine)
{
	var ret = '';
	var trendTotals = Trends.calcTotals();
	var headerShown = false;

	var req1Used = (g_company[0][g_company.Requirement1used] == '1')
	var req2Used = (g_company[0][g_company.Requirement2used] == '1')
	var picSuffix='';
	if (!req1Used && !req2Used) picSuffix='_a';
	else if (req1Used && !req2Used) picSuffix='_b';
	else if (!req1Used && req2Used) picSuffix='_c';
	for (var k in trendTotals) // each k is a person
	{
		var ttotal = trendTotals[k];
		var nameShown = false;
		var trendsThisLine = 0;

		var thresholds = Trends.getThresholds(ttotal.id);
		var fm_threshold = thresholds.FMThreshold;
		var ip_live_threshold = thresholds.IPThreshold;
		var dp_live_threshold = thresholds.DPThreshold;
		var close_threshold = thresholds.CloseThreshold;

		with (ttotal)
		{
			for (var i = 0; i < g_trends.length; ++i)
			{
				if (g_trends[i][g_trends.TrendID] == 13 && !req1Used) continue;
				if (g_trends[i][g_trends.TrendID] == 14 && !req2Used) continue;
				
				var TrendID=g_trends[i][g_trends.TrendID];
				var alg = g_trends[i][g_trends.Calc];
				if (eval(alg) == false) continue;

				if (!headerShown)
				{
					ret += '<p>The following trends were detected:</p>';
					headerShown = true;
				}

				if (!nameShown)
				{
					ret += '<p><b>' + ttotal.name + '</b></p><table cellspacing=5><tr valign="top">';
					nameShown = true;
				}

				if (trendsThisLine == trendsPerLine)
				{
					ret += '</tr><tr valign="top">';
					trendsThisLine = 0;
				}
				else trendsThisLine++;

				trendName = makeTrendName(g_trends[i][g_trends.TrendID], g_trends[i][g_trends.Name]);
/*
				var trendName = g_trends[i][g_trends.Name];
				if (g_trends[i][g_trends.TrendID] == 13)
					trendName = trendName.replace(/Requirement1/ig,  g_company[0][g_company.Requirement1]);
				else if (g_trends[i][g_trends.TrendID] == 14)
					trendName = trendName.replace(/Requirement2/ig,  g_company[0][g_company.Requirement2]);
*/
				var thisSuffix='';
				if (i>=8) thisSuffix=picSuffix;
				ret += '<td align="center" class="trend">';
				ret += '<a href="javascript:open_trend(' + g_trends[i][g_trends.TrendID] + ', ' + id + ')" style="color:red;">';
//				ret += '<span style="background-color: ' + color + ';"><img src="trendpics/trend' + (i + 1) + '_on'+thisSuffix+'.gif" style="border-color:red;" border="2"></span>';
				ret += '<div style="background-color: ' + color + '; width: 85px;"><img src="trendpics/trend' + TrendID + '_on'+thisSuffix+'.gif" style="border-color:red;" border="2"></div>';
				ret += '<br>' + trendName + '</a></td>';
			}
		}

		if (nameShown) ret += '</table>';
	}

	if (!headerShown) ret += '<p>No trends were detected at this time.</p>';

	return ret;
}

function _Trends_calc()
{
	var req1Used = (g_company[0][g_company.Requirement1used] == '1');
	var req2Used = (g_company[0][g_company.Requirement2used] == '1');

	var trendTotals = Trends.calcTotals();
	for (var k in trendTotals) // each k is a person
	{
		var ttotal = trendTotals[k];
		var nameShown = false;
		var trendsThisLine = 0;

		var thresholds = Trends.getThresholds(ttotal.id);
		var fm_threshold = thresholds.FMThreshold;
		var ip_live_threshold = thresholds.IPThreshold;
		var dp_live_threshold = thresholds.DPThreshold;
		var close_threshold = thresholds.CloseThreshold;

		var newTrendState = 0;
		with (ttotal)
		{
			for (var i = g_trends.length - 1; i >= 0; --i)
			{
				newTrendState = newTrendState << 1;
				if (g_trends[i][g_trends.TrendID] == 13 && !req1Used) continue;
				if (g_trends[i][g_trends.TrendID] == 14 && !req2Used) continue;
				var alg = g_trends[i][g_trends.Calc];
				if (eval(alg)) newTrendState |= 1;
			}
		}

		var person = find_person(ttotal.id);
		if (person == null) continue;

		var oldTrendState = person[g_people.TrendState];
		if (oldTrendState == newTrendState) continue;

		person[g_people.TrendState] = newTrendState;
		person.isDirty = true;
	}
}

function _Trends_getThresholds(personID)
{
	var person = null;
	for (var i = 0; i < g_people.length; ++i)
	{
		if (g_people[i][g_people.PersonID] != personID) continue;
		person = g_people[i];
		break;
	}

	if (!person) return null;

	var ret = new Object();
	var fields = new Array('FMThreshold', 'IPThreshold', 'DPThreshold', 'CloseThreshold');
	for (var i = 0; i < fields.length; ++i)
	{
		var fieldName = fields[i];
		var fieldNum = g_people[fieldName];
		ret[fieldName] = person[fieldNum];
	}

	return ret;
}
