Calendar = new Object();

// ----------------------------- "public" methods

Calendar.makeRoster = _Calendar_makeRoster;
Calendar.makeCalendar = _Calendar_makeCalendar;
Calendar.makeCalendarHeader = _Calendar_makeCalendarHeader;
Calendar.getMeetingsArray = _Calendar_getMeetingsArray;
Calendar.logMeeting = _Calendar_logMeeting;
Calendar.paintDayBoxTops = _Calendar_paintDayBoxTops;
Calendar.paintDayBoxBottoms = _Calendar_paintDayBoxBottoms;

Calendar.scrollWeekUp = _Calendar_scrollWeekUp;
Calendar.scrollWeekDown = _Calendar_scrollWeekDown;

Calendar.incMonth = _Calendar_incMonth;
Calendar.scrollMonthUp = _Calendar_scrollMonthUp;
Calendar.scrollMonthDown = _Calendar_scrollMonthDown;

Calendar.scrollMonYearUp = _Calendar_scrollMonYearUp;
Calendar.scrollMonYearDown = _Calendar_scrollMonYearDown;


Calendar.incYear = _Calendar_incYear;
Calendar.scrollYearUp = _Calendar_scrollYearUp;
Calendar.scrollYearDown = _Calendar_scrollYearDown;

Calendar.getCalendarDate = _Calendar_getCalendarDate;
Calendar.getWeekExtent = _Calendar_getWeekExtent;

Calendar.doMonthView =_Calendar_doMonthView;
Calendar.doWeekView =_Calendar_doWeekView;
Calendar.paintRow =_Calendar_paintRow;

Calendar.onChangeYear =_Calendar_onChangeYear;
Calendar.onChangeMonth =_Calendar_onChangeMonth;

Calendar.onChangeMonYear =_Calendar_onChangeMonYear;
Calendar.onChangeWeeks =_Calendar_onChangeWeeks;

Calendar.setSalesChecks =_Calendar_setSalesChecks;
Calendar.chkPerson =_Calendar_chkPerson;
Calendar.doDayView =_Calendar_doDayView;

Calendar.showPerson =_Calendar_showPerson;
Calendar.printCalendar =_Calendar_printCalendar;

// ----------------------------- "public" data

Calendar.enable = true;

// ----------------------------- "private" data
Calendar.getRowInfo = _Calendar_getRowInfo;
Calendar.showSat = false;
Calendar.showSun = false;
Calendar.cols = 5;
Calendar.Type = 'month';  //can also be 'week' or 'day';

Calendar.m_Roster = new Array();

var g_weekStart; //day padding for first of month
var isNav = (navigator.appName.indexOf("Netscape") != -1) ? true : false;

window.print = Calendar.printCalendar;

var ssYear = null;
var ssMonth = null;
var ssWeek = null;
var arrWeeks;

var arrMonthNames = new Array('January','February','March','April','May','June','July','August','September','October','November','December');


function _Calendar_printCalendar()
{
	var salesids='';
	for (var i=0; i<Calendar.m_Roster.length; i++)
	{
		if(document.getElementById(Calendar.m_Roster[i]['chkID']).checked == true)
		{
			if(salesids.length) salesids += ',';
			salesids += Calendar.m_Roster[i]['PersonID'];
		}
	}
	var strParams = '?caltype=' + Calendar.Type + '&month=' + 1*(Calendar.m_Month+1);
	strParams += '&week=' + Calendar.Week;
	strParams += '&year=' + Calendar.m_Year + '&salesidlist=' + salesids;
	Windowing.openSizedWindow('../reports/calendar_pdf.php' + strParams, 800, 790, 'print_calendar');
	document.body.style.cursor = 'auto';
}


function _Calendar_getCalendarDate(week, day)
{
	var dayOfMonth = (week * 7)  + day + g_weekStart;
	var calDate = new Date(Calendar.m_Year, Calendar.m_Month, dayOfMonth);
    //Display 2-digit year
	var Yr = String(calDate.getFullYear());
	if (navigator.appName.indexOf("Netscape") != -1)
		Yr = Yr.substr(1,2);
	else
		Yr = Yr.substr(2,2);

	var strDate = calDate.getMonth()+1 + '/' + calDate.getDate() + '/' + Yr;
	return strDate;
}

function _Calendar_getWeekExtent(singleWeek)
{
	this.cols = 5;
	this.showSat = false;
	this.showSun = false;
	if(singleWeek > -1)
	{
		if (typeof(this.m_arrMeetings[singleWeek*7]) != 'undefined')
			this.showSun = true;
		if (typeof(this.m_arrMeetings[singleWeek*7 + 6]) != 'undefined')
			this.showSat = true;
	}
	else
	{
		for (var week = 0; week < this.m_weekRows.length; week++)
		{
			if (typeof(this.m_arrMeetings[week*7]) != 'undefined')
				this.showSun = true;
			if (typeof(this.m_arrMeetings[week*7 + 6]) != 'undefined')
				this.showSat = true;
			if (this.showSat == true && this.showSun == true)
				break;
		}
	}
	if (this.showSat == true) this.cols++;
	if (this.showSun == true) this.cols++;
}

function _Calendar_logMeeting(msStart, recMeeting)
{
	var msMeeting =  Date.parse(recMeeting['dateMeeting']);
	msMeeting=Math.floor(msMeeting/86400000);
	var dayInPeriod = msMeeting - msStart;
	if(dayInPeriod > 34)
		alert('Over 35 days in period!');
	if(typeof(this.m_arrMeetings[dayInPeriod]) == 'undefined')
	{
		this.m_arrMeetings[dayInPeriod] = new Array();
	}
	this.m_arrMeetings[dayInPeriod].push(recMeeting);
	var wk = Math.floor(dayInPeriod / 7);
	if(typeof(this.m_weekRows[wk]) == 'undefined')
		this.m_weekRows[wk] = this.m_arrMeetings[dayInPeriod].length;
	else if (this.m_arrMeetings[dayInPeriod].length > this.m_weekRows[wk])
	{
		this.m_weekRows[wk] = this.m_arrMeetings[dayInPeriod].length;
	}

}

function catAbbr(cat)
{

	switch(cat)
	{
		case '1':
			return catLabel('FM');
		case '2':
			return catLabel('IP');
		case '3':
			return catLabel('SIP');
		case '4':
			return catLabel('DP');
		case '5':
			return catLabel('SDP');
		case '6':
			return catLabel('C');
		case '10':
			return catLabel('T');
	}

}

function getMeetingMS(sD, sT)
{
	var D = new Date(sD);
	return Date.parse(D.getMonth() + '/' + D.getDate() + '/' + D.getFullYear() + ' ' + sT);
}



function _Calendar_getMeetingsArray(msStart, msEnd)
{
	this.m_arrMeetings = new Array();
	this.m_weekRows = new Array(this.m_weeks * 1);
	var now = new Date();
	now.setHours(0);
	now.setMinutes(0);
	now.setSeconds(0);
	now.setMilliseconds(0);

	var msNow = now.getTime();
	for (var i=0; i < g_opps.length; i++)
	{
		if(g_opps[i][g_opps.Category] == '9' || g_opps[i][g_opps.Category] == '10' || g_opps[i][g_opps.Category] == '3' || g_opps[i][g_opps.Category] == '5')
			continue;
		var dateFM = Dates.mssql2us(g_opps[i][g_opps.FirstMeeting]);
		var dateNM = Dates.mssql2us(g_opps[i][g_opps.NextMeeting]);

        //Show meetings only today and in the future
		var msFM = Date.parse(dateFM);
		var msNM = Date.parse(dateNM);

		for (var j=0; j<g_people.length; j++)
		{
			if(document.getElementById(Calendar.m_Roster[j]['chkID']).checked == false)
				continue;
			if (g_people[j][g_people.PersonID] != g_opps[i][g_opps.PersonID])
				continue;

			if (msFM && msFM - msNow >= 0 && msFM - msEnd <= 0)
			{
				var tmp = new Array();
				tmp['PersonIndex'] = j;
				tmp['PersonID'] = g_people[j][g_people.PersonID];
				tmp['Color'] = g_people[j][g_people.Color];
				tmp['dateMeeting'] = dateFM;
				tmp['timeMeeting'] = g_opps[i][g_opps.FirstMeetingTime];
				tmp['timeSort'] = getMeetingMS(dateFM,g_opps[i][g_opps.FirstMeetingTime]);
				tmp['comp'] = g_opps[i][g_opps.Company];
				if (g_opps[i][g_opps.Division] != '')
					tmp['comp'] += ' - '+g_opps[i][g_opps.Division];
				tmp['cat'] = catAbbr(g_opps[i][g_opps.Category]);
				if(g_opps[i][g_opps.VLevel] > 0)
					tmp['vlevel'] = 'V' + g_opps[i][g_opps.VLevel];
				else
					tmp['vlevel'] = '';

				this.logMeeting(msStart, tmp);
			}

			if (msNM && msNM -  msNow >= 0 && msNM -  msEnd <= 0)
			{
				var tmp = new Array();
				tmp['PersonIndex'] = j;
				tmp['PersonID'] = g_people[j][g_people.PersonID];
				tmp['Color'] = g_people[j][g_people.Color];
				tmp['dateMeeting'] = dateNM;
				tmp['timeMeeting'] = g_opps[i][g_opps.NextMeetingTime];
				tmp['timeSort'] = getMeetingMS(dateNM, g_opps[i][g_opps.NextMeetingTime]);
				tmp['comp'] = g_opps[i][g_opps.Company];
				if (g_opps[i][g_opps.Division] != '')
					tmp['comp'] += ' - '+g_opps[i][g_opps.Division];
				tmp['cat'] = catAbbr(g_opps[i][g_opps.Category]);
				if(g_opps[i][g_opps.VLevel] > 0)
					tmp['vlevel'] = 'V' + g_opps[i][g_opps.VLevel];
				else
					tmp['vlevel'] = '';
				this.logMeeting(msStart, tmp);
			}
			break;

		}
	}

	for (var i=0; i<this.m_arrMeetings.length; i++)
	{
		if(typeof(this.m_arrMeetings[i]) == 'undefined')
			continue;
		this.m_arrMeetings[i].sort(sortMeetingsDay);
	}

}

function sortMeetingsDay(a, b)
{
	var ac = a['timeSort'];
	var bc = b['timeSort'];
	if (isNaN(ac))
		ac = 0;
	if (isNaN(bc))
		bc = 0;
	if(ac != bc)
		return ac - bc;
	return ac - bc;
}

function onclickGoTo()
{
	alert('Go to');
}

function onclickToday()
{
	alert('Today');
}

function makeWeeksArray()
{
	arrWeeks = Array();
	for (var i=0; i < Calendar.m_weekRows.length; i++)
	{
		var wkLabel = 'Week ' + (i*1 + 1);
		arrWeeks.push(wkLabel);
	}
}

function _Calendar_scrollWeekUp()
{
	if(this.m_Week  == this.m_weekRows.length - 1)
	{
		this.incMonth(1);
		this.m_Week = 0;
	}
	else
		this.m_Week++;
}

function _Calendar_scrollWeekDown()
{
	if(this.m_Week  == 0) {
		this.incMonth(-1);
		this.m_Week = this.weekRows.length - 1;
	}
	else {
		this.m_Week++;
	}
}

function _Calendar_incMonth(inc) {
	if(inc == 1)  //scroll up
	{
		if(Calendar.m_Month == 11)
		{
			Calendar.m_Month = 0;
			Calendar.m_Year++;
			document.getElementById('txtYear').value = Calendar.m_Year;
		}
		else
			Calendar.m_Month++;

	}
	else
	{
		if(Calendar.m_Month == 0)
		{
			Calendar.m_Month = 11;
			Calendar.m_Year--;
			document.getElementById('txtYear').value = Calendar.m_Year;
		}
		else
			Calendar.m_Month--;
	}
}

function _Calendar_scrollMonthUp() {
	this.incMonth(1);
	ssMonth.setVal(Calendar.m_Month);
	this.m_rowInfo = null;
	Calendar.makeCalendar();

}

function _Calendar_scrollMonthDown() {
	this.incMonth(-1);
	ssMonth.setVal(Calendar.m_Month);
	this.m_rowInfo = null;
	Calendar.makeCalendar();
}

function _Calendar_scrollMonYearUp() {
    var mon_year = document.getElementById('txtMonYear');
    if (mon_year.selectedIndex < mon_year.length - 1) {
        mon_year.selectedIndex++;
    }
    Calendar.onChangeMonYear();
}

function _Calendar_scrollMonYearDown() {
    var mon_year = document.getElementById('txtMonYear');
    if (mon_year.selectedIndex > 0) {
        mon_year.selectedIndex--;
    }
    Calendar.onChangeMonYear();
}


function _Calendar_incYear(inc) {
	Calendar.m_Year = 1*(Calendar.m_Year + inc);
	document.getElementById('txtYear').value = Calendar.m_Year;
}

function _Calendar_scrollYearUp() {
	this.incYear(1);
	this.m_rowInfo = null;
	Calendar.makeCalendar();
}

function _Calendar_scrollYearDown() {
	this.incYear(-1);
	this.m_rowInfo = null;
	Calendar.makeCalendar();
}

//Sort on ordinal value for month rather than month name
function monthSort(a, b)
{
	return a[1] - b[1];
}

function onYearKeyUp()
{
	ssYear.autofill(window.event);
}

function onMonthKeyUp()
{
	ssMonth.autofill(window.event);
}

function initSS()
{
	var now = new Date();
	if(typeof(Calendar.m_Month) == 'undefined')
		Calendar.m_Month = now.getMonth();

	if(typeof(Calendar.m_Year) == 'undefined')
		Calendar.m_Year = now.getFullYear();

    // Populate the dropdown with the next 12 months
    var this_month = now.getMonth();
    var this_year = now.getFullYear();
    var mon_year = document.getElementById('txtMonYear');
    for (var x = 0; x < 12; x++) {
        var month = this_month + x;
        var year = this_year;
        if (month > 11) {
            year++;
            month -= 12;
        }
        var date_text = arrMonthNames[month] + " " + year;
        var optn = document.createElement("OPTION");
        optn.text = date_text;
        optn.value = Date.parse((month + 1) + "/1/" + year);
        mon_year.options.add(optn);
    }

    
    if (0) {
        var weekly = document.getElementById('txtWeeks');
        var start_date = new Date();
        with (start_date) setDate(getDate() - getDay() + 1); 
    
        for (var x = 0; x < 16; x++) {
            with (start_date) var date_text = (getMonth() + 1) + "/" + getDate() + "/" + getYear();
            with (start_date) var this_span_text = date_text;
            with (start_date) setDate(getDate() + 32);
            with (start_date) this_span_text += " - " + (getMonth() + 1) + "/" + getDate() + "/" + getYear();
            with (start_date) setDate(getDate() - 25);
    
            var optn = document.createElement("OPTION");
            optn.text = this_span_text;
            optn.value = date_text;
            weekly.options.add(optn);
        }
    }
/*
    for (var x = 0; x < 52; x++) {
        var month = this_month + x;
        var year = this_year;
        if (month > 11) {
            year++;
            month -= 12;
        }
        var date_text = arrMonthNames[month] + " " + year;
        var optn = document.createElement("OPTION");
        optn.text = date_text;
        optn.value = Date.parse((month + 1) + "/1/" + year);
        mon_year.options.add(optn);
    }
*/


    if (0) {
        // Need to initialize these each time we redraw the roster
    	ssYear = null;
    	ssMonth = null;
    	var arrYears = new Array();
        // Init years to current year -2 to +1
    	var now = new Date();
    	var yr = now.getFullYear();
    	var start = yr - 2;
    	var end = (yr + 1) * 1;
    	for (var i = start; i <= end; i++)
    	{
    		var tmp = new Array();
    		tmp[0] = i;
    		tmp[1] = i;
    		arrYears.push(tmp);
    	}
    	//ssYear = new SmartSelect(document.getElementById('txtYear'), arrYears, 0, 1);
    	ssYear.setVal(Calendar.m_Year);
    
    	var arrMonths = new Array();
    	for (var i=0; i < arrMonthNames.length; i++)
    	{
    		var tmp = new Array();
    		tmp[0] = arrMonthNames[i];
    		tmp[1] = i;
    		arrMonths.push(tmp);
    	}
    	//ssMonth = new SmartSelect(document.getElementById('txtMonth'), arrMonths, 1, 0);
    	ssMonth.fnSort(monthSort);
    	ssMonth.setVal(Calendar.m_Month);
    	if(Calendar.Type == 'week')
    	{
    		var arrWeeks = new Array();
    		for (var i = 0; i < 5; i++)
    		{
    			var lbl = (i+1)*1;
    			var tmp = new Array();
    			tmp[0] = 'Week ' + lbl;
    			tmp[1] = i;
    			arrWeeks.push(tmp);
    		}
    		ssWeek = new SmartSelect(document.getElementById('txtWeek'), arrWeeks, 1, 0);
    	}
    }
}

function _Calendar_doMonthView()
{
	Calendar.Type = 'month';
	Calendar.m_rowInfo = null;
	Calendar.makeCalendar();
}

function _Calendar_doWeekView(week)
{
	Calendar.Week = week;
	Calendar.Type = 'week';
	Calendar.m_rowInfo = null;
	Calendar.makeCalendar();
}

function _Calendar_onChangeYear()
{
	Calendar.m_Year = ssYear.getSelectedVal();
	Calendar.m_Month = ssMonth.getSelectedVal();
	Calendar.m_rowInfo = null;
	Calendar.makeCalendar();
}

function _Calendar_onChangeMonth()
{
	Calendar.m_Year = ssYear.getSelectedVal();
	Calendar.m_Month = ssMonth.getSelectedVal();
	Calendar.m_rowInfo = null;
	Calendar.makeCalendar();
}

function _Calendar_onChangeMonYear()
{
    var mon_year = new Date(parseInt(document.getElementById('txtMonYear').value));
	Calendar.m_Year = mon_year.getYear();
	Calendar.m_Month = mon_year.getMonth();
	Calendar.m_rowInfo = null;
	Calendar.makeCalendar();
}

function _Calendar_onChangeWeeks()
{
    alert(document.getElementById('txtWeeks').value);
//    var mon_year = new Date(parseInt(document.getElementById('txtWeeks').value));
//	Calendar.m_Year = mon_year.getYear();
//	Calendar.m_Month = mon_year.getMonth();
//	Calendar.m_rowInfo = null;
//	Calendar.makeCalendar();
}


function _Calendar_setSalesChecks()
{
	var checked = document.getElementById('chkAll').checked;
	for (i=0; i < Calendar.m_Roster.length; i++)
	{
		var chk = document.getElementById(Calendar.m_Roster[i]['chkID']);
		chk.checked = checked;
	}
	if(checked == true)
	{
		Calendar.m_rowInfo = null;
		Calendar.makeCalendar();
	}
}

function paintButtonRow(calendarType, bCheckAll)
{
	var btnWidth = '20';

	var ret = '<table width="100%" border=0 cellspacing=0 cellpadding=0 ><tr>';
	ret+='<td><img src="../images/grid-head-left.jpg" ></td>';

	if(getUserLevel() > 1)
	{
		ret += '<td name="chkAll" width="33%" align="center"  valign="middle" height=25 class="grid_header_new"><input type="checkbox" ';
		if (bCheckAll) ret += 'checked=true '
		ret += 'id="chkAll" onclick="Calendar.setSalesChecks()">All Salespeople</td>';
	}
	else
	{
		var singleName = '';
		for(var i=0; i< Calendar.m_Roster.length; i++)
		{
			if(Calendar.m_Roster[i]['checked'] == true)
			{
				singleName = Calendar.m_Roster[i]['Name'];
				break;
			}
		}
		ret += '<td width="33%" align="center"  valign="middle" height=25 class="grid_header_new">' + singleName + '</td>';

	}


	ret += '<td width="33%" align="center"  valign="middle" style="height:25;" class="grid_header_new">';
	ret += '<input style="height:25" width="15" type="button" onclick="Calendar.scrollMonYearDown();" value="<">&nbsp;';
    ret += '<select id="txtMonYear" name="test" onchange="Calendar.onChangeMonYear();"  style="height:25; width:180; text-align:center;" ></select>';
	ret += '&nbsp;<input style="height:25" width="15" type="button" onclick="Calendar.scrollMonYearUp();" value=">">';
	ret += '</td>';

    ret += '<td width="33%" class="grid_header_new">&nbsp;</td>';
/*
	ret += '<td width="33%" align="center"  valign="middle" style="height:25;" class="grid_header_new">';
    ret += '<select id="txtWeeks" name="test" onchange="Calendar.onChangeWeeks();"   style="height:25; width:180; text-align:center;" >';
    ret += '</select>';
	ret += '</td>';
*/

/*
	ret += '<td width="33%" align="center"  valign="middle" style="height:25" class="grid_header_new">';
		ret += '<input style="height:25" width="15" type="button" onclick="Calendar.scrollMonYearDown();" value="<">&nbsp';
		ret += '<input id="txtMonYear" style="height:25;text-align:center"  width="180" type="text" onchange="Calendar.onChangeMonYear()" onKeyUp="onMonYearKeyUp()">&nbsp';
		ret += '<input width="15" style="height:25" type="button" onclick="Calendar.scrollMonYearUp();" value=">">';
	ret += '</td>';

	ret += '<td width="33%" align="center"  valign="middle" style="height:25" class="grid_header_new">';
		ret += '<input style="height:25" width="15" type="button" onclick="Calendar.scrollMonthDown();" value="<">&nbsp';
		ret += '<input id="txtMonth" style="height:25;text-align:center"  width="180" type="text" onchange="Calendar.onChangeMonth()" onKeyUp="onMonthKeyUp()">&nbsp';
		ret += '<input width="15" style="height:25" type="button" onclick="Calendar.scrollMonthUp();" value=">">';
	ret += '</td>';

	ret += '<td width="33%" align="center"  valign="middle" height=25 class="grid_header_new">';
		ret += '<input width="15"  style="height:25" type="button" onclick="Calendar.scrollYearDown();" value="<">&nbsp';
		ret += '<input id="txtYear" width="40"  style="height:25;text-align:center" type="text" onchange="Calendar.onChangeYear()" onKeyUp="onYearKeyUp()">&nbsp';
		ret += '<input width="15"  style="height:25" type="button" onclick="Calendar.scrollYearUp();" value=">">';
	ret += '</td>';
*/

	var periodLabel = (calendarType == 'month') ? 'View by Week' : 'View by Month';
	ret+='<td><img src="../images/grid-head-right.jpg" ></td>';

	ret += '</tr></table>';

	return ret;

}

function singleSP()
{

	var numChecked = 0;
	for(var i=0; i<Calendar.m_Roster.length; i++)
	{
		if(document.getElementById(Calendar.m_Roster[i]['chkID']).checked)
			numChecked++;
		if(numChecked > 1) return false;
	}

	return (numChecked == 1);
}


function _Calendar_paintRow(week, width, minHeight)
{
	var ret='';
	var oneChecked = singleSP();
	var btnText = (Calendar.Type == 'week') ? 'Month' : 'Week ' + (week+1);
	var btnAction = (Calendar.Type == 'week') ? 'Calendar.doMonthView()' : 'Calendar.doWeekView(' + week + ')';
	var indWeek = Calendar.Type == 'week' ? 0 : week;
	var rwSpan = 1*(Calendar.m_weekRows[indWeek] + 4);

	ret += this.paintDayBoxTops(width, week);
	for (var i = 0; i < Calendar.m_weekRows[indWeek]; i++)
	{
		temp = '<tr height="' + Calendar.m_cellHeight + '">'; // + lCell;



		for (var j = 0; j < 7; j++)
		{
			if (j == 0 && this.showSun == false) continue;
			if (j == 6 && this.showSat == false) continue;

//			var day = (week * 7) + j;
			var day = (indWeek * 7) + j;
			{
				var mtg='&nbsp';
				var mtgTime = '';
				var catval = '';
				var strstyle='';
				var rec = null;
				if(typeof(Calendar.m_arrMeetings[day]) != 'undefined' && i < Calendar.m_arrMeetings[day].length)
				{
					rec = this.m_arrMeetings[day][i];
					mtg = rec['comp'];
					mtgTime = rec['timeMeeting'];
					catval = '(' + rec['cat'];
					if(rec['vlevel'].length > 0)
						catval += ',' + rec['vlevel'];
					catval += ')';
					strstyle = 'style="color:black;"';

				}
				if (rec)
				{
					var dot='BlackDot';
					// determine if there should be a red dot
					for (var k=0;k<this.m_arrMeetings[day].length;k++)
					{
						if (k==i) continue;	// don't match self
						var test=this.m_arrMeetings[day][k];
						if (rec['PersonID']==test['PersonID'] && rec['timeMeeting']==test['timeMeeting'])
						{
							dot='RedDot';
							break;
						}
					}

					temp += '<td class="meeting_cell" ' + strstyle + ' valign="top">';
					temp += '<table width="100%" cellpadding="0" cellspacing="0"><tr>';

					var c = (dot == 'RedDot') ? 'red' : 'black';
					var mtgsep = '';
					if (i > 0)
						mtgsep = '<span style="font-size:6px;"><br></span>';
					if(!oneChecked)
					{
						if(isNav)
							temp += '<td width="20px" style="background-color:' + c + '; color:white;">'+mtgsep+'<span ><center style="font-weight:bold;font-size:10pt;">'+(rec.PersonIndex+1)+'</center></td>';
						else
							temp += '<td width="1" valign="top">'+mtgsep+'<span style="background-image:url(../images/'+dot+'.gif); background-repeat:no-repeat; color:white; width:20px; height:20px;" width="20" height="20"><center style="position:relative; top:1px; font-weight:bold; font-size:10pt;">'+(rec.PersonIndex+1)+'</center></span>';
					}
					else if (dot == 'RedDot')
					{
						if(isNav)
							temp += '<td width="20px" style="background-color:' + c + '; color:white;">'+mtgsep+'<span ><center style="font-weight:bold;font-size:10pt;"></center></td>';
						else
							temp += '<td width="1" valign="top">'+mtgsep+'<span style="background-image:url(../images/'+dot+'.gif); background-repeat:no-repeat; color:white; width:20px; height:20px;" width="20" height="20"><center style="position:relative; top:1px; font-weight:bold; font-size:10pt;"></center></span>';
					}


					temp += '</td><td align="center" class="meeting_cell" valign="bottom">';
					if (oneChecked && dot != 'RedDot')
						temp += mtgsep;
					temp += mtgTime;
					temp += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr></table>';
					temp += '<center>' + mtg + '<nobr>' + catval + '</nobr></center>';
					temp += '</td>';
				}
				else
					temp += '<td class="meeting_cell" ' + strstyle + '>'+ mtgTime + '<br>' + mtg + '<br>' + catval + '</td>';
			}
		}
		temp += '<td width="1"><img src="../images/spacer.gif" height="'+minHeight+'" width=1></td></tr>';
		ret+=temp;
	}
	ret+= Calendar.paintDayBoxBottoms();
	return ret;
}

function vt(al)
{
	var v = '';
	for(var i = 0; i< al.length; i++)
	{
		if(i)
			v += '<br>';
		v += al.charAt(i);
	}
	return v;
}

function _Calendar_paintDayBoxTops(width, week)
{
	var cellPix = Math.floor(width/this.cols);
	var cellRound=width-7*cellPix;

	var tlCell = '<td width="8" height=10><img src="../images/dgreybox_tl.gif" height="10" width=8></td>';
	var trCell = '<td width="8" height=10><img src="../images/dgreybox_tr.gif" height="10" width=11></td>';

	var ret = '<tr>';

	var btnText = (Calendar.Type == 'week') ? 'Month' : 'Week ' + (week+1);
	var btnAction = (Calendar.Type == 'week') ? 'Calendar.doMonthView()' : 'Calendar.doWeekView(' + week + ')';
	var indWeek = Calendar.Type == 'week' ? 0 : week;
	var rwSpan = 1*(Calendar.m_weekRows[indWeek] + 3);

	if(isNav)
	{
		ret += '<td style="background-color:#808080" rowspan="' + rwSpan + '"><button type="button" style="font-size:6pt" onclick="' + btnAction + '">';
		ret += vt(btnText);
		ret += '</button>';
	}
	else
	{
		ret += '<td style="background-color:#808080" rowspan="' + rwSpan + '"><button type="button" style="writing-mode:tb-rl" onclick="' + btnAction + '" >';
		ret += btnText;
		ret +='</button></td>';
	}

	for(var celli=0;celli < this.cols; celli++)
	{
		ret+=tlCell;
		var cellp;
		if (celli<cellRound) cellp=cellPix+1;
		else cellp=cellPix;
		ret+='<td height=10><img src="../images/dgreybox_t.jpg" height="10" width=' + cellp+'></td>';
		ret +=trCell;
	}

	ret+='<td width=1><img src="../images/spacer.gif" height=1 width=1></td></tr>';

	ret += '<tr>';

	for (var i = Dates.m_weekBeginsWith; true; )  //sunday=0; monday=1
	{
		var bSkip = false;
		if (i == 0) bSkip = (this.showSun == false);
		else if (i == 6) bSkip = (this.showSat == false);
		if(!bSkip)
		{
			var dayBoxHeight = Calendar.Type == 'week' ? Calendar.m_weekRows[0] + 1 : Calendar.m_weekRows[week] + 1;
			var strHead = Calendar.getCalendarDate(week, i + 1);
			ret += '<td width="8" rowspan='+(dayBoxHeight)+'><img src="../images/dgreybox_l.gif" height="100%" width=8 ></td>';
			var mtgDay = (Calendar.Type == 'month') ? week*7 + i : i;
			ret += Calendar.makeCalendarHeader(strHead, mtgDay);

			ret += '<td width="11"  rowspan='+(dayBoxHeight)+'><img src="../images/dgreybox_r.gif" height="100%" width=11></td>';
		}
		if (i == Dates.m_weekDays.length - 1) i = 0;
		else ++i;
		if (i == Dates.m_weekBeginsWith) break;
	}

	ret += '<td width="1"></td></tr>';
	return ret;
}

function _Calendar_paintDayBoxBottoms()
{
	var blCell = '<td width=8 height=11><img src="../images/dgreybox_bl.gif" height=11 width=8></td>';
	var brCell = '<td width=11 height=11><img src="../images/dgreybox_br.gif" height=11 width=11></td>';
	var bCell = '<td height=11 ><img src="../images/dgreybox_b.gif" height="11" width="100%"></td>';

	var ret = '<tr>'
	for(var i=0; i<this.cols; i++)
	{
		ret += blCell+bCell+brCell;
	}
	ret += '<td width="1"></td></tr>';
	return ret;

}

function getMonthLen(year, month)
{
	var len = Dates.m_monthDays[month];
	if (Dates.isLeapYear(year) && month == 1) ++len; // adjust for leap years
	return len;
}

function peekMeetingsOnDate(mtgDate)
{
	var dateParam = Dates.mssql2us(mtgDate);
	for(var i=0; i<g_opps.length; i++)
	{
		var cat= g_opps[i][g_opps.Category];
		if (cat == 10 || cat == 9) continue;
		var dateFM = Dates.mssql2us(g_opps[i][g_opps.FirstMeeting]);
		var dateNM = Dates.mssql2us(g_opps[i][g_opps.NextMeeting]);
		if(dateFM != dateParam && dateNM != dateParam)
			continue;
		var pid =  g_opps[i][g_opps.PersonID];
//		for (var j=0; j< g_people.length; j++)
		for (var j=0; j< Calendar.m_Roster.length; j++)
		{
			if(!document.getElementById(Calendar.m_Roster[j]['chkID']).checked)
				continue;
			if(Calendar.m_Roster[j]['PersonID'] != pid)
				continue;
			return true;
		}
	}
	return false;
}

function _Calendar_getRowInfo()
{
	Calendar.m_rowInfo = new Object();

    //Pad first of month to beginning of week
	g_weekStart = 0;
    // padding for weekdays before the 1st
	var now = new Date();
	if(typeof(Calendar.m_Month) == 'undefined')
		Calendar.m_Month = now.getMonth();
	if(typeof(Calendar.m_Year) == 'undefined')
		Calendar.m_Year = now.getFullYear();

	var firstOfMonth = 1*(Calendar.m_Month+1) + '/1/' + Calendar.m_Year;
    // get weekday for the first of the month
	var weekday = new Date(Calendar.m_Year, Calendar.m_Month, 1).getDay();
	if(weekday == 6 && false == peekMeetingsOnDate(firstOfMonth))
	{
		g_weekStart = 1;
		weekday = 1;
	}
	else
	{
		while (weekday != Dates.m_weekBeginsWith)
		{

			if (weekday == 0) weekday = 6;
			else
			{
				--weekday;
				--g_weekStart;
			}
		}
	}

	var len = getMonthLen(Calendar.m_Year, Calendar.m_Month);

	if(Calendar.Type == 'month')
	{
		Calendar.m_weeks = Math.round(len/7 + .5);
        //If the last of the month falls on Sunday
		var lastDay = new Date(Calendar.m_Year, Calendar.m_Month, len);

        Calendar.m_Day = 12;
        if(typeof(Calendar.m_Day) != 'undefined' && Calendar.m_Day != -1) {
            Calendar.m_weeks = 5;
        }
	}
	else if (Calendar.Type == 'week')
		Calendar.m_weeks = 1;

	first=Date.parse((Calendar.m_Month+1)+"/"+"1"+"/"+Calendar.m_Year+" 12:00 AM"); //day is not 0-based
	msStart=first+(g_weekStart*86400000);	// add/subtract the number of days to the begin of the calendar
	if(Calendar.Type == 'week')
		msStart += Calendar.Week * 7 * 86400000;
	msEnd = msStart + (((Calendar.m_weeks * 7)-1) * 86400000);

	msStart=Math.floor(msStart/86400000);	// number of days instead of number of milliseconds
	Calendar.getMeetingsArray(msStart, msEnd);

	Calendar.getWeekExtent(-1);

	Calendar.m_rowInfo.rows=0;
	for (var i = 0; i < Calendar.m_weekRows.length; i++)
	{
		if(typeof(Calendar.m_weekRows[i]) == 'undefined')
		{
			Calendar.m_weekRows[i] = 1;
			continue;
		}
		Calendar.m_rowInfo.rows += 1*Calendar.m_weekRows[i];
		if (Calendar.Type == 'month')
			Calendar.m_rowInfo.rows++;
	}

}

function _Calendar_showPerson(id)
{
	for (var i=0; i < Calendar.m_Roster.length; i++)
	{
		if(Calendar.m_Roster[i]['PersonID'] == id)
			return document.getElementById(Calendar.m_Roster[i]['chkID']).checked;
	}
	alert('PersonID ' + id + ' not found!');
	return false;
}

var arrDayNames= new Array('Sunday', 'Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');

function _Calendar_doDayView(strDate, mtgDay)
{

	var day = new Date(strDate);
	var fullDate = arrDayNames[day.getDay()] + ' ' + Dates.m_monthNames[day.getMonth()] + ' ' + day.getDate() + ', ' + day.getFullYear();
	alert('Single Date View: ' + fullDate + '; day index=' + mtgDay);
//Salespeople in cols
	var arrSP = new Array();
//Meeting times in rows
	var time_slots = new Array();
	var mtgs = Calendar.m_arrMeetings[mtgDay];
	for(var i=0; i < mtgs.length; i++)
	{
		if(!Calendar.showPerson(mtgs[i]['PersonID'])) continue;
		var foundTime = false;
		var foundSP = false;


		for (var j=0; j < arrSP.length; j++)
		{
			if(arrSP[j] == mtgs[i]['PersonID'])
			{
				foundSP = true;
				break;
			}
		}
		if(!foundSP) arrSP.push(mtgs[i]['PersonID']);


		for (var j=0; j < time_slots.length; j++)
		{
			if(time_slots[j] == mtgs[i]['timeMeeting'])
			{
				foundTime = true;
				break;
			}
		}
		if (!foundTime) time_slots.push(mtgs[i]['timeMeeting']);
	}
//alert('People to show(cols): ' + arrSP.length + ' Time slots(rows): ' + time_slots.length);

//Screen stuff
	var width=calcScreenWidth();

	Calendar.m_cellHeight = Math.floor(100/(1*time_slots.length)) + '%';
	var minHeight=28;




	var ret = '<table BORDER=0 height="100%" width=100% cellspacing=0 cellpadding=0 onselectstart="return false;">';

	var lCell = '<td width="8" ><img src="../images/dgreybox_l.gif" height="100%" width=8 ></td>';
	var _rCell = '<td width="11"><img src="../images/dgreybox_r.gif" height="100%" width=11></td>';
	var rrCell = '<td width="12"><img src="../images/dgreybox_r.gif" height="100%" width=11><img src="../images/spacer.gif" height="'+minHeight+'" width=1></td>';
	var rCell = '<td width="12"><img src="../images/dgreybox_r.gif" height="100%" width=11><img src="../images/spacer.gif" height="'+minHeight+'" width=1></td>';
//	var frills=(1+8+11);
	var frills=20;
	width=width-frills;

	width-=20; //button width




	var rwSpan = time_slots.length + 4;
//Paint the single button to the left ('Month')
//Figure out which week we have
	var week = Math.floor(mtgDay/7);
	ret += '<tr><td width="20" rowspan="' + rwSpan + '">';
	ret += '<input type="button" style="writing-mode:tb-rl" onclick="Calendar.doMonthView()" value="Month">';
	ret += '<input type="button" style="writing-mode:tb-rl" onclick="Calendar.doWeekView(' + week + ')" value="Week">';
	ret += '</td>';


//Now paint the top of the single cell
	var tlCell = '<td width="8" height="1%"><img src="../images/dgreybox_tl.gif" height="10" width=8></td>';
	var trCell = '<td width="8" height="1%"><img src="../images/dgreybox_tr.gif" height="10" width=11></td>';

//	ret += '<tr>' + tlCell;

	ret += tlCell;
	ret += '<td height="1%" colspan=' + arrSP.length + '><img src="../images/dgreybox_t.gif" height="10" width="' + width + '"></td>';
	ret+=trCell + '<td width=1><img src="../images/spacer.gif" height=1 width=1></td></tr>';

//Now the sides, with the heading
	ret += '<tr>';
	ret += '<td width="8" rowspan='+ (time_slots.length + 1) +'><img src="../images/dgreybox_l.gif" height="100%" width=8 ></td>';
	ret += '<td height="28" colspan="' + arrSP.length + ' "class="calendar_header">' + fullDate + '</td>';
//	ret += '<td height="28" colspan=1 class="calendar_header">' + fullDate + '</td>';
	ret += '<td width="11" rowspan=' + (time_slots.length + 1) + '><img src="../images/dgreybox_r.gif" height="100%" width=11></td>';

//And the spacer
	ret += '<td width="1"></td></tr>';

//Paint the data
	var ind=0;
	for (var i=0; i<time_slots.length; i++)
	{
		ret += '<tr>';
		for (var j=0; j< arrSP.length; j++)
		{
			var strMtg = '';
			var color = '';

			while(mtgs[ind]['timeMeeting'] == time_slots[i] && mtgs[ind]['PersonID'] == arrSP[j])
			{
				if (strMtg.length > 0) strMtg += '<br>';
				strMtg += time_slots[i] + ' ' + mtgs[ind]['comp'];
				strMtg += '<br>(' + mtgs[ind]['cat'];
				if(mtgs[ind]['vlevel'].length > 0)
					strMtg += ',' + mtgs[ind]['vlevel'];
				strMtg += ')';
				color = mtgs[ind]['Color'];
				if (++ind >= mtgs.length)
					break;
			}
			if(strMtg.length > 0)
			{
				var strStyle = 'style="color:' + color + '"';
				ret += '<td class="meeting_cell" ' + strStyle + '>' + strMtg + '</td>';
			}
			else
				ret += '<td class="meeting_cell">&nbsp</td>';
		}
		ret +='</tr>';
	}


//paint cell bottom
	var blCell = '<td width="8" height="1%"><img src="../images/dgreybox_bl.gif" height="11" width=8></td>';
	var brCell = '<td width="11" height="1%"><img src="../images/dgreybox_br.gif" height="11" width=11></td>';
	var bCell = '<td height="1%" colspan=' + arrSP.length + '><img src="../images/dgreybox_b.gif" height="11" width="100%"></td>';

	ret += '<tr>' + blCell + bCell + brCell + '<td width="1"></td></tr>';

	ret += '</table>';


	document.getElementById('theGrid').innerHTML = ret;


}

function calcScreenWidth()
{
	var width=800;
	if ((navigator.appName.indexOf("Netscape") != -1))
	{
		width=window.innerWidth;
		width-=20;	// somebody figure out how to determine this please
	}
	else
	{
		width=document.body.clientWidth;
		width-=document.body.rightMargin;
		width-=document.body.leftMargin;
	}
	return width;
}

function calcScreenHeight()
{
	var height=600;
	if ((navigator.appName.indexOf("Netscape") != -1))
	{
		height=window.innerHeight;
		height-=20;	// somebody figure out how to determine this please
	}
	else
	{
		height=document.body.clientHeight;
		height-=document.body.bottomMargin;
		height-=document.body.topMargin;
	}
	return height;
}

function _Calendar_makeCalendarHeader(label, mtgDay, width)
{
	if (!width)
		width = false;
//	var ret = '<td height="' + Calendar.m_cellHeight + '" colspan="' + colspan + '"';
//	var ret = '<td height="28" colspan="' + colspan + '"';
	var ret = '<td height="24" ';
	if (width)
		ret += 'width="'+width+'" ';

//NB COMMENT THIS LINE IN TO ENABLE DAY VIEW
	if(0/*Calendar.Type != 'day'*/)
		ret += ' style="cursor:hand" onclick="Calendar.doDayView(\'' + label + '\',' + mtgDay +')"';

	ret += ' class="calendar_header">';


	ret += '<table width=100% cellpadding=0 cellspacing=0 border=0><tr><td><img src="../images/grid-blue-left.jpg" ></td>';
	ret += '<td width=100% ';
	ret += ' class="grid_header_blue"';
	ret += '>'+ label + '</td><td><img src="../images/grid-blue-right.jpg" align="middle"></td></tr></table></td>';
	return ret;
}

function _Calendar_chkPerson()
{
	ok = false;
	for (i = 0; i < Calendar.m_Roster.length; i++)
	{
		if (document.getElementById(Calendar.m_Roster[i]['chkID']).checked == true)
		{
			ok = true;
			break;
		}
	}
	if(!ok)
	{
		alert('No salespeople selected!');
		return;
	}
	Calendar.m_rowInfo = null;
	Calendar.makeCalendar();
}



function _Calendar_makeCalendar()
{

//document.getElementById('theGrid').style.backgroundColor='black';

//************************************************************************************
//ultimately this will be toggled or passed in (stored in a cookie?)
	var now = new Date();
	var month = now.getMonth();
//************************************************************************************
	var width=calcScreenWidth()-16;

//	if (Calendar.m_rowInfo == null) Calendar.getRowInfo();
	Calendar.getRowInfo();

	Calendar.m_cellHeight = Math.floor(100/(1*Calendar.m_rowInfo.rows)) + '%';
	var minHeight=28;

	var dayheaderWidth = ((width)/5)-1;

	var ret = '<table BORDER=0 height="1%" width="'+(width-4)+'" cellspacing=0 cellpadding=0 onselectstart="return false;">';

	var lCell = '<td width="8" ><img src="../images/dgreybox_l.gif" height="100%" width=8 ></td>';
	var rCell = '<td width="11"><img src="../images/dgreybox_r.gif" height="100%" width=11></td>';
	var rrCell = '<td width="12"><img src="../images/dgreybox_r.gif" height="100%" width=11><img src="../images/spacer.gif" height="'+minHeight+'" width=1></td>';
	var mCell = rCell + lCell;
	var frills=1+this.cols*(8+11);
	width=width-frills;
	width-=9;

    //Paint headings: Days of week
	ret += '<tr>';

    //button for selecting week / month
	ret += '<td width="29" style="background-color:#808080"></td>';

	width -= 15;

	for (var i = Dates.m_weekBeginsWith; true; )  //sunday=0; monday=1
	{
		var bSkip = false;
		if (i == 0) bSkip = (this.showSun == false);
		else if (i == 6) bSkip = (this.showSat == false);

		if (!bSkip)
		{
			var strHead = arrDayNames[i];
			ret += '<td width="15" rowspan=1 style="background-color: #808080"></td>';
			ret += Calendar.makeCalendarHeader(strHead,  1, dayheaderWidth);
			ret += '<td width="11"  rowspan=1 style="background-color: #808080"></td>';
		}
		if (i == Dates.m_weekDays.length - 1) i = 0;
		else ++i;
		if (i == Dates.m_weekBeginsWith) break;

	}
	ret += '<td width="6" style="background-color: #808080"></td></tr>';
	var offset,i,id,temp;

	var row = 0;

	var scrollerHeight = calcScreenHeight()-(144+(this.numRosterRows*23));

//	ret += '<tr></thead><tbody>';
	//ret += '</thead><tbody>';
	ret += '</table>';
	ret += '<div style="overflow-y:scroll; height:'+scrollerHeight+'px;"><table BORDER=0 height="90%" width=1% cellspacing=0 cellpadding=0 onselectstart="return false;">';

	if (Calendar.Type == 'month')
	{
		for (var week=0; week < Calendar.m_weekRows.length; week++)
		{
			ret += Calendar.paintRow(week, width, minHeight);
		}
	}
	else
		ret += Calendar.paintRow(Calendar.Week, width, minHeight);

	ret += '</table></div>';
	document.getElementById('theGrid').innerHTML = ret;
	// ssMonth.positionControls();
	// ssYear.positionControls();
}



function _Calendar_makeRoster(idPerson, bShow)
{
	var bShowRoster = true;
	if(typeof(bShow != 'undefined') && bShow == false)
		bShowRoster = false;
	var tabhide = bShowRoster ? 'visible':'hidden';
	var trhide = bShowRoster ? 'inline':'none';

	var ret = '';
	if(bShowRoster)
		ret ='<table style="visiblity: visible" height=20 width="100%" cellspacing=1 cellpadding=0>';
	else
		ret ='<table style="visiblity: hidden">';
	Calendar.m_Roster = new Array();
	var cellWidth = Math.floor(100 / g_people.length) + '%';

	var mymod = 7;
	if (g_people.length > 7 && g_people.length < 14)
		mymod = Math.floor(g_people.length / 2);
	var checkAll = typeof(idPerson) == 'undefined';
	this.numRosterRows = 0;
//	var noBalls = (Grid.numPeopleShown() == 1);
	var noBalls = (getUserLevel() == 1);

	for (var i = 0; i < g_people.length; ++i)
	{
		var bChecked=false;
		if ((i%mymod)==0)
		{
			if (i > 0)
				ret += '</tr>';
			ret += '<tr style="display:' + trhide + '"><div style="display:' + trhide + '" align="center" onselectstart="return false;">';
			this.numRosterRows++;
		}
		var data = g_people[i];

		ret += '<td nowrap bgcolor="' + data[g_people.Color] + '" width="' + cellWidth + '" height=20';
		ret += ' class="scroller" style="cursor: default;height:20px;">';
		var tempinfo = get_salesperson_info(data[g_people.PersonID]);
		if (tempinfo.name)
		{

			ret += '<table border="0" width="100%" cellpadding="0" cellspacing="0"><tr style="display:' + trhide + ';width:100%">';
			if(noBalls)
				ret += '<td width="1"></td>';
			else
			{
				if(isNav)
					ret += '<td style="width:20px;background-color:black;color:white;font-weight:bold;font-size:10pt;align:center">'+(i+1)+'</td>';
				else
					ret += '<td width="1"><span style="background-image:url(../images/BlackDot.gif); background-repeat:no-repeat; color:white; width:20px; height:20px;" width="20" height="20"><center style="position:relative; top:1px; font-weight:bold;font-size:10pt;">'+(i+1)+'</center></span></td>';
			}



			ret += '<td nowrap align="left" class="scroller" style="height:20px;">&nbsp;' + tempinfo.name + '</td>';
			ret += '<td align="right" style="width:100%">&nbsp;<input type="checkbox" id="chkPerson' + i + '" onclick="Calendar.chkPerson()"';
			if(checkAll || idPerson == g_people[i][g_people.PersonID])
			{
				bChecked=true;
				ret += ' checked=true';
			}
			ret += '></td></tr></table></div></td>';

		}
		else
			ret += '</div></td>';

		var tmp=new Array();
		tmp['PersonID'] = data[g_people.PersonID];
		tmp['Name'] = tempinfo.name;
		tmp['chkID'] = 'chkPerson' + i;
		tmp['checked'] = (bChecked == true);
		Calendar.m_Roster.push(tmp);

	}

	ret += '</tr></table>';
	ret += paintButtonRow(Calendar.Type, checkAll);
	document.getElementById('theScroller').innerHTML = ret;
	initSS();
}



