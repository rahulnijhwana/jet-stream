Alerts = new Object();

Alerts.makeHTML = _Alerts_make;

//Is user an alias?  if so, does he have full rights?
g_alias_fullrights = 1;
if (g_login[0][g_login.LoginID] != g_login[0][g_login.PersonID])
	g_alias_fullrights = (g_login[0][g_login.FullRights] == '1');


// this is a local array used by these functions
// calcAlerts fills it
// isAlert checks to see if any are in it
// showAlerts displays alerts contained in it
// alerts are grouped by person, but an extra dimension wasn't add because it wasn't worth it

l_alerts = new Array();

function setAlertTable()
{
	l_alerts.AlertType = 0;
	l_alerts.State = 1;
	l_alerts.ID = 2;		// this is either opp ID, trend ID, or nothing
	l_alerts.UserID = 3;		// ?????
	l_alerts.PersonID =4;
	l_alerts.Days=5;
	l_alerts.SnoozeDate=6;
}
setAlertTable();

/*
AlertMsg=new Array(	"Sales Cycle Alert",
			"Missed "+g_company[0][g_company.CLabel]+" Date Alert",
			"Meeting <b>NOT</b> Updated Alert",
			"IP-Stalled Too Long Alert",
			"DP-Stalled Too Long Alert",
			"Trend Alert",
			"Unlikely Analysis Pattern Alert",
			"System Non-Use Alert");
*/	

AlertMsg=new Array(	"Sales Cycle Alert",
			"Missed "+g_company[0][g_company.CLabel]+" Date Alert",
			"Meeting <b>NOT</b> Updated Alert",
			catLabel('IP') + "-" + catLabel('Stalled') + " Too Long Alert",
			catLabel('DP') + "-" + catLabel('Stalled') + " Too Long Alert",
			"Trend Alert",
			"Unlikely Analysis Pattern Alert",
			"System Non-Use Alert");


// the following are used for the snooze popup
g_snooze_state="";	// whether the snoozee is snoozing or awake
g_snooze_index=-1;	// the index of the snoozee
g_snooze_date=""	// the date the snooze expires
g_snooze_type=-1;	// the type of the snoozee (only for snoozing complete types at once)
g_snooze_PersonID=-1;	// the id of the person snoozing (only for snoozing complete types at once)

// this is called to launch the snooze prompt window for one alert
function alertSnooze(i)
{
	g_snooze_index=i;
	g_snooze_type=-1;
	g_snooze_state=l_alerts[i][l_alerts.State];
	g_snooze_date=l_alerts[i][l_alerts.SnoozeDate];
	Windowing.openPrompt('snooze.html');
}

// this is called to launch the snooze prompt window for a type of alert
function alertSnoozeType(PersonID,type)
{
	g_snooze_state="awake";
	g_snooze_index=-1;	// indicates a type rather than a specific opportunity
	g_snooze_type=type;
	g_snooze_PersonID=PersonID;
	Windowing.openPrompt('snooze.html');
}

function unlikelyAlertInfo()
{
	var msg = "The analysis of this salesperson's board results in an unexpected outcome. ";
	msg += 'It is recommended that you review the opportunities on the board, as one or more ';
	msg += 'of them are probably incorrectly placed.'

	Windowing.dropBox.msg = msg;
	Windowing.dropBox.title = 'Unlikely Analysis Pattern Alert Details';
	Windowing.dropBox.msgClass = 'plabel';
	Windowing.dropBox.noInput = true;
	Windowing.dropBox.noCancel = true;
	Windowing.openSizedPrompt("../shared/prompt.html", 170, 550);
}

function nonUseAlertInfo(days)
{
	var msg;
	if (days < 0) msg = 'This salesperson has never logged in.';
	else
	{
		msg = 'This salesperson has not logged in for ' + days + ' day';
		if (days != 1) msg += 's';
	}

	Windowing.dropBox.msg = msg;
	Windowing.dropBox.title = 'System Non-Use Alert Details';
	Windowing.dropBox.msgClass = 'plabel';
	Windowing.dropBox.noInput = true;
	Windowing.dropBox.noCancel = true;
	Windowing.openSizedPrompt("../shared/prompt.html", 100, 550);
}

function changeAlertSnooze(state,sDate)
{
	if (g_snooze_index>=0)
	{
		var lert=l_alerts[g_snooze_index];
		lert[l_alerts.State]=state;
		if (state=="snoozed")
		{
			lert[l_alerts.SnoozeDate]=sDate;
			setSnooze(lert[l_alerts.PersonID], lert[l_alerts.AlertType], lert[l_alerts.ID], sDate);
		}
		else
			setSnooze(lert[l_alerts.PersonID], lert[l_alerts.AlertType], lert[l_alerts.ID], null);
	}
	else
	{
		var i;
		snoozeDate=Dates.makeDateObj(sDate);
		for (i=0;i<l_alerts.length;i++)
		{
			var lert=l_alerts[i];
			if (g_snooze_type!=lert[l_alerts.AlertType]) continue;
			if (g_snooze_PersonID!=lert[l_alerts.PersonID]) continue;
			if (state=="awake")
			{
				lert[l_alerts.State]=state;
				setSnooze(lert[l_alerts.PersonID],lert[l_alerts.AlertType],lert[l_alerts.ID],null);
			}
			else
			{
				if (lert[l_alerts.State]=="awake" || snoozeDate.getTime() > (Dates.makeDateObj(lert[l_alerts.SnoozeDate])).getTime())
				{
					lert[l_alerts.SnoozeDate]=sDate;
					lert[l_alerts.State]=state;
					setSnooze(lert[l_alerts.PersonID],lert[l_alerts.AlertType],lert[l_alerts.ID],sDate);
				}
			}
		}
	}
	win = Windowing.getWindow(g_user[0][g_user.PersonID]+'alerts');
	if (win) win.update_from_edit_window();

}

g_Show_Alerts_Detail=-1;		// alert type to show detail for, -1 means don't show detail
g_Show_Alerts_PersonID=-1;		// person to show alert detail for

function alertDetail(Alert,personID)
{

	g_Show_Alerts_Detail=Alert;
	g_Show_Alerts_PersonID=personID;
}


function _Alerts_make()
{
// this updates expired alerts

	var i;
	for (i=0;i<l_alerts.length;i++)
	{
		var lert=l_alerts[i];
		var lerttype=lert[l_alerts.AlertType];
		var SnoozeDate=getSnooze(lert[l_alerts.PersonID],lerttype,lert[l_alerts.ID]);
		if (SnoozeDate == null) lert[l_alerts.State]="awake";
		else
		{
			lert[l_alerts.SnoozeDate]=SnoozeDate;
			lert[l_alerts.State]="snoozed";
		}
	}


	var output="";
	if (window.g_accessType==1)
	{
		g_Show_Alerts_Detail=3;
		g_Show_Alerts_PersonID=g_user[0][g_user.PersonID];
	}
	else
	{
		if (g_alias_fullrights)
		{
			output='<table border=0><tr style="font-size:16; font-family:Arial; font-weight:bold"><td>Key:</b>&nbsp;</td><td>';
			output+='<img src="../images/clock_red.gif"> = Active alert, click to snooze';
			output+='&nbsp;&nbsp;<img src="../images/clock_black.gif"> = Snoozed alert, click to wake';
			output+='&nbsp;&nbsp;<img src="../images/go.gif"> = Detail available, click to view/edit';
			output+='</td></tr></table><br>';
		}
/*
		output+="<script language='javascript'>";
		output+="onfocus=redraw;";
		output+="</scr" + "ipt>";
*/		
		output+='<table border=1 cellspacing=0 cellpadding=2><tr><td style="font-size:16; font-family:Arial; font-weight:bold"><b>Alerts</b></td>';

		var i;
		for (i=0;i<g_people.length;i++)
		{
			var person=g_people[i];
			output+='<td style="font-size:16; font-family:Arial; font-weight:bold"><b>' + person[g_people.FirstName]+ '<br>' + person[g_people.LastName] +"</b></font></td>";
		}
		output+="</tr>";

		for (i=0;i<8;i++)	// loop thru alert types
		{
			var type=i+1;
			output+='</tr><td style="font-size:16; font-family:Arial; font-weight:bold">' + AlertMsg[i] + "</td>";
			var j;
			for (j=0;j<g_people.length;j++)
			{
				output+='<td align=center style="font-size:16; font-family:Arial; font-weight:bold">';
				var k;
				var any=false;		// do we have any
				var awake=false;	// do we have any awake

				var id=g_people[j][g_people.PersonID];
				for (k=0;k<l_alerts.length;k++)
				{
					var lert=l_alerts[k];
					var lerttype=lert[l_alerts.AlertType];
					if (lerttype!=type) continue;
					if (lert[l_alerts.PersonID]!=id) continue;
					any=true;
					if (lert[l_alerts.State]!="snoozed")
						awake=true;
					if (!awake && type<7) continue;	// there are more <6, >6 no more
					break;
				}
				if (any)
				{
					if (g_alias_fullrights)
					{
						if (type<7)
							output+="<a href='javascript:window.opener.alertSnoozeType("+id+","+type+");'>";
						else
							output+="<a href='javascript:window.opener.alertSnooze("+k+");'>";
						if (awake)
							output+="<img src='../images/clock_red.gif' border=0></a> ";
						else
							output+="<img src='../images/clock_black.gif' border=0></a> ";
					}
					if (type<7)
					{
						output+="<a href='javascript:window.opener.alertDetail("+type +","+ id +");redraw()'><img src='../images/go.gif' border=0></a> ";
					}
					else if (type == 7)
						output+="<a href='javascript:window.opener.unlikelyAlertInfo()'><img src='../images/go.gif' border=0></a> ";
					else if (type == 8)
					{
						var days = -1;
						if (lert[l_alerts.Days] > 0)
							days = lert[l_alerts.Days];
						
						output+="<a href='javascript:window.opener.nonUseAlertInfo("+days+")'><img src='../images/go.gif' border=0></a> ";
					}
				}
				else
					output+="&nbsp;";
				output+="</td>";

			}


			output+="</tr>";

		}

		output+="</table>"
	}
	if (g_Show_Alerts_Detail!=-1)
		output+=_Alerts_make2();

	if (output=="")
	{
		output="<br><b><div align='center'>There are no alerts.<div></b><br>";
	}

	return output;
}

function _Alerts_make2()
{
	var person=find_person(g_Show_Alerts_PersonID);
	var type=g_Show_Alerts_Detail;

	var output='<br><span style="font-size:12pt;">';
	output+=person[g_people.FirstName]+ ' ' + person[g_people.LastName];
	output+='<br><span style="color:red;">' + AlertMsg[type-1] + '</span>';
	output+="</span>";

	var i;
	var atLeastOne=false;
	output+="<table border=1 cellspacing=0 cellpadding=2><tr style='font-weight: bold'>";
	if(window.g_accessType==2)
	{
		output+="<td><input type='button' class='command' name='Close' value='Close' ";
		output+="onclick='window.opener.g_Show_Alerts_Detail=-1;redraw();'></td>";
	}
	else output+="<td></td>"
	;
	if (type==6)
		output+="<td>Trend</td></tr>";
	else
		output+="<td>Company</td><td>Offering</td><td>";

	if (type==3)
		output+="Meeting</td><td>Details";
	else if (type != 6)
	{
//		output+="First Meeting";
		output += catLabel("First Meeting");

		output+="</td><td>Details";
	}
	output += "</td></tr>";

	for (i=0;i<l_alerts.length;i++)
	{
		var lert=l_alerts[i];
		if (lert[l_alerts.PersonID]!=g_Show_Alerts_PersonID) continue;
		if (lert[l_alerts.AlertType]!=type) continue;
		//if (lert[l_alerts.State]=="snoozed") continue;


		atLeastOne=true;
		output+="<tr><td align=center>"
		if (window.g_accessType==2 && g_alias_fullrights)
		{
			output+="<a href='javascript:window.opener.alertSnooze("+i+");'>";
			if (lert[l_alerts.State]=="snoozed")
				output+="<img src='../images/clock_black.gif' border=0></a> ";
			else
				output+="<img src='../images/clock_red.gif' border=0></a> ";
		}


		if (type==6)
		{
			output+='<a href="javascript:window.opener.open_trend('+(lert[l_alerts.ID]+1) +","+lert[l_alerts.PersonID]+');">';
			output+="<img src='../images/go.gif' border=0></a>";
		}
		else
		{
			if (g_alias_fullrights)
			{
				output+='<a href="javascript:window.opener.do_edit(\'_'+lert[l_alerts.ID]+'\');">';
				output+="<img src='../images/go.gif' border=0></a>";
			}
		}
		output+="</td>";
		if (type==6)
		{
			var trendName = g_trends[lert[l_alerts.ID]][g_trends.Name];
			if (lert[l_alerts.ID] == 12)
				trendName = trendName.replace(/Requirement1/ig,  g_company[0][g_company.Requirement1]);
			else if (lert[l_alerts.ID] == 13)
				trendName = trendName.replace(/Requirement2/ig,  g_company[0][g_company.Requirement2]);

			output+="<td>Trend "+trendName;
		}
		else
		{
			var opp=find_opportunity(lert[l_alerts.ID]);
			output+="<td>" + opp[g_opps.Company];
			if (opp[g_opps.Division] != '')
				output+="<br>" + opp[g_opps.Division];
			/*
			var prod=find_product(opp[g_opps.ProductID]);
			if (prod==null)
				output+="</td><td>&nbsp;";
			else
				output+="</td><td>" + prod[g_products.Name];
			*/
			output+="</td><td>" + getOfferingsList(opp[g_opps.DealID]);

			if (type==3 && opp[g_opps.Category]!=1)
				output+="</td><td>" + Dates.formatDate(Dates.makeDateObj(Dates.mssql2us(opp[g_opps.NextMeeting])));
			else
				output+="</td><td>" + Dates.formatDate(Dates.makeDateObj(Dates.mssql2us(opp[g_opps.FirstMeeting])));

			switch(type)
			{
			case 1: output+="</td><td>" + lert[l_alerts.Days] + " days old";break;
			case 2: output+="</td><td>" + "missed by " + lert[l_alerts.Days] + " days";break;
			case 3: output+="</td><td>" + lert[l_alerts.Days] + " days out of date";break;
			case 4: output+="</td><td>" + lert[l_alerts.Days] + " days stalled";break;
			case 5: output+="</td><td>" + lert[l_alerts.Days] + " days stalled";break;
			}
		}
		if (lert[l_alerts.State]=="snoozed")
		{
			var tempdate = Dates.makeDateObj(Dates.mssql2us(lert[l_alerts.SnoozeDate]));
			var temphours = tempdate.getHours();
			var temp_ampm = 'AM';
			if (temphours > 11)
				temp_ampm = 'PM';
			if (temphours > 12)
				temphours -= 12;
			else if (temphours == 0)
				temphours = 12;
			var tempminutes = '' + tempdate.getMinutes();
			if (tempminutes.length == 1)
				tempminutes = ':0' + tempminutes;
			else
				tempminutes = ':' + tempminutes
			
			output += ', snoozed until ' + (tempdate.getMonth() + 1) + '/' + tempdate.getDate() + '/' + tempdate.getYear() + ' ' + temphours + tempminutes + temp_ampm;
		}
		output+="</td></tr>";
	}
	output+="</table>";
	if (atLeastOne) return output;
	return '';
}


function calcOppBorders()
{
	var today=new Date();	// don't want to do this a million times
	//alert(stdYellow+"  "+stdRed);

	var indPID=g_opps.PersonID;
	var indPRODID=g_opps.ProductID;
	var indCat=g_opps.Category;
	var indFM=g_opps.FirstMeeting;
	
	var indSCL=g_salescycles.SalesCycleLength;
	var indSD=g_salescycles.StandardDeviation;

	var i;
	for (i=0;i<g_opps.length;i++)
	{
		var opp = g_opps[i];
		if (opp[indCat] >= 6)
			continue;	// closed stuff is ok
		if (opp[indPRODID] == -1)
			continue;	// no salescyle for opp without a product
		var SalesCycle = find_salescycle(opp[indPID], opp[indPRODID]);
		if (SalesCycle == null)
			continue;	// this should never happen
		var Prod = find_product(opp[indPRODID]);
		if (Prod == null)
			continue;	// this should never happen
		var stdRed = getSTDfromPercent(Prod[g_products.RedAlertPercentage]);
		var stdYellow = getSTDfromPercent(Prod[g_products.YellowAlertPercentage]);
		//calculate number of days old
		var daysold = DaySpan(today,opp[indFM]);
		//calculate # of std's for red
		opp.SalesCycleLength = parseInt(SalesCycle[indSCL]);
		var limit = opp.SalesCycleLength + stdRed * SalesCycle[indSD];
		//alert("Daysold "+ daysold+ " stdRed: " + stdRed+" stdYellow: "+stdYellow + " SalesCycle: " +opp.SalesCycleLength+" dev: "+	SalesCycle[g_salescycles.StandardDeviation]);
		if (limit <= daysold)
			opp.border = "red";
		else
		{
			limit = opp.SalesCycleLength + stdYellow * SalesCycle[indSD];
			if (limit <= daysold)
				opp.border = "yellow";
		}
	}
}



// checks to see if there are any alerts
// sets the alerts button accordingly
function calcAllAlerts()
{
	l_alerts = new Array();
	setAlertTable();
	// look through each sales person
	var i;
	for (i=0;i<g_people.length;i++)
	{
		// calc individual or group, based on length of g_people
		if (g_people[i][g_people.CompanyID] == g_company[0][g_company.CompanyID])
			calcAlerts(g_people[i]);
	}

	setAlertButton();
}

function setAlertButton()
{
	for (i=0;i<l_alerts.length;i++)
	{
		// snoozed alerts don't make the alert button flash
		var lert=l_alerts[i];
		if (lert[l_alerts.State]=="snoozed") continue;

		// make sure the right alert for the right view
		if (lert[l_alerts.AlertType]==3 && g_people.length > 1) continue; // don't show 3 if group view
		if (lert[l_alerts.AlertType]!=3 && g_people.length <= 1) continue; // don't show manager's alerts if single view
		// found a valid alert, set blinking
		Header.blinkOn('Alerts');
		return;
	}

	// not one valid alert, turn off blinking
	Header.blinkOff('Alerts');
}


function changeField(row, fieldno, value)
{
	if (row[fieldno] != value)
	{
		row[fieldno]=value;
		row.isDirty=true;
		alerts_changed();
	}
}

// calcs alerts for specified salesmen
// new theory, calc all alerts, in order to update db correctly
// when we show alerts, we either show single or many
function calcAlerts(person)
{

	var personID=person[g_people.PersonID];

	today=new Date();	// don't want to do this a million times

	var indFCD=g_opps.AdjustedCloseDate;
	var indPID=g_opps.PersonID;
	var indOID=g_opps.DealID;
	var indCat=g_opps.Category;
	var indMove=g_opps.LastMoved;
	var indFM=g_opps.FirstMeeting;
	var indNM=g_opps.NextMeeting;
	var indRank=g_opps.Rank;
	var indCat=g_opps.Category;
	var indPRODID=g_opps.ProductID;

	var NonUseGracePeriod=g_company[0][g_company.NonUseGracePeriod];
	var MissedMeetingGracePeriod=g_company[0][g_company.MissedMeetingGracePeriod];
	var IPStalledLimit=g_company[0][g_company.IPStalledLimit];
	var DPStalledLimit=g_company[0][g_company.DPStalledLimit];

//alert(personID);
// check on the kind of board
	var i,Days;

// start with all snoozes deleted-if an alert matches a snooze, it will undelete it
	for (i=0;i<g_snoozealerts.length;i++)
	{
		var zzz=g_snoozealerts[i];
		if (zzz[g_snoozealerts.PersonID]==personID) zzz.used=false;
	}

	for (i=0;i<g_opps.length;i++)
	{
		opp=g_opps[i];
		if (opp[indPID]!=personID) continue;
		if (opp[indCat]==9) continue;	// never count removed opps for anything

	// check for "per opportunity" alerts
	// 1) a red border
		if (window.g_accessType==2)
		{
			if (opp.border=="red")
			{
				Days=DaySpan(today,opp[indFM]);
				addAlert(1,opp[indOID],0,personID,Days);
				changeField(opp,g_opps.Alert1,'1');
			}
			else changeField(opp,g_opps.Alert1,'0');
		// 2) forecast sales date in past and not sold
			if (opp[indPRODID]==-1) changeField(opp,g_opps.Alert2,'0');
			else
			{
				if (opp[indFCD]=="")	// if there is not an overridden adjusted close date, then calculate
				{
					Days=DaySpan(today,opp[indFM])-opp.SalesCycleLength;
				}
				else Days=DaySpan(today,opp[indFCD]);
				if (opp[indCat]<6 && Days>0)
				{
					addAlert(2,opp[indOID],0,personID,Days);
					changeField(opp,g_opps.Alert2,'1');
				}
				else changeField(opp,g_opps.Alert2,'0');
			}
		}
	// 3)  nextmeeting missed
		if (opp[indCat]==3 || opp[indCat]>=5 )
		{
				changeField(opp,g_opps.Alert3,'0');
		}
		else
		{
			if (opp[indCat]==1)
			{
				Days=DaySpan(today,opp[indFM]);
			}
			else
				Days=DaySpan(today,opp[indNM]);
			if (Days>MissedMeetingGracePeriod)
			{
				addAlert(3,opp[indOID],0,personID,Days);
				changeField(opp,g_opps.Alert3,'1');
			}
			else changeField(opp,g_opps.Alert3,'0');
		}
		if (window.g_accessType==2)
		{
		// 4) in IP-stalled too long
			Days=DaySpan(today,opp[indMove]);
			if (opp[indCat]==3 && Days>IPStalledLimit)
			{
				addAlert(4,opp[indOID],0,personID, Days);
				changeField(opp,g_opps.Alert4,'1');
			}
			else changeField(opp,g_opps.Alert4,'0');
		// 5) in DP-stalled too long
			if (opp[indCat]==5 && Days>DPStalledLimit)
			{
				addAlert(5,opp[indOID],0,personID, Days);
				changeField(opp,g_opps.Alert5,'1');
			}
			else changeField(opp,g_opps.Alert5,'0');
		}
	}
	if (window.g_accessType==2)
	{
		// 6
		var trendbits=person[g_people.TrendState];
		for (i=0;i<14;i++)		// there are 14 alerts i know
		{
			var bit=trendbits & 1;
			trendbits=trendbits >> 1;
			if (bit)
				addAlert(6,i,0,personID,0);
		}

		// 7 invalid ranking[g_people.AnalysisID]
		if (person[g_people.AnalysisID]==-1)
		{
			addAlert(7,0,0,personID,0);
		}

		// 8) not logged on for a while
		if (person[g_people.LastUsed]=="")
		{
			addAlert(8,0,0,personID,-1);	// never used the system
			//alert(personID + " never used");
		}
		else
		{
			Days = DaySpan(today, person[g_people.LastUsed])

			if (Days>NonUseGracePeriod || person[g_people.LastUsed]=="")
			{
				addAlert(8,0,0,personID,Days);
				//alert(personID + " not used for " + Days + " days");
			}
		}
	}
// start with all snoozes deleted-if an alert matches a snooze, it will undelete it
	for (i=0;i<g_snoozealerts.length;i++)
	{
		var zzz=g_snoozealerts[i];
		if (zzz[g_snoozealerts.PersonID]==personID && !zzz.used && !zzz.deleted)
		{
			zzz.deleted=true;
			alerts_changed();
		}
	}
}

//  U T I L I T I E S  f o r  A L E R T  H A N D L I N G

// first date passed in as a date, second as a string
// calcs secondString-firstDate in days
// do this calc once
msPerDay = 24 * 60 * 60 * 1000 // Number of milliseconds per day;
function DaySpan(firstDate, secondString)
{
//alert("["+secondString+"]");
//alert("["+Dates.mssql2us(secondString)+"]");
//	var secondDate=Dates.makeDateObj(Dates.mssql2us(secondString));
	
//	testDate=Dates.makeDateObj(secondDate);
//	if (testDate.toString()!=secondDate.toString()) alert("Test ["+testDate+"] secondDate ["+secondDate+"] Org ["+secondString+"]");
// using the above tests, i've tested this under IE 6.0.28 && Mozilla 1.6 (5.0)
//	secondDate=Dates.makeDateObj(secondString);
	secondDate=Dates.makeDateObj(Dates.mssql2us(secondString));
	secondDate.setHours(0);
	secondDate.setMinutes(0);
	secondDate.setSeconds(0);
	secondDate.setMilliseconds(0);

	var fDate=Dates.makeDateObj(firstDate);
	fDate.setHours(0);
	fDate.setMinutes(0);
	fDate.setSeconds(0);
	fDate.setMilliseconds(0);

	var daysLeft = (fDate.getTime()-secondDate.getTime()) / msPerDay;
	daysLeft = Math.round(daysLeft);
	return daysLeft;
}

function addAlert(AlertType, ID, UserID, PersonID, Days)
{
	newAlert=new Array();
	newAlert[l_alerts.AlertType]=AlertType;
	newAlert[l_alerts.ID]=ID;
	newAlert[l_alerts.UserID]=UserID;
	newAlert[l_alerts.PersonID]=PersonID;
	newAlert[l_alerts.Days]=Days;
	l_alerts[l_alerts.length] = newAlert;

	var SnoozeDate=getSnooze(PersonID,AlertType,ID);
	if (SnoozeDate == null) newAlert[l_alerts.State]="awake";
	else
	{
		newAlert[l_alerts.SnoozeDate]=SnoozeDate;
		newAlert[l_alerts.State]="snoozed";
	}

}

// sets rows in the snooze table by updating or adding
// if Date is null, then snooze should be deleted
// the ID could either be a trend id or an opportunity ID
g_snoozealerts.nextId=-1;
function setSnooze(PersonID,AlertType, ID, sDate)
{
	// first, try to find it
	var i;
	for (i=0;i<g_snoozealerts.length;i++)
	{
		var zzz=g_snoozealerts[i];
		if (zzz[g_snoozealerts.PersonID]==PersonID && zzz[g_snoozealerts.AlertType]==AlertType && zzz[g_snoozealerts.DealID]==ID)
		{
			if (sDate==null)
			{
				zzz.deleted=true;
				alerts_changed();
			}
			else
			{
				zzz.deleted=false;
//alert("Changing snooze date to " + sDate);				
				changeField(zzz,g_snoozealerts.EndDate,sDate);
			}
			setAlertButton();
			return;
		}
	}

	// couldn't find one, have to add :(
	if (sDate==null) return; // don't add a record being deleted
// alert("adding alert PersonID:"+PersonID+" Type:"+AlertType+" OppID:"+ID+" Date:"+sDate); 	
	newSnooze=new Array();
	newSnooze.isDirty=true;
	newSnooze[g_snoozealerts.PersonID]=PersonID;
	newSnooze[g_snoozealerts.AlertType]=AlertType;
	newSnooze[g_snoozealerts.DealID]=ID;
	newSnooze[g_snoozealerts.SnoozeAlertsID ]=g_snoozealerts.nextId--;
	newSnooze[g_snoozealerts.EndDate]=sDate;
	g_snoozealerts[g_snoozealerts.length] = newSnooze;
	alerts_changed();
	setAlertButton();
}

// given the snooze info, return the Date, or null if there is no snooze
function getSnooze(PersonID,AlertType, ID)
{
	// first, try to find it
	var i;
// alert("getting alert PersonID:"+PersonID+" Type:"+AlertType+" OppID:"+ID); 	
	for (i=0;i<g_snoozealerts.length;i++)
	{
		var zzz=g_snoozealerts[i];
		if (zzz[g_snoozealerts.PersonID]==PersonID && zzz[g_snoozealerts.AlertType]==AlertType && zzz[g_snoozealerts.DealID]==ID)
		{
			if (zzz.deleted)
				return null;
			if ((Dates.makeDateObj(zzz[g_snoozealerts.EndDate])).getTime()<(new Date()).getTime())
			{
				zzz.deleted=true;
				return null;
			}
			else
			{
				zzz.used=true;
				zzz.deleted=false;
 // alert("Date on file: "+zzz[g_snoozealerts.EndDate]);
				return Dates.mssql2us(zzz[g_snoozealerts.EndDate]);
			}
		}
	}
	return null;
}

g_std=new Array(
   .0000,//  50
   .0180,//  51
   .0461,//  52
   .0681,//  53
   .0962,//  54
   .1183,//  55
   .1464,//  56
   .1686,//  57
   .1968,//  58
   .2190,//  59
   .2474,//  60
   .2697,//  61
   .2983,//  62
   .3269,//  63
   .3494,//  64
   .3783,//  65
   .4072,//  66
   .4364,//  67
   .4592,//  68
   .4885,//  69
   .5181,//  70
   .5477,//  71
   .5776,//  72
   .6076,//  73
   .6378,//  74
   .6683,//  75
   .6989,//  76
   .7297,//  77
   .7677,//  78
   .7990,//  79
   .8377,//  80
   .8694,//  81
   .9088,//  82
   .9485,//  83
   .9887,//  84
  1.0292,//  85
  1.0779,//  86
  1.1192,//  87
  1.1690,//  88
  1.2193,//  89
  1.2785,//  90
  1.3385,//  91
  1.3993,//  92
  1.4694,//  93
  1.5494,//  94
  1.6395,//  95
  1.7492,//  96
  1.8794,//  97
  2.0497,//  98
  2.3199//  99
);

function getSTDfromPercent(percent)
{
	percent=percent-50;
	if (percent<0) percent=0;
	else if (percent>49) percent=49;
	return g_std[percent];
}
