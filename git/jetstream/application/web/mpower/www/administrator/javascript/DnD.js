
var _DnD_dragelem = null;

function _DnD_onMouseDown()
{
	if (event.button != 1)
		return;
	this.bMouseDown = true;
	
	_DnD_dragelem = document.createElement('SPAN');
	_DnD_dragelem.style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity=50)";
	_DnD_dragelem.potentialReceiver = null;
	_DnD_dragelem.origElem = this;
	_DnD_dragelem.style.position = 'absolute';
	if (this.dragContentElem)
		_DnD_dragelem.innerHTML = this.dragContentElem.outerHTML;
	else
		_DnD_dragelem.innerHTML = this.outerHTML;
	document.body.appendChild(_DnD_dragelem);
	_DnD_dragelem.style.left = Math.floor((event.clientX+document.body.scrollLeft)-(_DnD_dragelem.clientWidth/2))+'px';
	_DnD_dragelem.style.top = Math.floor((event.clientY+document.body.scrollTop)-(_DnD_dragelem.clientHeight/2))+'px';
}

var returningElems = new Array();

var timerID = -1;
var numSteps = 30;

function animateReturn()
{
	var changeCount = 0;
	for (var k = 0; k < returningElems.length; ++k)
	{
		var curElem = returningElems[k];
		if (curElem.curStep < numSteps)
		{
			curElem.curReturnX -= curElem.returnStepX;
			curElem.curReturnY -= curElem.returnStepY;
			curElem.style.left = Math.floor(curElem.curReturnX)+"px";
			curElem.style.top = Math.floor(curElem.curReturnY)+"px";
			curElem.curStep++;
			changeCount++;
			if (curElem.curStep == numSteps)
				document.body.removeChild(curElem);
		}
	}
	if (changeCount == 0)
		returningElems = new Array();
}

function _DnD_onMouseUp()
{
	if (_DnD_dragelem)
	{
		var oldDisplay = _DnD_dragelem.style.display;
		_DnD_dragelem.style.display = 'none';
		var temp = document.elementFromPoint(event.clientX, event.clientY);
		var hiddenDudes = new Array();
		while (true)
		{
			if (!temp.DnDCanReceive && temp.tagName.toLowerCase() != 'body')
			{
				hiddenDudes[hiddenDudes.length] = temp;
				temp.style.visibility = 'hidden';
				temp = document.elementFromPoint(event.clientX, event.clientY);
			}
			else
				break;
		}
		for (var k = 0; k < hiddenDudes.length; ++k)
			hiddenDudes[k].style.visibility = 'visible';
		var receiver = temp;
		if (!receiver.DnDCanReceive)
		{
			_DnD_dragelem.style.display = oldDisplay;
			_DnD_dragelem.returnStepX = (_DnD_dragelem.offsetLeft - _DnD_dragelem.origElem.offsetLeft)/numSteps;
			_DnD_dragelem.returnStepY = (_DnD_dragelem.offsetTop - _DnD_dragelem.origElem.offsetTop)/numSteps;
			_DnD_dragelem.curReturnX = _DnD_dragelem.offsetLeft;
			_DnD_dragelem.curReturnY = _DnD_dragelem.offsetTop;
			_DnD_dragelem.curStep = 0;
			//returningElems[returningElems.length] = _DnD_dragelem;
			document.body.removeChild(_DnD_dragelem);
		}
		else
		{
			if (onDrag && _DnD_dragelem.origElem != receiver)
				onDrag(_DnD_dragelem.origElem, receiver);
			document.body.removeChild(_DnD_dragelem);
		}
		_DnD_dragelem = null;
	}
}

function _DnD_onMouseMove()
{
	if (_DnD_dragelem)
	{
		_DnD_dragelem.style.left = Math.floor((event.clientX+document.body.scrollLeft)-(_DnD_dragelem.clientWidth/2))+'px';
		_DnD_dragelem.style.top = Math.floor((event.clientY+document.body.scrollTop)-(_DnD_dragelem.clientHeight/2))+'px';
		var oldDisplay = _DnD_dragelem.style.display;
		_DnD_dragelem.style.display = 'none';
		var temp = document.elementFromPoint(event.clientX, event.clientY);
		var hiddenDudes = new Array();
		while (true)
		{
			if (!temp.DnDCanReceive && temp.tagName.toLowerCase() != 'body')
			{
				hiddenDudes[hiddenDudes.length] = temp;
				temp.style.visibility = 'hidden';
				temp = document.elementFromPoint(event.clientX, event.clientY);
			}
			else
				break;
		}
		for (var k = 0; k < hiddenDudes.length; ++k)
			hiddenDudes[k].style.visibility = 'visible';
		_DnD_dragelem.potentialReceiver = temp;
		_DnD_dragelem.style.display = oldDisplay;
		if (_DnD_dragelem.oldPotentialReceiver != _DnD_dragelem.potentialReceiver)
		{
			if (onDragLeave && _DnD_dragelem.oldPotentialReceiver && _DnD_dragelem.origElem != _DnD_dragelem.oldPotentialReceiver && _DnD_dragelem.oldPotentialReceiver.DnDCanReceive)
				onDragLeave(_DnD_dragelem.origElem, _DnD_dragelem.oldPotentialReceiver);
			if (onDragEnter && _DnD_dragelem.origElem != _DnD_dragelem.potentialReceiver && _DnD_dragelem.potentialReceiver.DnDCanReceive)
				onDragEnter(_DnD_dragelem.origElem, _DnD_dragelem.potentialReceiver);
		}
		_DnD_dragelem.oldPotentialReceiver = _DnD_dragelem.potentialReceiver;
	}
}

function DnD_init()
{
	document.body.onmousemove = _DnD_onMouseMove;
	document.body.onmouseup = _DnD_onMouseUp;
	timerID = setInterval("animateReturn()", 1);
	if (!window.onDragLeave)
		window.onDragLeave = null;
	if (!window.onDragEnter)
		window.onDragEnter = null;
}

function DnD_shutdown()
{
	document.body.onmousemove = null;
	document.body.onmouseup = null;
	if (timerID != -1)
		clearInterval(timerID);
}

function DnD_enableDrag(elem, b)
{
	if (b)
	{
		elem.onmousedown = _DnD_onMouseDown;
		elem.onmouseup = _DnD_onMouseUp;
		elem.style.cursor = 'pointer';
		elem.onselectstart = new Function ("return false");
	}
	else
	{
		elem.onmousedown = null;
		elem.onmouseup = null;
		elem.style.cursor = 'auto';
		elem.onselectstart = null;
	}
}

function DnD_enableReceive(elem, b)
{
	elem.DnDCanReceive = b;
}
