
g_simpleColorsArray = new Array(
new Array("lightsalmon",	"lightcoral",		"salmon",		"coral",			"mistyrose",			"pink"),
new Array("lavender",		"plum",				"violet",		"orchid",			"magenta",				"lightblue"),
new Array("lightsteelblue",	"lightskyblue",		"deepskyblue",	"paleturquoise",	"aquamarine",			"yellowgreen"),
new Array("gainsboro",		"silver",			"darkgray",		"aqua",				"lightgreen",			"greenyellow"),
new Array("gold",			"orange",			"darkorange",	"peru",				"mediumspringgreen",	"lawngreen"),
new Array("palegoldenrod",	"khaki",			"burlywood",	"tan",				"sandybrown",			"goldenrod"));

g_simpleColorsArray_trans = new Array();
g_temp_width = g_simpleColorsArray[0].length;
g_temp_height = g_simpleColorsArray.length;

for (var k = 0; k < g_temp_width; ++k)
{
	var temp_array = new Array();
	for (var n = 0; n < g_temp_height; ++n)
		temp_array[n] = g_simpleColorsArray[n][k];
	g_simpleColorsArray_trans[g_simpleColorsArray_trans.length] = temp_array;
}