<?php
 /**
 * @package Reports
 */

function PrintTableRow($TableData)
{
	BeginTableRow();
	$count = count($TableData);
	$ind = 0;
	while($ind < $count)
	{
		PrintTableCell("$TableData[$ind]");
		$ind = $ind + 1;
	}
	EndTableRow();
}

function PrintTableCell($CellData)
{
	global $cellfontsize;
	if ($CellData=='') $CellData='&nbsp;';
//	Print("<td><font size=$cellfontsize> $CellData </td>");
	Print("<td");
	if($cellfontsize)
		Print(" style='font-size:$cellfontsize'");
	Print("> $CellData </td>");
}

function PrintTableCellColored($CellData, $Color)
{
	global $cellfontsize;

	Print("<td bgcolor='$Color'");
	if($cellfontsize)
//		Print("<font size=$cellfontsize>  ");
		Print(" style='font-size:$cellfontsize'");
	Print(">");
	Print($CellData);
	Print("  ");
	Print('</td>');
}


function PrintEmptyTableCellEx($extra)
{
	PrintTableCellEx("&nbsp;", $extra);
}

function PrintTableCellEx($CellData, $extra)
{
	global $cellfontsize;

	Print("<td $extra");
	if($cellfontsize)
		Print(" style='font-size:$cellfontsize'");
	Print(">  ");
	Print($CellData);
	Print("  ");
	Print('</td>');
}

function PrintTableSpan($Cols, $CellData)
{
	global $cellfontsize;
	Print("<td colspan=$Cols align='center'");
	if($cellfontsize)
		Print(" style='font-size:$cellfontsize'");
	Print(">$CellData</td>");
}

function PrintTableRowSpan($Rows, $CellData)
{
	Print("<td rowspan=$Rows align='center'>$CellData</td>");
}

function PrintTableRowColSpan($Rows, $Cols, $CellData)
{
	Print("<td rowspan=$Rows colspan=$Cols align='center'>$CellData</td>");
}

function PrintEmptyTableCell()
{
	PrintTableCell("&nbsp;");
}

function PrintEmptyTableRowSpan($Rows)
{
	PrintTableRowSpan($Rows,"&nbsp;");
}

function PrintEmptyTableRowColSpan($Rows,$Cols)
{
	PrintTableRowColSpan($Rows,$Cols,"&nbsp;");
}

function PrintTableDataRow($TableData, $colnames)
{
	BeginTableRow();
	$count = count($colnames);
	$ind = 0;
	while($ind < $count)
	{
		PrintTableCell($TableData[$colnames[$ind]]);
		$ind = $ind + 1;
	}
	EndTableRow();
}

function BeginTable()
{
	print("<table border=2 cellspacing=0 cellpadding=3 bgcolor='ivory'>");
}

function EndTable()
{
	print("</table>");
}

function BeginTableRow()
{
	print("<tr>");
}

function BeginTableRowData($Row)
{
	if ($Row%2)
		print("<tr bgcolor=lightgrey>");
	else
		print("<tr bgcolor='lightblue'>");
}

function EndTableRow()
{
	print("</tr>\n");
}


function GetSalesmanData($PersonID)
{
	$AvgCycleDays = 0;
	$sql = "select * from people where PersonID=$PersonID";

	$selection = mssql_query($sql);
	if($selection)
	{
		if($row = mssql_fetch_assoc($selection))
		{
			$Name = $row['FirstName'];
			$Name .= " ";
			$Name .= $row['LastName'];
			return $Name;
		}
	}

	return "";
}


?>