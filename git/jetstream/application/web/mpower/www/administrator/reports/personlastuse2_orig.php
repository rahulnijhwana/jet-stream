<?php
 /**
 * @package Reports
 */
include('../reports/_report_utils.php');
include('../reports/TableUtils.php');
include('../reports/path.php');
include('../reports/ci_pdf_utils.php');
include('../reports/GetSalespeopleTree.php');

define(SHADE_MED, "MED_BLUE");
define(SHADE_LIGHT, "LIGHT_BLUE");

if(isset($CompanyID))
	$sql = "select Name, NonUseGracePeriod from Company where companyid=$CompanyID";
else
	$sql = "select Name, NonUseGracePeriod from Company where companyid=$mpower_companyid";
$ok = mssql_query($sql);
$row=mssql_fetch_assoc($ok);

$GracePeriod = $row['NonUseGracePeriod'];
$CompanyName = $row['Name'];

$tsGraceDate = time() - ($GracePeriod * 24 * 60 * 60);

function striptime($t)
{
        // Strip out the time-of-day in a UNIX timestamp, that is,
        // set the time to midnight of that timestamp's date.
        $tmp = getdate($t);
        return mktime(0, 0, 0, $tmp['mon'], $tmp['mday'], $tmp['year']);
}

function daydiff($new, $old)
{
        // Compute the difference in days between two timestamps,
        // ignoring the time-of-day.
        return round((striptime($new) - striptime($old)) / (60 * 60 * 24));
}

function GetPrevMonth()
{
	$m = date("m");

	$y = date("Y");
	if($m == 1)
	{
		$m=12;
		$y--;
	}

	else
		$m--;
	return getdate(mktime(0, 0, 0, $m, 1,  $y));

}

function IsPastGrace($ldate)
{
	global $GracePeriod;
	$y = date("Y");
	$m = date("m");
	$d = date("d");
	$d -= $GracePeriod;
	$gpd = mktime(0, 0, 0, $m, $d,  $y);
	return $ldate < $gpd;

}

function DaysPastGrace($ldate)
{
	global $GracePeriod;
	if(!$ldate) return "NEVER";

	$y = date("Y");
	$m = date("m");
	$d = date("d");
	$d -= $GracePeriod;
	$gpd = mktime(0, 0, 0, $m, $d,  $y);
	return (($ldate - $gpd) / (60 * 60 * 24));

}

/*
function GetResultsArray($sql)
{
	$result = mssql_query($sql);
	$pm = GetPrevMonth();
	$comparray = array();

	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$cm = getdate();
			$t = strtotime($row['LastLogin']);
			$today = time();
			$DaysPastGrace = DaysPastGrace($t);
			$d = getdate($t);
			$strDate = $d["mon"] . "/" . $d["mday"] . "/" . $d["year"];
			$comp = array();
//			$comp['comp'] = $row['Comp'];
			$comp['Person'] = $row['Person'];
			$comp['LastLogin'] = $strDate;
			$comp['SupervisorID'] = $row['SupervisorID'];
			$comp['PersonID'] = $row['PersonID'];
			$comp['Level'] = $row['PersonLevel'];
			$comp['DaysSinceLast'] = round(($today - $t) / (60 * 60 * 24));
			array_push($comparray, $comp);
		}
	}
$dbg = count($comparray);
//print "count: $dbg<br>";
	return $comparray;
}
*/


function GetResultsArray($sql)
{
	$aliases=false;
	if (func_num_args() >1)
	{
		$args = func_get_args();
		if ($args[1] == true)
			$aliases = true;
	}

	$result = mssql_query($sql);
	$pm = GetPrevMonth();
	$comparray = array();

	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
//print "adding<br>";
			$cm = getdate();
			$t = strtotime($row['LastLogin']);
			$today = time();
			$DaysPastGrace = DaysPastGrace($t);
			$d = getdate($t);
			$strDate = $d["mon"] . "/" . $d["mday"] . "/" . $d["year"];
			$comp = array();
			if($aliases == true)
				$comp['Alias'] = $row['Alias'];

			$comp['Person'] = $row['Person'];
			$comp['LastLogin'] = $strDate;
			$comp['SupervisorID'] = $row['SupervisorID'];
			$comp['PersonID'] = $row['PersonID'];
			$comp['Level'] = $row['PersonLevel'];
			$comp['IsSalesperson'] = $row['IsSalesperson'];
			// $comp['DaysSinceLast'] = round(($today - $t) / (60 * 60 * 24));
			// RJP 9-15-05: strip out time-of-day in our calculation, so that
			// we don't get the situation of two people with the same last login date
			// getting different numbers of days because more than 24 hours passed.
			$comp['DaysSinceLast'] = daydiff($today, $t);
			array_push($comparray, $comp);
		}
	}
//$dbg = count($comparray);
//print "count: $dbg<br>";
	return $comparray;
}


function GetCompanyNonLogins($ID, &$arrResults, $isCompany)
{
	$sql = 	"SELECT PersonID, people.LastName + ', ' + people.FirstName AS Person, [level] AS PersonLevel,";
	$sql .= " SupervisorID FROM People";
	if($isCompany)
	{
		$sql .= " WHERE (people.CompanyID=$ID) and (people.Deleted = 0) and (NOT (people.SupervisorID = - 1)) and (NOT (SupervisorID = - 3)) and People.PersonID NOT IN";
		$sql .= " (SELECT PersonID from log where CompanyID = $ID and TransactionID = 300)";
	}
	else
	{
		$inclause = GetSalespeopleTree($ID, 1);
		$sql .= " WHERE (people.PersonID in ($inclause) and (people.Deleted = 0) and (NOT (people.SupervisorID = - 1)) and (NOT (SupervisorID = - 3)) and People.PersonID NOT IN";
		$sql .= " (SELECT PersonID from log where PersonID in ($inclause) and TransactionID = 300))";
	}

	$sql .= " ORDER BY PersonLevel, Person";
	$arr = array();
	$ok = mssql_query($sql);
$i=0;
//print $sql . '<br>';
	if($ok)
	{
		while($row = mssql_fetch_assoc($ok))
		{
			$cm = getdate();
			$today = time();
			$tmp = array();
			$tmp['Person'] = $row['Person'];
			$tmp['LastLogin'] = 'NEVER';
			$tmp['SupervisorID'] = $row['SupervisorID'];
			$tmp['PersonID'] = $row['PersonID'];
			$tmp['Level'] = $row['PersonLevel'];
			$tmp['DaysSinceLast'] = -1;

			array_push($arrResults, $tmp);
	$i++;
		}
	}
}

function GetPersonnelCounts($CompanyID)
{
//	$sql = "SELECT company.Name as Comp, people.LastName AS Person, MAX(log.WhenChanged) AS LastLogin ";
	$sql = "SELECT company.Name as Comp, people.LastName + ', ' + people.FirstName AS Person,";
	$sql .= " MAX(log.WhenChanged) AS LastLogin, SupervisorID, people.PersonID, people.[level] as PersonLevel";
	$sql .= " FROM company INNER JOIN";
	$sql .= " log ON company.CompanyID = log.CompanyID RIGHT JOIN";
	$sql .= " people ON log.PersonID = people.PersonID";
//	$sql .= " WHERE (company.CompanyID = $CompanyID) and (people.Deleted = 0) and (NOT (SupervisorID = - 1)) and (NOT (SupervisorID = - 3)) and (log.TransactionID = 300) and ((company.Demo=0) or (company.Demo IS NULL))";
	$sql .= " WHERE (company.CompanyID = $CompanyID) and (people.Deleted = 0) and (NOT (SupervisorID = - 1)) and (NOT (SupervisorID = - 3)) and (log.TransactionID = 300)";

	$sql .= " GROUP BY company.Name, people.LastName, people.FirstName, people.SupervisorID, people.PersonID, people.[Level]";
	$sql .= " ORDER BY company.Name, people.LastName, people.FirstName";

	return GetResultsArray($sql);
}

function GetUserPersonnelCounts($treeid)
{
	$inclause = GetSalespeopleTree($treeid, 1);

	$sql = "SELECT people.IsSalesperson, company.Name as Comp, people.LastName + ', ' + people.FirstName AS Person,";
	$sql .= " MAX(log.WhenChanged) AS LastLogin, SupervisorID, people.PersonID, people.[level] as PersonLevel  ";
	$sql .= " FROM company INNER JOIN log";
	$sql .= " ON company.CompanyID = log.CompanyID";
	$sql .= " RIGHT JOIN people ON log.PersonID = people.PersonID";
	$sql .= " WHERE (people.PersonID in ($inclause)) and (company.Disabled=0) and (people.Deleted = 0) and (log.TransactionID = 300)";
	$sql .= " GROUP BY company.Name, people.LastName, people.FirstName, people.SupervisorID, people.PersonID, people.[Level], people.IsSalesperson ";
	$sql .= " ORDER BY company.Name, people.LastName, people.FirstName";

//print "TREEID=$treeid<br>";
//print "$sql<br>";

	return GetResultsArray($sql);

}


//11/16/2005: include ALL aliases in both versions of report
function GetAliasCounts($ID, $isCompany)
{
	//NB in calls from the ASA utilities, $mpower_companyid is NOT defined
	global $mpower_companyid;

	$sql = "SELECT people.LastName + ', ' + people.FirstName AS Person, Logins.Name as Alias, people.[level] as PersonLevel, people.SupervisorID, Logins.ID, MAX([log].WhenChanged) AS LastLogin, Logins.PersonID, Logins.LoginID, Logins.PersonID";
	$sql .=" FROM Logins INNER JOIN people ON Logins.PersonID = people.PersonID";
	$sql .=" RIGHT JOIN [log] ON Logins.ID = [log].LogInt";

	if($isCompany)
		$sql .=" WHERE ([log].TransactionID = 300) and (people.Deleted = 0) and people.CompanyID = $ID";
	else
		$sql .=" WHERE ([log].TransactionID = 300) and (people.Deleted = 0) and people.CompanyID = $mpower_companyid";

	$sql .=" GROUP BY logins.name, people.LastName, people.FirstName, people.[level], people.SupervisorID, Logins.ID, Logins.PersonID, Logins.LoginID";
	$sql .=" ORDER BY Person";

	//print "$sql<br>";

	return GetResultsArray($sql, true);
}

//11/16/2005: include ALL aliases in both versions of report
function GetAliasNonLogins($ID, &$arrResults, $isCompany)
{
global $mpower_companyid;

	$sql = "SELECT people.LastName + ', ' + people.FirstName AS Person, Logins.Name as Alias, people.[level] as PersonLevel, people.SupervisorID, Logins.ID, Logins.PersonID, Logins.LoginID, Logins.PersonID";
	$sql .=" FROM Logins INNER JOIN people ON Logins.PersonID = people.PersonID";

	if($isCompany)
	{
		$sql .= " WHERE (people.CompanyID=$ID) and ((NOT(Logins.PersonID = Logins.LoginID)) or (Logins.LoginID is null)) and (people.Deleted = 0) and (NOT (people.SupervisorID = - 1)) and (NOT (SupervisorID = - 3)) and People.PersonID NOT IN";
		$sql .= " (SELECT PersonID from log where CompanyID = $ID and TransactionID = 300 and not(logint is null))";
	}
	else
	{
		$sql .= " WHERE (people.CompanyID=$mpower_companyid) and ((NOT(Logins.PersonID = Logins.LoginID)) or (Logins.LoginID is null)) and (people.Deleted = 0) and (NOT (people.SupervisorID = - 1)) and (NOT (SupervisorID = - 3)) and Logins.ID NOT IN";
		$sql .= " (SELECT LogInt from log where CompanyID = $mpower_companyid and TransactionID = 300 and not(logint is null))";

//		$inclause = GetSalespeopleTree($ID, 1);
//		$sql .= " WHERE (people.PersonID in ($inclause) and ((NOT(Logins.PersonID = Logins.LoginID)) or (Logins.LoginID is null)) and(people.Deleted = 0) and (NOT (people.SupervisorID = - 1)) and (NOT (people.SupervisorID = - 3)) and People.PersonID NOT IN";
//		$sql .= " (SELECT PersonID from log where PersonID in ($inclause) and TransactionID = 300 and NOT(logint is null)))";
	}


//	$sql .= " (SELECT PersonID from log where CompanyID = $mpower_companyid and TransactionID = 300 and not(logint is null))";

//	$sql .=" GROUP BY logins.name, people.LastName, people.FirstName, people.[level], people.SupervisorID, Logins.ID, Logins.PersonID, Logins.LoginID";
	$sql .= " ORDER BY Person";

	$ok = mssql_query($sql);
$i=0;
//print 'NON LOGINS:<br>' . $sql . '<br>';
	if($ok)
	{
		while($row = mssql_fetch_assoc($ok))
		{
			$cm = getdate();
			$today = time();
			$tmp = array();
			$tmp['Person'] = $row['Person'];
			$tmp['Alias'] = $row['Alias'];
			$tmp['LastLogin'] = 'NEVER';
			$tmp['SupervisorID'] = $row['SupervisorID'];
			$tmp['PersonID'] = $row['PersonID'];
			$tmp['Level'] = $row['PersonLevel'];
			$tmp['DaysSinceLast'] = -1;

			array_push($arrResults, $tmp);
	$i++;
		}
	}
}


$hilight;
function PrintPersonRec(&$report, $comp, $prefix)
{
	$aliases=0;
	if (func_num_args() > 3)
	{
		$args = func_get_args();
		if ($args[3] == true)
			$aliases = true;
	}
	global $GracePeriod;
	global $hilight;
	if($hilight == 1) $hilight = 0;
	else $hilight = 1;

	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
	if($hilight)
		SetRowColorStandard($report, -1, SHADE_LIGHT);
	if(!$aliases)
	{
		$dispLevels= array();
		$level = $comp['Level'];
		$dispLevels[$level - 1] = $level;
		for($i=5; $i> -1; $i--)
		{
			AddColumnText($report, $dispLevels[$i], "", 0, 0);
		}

		$l_prefix = '';
		for($i=6; $i > $level; $i--)
				$l_prefix .= '     ';

		AddColumnText($report, $l_prefix . $comp['Person'], "", 0, 0);

	}
	else
	{
		AddColumnText($report, $comp['Person'], "", 0, 0);
		AddColumnText($report, $comp['Alias'], "", 0, 0);
	}
	$DaysOld = $comp['DaysSinceLast'];
	if($DaysOld == -1)
	{
		$DaysOld = "NEVER";
		$LastLogin = '';
	}
	else
		$LastLogin = date("m/d/y", strtotime($comp['LastLogin']));
	$pad = '        ';
	AddColumnText($report, $DaysOld . $pad, "right", 0, 0);

	if ($DaysOld <= $GracePeriod)
		AddColumnText($report, $LastLogin, "center", 0, 0);
	else
		AddColumnText($report, " ", "center", 0, 0);

	if ($DaysOld > $GracePeriod && $DaysOld <=30)
		AddColumnText($report, $LastLogin, "center", 0, 0);
	else
		AddColumnText($report, " ", "center", 0, 0);
	if ($DaysOld > 30 && $DaysOld <=60)
		AddColumnText($report, $LastLogin, "center", 0, 0);
	else
		AddColumnText($report, " ", "center", 0, 0);
	if ($DaysOld > 60)
		AddColumnText($report, $LastLogin, "center", 0, 0);
	else
		AddColumnText($report, " ", "center", 0, 0);
}


function GetTotals($arr, &$totPeople, &$totGP, &$tot30, &$tot60, &$totOver, &$Never)
{
	global $GracePeriod;
	$totPeople=0;
	$totGP=0;
	$tot30=0;
	$tot60=0;
	$totOver=0;
	$Never=0;
	for($i = 0; $i < count($arr); $i++)
	{
		$totPeople++;
		$DaysOld = $arr[$i]['DaysSinceLast'];
		if($DaysOld == -1) $Never++;
		else if($DaysOld <= $GracePeriod) $totGP++;
		else if($DaysOld > $GracePeriod && $DaysOld <= 30)
			$tot30++;
		else if($DaysOld > 30 && $DaysOld <= 60)
			$tot60++;
		else if($DaysOld > 60)
			$totOver++;

	}
}


function PrintTree_cur(&$report, $SupervisorID, $arrRecs, $level)
{

	$level++;
	for($i=0; $i < $level; $i++)
		$prefix .= '     ';

	for ($i=0; $i < count($arrRecs); $i++)
	{
//print " SuperID=$SupervisorID ThisRecSuperID=" . $arrRecs[$i]['SupervisorID'] . "<br>";
		if($arrRecs[$i]['SupervisorID'] == $SupervisorID || ($arrRecs[$i]['PersonID']==$SupervisorID && $arrRecs[$i]['IsSalesperson']=='1'))
//print('IsSalesperson '.$arrRecs[$i]['IsSalesperson']);
//		if($arrRecs[$i]['SupervisorID'] == $SupervisorID || ($arrRecs[$i]['IsSalesperson']==1))
		{

			PrintPersonRec($report, $arrRecs[$i], $prefix);
			if($arrRecs[$i]['SupervisorID'] == $SupervisorID)	//  only descend the tree if we aren't printing the manager himself
			{
//print("Descending the tree!  Level=$level<br>");
				PrintTree($report, $arrRecs[$i]['PersonID'], $arrRecs, $level);
			}
		}
	}
}

function PrintTree(&$report, $SupervisorID, $arrRecs, $level)
{
global $treeid;
	$level++;
	for($i=0; $i < $level; $i++)
		$prefix .= '     ';

	for ($i=0; $i < count($arrRecs); $i++)
	{
//Print the top guy, as well as his subordinatets
		if(isset($treeid) && $treeid == $SupervisorID && $treeid == $arrRecs[$i]['PersonID'])
			PrintPersonRec($report, $arrRecs[$i], $prefix);
		if($arrRecs[$i]['SupervisorID'] == $SupervisorID)
		{

			PrintPersonRec($report, $arrRecs[$i], $prefix);
			PrintTree($report, $arrRecs[$i]['PersonID'], $arrRecs, $level);
		}
	}
}


/***********************************************************************************************/
$report = CreateReport();
$report['left'] = 72/4;
$report['right'] = 8.25 * 72;

$p = PDF_new();
PDF_set_parameter($p, "license", PDFLIB_LICENSE_KEY);
PDF_open_file($p, "");


StartPDFReport($report, $p, "A", "B", "C");
SetLogo($report, "report_logo.jpg", 10, 40, 72, 0);

$title = "Last Login Report";

$report['footertext'] = $title;

$left = 1;
$width1 = 1;
$width2 = 3;

//$test = GetPrevMonth();
//print $test["mon"] . '/' . $test["year"] . '<br>';


if(isset($treeid))
	{
	$arrCompanies = GetUserPersonnelCounts($treeid);
	GetCompanyNonLogins($treeid, $arrCompanies, false);
	}
else
	{
	$arrCompanies = GetPersonnelCounts($CompanyID);
	GetCompanyNonLogins($CompanyID, $arrCompanies, true);
	}

$dbg = count($arrCompanies);

GetTotals($arrCompanies, $totPeople, $totGP, $tot30, $tot60, $totOver, $totNever);

$count = count($arrCompanies);
$last = "";

BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, 0.1);
AddFreeFormText($report, $title, -1, "center", 1.5, 5.5);

BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0.25);

if (isset($treeid))
	AddFreeFormText($report, make_tree_path($treeid), -1, "center", 1.5, 5.5);
else
	AddFreeFormText($report, $CompanyName, -1, "center", 1.5, 5.5);

//---------Print Totals
		BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0.2);
		AddColumn($report, "TOTALS", "right", 1.5, 0);
		AddColumn($report, " ", "left", 0.5, 0);

		BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
		AddColumnText($report, "People:", "right", 0, 0);
		AddColumnText($report, $totPeople, "right", 0, 0);

		BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
		AddColumnText($report, "Within $GracePeriod days:", "right", 0, 0);
		AddColumnText($report, $totGP, "right", 0, 0);

		BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
		$GPPlus = $GracePeriod + 1;
		AddColumnText($report, "$GPPlus to 30 days:", "right", 0, 0);
		AddColumnText($report, $tot30, "right", 0, 0);

		BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
		AddColumnText($report, "31 to 60 days:", "right", 0, 0);
		AddColumnText($report, $tot60, "right", 0, 0);

		BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
		AddColumnText($report, "Over 60 days:", "right", 0, 0);
		AddColumnText($report, $totOver, "right", 0, 0);

		BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
		AddColumnText($report, "Never logged in:", "right", 0, 0);
		AddColumnText($report, $totNever, "right", 0, 0);


		BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
		AddColumnText($report, ' ', "left", 0, 0);

		BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
		AddColumnText($report, ' ', "left", 0, 0);

//------------End Totals

/*
if (isset($treeid))
;
else
	PrintCompanyNonLogins($report, $CompanyID);
*/

BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0.2);
SetColColorStandard($report, -1, -1, SHADE_MED);
//AddColumn($report, "Level", "left", 0.5, 0);
AddColumn($report, "", "left", 0.1, 0);
AddColumn($report, "", "left", 0.1, 0);
AddColumn($report, "", "left", 0.1, 0);
AddColumn($report, "", "left", 0.1, 0);
AddColumn($report, "", "left", 0.1, 0);
AddColumn($report, "", "left", 0.1, 0);

AddColumn($report, "People", "left", 2.5, 0);
AddColumn($report, "Days Since Last Login", "center", 0.8, 0);
AddColumn($report, "Within Grace Period\n($GracePeriod Days)", "center", 0.8, 0);
$postgrace = $GracePeriod + 1;
AddColumn($report, "$postgrace to 30 Days", "center", 0.8, 0);
AddColumn($report, "31 to 60 Days", "center", 0.8, 0);
AddColumn($report, "Over 60 Days", "center", 0.80, 0);

//SetColColorStandard($report, -1, -1, "MED_RED"/*, true*/);

sort($arrCompanies);

if (isset($treeid))
	PrintTree($report, $treeid, $arrCompanies, -1);
else
	PrintTree($report, -2, $arrCompanies, -1);

//Aliases
$break=1;
BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, 0.1, $break);

//BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0.25);
AddFreeFormText($report, "ALIASES", -1, "center", 1.5, 5.5);

BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0.2);
SetColColorStandard($report, -1, -1, SHADE_MED);
AddColumn($report, "Manager", "center", 2.0, 0);
AddColumn($report, "Alias", "center", 1.1, 0);
AddColumn($report, "Days Since Last Login", "center", 0.8, 0);
AddColumn($report, "Within Grace Period\n($GracePeriod Days)", "center", 0.8, 0);

AddColumn($report, "$postgrace to 30 Days", "center", 0.8, 0);
AddColumn($report, "31 to 60 Days", "center", 0.8, 0);
AddColumn($report, "Over 60 Days", "center", 0.80, 0);

if(isset($treeid))
{
	$arrAliases = GetAliasCounts($treeid, false);
	GetAliasNonLogins($treeid, $arrAliases, false);
}
else
{
	$arrAliases = GetAliasCounts($CompanyID, true);
	GetAliasNonLogins($CompanyID, $arrAliases, true);
}
$dbg = count($arrAliases);



$ok=sort($arrAliases);


for ($i=0; $i < count($ok); $i++)
{
	PrintPersonRec($report, $ok[$i], "", true);
}

PrintPDFReport($report, $p);

PDF_delete($p);



close_db();
?>
