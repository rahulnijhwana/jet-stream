<?php
 /**
 * @package Reports
 */
include('_report_utils.php');
include('TableUtils.php');
include('category_labels.php');

//include('firstmeeting_utils.php');
include('GetSalespeopleTree.php');

begin_report(GetCatLbl('Stalled') . ' Opportunities Report');

if (!isset($treeid))
{
	print_tree_prompt(MANAGEMENT);
	end_report();
	exit();
}

$compsql = "select ValuationUsed as vOK, SourceUsed as sOK from Company where CompanyID=$mpower_companyid";
$ok=mssql_query($compsql);
$comprow = mssql_fetch_assoc($ok);

$colcount=5;
if($comprow['vOK']) $colcount++;
if($comprow['sOK']) $colcount++;

print_tree_path($treeid);

$strdate = date("m/d/y", time());
print("<br><b>Date run:   $strdate</b><br><br>");


function GetProducts(&$Prods, $id)
{
	$sql = "select * from products where companyid = $id";
	$ok = mssql_query($sql);
	if($ok)
	{
		while($row = mssql_fetch_assoc($ok))
		{
			$id=$row['ProductID'];
			$Prods[$id] = $row['Name'];
		}
	}
}

function GetSources(&$Sources, $id)
{
	$sql = "select * from sources where companyid = $id";
	$ok = mssql_query($sql);
	if($ok)
	{
		while($row = mssql_fetch_assoc($ok))
		{
			$id=$row['SourceID'];
			$Sources[$id] = $row['Name'];
		}
	}
}



function GetStalledOpps(&$opps, $supervisorid, $prods, $sources)
{
	$inlist = GetSalespeopleTree($supervisorid);
	$sql = "SELECT * FROM  opportunities INNER JOIN people ON opportunities.PersonID = people.PersonID";
	$sql .= " WHERE (people.PersonID in ($inlist)) AND (people.Deleted = 0)";
	$sql .= " AND (opportunities.Category = 3 OR opportunities.Category = 5) order by people.LastName, people.FirstName, opportunities.Category, opportunities.Company";

	$ok=mssql_query($sql);
	if($ok)
	{
		while($row = mssql_fetch_assoc($ok))
		{
			$temp = array();
			$DealID = $row['DealID'];

			$temp['name'] = $row['LastName'] . ", " . $row['FirstName'];
			$temp['category'] = $row['Category'] == 3 ? GetCatLbl('Stalled').' '.GetCatLbl('IP') : GetCatLbl('Stalled').' '.GetCatLbl('DP');
			$temp['lastmoved'] = Date("m/d/y", strtotime($row['LastMoved']));
			$temp['daysold'] = CalcDays($row['LastMoved']);
			$temp['firstmeeting'] = date("m/d/y", strtotime($row['FirstMeeting']));
			$temp['company'] = $row['Company'];
			$temp['division'] = $row['Division'];

			$OppProducts = GetOppProducts($DealID);
			if(strlen($OppProducts) > 0) $temp['prodname'] = $OppProducts;
			else $temp['prodname'] = $prods[$row['ProductID']];

			if(is_null($temp['prodname'])) $temp['prodname'] = 'Unk.';
			$temp['sourcename'] = $sources[$row['SourceID']];
			if(is_null($temp['sourcename'])) $temp['sourcename'] = 'Unk.';
			$temp['vlevel'] = $row['VLevel'] > 0 ? $row['VLevel'] : 'Unk';
			array_push($opps, $temp);
		}
	}
}

function datediff($interval, $date1, $date2)
{
 $s = strtotime($date2)-strtotime($date1);
 $d = intval($s/86400);
 $s -= $d*86400;
 $h = intval($s/3600);
 $s -= $h*3600;
 $m = intval($s/60);
 $s -= $m*60;
 $arrRes=array("d"=>$d,"h"=>$h,"m"=>$m,"s"=>$s);
 return $arrRes[$interval];
}

function CalcDays($dateLastMoved)
{
	if(!strlen($dateLastMoved)) return 0;
	$now = Date("m/d/y");
	$then = Date("m/d/y", strtotime($dateLastMoved));
	return datediff("d", $then, $now);
}


function PrintColumnHeaders()
{
	global $comprow;
	BeginTableRowData(1);
	PrintTableCell("<b>Company");
	PrintTableCell("<b>Offering");
	if($comprow['sOK']) PrintTableCell("<b>Source");
	PrintTableCell("<b>" . GetCatLbl('First Meeting'));
	if($comprow['vOK']) PrintTableCell("<b>Val Level");
	PrintTableCell("<b>Entered " . GetCatLbl('Stalled'));
	PrintTableCell("<b>Days Old");
	EndTableRow();
}

function PrintBlankRow()
{
	global $colcount;
	BeginTableRow();
	PrintTableSpan($colcount,"<br>");
	EndTableRow();
}


?>



<?php
$personid = $treeid;

$companyid = $mpower_companyid;

$cellfontsize="16px";

BeginTable();

BeginTableRowData(1);
PrintTableSpan($colcount, "<b>" . GetCatLbl('Stalled') . " Report");
EndTableRow();


$Prods = array();
GetProducts($Prods, $companyid);
$Sources = array();
GetSources($Sources, $companyid);
$Opps=array();
GetStalledOpps($Opps, $personid, $Prods, $Sources);
$count = count($Opps);
$totals=array();
$lastSP='';
$lastCat='';
$catcount=0;
$totaldays=0;

$i=0;
while(1)
{
	if($i == $count) break;
	$rec=$Opps[$i++];

	if(strlen($lastSP) &&($i == $count || $lastSP != $rec['name'] || $lastCat != $rec['category']))
	{
		$tmp=array();
		$tmp['name']=$lastSP;
		$tmp['category'] = $lastCat;
		$tmp['totaldays'] = $totaldays;
		$tmp['catcount'] = $catcount;
		array_push($totals, $tmp);
		$totaldays=0;
		$catcount=0;
	}
	$lastSP = $rec['name'];
	$lastCat = $rec['category'];
	$totaldays += $rec['daysold'];
	$catcount++;
}


$indSP=0;
for($i=0; $i<$count; $i++)
{
	$rec = $Opps[$i];
	if($rec['name'] != $lastSP || $rec['category'] != $lastCat)
	{
		$name = $totals[$indSP]['name'];
		$cat = $totals[$indSP]['category'];
		$catcount = $totals[$indSP]['catcount'];
		$totaldays = $totals[$indSP]['totaldays'];
		$avg=0;
		if($catcount > 0) $avg = round($totaldays / $catcount);
		if($rec['name'] != $lastSP )
		{
			PrintBlankRow();
			BeginTableRowData(1);
			PrintTableSpan($colcount,"$name");
			EndTableRow();
		}
		BeginTableRowData(1);
		PrintTableSpan($colcount, "$cat: $catcount Opportunities Average $avg Days Old");
		EndTableRow();
		$indSP++;
		$lastSP=$rec['name'];
		$lastCat = $rec['category'];
		PrintColumnHeaders();
	}
	BeginTableRow();

	$tempcompany = $rec['company'];
	if ($rec['division'] != '')
		$tempcompany .= '&nbsp;-&nbsp;'.$rec['division'];
	PrintTableCell($tempcompany);
	PrintTableCell($rec['prodname']);
	if($comprow['sOK'])
		PrintTableCell($rec['sourcename']);
	PrintTableCell($rec['firstmeeting']);
	if($comprow['vOK'])
		PrintTableCell($rec['vlevel']);
	PrintTableCell($rec['lastmoved']);
	PrintTableCell($rec['daysold']);
	EndTableRow();

}


EndTable();


end_report();
?>
