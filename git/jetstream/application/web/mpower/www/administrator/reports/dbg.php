<?php
 /**
 * @package Reports
 */
include_once('settings.php');
include_once('log_obj.php');

 /**
 * A legacy logger utility for reports.  Now just calls the LogFile class.
 */
function dbg($msg)
{
    LogFile::WriteLine($msg);
}
?>
