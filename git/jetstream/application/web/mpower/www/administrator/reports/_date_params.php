<?php
 /**
 * @package Reports
 */
function print_date_params($personid, $sortcol, $ord)
{
$today=date("m/d/y");
$mt_lastmonth = mktime(0, 0, 0, date("m")-1, date("d"),  date("Y"));
$lastmonth = date("m/d/y", $mt_lastmonth);

//Extra arguments constrain dates to and from
$conditionDateTo="";
$conditionDateFrom="";
$argcount=func_num_args();
$args = func_get_args();
for($i=3; $i<$argcount; $i++)
{
	$cond = explode(':', $args[$i]);
	switch($cond[0])
	{
		case "dateto":
			$conditionDateTo = $cond[1];
			break;
		case "datefr":
			$conditionDateFrom = $cond[1];
			break;
	}
}
//Adjust initial vals of from and to
if(strlen($conditionDateTo)>0)
{
	$oper = substr($conditionDateTo, 0, 1);
	$strParam=substr($conditionDateTo, 1);
	$dateParam = strtotime($strParam);
	$ts = mktime();
	switch($oper)
	{
		case "<":
			if($ts >= $dateParam)
			{
				$ts = $dateParam - (24*60*60);
			}
			break;
//not relevant
		case "<":
			break;
	}
	$today=date("m/d/y", $ts);
	$mt_lastmonth = mktime(0, 0, 0, date("m", $ts)-1, date("d", $ts),  date("Y", $ts));
	$lastmonth = date("m/d/y", $mt_lastmonth);
}




?>
<script language="JavaScript" src="../javascript/utils.js"></script>
<form onsubmit="onSubmit1(this); return false;">
<center>
FROM: <input id="dateFrom" readonly="true" name="dateFrom" type="text" onchange="onEditDateFrom();" size="10" value="<?php print($lastmonth);?>" ><button type="button"  class="ButtonWithGradient"value="Change..." style="height:25px;" onclick="onClickChangeDateFrom();">Change...</button><br>
TO: <input id="dateTo" readonly="true" name="dateTo" type="text" onchange="onEditDateTo();" size="10" value="<?php print($today);?>" ><button type="button"  class="ButtonWithGradient"value="Change..." style="height:25px;" onclick="onClickChangeDateTo();">Change...</button><br><br>
<input type="submit" class="ButtonWithGradient"value="Continue" style="height:25px;" ></input><br>
<input type="hidden" name="treeid" value="<?php print($personid);?>" ></input><br>
<input type="hidden" name="sortcol" value="<?php print($sortcol);?>" ></input><br>
<input type="hidden" name="ord" value="<?php print($ord);?>" ></input><br>
<input type="hidden" id="cond_fr" value="<?php print($conditionDateFrom);?>" ></input><br>
<input type="hidden" id="cond_to" value="<?php print($conditionDateTo);?>" ></input><br>
</center>
</form>
<script language=Javascript>

			function onSubmit1(daform)
			{
				if (!check_dateFrom() || !check_dateTo())
					return;
				daform.submit();
			}

			function check_dateFrom()
			{
				var dateFrom = document.getElementById("dateFrom");
				if (!dateFrom)
					return true;
				var val = dateFrom.value;
				var dateTo = document.getElementById("dateTo");
				if (!dateTo)
					return true;
				var valTo = dateTo.value;
				if (val != '' && valTo != '')
				{
					val = Dates.normalize(val);
					if (!val)
					{
						alert('Invalid date format for From Date.\nMust be either MM/DD/YY or MM/DD/YYYY');
						return false;
					}
					var valDate = null;
					var valToDate = null;
					if (!Dates.makeDateObj)
					{
						valDate = new Date(val);
						valToDate = new Date(valTo);
					}
					else
					{
						valDate = Dates.makeDateObj(val);
						valToDate = Dates.makeDateObj(valTo);
					}
					if (valToDate.getTime() < valDate.getTime())
					{
						alert('The To date cannot be before the From date');
						return false;
					}
				}
				return true;
			}

			function check_dateTo()
			{
				var dateTo = document.getElementById("dateTo");
				if (!dateTo)
					return true;
				var val = dateTo.value;
				if (val != '')
				{
					val = Dates.normalize(val);
					if (!val)
					{
						alert('Invalid date format for To Date.\nMust be either MM/DD/YY or MM/DD/YYYY');
						return false;
					}
				}
				return true;
			}



			function onEditDateFrom()
			{
				//if (!check_dateFrom())
				//	document.getElementById("dateFrom").value = '';
			}

			function onEditDateTo()
			{
				//if (!check_dateTo())
				//	document.getElementById("dateTo").value = '';

			}


			function onClickChangeDateFrom()
			{
				var cond = "#" + document.getElementById("cond_fr").value;

				var calwin = Windowing.openSizedPrompt("../shared/calendar_rpt_param.html" + cond, 320, 600);

				Windowing.dropBox.calendarTarget = document.getElementById("dateFrom");
				Windowing.dropBox.onCalendarEdited = onEditDateFrom();
			}

			function onClickChangeDateTo()
			{
				var cond = "#" + document.getElementById("cond_to").value;
				var calwin = Windowing.openSizedPrompt("../shared/calendar_rpt_param.html" + cond, 320, 600);
				Windowing.dropBox.calendarTarget = document.getElementById("dateTo");
				Windowing.dropBox.onCalendarEdited = onEditDateTo();
			}

</script>
<?php
}
?>