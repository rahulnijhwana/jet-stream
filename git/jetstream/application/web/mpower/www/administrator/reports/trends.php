<?php
 /**
 * @package Reports
 */
include('category_labels.php');

function PrintTableRow($TableData)
{
	BeginTableRow();
	$count = count($TableData);
	$ind = 0;
	while($ind < $count)
	{
		PrintTableCell("$TableData[$ind]");
		$ind = $ind + 1;
	}
	EndTableRow();
}

function PrintTableCell($CellData)
{
	Print('<td>');
	Print("  ");
	Print($CellData);
	Print("  ");
	Print('</td>');
}

function PrintTableDataRow($TableData, $colnames)
{
	BeginTableRow();
	$count = count($colnames);
	$ind = 0;
	while($ind < $count)
	{
		PrintTableCell($TableData[$colnames[$ind]]);
		$ind = $ind + 1;
	}
	EndTableRow();
}

function BeginTable()
{
	print("<table border=2 cellspacing=0 cellpadding=10>");
}

function EndTable()
{
	print("</table>");
}

function BeginTableRow()
{
	print("<tr>");
}

function EndTableRow()
{
	print("</tr>");
}

function IsTrendCurrent($SalesId, $TrendId)
{
	$sql = "select * from log where PersonID=$SalesId and TransactionID=$TrendId order by WhenChanged desc";
	$selection = mssql_query($sql);
	if($selection)
	{
		if($row = mssql_fetch_assoc($selection))
		{
			$Current = $row['LogBit'];
			$WhenChanged = strtotime($row['WhenChanged']);
			if($Current)
			{
				return $WhenChanged;
			}
		}
	}
	return null;
}

function GetRegionCurrentTrendSummary($SalesIds, $TrendId, &$CountInTrend, &$AvgDays)
{
	$CountInTrend=0;
	$totdays=0;
	$count = count($SalesIds);
	$timenow = time();
	for($i=0; $i<$count; $i++)
	{
		if($starttime=IsTrendCurrent($SalesIds[$i], $TrendId))
		{
			$totdays += GetElapsedDays($starttime, $timenow);
			$CountInTrend++;
		}
	}

	if($CountInTrend > 0)
		$AvgDays = (int)($totdays/$CountInTrend);
}

function GetSupervisors($companyid, $level, $supervisorid, &$resultarray)
{
	$resultarray = array();

	$sql="";
	if($supervisorid < 0) $sql = "select * from people where CompanyID=$companyid and Level=$level";
	else $sql = "select * from people where CompanyID=$companyid and Level=$level and SupervisorID=$supervisorid";
//print("SQL=$sql<br>");
	$result = mssql_query($sql);
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$temp = array();
			$temp['level'] = $row['Level'];
			$temp['groupname'] = $row['GroupName'];
			$temp['superid'] = $row['PersonID'];
			array_push($resultarray, $temp);
		}
	}
}

function GetSalespeople(&$SalesNames, &$SalesIds, $CompanyID, $SupervisorID)
{
	$sql = "select * from people where Deleted=0 and IsSalesperson=1 and SupervisorID";

	if(is_array($SupervisorID))
	{
		$clause = " in(";
		$count = count($SupervisorID);
		for($i=0; $i<$count; $i++)
		{
			$super = $SupervisorID[$i];
			$id = $super['superid'];
			if($i > 0) $clause .= ',';
			$clause .= $id;
		}
		$clause .= ')';
		$sql .= $clause;
	}
	else
	{
		$sql .= "=$SupervisorID";
	}

	$sql .= " order by LastName, FirstName";

	$result = mssql_query($sql);
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$Name = $row['LastName'];
			$Name .= ', ';
			$Name .= $row['FirstName'];

			$id=$row['PersonID'];
			array_push($SalesNames, $Name);
			array_push($SalesIds, $id);
		}
	}
}

function GetElapsedDays($StartTime, $EndTime)
{
	$Diff = $EndTime - $StartTime;
	return (int)($Diff / (60 * 60 * 24));
}

function GetTrends(&$TrendNames, &$TrendIds, $companyid)
{

	$req1 = 0;
	$req2 = 0;
	$name1 = "";
	$name2 = "";

	$sql = "select * from company where CompanyID=$companyid";
	$result = mssql_query($sql);
	if($result)
	{
		if($row = mssql_fetch_assoc($result))
		{
			$req1 = $row['Requirement1used'];
			$req2 = $row['Requirement2used'];
			$name1 = $row['Requirement1'];
			$name2 = $row['Requirement2'];
			$MS2Label = $row['MS2Label'];
			$MS3Label = $row['MS3Label'];
			$MS4Label = $row['MS4Label'];
			$MS5Label = $row['MS5Label'];
		}
	}


//	$sql = "select * from trends";
	$sql = "select * from trends_config_categories";


	$result = mssql_query($sql);
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$trendid = $row['TrendID'];
			$name = $row['Name'];
			$name = ScanForLabels($name);
			if($trendid == 9)
			{
				if(strlen($MS2Label) > 0)
					$name = "Missing \"$MS2Label\" Milestone";
			}
			if($trendid == 10)
			{
				if(strlen($MS3Label) > 0)
					$name = "Missing \"$MS3Label\" Milestone";
			}
			if($trendid == 11)
			{
				if(strlen($MS4Label) > 0)
					$name = "Missing \"$MS4Label\" Milestone";
			}
			if($trendid == 12)
			{
				if(strlen($MS5Label) > 0)
					$name = "Missing \"$MS5Label\" Milestone";
			}

			if($trendid == 13)
			{
				if($req1) $name = "Missing \"$name1\" Milestone";
				else $trendid = -1;
			}

			if($trendid == 14)
			{
				if($req2) $name = "Missing \"$name2\" Milestone";
				else $trendid = -1;
			}

			if($trendid > -1)
			{

				array_push($TrendNames, $name);
				array_push($TrendIds, $trendid);

			}
		}
	}
}

function GetSalesmanName($PersonID)
{
	$sql = "select * from people where PersonID=$PersonID";
	$selection = mssql_query($sql);
	if($selection)
	{
		if($row = mssql_fetch_assoc($selection))
		{
			$Name = $row['FirstName'];
			$Name .= " ";
			$Name .= $row['LastName'];
			return $Name;
		}
	}

	return "";
}

function GetTrendHistory($trendid, $salesid, &$resultarray)
{
	$resultarray = array();

	$sql = "select * from log where TransactionID=$trendid and PersonID=$salesid order by WhenChanged";
	$selection = mssql_query($sql);
	if($selection)
	{
		$startdate = 0;
		$enddate = 0;
		$current = 0;
		$count = 0;

		while($row = mssql_fetch_assoc($selection))
		{
			$changed = strtotime($row['WhenChanged']);
			$current = $row['LogBit'];

			if($current)
			{
				$uselast = 0;
				$startdate = $changed;
				if($enddate > 0 )
				{
					$days = GetElapsedDays($enddate, $startdate);
					if($days == 0) $uselast = 1;
				}
			}
			else
			{
				$enddate = $changed;
				$days = GetElapsedDays($startdate, $enddate);
				if($days > 0)
				{
					if($uselast)
					{
						$temp = $resultarray[$count-1];
						$temp['enddate'] = $enddate;
						$temp['days'] = $temp['days'] + $days;
						$resultarray[$count-1] = $temp;
					}
					else
					{
						$temp = array();
						$temp['startdate'] = $startdate;
						$temp['enddate'] = $enddate;
						$temp['days'] = $days;
						$temp['current'] = false;
						array_push($resultarray, $temp);
						$count++;
					}
				}
			}
		}

		if($current)
		{
			if($uselast)
			{
				$temp = $resultarray[$count-1];
				$temp['enddate'] = time();
				$temp['days'] = $temp['days'] + GetElapsedDays($startdate, time());
				$temp['current'] = true;
				$resultarray[$count-1] = $temp;
			}
			else
			{
				$temp = array();
				$temp['startdate'] = $startdate;
				$temp['enddate'] = time();
				$temp['days'] = GetElapsedDays($startdate, time());
				$temp['current'] = true;
				array_push($resultarray, $temp);
			}
		}
	}

}

function GetMilestoneList($personid, &$MSList)
{
	$sql="select Requirement1,Requirement2 from Company where CompanyID=(select companyid from people where personid=$personid)";
	$res=mssql_query($sql);
	$MSList="1=Person; 2=Need; 3=Money; 4=Time";
	$count=4;
	if($res)
		{
		$row=mssql_fetch_assoc($res);
		if(!is_null($row['Requirement1']))
		{
			$r1=$row['Requirement1'];
			$MSList.="; 5=$r1";
			$count++;
		}
		if(!is_null($row['Requirement2']))
			{
			$r2=$row['Requirement2'];
			$MSList.="; 6=$r2";
			$count++;
			}
		}

	return $count;
}



function PrintLocationKey($treeid)
{
$MSCount=GetMilestoneList($treeid, $MSList);

print("Location abbreviations:<br>");
print("<table><tr><td width=20>&nbsp;</td><td><table cellspacing=2 cellpadding=5 bgcolor='ivory'>");
print("<tr bgcolor='lightgrey'><td><b><font size=2>FM</td><td><font size=2>First Meeting</td></tr>");
//print("<tr bgcolor='lightgrey'><td><b><font size=2>IP(1-6)</td><td><font size=2>Information Phase (no. of milestones reached)</td></tr>");
print("<tr bgcolor='lightgrey'><td><b><font size=2>IP(1-$MSCount)</td><td><font size=2>Information Phase ($MSList)</td></tr>");

print("<tr bgcolor='lightgrey'><td><b><font size=2>SIP</td><td><font size=2>Stalled Information Phase</td></tr>");
print("<tr bgcolor='lightgrey'><td><b><font size=2>DP</td><td><font size=2>Decision Point</td></tr>");
print("<tr bgcolor='lightgrey'><td><b><font size=2>SDP</td><td><font size=2>Stalled Decision Point</td></tr>");
print("<tr bgcolor='lightgrey'><td><b><font size=2>RM</td><td><font size=2>Removed</td></tr>");
print("</table></table><br>\n");
}

/***************************************
function GetConsolidatedTrend($StartDate, $TrendId, &$Count, &$AvgDays, &$CurrentCount)
{
	$Count = 0;
	$AvgDays = 0;
	$CurrentCount = 0;

	$sql = "select * from log where TransactionID=$TrendId order by PersonID,WhenChanged";

	$selection = mssql_query($sql);
	if($selection)
	{
		$totdays = 0;
		$starttime;
		$LastPerson = -1;
		$Current = 0;

		while($row = mssql_fetch_assoc($selection))
		{
			$PersonID = $row['PersonID'];
			if($Current && ($PersonID != $LastPerson))
			{
				$totdays += GetElapsedDays($starttime, time());
				$Count++;
				$CurrentCount++;
			}

			$LastPerson = $PersonID;
			$Current = $row['LogBit'];
			if($Current)
			{
				$starttime = strtotime($row['WhenChanged']);
			}
			else
			{
				$endtime = strtotime($row['WhenChanged']);
				$totdays += GetElapsedDays($starttime, $endtime);
				$Count++;
			}
		}


		if($Current)
		{
			$totdays += GetElapsedDays($starttime, time());
			$Count++;
			$CurrentCount++;
		}

		if($Count > 0)
		{
			$AvgDays = $totdays / $Count;
		}
	}
}
******************************************/
?>