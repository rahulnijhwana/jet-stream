<?php
$old_limit = ini_set("memory_limit", "32M");

include('_paul_utils.php');
include('GetSalespeopleTree.php');

if (isset($treeid))
{
	$sql = "select * from opportunities where PersonID=$treeid and Category<>6 and Category<>9 order by Company";
	$opps_result = mssql_query($sql);
	if (mssql_num_rows($opps_result) == 0)
	{
		unset($treeid);
		$nonefound = true;
	}
}

if (!isset($treeid))
{
	begin_report_orig('Opportunity Profile');
	if (isset($nonefound)):?>
		<span style="font-style:italic;">That user has no active opportunities. Please select a different user.</span>
	<?endif;
	print_tree_prompt(SALESPEOPLE);
	end_report_orig();
	exit();
}

if (!isset($oppid))
{
	begin_report_orig('Opportunity Profile');
	
	?>
	<center style="font-family:Arial; font-size:12pt;">Please select an opportunity.</center>
	<table>
	<?while ($row = mssql_fetch_assoc($opps_result)):?>
		<tr>
			<td><a href="<?$_SERVER['PHP_SELF']?>?oppid=<?=$row['DealID']?>&treeid=<?=$treeid?>"><?=$row['Company']?>
			    <?php if(strlen(trim($row['Division']))) {
			     echo " - " . $row['Division'];
			    } ?>
			     </a></td>
		</tr>
	<?endwhile;?>
	</table>
	<?
	
	end_report_orig();
	exit();
}

$sql = "select * from opportunities where DealID=$oppid";
$result = mssql_query($sql);
$row = mssql_fetch_assoc($result);

$mpower_companyid = $row['CompanyID'];
include('category_labels.php');


$companysql = "select * from Company where CompanyID=".$row['CompanyID'];
$companyresult = mssql_query($companysql);
$companyrow = mssql_fetch_assoc($companyresult);

$Source2Name = '';
if ($companyrow['Source2Used'] == 1 && $row['Source2ID'] != NULL && strlen(trim($row['Source2ID'])) != 0) { 
    $source2sql = "select * from sources2 where Deleted<>1 and Source2ID=".$row['Source2ID'];
    $source2result = mssql_query($source2sql);
    if ($source2row = mssql_fetch_assoc($source2result))
    	$Source2Name = $source2row['Name'];
}

$SourceName = '';
if ($row['SourceID'] != NULL && strlen(trim($row['SourceID'])) != 0) { 
    $sourcesql = "select * from sources where Deleted<>1 and SourceID=".$row['SourceID'];
    $sourceresult = mssql_query($sourcesql);
    if ($sourcerow = mssql_fetch_assoc($sourceresult))
    	$SourceName = $sourcerow['Name'];
}

$ValuationName = '';
if ($companyrow['ValuationUsed'] == 1 && $row['VLevel'] != NULL && strlen(trim($row['VLevel'])) != 0) { 
    $valsql = "select * from Valuations where VLevel=".$row['VLevel']." and CompanyID=".$row['CompanyID'];
    $valresult = mssql_query($valsql);
    if ($valrow = mssql_fetch_assoc($valresult))
    	$ValuationName = $valrow['Label'];
}

$personsql = "select * from people where PersonID=".$row['PersonID'];
$personresult = mssql_query($personsql);
if ($personrow = mssql_fetch_assoc($personresult))
	$PersonName = $personrow['FirstName'].' '.$personrow['LastName'];
else
	$PersonName = '';

ob_start();

// begin_report('Opportunity Profile');

?>

    
    <html>
    <head>
  
<style>


body
{
	margin: 60px 0 0 0;
}

table
{
    border-collapse: collapse; 
    page-break-inside: avoid; 
    margin:0 50 0 50;
    width: 600px;">
}

td
{
    padding: 3px;
}

.lab
{
    vertical-align:top;
    font-weight:bold;
}
    
.dat
{
    vertical-align:bottom;
}

body, table
{
	font-size: 9pt;
	font-family: Helvetica;
}


</style>
</head>
<body>
     <script type="text/php">
        
        if ( isset($pdf) ) {
        
            $font = Font_Metrics::get_font("helvetica");
            $pdf->page_text(500, 18, "Page {PAGE_NUM} of {PAGE_COUNT}", $font, 9);
            $pdf->page_text(500, 32, date("n/j/Y"), $font, 9, array(0,0,0));
            $font = Font_Metrics::get_font("helvetica", "bold");
            $pdf->page_text(243, 18, "Opportunity Profile", $font, 14);

            $footer = $pdf->open_object();
//            $pdf->image('c:\mpower\mplogo.jpg', 'jpg', 45, 16, 110, 39);
            $pdf->close_object();
            $pdf->add_object($footer, "all");
}
    </script>     

<center style="font-size:14pt; font-weight:bold;">
<?=$row['Company']?>
</center>
<br>
<br>
			<table style="padding: 1px; vertical-align:bottom;">
				<tr>
					<td class="lab">Company:</td>
					<td class="dat"><?=$row['Company']?></td>

    					<td class="lab">Date Assigned:</td>
                    <?php if (!$row['TargetDate'] == NULL): ?>
    					<td class="dat"><?=date('n/j/y', strtotime($row['TargetDate']))?></td>
                    <?php else: ?>
    					<td>&nbsp;</td>
                    <?php endif; ?>
				</tr>

				<tr>
					<td class="lab">Div/Dept/Loc:</td>
					<td class="dat"><?=$row['Division']?></td>

					<td class="lab">First Meeting:</td>
					<?if ($row['FirstMeeting'] != ''):?>
						<td class="dat"><?=date('n/j/y', strtotime($row['FirstMeeting']))?> <?=$row['FirstMeetingTime']?></td>
					<?else:?>
						<td class="dat">&nbsp;</td>
					<?endif;?>
				</tr>

				<tr>
					<td class="lab">Contact:</td>
					<td class="dat"><?=$row['Contact']?></td>

					<td class="lab">Next Meeting:</td>
					<?if ($row['NextMeeting'] != ''):?>
						<td class="dat"><?=date('n/j/y', strtotime($row['NextMeeting']))?> <?=$row['NextMeetingTime']?></td>
					<?else:?>
						<td class="dat">&nbsp;</td>
					<?endif;?>
				</tr>

				<tr>
                    <?php if ($companyrow['Source2Used'] == 1): ?>
    					<td class="lab"><?php echo $companyrow['Source2Label']; ?>:</td>
    					<td class="dat"><?=$Source2Name?></td>
                    <?php else: ?>
    					<td>&nbsp;</td>
    					<td>&nbsp;</td>
                    <?php endif; ?>
					<td class="lab">Days Since First Meeting:</td>
					<?$daysSinceFM = floor((time()-strtotime($row['FirstMeeting']))/86400);?>
					<?if ($row['FirstMeeting'] != '' && $daysSinceFM >= 0):?>
						<td class="dat"><?=$daysSinceFM?></td>
					<?else:?>
						<td class="dat">&nbsp;</td>
					<?endif;?>
				</tr>

				<tr>
					<td class="lab">Source:</td>
					<td class="dat"><?=$SourceName?></td>

					<td class="lab">Offerings:</td>
                    <?php 
					$prodSQL = "select Products.Abbr from Opp_Product_XRef, Products where Opp_Product_XRef.DealID = $oppid and Opp_Product_XRef.ProductID = Products.ProductID order by Products.Name";
					$prodResult = mssql_query($prodSQL);
					$offerings = "";
					while ($prodRow = mssql_fetch_assoc($prodResult)) {
						$offerings .= ' ' . $prodRow['Abbr'];
					}
                    
                    ?>
					<td class="dat"><?php echo $offerings ?>&nbsp;</td>
				</tr>

				<tr>
                    <?php if ($companyrow['ValuationUsed'] == 1): ?>
    					<td class="lab"><?=$companyrow['ValLabel']?>:</td>
    					<?if ($row['VLevel'] > 0):?>
    						<td class="dat">[<?=$row['VLevel']?>] <?=$ValuationName?></td>
    					<?else:?>
    						<td class="dat">&nbsp;</td>
    					<?endif;?>
                    <?php else: ?>
    					<td>&nbsp;</td>
    					<td>&nbsp;</td>
                    <?php endif; ?>
    
					<td class="lab"><?php echo (empty($companyrow['CustomAmountName'])) ? 'Dollars' : $companyrow['CustomAmountName'] ?>:</td>
					<?php 
					if ($row['EstimatedDollarAmount'] != NULL && $row['EstimatedDollarAmount'] != 0) {
						echo "<td class=\"dat\">" . $row['EstimatedDollarAmount'] . "</td>";
					}
					else {
						echo "<td class=\"dat\">&nbsp;</td>";
					} ?>
				</tr>
				<tr>
					<td class="lab">Salesperson:</td>
					<td class="dat"><?=$PersonName?></td>

					<td class="lab">Dashboard Location:</td>
					<td class="dat">
						<?
						$catname = '';
						switch ($row['Category'])
						{
							case 1: $catname = GetCatLbl('First Meeting'); break;
							case 2: $catname = GetCatLbl('Information Phase'); break;
							case 3: $catname = GetCatLbl('Information Phase Stalled'); break;
							case 4: $catname = GetCatLbl('Decision Point'); break;
							case 5: $catname = GetCatLbl('Decision Point Stalled'); break;
							case 6: $catname = GetCatLbl('Closed'); break;
							case 9: $catname = GetCatLbl('Removed'); break;
							case 10: $catname = GetCatLbl('Target'); break;
						}
						?>
						<?=$catname?>
					</td>
				</tr>
			</table>
<br><br>

<?php 

$milestone_sql = "select ans.Answer, submilestones.* from submilestones 
    left outer join 
        (select dealid, subid, Answer 
        from subanswers 
        where dealid = $oppid) as ans on ans.subid = submilestones.subid
    where companyid = " . $row['CompanyID'];
    
if ($companyrow['Requirement1used'] != 1) {
    $milestone_sql .= " and Location <> 0";
}

if ($companyrow['Requirement2used'] != 1) {
    $milestone_sql .= " and Location <> 5";
}

$milestone_sql .= " order by location, seq";

$milestone_result = mssql_query($milestone_sql);

$prev_loc = -1;

while ($milestone_row = mssql_fetch_assoc($milestone_result))
{
    if ($milestone_row['Location'] != $prev_loc) {

        if ($prev_loc != -1) {
            echo "<tr><td colspan=4>&nbsp;</td></tr>";
            echo "</table>";
        }
        echo '<table style="border-collapse: collapse; page-break-inside: avoid; margin:0 50 0 50; width: 600px;">';

        $prev_loc = $milestone_row['Location'];
        ?>
    	<tr>
    		<td width="40%" style="font-size:13pt; font-weight:bold; border-bottom:1px solid black;" valign="bottom" align="center">
    			<?
    				switch ($milestone_row['Location'])
    				{
    					case 0:
    						print($companyrow['Requirement1']);
    						break;
    					case 1:
    					case 2:
    					case 3:
    					case 4:
    						print($companyrow['MS'.($milestone_row['Location']+1).'Label']);
    						break;
    					case 5:
    						print($companyrow['Requirement2']);
    						break;
    				}
    			?>
    		</td>
    		<td width="15%" style="vertical-align:bottom; font-weight:bold; border-bottom:1px solid black;" align="center">
    			Mandatory
    		</td>
    		<td width="30%" style="vertical-align:bottom; font-weight:bold; border-bottom:1px solid black;" align="center">
    			Answer
    		</td>
    		<td width="15%" style="vertical-align:bottom; font-weight:bold; border-bottom:1px solid black;" align="center">
    		    Complete
    		</td>
    	</tr>
        <?php
    }
    ?>
	<tr>
		<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black;"><?php echo $milestone_row['Prompt'] ?></td>
		<td style="border-bottom:1px solid black; border-right:1px solid black;" align="center"><?=($milestone_row['Mandatory']==1)?'Yes':'No'?></td>
        <?
        $answer_value = urldecode($milestone_row['Answer']);
        $answer = "&nbsp;";
        $valid = "No";
        if ($milestone_row['YesNo'] == 1) {
            if ($milestone_row['Answer'] == 1) {
                $answer = "Yes";
                $valid = "Yes";
            }
        }
        elseif ($milestone_row['CheckBox'] == 1) {
            if ($milestone_row['Answer'] == 1) {
                $answer = "Checked";
                $valid = "Yes";
            }
            else {
                $answer = "Unchecked";
            }
        }
        elseif ($milestone_row['Calendar'] == 1 || $milestone_row['AlphaNum'] == 1) {
            if (strlen(trim($milestone_row['Answer'])) > 1) {
                $answer = $answer_value;
                $valid = "Yes";
            }
        }
        elseif ($milestone_row['DropDown'] == 1) {
            if (strlen(trim($milestone_row['Answer'])) > 1 && trim($milestone_row['Answer']) != "-1") {
                $answer = $answer_value;
                for ($x = 1; $x <= 20; $x++) {
                    if (trim($milestone_row["DropChoice$x"]) == trim($milestone_row['Answer']) && $milestone_row["DChoiceOK$x"] == 1) {
                        $valid = "Yes";
                    }
                }
            }
        }
        
        if ($milestone_row['Mandatory'] != 1) {
            $valid = "n/a";
        }
        
        ?>
		<td style="border-bottom:1px solid black; border-right:1px solid black;"><?php echo $answer ?></td>
		<td style="border-bottom:1px solid black; border-right:1px solid black;" align="center"><?php echo $valid ?></td>
	</tr>
    <?php
}
?>
</table>
</body>
</html>
<?php

//end_report();

$ende = ob_get_contents();

ob_end_clean();

require_once("/var/www/mpower/dompdf/dompdf_config.inc.php");


$dompdf = new DOMPDF(); 
$dompdf->load_html($ende); 
$dompdf->render(); 
$dompdf->stream("my_pdf.pdf", array("Attachment" => 0));

?>