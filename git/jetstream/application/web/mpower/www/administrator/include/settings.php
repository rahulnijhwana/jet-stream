<?php
/**
 * Settings
 *
 * Sets default values to allow M-Power to easily move from server to server.
 * 
 * @package Shared
 */

$db_server = '';
$log_file = '/webtemp/logs/admin.log';

if(strpos($_SERVER['SERVER_SOFTWARE'], "Win32") !== FALSE) { 
    // Windows default settings
    $db_server = "75.101.239.183";//"128.121.65.35";
    $log_file = "c:\sourcecode\mpower.log";
}
else {
    // Unix Settings
    $db_server = "ASA-VERIO";
    $log_file = "/var/log/httpd/mpower_log"; 
}
?>
