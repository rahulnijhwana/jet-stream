<?php
 /**
 * @package Shared
 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>M-Power</title>
<link rel="stylesheet" type="text/css" href="../css/admin.css,sharedstyles.css">
<STYLE>
   BODY {font-size: 12px}
</STYLE>

<script language="JavaScript" src="../javascript/category_labels.js,saver2.js"></script>
</head>
<script language="JavaScript" src="../data/get_company.php"></script>

<script language="JavaScript">

// g_company = window.opener.g_company;


g_valuationused = g_company[0][g_company.ValuationUsed];
g_sourceused = g_company[0][g_company.SourceUsed];
g_source2used = g_company[0][g_company.Source2Used];
g_source2label = g_company[0][g_company.Source2Label];

function loaded()
{

}

function writeHeaderTitle(text)
{
	document.writeln(text);
}

var strTitle = 'Import ' + catLabel('Target');
if (strTitle.charAt(strTitle.length - 1) == 's')
	strTitle += ' (CSV)';
else
	strTitle += 's (CSV)';

// g_company = window.opener.g_company;
function writeHeader()
{

//	writeHeaderTitle('Import Targets (CSV)');

	writeHeaderTitle(strTitle);

}


function onOKClicked()
{
	if(document.forms.form1.TargetFile.value == '')
	{
		alert('Please choose a file to upload, using the Browse button if necessary.');
		return;
	}
	document.forms.form1.submit();
//	window.close();
}

function onCancelClicked()
{
	window.close();
}


function printExampleRow(lName, fName, Comp, Src, Val, Division, Contact, Src2, SalesID)
{
	document.write(TR + '<td>' + lName + '</td>');
	document.write('<td>' + fName + '</td>');
	document.write('<td>' + Comp + '</td>');
	if(g_sourceused > 0)
		document.write('<td>' + Src + '</td>');
	if(g_valuationused > 0)
		document.write('<td align="right">' + Val + '</td>');
	document.write('<td style="color:blue">' + Division + '</td>');
	document.write('<td style="color:blue">' + Contact + '</td>');
	if(g_source2used > 0)
		document.write('<td style="color:blue">' + Src2 + '</td>');
	document.write('<td>' + SalesID + '</td></tr>');

}

function printAlignments()
{
	document.write('<tr style="color:red"><td align="center"><b>Required</b></td>');
	document.write('<td align="center"><b>Required</b></td>');
	document.write('<td align="center"><b>Required</b></td>');
	if(g_sourceused > 0)
		document.write('<td align="center"><b>Optional</b></td>');
	if(g_valuationused > 0)
		document.write('<td align="center"><b>Optional</b></td>');
	document.write('<td align="center" style="color:blue"><b>Omit unless<br>selected below</b></td>');
	document.write('<td align="center" style="color:blue"><b>Omit unless<br>selected below</b></td>');
	if(g_source2used > 0)
		document.write('<td align="center" style="color:blue"><b>Omit unless<br>selected below</b></td>');
	document.write('<td align="center"><b>Used only when duplicate<br>salesperson names exist</b></td></tr>');

}

function printHeadings()
{
	document.write('<tr><td align="center">Salesperson<br>Last Name</td>');
	document.write('<td align="center">Salesperson<br>First Name</td>');
	document.write('<td align="center">Target<br>Company<br>Name</td>');
	if(g_sourceused > 0)
		document.write('<td align="center">Source<br>Descriptor</td>');
	if(g_valuationused > 0)
		document.write('<td align="center">Valuation<br>Level</td>');
	document.write('<td align="center" style="color:blue">Division,<br>Department,<br>or Location</td>');
	document.write('<td align="center" style="color:blue">Contact</td>');
	if(g_source2used > 0)	/* was like this */
		document.write('<td align="center" style="color:blue">' + g_source2label + '</td>');

	document.write('<td align="center">Salesperson User ID</td></tr>');
}


</script>

<form name="form1" enctype="multipart/form-data" action="../data/proc_csv_targets.php" method="post">
<body style="background-color:lightgrey; font-family: arial" onload="loaded()" >
<table border=0 height="10%" width="100%" style="font-size:12px">
	<tr>
		<td class="title" colspan="2" align="center" valign="top" style="font-size:16px">
		<script language = "JavaScript">
			writeHeader();
		</script>
		</td>
	</tr>
	<tr><td>
		<script language = "JavaScript">
		document.writeln('The <b>' + strTitle + '</b> utility is used to assign a list of target companies to one or more salespeople.  If this is not your intended action, please exit this utility by selecting <b>Cancel</b> below.  The list used for importing can be created using a spreadsheet program, such as Excel.');
		document.writeln('All imported records must be in the following format:<br><br>');
		</script>
	</td></tr>
	</table>
	<table border=1 borderColor="black" height="10%" width="100%" style="font-size:12px">
	<script language="JavaScript">
		printHeadings();
		printAlignments();
	</script language = "JavaScript">
	</table>

<br>The following is an example of a valid target record list:<br><br>
	<table border=1 borderColor="black" height="20%" width="100%" style="font-size:12px"><tr>
	<script language="JavaScript">
		printHeadings();
		TR = '<tr>';
		printExampleRow('Robbins','Joyce','Testco01','Print Ad','2','Chicago','Ed Peterson','Pick 01', '&nbsp;');
		printExampleRow('Robbins','Joyce','Testco02','Telemktg','&nbsp;','&nbsp;','&nbsp;','Pick 02', '&nbsp;');
		printExampleRow('Williams','Robert','Testco03','Call In','2','London','Mike Jones','Pick 03', '&nbsp;');
		printExampleRow('Baxter','Mary','Testco04','&nbsp;','&nbsp;','Plastics','&nbsp;','Pick 04', '&nbsp;');
		printExampleRow('Smith','John','Testco05','&nbsp;','1','&nbsp;','Teresa Darby','Pick 05', 'jsmith822');
		printExampleRow('Smith','John','Testco06','Telemktg','4','Waukesha','&nbsp;','Pick 06', 'jsmith822');
		printExampleRow('Smith','John','Testco07','Radio Ad','&nbsp;','&nbsp;','&nbsp;','Pick 07', 'jsmith117');
	</script>
	</table><br><br>


	<table border=0 height="20%" width="100%"  style="font-size:12px">
	<tr>
		<td colspan="2">
		In the above example, the two salespeople named <b>John Smith</b> were assigned targets
		utilizing their Salesperson User ID.  If you have this situation, please contact your
		M-Power system administrator to obtain their
		M-Power User ID�s and include them within each record as shown.
		If the Source Descriptor or Valuation Level fields are utilized,
		they must contain values currently existing within M-Power.<br>
		</td>
	</tr>
	<tr><td>&nbsp</td></tr>
	<tr>
		<td colspan="2">
		The target spreadsheet must be saved as a CSV (Comma Separated Value) file prior to
		importing into M-Power.  Spreadsheet programs such as Excel can export data in CSV format.
		An import file with comma-separated fields may also be prepared in a text editor such
		as Notepad, or in a word processor. If a word processor is used, the file should be saved in plain text format.
		</td>
	</tr>
	<!-- RJP 1-12-06 -->
	<tr><td>&nbsp</td></tr>
	<tr>
		<td colspan="2" style="color:blue">
		<script language="JavaScript">
			document.write('<b>Division/Department/Location, Contact');
			if(g_source2used > 0)
				document.write(' and '+g_source2label);
			document.write(' fields</b>');
		</script>
		<br>These are optional fields that may be added to the CSV file.
		They must be placed in the order shown above. If the first two
		fields are added, the Contact field should come
		<b>after</b> the Division/Department/Location field.
		</td>
	</tr>
	<tr><td>&nbsp</td></tr>
	<tr>
		<td>&nbsp;</td>
		<td><input type="checkbox" name="usediv">Import Division/Department/Location</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><input type="checkbox" name="usecont">Import Contact</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<script language="JavaScript">
		if(g_source2used > 0)	/* was like this */
			document.write('<td><input type="checkbox" name="usesrc2">Import ' + g_source2label + '</td>');
		</script>
	</tr>
	<!-- end RJP 1-12-06 -->
<!--
	<tr>
		<td>&nbsp;</td>
		<td><input type="checkbox" name="popdd">Populate Dropdown only (targets will not be created)</td>
	</tr>
-->
	<tr>
		<td>Import File <i>(Must be CSV Format)</i></td><td><input type="hidden" name="MAX_FILE_SIZE" value="1000000" /><input type="file" name="TargetFile" size="70"/></td>
	</tr>
	<tr>
		<td align="center" colspan="2"><button type="button" onclick="onOKClicked();">Submit</button>&nbsp;
		<button type="button" onclick="onCancelClicked();">Cancel</button></td>
	</tr>

</table>
</body>
</form>
</html>
