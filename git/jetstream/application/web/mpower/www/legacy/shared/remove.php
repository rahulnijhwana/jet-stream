<?php
/**
* @package Shared
*/

define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- <title>M-Power</title> -->
<link rel="stylesheet" type="text/css" href="../css/sharedstyles.css">
<script language="JavaScript" src="../javascript/windowing.js,utils.js"></script>
<script language="JavaScript" src="../data/get_removereasons.php"></script>
</head>

<body style="background-color:#949597;">

<script language="JavaScript">
<!--
<?php
include_once('../data/_base_utils.php');
include_once('../data/_query_utils.php');

//echo "\n //"."SELECT * FROM removereasons WHERE CompanyID = '$mpower_companyid' AND Deleted = '0' order by Text"."; \n\n";
//echo "\n //----- >"."SELECT * FROM removereasons WHERE CompanyID = '$mpower_companyid' AND Deleted = '1'"."; \n\n";

dump_sql_as_array('g_removereasons', "SELECT * FROM removereasons WHERE CompanyID = '$mpower_companyid' AND Deleted = '0' order by Text");
dump_sql_as_array('g_removed_removereasons', "SELECT * FROM removereasons WHERE CompanyID = '$mpower_companyid' AND Deleted = '1'");
?>

function IsNumeric(strString)
{
	var strValidChars = "0123456789.-";
	var strChar;
	var blnResult = true;
	if (strString.length == 0)
		return false;
	for (i = 0; i < strString.length && blnResult == true; i++)
	{
		strChar = strString.charAt(i);
		if (strValidChars.indexOf(strChar) == -1)
			blnResult = false;
	}
	return blnResult;
}

function TestKeyPress(obj, e)
{
	if (e.keyCode == 13)
	{
		onNext();
		return;
	}
    var curChar    = String.fromCharCode(e.keyCode);
    var inpStr     = obj.value + curChar
    window.status  = '';
    obj.title      = '';

    result = inpStr.match('^[0-9]+$');
    if (!result)
    {
        window.status     = 'Please enter only numbers.';
        obj.title         = window.status;
        e.returnValue = false;
        e.cancel      = true;
    }
}

function ok()
{
	var elems = document.forms['form1'].elements;
	var reasonID = value_of_select(document.forms['form1'].elements.reason);
	if (Windowing.dropBox.isFM)
	{
		var selTookPlace = document.getElementById('selectHadFM');
		if ('-1' == value_of_select(selTookPlace))
		{
			alert('You must indicate whether the first meeting took place');
			return;
		}
		Windowing.dropBox.FM_took_place = value_of_select(selTookPlace);
	}
	if (elems.selAutoRevive.value == 1)
	{
		var autoReviveDate = new Date();
		autoReviveDate = autoReviveDate.getTime();
		if (elems.selWhen.value == -1)
		{
			if (elems.textDays.value == '')
			{
				alert('Please enter the number of days to wait before auto-reviving this opportunity.');
				return;
			}
			autoReviveDate += 86400000*elems.textDays.value;
		}
		else if (elems.selWhen.value == -2)
		{
			if (elems.textDate.value == '')
			{
				alert('Please enter the auto-revival date for this opportunity.');
				return;
			}
			autoReviveDate = Dates.makeDateObj(elems.textDate.value).getTime();
		}
		else
			autoReviveDate += 86400000*elems.selWhen.value;
		//alert(new Date(autoReviveDate));
		if (Windowing.dropBox.reasonCallback)
			Windowing.dropBox.reasonCallback(reasonID, Dates.formatDate(new Date(autoReviveDate)));
	}
	else if (Windowing.dropBox.reasonCallback)
		Windowing.dropBox.reasonCallback(reasonID);
	window.close();
}

function cancel()
{
	window.close();
}

// ---------------------------------------------------------------------------------------

function list_removal_options()
{
	var ret = '';
	for (var i = 0; i < g_removereasons.length; ++i)
	{
		var data = g_removereasons[i];
		ret += '<option ';
		if (i == 0) ret += ' selected';
		ret += ' value="' + data[g_removereasons.ReasonID] + '">' + data[g_removereasons.Text] + '</option>';
	}

	return ret;
}

function value_of_select(selectObj)
{
	if (!value_of_select.arguments.length) return '';

	var sel = selectObj.selectedIndex;
	if (sel == -1) return '';

	return selectObj.options[sel].value;
}

function onChangeAuto(elem)
{
	var trAutoOptions = document.getElementById('trAutoOptions');
	trAutoOptions.style.visibility = (elem.value == 1) ? 'visible' : 'hidden';
}

function onChangeWhen(elem)
{
	//alert(elem.value);
	var spanDays = document.getElementById('spanDays');
	var spanDate = document.getElementById('spanDate');
	spanDays.style.display = (elem.value == -1) ? 'inline' : 'none';
	spanDate.style.display = (elem.value == -2) ? 'inline' : 'none';
}

function show_calendar(targetName)
{
	document.body.style.cursor = 'wait';
	//alert(document.forms['form1'].elements[targetName]);
	Windowing.dropBox.calendarTarget = document.forms['form1'].elements[targetName];
	Windowing.dropBox.onCalendarEdited = null;
	var win = Windowing.openSizedWindow('../shared/calendar.html', 320, 600, 'reviveDate');
	win.focus();
	document.body.style.cursor = 'auto';
}

// -->
</script>

<form name="form1">

<table border=0 height="100%" width="100%">
<tr>
	<td rowspan=4 width=40>&nbsp;</td>
</tr>
<tr>
	<td>
		<table border=0>
		<tr>
			<td><img src="../images/removed_opp.gif" align="left"></td>
			<td class="title" valign="middle" style="color:white; font-weight:bold;">Opportunity Removal</td>
		</tr>
		</table>
	</td>
</tr>
<tr valign="top">
	<td>

		<table border=0>
		<tr>
			<td nowrap height="23" class="plabel">Reason &nbsp;&nbsp;&nbsp;</td>
			<td height="23">
				<select name="reason" style="width:500px;">
				<script language="JavaScript">
				<!--
				document.writeln(list_removal_options());
				// -->
				</script>
				</select>
			</td>
		</tr>

		<script language="JavaScript">

			if (Windowing.dropBox.isFM)
			{
				document.writeln('<tr><td class="plabel">');
				document.writeln('Did first meeting take place?</td>');
				if (!Windowing.dropBox.defaultYesFMTookPlace)
					document.writeln('<td><select id="selectHadFM">');
				else
					document.writeln('<td><select id="selectHadFM" disabled>');
				document.writeln('<option value="-1"></option>');
				if (!Windowing.dropBox.defaultYesFMTookPlace)
					document.writeln('<option value="yes">Yes</option>');
				else
					document.writeln('<option value="yes" selected>Yes</option>');
				document.writeln('<option value="no">No</option>');
				document.writeln('</select></td></tr>');
			}

		</script>

		<tr>
			<td class="plabel">Auto-Revive?</td>
			<td>
				<select name="selAutoRevive" onchange="onChangeAuto(this)">
					<option value="1">Yes</option>
					<option value="0" selected>No</option>
				</select>
			</td>
		</tr>
		<tr id="trAutoOptions" style="visibility:hidden;">
			<td class="plabel">When?</td>
			<td class="plabel">
				<select name="selWhen" onchange="onChangeWhen(this)">
					<option value="30">30 Days</option>
					<option value="60">60 Days</option>
					<option value="90">90 Days</option>
					<option value="180">180 Days</option>
					<option value="270">270 Days</option>
					<option value="365">1 Year</option>
					<option value="-1">Other Number of Days</option>
					<option value="-2">Specific Date</option>
				</select>
				<span id="spanDays" style="display:none;"><input type="text" size="4" name="textDays" onkeypress="TestKeyPress(this, event);"></input> Days</span>
				<span id="spanDate" style="display:none;"><input type="text" size="10" name="textDate" readonly></input> <input type="button" name="autoDateCal" class="etc-button" onclick="show_calendar('textDate')"></span>
			</td>
		</tr>

		</table>

	</td>
</tr>
<tr valign="bottom">
	<td align="right">
		<button type="button" class="sized_command" onclick="ok()">OK</button>
		&nbsp;
		<button type="button" class="sized_command" onclick="cancel()">Cancel</button>
	</td>
</tr>
</table>

</form>

</body>
</html>
