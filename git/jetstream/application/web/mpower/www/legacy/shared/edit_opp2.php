<?php
/**
* @package Shared
*/

define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');
require_once(BASE_PATH . '/include/class.DbConnManager.php');
require_once(BASE_PATH . '/include/class.SqlBuilder.php');
require_once(BASE_PATH . '/include/class.ReportingTree.php');

SessionManager::Init();
SessionManager::Validate();

$CMID = (isset($_REQUEST['CMID'])) ? $_REQUEST['CMID'] : null;
$sfUserID = (isset($_REQUEST['sfUserID'])) ? $_REQUEST['sfUserID'] : null;
$API_Partner_Server_URL_60 = (isset($_REQUEST['API_Partner_Server_URL_60'])) ? $_REQUEST['API_Partner_Server_URL_60'] : null;
$API_Session_ID = (isset($_REQUEST['API_Session_ID'])) ? $_REQUEST['API_Session_ID'] : null;


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">

    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Edit Opportunity</title>
	<link rel="stylesheet" type="text/css" href="../css/sharedstyles.css">
    <script language="JavaScript" src="../javascript/edit_opp2.js,load_dropdowns.js,utils.js,windowing.js,category_labels.js,smartselect.js,pageformat.js,save_board.js,saver2.js,ci_confirm.js,offerings.js"></script>

    <script language="JavaScript">
        function updateSFOppFromMPower()
        {
            var req = getXMLHttpRequest();
            req.open('POST', "../sf/sfUpdateOpp.php", false);
            req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            var str = "API_Session_ID="+encodeURI("<?=$API_Session_ID?>");
            str += "&API_Partner_Server_URL_60="+encodeURI("<?=$API_Partner_Server_URL_60?>");
            str += "&CMID="+encodeURI("<?=$CMID?>");
            str += "&sfUserID="+encodeURI("<?=$sfUserID?>");
            if (g_opportunity[g_oppCols.Division] != "")
            str += "&Name="+encodeURI(g_opportunity[g_oppCols.Division]);
            if (g_opportunity[g_oppCols.ActualCloseDate] != "")
            str += "&ActualCloseDate="+encodeURI(g_opportunity[g_oppCols.ActualCloseDate]);
            if (g_opportunity[g_oppCols.ForecastCloseDate] != "")
            str += "&ForecastCloseDate="+encodeURI(g_opportunity[g_oppCols.ForecastCloseDate]);
            if (g_opportunity[g_oppCols.FirstMeeting] != "" && g_opportunity[g_oppCols.FirstMeetingTime] != "")
            {
                str += "&FirstMeeting="+encodeURI(g_opportunity[g_oppCols.FirstMeeting]);
                str += "&FirstMeetingTime="+encodeURI(g_opportunity[g_oppCols.FirstMeetingTime]);
                <? if (isset($FirstMeetingID)): ?>
                str += "&FirstMeetingID=<?=$FirstMeetingID?>";
                <? endif; ?>
            }
            if (g_opportunity[g_oppCols.NextMeeting] != "" && g_opportunity[g_oppCols.NextMeetingTime] != "")
            {
                str += "&NextMeeting="+encodeURI(g_opportunity[g_oppCols.NextMeeting]);
                str += "&NextMeetingTime="+encodeURI(g_opportunity[g_oppCols.NextMeetingTime]);
                <? if (isset($NextMeetingID)): ?>
                str += "&NextMeetingID=<?=$NextMeetingID?>";
                <? endif; ?>
                str += "&NM_New="+(g_meetingOccurred?1:0);
            }
        }

        function cancel()
        {
            <?php if ($CMID):?>
            redirectSalesforce();
            <?php else:?>

			if (g_somethingWasChanged) {
				if(confirm('Are you sure you want to close and lose the changes?')) {
					window.close();
				}
			}else {
				window.close();
			}
            <?php endif;?>
        }

        function redirectSalesforce()
        {
            window.parent.parent.location.href = "https://na1.salesforce.com/<?php print($CMID);?>";
        }
    </script>

    </head>

    <body  oldstyle="visibility: hidden" leftmargin=0 rightmargin=0 topmargin=0 bottommargin=0 onload="loaded()" style="background-color:black;"><form name="form1" style="height:100%" style="background-color:black"><!-- height attribute is for mozilla 1.2.1 -->

        <script language="JavaScript">
        //11/10/04:  Admin capability now extended to managers on the board side.
        //Therefore, window.opener.g_admin_mode means a call from admin.html
        //The local variable, g_admin_mode means manager on the board side
        //g_admin_mode is set in write_header()

        isNav = (navigator.appName.indexOf("Netscape") != -1) ? true : false;
        isIE = (navigator.appName.indexOf("Microsoft") != -1) ? true : false;
        isDOM = (document.getElementById) ? true : false;

		window.onresize=positionSelects;
		g_isSpreadsheet = false;

		<?php if(isset($isspreadsheet) && $isspreadsheet == '1') echo 'g_isSpreadsheet = true;'."\n\n";?>

        g_enforce_rules = true;
        g_admin_mode = false;
        g_adminCall = false;
        g_editMode = true;      // true if editing, false if new
        g_somethingWasChanged = false;
        g_dealID = -1;
        g_personID = -1;
        g_opportunity = null;
        g_valuationID = -1;
        g_valuationLevel = -1;
        g_sourceID = -1;
        g_source2ID = -1;
        g_oppCols = new Object();
        g_oldCategory = -1;
        g_oldPersonID = -1;
        g_nextMeeting='';   // orginal nextmeeting of the opp
        g_amount_name = "Dollar Amount";
        g_has_custom_amount = false;
        g_has_close_questions = false;
        g_old_cat = -1;
        g_prev_cat = -1;
        g_standalone=false;
        g_meetingOccurred=false;    // indicates whether the new next meeting is a new meeting (true) or a rescheduled meeting (false)

        <?php
        extract($_GET);
		$companyList="('$mpower_companyid')";

		include_once('../data/_base_utils.php');
		include_once('../data/_query_utils.php');
		include_once('../data/get_valuations.php');
		
        // if a contact manager opp id is passed in, then edit_opp is in stand alone mode
        if ($CMID)
        {
            if ($currentuser_data['CMID'] == '' && $sf_userid)
            {
                $currentuser_data['CMID']=$sf_userid;
                $sql="UPDATE people SET CMID='$sf_userid' WHERE personid = $currentuser_personid";
				//echo "alert($sql)"  ;
                $result = mssql_query($sql);
            }

            include_once('../data/get_user.php');
            include_once('../data/get_company.php');
            include_once('../data/get_products.php');
            include_once('../data/get_submilestones.php');
            include_once('../data/get_sources.php');
            include_once('../data/get_sources2.php');

            dump_nothing_as_array(array('g_snoozealerts','g_login'));
            $sql = "SELECT * FROM opportunities WHERE CMID='$CMID' and CompanyID=$mpower_companyid";
			// is there a way not to query twice?
            // dump_sql_as_array('g_opps', $sql);
            $result = mssql_query($sql);
            dump_resultset_as_array('g_opps', $result);
            $DealID=-1;
            if($result) {
                if($row = mssql_fetch_assoc($result)) {
                    $DealID = $row['DealID'];
                }
            }

            if ($DealID == -1) {
                $DealID = -2;   // set it to something besides -1
                print("g_opportunity=new Array();\n");
                print("g_opps[0]=g_opportunity;\n");
            }
            print("g_dealID=$DealID;\n");

            // figure out field names
            $len = mssql_num_fields($result);
            for ($i = 0; $i < $len; ++$i) {
                $field = mssql_fetch_field($result, $i);
                if ($field->name == 'Category' && $DealID!=-2) continue;    // don't allow a change to an edit opps category
                if (${$field->name} || $DealID==-2) // if a new opp, seed all the columns anyway
                print("g_opps[0][$i]='".${$field->name}."';\n");
            }
            // for new (to us) opp, then we use what was passed in, otherwise we default to 1---could be 10?
            if ($DealID == -2) {
                print("if (g_opportunity[g_opps.Category]=='') g_opportunity[g_opps.Category]='1';\n");
            }

            if (isset($Category) && $Category == 6) {
                print("g_opps[0][g_opps.Person]='1';\n");
                print("g_opps[0][g_opps.Need]='1';\n");
                print("g_opps[0][g_opps.Money]='1';\n");
                print("g_opps[0][g_opps.Time]='1';\n");
                print("g_opps[0][g_opps.Requirement1]='1';\n");
                print("g_opps[0][g_opps.Requirement2]='1';\n");
            }

            if (isset($Source)) {   // if source is passed in, figure out what the key is and update the opp
                $SourceID=-1;
                if (isset($SourceList[$Source])) {
                    $SourceID=$SourceList[$Source];
                }
                else {  // found a new one
                    $sql = "insert into sources (name, companyid) values ('$Source', $mpower_companyid)";
                    $result = mssql_query($str);
                    if ($result != false)
                    {
                        $id_result = mssql_query("SELECT @@IDENTITY AS 'SourceID'");
                        $SourceID = mssql_fetch_assoc($id_result);
                        print("newSource=new Array();\n");
                        print("newSource[g_sources.Name]=$Source;\n");
                        print("newSource[g_sources.SourceID]=$SourceID;\n");
                        print("newSource[g_sources.Deleted]='0';\n");
                        print("g_sources[g_sources.length]=newSource;\n");
                    }
                }
                if ($SourceID!=-1)
                {
                    print("g_opps[0][g_opps.SourceID]=$SourceID;\n");
                }
            }

            if (isset($Source2))        // if source is passed in, figure out what the key is and update the opp
            {
                $Source2ID=-1;
                if (isset($Source2List[$Source]))
                {
                    $Source2ID=$Source2List[$Source];
                }
                else    // found a new one
                {
                    $sql = "insert into sources2 (name, companyid) values ('$Source2', $mpower_companyid)";
                    $result = mssql_query($str);
                    if ($result != false)
                    {
                        $id_result = mssql_query("SELECT @@IDENTITY AS 'Source2ID'");
                        $Source2ID = mssql_fetch_assoc($id_result);
                        print("newSource2=new Array();\n");
                        print("newSource2[g_sources2.Name]=$Source2;\n");
                        print("newSource2[g_sources2.SourceID]=$Source2ID;\n");
                        print("newSource2[g_sources2.Deleted]='0';\n");
                        print("g_sources2[g_sources2.length]=newSource2;\n");
                    }
                }
                if ($Source2ID!=-1)
                {
                    print("g_opps[0][g_opps.Source2ID]=$Source2ID;\n");
                }
            }

            $sql = "SELECT * FROM Opp_Product_XRef where (DealID = '$DealID') order by Seq ";
            dump_sql_as_array('g_opp_products_xref', $sql);

            ?>

            g_admin_mode=false;
            g_standalone=true;
            g_people=g_user;

            g_personID=g_opps[0][g_opps.PersonID]=g_people[0][g_people.PersonID];
            g_editedSubAnswers = new Array();

            // this same function is called setSubAnswer in main.js, but had to be renamed since we already have a local function
            // with the same name that gets called from popup windows with the same name

            <?php
            $reqPersonId=$currentuser_personid;
        }
        else
        {

			$targetsalesperson = $_SESSION['tree_obj']->GetTarget(true);
			$peers = is_array($targetsalesperson) && count($targetsalesperson) ? implode(',',array_values($targetsalesperson)) : $mpower_effective_userid;

			include('../data/get_login.php');
			include_once('../data/get_company.php');
			include_once('../data/get_user.php');

			dump_sql_as_array('g_submilestones', "SELECT * FROM SubMilestones WHERE CompanyID IN ($companyList) ORDER BY Location, Seq");
			include_once('../data/get_opp_products_xrefs.php');


			if ($currentuser_loginid==7687) print("g_showaffiliates=true;\n"); 
			else print("g_showaffiliates=false;\n");

			$sql="SELECT * FROM people 
					WHERE isSalesperson = 1 AND 
						  PersonID IN ($peers)
					ORDER BY LastName";
			$getpeople = get_data($sql);
			dump_sql_as_array('g_people', $sql);
			
			$first = date('Y-m-d h:i:s');
			$sql = "SELECT * FROM opportunities WHERE CompanyID = '$mpower_companyid'";
			//$sql .= " AND (Category != '6' OR ActualCloseDate >= '$first')";
			$sql .= " AND (Category != '9')";
			$sql .= " AND PersonID in ($peers)";
			dump_sql_as_array('g_opps', $sql);
			
			dump_sql_as_array('g_submilestones', "SELECT * FROM SubMilestones WHERE CompanyID IN ($companyList) ORDER BY Location, Seq");
			dump_sql_as_array('g_snoozealerts', "SELECT * FROM snoozealerts WHERE PersonID IN ($peers)");
			dump_sql_as_array('g_logins', "SELECT * FROM Logins WHERE CompanyID = '$mpower_companyid' AND PersonID in ($peers)");
			
			include_once('../data/get_spreadsheet_subanswers.php');
			include_once('../data/get_sources.php');
			include_once('../data/get_sources2.php');
			
			dump_sql_as_array('g_opp_products_xref', "SELECT * FROM Opp_Product_XRef where CompanyID IN ($companyList) order by DealID, Seq");
			dump_sql_as_array('g_products', "select * from products where CompanyID IN ($companyList) and Deleted = '0' order by Name");
			
			
			$strsql = "SELECT CompanyID, FiscalYearEndDate FROM FiscalYear where companyID = $mpower_companyid and FiscalYearEndDate is not NULL" ; 
			$result1 = mssql_query($strsql);
			$fiscal_date = mssql_fetch_assoc($result1);
			$fiscal_month = getMonth($fiscal_date['FiscalYearEndDate']) +1 ; 
			
			echo "g_fiscal_month_start = $fiscal_month ;" 
			?>

			g_opp_age = 0;			
			g_editedSubAnswers = new Array();
			
			if(window.opener && g_isSpreadsheet && false) {
				g_opps=window.opener.g_opps;
				do_save=window.opener.do_save;
			}else {

				function do_save()
				{	
					//Header.disableAllButtons();
					g_saver = new Saver();
					g_saver.open();
					save_table(g_opps, g_opps.DealID, true, '\012', saved_new_opps, '../data/insert_opps.php');
				}
			}
			
			function find_opportunity(id)
			{	
				for (var i = 0; i < g_opps.length; ++i) {
					if (g_opps[i][g_opps.DealID] == id) return g_opps[i];
				}			
				return null;
			}


            // figure out if in new or edit mode
            var hash = window.location.hash.toString();
            if (hash.length && hash.charAt(0) == '#')
            hash = hash.substring(1);

            var pos = hash.indexOf('_');
            g_dealID = parseInt(hash.substr(0, pos), 10);
            if (g_dealID == -1) g_editMode = false;

            hash=hash.substr(++pos);
            var pos = hash.indexOf('_');
            if(pos==-1) g_personID = hash;
            else
            {
                g_personID = hash.substr(0,pos);
                hash=hash.substr(++pos);
                g_valuationLevel=hash;
            }

            <?php

        }

		$reqPersonId = isset($reqPersonId) ? $reqPersonId : -1;
        $sql = "SELECT Company,Division,Contact FROM Opportunities where (PersonID = '$reqPersonId') AND Company is not null group by Company,Division,Contact order by Company,Division,Contact";
        dump_sql_as_array('g_company_list', $sql);

        ?>

        g_the_only_company = g_company[0];
        g_CMInterface=g_the_only_company[g_company.CMInterface]=='1';
        g_showcontact=!g_standalone;        // show contact if not standalone--may change for other systems
        g_showcontact = true;

		

        var strIPS = getCatLabel('{{IP}}', '{{IP}}','IPLabel', 'Information Phase') + ' - ' + getCatLabel('{{S}}', '{{S}}','SLabel', 'Stalled');
        var strDPS = getCatLabel('{{DP}}', '{{DP}}','DPLabel', 'Decision Point') + ' - ' + getCatLabel('{{S}}', '{{S}}','SLabel', 'Stalled');

        g_boardLocations = new Array(null,
        getCatLabel('{{FM}}', '{{FM}}','FMLabel', 'First Meeting'),
        getCatLabel('{{IP}}', '{{IP}}','IPLabel', 'Information Phase'),
        strIPS,
        getCatLabel('{{DP}}', '{{DP}}','DPLabel', 'Decision Point'),
        strDPS,
        getCatLabel('{{C}}', '{{C}}','CLabel', 'Closed')
        );

        g_mileStoneLocs = new Array('Requirement1','Person','Need','Money','Time','Requirement2');

        g_req1Used = false;
        g_req2Used = false;

        hasPersonQuestions = g_the_only_company[g_company.AskPersonQuestions] == '1';
        hasNeedQuestions = g_the_only_company[g_company.AskNeedQuestions] == '1';
        hasMoneyQuestions = g_the_only_company[g_company.AskMoneyQuestions] == '1';
        hasTimeQuestions = g_the_only_company[g_company.AskTimeQuestions] == '1';
        hasReq1Questions = g_the_only_company[g_company.AskReq1Questions] == '1';
        hasReq2Questions = g_the_only_company[g_company.AskReq2Questions] == '1';
        hasMilestoneQuestions = (hasPersonQuestions || hasNeedQuestions || hasMoneyQuestions || hasTimeQuestions || hasReq1Questions || hasReq2Questions);

        l_editedSubAnswers = new Object();
        g_subMSEnabled = (g_the_only_company[g_company.SubMSEnabled] == '1');
        g_target_used = (g_the_only_company[g_company.TargetUsed] == '1');
        g_valuation_used = (g_the_only_company[g_company.ValuationUsed] == '1');
        g_source_used = (g_the_only_company[g_company.SourceUsed] == '1');
        g_source2_used = (g_the_only_company[g_company.Source2Used] == '1');
        g_ValLabel = g_the_only_company[g_company.ValLabel];
        g_ValAbbr = g_the_only_company[g_company.ValAbbr];

        g_Transactional = g_the_only_company[g_company.Transactional]=='1';
		g_CallIn = g_the_only_company[g_company.CallIn]=='1';
		
		 
        g_TransLabel = g_the_only_company[g_company.Translabel];
        if (!g_TransLabel) g_TransLabel='Transactional';
		
		g_CallInLabel = g_the_only_company[g_company.CallInLabel];
        if (!g_CallInLabel) g_CallInLabel='Call In';

        g_CompanyPopup = g_the_only_company[g_company.CompanyPopup]=='1';
        g_bill_used=(g_the_only_company[g_company.BillDateUsed] == '1');

        if (g_company[0][g_company.Requirement1used] == '1') g_req1Used = true;

        if (g_company[0][g_company.Requirement2used] == '1') g_req2Used = true;

        if (g_company[0][g_company.HasCustomAmount] == '1') {
            g_amount_name = g_company[0][g_company.CustomAmountName];
            g_has_custom_amount = true;
        }

        if (g_company[0][g_company.AskCloseQuestions] == '1') g_has_close_questions = true;

        var ssSource = null;
        var ssSource2 = null;
        var ssValuation = null;
        var ssSalesPerson = null;
        var ssCompany = null;
        var did_revive = false;

        g_callBack=null;
        g_timer='';

        var currentClockTarget = null;

        if (g_editMode){
            Header.addButton(new HeaderButton('OK', null, 'ok();'));
            Header.addButton(new HeaderButton('Cancel', null, 'cancel();'));
        }
        else {
            Header.addButton(new HeaderButton('OK-Close', null, 'ok();'));
            Header.addButton(new HeaderButton('OK-New', null, 'ok(true);'));
            Header.addButton(new HeaderButton('Cancel', null, 'cancel();'));
        }

        wr('<table cellspacing=0 cellpadding=5 style="border:0px solid green" bgcolor="white" width=100%><tr><td width=100%>');
        write_header();
        wr('</td></tr></table>');

        var ivoryBox = new IvoryBox('100%', null);
        ivoryBox.setType('grbl','.gif','#949597');
        ivoryBox.setBorderSize(8,9,8,9);

        var boxtop = ivoryBox.makeTop();
        var boxbottom = ivoryBox.makeBottom();

        var ivoryBox100 = new IvoryBox('100%', '100%');
        ivoryBox100.setType('grbl','.gif','#949597');
        ivoryBox100.setBorderSize(8,9,8,9);
        var boxtop100=ivoryBox100.makeTop();

        var ivoryBoxTall = new IvoryBox('100%', '100%');
        ivoryBoxTall.setType('grbl','.gif','#949597');
        ivoryBoxTall.setBorderSize(8,9,8,9);

        firstboxtop=ivoryBoxTall.makeTop();
        firstboxbottom=ivoryBoxTall.makeBottom();

		// optional 2nd argument is the callback function on ok
		function popupSubMilestones(location, highlightMissing)
		{
			//Need to derive companyID from displayed person's record
			var companyID = g_the_only_company[g_company.CompanyID];
		
			if(g_showaffiliates)
			{
				var pid = g_opportunity[g_oppCols.PersonID];
				for (var i=0; i< g_people.length; i++)
				{
					if (pid == g_people[i][g_people.PersonID])
					{
						companyID = g_people[i][g_people.CompanyID];
						break;
					}
				}
		
			}
			if (!highlightMissing) 
				highlightMissing = false;
			var fieldname = location;
			if (g_subMSEnabled)
			{
				var location_id = null;
				if (location == 'Requirement1')
				{
					location_id = 0;
					location = g_the_only_company[g_company.Requirement1];
				}
				else if (location == 'Person')
					location_id = 1;
				else if (location == 'Need')
					location_id = 2;
				else if (location == 'Money')
					location_id = 3;
				else if (location == 'Time')
					location_id = 4;
				else if (location == 'Requirement2')
				{
					location_id = 5;
					location = g_the_only_company[g_company.Requirement2];
				}
				else
				{
					alert('Invalid location in popupSubMilestones');
					return;
				}
				Windowing.dropBox.editOppWin = window;
				Windowing.dropBox.allChecked = getMilestone(fieldname) == '1';
				//How many submilestones?  If ANY are more than 10, set height = main browser window
				var cts=new Array();
				for(var i=0; i<6; i++)
				{
					cts[i]=0;
				}
				for (var k = 0; k < g_submilestones.length; k++)
				{
					if(0 == g_submilestones[k][g_submilestones.Enable]) continue;
					if(g_submilestones[k][g_submilestones.CompanyID] != companyID) continue;
					if(!g_req1Used && 0 == g_submilestones[k][g_submilestones.Location])
						continue;
					if(!g_req2Used && 5 == g_submilestones[k][g_submilestones.Location])
						continue;
					cts[g_submilestones[k][g_submilestones.Location]]++;
				}
				var high=0;
				for(var i=0; i<6; i++)
				{
					if(cts[i] > high) high = cts[i];
				}

				var winHeight = 540;
		
				Windowing.dropBox.highlightMissing = highlightMissing;
				var sn = 'SN=<?=$_REQUEST['SN']?>';

//				window.criteriaWin = Windowing.openSized2ndPromptScroll('../shared/subms_prompt.php?' + sn + '&fieldname=' + fieldname + '&location=' + location + '&location_id=' + location_id + '&companyid=' + companyID, winHeight, 600);
				
				   document.body.style.cursor = 'wait';
				    Windowing.dropBox.editOppWin = window;
					var w = 700;
					var h = 600;

					w += 32;
					h += 96;
					wleft = (screen.width - w) / 2;
					wtop = (screen.height - h) / 2;
					if (wleft < 0) {
						w = screen.width;
						wleft = 0;
					}
					if (wtop < 0) {
						h = screen.height;
						wtop = 0;
					}
					var win = window.open('../shared/subms_prompt.php?' 
							+ sn + '&fieldname=' + fieldname + '&location=' + location + '&location_id=' 
							+ location_id + '&companyid=' + companyID ,
					'Milestones',
					'width=' + w + ', height=' + h + ', ' +
					'left=' + wleft + ', top=' + wtop + ', ' +
					'location=no, menubar=no, ' +
					'status=no, toolbar=no, scrollbars=no, resizable=no');
					win.resizeTo(w, h);
					win.moveTo(wleft, wtop);
					win.focus();

				    document.body.style.cursor = 'auto';	
	
			}
			else {
				setMilestone(location, (document.getElementById('chk' + location).className == 'milestone_no'));
			}
		}
		
		function remove(reasonID, autoReviveDate)
		{
			if (reasonID)
			{
				var els = document.forms['form1'].elements;
				g_opportunity[g_oppCols.Category] = '9';
				var now = new Date();
				var today = (now.getMonth() + 1) + '/' + now.getDate() + '/' + now.getFullYear();
				g_opportunity[g_oppCols.RemoveDate] = today;
				g_opportunity[g_oppCols.RemoveReason] = reasonID;
				if(Windowing.dropBox.isFM)
					g_opportunity[g_oppCols.FM_took_place] = (Windowing.dropBox.FM_took_place.toUpperCase() == 'YES') ? 1 : 0;
				if (autoReviveDate)
					g_opportunity[g_oppCols.AutoReviveDate] = autoReviveDate;
				else
					g_opportunity[g_oppCols.AutoReviveDate] = "";
				g_timer=setInterval("remove_end();", 1);
				return;
			}
		
			if (!g_editMode)
			{
				var els = document.forms['form1'].elements;
				els.company.value = els.company.value.trim();
				els.firstMeetingTime.value = els.firstMeetingTime.value.trim();
				els.firstMeeting.value = els.firstMeeting.value.trim();
				if (els.company.value == '' || els.firstMeeting.value == '' || els.firstMeetingTime.value == '')
				{
					alert("You must enter at least a Company Name and First Meeting Date & ACCURATE Time to enter a 'No Potential' opportunity.");
					return;
				}
				var now = new Date();
				var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
				if (Dates.makeDateObj(els.firstMeeting.value).getTime() > today.getTime())
				{
					alert("A 'No Potential' opportunity cannot have a First Meeting in the future.");
					return;
				}
				g_opportunity[g_oppCols.Category] = 1;
		
			}
		
			Windowing.dropBox.isFM = g_opportunity[g_oppCols.Category] == 1;
			Windowing.dropBox.FM_took_place = 0;
			Windowing.dropBox.reasonCallback = remove;
			Windowing.dropBox.oppWin = this;
			var newwidth = 0;
			var newheight = 0;
			if (isNav)
			{
				newwidth = window.innerWidth;
				newheight = window.innerHeight;
			}
			else
			{
				newwidth = document.body.offsetWidth;
				newheight = document.body.offsetHeight;
			}
			Windowing.dropBox.defaultYesFMTookPlace = !g_editMode;
			g_RemoveWindow = Windowing.openSized2ndPrompt('../shared/remove.php?SN=<?= $_REQUEST['SN']?>', newheight, newwidth);
		
		}
		function revive(opp)
		{
			if (opp)
			{
				g_oldCategory = '9';
				g_dealID = parseInt(opp[g_oppCols.DealID], 10);
				var els = document.forms['form1'].elements;
				var now = new Date();	
	
				els.company.value = opp[g_oppCols.Company].unescape_html();
				els.division.value = opp[g_oppCols.Division].unescape_html();
				if (g_showcontact) els.contact.value = opp[g_oppCols.Contact].unescape_html();
		
				els.firstMeeting.value = opp[g_oppCols.FirstMeeting];
				els.firstMeetingTime.value = opp[g_oppCols.FirstMeetingTime];
				els.firstMeeting.value = format_date(els.firstMeeting.value);
		
				if (g_target_used)
				{
					els.targetDate.value = format_date(opp[g_oppCols.TargetDate]);
					if (els.targetDate.value=='') els.targetDate.value=Dates.formatShortDate(now);
				}
				setOfferingsList();
		
				els.estimatedDollarAmount.value = format_money(opp[g_oppCols.EstimatedDollarAmount]);
				els.estimatedDollarAmount.disabled=true;
				if(g_bill_used)
					els.BillDate.value = '';
				did_revive = true;
				category_changed(1);
		
				something_changed();
				return;
			}
			Windowing.dropBox.reviveCallback = revive;
			Windowing.openSized2ndPrompt('revive.php?SN=<?=$SN?>', 550, 780);
			//Windowing.openSized2ndPrompt('../shared/revive.html', 550, 780);
		}
		function setCompany(cell) {
			document.getElementById("select_company").value=cell.innerText;
			document.getElementById('selectWin').style.display='none';
		}

		function getCompanies()
		{
			var compName=document.getElementById("search_company").value;
				var req = getXMLHttpRequest();
				req.open('GET', "../data/ajax_get_company_list.php?person_id="+g_personID+"&compName="+compName+"&time="+(new Date()).getTime(), false);
				req.setRequestHeader('Content-Type', 'text/javascript');
				var str="";
				req.send(str);
				document.getElementById('selectWinList').innerHTML=req.responseText;
		}

		function onTransactional(elm) {			
		
			if(g_Transactional && g_CallIn && document.getElementById('transactional').checked && document.getElementById('callIn').checked) {
				alert('Please select either ' + g_TransLabel + ' or ' + g_CallInLabel + '.');
				elm.checked = false;
				return;
			}
			allSubAnswersChecked(true);
			showClosedAmount();
			something_changed();
		}
		
		function onAgency(elm) {	
			if ( elm.checked == true ) {
				g_opportunity[g_opps.Agency] = 1;
			}
			else {
				g_opportunity[g_opps.Agency] = 0;
			}

			something_changed();
		}		

//		print_r ( g_opp_products_xref, true ) ; 
        wr('<table width=100% cellpadding=5 cellspacing=0 style="border:0px solid cyan"><tr><td>');// margin table
        // ============================================== BEGINNING OF BODY
        wrln('<table width=100% cellpadding=3 cellspacing=0 style="border:0px solid cyan"><tr ><td rowspan=2 width=50% style="height:200px; border:0px solid green">');
        // ----------------------------------------------First column
        wrln(firstboxtop);
        wr('<table width=100% style="border:0px solid coral">');
        wr(makeBoxHeader('ID & Description'));
        wr('<tr><td nowrap align="left" class="plabel_eo" style="text-align:left;"  width="35%">');
        wr('<b>Company:&nbsp;');
        if (g_CompanyPopup) {
	        wr('<input type="button" class="etc-button" ');
	        wr(' onclick="document.getElementById(\'selectWin\').style.display=\'block\';">');
		}
        wr('</td><td width="65%">');
        wr('<input id="select_company" onfocus="CantChange(\'company\');" onkeypress="something_changed()" onchange="something_changed()" name="company" maxlength="100" type="text" style="width: 240px;">');
        wr('</td></tr>');
        wr('<tr><td nowrap align="left"  class="plabel_eo" style="text-align:left;" >');
        wr('<b>'+((g_the_only_company[g_company.DeptLabel]!='')?g_the_only_company[g_company.DeptLabel]:'Div/Dept/Loc')+':&nbsp;</td><td>');
        wr('<input onkeypress="something_changed()" onchange="something_changed()" name="division" maxlength="100" type="text" style="width: 240px;">');
        wr('</td></tr>');

        if (g_showcontact) {
            wr('<tr><td nowrap align="left"  class="plabel_eo" style="text-align:left;" >');
            wr('<b>'+((g_the_only_company[g_company.ContactLabel]!='')?g_the_only_company[g_company.ContactLabel]:'Contact')+':&nbsp;</td><td>');
            wr('<input onkeypress="something_changed()" onchange="something_changed()" name="contact" maxlength="100" type="text" style="width: 240px;">');
            wr('</td></tr>');
        }

        if (g_source2_used) {
            document.write('<tr valign="bottom">');
            document.write('<td class="plabel_eo" style="text-align:left;" >');
            document.write(g_the_only_company[g_company.Source2Label]+':');
            document.write('</td><td><input type="text" id="select_source2"  onchange="something_changed();" style="width: 240px;">');
            document.write('</td>');
            document.write('</tr>');
        }
        else {
          wr('<tr><td>&nbsp</td><td></td></tr>');
        }

        if (g_source_used) {
            document.write('<tr valign="bottom">');
            document.write('<td class="plabel_eo" style="text-align:left;" >');
            document.write('Source:');
            document.write('</td><td><input type="text" id="select_source"  onchange="something_changed();" style="width: 240px;">');
            document.write('</td>');
            document.write('</tr>');
        }

        if(g_valuation_used) {
            document.write('<tr  valign="bottom">');
            document.write('<td class="plabel_eo" style="text-align:left;" >');
            document.write(g_ValLabel+':');
            document.write('</td><td><input type="text" id="select_valuation" onchange="something_changed();" style="width: 240px;">');
            document.write('</td>');
            document.write('</tr>');
        }

        if (g_admin_mode) {
            document.write('<tr  valign="bottom">');
            document.write('<td class="plabel_eo" style="text-align:left;">');
            document.write('Salesperson:');
            document.write('</td><td><input type="text" id="select_salesperson"  onchange="onChangePerson()" style="width: 240px;">');
            document.write('</td>');
            document.write('</tr>');
        }

        if (g_Transactional) {
       		wr('<tr valign="bottom">');
       		wr('<td class="plabel_eo" style="text-align:left;">');
       		wr(g_TransLabel);
       		wr(':</td><td><input type="checkbox" id="transactional" name="transactional" onclick="onTransactional(this)">');
			 if ( g_the_only_company[g_company.PendingReport] == 1  ){
				wr('&nbsp; &nbsp; <span class="plabel_eo">Agency:</span> &nbsp; &nbsp; <input type="checkbox" id="agency" name="agency" onclick="onAgency(this)">'); 
				
			 }  
        	wr('</td></tr>');
        }
		else if ( g_the_only_company[g_company.PendingReport] == 1  ){
       		wr('<tr valign="bottom">');
       		wr('<td class="plabel_eo" style="text-align:left;">');
       		wr(' Agency'); // change to Agency
			if ( g_the_only_company[g_company.PendingReportDefaultValue] == 1 ) $PendingValue = 'checked' ; 
       		wr(':</td><td><input type="checkbox" id="agency" name="agency" onclick="onAgency(this)" '+ $PendingValue +'>');
			
    // 		wr(':</td><td><input type="checkbox" id="agency" name="agency" value="1" onclick="onAgency(this)">');
			//xxxx
        	wr('</td></tr>');		
		}
		
        if (g_CallIn) {
       		wr('<tr valign="bottom">');
       		wr('<td class="plabel_eo" style="text-align:left;">');
       		wr(g_CallInLabel);
       		wr(':</td><td><input type="checkbox" id="callIn" name="callIn" onclick="onTransactional(this)">');
        	wr('</td></tr>');
        }

        wr('</table>');
        document.writeln(firstboxbottom);


        wr('</td><td valign="middle" style="height:200px; border:0px solid red; padding:0px 0px 0px 0px ">'); 
        // ---------------------------------------------- Second column
        //wrln('<div id="div_questions">');
        wrln(boxtop);
        wrln('<table width=100% style="border:0px solid red">');
        wr(makeBoxHeader('Dates & Times'));

        if (g_target_used)
        {
            document.write('<tr valign="bottom">');
            document.write('<td  class="plabel_eo" style="text-align:left;" width=50%>');
            document.write('Date Assigned:');
            if (g_admin_mode)
            document.write('</td><td align="right"><input type="text" id="edit_dateid" onkeypress="something_changed()" onchange="something_changed(); check_date(\'edit_dateid\', \'Date Assigned\', \'nofuture\');" name="targetDate" ');
            else
            document.write('</td><td align="right"><input type="text" id="edit_dateid" name="targetDate" readonly="true"');
            document.write('style="width: 100px;text-align:right" >');
            wr('<img src="../images/spacer.gif" width=3>');
            if (g_admin_mode)
            {
                document.write('<input type="button" id="targetDateCal" class="etc-button" ');
                document.write('onclick="show_calendar(\'targetDate\')">');
            }
            else
            {
                document.write('<span style="font-size:1pt; width:24px;">&nbsp;</span>');
            }
            document.write('</td>');

            document.write('</tr>');
        }

        document.write('<tr valign="bottom">');
        document.write('<td  class="plabel_eo" style="text-align:left;" width="60%">');

        //  First Meeting Date
        var fm = catLabel('First Meeting');
        wr(fm + ' Date:');
        wr('</td><td align="right"><input readonly onkeypress="something_changed();" onkeydown="clearThis(this,event)" onchange="something_changed(); check_date(\'firstMeeting\', \'First Meeting\'); processFirstMeeting()" name="firstMeeting" id="firstMeeting" type="text"');
        wr('style="width: 100px;text-align:right">')

        wr('<img src="../images/spacer.gif" width=3>');
        wr('<input type="button" name="firstMeetingCal" class="etc-button" ');
        wr('    onclick="show_calendar_clock(\'firstMeeting\', \'firstMeetingTime\')">');
        wr('    </td></tr><tr><td class="plabel_eo" style="text-align:left;">');

        //  First Meeting Time
        wr(fm + ' Time:&nbsp </td><td align="right"><input readonly onkeypress="something_changed()" onkeydown="clearThis(this,event)" onchange="something_changed(); check_time(\'firstMeetingTime\', \'First Meeting Time\'); daysSinceFM()" type="text" style="width: 100px;text-align:right"');
        wr(' name="firstMeetingTime" id="firstMeetingTime">');
        wr('<img src="../images/spacer.gif" width=3>');
        wr('<input type="button" name="firstMeetingTimePopup" class="etc-button" ');
        wr(' onclick="show_calendar_clock(\'firstMeeting\', \'firstMeetingTime\')">');
        wr('</td></tr>');

        //  Days since First Meeting
        wr('<tr id="trDaysSinceFM" style="visibility:hidden;"><td class="plabel_eo" style="text-align:left;">');
        wr('Sales Cycle Days:</td><td align="right" style="padding-right:28px"><input readonly="true" type="text" style="width: 100px;text-align:right;"');
        wr('    id="sinceFirstMeetingTime" name="sinceFirstMeetingTime">');
        wr('</td>');
        wr('</tr>');

        wrln('</table>');
        document.writeln(boxbottom);
        //wr('&nbsp;');
        wr('</td></tr><tr><td>');
        wrln(boxtop);

        wrln('<table id="panelnext" width=100%>');
        wr('<tr><td class="plabel_eo" style="text-align:left;" nowrap width="50%">');
        wr('Next Meeting Date:</td>');
        wr('<td align="right"><input readonly onkeypress="something_changed()" onkeydown="clearThis(this,event)" onchange="something_changed(); check_date(\'nextMeeting\', \'Next Meeting\');chkNextMeeting();" type="text" style="width: 100px;text-align:right"');
        wr(' name="nextMeeting" id="nextMeeting">');
        wr('<img src="../images/spacer.gif" width=3>');
        wr('<input type="button" name="nextMeetingCal" class="etc-button" onclick="show_calendar_clock(\'nextMeeting\', \'nextMeetingTime\')">');
        wr('</td></tr>');
        wr('<tr><td class="plabel_eo" style="text-align:left;" nowrap width="50%">Next Meeting Time:</td><td align="right"><input readonly onkeypress="something_changed()" onkeydown="clearThis(this,event)" onchange="something_changed();check_time(\'nextMeetingTime\', \'Next Meeting Time\');" type="text" style="width:100px;text-align:right"');
        wr(' id="nextMeetingTime" name="nextMeetingTime">');
        wr('<img src="../images/spacer.gif" width=3><input type="button"   name="nextMeetingTimePopup" class="etc-button" ');
        wr(' onclick="show_calendar_clock(\'nextMeeting\', \'nextMeetingTime\')">');
        wr('</td></tr>');
        wrln('</table>');
        document.writeln(boxbottom);

        wr('</td></tr><tr><td colspan=2>');

        // ------------------------------------------------- start of second horizontal block
        wrln(makeBoxMiddleTop());
        wr('<table width="100%">');
        wr('<tr><td nowrap align="left" width="17%" class="plabel_eo">');
        wr('Offerings:</td><td width="33%">');
        wr('<input type="text" id="listOfferings" name="listOfferings" style="width:200px" readonly="true">');
        wr('<img src="../images/spacer.gif" width=3>');
        wr('<input type="button" name="btnOfferings" id="btnOfferings" class="edit-button"  ');
        wr('onclick="onClickedEditOfferings('+ g_the_only_company[g_company.PendingReport] +');">');
		// for offerings2.html var = 1 
        wr('</td><td id="estLabel" nowrap align="left" width="26%" style="padding-left:15px; padding-right:30px;" class="plabel_eo" style="text-align:right;">');
        //document.write(g_amount_name);
        //wr('Dollar amount:&nbsp;');
        wr('Estimated ' + g_amount_name + ':</td><td width="15%" align="right" style="padding-right:28px">');
        wr('<input onkeypress="something_changed()" onchange="something_changed()" id="estimatedDollarAmount" disabled');
        wr(' name="estimatedDollarAmount" type="text" style="width: 100px;text-align:right" readonly>');
        wr('</td></tr>');
        wr('</table>');
        document.writeln(makeBoxMiddleBottom());
        wr('</td></tr></table>');
        //-------------------------------------------------- start of row with push buttons
        wr('<table width=100% cellpadding=5 cellspacing=0><tr>');
        wr('<td width=50% align="center">');
        wrln(boxtop100);
        wr('<table cellpadding=0 cellspacing=0 border=0 width=100%><tr><td width=100% align="center">');
        wr('<table cellpadding=0 cellspacing=2 border=0><tr>');
        if (g_target_used && g_editMode == true)
        wr('<td id="radioT" class="panel" height=90 width=54  onclick="category_changed(10)" title="'+catLabel('Target')+'" style="font-size:10pt">' + vt('T') + '</td>');
        wr('<td id="radioFM" class="panel" height=90 width=54 onclick="category_changed(1)" title="'+catLabel('First Meeting')+'" style="font-size:10pt">' + vt('FM') + '</td>');

        wr('<td><table cellpadding=0 cellspacing=0 border=0><tr>');
        wr('<td id="radioI" class="panelmid" height=56 width=54 onclick="category_changed(2)" title="'+catLabel('Information Phase')+'" style="font-size:10pt">' + vt('IP') + '</td>');
        wr('<td id="radioDP" class="panelmid" height=56 width=54 onclick="category_changed(4)" title="'+catLabel('Decision Point')+'" style="font-size:10pt">' + vt('DP') + '</td>');
        wr('</tr><tr><td id="radioIS" class="panelsmall" height=34 width=54 onclick="category_changed(3)" title="'+catLabel('Information Phase')+' '+catLabel('Stalled')+'" style="font-size:10pt">' + catLabel('IP')+catLabel('S')+'</td>');
        wr('<td id="radioDPS" class="panelsmall" height=34 width=54 onclick="category_changed(5)" title="'+catLabel('Decision Point')+' '+catLabel('Stalled')+'" style="font-size:10pt">' + catLabel('DP')+catLabel('S') + '</td>');
        wr('</tr></table></td>');

        wr('<td id="radioC" class="panel" height=90 width=54 onclick="category_changed(6)" title="'+catLabel('Closed')+'" style="font-size:10pt">' + vt('C') + '</td>');
        if(g_editMode == false)
        {
            wr('<td height=90 width=5><img src="../images/spacer.gif" width=5></td>');
            wr('<td><table cellpadding="0" cellspacing="0" border=0><tr>');
            wr('<td id="radioR" class="panel" height=45 width=90 onclick="category_changed(9);remove()" style="font-size:9pt;background-image: url(../images/edop-button-rot.gif);">Revive</td>');
            wr('</tr><tr>');
            wr('<td id="radioKill" class="panel" height=45 width=90 onclick="category_changed(9);remove()" style="font-size:9pt;background-image: url(../images/edop-button-rot.gif);">No<br>Potential</td>');
            wr('</tr></table></td>');
        }
        else
        {
            wr('<td height=90 width=20><img src="../images/spacer.gif" width=20></td>');
            wr('<td id="radioR" class="panel" height=90 width=54 onclick="category_changed(9);remove()" style="font-size:7pt">' + vt('Remove') + '</td>');
        }
        wr('</tr></table>');

        wr('</td></tr></table>');
        wr(boxbottom);


        wr('</td>');
        wr('<td width=50% height=50%>');
        wrln(boxtop100);
        wr('<table  width=100% border=0 >');
        wr('<tr><td id="closedlabel" class="taglabel_disabled_eo">Date '+catLabel('Closed')+':</td><td align="right"><input type="text" style="width:100px;text-align:right;" disabled=true name="actualCloseDate" id="actualCloseDate" onkeypress="something_changed()" onchange="something_changed(); check_date(\'actualCloseDate\', \'Close Date\'); daysSinceFM();">');
        wr('<img src="../images/spacer.gif" width=3>');
        wr('<input type="button" disabled=true name="actualCloseCal" class="etc-button" onclick="show_calendar(\'actualCloseDate\')"></td>');
        wr('</tr>');

		wr('<tr id="rowClosedAmount"><td id="closedlabel2" class="taglabel_disabled_eo" width=20%>'+catLabel('Closed')+'&nbsp;');
        wr(g_amount_name + ':');
        wr('</td><td style="padding-right: 28px;" align="right"><input type="text" style="width:100px;text-align:right;" name="actualDollarAmount" disabled=true onchange="something_changed();">');
        wr('</td></tr>');

        if (g_bill_used)
        {
            wr('<tr><td class="taglabel_eo" style="color: white;font-weight:bold">');
            var BillDatePrompt=g_the_only_company[g_company.BillDatePrompt];
            if (BillDatePrompt=='') {
                BillDatePrompt='Bill Date:';
            }
            wr(BillDatePrompt);
            wr('</td><td align="right"><input type="text" style="width:100px;text-align:right;" name="BillDate" id="BillDate" disabled=true  onkeypress="something_changed()" onchange="something_changed(); check_date(\'BillDate\', \''+BillDatePrompt+'\');">');
            wr('<img src="../images/spacer.gif" width=3>');
            wr('<input type="button" name="BillDateCal" class="etc-button" onclick="show_calendar(\'BillDate\')">');
            wr('</td></tr>');
        }
        if (g_admin_mode)
        {
            wr('<tr><td style="text-align:left" class="plabel_eo" id="labelAdjEst">');
            wr('Adj. Est. '+catLabel('Closed')+' Date:</td><td align="right"><input type="text" style="width:100px;text-align:right;" name="adjustedCloseDate" id="adjustedCloseDate"  onkeypress="something_changed()" onchange="something_changed(); check_date(\'adjustedCloseDate\', \'Adjusted Close Date\');">');
            wr('<img src="../images/spacer.gif" width=3>');
            wr('<input type="button" name="adjustedCloseCal" class="etc-button" onclick="if (!document.getElementById(\'adjustedCloseDate\').disabled) show_calendar(\'adjustedCloseDate\')">');
            wr('</td></tr>');
        }

        wr('</table>');
        document.writeln(boxbottom);
        wr('</td></tr></table>');

        //-------------------------------------------------------------- Start of milestones
        wr('<table width="100%" cellspacing=0 cellpadding=5 border=0>');
        wr('<tr valign="center">');

        var milecnt=4;
        if (g_req1Used) milecnt++;
        if (g_req2Used) milecnt++;
        var milewidth=(100/milecnt)+'%';

        if (g_req1Used) {
            wr('<td id="chkRequirement1" class="milestone_no" width="'+milewidth+'"');
            wr(' onclick="popupSubMilestones(\'Requirement1\')"  align="center">');
            if (!g_subMSEnabled)
            wr('<input type="checkbox" id="checkboxRequirement1">');
            wr(makeBoxMileTop('Requirement1'));
            wr(g_the_only_company[g_company.Requirement1]);
            wr(makeBoxMileBottom('Requirement1'));
            wrln('</td>');
        }

        wr('<td id="chkPerson" class="milestone_no" width="'+milewidth+'" onclick="popupSubMilestones(\'Person\')" align="center">');
        if (!g_subMSEnabled) {
            document.write('<input type="checkbox" id="checkboxPerson">');
        }
        var lblPerson = g_the_only_company[g_company.MS2Label] == '' ? 'Person' : g_the_only_company[g_company.MS2Label];
        wr(makeBoxMileTop('Person'));
        document.write(lblPerson);
        wr(makeBoxMileBottom('Person'));
        wr('</td>');

        wr('<td id="chkNeed" class="milestone_no" width="'+milewidth+'" onclick="popupSubMilestones(\'Need\')" align="center">');
        if (!g_subMSEnabled) {
            document.write('<input type="checkbox" id="checkboxNeed">');
        }
        var lblNeed = g_the_only_company[g_company.MS3Label] == '' ? 'Need' : g_the_only_company[g_company.MS3Label];
        wr(makeBoxMileTop('Need'));
        document.write(lblNeed);
        wr(makeBoxMileBottom('Need'));
        wr('</td>');

        wr('<td id="chkMoney" class="milestone_no" width="'+milewidth+'" onclick="popupSubMilestones(\'Money\')" align="center">');
        if (!g_subMSEnabled)
        document.write('<input type="checkbox" id="checkboxMoney">');
        var lblMoney = g_the_only_company[g_company.MS4Label] == '' ? 'Money' : g_the_only_company[g_company.MS4Label];
        wr(makeBoxMileTop('Money'));
        document.write(lblMoney);
        wr(makeBoxMileBottom('Money'));
        wr('</td>');

        wr('<td id="chkTime" class="milestone_no" width="'+milewidth+'" onclick="popupSubMilestones(\'Time\')"  align="center">');
        if (!g_subMSEnabled)
        document.write('<input type="checkbox" id="checkboxTime">');
        var lblTime = g_the_only_company[g_company.MS5Label] == '' ? 'Time' : g_the_only_company[g_company.MS5Label];
        wr(makeBoxMileTop('Time'));
        document.write(lblTime);
        wr(makeBoxMileBottom('Time'));
        wr('</td>');

        if (g_req2Used)
        {
            document.write('<td id="chkRequirement2" class="milestone_no" width="'+milewidth+'"');
            document.write(' onclick="popupSubMilestones(\'Requirement2\')" align="center">');
            if (!g_subMSEnabled)
            document.write('<input type="checkbox" id="checkboxRequirement2">');
            wr(makeBoxMileTop('Requirement2'));
            document.write(g_the_only_company[g_company.Requirement2]);
            wr(makeBoxMileBottom('Requirement2'));
            document.writeln('</td>');
        }

        wr('</tr></table>');

        wr('<table width=100% border=0><tr><td align="right">');
        if (g_adminCall) { //If called from Admin program (not manager as administrator)
        	document.writeln('<nobr><input type="checkbox" name="checkEnforceRules" checked onclick="g_enforce_rules = ((this.checked) ? true : false);"> Enforce Rules&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');			
		}

        wr('</td></tr></table>');
        wr('</td></tr></table>');   // end margin table

        if (g_CMInterface) document.getElementById('division').focus();
        </script>
        </form>
    </body>

    <script language="JavaScript">

        document.forms['form1'].elements.actualDollarAmount.onblur=onBlurDollar;
        //document.forms['form1'].elements.actualDollarAmount.onfocus=onFocusDollar;

        if (hasMilestoneQuestions)
        {
            var temp_script = document.createElement('SCRIPT');
            temp_script.src = '../data/answer_check.php?oppid=' + g_dealID;
            document.body.appendChild(temp_script);
        }

        // why do we only do this in editMode?
        // because if we're adding a new opp, there can't be any stored answers
        if (g_editMode && g_dealID>=0)
        {
            var temp_script = document.createElement('SCRIPT');
            temp_script.src = '../data/get_subanswers.php?SN=<?=$_REQUEST['SN']?>&oppid=' + g_dealID + '&req1Used=' + (g_req1Used ? 1 : 0) + '&req2Used=' + (g_req2Used ? 1 : 0);
            document.body.appendChild(temp_script);
        }

        setCategoryLabels();

    </script>
	<script language="javascript" src="../javascript/main_spreadsheet.js"></script>
</html>
<?
function getMonth ($a){ 

	$months_list = array (  'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' );
	$parts = explode(" ", $a);
	if ( count ($parts ) > 2 ) {
		for ($v = 0 ; $v < 12 ; $v++){
			if ( $parts[0] == $months_list[$v] ) return ($v + 1); 	
		}	
	}
	else {
		$part = explode("/", $parts[0]) ;
		return (int) $part[0] ; 
	}
	
	
	return 0 ; 
}
?>
