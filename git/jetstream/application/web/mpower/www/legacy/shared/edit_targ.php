<?php
/**
* @package Shared
*/

define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');
require_once(BASE_PATH . '/include/class.DbConnManager.php');
require_once(BASE_PATH . '/include/class.SqlBuilder.php');
require_once(BASE_PATH . '/include/class.ReportingTree.php');

SessionManager::Init();
SessionManager::Validate();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>M-Power</title>
<link rel="stylesheet" type="text/css" href="../css/sharedstyles.css">

<script language="javascript" src="../javascript/utils.js,windowing.js,category_labels.js,smartselect.js,save_board.js,saver2.js,pageformat.js,edit_targ.js"></script>
<script language="JavaScript">
	g_personID = -1;
	var hash = '<?=$hash?>';

	var pos = hash.indexOf('_');
	if (pos != -1)
	{
		g_dealID = parseInt(hash.substr(0, pos), 10);
		if (g_dealID == -1) g_editMode = false;

		hash=hash.substr(++pos);
		var pos = hash.indexOf('_');
		if(pos==-1) g_personID = hash;
		else
		{
			g_personID = hash.substr(0,pos);
			hash=hash.substr(++pos);
			g_valuationLevel=hash;
		}
	}
	document.write("<script src='../data/get_company_list.php?reqPersonId=" + parseInt(g_personID, 10) + "'></scr"+"ipt>");
</script>
</head>

<body style="background-color:#949597" onload="loaded()">

<script language="JavaScript">

<?php
	$targetsalesperson = $_SESSION['tree_obj']->GetTarget(true);
	$peers = is_array($targetsalesperson) && count($targetsalesperson) ? implode(',',array_values($targetsalesperson)) : $mpower_effective_userid;
	$companyList="('$mpower_companyid')";
	
	include_once('../data/_base_utils.php');
	include_once('../data/_query_utils.php');
	include_once('../data/get_user.php');
	include_once('../data/get_company.php');
	include_once('../data/get_valuations.php');

?>

g_temp_id_counter = 0 - Math.ceil(Math.random() * 100000);

function getTempID() {
    return g_temp_id_counter--;
}

function make_new_opportunity()
{
    var ret = new Array();
    for (var k in g_opps)
    {
        var n = parseInt(k, 10);
        if (!isNaN(n))
            continue;
        if (k.charAt(0) == '_' || k == 'toPipes' || k == 'quicksort' || k == 'radixsort')
            continue;
        ret[ret.length] = '';
    }
    ret[g_opps.DealID] = getTempID();
    return ret;
}

isNav = (navigator.appName.indexOf("Netscape") != -1) ? true : false;
isIE = (navigator.appName.indexOf("Microsoft") != -1) ? true : false;
isDOM = (document.getElementById) ? true : false;

window.onresize=positionSelects;
var g_string_hello = 'Hello';
g_admin_mode = true;
g_editMode = true;
g_somethingWasChanged = false;
g_dealID = -1;
g_personID = -1;
g_opportunity = null;
g_valuationID = -1;
g_valuationLevel = -1;
g_sourceID = -1;
g_source2ID = -1;
g_oppCols = new Object();
//g_color = 'ivory';
g_oldCategory = -1;
g_old_cat = -1;
g_prev_cat = -1;
g_boardLocations = new Array(null, 'First Meeting', 'Information Phase', 'Information Phase - Stalled','Decision Point', 'Decision Point - Stalled', 'Closed');

g_req1Used = false;
g_req2Used = false;

g_the_only_company = g_company[0];

g_valuation_used = (g_company[0][g_company.ValuationUsed] == '1');
g_source_used = (g_company[0][g_company.SourceUsed] == '1');
g_source2_used = (g_company[0][g_company.Source2Used] == '1');

if (g_company[0][g_company.Requirement1used] == '1') g_req1Used = true;
if (g_company[0][g_company.Requirement2used] == '1') g_req2Used = true;
if (g_company[0][g_company.HasCustomAmount] == '1')
{
	g_amount_name = g_company[0][g_company.CustomAmountName];
	g_has_custom_amount = true;
}
if (g_company[0][g_company.AskCloseQuestions] == '1') g_has_close_questions = true;

<?php

	$sql="SELECT * FROM people 
			WHERE isSalesperson = 1 AND 
				  PersonID IN ($peers)
			ORDER BY LastName";
	dump_sql_as_array('g_people', $sql);
	
	$first = date('Y-m-d h:i:s');
	$sql = "SELECT * FROM opportunities WHERE CompanyID = '$mpower_companyid'";
	$sql .= " AND (Category != '6' OR ActualCloseDate >= '$first')";
	$sql .= " AND (Category != '9')";
	$sql .= " AND PersonID in ($peers)";
	dump_sql_as_array('g_opps', $sql); 
	
	dump_sql_as_array('g_snoozealerts', "SELECT * FROM snoozealerts WHERE PersonID IN ($peers)");
	dump_sql_as_array('g_logins', "SELECT * FROM Logins WHERE CompanyID = '$mpower_companyid' AND PersonID in ($peers)");
	
	include_once('../data/get_sources.php');
	include_once('../data/get_sources2.php');
	
	dump_sql_as_array('g_opp_products_xref', "SELECT * FROM Opp_Product_XRef where CompanyID IN ($companyList) order by DealID, Seq");
	dump_sql_as_array('g_products', "select * from products where CompanyID IN ($companyList) and Deleted = '0' order by Name");

?>

g_editedSubAnswers = new Array();
g_adminCall = false;
g_standalone = false;

var did_revive = false;
var ssSource = null;
var ssSource2 = null;
var ssValuation = null;
var ssSalesPerson = null;
var ssCompany = null;


function initSmartSelect()
{
	if(g_source_used) ssSource = new SmartSelect(document.getElementById('select_source'), g_sources, g_sources.SourceID, g_sources.Name, g_sourceID);
	if(g_source2_used) ssSource2 = new SmartSelect(document.getElementById('select_source2'), g_sources2, g_sources2.Source2ID, g_sources2.Name, g_source2ID);
	if (g_source2_used && g_sources2.length > 10) ssSource2.listSize = 10;
	if (g_source_used && g_sources.length > 8) ssSource.listSize = 10;
	if (g_valuation_used)
	{
		var arrVals = getValArray();
		arrVals.noSort = true;
		ssValuation = new SmartSelect(document.getElementById('select_valuation'), arrVals, 0, 1, g_valuationLevel);
		if (arrVals.length > 10) ssValuation.listSize = 8;
	}
	if (g_admin_mode)
	{
		var arrPeople = getPeopleArray();
		ssSalesPerson = new SmartSelect(document.getElementById('select_salesperson'), arrPeople, 0, 1, g_personID);
		if (arrPeople.length > 10) ssSalesPerson.listSize = 6;
	}
	ssCompany = new SmartSelect(document.getElementById('select_company'), g_company_list, g_company_list.Company, new Array(g_company_list.Company, g_company_list.Division, g_company_list.Contact));
	ssCompany.exact=false;
	ssCompany.setSelectWidth(600);
	ssCompany.onIndexChanged=onCompanyIndexChanged;

}

function loaded()
{
	init_column_ids();

	if (g_editMode)	init_edit_window();
	else init_new_window();

	initSmartSelect();
}


function ok(do_another)
{

	if (!do_another) do_another = false;

	if (check_rules())
	if (true)
	{
		if (g_somethingWasChanged)
			broadcast_changes(!do_another);

		if (do_another)
		{
			document.forms.form1.reset();

			g_sourceID = -1;
			g_source2ID = -1;
			g_valuationLevel = -1;

			if(g_admin_mode) ssSalesPerson.setVal(g_personID);
			if(g_source_used) ssSource.setVal(g_sourceID);
			if(g_source2_used) ssSource2.setVal(g_source2ID);
			init_new_window();
		}
	}
}

function remove(reasonID)
{
	if (reasonID)
	{
		var els = document.forms['form1'].elements;
		for (var i = 0; i < els.category.length; ++i)
			els.category[i].checked = false;
		g_opportunity[g_oppCols.Category] = '9';

		var now = new Date();
		var today = (now.getMonth() + 1) + '/' + now.getDate() + '/' + now.getFullYear();
		g_opportunity[g_oppCols.RemoveDate] = today;
		g_opportunity[g_oppCols.RemoveReason] = reasonID;

		broadcast_changes(true);
		window.close();
		return;
	}

	Windowing.dropBox.isFM = g_opportunity[g_oppCols.Category] == 1;
	Windowing.dropBox.reasonCallback = remove;
	var newwidth = 0;
	var newheight = 0;
	if (isNav)
	{
		newwidth = window.innerWidth;
  		newheight = window.innerHeight;
	}
	else
	{
		newwidth = document.body.offsetWidth;
  		newheight = document.body.offsetHeight;
	}
	var newwin = Windowing.openSized2ndPrompt('../shared/remove.html', newheight, newwidth);
}

function write_header()
{
	if (g_admin_mode) g_admin_mode = true;
	else
	{
		var level = g_user[0][g_people.Level];
		if(level >= 2) g_admin_mode = true;
	}

	var hash = '<?=$hash?>';
	var pos = hash.indexOf('_');

	if (pos == -1) return; 
	g_dealID = parseInt(hash.substr(0, pos), 10);
	if (g_dealID == -1) g_editMode = false;

	hash=hash.substr(++pos);
	var pos = hash.indexOf('_');
	if(pos==-1) g_personID = hash;
	else
	{
		g_personID = hash.substr(0,pos);
		hash=hash.substr(++pos);
		g_valuationLevel=hash;
	}

	var header = '';
	if (g_editMode)
		header = 'Edit ' + catLabel('Target');
	else
		header = 'New ' + catLabel('Target');

	if (g_personID != '-1')
		var info = get_salesperson_info(g_personID);
	document.write(makeBoxHeader(header));
}

function init_new_window()
{
	g_opportunity = make_new_opportunity();
	g_dealID = g_opportunity[g_oppCols.DealID];

	var now = new Date();
	var today = Dates.formatShortDate(new Date());
	var TargetDate = document.getElementById('targetDate');
	TargetDate.value = today;
}


function revive(opp)
{
	if (opp)
	{
		g_oldCategory = '9';
		g_dealID = parseInt(opp[g_oppCols.DealID], 10);
		var els = document.forms['form1'].elements;
		els.company.value = opp[g_oppCols.Company];
		els.division.value = opp[g_oppCols.Division];
		els.contact.value = opp[g_oppCols.Contact];
		var now = new Date();
		var today = (now.getMonth() + 1) + '/' + now.getDate() + '/' + now.getFullYear();
		els.targetDate.value = today;
		g_valuationLevel = opp[g_oppCols.VLevel];
		g_sourceID = opp[g_oppCols.SourceID];
		g_source2ID = opp[g_oppCols.Source2ID];
		setSSVals();
		something_changed();
		return;
	}
	Windowing.dropBox.reviveCallback = revive;
	Windowing.openSized2ndPrompt('revive.php?SN=<?=$SN?>', 550, 780);
}

</script>
<div align="center">
<form name="form1">

<table cellpadding="0" cellspacing="0">
	<tr>
		<td>
<table border=0>
<tr>
	<td class="title" colspan=2>
		<script language="JavaScript">
		<!--
		write_header();
		// -->
		</script>
	</td>
</tr>
<tr>
	<td valign="top">
		<table id="tagtable" border=0 cellspacing=0 cellpadding=0 width="100%">
		<tr>
			<td nowrap align="left" class="taglabel" style="font-size:16; font-family:Arial; font-weight:bold;">
				Date Assigned:
			</td><td>
				<input onkeypress="something_changed()" onchange="something_changed(); check_date('targetDate', 'Date Assigned','nofuture');return false;" name="targetDate" id="targetDate" type="text"
				style="width: 100px"><img src="../images/spacer.gif" width=3><input type="button" name="targetDateCal" class="etc-button" value="..." style="width: 20px"
				onclick="show_calendar('targetDate')">
			</td>
		</tr>
		<tr valign="middle" class="taglabel" style="font-size:16; font-family:Arial; font-weight:bold">
			<td nowrap align="left">
				Company:
			</td><td>
				<input id="select_company" maxlength="100" onchange="something_changed()" type="text" name="company" style="width: 200px">
			</td>
		</tr>
		<tr>
			<td nowrap align="left" class="taglabel" style="font-size:16; font-family:Arial; font-weight:bold">
				<script language="Javascript">
					document.writeln(((g_the_only_company[g_company.DeptLabel]!='')?g_the_only_company[g_company.DeptLabel]:'Div/Dept/Loc')+":")
				</script>
			</td><td>
				<input maxlength="100" onkeypress="something_changed()" onchange="something_changed()" name="division" type="text" style="width: 200px">

			</td>
		</tr>
		<tr>
			<td nowrap align="left" class="taglabel" style="font-size:16; font-family:Arial; font-weight:bold">
				Contact:
			</td><td>
				<input maxlength="100" onkeypress="something_changed()" onchange="something_changed()" name="contact" type="text" style="width: 200px">
			</td>
		</tr>
<tr>
	<script language="JavaScript">
		if(g_source2_used)
		{
			document.write('<td align="left" nowrap  class="plabel" style="font-size:16; font-family:Arial; font-weight:bold">');
			document.write(g_the_only_company[g_company.Source2Label]+':</td>');
			document.write('<td>');
			document.write('<input type="text" id="select_source2"  style="width: 200px" onchange="something_changed();">');
			document.write('</td>');
			document.write('</tr>');
			document.write('<tr>');
		}
		if(g_source_used)
		{
			document.write('<td align="left" nowrap  class="plabel" style="font-size:16; font-family:Arial; font-weight:bold">');
			document.write('Source:</td>');
			document.write('<td>');
			document.write('<input type="text" id="select_source"  style="width: 200px" onchange="something_changed();">');
			document.write('</td>');
		}
	</script>
</tr>
<tr>
	<script language="JavaScript">
		if(g_valuation_used)
		{
			document.write('<td class="plabel" align="left" nowrap  style="font-size:16; font-family:Arial; font-weight:bold">');
			document.write('Valuation:</td>');
			document.write('<td>');
			document.write('<input type="text" id="select_valuation" style="width: 200px" onchange="something_changed();">');
			document.write('</td>');
		}
	</script>
</tr>
<tr>
	<script language="JavaScript">
		if(g_admin_mode)
		{
			document.write('<td class="plabel" align="left" style="font-size:16; font-family:Arial; font-weight:bold">');
			document.write('Salesperson:</td>');
			document.write('<td>');
			document.write('<input type="text" style="width: 200px" id="select_salesperson"  onchange="something_changed();">');
			document.write('</td></tr>');
		}
	</script>
</tr>
</table>
</td></tr>

<tr><td><img src="../images/spacer.gif" height=5></td></tr>
<tr valign="bottom">
	<td>
		<script language="JavaScript">
		<!--
		if (g_editMode)
			document.writeln('<button type="button" class="sized_command_new_gb_70" id="buttonRemRev" onclick="remove()">Remove...</button>&nbsp;');
		else
			document.writeln('<button type="button" class="sized_command_new_gb_70" onclick="revive()">Revive</button>&nbsp;');

		if (g_editMode)
		{
			document.write('<button type="button" class="sized_command_new_gb_70" onclick="ok()">OK</button>&nbsp;');
			document.write('<button type="button" class="sized_command_new_gb_70" onclick="cancel()">Cancel</button></nobr>');
		}
		else
		{
			document.write('<button type="button" class="sized_command_new_gb_70" onclick="ok()">OK-Close</button>&nbsp;');
			document.write('<button type="button" class="sized_command_new_gb_70" onclick="ok(true)">OK-New</button>&nbsp;');
			document.write('<button type="button" class="sized_command_new_gb_70" onclick="cancel()">Cancel</button></nobr>');
		}

		// -->
		</script>
	</td>
</tr>
</table>
		</td>
	</tr>
</table>

</form>
</div>
</body>
</html>