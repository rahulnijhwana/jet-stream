<?php
/**
 * @package Data
 */

include_once('_query_utils.php');
include_once('log_obj.php');

$UPDATE_ERROR_MSG = '';

function writelog($message)
{
	// $handle=fopen("/usr/local/apache/htdocs/logs/update.log","a+");
	// $logtime=date("Y M j D G:i:s");
	// fwrite($handle,"$logtime $message\n");
	// fclose($handle);
	LogFile::WriteLine("$message");
}

// so as not to add extra parameters, I reused ignore_field_list
// if it is equal to 'ChangedByID', then it indicates that the ChangedByID field should be added to the field list
// and the $currentuser_loginid should be added to the data

function update_rows($table_name, $field_list, $ps_data, $index_field_name, $ignore_field_list)
{
	file_put_contents('/webtemp/remove.txt', "no error ", FILE_APPEND) ; 
	writelog("$table_name: Updating Rows");
	//$handle=fopen("/usr/local/apache/htdocs/logs/login.log","a+");
	//$logtime=date("Y M j D G:i:s");
	//fwrite($handle,"$logtime Updating Rows for $table_name\n");
	//fclose($handle);

	global $UPDATE_ERROR_MSG,$currentuser_loginid;

	$bUseChangedByID=$ignore_field_list=='ChangedByID';

	$ignore_array = explode(',', $ignore_field_list);
	$ignore_count = count($ignore_array);
	$ignore_map = array();
	for ($k = 0; $k < $ignore_count; ++$k)
	{
		$temp_str = trim($ignore_array[$k]);
		$ignore_map[$temp_str] = $temp_str;
	}

	$field_array = explode(',', $field_list);
	// RJP 5-22-03 - need two single quotes in str_replace
	$value_array = explode('|', str_replace("'", "''", str_replace("\\\"", "\"", str_replace("\\'","'", $ps_data))));
	$field_count = count($field_array);
	$value_count = count($value_array);
	if (($value_count % $field_count) != 0)
	{
		$UPDATE_ERROR_MSG = "Error updating $table_name: Odd number of values in incoming data ($value_count) compared with field count ($field_count)";
		for ($k = 0; $k < $field_count; ++$k)
		$UPDATE_ERROR_MSG .= " " . $field_array[$k] . "=" . $value_array[$k];
		writelog("$table_name: $UPDATE_ERROR_MSG");
		return false;
	}
	$row_count = $value_count / $field_count;
	$index_index = -1;
	for ($k = 0; $k < $field_count; ++$k)
	{
		if ($field_array[$k] == $index_field_name)
		{
			$index_index = $k;
			break;
		}
	}
	if ($index_index == -1)
	{
		$UPDATE_ERROR_MSG = "Error updating row $table_name: Could not find the index field, $index_field_name.";
		writelog("$table_name: $UPDATE_ERROR_MSG");
		return false;
	}
	$bad_query_array = array();
	$bad_query_count = 0;
	for ($n = 0; $n < $row_count; ++$n)
	{
		$first_time = true;
		$query_str = "UPDATE $table_name SET ";
		for ($k = 0; $k < $field_count; ++$k)
		{
			if ($k != $index_index && !isset($ignore_map[$field_array[$k]]))
			{
				if ($first_time == false)
				$query_str .= ', ';
				else
				$first_time = false;
				$sql_field_name = $field_array[$k];
				if ($sql_field_name == 'Use')
				$sql_field_name = '[' . $sql_field_name . ']';
				if (strlen($value_array[($n * $field_count) + $k]) == 0)
				$query_str .= $sql_field_name . ' = NULL';
				else
				$query_str .= $sql_field_name . " = '" . $value_array[($n * $field_count) + $k] . "'";
				// RJP 5-22-2003: field values MUST
				// be in single quotes for the update
				// query to work in both Windows and
				// Unix.  Otherwise, on Unix, the
				// values will be incorrectly
				// interpreted as column names.
			}
		}
		if ($bUseChangedByID) $query_str.=", ChangedByID=$currentuser_loginid ";

		$query_str .= " WHERE $index_field_name = " . $value_array[($n * $field_count) + $index_index];
		//$UPDATE_ERROR_MSG .= $query_str . "\n";
		file_put_contents('/webtemp/remove.txt', "update row ". $query_str, FILE_APPEND) ; 
		$temp_result = mssql_query($query_str);
		if ($temp_result == false)
		{
			$bad_query_array[$bad_query_count] = $query_str;
			++$bad_query_count;
		}
	}
	if (0 < $bad_query_count)
	{
		$UPDATE_ERROR_MSG = "The was an error updating $table_name: The following queries failed:<br>";
		for ($k = 0; $k < $bad_query_count; ++$k)
		$UPDATE_ERROR_MSG .= '<nobr><pre>' . $bad_query_array[$k] . '</pre></nobr>';
		writelog("$table_name: $UPDATE_ERROR_MSG");
		return false;
	}
	else
	{
		writelog("$table_name: Success");
		return true;
	}
}

?>
