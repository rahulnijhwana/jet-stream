<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

/*
global $currentuser_level, $currentuser_isadmin, $currentuser_viewsiblings,
			$currentuser_personid, $currentuser_eula,
			$mpower_userid, $mpower_pwd, $mpower_companyid;
*/

if (!check_user_login()) exit("alert('mpower_userid=$mpower_userid mpower_pwd=$mpower_pwd mpower_companyid=$mpower_companyid');");

if (isset($opplist) && strlen(trim($opplist)) > 0) {
	dump_sql_as_array('g_subanswers_disp', "SELECT * FROM SubAnswers WHERE DealID in ($opplist) order by DealID, SubID");
}

?>