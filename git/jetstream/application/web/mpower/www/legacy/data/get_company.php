<?php
/**
 * @package Data
 */

//include_once('_base_utils.php');
include_once('_query_utils.php');

if (!defined('BASE_PATH')) define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once BASE_PATH . '/class.SessionManager.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';

//if (!check_user_login()) exit();

print("// ----- get_company -----\n\n");

//$result = mssql_query("select * from company where CompanyID = '$mpower_companyid'");

$query = "SELECT * FROM company WHERE CompanyID = ?";
$query = SqlBuilder()->LoadSql($query)->BuildSql(array(DTYPE_INT, $mpower_companyid));
$row = DbConnManager::GetDb('mpower')->Execute($query);

$currentCompany=$row;
if (isset($company_lean_and_mean) && $company_lean_and_mean)
{
	$row['Requirement1trend'] = '';
	$row['Requirement2trend'] = '';
}

$record = array();
foreach($row->GetFieldDef() as $key=>$comp) {
	$record[$key] = $row[0][$key];
}

dump_assoc_as_array('g_company', $record);

?>