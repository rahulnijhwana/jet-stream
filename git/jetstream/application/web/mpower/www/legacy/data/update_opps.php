<?php
/**
 * @package Data
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');
require_once(BASE_PATH . '/include/class.DbConnManager.php');
require_once(BASE_PATH . '/include/class.SqlBuilder.php');

SessionManager::Init();
SessionManager::Validate();

include_once('_base_utils.php');
include_once('_update_utils.php');
include_once('log_obj.php');
include_once('autorevive.php');

function print_result($result)
{
	print("window.result = '$result';\n");
	print("</script>\n");
	close_db();
	exit();
}

function writeCMlog($message)
{
//$handle=fopen("/usr/local/apache/htdocs/logs/CM.log","a+");
//$logtime=date("Y M j D G:i:s");
//fwrite($handle,"$logtime $message\n");
//fclose($handle);
    LogFile::WriteLine($message);
}


print("<script language=\"JavaScript\">\n");

if (!check_user_login()) print_result('Invalid login');
if (!isset($mpower_data) || !strlen($mpower_data)) print_result('No data to save');
if (!isset($mpower_fieldlist) || !strlen($mpower_fieldlist)) print_result('Field list is missing');

print("</script>\n");

// determine if there is a CM, and set vars accordingly
// LF Optimize there is now a global array that holds the company info, this should be used instead of a query
$sql="SELECT CMInterface, CMPath, CMLogin, CMPass FROM company where companyid = $mpower_companyid";
$result=mssql_query($sql);
$CMInterface=false;
if ($row=mssql_fetch_assoc($result))
{
	if ($row['CMInterface']=='1')
	{

		$CMInterface=true;
		$CMPath=$row['CMPath'];
		$CMLogin=$row['CMLogin'];
		$CMPass=$row['CMPass'];
		$CMopps=array();
	}
}
else writeCMlog('Get company failed');

print('<div id="errmsg">');
$ok = update_rows('opportunities', $mpower_fieldlist, $mpower_data, 'DealID', 'ChangedByID');
CheckAutoRevive($mpower_companyid);

//include('calc_salescycles.php');
print('</div>');
print("<script language=\"JavaScript\">\n");

if ($CMInterface)
{
	include_once("../$CMPath/cm_interface.php");
	$field_array = explode(',', $mpower_fieldlist);
	// RJP 5-22-03 - need two single quotes in str_replace
	// this explosion should match the one in _update_utils.php (update_rows)
	$value_array = explode('|', str_replace("\\\"", "\"", str_replace("\\'","''", $mpower_data)));
	$field_count = count($field_array);
	$value_count = count($value_array);
	$row_count = $value_count / $field_count;

	$CMOpps=array();
	$i=0;
	for ($n = 0; $n < $row_count; ++$n)
	{
		$CMOpp=array();
		for ($k = 0; $k < $field_count; ++$k)
		{
			$CMOpp[$field_array[$k]]=$value_array[$i];
			++$i;
		}
		if ($CMOpp['CMID']!='-1')
		{
			$sql = "select CMID from people where PersonID=".$CMOpp['PersonID'];
			$cmidResult = mssql_query($sql);
			if ($cmidResult && $cmidRow = mssql_fetch_assoc($cmidResult))
				$CMOpp['PersonCMID'] = $cmidRow['CMID'];
			array_push($CMOpps,$CMOpp);
		}
	}
	writeCMlog($CMLogin.' '.$CMPass);
//	writeCMlog(print_r($CMOpps,TRUE));
	CM_Connect($CMLogin, $CMPass);
	CM_UpdateOpps($CMOpps);
	writeCMlog("Done from CM_UpdateOpps test");
}

if ($ok) print_result('ok');
else if (strlen($UPDATE_ERROR_MSG)) print_result($UPDATE_ERROR_MSG);
else print_result('Update opportunities failed.');
close_db();

?>
