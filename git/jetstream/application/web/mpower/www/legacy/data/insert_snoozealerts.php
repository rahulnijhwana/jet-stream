<?php
/**
 * @package Data
 */

define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');
require_once(BASE_PATH . '/include/class.DbConnManager.php');
require_once(BASE_PATH . '/include/class.SqlBuilder.php');

SessionManager::Init();
SessionManager::Validate();

include_once('_base_utils.php');
include_once('_insert_utils.php');

function print_result($result)
{
	print("<script language=\"JavaScript\">\n");
	print("window.result = '$result';\n");
	print("</script>\n");
	close_db();
	exit();
}


if (!check_user_login()) print_result('Invalid login.');
if (!isset($mpower_data) || !strlen($mpower_data)) print_result('No data to save.');
if (!isset($mpower_fieldlist) || !strlen($mpower_fieldlist)) print_result('Field list is missing.');

$rows = explode("\n", $mpower_data);
$ok = true;
$len = count($rows);

print('<div id="errmsg">');
for ($i = 0; $i < $len; ++$i)
{
	$result = insert_row('snoozealerts', $mpower_fieldlist, trim($rows[$i]), 'SnoozeAlertsID', '');
	if (!$result) $ok = false;
}
print('</div>');

if ($ok) print_result('ok');
else print_result('Insert snoozealerts failed.');

close_db();

?>
