<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');
include_once('_base_utils.php');
include_once('_update_utils.php');

SessionManager::Init();
SessionManager::Validate();

function print_result($result)
{
	print("<script language=\"JavaScript\">\n");
	print("window.result = '$result';\n");
	print("</script>\n");
	close_db();
	exit();
}

if (!check_user_login()) print_result('Invalid login' . check_user_login() );
if (!isset($mpower_data) || !strlen($mpower_data)) print_result('No data to save');
if (!isset($mpower_fieldlist) || !strlen($mpower_fieldlist)) print_result('Field list is missing');

print('<div id="errmsg">');
$result = update_rows('snoozealerts', $mpower_fieldlist, $mpower_data, 'SnoozeAlertsID', '');
print('</div>');

if ($result) print_result('ok');
else if (strlen($UPDATE_ERROR_MSG)) print_result($UPDATE_ERROR_MSG);
else print_result('Update snoozealerts failed');

close_db();

?>
