<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

print("// ----- get_sources -----\n\n");

$SourceList=array();	// this array associates the string with the ID
$SourceName=-1;
$SourceID=-1;

// callback function for dump_result that is processed for every row, used to build CM info
function BuildSourceList($resultset,$row)
{
	GLOBAL $SourceList,$SourceName,$SourceID;
	if ($SourceName == -1)
	{
		$len = mssql_num_fields($resultset);
		for ($i = 0; $i < $len; ++$i)
		{
			$field = mssql_field_name($resultset, $i);
			if ($field=='SourceID')
			{
				$SourceID=$i;
			}
			else if ($field=='Name')
			{
				$SourceName=$i;
			}
		}
	}
	$SourceList[$row[$SourceName]]=$row[$SourceID];
}

$result = mssql_query("select * from sources where CompanyID IN ($companyList) and Deleted = '0' order by Name");
if ($result)
{
	dump_resultset_as_array('g_sources', $result, 'BuildSourceList');
}

$result = mssql_query("select * from sources where CompanyID IN ($companyList) and Deleted = '1'");
if ($result)
{
	dump_resultset_as_array('g_removed_sources', $result, 'BuildSourceList');
}

?>