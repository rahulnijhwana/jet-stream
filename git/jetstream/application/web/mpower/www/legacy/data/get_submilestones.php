<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

print("// ----- get_submilestones -----\n\n");
dump_sql_as_array('g_submilestones', "SELECT * FROM SubMilestones WHERE CompanyID IN ($companyList) ORDER BY Location, Seq");

?>
