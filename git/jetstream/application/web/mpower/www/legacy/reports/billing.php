<?php
/**
 * @package Reports
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once (BASE_PATH . '/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include ('_report_utils.php');
include ('_date_params.php');

$sql = "select BillDatePrompt from company where CompanyID=$mpower_companyid";
$result = mssql_query($sql);
$row = mssql_fetch_assoc($result);
$BillDatePrompt = $row['BillDatePrompt'];

/********************************************/
begin_report('Expected ' . $BillDatePrompt . ' Report');

if (!isset($treeid)) {
	include ('GetSalespeopleTree.php');
	print_tree_prompt(ANYBODY);
	end_report();
	exit();
}

if (!isset($dateFrom) || !isset($dateTo)) {
	$sortcol = 0;
	$ord = 0;
	$mt_nextmonth = mktime(0, 0, 0, date("m") + 1, date("d"), date("Y"));
	$nextmonth = date("m/d/Y", $mt_nextmonth);
	
	print_date_params($treeid, $sortcol, $ord, date("m/d/Y"), $nextmonth);
	if (!isset($dateFrom))
		$dateFrom = "N/A";
	if (!isset($dateTo))
		$dateTo = "N/A";
	end_report();
	exit();
}

include ('TableUtils.php');
include ('jim_utils.php');
include ('GetSalespeopleTree.php');
include ('category_labels.php');

define(SHADE_MED, "MED_BLUE");
define(SHADE_LIGHT, "LIGHT_BLUE");

function array_csort($marray, $column, $sortord) {
	$sortarr = array ();
	foreach ($marray as $row) {
		$sortarr[] = $row[$column];
	}
	array_multisort($sortarr, $sortord, $marray);
	return $marray;
}

function GetCompanyName($id) {
	$sql = "select * from company where CompanyID='$id'";
	$ok = mssql_query($sql);
	if (($row = mssql_fetch_assoc($ok))) {
		return $row['Name'];
	}
	return 'Unknown!';
}

function EnterTotals(&$arrTotals, $key, $Bill) {
	for($i = 0; $i < count($arrTotals); $i++) {
		if ($key == $arrTotals[$i][0]) {
			$arrTotals[$i][1] += $Bill;
			return;
		}
	}
	$tmp = array ();
	$tmp[0] = $key;
	$tmp[1] = $Bill;
	array_push($arrTotals, $tmp);

}

function GetMonthQuarter($m) {
	$m--;
	return floor($m / 3) + 1;
}

$firstqtr = 5;
$lastqtr = 0;
$firstyr = 9999;
$lastyr = 0;

function MakeTotals(&$arrTotals, $BillDate, $Bill) {
	global $firstqtr, $lastqtr, $firstyr, $lastyr;
	$info = getdate(strtotime($BillDate));
	$y = $info['year'];
	$m = $info['mon'];
	$q = GetMonthQuarter($m);
	$m--;
	EnterTotals($arrTotals, "$y", $Bill);
	EnterTotals($arrTotals, "$y|$q", $Bill);
	EnterTotals($arrTotals, "$y|X|$m", $Bill);
	
	if ($firstyr > $y || ($firstyr == $y && $firstqtr > $q)) {
		$firstyr = $y;
		$firstqtr = $q;
	}
	
	if ($lastyr < $y || ($lastyr == $y && $lastqtr < $q)) {
		$lastyr = $y;
		$lastqtr = $q;
	}
}

function GetTotals($key, &$Bill) {
	global $arrTotals;
	$Estimated = 0;
	$Actual = 0;
	for($i = 0; $i < count($arrTotals); $i++) {
		if ($key == $arrTotals[$i][0]) {
			$Bill = '$' . number_format($arrTotals[$i][1]);
			return;
		}
	}
	$Bill = '&nbsp;';
}

function PrintQtrRow($yr, $q) {
	$arrMonths = array ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec');
	$mInd = ($q - 1) * 3;
	BeginTableRowData(1);
	GetTotals("$yr|$q", $Bill);
	PrintTableCellEx("$yr - Q$q", "align='center' width=15% bgcolor='lightblue'");
	PrintTableCellEx("$Bill", "align='right' width=14% bgcolor='lightblue'");
	for($j = $mInd; $j < $mInd + 3; $j++) {
		$key = "$yr|X|$j";
		GetTotals($key, $Bill);
		PrintTableCellEx("$arrMonths[$j]", "align='center' width=10%  ");
		PrintTableCellEx("$Bill", "align='right' width=14%");
	}
	EndTableRow();
}

function PrintTotalsHeader($yr) {
	GetTotals("$yr", $Estimated, $Actual);
	$str = "<table border=0 cellspacing=0 cellpadding=3 bgcolor='ivory' width='70%'>";
	$str .= "<tr><td align='left' width='20%' border=0><b>$yr</td>";
	$str .= "<td align='right' width='10%' border=0>Estimated:</td><td align='right' width='20%' border=0>$Estimated</td>";
	$str .= "<td align='right' width='10%' border=0>Actual:</td><td align='right' width='20%' border=0>$Actual</td></tr></table>";
	print($str);
}

function PrintTotals($yr, $qStart, $qStop) {
	global $firstqtr, $lastqtr, $firstyr, $lastyr;
	global $arrTotals;
	//	GetTotals("$yr", $Estimated, $Actual);
	//	PrintTableSpan(12, "<b>$yr Est: $Estimated Act: $Actual");
	//	PrintTotalsHeader($yr);
	//	for ($i=1; $i<5; $i++)
	print "<table border=2 cellspacing=0 cellpadding=3 bgcolor='ivory' width='100%'>";
	for($y = $firstyr, $q = $firstqtr; $y < $lastyr || ($y == $lastyr && $q <= $lastqtr);) {
		PrintQtrRow($y, $q);
		if (++$q > 4) {
			$y++;
			$q = 1;
		}
	}
	EndTable();
}

function GetDateComponent($timestamp, $key) {
	$info = getdate($timestamp);
	return $info[$key];
}

function PrintDetail($dFrom, $dTo, $sortind, $sortord) {
	global $arrDeals, $treeid, $BillDatePrompt;
	
	$sn = $_GET['SN'];
	
	$url = $_SERVER['PHP_SELF'] . "?treeid=$treeid";
	if ($dFrom != "" && $dFrom != "N/A") {
		$uFrom = urlencode($dFrom);
		$uTo = urlencode($dTo);
		$url .= "&dateFrom=$uFrom&dateTo=$uTo";
	}
	
	$arrColHeads = array ("<b>Company<br>(<A HREF=\"$url&sortcol=0&ord=0&SN=$sn\">A</A> <A HREF=\"$url&sortcol=0&ord=1&SN=$sn\">D </A>)", "<b>Salesperson<br>(<A HREF=\"$url&sortcol=1&ord=0&SN=$sn\">A</A> <A HREF=\"$url&sortcol=1&ord=1&SN=$sn\">D</A>)", "<b>Offering<br>(<A HREF=\"$url&sortcol=2&ord=0&SN=$sn\">A</A> <A HREF=\"$url&sortcol=2&ord=1&SN=$sn\">D</A>)", "<b>Sales Cycle Locaton<br>(<A HREF=\"$url&sortcol=3&ord=0&SN=$sn\">A</A> <A HREF=\"$url&sortcol=3&ord=1&SN=$sn\">D</A>)", "<b>" . GetCatLbl("Closed") . " Date<br>(<A HREF=\"$url&sortcol=4&ord=0&SN=$sn\">A</A> <A HREF=\"$url&sortcol=4&ord=1&SN=$sn\">D</A>)", "<b>Actual Monthly Billing<br>(<A HREF=\"$url&sortcol=5&ord=0&SN=$sn\">A</A> <A HREF=\"$url&sortcol=5&ord=1&SN=$sn\">D</A>)", "<b>$BillDatePrompt<br>(<A HREF=\"$url&sortcol=6&ord=0&SN=$sn\">A</A> <A HREF=\"$url&sortcol=6&ord=1&SN=$sn\">D</A>)", "<b>Estimated Monthly Billing<br>(<A HREF=\"$url&sortcol=7&ord=0&SN=$sn\">A</A> <A HREF=\"$url&sortcol=7&ord=1&SN=$sn\">D</A>)", "<b>Valuation<br>(<A HREF=\"$url&sortcol=8&ord=0&SN=$sn\">A</A> <A HREF=\"$url&sortcol=8&ord=1&SN=$sn\">D</A>)");
	
	//	BeginTable();
	print("<table border=2 cellspacing=0 cellpadding=3 bgcolor='ivory' width='100%'>");
	
	BeginTableRowData(2);
	PrintTableSpan(9, "<br><b>Sort Col: $sortind Sort Order: $sortord");
	EndTableRow();
	
	BeginTableRowData(2);
	PrintTableSpan(9, "<br><b>Detail");
	EndTableRow();
	
	PrintColHeads($arrColHeads, 1, 0, count($arrColHeads), FALSE);
	
	//	$arrFieldNames= array('ActualCloseDate','SalesName','Company','ProductName','EstimatedDollarAmount','ActualDollarAmount');
	$arrFieldNames = array ('Company', 'SalesName', 'ProductName', 'Category', 'ActualCloseDate', 'ActualDollarAmount', 'BillDate', 'EstimatedDollarAmount', 'VLevel');
	
	$arrDeals = array_csort($arrDeals, $arrFieldNames[$sortind], $sortord);
	
	for($i = 0; $i < count($arrDeals); $i++) {
		
		// if there's no close date, then only show estimated, otherwise show actual $
		

		if ($arrDeals[$i]['ActualCloseDate'] == 0) {
			$d = '&nbsp;';
			$actual = '&nbsp;';
			$estimated = '$' . number_format($arrDeals[$i]['EstimatedDollarAmount']);
		} else {
			$d = date("m/d/Y", $arrDeals[$i]['ActualCloseDate']);
			$actual = '$' . number_format($arrDeals[$i]['ActualDollarAmount']);
			$estimated = '&nbsp;';
		}
		
		if ($arrDeals[$i]['VLevel'] == -1)
			$V = '&nbsp;';
		else
			$V = $arrDeals[$i]['VLevel'];
		$b = date("m/d/Y", $arrDeals[$i]['BillDate']);
		BeginTableRow();
		PrintTableCell($arrDeals[$i]['Company'] . '<br>' . $arrDeals[$i]['Division']);
		PrintTableCell($arrDeals[$i]['SalesName']);
		PrintTableCell(str_replace('|', '<br>', $arrDeals[$i]['ProductName']));
		//			PrintTableCellEx(GetCatLbl(GetLoc($arrDeals[$i]['Category'])),"align='center'");
		PrintTableCellEx($arrDeals[$i]['Category'], "align='center'");
		PrintTableCell($d);
		PrintTableCellEx($actual, "align='right'");
		PrintTableCell($b);
		PrintTableCellEx($estimated, "align='right'");
		PrintTableCellEx($V, "align='center'");
		
		EndTableRow();
	}
	EndTable();

}

if (!isset($sortcol))
	$sortcol = 0;

if ($ord == 0)
	$sortord = SORT_ASC;
else
	$sortord = SORT_DESC;

$total = 0;

$idList = GetSalespeopleTree($treeid);

$sql = "SELECT opportunities.Company, opportunities.Division, opportunities.EstimatedDollarAmount, opportunities.ActualDollarAmount,
opportunities.ActualCloseDate, opportunities.BillDate, opportunities.Category, opportunities.VLevel, people.FirstName, people.LastName,
substring(OrdIdList, 1, datalength(OrdIdList)/2 - 1) as ProductName
FROM
opportunities INNER JOIN people ON opportunities.PersonID = people.PersonID CROSS APPLY
(SELECT CONVERT(NVARCHAR(30), Products.Name) + '|' AS [text()]
FROM opp_product_xref INNER JOIN products ON opp_product_xref.productid = products.productid
WHERE opp_product_xref.DealID = opportunities.DealID
ORDER BY Products.Name
FOR XML PATH('')) as Dummy(OrdIdList)
WHERE
opportunities.Category < 9 AND people.PersonID IN ($idList)
AND opportunities.BillDate >= '$dateFrom' AND opportunities.BillDate <= '$dateTo'
ORDER BY opportunities.ActualCloseDate DESC";

//$sql = "SELECT opportunities.Company, opportunities.Division, opportunities.EstimatedDollarAmount, opportunities.ActualDollarAmount,";
//$sql .= " opportunities.ActualCloseDate, opportunities.BillDate, opportunities.Category, opportunities.VLevel, products.Name AS ProductName, people.FirstName, people.LastName";
//$sql .= " FROM opportunities INNER JOIN people ON opportunities.PersonID = people.PersonID";
//$sql .= " INNER JOIN products ON opportunities.ProductID = products.ProductID";
//$sql .= " WHERE opportunities.Category < 9 AND people.PersonID IN ($idList)";
//$sql .= " AND opportunities.BillDate >= '$dateFrom' AND opportunities.BillDate <= '$dateTo'";
//$sql .= " ORDER BY opportunities.ActualCloseDate DESC";

$ok = mssql_query($sql);
$arrDeals = array ();
$arrTotals = array ();
while (($row = mssql_fetch_assoc($ok))) {
	$tmp = array ();
	$tmp['Company'] = $row['Company'];
	$tmp['Division'] = $row['Division'];
	$tmp['ProductName'] = $row['ProductName'];
	$tmp['SalesName'] = $row['LastName'] . ', ' . $row['FirstName'];
	if (strlen($row['ActualCloseDate'])) {
		$tmp['ActualCloseDate'] = strtotime($row['ActualCloseDate']);
		// actual not included in quarter totals
		//		$bill=$row['ActualDollarAmount'];
		$tmp['ActualDollarAmount'] = $row['ActualDollarAmount'];
		$tmp['EstimatedDollarAmount'] = 0;
	} else {
		$tmp['ActualCloseDate'] = 0;
		$tmp['ActualDollarAmount'] = 0;
		$bill = $row['EstimatedDollarAmount'];
		$tmp['EstimatedDollarAmount'] = $bill;
		MakeTotals($arrTotals, $row['BillDate'], $bill);
	}
	//	$tmp['ActualDollarAmount'] = $row['ActualDollarAmount'];
	$tmp['BillDate'] = strtotime($row['BillDate']);
	//	$tmp['EstimatedDollarAmount'] = $row['EstimatedDollarAmount'];
	$tmp['Category'] = GetCatLbl(GetLoc($row['Category']));
	$tmp['VLevel'] = $row['VLevel'];
	array_push($arrDeals, $tmp);
}

/****************Print**********************/
/*
print("<meta http-equiv=\"Refresh\" content=\"10; URL=ClosedOpportunities_HTML.php?treeid=$treeid&dateFrom=$dateFrom&dateTo=$dateTo\">");


print("<br><br><b>     Please wait while your report is being prepared.<br><br>");

end_report();
*/

print_tree_path($treeid);

$strdate = date("M d Y", time());

$title = GetCatLbl("Closed") . " Opportunities Report";

$salestree = make_tree_path($treeid);
BeginTable();

//print("<table width='100%'>");
print("<table border=2 cellspacing=0 cellpadding=3 bgcolor='ivory' width='100%'>");

//print("<tr ><td align='center'><b>$title</td></tr>");


$companyname = GetCompanyName($mpower_companyid);
BeginTableRow();
PrintTableSpan(4, "<b>$companyname");
EndTableRow();

$daterange = "Date Range: $dateFrom to $dateTo";

BeginTableRow();
PrintTableSpan(4, "<b>$daterange<br><br>");
EndTableRow();
EndTable();
//print('<table width="100%">');
print("<table border=2 cellspacing=0 cellpadding=3 bgcolor='ivory' width='100%'>");
BeginTableRowData(2);
PrintTableSpan(12, "<b>Projected Revenues");
EndTableRow();

$endyear = GetDateComponent(strtotime($dateTo), 'year');
$startyear = GetDateComponent(strtotime($dateFrom), 'year');
$qStop = GetMonthQuarter(GetDateComponent(strtotime($dateTo), 'mon'));
$qStart = GetMonthQuarter(GetDateComponent(strtotime($dateFrom), 'mon'));

$year = $endyear;
EndTable();
PrintTotals($year--, $q1, $q2);

//EndTable();


if (!isset($sortcol))
	$sortcol = 0;

if ($ord == 0)
	$sortord = SORT_ASC;
else
	$sortord = SORT_DESC;
	
//print detail
PrintDetail($dateFrom, $dateTo, $sortcol, $sortord);

end_report();

close_db();

?>
