<?php
 /**
 * @package Reports
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_paul_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');

$CLabel = GetCatLbl('Closed');
//$reportName = 'Criteria Not Completed until '.$CLabel;
$reportName = 'Criteria Auto/Forced Completed';

$EarliestFromDate = '1/24/2007';

if (!isset($treeid))
{
	begin_report_orig($reportName);
	print_tree_prompt(MANAGEMENT);
	end_report_orig();
	exit();
}

if (!isset($dateFrom) || !isset($dateTo))
{
	begin_report_orig($reportName);
	?>
	<center style="font-family:Arial; font-size:12pt;">Please select the date span to be used for the inclusion of <?=$CLabel?> opportunities.</center>
	<?
	$sortcol=0;
	$ord=0;
	$tomorrow = date("m/d/Y", strtotime("+1 day"));
	print_date_params($treeid,$sortcol,$ord, "datefr:<$tomorrow", "dateto:<$tomorrow");
	if(!isset($dateFrom)) $dateFrom="N/A";
	if(!isset($dateTo)) $dateTo="N/A";
	?>
	<center style="font-family:Arial; font-size:12pt; color:red;">
		Earliest acceptable From Date for this report is <?=date('m/d/y', strtotime($EarliestFromDate))?>
	</center><br>
	<?
	end_report_orig();
	exit();
}

$sql = "select *, (FirstName+' '+LastName) as FullName from people where PersonID=$treeid";
$reportPerson = mssql_fetch_assoc(mssql_query($sql));

$sql = "select *, (FirstName+' '+LastName) as FullName from people where CompanyID=$mpower_companyid and Deleted<>1 and SupervisorID<>-1 and SupervisorID<>-3 order by LastName, FirstName";
$result = mssql_query($sql);

$allpeople = array();
while ($row = mssql_fetch_assoc($result))
	array_push($allpeople, $row);


$shortdateFrom = date('m/d/y', strtotime($dateFrom));
$shortdateTo = date('m/d/y', strtotime($dateTo));

begin_report($reportName);

$sql = "select * from company where CompanyID=$mpower_companyid";
$companyRow = mssql_fetch_assoc(mssql_query($sql));

$sql = <<<EOD

select
	SubID,
	Prompt,
	Location,
	IsHeading,
	Mandatory
from
	SubMilestones
where
	CompanyID=$mpower_companyid and
	Enable=1
order by
	Location,
	Seq

EOD;

$result = mssql_query($sql);

$subms = array();
$submsmap = array();
while ($row = mssql_fetch_assoc($result))
{
	array_push($subms, $row);
	$submsmap[$row['SubID']] = $row;
}

$idList = GetSalespeopleTree($treeid);
$personIDs = explode(',', $idList);


$peopleTotals = array();
for ($k = 0; $k < count($personIDs); ++$k)
{
	$peopleTotals[$personIDs[$k]] = array();
	$peopleTotals[$personIDs[$k]]['TotalAuto'] = 0;
	$peopleTotals[$personIDs[$k]]['Total'] = 0;
	for ($n = 0; $n < count($subms); ++$n)
		$peopleTotals[$personIDs[$k]][$subms[$n]['SubID']] = 0;
}

$sql = <<<EOD

select
	isnull(SubAnswers.CompletedLocation, 0) as CompletedLocation,
	SubAnswers.SubID,
	opportunities.PersonID,
	SubAnswers.DealID
from
	SubAnswers
inner join opportunities on
	SubAnswers.DealID = opportunities.DealID
where
	SubAnswers.DealID in
	(
		select DealID from opportunities where PersonID in ($idList)
		and Category=6 and ActualCloseDate between '$dateFrom' and '$dateTo'
	)
	
EOD;

$result = mssql_query($sql);

$dealids = array();
$grandTotals = array();
while ($row = mssql_fetch_assoc($result))
{
	if ($row['CompletedLocation'] == 6)
	{
		if (!isset($peopleTotals[$row['PersonID']]['TotalAuto'])) {
			$peopleTotals[$row['PersonID']]['TotalAuto'] = 0;
		}
		$peopleTotals[$row['PersonID']]['TotalAuto']++;
		
		if (!isset($peopleTotals[$row['PersonID']][$row['SubID']])) {
			$peopleTotals[$row['PersonID']][$row['SubID']] = 0;
		}
		$peopleTotals[$row['PersonID']][$row['SubID']]++;

		if (!isset($peopleTotals[$row['PersonID']]['Location'.$submsmap[$row['SubID']]['Location']])) {
			$peopleTotals[$row['PersonID']]['Location'.$submsmap[$row['SubID']]['Location']] = 0;
		}
		$peopleTotals[$row['PersonID']]['Location'.$submsmap[$row['SubID']]['Location']]++;
		if (!isset($grandTotals[$row['SubID']])) {
			$grandTotals[$row['SubID']] = 0;
		}
		$grandTotals[$row['SubID']]++;
		
	}

	if (!isset($peopleTotals[$row['PersonID']][$row['SubID'].'Total'])) {
		$peopleTotals[$row['PersonID']][$row['SubID'].'Total'] = 0;
	}
	$peopleTotals[$row['PersonID']][$row['SubID'].'Total']++;
	$dealids[$row['DealID']] = true;
}

$TotalOppCount = 0;
$dealids = implode(',', array_keys($dealids));
if ($dealids != '')
{
	$sql = "select PersonID from opportunities where DealID in ($dealids)";
	$result = mssql_query($sql);

	while ($row = mssql_fetch_assoc($result))
	{
		if (!isset($peopleTotals[$row['PersonID']]['OppCount'])) {
			$peopleTotals[$row['PersonID']]['OppCount'] = 0;
		}
		$peopleTotals[$row['PersonID']]['OppCount']++;
		$TotalOppCount++;
	}
}

$sql = <<<EOD

select
	(FirstName+' '+LastName) as FullName, LastName, PersonID
from
	people
where
	SupervisorID = $treeid and
	Deleted<>1
order by
	LastName asc
	
EOD;

$result = mssql_query($sql);
$sp = array();
while ($row = mssql_fetch_assoc($result))
	array_push($sp, $row);

$locs = array("", "First Meeting", "Information Phase", "Information Phase Stalled", "Decision Point", "Decision Point Stalled", "Closed", "", "", "Removed", "Target");

function nbsp($val)
{
	if ($val == '')
		return '&nbsp;';
	else
		return $val;
}

function zero($val)
{
	if ($val == '')
		return '0';
	else
		return $val;
}

function RollupTotalAuto($PersonID, $SubID)
{
	global $allpeople, $peopleTotals;
	$t = 0;
	if (isset($peopleTotals[$PersonID]))
		$t = $peopleTotals[$PersonID][$SubID];
	for ($k = 0; $k < count($allpeople); ++$k)
	{
		if ($allpeople[$k]['SupervisorID'] == $PersonID)
			$t += RollupTotalAuto($allpeople[$k]['PersonID'], $SubID);
	}
	return $t;
}

function RollupTotalOppCount($PersonID)
{
	global $allpeople, $peopleTotals;
	$t = 0;
	if (isset($peopleTotals[$PersonID]['OppCount']))
		$t = $peopleTotals[$PersonID]['OppCount'];
	for ($k = 0; $k < count($allpeople); ++$k)
	{
		if ($allpeople[$k]['SupervisorID'] == $PersonID)
			$t += RollupTotalOppCount($allpeople[$k]['PersonID']);
	}
	return $t;
}

?>

<style>

BODY
{
	font-family:Arial;
	font-size:10pt;
}

.ColumnHeader
{
	font-size:10pt;
	font-weight:bold;
	border-right:1px solid black;
	border-bottom:1px solid black;
	background-color:#DDDDDD;
}

.ColumnHeaderDarker
{
	font-size:13pt;
	font-weight:bold;
	border-right:1px solid black;
	border-bottom:1px solid black;
	background-color:#888888;
	color:white;
}

.ColumnHeaderDarkerThickLeft
{
	font-size:13pt;
	font-weight:bold;
	border-right:1px solid black;
	border-bottom:1px solid black;
	border-left:1px solid black;
	background-color:#888888;
	color:white;
}

.ColumnHeaderThickLeft
{
	font-size:10pt;
	font-weight:bold;
	border-right:1px solid black;
	border-bottom:1px solid black;
	border-left:1px solid black;
	background-color:#DDDDDD;
}

.ColumnHeaderThickLeftFlipped
{
	font-size:10pt;
	font-weight:bold;
	border-right:1px solid black;
	border-top:1px solid black;
	border-left:1px solid black;
	background-color:#DDDDDD;
	writing-mode: tb-rl;
	filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=2)
}

.TableCell
{
	border-right:1px solid black;
	border-bottom:1px solid black;
}

.TableCellThickLeft
{
	border-right:1px solid black;
	border-bottom:1px solid black;
	border-left:1px solid black;
}

table.sortable a.sortheader
{
	text-decoration: none;
	color: black;
}

table.sortable span.sortarrow
{
	text-decoration: none;
}

@media print
{
    .pagestart
    {
        page-break-before: always;
    }
}

</style>
<script src="../javascript/sorttable.js,utils.js"></script>
<!-- <div style="width:100%; height:100%; overflow:auto;"> -->

<center style="font-size:12pt; font-weight:bold;">
	<span class="OnlyScreenInline"><?=$reportName?><br></span>
	<span style="font-weight:normal; font-size:10pt;">(Criteria Not Completed until <?=$CLabel?>)</span><br>
	<?=make_tree_path($treeid)?><br><span style="font-size:4px;"><br></span>
</center>
<?
$cellwidth = 18;
$personwidth = (($cellwidth+8)*2);
$promptColWidth = 179;
$curSPIndex = 0;
$spPerPage = 10;
$rowHeight = 36;
?>
<div class="OnlyScreenBlock">
<?while ($curSPIndex < count($sp)):?>
	<?
		$spBreakIndex = $curSPIndex+$spPerPage;
		if ($spBreakIndex > count($sp))
			$spBreakIndex = count($sp);
		$curPageCount = $spBreakIndex-$curSPIndex;
		//$divWidth = 704;
		$divWidth = round($promptColWidth+$personwidth+($personwidth*$curPageCount))+16;
		$divWidth += ($spPerPage-$curPageCount)*1;
	?>
	<table cellpadding="3" cellspacing="0" style="border-left:1px solid black; border-top:1px solid black;font-family:Arial; font-size:8pt; background-color:white;">
		<thead style="display: table-header-group;">
			<tr>
				<td class="ColumnHeader" width="<?=$promptColWidth?>" style="border-top:1px solid black;" valign="middle" align="center">
					<?=$shortdateFrom?> - <?=$shortdateTo?><br>
					<span style="font-weight:normal;">Optional criteria are not reported</span>
				</td>
				<td colspan="2" class="ColumnHeaderThickLeftFlipped" style="border-bottom:1px solid black; font-size:14px;" align="left" nowrap>Report Totals</td>
				<?for ($k = $curSPIndex; $k < $spBreakIndex; ++$k):?>
					<td colspan="2" style="border-bottom:1px solid black;" class="ColumnHeaderThickLeftFlipped" align="left" nowrap><?=$sp[$k]['FullName']?></td>
				<?endfor;?>
			</tr>
			<tr>
				<td class="ColumnHeader" style="font-size:13px;" width="<?=$promptColWidth?>">Total <?=$CLabel?> Opportunities</td>
				<td class="ColumnHeaderThickLeft" colspan="2" align="center" nowrap><?=$TotalOppCount?></td>
				<?for ($k = $curSPIndex; $k < $spBreakIndex; ++$k):?>
					<td colspan="2" class="ColumnHeaderThickLeft" align="center" nowrap><?=zero(RollupTotalOppCount($sp[$k]['PersonID']))?></td>
				<?endfor;?>
			</tr>
			<tr>
				<td class="ColumnHeaderDarker" width="<?=$promptColWidth?>">&nbsp;</td>
				<td align="center" width="<?=$cellwidth?>" class="ColumnHeaderDarkerThickLeft" nowrap>#</td>
				<td align="center" width="<?=$cellwidth?>" class="ColumnHeaderDarker" nowrap>%</td>
				<?for ($k = $curSPIndex; $k < $spBreakIndex; ++$k):?>
					<td align="center" width="<?=$cellwidth?>" class="ColumnHeaderDarkerThickLeft" nowrap>#</td>
					<td align="center" width="<?=$cellwidth?>" class="ColumnHeaderDarker" nowrap>%</td>
				<?endfor;?>
			</tr>
		</thead>
	</table>
	<div style="width:<?=$divWidth?>px; height:405px; overflow-y:auto;">
	<table cellpadding="3" cellspacing="0" style="border-left:1px solid black; border-top:1px solid black;font-family:Arial; font-size:8pt; background-color:white;">
		<?
		$curloc = -1;
		?>
		<tbody>
			<?for ($k = 0; $k < count($subms); ++$k):?>
				<?
				$temploc = $subms[$k]['Location']+1;
				if ($temploc == 1 && $companyRow['Requirement1used'] != 1)
					continue;
				if ($temploc == 6 && $companyRow['Requirement2used'] != 1)
					continue;
				?>
				<?$colspan = (($spBreakIndex+1)*2)+1;?>
				<?if ($temploc != $curloc):?>
					<?
					$curloc = $temploc;
					//Criteria Not Completed by {{Closed}}
					?>
					<tr>
						<td class="ColumnHeader" colspan="<?=$colspan?>" height="<?=$rowHeight?>">
							<?if ($curloc == 1):?>
								<?=$companyRow['Requirement1']?>
							<?elseif ($curloc == 6):?>
								<?=$companyRow['Requirement2']?>
							<?else:?>
								<?=$companyRow['MS'.$temploc.'Label']?>
							<?endif;?>
						</td>
					</tr>
				<?endif;?>
				<tr>
					<?if ($subms[$k]['IsHeading'] == 1):?>
						<td class="TableCell" colspan="<?=$colspan?>" height="<?=$rowHeight?>"><b><?=$subms[$k]['Prompt']?></b></td>
						<?continue;?>
					<?else:?>
						<td class="TableCell" width="<?=$promptColWidth?>" height="<?=$rowHeight?>">
							<?=$subms[$k]['Prompt']?>
						</td>
					<?endif;?>
					<?if ($subms[$k]['Mandatory'] != 1):?>
						<td class="TableCellThickLeft" colspan="<?=$colspan-1?>" align="center">OPTIONAL</td>
					<?else:?>
						<?if ($grandTotals[$subms[$k]['SubID']] > 0):?>
							<td class="TableCellThickLeft" style="background-color:#DDDDDD;" width="<?=$cellwidth?>" align="right"><?=$grandTotals[$subms[$k]['SubID']]?></td>
							<td class="TableCell" style="background-color:#DDDDDD;" width="<?=$cellwidth?>" align="right"><?=round(($grandTotals[$subms[$k]['SubID']]/$TotalOppCount)*100)?></td>
						<?else:?>
							<td class="TableCellThickLeft" style="background-color:#DDDDDD;" width="<?=$cellwidth?>" align="right">&nbsp;</td>
							<td class="TableCell" style="background-color:#DDDDDD;" width="<?=$cellwidth?>" align="right">&nbsp;</td>
						<?endif;?>
						</td>
						<?for ($n = $curSPIndex; $n < $spBreakIndex; ++$n):?>
							<?$temptotal = RollupTotalAuto($sp[$n]['PersonID'], $subms[$k]['SubID']);?>
							<?if ($temptotal > 0):?>
								<td class="TableCellThickLeft" width="<?=$cellwidth?>" align="right"><?=$temptotal?></td>
								<td class="TableCell" width="<?=$cellwidth?>" align="right"><?=round(($temptotal/RollupTotalOppCount($sp[$n]['PersonID']))*100)?></td>
							<?else:?>
								<td class="TableCellThickLeft" width="<?=$cellwidth?>" align="right">&nbsp;</td>
								<td class="TableCell" width="<?=$cellwidth?>" align="right">&nbsp;</td>
							<?endif;?>
						<?endfor;?>
					<?endif;?>
				</tr>
			<?endfor;?>
		</tbody>
	</table></div><br>
	<?$curSPIndex += $spPerPage;?>
<?endwhile;?>
</div>
<div class="OnlyPrintBlock">
<?$curSPIndex = 0;?>
<?while ($curSPIndex < count($sp)):?>
	<?
		$spBreakIndex = $curSPIndex+$spPerPage;
		if ($spBreakIndex > count($sp))
			$spBreakIndex = count($sp);
	?>
	<table
		<?if ($curSPIndex != 0):?>
			class="pagestart"
		<?endif;?>
		cellpadding="3" cellspacing="0" style="border-left:1px solid black; border-top:1px solid black;font-family:Arial; font-size:8pt; background-color:white;">
		<thead style="display: table-header-group;">
			<tr>
				<td class="ColumnHeader" style="border-top:1px solid black;" valign="middle" align="center">
					<?=$shortdateFrom?> - <?=$shortdateTo?><br>
					<span style="font-weight:normal;">Optional criteria are not reported</span>
				</td>
				<td colspan="2" class="ColumnHeaderThickLeftFlipped" style="border-bottom:1px solid black; font-size:14px;" align="left" nowrap>Report Totals</td>
				<?for ($k = $curSPIndex; $k < $spBreakIndex; ++$k):?>
					<td colspan="2" style="border-bottom:1px solid black;" class="ColumnHeaderThickLeftFlipped" align="left" nowrap><?=$sp[$k]['FullName']?></td>
				<?endfor;?>
			</tr>
			<tr>
				<td class="ColumnHeader" style="font-size:13px;">Total <?=$CLabel?> Opportunities</td>
				<td class="ColumnHeaderThickLeft" colspan="2" align="center" nowrap><?=$TotalOppCount?></td>
				<?for ($k = $curSPIndex; $k < $spBreakIndex; ++$k):?>
					<td colspan="2" class="ColumnHeaderThickLeft" align="center" nowrap><?=zero(RollupTotalOppCount($sp[$k]['PersonID']))?></td>
				<?endfor;?>
			</tr>
			<tr>
				<td class="ColumnHeaderDarker">&nbsp;</td>
				<td align="center" class="ColumnHeaderDarkerThickLeft" nowrap>#</td>
				<td align="center" class="ColumnHeaderDarker" nowrap>%</td>
				<?for ($k = $curSPIndex; $k < $spBreakIndex; ++$k):?>
					<td align="center" class="ColumnHeaderDarkerThickLeft" nowrap>#</td>
					<td align="center" class="ColumnHeaderDarker" nowrap>%</td>
				<?endfor;?>
			</tr>
		</thead>
		<?
		$curloc = -1;
		?>
		<tbody>
			<?for ($k = 0; $k < count($subms); ++$k):?>
				<?
				$temploc = $subms[$k]['Location']+1;
				if ($temploc == 1 && $companyRow['Requirement1used'] != 1)
					continue;
				if ($temploc == 6 && $companyRow['Requirement2used'] != 1)
					continue;
				?>
				<?$colspan = (($spBreakIndex+1)*2)+1;?>
				<?if ($temploc != $curloc):?>
					<?
					$curloc = $temploc;
					//Criteria Not Completed by {{Closed}}
					?>
					<tr>
						<td class="ColumnHeader" colspan="<?=$colspan?>" height="<?=$rowHeight?>">
							<?if ($curloc == 1):?>
								<?=$companyRow['Requirement1']?>
							<?elseif ($curloc == 6):?>
								<?=$companyRow['Requirement2']?>
							<?else:?>
								<?=$companyRow['MS'.$temploc.'Label']?>
							<?endif;?>
						</td>
					</tr>
				<?endif;?>
				<tr>
					<?if ($subms[$k]['IsHeading'] == 1):?>
						<td class="TableCell" width="220" height="<?=$rowHeight?>" colspan="<?=$colspan?>"><b><?=$subms[$k]['Prompt']?></b></td>
						<?continue;?>
					<?else:?>
						<td class="TableCell" width="220" height="<?=$rowHeight?>">
							<?=$subms[$k]['Prompt']?>
						</td>
					<?endif;?>
					<?if ($subms[$k]['Mandatory'] != 1):?>
						<td class="TableCellThickLeft" colspan="<?=$colspan-1?>" align="center">OPTIONAL</td>
					<?else:?>
						<?if ($grandTotals[$subms[$k]['SubID']] > 0):?>
							<td class="TableCellThickLeft" style="background-color:#DDDDDD;" width="<?=$cellwidth?>" align="right"><?=$grandTotals[$subms[$k]['SubID']]?></td>
							<td class="TableCell" style="background-color:#DDDDDD;" width="<?=$cellwidth?>" align="right"><?=round(($grandTotals[$subms[$k]['SubID']]/$TotalOppCount)*100)?></td>
						<?else:?>
							<td class="TableCellThickLeft" style="background-color:#DDDDDD;" width="<?=$cellwidth?>" align="right">&nbsp;</td>
							<td class="TableCell" style="background-color:#DDDDDD;" width="<?=$cellwidth?>" align="right">&nbsp;</td>
						<?endif;?>
						</td>
						<?for ($n = $curSPIndex; $n < $spBreakIndex; ++$n):?>
							<?$temptotal = RollupTotalAuto($sp[$n]['PersonID'], $subms[$k]['SubID']);?>
							<?if ($temptotal > 0):?>
								<td class="TableCellThickLeft" width="<?=$cellwidth?>" align="right"><?=$temptotal?></td>
								<td class="TableCell" width="<?=$cellwidth?>" align="right"><?=round(($temptotal/RollupTotalOppCount($sp[$n]['PersonID']))*100)?></td>
							<?else:?>
								<td class="TableCellThickLeft" width="<?=$cellwidth?>" align="right">&nbsp;</td>
								<td class="TableCell" width="<?=$cellwidth?>" align="right">&nbsp;</td>
							<?endif;?>
						<?endfor;?>
					<?endif;?>
				</tr>
			<?endfor;?>
		</tbody>
	</table>
	<?$curSPIndex += $spPerPage;?>
<?endwhile;?>
</div>
<?php

end_report();

?>