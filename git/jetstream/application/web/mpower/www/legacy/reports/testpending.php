<?php
 /**
 * @package Reports
 */


define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_paul_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');


$fiscal_date = getFiscalDate($mpower_companyid );
$fiscal_year = getYear($fiscal_date);

if ( $_GET['output']=='xls') {
header("Content-Type: application/force-download");
header("Content-Type: application/vnd.ms-excel");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Transfer-Encoding: binary");
header("Pragma: no-cache");
header("Expires: 0"); 
header("Content-disposition: attachment;filename=Yellow.xls");
}
//begin_report('Pending Report');
?>
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
	<html lang="en">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Pending Report</title>
	<script language="JavaScript" src="../javascript/pageformat.js,windowing.js"></script>
	<style>
	@media print
	{
		.OnlyPrintBlock
		{
			display:block;
		}
		.OnlyPrintInline
		{
			display:inline;
		}
		.OnlyScreenBlock
		{
			display:none;
		}
		.OnlyScreenInline
		{
			display:none;
		}
	}
	@media screen
	{
		.OnlyPrintBlock
		{
			display:none;
		}
		.OnlyPrintInline
		{
			display:none;
		}
		.OnlyScreenBlock
		{
			display:block;
		}
		.OnlyScreenInline
		{
			display:inline;
		}
	}
	body {
	font-family: Arial;
	font-size: 10pt;
}

table.report .jr, table.sortable .jr {
	text-align: right;
}

table.report .jl, table.sortable .jl {
	text-align: left;
}

table.report, table.sortable {
	border-collapse: collapse;
	font-size: 8pt;
	border: 1px solid black;
}

table.report th, table.sortable th {
	font-weight:bold;
	background-color:#DDDDDD;
}

table.report .unused {
	background-color:#DDDDDD;
}

table.report td, table.sortable td, table.report th, table.sortable th {
	border:1px solid black;
	padding: 3px 7px 3px 7px;
	white-space: nowrap;
 	text-align: center;
 }

a.sortheader {
	text-decoration: none;
	color: black;
}

span.sortarrow {
	text-decoration: none;
}

table.report .ThickLeft {
	border-left:2px solid black;
}





TABLE.km {
}

TABLE.km TD {
	border:0px solid white;
	padding: 0px 0px 0px 0px;
}

/* Styles for command controls ********************************/

a:link { color: black; }
a:visited { color: black; }
a:hover { color: steelblue; }
a:active { color: steelblue; }

.command
{
	height: 25px;
	font-family: tahoma, arial, sans-serif;
	font-size: 12px;
	border-color: white dimgray dimgray white;
	background-color: #cacaca;
	background-image: url(../images/command.jpg);
	background-repeat: repeat-x;
	background-position: 50% 50%;
}

.etc-button
{
	height: 20px;
	width: 24px;
	font-family: tahoma, arial, sans-serif;
	font-size: 12px;
	border: none;
	border-color: white dimgray dimgray white;
	background-color: #cacaca;
	background-image: url(../images/etc-button.gif);
	background-repeat: repeat-x;
	background-position: 50% 50%;
}

.edit-button
{
	height: 20px;
	width: 35px;
	font-family: tahoma, arial, sans-serif;
	font-size: 12px;
	border: none;
	border-color: white dimgray dimgray white;
	background-color: #cacaca;
	background-image: url(../images/edop-editbutton.gif);
	background-repeat: repeat-x;
	background-position: 50% 50%;
}

.command_new
{
	height: 32px;
	border: none;
	font-family: tahoma, arial, sans-serif;
	font-size: 12px;
	background-color: #E7E7E8;
}

.sized_command
{
	width: 70px;
	height: 25px;
	font-family: tahoma, arial, sans-serif;
	font-size: 12px;
	border-color: white dimgray dimgray white;
	background-color: #cacaca;
	background-image: url(../images/command.jpg);
	background-repeat: repeat-x;
	background-position: 50% 50%;
}

.sized_command_new
{
	width: 60px;
	height: 22px;
	line-height: 20px;
	font-family: "arial narrow", arial, sans-serif;
	font-size: 11px;
	border: none;
	background-image: url(../images/button-grey60.gif);
	background-repeat: repeat-x;
	background-position: 50% 50%;
	text-align: center;
	white-space: nowrap;
	margin: 0 1px 0 1px;
	cursor: pointer;
}

.sized_command_new_gb
{
	width: 60px;
	height: 22px;
	font-family: "arial", arial, sans-serif;
	font-size: 12px;
	border: none;
	background-image: url(../images/button-grey60-gb.gif);
	background-repeat: repeat-x;
	background-position: 50% 50%;
}

.sized_command_new_gb_70
{
	width: 70px;
	height: 22px;
	font-family: "arial", arial, sans-serif;
	font-size: 12px;
	border: none;
	background-image: url(../images/button-grey70-gb.gif);
	background-repeat: repeat-x;
	background-position: 50% 50%;
}

.menu_button_l
{
	width: 3px;
	height: 22px;
	border: none;
}

.menu_button_m
{
	height: 22px;
	line-height: 20px;
	font-family: "arial narrow", arial, sans-serif;
	font-size: 11px;
	border: none;
	background-image: url(../images/button-grey-m.gif);
	background-repeat: repeat-x;
	background-position: 50% 50%;
	text-align: center;
	white-space: nowrap;
	padding-left: 5px;
	padding-right: 5px;
	cursor: pointer;
}

.menu_button_r
{
	width: 2px;
	height: 22px;
	border: none;
}

.sized_command_hl
{
	width: 70px;
	height: 25px;
	font-family: tahoma, arial, sans-serif;
	font-size: 12px;
	font-weight: bold;
	border-color: white maroon maroon white;
	background-color: red;
	background-repeat: repeat-x;
	background-position: 50% 50%;
	color: white;
}

.sized_command_hl_new
{
	width: 60px;
	height: 22px;
	line-height: 20px;
	font-family: arial, sans-serif;
	font-size: 12px;
	font-weight: bold;
	border: none;
	background-color: red;
	background-image: url(../images/button-red60.gif);
	background-repeat: repeat-x;
	background-position: 50% 50%;
	color: white;
	white-space: nowrap;
	text-align: center;
	margin: 0 1px 0 1px;
	cursor: pointer;
}

.sized_command_nogradient
{
	width: 70px;
	height: 25px;
	font-family: tahoma, arial, sans-serif;
	font-size: 12px;
	border-color: white dimgray dimgray white;
	background-color: #efefef;
	background-repeat: repeat-x;
	background-position: 50% 50%;
}

.sized_command_nogradient_new
{
	width: 60px;
	height: 22px;
	line-height: 20px;
	font-family: "arial narrow", sans-serif;
	font-size: 12px;
	border: none;
	background-color: #efefef;
	background-image: url(../images/button-grey60.gif);
	background-repeat: repeat-x;
	background-position: 50% 50%;
	white-space: nowrap;
	text-align: center;
	margin: 0 1px 0 1px;
	cursor: pointer;
}

.panel
{
	background-image: url(../images/edop-button.gif);
	text-align: center;
	font-weight: bold;
	color: white;
	cursor: pointer;
}

.panelgold
{
	background-image: url(../images/edop-buttongold.gif);
	text-align: center;
	font-weight: bold;
	color: white;
	cursor: pointer;
}

.panelgray
{
	background-image: url(../images/edop-buttongray.gif);
	text-align: center;
	font-weight: bold;
	color: white;
	cursor: pointer;
}

.panelblue
{
	background-image: url(../images/edop-buttonblue.gif);
	text-align: center;
	font-weight: bold;
	color: white;
	cursor: pointer;
}

.panelon
{
	background-image: url(../images/edop-buttonon.gif);
	text-align: center;
	font-weight: bold;
	cursor: pointer;
}

.panelmid
{
	background-image: url(../images/edop-buttonmid.gif);
	text-align: center;
	font-weight: bold;
	color: white;
	cursor: pointer;
}

.panelmidon
{
	background-image: url(../images/edop-buttonmidon.gif);
	text-align: center;
	font-weight: bold;
	cursor: pointer;
}

.panelsmall
{
	background-image: url(../images/edop-buttonsmall.gif);
	text-align: center;
	font-weight: bold;
	color: white;
	cursor: pointer;
}

.panelsmallon
{
	background-image: url(../images/edop-buttonsmallon.gif);
	text-align: center;
	font-weight: bold;
	color: white;
	cursor: pointer;
}


.raised
{
	border: 2px outset;
}

.raisedred
{
	border: 2px solid red;
}

.raisedtemp
{
	border-left: 2px inset white;
	border-top: 2px inset white;
	border-bottom: 2px inset black;
	border-right: 2px inset black;
}

.raised1
{
	border-left: 1px solid white;
	border-top: 1px solid white;
	border-bottom: 1px solid black;
	border-right: 1px solid black;
}

.raised1red
{
	border: 1px solid red;
}

.lowered
{
	border-left: 1px solid white;
	border-top: 1px solid white;
	border-bottom: 1px solid black;
	border-right: 1px solid black;
}

/* Styles for popup windows ***********************************/

.title
{
	font-family: verdana, arial, sans-serif;
	font-size: 14px;
}

.plabel
{
	font-family: arial, sans-serif;
	color: white;
	font-weight: bold;
	font-size:12pt;
}

.plabel-black
{
	font-family: arial, sans-serif;
	color: black;
	font-weight: bold;
}

/* Subclass of plabel, with font size, for edit opportunity window */
.plabel_eo
{
	font-family: verdana, arial, sans-serif;
	font-size: 14px;
	font-weight: bold;
	text-align: center;
	color: white;
}

.plabel-black_eo
{
	font-family: arial, sans-serif;
	color: black;
	font-weight: bold;
	font-size: 16px;
}

.plabel-black14
{
	font-family: arial, sans-serif;
	color: black;
	font-size: 14px;
}

.info
{
	font-family: arial, sans-serif;
}

.grid_header
{
	font-family: verdana, arial, sans-serif;
	font-size: 14px;
	font-weight: bold;
	text-align: center;
	background-color: #cacaca;
	background-image: url(../images/command_big.jpg);
	background-repeat: repeat-x;
	background-position: 50% 50%;
}

.grid_header_new
{
	font-family: verdana, arial, sans-serif;
	font-size: 14px;
	font-weight: bold;
	text-align: center;
	color: white;
	background-image: url(../images/grid-head-bk.jpg);
	background-repeat: repeat-x;
	background-position: 50% 50%;
}

.grid_header_red
{
	font-family: verdana, arial, sans-serif;
	font-size: 14px;
	font-weight: bold;
	text-align: center;
	color: white;
	background-image: url(../images/grid-red-bk.jpg);
	background-repeat: repeat-x;
	background-position: 50% 50%;
}

.grid_header_blue
{
	font-family: verdana, arial, sans-serif;
	font-size: 11px;
	font-weight: bold;
	text-align: center;
	color: white;
	background-image: url(../images/grid-blue-bk.jpg);
	background-repeat: repeat-x;
	background-position: 50% 50%;
}

.edop_header
{
	font-family: verdana, arial, sans-serif;
	font-size: 14px;
	font-weight: bold;
	text-align: center;
	color: white;
	background-image: url(../images/edophead-back.gif);
	background-repeat: repeat-x;
	background-position: 50% 50%;
}

.edop_middle
{
	font-family: verdana, arial, sans-serif;
	font-size: 11px;
	font-weight: bold;
	text-align: center;
	color: white;
	background-image: url(../images/edopmiddle-back.gif);
	background-repeat: repeat-x;
	background-position: 50% 50%;
}

.edop_mile
{
	font-family: verdana, arial, sans-serif;
	font-size: 18px;
	font-weight: bold;
	text-align: center;
	color: white;
	background-image: url(../images/edopmile-back.gif);
	background-repeat: repeat-x;
	background-position: 50% 50%;
}

.edop_mileon
{
	font-family: verdana, arial, sans-serif;
	font-size: 18px;
	font-weight: bold;
	text-align: center;
	color: white;
	background-image: url(../images/edopmileon-back.gif);
	background-repeat: repeat-x;
	background-position: 50% 50%;
}

.gradient
{
/*	background-color: white;
	background-image: url(../images/gradient.jpg);
	background-repeat: repeat-y;*/
}

.taglabel
{
	font-family: arial, sans-serif;
	color: white;
	font-weight: bold;
}

.taglabel_disabled
{
	font-family: arial, sans-serif;
	color: gray;
	font-weight: bold;
}

/* taglabel classes for editopportunity window*/
.taglabel_eo
{
	font-family: verdana, arial, sans-serif;
	color: white;
	font-weight: bold;
	font-size: 14px;
}

.taglabel_disabled_eo
{
	font-family: verdana, arial, sans-serif;
	color: gray;
	font-weight: bold;
	font-size: 14px;
}

/* Styles for the page header *********************************/

td.header_text
{
	text-align: left;
	font-family: verdana, arial, sans-serif;
	font-weight: bold;
	font-size: 12px;
	background-color: white;
}

td.header_table
{
	background: url(../images/header_bg.jpg);
}

td.header_top_left_cell
{
	background: url(../images/header_top_left.jpg);
	background-repeat: no-repeat;
}

td.header_bottom_left_cell
{
	background: url(../images/header_bottom_left.jpg);
	background-repeat: no-repeat;
}

td.header_right_cell
{
	background: url(../images/header_right.jpg);
	background-repeat: no-repeat;
}

/* Miscellaneous styles ***************************************/

.error
{
	font-family: tahoma, arial, helvetica, sans-serif;
	font-size: 14px;
	color: red;
	font-weight: bold;
}

.stalled_header
{
	font-family: verdana, arial, sans-serif;
	font-size: 14px;
	color: white;
	font-weight: bolder;
	text-align: center;
	background-color: #ff0000;
}

.notifier
{
	position: absolute;
	top: 0px;
	left: 0px;
	display: none;
	background-color: white;
	border: 2px solid black;
	height: 70px;
	width: 250px;
}

.saver1
{
	position: absolute;
	visibility: hidden;
	width: 10px;
	height: 10px;
	left: 0px;
	top: 0px;
	overflow:hidden;
}

.saver
{
	position: relative;
	visibility: visible;
	width: 100px;
	height: 100px;
	left: 0px;
	top: 0px;
	overflow:hidden;
}

.removed_default
{
	font-family: arial, sans-serif;
	font-size: 12px;
	background-color: white;
	color: black;
}

.removed_selected
{
	font-family: arial, sans-serif;
	font-size: 12px;
	background-color: navy;
	color: white;
}

/* Styles for milestones **************************************/

.milestone_no
{
	font-family: tahoma, sans-serif;
	font-size: 12px;
	color: black;
	cursor: pointer;
	text-align: center;
}

.milestone_yes
{
	font-family: tahoma, sans-serif;
	font-size: 12px;
	color: white;
	background-color: black;
	cursor: pointer;
	text-align: center;
}

.milestone_yes_small
{
	font-family: tahoma, sans-serif;
	font-size: 9px;
	font-weight: bold;
	color: white;
	background-color: black;
	text-align: center;
}

.milestone_no_small
{
	font-family: tahoma, sans-serif;
	font-size: 10px;
	color: black;
	text-align: center;
}

/* Calendar styles ********************************************/

.calendar_weekday
{
	background-color: white;
	border: 3px solid white;
	text-align: center;
	font-family: arial, sans-serif;
	font-weight: bold;
	font-size:12pt;
}

.calendar_weekend
{
	background-color: #ededf2;
	border: 3px solid #ededf2;
	text-align: center;
	font-family: arial, sans-serif;
	font-size:12pt;
}

.calendar_today
{
	/*background-color: #f0faf5;*/
	background-color: #ebedeb;
	text-align: center;
	font-family: arial, sans-serif;
	font-weight: bold;
	border: 3px solid gold;
	font-size:12pt;
}

.calendar_real_today
{
	/*background-color: #f0faf5;*/
	background-color: #bbbbff;
	text-align: center;
	font-family: arial, sans-serif;
	font-weight: bold;
	border: 3px solid #bbbbff;
	font-size:12pt;
}

.calendar_real_today_selected
{
	/*background-color: #f0faf5;*/
	background-color: #bbbbff;
	text-align: center;
	font-family: arial, sans-serif;
	font-weight: bold;
	border: 3px solid gold;
	font-size:12pt;
}

.calendar_daynames
{
	font-family: tahoma, sans-serif;
	background-color: lightyellow;
	text-align: center;
	font-size:10pt;
}

.calendar_top
{
	
}







	</style>

	<script language="JavaScript">
	
	
	function use_treeid(id)
	{
		var sep = '?';
		if (window.location.href.indexOf('?') != -1) sep = '&';
		window.location.href = window.location.href + sep + 'treeid=' + id;
	}

	function do_reselect()
	{
		//var url = '/v3/legacy/reports/Testpending.php' + '?SN=4b0ac512ca8f4';
		//window.location.href = url;
		var url = '/v3/legacy/reports/Testpending.php' ;
		var sep = url.indexOf('?') != -1 ? '&' : '?';
		url = url + sep + 'SN=4b0ac512ca8f4';
		window.location.href = url;
	}
	
	</script>
	
	</head>
	<body leftmargin="3" rightmargin="3" topmargin="3" bottommargin="3" bgcolor="white" onload="if (!(!window.myOnLoad)) myOnLoad();">
	<div class="OnlyScreenBlock" style="padding-bottom:3px;">
		<script language="JavaScript">
			Header.setText('Pending Report');
			Header.addButton(ButtonStore.getButton('Reselect'));
			Header.addButton(ButtonStore.getButton('Print'));
			document.writeln(Header.makeHTML());
		</script>
	</div>
	<table width="100%" cellpadding="0" cellspacing="0">
		<thead style="display: table-header-group;">

			<tr>
				<td valign="top" height="1" style="padding-bottom:3px;">
					<table width="100%" cellpadding="0" cellspacing="0" style="font-family:Arial; font-size:12pt;">
						<tr>
							<td align="left"><img class="OnlyPrintInline" src="http://www.mpasa.com/mpower/legacy/images/mplogosm.gif"></td>
							<td align="right" nowrap><span class="OnlyPrintInline">Radio Disney</span></td>
						</tr>
					</table>

				</td>
			</tr>
		<tbody>
			<tr>
				<td valign="top">
					<div class="OnlyPrintBlock" style="font-family:Arial; font-size:14pt; font-weight:bold; padding-bottom:3px;">
						<center>Pending Report</center>
					</div>

				</td>
				<td valign="top">
<?



$idList = GetSalespeopleTree($treeid);

$productSql = "select Opportunities.Company, Opportunities.Category, Opportunities.dealID,
     Opp_Product_XRef.Month_1, Opp_Product_XRef.Month_2, Opp_Product_XRef.Month_3,
     Opp_Product_XRef.Month_4, Opp_Product_XRef.Month_5, Opp_Product_XRef.Month_6,
     Opp_Product_XRef.Month_7, Opp_Product_XRef.Month_8, Opp_Product_XRef.Month_9,
     Opp_Product_XRef.Month_10, Opp_Product_XRef.Month_11, Opp_Product_XRef.Month_12,
     Opp_Product_XRef.Val, Opportunities.Agency
from Opportunities 
left outer join Opp_Product_XRef on Opportunities.DealID = Opp_Product_XRef.DealID 
left join products on Products.ProductID = Opp_Product_XRef.ProductID 
where opportunities.PersonID in ($idList) and 
      products.companyid = $mpower_companyid and 
      opportunities.category in (2,3,4,5) 
order by Products.ProductID, Opportunities.Category ";

$productResult = mssql_query($productSql);

$pending = array( 0 => array() , 1 => array(), 2 => array() ) ;  

$a = 2 ; 
while ($row = mssql_fetch_assoc($productResult))
{
	if ( $row['Category'] == 4 ) {
		$a = 0 ;
	}
	else if ( $row['Category'] == 2 ) {
		$a = 1 ;
	}
	else {
		$a = 2 ; 
	}
	if ( $pending[$a][$row['dealID']] == NULL ) {
		$pending[$a][$row['dealID']]['company'] = $row['Company'] ; 
		for ($i = 1 ; $i <= 12 ; $i++){
			$pending[$a][$row['dealID']][$i] = $row['Month_' . $i];
		}
		$pending[$a][$row['dealID']]['Val'] = $row['Val'];
		$pending[$a][$row['dealID']]['agency'] = $row['Agency'];
		
	}
	else {
		for ($i = 1 ; $i <= 12 ; $i++){
			$pending[$a][$row['dealID']][$i] += $row['Month_' . $i];
		}

		$pending[$a][$row['dealID']]['Val'] += $row['Val'];
		
	}
// calculate for direct and indirect 	
}

$sqlstr = "select FirstName, LastName from people where PersonID = $treeid";
$result = mssql_query($sqlstr);
$reportPerson = mssql_fetch_assoc($result);

?>

		<link rel="stylesheet" type="text/css" href="../css/report.css">
		<script src="../javascript/utils.js,sorttable2.js,filter2.js"></script>
		</td>
	</tr>
</table>
<br>

<p align=left style="font-size:12pt; font-weight:bold;"><?=$reportPerson['FirstName']?> <?=$reportPerson['LastName']?>  <a href="" style="font-size:10pt; font-weight:normal;text-decoration:none">- Edit Actuals</a></p>
<br><?
	 
	$fiscal_month = getMonth($fiscal_date) +1 ; // value has to be 1 < $fiscal_month < 13 

	$current_month = (int) date("m");
	$current_year = (int) date("y") + 2000 ;

	$months_list = array ( '0', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' );
	$quarters = array ( 'First Quarter', 'Second Quarter', 'Third Quarter', 'Fourth Quarter' ) ; 
	$quarters_color = array ( '#F2FFF2', '#DBFFDB', '#FFEAEA', '#FFBFBF') ; 
	$pending_list = array ( 0 => '"A" Pending (95%)', 1 => '"B" Pending (75%)', 2 => '"C" Pending (50%)' ) ;

	$current_q = which_quarter($current_month, $fiscal_month) ; 
	
	// check what is the start month 
	$start_month = ( $fiscal_month + ( ($current_q - 1) * 3 ) )% 12;
	if ( $start_month == 0 ) $start_month = 1 ;  
	
	$totalMonth = array() ;  

	echo '<table class="report">';
	// change for color coded quarters 
	foreach ($pending_list as $a => $header) {
	?>
			<center>
				<colgroup>
					<col span=2 />
					<col span=1 />
				<?
				$q = $current_q - 1; 
				
				for ($f =0 ; $f < 4 ; $f++ ){
					if ($q > 3 ){
						$q = 0 ;
					}  
					echo '<col span=4 style="background-color: ' .  $quarters_color[$q] . '"/>'; 
					$q++ ; 
				}
				?>
					<col span=4 style="background-color: #DDDDFF" />
				</colgroup>
				<tr>
                    <th rowspan=2>&nbsp; </th>
					<th rowspan=2><?echo $header ?> </th>
					<th rowspan=2 class="ThickLeft">Type  </th>
	<?			
				// print the quarters 
				$q = $current_q - 1; 
				$current_years = $current_year ;  
				for ($f =0 ; $f < 4 ; $f++ ){
					if ($q > 3 ){
						$q = 0 ;
						$current_years++ ;
					}  
					$year = (string) $current_years; 
					echo '<th colspan=4 class="ThickLeft">' .  $quarters[$q] . " '" . $year[2] . $year[3] .  '</th>' ; 
					$q++ ; 
				}

				echo '<th rowspan=2 class="ThickLeft">Total</th>';
				echo '</tr>';
				echo '<tr>';
			
				// print the months 
				$q =$start_month ; 
				for ( $f= 1 ; $f < 17 ; $f++ ) {
					
					if ($q > 12 ) $q = 1; 
					
					if ( ($f%4) == 0 ){
						echo '<th class="ThickLeft">Total</th>' ;  
					}
					else {
						echo '<th class="ThickLeft">' . $months_list[$q]  .  '</th>' ;
						$q++;
					}
				}	
				echo '</tr>'; 

				$total = 0 ; 
				$b = 1 ;
				
                foreach ($pending[$a] as $id => $name) {
					 
					echo '<tr>';
                    echo '<td>'. $b .'</td>';
					echo '<td class="jl">' . $pending[$a][$id]['company'] . '</td>'; // name 
					echo '<td class="jl">' ; 
					if ( $pending[$a][$id]['agency'] == 1 ) echo 'Agency' ;
					else echo 'Direct' ; 
					
					echo '</td>'; // type 

					$total_q = array ( 0, 0, 0, 0 ) ;
					
					$e = $start_month ; 
					$f = $current_year ; 
					for ( $d = 1 ; $d < 17 ; $d++ ){
						if ($e > 12 ){
							$e = 1 ;
							$f++;
						}
						
						if ( $d % 4 != 0 ) {
							if ( ( $e < $current_month && $current_year == $f ) || ( $current_year != $f && $e >= $current_month)) {
								echo '<td class="jr" style="text-align:right"><strike>' . CurrencyFormat($pending[$a][$id][$e], 0 ) . '</strike></td>';
							} // case if the value is 12 and 1 ?? how 
							else {
							echo '<td class="jr" style="text-align:right">' . CurrencyFormat($pending[$a][$id][$e], 0 ) . '</td>';
							$total_q[which_quarter($e, $fiscal_month)] += $pending[$a][$id][$e] ; 
							}
							$e++ ; 
							
						} 
						else{
							if ( $e == 1 ) echo '<td class="ThickLeftjr" style="text-align:right">' . CurrencyFormat($total_q[which_quarter(12, $fiscal_month)], 0 )  . '</td>';
							else echo '<td class="ThickLeftjr" style="text-align:right">' . CurrencyFormat($total_q[which_quarter($e-1, $fiscal_month)], 0 )  .  '</td>';
						} 
					}		
				
					if ( $pending[$a][$id]['Val'] == NULL ) {
						$val = $total_q[1] + $total_q[2] + $total_q[3] + $total_q[4] ; 
					}
					else $val = $pending[$a][$id]['Val'] ;
					
					echo '<td class="ThickLeft jr" style="text-align:right">' . CurrencyFormat($val, 0 ) . '</td>';
					echo '</tr>';
					$total += $val ;
					$b++; 
					for ($i = 1 ; $i <= 12 ; $i++){
						$totalMonth[$a][$i] += $pending[$a][$id][$i] ; 
					}

					$totalMonth[$a][13] += $total_q[1] ; 
					$totalMonth[$a][14] += $total_q[2] ; 
					$totalMonth[$a][15] += $total_q[3] ; 
					$totalMonth[$a][16] += $total_q[4] ; 
					$totalMonth[$a][17] += $pending[$a][$id]['Val'] ; 
					
					if ( $pending[$a][$id]['agency'] == 1 ){
						for ($i = 1 ; $i <= 12 ; $i++){
							$totalMonth[$a]['agency'][$i] += $pending[$a][$id][$i] ; 
						}
						$totalMonth[$a]['agency'][13] += $total_q[1] ; 
						$totalMonth[$a]['agency'][14] += $total_q[2] ; 
						$totalMonth[$a]['agency'][15] += $total_q[3] ; 
						$totalMonth[$a]['agency'][16] += $total_q[4] ; 
						$totalMonth[$a]['agency'][17] += $pending[$a][$id]['Val'] ; 	
					}
				}
				if ( $b == 1 ) {
					echo '<tr>';
					echo '<td colspan=19 class="ThickLeftjr">' . 'Empty' . '</td>';
					echo '</tr>';
				}
				echo '<tr>' ; 
				echo '<th colspan=3>Totals</th>';
				$e=$start_month ; 
				$q = $current_year ; 
				for ( $f= 1 ; $f < 17 ; $f++ ) {
					if ($e > 12 ){
						$e = 1 ;
						$q++ ; 
					}
					if ( $f % 4 != 0 )  {
						if ( ( $e < $current_month && $current_year == $q ) || ( $current_year != $q && $e >= $current_month)) {
							echo '<th class="jr" style="text-align:right"><strike>' . CurrencyFormat($totalMonth[$a][$e], 0 ) . '</strike></th>' ; 
						}
						else {
							echo '<th class="jr" style="text-align:right">' . CurrencyFormat($totalMonth[$a][$e], 0 ) . '</th>' ; 
						}$e++; 
					}
					else {						
						if ( $e == 1 ) echo '<th class="jr" style="text-align:right">' . CurrencyFormat($totalMonth[$a][which_quarter(12, $fiscal_month) + 12], 0 )  . '</td>';
						else echo '<th class="ThickLeft jr" style="text-align:right">' . CurrencyFormat($totalMonth[$a][which_quarter($e-1, $fiscal_month) + 12], 0 )  .  '</td>';
					}
				}
				?>

					<th class="ThickLeft jr" style="text-align:right">$<?=CurrencyFormat($totalMonth[$a][17], 0 ) ?></th>

				</tr>
				<tr>
					<td colspan=20 height="40px" style="border-style:none"> &nbsp; </td>
				</tr>	
	<?
	}
	?>
	</table>
		
	
		<P style="page-break-after:always; height:0;line-height:0;"> </P>
		<br>
		<br>
	<table border=1 cellspacing=10 cellpadding=0 >
	<tr>
	<?
	
	foreach ( array($fiscal_year, $fiscal_year+1) as $j ){

	// Obtaining Goals and Actuals 
	$sqlAct = "select p.Goal, p.TotalGoal, p.Agency, p.Direct 
			from pending p where personID in ($idList) and p.Year = $j";
	$Act = mssql_query($sqlAct);
	$actuals = array() ;
	$pending = array () ;
	while ( $row = mssql_fetch_assoc($Act)) {
		$temp1 = explode( ";", $row['Agency']) ;
		$temp2 = explode( ";", $row['Direct']) ;
		$temp3 = explode( ";", $row['Goal']) ; 
		$pending['total'] += (float) $row['TotalGoal'];
		for ($i =0 ; $i < 12 ; $i++){
			$actuals['agency'][$i+1] += (float) unformat_money($temp1[$i]) ;
			$actuals['direct'][$i+1] += (float) unformat_money($temp2[$i]) ;
			$actuals['both'][$i+1] += (float) unformat_money($temp1[$i]) + (float) unformat_money($temp2[$i]) ;
			$pending['goal'][$i+1] += (float) unformat_money($temp3[$i]) ;		
			
		}

	}
	
	?>
	<td>
	<table class="report">
	<center>
		<tr>
			<th colspan=8 >Pending Report w/ Actuals of Fiscal Year <?echo ($j+1)?></th>
		</tr>
		<tr>
            <th > &nbsp;  </th>
			<th class="ThickLeft">Agency </th>
			<th class="ThickLeft">Direct  </th>
			<th class="ThickLeft">Total  </th>
			<th class="ThickLeft">Goal </th>
			<th class="ThickLeft">% of Goal  </th>
			<th class="ThickLeft"><font color=red>$ Needed  </font></th>
			<th class="ThickLeft">Pending A & B </th>
		</tr>
		<?
		$month = $fiscal_month; // $current_month ; instead of current month use fiscal month
		$prev_year = 0 ; 
		$old_q = $current_q ; 
		$goal = 10000 ;  
			
		for ($i = 0 ; $i < 12 ; $i++ ) {

			
			echo '<tr style="background-color: #F2FFF2; ">';
			$year = (string) $current_year;
			echo "<td>". $months_list[$month] .  " '" . $year[2]. $year[3] . " </td>" ;
			echo '<td style="text-align:right"> $'. CurrencyFormat($actuals['agency'][$month], 0) . "</td>" ;
			echo '<td style="text-align:right"> $'. CurrencyFormat($actuals['direct'][$month], 0) . "</td>" ;
			echo '<td style="text-align:right"> $'. CurrencyFormat($actuals['both'][$month], 0 ). "</td>" ;
			echo '<td style="text-align:right"> $'. CurrencyFormat($pending['goal'][$month], 0 ). "</td>" ;
			echo '<td style="text-align:right">'. redblack(GetPercent( $actuals['both'][$month], $pending['goal'][$month] ), '%'). " </td>" ;
			echo '<td style="text-align:right"> '. redblack( ($pending['goal'][$month] - $actuals['both'][$month]), '$'  ). " </td>" ;
			echo '<td style="text-align:right"> $'. CurrencyFormat( ($totalMonth[0][$month] + $totalMonth[1][$month]) , 0 ). "</td>" ;
			echo '</tr>'; 
			$totalmonth['agency'] += $actuals['agency'][$month]; 
			$totalmonth['direct'] += $actuals['direct'][$month]; 
			$totalmonth['both'] += $actuals['both'][$month] ; 
			$totalmonth['goal'] += $pending['goal'][$month] ; 
			$totalmonth['needed'] += ($pending['goal'][$month] - $actuals['both'][$month]) ; 
			$totalmonth['pending'] += ($totalMonth[0][$month] + $totalMonth[1][$month]) ; 
			$totalmonth['total']['agency'] += $actuals['agency'][$month]; 
			$totalmonth['total']['direct'] += $actuals['direct'][$month]; 
			$totalmonth['total']['both'] += $actuals['both'][$month] ; 
			$totalmonth['total']['goal'] += $pending['goal'][$month] ; 
			$totalmonth['total']['needed'] += ($pending['goal'][$month] - $actuals['both'][$month]) ; 
			$totalmonth['total']['pending'] += ($totalMonth[0][$month] + $totalMonth[1][$month]) ; 
			$month++ ; 

			if ($month > 12 ){
				$month = 1 ;/*
				$prev_year_report = '' ; 
				$prev_year_report .= '<tr style="background-color: #FFBFBF; font-weight:bold;">' ; 
				$prev_year_report .= "<td style=\"text-align:right\"> Total for $current_year </td>" ;
				$prev_year_report .= "<td style=\"text-align:right\"> $". CurrencyFormat($totalmonth['total']['agency']) . "</td>" ;
				$prev_year_report .= "<td style=\"text-align:right\"> $". CurrencyFormat($totalmonth['total']['direct']) . "</td>" ;
				$prev_year_report .= "<td style=\"text-align:right\"> $". CurrencyFormat($totalmonth['total']['both']). "</td>" ;
				$prev_year_report .= "<td style=\"text-align:right\"> $". CurrencyFormat($totalmonth['total']['goal']) . "</td>" ;
				$prev_year_report .= "<td style=\"text-align:right\"> ". GetPercent($totalmonth['total']['both'] , $totalmonth['total']['goal']  )." % </td>" ;
				$prev_year_report .= "<td style=\"text-align:right\"> $". CurrencyFormat($totalmonth['total']['needed']). " </td>" ;
				$prev_year_report .= "<td style=\"text-align:right\"> $". CurrencyFormat($totalmonth['total']['pending']). "</td>" ;
				$prev_year_report .= "</tr>"; 
				$totalmonth['total']['agency'] = 0; 
				$totalmonth['total']['direct'] = 0; 
				$totalmonth['total']['both'] = 0 ; 
				$totalmonth['total']['goal'] = 0 ; 
				$totalmonth['total']['needed'] = 0 ; 
				$totalmonth['total']['pending'] = 0 ; 
				*/
				$current_year++ ; 
				
			}
			
			if ( $old_q != which_quarter($month, $fiscal_month)) {
				echo '<tr style="background-color: '.	$quarters_color[1].'">' ; 
				echo '<td style="border-width:2 1 2 2; "> Quarter '.  $old_q . " </td>" ;
				echo '<td style="border-width:2 1 2 1; text-align:right;"> $'. CurrencyFormat($totalmonth['agency'], 0) . "</td>" ;
				echo '<td style="border-width:2 1 2 1; text-align:right;"> $'. CurrencyFormat($totalmonth['direct'], 0) . "</td>" ;
				echo '<td style="border-width:2 1 2 1; text-align:right;"> $'. CurrencyFormat($totalmonth['both'], 0 ). "</td>" ;
				echo '<td style="border-width:2 1 2 1; text-align:right;"> $'. CurrencyFormat($totalmonth['goal'], 0) . "</td>" ;
				echo '<td style="border-width:2 1 2 1; text-align:right;"> '. redblack((GetPercent($totalmonth['both'] , $totalmonth['goal'])), '%'  ) ."  </td>" ;
				echo '<td style="border-width:2 1 2 1; text-align:right;"> '. redblack($totalmonth['needed'], '$'). " </td>" ;
				echo '<td style="border-width:2 2 2 1; text-align:right;"> $'. CurrencyFormat($totalmonth['pending'], 0  ). "</td>" ;
				echo "</tr>"; 
				$old_q = which_quarter($month, $fiscal_month);
				$totalmonth['agency'] = 0; 
				$totalmonth['direct'] = 0; 
				$totalmonth['both'] = 0 ; 
				$totalmonth['goal'] = 0 ; 
				$totalmonth['needed'] = 0 ; 
				$totalmonth['pending'] = 0 ; 

			}
			

		}
		if ( $prev_year_report == 0 ) echo $prev_year_report ;
		
		echo '<tr style="background-color: #FFBFBF; font-weight:bold;">' ; 
		echo "<td> Total for $current_year</td>" ;
		echo "<td  style=\"text-align:right\"> $". CurrencyFormat($totalmonth['total']['agency'], 0 ) . "</td>" ;
		echo "<td style=\"text-align:right\"> $". CurrencyFormat($totalmonth['total']['direct'] , 0 ) . "</td>" ;
		echo "<td style=\"text-align:right\"> $". CurrencyFormat($totalmonth['total']['both'] , 0 ). "</td>" ;
		echo "<td style=\"text-align:right\"> $". CurrencyFormat($totalmonth['total']['goal'] , 0 ) . "</td>" ;
		echo "<td style=\"text-align:right\"> ".GetPercent($totalmonth['total']['both'] , $totalmonth['total']['goal']  )." % </td>" ;
		echo "<td style=\"text-align:right\"> $". CurrencyFormat($totalmonth['total']['needed'], 0 ). " </td>" ;
		echo "<td style=\"text-align:right\"> $". CurrencyFormat($totalmonth['total']['pending'], 0). "</td>" ;
		echo "</tr>"; 
			
		
		?>
		

	<?

	?>
	</table>	
	</td>
	<?
	
	} 
	echo "</tr></table>" ; 
	end_report();
	?>

<?php

function GetPercent($numerator, $denominator) {
    $output = ($denominator != 0) ? CurrencyFormat((round($numerator/$denominator * 1000)/10), 1) : '-';    
//	$output = ($output <= 0.1) ? '0.0' : $output ; 
    return $output;
}

function NullToZero($value) {
    $output = (is_null($value)) ? 0 : $value;
    return $output;
}

// returns 0 for error 
function which_quarter($month, $fiscal_month){

	 if ( $month == $fiscal_month ) return 1 ; 
	else if ( $month < $fiscal_month ) {
		$month = $month + 12 ;
	}
	
	$diff = $month - $fiscal_month ; 
	
	if ( $diff >= 9 ) return 4 ; 
	else if ( $diff >= 6 ) return 3 ; 
	else if ( $diff >= 3 ) return 2 ; 
	else if ( $diff > 0 ) return 1 ; 
	else return 0 ; 

}

// takes in sql timestamp
// input "Sep 30 2009 12:00AM"  or 09/30/2009 00:00:00
// output 9 
// return 0 if error 
function getMonth ($a){ 

	$months_list = array (  'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' );
	$part = explode(" ", $a);
	if ( count($part) > 2 ) {
		for ($v = 0 ; $v < 12 ; $v++){
			if ( $part[0] == $months_list[$v] ) return ($v + 1); 	
		}	
	}
	else {
		$parts = explode("/", $part[0]) ;
		return (int) $parts[0] ; 
	}
	return 0  ; 
}
function getYear ($a) {
	$part = explode(" ", $a);
	if ( count($part) > 2 ) {
		return (int) $part[2] ; 
	}
	else {
		$parts = explode("/", $part[0]) ;
		return (int) $parts[2] ; 
	}
	return 0  ; 
}

function getFiscalDate($companyID ) {
	$strsql = "SELECT CompanyID, FiscalYearEndDate FROM FiscalYear where companyID = $companyID and FiscalYearEndDate is not NULL" ; 
	$result1 = mssql_query($strsql);
	$fiscal_date =  mssql_fetch_assoc($result1);
	return $fiscal_date['FiscalYearEndDate']; 
}

 function checkSalesPerson ($id ) {
	$sql = "select isSalesperson from people where personID = $id" ; 
	$productResult = mssql_query($sql);
	$result = mssql_fetch_assoc($productResult) ;  
	return $result['isSalesperson'] ; 
}

// input array $arr
// output csv 
function convArrayFormat( $arr, $count = 0 ) {
	
	if ($count == 0) $count = count($arr) ; 
	ksort($arr) ; 
	// cleaning data 
	for ( $i = 1 ; $i <= $count ; $i++ ) {
		$arr[$i] = trim ((string) $arr[$i]);
		if ($arr[$i][0] == '$' )$arr[$i] = str_replace("$", '', $arr[$i]) ;//array_shift($arr[$i]) ; 
	}
	return implode ( ";", $arr)  ; 
}

function unformat_money( $money) {
	$money = str_replace("$", '', $money); 
	$money = str_replace(",", '', $money); 
	return $money;
}

function redblack( $money, $type= '$'){
   if ( $type == '$' ){
	if ( $money < 0 ) return "$ ". currencyFormat(($money * -1), 0 ) ; 
	else return "<font color=red>$ ". currencyFormat($money, 0) ."</font>"; 
   }
   else {
	if ( $money < 100 ) return "<font color=red>".(  (int)$money  ) ."% </font>"; 
	else return $money . " %";
   
   } 
	
}


?>

