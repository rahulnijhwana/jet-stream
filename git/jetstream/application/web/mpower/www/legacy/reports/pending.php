<?php
 /**
 * @package Reports
 */


define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_paul_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');

if (!isset($treeid))
{
	begin_report('Pending Report');
	print_tree_prompt(ANYBODY);
	end_report();
	exit();
}

/**** defining constants and variables *****/ 

$fiscal_date = getFiscalDate($mpower_companyid );

$fiscal_year = getFiscalYear($fiscal_date);
$fiscal_year = ($fiscal_year > 2000 ) ?  $fiscal_year : $fiscal_year + 2000;  
$next_fiscal_year = $fiscal_year + 1 ; 

$current_month = (int) date("n") ;
$fiscal_month =  (getMonth (getFiscalDate($mpower_companyid ))+1 ) %12;

$months_list = array (  0, 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' );

$current_year = (int) date("Y")  ;	
$next_year = $current_year + 1 ; 

$quarters = array ( 'First Quarter', 'Second Quarter', 'Third Quarter', 'Fourth Quarter' ) ; 
$quarters_color = array ( '#F2FFF2', '#DBFFDB', '#FFEAEA', '#FFBFBF') ; 
$pending_list = array ( 0 => '"A" Pending (95%)', 1 => '"B" Pending (75%)', 2 => '"C" Pending (50%)' ) ;

if ( checkSalesPerson($treeid) )  {

	if (!isset($UA) ) {
	begin_report_orig('Pending Report');
	updateActuals($treeid, $fiscal_year); 
	end_report();
	exit (); 
	}
}

begin_report('Pending Report');

if ( isset( $_GET['UA']) ) {
	if ( $_POST['submit'] == 'UPDATE' ) {
		$updatesql = '' ; 
				
		foreach (array($fiscal_year, ($fiscal_year+1 )) as $i ) {
			$agency1 = convArrayFormat($_POST['agency_'.$i  ], 12); 
			$direct1 = convArrayFormat($_POST['direct_'.$i  ], 12);
			if ( $_POST[$i . "_id"] != ''){ 	// update value 
				$updatesql .=" UPDATE Pending
						SET Agency = '$agency1', Direct = '$direct1', LastUpdated = getDate() ,  CompanyID = '$mpower_companyid' 
						WHERE PendingID =" . $_POST[$i . "_id"] .';';
			} 
			else {										// insert new 
				$updatesql .= " INSERT INTO Pending 
								   (PersonID, Year, CompanyID, Agency, Direct, LastUpdated)
							 VALUES ('$treeid', '$i', '$mpower_companyid', '$agency1', '$direct1', getDate() ); ";
			}
		}
//		print_r($_POST['agency_2009']);
//		echo $updatesql ; 
		$result = mssql_query($updatesql);
	}
}


$idList = GetSalespeopleTree($treeid);

$productSql = "select Opportunities.Company, Opportunities.Category, Opportunities.dealID,
     Opp_Product_XRef.Month_1, Opp_Product_XRef.Month_2, Opp_Product_XRef.Month_3,
     Opp_Product_XRef.Month_4, Opp_Product_XRef.Month_5, Opp_Product_XRef.Month_6,
     Opp_Product_XRef.Month_7, Opp_Product_XRef.Month_8, Opp_Product_XRef.Month_9,
     Opp_Product_XRef.Month_10, Opp_Product_XRef.Month_11, Opp_Product_XRef.Month_12,
     Opp_Product_XRef.Val, Opportunities.Agency
from Opportunities 
left outer join Opp_Product_XRef on Opportunities.DealID = Opp_Product_XRef.DealID 
left join products on Products.ProductID = Opp_Product_XRef.ProductID 
where opportunities.PersonID in ($idList) and 
      products.companyid = $mpower_companyid and 
      opportunities.category in (2,3,4,5) 
order by Products.ProductID, Opportunities.Category ";

echo $productSql;

$productResult = mssql_query($productSql);

$pending = array( 0 => array() , 1 => array(), 2 => array() ) ;  

$a = 2 ; 
while ($row = mssql_fetch_assoc($productResult))
{
	if ( $row['Category'] == 4 ) {
		$a = 0 ;
	}
	else if ( $row['Category'] == 2 ) {
		$a = 1 ;
	}
	else {
		$a = 2 ; 
	}
//	if ( $pending[$a][$row['dealID']] == NULL ) {
	if ( empty ($pending[$a][$row['dealID']] )  ) {
		$pending[$a][$row['dealID']]['company'] = $row['Company'] ; 
		for ($i = 1 ; $i <= 12 ; $i++){
			$pending[$a][$row['dealID']][$i] = $row['Month_' . $i];
		}
		$pending[$a][$row['dealID']]['Val'] = $row['Val'];
		$pending[$a][$row['dealID']]['agency'] = $row['Agency'];
		
	}
	else {
		for ($i = 1 ; $i <= 12 ; $i++){
			$pending[$a][$row['dealID']][$i] += $row['Month_' . $i];
		}

		$pending[$a][$row['dealID']]['Val'] += $row['Val'];
		
	}
// calculate for direct and indirect 	
}

$sqlstr = "select FirstName, LastName from people where PersonID = $treeid";
$result = mssql_query($sqlstr);
$reportPerson = mssql_fetch_assoc($result);

?>

		<link rel="stylesheet" type="text/css" href="../css/report.css">
		<script src="../javascript/utils.js,sorttable2.js,filter2.js"></script>
		</td>
	</tr>
</table>
<br>

<p align=left style="font-size:12pt; font-weight:bold;"><?=$reportPerson['FirstName']?> <?=$reportPerson['LastName']?>  <a href="" style="font-size:10pt; font-weight:normal;text-decoration:none">- Edit Actuals</a></p>
<br><?
	 
/***** quarters calculation ******/
	 
	$current_q = which_quarter($current_month, $fiscal_month) ; 
	// check what is the start month 
	$start_month = ( $fiscal_month + ( ($current_q - 1) * 3 ) )% 12;
	if ( $start_month == 0 ) $start_month = 1 ;  
//	echo "Current quarter " .$current_q . " start month " .  $start_month;
	
	$totalMonth = array() ;  

	echo '<table class="report">';
	// change for color coded quarters 
	foreach ($pending_list as $a => $header) {
	?>
			<center>
				<colgroup>
					<col span=2 />
					<col span=1 />
				<?
				$q = $current_q - 1; 
				
				for ($f =0 ; $f < 4 ; $f++ ){
					if ($q > 3 ){
						$q = 0 ;
					}  
					echo '<col span=4 style="background-color: ' .  $quarters_color[$q] . '"/>'; 
					$q++ ; 
				}
				?>
					<col span=4 style="background-color: #DDDDFF" />
				</colgroup>
				<tr>
                    <th rowspan=2>&nbsp; </th>
					<th rowspan=2><?echo $header ?> </th>
					<th rowspan=2 class="ThickLeft">Type  </th>
	<?			
				// print the quarters 
				$q = $current_q - 1; 
				$current_years = $current_year ;  
				for ($f =0 ; $f < 4 ; $f++ ){
					if ($q > 3 ){
						$q = 0 ;
						$current_years++ ;
					}  
					$year = (string) $current_years; 
					echo '<th colspan=4 class="ThickLeft">' .  $quarters[$q] . " '" . $year[2] . $year[3] .  '</th>' ; 
					$q++ ; 
				}

				echo '<th rowspan=2 class="ThickLeft">Total</th>';
				echo '</tr>';
				echo '<tr>';
			
				// print the months 
				$q =$start_month ; 
				for ( $f= 1 ; $f < 17 ; $f++ ) {
					
					if ($q > 12 ) $q = 1; 
					
					if ( ($f%4) == 0 ){
						echo '<th class="ThickLeft">Total</th>' ;  
					}
					else {
						echo '<th class="ThickLeft">' . $months_list[$q]  .  '</th>' ;
						$q++;
					}
				}	
				echo '</tr>'; 

				$total = 0 ; 
				$b = 1 ;
				
                foreach ($pending[$a] as $id => $name) {
					 
					echo '<tr>';
                    echo '<td>'. $b .'</td>';
					echo '<td class="jl">' . $pending[$a][$id]['company'] . '</td>'; // name 
					echo '<td class="jl">' ; 
					if ( $pending[$a][$id]['agency'] == 1 ) echo 'Agency' ;
					else echo 'Direct' ; 
					
					echo '</td>'; // type 

					$total_q = array ( 0, 0, 0, 0 ) ;
					
					$e = $start_month ; 
					$f = $current_year ; 
					for ( $d = 1 ; $d < 17 ; $d++ ){
						if ($e > 12 ){
							$e = 1 ;
							$f++;
						}
						
						if ( $d % 4 != 0 ) {
							if ( ( $e < $current_month && $current_year == $f ) || ( $current_year != $f && $e >= $current_month)) {
								echo '<td class="jr" style="text-align:right"><strike>' . CurrencyFormat($pending[$a][$id][$e], 0 ) . '</strike></td>';
							} // case if the value is 12 and 1 ?? how 
							else {
							echo '<td class="jr" style="text-align:right">' . CurrencyFormat($pending[$a][$id][$e], 0 ) . '</td>';
							$total_q[which_quarter($e, $fiscal_month)] = (!(isset($total_q[which_quarter($e, $fiscal_month)]))) ? $pending[$a][$id][$e] : $total_q[which_quarter($e, $fiscal_month)] + $pending[$a][$id][$e] ; 
							}
							$e++ ; 
							
						} 
						else{
							if ( $e == 1 ) echo '<td class="ThickLeftjr" style="text-align:right">' . CurrencyFormat($total_q[which_quarter(12, $fiscal_month)], 0 )  . '</td>';
							else echo '<td class="ThickLeftjr" style="text-align:right">' . CurrencyFormat($total_q[which_quarter($e-1, $fiscal_month)], 0 )  .  '</td>';
						} 
					}		
				
					if ( $pending[$a][$id]['Val'] == NULL ) {
						$val = $total_q[1] + $total_q[2] + $total_q[3] + $total_q[4] ; 
					}
					else $val = $pending[$a][$id]['Val'] ;
					
					echo '<td class="ThickLeft jr" style="text-align:right">' . CurrencyFormat($val, 0 ) . '</td>';
					echo '</tr>';
					$total += $val ;
					$b++; 
					for ($i = 1 ; $i <= 12 ; $i++){
					//	if  $totalMonth[$a][$i] = 0 ; 
						$totalMonth[$a][$i] = (!(isset($totalMonth[$a][$i]))) ? $pending[$a][$id][$i] : $totalMonth[$a][$i] + $pending[$a][$id][$i] ; 
					}

					$totalMonth[$a][13] = (!(isset($totalMonth[$a][13]))) ? $total_q[1] : $totalMonth[$a][13] + $total_q[1] ; 
					$totalMonth[$a][14] = (!(isset($totalMonth[$a][14]))) ? $total_q[2]: $totalMonth[$a][14] + $total_q[2] ; 
					$totalMonth[$a][15] = (!(isset($totalMonth[$a][15]))) ? $total_q[3] : $totalMonth[$a][15] + $total_q[3] ; 
					$totalMonth[$a][16] = (!(isset($totalMonth[$a][16]))) ? $total_q[4] : $totalMonth[$a][16] + $total_q[4] ; 
					$totalMonth[$a][17] = (!(isset($totalMonth[$a][17]))) ? $pending[$a][$id]['Val'] : $totalMonth[$a][17] + $pending[$a][$id]['Val'] ; 
					
					if ( $pending[$a][$id]['agency'] == 1 ){
						for ($i = 1 ; $i <= 12 ; $i++){
							$totalMonth[$a]['agency'][$i] += $pending[$a][$id][$i] ; 
						}
						$totalMonth[$a]['agency'][13] += $total_q[1] ; 
						$totalMonth[$a]['agency'][14] += $total_q[2] ; 
						$totalMonth[$a]['agency'][15] += $total_q[3] ; 
						$totalMonth[$a]['agency'][16] += $total_q[4] ; 
						$totalMonth[$a]['agency'][17] += $pending[$a][$id]['Val'] ; 	
					}
				}
				if ( $b == 1 ) {
					echo '<tr>';
					echo '<td colspan=19 class="ThickLeftjr">' . 'Empty' . '</td>';
					echo '</tr>';
				}
				echo '<tr>' ; 
				echo '<th colspan=3>Totals</th>';
				$e=$start_month ; 
				$q = $current_year ; 
				for ( $f= 1 ; $f < 17 ; $f++ ) {
					if ($e > 12 ){
						$e = 1 ;
						$q++ ; 
					}
					if ( $f % 4 != 0 )  {
						if ( ( $e < $current_month && $current_year == $q ) || ( $current_year != $q && $e >= $current_month)) {
							echo '<th class="jr" style="text-align:right"><strike>' . CurrencyFormat($totalMonth[$a][$e], 0 ) . '</strike></th>' ; 
						}
						else {
							echo '<th class="jr" style="text-align:right">' . CurrencyFormat($totalMonth[$a][$e], 0 ) . '</th>' ; 
						}$e++; 
					}
					else {						
						if ( $e == 1 ) echo '<th class="jr" style="text-align:right">' . CurrencyFormat($totalMonth[$a][which_quarter(12, $fiscal_month) + 12], 0 )  . '</td>';
						else echo '<th class="ThickLeft jr" style="text-align:right">' . CurrencyFormat($totalMonth[$a][which_quarter($e-1, $fiscal_month) + 12], 0 )  .  '</td>';
					}
				}
				?>

					<th class="ThickLeft jr" style="text-align:right">$<?=CurrencyFormat($totalMonth[$a][17], 0 ) ?></th>

				</tr>
				<tr>
					<td colspan=20 height="40px" style="border-style:none"> &nbsp; </td>
				</tr>	
	<?
	}
	?>
	</table>
		
	
		<P style="page-break-after:always; height:0;line-height:0;"> </P>
		<br>
		<br>
		
<table border=1 cellspacing=10 cellpadding=0 >
<tr>
		
	<?
	
	foreach ( array($fiscal_year, $next_fiscal_year ) as $j ){

	// Obtaining Goals and Actuals 
	$sqlAct = "select p.Goal, p.TotalGoal, p.Agency, p.Direct 
			from pending p where personID in ($idList) and p.Year = " . ($j ) ;
	$Act = mssql_query($sqlAct);
	$actuals = array() ;
	$pending = array () ;
	while ( $row = mssql_fetch_assoc($Act)) {
		$temp1 = explode( ";", $row['Agency']) ;
		$temp2 = explode( ";", $row['Direct']) ;
		$temp3 = explode( ";", $row['Goal']) ; 
		$pending['total'] = (!(isset($pending['total']))) ? (float) $row['TotalGoal'] : $pending['total'] + (float) $row['TotalGoal'];
//		$pending['total'] += (float) $row['TotalGoal'];
		for ($i =0 ; $i < 12 ; $i++){
			$actuals['agency'][$i+1] = (!(isset($actuals['agency'][$i+1]))) ? (float) (float) unformat_money($temp1[$i]) : (float)$actuals['agency'][$i+1] + (float) unformat_money($temp1[$i]) ;
			$actuals['direct'][$i+1] = (!(isset($actuals['direct'][$i+1]))) ? (float) (float) unformat_money($temp2[$i]) : (float)$actuals['direct'][$i+1] + (float) unformat_money($temp2[$i]) ;
			$actuals['both'][$i+1] = (!(isset($actuals['both'][$i+1]))) ? (float) unformat_money($temp1[$i]) + (float) unformat_money($temp2[$i]) : (float)$actuals['both'][$i+1] + (float) unformat_money($temp1[$i]) + (float) unformat_money($temp2[$i]) ;
			$pending['goal'][$i+1] = (!(isset($pending['goal'][$i+1]))) ? (float) unformat_money($temp3[$i]) : (float)$pending['goal'][$i+1] + (float) unformat_money($temp3[$i]) ;		
//			$actuals['agency'][$i+1] += (float) unformat_money($temp1[$i]) ;
//			$actuals['direct'][$i+1] += (float) unformat_money($temp2[$i]) ;
//			$actuals['both'][$i+1] += (float) unformat_money($temp1[$i]) + (float) unformat_money($temp2[$i]) ;
//			$pending['goal'][$i+1] += (float) unformat_money($temp3[$i]) ;		
			
		}

	}
	$totalmonth['total']['agency'] = 0; 
	$totalmonth['total']['direct'] = 0; 
	$totalmonth['total']['both'] = 0 ; 
	$totalmonth['total']['goal'] = 0 ; 
	$totalmonth['total']['needed'] = 0 ; 
	$totalmonth['total']['pending'] = 0 ; 
	
//	print print_r($actuals, true ); 
	?>

	<td>
	<table class="report">
	<center>
		<tr>
			<th colspan=8 >Pending Report w/ Actuals of Fiscal Year <?echo ($j)?></th>
		</tr>
		<tr>
            <th > &nbsp;  </th>
			<th class="ThickLeft">Agency </th>
			<th class="ThickLeft">Direct  </th>
			<th class="ThickLeft">Total  </th>
			<th class="ThickLeft">Goal </th>
			<th class="ThickLeft">% of Goal  </th>
			<th class="ThickLeft"><font color=red>$ Needed  </font></th>
			<th class="ThickLeft">Pending A & B </th>
		</tr>
		<?
		$month = $fiscal_month % 12; // $current_month ; instead of current month use fiscal month
		$prev_year = 0 ; 
		$old_q = '1' ; // old always starts off with 1 
		$goal = 10000 ;  
			
		for ($i = 0 ; $i < 12 ; $i++ ) {
			

			echo '<tr style="background-color: #F2FFF2; ">';
			$year = ( getMonth(getFiscalDate($mpower_companyid )) < 12 ) ?(string) ($j-1 ) : (string) ($j );
			echo "<td>". $months_list[$month] .  " '" . $year[2]. $year[3] . " </td>" ;
			echo '<td style="text-align:right"> $'. CurrencyFormat($actuals['agency'][$month], 0) . "</td>" ;
			echo '<td style="text-align:right"> $'. CurrencyFormat($actuals['direct'][$month], 0) . "</td>" ;
			echo '<td style="text-align:right"> $'. CurrencyFormat($actuals['both'][$month], 0 ). "</td>" ;
			echo '<td style="text-align:right"> $'. CurrencyFormat($pending['goal'][$month], 0 ). "</td>" ;
			echo '<td style="text-align:right">'. redblack(GetPercent( $actuals['both'][$month], $pending['goal'][$month] ), '%'). " </td>" ;
			echo '<td style="text-align:right"> '. redblack( ($pending['goal'][$month] - $actuals['both'][$month]), '$'  ). " </td>" ;
			echo '<td style="text-align:right"> $'. CurrencyFormat( ($totalMonth[0][$month] + $totalMonth[1][$month]) , 0 ). "</td>" ;
			echo '</tr>'; 
			$totalmonth['agency'] = (!(isset($totalmonth['agency']))) ? 0 : $totalmonth['agency'] + $actuals['agency'][$month]; 
			$totalmonth['direct'] = (!(isset($totalmonth['direct']))) ? 0 : $totalmonth['direct'] + $actuals['direct'][$month]; 
			$totalmonth['both'] = (!(isset($totalmonth['both']))) ? 0 : $totalmonth['both'] + $actuals['both'][$month] ; 
			$totalmonth['goal'] = (!(isset($totalmonth['goal']))) ? 0 : $totalmonth['goal'] + $pending['goal'][$month] ; 
			$totalmonth['needed'] = (!(isset($totalmonth['needed']))) ? 0 : $totalmonth['needed'] + ($pending['goal'][$month] - $actuals['both'][$month]) ; 
			$totalmonth['pending'] = (!(isset($totalmonth['pending'])) ) ? 0 : $totalmonth['pending'] + ($totalMonth[0][$month] + $totalMonth[1][$month]) ; 
			$totalmonth['total']['agency'] += $actuals['agency'][$month]; 
			$totalmonth['total']['direct'] += $actuals['direct'][$month]; 
			$totalmonth['total']['both'] += $actuals['both'][$month] ; 
			$totalmonth['total']['goal'] += $pending['goal'][$month] ; 
			$totalmonth['total']['needed'] += ($pending['goal'][$month] - $actuals['both'][$month]) ; 
			$totalmonth['total']['pending'] += ($totalMonth[0][$month] + $totalMonth[1][$month]) ; 
			$month++ ; 

			if ($month > 12 ){
				$month = 1 ;
				$j++ ; 
			}
			
			if ( $old_q != which_quarter($month, $fiscal_month)) {
		
				echo '<tr style="background-color: '.	$quarters_color[1].'">' ; 
				echo '<td style="border-width:2 1 2 2; "> Quarter '.  $old_q . " </td>" ;
				echo '<td style="border-width:2 1 2 1; text-align:right;"> $'. CurrencyFormat($totalmonth['agency'], 0) . "</td>" ;
				echo '<td style="border-width:2 1 2 1; text-align:right;"> $'. CurrencyFormat($totalmonth['direct'], 0) . "</td>" ;
				echo '<td style="border-width:2 1 2 1; text-align:right;"> $'. CurrencyFormat($totalmonth['both'], 0 ). "</td>" ;
				echo '<td style="border-width:2 1 2 1; text-align:right;"> $'. CurrencyFormat($totalmonth['goal'], 0) . "</td>" ;
				echo '<td style="border-width:2 1 2 1; text-align:right;"> '. redblack((GetPercent($totalmonth['both'] , $totalmonth['goal'])), '%'  ) ."  </td>" ;
				echo '<td style="border-width:2 1 2 1; text-align:right;"> '. redblack($totalmonth['needed'], '$'). " </td>" ;
				echo '<td style="border-width:2 2 2 1; text-align:right;"> $'. CurrencyFormat($totalmonth['pending'], 0  ). "</td>" ;
				echo "</tr>"; 
				$old_q = which_quarter($month, $fiscal_month);
				$totalmonth['agency'] = 0; 
				$totalmonth['direct'] = 0; 
				$totalmonth['both'] = 0 ; 
				$totalmonth['goal'] = 0 ; 
				$totalmonth['needed'] = 0 ; 
				$totalmonth['pending'] = 0 ; 

			}
			

		}
		if (isset($prev_year_report) &&  $prev_year_report == 0 ) echo $prev_year_report ;
		
		echo '<tr style="background-color: #FFBFBF; font-weight:bold;">' ; 
		echo "<td> Total for". ($j-1) ."</td>" ;
		echo "<td  style=\"text-align:right\"> $". CurrencyFormat($totalmonth['total']['agency'], 0 ) . "</td>" ;
		echo "<td style=\"text-align:right\"> $". CurrencyFormat($totalmonth['total']['direct'] , 0 ) . "</td>" ;
		echo "<td style=\"text-align:right\"> $". CurrencyFormat($totalmonth['total']['both'] , 0 ). "</td>" ;
		echo "<td style=\"text-align:right\"> $". CurrencyFormat($totalmonth['total']['goal'] , 0 ) . "</td>" ;
		echo "<td style=\"text-align:right\"> ".GetPercent($totalmonth['total']['both'] , $totalmonth['total']['goal']  )." % </td>" ;
		echo "<td style=\"text-align:right\"> $". CurrencyFormat($totalmonth['total']['needed'], 0 ). " </td>" ;
		echo "<td style=\"text-align:right\"> $". CurrencyFormat($totalmonth['total']['pending'], 0). "</td>" ;
		echo "</tr>"; 
			
		
		?>
		

	<?

	?>
	</table>	
		
	</td>
	<?
	
	} 
	echo "</tr></table>" ; 
	end_report();
	?>

<?php

function GetPercent($numerator, $denominator) {
    $output = ($denominator != 0) ? CurrencyFormat((round($numerator/$denominator * 1000)/10), 1) : '-';    
//	$output = ($output <= 0.1) ? '0.0' : $output ; 
    return $output;
}

function NullToZero($value) {
    $output = (is_null($value)) ? 0 : $value;
    return $output;
}

// returns 0 for error 
function which_quarter($month, $fiscal_month){

	 if ( $month == $fiscal_month ) return 1 ; 
	else if ( $month < $fiscal_month ) {
		$month = $month + 12 ;
	}
	
	$diff = $month - $fiscal_month ; 
	
	if ( $diff >= 9 ) return 4 ; 
	else if ( $diff >= 6 ) return 3 ; 
	else if ( $diff >= 3 ) return 2 ; 
	else if ( $diff > 0 ) return 1 ; 
	else return 0 ; 

}

// takes in sql timestamp
// input "Sep 30 2009 12:00AM"  or 09/30/2009 00:00:00
// output 9 
// return 0 if error 
function getMonth ($a){ 

	$months_list = array (  'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' );
	$part = explode(" ", $a);
	if ( count($part) > 2 ) {
		for ($v = 0 ; $v < 12 ; $v++){
			if ( $part[0] == $months_list[$v] ) return ($v + 1); 	
		}	
	}
	else {
		$parts = explode("/", $part[0]) ;
		return (int) $parts[0] ; 
	}
	return 0  ; 
}
function getYear ($a) {
	$part = explode(" ", $a);
	if ( count($part) > 2 ) {
		return (int) $part[2] ; 
	}
	else {
		$parts = explode("/", $part[0]) ;
		return (int) $parts[2] ; 
	}
	return 0  ; 
}

function getFiscalYear($fiscalDate){
	$parts = explode(" ", $fiscalDate);
	if ( count($parts) > 2 ) {
		$date = array ( 'm' => getMonth($fiscalDate), 'd' => $parts[1], 'y'=> $parts[2] ) ;
	}
	else {
		$win = explode("/", $parts[0]) ;
		$date = array ( 'm' =>  getMonth($fiscalDate), 'd' => $win[1], 'y'=> $win[2] ) ;
	}
	
	$today = mktime(0, 0, 0, date("m") , date("d"), date("Y"));
	$fiscaldate = mktime(0, 0,0, $date['m'], $date['d'], $date['y']);
	while ($today > $fiscaldate) {
		$date['y']++ ; 
		$fiscaldate = mktime(0, 0,0, $date['m'], $date['d'], $date['y']);
	
	}

	return $date['y'];
	
}


function getFiscalDate($companyID ) {
	$strsql = "SELECT *, DATEDIFF(month, GETDATE(), FiscalYearEndDate) as datadiff FROM FiscalYear where companyID = $companyID and DATEDIFF(month, GETDATE(), FiscalYearEndDate) >= 0  AND DATEDIFF(month, GETDATE(), FiscalYearEndDate) <= 12  " ; 
	$result1 = mssql_query($strsql);
	$fiscal_date =  mssql_fetch_assoc($result1);
	return $fiscal_date['FiscalYearEndDate']; 
}

 function checkSalesPerson ($id ) {
	$sql = "select isSalesperson from people where personID = $id" ; 
	$productResult = mssql_query($sql);
	$result = mssql_fetch_assoc($productResult) ;  
	return $result['isSalesperson'] ; 
}

// input array $arr
// output csv 
function convArrayFormat( $arr, $count = 0 ) {
	
	if ($count == 0) $count = count($arr) ; 
	ksort($arr) ; 
	// cleaning data 
	for ( $i = 1 ; $i <= $count ; $i++ ) {
		$arr[$i] = trim ((string) $arr[$i]);
		if ($arr[$i][0] == '$' )$arr[$i] = str_replace("$", '', $arr[$i]) ;//array_shift($arr[$i]) ; 
	}
	return implode ( ";", $arr)  ; 
}

function unformat_money( $money) {
	$money = str_replace("$", '', $money); 
	$money = str_replace(",", '', $money); 
	return $money;
}

function redblack( $money, $type= '$'){
   if ( $type == '$' ){
	if ( $money < 0 ) return "$ ". currencyFormat(($money * -1), 0 ) ; 
	else return "<font color=red>$ ". currencyFormat($money, 0) ."</font>"; 
   }
   else {
	if ( $money < 100 ) return "<font color=red>".(  (int)$money  ) ."% </font>"; 
	else return $money . " %";
   
   } 
	
}


?>

