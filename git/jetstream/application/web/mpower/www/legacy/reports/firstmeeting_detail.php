<?php
 /**
 * @package Reports
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_paul_utils.php');
include('TableUtils.php');
include('firstmeeting_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');

if (!isset($treeid))
{
	begin_report_orig(GetCatLbl('First Meeting') . ' Detail Report');
	print_tree_prompt(SALESPEOPLE);
	end_report_orig();
	exit();
}

if (!isset($span_mode) || $span_mode < 0)
{
	begin_report_orig(GetCatLbl('First Meeting') . ' Detail Report');
	?>
	<center>
	<br>
	Please select the desired time period:<br>
	<form name="form1" method="GET" action="firstmeeting_detail.php">
		<input type="hidden" name="treeid" value="<?=$treeid?>"></input>
		<input type="hidden" name="SN" value="<?=$_REQUEST['SN']?>"></input>	
		<select name="span_mode">
			<option value="-1"></option>
			<option value="0">Current Month to Date</option>
			<option value="1">Last Month to Date</option>
			<option value="2">Last Month</option>
			<option value="3">Current Quarter to Date</option>
			<option value="4">Last Quarter to Date</option>
			<option value="5">Last Quarter</option>
			<option value="6">Current Year to Date</option>
			<option value="7">Last Year to Date</option>
			<option value="8">Last Year</option>
		</select>
		<input type="submit" value="Go"></input>
	</form>
	</center>
	<?
	end_report_orig();
	exit();
}

$sql = "select * from people where PersonID=$treeid";
$result = mssql_query($sql);
$sprow = mssql_fetch_assoc($result);

begin_report(GetCatLbl('First Meeting') . ' Detail Report');
//print_tree_path($treeid);
$id = $treeid;

$sql = "select *, CAST(FirstMeetingTime as DateTime) as AltFMTime from opportunities where ISDATE(FirstMeetingTime) = 1 and PersonID in ($id) and FirstMeeting is not null and Category<>1 and Category<>10 order by FirstMeeting asc, AltFMTime asc";

$result = mssql_query($sql);

$current = time();
$thismonth = GetMonth($current);
$thisyear = GetYear($current);
$thisday = GetDay($current);

$span_text = '';
switch ($span_mode)
{
	case 0: $span_text = 'Current Month to Date'; break;
	case 1: $span_text = 'Last Month to Date'; break;
	case 2: $span_text = 'Last Month'; break;
	case 3: $span_text = 'Current Quarter to Date'; break;
	case 4: $span_text = 'Last Quarter to Date'; break;
	case 5: $span_text = 'Last Quarter'; break;
	case 6: $span_text = 'Current Year to Date'; break;
	case 7: $span_text = 'Last Year to Date'; break;
	case 8: $span_text = 'Last Year'; break;
}

?>
<style>

BODY
{
	font-family:Arial;
	font-size:10pt;
}

.ColumnHeader
{
	font-size:10pt;
	font-weight:bold;
	border:1px solid black;
	background-color:#DDDDDD;
}

.ColumnHeaderThickLeft
{
	font-size:10pt;
	font-weight:bold;
	border:1px solid black;
	border-left:3px solid black;
	background-color:#DDDDDD;
}

.TableCell
{
	border:1px solid black;
}

.TableCellNoRight
{
	border-top:1px solid black;
	border-bottom:1px solid black;
	border-left:1px solid black;
}

.TableCellNoLeft
{
	border-top:1px solid black;
	border-bottom:1px solid black;
	border-right:1px solid black;
}

.TableCellThickLeft
{
	border:1px solid black;
	border-left:3px solid black;
}

table.sortable a.sortheader
{
	text-decoration: none;
	color: black;
}

table.sortable span.sortarrow
{
	text-decoration: none;
}

</style>
<script src="../javascript/sorttable.js,utils.js"></script>
<center>
<span style="font-size:12pt; font-weight:bold;">
First Meetings Held<br>
<?=$span_text?><br>
for<br>
<?=$sprow['FirstName']?> <?=$sprow['LastName']?>
</span>
<br>
<br>
<table width="600" cellpadding="4" cellspacing="0" style="border:1px solid black; font-family:Arial; font-size:8pt; background-color:white;">
	<thead style="display: table-header-group;">
		<td align="center" class="ColumnHeader" nowrap width="1">&nbsp</td>
		<td align="center" class="ColumnHeader" nowrap>Company<br><nobr style="font-size:8pt;"><div style="font-size:1px; position:relative; top:2px; width:12px; height:12px; background-color:black; display:inline;">&nbsp;</div>&nbsp;=&nbsp;two or more of<br>same company on report</nobr></td>
		<td align="center" class="ColumnHeader" nowrap>Current<br>Location</td>
		<td align="center" class="ColumnHeader" nowrap>Last<br>Location<br><span style="font-size:8pt;">(if removed)</span></td>
		<td align="center" class="ColumnHeader" nowrap>Date<br>Removed</td>
		<td align="center" class="ColumnHeader" nowrap>Days In<br>Sales<br>Cycle</td>
		<td align="center" class="ColumnHeader" nowrap colspan="2">First Meeting<br>Date & Time<br><nobr style="font-size:8pt;"><div style="font-size:1px; position:relative; top:2px; width:12px; height:12px; background-color:black; display:inline;">&nbsp;</div>&nbsp;=&nbsp;scheduling conflict</nobr></td>
	</thead>
<?

$dateTimeMap = array();
$companyMap = array();

$k = 0;
while ($row = mssql_fetch_assoc($result))
{
	$lastcat = '&nbsp;';
	if ($row['Category'] == 9)
	{
		$dealid = $row['DealID'];
		$sql = "select LogInt from [log] where DealID=$dealid and TransactionID=1 order by WhenChanged desc";
		$logresult = mssql_query($sql);
		$lastcat = 'Unk.';
		while ($logrow = mssql_fetch_assoc($logresult))
		{
			if ($logrow['LogInt'] != 9)
			{
				switch ($logrow['LogInt'])
				{
					case 1: $lastcat = GetCatLbl('FM'); break;
					case 2: $lastcat = GetCatLbl('IP'); break;
					case 3: $lastcat = GetCatLbl('IPS'); break;
					case 4: $lastcat = GetCatLbl('DP'); break;
					case 5: $lastcat = GetCatLbl('DPS'); break;
					case 6: $lastcat = GetCatLbl('C'); break;
					case 9: $lastcat = GetCatLbl('RM'); break;
					case 10: $lastcat = GetCatLbl('T'); break;
				}
				break;
			}
		}
		if ($logrow['LogInt'] == 1 && $row['FM_took_place'] != 1)
			continue;
	}
	
	$date = strtotime($row['FirstMeeting']);

	if (fm_ok($date, $thismonth, $thisyear, $thisday, $span_mode)):
		$k++;
	?>
		<tr>
			<td class="TableCell" align="right"><?=$k?></td>
			<?
				$extrastyle = '';
				if (isset($companyMap[$row['Company']]))
					$extrastyle = 'style="background-color:black; color:white;"';
				else
					$companyMap[$row['Company']] = $k;
			?>
			<td id="tdcompany<?=$k?>" class="TableCell" <?=$extrastyle?> ><?=$row['Company']?></td>
			<?if ($extrastyle != '' && $companyMap[$row['Company']] >= 0):?>
				<script language="Javascript">
					var tdcompany = document.getElementById("tdcompany<?=$companyMap[$row['Company']]?>");
					tdcompany.style.backgroundColor = 'black';
					tdcompany.style.color = 'white';
				</script>
				<?$companyMap[$row['Company']] = -1;?>
			<?endif;?>
			<td class="TableCell" align="center">
				<?
				switch ($row['Category'])
				{
					case 1: print(GetCatLbl('FM')); break;
					case 2: print(GetCatLbl('IP')); break;
					case 3: print(GetCatLbl('IPS')); break;
					case 4: print(GetCatLbl('DP')); break;
					case 5: print(GetCatLbl('DPS')); break;
					case 6: print(GetCatLbl('C')); break;
					case 9: print(GetCatLbl('RM')); break;
					case 10: print(GetCatLbl('T')); break;
				}
				?>
			</td>
			<td class="TableCell" align="center">
				<?if ($row['Category'] == 9):
					$dealid = $row['DealID'];
					$sql = "select * from [log] where DealID=$dealid and TransactionID=1 order by WhenChanged desc";
					$logresult = mssql_query($sql);
					$lastcat = '&nbsp;';
					while ($logrow = mssql_fetch_assoc($logresult))
					{
						if ($logrow['LogInt'] != 9)
						{
							switch ($logrow['LogInt'])
							{
								case 1: $lastcat = GetCatLbl('FM'); break;
								case 2: $lastcat = GetCatLbl('IP'); break;
								case 3: $lastcat = GetCatLbl('IPS'); break;
								case 4: $lastcat = GetCatLbl('DP'); break;
								case 5: $lastcat = GetCatLbl('DPS'); break;
								case 6: $lastcat = GetCatLbl('C'); break;
								case 9: $lastcat = GetCatLbl('RM'); break;
								case 10: $lastcat = GetCatLbl('T'); break;
							}
							break;
						}
					}
				?>
					<?=$lastcat?>
				<?else:?>
					&nbsp;
				<?endif;?>
			</td>
			<td class="TableCell" align="right">
				<?if ($row['Category'] == 9):?>
					<?=date('m/d/y', strtotime($row['RemoveDate']))?>
				<?else:?>
					&nbsp;
				<?endif;?>
			</td>
			<td class="TableCell" align="right" width="28">
				<?
				if ($row['Category'] == 9)
					$enddate = strtotime($row['RemoveDate']);
				else if ($row['Category'] == 6)
					$enddate = strtotime($row['ActualCloseDate']);
				else
					$enddate = time();
				$startdate = strtotime($row['FirstMeeting']);
				print(floor(($enddate-$startdate)/86400));
				?>
			</td>
			<?
				$extrastyle = '';
				$dthash = $row['FirstMeeting'].$row['FirstMeetingTime'];
				if (isset($dateTimeMap[$dthash]))
					$extrastyle = 'style="background-color:black; color:white;"';
				else
					$dateTimeMap[$dthash] = true;
			?>
			<td id="fmdate<?=$k?>" class="TableCellNoRight" align="right" <?=$extrastyle?> nowrap><?=date('m/d/y', strtotime($row['FirstMeeting']))?></td>
			<td id="fmtime<?=$k?>" id="" class="TableCellNoLeft" align="right" <?=$extrastyle?> nowrap><?=$row['FirstMeetingTime']?></td>
			<?if ($extrastyle != ''):?>
				<script language="Javascript">
					var fmdate = document.getElementById('fmdate<?=$k-1?>');
					fmdate.style.backgroundColor = 'black';
					fmdate.style.color = 'white';
					var fmtime = document.getElementById('fmtime<?=$k-1?>');
					fmtime.style.backgroundColor = 'black';
					fmtime.style.color = 'white';
				</script>
			<?endif;?>
		</tr>
	<?
	endif;
}
?>
</table>
</center>

<? end_report(); ?>
