<?php
 /**
 * @package Reports
 */

function PrintColumnHeaders()
{
	BeginTableRowData(1);
	PrintEmptyTableCell();
	PrintTableCell("<b>Current Month to Date");
	PrintTableCell("<b>Last Month to Date");
	PrintTableCell("<b>Last Month");
	PrintEmptyTableCell();
	PrintTableCell("<b>Current Quarter to Date");
	PrintTableCell("<b>Last Quarter to Date");
	PrintTableCell("<b>Last Quarter");
	PrintEmptyTableCell();
	PrintTableCell("<b>Year to Date");
	PrintTableCell("<b>Last Year to Date");
	PrintTableCell("<b>Last Year");
	EndTableRow();
}

function GetMonth($date)
{
	$str = date("m", $date);
	return (int)$str;
}

function GetYear($date)
{
	$str = date("Y", $date);
	return (int)$str;
}

function GetDay($date)
{
	$str = date("d", $date);
	return (int)$str;
}

function DateInRange($date, $firstmonth, $lastmonth, $firstyear, $lastyear, $firstday, $lastday)
{
	if($lastday < 0)
	{
		$lastday = 1;
		if($lastmonth < 12) $lastmonth ++;
		else
		{
			$lastmonth = 1;
			$lastyear++;
		}
	}

	if($lastmonth == 2)
	{
		$maxdays = 28;
		if ($lastyear % 4 == 0) $maxdays++;

		if($lastday > $maxdays)
		{
			$lastday = 1;
			$lastmonth = 3;
		}
	}

	if($lastmonth == 4 || $lastmonth == 6 || $lastmonth == 9 || $lastmonth == 11)
	{
		$maxdays = 30;
		if($lastday > $maxdays)
		{
			$lastday = 1;
			$lastmonth++;
		}
	}

	$strFirst = "$firstmonth/$firstday/$firstyear";
	$strLast = "$lastmonth/$lastday/$lastyear";
	
	$tempdate = date('n/j/Y', $date);
	//print("$tempdate in $strFirst - $strLast<br></nobr>");

//print("$strFirst -- $strLast<br>");

	$firstdate = strtotime($strFirst);
	$lastdate = strtotime($strLast);
//print("$strFirst($firstdate) -- $strLast($lastdate)<br>");

	if($date < $firstdate) return false;
	if($date < $lastdate) return true;
	return false;
}

function fm_ok($date, $thismonth, $thisyear, $thisday, $rangecode)
{
	$firstmonth = -1;
	$lastmonth = -1;
	$firstyear = -1;
	$lastyear = -1;
	$firstday = 1;
	$lastday = -1;
	
	//print("<nobr>$rangecode: ");

	switch($rangecode)
	{
		case 1: // last month to date
			$lastday = $thisday;
		case 2: // last month
			if($thismonth > 1)
				$thismonth--;
			else
			{
				$thismonth = 12;
				$thisyear--;
			}
		case 0: // thismonth
			$firstmonth = $thismonth;
			$lastmonth = $thismonth;
			$firstyear = $thisyear;
			$lastyear = $thisyear;
			break;

		case 3: // current quarter
			$firstyear = $thisyear;
			$lastyear = $thisyear;

			if($thismonth < 4)
			{
				$firstmonth = 1;
				$lastmonth = 3;
			}
			else if ($thismonth < 7)
			{
				$firstmonth = 4;
				$lastmonth = 6;
			}
			else if ($thismonth < 10)
			{
				$firstmonth = 7;
				$lastmonth = 9;
			}
			else
			{
				$firstmonth = 10;
				$lastmonth = 12;
			}

			break;

		case 4: // last quarter to date
			$firstyear = $thisyear;
			$lastyear = $thisyear;
			$lastday = $thisday;

			if($thismonth < 4)
			{
				$firstmonth = 10;
				$lastmonth = $thismonth + 9;
				$firstyear--;
				$lastyear--;
			}
			else if ($thismonth < 7)
			{
				$firstmonth = 1;
				$lastmonth = $thismonth - 3;
			}
			else if ($thismonth < 10)
			{
				$firstmonth = 4;
				$lastmonth = $thismonth - 3;
			}
			else
			{
				$firstmonth = 7;
				$lastmonth = $thismonth - 3;
			}
			break;

		case 5: // last quarter
			$firstyear = $thisyear;
			$lastyear = $thisyear;

			if($thismonth < 4)
			{
				$firstmonth = 10;
				$lastmonth = 12;
				$firstyear--;
				$lastyear--;
			}
			else if ($thismonth < 7)
			{
				$firstmonth = 1;
				$lastmonth = 3;
			}
			else if ($thismonth < 10)
			{
				$firstmonth = 4;
				$lastmonth = 6;
			}
			else
			{
				$firstmonth = 7;
				$lastmonth = 9;
			}
			break;

		case 6:  // this year
			$firstmonth = 1;
			$lastmonth = 12;
			$firstyear = $thisyear;
			$lastyear = $thisyear;
			break;

		case 7:  // last year to date
			$lastday = $thisday;
			$firstmonth = 1;
			$lastmonth = $thismonth;
			$firstyear = $thisyear - 1;
			$lastyear = $thisyear - 1;
			break;

		case 8:  // last year
			$firstmonth = 1;
			$lastmonth = 12;
			$firstyear = $thisyear - 1;
			$lastyear = $thisyear - 1;
			break;
	}

	return DateInRange($date, $firstmonth, $lastmonth, $firstyear, $lastyear, $firstday, $lastday);
}

function CalculateRanks(&$Districts)
{
	$count = count($Districts);
	for($i = 0; $i < 9; $i++)
	{
		$currentrank = 1;
		$prevrank = 0;
		for($keepgoing=1; $keepgoing>0; )
		{
			$maxcount = 0;
			$keepgoing = 0;

			for($j=0; $j<$count; $j++)
			{
				$dist = $Districts[$j];
				$rank = $dist["rank$i"];
				if($rank > -1)
					continue;

				$keepgoing = 1;
				$c = $dist["count$i"];
				if($c > $maxcount)
					$maxcount = $c;
			}

			$num_of_ties = 0;
			for($j=0; $j<$count; $j++)
			{
				$dist = $Districts[$j];
				$rank = $dist["rank$i"];
				if($rank > -1)
					continue;

				$c = $dist["count$i"];
				if($c == $maxcount)
				{
					$dist["rank$i"] = $currentrank;
					$Districts[$j] = $dist;
					++$num_of_ties;
				}
			}
			$currentrank += $num_of_ties;
		}
	}
}

?>
