<?php
 /**
 * @package Reports
 */
include('../data/_base_utils.php');

function CurrencyFormat($amount, $precision = 2, $use_commas = true, $show_currency_symbol = false, $parentheses_for_negative_amounts = false)
{
	/*
	**    An improvement to number_format.  Mainly to get rid of the annoying behaviour of negative zero amounts.
	*/
	$currencySymbol = '$ ';
	$amount = (float) $amount;
	// Get rid of negative zero
	$zero = round(0, $precision);
	if (round($amount, $precision) == $zero) $amount = $zero;

	if ($use_commas) {
		if ($parentheses_for_negative_amounts && ($amount < 0)) {
			$amount = '(' . number_format(abs($amount), $precision) . ')';
		}
		else {
			$amount = number_format($amount, $precision);
		}
	}
	else {
		if ($parentheses_for_negative_amounts && ($amount < 0)) {
			$amount = '(' . round(abs($amount), $precision) . ')';
		}
		else {
			$amount = round($amount, $precision);
		}
	}

	if ($show_currency_symbol) {
		$amount = $currencySymbol . $amount;  // Change this to use the organization's country's symbol in the future
	}
	return $amount;
}


function begin_report($title)
{
	print_report_top($title);

	if (!check_report_login())
	{
		print_login_prompt();
		end_report();
	}

	// this is no longer used, but I left it and the ensure_parameters() function here in
	// case similar functionality is needed some time in the future.  -jason
	/*if ($params && !ensure_parameters($params))
	{
		print_parameter_prompt($params);
		end_report();
	}*/
}

function end_report()
{
	print_report_bottom();
	close_db();
	exit();
}

function begin_report_orig($title)
{
	print_report_top_orig($title);

	if (!check_report_login())
	{
		print_login_prompt();
		end_report();
	}

	// this is no longer used, but I left it and the ensure_parameters() function here in
	// case similar functionality is needed some time in the future.  -jason
	/*if ($params && !ensure_parameters($params))
	{
		print_parameter_prompt($params);
		end_report();
	}*/
}

function end_report_orig()
{
	print_report_bottom_orig();
	close_db();
	exit();
}

define('ANYBODY', 1);
define('SALESPEOPLE', 2);
define('SALESPEOPLE_MGR', 3);
define('MANAGEMENT', 4);
define('SALESPEOPLE_AND_MGR', 5);


function GetOppProducts($DealID)
{
	$sql = 	"SELECT Opp_Product_XRef.ProductID, products.Name AS ProductName";
	$sql .= " FROM Opp_Product_XRef INNER JOIN products ON Opp_Product_XRef.ProductID = products.ProductID";
	$sql .= " WHERE Opp_Product_XRef.DealID = $DealID";

	$Products = "";
	$ok = mssql_query($sql);
	while(($row=mssql_fetch_assoc($ok)))
	{
		if(strlen($Products) > 0) $Products .= ", ";
		$Products .= $row['ProductName'];
	}
	return $Products;
}

// print the tree and allow the user to select a spot in it to run the report with
// type can be ANYBODY, SALESPEOPLE, SALESPEOPLE_MGR
function print_tree_prompt($type)
{
	global $mpower_companyid, $currentuser_personid, $currentuser_isadmin;

	$peopledata = array();
	$sql = "select * from people where CompanyID = '$mpower_companyid' and Deleted = '0'";
	$sql .= " order by LastName";
	$result = mssql_query($sql);
	while ($row = mssql_fetch_assoc($result)) array_push($peopledata, $row);
	$len = count($peopledata);

	if ($len == 0)
	{
		print('<p class="report_error">There are no people defined for this company.</p>');
		return;
	}

	print('<p>Please select a person or group to continue:</p>');
	print('<table align="center"><tr><td nowrap>');

	if ($currentuser_isadmin)
	{
		// people at the top of the tree have SupervisorID = -2
		for ($i = 0; $i < $len; ++$i)
		{
			$person = $peopledata[$i];
			if ($person['SupervisorID'] == -2)
				print_person($person, $peopledata, $type, 0);
		}

		// unassigned people have SupervisorID = -1
		for ($i = 0; $i < $len; ++$i)
		{
			$person = $peopledata[$i];
			if ($person['SupervisorID'] == -1)
				print_person($person, $peopledata, $type, 0);
		}
	}
	else
	{
		for ($i = 0; $i < $len; ++$i)
		{
			$person = $peopledata[$i];
			if ($person['PersonID'] != $currentuser_personid)
				continue;
			print_person($person, $peopledata, $type, 0);
			break;
		}
	}

	print('<br></td></tr></table>');
}

function print_person($person, $peopledata, $type, $indent)
{
	for ($i = 0; $i < $indent; ++$i)
		print('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');

	$link = false;
	if ($type == ANYBODY)
		$link = true;
	else if ($type == SALESPEOPLE && $person['IsSalesperson'] == '1')
		$link = true;
	else if ($type == SALESPEOPLE_MGR)
	{
		$len = count($peopledata);
		for ($i = 0; $i < $len; ++$i)
		{
			if ($peopledata[$i]['SupervisorID'] == $person['PersonID'] && $peopledata[$i]['IsSalesperson'] == '1')
			{
				$link = true;
				break;
			}
		}
	}
	else if ($type == MANAGEMENT && $person['Level'] > 1)
		$link = true;
	else if ($type == SALESPEOPLE_AND_MGR && $person['Level'] <= 2)
		$link = true;

	if ($link) $anchor = '<a href="javascript:use_treeid(\'' . $person['PersonID'] . '\')">';

	if ($link) print($anchor . '<img src="../images/sp_icon.gif" border=0></a>');
	else print('<img src="../images/sp_gray.gif">');

	print(' ');

	if ($link) print("$anchor<b>");
	print($person['FirstName'] . ' ' . $person['LastName']);
	if (strlen($person['GroupName'])) print(' (' . $person['GroupName'] . ')');
	if ($link) print('</b></a>');

	print('<br>');

	$len = count($peopledata);
	for ($i = 0; $i < $len; ++$i)
	{
		$subperson = $peopledata[$i];
		if ($subperson['SupervisorID'] == $person['PersonID'])
			print_person($subperson, $peopledata, $type, $indent + 1);
	}
}

function print_tree_path($id)
{
	print('<p><b>' . make_tree_path($id) . "</b></p>\n\n");
}

function make_tree_path($id)
{
	global $mpower_companyid;

	$fields = 'FirstName, LastName, GroupName, SupervisorID';
	$sql = "select $fields from people where CompanyID = '$mpower_companyid' and PersonID = '$id'";
	$result = mssql_query($sql);
	if ($result && ($row = mssql_fetch_assoc($result)))
	{
		$name = $row['FirstName'] . ' ' . $row['LastName'];
		$gname = $row['GroupName'];
		if ($gname != null && strlen($gname))
			$name = $gname;

		$sid = (int) $row['SupervisorID'];
		if (0 < $sid)
			$name = make_tree_path($sid) . ': ' . $name;
		return $name;
	}
	else return "";
}

function ensure_parameters($params)
{
	$len = count($params);
	$out = '';
	for ($i = 0; $i < $len; ++$i)
	{
		$param = explode('|', $params[$i]);
		$pname = trim($param[0]);
		if (!isset($GLOBALS[$pname]) || !strlen($GLOBALS[$pname])) return false;
	}

	return true;
}

// this could be beefed up a bit, to allow for other than string input
function print_parameter_prompt($params)
{
	$len = count($params);
	$out = '';
	for ($i = 0; $i < $len; ++$i)
	{
		$pinfo = explode('|', $params[$i]);
		$pname = trim($pinfo[0]);
		$out .= '<tr><td nowrap class="plabel">' . trim($pinfo[1]);
		$out .= '</td><td><input type="text" name="' . $pname . '"';
		if (isset($GLOBALS[$pname])) $out .= ' value="' . $GLOBALS[$pname] . '"';
		$out .= '></td></tr>';
	}

	$url = $_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING'];
	print('<p>Please enter the following information to continue:</p>');
	print('<form name="paramform" method="post" action="' . $url . '">');
	print('<table border=0 align="center">' . $out);
	print('<tr><td>&nbsp;</td><td><input class="command" type="submit" value="continue"></td></tr></table>');
}

function print_login_prompt()
{
	global $mpower_companyid, $mpower_userid;

	print('<script language="JavaScript" src="../javascript/loginscreen.js"></script>');
	print('<script language="JavaScript">');
	print("<!--\n");
	if (!isset($mpower_companyid))
		print("document.writeln('<p class=\"error\">You must log in before viewing reports.</p><br><br>');\n");
	else
	{
		if (isset($mpower_userid))
			print("document.writeln('<p class=\"error\">Invalid login information. Please check your spelling and try again.</p>');\n");
		else
			print("document.writeln('<p>Please enter your user ID and password to continue:</p>')\n");
		print("document.writeln(LoginScreen.makeHTML());\n");
	}
	print("// -->\n");
	print('</script>');
}


//Update the actuals data from the table 
// 
function updateActuals($id, $year ){
	
	// create a form table for the
	// agency  and direct 
	// get the fiscal month too 
	
	global $mpower_companyid, $manager_admin;
	$url = $_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING'] . "&UA=y";
	
	$months_list = array (  0, 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' );

	$current_year = (int) date("Y")  ;	
	$next_year = $current_year + 1 ; 
	
	$fiscal_year = getFiscalYear(getFiscalDate($mpower_companyid )) ;
	$fiscal_year = ($fiscal_year > 2000 ) ?  $fiscal_year : $fiscal_year + 2000;  
	$next_fiscal_year = $fiscal_year + 1 ; 
	
	
	$current_month = (int) date("n") ;
	$fiscal_month =  (getMonth (getFiscalDate($mpower_companyid ))+1)%12;

	if ($fiscal_month) 
	
	$sql = "SELECT P.PersonID, R.Agency, R.Direct, P.FirstName + ' '+ P.LastName as Name, R.PendingID, R.Year, R.LastUpdated
			FROM People P 
			left OUTER JOIN Pending R ON P.PersonID = R.PersonID and Year in ($current_year, $next_year)  
			WHERE P.PersonID in ($id) ORDER BY Year";
	$result = mssql_query($sql);
	$actuals = array() ; 
	
	while ($row = mssql_fetch_assoc($result)) {
		$name = $row['Name']; 
		if ($row['Year'] == NULL ) $row['Year'] = $current_year ;  
		$actuals[$row['Year']]['PersonID'] = $row['PersonID'];
		$actuals[$row['Year']]['PendingID'] = $row['PendingID'];
		$actuals['LastUpdated'] = $row['LastUpdated'] ; 
		foreach (array('Agency','Direct') as $value) {
			echo"<br>" ;
 			if ( $row[$value] ) $actuals_value = explode (";", $row[$value]);
			else $actuals_value = array(0,0,0,0,0,0,0,0,0,0,0,0 ); 
			for ( $i = 1 ; $i <= 12 ; $i++ ) {
				$actuals[$row['Year']][$value][$i] = unformat_money($actuals_value[$i-1]); 
			}
		}	
	}
	

	?>
	<?echo "<span align=right> Last updated on " . $actuals['LastUpdated'] . "</span>" ; ?>
	<br><br>

	<center>
	
	<form name="myform" id="myform" action="<?echo $url ?>" method="post">
	<?
		
	?>
	<table class="report" border=1 cellpadding=5 cellspacing=0 > 
	<tr>
		<th rowspan=2 class="ThickLeft" style="background-color:gray" > Month </th> 
		<th colspan=3 class="ThickLeft" style="background-color:gray">Fiscal Year of <?echo $fiscal_year ?>  </th>
		<th rowspan=15  style="background-color:gray">&nbsp;   </th>
		<th rowspan=2 class="ThickLeft" style="background-color:gray" > Month </th> 
		<th colspan=3 class="ThickLeft" style="background-color:gray">Fiscal Year of <?echo $next_fiscal_year ?>  </th>
	</tr>
		<tr>
 			<th class="ThickLeft" style="background-color:gray">Agency </th>
			<th class="ThickLeft" style="background-color:gray">Direct  </th>
			<th class="ThickLeft" style="background-color:gray">Total  </th>
 			<th class="ThickLeft" style="background-color:gray">Agency </th>
			<th class="ThickLeft" style="background-color:gray">Direct  </th>
			<th class="ThickLeft" style="background-color:gray">Total  </th>
		</tr>
	<?
	
	$totals = array() ; 
	$difference = -3 ; // the difference in month that cannot be edited

	$month = $fiscal_month %12 ;  // month lcv

	if ( getMonth(getFiscalDate($mpower_companyid )) < 12 ) {
		$i = $fiscal_year -1; 
		$j = $next_fiscal_year -1; 
	}else {
		$i = $fiscal_year ; 
		$j = $next_fiscal_year ; 

	}
	for ($v = 1 ; $v <= 12 ; $v++ ) {
		
		echo '<tr style="background-color: #F2FFF2; border-width:50">';
		
			if ((( ($v) % 3) == 0 ) && $v != 0 )$border = 'style="border-width:3 1 3 1"'; 
			else $border = '' ; 			
			
			$print_year = (string) $i ;
			$totals[$fiscal_year]['Agency'] = (!(isset($totals[$fiscal_year]['Agency']))) ? $actuals[$fiscal_year]['Agency'][$month] : $totals[$fiscal_year]['Agency'] + $actuals[$fiscal_year]['Agency'][$month];  
			$totals[$fiscal_year]['Direct'] = (!(isset($totals[$fiscal_year]['Direct']))) ? $actuals[$fiscal_year]['Direct'][$month] : $totals[$fiscal_year]['Direct'] + $actuals[$fiscal_year]['Direct'][$month] ;
			$totals[$fiscal_year]['Both'] = (!(isset($totals[$fiscal_year]['Both']))) ? $actuals[$fiscal_year]['Agency'][$month] + $actuals[$fiscal_year]['Direct'][$month] : $totals[$fiscal_year]['Both'] + $actuals[$fiscal_year]['Direct'][$month] + $actuals[$fiscal_year]['Agency'][$month];
			echo "<td $border>" . $months_list[$month] .  " '" . $print_year[2]. $print_year[3] . " </td>" ;
			echo "<td $border> <input type=text name=\"agency_" .$fiscal_year."[" .($month) ."]\" value=\"". fm($actuals[$fiscal_year]['Agency'][$month])." \" maxlenght=8 size=8 onChange=\"javascript:UpdateTotals('agency','$month' , '$i' )\" onFocus=\"javascript:onFocusDollar(this)\" onBlur=\"javascript:onBlurDollar(this)\" onKeyPress=\"javascript:return moneyonly(this, event)\"". checkReadOnly ($current_year, $month, $difference) .">  </td>" ;
			echo "<td $border> <input type=text name=\"direct_". $fiscal_year ."[" .($month) ."]\" value=\"". fm($actuals[$fiscal_year]['Direct'][$month])." \" maxlenght=8 size=8 onChange=\"javascript:UpdateTotals('direct','$month' , '$i' )\" onFocus=\"javascript:onFocusDollar(this)\" onBlur=\"javascript:onBlurDollar(this)\" onKeyPress=\"javascript:return moneyonly(this, event)\"". checkReadOnly ($current_year, $month, $difference) ."></td>" ;
			echo "<td $border> <input type=text name=\"total_".  $fiscal_year ."[" .($month) ."]\" value=\"". fm($actuals[$fiscal_year]['Direct'][$month] + $actuals[$fiscal_year]['Agency'][$month] )."\" maxlenght=8 size=8 readonly></td>" ;
//				if (isset($totals[$i]) && isset($totals[$i]['agency']))$totals[$i]['Agency'] += $actuals[$i]['Agency'][$month];
//				if (isset($totals[$i]) && isset($totals[$i]['direct']))$totals[$i]['Direct'] += $actuals[$i]['Direct'][$month];
//				if (isset($totals[$i]) && isset($totals[$i]['agency']) && isset($totals[$i]['agency']) )$totals[$i]['Both'] += $actuals[$i]['Direct'][$month] + $actuals[$i]['Agency'][$month]; 
			
			$print_year = (string) $j ;
			$totals[$next_fiscal_year]['Agency'] = (!(isset($totals[$next_fiscal_year]['Agency']))) ? $actuals[$next_fiscal_year]['Agency'][$month] : $totals[$next_fiscal_year]['Agency'] + $actuals[$next_fiscal_year]['Agency'][$month];
			$totals[$next_fiscal_year]['Direct'] = (!(isset($totals[$next_fiscal_year]['Direct']))) ? $actuals[$next_fiscal_year]['Direct'][$month] : $totals[$next_fiscal_year]['Direct'] + $actuals[$next_fiscal_year]['Direct'][$month];
			$totals[$next_fiscal_year]['Both'] = (!(isset($totals[$next_fiscal_year]['Both']))) ? $actuals[$next_fiscal_year]['Direct'][$month] + $actuals[$next_fiscal_year]['Agency'][$month] : $totals[$next_fiscal_year]['Both'] + $actuals[$next_fiscal_year]['Direct'][$month] + $actuals[$next_fiscal_year]['Agency'][$month];
			echo "<td $border>". $months_list[$month] .  " '" . $print_year[2]. $print_year[3] . " </td>" ;
			echo "<td $border> <input type=text name=\"agency_" .$next_fiscal_year ."[" .($month) ."]\" value=\"". fm($actuals[$next_fiscal_year]['Agency'][$month])." \" maxlenght=8 size=8 onChange=\"javascript:UpdateTotals('agency','$month' , '$i' )\" onFocus=\"javascript:onFocusDollar(this)\" onBlur=\"javascript:onBlurDollar(this)\" onKeyPress=\"javascript:return moneyonly(this, event)\"". checkReadOnly ($current_year, $month, $difference) .">  </td>" ;
			echo "<td $border> <input type=text name=\"direct_". $next_fiscal_year ."[" .($month) ."]\" value=\"". fm($actuals[$next_fiscal_year]['Direct'][$month])." \" maxlenght=8 size=8 onChange=\"javascript:UpdateTotals('direct','$month' , '$i' )\" onFocus=\"javascript:onFocusDollar(this)\" onBlur=\"javascript:onBlurDollar(this)\" onKeyPress=\"javascript:return moneyonly(this, event)\"". checkReadOnly ($current_year, $month, $difference) ."></td>" ;
			echo "<td $border> <input type=text name=\"total_".  $next_fiscal_year ."[" .($month) ."]\" value=\"". fm($actuals[$next_fiscal_year]['Direct'][$month] + $actuals[$next_fiscal_year]['Agency'][$month] )."\" maxlenght=8 size=8 readonly></td>" ;
//				if (isset($totals[$i]) && isset($totals[$i]['agency']))$totals[$i]['Agency'] += $actuals[$i]['Agency'][$month];
//				if (isset($totals[$i]) && isset($totals[$i]['direct']))$totals[$i]['Direct'] += $actuals[$i]['Direct'][$month];
//				if (isset($totals[$i]) && isset($totals[$i]['agency']) && isset($totals[$i]['agency']) )$totals[$i]['Both'] += $actuals[$i]['Direct'][$month] + $actuals[$i]['Agency'][$month];

		echo "</tr>"; 
		
		$month++ ; 

		if ($month > 12 ){
			$month = 1 ;
			$i++; 
			$j++ ; 
		}
	}
	
	echo " 	<tr style=\"background-color: #F2FFF2\"> "; 
	$border = 'style="border-width:3 1 3 1"';
	foreach ( array($fiscal_year , $next_fiscal_year ) as $i ){
		echo "<input type=\"hidden\" name=\"".$i."_id\" value=\"". $actuals[$i]['PendingID'] ."\" >"; 
		echo "<td $border> Total </td>" ;
		echo "<td $border> <input type=text name=\"total_agency_". $i ."\" value=\"". fm($totals[$i]['Agency']) ."\" maxlenght=8 size=8 readonly></td>";
		echo "<td $border> <input type=text name=\"total_direct_". $i ."\" value=\"". fm($totals[$i]['Direct'])."\" maxlenght=8 size=8 readonly></td>";
		echo "<td $border> <input type=text name=\"total_" . $i ."\" value=\"".fm($totals[$i]['Both']) ."\" maxlenght=8 size=8 readonly></td>";
		
	} 
	
	
	?>
	</tr>
	</table><br><br>
	
	<input type="submit" name="submit" id="submit" value="UPDATE"  >
	<input type="submit" name="submit" id="submit" value="SKIP"  >
	</center>
	</form>	
	<br><br>
	
	<?
}


//
// Year and date has to be string 
// compare the year and month and return the month difference 
//
function monthDiff($year, $month){
	$current_year = (int) date("y") + 2000 ;	
	$current_month = (int) date("m");
	$difference = 0 ; 
	
	if      ( ($year >= $current_year) && ($month >= $current_month) ) return ($month - $current_month) + ( ($year - $current_year) * 12 ) ;
	else if ( ($year >= $current_year) && ($month  < $current_month) ) return ((($year - $current_year) * 12  ) + $month  - $current_month); 
	else if ( ($year  < $current_year) && ($month >= $current_month) ) return (($month - $current_month) + ( ($current_year - $year) * 12 )) * (-1);
	else if ( ($year  < $current_year) && ($month  < $current_month) ) return ((($current_year - $year) * 12  ) + $month  - $current_month) * (-1) ;
	else return "INPUT ERROR" ; 

} 

//
// To change to readonly or not 
//
function checkReadOnly ($year, $month, $difference){
	if ( monthDiff($year, $month) < $difference ) return "readonly" ;
	else return ; 
} 


function print_report_top($title)
{
	global $PHP_SELF, $SELF_URL_EXTRA, $mpower_companyid;
	
	$result = mssql_query("select Name from company where CompanyID=$mpower_companyid");
	$row = mssql_fetch_assoc($result);
	$CompanyName = $row['Name'];
	
	?>
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
	<html lang="en">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title><?=$title?></title>
	<link rel="stylesheet" type="text/css" href="../css/sharedstyles.css">
	<script language="JavaScript" src="../javascript/pageformat.js,windowing.js"></script>
	<style>
	@media print
	{
		.OnlyPrintBlock
		{
			display:block;
		}
		.OnlyPrintInline
		{
			display:inline;
		}
		.OnlyScreenBlock
		{
			display:none;
		}
		.OnlyScreenInline
		{
			display:none;
		}
	}
	@media screen
	{
		.OnlyPrintBlock
		{
			display:none;
		}
		.OnlyPrintInline
		{
			display:none;
		}
		.OnlyScreenBlock
		{
			display:block;
		}
		.OnlyScreenInline
		{
			display:inline;
		}
	}
	</style>
	<script language="JavaScript">
	
	
	function use_treeid(id)
	{
		var sep = '?';
		if (window.location.href.indexOf('?') != -1) sep = '&';
		window.location.href = window.location.href + sep + 'treeid=' + id;
	}

	function do_reselect()
	{
		//var url = '<?=$PHP_SELF?><?=$SELF_URL_EXTRA?>' + '?SN=<?=$_REQUEST['SN']?>';
		//window.location.href = url;
		var url = '<?=$PHP_SELF?><?=$SELF_URL_EXTRA?>' ;
		var sep = url.indexOf('?') != -1 ? '&' : '?';
		url = url + sep + 'SN=<?=$_REQUEST['SN']?>';
		window.location.href = url;
	}
	
	</script>
	</head>
	<body leftmargin="3" rightmargin="3" topmargin="3" bottommargin="3" bgcolor="white" onload="if (!(!window.myOnLoad)) myOnLoad();">
	<div class="OnlyScreenBlock" style="padding-bottom:3px;">
		<script language="JavaScript">
			Header.setText('<?=$title?>');
			Header.addButton(ButtonStore.getButton('Reselect'));
			Header.addButton(ButtonStore.getButton('Print'));
			document.writeln(Header.makeHTML());
		</script>
	</div>
	<table width="100%" cellpadding="0" cellspacing="0">
		<thead style="display: table-header-group;">
			<tr>
				<td valign="top" height="1" style="padding-bottom:3px;">
					<table width="100%" cellpadding="0" cellspacing="0" style="font-family:Arial; font-size:12pt;">
						<tr>
							<td align="left"><img class="OnlyPrintInline" src="../images/mplogosm.gif"></td>
							<td align="right" nowrap><span class="OnlyPrintInline"><?=$CompanyName?></span></td>
						</tr>
					</table>
				</td>
			</tr>
		<tbody>
			<tr>
				<td valign="top">
					<div class="OnlyPrintBlock" style="font-family:Arial; font-size:14pt; font-weight:bold; padding-bottom:3px;">
						<center><?=$title?></center>
					</div>
				</td>
				<td valign="top">
	<!-- begin report-specific output -->
	<?php
}

function print_report_bottom()
{
	?>
	<!-- end report-specific output -->
				</td>
			</tr>
		</tbody>
	</table>
	</body>
	</html>
	<?
}

function print_report_top_orig($title)
{
	global $PHP_SELF, $SELF_URL_EXTRA;

	print('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">' . "\n");
	print('<html lang="en">' . "\n");
	print('<head>' . "\n");
	print('<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">' . "\n");
	// "M-Power" removed (for Systema)
	print("<title>$title</title>\n");
	print('<link rel="stylesheet" type="text/css" href="../css/sharedstyles.css">' . "\n");
	print('<script language="JavaScript" src="../javascript/pageformat.js,windowing.js,utils.js"></script>' . "\n");
	print('</head>' . "\n");
	print('<body leftmargin=3 rightmargin=3 topmargin=3 bottommargin=3 bgcolor="white">' . "\n");
	print('<script language="JavaScript">' . "\n");
	print('<!--' . "\n");


	print("function use_treeid(id)\n");
	print("{\n");
	print("var sep = '?';\n");
	print("if (window.location.href.indexOf('?') != -1) sep = '&';\n");
	print("window.location.href = window.location.href + sep + 'SN=".$_REQUEST['SN']."' + '&treeid=' + id;\n");
	print("}\n");
	?>
	function onFocusDollar(obj){
	var len=0;
	if (document.selection)
		len=document.selection.createRange().text.length;
	else if (obj.selectionStart || obj.selectionStart==0)
		len=obj.selectionEnd-obj.selectionStart;
	oldDollarValue=obj.value;
	obj.value=unformat_money(obj.value);
	if (len) obj.select();
	}

	function onBlurDollar(obj){
	obj.value=format_money_cents(obj.value);
	//update total right and bottom 
	
	}

	function moneyonly(myfield, e, dec){
    var key;
    var keychar;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
       return true;
    keychar = String.fromCharCode(key);
   
	// control keys
    if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
       return true;
    // numbers
    else if ((("0123456789").indexOf(keychar) > -1))
       return true;
    // decimal point jump
    else if (keychar == "."){
    //   myfield.form.elements[dec].focus();
       return true;
      }
    else
       return false;
	}
	function updateMonthTotal(type, month, year){
		 
		var x = document.getElementsByName("agency_" + year +"[" + month + "]" );
		var y = document.getElementsByName("direct_" + year +"[" + month + "]" );
		var w = document.getElementsByName("total_" + year +"[" + month + "]" );

		w[0].value = format_money_cents(parseFloat(unformat_money(x[0].value)) + parseFloat(unformat_money(y[0].value)) );
	}
	function UpdateTotals(type, month, year){
		updateMonthTotal(type, month, year) ; 
		updateYearly(type, year) ; 
		updateAll (year) ; 
	}

	function updateYearly(type, year){
		var total = 0 ; 
		for (var i = 1 ; i <=12 ; i++ ){
			
			var x = document.getElementsByName(type + "_" + year + '[' + i + ']');
			
			total = parseFloat(total) + parseFloat(unformat_money(x[0].value)) ;  
		}
		var w = document.getElementsByName('total_'+ type + '_'+ year );
		w[0].value = format_money_cents(total)  ; 
	
	}

	function updateAll(year){

		var x = document.getElementsByName("total_agency_" + year );
		var y = document.getElementsByName("total_direct_" + year );
		var w = document.getElementsByName("total_" + year );
		
		w[0].value = format_money_cents(parseFloat(unformat_money(x[0].value)) +  parseFloat(unformat_money(y[0].value)));
	
	}
	
	<?
	print("function do_reselect()\n");
	print("{\n");
	print("var url = '$PHP_SELF$SELF_URL_EXTRA';\n");
	print("var sep = url.indexOf('?') != -1 ? '&' : '?';\n");
	print("url = url + sep + 'SN=".$_REQUEST['SN']."';\n");
	print("window.location.href = url;\n");
	//print("window.location.href = '$PHP_SELF$SELF_URL_EXTRA' + '?SN=".$_REQUEST['SN']."';\n");
	print("}\n");

	print("Header.setText('$title');\n");
	print("Header.addButton(ButtonStore.getButton('Reselect'));\n");
	print("Header.addButton(ButtonStore.getButton('Print'));\n");
	print('document.writeln(Header.makeHTML());' . "\n");
	print("var ivoryBox = new IvoryBox('100%', null);\n");
	print('document.writeln(ivoryBox.makeTop());' . "\n");
	print('// -->' . "\n");

	print('</script>' . "\n");
	print('<!-- begin report-specific output -->' . "\n");
}

function print_report_bottom_orig()
{
	print('<!-- end report-specific output -->' . "\n");
	print('<script language="JavaScript">' . "\n");
	print('<!--' . "\n");
	print('document.writeln(ivoryBox.makeBottom());' . "\n");
	print('// -->' . "\n");
	print('</script>' . "\n");
	print('</body>' . "\n");
	print('</html>' . "\n");
}

function apply_symbol()
{
	global $mpower_companyid, $apply_symbol_cached;
	if (!isset($apply_symbol_cached))
	{
		$sql = "select MonetaryValue from Company where CompanyID = '$mpower_companyid'";
		$result = mssql_query($sql);
		$row = mssql_fetch_assoc($result);
		$apply_symbol_cached = $row['MonetaryValue'];
	}
	if(is_null($apply_symbol_cached))
		return true;
	return ($apply_symbol_cached == 1);
}
//Optional argument no_dec switches off decimal formatting.
//If no_dec  == -1, apply no formatting at all; just prepend $
function fm($money_value)
{
	$no_dec = 0;
	if(func_num_args() > 1)
		$no_dec = func_get_arg(1);
	switch($no_dec)
	{
		case 0:
			$fm = number_format($money_value, 2);
			break;
		case 1:
			$fm = number_format($money_value);
			break;
		case -1:
			$fm = $money_value;
	}

	if (apply_symbol())
		$fm = '$' . $fm;
	return $fm;
}

function print_date_params($personid, $sortcol, $ord)
{
	global $EarliestFromDate;
	
	$today=date("m/d/y");
	$mt_lastmonth = mktime(0, 0, 0, date("m")-1, date("d"),  date("Y"));
	$lastmonth = date("m/d/y", $mt_lastmonth);

	//Extra arguments constrain dates to and from
	$conditionDateTo="";
	$conditionDateFrom="";
	$argcount=func_num_args();
	$args = func_get_args();
	for($i=3; $i<$argcount; $i++)
	{
		$cond = explode(':', $args[$i]);
		switch($cond[0])
		{
			case "dateto":
				$conditionDateTo = $cond[1];
				break;
			case "datefr":
				$conditionDateFrom = $cond[1];
				break;
		}
	}
	//Adjust initial vals of from and to
	if(strlen($conditionDateTo)>0)
	{
		$oper = substr($conditionDateTo, 0, 1);
		$strParam=substr($conditionDateTo, 1);
		$dateParam = strtotime($strParam);
		$ts = mktime();
		switch($oper)
		{
			case "<":
				if($ts >= $dateParam)
				{
					$ts = $dateParam - (24*60*60);
				}
				break;
			//not relevant
			case "<":
				break;
		}
		$today=date("m/d/y", $ts);
		$mt_lastmonth = mktime(0, 0, 0, date("m", $ts)-1, date("d", $ts),  date("Y", $ts));
		$lastmonth = date("m/d/y", $mt_lastmonth);
	}
	
	if (isset($EarliestFromDate))
	{
		$EarliestFromDate_t = strtotime($EarliestFromDate);
		if ($mt_lastmonth < $EarliestFromDate_t)
			$lastmonth = date("m/d/y", $EarliestFromDate_t);
	}
?>
<script language="JavaScript" src="../javascript/utils.js"></script>
<form onsubmit="onSubmit1(this); return false;">
<center>
FROM: <input id="dateFrom" readonly="true" name="dateFrom" type="text" size="10" value="<?php print($lastmonth);?>" ><button type="button"  class="ButtonWithGradient"value="Change..." style="height:25px;" onclick="onClickChangeDateFrom();">Change...</button><br>
TO: <input id="dateTo" readonly="true" name="dateTo" type="text" size="10" value="<?php print($today);?>" ><button type="button"  class="ButtonWithGradient"value="Change..." style="height:25px;" onclick="onClickChangeDateTo();">Change...</button><br><br>
<input type="submit" class="ButtonWithGradient" value="Continue" style="height:25px;" ></input><br>
<input type="hidden" name="treeid" value="<?php print($personid);?>" ></input>
<input type="hidden" name="sortcol" value="<?php print($sortcol);?>" ></input>
<input type="hidden" name="ord" value="<?php print($ord);?>" ></input>
<input type="hidden" id="cond_fr" value="<?php print($conditionDateFrom);?>" ></input><br>
<input type="hidden" id="cond_to" value="<?php print($conditionDateTo);?>" ></input><br>
<input type="hidden" name="SN" id="SN" value="<?=$_REQUEST['SN']?>" ></input><br>

</center>
</form>
<script language=Javascript>

			function onSubmit1(daform)
			{	
				if (!check_dateFrom() || !check_dateTo())
					return;
				daform.submit();
			}

			function check_dateFrom()
			{
				var dateFrom = document.getElementById("dateFrom");
				if (!dateFrom)
					return true;
				var val = dateFrom.value;
				var dateTo = document.getElementById("dateTo");
				if (!dateTo)
					return true;
				var valTo = dateTo.value;
				if (val != '' && valTo != '')
				{
					val = Dates.normalize(val);
					if (!val)
					{
						alert('Invalid date format for From Date.\nMust be either MM/DD/YY or MM/DD/YYYY');
						return false;
					}
					var valDate = null;
					var valToDate = null;
					if (!Dates.makeDateObj)
					{
						valDate = new Date(val);
						valToDate = new Date(valTo);
					}
					else
					{
						valDate = Dates.makeDateObj(val);
						valToDate = Dates.makeDateObj(valTo);
					}
					if (valToDate.getTime() < valDate.getTime())
					{
						alert('The To date cannot be before the From date');
						return false;
					}
					<?if (isset($EarliestFromDate)):?>
						var EarliestFromDate = null;
						if (!Dates.makeDateObj)
							EarliestFromDate = new Date('<?=$EarliestFromDate?>');
						else
							EarliestFromDate = Dates.makeDateObj('<?=$EarliestFromDate?>');
						if (valDate.getTime() < EarliestFromDate.getTime())
							return false;
					<?endif;?>
				}
				return true;
			}

			function check_dateTo()
			{
				var dateTo = document.getElementById("dateTo");
				if (!dateTo)
					return true;
				var val = dateTo.value;
				if (val != '')
				{
					val = Dates.normalize(val);
					if (!val)
					{
						alert('Invalid date format for To Date.\nMust be either MM/DD/YY or MM/DD/YYYY');
						return false;
					}
				}
				return true;
			}



			function onEditDateFrom()
			{
				//if (!check_dateFrom())
				//	document.getElementById("dateFrom").value = '';
			}

			function onEditDateTo()
			{
				//if (!check_dateTo())
				//	document.getElementById("dateTo").value = '';

			}


			function onClickChangeDateFrom()
			{
				var cond = "#" + document.getElementById("cond_fr").value;
				var calwin = Windowing.openSizedPrompt("../shared/calendar_rpt_param.html"+cond, 320, 600);
				Windowing.dropBox.calendarTarget = document.getElementById("dateFrom");
				Windowing.dropBox.onCalendarEdited = onEditDateFrom;
			}

			function onClickChangeDateTo()
			{
				var cond = "#" + document.getElementById("cond_to").value;
				var calwin = Windowing.openSizedPrompt("../shared/calendar_rpt_param.html"+cond, 320, 600);
				Windowing.dropBox.calendarTarget = document.getElementById("dateTo");
				Windowing.dropBox.onCalendarEdited = onEditDateTo;
			}

</script>
<?php
}

?>
