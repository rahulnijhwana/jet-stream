<?php
 /**
 * @package Reports
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

/*foreach($_SESSION as $key=>$val){
	echo '<br>-->'.$key;
}*/

include('_paul_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');

$sqlstr = "select Source2Used, Source2Label from company where CompanyID = $mpower_companyid";
$result = mssql_query($sqlstr);
$temprow = mssql_fetch_assoc($result);
$Source2Used = $temprow['Source2Used'];
$Source2Label = $temprow['Source2Label'];

if (!isset($treeid))
{
	begin_report_orig($Source2Label.' Report');
	print_tree_prompt(ANYBODY);
	end_report_orig();
	exit();
}


if (!isset($dateFrom) || !isset($dateTo))
{
	begin_report_orig($Source2Label.' Report');
	?>
	<center style="font-family:Arial; font-size:12pt;">Please select the date span to be used for the inclusion of closed opportunities.</center>
	<?
	$sortcol=0;
	$ord=0;
	$tomorrow = date("m/d/Y", strtotime("+1 day"));
	print_date_params($treeid,$sortcol,$ord, "datefr:<$tomorrow", "dateto:<$tomorrow");
	if(!isset($dateFrom)) $dateFrom="N/A";
	if(!isset($dateTo)) $dateTo="N/A";
	end_report_orig();
	exit();
}

begin_report($Source2Label.' Report');

$sources = array();
$valuationmap = array();
$peoplearr = array();
$opps = array();
$summary = array();

$idList = GetSalespeopleTree($treeid);

$sourcelist = '';

$sqlstr = "select * from sources2 where CompanyID = $mpower_companyid and isnull(Deleted,0) <> 1 order by Name";
$result = mssql_query($sqlstr);
//print(mssql_num_rows($result));
while ($row = mssql_fetch_assoc($result))
{
	array_push($sources, $row);
	$temp = array();
	$temp['Name'] = $row['Name'];
	$temp['FMCount'] = 0;
	$temp['FMPercent'] = 0;
	$temp['IPCount'] = 0;
	$temp['IPPercent'] = 0;
	$temp['DPCount'] = 0;
	$temp['DPPercent'] = 0;
	$temp['CCount'] = 0;
	$temp['CPercent'] = 0;
	$temp['RMCount'] = 0;
	$temp['RMPercent'] = 0;
	$temp['TotalNumber'] = 0;
	$temp['TotalPercent'] = 0;
	$temp['ValuationSet'] = 0;
	$temp['ValuationRunningTotal'] = 0;
	$temp['ClosedValuationSet'] = 0;
	$temp['ClosedValuationRunningTotal'] = 0;
	$temp['RemovedValuationSet'] = 0;
	$temp['RemovedValuationRunningTotal'] = 0;
	$temp['DollarsTotal'] = 0;
	$temp['ClosedDollarsTotal'] = 0;
	$temp['RemovedDollarsTotal'] = 0;
	$summary[$row['Source2ID']] = $temp;
	if ($sourcelist != '')
		$sourcelist .= ',';
	$sourcelist .= $row['Source2ID'];
}

$sqlstr = "select FirstName, LastName from people where PersonID in ($idList)";
$result = mssql_query($sqlstr);
while ($row = mssql_fetch_assoc($result))
	array_push($peoplearr, $row);

$sqlstr = "select FirstName, LastName from people where PersonID = $treeid";
$result = mssql_query($sqlstr);
$reportPerson = mssql_fetch_assoc($result);

$sqlstr = "select * from valuations where CompanyID = $mpower_companyid";
$result = mssql_query($sqlstr);
while ($row = mssql_fetch_assoc($result))
	$valuationmap[$row['VLevel']] = $row;

$sqlstr = "select opportunities.*, people.FirstName, people.LastName, Sources2.Name as SourceName, Sources2.Abbr as SourceAbbr";
$sqlstr .= " from opportunities, people, Sources2";
$sqlstr .= " where opportunities.PersonID in ($idList) and opportunities.PersonID = people.PersonID";
$sqlstr .= " and (opportunities.Category not in (6,9,10) 
	or (opportunities.Category = 6 and ActualCloseDate between '$dateFrom' and '$dateTo')
	or (opportunities.Category = 9 and RemoveDate between '$dateFrom' and '$dateTo'))";
$sqlstr .= " and opportunities.Source2ID = Sources2.Source2ID and opportunities.Source2ID in ($sourcelist) order by SourceAbbr";
$result = mssql_query($sqlstr);
while ($row = mssql_fetch_assoc($result))
	array_push($opps, $row);

$sqlstr = "select opportunities.*, people.FirstName, people.LastName, SourceName = 'No Source', SourceAbbr = 'none', Source2ID = null from opportunities, people where opportunities.PersonID in ($idList) and opportunities.PersonID = people.PersonID and opportunities.Category not in (6,9,10) and (opportunities.Source2ID = -1 or opportunities.Source2ID is null or opportunities.Source2ID not in ($sourcelist))";
$result = mssql_query($sqlstr);
//print(mssql_num_rows($result));
while ($row = mssql_fetch_assoc($result))
	array_push($opps, $row);

for ($k = 0; $k < count($opps); ++$k)
{
	$row = $opps[$k];
	$sqlstr = "select Opp_Product_XRef.*, Products.Abbr from Opp_Product_XRef, Products where Opp_Product_XRef.DealID = ".$row['DealID']." and Opp_Product_XRef.ProductID = Products.ProductID";
	$result = mssql_query($sqlstr);
	$offeringstr = "";
	while ($prodrow = mssql_fetch_assoc($result))
	{
		//$offeringstr .= (($prodrow['ActQty'] != '') ? $prodrow['ActQty'] : $prodrow['Qty']).'x'.$prodrow['Abbr'];
		if ($offeringstr != "")
			$offeringstr .= ", ";
		$offeringstr .= $prodrow['Abbr'];
	}
	//print_r($offeringstr);
	$opps[$k]['OfferingStr'] = ($offeringstr == "") ? "&nbsp;" : $offeringstr;
}

$countfm = 0;
$countip = 0;
$countdp = 0;
$countactive = 0;
$countclosed = 0;
$countremoved = 0;

$totaldollars = 0;
$closedtotaldollars = 0;
$removedtotaldollars = 0;

$MasterValuationRunningTotal = 0;
$MasterValuationSet = 0;

$MasterClosedValuationRunningTotal = 0;
$MasterClosedValuationSet = 0;
$MasterRemovedValuationRunningTotal = 0;
$MasterRemovedValuationSet = 0;

for ($k = 0; $k < count($opps); ++$k)
{
	$row = $opps[$k];
	if ($row['Source2ID'] < 0 || $row['Source2ID'] == "")
	{
		if (!isset($summary["none"]))
		{
			$temp = array();
			$temp['Name'] = "None";
			$temp['FMCount'] = 0;
			$temp['FMPercent'] = 0;
			$temp['IPCount'] = 0;
			$temp['IPPercent'] = 0;
			$temp['DPCount'] = 0;
			$temp['DPPercent'] = 0;
			$temp['CCount'] = 0;
			$temp['CPercent'] = 0;
			$temp['RMCount'] = 0;
			$temp['RMPercent'] = 0;
			$temp['TotalNumber'] = 0;
			$temp['TotalPercent'] = 0;
			$temp['ValuationSet'] = 0;
			$temp['ValuationRunningTotal'] = 0;
			$temp['ClosedValuationSet'] = 0;
			$temp['ClosedValuationRunningTotal'] = 0;
			$temp['DollarsTotal'] = 0;
			$temp['ClosedDollarsTotal'] = 0;
			$temp['RemovedValuationSet'] = 0;
			$temp['RemovedValuationRunningTotal'] = 0;
			$temp['RemovedDollarsTotal'] = 0;
			$summary["none"] = $temp;
		}
		$sumrow = &$summary["none"];
	}
	else
		$sumrow = &$summary[$row['Source2ID']];
	switch ($row['Category'])
	{
		case 1:
			$sumrow['FMCount']++;
			$countactive++;
			$countfm++;
			break;
		case 2:
		case 3:
			$sumrow['IPCount']++;
			$countactive++;
			$countip++;
			break;
		case 4:
		case 5:
			$sumrow['DPCount']++;
			$countactive++;
			$countdp++;
			break;
		case 6:
			$sumrow['CCount']++;
			$countclosed++;
			break;
		case 9:
			$sumrow['RMCount']++;
			$countremoved++;
			break;
	};
	if ($row['VLevel'] > 0)
	{
		if ($row['Category'] == 6)
		{
			$sumrow['ClosedValuationRunningTotal'] += $row['VLevel'];
			$sumrow['ClosedValuationSet']++;
			$MasterClosedValuationRunningTotal += $row['VLevel'];
			$MasterClosedValuationSet++;
		}
		else if ($row['Category'] == 9)
		{
			$sumrow['RemovedValuationRunningTotal'] += $row['VLevel'];
			$sumrow['RemovedValuationSet']++;
			$MasterRemovedValuationRunningTotal += $row['VLevel'];
			$MasterRemovedValuationSet++;
		}
		else
		{
			$sumrow['ValuationRunningTotal'] += $row['VLevel'];
			$sumrow['ValuationSet']++;
			$MasterValuationRunningTotal += $row['VLevel'];
			$MasterValuationSet++;
		}
	}
	if (($row['ActualDollarAmount'] > 0) && ($row['Category'] != 9))
	{
		$sumrow['ClosedDollarsTotal'] += $row['ActualDollarAmount'];
		$closedtotaldollars += $row['ActualDollarAmount'];
	}
	else if ($row['EstimatedDollarAmount'] > 0)
	{
		if($row['Category'] == 9)
		{
			$sumrow['RemovedDollarsTotal'] += $row['EstimatedDollarAmount'];
			$removedtotaldollars += $row['EstimatedDollarAmount'];
		}
		else
		{
			$sumrow['DollarsTotal'] += $row['EstimatedDollarAmount'];
			$totaldollars += $row['EstimatedDollarAmount'];
		}
	}
}

foreach ($summary as $key => $row)
{
	$summary[$key]['FMPercent'] = ($summary[$key]['FMCount'] == 0) ? 0 : round(($summary[$key]['FMCount'] * 100) / $countfm, 1);
	$summary[$key]['IPPercent'] = ($summary[$key]['IPCount'] == 0) ? 0 : round(($summary[$key]['IPCount'] * 100) / $countip, 1);
	$summary[$key]['DPPercent'] = ($summary[$key]['DPCount'] == 0) ? 0 : round(($summary[$key]['DPCount'] * 100) / $countdp, 1);
	$summary[$key]['TotalNumber'] = $row['FMCount'] + $row['IPCount'] + $row['DPCount'];
	$summary[$key]['TotalPercent'] = ($summary[$key]['TotalNumber'] == 0) ? 0 : round(($summary[$key]['TotalNumber'] * 100) / $countactive, 1);
	$summary[$key]['ValAvg'] = ($summary[$key]['ValuationSet'] == 0) ? 0 : round($summary[$key]['ValuationRunningTotal'] / $summary[$key]['ValuationSet'], 1);
	$summary[$key]['ClosedValAvg'] = ($summary[$key]['ClosedValuationSet'] == 0) ? 0 : round($summary[$key]['ClosedValuationRunningTotal'] / $summary[$key]['ClosedValuationSet'], 1);
	$summary[$key]['RemovedValAvg'] = ($summary[$key]['RemovedValuationSet'] == 0) ? 0 : round($summary[$key]['RemovedValuationRunningTotal'] / $summary[$key]['RemovedValuationSet'], 1);
	$summary[$key]['DollarsPercent'] = ($summary[$key]['DollarsTotal'] == 0) ? 0 : round(($summary[$key]['DollarsTotal'] * 100) / $totaldollars, 1);
	$summary[$key]['TotalPercentRemoved'] = ($summary[$key]['RMCount'] == 0) ? 0 : round(($summary[$key]['RMCount'] * 100) / $countremoved, 1);
	$summary[$key]['RemovedDollarsPercent'] = ($summary[$key]['RemovedDollarsTotal'] == 0) ? 0 : round(($summary[$key]['RemovedDollarsTotal'] * 100) / $removedtotaldollars, 1);
}

$locabbr = array("", "FM", "IP", "IPS", "DP", "DPS", "C", "", "", "RM", "T");

?>

<style>

BODY
{
	font-family:Arial;
	font-size:10pt;
}

.ColumnHeader
{
	font-size:10pt;
	font-weight:bold;
	border:1px solid black;
	background-color:#DDDDDD;
}

.ColumnHeaderThickLeft
{
	font-size:10pt;
	font-weight:bold;
	border:1px solid black;
	border-left:3px solid black;
	background-color:#DDDDDD;
}

.TableCell
{
	border:1px solid black;
}

.TableCellThickLeft
{
	border:1px solid black;
	border-left:3px solid black;
}

table.sortable a.sortheader
{
	text-decoration: none;
	color: black;
}

table.sortable span.sortarrow
{
	text-decoration: none;
}

</style>
<script src="../javascript/sorttable.js,utils.js"></script>
<!-- <div style="width:100%; height:100%; overflow:auto;"> -->
<center>
<?
$grandTotals = array();
$grandTotals['FMCount'] = 0;
$grandTotals['FMPercent'] = 0;
$grandTotals['IPCount'] = 0;
$grandTotals['IPPercent'] = 0;
$grandTotals['DPCount'] = 0;
$grandTotals['DPPercent'] = 0;
$grandTotals['TotalNumber'] = 0;
$grandTotals['TotalPercent'] = 0;
$grandTotals['ValAvg'] = ($MasterValuationSet == 0) ? 0 : round($MasterValuationRunningTotal / $MasterValuationSet, 1);
$grandTotals['DollarsTotal'] = 0;
$grandTotals['DollarsPercent'] = 0;
$grandTotals['CCount'] = 0;
$grandTotals['TotalPercentClosed'] = 0;
$grandTotals['ClosedValAvg'] = ($MasterClosedValuationSet == 0) ? 0 : round($MasterClosedValuationRunningTotal / $MasterClosedValuationSet, 1);
$grandTotals['ClosedDollarsTotal'] = 0;
$grandTotals['ClosedDollarsPercent'] = 0;
$grandTotals['RMCount'] = 0;
$grandTotals['TotalPercentRemoved'] = 0;
$grandTotals['RemovedValAvg'] = ($MasterRemovedValuationSet == 0) ? 0 : round($MasterRemovedValuationRunningTotal / $MasterRemovedValuationSet, 1);
$grandTotals['RemovedDollarsTotal'] = 0;
$grandTotals['RemovedDollarsPercent'] = 0;
?>

<?

$shortdateFrom = date('m/d/y', strtotime($dateFrom));
$shortdateTo = date('m/d/y', strtotime($dateTo));

?>

<center style="font-size:12pt; font-weight:bold;"><?=$reportPerson['FirstName']?> <?=$reportPerson['LastName']?></center>
<table cellpadding="4" cellspacing="0" style="border:1px solid black; font-family:Arial; font-size:8pt; background-color:white;">
	<thead style="display: table-header-group;">
		<tr>
			<td align="center" class="ColumnHeader" nowrap rowspan="3"><?=$Source2Label?></td>
			<td align="center" class="ColumnHeaderThickLeft" nowrap colspan="11">Active</td>
			<td align="center" class="ColumnHeaderThickLeft" nowrap colspan="5" rowspan="2">
				<?=GetCatLbl("Closed")?><br>
				<?=$shortdateFrom?> - <?=$shortdateTo?>
			</td>
			<td align="center" class="ColumnHeaderThickLeft" nowrap colspan="5" rowspan="2">
				<?=GetCatLbl("Removed")?><br>
				<?=$shortdateFrom?> - <?=$shortdateTo?>
			</td>
		</tr>
		<tr>
			<td align="center" class="ColumnHeaderThickLeft" nowrap colspan="2"><?=GetCatLbl("FM")?></td>
			<td align="center" class="ColumnHeaderThickLeft" nowrap colspan="2"><?=GetCatLbl("IP")?></td>
			<td align="center" class="ColumnHeaderThickLeft" nowrap colspan="2"><?=GetCatLbl("DP")?></td>
			<td align="center" class="ColumnHeaderThickLeft" nowrap colspan="5">Total</td>
		</tr>
		<tr>
			<td align="center" class="ColumnHeaderThickLeft" nowrap>#</td>
			<td align="center" class="ColumnHeader" nowrap>%</td>
			<td align="center" class="ColumnHeaderThickLeft" nowrap>#</td>
			<td align="center" class="ColumnHeader" nowrap>%</td>
			<td align="center" class="ColumnHeaderThickLeft" nowrap>#</td>
			<td align="center" class="ColumnHeader" nowrap>%</td>
			<td align="center" class="ColumnHeaderThickLeft" nowrap>#</td>
			<td align="center" class="ColumnHeader" nowrap>%</td>
			<td align="center" class="ColumnHeader" nowrap>Val Avg</td>
			<td align="center" class="ColumnHeader" nowrap>$</td>
			<td align="center" class="ColumnHeader" nowrap>%</td>
			<td align="center" class="ColumnHeaderThickLeft" nowrap>#</td>
			<td align="center" class="ColumnHeader" nowrap>%</td>
			<td align="center" class="ColumnHeader" nowrap>Val Avg</td>
			<td align="center" class="ColumnHeader" nowrap>$</td>
			<td align="center" class="ColumnHeader" nowrap>%</td>
			<td align="center" class="ColumnHeaderThickLeft" nowrap>#</td>
			<td align="center" class="ColumnHeader" nowrap>%</td>
			<td align="center" class="ColumnHeader" nowrap>Val Avg</td>
			<td align="center" class="ColumnHeader" nowrap>$</td>
			<td align="center" class="ColumnHeader" nowrap>%</td>
		</tr>
	</thead>
	<tbody>
		<?foreach ($summary as $row):?>
			<tr>
				<td class="TableCell" align="center"><?=$row['Name']?></td>
				<td class="TableCellThickLeft" align="center" width="20"><?=$row['FMCount']?></td>
				<td class="TableCell" align="center" width="20"><?=number_format($row['FMPercent'],1)?></td>
				<td class="TableCellThickLeft" align="center" width="20"><?=$row['IPCount']?></td>
				<td class="TableCell" align="center" width="20"><?=number_format($row['IPPercent'],1)?></td>
				<td class="TableCellThickLeft" align="center" width="20"><?=$row['DPCount']?></td>
				<td class="TableCell" align="center" width="20"><?=number_format($row['DPPercent'],1)?></td>
				<td class="TableCellThickLeft" align="center" width="20" style="background-color:#DDDDFF;"><?=$row['TotalNumber']?></td>
				<td class="TableCell" align="center" style="background-color:#DDDDFF;"><?=number_format($row['TotalPercent'],1)?></td>
				<td class="TableCell" align="center"><?=number_format($row['ValAvg'],1)?></td>
				<td class="TableCell" align="right"><?=fm($row['DollarsTotal'], 1)?></td>
				<td class="TableCell" align="right"><?=number_format($row['DollarsPercent'], 1)?></td>
				<td class="TableCellThickLeft" align="center" width="20" style="background-color:#DDDDFF;"><?=$row['CCount']?></td>
				<td class="TableCell" align="center" style="background-color:#DDDDFF;"><?=number_format($row['TotalPercentClosed'],1)?></td>
				<td class="TableCell" align="center"><?=number_format($row['ClosedValAvg'],1)?></td>
				<td class="TableCell" align="right"><?=fm($row['ClosedDollarsTotal'], 1)?></td>
				<td class="TableCell" align="right"><?=number_format($row['ClosedDollarsPercent'], 1)?></td>
				
				<td class="TableCellThickLeft" align="center" width="20" style="background-color:#DDDDFF;"><?=$row['RMCount']?></td>
				<td class="TableCell" align="center" style="background-color:#DDDDFF;"><?=number_format($row['TotalPercentRemoved'],1)?></td>
				<td class="TableCell" align="center"><?=number_format($row['RemovedValAvg'],1)?></td>
				<td class="TableCell" align="right"><?=fm($row['RemovedDollarsTotal'], 1)?></td>
				<td class="TableCell" align="right"><?=number_format($row['RemovedDollarsPercent'], 1)?></td>
			</tr>
			<?
			$grandTotals['FMCount'] += $row['FMCount'];
			$grandTotals['FMPercent'] += $row['FMPercent'];
			$grandTotals['IPCount'] += $row['IPCount'];
			$grandTotals['IPPercent'] += $row['IPPercent'];
			$grandTotals['DPCount'] += $row['DPCount'];
			$grandTotals['DPPercent'] += $row['DPPercent'];
			$grandTotals['TotalNumber'] += $row['TotalNumber'];
			$grandTotals['TotalPercent'] += $row['TotalPercent'];
			//$grandTotals['ValAvg'] += $row['ValAvg'];
			$grandTotals['DollarsTotal'] += $row['DollarsTotal'];
			$grandTotals['DollarsPercent'] += $row['DollarsPercent'];
			$grandTotals['CCount'] += $row['CCount'];
			$grandTotals['TotalPercentClosed'] += $row['TotalPercentClosed'];
			//$grandTotals['ClosedValAvg'] += $row['ClosedValAvg'];
			$grandTotals['ClosedDollarsTotal'] += $row['ClosedDollarsTotal'];
			$grandTotals['ClosedDollarsPercent'] += $row['ClosedDollarsPercent'];			
			$grandTotals['RMCount'] += $row['RMCount'];
			$grandTotals['TotalPercentRemoved'] += $row['TotalPercentRemoved'];
			$grandTotals['RemovedDollarsTotal'] += $row['RemovedDollarsTotal'];
			$grandTotals['RemovedDollarsPercent'] += $row['RemovedDollarsPercent'];
			?>
		<?endforeach;?>
		<tr style="background-color:#DDDDDD; font-weight:bold;">
			<td class="TableCell" align="center">Totals</td>
			<td class="TableCellThickLeft" align="center" width="20"><?=$grandTotals['FMCount']?></td>
			<td class="TableCell" align="center" width="20"><?=($grandTotals['FMPercent'] == 0) ? 0 : 100?></td>
			<td class="TableCellThickLeft" align="center" width="20"><?=$grandTotals['IPCount']?></td>
			<td class="TableCell" align="center" width="20"><?=($grandTotals['IPPercent'] == 0) ? 0 : 100?></td>
			<td class="TableCellThickLeft" align="center" width="20"><?=$grandTotals['DPCount']?></td>
			<td class="TableCell" align="center" width="20"><?=($grandTotals['DPPercent'] == 0) ? 0 : 100?></td>
			<td class="TableCellThickLeft" align="center" width="20"><?=$grandTotals['TotalNumber']?></td>
			<td class="TableCell" align="center" width="20"><?=($grandTotals['TotalPercent'] == 0) ? 0 : 100?></td>
			<td class="TableCell" align="center"><?=number_format($grandTotals['ValAvg'],1)?></td>
			<td class="TableCell" align="right"><?=fm($grandTotals['DollarsTotal'], 1)?></td>
			<td class="TableCell" align="right"><?=number_format(100, 1)?></td>
			<td class="TableCellThickLeft" align="center" width="20"><?=$grandTotals['CCount']?></td>
			<td class="TableCell" align="center"><?=($grandTotals['TotalPercentClosed'] == 0) ? 0 : 100?></td>
			<td class="TableCell" align="center"><?=number_format($grandTotals['ClosedValAvg'],1)?></td>
			<td class="TableCell" align="right"><?=fm($grandTotals['ClosedDollarsTotal'], 1)?></td>
			<td class="TableCell" align="right"><?=number_format(100, 1)?></td>
			<td class="TableCellThickLeft" align="center" width="20"><?=$grandTotals['RMCount']?></td>
			<td class="TableCell" align="center"><?=($grandTotals['TotalPercentRemoved'] == 0) ? 0 : 100?></td>
			<td class="TableCell" align="center"><?=number_format($grandTotals['RemovedValAvg'],1)?></td>
			<td class="TableCell" align="right"><?=fm($grandTotals['RemovedDollarsTotal'], 1)?></td>
			<td class="TableCell" align="right"><?=number_format(100, 1)?></td>
		</tr>
	</tbody>
</table>
</center>
<br>
<table class="sortable" id="tableMain" width="100%" cellpadding="4" cellspacing="0" style="border:1px solid black; font-family:Arial; font-size:8pt; background-color:white;">
	<thead style="display: table-header-group;">
		<tr>
			<td align="center" class="ColumnHeader"><?=$Source2Label?></td>
			<td align="center" class="ColumnHeader" nowrap>Salesperson</td>
			<td align="center" class="ColumnHeader" nowrap>Company</td>
			<td align="center" class="ColumnHeader" nowrap>Val</td>
			<td align="center" class="ColumnHeader" nowrap>Offerings</td>
			<td align="center" class="ColumnHeader" nowrap>Value</td>
			<td align="center" class="ColumnHeader" nowrap><?=GetCatLbl("FM")?> Date</td>
			<td align="center" class="ColumnHeader" nowrap>Loc</td>
		</tr>
	</thead>
	<tbody>
		<?for ($k = 0; $k < count($opps); ++$k):?>
			<?$row = $opps[$k];?>
			<tr>
				<td class="TableCell" title="<?=$row["SourceName"]?>"><?=$row["SourceAbbr"]?></td>
				<td class="TableCell"><?=$row["LastName"]?>, <?=$row["FirstName"]?></td>
				<td class="TableCell">
					<?=$row["Company"]?>
					<?if ($row["Division"] != ''):?>
						 - <?=$row["Division"]?>
					<?endif;?>
				</td>
				<?php
					echo '<td class="TableCell" align="center" title="';
					echo (isset($valuationmap[$row["VLevel"]]["Label"])) ? isset($valuationmap[$row["VLevel"]]["Label"]) : 0;
					echo '">';
					echo ($row["VLevel"] == "" || $row["VLevel"] < 1) ? '&nbsp;' : $row["VLevel"];
					echo '</td>';
				?>
				<td class="TableCell"><?=$row["OfferingStr"]?></td>
				<td class="TableCell" align="right">
					<?
					if ($row["Category"] == 6)
						$tempvalue = $row["ActualDollarAmount"];
					else
						$tempvalue = $row["EstimatedDollarAmount"];
					?>
					<?=($tempvalue == "") ? "&nbsp;" : fm($tempvalue, 1)?>
				</td>
				<td class="TableCell" align="right"><?=($row["FirstMeeting"] == "") ? "&nbsp;" : date("m/d/y", strtotime($row["FirstMeeting"]))?></td>
				<td class="TableCell" align="center"><?=GetCatLbl($locabbr[$row["Category"]])?></td>
			</tr>
		<?endfor;?>
	</tbody>
</table>
<!-- </div> -->

<?php

end_report();

?>