<?php 
/**
* @package Reports
*/

define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();


include('_paul_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');


//include('_report_utils.php');
//include('category_labels.php');
//include('GetSalespeopleTree.php');

$report_name = GetCatLbl('First Meeting') . ' Report';

if (!isset($treeid))
{
    begin_report_orig($report_name);
    print_tree_prompt(MANAGEMENT);
    end_report_orig();
    exit();
}

begin_report($report_name);

?>
		<link rel="stylesheet" type="text/css" href="../css/report.css">
		<script src="../javascript/sorttable.js,utils.js"></script>

		</td>
	</tr>
</table>
<center>
    <br>
<?php

print_tree_path($treeid);

$strdate = date("m/d/y");

print("<br><b>Date run: $strdate</b><br><br>");

$reporting_on = array();
$people_array = GetTreeRollup($treeid, $reporting_on);

/*
echo "<pre>";
print_r($people_array);
print_r($reporting_on);
echo "</pre>";
*/

list($month, $day, $year) = split("/", $strdate);

$month_ago = mktime(0, 0, 0, $month - 1, $day, $year);
$quarter_ago = mktime(0, 0, 0, $month - 3, $day, $year);
$year_ago = mktime(0, 0, 0, $month, $day, $year - 1);

$this_month_start = mktime(0, 0, 0, $month, 1, $year);
$last_month_start = mktime(0, 0, 0, $month - 1, 1, $year);
$last_month_end = mktime(0, 0, 0, $month, 0, $year);

$quarter = 0;
$this_quarter_start = mktime(0, 0, 0, ((ceil($month / 3) - 1) * 3 + 1 - $quarter * 3), 1, $year);
$quarter = 1;
$last_quarter_start = mktime(0, 0, 0, ((ceil($month / 3) - 1) * 3 + 1 - $quarter * 3), 1, $year);
$quarter = 0;
$last_quarter_end = mktime(0, 0, 0, ((ceil($month / 3) - 1) * 3 + 1 - $quarter * 3), 0, $year);

$this_year_start = mktime(0, 0, 0, 1, 1, $year);
$last_year_start = mktime(0, 0, 0, 1, 1, $year - 1);
$last_year_end = mktime(0, 0, 0, 1, 0, $year);




$people_list = implode(", ", array_keys($people_array));

$opp_sql = "SELECT DealID, PersonID, FirstMeeting, Category, FM_took_place 
    FROM opportunities 
    WHERE PersonID IN ($people_list) 
    AND FirstMeeting IS NOT NULL 
    AND Category NOT IN (1, 10)
    AND FirstMeeting > DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()) - 1, 0)
    ORDER BY FirstMeeting ASC";

//echo $opp_sql . "<br>";

$opp_result = mssql_query($opp_sql);

$result_grid = array();
$fm_grid = array();
$removed = array();

while($opp_row = mssql_fetch_assoc($opp_result)) {
    $first_meeting = strtotime($opp_row['FirstMeeting']);

	if ($opp_row['Category'] == 9)
	{
        $removed[$opp_row['DealID']]['PersonID'] = $people_array[$opp_row['PersonID']];
        $removed[$opp_row['DealID']]['FirstMeeting'] = $first_meeting;
        $removed[$opp_row['DealID']]['FM_took_place'] = $opp_row['FM_took_place'];
    }
//		$dealid = $opp_row['DealID'];
//		$log_sql = "select LogInt 
//		    from log 
//		    where DealID = {$opp_row ['DealID']} 
//		    and TransactionID = 1 
//		    order by WhenChanged desc";
//        // echo "$log_sql<br>";
//		$log_result = mssql_query($log_sql);
//		while ($log_row = mssql_fetch_assoc($log_result)) {
//			if ($log_row['LogInt'] != 9)
//				break;
//		}
//        // echo "LogInt: " . $log_row['LogInt'] . " " . $opp_row['FM_took_place'] . "<br>";
//		if ($log_row['LogInt'] == 1 && $opp_row['FM_took_place'] != 1) {
//            echo "Not processing {$opp_row['DealID']} ({$opp_row['PersonID']})<br>";
//			continue;
//        }
//	}
    
    $person = $people_array[$opp_row['PersonID']];
    
    increment($fm_grid[$person], $first_meeting, 1);
}


$removed_vals = implode(', ' , array_keys($removed));
$log_sql = "select DealID, LogInt 
    from log 
    where DealID in ($removed_vals) 
    and TransactionID = 1 
    order by DealID, WhenChanged desc";

$log_result = mssql_query($log_sql);

//echo $log_sql;

$last_deal = -1;
$last_category = -1;
while ($log_row = mssql_fetch_assoc($log_result)) {
    if ($log_row['DealID'] != $last_deal) {
        if ($last_category == 1 && $removed[$last_deal]['FM_took_place'] != 1) {
            increment($fm_grid[$removed[$last_deal]['PersonID']], $removed[$last_deal]['FirstMeeting'], -1);
        }
        $last_category =  -1;
        $last_deal = $log_row['DealID'];
    }

    if ($log_row['LogInt'] != 9 && $last_category == -1) {
        $last_category = $log_row['LogInt'];
    }
}

/*
echo "<pre>";
print implode(", " , array_keys($removed));
echo "</pre>";
*/

?>
<table class="report">
    <tr>
        <th colspan=10>First Meeting Numbers</th>
    </tr>
<?php
ShowColumnHeaders();

$reporting_on_list = implode(", ", $reporting_on);
$people_sql = "select PersonID, FirstName, LastName from people where PersonID in ($reporting_on_list) order by LastName, FirstName";
$people_result = mssql_query($people_sql);
$column_sum = array();
$rank_grid = array();
while($people_row = mssql_fetch_assoc($people_result)) {
    echo "<tr><td class=\"jl\">{$people_row['FirstName']} {$people_row['LastName']}</td>";
    for ($x = 0; $x < 9; $x++) {
//       echo "<td>$x : $person</td>";
        $val = $fm_grid[$people_row['PersonID']][$x];

        if ($x == 0 || $x == 3 || $x == 6) {
            $class = 'class="ThickLeft jr"';
        }
        else {
            $class = 'class="jr"';
        }
        if (!isset($val)) {
            $val = 0;
        }
        echo "<td $class>" . $val . "</td>";
        $column_sum[$x] += $val;

        $rank_grid[$x][$people_row['PersonID']] = $val;
    }
    echo "</tr>";
}

echo "<tr><th class=\"jl\">Totals</th>";
for ($x = 0; $x < 9; $x++) {
    if ($x == 0 || $x == 3 || $x == 6) {
        $class = 'class="ThickLeft jr"';
    }
    else {
        $class = 'class="jr"';
    }
    if(isset($column_sum[$x])) {
        echo "<th $class>" . $column_sum[$x] . "</th>";
    }
    else {
        echo "<th $class>0</th>";
    }
}
echo "</tr>";
echo "</table>";

for ($x = 0; $x < 9; $x++) {
    arsort($rank_grid[$x]);
    $prev_total = -1;
    $prev_pos = $pos = 0;
    foreach($rank_grid[$x] as $person => $total) {
        $pos++;
        if ($total != $prev_total) {
            $rank_grid[$x][$person] = $pos;
            $prev_pos = $pos;
            $prev_total = $total;
        }
        else {
            $rank_grid[$x][$person] = $prev_pos;
        }
    }
}
mssql_data_seek($people_result, 0);

?>
<br><br>
<table class="report">
    <tr>
        <th colspan=10>First Meeting Ranks</th>
    </tr>
<?php
ShowColumnHeaders();

while($people_row = mssql_fetch_assoc($people_result)) {
    echo "<tr><td class=\"jl\">{$people_row['FirstName']} {$people_row['LastName']}</td>";
    for ($x = 0; $x < 9; $x++) {
        $val = $rank_grid[$x][$people_row['PersonID']];

        if ($x == 0 || $x == 3 || $x == 6) {
            $class = 'class="ThickLeft jr"';
        }
        else {
            $class = 'class="jr"';
        }
        if (!isset($val)) {
            $val = 0;
        }
        echo "<td $class>" . $val . "</td>";
    }
    echo "</tr>";
}
echo "</table></center>";

end_report();



function increment(&$counter, $first_meeting, $by_value) {
    global $month_ago, $quarter_ago, $year_ago;
    global $this_month_start, $last_month_start, $last_month_end;
    global $this_quarter_start, $last_quarter_start, $last_quarter_end;
    global $this_year_start, $last_year_start, $last_year_end;

    if($first_meeting >= $this_month_start) {
        $counter[0] += $by_value;
    }

    if($first_meeting >= $last_month_start && $first_meeting <= $month_ago) {
        $counter[1] += $by_value;
    }

    if($first_meeting >= $last_month_start && $first_meeting <= $last_month_end) {
        $counter[2] += $by_value;
    }

    if($first_meeting >= $this_quarter_start) {
        $counter[3] += $by_value;
    }

    if($first_meeting >= $last_quarter_start && $first_meeting <= $quarter_ago) {
        $counter[4] += $by_value;
    }

    if($first_meeting >= $last_quarter_start && $first_meeting <= $last_quarter_end) {
        $counter[5] += $by_value;
    }

    if($first_meeting >= $this_year_start) {
        $counter[6] += $by_value;
    }

    if($first_meeting >= $last_year_start && $first_meeting <= $year_ago) {
        $counter[7] += $by_value;
    }

    if($first_meeting >= $last_year_start && $first_meeting <= $last_year_end) {
        $counter[8] += $by_value;
        // $counter[8]['opps'][] = $opp_row['DealID'];
    }
}

function ShowColumnHeaders() {
?>    
    <tr>
        <th>&nbsp;</th>
        <th class="ThickLeft" style="width:45px; white-space: normal;">Current Month to Date</th>
        <th style="width:45px; white-space: normal;">Last Month to Date</th>
        <th style="width:45px; white-space: normal;">Last Month</th>
        <th class="ThickLeft" style="width:45px; white-space: normal;">Current Quarter to Date</th>
        <th style="width:45px; white-space: normal;">Last Quarter to Date</th>
        <th style="width:45px; white-space: normal;">Last Quarter</th>
        <th class="ThickLeft" style="width:45px; white-space: normal;">Year to Date</th>
        <th style="width:45px; white-space: normal;">Last Year to Date</th>
        <th style="width:45px; white-space: normal;">Last Year</th>
    </tr>
<?php
}


function GetTreeRollup($tree_id, &$primaries) {
    $result = array();
	$rollup_sql = "select PersonID from people where Deleted=0 and SupervisorID = $tree_id";
	$rollup_result = mssql_query($rollup_sql);
    while($rollup_row = mssql_fetch_assoc($rollup_result)) {
        $primaries[] = $rollup_row['PersonID'];
        $subordinants = array();
        _GetSalespeopleTree($subordinants, $rollup_row['PersonID'], FALSE);
        foreach($subordinants as $sub) {
            $result[$sub] = $rollup_row['PersonID'];
        }
    }

    return $result;
}

?>