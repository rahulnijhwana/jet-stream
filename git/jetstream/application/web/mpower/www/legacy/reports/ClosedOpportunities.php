<?php
 /**
 * @package Reports
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_report_utils.php');
include('_date_params.php');
include('TableUtils.php');
include('jim_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');

define(SHADE_MED, "MED_BLUE");
define(SHADE_LIGHT, "LIGHT_BLUE");

$cellfontsize="16px";

function array_csort($marray, $column, $sortord)
{

  $sortarr=array();
  foreach ($marray as $row) {
    $sortarr[] = strtoupper($row[$column]);
  }
  array_multisort($sortarr, $sortord, $marray);
  return $marray;
}


function GetCompanyName($id)
{
	$sql = "select * from company where CompanyID='$id'";
	$ok = mssql_query($sql);
	if(($row = mssql_fetch_assoc($ok)))
	{
		return $row['Name'];
	}
	return 'Unknown!';
}

function EnterTotals(&$arrTotals, $key, $Estimated, $Actual)
{
	for ($i=0; $i<count($arrTotals); $i++)
	{
		if($key == $arrTotals[$i][0])
		{
			$arrTotals[$i][1] += $Estimated;
			$arrTotals[$i][2] += $Actual;
			return;
		}
	}
	$tmp = array();
	$tmp[0]=$key;
	$tmp[1]=$Estimated;
	$tmp[2]=$Actual;
	array_push($arrTotals, $tmp);


}

function GetMonthQuarter($m)
{
	$m--;
	return floor($m/3) + 1;
}

function MakeTotals(&$arrTotals, $CloseDate, $Estimated, $Actual)
{
	$info = getdate(strtotime($CloseDate));
	$y = $info['year'];
	$m = $info['mon'];
	$q = GetMonthQuarter($m);
	$m--;
	EnterTotals($arrTotals, "$y", $Estimated, $Actual);
	EnterTotals($arrTotals, "$y|$q", $Estimated, $Actual);
	EnterTotals($arrTotals, "$y|X|$m", $Estimated, $Actual);
}

function GetTotals($key, &$Estimated, &$Actual)
{
global $arrTotals;
	$Estimated = 0;
	$Actual = 0;
	for ($i = 0; $i < count($arrTotals); $i++)
	{
		if($key == $arrTotals[$i][0])
		{
//			$Estimated = '$' . number_format($arrTotals[$i][1]);
//			$Actual = '$' . number_format($arrTotals[$i][2]);
			$Estimated = fm($arrTotals[$i][1]);
			$Actual = fm($arrTotals[$i][2]);

		}
	}
}


function PrintQtrRow($yr, $q)
{
$arrMonths = array('Jan','Feb','Mar','Apr','May','June','July','Aug','Sept','Oct','Nov','Dec');
$mInd = ($q-1) * 3;
	for ($i=0; $i<2; $i++)
	{
		BeginTableRowData(1);
		GetTotals("$yr|$q", $Estimated, $Actual);
		if (!$i)
		{
			PrintTableCellEx("Q$q","align='center' width='6%' rowspan='2' bgcolor='lightblue'");
			PrintTableCellEx("Est:","align='left' width='6%' bgcolor='lightblue'");
			PrintTableCellEx("$Estimated", "align='right' width='13%' bgcolor='lightblue'");
		}
		else
		{
			PrintTableCellEx("Act:","align='left' width='6%' bgcolor='lightblue'");
			PrintTableCellEx("$Actual", "align='right' width='13%' bgcolor='lightblue'");
		}
		for($j=$mInd; $j < $mInd + 3; $j++)
		{
			$key="$yr|X|$j";
			GetTotals($key, $Estimated, $Actual);
			if (!$i)
			{
				PrintTableCellEx("$arrMonths[$j]","align='center' rowspan='2' width='6%' ");
				PrintTableCellEx("Est:","align='left' width='6%' ");
				PrintTableCellEx("$Estimated", "align='right'width='13%'");
			}
			else
			{
				PrintTableCellEx("Act:","align='left' width='6%'");
				PrintTableCellEx("$Actual", "align='right' width='13%' ");
			}
		}
		EndTableRow();

	}

}

function PrintTotalsHeader($yr)
{
	GetTotals("$yr", $Estimated, $Actual);
	$str  = "<table border=0 cellspacing=0 cellpadding=3 bgcolor='ivory' width='70%'>";
	$str .= "<tr><td align='left' width='20%' border=0><b>$yr</td>";
	$str .= "<td align='right' width='10%' border=0>Estimated:</td><td align='right' width='20%' border=0>$Estimated</td>";
	$str .= "<td align='right' width='10%' border=0>Actual:</td><td align='right' width='20%' border=0>$Actual</td></tr></table>";
	print($str);
}

function PrintTotals($yr, $qStart, $qStop)
{

	global $arrTotals;
//	GetTotals("$yr", $Estimated, $Actual);
//	PrintTableSpan(12, "<b>$yr Est: $Estimated Act: $Actual");

	PrintTotalsHeader($yr);
//	for ($i=1; $i<5; $i++)
print "<table border=2 cellspacing=0 cellpadding=3 bgcolor='ivory' width='100%'>";
	for ($i=4; $i>0; $i--)
	{
		if ($i > $qStop) continue;
		if ($i < $qStart) break;
		PrintQtrRow($yr, $i);
	}
EndTable();
}

function GetDateComponent($timestamp, $key)
{
	$info = getdate($timestamp);
	return $info[$key];
}


function PrintDetail($dFrom, $dTo, $sortind, $sortord)
{
	global $arrDeals, $treeid;

	$url = $_SERVER['PHP_SELF'] . "?SN={$_REQUEST['SN']}&treeid=$treeid";
	if($dFrom!=""&&$dFrom!="N/A")
	{
		$uFrom=urlencode($dFrom);
		$uTo=urlencode($dTo);
		$url.="&dateFrom=$uFrom&dateTo=$uTo";
	}

	$arrColHeads=array(
		"<b>".GetCatLbl("Closed")." Date<br>(<A HREF=\"$url&sortcol=0&ord=0\">A </A><A HREF=\"$url&sortcol=0&ord=1\">D</A>)",
		"<b>Company<br>(<A HREF=\"$url&sortcol=1&ord=0\">A </A><A HREF=\"$url&sortcol=1&ord=1\">D </A>)",
		"<b>Sales Name<br>(<A HREF=\"$url&sortcol=2&ord=0\">A </A><A HREF=\"$url&sortcol=2&ord=1\">D</A>)",
		"<b>Offering<br>(<A HREF=\"$url&sortcol=3&ord=0\">A </A><A HREF=\"$url&sortcol=3&ord=1\">D</A>)",
		"<b>Source<br>(<A HREF=\"$url&sortcol=4&ord=0\">A </A><A HREF=\"$url&sortcol=4&ord=1\">D</A>)",
		"<b>Estimated<br>(<A HREF=\"$url&sortcol=5&ord=0\">A </A><A HREF=\"$url&sortcol=5&ord=1\">D</A>)",
		"<b>Actual<br>(<A HREF=\"$url&sortcol=6&ord=0\">A </A><A HREF=\"$url&sortcol=6&ord=1\">D</A>)",
	);

//	BeginTable();
print("<table border=2 cellspacing=0 cellpadding=3 bgcolor='ivory' width='100%'>");

	BeginTableRowData(2);
	PrintTableSpan(8, "<br><b>Detail");
	EndTableRow();

	PrintColHeads($arrColHeads, 1, 0, count($arrColHeads),FALSE);


	$arrFieldNames= array('ActualCloseDate','Company','SalesName','ProductName','Abbr','EstimatedDollarAmount','ActualDollarAmount');

	$arrDeals=array_csort($arrDeals, $arrFieldNames[$sortind], $sortord);


for ($i=0; $i < count($arrDeals); $i++)
{
		$d = date("m/d/y", $arrDeals[$i]['ActualCloseDate']);
		$DealID = $arrDeals[$i]['DealID'];
		BeginTableRow();
		PrintTableCell($d);
		$tempcompany = $arrDeals[$i]['Company'];
		if ($arrDeals[$i]['Division'] != '')
			$tempcompany .= '&nbsp;-&nbsp;'.$arrDeals[$i]['Division'];
		PrintTableCell($tempcompany);
		PrintTableCell($arrDeals[$i]['SalesName']);

//		$OppProducts = GetOppProducts($DealID);
		$OppProducts = $arrDeals[$i]['ProductName'];

		if(strlen($OppProducts) > 0) PrintTableCell($OppProducts);
		else PrintTableCell($arrDeals[$i]['ProductName']);

		PrintTableCell($arrDeals[$i]['Abbr']);

//		PrintTableCellEx('$' . number_format($arrDeals[$i]['EstimatedDollarAmount']), "align='right'");
		PrintTableCellEx(fm($arrDeals[$i]['EstimatedDollarAmount']), "align='right'");

//		PrintTableCellEx('$' . number_format($arrDeals[$i]['ActualDollarAmount']), "align='right'");
		PrintTableCellEx(fm($arrDeals[$i]['ActualDollarAmount']), "align='right'");

		EndTableRow();
}
EndTable();



}


function PrintDetailColumnHeaders()
{
	BeginTableRowData(1);
	PrintTableCell("<b>".GetCatLbl("Closed")." Date");
	PrintTableCell("<b>Company");
	PrintTableCell("<b>Sales Name");
	PrintTableCell("<b>Offering");
	PrintTableCell("<b>Source");
	PrintTableCell("<b>Estimated");
	PrintTableCell("<b>Actual");
	EndTableRow();
}




/********************************************/
begin_report(GetCatLbl('Closed'). ' Opportunities Report');


if (!isset($treeid))
{
	print_tree_prompt(ANYBODY);
	end_report();
	exit();
}


if (!isset($dateFrom)||!isset($dateTo))
	{
	$sortcol=0;
	$ord=0;
	$tomorrow = date("m/d/Y", strtotime("+1 day"));
	print_date_params($treeid,$sortcol,$ord, "datefr:<$tomorrow", "dateto:<$tomorrow");
	if(!isset($dateFrom)) $dateFrom="N/A";
	if(!isset($dateTo)) $dateTo="N/A";
	end_report();
	exit();
	}

if(!isset($sortcol)) $sortcol=0;

if($ord==0) $sortord=SORT_ASC;
else $sortord=SORT_DESC;




$total=0;

$idList = GetSalespeopleTree($treeid);

$sql = "SELECT opportunities.Company, opportunities.Division, opportunities.DealID, opportunities.EstimatedDollarAmount, opportunities.ActualDollarAmount,";
$sql .= " Sources.Abbr,";
//$sql .= " opportunities.ActualCloseDate, products.Name AS ProductName, people.FirstName, people.LastName";
$sql .= " opportunities.ActualCloseDate, people.FirstName, people.LastName";
$sql .= " FROM opportunities INNER JOIN people ON opportunities.PersonID = people.PersonID";
$sql .= " LEFT OUTER JOIN Sources ON opportunities.SourceID = Sources.SourceID";
//$sql .= " INNER JOIN products ON opportunities.ProductID = products.ProductID";
$sql .= " WHERE opportunities.Category = 6 AND people.PersonID IN ($idList)";
$sql .= " AND opportunities.ActualCloseDate >= '$dateFrom' AND opportunities.ActualCloseDate <= '$dateTo'";
$sql .= " ORDER BY opportunities.ActualCloseDate DESC";

$ok = mssql_query($sql);
$arrDeals = array();
$arrTotals = array();
while(($row=mssql_fetch_assoc($ok)))
{
	$tmp = array();
	$tmp['ActualCloseDate'] = strtotime($row['ActualCloseDate']);
	$tmp['Company'] = $row['Company'];
	$tmp['Division'] = $row['Division'];
	$tmp['DealID'] = $row['DealID'];
//	$tmp['ProductName'] = $row['ProductName'];
	$tmp['ProductName'] = GetOppProducts($row['DealID']);
	$tmp['SalesName'] = $row['LastName'] . ', ' . $row['FirstName'];
	$tmp['EstimatedDollarAmount'] = $row['EstimatedDollarAmount'];
	$tmp['ActualDollarAmount'] = $row['ActualDollarAmount'];
    $tmp['Abbr'] = $row['Abbr'];
	MakeTotals($arrTotals, $row['ActualCloseDate'], $tmp['EstimatedDollarAmount'], $tmp['ActualDollarAmount']);
	array_push($arrDeals, $tmp);
}




/****************Print**********************/
/*
print("<meta http-equiv=\"Refresh\" content=\"10; URL=ClosedOpportunities_HTML.php?treeid=$treeid&dateFrom=$dateFrom&dateTo=$dateTo\">");


print("<br><br><b>     Please wait while your report is being prepared.<br><br>");

end_report();
*/




print_tree_path($treeid);

$strdate = date("M d Y", time());


$title=GetCatLbl("Closed") . " Opportunities Report";

$salestree=make_tree_path($treeid);

BeginTable();

//print("<table width='100%'>");
print("<table border=2 cellspacing=0 cellpadding=3 bgcolor='ivory' width='100%'>");

//print("<tr ><td align='center'><b>$title</td></tr>");

$companyname = GetCompanyName($mpower_companyid);
BeginTableRow();
PrintTableSpan(4, "<b>$companyname");
EndTableRow();

$shortdateFrom = date('m/d/y', strtotime($dateFrom));
$shortdateTo = date('m/d/y', strtotime($dateTo));

$daterange = "Date Range: $shortdateFrom to $shortdateTo";

BeginTableRow();
PrintTableSpan(4, "<b>$daterange<br><br>");
EndTableRow();
EndTable();
//print('<table width="100%">');
print("<table border=2 cellspacing=0 cellpadding=3 bgcolor='ivory' width='100%'>");
BeginTableRowData(2);
PrintTableSpan(12, "<b>Totals");
EndTableRow();

$endyear = GetDateComponent(strtotime($dateTo),'year');
$startyear = GetDateComponent(strtotime($dateFrom),'year');
$qStop=GetMonthQuarter(GetDateComponent(strtotime($dateTo), 'mon'));
$qStart=GetMonthQuarter(GetDateComponent(strtotime($dateFrom), 'mon'));

$year=$endyear;
EndTable();
while($year >= $startyear)
{
	$q1 = 0; $q2 = 5;
	if ($year==$startyear) $q1 = $qStart;
	if ($year==$endyear) $q2 = $qStop;
	PrintTotals($year--, $q1, $q2);
}

//EndTable();

if(!isset($sortcol)) $sortcol=0;

if($ord==0) $sortord=SORT_ASC;
else $sortord=SORT_DESC;


//print detail
PrintDetail($dateFrom, $dateTo, $sortcol, $sortord);

end_report();

close_db();

?>
