<?php
 /**
 * @package Reports
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_report_utils.php');
include('trends_pdf.php');
include('ci_pdf_utils.php');

define(SHADE_MED, "MED_BLUE");
define(SHADE_LIGHT, "LIGHT_BLUE");



$report = CreateReport();
$report['left'] = 72/4;
$report['right'] = 8.25 * 72;

$title = "Trend Detail Report";

$p = PDF_new();
PDF_set_parameter($p, "license", PDFLIB_LICENSE_KEY);
PDF_open_file($p, "");
StartPDFReport($report, $p, "AAAA", "BBBB", "CCCCC");
//SetLogo($report, "report_logo.jpg", 10, 40, 72, 0);
SetLogo($report, "report_logo.jpg", 14, 50, 110, 0);
$salestree=make_tree_path($treeid);
$report['footertext']=$title;

BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, 0.1);
AddFreeFormText($report, $title, -1, "center", 1.5, 5.5);

BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0.25);
AddFreeFormText($report, $salestree, -1, "center", 1.5, 5.5);

$left = 1;
$width1 = 1;
$width2 = 3;


//PrintLocationKey($report, 1, 2, 2,$treeid);

TrendDetailReport($report, $treeid, $mpower_companyid);

PrintPDFReport($report, $p);
PDF_delete($p);

/****************************************************************************************/

function IsSalesmanInTrend($TrendId, $SalesId, &$StartDate, &$NumDays)
{
	$StartDate = IsTrendCurrent($SalesId, $TrendId);
	if($StartDate)
	{
		$Today = time();
		$elapsed = $Today - $StartDate;
		$NumDays = (int)($elapsed / (60 * 60 * 24));
//		$StartDate = date("M d Y", $StartDate);
		$StartDate = date("m/d/y", $StartDate);
		return true;
	}
	return false;
}

function PrintSalesmanTrendCell(&$report, $TrendId, $SalesId)
{
	if(IsSalesmanInTrend($TrendId, $SalesId, $StartDate, $NumDays))
	{
		AddColumnText($report, $StartDate . "\r(" . $NumDays . " days)", "", 0, 0);
	}
	else
	{
		AddColumnText($report, " ", "", 0, 0);
	}
}

function PrintTrendRepRow(&$report, $TrendName, $TrendId, &$SalesArray, $begin, $end, $shade = SHADE_LIGHT)
{
	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0.1);
	if ($shade != '')
		SetRowColorStandard($report, -1, $shade);
	AddColumnText($report, $TrendName, "", 0, 0);

	for($i=$begin; $i<$end; $i++)
	{
 		PrintSalesmanTrendCell($report, $TrendId, $SalesArray[$i]);
	}

}

function TrendDetailReport(&$report, $SupervisorID, $companyid)
{

	$SalesNames = Array();
	$SalesIds = Array();

	$TrendNames = Array();
	$TrendIds = Array();

	GetSalespeople($SalesNames, $SalesIds, -1, $SupervisorID);
	GetTrends($TrendNames, $TrendIds, $companyid);

	$columns = 5;
	$begin = 0;
	$end = $columns;
	$salescount = count($SalesNames);

	while(1)
	{
		if($end > $salescount) $end = $salescount;
		if($end <= $begin) break;

		BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0);
		SetRowColorStandard($report, -1, SHADE_MED);
		AddColumn($report, "TRENDS", "left", 1.2, 0);
		SetColColorStandard($report, -1, -1, SHADE_MED);

		for($i=$begin; $i<$end; $i++)
		{
			AddColumn($report, $SalesNames[$i], "center", 1, 0.2);
		}

		$ind = 0;
		$TrendCount = count($TrendNames);
		$k = 0;
		while($ind < $TrendCount)
		{
			$ID = $TrendIds[$ind] + 100;
			$Name = $TrendNames[$ind];
			if (($k % 2) == 0)
				PrintTrendRepRow($report, $Name, $ID, $SalesIds, $begin, $end, SHADE_LIGHT);
			else
				PrintTrendRepRow($report, $Name, $ID, $SalesIds, $begin, $end, '');
			$ind = $ind + 1;
			++$k;
		}

		$begin += $columns;
		$end += $columns;
	}

}

close_db();

?>
