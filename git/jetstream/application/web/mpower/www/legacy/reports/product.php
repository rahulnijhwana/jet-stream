<?php
 /**
 * @package Reports
 */

define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_paul_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');

if (!isset($treeid))
{
	begin_report_orig('Product Report');
	print_tree_prompt(ANYBODY);
	end_report_orig();
	exit();
}

if (!isset($dateFrom) || !isset($dateTo))
{
	begin_report_orig('Product Report');
	?>
	<center style="font-family:Arial; font-size:12pt;">Please select the date span to be used for the inclusion of closed opportunities.</center>
	<?
	$sortcol=0;
	$ord=0;
	$tomorrow = date("m/d/Y", strtotime("+1 day"));
	print_date_params($treeid,$sortcol,$ord, "datefr:<$tomorrow", "dateto:<$tomorrow");
	if(!isset($dateFrom)) $dateFrom="N/A";
	if(!isset($dateTo)) $dateTo="N/A";
	end_report_orig();
	exit();
}

begin_report('Product Report');

$idList = GetSalespeopleTree($treeid);

$productSql = "select Products.ProductID, Products.Name, Products.Abbr, Opportunities.Category, count(Opp_Product_XRef.ProductID) as transCount, count(Opportunities.DealID) as TotalOpps, sum(round(val * qty, 0)) as TotalVal 
	from Products 
	left outer join Opp_Product_XRef on Products.ProductID = Opp_Product_XRef.ProductID 
	left join Opportunities on Opp_Product_XRef.DealID = Opportunities.DealID 
	where Opportunities.Category is not null and
	opportunities.PersonID in ($idList) and 
	products.companyid = $mpower_companyid and (opportunities.category not in (1,6,9,10) or ActualCloseDate between '$dateFrom' and '$dateTo') 
	group by products.productid, Products.Name, Products.Abbr, Opportunities.Category 
	order by Products.Name, opportunities.category";

// echo $productSql . "<br>";

$productResult = mssql_query($productSql);

while ($row = mssql_fetch_assoc($productResult))
{
    $name_list[$row['ProductID']]['name'] = $row['Name'];
    $name_list[$row['ProductID']]['abbr'] = $row['Abbr'];
    $quantity[$row['Category']][$row['ProductID']] += $row['TotalOpps'];
    $value[$row['Category']][$row['ProductID']] += $row['TotalVal'];
}

for($phase = 2; $phase <= 6; $phase++) {
    $quantity[$phase]['total'] = (array_key_exists($phase, $quantity)) ? array_sum($quantity[$phase]) : 0;
    $value[$phase]['total'] = (array_key_exists($phase, $value)) ? array_sum($value[$phase]) : 0;
}

$activeTotalQty = $quantity[2]['total'] + $quantity[4]['total'];
$activeTotalVal = $value[2]['total'] + $value[4]['total'];
$stalledTotalQty = $quantity[3]['total'] + $quantity[5]['total'];
$stalledTotalVal = $value[3]['total'] + $value[5]['total'];

$shortdateFrom = date('m/d/y', strtotime($dateFrom));
$shortdateTo = date('m/d/y', strtotime($dateTo));
$sqlstr = "select FirstName, LastName from people where PersonID = $treeid";
$result = mssql_query($sqlstr);
$reportPerson = mssql_fetch_assoc($result);
?>

		<link rel="stylesheet" type="text/css" href="../css/report.css">
		<script src="../javascript/utils.js,sorttable2.js,filter2.js"></script>
		</td>
	</tr>
</table>
<br>
<center style="font-size:12pt; font-weight:bold;"><?=$reportPerson['FirstName']?> <?=$reportPerson['LastName']?></center>
<br>
		<center>
            <form>
			<table class="report">
				<colgroup>
					<col span=2 />
					<col span=4 style="background-color: #F2FFF2"/>
					<col span=4 style="background-color: #DBFFDB"/>
					<col span=4 style="background-color: #FFEAEA" />
					<col span=4 style="background-color: #FFBFBF" />
					<col span=4 style="background-color: #DDDDFF" />
				</colgroup>
				<tr>
                    <th rowspan=3>Filter</th>
					<th rowspan=3>Product</th>
					<th colspan=8 class="ThickLeft">Active</th>
					<th colspan=8 class="ThickLeft">Stalled</th>
					<th colspan=4 rowspan=2 class="ThickLeft">
						<?=GetCatLbl("Closed")?><br>
						<?=$shortdateFrom?> - <?=$shortdateTo?>
					</th>
				</tr>
				<tr>
					<th colspan=2 class="ThickLeft"><?php echo GetCatLbl("IP") ?></th>
					<th colspan=2 class="ThickLeft"><?php echo GetCatLbl("DP") ?></th>
					<th colspan=4 class="ThickLeft">Total</th>
					<th colspan=2 class="ThickLeft"><?php echo GetCatLbl("IPS") ?></th>
					<th colspan=2 class="ThickLeft"><?php echo GetCatLbl("DPS") ?></th>
					<th colspan=4 class="ThickLeft">Total</th>
				</tr>
				<tr>
					<th class="ThickLeft">#</th>
					<th>%</th>
					<th class="ThickLeft">#</th>
					<th>%</th>
					<th class="ThickLeft">#</th>
					<th>%</th>
					<th>$</th>
					<th>%</th>

					<th class="ThickLeft">#</th>
					<th>%</th>
					<th class="ThickLeft">#</th>
					<th>%</th>
					<th class="ThickLeft">#</th>
					<th>%</th>
					<th>$</th>
					<th>%</th>

					<th class="ThickLeft">#</th>
					<th>%</th>
					<th>$</th>
					<th>%</th>
				</tr>


				<?
                foreach ($name_list as $id => $name) {
                    $active_total_qty = $quantity[2][$id] + $quantity[4][$id];
                    $active_total_val = $value[2][$id] + $value[4][$id];
                    $stalled_total_qty = $quantity[3][$id] + $quantity[5][$id];
                    $stalled_total_val = $value[3][$id] + $value[5][$id];

					echo '<tr>';
                    echo '<td><input type="checkbox" name="filter" value="' . $name['abbr'] . '" checked onclick="FilterByCheckbox(this)"></td>';
					echo '<td class="jl">' . $name['name'] . '</td>';
					echo '<td class="ThickLeft jr">' . NullToZero($quantity[2][$id]) . '</td>';
					echo '<td class="jr">' . GetPercent($quantity[2][$id], $quantity[2]['total']) . '</td>';
					echo '<td class="ThickLeft jr">' . NullToZero($quantity[4][$id]) . '</td>';
					echo '<td class="jr">' . GetPercent($quantity[4][$id], $quantity[4]['total']) . '</td>';
					echo '<td class="ThickLeft jr">' . $active_total_qty . '</td>';
					echo '<td class="jr">' . GetPercent($active_total_qty, $activeTotalQty) . '</td>';
					echo '<td class="jr">' . CurrencyFormat($active_total_val, 0, true, true) . '</td>';
					echo '<td class="jr">' . GetPercent($active_total_val, $activeTotalVal) . '</td>';

					echo '<td class="ThickLeft jr">' . NullToZero($quantity[3][$id]) . '</td>';
					echo '<td class="jr">' . GetPercent($quantity[3][$id], $quantity[3]['total']) . '</td>';
					echo '<td class="ThickLeft jr">' . NullToZero($quantity[5][$id]) . '</td>';
					echo '<td class="jr">' . GetPercent($quantity[5][$id], $quantity[5]['total']) . '</td>';
					echo '<td class="ThickLeft jr">' . $stalled_total_qty . '</td>';
					echo '<td class="jr">' . GetPercent($stalled_total_qty, $stalledTotalQty) . '</td>';
					echo '<td class="jr">' . CurrencyFormat($stalled_total_val, 0, true, true) . '</td>';
					echo '<td class="jr">' . GetPercent($stalled_total_val, $stalledTotalVal) . '</td>';

					echo '<td class="ThickLeft jr">' . $quantity[6][$id] . '</td>';
					echo '<td class="jr">' . GetPercent($quantity[6][$id], $quantity[6]['total']) . '</td>';
					echo '<td class="jr">' . CurrencyFormat($value[6][$id] . '</td>', 0, true, true);
					echo '<td class="jr">' . GetPercent($value[6][$id], $value[6]['total']) . '</td>';
					echo '</tr>';
				}
				?>
				<tr>
					<th>&nbsp;</th>
					<th>Totals</th>
					<th class="ThickLeft jr"><?=$quantity[2]['total'] ?></th>
					<th class="jr">100.0</th>
					<th class="ThickLeft jr"><?=$quantity[4]['total'] ?></th>
					<th class="jr">100.0</th>
					<th class="ThickLeft jr"><?=$activeTotalQty ?></th>
					<th class="jr">100.0</th>
					<th class="jr"><?=CurrencyFormat($activeTotalVal, 0, true, true) ?></th>
					<th class="jr">100.0</th>

					<th class="ThickLeft jr"><?=$quantity[3]['total'] ?></th>
					<th class="jr">100.0</th>
					<th class="ThickLeft jr"><?=$quantity[5]['total'] ?></th>
					<th class="jr">100.0</th>
					<th class="ThickLeft jr"><?=$stalledTotalQty ?></th>
					<th class="jr">100.0</th>
					<th class="jr"><?=CurrencyFormat($stalledTotalVal, 0, true, true) ?></th>
					<th class="jr">100.0</th>

					<th class="ThickLeft jr"><?=$quantity[6]['total'] ?></th>
					<th class="jr">100.0</th>
					<th class="jr"><?=CurrencyFormat($value[6]['total'], 0, true, true) ?></th>
					<th class="jr">100.0</th>
				</tr>
			</table>
            </form>

			<br>

			<?

			/* $companyID = 33;

			$companySql = "select * from Company where CompanyID = $companyID";
			$companyResult = mssql_query($companySql);

			$company = mssql_fetch_assoc($companyResult);
			*/

			$oppSql = "select opportunities.DealID
                	, opportunities.Division
                	, opportunities.Company
                	, opportunities.Category
                	, opportunities.ActualDollarAmount
                	, opportunities.EstimatedDollarAmount
                	, opportunities.FirstMeeting
                	, people.FirstName
                	, people.LastName
                	, Sources2.Name as SourceName
                	, Sources2.Abbr as SourceAbbr
                	, Valuations.VLevel as ValLevel
                	, Valuations.Label as ValLabel 
                from opportunities 
                	left join people on opportunities.PersonID = people.PersonID 
                	left outer join Sources2 on opportunities.Source2ID = Sources2.Source2ID 
                	left outer join Valuations 
                		on Opportunities.CompanyID = Valuations.CompanyID 
                		and Opportunities.VLevel = Valuations.VLevel 
                where opportunities.PersonID in ($idList) 
                	and (opportunities.Category not in (1,6,9,10) 
                		or (opportunities.Category = 6 and ActualCloseDate between '$dateFrom' and '$dateTo')) 
                order by SourceAbbr";

				
			// echo $oppSql . '<br>';

			$oppResult = mssql_query($oppSql);

			?>
			<table class="sortable" id="tableMain">
				<thead>
					<tr>
						<th><?=$source2label?></th>
						<th>Salesperson</th>
						<th>Company</th>
						<th>Val</th>
						<th>Offerings</th>
						<th>Value</th>
						<th><?=GetCatLbl("FM")?> Date</th>
						<th>Loc</th>
					</tr>
				</thead>
				<tbody>
					<?
					$locabbr = array("", "FM", "IP", "IPS", "DP", "DPS", "C", "", "", "RM", "T");
					while ($row = mssql_fetch_assoc($oppResult)) {

						$prodSQL = "select Products.Abbr, Opp_Product_XRef.val, Opp_Product_XRef.qty from Opp_Product_XRef, Products where Opp_Product_XRef.DealID = ".$row['DealID']." and Opp_Product_XRef.ProductID = Products.ProductID order by Products.Name";
						$prodResult = mssql_query($prodSQL);
						$offerings = "";
                        $value = 0;
  						while ($prodRow = mssql_fetch_assoc($prodResult)) {
							$offerings .= $prodRow['Abbr'] . ' ';
                            $value += $prodRow['qty'] * $prodRow['val'];
						}
						$val = ($row["ValLevel"] == "" || $row["ValLevel"] < 1) ? "&nbsp;" : $row["ValLevel"];
						$companyText = ($row["Division"] != '') ? $row["Company"] . ' - ' . $row["Division"] : $row["Company"];

                        if ($row["Category"] == 6) {
                           $value = $row["ActualDollarAmount"];
                        }

						// $value = ($row["Category"] == 6) ? $row["ActualDollarAmount"] : $row["EstimatedDollarAmount"];

						if (mssql_num_rows($prodResult) > 0) {
							echo '<tr>';
							echo '<td title="' . $row["SourceName"] . '">' . $row["SourceAbbr"] . '</td>';
							echo '<td class="jl">' . $row["LastName"] . ', ' . $row["FirstName"] . '</td>';
							echo '<td class="jl">' . $companyText . '</td>';
							echo '<td title="' . $row["ValLabel"] . '">' . $val . '</td>';
							echo '<td>' . $offerings . '</td>';
							echo '<td class="jr">' . CurrencyFormat($value, 0, true, true) . '</td>';
							echo '<td class="jr">' . date("m/d/y", strtotime($row["FirstMeeting"])) . '</td>';
							echo '<td>' . GetCatLbl($locabbr[$row["Category"]]) . '</td>';
							echo '</tr>';
						}
					}
					?>
				</tbody>
                <tfoot>
                    <tr>
                        <td colspan=5 class="jr">Active Total</td>
                        <td class="jr"></td>
                        <td colspan=2></td>
                    </tr>
                    <tr>
                        <td colspan=5 class="jr">Stalled Total</td>
                        <td class="jr"></td>
                        <td colspan=2></td>
                    </tr>
                    <tr>
                        <td colspan=5 class="jr">Closed Total</td>
                        <td class="jr"></td>
                        <td colspan=2></td>
                    </tr>
                    <tr>
                        <td colspan=5 class="jr">Grand Total</td>
                        <td class="jr"></td>
                        <td colspan=2></td>
                    </tr>
                </tfoot>
			</table>
		</center>
        <script>
        
            function GetPhaseType(phase_name) {
            	
				var ip_lbl = "<?php echo GetCatLbl('IP') ?>".replace(/^\s+|\s+$/g,"");
				var dp_lbl = "<?php echo GetCatLbl('DP') ?>".replace(/^\s+|\s+$/g,"");
				var ips_lbl = "<?php echo GetCatLbl('IPS') ?>".replace(/^\s+|\s+$/g,"");
				var dps_lbl = "<?php echo GetCatLbl('DPS') ?>".replace(/^\s+|\s+$/g,"");
				var c_lbl = "<?php echo GetCatLbl('C') ?>".replace(/^\s+|\s+$/g,"");
				phase_name = phase_name.replace(/^\s+|\s+$/g,"");
				
				switch (phase_name)
                {
                    case ip_lbl:
                    case dp_lbl:
                        return 'active';
                    case ips_lbl:
                    case dps_lbl:
                        return 'stalled';
                    case c_lbl:
                        return 'closed';
                }
            }

           SumTable('tableMain');
        </script>

	</body>
</html>

<?php

function GetPercent($numerator, $denominator) {
    $output = ($denominator != 0) ? CurrencyFormat((round($numerator/$denominator * 1000)/10), 1) : '-';    
    return $output;
}

function NullToZero($value) {
    $output = (is_null($value)) ? 0 : $value;
    return $output;
}

?>

