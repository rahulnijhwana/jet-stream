<?php
 /**
 * @package Reports
 */

define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_paul_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');

if (!isset($treeid))
{
	begin_report_orig('BreakDown Report');
	print_tree_prompt(ANYBODY);
	end_report_orig();
	exit();
}

begin_report('BreakDown Report');

$idList = GetSalespeopleTree($treeid);

$productSql = "select Products.ProductID, Products.Name, Products.Abbr, Opportunities.Category, 
	Opp_Product_XRef.Month_1, Opp_Product_XRef.Month_2, Opp_Product_XRef.Month_3,
	Opp_Product_XRef.Month_4, Opp_Product_XRef.Month_5, Opp_Product_XRef.Month_6,
	Opp_Product_XRef.Month_7, Opp_Product_XRef.Month_8, Opp_Product_XRef.Month_9,
	Opp_Product_XRef.Month_10, Opp_Product_XRef.Month_11, Opp_Product_XRef.Month_12, Opp_Product_XRef.Val 
  from [test].[dbo].Opportunities  
	left outer join [test].[dbo].Opp_Product_XRef on Opportunities.DealID = Opp_Product_XRef.DealID 
	left join [test].[dbo].products on Products.ProductID = Opp_Product_XRef.ProductID
  where opportunities.PersonID in ($idList) 
	and products.companyid = $mpower_companyid
	and opportunities.category in (2,3,4,5)  
  order by Products.ProductID, Opportunities.Category ; ";
	
// echo $productSql . "<br>";

$productResult = mssql_query($productSql);


while ($row = mssql_fetch_assoc($productResult))
{
    $name_list[$row['ProductID']]['name'] = $row['Name'];
    $name_list[$row['ProductID']]['abbr'] = $row['Abbr'];
    $name_list[$row['ProductID']]['Month_1'] = $row['Month_1'];
    $name_list[$row['ProductID']]['Month_2'] = $row['Month_2'];
    $name_list[$row['ProductID']]['Month_3'] = $row['Month_3'];
    $name_list[$row['ProductID']]['Month_4'] = $row['Month_4'];
    $name_list[$row['ProductID']]['Month_5'] = $row['Month_5'];
    $name_list[$row['ProductID']]['Month_6'] = $row['Month_6'];
    $name_list[$row['ProductID']]['Month_7'] = $row['Month_7'];
    $name_list[$row['ProductID']]['Month_8'] = $row['Month_8'];
    $name_list[$row['ProductID']]['Month_9'] = $row['Month_9'];
    $name_list[$row['ProductID']]['Month_10'] = $row['Month_10'];
    $name_list[$row['ProductID']]['Month_11'] = $row['Month_11'];
    $name_list[$row['ProductID']]['Month_12'] = $row['Month_12'];
	$totalMonth[0] += $row['Val'];
	$totalMonth[1] += $row['Month_1'];
	$totalMonth[2] += $row['Month_2'];
	$totalMonth[3] += $row['Month_3'];
	$totalMonth[4] += $row['Month_4'];
	$totalMonth[5] += $row['Month_5'];
	$totalMonth[6] += $row['Month_6'];
	$totalMonth[7] += $row['Month_7'];
	$totalMonth[8] += $row['Month_8'];
	$totalMonth[9] += $row['Month_9'];
	$totalMonth[10] += $row['Month_10'];
	$totalMonth[11] += $row['Month_11'];
	$totalMonth[12] += $row['Month_12'];
	$totalMonth[13] += $row['Month_1'] + $row['Month_2'] + $row['Month_3'] ; 
	$totalMonth[14] += $row['Month_4'] + $row['Month_5'] + $row['Month_6'] ; 
	$totalMonth[15] += $row['Month_7'] + $row['Month_8'] + $row['Month_9'] ; 
	$totalMonth[16] += $row['Month_10'] + $row['Month_11'] + $row['Month_12'] ; 
	$totalMonth[17] += $row['Val'];
}
$sqlstr = "select FirstName, LastName from people where PersonID = $treeid";
$result = mssql_query($sqlstr);
$reportPerson = mssql_fetch_assoc($result);

?>

		<link rel="stylesheet" type="text/css" href="../css/report.css">
		<script src="../javascript/utils.js,sorttable2.js,filter2.js"></script>
		</td>
	</tr>
</table>
<br>
<center style="font-size:12pt; font-weight:bold;"><?=$reportPerson['FirstName']?> <?=$reportPerson['LastName']?></center>
<br>
		<center>
            <form>
			<table class="report">
				<colgroup>
					<col span=2 />
					<col span=4 style="background-color: #F2FFF2"/>
					<col span=4 style="background-color: #DBFFDB"/>
					<col span=4 style="background-color: #FFEAEA" />
					<col span=4 style="background-color: #FFBFBF" />
					<col span=4 style="background-color: #DDDDFF" />
				</colgroup>
				<tr>
                    <th rowspan=2>Filter</th>
					<th rowspan=2>Product</th>
					<th colspan=4 class="ThickLeft">First Quarter '09</th>
					<th colspan=4 class="ThickLeft">Second Quarter '09</th>
					<th colspan=4 class="ThickLeft">Third Quarter '09</th>
					<th colspan=4 class="ThickLeft">Fourth Quarter '09</th>
					<th rowspan=2 class="ThickLeft">Grand Total</th>
				</tr>
				<tr>
					<th class="ThickLeft" width=100px>Jan</th>
					<th class="ThickLeft" width=100px>Feb</th>
					<th class="ThickLeft" width=100px>Mar</th>
					<th class="ThickLeft">Total</th>
					<th class="ThickLeft" width=100px>Apr</th>
					<th class="ThickLeft" width=100px>May</th>
					<th class="ThickLeft" width=100px>Jun</th>
					<th class="ThickLeft">Total</th>
					<th class="ThickLeft" width=100px>Jul</th>
					<th class="ThickLeft" width=100px>Aug</th>
					<th class="ThickLeft" width=100px>Sep</th>
					<th class="ThickLeft">Total</th>
					<th class="ThickLeft" width=100px>Oct</th>
					<th class="ThickLeft" width=100px>Nov</th>
					<th class="ThickLeft" width=100px>Dec</th>
					<th class="ThickLeft">Total</th>
				
				</tr>

				<?
				$total = 0 ; 
                foreach ($name_list as $id => $name) {

					echo '<tr>';
                    echo '<td><input type="checkbox" name="filter" value="' . $name['abbr'] . '" checked onclick="FilterByCheckbox(this)"></td>';
					echo '<td class="jl">' . $name['name'] . '</td>'; // name 

					$total_q1 = $name_list[$id]['Month_1'] + $name_list[$id]['Month_2'] + $name_list[$id]['Month_3'] ;
					echo '<td class="jr">' . CurrencyFormat($name_list[$id]['Month_1']) . '</td>';
					echo '<td class="jr">' . CurrencyFormat($name_list[$id]['Month_2']) . '</td>';
					echo '<td class="jr">' . CurrencyFormat($name_list[$id]['Month_3']) . '</td>';
					echo '<td class="ThickLeftjr">' . CurrencyFormat($total_q1) . '</td>';

					$total_q2 = $name_list[$id]['Month_4'] + $name_list[$id]['Month_5'] + $name_list[$id]['Month_6'] ;
					echo '<td class="jr">' . CurrencyFormat($name_list[$id]['Month_4']) . '</td>';
					echo '<td class="jr">' . CurrencyFormat($name_list[$id]['Month_5']) . '</td>';
					echo '<td class="jr">' . CurrencyFormat($name_list[$id]['Month_6']) . '</td>';
					echo '<td class="ThickLeftjr">' . CurrencyFormat($total_q2) . '</td>';

					$total_q3 = $name_list[$id]['Month_7'] + $name_list[$id]['Month_8'] + $name_list[$id]['Month_9']  ;
					echo '<td class="jr">' . CurrencyFormat($name_list[$id]['Month_7']) . '</td>';
					echo '<td class="jr">' . CurrencyFormat($name_list[$id]['Month_8']) . '</td>';
					echo '<td class="jr">' . CurrencyFormat($name_list[$id]['Month_9']) . '</td>';
					echo '<td class="ThickLeftjr">' . CurrencyFormat($total_q3) . '</td>';
					
					$total_q4 =  $name_list[$id]['Month_10'] + $name_list[$id]['Month_11'] + $name_list[$id]['Month_12']  ;
					echo '<td class="jr">' . CurrencyFormat($name_list[$id]['Month_10']) . '</td>';
					echo '<td class="jr">' . CurrencyFormat($name_list[$id]['Month_11']) . '</td>';
					echo '<td class="jr">' . CurrencyFormat($name_list[$id]['Month_12']) . '</td>';
					echo '<td class="ThickLeftjr">' . CurrencyFormat($total_q4) . '</td>';
					
					if ( $name_list[$id]['Val'] == NULL ) {
						$val = $total_q1 + $total_q2 + $total_q3 + $total_q4 ; 
					}
					else $val = $name_list[$id]['Val'] ;
					
					echo '<td class="ThickLeft jr">' . CurrencyFormat($val) . '</td>';
					echo '</tr>';
					$total += $val ;
				}
				if ( $total != $totalMonth[17]) {
					$totalMonth[17] = $total ; 
				}
				?>
				<tr>
					<th>&nbsp;</th>
					<th>Totals</th>
					<th class="jr"><?=CurrencyFormat($totalMonth[1]) ?></th>
					<th class="jr"><?=CurrencyFormat($totalMonth[2]) ?></th>
					<th class="jr"><?=CurrencyFormat($totalMonth[3]) ?></th>
					<th class="ThickLeft jr"><?=CurrencyFormat($totalMonth[13]) ?></th>
					
					<th class="jr"><?=CurrencyFormat($totalMonth[4]) ?></th>
					<th class="jr"><?=CurrencyFormat($totalMonth[5]) ?></th>
					<th class="jr"><?=CurrencyFormat($totalMonth[6]) ?></th>
					<th class="ThickLeft jr"><?=CurrencyFormat($totalMonth[14]) ?></th>

					<th class="jr"><?=CurrencyFormat($totalMonth[7]) ?></th>
					<th class="jr"><?=CurrencyFormat($totalMonth[8]) ?></th>
					<th class="jr"><?=CurrencyFormat($totalMonth[9]) ?></th>
					<th class="ThickLeft jr"><?=CurrencyFormat($totalMonth[15]) ?></th>

					<th class="jr"><?=CurrencyFormat($totalMonth[10]) ?></th>
					<th class="jr"><?=CurrencyFormat($totalMonth[11]) ?></th>
					<th class="jr"><?=CurrencyFormat($totalMonth[12]) ?></th>
					<th class="ThickLeft jr"><?=CurrencyFormat($totalMonth[16]) ?></th>

					<th class="ThickLeft jr">$<?=CurrencyFormat($totalMonth[17]) ?></th>

				</tr>
			</table>
            </form>

			<br>

	</body>
</html>

<?php

function GetPercent($numerator, $denominator) {
    $output = ($denominator != 0) ? CurrencyFormat((round($numerator/$denominator * 1000)/10), 1) : '-';    
    return $output;
}

function NullToZero($value) {
    $output = (is_null($value)) ? 0 : $value;
    return $output;
}

?>

