<?php
 /**
 * @package Reports
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_paul_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');

if (!isset($treeid))
{
	begin_report_orig('Product Report');
	print_tree_prompt(SALESPEOPLE);
	end_report_orig();
	exit();
}

begin_report('Target Report');

$sql = "SELECT Company, Division, DATEDIFF(DAY, TargetDate, GETDATE()) AS Age, Renewed, AutoReviveDate
    FROM opportunities WHERE Category = 10 AND PersonID = $treeid ORDER BY Company, Division, Age";

// echo $sql . "<br>";

$result = mssql_query($sql);

$grid = array('new' => array(), 'renew' => array(), 'retarget' => array(), 'total' => array());

$gridRow = -1;
while ($row = mssql_fetch_assoc($result))
{
    $group = floor(($row['Age'] - 1) / 30);
    if ($group > 4) $group = 4;
    
    $grid['total'][$group] += 1;
    $grid['total']['total'] += 1;

    if ($row['Renewed'] == 1) {
        $grid['renew'][$group] += 1;
        $grid['renew']['total'] += 1;
    }
    elseif ($row['AutoReviveDate'] != NULL) {
        $grid['retarget'][$group] += 1;
        $grid['retarget']['total'] += 1;
    }
    else {
        $grid['new'][$group] += 1;
        $grid['new']['total'] += 1;
    }
}

/*
echo "<pre>";
print_r($resultGrid);
echo "</pre>";
*/

$person_sql = "select FirstName, LastName from people where PersonID = $treeid";
$person_result = mssql_query($person_sql);
$report_person = mssql_fetch_assoc($person_result);
?>
		<link rel="stylesheet" type="text/css" href="../css/report.css">
		<script src="../javascript/sorttable.js,utils.js"></script>

		</td>
	</tr>
</table>
<br>
<center style="font-size:12pt; font-weight:bold;"><?=$report_person['FirstName']?> <?=$report_person['LastName']?>
    <br><?php echo date("n/j/Y") ?>
    </center>
<br>
		<center>
			<table class="report">
				<colgroup>
					<col span=9 />
					<col span=3 style="background-color: #DDDDFF" />
				</colgroup>
				<tr>
					<th colspan=3 class="ThickLeft">New (N)</th>
					<th colspan=3 class="ThickLeft">Renew (RN)</th>
					<th colspan=3 class="ThickLeft">Retarget (RT)</th>
					<th colspan=3 class="ThickLeft">Total</th>
				</tr>
				<tr>
					<th class="ThickLeft">&nbsp;</th>
					<th>Days</th>
					<th>%</th>
					<th class="ThickLeft">&nbsp;</th>
					<th>Days</th>
					<th>%</th>
					<th class="ThickLeft">&nbsp;</th>
					<th>Days</th>
					<th>%</th>
					<th class="ThickLeft">&nbsp;</th>
					<th>Days</th>
					<th>%</th>
				</tr>
                <tr style="background-color: #DDDDFF">
                    <td class="ThickLeft">Total</td>
                    <td><?php echo $grid['new']['total'] ?></td>
                    <td>100.0</td>
                    <td class="ThickLeft">Total</td>
                    <td><?php echo $grid['renew']['total'] ?></td>
                    <td>100.0</td>
                    <td class="ThickLeft">Total</td>
                    <td><?php echo $grid['retarget']['total'] ?></td>
                    <td>100.0</td>
                    <td class="ThickLeft">Total</td>
                    <td><?php echo $grid['total']['total'] ?></td>
                    <td>100.0</td>
                </tr>
                <?php
                for ($x = 0; $x < 5; $x++) { 
                    $label_start = $x* 30 + 1;
                    if ($label_start == 1) $label_start = 0;
                    $label_end = ($x + 1) * 30;
                    $label = $label_start . "-" . $label_end;
                    ?>
                <tr>
                    <td class="ThickLeft"><?php echo $label ?></td>
                    <td><?php echo $grid['new'][$x] ?></td>
                    <td class="jr"><?php
                         $percent = round($grid['new'][$x]/$grid['new']['total'] * 1000)/10;
                         if ($percent > 0) echo CurrencyFormat($percent, 1); 
                    ?></td>
                    <td class="ThickLeft"><?php echo $label ?></td>
                    <td><?php echo $grid['renew'][$x] ?></td>
                    <td class="jr"><?php
                         $percent = round($grid['renew'][$x]/$grid['renew']['total'] * 1000)/10;
                         if ($percent > 0) echo CurrencyFormat($percent, 1); 
                    ?></td>
                    <td class="ThickLeft"><?php echo $label ?></td>
                    <td><?php echo $grid['retarget'][$x] ?></td>
                    <td class="jr"><?php
                         $percent = round($grid['retarget'][$x]/$grid['retarget']['total'] * 1000)/10;
                         if ($percent > 0) echo CurrencyFormat($percent, 1); 
                    ?></td>
                    <td class="ThickLeft"><?php echo $label ?></td>
                    <td><?php echo $grid['total'][$x] ?></td>
                    <td class="jr"><?php
                         $percent = round($grid['total'][$x]/$grid['total']['total'] * 1000)/10;
                         if ($percent > 0) echo CurrencyFormat($percent, 1); 
                    ?></td>
                </tr>
                <?php } ?>
           </table>
           
           <br>

			<table class="sortable" id="tableMain">
				<thead>
					<tr>
						<th>&nbsp;</th>
						<th>Company</th>
						<th>Div/Dept/Loc</th>
						<th>Age</th>
						<th>Type</th>
					</tr>
				</thead>
				<tbody>
					<?
					mssql_data_seek($result, 0);
					$count = 0;
					while ($row = mssql_fetch_assoc($result)) {
                        $count++;
                        if ($row['Renewed'] == 1) {
                            $type = "RN";
                        }
                        elseif ($row['AutoReviveDate'] != NULL) {
                            $type = "RT";
                        }
                        else {
                            $type = "N";
                        }

    					echo '<tr>';
    					echo '<td class="jr">' . $count . '</td>';
    					echo '<td class="jl">' . $row["Company"] . '</td>';
    					echo '<td class="jl">' . $row["Division"] . '</td>';
    					echo '<td class="jr">' . $row["Age"] . '</td>';
    					echo '<td>' . $type . '</td>';
    					echo '</tr>';
					}
					?>
				</tbody>
			</table>
		</center>
	</body>
</html>
