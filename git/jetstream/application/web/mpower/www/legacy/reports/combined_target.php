<?php
/**
* @package Reports
*/
include('_paul_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');

$report_name = 'Combined Target Report';

if (!isset($treeid))
{
    begin_report_orig($report_name);
    print_tree_prompt(SALESPEOPLE_MGR);
    end_report_orig();
    exit();
}

begin_report($report_name);

        $person_array = GetSalespeopleTreeArray($treeid);
        $person_list = implode(", ", GetSalespeopleTreeArray($treeid));

        $sql = "SELECT PersonID,
            DATEDIFF(DAY, TargetDate, GETDATE()) AS Age
            FROM opportunities
            WHERE Category = 10
            AND PersonID in ($person_list)
            ORDER BY Company, Division";

        //echo $sql . "<br>";

        $result = mssql_query($sql);

        $grid = array();
        $opp_list = array();

        while ($row = mssql_fetch_assoc($result))
        {
            $group = floor(($row['Age'] - 1) / 30);
            if ($group > 4) $group = 4;

            $grid[$row['PersonID']][$group] += 1;
            $grid[$row['PersonID']]['total'] += 1;
            $grid[$row['PersonID']]['opps'][] = $row;
            $grid['total'][$group] += 1;
            $grid['total']['total'] += 1;
        }


        /*
        echo "<pre>";
        print_r($opp_list);
        echo "</pre>";
        */

        $people_sql = "select PersonID, FirstName, LastName from people where PersonID in ($person_list, $treeid)";
        $people_result = mssql_query($people_sql);

        while ($row = mssql_fetch_assoc($people_result))
        {
            $person_grid[$row['PersonID']]['firstname'] = $row['FirstName'];
            $person_grid[$row['PersonID']]['lastname'] = $row['LastName'];
        }

        foreach ($person_array as $person_id) {
            $sort_array[$person_id] = $person_grid[$person_id]['lastname'] . ', ' . $person_grid[$person_id]['firstname'];
        }

        asort($sort_array);
        $person_array = array_keys($sort_array);

        //$report_person = mssql_fetch_assoc($person_result);
        ?>
        <link rel="stylesheet" type="text/css" href="../css/report.css">
        <script src="../javascript/sorttable.js,utils.js"></script>

        </td>
    </tr>
</table>
<br>
<center style="font-size:12pt; font-weight:bold;"><?=$person_grid[$treeid]['firstname']?> <?=$person_grid[$treeid]['lastname']?>
    <br><?php echo date("n/j/Y") ?>
</center>
<br>
<center>
    <table class="report">
        <tr>
            <th>&nbsp;</th>
            <th>Salesperson</th>
            <th class="ThickLeft">Total</th>
            <?php
            for ($x = 0; $x < 5; $x++) {
                echo "<th class=\"ThickLeft\">" . GetTargetLabel($x) . "</th>";
                echo "<th>%</th>";
            }
            ?>
        </tr>
        <?php
        $num = 0;
        foreach($person_array as $person) {
            $num++;
            $person_grid[$person]['number'] = $num;
            echo "<tr>";
            echo "<td>$num</td>";
            echo "<td class=\"jl\">{$person_grid[$person]['firstname']} {$person_grid[$person]['lastname']} (<a href=\"{$_SERVER["PHP_SELF"]}?treeid=$treeid&detailid=$person\">detail</a>)</td>";
            echo "<td class=\"ThickLeft\" style=\"background-color: #DDDDFF\">{$grid[$person]['total']}</td>";

            for ($x = 0; $x < 5; $x++) {
                $percent = round($grid[$person][$x]/$grid[$person]['total'] * 1000)/10;
                echo "<td class=\"ThickLeft\">{$grid[$person][$x]}</td>";
                echo "<td class=\"jr\">";
                if ($percent > 0) {
                    echo CurrencyFormat($percent, 1);
                }
                echo "</td>";
            }

            echo "</tr>";
        }
        echo "<tr>";
        echo "<th>&nbsp;</th>";
        echo "<th>Total</th>";
        echo "<th class=\"ThickLeft\">{$grid['total']['total']}</th>";

        for ($x = 0; $x < 5; $x++) {
            $percent = round($grid['total'][$x]/$grid['total']['total'] * 1000)/10;
            echo "<th class=\"ThickLeft\">{$grid['total'][$x]}</th>";
            echo "<th class=\"jr\">";
            if ($percent > 0) {
                echo CurrencyFormat($percent, 1);
            }
            echo "</th>";
        }

        echo "</tr>";
        ?>
    </table>
</center>
<br>


<?php
if (isset($detailid) && in_array($detailid, $person_array)) {

    $grid = array();

    $sql = "SELECT DealID, Company, Division,
        DATEDIFF(DAY, TargetDate, GETDATE()) AS Age,
        Renewed, AutoReviveDate
        FROM opportunities
        WHERE Category = 10
        AND PersonID = $detailid
        ORDER BY Company, Division, Age";

    // echo $sql . "<br>";

    $result = mssql_query($sql);



    while ($row = mssql_fetch_assoc($result)) {

        $opp_list[$row['DealID']] = strtolower(trim($row['Company'])) . " " . strtolower(trim($row['Division']));

        $group = floor(($row['Age'] - 1) / 30);
        if ($group < 0) $group = 0;
        if ($group > 4) $group = 4;

        $grid['total'][$group] += 1;
        $grid['total']['total'] += 1;

        if ($row['Renewed'] == 1) {
            $grid['renew'][$group] += 1;
            $grid['renew']['total'] += 1;
        }
        elseif ($row['AutoReviveDate'] != NULL) {
            $grid['retarget'][$group] += 1;
            $grid['retarget']['total'] += 1;
        }
        else {
            $grid['new'][$group] += 1;
            $grid['new']['total'] += 1;
        }
    }

    /*
    echo "<pre>";
    print_r($resultGrid);
    echo "</pre>";
    */

    //$person_sql = "select FirstName, LastName from people where PersonID = $detailid";
    //$person_result = mssql_query($person_sql);
    //$report_person = mssql_fetch_assoc($person_result);
    ?>

    <br>
    <center style="font-size:12pt; font-weight:bold;">Detail for <? echo $person_grid[$detailid]['firstname'] . " " . $person_grid[$detailid]['lastname']?></center>
    <br>
    <center>
        <table class="report">
            <colgroup>
                <col span=9 />
                <col span=3 style="background-color: #DDDDFF" />
            </colgroup>
            <tr>
                <th colspan=3 class="ThickLeft">New (N)</th>
                <th colspan=3 class="ThickLeft">Renew (RN)</th>
                <th colspan=3 class="ThickLeft">Retarget (RT)</th>
                <th colspan=3 class="ThickLeft">Total</th>
            </tr>
            <tr>
                <th class="ThickLeft">&nbsp;</th>
                <th>Days</th>
                <th>%</th>
                <th class="ThickLeft">&nbsp;</th>
                <th>Days</th>
                <th>%</th>
                <th class="ThickLeft">&nbsp;</th>
                <th>Days</th>
                <th>%</th>
                <th class="ThickLeft">&nbsp;</th>
                <th>Days</th>
                <th>%</th>
            </tr>
            <tr style="background-color: #DDDDFF">
                <td class="ThickLeft">Total</td>
                <td><?php echo $grid['new']['total'] ?></td>
                <td>100.0</td>
                <td class="ThickLeft">Total</td>
                <td><?php echo $grid['renew']['total'] ?></td>
                <td>100.0</td>
                <td class="ThickLeft">Total</td>
                <td><?php echo $grid['retarget']['total'] ?></td>
                <td>100.0</td>
                <td class="ThickLeft">Total</td>
                <td><?php echo $grid['total']['total'] ?></td>
                <td>100.0</td>
            </tr>
            <?php
            for ($x = 0; $x < 5; $x++) {
                $label = GetTargetLabel($x);
                ?>
                <tr>
                    <td class="ThickLeft"><?php echo $label ?></td>
                    <td><?php echo $grid['new'][$x] ?></td>
                    <td class="jr"><?php
                        $percent = 0;
                        if ($grid['new']['total'] > 0) {
                            $percent = round($grid['new'][$x]/$grid['new']['total'] * 1000)/10;
                        }
                        if ($percent > 0) echo CurrencyFormat($percent, 1);
                    ?></td>
                    <td class="ThickLeft"><?php echo $label ?></td>
                    <td><?php echo $grid['renew'][$x] ?></td>
                    <td class="jr"><?php
                        $percent = 0;
                        if ($grid['renew']['total'] > 0) {
                            $percent = round($grid['renew'][$x]/$grid['renew']['total'] * 1000)/10;
                        }
                        if ($percent > 0) echo CurrencyFormat($percent, 1);
                    ?></td>
                    <td class="ThickLeft"><?php echo $label ?></td>
                    <td><?php echo $grid['retarget'][$x] ?></td>
                    <td class="jr"><?php
                        $percent = 0;
                        if ($grid['retarget']['total'] > 0) {
                            $percent = round($grid['retarget'][$x]/$grid['retarget']['total'] * 1000)/10;
                        }
                        if ($percent > 0) echo CurrencyFormat($percent, 1);
                    ?></td>
                    <td class="ThickLeft"><?php echo $label ?></td>
                    <td><?php echo $grid['total'][$x] ?></td>
                    <td class="jr"><?php
                        $percent = 0;
                        if ($grid['total']['total'] > 0) {
                            $percent = round($grid['total'][$x]/$grid['total']['total'] * 1000)/10;
                        }
                        if ($percent > 0) echo CurrencyFormat($percent, 1);
                    ?></td>
                </tr>
            <?php } ?>
        </table>

        <br>

        <table class="sortable" id="tableMain">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Company</th>
                    <th>Div/Dept/Loc</th>
                    <th>Age</th>
                    <th>Type</th>
                    <th>Target Match</th>
                </tr>
            </thead>
            <tbody>
                <?
                mssql_data_seek($result, 0);
                $count = 0;

                while ($row = mssql_fetch_assoc($result)) {
                    $count++;
                    if ($row['Renewed'] == 1) {
                        $type = "RN";
                    }
                    elseif ($row['AutoReviveDate'] != NULL) {
                        $type = "RT";
                    }
                    else {
                        $type = "N";
                    }
                    $comp_div = strtolower(trim($row['Company']));
                    if (strlen(trim($row['Division'])) > 0) {
                        $comp_div .= " ~ " . strtolower(trim($row['Division']));
                    }



                    $match = implode(" ", array_remval($row['DealID'], array_keys($opp_list, $comp_div)));

                    echo '<tr>';
                    echo '<td class="jr">' . $count . '</td>';
                    echo '<td class="jl">' . $row["Company"] . '</td>';
                    echo '<td class="jl">' . $row["Division"] . '</td>';
                    echo '<td class="jr">' . $row["Age"] . '</td>';
                    echo '<td>' . $type . '</td>';

                    echo '<td>' . $match . '</td>';

                    echo '</tr>';
                }
                ?>
            </tbody>
        </table>
    </center>
    <?php } ?>
</body>
</html>


<?php

function GetTargetLabel($section) {
    $label_start = $section * 30 + 1;
    if ($label_start == 1) {
        $label_start = 0;
    }
    $label_end = ($section + 1) * 30;
    $label = $label_start . "-" . $label_end;
    if ($section == 4) {
        $label = "> 120";
    }
    return $label;
}

function array_remval($val, &$arr) {
    $array_remval = array();
    foreach($arr as $key => $element)
    {
        if ($element != $val) {
            $array_remval[$key] = $element;
        }
    }
    return $array_remval;
}

?>
