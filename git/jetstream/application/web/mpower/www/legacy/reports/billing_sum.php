<?php
 /**
 * @package Reports
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_report_utils.php');
include('_date_params.php');


/********************************************/
begin_report('Expected Billing Summary Report');

if (!isset($treeid))
{
	include('GetSalespeopleTree.php');
	print_tree_prompt(ANYBODY);
	end_report();
	exit();
}


if (!isset($dateFrom)||!isset($dateTo))
	{
	$sortcol=0;
	$ord=0;
	$mt_nextmonth = mktime(0, 0, 0, date("m")+1, date("d"),  date("Y"));
	$nextmonth = date("m/d/Y", $mt_nextmonth);

	print_date_params($treeid,$sortcol,$ord,date("m/d/Y"),$nextmonth);
	if(!isset($dateFrom)) $dateFrom="N/A";
	if(!isset($dateTo)) $dateTo="N/A";
	end_report();
	exit();
	}

include('TableUtils.php');
include('jim_utils.php');
include('GetSalespeopleTree.php');

define(SHADE_MED, "MED_BLUE");
define(SHADE_LIGHT, "LIGHT_BLUE");

function GetCompanyName($id)
{
	$sql = "select * from company where CompanyID='$id'";
	$ok = mssql_query($sql);
	if(($row = mssql_fetch_assoc($ok)))
	{
		return $row['Name'];
	}
	return 'Unknown!';
}



function GetDateComponent($timestamp, $key)
{
	$info = getdate($timestamp);
	return $info[$key];
}

function PrintDetail($dateFrom, $dateTo)
{
	global $arrTotals, $treeid;
	global $firstmon,$lastmon,$firstyr,$lastyr;
	$arrMonths = array('Jan','Feb','Mar','Apr','May','June','July','Aug','Sept','Oct','Nov','Dec');

	$url = $_SERVER['PHP_SELF'] . "?treeid=$treeid";
	if($dFrom!=""&&$dFrom!="N/A")
	{
		$uFrom=urlencode($dFrom);
		$uTo=urlencode($dTo);
		$url.="&dateFrom=$uFrom&dateTo=$uTo";
	}

	$arrColHeads=array(
		"<b>OFFERING",
	);

	for ($y=$firstyr,$m=$firstmon; $y<$lastyr || ($y==$lastyr && $m<=$lastmon);)
	{
		array_push($arrColHeads, '<center><b>'.$arrMonths[$m-1]." - $y");
		if (++$m > 12)
		{
			$y++;
			$m=1;
		}
	}
	array_push($arrColHeads, '<center><b>TOTALS');


print("<table border=2 cellspacing=0 cellpadding=3 bgcolor='ivory' width='100%'>");

	PrintColHeads($arrColHeads, 1, 0, count($arrColHeads),FALSE);
	$arrTots = array();

	for ($i=0; $i < count($arrTotals); $i++)
	{
		BeginTableRow();
		PrintTableCell($arrTotals[$i]['ProductName']);
		$tot=0;
		for ($y=$firstyr,$m=$firstmon; $y<$lastyr || ($y==$lastyr && $m<=$lastmon);)
		{
			PrintTableCellEx('$' . number_format($arrTotals[$i]["$y-$m"]),"align='right'");
			$tot+=$arrTotals[$i]["$y-$m"];
			$arrTots["$y-$m"]+=$arrTotals[$i]["$y-$m"];
			if (++$m > 12)
			{
				$y++;
				$m=1;
			}
		}
		PrintTableCellEx('$' . number_format($tot),"align='right' bgcolor=lightgrey");
		EndTableRow();


	}

	BeginTableRowData(1);
	PrintTableCell('<b>TOTALS');
	$tot=0;
	for ($y=$firstyr,$m=$firstmon; $y<$lastyr || ($y==$lastyr && $m<=$lastmon);)
	{
		PrintTableCellEx('$' . number_format($arrTots["$y-$m"]),"align='right'");
		$tot+=$arrTots["$y-$m"];
		if (++$m > 12)
		{
			$y++;
			$m=1;
		}
	}
	PrintTableCellEx('$' . number_format($tot),"align='right' bgcolor=lightblue");


	EndTableRow();

	EndTable();
}


$idList = GetSalespeopleTree($treeid);

$sql = 	"SELECT opportunities.EstimatedDollarAmount, opportunities.ActualDollarAmount,";
$sql .= " opportunities.ActualCloseDate, opportunities.BillDate, products.Name AS ProductName";
$sql .= " FROM opportunities ";
$sql .= " INNER JOIN products ON opportunities.ProductID = products.ProductID";
//No closed opps in billing summary! Al Lencioni, 9/14/2005
//$sql .= " WHERE opportunities.Category < 9 AND PersonID IN ($idList)";
$sql .= " WHERE opportunities.Category < 6 AND PersonID IN ($idList)";
$sql .= " AND opportunities.BillDate >= '$dateFrom' AND opportunities.BillDate <= '$dateTo'";
$sql .= " ORDER BY products.Name, opportunities.BillDate";

$ok = mssql_query($sql);
$arrTotals = array();

$lastprod='';
$firstmon=13;
$lastmon=0;
$firstyr=9999;
$lastyr=0;

while(($row=mssql_fetch_assoc($ok)))
{
	// if we hit a new product, write the last one off and create a new array
	if ($lastprod != $row['ProductName'])
	{
		if ($lastprod != '')	// don't write off the first empty product
		{
			array_push($arrTotals, $tmp);
		}

		$tmp = array();
		$tmp['ProductName'] = $row['ProductName'];
		$lastprod = $tmp['ProductName'];
	}

	$BillDate=strtotime($row['BillDate']);
	$info = getdate($BillDate);
	$y = $info['year'];
	$m = $info['mon'];
	if (strlen($row['ActualCloseDate']))
		$bill=$row['ActualDollarAmount'];
	else
		$bill=$row['EstimatedDollarAmount'];
	$tmp["$y-$m"]+=$bill;

	if ($firstyr>$y || ($firstyr==$y && $firstmon>$m))
	{
		$firstyr=$y;
		$firstmon=$m;
	}

	if ($lastyr<$y || ($lastyr==$y && $lastmon<$m))
	{
		$lastyr=$y;
		$lastmon=$m;
	}

}
// put the last product row into the array
if ($lastprod != '') array_push($arrTotals, $tmp);




/****************Print**********************/
/*
print("<meta http-equiv=\"Refresh\" content=\"10; URL=ClosedOpportunities_HTML.php?treeid=$treeid&dateFrom=$dateFrom&dateTo=$dateTo\">");


print("<br><br><b>     Please wait while your report is being prepared.<br><br>");

end_report();
*/

print_tree_path($treeid);

$strdate = date("M d Y", time());

$salestree=make_tree_path($treeid);
BeginTable();

//print("<table width='100%'>");
print("<table border=2 cellspacing=0 cellpadding=3 bgcolor='ivory' width='100%'>");

//print("<tr ><td align='center'><b>$title</td></tr>");

$companyname = GetCompanyName($mpower_companyid);
BeginTableRow();
PrintTableSpan(4, "<b>$companyname");
EndTableRow();

$daterange = "Date Range: $dateFrom to $dateTo";

BeginTableRow();
PrintTableSpan(4, "<b>$daterange<br><br>");
EndTableRow();
EndTable();
//print('<table width="100%">');
print("<table border=2 cellspacing=0 cellpadding=3 bgcolor='ivory' width='100%'>");
BeginTableRowData(2);
PrintTableSpan(12, "<b>Projected Revenues");
EndTableRow();

EndTable();

//print detail
PrintDetail($dateFrom, $dateTo);

end_report();

close_db();

?>
