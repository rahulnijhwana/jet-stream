<?php
 /**
 * @package Reports
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_report_utils.php');
include('jim_utils.php');
include('ci_pdf_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');

define(SHADE_MED, "MED_BLUE");
define(SHADE_LIGHT, "LIGHT_BLUE");


function GetElapsedDays($StartTime, $EndTime)
{
	$Diff = $EndTime - $StartTime;
	return (int)($Diff / (60 * 60 * 24));
}


function GetLevel($personid)
{
	$sql = "select level from people where PersonID=$personid";

	$result = mssql_query($sql);
	if ($result)
	{
		if($row = mssql_fetch_object($result))
			return $row->level;
	}
	return -1;
}

function GetMonth($date)
{
	$str = date("m", $date);
	return (int)$str;
}

function GetYear($date)
{
	$str = date("Y", $date);
	return (int)$str;
}

function GetStartCurrentQuarter()
{
	$today = time();
	$month = GetMonth($today);
	$year = GetYear($today);
	$startmonth;
	if($month < 4) $startmonth = 1;
	else if ($month < 7) $startmonth = 4;
	else if ($month < 10) $startmonth = 7;
	else $startmonth = 10;
//$year=2000;
	$startdate = "$month-1-$year";

	return $startdate;
//return "5-1-2003";
}

function GetClosedCurrentQuarter($salesid)
{
	$closed = array();
	$startdate = GetStartCurrentQuarter();
	$sql = "select * from opportunities where PersonID in ($salesid) and ActualCloseDate>='$startdate'";

	$result = mssql_query($sql);
	if ($result)
	{
		while($row = mssql_fetch_object($result))
		{
			$product = $row->ProductID;
			$amount = $row->ActualDollarAmount;
			$baseamount = 0;
			$tag = "$product";
			if(isset($closed[$tag]))
				$baseamount = $closed[$tag];
			$closed[$tag] = $baseamount + $amount;
		}
	}

	return $closed;
}

function GetDefaultRatios($CompanyID, &$IPRatio, &$DPRatio)
{
	$IPRatio = 0;
	$DPRatio = 0;

	$sql = "select * from company where CompanyID=$CompanyID";
	$result = mssql_query($sql);
	if($result)
	{
		if($row = mssql_fetch_object($result))
		{
			$IPRatio = $row->IPCloseRatioDefault / 100;
			$DPRatio = $row->DPCloseRatioDefault / 100;
		}
	}
}

function GetProducts(&$Products, $CompanyID)
{
	$Products = array();
	$sql = "select * from products where CompanyID=$CompanyID and Deleted=0 order by Name";
	$result = mssql_query($sql);
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$prod=array();
			$id=$row['ProductID'];
			$name=$row['Name'];
			$def = $row['DefaultSalesCycle'];
			$deleted = $row['Deleted'];

			$prod['ProductID'] = $id;
			$prod['Name'] = $name;
			$prod['DefCycle'] = $def;
			array_push($Products, $prod);
		}
	}
}

function GetSalesCycle($personid, $productid)
{
	if(FALSE==strstr($personid,','))
		$sql="select * from salescycles where PersonID=$personid and productid=$productid";
	else
		$sql="select * from salescycles where PersonID in ($personid) and productid=$productid";

	$result=mssql_query($sql);
	if($result)
	{
		$row=mssql_fetch_object($result);
		if($row) return $row->SalesCycleLength;
	}
}

function GetOpenOpportunities(&$Opportunities, $SalesmanID, &$Cycles, &$Errors, $issales)
{
	$Opportunities = array();
	$today = time();

	if($issales)
		$sql = "select * from opportunities where PersonID=$SalesmanID and (Category=2 or Category=4) order by ProductID";
	else
	{
		$inclause = GetSalespeopleTree($SalesmanID);
		if(strlen($inclause == 0)) return;
		$sql = "select * from opportunities where PersonID IN($inclause) and (Category=2 or Category=4) order by ProductID";
	}

	$result = mssql_query($sql);
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$firstmeeting = strtotime($row['FirstMeeting']);

			$value = $row['EstimatedDollarAmount'];
			$product = $row['ProductID'];
			$elapseddays = GetElapsedDays($firstmeeting, $today);
			$category = $row['Category'];
			$foreclosedate = $row['ForecastCloseDate'];
			$adjclosedate = $row['AdjustedCloseDate'];
			$company = $row['Company'];

			if($product < 0) continue;
			if($value == 0) continue;

			$closedate;
			if(isset($adjclosedate)) $closedate = $adjclosedate;
			else if(isset($foreclosedate)) $closedate = $foreclosedate;
			else
			{
				$personid=$row['PersonID'];
//				$code = "$SalesmanID:$product";
				$code = "$personid:$product";
				$defaultdays = $Cycles[$code];
				$closedate = $firstmeeting + ($defaultdays * 60 * 60 * 24);
			}

			$forecastdays = GetElapsedDays($firstmeeting, $closedate);
			$days = $forecastdays - $elapseddays;
//print("first mtg: $firstmeeting closedate: $closedate  forecastdays: $forecastdays elapseddays: $elapseddays days: $days<br>");

			if($days < 0)
			{
				$err = $company;
				array_push($Errors, $err);
				continue;
			}

			$op = array();
			$op['Value'] = $value;
			$op['ProductID'] = $product;
			$op['Days'] = $days;
			$op['Used'] = 0;
			$op['Category'] = $category;
			$op['Company'] = $company;
			array_push($Opportunities, $op);
		}
	}
}


function GetCloseRatios($SalesID, &$InfoCount, &$DecisionCount, &$CloseCount)
{
	$InfoCount=0;
	$DecisionCount=0;
	$CloseCount=0;
//	$sql = "select * from log where PersonID=$SalesID and TransactionID=1";
	$sql = "select * from log where PersonID in($SalesID) and TransactionID=1";

	$sql .= " order by DealID, WhenChanged";

	$result = mssql_query($sql);
	if($result)
	{
		$lastdeal=-1;
		$info=0;
		$decision=0;
		$close=0;
		$lastcat = -1;

		while($row = mssql_fetch_object($result))
		{
			$dealid = $row->DealID;
			$category = $row->LogInt;
			if($dealid != $lastdeal)
			{
				if($lastcat == 6)
				{
					$CloseCount++;
					$InfoCount++;
					$DecisionCount++;
				}

				else if($lastcat == 9)
				{
					if($info>0) $InfoCount++;
					if($decision>0) $DecisionCount++;
				}

				$lastdeal = $dealid;
				$info=0;
				$decision=0;
				$close=0;
			}

			if($category == 2)
				$info++;

			if($category == 4)
			{
				$info++;
				$decision++;
			}

			$lastcat = $category;

		}

		if($lastcat == 6 || $lastcat == 9)
		{
			if($info > 0) $InfoCount++;
			if($decision > 0) $DecisionCount++;
		}
	}
}

function ReadSalesmanRow(&$row, &$Errors, &$Cycles, $issales)
{
	$sales=array();
	$id=$row['PersonID'];
	$name=$row['FirstName'];
	$name .= ' ';
	$name .= $row['LastName'];
	$sales['PersonID'] = $id;
	if(!$issales)
	{
		$group = $row['GroupName'];
		if(strlen($group)) $name .= " ($group)";
	}
//print "Name: $name<br>";
	$sales['Name'] = $name;

	$Opportunities = array();
	GetOpenOpportunities($Opportunities, $id, $Cycles, $Errors, $issales);
	$sales['Opportunities'] = $Opportunities;

	$InfoCount=0;
	$DecisionCount=0;
	$CloseCount=0;
	GetCloseRatios($id, $InfoCount, $DecisionCount, $CloseCount);
	$sales['InfoCount'] = $InfoCount;
	$sales['DecisionCount'] = $DecisionCount;
	$sales['CloseCount'] = $CloseCount;
	$closed = GetClosedCurrentQuarter($id);
	$sales['Closed'] = $closed;

	return $sales;
}


function GetSalesmen(&$Salesmen, $supervisorid, &$Cycles, &$Errors)
{
	$sql = "select * from people where PersonID=$supervisorid and Deleted=0";
	$result = mssql_query($sql);
	if($result)
	{
		if($row = mssql_fetch_assoc($result))
		{
			$sales = ReadSalesmanRow($row, $Errors, $Cycles, 0);
			array_push($Salesmen, $sales);
		}
	}


	$sql = "select * from people where SupervisorID=$supervisorid and Level>1 and Deleted=0";
	$sql .= " order by LastName";
	$result = mssql_query($sql);
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$sales = ReadSalesmanRow($row, $Errors, $Cycles, 0);
			array_push($Salesmen, $sales);
		}
	}


	$sql = "select * from people where SupervisorID=$supervisorid and IsSalesperson=1 and Deleted=0";
	$sql .= " order by LastName";
	$result = mssql_query($sql);
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$sales = ReadSalesmanRow($row, $Errors, $Cycles, 1);
			array_push($Salesmen, $sales);
		}
	}
}


function GetSingleSalesman(&$Salesmen, $personid, &$Cycles, &$Errors)
{
	$sql = "select * from people where PersonID=$personid";

	$result = mssql_query($sql);
	if($result)
	{
		if($row = mssql_fetch_assoc($result))
		{
			$sales = ReadSalesmanRow($row, $Errors, $Cycles, true);
			array_push($Salesmen, $sales);
		}
	}
}




function CalcDays(&$Opportunities, $ProductID, $colcount, $IPRatio, $DPRatio, &$arrAnswers, $ind, $amtclosed, $amtopen, $amttotal)
{
	$tmp = array();
	for($i=0; $i<$colcount; $i++)
	{
		$totval = 0;

		$opcount = count($Opportunities);
		$maxdays = ($i+1) * 30;
		$mindays=$i*30;
		for($j=0; $j<$opcount; $j++)
		{
			$op = $Opportunities[$j];
			$prodid = $op['ProductID'];
			$value = $op['Value'];
			$days = $op['Days'];
			$category = $op['Category'];

			if($prodid != $ProductID) continue;
			if($days > $maxdays) continue;
			if($days < $mindays) continue;

			if($category == 2) $value *= $IPRatio;
			else if($category == 4) $value *= $DPRatio;
			else $value = 0;
			$totval += (int)$value;
			$Opportunities[$j] = $op;
		}
//		AddColumnText($report, number_format(round(k($totval), 1), 1), "right", 0, 0);
		$strInd = $maxdays . 'days';
		$tmp[$i] = $totval;

	}
	$tmp[$colcount] = $amtclosed;
	$tmp[$colcount+1] = $amtopen;
	$tmp[$colcount+2] = $amttotal;

	$arrAnswers[$ind] = $tmp;
}

function PrintDays(&$report, $arrRow, $colcount)
{
	for ($i=0; $i < count($arrRow); $i++)
	{
		if($i < $colcount)
		{
			AddColumnText($report, fm(number_format(round(k($arrRow[$i]), 1), 1), -1), "right", 0, 0);
		}
		else
		{
			AddColumnText($report, fm(round(k($arrRow[$i])), 1), "right", 0, 0);
		}

	}
}


/*
function PrintDays(&$report, &$Opportunities, $ProductID, $colcount, $IPRatio, $DPRatio)
{
	for($i=0; $i<$colcount; $i++)

	{
		$totval = 0;

		$opcount = count($Opportunities);
		$maxdays = ($i+1) * 30;
		$mindays=$i*30;
		for($j=0; $j<$opcount; $j++)
		{
			$op = $Opportunities[$j];
			$prodid = $op['ProductID'];
			$value = $op['Value'];
			$days = $op['Days'];
			$category = $op['Category'];

			if($prodid != $ProductID) continue;
			if($days > $maxdays) continue;
			if($days < $mindays) continue;

			if($category == 2) $value *= $IPRatio;
			else if($category == 4) $value *= $DPRatio;
			else $value = 0;
			$totval += (int)$value;
			$Opportunities[$j] = $op;
		}
		AddColumnText($report, number_format(round(k($totval), 1), 1), "right", 0, 0);

	}
}*/


function PrintDays_dummy(&$report, &$Opportunities, $ProductID, $colcount, $IPRatio, $DPRatio)
{
	for($i=0; $i<$colcount; $i++)
	{
		AddColumnText($report, '9,999.9', "right", 0, 0);
	}
}

function CalcRatio($total, $closed, $defaultratio)
{
	$makeup=0;
	if($total < 30) $makeup = 30 - $total;

	$total += $makeup;
	$closed += $makeup * $defaultratio;
	$ratio = $closed / $total;
	return $ratio;
}

function TotalOpenOpportunities($ProductID, &$Opportunities)
{
	$opcount = count($Opportunities);
	$total = 0;
	for($i=0; $i<$opcount; $i++)
	{
		$op = $Opportunities[$i];
		$prodid = $op['ProductID'];
		if($prodid == $ProductID)
		{
			$value = $op['Value'];
			$total += $value;
		}
	}
	return $total;
}

function MakeBreaks($name)
{
	$name = str_replace('/', ' / ', $name);
	$name = str_replace('-', ' - ', $name);
	$name = str_replace(':', ': ', $name);
	return $name;
}

function BoxRowItems(&$report, $startcol, $count, $top)
{
	$c_row = $report['currentrow'];
	for ($i=0; $i < $count; $i++)
	{
		$colind = $startcol + $i;
		SetItemValue($report, $c_row, $colind, 'border_right', 1);
		SetItemValue($report, $c_row, $colind, 'border_left', 1);
		if($top)
		{
			SetItemValue($report, $c_row, $colind, 'border_bottom', 1);
			SetItemValue($report, $c_row, $colind, 'border_top', 1);
		}
	}
}

function k($val)
{
	return $val / 1000;
}

function SumTotals($arrAnswers, &$arrTotals)
{
	for ($i=0; $i < count($arrAnswers); $i++)
	{
		$row = $arrAnswers[$i];
		for($j=0; $j < count($row); $j++)
		{
			$arrTotals[$j] += $row[$j];
		}
	}
	return $arrTotals;
}


function PrintSalesmanNumbers(&$report, $level, &$sales, $colcount, &$Products, $IPDefault, $DPDefault)
{
	$sname = $sales['Name'];
	$id = $sales['PersonID'];
	$InfoCount = $sales['InfoCount'];
	$DecisionCount = $sales['DecisionCount'];
	$CloseCount = $sales['CloseCount'];

//print("info = $InfoCount dp = $DecisionCount close = $CloseCount<br>");

	$IPRatio = CalcRatio($InfoCount, $CloseCount, $IPDefault);
	$DPRatio = CalcRatio($DecisionCount, $CloseCount, $DPDefault);
//print "IP=$IPRatio DP=$DPRatio";

	$Opportunities = $sales['Opportunities'];
	$closed = $sales['Closed'];

	$count = count($Products);

	$arrAnswers=array();

	for($i=0; $i<$count; $i++)
	{
		$prod = $Products[$i];
		$pid = $prod['ProductID'];
		$tag = "$id";
		$closedamount = 0;
		if(isset($closed[$tag]))
			$closedamount = $closed[$tag];

		$openamount = TotalOpenOpportunities($pid, $Opportunities);

		CalcDays($Opportunities, $pid, $colcount, $IPRatio, $DPRatio, $arrAnswers, $i, $closedamount, $openamount, $closedamount+$openamount);
	}


	$totals=array(count($arrAnswers[0]));
	SumTotals($arrAnswers, $totals);
	BeginNewRow($report, RT_COL_TEXT_BOLD, NORMAL_FONTSIZE, 0);
	SetRowColorStandard($report, -1, SHADE_MED);
	$hilight=0;
	if ($level>1)
	{
		AddColumnText($report, $sname, "", 0, 0);

	}
	$hilight=1;
	AddColumnText($report, "Totals:", "center", 0,0);
	$tcount = count($totals);
	for ($i=0; $i < $tcount; $i++)
		{
			if ($i < $tcount - 3)
				AddColumnText($report, number_format(round(k($totals[$i]),1),1), "right", 0, 0);
			else
				AddColumnText($report, number_format(round(k($totals[$i]))), "right", 0, 0);
		}
	BoxRowItems($report, 0, $colcount+5, 1);

	for($i=0; $i<$count; $i++)
	{
		$prod = $Products[$i];
		$name = $prod['Name'];
		BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
		if($hilight) SetRowColorStandard($report, -1, SHADE_LIGHT);
		if($hilight) $hilight=0;
		else $hilight=1;

		if($level>1) AddColumnText($report, " ", "", 0, 0);

		AddColumnText($report, MakeBreaks($name), "left", 0, 0);

		PrintDays($report, $arrAnswers[$i], $colcount);
		$startcol= $level > 1 ? 1 : 2;
		BoxRowItems($report, 0, $colcount+5, 0);

	}

}


function PrintColTitles(&$report, $firstcol, $secondcol, $maxdays)
{
	BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0);
	SetRowValue($report, -1, "rotate", 1);
	if($firstcol)
		{
			AddColumn($report, "$firstcol", "", 1.0, 0);
		}
	AddColumn($report, "$secondcol", "center", 1.0, 0);

	SetRowColorStandard($report, -1, SHADE_MED);
	SetColColorStandard($report, -1, -1, SHADE_MED);
	$colcount=0;
	for($i=30; $i<=$maxdays; $i+=30)
	{
		AddColumn($report, "$i Day", "center", .4, 0);
		$colcount++;
	}
	AddColumn($report, "This Quarter ".GetCatLbl("Closed"), "", .5, 0);
	AddColumn($report, "This Quarter Open", "", .5, 0);
	AddColumn($report, "This Quarter Total", "", .5, 0);

}

/*
function AddLocationRow(&$report, $text1, $text2, $left, $width1, $width2)
{
	BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0);
	AddFreeFormText($report, $text1, -1, "center", $left, $width1);
	AddFreeFormText($report, $text2, -1, "left", $left+$width1, $width2);
}
*/

function GetSalesCycles(&$Cycles, $CompanyID, &$maxdays)
{
	$maxdays = 0;
	$Cycles = array();
	$sql = "select * from salescycles where CompanyID=$CompanyID order by PersonID, ProductID";
	$result = mssql_query($sql);
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$person = $row['PersonID'];
			$product = $row['ProductID'];
			$days = $row['SalesCycleLength'];
			if($days > $maxdays) $maxdays = $days;
			$code = "$person:$product";

			$Cycles[$code] = $days;
		}
	}
}

function GetMaxDays($companyid)
{
	$sql="select Max(SalesCycleLength) as MaxDays from SalesCycles where CompanyID=$companyid group by CompanyID";
	$result=mssql_query($sql);
	if ($result)
	{
		$row=mssql_fetch_assoc($result);
		if($row) return $row['MaxDays'];
	}
}

/***************************PDF*************************/

$report = CreateReport();
$report['pagewidth'] = 11*72;
$report['pageheight'] = 8.5*72;
$report['left'] = 72/4;
$report['right'] = 10.75 * 72;

$report['bottom'] = 8.25 * 72;

$report['normalfontsize'] = 8;

$p = PDF_new();
PDF_set_parameter($p, "license", PDFLIB_LICENSE_KEY);
PDF_open_file($p, "");
StartPDFReport($report, $p, "AAAA", "BBBB", "CCCCC");
//SetLogo($report, "report_logo.jpg", 10, 40, 72, 0);
SetLogo($report, "report_logo.jpg", 14, 50, 110, 0);

$sql="select name from company where companyid=$mpower_companyid";
$ok=mssql_query($sql);
if(($row=mssql_fetch_assoc($ok)))
{
	$companyname=$row['name'];
}
$title="Sales Performance Indicator Report: $companyname";

$salestree=make_tree_path($treeid);
$report['footertext']=$title;



BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, 0.1);
AddFreeFormText($report, $title, -1, "center", 1.5, 8);

BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0.25);
AddFreeFormText($report, $salestree, -1, "center", 2.75, 5.5);

$left = 1;
$width1 = 1;
$width2 = 3;

//PrintLocAbbr($report, $left, $width1, $width2);

$personid = $treeid;

$companyid = $mpower_companyid;

$cellfontsize=2;
$maxdays = 0;
$colcount = 0;
$IPRatio = 0;
$DPRatio = 0;

$Errors = array();
$Products = array();
$Cycles = array();
GetSalesCycles($Cycles, $companyid, $maxdays);

GetProducts($Products, $companyid);
GetDefaultRatios($companyid, $IPRatio, $DPRatio);

$level = GetLevel($personid);

$Salesmen=array();

if($level > 1) GetSalesmen($Salesmen, $personid, $Cycles, $Errors);
else GetSingleSalesman($Salesmen, $personid, $Cycles, $Errors);

$ErrCount = count($Errors);

if($ErrCount > 0)
{

//	BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, .25);
//	AddFreeFormText($report, "You must adjust Forecast Close Date for the following opportunities before they can be included in this report:", -1, "left",2.75, 5.5);

	//Print("You may not view this report until Forecast Close Date is adjusted for the following opportunities:<br><br>");
	BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0);
	AddColumn($report, "You must adjust Forecast ".GetCatLbl("Closed")." Date for the following opportunities before they can be included in this report:", "left", 4.0, 0);

	for($i=0; $i<$ErrCount; $i++)
	{
		$err = $Errors[$i];
		BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
		AddColumnText($report, $err, "left", 2.75, 5.5);
	}

}


BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0);
AddColumn($report, "Values in Thousands", "right", 10.0, 0);


	$count = count($Salesmen);

	$DoPrint=TRUE;
	if(!$count)
	{
		BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, .25);
		AddFreeFormText($report, "No subordinates found!", -1, "center", 2.75, 5.5);
	}
	else
	{

		$increment=16;
		$ccount=(int)($maxdays/30);

if(0)
{
//DDDDDUmmy settings!
		$increment=25;
		$maxdays=540;
		$ccount=(int)($maxdays/30);
}

		if($stop>$ccount) $stop=$ccount;
		if($level>1)
			PrintColTitles($report, "Users", "Products", $maxdays);
		else
			PrintColTitles($report, false, "Products", $maxdays);

		for($i=0; $i<$count; $i++)
		{
			$sales = $Salesmen[$i];
			PrintSalesmanNumbers($report, $level, $sales, $ccount, $Products, $IPRatio, $DPRatio);
			BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
			SetRowColorStandard($report, -1, SHADE_MED);
			AddColumnText($report, " ", "", 0, 0);
			$Salesmen[$i] = $sales;
		}


	}


PrintPDFReport($report, $p);
PDF_delete($p);

close_db();

?>
