function mtgPast(strD)
{
	var now = new Date();
	var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
	var d = Dates.makeDateObj(strD);
	if (today.getTime() > d.getTime())
		return true;
	return false;
}

function zapMSWindow(locid, dealid)
{
	var strID = 'div_l' + locid + 'd' + dealid;
	var divThis=document.getElementById(strID);
	if(divThis)
	{
		var tdParent = divThis.parentNode;
		tdParent.removeChild(document.getElementById(strID));
	}
}

function makeMassOps()
{
	var i;
	g_massOpps=new Array();
	var selected=false;
	for (i=0;;i++)
	{
		var check=document.getElementById("check"+i);
		if (!check) break;
			if (check.checked)
			{
				selected=true;
				var name=check.name;
				name=name.substr(5);	// the last part of the name is the dealid
				g_massOpps.push(name);
			}

	}
	if (selected) return true;
	return false;
}

function select_all_none()
{
	var selected=true;
	var i;
	for (i=0;;i++)
	{
		var check=document.getElementById("check"+i);
		if (!check) break;
			if (!check.checked)
			{
				selected=false;
				break;
			}
	}

	selected=!selected;
	for (i=0;;i++)
	{
		var check=document.getElementById("check"+i);
		if (!check) break;
			check.checked=selected;
	}

}

function cancelRevive(dealID)
{
	var data = null;
	for (var k = 0; k < g_opps.length; ++k)
	{
		if (g_opps[k][g_opps.DealID] == dealID)
		{
			data = g_opps[k];
			break;
		}
	}
	if (!data) return;

	if (confirm('Are you sure you want to cancel the scheduled auto-'+((data[g_opps.Category]==9)?'revival':'renewal')+' of '+data[g_opps.Company]+'?'))
	{
		data[g_opps.AutoReviveDate] = '';
		data[g_opps.AutoRepeatIncrement] = '';
		data.isDirty = true;
		document.getElementById('theSpreadsheet').innerHTML = make_spreadsheet();
		do_save();
	}
}

function onEditReviveDate(datestr)
{
	var data = null;
	for (var k = 0; k < g_opps.length; ++k)
	{
		if (g_opps[k][g_opps.DealID] == Windowing.dropBox.editedDealID)
		{
			data = g_opps[k];
			break;
		}
	}
	if (!data) return;
	data[g_opps.AutoReviveDate] = datestr;
	data.isDirty = true;
	document.getElementById('theSpreadsheet').innerHTML = make_spreadsheet();
}

function show_calendar(targetName)
{
	document.body.style.cursor = 'wait';
	Windowing.dropBox.calendarTarget = document.forms['form1'].elements[targetName];
	Windowing.dropBox.onCalendarEdited = onEditReviveDate;
	var win = Windowing.openSizedWindow('../shared/calendar.html', 320, 600, 'reviveDate');
	win.focus();
	document.body.style.cursor = 'auto';
}


function editAutoReviveCloseFinished()
{
	var data = Windowing.dropBox.g_opportunity;
	data.isDirty = true;
	document.getElementById('theSpreadsheet').innerHTML = make_spreadsheet();
}


function writeMouseoverCall(locid, dealid, locname, company, companyid)
{
	var html = ' onmouseover="popMSWindow(' + locid + ', ' + dealid + ', \'' + escape(locname) + '\',\'' + escape(company) + '\',' + companyid + ')" ';
	html += ' onmouseout="zapMSWindow(' + locid + ', ' + dealid + ')" ';
	return html;

}

function getSrcAbbr(idSource)
{
	for (var i=0; i<g_sources.length; i++)
	{
		if(idSource == g_sources[i][g_sources.SourceID]) return g_sources[i][g_sources.Abbr];
	}
	return '';
}


function gbload() {
	setTimeout("parent.gbload()", 1000);
}

function getPeopleArray(userlogedinID) {
	var user = g_user;
		userlogedinID = user[0];	

	var ret = Array();
	for (var k = 0; k < g_people.length; k++) {
		var data = g_people[k];

		if (data[g_people.IsSalesperson] != '1' || data[g_people.SupervisorID] != userlogedinID[user.PersonID]) 
			continue;
		var tmp = Array();
		tmp[0] = data[g_people.PersonID];
		tmp[1] = data[g_people.LastName] + ', ' + data[g_people.FirstName];
		ret[ret.length] = tmp;
	}
	return ret;
}

function Reassign()
{
	var personID=ssSalesPerson.getSelectedVal();
	if (personID==-1)
	{
		alert("Select a new salesman.");
		return;
	}
	if(!makeMassOps())
	{
		alert("No opportunities selected.");
		return;
	}

	do_reassign(personID);
}


function printSpreadsheet()
{
	Header.hideButton('Print');
	window.print();
	Header.showButton('Print');
}

function edit_one(id)
{
	document.body.style.cursor = 'wait';
	do_edit(id);
	document.body.style.cursor = 'auto';
}

function find_opportunity(id)
{	
	for (var i = 0; i < g_opps.length; ++i)
	if (g_opps[i][g_opps.DealID] == id) return g_opps[i];

	return null;
}


function sort(fieldName)
{
	document.body.style.cursor = 'wait';

	if (g_sortField == fieldName) g_sortDescending = !g_sortDescending;
	else g_sortDescending = false;

	g_sortField = fieldName;
	document.getElementById('theSpreadsheet').innerHTML = make_spreadsheet();
	document.body.style.cursor = 'auto';

	if(g_mass_move)	initSPSelect(g_personID);
}

function compare(a, b)
{
	var aa = 0;
	var bb = 0;

	if (Spreadsheet.m_sortField == 'RemoveAndCloseDate')
	{
		if (a[g_opps.Category] == 6)
		{
			if (a[g_opps.ActualCloseDate] == '')
				aa = Number.NEGATIVE_INFINITY;
			else
				aa = Dates.makeDateObj(a[g_opps.ActualCloseDate]).getTime();
		}
		else if (a[g_opps.Category] == 9)
		{
			if (a[g_opps.RemoveDate] == '')
				aa = Number.NEGATIVE_INFINITY;
			else
				aa = Dates.makeDateObj(a[g_opps.RemoveDate]).getTime();
		}
		if (b[g_opps.Category] == 6)
		{
			if (b[g_opps.ActualCloseDate] == '')
				bb = Number.NEGATIVE_INFINITY;
			else
				bb = Dates.makeDateObj(b[g_opps.ActualCloseDate]).getTime();
		}
		else if (b[g_opps.Category] == 9)
		{
			if (b[g_opps.RemoveDate] == '')
				bb = Number.NEGATIVE_INFINITY;
			else
				bb = Dates.makeDateObj(b[g_opps.RemoveDate]).getTime();
		}
	}
	else if (Spreadsheet.m_sortField == 'milestones')
	{
		for (var i = 0; i < m_milestones.length; ++i)
		{
			var idx = m_milestones[i];
			var fld = g_opps[idx];
			if (a[fld] == '1') ++aa;
			if (b[fld] == '1') ++bb;
		}
	}
	else if (Spreadsheet.m_sortField == 'Src2Abbr')
	{
		aa = getSrc2Abbr( a[g_opps.Source2ID]);
		bb = getSrc2Abbr( b[g_opps.Source2ID]);

	}
	else if (Spreadsheet.m_sortField == 'SrcAbbr')
	{
		aa = getSrcAbbr( a[g_opps.SourceID]);
		bb = getSrcAbbr( b[g_opps.SourceID]);

	}
	else if (Spreadsheet.m_sortField == 'DaysOld')
	{
		var targetDate = Dates.mssql2us(a[g_opps.TargetDate]);
		var space = targetDate.indexOf(' ');
		if (space > 1) targetDate = targetDate.substr(0, space);
		var tdRaw = Dates.makeDateObj(targetDate);
		targetDate = Dates.formatShortDate(tdRaw);
		var now = new Date();
		aa = Math.round((now - tdRaw) / 86400000);

		targetDate = Dates.mssql2us(b[g_opps.TargetDate]);
		space = targetDate.indexOf(' ');
		if (space > 1) targetDate = targetDate.substr(0, space);
		tdRaw = Dates.makeDateObj(targetDate);
		targetDate = Dates.formatShortDate(tdRaw);
		bb = Math.round((now - tdRaw) / 86400000);

	}
	else if (Spreadsheet.m_sortField == g_opps.ProductID)
	{
		aa = find_product(a[Spreadsheet.m_sortField]);
		bb = find_product(b[Spreadsheet.m_sortField]);
		if (aa) aa = aa[g_products.Name];
		else aa = '';
		if (bb) bb = bb[g_products.Name];
		else bb = '';
	}
	else if (Spreadsheet.m_sortField == g_opps.EstimatedDollarAmount ||
		Spreadsheet.m_sortField == g_opps.ActualDollarAmount)
	{
		aa = parseInt(a[Spreadsheet.m_sortField], 10);
		bb = parseInt(b[Spreadsheet.m_sortField], 10);
		if (isNaN(aa)) aa = 0;
		if (isNaN(bb)) bb = 0;
	}
	else if (Spreadsheet.m_sortField == g_opps.FirstMeeting ||
		Spreadsheet.m_sortField == g_opps.NextMeeting ||
		Spreadsheet.m_sortField == g_opps.ActualCloseDate ||
		Spreadsheet.m_sortField == g_opps.AutoReviveDate)
	{
		if (a[Spreadsheet.m_sortField] == '')
			aa = Number.NEGATIVE_INFINITY;
		else
			aa = Dates.makeDateObj(a[Spreadsheet.m_sortField]).getTime();

		if (b[Spreadsheet.m_sortField] == '')
			bb = Number.NEGATIVE_INFINITY;
		else
			bb = Dates.makeDateObj(b[Spreadsheet.m_sortField]).getTime();

		if (isNaN(aa)) aa = Number.NEGATIVE_INFINITY;
		if (isNaN(bb)) bb = Number.NEGATIVE_INFINITY;
	}
	else if (Spreadsheet.m_sortField == g_opps.Company)
	{
		var str1 =  a[Spreadsheet.m_sortField];
		var str2 =  b[Spreadsheet.m_sortField];
		aa = str1.toUpperCase();
		bb = str2.toUpperCase();
	}
	else
	{
		aa = a[Spreadsheet.m_sortField];
		bb = b[Spreadsheet.m_sortField];
	}

	if (Spreadsheet.m_sortField==g_opps.Category)
	{
		if (aa==10) aa='0';
		if (bb==10) bb='0';
	}

	if (Spreadsheet.m_sortDescending)
	{
		if (aa < bb) return 1;
		if (aa > bb) return -1;
		return 0;
	}
	else
	{
		if (aa < bb) return -1;
		if (aa > bb) return 1;
		return 0;
	}
}

function doReviveNow(dealID)
{
	var data = null;
	for (var k = 0; k < g_opps.length; ++k)
	{
		if (g_opps[k][g_opps.DealID] == dealID)
		{
			data = g_opps[k];
			break;
		}
	}
	if (!data)
		return;
	if (confirm('Are you sure you want to '+((data[g_opps.Category]==9)?g_company[0][g_company.ReviveLabel]:g_company[0][g_company.RenewLabel])+' '+data[g_opps.Company]+' now?'))
	{
		data[g_opps.AutoReviveDate] = Dates.formatShortDate(new Date());
		data.isDirty = true;
		do_save();
	}
}
function makeBody(personID, category, sortField, sortDescending)
{	
	calcOppBorders();
//For g_showaffiliates:  Need to filter submilestones by companyid of the person being displayed
	var companyid = g_the_only_company[g_company.CompanyID];
	if(g_showaffiliates)
	{
		for (var i=0; i< g_people.length; i++)
		{
			if(g_people[i][g_people.PersonID] == personID)
			{
				companyid = g_people[i][g_people.CompanyID];
				break;
			}
		}
	}
	if (category == -1)
		g_mass_move = true;
	else
		g_mass_move = false;
	if (category == -2)
		g_auto_revive = true;
	else
		g_auto_revive = false;

	var opps = new Array();
	for (var i = 0; i < g_opps.length; ++i)
	{
		var data = g_opps[i];
		if (data[g_opps.PersonID] != personID)
			continue;
		if (category == -2)
		{
			if (data[g_opps.AutoReviveDate] == '' || (data[g_opps.Category] != 6 && data[g_opps.Category] != 9)) continue;
		}
		else
		{
			if (data[g_opps.Category] != category && category != -1)
				continue;
		}
		opps[opps.length] = data;
	}

	if (sortField)
	{
		if (sortField == 'milestones' || sortField == 'SrcAbbr' || sortField == 'Src2Abbr' || sortField == 'DaysOld' || sortField == 'RemoveAndCloseDate')
			Spreadsheet.m_sortField = sortField;
		else
			Spreadsheet.m_sortField = g_opps[sortField];
		Spreadsheet.m_sortDescending = sortDescending;
		opps.sort(compare);
	}

	Spreadsheet.dollarTotal = 0;
	var output = '';
	var row_num = 0;
	for (var i = 0; i < opps.length; ++i)
	{	
		var data = opps[i];
        var row_output = makeRow(data, row_num, companyid);
        if (row_output != '') {
    		output += '<div id="div' + data[g_opps.DealID] + '">';
    		output += row_output;
    		output += '</div>';
            row_num += 1;
        }
	}
	if (opps.length > 0 && category != 10 && category != 1 && !g_auto_revive)
	{
		var colbefore = 9;
		var colafter = 7;
		if (category == -1)
			colbefore = 9;
		if (!g_source2_used)
			colbefore--;
		if (!g_source_used)
			colbefore--;
		if (g_company[0][g_company.Requirement1used] != '1')
			colafter--;
		if (g_company[0][g_company.Requirement2used] != '1')
			colafter--;
		output += '<tr class="spreadsheet" style="background-color:#F1F1FF;">';
		for (var k = 0; k < colbefore; ++k)
			output += '<td style="border-top:2px double silver;">&nbsp;</td>';
		output += '<td style="border-top:2px double silver; font-weight:bold;" align="right">'+format_money(Spreadsheet.dollarTotal)+'</td>';
		for (var k = 0; k < colafter; ++k)
			output += '<td style="border-top:2px double silver;">&nbsp;</td>';
		output += '</tr>';
	}

	return output;
}
function translateAutoIncrement(str)
{
	str = str.replace(/\+/g, '');
	str = str.replace('1 ', '');
	if (str == '3 months')
		str = 'quarter';
	return str;
}

function getSrc2Abbr(idSource2)
{

	for (var i=0; i<g_sources2.length; i++)
	{
		if(idSource2 == g_sources2[i][g_sources2.Source2ID])
			return g_sources2[i][g_sources2.Abbr];
	}
	return '';
}

function makeRow(data, rowcnt, companyid)
{	
	var bVal = g_valuation_used;
	var bSrc = g_source_used;
	var bSrc2 = g_source2_used;
	var bTarg = (data[g_opps.Category] == '10');

	if (data[g_opps.Category] == '9' && !g_auto_revive) {
		return '';
	}
	var id = data[g_opps.DealID];

	var targetDate;
	var daysOld;
	targetDate = Dates.mssql2us(data[g_opps.TargetDate]);
    daysOld = getDaysOld(targetDate);
	var space = targetDate.indexOf(' ');
	if (space > 1) targetDate = targetDate.substr(0, space);
	tdRaw = mkShortDate(targetDate);
	targetDate = Dates.formatShortDate(tdRaw);
	var now = new Date();
//	daysOld = Math.round((now - tdRaw) / 86400000);
	var firstMeeting = Dates.mssql2us(data[g_opps.FirstMeeting]);
	var space = firstMeeting.indexOf(' ');
	if (space > 1) firstMeeting = firstMeeting.substr(0, space);
	firstMeeting = Dates.formatShortDate(Dates.makeDateObj(firstMeeting));

	var nextMeeting;
	if (data[g_opps.Category] == '6')
		nextMeeting = Dates.mssql2us(data[g_opps.ActualCloseDate]);
	else
	{
		var nextMeeting = Dates.mssql2us(data[g_opps.NextMeeting]);
		var space = nextMeeting.indexOf(' ');
		if (space > 1) nextMeeting = nextMeeting.substr(0, space);
	}
	nextMeeting = Dates.formatShortDate(Dates.makeDateObj(nextMeeting));

    if (data[g_opps.Category] == '6') {
        var daysSinceFM = getDaysDiff(nextMeeting, firstMeeting);
    }
    else {
        var daysSinceFM = getDaysOld(firstMeeting);
    }


	var oppID = data[g_opps.ProductID];
	var oppDesc = getOfferingsList(data[g_opps.DealID]);
	var dollarAmount = data[g_opps.ActualDollarAmount];
	if (dollarAmount == '0' || dollarAmount == 0 || dollarAmount == '')
		dollarAmount = data[g_opps.EstimatedDollarAmount];
	if (dollarAmount != '0' && dollarAmount != 0 && dollarAmount != '')
		Spreadsheet.dollarTotal += parseInt(dollarAmount);
	dollarAmount = format_money(dollarAmount);

	var statusColor = 'ivory';
	var statusChar = '&nbsp;';

	if (data[g_opps.Category] == '1')
	{
		var now = new Date();
		var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		var firstMtgDate = Dates.makeDateObj(Dates.mssql2us(data[g_opps.FirstMeeting]));
	}
	else if (data[g_opps.Category] != '6' && data.border)
	{
		statusColor = data.border;
		if (data.border=="red")
			statusChar='R';
		else statusChar = 'Y';
	}

	var output = '<tr class="spreadsheet" height=25 id="' + id + '">';
	var vLevel = data[g_opps.VLevel] > -1 ? data[g_opps.VLevel] : '';
	var srcAbbr = getSrcAbbr( data[g_opps.SourceID]);
	var src2Abbr = getSrc2Abbr( data[g_opps.Source2ID]);
	if (g_auto_revive)
	{	
		output += '<td width="1" align="center"><nobr>';
		var templabel = ((data[g_opps.Category]==9)?g_company[0][g_company.ReviveLabel]:g_company[0][g_company.RenewLabel]);
		output += '<input type="button" style="width:90px;" value="'+templabel+' Now" onclick="doReviveNow('+data[g_opps.DealID]+')"></input>';
		output += '<input type="button" value="Cancel" onclick="cancelRevive('+data[g_opps.DealID]+')"></input>';
		output += '</nobr></td>';
	}
	if(g_alias_fullrights && !g_mass_move && !g_auto_revive)
	{
		output += '<td width=30 align="center"><a href="javascript:edit_one(\'i' + id + '\')">';
		output += '<img border=0 src="../images/go.gif" alt="Edit Opportunity"></a></td>';
	}
//CHECKBOXES
	if (g_mass_move)
	{
		output+= '<td width=20 align="center"><input type=checkbox id="check'+rowcnt+'" name="check'+id+'"';
		if (!(!Spreadsheet.checkedRows['check'+id]))
			output+=' checked';
		output+='></td>';
		output+= '<td width=20 align="center">' + targetDate + '</td>';
	}
	if((data[g_opps.Category] != '10' || g_mass_move) && !g_auto_revive)

	{
		output += '<td width=10 align="center" bgcolor="' + statusColor + '"><b>' + statusChar + '</b></td>';
	}
//CATEGORIES
	if (g_mass_move)
	{
		output+= '<td width=20>' + getCatAbbr(data[g_opps.Category]) + '</td>';
	}

	var strCompany =  data[g_opps.Company];
	if(data[g_opps.Division].length)
		strCompany += '<br>' + data[g_opps.Division];
	if(data[g_opps.Contact].length)
		strCompany += '<br>' + data[g_opps.Contact];
	output += '<td '+ ((bTarg || g_auto_revive)? '' : 'width="115"')+'>' + strCompany + '</td>';

	if (g_auto_revive)
	{	
		output += '<td width=35 style="text-align: center">';
		output += getCatAbbr(data[g_opps.Category]);
		output += '</td>';
	}


	if(bVal)
	{
		output += '<td width=35 style="text-align: center">' + vLevel + '</td>';
	}
//	if(bSrc && bTarg) source should be visible all the time 2/23/2005
	if(bSrc2)
	{
		output += '<td width=35>' + src2Abbr + '</td>';

	}
	if(bSrc)
	{
		output += '<td width=35>' + srcAbbr + '</td>';

	}
	if(bTarg)
	{
		if(!g_mass_move)
		{
			output += '<td align="right">' + targetDate + '</td>';
			output += '<td align="right">' + daysOld + '</td>';
		}
		else
		{
			for (var i=0; i<8; i++)
				output += '<td></td>';
			output += '</tr>';
		}
	}
	else
	{
		if (!g_auto_revive)
		{	
			var cat = data[g_opps.Category];
			if(cat == 1 && mtgPast(Dates.mssql2us(data[g_opps.FirstMeeting])))
				output += '<td width=55 bgcolor="black" style="color:white" align="right">' + firstMeeting + '</td>';
			else
				output += '<td width=55 align="right">' + firstMeeting + '</td>';
			if(!g_mass_move && cat > 1 && cat < 10)
				output += '<td width=55 align="center">' + daysSinceFM + '</td>';

			output += '<td>' + oppDesc + '</td>';
			output += '<td width=62 align="right">' + dollarAmount + '</td>';

			var msNames = new Array();
			var msCols = new Array();
			var msLocs = new Array();
			if (g_the_only_company[g_company.Requirement1used] == '1')
			{
				msNames[msNames.length] = g_company[0][g_company.Requirement1];
				msCols[msCols.length] = g_opps.Requirement1;
				msLocs[msLocs.length] = 0;
			}

			msNames[msNames.length] = (g_company[0][g_company.MS2Label] == '') ? 'Person' : g_company[0][g_company.MS2Label];
			msCols[msCols.length] = g_opps.Person;
			msLocs[msLocs.length] = 1;
			msNames[msNames.length] = (g_company[0][g_company.MS3Label] == '') ? 'Need' :  g_company[0][g_company.MS3Label];
			msCols[msCols.length] = g_opps.Need;
			msLocs[msLocs.length] = 2;
			msNames[msNames.length] =  (g_company[0][g_company.MS4Label] == '') ? 'Money' :  g_company[0][g_company.MS4Label];
			msCols[msCols.length] = g_opps.Money;
			msLocs[msLocs.length] = 3;
			msNames[msNames.length] =  (g_company[0][g_company.MS5Label] == '') ? 'Time' :  g_company[0][g_company.MS5Label];
			msCols[msCols.length] = g_opps.Time;
			msLocs[msLocs.length] = 4;

			if (g_company[0][g_company.Requirement2used] == '1')
			{
				msNames[msNames.length] = g_company[0][g_company.Requirement2];
				msCols[msCols.length] = g_opps.Requirement2;
				msLocs[msLocs.length] = 5;

			}

			for (var i = 0; i < msCols.length; ++i)
			{
				var col = msCols[i];
	//Show rollover even if milestone is completed because of new data stored
				if (data[col] == '1')
					output += '<td width=40 align="center" class="milestone_yes_small" style="border: thick solid black"';
				else
				{
					output += '<td width=45 align="center" class="milestone_no_small" ';
				}

				output += ' id="td_l' + msLocs[i] + 'd' + data[g_opps.DealID] + '" ';
				output += writeMouseoverCall(msLocs[i], data[g_opps.DealID], msNames[i], data[g_opps.Company], companyid);
				output += '> ';
				output += msNames[i] + '</td>';
			}
			if(cat !=1 && cat != 6 && mtgPast(Dates.mssql2us(data[g_opps.NextMeeting])))
				output += '<td width=55  bgcolor="black" style="color:white" align="right">' + nextMeeting + '</td>';
			else
				output += '<td width=55 align="right">' + nextMeeting + '</td>';
		}
		else
		{
			output += '<td width=80 align="right" nowrap>';
			if (data[g_opps.Category] == 6)
				output += Dates.formatShortDate(Dates.makeDateObj(data[g_opps.ActualCloseDate]))
			else
				output += Dates.formatShortDate(Dates.makeDateObj(data[g_opps.RemoveDate]))
			output += '</td>';
			
			output += '<td width=130 align="right" nowrap><nobr><input type="text" style="text-align:right; border:none; background-color:transparent; font-family:Arial; font-size:10pt;" size="10" readonly name="textAutoReviveDate'+data[g_opps.DealID]+'" value="'+Dates.formatShortDate(Dates.makeDateObj(data[g_opps.AutoReviveDate]))+'"></input>';
			if (data[g_opps.Category] == 6)
				output += '&nbsp;&nbsp;<input type="button" name="autoDateCal" class="etc-button" onclick="editAutoReviveClose('+data[g_opps.DealID]+');"></nobr>';
			else
				output += '&nbsp;&nbsp;<input type="button" name="autoDateCal" class="etc-button" onclick="Windowing.dropBox.editedDealID = '+data[g_opps.DealID]+'; show_calendar(\'textAutoReviveDate'+data[g_opps.DealID]+'\')"></nobr>';
			if (data[g_opps.AutoRepeatIncrement] != '')
				output += '<br>Repeats every '+translateAutoIncrement(data[g_opps.AutoRepeatIncrement]);
			output += '</td>';
		}
	}
	output += '</tr>';
	return output;
}



function make_spreadsheet()
{
	var bTarg = (g_category == 10);
	var bVal = g_valuation_used;
	var bSrc = g_source_used;
	var bSrc2 = g_source2_used;

	var output;
	if (!bTarg)
	{
		output='<table border=0 width="100%"><tr width="100%" style="font-size:16; font-family:Arial; font-weight:bold"><td>! Key:&nbsp;</td><td>';
		if (g_category == '1')
		{
			output+='! = Missed ' + catLabel('First Meeting');
		}
		else
		{
			output+='&nbsp;&nbsp;Y = yellow alert on board';
			output+='&nbsp;&nbsp;R = red alert on board';
		}
		output+='</td>';
		if(g_mass_move)
		{
			output += '<td align="right"><b>New Salesperson:&nbsp;';
			output += '<input type="text" id="select_salesperson" style="width: 150px"></td>';
		}

		output += '</tr></table><br>';
	}
	else
		output = '';
	output += '<div id="spreadsheetdiv"><table id="thetable" border=0 bgcolor="silver" cellspacing=1 cellpadding=2 width="100%">';
	
	output += '<tr onselectstart="return false;" align="center" height=30>';
	if (g_auto_revive) {
		output += '<td class="command raised" style="cursor: default;">Actions</td>';
	}
	if(g_alias_fullrights && !g_mass_move && !g_auto_revive)
	{
		output += '<td class="command raised" style="cursor: default;">Edit</td>';
	}
// check column
	if (g_mass_move)
	{
		output += '<td class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this);select_all_none();return false;" style="cursor: default;font-size:9px">All/<br>None</td>';
		output += '<td class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this);select_all_none();return false;" style="cursor: default;font-size:9px">Date<br>Assigned</td>';
	}
	if (!bTarg && !g_auto_revive)
		output += '<td class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this); sort(\'FirstMeeting\');return false;" style="cursor: default;">!</td>';
// category column
	if (g_mass_move)
	{
		output += '<td class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this);sort(\'Category\');return false;" style="cursor: default;">Cat</td>';
	}
	output += '<td class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this); sort(\'Company\');return false;" style="cursor: default;">Company</td>';
	if (g_auto_revive) {		
		output += '<td class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this); sort(\'Category\');return false;" style="cursor: default;">Type</td>';
	}
//New VLevel column
	if (bVal)
	{
		output += '<td nowrap class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this); sort(\'VLevel\');return false;" style="cursor: default;">'+g_company[0][g_company.ValAbbr]+'</td>';
	}
	if (bSrc2)
	{
		output += '<td nowrap class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this); sort(\'Src2Abbr\');return false;" style="cursor: default;">'+g_company[0][g_company.Source2Abbr]+'</td>';

	}
	if (bSrc)	//Source abbrev should be visible all the time 2/23/2005
	{
		output += '<td nowrap class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this); sort(\'SrcAbbr\');return false;" style="cursor: default;">Src</td>';

	}
	if (bTarg)
	{
		output += '<td nowrap class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this); sort(\'TargetDate\');return false;" style="cursor: default;">Date Assigned</td>';
		output += '<td class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this); sort(\'DaysOld\');return false;" style="cursor: default;">Days Old</td>';
	}
	else
	{
		if (!g_auto_revive)
			output += '<td nowrap class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this); sort(\'FirstMeeting\');return false;" style="cursor: default;">' + catLabel('FM') + '</td>';
//New 'days since FM col'
		if(g_category > 1 && g_category < 10)
			output += '<td nowrap class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this); sort(\'FirstMeeting\');return false;" style="cursor: default;">Days<br>since<br>' + catLabel('FM') + '</td>';

		if (!g_auto_revive)
		{
			output += '<td class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this); sort(\'ProductID\');return false;" style="cursor: default;">Offering</td>';
			if (g_category == 6)
				output += '<td class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this); sort(\'ActualDollarAmount\');return false;" style="cursor: default;">' + g_amount_name + '</td>';
			else
				output += '<td class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this); sort(\'EstimatedDollarAmount\');return false;" style="cursor: default;">' + g_amount_name + '</td>';

			var cspan = 4;

			if (g_company[0][g_company.Requirement1used] == '1')
				++cspan;
			if (g_company[0][g_company.Requirement2used] == '1')
				++cspan;

			output += '<td class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this); sort(\'milestones\');return false;" style="cursor: default;" colspan=' + cspan + '>Milestones</td>';

			if (g_category == 6)
				output += '<td class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this); sort(\'ActualCloseDate\');return false;" style="cursor: default;">Closed</td>';
			else
				output += '<td nowrap class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this); sort(\'NextMeeting\');return false;" style="cursor: default;">Next<br>Mtg</td>';
		}
		else
		{
			output += '<td class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this); sort(\'RemoveAndCloseDate\');return false;" style="cursor: default;">Date '+catLabel('Closed')+' or '+catLabel('Removed')+'</td>';
			output += '<td class="command raised" onmousedown="press_fake_button(this);return false;" onmouseup="lift_fake_button(this); sort(\'AutoReviveDate\');return false;" style="cursor: default;">'+g_company[0][g_company.ReviveLabel]+'/'+g_company[0][g_company.RenewLabel]+' Date</td>';
		}

	}
	output += '</tr>';

	if (g_mass_move)
	{
		var checkedRows = new Object();
		var elem = null;
		var k = 0;
		while (elem = document.getElementById('check'+(k++)))
			checkedRows[elem.name] = elem.checked;
		Spreadsheet.checkedRows = checkedRows;
	}

	var tempcat = g_category;
	if (g_mass_move)
		tempcat = -1;
	else if (g_auto_revive)
		tempcat = -2;
	output += makeBody(g_personID, tempcat, g_sortField, g_sortDescending);
	output += '</table></div>';

	return output;
}

function writeTooltipDiv(locid, dealid, locname, company, companyid)
{

	var idParent = 'td_l' + locid + 'd' + dealid;
	var tdParent = document.getElementById(idParent);

	var divThis = document.createElement("DIV");
	tdParent.appendChild(divThis);
	divThis.id = 'div_l' + locid + 'd' + dealid;
	divThis.className="tooltip";

	var html = '<table class="tooltip_header" width="100%">';

	html += '<tr><td align="center"><b>' + unescape(company) + '</b></td></tr>';
	html += '<tr><td align="center"><b>' + unescape(locname) + '</b></td></tr></table>';
	if (locid == 9) {
		html += writeOffTable(locid, dealid, companyid);
	} else {
		html += writeMSTable(locid, dealid, companyid);
	}
	divThis.innerHTML = html;

	return divThis;
}

function writeOffTable(locid, dealid, companyid) {
	var idTab = 'tab_l' + locid + 'd' + dealid;
	var html = '<table id="' + idTab + '" class="mstable" width=100%>';

	html += '<tr><th>Description</th><th>Value</th><th>Qty</th><th>Extension</th></tr>';
	var total = 0;
	for (var k = 0; k < g_opp_products_xref.length; ++k) {
		if (g_opp_products_xref[k][g_opp_products_xref.Deleted] == '1') continue;
		if (g_opp_products_xref[k][g_opp_products_xref.DealID] == dealid) {
			var val = (g_opp_products_xref[k][g_opp_products_xref.Val] == '') ? 0 : g_opp_products_xref[k][g_opp_products_xref.Val];
			var qty = (g_opp_products_xref[k][g_opp_products_xref.Qty] == '') ? 0 : g_opp_products_xref[k][g_opp_products_xref.Qty];
			var ext = val * qty;
			for (l = 1; l < g_products.length; ++l) {
				if (g_products[l][g_products.ProductID] == g_opp_products_xref[k][g_opp_products_xref.ProductID]) {
					var prod_name = g_products[l][g_products.Name];
					break;
				}
			}
			
			total += ext;
			html += '<tr><td>' + prod_name + '</td>';
			html += '<td align=right>' + format_money(val) + '</td>';
			html += '<td align=right>' + qty + '</td>';
			html += '<td align=right>' + format_money(ext) + '</td></tr>';
		}
	}
	html += '<tr><td colspan=3 align=right><b>Total:</b></td><td align=right><b>' + format_money(total) + '</b></td></tr>';
	html += '</table>';

	return html;
}

function writeMSTable(locid, dealid, companyid) //f8f8fb
{
	var idTab = 'tab_l' + locid + 'd' + dealid;
	var html = '<table id="' + idTab + '" class="mstable">';

	var oppCompanyID = null;
	for (var k = 0; k < g_opps.length; ++k)
	{
		if (g_opps[k][g_opps.DealID] == dealid)
		{
			oppCompanyID = g_opps[k][g_opps.CompanyID];
			break;
		}
	}

	for (var k = 0; k < g_submilestones.length; ++k)
	{
		if (g_submilestones[k][g_submilestones.Enable] != '1') continue;
		if (g_submilestones[k][g_submilestones.CompanyID] != oppCompanyID) continue;
		if (g_submilestones[k][g_submilestones.Location] == locid)
		{
			var tempid = 'sub' + g_submilestones[k][g_submilestones.SubID];
			if(g_submilestones[k][g_submilestones.IsHeading] == '1')
			{
				html += '<td colspan=2 align="center" style="font-weight:bold;font-style:italic">' + g_submilestones[k][g_submilestones.Prompt] + '</td></tr>';
			}
			else
			{
				html += '<tr><td style="width:60%" align="left">' + g_submilestones[k][g_submilestones.Prompt];
				if (g_submilestones[k][g_submilestones.Mandatory] != '1')
					html += '(optional)';
				html += '</td>';

				html += '<td align="left">';
				if (g_submilestones[k][g_submilestones.CheckBox] == '1')
				{

					html += '<input type="checkbox" id="' + tempid + '" style="font-size: 12px;height: 16px" ';
					if ('1'==getSubAnswer_Disp(dealid, g_submilestones[k][g_submilestones.SubID]))
					{
						html +=' checked ';
					}
					html += ' readOnly="true" ';

				}
				else if (g_submilestones[k][g_submilestones.YesNo] == '1')
				{
					var ans = getSubAnswer_Disp(dealid, g_submilestones[k][g_submilestones.SubID]);
					var disp = "&nbsp&nbsp&nbsp&nbsp&nbsp";
					switch(ans)
					{
						case '0': disp = "No"; break;
						case '1': disp = "Yes"; break;
						case '2': disp = "N/A";
					}
					html += '<select><option>' + disp + '</select';
				}
				else if (g_submilestones[k][g_submilestones.DropDown] == '1')
				{
					var ans = getSubAnswer_Disp(dealid, g_submilestones[k][g_submilestones.SubID]);
					if(ans == "") ans = "&nbsp&nbsp&nbsp&nbsp&nbsp";
					html += '<select><option>' + ans + '</select';
				}
				else
				{
					var disp = unescape(getSubAnswer_Disp(dealid, g_submilestones[k][g_submilestones.SubID]));
					if(disp == "") disp = "&nbsp&nbsp&nbsp&nbsp&nbsp";
					html += '<input type="text" value="' + disp + '"';
				}
//Prompt is common to all types
				html += '></td></tr>';
			}
		}
	}

	html += '</table>';
	return html;
}

function setSubAnswer(DealID, SubID, Answer, CompletedLocation)
{
	//	No longer adequate!
	//	g_editedSubAnswers[g_editedSubAnswers.length] = new Array(DealID, SubID, Answer);
	var bFound = 0;
	for(var i = 0; i< g_editedSubAnswers.length; ++i)
	{
		if (g_editedSubAnswers[i][0] == DealID && g_editedSubAnswers[i][1] == SubID)
		{
			bFound = 1;
			g_editedSubAnswers[i][2] = Answer;
			g_editedSubAnswers[i][3] = CompletedLocation;
		}
	}
	if(!bFound);
	g_editedSubAnswers[g_editedSubAnswers.length] = new Array(DealID, SubID, Answer, CompletedLocation);
}

function getSubAnswer_Disp(dealid, subid)
{

//First, look through the array of unsaved, edited answers
	for (var i = 0; i < g_editedSubAnswers.length; ++i)
	{
		if(g_editedSubAnswers[i][0]*1 == dealid*1 && g_editedSubAnswers[i][1]*1 == subid*1)
		{
			return g_editedSubAnswers[i][2];
		}
	}
//If not found, look through the array of retrieved answers from the DB
	for (var i = 0; i < g_subanswers_disp.length; ++i)
	{
		if(g_subanswers_disp[i][0]*1 == dealid*1 && g_subanswers_disp[i][1]*1 == subid*1)
			return g_subanswers_disp[i][2];
	}
	return "";
}

function setSpreadsheetSubAnswer_Disp(DealID, SubID, Answer)
{

	var bFoundIt = 0;
	for (i=0; i< g_subanswers_disp.length; i++)
	{
		if(DealID > g_subanswers_disp[i][0]) continue;
		if(DealID == g_subanswers_disp[i][0])
		{
			if (SubID == g_subanswers_disp[i][1])
			{
				g_subanswers_disp[i][2] = Answer;
				bFoundIt = 1;
				break;
			}
		}
		else break;

	}
	if(!bFoundIt)
	{
		g_subanswers_disp[g_subanswers_disp.length] = new Array(DealID, SubID, Answer);
	}

}


//Milestone tooltip window

function popMSWindow(locid, dealid, locname, company, companyid)
{

	var tip = writeTooltipDiv(locid, dealid, locname, company, companyid);
	var tdMS = tip.parentNode;
	var strMSTab = 'tab_l' +  locid + 'd' + dealid;
	var tabMS = document.getElementById(strMSTab);
	var SS = document.getElementById('theSpreadsheet');
	var maxHeight = SS.clientHeight;
	var maxWidth = SS.clientWidth;

	var tdX = absLeft(tdMS);
	var tdY = absTop(tdMS);

//Set the tooltip top, left

	tip.style.height = tabMS.clientHeight + tabMS.offsetTop;
	var testTop = tdY + 50;
	var testLeft = tdX - (tip.clientWidth / 2);

//Instead, just put it off to the left:
//If tip window height is greater than half the height of the spreadsheet, position the tip window
//at the very top

	testTop = tdY - (tip.clientHeight / 2);
    offset = 10;  // This value is to offset uncounted pixels in the tool tip window height
    if (testTop + tip.clientHeight + offset > document.body.clientHeight) {
        //alert(testTop + " " + tip.clientHeight + " " + document.body.clientHeight);
        testTop = document.body.clientHeight - tip.clientHeight - offset;
    }    
    if (testTop < 0) testTop = 0;

    testLeft = tdX - (tip.clientWidth + 50);
    var divTop = document.getElementById('spreadsheetdiv').scrollTop;

    tip.style.left = testLeft;
    tip.style.top = testTop - divTop;
    
    tip.style.visibility = 'visible';
}


function absTop(obj)
{
	var par = obj.offsetParent;
	if (par == null)
		return obj.offsetTop;
	else
		return obj.offsetTop + absTop(par);
}

function absLeft(obj)
{
	var par = obj.offsetParent;
	if (par == null)
		return obj.offsetLeft;
	else
		return obj.offsetLeft + absLeft(par);
}

function calcLeft(obj)
{
	var x = absLeft(obj);
	return x;
}

function calcTop(obj)
{
	var y = absTop(obj);
	return y;
}

function getMousePosX()
{
	return tempX;
}

function getMousePosY()
{
	return tempY;
}
function find_opportunity(id)
{
	for (var i = 0; i < g_opps.length; ++i)
	if (g_opps[i][g_opps.DealID] == id) return g_opps[i];

	return null;
}

function do_save()
{
	g_saver = new Saver();
	g_saver.open();
	save_table(g_opps, g_opps.DealID, true, '\012', saved_new_opps, '../data/insert_opps.php');
}

function get_salesperson_info(id)
{
	var ret = new Object();

	var monthsNew = g_company[0][g_company.MonthsConsideredNew];
	for (var i = 0; i < g_people.length; ++i)
	{
		if (g_people[i][g_people.PersonID] != id) continue;
		ret.rating = g_people[i][g_people.Rating];
		ret.color = g_people[i][g_people.Color];
		ret.name = g_people[i][g_people.FirstName] + ' ' + g_people[i][g_people.LastName];
		ret.rawdata = g_people[i];

		var expDate = new Date(Dates.mssql2us(g_people[i][g_people.StartDate]));
		for (; monthsNew>0; --monthsNew)
		{
			var m = expDate.getMonth();
			if (m == 11)
			{
				expDate.setMonth(0);
				expDate.setYear(expDate.getYear() + 1);
			}
			else expDate.setMonth(m + 1);
		}
		if (expDate.getTime() > new Date().getTime())
		{
			ret.tenure = 'New';
			if (g_user[0][g_user.Level] > 1) ret.name += '&nbsp;(n)';
		} else ret.tenure = 'Experienced';

		ret.analysisID = g_people[i][g_people.AnalysisID];
		ret.PersonID = g_people[i][g_people.PersonID];
		ret.CompanyID= g_people[i][g_people.CompanyID];
		break;
	}

	return ret;
}

function update_from_edit_window(opp, oldCategory, oldPersonID)
{	
	var skip_calcs=false;
	if(arguments.length > 3)
	skip_calcs=('skip_calcs' == arguments[3]);
	document.body.style.cursor = 'wait';

	opp.isDirty = true;

	var dealID = opp[g_opps.DealID];
	if ((dealID < 0 && !opp.added) || oldCategory == 9)
	{
		// add a new row & get a new deal ID
		//if (dealID == -1)
		//	dealID = --g_newDealID;
		opp[g_opps.DealID] = dealID;
		g_opps[g_opps.length] = opp;
		var info = get_salesperson_info(opp[g_opps.PersonID]);
		opp.color = info.color;
		//Grid.addOpportunity(opp);
		opp.added = true;
	}
	else if (oldCategory == opp[g_opps.Category] && oldPersonID == opp[g_opps.PersonID])
	{
	}
	else
	{
		if (oldPersonID != opp[g_opps.PersonID])
		opp.color = get_salesperson_info(opp[g_opps.PersonID]).color;
		var newCategory = opp[g_opps.Category];
		var newPersonID = opp[g_opps.PersonID];
		opp[g_opps.Category] = oldCategory;
		opp[g_opps.PersonID] = oldPersonID;
		opp[g_opps.Category] = newCategory;
		opp[g_opps.PersonID] = newPersonID;
	}

	document.body.style.cursor = 'auto';
}

function getSrc2Abbr(idSource2)
{

	for (var i=0; i<g_sources2.length; i++)
	{
		if(idSource2 == g_sources2[i][g_sources2.Source2ID])
			return g_sources2[i][g_sources2.Abbr];
	}
	return '';
}

function find_product(id)
{
	for (var i = 0; i < g_products.length; ++i)
	if (g_products[i][g_products.ProductID] == id) return g_products[i];

	return null;
}

function callbackMassMove()
{
	for (i=0;i<g_opportunities.length;i++)
	{
		if (g_opportunities[i].checked && g_opportunities[i].checked==true)	g_opportunities[i].checked=false;
	}
}
