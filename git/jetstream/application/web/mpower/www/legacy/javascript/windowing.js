var Windowing = undefined;
if (window.opener && !window.forceAsWindower) {
    Windowing = window.opener.Windowing;    
}

if(Windowing == undefined)
{
	Windowing = new Object();

	Windowing.dropBox = new Object(); // dropbox for passing info between windows

	Windowing.openWindow = _Windowing_openWindow;
	Windowing.openSizedWindow = _Windowing_openSizedWindow;
	Windowing.openPrompt = _Windowing_openPrompt;
	Windowing.openSizedPrompt = _Windowing_openSizedPrompt;
	Windowing.open2ndPrompt = _Windowing_open2ndPrompt;
	Windowing.openSized2ndPrompt = _Windowing_openSized2ndPrompt;
	Windowing.openSized2ndPromptScroll = _Windowing_openSized2ndPromptScroll;
	Windowing.closeAllWindows = _Windowing_closeAllWindows;
	Windowing.closeWindow = _Windowing_closeWindow;
	Windowing.getWindow = _Windowing_getWindow;
	Windowing.open = _Windowing_open;
	Windowing.openSizedWindowMousePos = _Windowing_openSizedWindowMousePos;
	Windowing.closePopupBlockedMessage = _Windowing_closePopupBlockedMessage;
	
	Windowing.m_windows = new Object();
	Windowing.m_windowNum = 0;

	// change these to affect window appearance of all windows
	Windowing.m_attribs = 'location=no,menubar=no,toolbar=no,resizable=yes';
}

/**
 * open an ok/cancel prompt
 */
function _Windowing_openPrompt(url)
{
	return _Windowing_openImpl(url, 200, 400, 'promptwind', true, true, false);
}

/**
 * open an ok/cancel prompt, specifying its size
 */
function _Windowing_openSizedPrompt(url, height, width)
{
	return _Windowing_openImpl(url, height, width, 'promptwind', true, true, false);
}

/**
 * open an ok/cancel prompt from an existing prompt, specifying its size
 */
function _Windowing_open2ndPrompt(url)
{
	return _Windowing_openImpl(url, 200, 400, '2ndpromptwind', true, false, false);
}

/**
 * open an ok/cancel prompt from an existing prompt, specifying its size
 */
function _Windowing_openSized2ndPrompt(url, height, width)
{
	return _Windowing_openImpl(url, height, width, '2ndpromptwind', true, false, false);
}

function _Windowing_openSized2ndPromptScroll(url, height, width)
{
	return _Windowing_openImpl(url, height, width, '2ndpromptwind', true, false, true);
}

/**
 * open a window that's a child of the top window
 */
function _Windowing_openWindow(url, name)
{
	if (!name || name == '') name = 'wind';
	return _Windowing_openImpl(url, -1, -1, name, false, true, true);
}

/**
 * open a window that's a child of the top window, specifying its size
 */
function _Windowing_openSizedWindow(url, height, width, name)
{
	if (!name || name == '') name = 'wind';
	return _Windowing_openImpl(url, height, width, name, false, true, true);
}

/**
 * open a window that's a child of the top window, specifying its size
 * relative to the mouse position
 */
function _Windowing_openSizedWindowMousePos(url, height, width, name, mouseX, mouseY)
{
	if (!name || name == '') name = 'wind';
	return _Windowing_openImpl(url, height, width, name, false, true, true, mouseX, mouseY);
}


function _Windowing_open(url, height, width, name, centerIt, closePrompts, scrollbars)
{
	return _Windowing_openImpl(url, height, width, name, centerIt, closePrompts, scrollbars);
}

function _Windowing_openImpl(url, height, width, name, centerIt, closePrompts, scrollbars)
{
	var isIE = document.all? true : false;

	var args = _Windowing_openImpl.arguments;
	
	//if (!(!window.loginSessionNotIdle))
		//loginSessionNotIdle();

	var h = parseInt(height);
	if (isNaN(h) || h <= 0) h = window.height;

	var w = parseInt(width);
	if (isNaN(w) || w <= 0)
	{
		w = document.body.offsetWidth;
		if (w > 790) w = 790;
	}

	var attribs = Windowing.m_attribs + ',height=' + h + ',width=' + w;

	if (scrollbars) attribs += ',scrollbars=yes';
	else attribs += ',scrollbars=no';

	if (centerIt)
	{
		var top = (screen.height / 2) - (h / 2) - 15;
		if (top < 0) top = 0;
		attribs += ',top=' + top;
		attribs += ',left=' + ((screen.width / 2) - (w / 2));
	}
	else if(args.length == 9) //passed in mouse screen pos coords
	{
		var mouseX = args[7];
		var mouseY = args[8];	
        //Need to convert client window coords to screen coords	
        //Use window.screenX, window.screenY for Netscape/Mozilla
		var top, left, topMargin, leftMargin;
		topMargin = isIE ? window.screenTop : window.screenY;
		leftMargin = isIE ? window.screenLeft : window.screenX;
		top = (mouseY + topMargin)+10;  
		left = ((mouseX + leftMargin) - (w / 2));
		attribs += ',top=' + top;
		attribs += ',left=' + left;		
	}

	if (closePrompts) Windowing.closeWindow('promptwind');
	if (window.g_user)
		name=g_user[0][g_user.PersonID]+name;	
	Windowing.closeWindow(name);

	var newWindowName = name + (++Windowing.m_windowNum);
	Windowing.m_windows[newWindowName] = window.open(url, newWindowName, attribs);
	if (!Windowing.m_windows[newWindowName])
	{
		window.focus();
		Windowing.coverdiv = document.createElement('DIV');
		Windowing.coverdiv.innerHTML = '&nbsp;';
		Windowing.coverdiv.style.width = "100%";
		Windowing.coverdiv.style.height = "100%";
		Windowing.coverdiv.style.backgroundColor = 'black';
		Windowing.coverdiv.style.position = "absolute";
		Windowing.coverdiv.style.padding = '0px';
		Windowing.coverdiv.style.margin = '0px';
		Windowing.coverdiv.style.left = "0px";
		Windowing.coverdiv.style.top = "0px";
		Windowing.coverdiv.style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity=50)";
		Windowing.coverdiv.style.zIndex = 1;
		
		Windowing.covertable = document.createElement('TABLE');
		Windowing.covertable.cellPadding = 0;
		Windowing.covertable.cellSpacing = 0;
		Windowing.covertable.width = "100%";
		Windowing.covertable.height = "100%";
		Windowing.covertable.style.position = "absolute";
		Windowing.covertable.style.left = "0px";
		Windowing.covertable.style.top = "0px";
		Windowing.covertable.style.zIndex = 2;
		var temptr = Windowing.covertable.insertRow(Windowing.covertable.rows.length);
		Windowing.coverttd = temptr.insertCell(temptr.cells.length);
		Windowing.coverttd.align = "center";
		Windowing.coverttd.vAlign = "middle";
		Windowing.coverttd.innerHTML = '<iframe src="../shared/popup_blocked.html" scrolling="no" style="border:2px solid black; width:700px; height:480px;" frameborder="0"></iframe>';
		document.body.appendChild(Windowing.coverdiv);
		document.body.appendChild(Windowing.covertable);
		
		Windowing.blockedArgs = new Object();
		Windowing.blockedArgs.url = url;
		Windowing.blockedArgs.height = height;
		Windowing.blockedArgs.width = width;
		Windowing.blockedArgs.name = name;
		Windowing.blockedArgs.centerIt = centerIt;
		Windowing.blockedArgs.closePrompts = closePrompts;
		Windowing.blockedArgs.scrollbars = scrollbars;
				
		window.onscroll = function()
		{
			Windowing.coverdiv.style.top = document.body.scrollTop+'px';
			Windowing.covertable.style.top = document.body.scrollTop+'px';
		};
		
		return;
	}
	Windowing.m_windows[newWindowName].focus();

	return Windowing.m_windows[newWindowName];
}

function _Windowing_closePopupBlockedMessage()
{
	document.body.removeChild(Windowing.coverdiv);
	document.body.removeChild(Windowing.covertable);
}

/**
 * close a window by name
 */
function _Windowing_closeWindow(windowName)
{
	for (var k in this.m_windows)
	{
		if (k.length < windowName.length) continue;
		
		if (k.substr(0, windowName.length).toLowerCase() != windowName.toLowerCase()) continue;

		try
		{
			if (this.m_windows[k]) this.m_windows[k].close();
		}
		catch (thingy) {}
		this.m_windows[k] = null;
	}
}

/**
 * close all windows that are children of the top window
 */
function _Windowing_closeAllWindows()
{
	for (var k in this.m_windows)
	{
		try
		{
			if (this.m_windows[k]) this.m_windows[k].close();
		}
		catch (thingy) {}
		this.m_windows[k] = null;
	}
}

/**
 * retreive a reference to a window by name (null if it's not open)
 */
function _Windowing_getWindow(windowName)
{
	for (var k in this.m_windows)
	{
		if (k.length < windowName.length) continue;
		if (k.substr(0, windowName.length).toLowerCase() != windowName.toLowerCase()) continue;

		var win = this.m_windows[k];
		if (win && !win.closed) return win;
	}

	return null;
}



