Saver.stripHTML = _Saver_stripHTML;
Saver.getResult = _Saver_getResult;
Saver.savers = new Array();
Saver.uniqueID = 1;

Array.prototype.toPipes = _Array_toPipes;

function Saver()
{
	// input parameters
	this.m_data = null;
	this.m_callback = null;
	this.m_url = '';
	this.m_msg = 'Saving...'; // detail message
	this.m_params = new Object();
	this.m_interval = 500; // milliseconds between checks for results
	this.m_timeout = 30000; // milliseconds to wait before timing out

	// output parameters
	this.result = ''; // this is set when the window comes back with a result

	// public methods
	this.open = _Saver_open; // open a Saver window
	this.save = _Saver_save; // save some date .. you can call it multiple times
	this.reset = _Saver_reset; // reset member variables to default values
	this.close = _Saver_close; // close the Saver window

	// private stuff
	this.m_id = 'saver' + Saver.uniqueID++;
	this.m_timeStamp = 0; // when the latest save started
	this.m_saveFrame = null; // the iframe inside the popup, that submits the form
}

function _Saver_open(focusWindow)
{
	this.m_window = Windowing.open('/shared/blank.html', 100, 300, this.m_id, true, false, false);
	if (focusWindow) {
		focusWindow.focus();
	} else {
		this.m_window.focus();
	}
}

function _Saver_close()
{
	this.m_window.close();
	for (var i = 0; i < Saver.savers.length; ++i)
	{
		if (Saver.savers[i] != this)
		continue;
		Saver.savers[i] = null;
		break;
	}
	if (window.unsavedTimerSource)
	window.unsavedTimerSource.afterSaveComplete();
}

function _Saver_reset()
{
	this.m_data = null;
	this.m_callback = null;
	this.m_url = '';
	this.m_msg = 'Saving...';
	this.m_params = new Object();
	this.m_interval = 500;
	this.m_timeout = 30000;
}

function _Saver_save(msg)
{
	
	try {
		var url = window.location.href;
		var querystring = url.split('?');
		var str = querystring[1].toString();
		var sntemp = str.split("&");
		var sn = sntemp[0].toString();

	}catch(e) {
		var sn = '';
	}

	if (msg) this.m_msg = msg;

	try {
		var myurl = window.location.href;
		var urlitem = myurl.split('/shared/');
		if(urlitem.length == 1) var urlitem = myurl.split('/board/');	
		var urltosubmit = urlitem[0] + this.m_url.replace('../data', '/data');
	}catch(e) {
		var urltosubmit = this.m_url;
	}
	
	// "M-Power" removed (for Systema)
	// var str = '<html><title>M-Power</title></head>';
	var str = '<html></head>';
	str += '<body bgcolor="white" topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 marginheight=0 marginwidth=0>';

	str += '<table border=0 align="center" height="100%" width="100%">';
	str += '<tr valign="middle"><td><img src="../images/saving.gif"></td>';
	str += '<td style="font-family: arial, sans-serif"><b><div id="msgdiv">' + this.m_msg;
	str += '</div></b></td></tr></table>';

	str += '<iframe id="saveframe" src="blank.html" name="saveframe" class="saver"> </iframe>';
	str += '</body></html>';

	this.m_window.document.writeln(str);
	this.m_window.document.close();

	this.m_saveFrame = this.m_window.frames['saveframe'];
	str = '<script language="JavaScript">\n';
	str += '<!--\n';
	str += 'window.result = "waiting";\n';
	str += '// -->\n';
	str += '</script>\n';
	str += '<form name="form1" method="post" action="' + urltosubmit + '?' + sn + '">';

	str += 'random_saver2: <input type="text" name="random_saver2" value="' + Math.random() + '"><br>';
	for (var k in this.m_params)
	str += k + ': <input type="text" name="' + k + '" value="' + this.m_params[k] + '"><br>';
	if (this.m_data)
	str += '<textarea style="width: 100%" rows=20 name="mpower_data">' + this.m_data + '</textarea>';
	str += '</form>';
	this.m_saveFrame.document.writeln(str);

	var index = Saver.savers.length;
	for (var i = 0; i < Saver.savers.length; ++i)
	{
		if (Saver.savers[i]) continue;
		index = i;
		break;
	}
	Saver.savers[index] = this;
	if (window.unsavedTimerSource)
	this.m_timer = window.unsavedTimerSource.setInterval('g_openerWindow.Saver.getResult(' + index + ')', this.m_interval);
	else
	this.m_timer = window.setInterval('Saver.getResult(' + index + ')', this.m_interval);
	this.m_timeStamp = new Date().getTime();
	this.m_saveFrame.document.forms['form1'].submit();
}

function _Saver_getResult(index)
{
	var s = Saver.savers[index];
	if (s.m_saveFrame.result == 'waiting')
	{
		if (new Date().getTime() > (s.m_timeStamp + s.m_timeout))
		{
			if (window.unsavedTimerSource)
			window.unsavedTimerSource.clearInterval(s.m_timer);
			else
			window.clearInterval(s.m_timer);
			if (s.m_callback)
			{
				alert('timed out');
				s.m_callback('timed out');

			}
		}

		return; // keep waiting
	}

	if (window.unsavedTimerSource) window.unsavedTimerSource.clearInterval(s.m_timer);
	else window.clearInterval(s.m_timer);
	if (s.m_saveFrame.result && s.m_saveFrame.result.sort)
	{
		s.m_result = new Array();
		for (var k = 0; k < s.m_saveFrame.result.length; ++k)
		s.m_result[k] = s.m_saveFrame.result[k];
		s.m_result.has_error = false;
	}
	else if (s.m_saveFrame.result)
	{

		s.m_result = new String(s.m_saveFrame.result);
		s.m_result.has_error = false;
	}
	else
	{
		if (s.m_saveFrame.document.body)
		s.m_result = new String(s.m_saveFrame.document.body.innerHTML);
		else s.m_result= new String('Unknown error');
		s.m_result.has_error = true;
	}
	var errMsg = s.m_saveFrame.document.getElementById('errmsg');
	if (errMsg && errMsg.innerHTML && errMsg.innerHTML.length)
	{
		s.m_result += '\n\n' + errMsg.innerHTML;
		s.m_result.has_error = true;
	}

	if (s.m_callback)
	s.m_callback(s.m_result);
}

function _Saver_stripHTML(str)
{
	if (!str.replace)
	return str;
	var ret = str;
	ret = ret.replace(/<br>/ig, '\n');
	ret = ret.replace(/<[a-zA-Z\/][^>]*>/ig, '');
	return ret;
}

function _Array_toPipes()
{
	var out = '';
	for (var i = 0; i < this.length; ++i)
	{
		if (i)
		out += '|';
		if (this[i] != null)
		out += this[i];
	}

	return out;
}

