//--------------------------------------------------------------------
//
//	B R O W S E R   I D E N T I F I C A T I O N
//
//--------------------------------------------------------------------

isNav = (navigator.appName.indexOf("Netscape") != -1) ? true : false;

//----------------------------------------------------------------------
Dates = new Object();

Dates.m_monthNames = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
Dates.m_weekDays = new Array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
Dates.m_weekBeginsWith = 0; // 0 to 6 .. 1 means Monday
Dates.m_monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31); // (not counting leap years)

Dates.m_thisMonthNum = new Date().getMonth() + 1;
Dates.m_lastMonthNum = (Dates.m_thisMonthNum == 1) ? 12 : Dates.m_thisMonthNum - 1;
Dates.m_previousMonthNum = (Dates.m_lastMonthNum == 1) ? 12 : Dates.m_lastMonthNum - 1;

Dates.m_thisMonth = Dates.m_monthNames[Dates.m_thisMonthNum - 1].substring(0, 3);
Dates.m_lastMonth = Dates.m_monthNames[Dates.m_lastMonthNum - 1].substring(0, 3);
Dates.m_previousMonth = Dates.m_monthNames[Dates.m_previousMonthNum - 1].substring(0, 3);

Dates.m_thisMonthName = Dates.m_monthNames[Dates.m_thisMonthNum - 1];
Dates.m_lastMonthName = Dates.m_monthNames[Dates.m_lastMonthNum - 1];
Dates.m_previousMonthName = Dates.m_monthNames[Dates.m_previousMonthNum - 1];

Dates.m_thisYear = new Date().getFullYear();
Dates.m_lastYear = Dates.m_thisYear - 1;

if (Dates.m_thisMonthNum < 4) Dates.m_thisQuarterNum = 1;
else if (Dates.m_thisMonthNum < 7) Dates.m_thisQuarterNum = 2;
else if (Dates.m_thisMonthNum < 10) Dates.m_thisQuarterNum = 3;
else Dates.m_thisQuarterNum = 4;
Dates.m_lastQuarterNum = (Dates.m_thisQuarterNum == 1) ? 4 : Dates.m_thisQuarterNum - 1;
Dates.m_previousQuarterNum = (Dates.m_lastQuarterNum == 1) ? 4 : Dates.m_lastQuarterNum - 1;

Dates.m_thisQuarter = 'Q' + Dates.m_thisQuarterNum + ' ' + Dates.m_thisYear;
Dates.m_lastQuarter = 'Q' + Dates.m_lastQuarterNum + ' ' + ((Dates.m_lastQuarterNum > Dates.m_thisQuarterNum) ? Dates.m_lastYear : Dates.m_thisYear);
Dates.m_previousQuarter = 'Q' + Dates.m_previousQuarterNum + ' ' + ((Dates.m_previousQuarterNum > Dates.m_thisQuarterNum) ? Dates.m_lastYear : Dates.m_thisYear);

Dates.mssql2us = _Dates_mssql2us;
Dates.us2ordered = _Dates_us2ordered;
Dates.compare = _Dates_compare;
Dates.normalize = _Dates_normalize;
Dates.formatDate = _Dates_formatDate;
Dates.formatShortDate = _Dates_formatShortDate;
Dates.parseTime = _Dates_parseTime;
Dates.isLeapYear = _Dates_isLeapYear;
Dates.normalizeTime = _Dates_normalizeTime;
Dates.makeDateObj = _Dates_makeDateObj;

function killLeadingChar(str, c)
{
	var pos = 0;
	while (pos < str.length)
	{
		if (str.charAt(pos) != c)
			return str.substr(pos);
		++pos;
	}
	return '';
}

function _Dates_mssql2us_OLD(datestr)
{
	while (datestr.indexOf(' ') == 0)
		datestr = datestr.substr(1);
	datestr = datestr.replace('  ', ' ');
	var the_pieces = datestr.split(' ');

	if (the_pieces.length != 4)
		return datestr;
	var month = -1;
	switch (the_pieces[0].toLowerCase())
	{
		case 'jan':
			month = 1;
			break;
		case 'feb':
			month = 2;
			break;
		case 'mar':
			month = 3;
			break;
		case 'apr':
			month = 4;
			break;
		case 'may':
			month = 5;
			break;
		case 'jun':
			month = 6;
			break;
		case 'jul':
			month = 7;
			break;
		case 'aug':
			month = 8;
			break;
		case 'sep':
			month = 9;
			break;
		case 'oct':
			month = 10;
			break;
		case 'nov':
			month = 11;
			break;
		case 'dec':
			month = 12;
			break;
	}

	if (month == -1)
		return datestr;

	var leftstuff = the_pieces[3].substr(0, the_pieces[3].length - 2);
	var rightstuff = the_pieces[3].substr(the_pieces[3].length - 2);

	return (month + '/' + killLeadingChar(the_pieces[1], '0') + '/' + the_pieces[2] + ' ' + leftstuff + ' ' + rightstuff);
}

function _Dates_mssql2us_TEST(datestr)
{
	var d1=_Dates_mssql2us_NEW(datestr);
	var d2=_Dates_mssql2us_OLD(datestr);
	if (d1!=d2)
		alert("d1 ["+d1+"] d2 ["+d2+"]");
	return d2;
}


function _Dates_mssql2us(datestr)
{
	if(datestr == null)
		return '';
	datestr = datestr.replace('  ', ' ');
	if (datestr.indexOf(' ') == 0)
		datestr = datestr.substr(1);

	var monthInd=datestr.indexOf(' ');
	if (monthInd==-1) return datestr;
	var month = -1;
	switch (datestr.substr(0,monthInd).toLowerCase())
	{
		case 'jan':
			month = 1;
			break;
		case 'feb':
			month = 2;
			break;
		case 'mar':
			month = 3;
			break;
		case 'apr':
			month = 4;
			break;
		case 'may':
			month = 5;
			break;
		case 'jun':
			month = 6;
			break;
		case 'jul':
			month = 7;
			break;
		case 'aug':
			month = 8;
			break;
		case 'sep':
			month = 9;
			break;
		case 'oct':
			month = 10;
			break;
		case 'nov':
			month = 11;
			break;
		case 'dec':
			month = 12;
			break;
	}

	if (month == -1)
		return datestr;

	var ind1=datestr.indexOf(' ',monthInd+1);
	if (ind1==-1) return datestr;
	var piece1=datestr.substr(monthInd+1,ind1-monthInd-1);

	var ind2=datestr.indexOf(' ',ind1+1);
	if (ind2==-1) return datestr;
	var piece2=datestr.substr(ind1+1,ind2-ind1-1);

	var piece3=datestr.substr(ind2+1);

	var leftstuff = piece3.substr(0, piece3.length - 2);
	var rightstuff = piece3.substr(piece3.length - 2);

	return (month + '/' + killLeadingChar(piece1, '0') + '/' + piece2 + ' ' + leftstuff + ' ' + rightstuff);
}

function _Dates_us2ordered(datestr)
{
		return datestr;
	var retval = a[2];
	retval += '/';
	if (a[0].length == 1)
		retval += '0';
	retval += a[0];
	retval += '/';
	if (a[1].length == 1)
		retval += '0';
	retval += a[1];
	return retval;
}

function _Dates_compare(datestr1, datestr2)
{
	var d1 = Date.parse(datestr1);
	var d2 = Date.parse(datestr2);

	if (isNaN(d1) || isNaN(d2))
		return NaN;
	return d1 - d2;
}

var _normalize_validChars = '0123456789/';

function _Dates_normalize(datestr)
{
	datestr = datestr + '';
	if (datestr == '')
		return null;
	while (datestr.indexOf('-') != -1)
		datestr = datestr.replace('-', '/');
	while (datestr.indexOf('\\') != -1)
		datestr = datestr.replace('\\', '/');
	for (var i = 0; i < datestr.length; ++i)
	{
		if (_normalize_validChars.indexOf(datestr.charAt(i)) == -1)
			return null;
	}
	var comps = datestr.split('/');
	if (comps.length != 3)
		return null;
	var is_short_year = false;
	if (comps[2].length == 2)
		is_short_year = true;
	else if (comps[2].length != 4)
		return null;
	var year = parseInt(killLeadingChar(comps[2], '0'));
	if (isNaN(year) || year < 0)
		return null;
	if (is_short_year)
	{
		if (year < 50)
			year += 2000;
		else
			year += 1900;
	}
	year = year % 100;
	if (year < 10)
		year = '0'+year;
	var month = parseInt(killLeadingChar(comps[0], '0'));
	if (isNaN(month) || month < 1 || month > 12)
		return null;
	var day = parseInt(killLeadingChar(comps[1], '0'));
	if (isNaN(day))
		return null;
	var day_max = Dates.m_monthDays[month - 1];
	if (month == 2 && Dates.isLeapYear(year))
		++day_max;
	if (day > day_max)
		return null;
	if (month < 10)
		month = '0'+month;
	if (day < 10)
		day = '0'+day;
	return month + '/' + day + '/' + year;
}

function _Dates_formatDate(dateObj)
{
	if (isNaN(dateObj.getMonth()))
		return '';

	var month = (dateObj.getMonth() + 1);
	if (month < 10)
		month = '0' + month;
	var day = dateObj.getDate();
	if (day < 10)
		day = '0' + day;
	var year = dateObj.getFullYear() % 100;
	if (year < 10)
		year = '0' + year;
	return month + '/' + day + '/' + year;
}

function _Dates_formatShortDate(dateObj)
{
	return _Dates_formatDate(dateObj);
}

function _Dates_makeDateObj(dateStr)
{
	var tempdate = new Date(dateStr);
	if (tempdate.getFullYear() < 1950)
		tempdate.setFullYear(tempdate.getFullYear()+100);
	return tempdate;
}

// parses 24-hr hh, hh:mm or hh:mm:ss and returns 12-hr hour, minutes, and am_pm as properties of an object
function _Dates_parseTime(timeStr)
{
	var ret = new Object();

	var pos = timeStr.indexOf(':');
	if (pos != -1)
	{
		ret.hour = timeStr.substr(0, pos);
		ret.minutes = timeStr.substr(++pos);
		pos = ret.minutes.indexOf(':');
		if (pos != -1)
			ret.minutes = ret.minutes.substr(0, pos);
	}
	else
	{
		ret.minutes = 0;
		ret.hour = timeStr;
	}

	if (!isNaN(ret.hour) && ret.hour > 12)
	{
		ret.am_pm = 'PM';
		ret.hour -= 12;
	}
	else
		ret.am_pm = 'AM';

	return ret;
}

function _Dates_normalizeTime(timeInput)
{
	if(timeInput.length == 0) return ' ';
	var testTime = Date.parse('1/1/1970 ' + timeInput);

	if(isNaN(testTime)) return null;

	var objTime = new Date('1/1/1970 ' + timeInput);
	var hrs = objTime.getHours();
	var mins = objTime.getMinutes();
	mins = mins < 10 ? '0' + mins : mins;
	var ampm = hrs > 11 ? 'PM' : 'AM';
	hrs = hrs > 12 ? hrs - 12 : hrs;
	if(!hrs) hrs = 12;
	var strTime = hrs + ':' + mins + ' ' + ampm;
	return strTime;
}


// Years evenly divisible by four are normally leap years, except
// years that also evenly divisible by 100 are not leap years, except
// that years also evenly divisible by 400 are leap years.
function _Dates_isLeapYear(year)
{
	if ((year % 4) != 0)
		return false;
	if ((year % 100) == 0 && (year % 400) != 0)
		return false;
	return true;
}


function trim(str)
{
	var ret = str.toString();
	ret = ret.replace(/^\s*/, '');
	ret = ret.replace(/\s*$/, '');
	return ret;
}

function commas(amt)
{
	amt = unformat_money(amt);
	var ind=amt.indexOf('.');
	var cents = '';
	if (ind >= 0)
	{
		cents = amt.substr(ind+1);
		amt = amt.substr(0, ind);
	}
	var ret = '';
	for (var i = 0; i < amt.length; ++i)
	{
		if (i && (i % 3) == (amt.length % 3))
			ret += ',';
		ret += amt.charAt(i);
	}
	if(cents.length) ret += '.' + cents;
	return ret;

}

function format_money(amt)
{
	amt = amt.toString();
	if (amt == '&nbsp;')
		return amt;

	var ind=amt.indexOf('.');
	if (ind >=0) amt=amt.substr(0,ind);

	var neg=amt.charAt(0)=='-';
	if (neg) amt=amt.substr(1);

	var ret = '';
	ret = commas(amt);
	if (neg) ret='-'+ret;

	if (g_the_only_company[g_company.MonetaryValue]=='1')
	{
		if (ret == '')
			return ret;
		else
			return '$' + ret;
	}
	else return ret;
}

function format_money_cents(amt)
{
	amt = amt.toString();
	if (amt == '&nbsp;')
		return amt;
	var ind=amt.indexOf('.');
	var cents = '';
	if (ind >=0)
	{
		cents = amt.substr(ind+1);
		amt = amt.substr(0, ind);
		if(cents.length < 2)
			cents += 0;
		else if (cents.length > 2)
			cents = cents.substr(0, 2);
	}
	else
		cents = '00';
	var neg=amt.charAt(0)=='-';
	if (neg) amt=amt.substr(1);

	var ret = '';
	for (var i = 0; i < amt.length; ++i)
	{
		if (i && (i % 3) == (amt.length % 3))
			ret += ',';
		ret += amt.charAt(i);
	}

	if (neg) ret='-'+ret;
	if (ret) ret += '.' + cents;

	if ( typeof(g_the_only_company) == 'undefined' || (g_the_only_company[g_company.MonetaryValue]=='1'))
	{
		if (ret == '')
			return ret;
		else
			return '$' + ret;
	}
	else return ret;
}

function unformat_money(amt)
{
	amt = amt.toString();
	amt = amt.replace(/\$/g, '');
	amt = amt.replace(/,/g, '');
	return amt;
}

function press_fake_button(obj)
{
	obj.className = obj.className.replace(/raised/, 'lowered');
}

function lift_fake_button(obj)
{
	obj.className = obj.className.replace(/lowered/, 'raised');
}

function _String_trimLeft()
{
	return this.replace(/^\s+/,'');
}

function _String_trimRight()
{
	return this.replace(/\s+$/,'');
}

function _String_trim()
{
	return this.replace(/^\s+/,'').replace(/\s+$/,'');
}

String.prototype.trimLeft = _String_trimLeft;
String.prototype.trimRight = _String_trimRight;
String.prototype.trim = _String_trim;

function _Array_quicksort(loBound, hiBound)
{
	if (!loBound)
		loBound = 0;
	if (!hiBound)
		hiBound = this.length - 1;
	var pivot, loSwap, hiSwap, temp;

	if (hiBound - loBound == 1)
	{
		if (this.sort_compare(this[loBound], this[hiBound]) > 0)
		{
			temp = this[loBound];
			this[loBound] = this[hiBound];
			this[hiBound] = temp;
		}
		return;
	}

	pivot = this[parseInt((loBound + hiBound) / 2)];
	this[parseInt((loBound + hiBound) / 2)] = this[loBound];
	this[loBound] = pivot;
	loSwap = loBound + 1;
	hiSwap = hiBound;

	do
	{
		while (loSwap <= hiSwap && this.sort_compare(this[loSwap], pivot) < 1)
			++loSwap;
		while (this.sort_compare(this[hiSwap], pivot) > 0)
			--hiSwap;
		if (loSwap < hiSwap)
		{
			temp = this[loSwap];
			this[loSwap] = this[hiSwap];
			this[hiSwap] = temp;
		}
	} while (loSwap < hiSwap);

	this[loBound] = this[hiSwap];
	this[hiSwap] = pivot;

	if (loBound < hiSwap - 1)
		this.quicksort(loBound, hiSwap - 1);

	if (hiSwap + 1 < hiBound)
		this.quicksort(hiSwap + 1, hiBound);
}

Array.prototype.quicksort = _Array_quicksort;

function _Array_radixsort()
{
	if (this.length == 0)
		return;
	var b0 = new Array();  // Bin for 0 digits
	var b1 = new Array();  // Bin for 1 digits

	var greatest_length = 0;
	for (var k = 0; k < this.length; ++k)
	{
		if (this[k].length > greatest_length)
			greatest_length = this[k].length;
	}

	if (greatest_length == 0)
		return;

	greatest_length *= 8;

	for (var i = greatest_length; i > 0; --i)
	{
		var current_char_index = i / 8;
		var mask = 1 << (8 - (i % 8));     // Digit (2**i)
		var biggest = 2 << (8 - (i % 8));  // If all of a is smaller, we're done
		var zeros = 0;         // Number of elements in b0, b1
		var ones = 0;
		var found = false;     // Any digits past i?
		for (var j = 0; j < this.length; ++j)  // Sort into bins b0, b1
		{
			var current_val = 0;
			if (current_char_index < this[j].length)
				current_val += this[j].charCodeAt(current_char_index);
			if ((current_char_index - 1) < this[j].length && (current_char_index - 1) >= 0)
				current_val += (this[j].charCodeAt(current_char_index - 1) << 8);
			if ((current_val & mask) == 0)
				b0[zeros++] = this[j];
			else
				b1[ones++] = this[j];

			if (current_char_index > 0 || current_val >= biggest)  // Any more digits to sort on?
				found = true;
		}

		for (j = 0; j < zeros; ++j)  // Concatenate b0, b1 back to a
			this[j] = b0[j];

		for (j = 0; j < ones; ++j)
			this[j + zeros] = b1[j];

		if (!found)
			break;
	}
}

Array.prototype.radixsort = _Array_radixsort;

function _String_escape_html()
{
	var temp = this.replace('&', '&amp;');
	temp = temp.replace('<', '&lt;');
	temp = temp.replace('>', '&gt;');
	return temp;
}

function _String_unescape_html()
{
	var temp = this.replace('&gt;', '>');
	temp = temp.replace('&lt;', '<');
	temp = temp.replace('&amp;', '&');
	return temp;
}

String.prototype.escape_html = _String_escape_html;
String.prototype.unescape_html = _String_unescape_html;


function getQueryVariable(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if (pair[0] == variable) {
      return pair[1];
    }
  }
  alert('Query Variable ' + variable + ' not found');
}

//Construct dates built on d/m/yr; do arithmetic
function getDaysOld(strDate)
{
	var now=new Date();
	var strNow = (now.getMonth()+1)*1 + '/' + now.getDate() + '/' + now.getFullYear();
	var then = Dates.makeDateObj(strDate);
	var strThen = (then.getMonth()+1)*1 + '/' + then.getDate() + '/' + then.getFullYear();
	var dNow = new Date(strNow);
	var dThen = new Date(strThen);
	return 	Math.round((dNow.getTime() - dThen.getTime()) / 86400000);
}

function getDaysDiff(strDate1, strDate2)
{
	var now = Dates.makeDateObj(strDate1);
	var then = Dates.makeDateObj(strDate2);
	var strNow = (now.getMonth()+1)*1 + '/' + now.getDate() + '/' + now.getFullYear();
	var strThen = (then.getMonth()+1)*1 + '/' + then.getDate() + '/' + then.getFullYear();
	var dNow = new Date(strNow);
	var dThen = new Date(strThen);
	return 	Math.round((dNow.getTime() - dThen.getTime()) / 86400000);
}

//Date, Time validation routines
function check_date(elemid, field_name)
{
	var elem = document.getElementById(elemid);
	if (elem.value == '')
		return;
	var temp = Dates.normalize(elem.value);
	if (!temp)
	{
		if(!isNav)
			window.event.returnValue = false;
		if (field_name)
		{
			var strFN = field_name;
			if(field_name == 'First Meeting')
			{
				field_name = getCatLabel('{{FM}}','{{FM}}','FMLabel', 'First Meeting');
			}
			alert('Invalid date format for ' + field_name + '.\nMust be either MM/DD/YY or MM/DD/YYYY');
		}
		else
			alert('Invalid date format.\nMust be either MM/DD/YY or MM/DD/YYYY');
		elem.focus();
		elem.select();

	}
	else if(arguments.length > 2)
	{
		var test = new Date(temp);
		var condition = arguments[2];
		elem.value = temp;
		if(condition == 'nofuture')
		{
			var today = new Date();
			if(test.getTime() > today.getTime())
			{
				if(!isNav && window.event) window.event.returnValue = false;
				alert(field_name + ' cannot be in the future');
				elem.focus();
				elem.select();
			}
		}
	}

	else
	{
		elem.value = temp;
	}
}

function check_time(elemid, field_name)
{
	var elem = document.getElementById(elemid);
	if (elem.value == '')
		return;
	var temp = Dates.normalizeTime(elem.value);
	if (!temp)
	{
		window.event.returnValue = false;
		if (field_name)
		{
			var strFN = field_name;
			if(field_name == 'First Meeting Time')
			{
				field_name = getCatLabel('{{FM}}','{{FM}}','FMLabel', 'First Meeting') + ' Time';
			}
			alert("'" + elem.value + '" is in invalid date format for ' + field_name + '.\nMust be either HH:MM AM/PM or 24 hour time');
		}
		else
			alert("'" + elem.value + '" is in invalid date format.\nMust be either HH:MM AM/PM or 24 hour time');
		elem.focus();
		elem.select();
	}
	else
		elem.value = temp;

}

function mkShortDate(shortDate)
{
	if(shortDate.length == 0)
		return new Date();
	shortDate=Dates.mssql2us(shortDate);
	var arr=shortDate.split('/');
	var monthDay = arr[0] + '/' + arr[1];
	var shortYear = arr[2];
	if(shortYear.length >= 4)
		return new Date(shortDate);
	var prefix = shortYear < 50 ? '20' : '19';
	var longDate = monthDay+'/'+prefix+shortYear;
	return new Date(longDate);
}


document.onkeydown = onAltKeyDown;

function onAltKeyDown(e)
{
	if (!isNav)
		e = event;
	if (e.altKey)
	{
		if(e.keyCode == 83) view_rendered_source();
	}


}

function view_rendered_source()
{
	Windowing.openSizedPrompt("../shared/srcWin.html", 575, 775);
}

function getHTTPObject()
{
	var xmlhttp;
	if (!xmlhttp && typeof XMLHttpRequest != 'undefined')
	{
		try
		{
			xmlhttp = new XMLHttpRequest();
		}
		catch (e)
		{
			xmlhttp = false;
		}
	}
	return xmlhttp;
}


function Trim(stringToTrim) {
    return stringToTrim.replace(/^\s+|\s+$/g,"");
}
function LTtrim(stringToTrim) {
    return stringToTrim.replace(/^\s+/,"");
}
function RTrim(stringToTrim) {
    return stringToTrim.replace(/\s+$/,"");
}
