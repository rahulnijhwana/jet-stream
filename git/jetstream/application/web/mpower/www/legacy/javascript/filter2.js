function FilterByCheckbox(box) {
    boxes = box.form.filter;
    var display = new Object();
    display.value = '';
    for (i = 0; i < boxes.length; i++){
        if (boxes[i].checked == true){
            display.value = display.value + boxes[i].value + " |";
        }
    }
    TableFilter(display, 'tableMain', 4)
}

function TableFilter (phrase, _id, cellNr){
	var words = phrase.value.toLowerCase().split("|");
	var table = document.getElementById(_id);
	var ele;
	for (var r = 1; r < table.rows.length - 4; r++){
		ele = table.rows[r].cells[cellNr].innerHTML.replace(/<[^>]+>/g,"");
        var displayStyle = 'none';
        for (var i = 0; i < words.length; i++) {
        	var this_word = words[i].replace(/^\s+|\s+$/g,"");
        	if (this_word.length > 0) {
        		if (ele.toLowerCase().indexOf(this_word)>=0) {
        			displayStyle = '';
                }
            }
        }
		table.rows[r].style.display = displayStyle;
	}
    SumTable(_id);
}

function TableFilter2 (phrase, _id){
	var words = phrase.value.toLowerCase().split(" ");
	var table = document.getElementById(_id);
	var ele;
	for (var r = 1; r < table.rows.length; r++){
		ele = table.rows[r].innerHTML.replace(/<[^>]+>/g,"");
        var displayStyle = 'none';
        for (var i = 0; i < words.length; i++) {
		    if (ele.toLowerCase().indexOf(words[i])>=0)
    			displayStyle = '';
		    else {
    			displayStyle = 'none';
    			break;
		    }
        }
		table.rows[r].style.display = displayStyle;
	}
}

function SumTable (_id) {
	var table = document.getElementById(_id);
    var sum = 0;
    var asum = 0;
    var ssum = 0;
    var csum = 0;
    dollar_cell = 5;
    phase_cell = 7;
	for (var r = 1; r < table.rows.length - 4; r++){
        if (table.rows[r].style.display != 'none') {
            num = parseInt(table.rows[r].cells[dollar_cell].innerHTML.replace(/[^0-9.]/g,''));
            sum += num;
            // alert(table.rows[r].cells[phase_cell].innerHTML + " = " + GetPhaseType(table.rows[r].cells[phase_cell].innerHTML));
            switch (GetPhaseType(table.rows[r].cells[phase_cell].innerHTML))
            {
                case 'active':
                    asum += num;
                    break;
                case 'stalled':
                    ssum += num;
                    break;
                case 'closed':
                    csum += num;
                    break;
            }
        }
	}
    table.rows[table.rows.length - 1].cells[1].innerHTML = prodFormatCurr(sum);
    table.rows[table.rows.length - 2].cells[1].innerHTML = prodFormatCurr(csum);
    table.rows[table.rows.length - 3].cells[1].innerHTML = prodFormatCurr(ssum);
    table.rows[table.rows.length - 4].cells[1].innerHTML = prodFormatCurr(asum);
}

function prodFormatCurr(num) {
    num = num.toString().replace(/[^0-9.]/g,'');
    if(isNaN(num))
        num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num*100+0.50000000001);
    cents = num%100;
    num = Math.floor(num/100).toString();
    if(cents<10)
    cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
    num = num.substring(0,num.length-(4*i+3))+','+
    num.substring(num.length-(4*i+3));
    return (((sign)?'':'-') + '$ ' + num);
}
