isNav = (navigator.appName.indexOf("Netscape") != -1) ? true : false;
isIE = (navigator.appName.indexOf("Microsoft") != -1) ? true : false;
isDOM = (document.getElementById) ? true : false;

function onTabTDClicked()
{
	this.myTabbedPane.selectTab(this.myIndex);
}

function JSTabbedPane_addTabFromElem(strTabButtonText, elem)
{
	var tempimgtd = this.m_tab_tr.insertCell(this.m_tab_tr.cells.length);
	tempimgtd.className = "TabbedPane_TabImgTDNotSelected";
	var tempimg = document.createElement("IMG");
	tempimgtd.appendChild(tempimg);
	var temptabtd = this.m_tab_tr.insertCell(this.m_tab_tr.cells.length);
	temptabtd.mytabimgtd = tempimgtd;
	temptabtd.innerHTML = strTabButtonText;
	if (this.m_contentarray.length == 0)
	{
		tempimg.src = this.imgpath + "tabedge_left.gif";
		temptabtd.className = "TabbedPane_FirstTabNotSelected";
	}
	else
	{
		tempimg.src = this.imgpath + "tabedge.gif";
		temptabtd.className = "TabbedPane_TabNotSelected";
	}
	temptabtd.onclick = onTabTDClicked;
	temptabtd.myTabbedPane = this;
	temptabtd.myIndex = this.m_contentarray.length;
	if (arguments.length > 2)
	{
		temptabtd.exceptionals = new Array();
		for (var k = 2; k < arguments.length; ++k)
			temptabtd.exceptionals[temptabtd.exceptionals.length] = arguments[k];
	}
	this.m_tabarray[this.m_tabarray.length] = temptabtd;
	elem.className = "TabbedPane_Content";
	this.m_contentarray[this.m_contentarray.length] = elem;
	this.m_checkbox_arrays[this.m_checkbox_arrays.length] = new Array();
	elem.style.display = 'none';
	this.ivory_box.content_td.appendChild(elem);
	if (this.selectedIndex == -1)
		this.selectTab(0);
	return this.m_tabarray.length - 1;
}

function JSTabbedPane_addCheckboxException(index, checkbox_elem)
{
	this.m_checkbox_arrays[index][this.m_checkbox_arrays[index].length] = checkbox_elem;
	if (this.selectedIndex != index)
	{
		checkbox_elem.old_check = checkbox_elem.checked;
		checkbox_elem.old_check_set = true;
	}
}

g_checkbox_array = null;

function doCheckboxFix()
{
	if (!g_checkbox_array)
		return;
	for (var k = 0; k < g_checkbox_array.length; ++k)
	{
		if (g_checkbox_array[k].old_check_set)
		{
			//alert(g_checkbox_array[k].old_check);
			g_checkbox_array[k].checked = g_checkbox_array[k].old_check;
		}
	}
}

function JSTabbedPane_hideTab(index)
{
	if (index >= this.m_tabarray.length) return;
	this.m_tabarray[index].mytabimgtd.style.visibility = 'hidden';
	this.m_tabarray[index].style.visibility = 'hidden';
}

function JSTabbedPane_showTab(index)
{
	if (index >= this.m_tabarray.length) return;
	this.m_tabarray[index].mytabimgtd.style.visibility = 'visible';
	this.m_tabarray[index].style.visibility = 'visible';
}



function JSTabbedPane_selectTab(index)
{
	if (index < 0 || index >= this.m_contentarray.length)
	{
		//alert("Invalid arg to JSTabbedPane.selectTab");
		return;
	}
	if (index == this.selectedIndex)
		return;
	if (this.onTabChanging)
	{
		if (!this.onTabChanging(this.selectedIndex, index))
			return;
	}
	
	if (this.selectedIndex != -1)
	{
		var checkarray = this.m_checkbox_arrays[this.selectedIndex];
		for (var k = 0; k < checkarray.length; ++k)
		{
			checkarray[k].old_check = checkarray[k].checked;
			checkarray[k].old_check_set = true;
		}
		
		temptab = this.m_tabarray[this.selectedIndex];
		temptab.mytabimgtd.className = "TabbedPane_TabImgTDNotSelected";
		if (temptab.exceptionals)
		{
			for (var k = 0; k < temptab.exceptionals.length; ++k)
			{
				temptab.exceptionals[k].oldvis = temptab.exceptionals[k].style.visibility;
				temptab.exceptionals[k].style.visibility = "hidden";
			}
		}
		if (this.selectedIndex == 0)
			this.m_tabarray[this.selectedIndex].className = "TabbedPane_FirstTabNotSelected";
		else
			this.m_tabarray[this.selectedIndex].className = "TabbedPane_TabNotSelected";
	}
	
	//this.ivory_box.content_td.removeChild(this.m_content_elem); //CRASH!
	this.m_content_elem.style.display = 'none';
	this.m_contentarray[index].style.display = 'block';
	
	this.m_content_elem = this.m_contentarray[index];
	var temptab = this.m_tabarray[index];
	temptab.mytabimgtd.className = "TabbedPane_TabImgTDSelected";
	if (temptab.exceptionals)
	{
		for (var k = 0; k < temptab.exceptionals.length; ++k)
			temptab.exceptionals[k].style.visibility = temptab.exceptionals[k].oldvis;
	}
	if (index == 0)
		this.m_tabarray[index].className = "TabbedPane_FirstTabSelected";
	else
		this.m_tabarray[index].className = "TabbedPane_TabSelected";
	
	this.selectedIndex = index;
	g_checkbox_array = this.m_checkbox_arrays[index];
	setTimeout('doCheckboxFix();', 10);

}

function JSTabbedPane_addToElement(elem)
{
	elem.appendChild(this.m_main_table);
}

function JSTabbedPane_setTabButtonText(index, str)
{
	this.m_tabarray[index].innerHTML = str;
}

function JSTabbedPane(imgpath)
{
	this.addToElement = JSTabbedPane_addToElement;
	this.selectTab = JSTabbedPane_selectTab;
	this.addTabFromElem = JSTabbedPane_addTabFromElem;
	this.addCheckboxException = JSTabbedPane_addCheckboxException;
	this.setTabButtonText = JSTabbedPane_setTabButtonText;
	this.hideTab = JSTabbedPane_hideTab;
	this.showTab = JSTabbedPane_showTab;
	
	this.imgpath = imgpath;
	
	this.m_main_table = document.createElement("TABLE");
	this.m_main_table.cellPadding = "0";
	this.m_main_table.cellSpacing = "0";
	this.m_main_table.className = "TabbedPane_MainTable";
	this.m_tab_maintr = this.m_main_table.insertRow(this.m_main_table.rows.length);
	this.m_tab_maintr.className = "TabbedPane_TabTR";
	this.m_tab_maintd = this.m_tab_maintr.insertCell(this.m_tab_maintr.cells.length);
	this.m_tab_table = document.createElement("TABLE");
	this.m_tab_table.className = "TabbedPane_TabTable";
	this.m_tab_table.cellPadding = "0";
	this.m_tab_table.cellSpacing = "0";
	this.m_tab_table.height = "100%";
	this.m_tab_tr = this.m_tab_table.insertRow(this.m_tab_table.rows.length);
	this.m_tab_maintd.appendChild(this.m_tab_table);
	this.content_tr = this.m_main_table.insertRow(this.m_main_table.rows.length);
	this.content_td = this.content_tr.insertCell(this.content_tr.cells.length);
	
	this.ivory_box = makeFloatingBoxElem("100%", "100%");
	
	this.m_content_elem = document.createElement("DIV");
	this.m_content_elem.className = "TabbedPane_Content";
	this.ivory_box.content_td.appendChild(this.m_content_elem);
	this.content_td.appendChild(this.ivory_box);
	
	this.m_tabarray = new Array();
	this.m_contentarray = new Array();
	this.m_checkbox_arrays = new Array();
	
	this.selectedIndex = -1;
}