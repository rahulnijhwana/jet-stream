function make_curve_desc()
{
	if (g_curve.fmValue == null || g_curve.ipValue == null || g_curve.dpValue == null ||
		g_curve.cValue == null)
		return 'Curve Unknown';

	var ret = '<div align="right" style="color: ivory">';
	ret += '</div>';
	return ret;
}


function show()
{
	var els = document.forms['form1'].elements;
	update_visibility('div_analysis', els.chk_analysis.checked);
	update_visibility('div_questions', els.chk_questions.checked);
	update_visibility('div_actions', els.chk_actions.checked);
}

function update_visibility(id, makeVisible)
{
	var el = document.getElementById(id);
	if (!el) return;

	if (makeVisible) el.style.display = 'block';
	else el.style.display = 'none';
}

function write_actions()
{
	document.writeln('<div id="div_actions">');
	document.writeln(ivoryBox.makeTop());
	write_subheader('Actions');
	document.writeln('<ol>');

	var fld = (g_isNew ? g_analysisActions.ActionTextNew : g_analysisActions.ActionTextExperienced);

	for (var i = 0; i < g_analysisActions.length; ++i)
		document.writeln('<li>' + substituteCategoryLabels(g_analysisActions[i][fld]) + '</li>');

	if (g_analysisActions.length == 0)
		document.writeln('<p>No actions available.</p>');

	document.writeln('</ol>');
	document.writeln(ivoryBox.makeBottom());
	document.writeln('</div>');
}

function write_analysis(ivoryBox)
{
	document.writeln('<div id="div_analysis">');
	document.writeln(ivoryBox.makeTop());
	write_subheader('Analysis');
	document.writeln('<ol>');

	for (var i = 0; i < g_analysisReasons.length; ++i)
		document.writeln('<li>' + substituteCategoryLabels(g_analysisReasons[i][g_analysisReasons.ReasonText]) + '</li>');


	if (g_analysisReasons.length == 0)
		document.writeln('<p>No analysis available.</p>');

	document.writeln('</ol>');
	document.writeln(ivoryBox.makeBottom());
	document.writeln('</div>');
}



// displays the user defined questions from the milestones
// for the specified milestone
function write_MSquestions(milestone,showQuestions)
{
	for (var i=0;i<g_msAnalysisQuestions.length; i++)
	{
		if (g_msAnalysisQuestions[i][g_msAnalysisQuestions.Location]==milestone)
		{
			if (!showQuestions) return true;
			document.writeln('<li>' + g_msAnalysisQuestions[i][g_msAnalysisQuestions.Prompt] + '</li>');
		}
	}
	return false;
}

function write_questions()
{
	document.writeln('<div id="div_questions">');
	document.writeln(ivoryBox.makeTop());
	write_subheader('Questions');
	document.writeln('<ol>');

	var bAnyQuestions=false;
	var Milestone= 0;
	var Heading="";
	var bShowHeading=false;
	for (var i = 0; i < g_analysisQuestions.length; ++i)
	{
		if (g_analysisQuestions[i][g_analysisQuestions.IsHeader] > '0')
		{
			if (Milestone != 0)
			{
				// first, write any final user defined questions
				if (bShowHeading==true && write_MSquestions(Milestone,false))
				{
					document.writeln('<br><br>');
					document.writeln('<div class="title" style="height: 23px">');
					document.writeln( Heading+ '</div>');
				}
				write_MSquestions(Milestone,true);
				bAnyQuestions=true;
			}
			Heading = substituteCategoryLabels(g_analysisQuestions[i][g_analysisQuestions.QuestionText]);
			bShowHeading=true;
			Milestone=g_analysisQuestions[i][g_analysisQuestions.IsHeader];
		}
		else
		{
			if (bShowHeading==true)
			{
				document.writeln('<br><br>');
				document.writeln('<div class="title" style="height: 23px">');
				document.writeln( Heading+ '</div>');
				bShowHeading=false;
			}
			document.writeln('<li>' + substituteCategoryLabels(g_analysisQuestions[i][g_analysisQuestions.QuestionText]) + '</li>');

			bAnyQuestions=true;
		}
	}
	Requirement1used = '1';
	if (Requirement1used=='1')
	{
		if (write_MSquestions(0,false))
		{
			document.writeln('<br><br>');
			document.writeln('<div class="title" style="height: 23px">');
			document.writeln( "Questions about " + opener.g_company[0][opener.g_company.Requirement1] + '</div>');
			write_MSquestions(0,true);
			bAnyQuestions=true;
		}
	}

	Requirement2used = '1'; //get from the company

	if (Requirement2used =='1')
	{
		if (write_MSquestions(5,false))
		{
			document.writeln('<br><br>');
			document.writeln('<div class="title" style="height: 23px">');
			document.writeln( "Questions about " + opener.g_company[0][opener.g_company.Requirement2] + '</div>');
			write_MSquestions(5,true);
			bAnyQuestions=true;
		}
	}

	if (g_analysisQuestions.length == 0)
		document.writeln('<p>No questions available.</p>');

	document.writeln('</ol>');
	document.writeln(ivoryBox.makeBottom());
	document.writeln('</div>');
}


function write_subheader(val)
{
	document.writeln('<div class="title" align="center">');
	document.writeln(val + '</div>');
}

function write_unlikely_reason(ivoryBox, strReason)
{
	document.writeln('<div id="div_analysis">');
	document.writeln(ivoryBox.makeTop());
	write_subheader('Unlikely Curve');
	document.writeln('<ol>');

	document.writeln('<p>' + strReason + '</p>');

	document.writeln('</ol>');
	document.writeln(ivoryBox.makeBottom());
	document.writeln('</div>');
}
