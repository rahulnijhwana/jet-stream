<?php
/**
* @package Board
*/

define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');
require_once(BASE_PATH . '/include/class.DbConnManager.php');
require_once(BASE_PATH . '/include/class.SqlBuilder.php');

SessionManager::Init();
SessionManager::Validate();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>M-Power</title>
        <link rel="stylesheet" type="text/css" href="../css/sharedstyles.css,boardstyles.css">
		<script language="JavaScript" src="../javascript/analysis.js,utils.js,pageformat.js,category_labels.js"></script>
    </head>

<script language="javascript">
//<!--
<?php

$salesid = $target;

include_once('../data/_base_utils.php');
include_once('../data/_query_utils.php');
include('../data/get_company.php');
include('../data/get_msanalysisquestions.php');

$now = getdate();
$first = mktime(0, 0, 0, $now['mon'], 1, $now['year']);
if (isset($period) && $period == 'm') {
	$first = strtotime('-2 months', $first);
} else {
	$dif = (((int) $now['mon'] - 1) % 3) + 6;
	$first = strtotime("-$dif months", $first);
}

$first = date("Y/m/d", $first);

// opportunities for the sales person
$sql = "SELECT sum(VLevel) as tvlevel, count(*) as tcount  FROM opportunities 
		WHERE (Category != '6' OR ActualCloseDate >= cast('$first' AS datetime)) AND 
		(Category != '9' OR AutoReviveDate IS NOT NULL) AND 
		PersonID = $salesid AND VLevel >= 1 AND Category != '6'";
$oppdata = get_data($sql);

$tvlevel = $oppdata[0]['tvlevel'];
$tcount = $oppdata[0]['tcount'];	
$avgVal = 0;

if($tcount > 0) $avgVal = round(($tvlevel/$tcount) * 10)/10;


// get the company data
$query = "SELECT MonthsConsideredNew, ValuationUsed, ValLabel
			FROM company
			WHERE companyid = '$mpower_companyid'";
$companydata = get_data($query);


// get the sales person data
$query = "SELECT DATEDIFF(mm, StartDate, getdate()) as experience, AnalysisID, FirstName, LastName, Rating, Color, FMValue, IPValue, DPValue, CloseValue
			FROM people
			WHERE PersonID = '$salesid'";
$peopledata = get_data($query);
$analysisid = $peopledata[0]['AnalysisID'];

$tenure = 'New';
if($peopledata[0]['experience'] >= $companydata[0]['MonthsConsideredNew']) $tenure = 'Experienced';
if (!isset($analysisid) || !strlen($analysisid)) $analysisid = -1;
if (!isset($tenure) || !strlen($tenure)) $tenure = 'New';
$textfield = 'ActionText' . $tenure;

if (strcasecmp($tenure, 'New') == 0) print("g_isNew = true;\n");
else print("g_isNew = false;\n");

// get the analysis
$sql = "select * from analysis where AnalysisID = '$analysisid'";
dump_sql_as_array('g_analysis', $sql);

// get the  reasons
$sql = 'select * from reasons where ReasonID in ';
$sql .= "(select ReasonID from analysisreasonxref where AnalysisID = '$analysisid') order by ReasonID";
dump_sql_as_array('g_analysisReasons', $sql);

// get the questions
$sql = 'select * from questions where IsHeader > 1 or QuestionID in ';
$sql .= "(select QuestionID from analysisquestionxref where AnalysisID = '$analysisid') order by WithHeader, IsHeader DESC, QuestionID";
dump_sql_as_array('g_analysisQuestions', $sql);

// get the actions
$sql = "select ActionID, $textfield from actions where ActionID in ";
$sql .= "(select ActionID from analysisactionxref where AnalysisID = '$analysisid') order by ActionID";
dump_sql_as_array('g_analysisActions', $sql);

?>
//-->
</script>

    <body bgcolor="white">

        <script language="JavaScript">
            <!--
			g_header = <?= $salesid?>;
			ValuationUsed = <?= $companydata[0]['ValuationUsed'] ?>;
			PersonID = <?= $salesid ?>;
			ValLabel = '<?= $companydata[0]['ValLabel'] ?>';
			
            var name = "<?= $peopledata[0]['FirstName'].' '.$peopledata[0]['LastName'] ?>";
            g_neverHappens = '<p>&nbsp;&nbsp;&nbsp;This analysis is not expected to occur during normal sales operations.</p><p> </p>';

            var text = 'Coaching';
            if (name.length) text += ' / ' + name;
            Header.setText(text);
			
            Header.addButton(ButtonStore.getButton('Print'));
            document.writeln(Header.makeHTML());

            var ivoryBox = new IvoryBox('100%', null);
            document.writeln(ivoryBox.makeTop());
			
			var rating = <?= $peopledata[0]['Rating']?>;
			if (rating < 0) rating = 'Unlikely';

			document.writeln('<br><table border="0" width="98%" class="analysis" bgcolor="black">');
			document.writeln('<tr border="0" bgcolor="<?= $peopledata[0]['Color'] ?>">');
			document.writeln('<td border="0" width="50%" nowrap><b>Rating:</b> '+ rating +'&nbsp;&nbsp;&nbsp;<b>Tenure Level:</b> <?= $tenure ?></td>');

			if (ValuationUsed == '1')
			{
				document.writeln('<td border="0" width="50%" nowrap align="right"><b>Average <?= $companydata[0]['ValLabel'] ?> :</b> <?=$avgVal?></td>');
			}
			document.writeln('</tr></table><br>');
			
			
			try {
				var tempAnalysisID = g_analysis[0][g_analysis.AnalysisID];
				var tempAnalysisReason = g_analysis[0][g_analysis.Reason];
			} catch(err) {
				var tempAnalysisID = -3;
				var tempAnalysisReason = 'No analysis available for this salesperson.';
			}
			
            if ( tempAnalysisID== '-1') {
                document.writeln(g_neverHappens);
            }
            else if (tempAnalysisReason == '') {
				document.writeln('<form name="form1">&nbsp;&nbsp;&nbsp; Show:');
				document.writeln('&nbsp;&nbsp;&nbsp;<input type="checkbox" name="chk_analysis" onclick="show()" checked> Analysis');
				document.writeln('&nbsp;&nbsp;&nbsp;<input type="checkbox" name="chk_questions" onclick="show()" checked> Questions');
				document.writeln('&nbsp;&nbsp;&nbsp;<input type="checkbox" name="chk_actions" onclick="show()" checked> Actions');
				document.writeln('</form>');
            }
            document.writeln(ivoryBox.makeBottom());

            if (tempAnalysisID != '-1')
            {
                if(tempAnalysisReason != '') {
                    write_unlikely_reason(ivoryBox, tempAnalysisReason);
                }
                else {
                    write_analysis(ivoryBox);
                    write_questions(ivoryBox);
                    write_actions(ivoryBox);
                }
            }

            // -->
        </script>
    </body>
</html>
