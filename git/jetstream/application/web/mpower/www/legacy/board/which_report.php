<?php
/**
* @package Board
*/
/*
Here are the rules for who can view what reports:
	1. Sales managers can view all reports, limited only by company settings (eg, if the company
		doesn't use valuations, then no valuation report is displayed).
	2. At their discretion, companies can make certain reports available to salespeople.  However, ASA decides
		which reports those are.  Hence the 'Salespeople' flag in the reports table.  The company administrator
		sets permissions for those reports in Admin--hence the xref table referenced in allowSalesView().

*/

define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/class.SessionManager.php');
require_once(BASE_PATH . '/include/class.DbConnManager.php');
require_once(BASE_PATH . '/include/class.SqlBuilder.php');

SessionManager::Init();
SessionManager::Validate();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>M-Power</title>
<link rel="stylesheet" type="text/css" href="../css/sharedstyles.css,boardstyles.css">
<script language="JavaScript" src="../javascript/utils.js,category_labels.js"></script>

</head>

<body style="background-color:#949597" onload="onLoaded()">

<script language="JavaScript">
<?php
include_once('../data/_base_utils.php');
include_once('../data/_query_utils.php');		

if (!check_user_login()) {
	close_db();
	print('</script></head></html>');
	exit();
}

echo '<!--'."\n";
include_once('../data/get_reports.php');
include_once('../data/get_people.php');
include_once('../data/get_user.php');
include_once('../data/get_company.php');
include_once('../data/get_reports_company_xref.php');

?>

g_the_only_company = g_company[0];
g_valuation_used = (g_the_only_company[g_company.ValuationUsed] == '1');
g_billing_used = (g_the_only_company[g_company.BillDateUsed] == '1');
g_source_used = (g_the_only_company[g_company.SourceUsed] == '1');
g_source2_used = (g_the_only_company[g_company.Source2Used] == '1');
g_viewExpectedBilling = g_billing_used && (g_the_only_company[g_company.ViewExpectedBilling] == '1');
g_isSales = g_user[0][g_people.Level] == '1';


var ssReports = null;
function onLoaded()
{
	var reports = getReports();
	var selectbox = '<select name="reportbox" id="reportbox"><option></option>';
	for(var option in reports) {
		if(typeof(reports[option][1]) != 'undefined') selectbox += '<option value="' + reports[option][0] + '">' + reports[option][1] + '</option>';
	}
	
	document.getElementById('selectbox').innerHTML = selectbox;
}


function sortName(a, b)
{
	if(a[0] > b[0]) return 1;
	if (a[0] < b[0]) return -1;
	return 0;

}


function positionSelect()
{
	if (ssReports) ssReports.positionControls();
}

function ok()
{
	var which = document.getElementById('reportbox').value;
	var urlstr = '../reports/' + which;
	var sep = which.indexOf('?') == -1 ? '?' : '&';
	urlstr += sep + 'SN=<?=$_REQUEST['SN']?>';
	window.location.href = urlstr;
}


function allowSalesView(data)
{
	var ret = 1;
	if(g_isSales)
	{
		ret = 0;
		if (data[g_reports.Salespeople] == '0') return 0;
		for (var i = 0; i < g_reports_company_xrefs.length; i++)
		{
			if(g_reports_company_xrefs[i][g_reports_company_xrefs.ReportID] == data[g_reports.ReportID])
				return (g_reports_company_xrefs[i][g_reports_company_xrefs.Salespeople] == '1');
		}
	}

	return ret;
}


function getReports()
{
	var ret = Array();
	for (var i = 0; i < g_reports.length; ++i)
	{
		var data = g_reports[i];
		var nameRep = data[g_reports.Name];
		if(!g_valuation_used && -1 < nameRep.indexOf('Valuation'))
			continue;
		if(!g_source_used && nameRep == 'Source')
			continue;
		if(!g_source2_used && nameRep == 'Source2')
			continue;
		if(!g_billing_used && -1 < nameRep.indexOf('Billing'))	// can't see billing reports if no billing
			continue;
		if (g_isSales)
		{
			if (!(g_viewExpectedBilling && -1 < nameRep.indexOf('Billing')))
			{
				if(!allowSalesView(data))
					continue;
			}
		}
		if (-1 < nameRep.indexOf('Source2'))
			data[g_reports.Name] = g_the_only_company[g_company.Source2Label];
		if (data[g_reports.Develop] != 1)
		{
			var tmp = Array();
			tmp[0] = data[g_reports.FileName];
			tmp[1] = substituteCategoryLabels(data[g_reports.Name]);
			tmp[1] = replaceAll(tmp[1], '{{Valuation}}', g_the_only_company[g_company.ValLabel]);
			tmp[1] = replaceAll(tmp[1], '{{Billing}}', g_the_only_company[g_company.BillDatePrompt]);
			if (data[g_reports.Develop] == 1)
				tmp[1] = '(dev) '+tmp[1];

			ret[ret.length] = tmp;
		}
	}

	ret.sort(function(a, b)
	{
		var sA = new String(a[1]).toUpperCase();
		var sB = new String(b[1]).toUpperCase();
		if(sA > sB) return 1;
		if(sA < sB) return -1;

		return 0;
	});

	return ret;

}


// -->
</script>
<form name="form1">
<table border=0>
<tr>
	<td rowspan=4 width=100>&nbsp;</td><td></td><td></td><td rowspan=4 width="50%" align="right" valign="top"><br /><a href="<?= SessionManager::CreateUrl('which_report.php')?>" target="_blank">Open in a new window</a></td>
</tr>
<tr>
	<td><img src="../images/reports.gif" align="left"></td>
	<td class="title" valign="middle">Reports</td>
</tr>
<tr>
	<td class="plabel">Reports</td>
	<td>
		<div id="selectbox"></div>
	</td>
</tr>

<tr valign="bottom">
	<td>&nbsp;</td>		
	<td>
		<input type="button" class="sized_command" id='btnOK' name='btnOK' value="Ok" onclick="javascript:ok()">
	</td>
</tr>

</table>
</form>
</body>
</html>
