var perline = 9;
var curId;
var colorLevels = Array('0', '3', '6', '9', 'C', 'F');
var current_color_picker = '';
var colorArray = new Array();

function setColor(color) {		
	
	var link = document.getElementById(curId);
	var field = document.getElementById(curId + 'field');
	var picker = document.getElementById(current_color_picker);
	
	field.value = color;
	link.style.background = color;
	link.style.color = color;
	
	picker.style.display = 'none';	
	document.getElementById(curId + '_div').innerHTML = '';
}


function pickColor(id) {
	
	var colorArray = new Array("lightsalmon", "lightcoral", "salmon", "coral", "mistyrose", "pink", "lavender", "plum", "violet",
					   "orchid", "magenta",	"lightblue", "lightsteelblue", "lightskyblue", "deepskyblue", "paleturquoise", "aquamarine",
					   "yellowgreen", "gainsboro", "silver", "darkgray", "aqua", "lightgreen", "greenyellow", "gold", "orange",
					   "darkorange", "peru", "mediumspringgreen", "lawngreen", "palegoldenrod",	"khaki", "burlywood", "tan", "sandybrown", "goldenrod");	


	var tempid = 1;
	while (document.getElementById('pick_' + tempid + '_div')) {
		if('pick_' + id != 'pick_' + tempid) {
			document.getElementById('pick_' + tempid + '_div').innerHTML = '';
		}
		tempid++;	
	}

	var elemDiv = document.createElement('div');
	
	try {
		var tid = 1;
		while(document.getElementById('pick_' + tid + 'field')) {
			var temp_color = document.getElementById('pick_' + tid + 'field').value;
			
			for(var i in colorArray) {
				if (colorArray[i] == temp_color) {
					var t = colorArray.splice(i,1);
				}
			}
			tid++;		
		}
	} catch(e){}
	
	colorArray.sort();	
	

	 var colors = colorArray;
	 var tableCode = '';
	 tableCode += '<table border="0" cellspacing="1" cellpadding="1">';
	 for (i = 0; i < colors.length; i++) {
		  if (i % perline == 0) { tableCode += '<tr>'; }
		  tableCode += '<td bgcolor="#000000"><a style="outline: 1px solid #000000; border:1px solid black; color: '
			  + colors[i] + '; background: ' + colors[i] + ';font-size: 16px;" title="'
			  + colors[i] + '" href="javascript:setColor(\'' + colors[i] + '\')">&nbsp;&nbsp;&nbsp;</a></td>';
		  if (i % perline == perline - 1) { tableCode += '</tr>'; }
	 }
	 if (i % perline != 0) { tableCode += '</tr>'; }
	 tableCode += '</table>';


	current_color_picker = 'colorpicker' + 'pick_' + id;
	elemDiv.id = 'colorpicker' + 'pick_' + id;
	elemDiv.style.display = 'none';

	elemDiv.innerHTML = '<div class="color_div" style="font-family:Verdana; font-size:11px;"><span><nobr>Please select a color</nobr></span>'          	
		+ tableCode
		+ '</div>';
	
	document.getElementById('pick_' + id + '_div').appendChild(elemDiv);
	
	
	var picker = document.getElementById(current_color_picker);
	if ('pick_' + id == curId && picker.style.display == 'block') {
		picker.style.display = 'none';			
		return;
	}
	curId = 'pick_' + id;
	picker.style.display = 'block';
}



 
