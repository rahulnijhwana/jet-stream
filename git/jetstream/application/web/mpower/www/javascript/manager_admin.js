var is_seasonality_valid = true;

$(document).ready(function(){	
	$("input").attr("disabled", "disabled");
	$("#GoBtn").attr("disabled", "");
	calculateSeasonality();
	$("div.info").fadeIn('slow');	
});

function doSubmit() {	
	var data = $("form").serialize();
	
	calculateSeasonality();
	if (!is_seasonality_valid) {
		alert('Seasonality must total to 100%.');
		return;
	}
	
	$.post("ajax/save_manager_admin.php?SN=" + sn, data,
	function(data){
		if(data) {}
	});
	
	intialSate();
}


function calculateSeasonality() {
	is_seasonality_valid = true;
	var id = 1;
	
	while (document.getElementById('Sesonality_' + id + '_1')) {
		
			var total = 0;
			for (var i = 1; i <= 13; ++i) {			
				if (i == 13 && (document.getElementById('extra_period_' + id).style.display == 'none')) {
					total = total;
				} else {
					var seasonality_value = parseInt(document.getElementById('Sesonality_' + id + '_' + i).value);
					if (isNaN(parseInt(seasonality_value))) seasonality_value = 0;
					
					total += seasonality_value;
				}
			}
			
			if (total == 0) total_display = '-';
			else total_display = total + '%';
			
			document.getElementById('Sesonality_total_' + id).innerHTML = total_display;
			
			if (total != 100 && total != 0) {
				$('#Sesonality_total_' + id).css('color','red');
				if (document.getElementById('Sesonality_check_' + id).checked) {
					is_seasonality_valid = false;
				}
			} else {
				$('#Sesonality_total_' + id).css('color','black');
			}
		
		id++;
	}	
}

function checkGoal(id){
	var fieldvalue = $('#' + id).val();	
	$('#' + id).val(formatCurrency(fieldvalue));
}

function edit() {
	$("input").attr("disabled", "");
	$('#submit').show();
	$('#cancel').show();	
	checkSeasonality();
}

function intialSate() {
	$("input").attr("disabled", "disabled");
	$("#GoBtn").attr("disabled", "");
	$('#submit').hide();
	$('#cancel').hide();
	
	
}

function doCancel() {
	intialSate();
	calculateSeasonality();
}

/**
 * This function converts a given number to money format. It converts to US mony format
 *
 * @param {String} numb. It holds the amount string.
 */
function formatCurrency(numb){
	var num = numb.toString().replace(/\,/g,'');
	if (num.charAt(0) == '$') num = num.substr(1);
	num = parseFloat(num,10);
	if (isNaN(parseInt(num))) num = 0;

	var sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	var cents = num%100;
	num = Math.floor(num/100).toString();
	if (cents < 10){
		cents = "0" + cents;
	}
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++){
		num = num.substring(0,num.length-(4*i+3))+','+	num.substring(num.length-(4*i+3));
	}	
	
	return (((sign)?'':'-') + '$' + num);
}

function checkSeasonality() {
		var id = 1;
		while (document.getElementById('Sesonality_check_' + id)) {
			SeasonalityCheck('Sesonality_check_' + id, id, false);
			id++;
		}
}

/**
 * This function checks if the check box is checked then enable seasonality else not
 *
 * 
 */
function SeasonalityCheck(id, rowid, checked){	

	if (total_period == 13) {		
		var seasonality_values = new Array(7, 8, 8, 8, 7, 8, 8, 8, 7, 8, 8, 8, 7);
	} else {
		var seasonality_values = new Array(8, 8, 9, 8, 8, 9, 8, 8, 9, 8, 8, 9);		
	}
	
	var sesonality_total = $('#Sesonality_total_' + rowid).html();
	
	if (document.getElementById(id).checked) {
		var i = 1;
		while (document.getElementById('Sesonality_' + rowid + '_' + i)) {
			$('#Sesonality_' + rowid + '_' + i).attr("disabled", "");
			
			if(sesonality_total == '-' && checked)
				$('#Sesonality_' + rowid + '_' + i).val(seasonality_values[i -1]);
			
			i++;
		}		
		$('#equal_sign_' + rowid).attr("disabled", "");
		$('#Sesonality_total_' + rowid).attr("disabled", "");
	}
	else {
		var i = 1;
		while (document.getElementById('Sesonality_' + rowid + '_' + i)) {
			$('#Sesonality_' + rowid + '_' + i).attr("disabled", "disabled");
			i++;
		}
		
		$('#equal_sign_' + rowid).attr("disabled", "disabled");
		$('#Sesonality_total_' + rowid).attr("disabled", "disabled");
	}
	
	calculateSeasonality();
}

function onFocusDollar(obj){
	var len=0;
	if (document.selection)
		len=document.selection.createRange().text.length;
	else if (obj.selectionStart || obj.selectionStart==0)
		len=obj.selectionEnd-obj.selectionStart;
	oldDollarValue=obj.value;
	obj.value=unformat_money(obj.value);
	if (len) obj.select();
	}

function onBlurDollar(obj){
	obj.value=format_money(obj.value);
	//update total right and bottom 
	
	}

function moneyonly(myfield, e, dec){
    var key;
    var keychar;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
       return true;
    keychar = String.fromCharCode(key);
   
	// control keys
    if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
       return true;
    // numbers
    else if ((("0123456789").indexOf(keychar) > -1))
       return true;
    // decimal point jump
    else if (keychar == "."){
    //   myfield.form.elements[dec].focus();
       return true;
      }
    else
       return false;
	}

function unformat_money(amt){
	amt = amt.toString();
	amt = amt.replace(/\$/g, '');
	amt = amt.replace(/,/g, '');
	return amt;
}

function format_money_cents(amt){
	amt = amt.toString();
	if (amt == '&nbsp;')
		return amt;
	var ind=amt.indexOf('.');
	var cents = '';
	if (ind >=0)
	{
		cents = amt.substr(ind+1);
		amt = amt.substr(0, ind);
		if(cents.length < 2)
			cents += 0;
		else if (cents.length > 2)
			cents = cents.substr(0, 2);
	}
	else
		cents = '00';
	var neg=amt.charAt(0)=='-';
	if (neg) amt=amt.substr(1);

	var ret = '';
	for (var i = 0; i < amt.length; ++i)
	{
		if (i && (i % 3) == (amt.length % 3))
			ret += ',';
		ret += amt.charAt(i);
	}

	if (neg) ret='-'+ret;
	if (ret) ret += '.' + cents;

	if ( typeof(g_the_only_company) == 'undefined' || (g_the_only_company[g_company.MonetaryValue]=='1'))
	{
		if (ret == '')
			return ret;
		else
			return '$' + ret;
	}
	else return ret;
}

function format_money(amt){
	amt = amt.toString();
	if (amt == '&nbsp;')
		return amt;

	var ind=amt.indexOf('.');
	if (ind >=0) amt=amt.substr(0,ind);

	var neg=amt.charAt(0)=='-';
	if (neg) amt=amt.substr(1);

	var ret = '';
	ret = commas(amt);
	if (neg) ret='-'+ret;

	if ( typeof(g_the_only_company) == 'undefined' ||g_the_only_company[g_company.MonetaryValue]=='1')
	{
		if (ret == '')
			return ret;
		else
			return '$' + ret;
	}
	else return ret;
}

function commas(amt){
	amt = unformat_money(amt);
	var ind=amt.indexOf('.');
	var cents = '';
	if (ind >= 0)
	{
		cents = amt.substr(ind+1);
		amt = amt.substr(0, ind);
	}
	var ret = '';
	for (var i = 0; i < amt.length; ++i)
	{
		if (i && (i % 3) == (amt.length % 3))
			ret += ',';
		ret += amt.charAt(i);
	}
	if(cents.length) ret += '.' + cents;
	return ret;

}

function updateValues(id){
	
	var total = 0 ; 
	var i = 0 ; 
	var y = document.getElementsByName('total_'+ id );
	
	for (i = 1 ; i <=12 ; i++ ){
		var x = document.getElementById('goal_' + id + '[' + i + ']');
		total = parseInt(total) + parseInt(unformat_money(x.value)) ;  
	}
	
	y[0].value = format_money(total)  ; 
} 

