<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));
require_once(BASE_PATH . '/class.Header.php');
require_once(BASE_PATH . '/class.Footer.php');
require_once(BASE_PATH . '/class.Calendar.php');
require_once(BASE_PATH . '/class.Salespeople.php');
require_once(BASE_PATH . '/class.SessionManager.php');
require_once BASE_PATH . '/include/class.ReportingTree.php';
require_once(BASE_PATH . '/include/class.Language.php');
require_once(BASE_PATH . '/lib.html.php');

SessionManager::Init();
SessionManager::Validate();

$page_title = 'M-Power&trade; Calendar';

$header = new Header();
$footer = new Footer();

//$header->SetSubtabs($_SESSION['tree_obj']->GetLimb($_SESSION['login_id']));

$salespeople = new Salespeople();
$salespeople->type = 'calendar';
$calendar = new Calendar();

$target = (isset($_GET['target'])) ? $_GET['target'] : (isset($_SESSION['target'])) ? $_SESSION['target'] : false;

$selected_cals = array();

if (isset($_POST['checked'])) {
	$selected_cals = split(' ', trim($_POST['checked']));
} elseif ($target && $_SESSION['tree_obj']->IsViewable($target)) {
	$selected_cals[] = $target;
}

$salespeople->checked = $selected_cals;

$calendar->target = $_SESSION['tree_obj']->GetTarget();

CreatePage($page_title, array($header, $salespeople, $calendar, $footer));
