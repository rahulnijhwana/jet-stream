<?php

define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

require_once BASE_PATH . '/include/mpconstants.php';
require_once BASE_PATH . '/class.SessionManager.php';
require_once BASE_PATH . '/class.Header.php';
require_once BASE_PATH . '/class.ShowTree.php';
require_once BASE_PATH . '/class.Footer.php';
require_once BASE_PATH . '/include/class.ReportingTree.php';
require_once BASE_PATH . '/include/class.Language.php';
require_once BASE_PATH . '/lib.html.php';
SessionManager::Init();
SessionManager::Validate();

$page_title = 'M-Power&trade;';

$header = new Header();
$header->NoTabs();

$tree = new ShowTree();

$footer = new Footer();

CreatePage($page_title, array($header, $tree, $footer));
