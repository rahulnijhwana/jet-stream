<?php

define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

require_once(BASE_PATH . '/include/mpconstants.php');
require_once(BASE_PATH . '/class.SessionManager.php');
require_once(BASE_PATH . '/class.Header.php');
require_once(BASE_PATH . '/class.ForecastAssistant.php');
require_once(BASE_PATH . '/class.Footer.php');
require_once BASE_PATH . '/include/class.ReportingTree.php';
require_once(BASE_PATH . '/include/class.Language.php');
require_once(BASE_PATH . '/lib.html.php');
SessionManager::Init();
SessionManager::Validate();

$page_title = 'M-Power&trade; Forecast Assistant';

$header = new Header();


$target = (isset($_GET['target'])) ? (int)$_GET['target'] : $_SESSION['login_id'];

$forecast = new ForecastAssistant();
$forecast->target = $target;

$footer = new Footer();

if (isset($_GET['download'])) {
	$forecast->download = $_GET['download'];
	$forecast->Download();
} else {
	CreatePage($page_title, array($header, $forecast, $footer));
}
