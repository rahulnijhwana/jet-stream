<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

require_once(BASE_PATH . '/include/mpconstants.php');
require_once(BASE_PATH . '/class.SessionManager.php');
require_once(BASE_PATH . '/class.Header.php');
require_once(BASE_PATH . '/class.Footer.php');
require_once BASE_PATH . '/include/class.ReportingTree.php';
require_once(BASE_PATH . '/include/class.Language.php');
require_once(BASE_PATH . '/lib.html.php');
SessionManager::Init();
SessionManager::Validate();

$salesperson_name = $_SESSION['tree_obj']->GetInfo($_SESSION['login_id'], 'FirstName').' '.$_SESSION['tree_obj']->GetInfo($_SESSION['login_id'], 'LastName');
$salesperson_email = $_SESSION['tree_obj']->GetInfo($_SESSION['login_id'], 'Email');

$page_title = 'M-Power&trade; Help';

$header = new Header();
$header->SetTitle($_SESSION['tree_obj']->GetPath($_SESSION['login_id']));
$footer = new Footer();


$css = <<<CSS
<style type="text/css">
.qfi {
	border: 1px solid #BFBFBF;
	margin-bottom: 2px;
	padding: 0 3px;
	width: 185px;
	color: #666;
}
.page_head {
	font-size: 24px;
	padding-bottom: 16px;
	color: #009966;
}
.qta {
	margin-top: 10px;
	border: 1px solid #BFBFBF;
	width: 450px;
	height: 80px;
	color: #666;
	padding: 2px 3px;
}

</style>
CSS;

$script =<<<SCRIPT
<script src="javascript/jquery.js"></script>
<script language="javascript">

$(document).ready(function(){
	$("div.info").fadeIn('normal')
});

function validate() {
	
	$("#status").hide('fast');

	var name = document.getElementById('name').value;
	var email = document.getElementById('email').value;
	var phone = document.getElementById('phone').value;
	var subject = document.getElementById('subject').value;
	var issue = document.getElementById('issue').value;
	var sn = '{$_REQUEST['SN']}';

	var error = false;
	if(name == '') {
		 $("#name").fadeOut("slow").css("border","1px solid red").fadeIn("normal");
		error = true;
	}else {
		$("#name").css("border","1px solid #BFBFBF");
	}

	emailpat = /^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9])+(\.[a-zA-Z0-9_-]+)+$/;

	if(email == '' || !emailpat.test( email )) {
		$("#email").fadeOut("slow").css("border","1px solid red").fadeIn("normal");
		error = true;
	}else {
		$("#email").css("border","1px solid #BFBFBF");
	}

	if(issue == '') {
		 $("#issue").fadeOut("slow").css("border","1px solid red").fadeIn("normal");
		error = true;
	}else {
		$("#issue").css("border","1px solid #BFBFBF");
	}

	if (subject == '') subject = 'New Submission';


	if(!error) {
		$("#submit").attr("disabled","true");

		$.post("ajax/sendemail.php?SN=" + sn, { 'name': name, 'email': email, 'phone' : phone, 'subject':subject, 'issue':issue},
		  function(data){			
			if(data == 'OK') {
				$("#status").text('Thank you for submitting your issue. Our suport team will contact you shortly.').fadeIn('slow').css('color','#820000');	
				$("#phone").attr("value","");
				$("#subject").attr("value","");
				$("textarea").attr("value","");
			}
			else {
				$("#status").text('There is an error in sending the email. Please try again later or call.').fadeIn('slow').css('color','red');				
			}
			$("#submit").attr("disabled","").fadeIn();
		  });
	} 
}

</script>
SCRIPT;


$contact_form = <<<EMAIL

<div id="status" style="display:none; padding-bottom:20px"></div>
<div id="contactdiv">
<form id="send">
<table style="border:0;margin-left:40px;">
<tr><td style="width:80px;">Name</td><td><input type="text" class="qfi" id="name" value="$salesperson_name" disabled="true"></td></tr>
<tr><td>Email</td><td><input type="text" class="qfi" id="email" value="$salesperson_email"></td></tr>
<tr><td>Phone </td><td ><input type="text" class="qfi" id="phone"></td></tr>
<tr><td>Subject </td><td><input type="text" class="qfi" style="width:450px" id="subject"></td></tr>
<tr><td>Issue </td><td>
<textarea cols="3" rows="5" class="qta" id="issue"></textarea>
</td></tr>
<tr><td colspan="2" align="center" style="text-align:center;"><input type="button" class="button" name="submit" id="submit" value="Submit" onClick="validate();"></td></tr>
</table>
</form>
</div>
<div style="clear:both"></div>
EMAIL;

$help_body = '<h2 style="margin-top:0">M-Power<span style="font-size: 50%;vertical-align: sub;">&trade;</span> Help</h2>
<p>Support is available Monday thru Friday 7am to 6pm (CST).</p>
<h3>Contact Support</h3>
<p>Phone: (773)714-8000</p><p>Email:  Direct at <a href="mailto:support@mpasa.com">support@mpasa.com</a> or use this form:</p>' . $contact_form . '
<h3>Other Info</h3>
<p><a href="http://www.asasales.com/client/home.php?section=manuals" target="manual">User Manuals</a></p>';



$help_body = $css . $script . $help_body;
$help_body = CreateTextDiv($help_body);

CreatePage($page_title, array($header, $help_body, $footer));

