<?php

require_once BASE_PATH . '/include/class.MpRecord.php';

class RecContact extends MpRecord
{

	public function Initialize() {
		$this->db_table = 'Contact';
		$this->recordset->GetFieldDef('ContactID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('CompanyID')->required = TRUE;
		parent::Initialize();
	}
}
