<?php
require_once 'constants.php';
require_once 'mpconstants.php';
require_once JETSTREAM_ROOT . '/../include/class.GoogleConnect.php';
require_once JETSTREAM_ROOT . '/../slipstream/class.Dashboard.php'; //used to get client data

// for Map lookup for db column's
require_once JETSTREAM_ROOT . '/../include/class.MapLookup.php';
require_once JETSTREAM_ROOT . '/../slipstream/class.CompanyContactNew.php';

//require_once 'Zend/Gdata/Contacts.php';
//require_once 'Zend/Gdata_OAuth_Helper.php';

class GoogleContactBatchPush {

    private $client;
    private $contacts;
   
    private $contactService;  

    private $contactGroupID;
    private $companyid;
    private $userid;
    private $consumer;
    private $flatFile;
    private $totalCount;
    private $partitionCount = 100;
    private $maps = array(); 
    private $debug = false;
    private $aMapWithGoogle;

    public function __construct($client, $companyid, $userid, $mode = 'insert') {

        if ( $_GET['debug'] == true ) {
            $this->debug = true;
        }

        $this->aMapWithGoogle= unserialize( CONTACT_MAP );

        if ( $this->debug ) {
            error_log('Contact Map File ===>');
            error_log(print_r($this->aMapWithGoogle,true));
        }

        // besso for test
        //$companyid = 430;
        //$userid = 7329;
        
        // TODO set php.ini
        ini_set('memory_limit', '-1');
        set_time_limit(1800);

        $this->client = $client;
        $this->contacts = $client;

        $mirrorService = new Google_MirrorService($this->client);
        $this->contactService = $mirrorService->contacts;


        $this->companyid = $companyid;
        $this->userid = $userid;

        $this->setContactEmail();
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		/*$url = 'https://www.google.com/m8/feeds/contacts/default/full';
        $req = new Google_HttpRequest($url);
        $req->setRequestHeaders(array('GData-Version'=> '3.0','content-type'=>'application/atom+xml; charset=UTF-8; type=feed'));
        $val = $this->contacts->getIo()->authenticatedRequest($req);
        $response =$val->getResponseBody();
        $xml = simplexml_load_string($response);
        var_dump($xml);exit;*/
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        /* contact group id is the ID of name 'Jeststream' at google side
         * to lessen google request, store in googleToken table
         */
        if ('' == $this->contactGroupID) {
         //   $this->contactGroupID = $this->getContactGroup();
         /*Start JET-37 */
            $this->contactGroupID = $this->getContactGroupNew();
            /*End JET-37 */
            //exit;
            if ( $this->contactGroupID == '' ) {
                error_log( 'Google contact id "Jetstream" is not found' );
                return false;
            }
        }
       // var_dump($this->contactGroupID);exit;
        if ( $this->debug ) {
            error_log('contact group id : ' . $this->contactGroupID);
        }

        // insert is fired just once when user create Token
        if ( $mode == 'insert' ) {
            // TODO check if groupid is created well
            if ('' != $this->contactGroupID) {
                $aClientContact = $this->prepareClientContact();
            }
        }

			
    }


    private function prepareClientContact() {

        /***
        // TODO block who has massive contact
        $this->totalCount = $this->getClientContactsCount();
        if ( $this->debug ) {
            error_log('Total count to insert to google : ' . $this->totalCount);
        }
        ***/

        //if ( $this->totalCount > 10000) return;

        //$this->createViewTable();
        //$file = '/webtemp/contact_' . $this->companyid . '_0';
        //if ( !file_exists($file) ) {
            $this->getMapColumns();
            //echo '<pre>'; print_r($this->maps); echo '</pre>'; exit;
            if ( $this->debug ) {
                error_log( 'Map for company : ' . $this->companyid . ' ===>');
                error_log( print_r($this->maps, true) );
            }
        //}

        if ( !$this->createFlatFile() ) {
            error_log('Fail to create flat file : ' . $this->flatFile );
            return false;
        }

        // TODO later with file partitioning
        $this->transactGoogleContactPush();

        return true;

        // parsing data for google usage
        //$aParsed = $this->parseClientContact($clientContacts);
        //echo '<pre>'; print_r($aParsed); echo '</pre>';
    }

    private function transactGoogleContactPush() {
        $dir = '/webtemp';
        $filePrefix = 'contact_' . $this->companyid;
        if ($handle = opendir($dir)) { 
            while (false !== ($file = readdir($handle))) {
                
                if ($file == '.' || $file == '..') { 
                    continue; 
                } 
                if ( strstr($file, $filePrefix) ) {
                    $file = $dir . '/' . $file;
                    /***
                    $file = $dir . '/' . $file;
                    $url = 'https://www.mpasatest.com/jetstream/ajax/ajax.googleContactBatchOneFile.php?userid=' . $this->userid . '&companyid=' . $this->companyid . '&file=' . $file;
                    $command = "/usr/bin/curl --insecure --request GET '$url'";
                    shell_exec(sprintf('%s > /dev/null 2>&1 &', $command));
                    ***/
                    
                    $this->push($file);

                } 
            } 
            closedir($handle); 
            //error_log('massive push done : besso');
        } 
    }

    public function push($file) {

        $feed = $this->generateGoogleXmlHeader();
        $idx = 0;
        $aContact = array();
        $fp = fopen($file, 'r');
        while (($line = fgets($fp)) !== false) {
            $c = json_decode($line);
            $c = (array)$c;
            $aContact[] = $c;
            $xml .= $this->generateGoogleXml($c);
        }
        $feed .= $xml;
        $feed .= $this->generateGoogleXmlFooter();

        try {
            $contactid = isset($c['ContactID']) ? $c['ContactID'] : '';

            $len = strlen($feed);
            $add = new Google_HttpRequest("https://www.google.com/m8/feeds/contacts/".$this->contactUserEmail."/full/batch");
            $add->setRequestMethod("POST");
            $add->setPostBody($feed);
            $add->setRequestHeaders(array('content-length' => $len, 'GData-Version'=> '3.0','content-type'=>'application/atom+xml; charset=UTF-8; type=feed'));

            $submit = $this->contacts->getIo()->authenticatedRequest($add);

            $sub_response = $submit->getResponseBody();
            $code = $submit->getResponseHttpCode();
            if ( substr($code,0,1) != 2 ) {
                error_log('Error while inserting contact (' . $this->companyid . ') ===>');
                error_log(print_r($c,true));
                error_log('Error message : ' . $sub_response);
                error_log("\n");
                return false;
            }

            $response = simplexml_load_string($sub_response);

            //echo '<pre>'; print_r($response); echo '</pre>';

            $aEntry = $response->entry;
            //echo '<pre>'; print_r(count($aEntry)); echo '</pre>';

            echo '<pre>'; print_r('Batch pushed for ' . $file); echo '</pre>';
            error_log($file);

            $i = 0;
            foreach( $aEntry as $entry ) {

                $title = $entry->title;
                if ( $title == 'Error' ) {
                    $i++;
                    continue;
                }

                $contactid = $aContact[$i]['ContactID']; 

                $aClient = explode("base/",$entry->id);
                //echo '<pre>'; print_r($aClient); echo '</pre>';
                $googleContactId = $aClient[1];

                if ( $googleContactId == '' ) {
                    $i++;
                    continue;
                }

                $sql = 'Insert GoogleContact (PersonID, ContactID, GoogleContactId) values (?, ?, ?)';
                $params = array();
                $params[] = array(DTYPE_INT, $this->userid);
                $params[] = array(DTYPE_INT, $contactid);
                $params[] = array(DTYPE_STRING, $googleContactId);
                $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
                //echo '<pre>'; print_r($sql); echo '</pre>';
                $status = DbConnManager::GetDb('mpower')->Exec($sql);

                $i++;
            }

        } catch (Exception $e) {
            error_log('Error while google contact sync on file');
            error_log(print_r($e,true));
            continue;
        }

        unlink($file);
        fclose($fp);


        /******** 
         * TODO for bulk, let's change it as CLI
        $url = 'https://www.mpasatest.com/jetstream/googleContactPush.php?file=' . $file. '&userid=' . $this->userid . '&groupid=' . $this->contactGroupID;
        error_log($url);

        $command = "/usr/bin/curl --insecure --request GET '$url'";
        shell_exec(sprintf('%s > /dev/null 2>&1 &', $command));
        return true;
        ******/
    }

    /* set $this->maps member */
    public function getMapColumns() {
		//$sql = "Select googleFieldName,googleField from googleFields";
		//$aContact = DbConnManager::GetDb('mpower')->Exec($sql);
        foreach ( $this->aMapWithGoogle as $type => $aSection ) {
            foreach ( $aSection as $section => $aLabel ) {
                foreach ( $aLabel as $gLabel => $cLabel ) {
					//$lab[] = $cLabel;
                    $fieldname = MapLookup::GetFieldDefByGoogleField($this->companyid, $cLabel, $type);
                  // print_r($cLabel .'---'. $fieldname['FieldName'].'---'.$fieldname['googleField']); echo '<br/>';
                    if ( $fieldname != '' ) {
						if(isset($fieldname['googleField']))
                        $this->maps[$type][$section][$gLabel] = $fieldname['FieldName'].'_&_'.$fieldname['googleField'];
                        else
                        $this->maps[$type][$section][$gLabel] = $fieldname['FieldName'];
                       // $this->maps[$type][$section] = $fieldname['googleField'];
                        
                    }
                }
                
            }
        }
     //   exit;
      //  echo '</pre>';
      //  exit('outside');
        //print_r($lab);exit('kk');
       // echo '<pre>'; print_r($this->maps); echo '</pre>';exit;
        return true;
    }

    /* TODO move to common util later */
    public static function getValidFieldname ( $map, $name , $exclude = '' ) {
        $fieldname = '';
        foreach ( $map as $m ) {
            if ( strstr($m['LabelName'], $name) ) {
                if ( $m['FieldName'] == $exclude ) continue;
                if ( $m['value'] != '' ) {
                    $fieldname = $m['FieldName'];
                    break;
                }
            }
        }
        return $fieldname;
    }

    /***
    private function getClientContactsCount() {
        $sql = "Select count(contactId) as cnt from Contact Where CompanyID = ? And Deleted != 1";
        $params = array();
        $params[] = array(DTYPE_INT, $this->companyid);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aContact = DbConnManager::GetDb('mpower')->Exec($sql);
        return $aContact[0]['cnt'];
    }
    ***/

    /*
     * get contact & company list with CONTACT_MAP list
     */
    /*
    private function getContact() {
        $limit = 100;
        $sql = "Select top $limit ContactID, Text01, Text02, Text03, Text04, Text05,
            Text06, Text07, Text08, Text09, Text10, Text11,
            Text12, Text13, Text14, Text15, Text16, Text17,
            Text18, Text19, Text20 from Contact Where CompanyID = ? And Deleted != 1";
        $params[] = array(DTYPE_INT, $this->companyid);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aContact = DbConnManager::GetDb('mpower')->Exec($sql);
        return $aContact;
    }
    */

    /*
     * select count(*) from contact , account where contact.companyid = account.companyid and contact.companyid = 525
     */
    private function generateSql( $contactid = '' ) {
        $isFirst = true;
        $aColumnResult = array();
        $sql .= "SELECT Contact.ContactID, ";
        foreach ( $this->maps as $tableName => $aSection) {
            foreach ( $aSection as $section => $aColumn ) {
                foreach ( $aColumn as $alias => $column ) {
					
					if (strpos($column,'_&_') !== false) {
							$column = preg_replace("/_&_/"," AS '",$column);
							$aColumnResult[] = "$tableName.$column'";
						}else{
                    $aColumnResult[] = $tableName.'.'.$column. ' AS '. $section . '_' . $alias;
					}
                }
            }
        } 
        $columns = implode(',',$aColumnResult);
        $sql .= " $columns FROM CONTACT ";
        $sql .= " LEFT JOIN ACCOUNT ON ACCOUNT.AccountID = CONTACT.AccountID" ;
        $sql .= " WHERE CONTACT.CompanyID = '". $this->companyid ."' and Contact.Deleted != 1 and Contact.Inactive != 1 and PrivateContact != 1";
        if ( $contactid != '' ) {
            $sql .= " and CONTACT.ContactId = '$contactid'";
        }
        //print_r($sql);exit;
        return $sql;
    }

    public function getContacts($contactid = '') {

        //MSSQL connection string details.
        if ( ENV == ENV_STAGING ) {
            $db = mssql_connect(MP_DATABASE_SERVER . ':1433', MP_DATABASEU, MP_DATABASEP);
        } else {
            $db = mssql_connect(MP_DATABASE_SERVER, MP_DATABASEU, MP_DATABASEP);
        }
        if (!$db) {
            die('Could not connect to mssql');
        }
        $db_selected = mssql_select_db(MP_DATABASE, $db);
        if (!$db_selected) {
            die('Could not select mssql db');
        }

        $sql = $this->generateSql($contactid); 
        $results = mssql_query($sql, $db);
        //mssql_close($db);

        return $results;
    }

    /* Not use file management but use as main file */
    private function createFlatFile() {

        $suffix = 0;
        $file = '/webtemp/contact_' . $this->companyid . '_' . $suffix;
        if ( file_exists($file) ) {
            return true;
        }

        $results = $this->getContacts();
        $total = mssql_num_rows($results);

        error_log( 'Total count for ' . $this->companyid . ' is ' . $total );
        /*
        if ($total > 10000) {
            return;
        }
        */

        $idx = 1;
        $suffix = 0;
        $aTransaction = array();
        while ($l = mssql_fetch_array($results, MSSQL_ASSOC)) {

            //$this->push( $l );

            $json = json_encode($l);

            $out = json_encode($l);
            $out .= "\n";
            if ( $idx == 1 
                || ( $idx > $this->partitionCount && ( $idx % $this->partitionCount ) == 1 ) ) {
                $file = '/webtemp/contact_' . $this->companyid . '_' . $suffix;
                $fp = fopen($file, 'w');
                $suffix++;

            }

            fwrite($fp, $out);

            if ( $idx == 5 
                || ( $idx > 5 && ( $i % $this->partitionCount ) == ($this->partitionCount -1 ) ) ) {
                //fclose($fp);
            }

            $idx++;
        }
        return true;
    }

    private function getClientContacts($isView = false) {

        /*
        $search = array(
                'page_no' => '1',
                'records_per_page' => '999999',
                'assigned_search' => 0,
                'query_type' => 2,
                'search_string' => '',
                'inactive_type' => 0,
                'assign' => '',
                'sort_type' => '',
                'Advance' => '',
                'inclContacts' => '',
                'sort_dir' => ''
                );

        $oDashboard = new Dashboard($this->companyid, $this->userid);
        $oDashboard->Build($search);
        $aResult = $oDashboard->SearchResults();
        */

        /* TODO find out how to avoid full sanning of DB
         *
         */
        /*
        $sql = "Select * From (
                    Select *, ROW_NUMBER() over ( ORDER BY ContactID ) as ct
                      from Contact Where CompanyID = ? And Deleted != 1 ) sub
                 Where ct >= ? and ct <= ?";
        */
        if ($isView) {

            $table = 'VIEW_CONTACT_' . $this->companyid;

            $columns  = $this->maps['firstname'] . ' as fname, ';
            $columns .= $this->maps['lastname'] . ' as lname, ';
            $columns .= $this->maps['phone1'] . ' as phone1, ';
            $columns .= $this->maps['phone2'] . ' as phone2, ';
            $columns .= $this->maps['email1'] . ' as email1 ';
            $sql = "Select $columns from $table";

            $aContact = DbConnManager::GetDb('mpower')->Exec($sql);

        } else {
            $sql = "Select * from $table Where CompanyID = ? And Deleted != 1";
            $params[] = array(DTYPE_INT, $this->companyid);
            $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
            $aContact = DbConnManager::GetDb('mpower')->Exec($sql);
        }
        return $aContact;

    }

    // create view table first
    private function createViewTable() {
        $viewTableName = 'VIEW_CONTACT_' . $this->companyid;

        /* TODO delete cost too high for 200,000 rows
        $sql = 
        "IF OBJECT_ID('" . $viewTableName . "', 'V') IS NOT NULL
        BEGIN
            DROP VIEW " . $viewTableName . ";
        END";
        $status = DbConnManager::GetDb('mpower')->Exec($sql);
        echo '<pre>'; print_r($status); echo '</pre>';
        */

        $sql = 
        "IF OBJECT_ID('" . $viewTableName . "', 'V') IS NULL
        BEGIN
            EXECUTE('CREATE VIEW VIEW_CONTACT_".$this->companyid." AS Select ROW_NUMBER() over ( ORDER BY ContactID ) as idx, * from Contact Where CompanyID = ? And Deleted != 1')
        END";
        $params = array();
        $params[] = array(DTYPE_INT, $this->companyid);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $status = DbConnManager::GetDb('mpower')->Exec($sql);
        return true;
    }

    /* Get Jetstream group from google, if not create new Jetstream one */
    private function getContactGroup() {
        //error_log('call getContactGroup');
        $req = new Google_HttpRequest("https://www.google.com/m8/feeds/groups/default/full");
        // Google Contacts way
        $val = $this->contacts->getIo()->authenticatedRequest($req);

        // The contacts api only returns XML responses.
        $response =$val->getResponseBody();
        $xml = simplexml_load_string($response); // Convert to an ARRAY
        $sql = "Select Name from company where CompanyID = ? ";
        $params = array();
        $params[] = array(DTYPE_INT, $this->companyId);
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		$company = DbConnManager::GetDb('mpower')->Exec($sql);

        /* get author email and define as contactEmail */
        if ( isset ( $xml->author ) ) {
            $this->contactEmail = $xml->author->email;
        }

        $aEntry = array();
        $hasClient = false;
        
        if ( isset ( $xml->entry ) ) {
            $aEntry = $xml->entry;
            //var_dump($company[0]['Name'] == $e->title);exit;
            /* check 'Jetstream' group */
            foreach ( $aEntry as $e ) {
                if ( $company[0]['Name'] == $e->title ) {
                    /* ID is full url of contact group */
                    /* TODO no store in DB, lookup GoogleToken::contact_group_id */

                    return (string)$e->id;
                    break;
                }
            }
        }
        //var_dump($hasClient);exit;
        if ( $hasClient == false ) {
            return $this->createContactGroup();
        }
    }  
    /*Start JET-37 */
    private function getContactGroupNew() {
			//	print_r($_SESSION);exit;
		$sql = "Select contact_group_id from GoogleToken where PersonID = ? ";
        $params = array();
        $params[] = array(DTYPE_INT, $_SESSION['USER']['USERID']);
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		$contact_group_id = DbConnManager::GetDb('mpower')->Exec($sql);
		if(isset($contact_group_id)){
		return $contact_group_id[0]['contact_group_id'];
		}else{
			return createContactGroup();
			}
	}
/*END JET-37 */
    private function createContactGroup() {
		$sql = "Select Name from company where CompanyID = ? ";
        $params = array();
        $params[] = array(DTYPE_INT, $this->companyId);
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		$company = DbConnManager::GetDb('mpower')->Exec($sql);
        //error_log('call createContactGroup');
        $group = "
            <atom:entry xmlns:atom='http://www.w3.org/2005/Atom' xmlns:gd='http://schemas.google.com/g/2005'>
            <atom:category scheme='http://schemas.google.com/g/2005#kind' term='http://schemas.google.com/contact/2008#group'/>
            <atom:title type='text'>{$company[0]['Name']}</atom:title>
            <gd:extendedProperty name='more info about the group'>
            <info>Nice people.</info>
            </gd:extendedProperty>
            </atom:entry>";

        $len = strlen($group);
        $add = new Google_HttpRequest("https://www.google.com/m8/feeds/groups/".$this->contactEmail."/full/");
        //$add = new Google_HttpRequest("https://www.google.com/m8/feeds/groups/default/full/");
        $add->setRequestMethod("POST");
        $add->setPostBody($group);
        $add->setRequestHeaders(array('content-length' => $len, 'GData-Version'=> '3.0','content-type'=>'application/atom+xml; charset=UTF-8; type=feed'));
        $submit = $this->contacts->getIo()->authenticatedRequest($add);
       // var_dump($submit);exit;
        $sub_response = $submit->getResponseBody();

        $parsed = simplexml_load_string($sub_response);
        $aGroup = explode("base/",$parsed->id);

        $personId = $this->user;

        $googleContactId = $aGroup[1];

        $googleContactId = 'http://www.google.com/m8/feeds/groups/'. urlencode($this->contactEmail) .'/base/' . $googleContactId;
        $sql = 'Update googleToken set contact_group_id = ? Where personId = ? ';
        $params = array();
        $params[] = array(DTYPE_STRING, $googleContactId);
        $params[] = array(DTYPE_INT, $personId);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $status = DbConnManager::GetDb('mpower')->Exec($sql);

        if (!$status) {
            return false;
        } else {
            return $googleContactId;
        }
    }

    private function setContactEmail() {
        $url = 'https://www.google.com/m8/feeds/contacts/default/full';
        $req = new Google_HttpRequest($url);
        $req->setRequestHeaders(array('GData-Version'=> '3.0','content-type'=>'application/atom+xml; charset=UTF-8; type=feed'));
        // Google Contacts way
        $val = $this->contacts->getIo()->authenticatedRequest($req);

        // The contacts api only returns XML responses.
        $response =$val->getResponseBody();
        $xml = simplexml_load_string($response); // Convert to an ARRAY
        $this->contactUserEmail = urlencode($xml->id); // email address
        unset($xml);
    }

    private function preparePhoneMain($val) {
        return "<gd:phoneNumber rel='http://schemas.google.com/g/2005#main' primary='true'>$val</gd:phoneNumber>";
    }

    private function preparePhoneWork($val) {
        return "<gd:phoneNumber rel='http://schemas.google.com/g/2005#work' primary='false'>$val</gd:phoneNumber>";
    }

    private function preparePhoneWorkFax($val) {
        return "<gd:phoneNumber rel='http://schemas.google.com/g/2005#work_fax' primary='false'>$val</gd:phoneNumber>";
    }

    /* PObox and Neighborhood is not defined at google doc 
     * https://developers.google.com/google-apps/contacts/v3/
     */
    private function prepareStructuredPostalAddress($addressStreet, $addressPoBox, $addressNeighborhood, $addressCity, $addressProvince, $addressPostalCode, $addressCountry ) {
        $aFormattedAddress = array();
        $xml = 
        "<gd:structuredPostalAddress rel='http://schemas.google.com/g/2005#work' primary='true'>";
        if ( $addressStreet != '' ) {
            $xml .= "<gd:street>$addressStreet</gd:street>";
            //$aFormattedAddress[] = $addressStreet;
        }

        if ( $addressNeighborhood != '' ) {
            $xml .= "<gd:neighborhood>$addressNeighborhood</gd:neighborhood>";
            //$aFormattedAddress[] = $addressNeighborhood;
        }

        if ( $addressPoBox != '' ) {
            $xml .= "<gd:pobox>$addressPoBox</gd:pobox>";
            //$aFormattedAddress[] = $addressPoBox;
        }

        if ( $addressStreet != '' || $addressNeighborhood != '' || $addressPoBox != '' ) {
            $aFormattedAddress[] = $addressStreet . ' '  . $addressNeighborhood . ' ' . $addressPoBox;
        }

        if ( $addressCity != '' ) {
            $xml .= "<gd:city>$addressCity</gd:city>";
            $aFormattedAddress[] = $addressCity;
        }

        if ( $addressProvince != '' ) {
            $addressProvince = self::getOption($addressProvince);
            if ( $addressProvince == '0' ) {
                $addressProvince = ' ';
            }
            $xml .= "<gd:region>$addressProvince</gd:region>";
            $aFormattedAddress[] = $addressProvince;
        }
        if ( $addressPostalCode != '' ) {
            $xml .= "<gd:postcode>$addressPostalCode</gd:postcode>";
            $aFormattedAddress[] = $addressPostalCode;
        }
        if ( $addressCountry != '' ) {
            $addressCountry = self::getOption($addressCountry);
            $xml .= "<gd:country>$addressCountry</gd:country>";
            $aFormattedAddress[] = $addressCountry;
        } else {
            // hard code for US
            $xml .= "<gd:country>US</gd:country>";
        }

        $formattedAddress = implode(', ', $aFormattedAddress);

        if ( $formattedAddress != '' ) {
            $xml .= "<gd:formattedAddress>$formattedAddress</gd:formattedAddress>";
        }
        $xml .= "</gd:structuredPostalAddress>";
        return $xml;
    }

    private function prepareEmailHome($val) {
        return "<gd:email rel='http://schemas.google.com/g/2005#home' address='$val' />";
    }

    private function prepareEmailWork($val) {
        return "<gd:email rel='http://schemas.google.com/g/2005#work' address='$val' />";
    }

    private function preparePhoneMobile($val) {
        return "<gd:phoneNumber rel='http://schemas.google.com/g/2005#mobile'>$val</gd:phoneNumber>";
    }

    /*
    private function preparePhoneWork($val) {
        return "<gd:phoneNumber rel='http://schemas.google.com/g/2005#work'>$val</gd:phoneNumber>";
    }
    */

    private function preparePhoneHome($val) {
        return "<gd:phoneNumber rel='http://schemas.google.com/g/2005#home'>$val</gd:phoneNumber>";
    }

    private function preparePhoneHomeFax($val) {
        return "<gd:phoneNumber rel='http://schemas.google.com/g/2005#home_fax'>$val</gd:phoneNumber>";
    }

    private function prepareGroupWorkUrl($val) {
        return "<gContact:website rel='work' href='$val' />";
    }

    private function prepareOrganization($name, $title) {
        $xml = "<gd:organization rel='http://schemas.google.com/g/2005#work' primary='true'>";
        if ( $name != '' ) {
            $xml .= "<gd:orgName>$name</gd:orgName>";
        }
        if ( $title != '' ) {
            $xml .= "<gd:orgTitle>$title</gd:orgTitle>";
        }
        $xml .= "</gd:organization>";

        return $xml;
    }

    private static function getOption($id) {

        $sql = "Select OptionName from [Option] Where OptionId = ?";
        $params = array();
        $params[] = array(DTYPE_INT, $id);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aOption = DbConnManager::GetDb('mpower')->Exec($sql);
        $val = $aOption[0]['OptionName'];
        return $val;

    }

    public function generateGoogleXmlHeader() {
        $xml = "<?xml version='1.0' encoding='UTF-8'?>
            <feed xmlns='http://www.w3.org/2005/Atom'
            xmlns:gContact='http://schemas.google.com/contact/2008'
            xmlns:gd='http://schemas.google.com/g/2005'
            xmlns:batch='http://schemas.google.com/gdata/batch'>";
        return $xml;
    }

    public function generateGoogleXmlFooter() {
        $xml = '</feed>';
        return $xml;
    }

    public function generateGoogleXml($c, $mode='insert') {

        //print_r($c);exit;
			$contactid = isset($c['ContactID']) ? $c['ContactID'] : '';
       //First Name 
		if(isset($c['name-givenName'])){//[name-givenName] => prabhat
			$firstname = isset($c['name-givenName']) ? $c['name-givenName'] : ' ';
			$firstname = htmlspecialchars($firstname);
		}else{
			$firstname = isset($c['Name_Firstname']) ? $c['Name_Firstname'] : ' ';
			$firstname = htmlspecialchars($firstname);
		}
		
		//Last Name 
		if(isset($c['name-familyName'])){
			$lastname= isset($c['name-familyName']) ? $c['name-familyName'] : ' ';
			$lastname = htmlspecialchars($lastname);
			}else{
			$lastname= isset($c['Name_Lastname']) ? $c['Name_Lastname'] : ' ';
			$lastname = htmlspecialchars($lastname);
			}
			
		//Phone - Mobile
		if(isset($c['phoneNumber-mobile'])){
			$phoneMobile= isset($c['phoneNumber-mobile']) ? htmlspecialchars($c['phoneNumber-mobile']) : '';
			}else{
			$phoneMobile= isset($c['Phone_Mobile']) ? htmlspecialchars($c['Phone_Mobile']) : '';
			}
		//Phone - Main 
		if(isset($c['phoneNumber-work'])){
			$phoneWork = isset($c['phoneNumber-work']) ? htmlspecialchars($c['phoneNumber-work']) : '';
			}else{
			$phoneWork = isset($c['Phone_Work']) ? htmlspecialchars($c['Phone_Work']) : '';
			}
			
		if(isset($c['phoneNumber-main'])){
			$phoneMain = isset($c['phoneNumber-main']) ? htmlspecialchars($c['phoneNumber-main']) : '';
			}else{
			$phoneMain = isset($c['Phone_Main']) ? htmlspecialchars($c['Phone_Main']) : '';
			}
			
		if(isset($c['website-website'])){
			$groupWorkUrl= isset($c['website-website']) ? htmlspecialchars($c['website-website']) : '';
			}else{
			$groupWorkUrl= isset($c['Group_Work_URL']) ? htmlspecialchars($c['Group_Work_URL']) : '';
			}
		if(isset($c['email-home'])){
			$emailHome= isset($c['email-home']) ? htmlspecialchars($c['email-home']) : '';
			}else{
			$emailHome= isset($c['Email_Home']) ? htmlspecialchars($c['Email_Home']) : '';
			}
		if(isset($c['email-work'])){
			$emailWork= isset($c['email-work']) ? htmlspecialchars($c['email-work']) : '';
			}else{
			$emailWork= isset($c['Email_Work']) ? htmlspecialchars($c['Email_Work']) : '';
			}
		if(isset($c['phoneNumber-work_fax'])){
			$phoneWorkFax = isset($c['phoneNumber-work_fax']) ? htmlspecialchars($c['phoneNumber-work_fax']) : '';
			}else{
			$phoneWorkFax = isset($c['Phone_Work_Fax']) ? htmlspecialchars($c['Phone_Work_Fax']) : '';
			}
			
			//Address 1
		if(isset($c['structuredPostalAddress-street'])){
			$addressStreet = isset($c['structuredPostalAddress-street']) ? htmlspecialchars($c['structuredPostalAddress-street']) : '';
			}else{
			$addressStreet = isset($c['Address_Street']) ? htmlspecialchars($c['Address_Street']) : '';
			}
		if(isset($c['structuredPostalAddress-pobox'])){
		$addressPoBox = isset($c['structuredPostalAddress-pobox']) ? htmlspecialchars($c['structuredPostalAddress-pobox']) : '';
			}else{
		$addressPoBox= isset($c['Address_PO_Box']) ? htmlspecialchars($c['Address_PO_Box']) : '';
			}
			
			//Address 2
		if(isset($c['structuredPostalAddress-neighborhood'])){
			$addressNeighborhood= isset($c['structuredPostalAddress-neighborhood']) ? htmlspecialchars($c['structuredPostalAddress-neighborhood']) : '';
			}else{
			$addressNeighborhood= isset($c['Address_Neighborhood']) ? htmlspecialchars($c['Address_Neighborhood']) : '';
			}
		if(isset($c['structuredPostalAddress-city'])){//
			$addressCity= isset($c['structuredPostalAddress-city']) ? htmlspecialchars($c['structuredPostalAddress-city']) : '';
			}else{
			$addressCity= isset($c['Address_City']) ? htmlspecialchars($c['Address_City']) : '';
			}
		if(isset($c['structuredPostalAddress-region'])){
			$addressProvince = isset($c['structuredPostalAddress-region']) ? htmlspecialchars($c['structuredPostalAddress-region']) : '';
			}else{
			$addressProvince = isset($c['Address_Province']) ? htmlspecialchars($c['Address_Province']) : '';
			}
			
		if(isset($c['structuredPostalAddress-postcode'])){
			$addressPostalCode= isset($c['structuredPostalAddress-postcode']) ? htmlspecialchars($c['structuredPostalAddress-postcode']) : '';
			}else{
			$addressPostalCode= isset($c['Address_PostalCode']) ? htmlspecialchars($c['Address_PostalCode']) : '';
			}
		if(isset($c['structuredPostalAddress-country'])){//[Address_Country] => 1209
			$addressCountry= isset($c['structuredPostalAddress-country']) ? htmlspecialchars($c['structuredPostalAddress-country']) : '';
			}else{
			$addressCountry= isset($c['Address_Country']) ? htmlspecialchars($c['Address_Country']) : '';
			}
		if(isset($c['organization-orgName'])){
			$groupCompany= isset($c['organization-orgName']) ? htmlspecialchars($c['organization-orgName']) : '';
			}else{
			$groupCompany= isset($c['Group_Company']) ? htmlspecialchars($c['Group_Company']) : '';
			}
		if(isset($c['organization-orgTitle'])){
			$groupTitle = isset($c['organization-orgTitle']) ? htmlspecialchars($c['organization-orgTitle']) : '';
			}else{
			$groupTitle = isset($c['Group_Title']) ? htmlspecialchars($c['Group_Title']) : '';
			}
		if(isset($c['phoneNumber-home'])){
		$phoneHome= isset($c['phoneNumber-home']) ? htmlspecialchars($c['phoneNumber-home']) : '';
			}else{
		$phoneHome= isset($c['Phone_Home']) ? htmlspecialchars($c['Phone_Home']) : '';
			}
		if(isset($c['phoneNumber-home_fax'])){
		$phoneHomeFax = isset($c['phoneNumber-home_fax']) ? htmlspecialchars($c['phoneNumber-home_fax']) : '';
			}else{
		$phoneHomeFax = isset($c['Phone_Home_Fax']) ? htmlspecialchars($c['Phone_Home_Fax']) : '';
			}
			$fullname = $firstname . ' ' . $lastname;
			

        if ( $mode == 'insert' ){
            $xml = 
                "<entry xmlns='http://www.w3.org/2005/Atom' 
                xmlns:gd='http://schemas.google.com/g/2005' 
                xmlns:batch='http://schemas.google.com/gdata/batch' 
                xmlns:gContact='http://schemas.google.com/contact/2008'
 >
                <batch:id>$contactid</batch:id>
                <batch:operation type='insert'/>
                <category scheme='http://schemas.google.com/g/2005#kind' term='http://schemas.google.com/g/2008#contact'/>
                <gd:extendedProperty name='jetid' value='$contactid' />
                <gd:name>
                <gd:givenName>$firstname</gd:givenName>
                <gd:familyName>$lastname</gd:familyName>
                <gd:fullName>$fullname</gd:fullName>
                </gd:name>";
        }else{
            $xml = 
                "<entry xmlns='http://www.w3.org/2005/Atom'
                xmlns:gd='http://schemas.google.com/g/2005'
                xmlns:gContact='http://schemas.google.com/contact/2008'>
                <gd:extendedProperty name='jetid' value='$contactid' />
                <gd:name>
                <gd:givenName>$firstname</gd:givenName>
                <gd:familyName>$lastname</gd:familyName>
                <gd:fullName>$fullname</gd:fullName>
                </gd:name>";
        }

        if ( $phoneMain != '' ) {
            $xml .= $this->preparePhoneMain($phoneMain);
        }

        if ( $phoneWorkFax != '' ) {
            $xml .= $this->preparePhoneWorkFax($phoneWorkFax);
        }

        if ( $addressStreet != '' || $addressPoBox != '' || $addressNeighborhood != '' || $addressCity != '' ||
                $addressProvince != '' || $addressPostalCode != '' || $addressCountry != '' ) {
            $xml .= $this->prepareStructuredPostalAddress($addressStreet, $addressPoBox, $addressNeighborhood, $addressCity, $addressProvince, $addressPostalCode, $addressCountry );
        }

        if ( $groupTitle != '' || $groupCompany != '' ) {
            $xml .= $this->prepareOrganization($groupCompany, $groupTitle);
        }

        if ( $emailHome != '' ) {
            $xml .= $this->prepareEmailHome($emailHome);
        }

        if ( $emailWork != '' ) {
            $xml .= $this->prepareEmailWork($emailWork);
        }

        if ( $groupWorkUrl != '' ) {
            $xml .= $this->prepareGroupWorkUrl($groupWorkUrl);
        }

        // phone
        if ( $phoneMobile != '' ) {
            $xml .= $this->preparePhoneMobile($phoneMobile);
        }
        if ( $phoneWork != '' ) { // direct in jetstream
           $xml .= $this->preparePhoneWork($phoneWork);
        }
        if ( $phoneHome != '' ) {
            $xml .= $this->preparePhoneHome($phoneHome);
        }

        if ( $phoneHomeFax != '' ) {
            $xml .= $this->preparePhoneHomeFax($phoneHomeFax);
        }

        /*
           if ( '' != $primaryPhone ) {
           $xml .= "<gd:email rel='http://schemas.google.com/g/2005#work' primary='true' address='$primaryEmail' displayName='E. Bennet'/>";
           }
           if ( '' != $primaryPhone ) {
           $xml .= "<gd:phoneNumber rel='http://schemas.google.com/g/2005#work' primary='true'>$primaryPhone</gd:phoneNumber>";
           }
           if ( '' != $secondPhone ) {
           $xml .= "<gd:phoneNumber rel='http://schemas.google.com/g/2005#home'>$secondPhone</gd:phoneNumber>";
           }
         */

        $xml .=
            "<gContact:groupMembershipInfo deleted='false' href='" . $this->contactGroupID. "'/>
            </entry>";
//print_r($xml);exit;
        return $xml;
    }

}
