<?php
/* 
 * Batch push Using Zend/Gdata
 */

require_once 'constants.php';
require_once 'mpconstants.php';
require_once JETSTREAM_ROOT . '/../include/class.GoogleConnect.php';
require_once JETSTREAM_ROOT . '/../slipstream/class.Dashboard.php'; //used to get client data

// for Map lookup for db column's
require_once JETSTREAM_ROOT . '/../include/class.MapLookup.php';
require_once JETSTREAM_ROOT . '/../slipstream/class.CompanyContactNew.php';

//require_once 'Zend/Gdata/Contacts.php';
//require_once 'Zend/Gdata_OAuth_Helper.php';

class GoogleBackgroundPush {

    private $contactGroupID;
    private $client;
    private $contacts;
    private $userid;

    public function __construct($oConnect, $file, $groupid, $userid) {
        $this->userid = $userid;

        //error_log('call Bacground Process : ' . $groupid); exit;

        //$this->contacts = $oConnect->mirrorContacts;
        $this->client = $oConnect->client;
        $this->contacts = $oConnect->client;

        // TODO set php.ini
        ini_set('memory_limit', '-1');
        set_time_limit(1800);

        $this->setContactEmail();

        $this->contactGroupID = $groupid;

        /* contact group id is the ID of name 'Jeststream' at google side
         * to lessen google request, store in googleToken table
         */
        /*
        if ('' == $this->contactGroupID) {
            $this->contactGroupID = $this->getContactGroup();
            //echo $this->contactGroupID; exit;
            if ( $this->contactGroupID == '' ) {
                error_log( 'Google contact id "Jetstream" is not found' );
                return false;
            }
        }
        */

        //error_log($this->contactGroupID); exit;

        $this->push($file);

    }

    /* Need to fork with backend process */
    private function push($file) {

        //$oBatch = new Google_BatchRequest();

        /* 
        $batch->add($plus->people->get('me'), 'key1');
        $batch->add($plus->people->get('me'), 'key2');
        $result = $batch->execute();
        print "<pre>" . print_r($result, true) . "</pre>";
        */

        //$oContact = new Google_Contact($this->client);
        //echo '<pre>'; print_r($oContact); echo '</pre>';
        //exit;

        //$this->contacts->insert

        //error_log( 'call push in Batch : ' . $file );

        $fp = fopen($file, 'r');

        while (($line = fgets($fp)) !== false) {

            $aLine = explode(',', $line);

            $givenName = trim($aLine[0],'"');
            $familyName = trim($aLine[1],'"');
            $fullName = $givenName . ' ' . $familyName;
            $primaryPhone = trim($aLine[2],'"');
            $secondPhone = trim($aLine[3],'"');
            $primaryEmail = trim($aLine[4],'"');
            $contactid = trim($aLine[5],'"');

            $contact = "<entry xmlns='http://www.w3.org/2005/Atom'
                xmlns:gd='http://schemas.google.com/g/2005'
                xmlns:gContact='http://schemas.google.com/contact/2008'
                >
                <gd:name>
                <gd:givenName>$givenName</gd:givenName>
                <gd:familyName>$familyName</gd:familyName>
                <gd:fullName>$fullName</gd:fullName>
                </gd:name>";
                if ( '' != $primaryPhone ) {
                    $contact .= "<gd:email rel='http://schemas.google.com/g/2005#work' primary='true' address='$primaryEmail' displayName='E. Bennet'/>";
                }
                if ( '' != $primaryPhone ) {
                    $contact .= "<gd:phoneNumber rel='http://schemas.google.com/g/2005#work' primary='true'>$primaryPhone</gd:phoneNumber>";
                }
                if ( '' != $secondPhone ) {
                    $contact .= "<gd:phoneNumber rel='http://schemas.google.com/g/2005#home'>$secondPhone</gd:phoneNumber>";
                }
            $contact.=
                "<gContact:groupMembershipInfo deleted='false' href='" . $this->contactGroupID. "'/>
                </entry>";

            //echo $this->contactUserEmail; exit;
            $len = strlen($contact);
            $add = new Google_HttpRequest("https://www.google.com/m8/feeds/contacts/".$this->contactUserEmail."/full/");
            $add->setRequestMethod("POST");
            $add->setPostBody($contact);
            $add->setRequestHeaders(array('content-length' => $len, 'GData-Version'=> '3.0','content-type'=>'application/atom+xml; charset=UTF-8; type=feed'));

            $submit = $this->contacts->getIo()->authenticatedRequest($add);
            $sub_response = $submit->getResponseBody();
            $parsed = simplexml_load_string($sub_response);
            $aClient = explode("base/",$parsed->id);

            /* TODO set files to include contactid and get userid */
            $googleContactId = $aClient[1];

            $sql = 'Insert GoogleContact (PersonID, ContactID, GoogleContactId) values (?, ?, ?)';
            $params = array();
            $params[] = array(DTYPE_INT, $this->userid);
            $params[] = array(DTYPE_INT, $contactid);
            $params[] = array(DTYPE_STRING, $googleContactId);
            $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
            $status = DbConnManager::GetDb('mpower')->Exec($sql);

            if (!$status) {
                return false;
            }

        }

        unlink($file);
        fclose($fp);
    }

    /* Get Jetstream group from google, if not create new Jetstream one */
    private function getContactGroup() {
        error_log('call getContactGroup');
        $req = new Google_HttpRequest("https://www.google.com/m8/feeds/groups/default/full");
        // Google Contacts way
        $val = $this->contacts->getIo()->authenticatedRequest($req);

        // The contacts api only returns XML responses.
        $response =$val->getResponseBody();
        $xml = simplexml_load_string($response); // Convert to an ARRAY

        /* get author email and define as contactEmail */
        if ( isset ( $xml->author ) ) {
            $this->contactEmail = $xml->author->email;
        }
        $sql = "Select Name from company where CompanyID = ? ";
        $params = array();
        $params[] = array(DTYPE_INT, $this->companyId);
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		$company = DbConnManager::GetDb('mpower')->Exec($sql);

        $aEntry = array();
        $hasClient = false;
        
        if ( isset ( $xml->entry ) ) {
            $aEntry = $xml->entry;

            foreach ( $aEntry as $e ) {
                if ( $company[0]['Name'] == $e->title ) {
                    /* ID is full url of contact group */
                    /* TODO no store in DB, lookup GoogleToken::contact_group_id */
                    $hasClient = true;
                    return $e->id;
                    break;
                }
            }
        }

        if ( $hasClient == false ) {
            return $this->createContactGroup();
        }
    }
    private function createContactGroup() {
		
		$sql = "Select Name from company where CompanyID = ? ";
        $params = array();
        $params[] = array(DTYPE_INT, $this->companyId);
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		$company = DbConnManager::GetDb('mpower')->Exec($sql);
		
        error_log('createContactGroup');
        $group = "
            <atom:entry xmlns:atom='http://www.w3.org/2005/Atom' xmlns:gd='http://schemas.google.com/g/2005'>
            <atom:category scheme='http://schemas.google.com/g/2005#kind' term='http://schemas.google.com/contact/2008#group'/>
            <atom:title type='text'>{$company[0]['Name']}</atom:title>
            <gd:extendedProperty name='more info about the group'>
            <info>Nice people.</info>
            </gd:extendedProperty>
            </atom:entry>";

        $len = strlen($group);
        $url = "https://www.google.com/m8/feeds/groups/".$this->contactEmail."/full/";

        $add = new Google_HttpRequest($url);
        //$add = new Google_HttpRequest("https://www.google.com/m8/feeds/groups/default/full/");
        $add->setRequestMethod("POST");
        $add->setPostBody($group);
        $add->setRequestHeaders(array('content-length' => $len, 'GData-Version'=> '3.0','content-type'=>'application/atom+xml; charset=UTF-8; type=feed'));
        $submit = $this->contacts->getIo()->authenticatedRequest($add);
        $sub_response = $submit->getResponseBody();

        $parsed = simplexml_load_string($sub_response);
        $aGroup = explode("base/",$parsed->id);

        $personId = $this->user;

        $googleContactId = $aGroup[1];

        $googleContactId = 'http://www.google.com/m8/feeds/groups/'. $this->contactEmail .'/base/' . $googleContactId;
        $sql = 'Update googleToken set contact_group_id = ? Where personId = ? ';
        $params = array();
        $params[] = array(DTYPE_STRING, $googleContactId);
        $params[] = array(DTYPE_INT, $personId);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $status = DbConnManager::GetDb('mpower')->Exec($sql);
        if (!$status) {
            return false;
        } else {
            return $googleContactId;
        }
    }

    public function setContactEmail() {
        $url = 'https://www.google.com/m8/feeds/contacts/default/full';
        $req = new Google_HttpRequest($url);
        $req->setRequestHeaders(array('GData-Version'=> '3.0','content-type'=>'application/atom+xml; charset=UTF-8; type=feed'));
        // Google Contacts way
        $val = $this->contacts->getIo()->authenticatedRequest($req);

        // The contacts api only returns XML responses.
        $response =$val->getResponseBody();
        $xml = simplexml_load_string($response); // Convert to an ARRAY
        $this->contactUserEmail = urlencode($xml->id); // email address
        unset($xml);
    }
}
