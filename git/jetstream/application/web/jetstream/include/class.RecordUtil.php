<?php
require_once BASE_PATH . '/include/class.DebugUtil.php';

class RecordUtil {
	
	private $inserted;
	private $key;
	
	public function __construct($key=null, $values=null){
		
		if(!(is_null($key) && is_null($values))){
			
			$this->inserted['key'] = $key;
			
			foreach($values as $k => $v){
				$this->inserted[$k] = $v;
			}
		}
	}
	
	public function isInserted($search, $recordUtilArray){
		foreach($recordUtilArray as $recordUtil){
			foreach($recordUtil as $record){
				
				$found = false;
				
				foreach($search as $val){
					if((strlen($val) > 0) && is_string($val)){
						if(is_array($record)){
							if(in_array($val, $record)){
								$found = true;
							}
							else {
								$found = false;
								break;
							}
						}
						else {
							if($record == $val){
								$found = true;
							}
							else {
								$found = false;
								break;
							}
						}
					}
				}
				if($found){
					$this->key = $record['key'];
					return true;
				}
			}
		}
	}
	
	public function getKey(){
		return $this->key;
	}

}