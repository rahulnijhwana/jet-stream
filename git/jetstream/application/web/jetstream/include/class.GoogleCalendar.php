<?php
require_once 'constants.php';
//require_once JETSTREAM_ROOT . '/../include/mpconstants.php';
require_once JETSTREAM_ROOT . '/../include/class.GoogleConnect.php';
require_once JETSTREAM_ROOT . '/../include/class.AccountUtil.php';
require_once JETSTREAM_ROOT . '/../include/class.ContactUtil.php';
require_once JETSTREAM_ROOT . '/../include/class.EventUtil.php';
require_once JETSTREAM_ROOT . '/../include/class.DbConnManager.php';
require_once JETSTREAM_ROOT . '/../include/class.SqlBuilder.php';
require_once JETSTREAM_ROOT . '/../slipstream/class.ModuleEventNew.php';
require_once JETSTREAM_ROOT . '/../slipstream/class.Note.php';

class GoogleCalendar {

    private $sync = false;
    private $calendar;
    private $user;
    private $userid;
    private $companyId; 
    private $eventProcessed;
    private $calendarID;
    private $user_timezone;
    //private $timezoneCode;

    private $debug = false;

    public function __construct($oCalendar, $user, $sync=true) {

        if ( $_GET['debug'] == true ) {
            $this->debug = true;
        }

        $this->calendar = $oCalendar;
        $this->user = $user;
        $this->userid = $user;
        $this->companyId = GoogleConnect::getCompanyId($user);
     //  print_r($oCalendar);exit; 

        if (!empty($_SESSION['USER']['BROWSER_TIMEZONE'])) {
            $this->user_timezone = $_SESSION['USER']['BROWSER_TIMEZONE'];
        } else {
            $this->user_timezone = GoogleConnect::getUserTimezone($user);
        }

        //$this->timezoneCode = GoogleConnect::getTimezoneCode($user);

        if ('' == $this->calendarID) {
            $this->calendarID = $this->getGoogleCalendarLists();

            if ( false == $this->calendarID ) {
                return false;
            }

        }
        

        if ( $sync ) {
            if ( $this->hasProcessed() == false ) {
                $this->getProcessed();
                $this->syncEvent();
            } else {
                $this->getProcessed();
                if ( $this->decideSync() ) {
                    error_log('sync calender : ' . $this->eventProcessed);
                    $this->syncEvent();
                } else {
                    error_log('sync calender is not run for interval');
                }
            }
        }
    }

    private function decideSync() {

        if ( $this->eventProcessed == '' ) {
            return false;
        }

        $processedTimestamp = strtotime($this->eventProcessed);
        $nowTimestamp = strtotime(GoogleConnect::getTodayWithGoogleFormat());
        $diff = $nowTimestamp - $processedTimestamp;    
        /*
        error_log('processedTimestamp : ' . $processedTimestamp);
        error_log('nowTimestamp : ' . $nowTimestamp);
        error_log('decideSync : ' . $diff);
        */

        if ( $diff > GOOGLE_SYNC_INTERVAL ) {
            return true;
        } else {
            return false;
        }

    }

    private function hasProcessed() {
        $sql = 'Select TOP 1 event_processed, calendar_id from GoogleToken where personId = ?';
        $params[] = array(DTYPE_INT, $this->user);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aProcessed = DbConnManager::GetDb('mpower')->Exec($sql);

        return ( !empty($aProcessed[0]['event_processed']) ) ? true : false;
    }

 

    /* Get Event processed time and calendar id */
    private function getProcessed() {
        $sql = 'Select TOP 1 event_processed, calendar_id from GoogleToken where personId = ?';
        $params[] = array(DTYPE_INT, $this->user);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aProcessed = DbConnManager::GetDb('mpower')->Exec($sql);
        if (count($aProcessed) > 0) {
            $today = date("YmdHis");
            $utc_timezone = new DateTimeZone('UTC');
            $jetDate = new JetDateTime($today, $utc_timezone);
            $today = $jetDate->asMsSql('g');
            $this->eventProcessed = ('' != $aProcessed[0]['event_processed']) ? GoogleConnect::convertGoogleDateFormat($aProcessed[0]['event_processed']) : $today ;
            $this->calendarID = ('' != $aProcessed[0]['calendar_id']) ? $aProcessed[0]['calendar_id'] : '' ;
        }
        return true;
    }

    private function getGoogleCalendarLists() {
		//print_r($this->calendar);
        $calendarLists = $this->calendar->calendarList->listCalendarList();
//print_r($calendarLists);exit;
        if ( isset($calendarLists['error']) ) {
            return false;
        }
        $calendarListItems = $calendarLists['items'];
        $existJetstreamCalender = false;
        $calendarID = false;
        
        
        $sql = "Select Name from company where CompanyID = ? ";
        $params = array();
        $params[] = array(DTYPE_INT, $this->companyId);
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		$company = DbConnManager::GetDb('mpower')->Exec($sql);
		//print_r($company[0]['Name']);
        
        //$sql = 'Update Event set googleEventId = ?,googleCalendarId = ?,googleStatus = ?
                //Where eventId = ?';
        //$params = array();
		//$params[] = array(DTYPE_STRING, $googleEventId);
		//$params[] = array(DTYPE_STRING, $this->calendarID);
		//$params[] = array(DTYPE_INT, 1);
		//$params[] = array(DTYPE_INT, $eventId);
		

		//$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);

		//$status = DbConnManager::GetDb('mpower')->Exec($sql);
        
        
        foreach($calendarListItems as $calendar) {
            $summary = $calendar['summary'];
           // if ($summary == GOOGLE_CALENDAR_JETSTREAM) {
            if ($summary == $company[0]['Name']) {
                $existJetstreamCalender = true;
                $calendarID = $calendar['id'];
                break;
            }
        }

        if(!$existJetstreamCalender) {
            $calendarID = $this->createJetstreamCalendar();
        }
        if (false == $calendarID) {
            return false;   
        } else {
            $this->setCalendarID($calendarID);
        }

        return $calendarID; 
    }

    // Update calendarID in GoogleToken table
    protected function setCalendarID($calendarID) {
        // save the record
        $sql = 'Update GoogleToken
            Set calendar_id = ?
            Where PersonID = ?';

        $params[] = array(DTYPE_STRING, $calendarID);
        $params[] = array(DTYPE_INT, $this->user);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $status = DbConnManager::GetDb('mpower')->Exec($sql);

        if (!$status) {
            return false;
        }
    }

    /* 
     * google calendar name(summary) should be 'Jetstream'
     */
    private function createJetstreamCalendar() {
		
		$sql = "Select Name from company where CompanyID = ? ";
        $params = array();
        $params[] = array(DTYPE_INT, $this->companyId);
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		$company = DbConnManager::GetDb('mpower')->Exec($sql);
		
        $newCalendar = new Google_Calendar();
        //$newCalendar->setSummary(GOOGLE_CALENDAR_JETSTREAM); 
        $newCalendar->setSummary($company[0]['Name']); 
        $createdCalendar = $this->calendar->calendars->insert($newCalendar);
        if (isset($createdCalendar['id'])) {
            return $createdCalendar['id'];
        } else {
            error_log('Fail to create Jetstream calendar in Google');
            return false;
        }
    }

    /* 
     * insert google event on jetstream push 
     * called in ModuleEventNew->SaveForm
     * @param $jetstreamEvent, the jetstream Format
     */
    public function insertGoogleEvent($jetstreamEvent) {

        
        if ( $this->debug ) {
            error_log('Call insertGoogleEvent');
        }

        $jEvent = $jetstreamEvent->GetDataset();

        /* Insert jEvent don't contain googleEventId
         * flag for Insert/Update
         */
        //$googleEventId = (isset($jEvent['googleEventId'])) ? $jEvent['googleEventId'] : '' ;

        $repeatParent = (isset($jEvent['RepeatParent'])) ? $jEvent['RepeatParent'] : '' ;

        $eventId = $jEvent['EventID'];
        $googleEventId = $this->getGoogleEventId($eventId);


        $clientName = '';
        if (isset($jEvent['ContactID'])) {
            $contact = ContactUtil::getContact($jEvent['ContactID']);
            $companyName = AccountUtil::getAccountName($contact['AccountID']);
            $companyName = strtoupper($companyName);
            $clientName = $contact['Text01'] . ' ' . $contact['Text02'].' - '. $companyName;
        } else if (isset($Event['AccountID'])) {
            $clientName = AccountUtil::getAccountName($jEvent['AccountID']);
        }

        if ('' != $clientName ) {
            $clientName = ' ('.$clientName.')';
        }

        $eventTypeName = EventUtil::getEventTypeName($jEvent['EventTypeID']);
        $title = $jEvent['Subject'] . $clientName;
        $notes = "Event Type : " . $eventTypeName . "\n\n";
        $jetNotes = $this->getNotes($eventId);
        if ('' != $jetNotes) {
            $notes .= $jetNotes;
        }

        $notes .= (!empty($jEvent['Note'])) ? $jEvent['Note'] : '' ;
        $location = $jEvent['Location'];
        //$start = new DateTime($jEvent['StartDateUTC']);
        //$end = new DateTime($jEvent['EndDateUTC']);

        $start = GoogleConnect::convertGoogleDateFormat($jEvent['StartDateUTC']);
        $end = GoogleConnect::convertGoogleDateFormat($jEvent['EndDateUTC']);
        $updated = GoogleConnect::convertGoogleDateFormat($jEvent['ModifyTime']);
        

        $oStart = new Google_EventDateTime();
        $oStart->setDateTime($start);
        $oStart->setTimeZone($this->user_timezone);

        $oEnd = new Google_EventDateTime();
        $oEnd->setDateTime($end);
        $oEnd->setTimeZone($this->user_timezone);

        $oUpdated = new Google_EventDateTime();
        $oUpdated->setDateTime($updated);
        $oUpdated->setTimeZone($this->user_timezone);

        $attendees = $this->getAttendees($eventId);
        if (false != $attendees) {
            $googleAttendees = array();
                foreach ($attendees as $attendee) {
                        $displayName = $attendee['FirstName'] . ' ' . $attendee['LastName'];
                        $aAttendee = new Google_EventAttendee();
                        $aAttendee->setDisplayName($displayName);
                        $aAttendee->setEmail($attendee['Email']);
                        /* ID of Attendee is Google specific one */
                        //$aAttendee->setId($attendee['personID']);
                        array_push($googleAttendees, $aAttendee);
                }
        }

        // recurrence
        $jetRepeatType = $jEvent['RepeatType'];
        $jetRepeatEnd = (!empty($jEvent['RepeatEndDateUTC'])) ? $jEvent['RepeatEndDateUTC'] : '';
        $jetRepeatInterval = $jEvent['RepeatInterval'];

        $googleRecurrence = '';
        if ('' != $jetRepeatType && '' != $jetRepeatEnd && '' != $jetRepeatInterval ) {
            $googleRecurrence = $this->generateGoogleRecurrence($jetRepeatType, $jetRepeatEnd, $jetRepeatInterval);
        }



        if (!$googleEventId) {     //insert

            $newEvent = new Google_Event();
            $newEvent->setSummary($title);
            $newEvent->setLocation($location);
            $newEvent->setDescription($notes);

            $newEvent->setStart($oStart);
            $newEvent->setEnd($oEnd);
            $newEvent->setUpdated($oUpdated);
            
            if (count($googleAttendees) > 0) {
                $newEvent->setAttendees($googleAttendees);
            }
            
            if ( $googleRecurrence != '' ) {
                //echo '<pre>'; print_r($googleRecurrence); echo '</pre>'; exit;
                $newEvent->setRecurrence($googleRecurrence);
            }

            /* REVIEW
             * It just print google native errors
             * how to set exception handling, besso
             */
             
            $createdEvent = $this->calendar->events->insert( $this->calendarID, $newEvent );

            if ( isset($createdEvent['error']) ) {
                //echo '<pre>'; print_r($this->user_timezone); echo '</pre>';
                error_log( 'Google event insert : ' . $createdEvent['message'] );
                return false;
            }

            $createdEventId = $createdEvent['id'];
            $this->updateGoogleEventId($eventId, $createdEventId);

            if ( '' != $repeatParent ) {
                $recurringGoogleId = $this->getGoogleEventId($repeatParent);
                $instanceDate = date("Ymd", strtotime($start));

                // do something
                $optParams = array();
                $deleteRecurrenceID = '';
                $instance = $this->calendar->events->instances( $this->calendarID, $recurringGoogleId, $optParams );
                foreach ( $instance['items'] as $i ) {
                    $rStart = date( "Ymd", strtotime($i['start']['dateTime']) );
                    if ( $instanceDate == $rStart ) {
                        $deleteRecurrenceID = $i['id'];
                    }
                }
               
                if ( '' != $deleteRecurrenceID ) { 
                    $this->calendar->events->delete( $this->calendarID, $deleteRecurrenceID ); 
                }

            }

        } else {        // update

            $existEvent = $this->calendar->events->get($this->calendarID, $googleEventId);
            $existEvent = new Google_Event($existEvent);
            $existEvent->setSummary($title);
            $existEvent->setLocation($location);

            /* update is not allowed for Google description */
            //$existEvent->setDescription($notes);

            $existEvent->setStart($oStart);
            $existEvent->setEnd($oEnd);
            $existEvent->setUpdated($oUpdated);

            if (count($googleAttendees) > 0) {
                $existEvent->setAttendees($googleAttendees);
            }

            $existEvent->setRecurrence($googleRecurrence);
            $updatedEvent = $this->calendar->events->update( $this->calendarID, $googleEventId, $existEvent );
            /*Start JET-37 For edit*/
            $sql = "Update Event set googleStatus = '1' Where googleEventId = '$googleEventId'";
            DbConnManager::GetDb('mpower')->Exec($sql);
            /*End JET-37 For edit*/

        }
        return true;
    }

    private function getAttendees($eventID) {
        // Load all attendees of this record, index by PersonID
                $sql = 'SELECT EA.personID, P.FirstName, P.LastName, P.Email 
              FROM EventAttendee EA 
              JOIN People P ON P.PersonID = EA.PersonID
             WHERE EA.EventID = ? 
               AND (EA.Deleted != 1 OR EA.Deleted IS NULL)';
        $params = array();
                $params[] = array(DTYPE_INT, $eventID);
                $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $attendees = DbConnManager::GetDb('mpower')->Exec($sql);
        if (count($attendees) > 0) {
            return $attendees;
        }
        return false;
    }

    private function updateGoogleEventId($eventId, $googleEventId) {

                if (empty($googleEventId)) {
                        return false;
                }

                // save the record
                $sql = 'Update Event set googleEventId = ?,googleCalendarId = ?,googleStatus = ?
                         Where eventId = ?';
        $params = array();
                $params[] = array(DTYPE_STRING, $googleEventId);
                $params[] = array(DTYPE_STRING, $this->calendarID);
                $params[] = array(DTYPE_INT, 1);
                $params[] = array(DTYPE_INT, $eventId);
                

                $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);

                $status = DbConnManager::GetDb('mpower')->Exec($sql);

                if (!$status) {
                        return false;
                }
                return true;
   }

    private function syncEvent() {

        /*        
        echo '<pre>'; print_r($this->user); echo '</pre>';
        echo '<pre>'; print_r($this->companyId); echo '</pre>';
        echo '<pre>'; print_r('call syncEvent'); echo '</pre>';
        */

        /*
        $utc_timezone = new DateTimeZone('UTC');
        $jetDate = new JetDateTime($this->eventProcessed, $utc_timezone);
        $maxDate = $jetDate->addDate( GOOGLE_TO_DATE, $this->eventProcessed, GOOGLE_DATE_RFC3339 );
        */

        $minDate = GoogleConnect::getTodayWithGoogleFormat('Ymd');
        $options = array(
            'timeMin' => $minDate,
        );
        $eventLists = array();
        $eventLists = $this->getGoogleEventLists($options);

        //error_log(print_r($eventLists,true));
        //error_log(count($eventLists));

        if ($this->debug) {
            error_log('New Event Count ===>');
            error_log(count($eventLists));
        }

        if ( count($eventLists) > 0 ) {
            foreach ($eventLists as $googleEvent) {
                $this->insertJetEvent($googleEvent);
            }
        }

        /* TODO
         * we should loop all jetstream events to delete all deleted google event
         * This could be load , let's talk. besso
         */
        $this->deleteJetEvents($eventLists);

        /* TODO
         * $today is not defined
         */
        $today = '';
        $this->setProcessed($today);
        return true;
    }

    private function setProcessed($today = '') {
        if ('' == $today) {
            $today = date("YmdHis");
            $utc_timezone = new DateTimeZone('UTC');
            $jetDate = new JetDateTime($today, $utc_timezone);
            $today = $jetDate->asMsSql('g');
        }

        // save the record
        $sql = 'Update GoogleToken
            Set event_processed = ?
                    Where PersonID = ?';

        $params[] = array(DTYPE_STRING, $today);
        $params[] = array(DTYPE_INT, $this->user);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $status = DbConnManager::GetDb('mpower')->Exec($sql);

        if (!$status) {
            return false;
        }
    }

    public function getGoogleEventLists($options = array()) {
            if ('' == $this->calendarID)  return false;
            $eventLists = $this->calendar->events->listEvents($this->calendarID, $options);
            if (!isset($eventLists['items'])) {
                return false;
            }
            $eventList = $eventLists['items'];
            rsort($eventList);
            return $eventList;
    }


    /* main method to insert Jet Event on google pulling */
    private function insertJetEvent( $googleEvent ) {

        $eventTypeID = GOOGLE_EVENT_TYPE_IN_JETSTREAM;
       
        if ( $this->debug ) { 
            error_log('New or Updated Google Event ===> ');
            error_log( print_r($googleEvent,true) );
        }

        $googleEventId = $googleEvent['id'];
        $eventId = $this->existGoogleEvent($googleEventId);
        $eventId = ($eventId > 0) ? $eventId : '';

        /*
        $subject = $googleEvent['summary'];
        error_log('googleid : ' . $googleEventId);
        error_log('eventid : ' . $eventId);
        error_log('summary : ' . $subject);
        */


        if ( $this->debug ) { 
            error_log('Aleady has google event id : ===>');
            error_log($eventId);
        }

        /* Jetstream has googleId so it's update */
        if ($eventId != '') {
            // TODO. it call update, lookup exist Edit process
            $this->updateJetEvent($eventId, $googleEvent);
            return false;
        }

        /* if one has parent, this mean it's one of reccurring to save as separately */
        $repeatParent = isset($googleEvent['recurringEventId']) ? $googleEvent['recurringEventId'] : '';

        if ( $this->debug ) { 
            error_log('Change just one of reccuring with repeatParent : ===>');
            error_log($repeatParent);
        }

        /* TODO. need to perserve EventType for one instance change of recurring
         * There are no column 'EventType' at Google side, besso
         */
        if ( '' != $repeatParent ) {
            $sql = 'Select RepeatExceptions,EventTypeID From Event Where googleEventId = ?';
            $params = array();
            $params[] = array(DTYPE_STRING, $repeatParent);
            $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
            $aException = DbConnManager::GetDb('mpower')->Exec($sql);
            if (count($aException) > 0) {
                $exception_string = $aException[0]['RepeatExceptions'];
                $eventTypeID = $aException[0]['EventTypeID'];
            }

            if (!empty($exception_string)) {
                $exceptions = explode(',', $exception_string);
            } else {
                $exceptions = array();
            }

            if ( $this->debug ) { 
                error_log('Change just one of reccuring with exceptions : ===>');
                error_log(print_r($exceptions,true));
            }

            $rDate = isset($googleEvent['originalStartTime']['dateTime']) ? $googleEvent['originalStartTime']['dateTime'] : '';
            if ( '' != $rDate ) {
                $origStartDate = date("Ymd", strtotime($rDate));
            }
            $exceptions[] = $origStartDate;
            $exceptions = array_unique($exceptions);
            $excep_upd_sql = "UPDATE Event SET RepeatExceptions = ? WHERE googleEventId = ?";
            $excep_upd_sql = SqlBuilder()->LoadSql($excep_upd_sql)->BuildSql(array(DTYPE_STRING, implode(',', $exceptions)), array(DTYPE_STRING, $repeatParent));
            DbConnManager::GetDb('mpower')->GetOne($excep_upd_sql);
        }   // check if one has repeating parent

        $subject = $googleEvent['summary'];

        if ( $this->debug ) { 
            error_log('Event - subject to insert : ===>');
            error_log($subject);
        }

        if ( '' == $subject) {
            return false;
        }

        $updated = $googleEvent['updated'];

        /* TODO
         * If event is all day, it wasn't stored well in DB
         * check that, besso
         */
        $aStartDate = (isset($googleEvent['start'])) ? $googleEvent['start'] : '' ;
        $oStartDate = new DateTime(GoogleConnect::convertJetstreamDateFormat($aStartDate['dateTime'], $this->userid) );
        error_log($aStartDate['dateTime']);

        $startDate = $oStartDate->format(JETSTREAM_DATE_FORMAT);
        $startTime = $oStartDate->format(JETSTREAM_TIME_FORMAT);

        if ( $this->debug ) { 
            error_log('Event - converted startDate to insert : ===>');
            //error_log('get startTime from Google response : ' . $startTime);
            error_log($startDate);
        }

        if ('' == $startDate) {
            return false;
        }
        
        $aEndDate = (isset($googleEvent['end'])) ? $googleEvent['end'] : '' ;
        $oEndDate = new DateTime(GoogleConnect::convertJetstreamDateFormat($aEndDate['dateTime'], $this->userid) );
        $endDate = $oEndDate->format(JETSTREAM_DATE_FORMAT);
        $endTime = $oEndDate->format(JETSTREAM_TIME_FORMAT);
        if ('' == $endDate) {
            return false;
        }

        if ( $this->debug ) { 
            error_log('Event - endDate to insert : ===>');
            error_log($endDate);
        }
        $description = ($googleEvent['description'] != '') ? $googleEvent['description'] : '';
        $location = ($googleEvent['location'] != '') ? $googleEvent['location'] : '';

        /* TODO. 
         * Pull-Update of Event by Jetstream is screwed because both structure be quite different, No update via Google Pull, besso
        if ($eventId > 0) {
            $description = '';
        }
        */

        $userID = $this->user;
        $oEventNew = new ModuleEventNew($this->companyId, $this->user, $this->user_timezone);

        if (isset($googleEvent['attendees'])) {
            $attendee_list = $this->generateAttendees($googleEvent['attendees']);
            $description .= $this->generateAttendeesInNotes($googleEvent['attendees']);
        } else {
            $attendee_list = json_encode(array($userID));
        }

        $attendee_list = str_replace('"','',$attendee_list);
        //error_log($attendee_list); exit;
        //echo '<pre>'; print_r($description); echo '</pre>'; exit;

        /*
         * Google Event status
         * https://developers.google.com/google-apps/calendar/v3/reference/events#resource
         */     
        $status = $googleEvent['status'];
        if (GOOGLE_EVENT_STATUS_CONFIRMED == $status ||
            GOOGLE_EVENT_STATUS_TENTATIVE == $status) {
            $closed = '';
        } else {
            $closed = 1;
        }

        /*
        $recurrence = (!empty($googleEvent['recurrence'][0])) ? $googleEvent['recurrence'][0] : '' ;
        $repeat = ($recurrence == '') ? false : true;
        */

        $req = array(
            'EventID' => $eventId,
            'AccountID' => '',
            'ContactID' => $userID,
            'DealID' => '',
            'Contact' => '',
            'Subject' => $subject,
            'Location' => $location,
            'EventType' => $eventTypeID,
            'StartDate' => $startDate,
            'StartTime' => $startTime,
            'EndDate' => $endDate,
            'EndTime' => $endTime,
            'Repeat' => $repeat,
            'attendee_list' => $attendee_list,
            'CheckPrivate' => '',
            'RepeatParent' => '',
            'OrigStartDate' => '',
            'Note' => $description,
            'priority' => '',
            'add_event_alert_hidden' => '',
            'email_event_alert' => '',
            'standard_event_alert' => 'on',
            'send_event_immediately' => '1',
            'delay_event_until' => '',
            'event_hours' => '',
            'event_minutes' => '',
            'event_periods' => '',
            'alert_users' => '',
            'googleEventId' => $googleEventId,
            'ModifyDate' => $updated,
            'Closed' => $closed,
            'repeat_detail' => array()
        );

        if ( $this->debug ) {
            error_log('Event Form with google : ===>' );
            error_log( print_r($req,true) );
        }

        /* Generate Jetstream style Recurrence */
        $repeatParent = '';
        $repeat = 0;
        $repeat_detail = array();

        if (isset($googleEvent['recurrence'])) {
            $googleRecurrence = $googleEvent['recurrence'][0];
            $aJetRecurrence = $this->generateJetstreamRecurrence($googleRecurrence);

            $repeat_detail = $aJetRecurrence['repeat_detail'];
            $repeat_enddate_typa = $aJetRecurrence['repeat_enddate_type']; $repeat_enddate = $aJetRecurrence['repeat_enddate'];

            $oRepeatEnd = new DateTime($repeat_enddate);
            $repeat_enddate = $oRepeatEnd->format('m/d/y');

            $req['Repeat'] = 1;
            $req['repeat_detail'] = $repeat_detail;
            $req['repeat_enddate_type'] = $repeat_enddate_type;
            $req['repeat_enddate'] = $repeat_enddate;
        }

        //error_log(print_r($req,1));
        //echo '<pre>'; print_r($req); echo '</pre>'; exit;

        $sync = false;
        $status = $oEventNew->SaveForm($req, false, 'dashboard', false, $sync);
        /* TODO error handling */
        //echo '<pre>'; print_r($status); echo '</pre>';
        //error_log(print_r($status,1));
        return true;
    }

    private function existGoogleEvent($googleEventId) {
        $sql = 'select EventID from Event where googleEventId = ?';
        $params = array();
        $params[] = array(DTYPE_STRING, $googleEventId);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aEvent = DbConnManager::GetDb('mpower')->Exec($sql);
        if (count($aEvent) > 0) {
                return $aEvent[0]['EventID'];
        }
        return false;
    }

    private function updateJetEvent($eventId, $googleEvent ) {

        /* we dont need to get. Just update google setting
        $jetEvent = $this->getJetstreamEvent($eventId);
        $startDate = $jetEvent['StartDateUTC'];
        $startDate = GoogleConnect::convertGoogleDateFormat($startDate);
        $status = $jetEvent['status'];
        error_log("Start Date of $eventId : $startDate");
        error_log("Status of $eventId : $status");
        */
    
        if ( isset($googleEvent['start']['dateTime']) ) {
            $start = $googleEvent['start']['dateTime'];     
        }

        if ( isset($googleEvent['end']['dateTime']) ) {
            $end = $googleEvent['end']['dateTime'];     
        }

        $googleStatus = $googleEvent['status'];
        $closed = ($googleStatus == GOOGLE_EVENT_STATUS_CANCELLED) ? 1 : NULL;

        $attendees = isset($googleEvent['attendees']) ? $googleEvent['attendees'] : array();
        $this->updateJetstreamAttendees($eventId, $attendees);

        /* TODO. it's combine of google description + attendee. hard to merge and upage in Jet side */
        //$this->updateJetstreamNotes($eventId, $description);

        /* Generate Jetstream style Recurrence */
        $repeatParent = '';
        $repeatType = 0;
        $repeat_detail = array();
        $repeatEndDateUTC = '';
        $repeat_interval = array();
        $freq = '';
        if (isset($googleEvent['recurrence'])) {
                $googleRecurrence = $googleEvent['recurrence'][0];
                $aJetRecurrence = $this->generateJetstreamRecurrence($googleRecurrence);
                $repeat_detail = $aJetRecurrence['repeat_detail'];
                $repeat_enddate_type = $aJetRecurrence['repeat_enddate_type'];
                $repeat_enddate = $aJetRecurrence['repeat_enddate'];
                $freq = $aJetRecurrence['freq'];

                $oRepeatEnd = new DateTime($repeat_enddate);
                $repeat_enddate = $oRepeatEnd->format('m/d/y');
        
                $repeat_interval = array();
                switch($freq) {
                    case '' :
                        break;
                    case 'DAILY' :
                        $repeat_interval[0] = $repeat_detail['daily_freq'];
                        $repeatType = 1;    
                        break;
                    case 'WEEKLY' :
                        $repeat_interval[0] = $repeat_detail['weekly_freq'];
                        $repeatType = 2;    
                        break;
                    case 'MONTHLY' :
                        $repeat_interval[0] = $repeat_detail['monthly_freq'];
                        $repeatType = 3;    
                        break;
                    case 'YEARLY' :
                        $repeat_interval[0] = $repeat_detail['yearly_freq'];
                        $repeatType = 4;    
                        break;
                }
                $repeat_interval = serialize($repeat_interval);
            }

            $sql = 'Update Event Set StartDateUTC = ?, EndDateUTC = ?,';
            if ($repeatType > 0 ) {
                $sql.= 'RepeatEndDateUTC = ?, RepeatType = ?, RepeatInterval = ?,';
            }
                    $sql.= ' Closed = ? Where EventID = ?';
            $params = array();
                    $params[] = array(DTYPE_STRING, $start);
                    $params[] = array(DTYPE_STRING, $end);
            if ($repeatType > 0 ) {
                        $params[] = array(DTYPE_STRING, $repeat_enddate);
                        $params[] = array(DTYPE_INT, $repeatType);
                //$params[] = array(DTYPE_INT, $repeatParent);
                $params[] = array(DTYPE_STRING, $repeat_interval);
            }
            $params[] = array(DTYPE_INT, $closed);
            $params[] = array(DTYPE_INT, $eventId);
            $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
            $status = DbConnManager::GetDb('mpower')->Exec($sql);

            if (!$status) {
                    return false;
            }
            return true;
    }

    /* used just update Duedate and status */
    private function getJetstreamEvent( $eventId ) {
        $sql = 'select * from Event where eventId = ?';
        $params = array();
        $params[] = array(DTYPE_INT, $eventId);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aEvent = DbConnManager::GetDb('mpower')->Exec($sql);

        if (count($aEvent) > 0) {
            return $aEvent[0];
        }
        return false;
    }

    private function generateAttendees( $attendees ) {
        $userID = $this->user;
        $list = array($userID);
        foreach($attendees as $attendee) {
            $aPerson = $this->getPersonWithEmail($attendee['email']);
            $id = $aPerson['personId'];
            $list[] = $id;
        }
        return json_encode($list);
    }

    private function generateAttendeesInNotes($attendees) {
        $desc = '';
        foreach($attendees as $attendee) {
            $aPerson = $this->getPersonWithEmail($attendee['email']);
            $name = '';
            if (false != $aPerson) {
                $name = $aPerson['FirstName'] . ' ' . $aPerson['LastName'];
            } else {
                $name = isset($attendee['displayname']) ? $attendee['displayname'] : '';
            }
            if ('' == $name) {
                $desc .= $attendee['email'] . "\n";
            } else {
                $desc .= "$name (". $attendee['email'] .") \n";
            }
        }
        if ('' != $desc) {
            // TODO get user firstname and lastname with existing method, besso 
            //$hostInfo = $_SESSION['USER']['FIRSTNAME'] . ' ' . $_SESSION['USER']['LASTNAME'] . "\n";    
            $hostInfo = '';
            $desc = "\n[Attendees]\n" . $hostInfo . $desc;
        }
        return $desc;
    }

    private function getGoogleEventId($id) {
        $sql = 'Select TOP 1 googleEventId from event
                Where EventID = ?';
        $params = array();
        $params[] = array(DTYPE_INT, $id);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aEvent = DbConnManager::GetDb('mpower')->Exec($sql);

        if (count($aEvent) > 0) {
            return $aEvent[0]['googleEventId'];
        }
        return false;
    }

    private function getPersonWithEmail($email) {
        $sql = 'Select TOP 1 personId,FirstName,LastName from People 
            Join Contact on Contact.contactID = personID
            where InActive = 0 and Email = ?';
        $params = array();
        $params[] = array(DTYPE_STRING, $email);
                $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aPerson = DbConnManager::GetDb('mpower')->Exec($sql);
        if (count($aPerson) > 0) {
            return $aPerson[0];
        }

        return false;
    }

    private function getNotes($eventId) {
        $notes = '';    
                $sql = 'Select NoteText from Notes
             where ObjectReferer = ?';
        $params = array();
        $params[] = array(DTYPE_INT, $eventId);
                $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aNote = DbConnManager::GetDb('mpower')->Exec($sql);
        if (isset($aNote)){
            foreach($aNote as $idx => $note) {
                //$notes .= "[Note $idx]\n";
                $notes .= $note['NoteText'] . "\n\n";
            }
        }
        return $notes;
    }

    /*
    private function updateJetstreamNotes($eventId) {
        $notes = $this->getNotes($eventId);
        return false;
    }
    */

    private function updateJetstreamAttendees($eventId, $googleAttendees) {

        $sql = 'Delete FROM EventAttendee WHERE EventID = ?';
        $params = array();
            $params[] = array(DTYPE_INT, $eventId);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
                $status = DbConnManager::GetDb('mpower')->Exec($sql);

        $userID = $this->user;
        $attendees = array($userID);

        foreach($googleAttendees as $googleAttendee) {
            $aPerson = $this->getPersonWithEmail($googleAttendee['email']);
            $personID = $aPerson['personId'];

            $attendees[] = $personID;
        }
        /* TODO which add same attendee ? */
        $attendees = array_unique($attendees);

        foreach($attendees as $attendee) {
            if ('' == $attendee) continue;
            $creator = ( $attendee == $userID ) ? 1 : 0;
            $sql = 'Insert into EventAttendee (EventID, PersonID, Creator)
                Values (?,?,?)';
            $params = array();
                    $params[] = array(DTYPE_INT, $eventId);
                    $params[] = array(DTYPE_INT, $attendee);
                    $params[] = array(DTYPE_INT, $creator);
                    $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
                    $status = DbConnManager::GetDb('mpower')->Exec($sql);
        }

        return true;
    }

    private function deleteJetEvents($googleLists) {
        //echo '<pre>'; print_r($googleLists); echo '</pre>'; exit;
        $jetLists = $this->getJetstreamEvents();
        //error_log(print_r($jetLists,true)); exit;
        $googleCount = count($googleLists);
        $jetCount = count($jetLists);

        if ($googleCount != $jetCount) {
            $gLists = array();
            foreach ($googleLists as $googleList ) {
                $gLists[] = $googleList['id']; 
            }
            $jLists = array();
            foreach ($jetLists as $jetList) {
                $jLists[] = $jetList['id'];
            }

            $aDiff = array_diff($jLists,$gLists);
            //if (count($aDiff) == ($jetCount - $googleCount)) {
                $this->closeJetEvents($aDiff);
            //}
        }
        return false;
    }

    private function closeJetEvents($aEvent) {
        foreach($aEvent as $googleId) {
            //error_log('Deleted Jet Event with ' . $googleId);
            $sql = 'Update Event set Cancelled = 1, Closed = 1 where googleEventId = ?';
            $params = array();
            $params[] = array(DTYPE_STRING, (string)$googleId);
                    $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
            //error_log('Deleted Jet Event with ' . $sql);
                    $status = DbConnManager::GetDb('mpower')->Exec($sql);
        }
        return true;
    }

    /* The date condition should be 00:00:00 of today */
    private function getJetstreamEvents() {
        $sql = 'SELECT E.googleEventId as id FROM Event E
              JOIN EventAttendee EA on EA.eventId = E.eventId
             WHERE EA.personId = ?
               AND EA.Creator = 1
               AND E.Closed IS NULL
               AND E.StartDateUTC >= ?';
        $params[] = array(DTYPE_INT, $this->user);
        $date = GoogleConnect::getTodayWithJetstreamFormat("Ymd"); 
        $params[] = array(DTYPE_STRING, $date);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aJetstreamEvent = DbConnManager::GetDb('mpower')->Exec($sql);
        return $aJetstreamEvent;
    }

    public function setCompletedGoogleEvent($googleId) {
        $existEvent = $this->calendar->events->get($this->calendarID, $googleId);
        $existEvent = new Google_Calendar($existEvent);
        $existEvent->setStatus('completed');
        $createdEvent = $this->calendar->events->update( $this->calendarID, $googleId, $existEvent );
        return true;
    }

    public function setDeletedGoogleEvent($googleId) {
        $this->calendar->events->delete( $this->calendarID, $googleId );
        return true;
    }

    /* Generate Jetstream style Recurrence */
    private function generateJetstreamRecurrence($googleRecurrence) {
        $aJetstreamRecurrence = array(
                    'repeat_detail' => array(),
                    'repeat_enddate_type' => 0,
                    'repeat_enddate' => '',
            'freq' => ''
        );

        $googleRecurrence = str_replace('RRULE:', '', $googleRecurrence); 
        $aRecurrence = explode(';', $googleRecurrence);

        $freq = $until = '';
        /* if interval is 1 (day,week,month) , Google does not send INTERVAL key, so define as 1 */
        $interval = 1;
        foreach( $aRecurrence as $row ) {
            $aRow = explode('=', $row);
            if(GOOGLE_RECURRENCE_FREQ == $aRow[0]) {
                $freq = $aRow[1];
            }
            if(GOOGLE_RECURRENCE_UNTIL == $aRow[0]) {
                $until = $aRow[1];
            }
            if(GOOGLE_RECURRENCE_INTERVAL == $aRow[0]) {
                $interval = $aRow[1];
            }
        }
        switch ($freq) {
            case 'DAILY' :
                $repeat_detail = array(
                    'daily_freq' => $interval
                );
                break;
                
            case 'WEEKLY' :
                $repeat_detail = array(
                    'weekly_freq' => $interval, 
                    //'weekly_day' => array('filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY)
                );
                break;
                
            case 'MONTHLY' :
                $repeat_detail = array(
                    'monthly_freq' => $interval,
                    //'monthly_type' => FILTER_SANITIZE_NUMBER_INT,
                    //'monthly_daynum' => FILTER_SANITIZE_NUMBER_INT,
                    //'monthly_pos' => FILTER_SANITIZE_NUMBER_INT,
                    //'monthly_pos_type' => FILTER_SANITIZE_NUMBER_INT
                );
                break;
                
            case 'YEARLY' :
                $repeat_detail = array(
                    'yearly_freq' => $interval,
                    //'yearly_type' => FILTER_SANITIZE_NUMBER_INT,
                    //'yearly_monthnum' => FILTER_SANITIZE_NUMBER_INT,
                    //'yearly_daynum' => FILTER_SANITIZE_NUMBER_INT,
                    //'yearly_pos' => FILTER_SANITIZE_NUMBER_INT,
                    //'yearly_pos_type' => FILTER_SANITIZE_NUMBER_INT,
                    //'yearly_pos_month' => FILTER_SANITIZE_NUMBER_INT
                );
                break;
                
            default :
                $repeat_detail = array();
        }

        $aJetstreamRecurrence['freq'] = $freq;
        $aJetstreamRecurrence['repeat_detail'] = $repeat_detail;

        if (count($repeat_detail) > 0) {
            if ('' != $until) {
                $aJetstreamRecurrence['repeat_enddate_type'] = 1;
                $aJetstreamRecurrence['repeat_enddate'] = $until;
            }
        }

        return $aJetstreamRecurrence;
        

    }

    private function generateGoogleRecurrence($jetRepeatType, $jetRepeatEnd, $jetRepeatInterval) {

        $repeat_detail = unserialize($jetRepeatInterval);
        $repeat_interval = array();
        switch($jetRepeatType) {
        case REPEAT_NONE :
                break;
        case REPEAT_DAILY :
                $repeat_interval['daily_freq'] = $repeat_detail[0];
            $freq = 'DAILY';
                break;
        case REPEAT_WEEKLY :
                list($repeat_interval['weekly_freq'], $repeat_interval['weekly_days']) = $repeat_detail;
            $freq = 'WEEKLY';
                break;
        case REPEAT_MONTHLY :
                list($repeat_interval['monthly_freq'], $repeat_interval['monthly_day']) = $repeat_detail;
            $freq = 'MONTHLY';
                break;
        case REPEAT_MONTHLY_POS :
                list($repeat_interval['monthly_freq'], $repeat_interval['monthly_pos'], $repeat_interval['monthly_pos_type']) = $repeat_detail;
            $freq = 'MONTHLY';
                break;
        case REPEAT_YEARLY :
                list($repeat_interval['yearly_freq'], $repeat_interval['yearly_month'], $repeat_interval['yearly_month_day']) = $repeat_detail;
            $freq = 'YEARLY';
                break;
        case REPEAT_YEARLY_POS :
                list($repeat_interval['yearly_freq'], $repeat_interval['yearly_pos'], $repeat_interval['yearly_pos_type'], $repeat_interval['yearly_pos_month']) = $repeat_detail;
            $freq = 'YEARLY';
                break;
        }   
        $end = GoogleConnect::convertGoogleDateFormat($jetRepeatEnd, 'google-recurrence');
        $interval = $repeat_interval['daily_freq'];
        $googleRecurrence = array("RRULE:FREQ=$freq;UNTIL=$end;INTERVAL=$interval");
        return $googleRecurrence;
    }

} 
