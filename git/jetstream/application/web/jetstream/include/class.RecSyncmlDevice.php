<?php
/**
 * @package database
 */

require_once 'class.MpRecord.php';
require_once 'mpconstants.php';

/**
 * The DB fields initialization
 */
class RecSyncmlDevice extends MpRecord
{
	public function Initialize() {
		$this->db_table = 'SyncmlDevice';
		$this->recordset->GetFieldDef('SyncmlDeviceID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('DeviceID')->required = TRUE;

		parent::Initialize();
	}
}
