<?php
class ReportUtil
{

	/**
	 * Due to a limitation with phpExcel, a row height
	 * must be specified when applying wrapping to a merged row
	 * 
	 * String form:
	 * 
	 * Salesperson(s): Brian Mackley, Bill White, Boris Palanov, Darryl Dunnington, Denise Greenberg, Doug Wheeler, Dwayne Bosse,
	 * Jess Campbell, Jim Labosky, Lori Maner, Scott Willingham, Terrance Menyweather, Wayne Mackley, John Bogard, Aaron Ross,
	 * Bob Bryant, Steve Contreras, Gary Taylor, Abby Smith, Sherra Boyd
	 */
	public static function calcSalesPersonRowHeight($str)
	{
		$height = 15;
		$break = 7;
		$px = 15;
		
		$parts = explode(':', $str);
		$pieces = explode(',', $parts[1]);
		
		$count = count($pieces);
		
		return $height + (round($count/$break) * $px);
		
	}
}