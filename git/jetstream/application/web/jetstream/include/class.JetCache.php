<?php

class JetCache
{
	private static $cache_files = array();
	private static $cache_path = array();
	private static $shutdown = FALSE;
	private static $index_file = 'file_index.csh';

	/*
	 * Load a cache file from disk
	 */
	static public function Load($company_id, $cache_name) {
		// echo "Jetcache Load <pre>" . print_r(func_get_args(), TRUE) . '</pre>';
		if (!isset($company_id) || !isset($cache_name)) {
			return FALSE;
		}

		// Load the cache file if it isn't already loaded
		if (count(self::$cache_files) == 0) {
			self::LoadFileIndex($company_id);
		}
		
		// Return cache if this cache exists for this company
		if (isset(self::$cache_files[$company_id][$cache_name])) {
			$file = self::CachePath($company_id) . self::$cache_files[$company_id][$cache_name];
			if (is_readable($file)) {
				$cache_file = file_get_contents($file);
				return unserialize($cache_file);
			}
		}
		return FALSE;
	}

	/*
	 * Save a cache file to disk
	 */
	static public function Save($company_id, $cache_name, $cache_object) {
		if (!isset($company_id) || !isset($cache_name) || !isset($cache_object)) return FALSE;
		// if (!isset($_SESSION['company_id'])) return FALSE;
		
		// Load the cache file if it isn't already loaded
		if (!isset(self::$cache_files[$company_id])) {
			self::LoadFileIndex($company_id);
		}
		// If this cache already exists, get the name so we can replace, otherwise create a unique ID
		if (isset(self::$cache_files[$company_id][$cache_name])) {
			$file_name = self::$cache_files[$company_id][$cache_name];
		} else {
			$file_name = uniqid() . '.csh';
		}
		$file = self::CachePath($company_id) . $file_name;
		
		file_put_contents($file, serialize($cache_object));
		
		// Add this file name to the index
		self::AddFileIndex($company_id, $cache_name, $file_name);
	}

	/*
	 * Get the path to the cache for the current user
	 * Also creates the cache directory if it is not present
	 */
	static private function CachePath($company_id) {
		// We store the paths in a static var so we can avoid running file_exists every time
		// the cache path is accessed
		if (!isset(self::$cache_path[$company_id])) {
			$base_cache_path = CACHE_PATH . '/jetcache/';
			self::$cache_path[$company_id] = $base_cache_path . $company_id . '/';
			if (!file_exists(self::$cache_path[$company_id])) {

				mkdir(self::$cache_path[$company_id], 0755, TRUE);
			}
		}
		return self::$cache_path[$company_id];
	}

	/*
	 * Load the file index for the cache files (automatic)
	 */
	static private function LoadFileIndex($company_id) {
		$file = self::CachePath($company_id) . self::$index_file;
		if (is_readable($file)) {
			// Index already exists
			$contents = file_get_contents($file);
			$cache_files = unserialize($contents);
			self::$cache_files[$company_id] = $cache_files;
		} else {
			// Create empty array as starting place for new index
			self::$cache_files[$company_id] = array();
		}
	}

	/*
	 * Add a file name to the file index and set JetCache to save when prog terminates
	 * Note:  using register_shutdown_function, because static functions cannot use __destructor
	 */	
	static private function AddFileIndex($company_id, $cache_name, $file_name) {
		// To avoid registering the save multiple times, $shutdown static function indicates if it has been
		// registered
		if (!self::$shutdown) {
			self::$shutdown = TRUE;
			register_shutdown_function(array('JetCache', 'SaveFileIndex'));
		}
		self::$cache_files[$company_id][$cache_name] = $file_name;
	}

	static public function SaveFileIndex() {
		foreach (self::$cache_files as $company => $cache_file) {
			$file = self::CachePath($company) . self::$index_file;
			file_put_contents($file, serialize($cache_file));
		}
	}
}

?>