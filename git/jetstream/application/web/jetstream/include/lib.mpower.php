<?php
/**
 * @package library
 */

require_once 'class.SqlBuilder.php';
require_once 'class.DbConnManager.php';
require_once 'mpconstants.php';
require_once 'lib.string.php';


/**
 * Lookup an M-Power PersonID by their CMID
 *
 * @param string $source_name
 * @param int $company
 * @return object
 */
function LookupPersonIdByCmid($cmid) {
    $user_sql = "SELECT PersonID, CompanyID
        FROM People
        WHERE CMID = ?";
    
    $user_sql = SqlBuilder()->LoadSql($user_sql)->BuildSql(array(DTYPE_STRING, SfFormatId($cmid)));
    $recordset = DbConnManager::GetDb('mpower')->Execute($user_sql);

    switch (count($recordset)) {
        case 0:
            return FALSE;
        case 1:
            return $recordset[0];
        default:
            throw new Exception('Multiple users match: ' . $cmid);
    }
}

/**
 * Lookup the M-Power SourceId by the name string
 *
 * @param string $source_name
 * @param int $company
 * @return object
 */
function LookupSourceByName($source_name, $company) {
    $sql = "SELECT SourceID
        FROM Sources
        WHERE Name = ?
            AND CompanyID = ?
            AND Deleted = 0";

    $source_name->type = DTYPE_STRING;
    $company->type = DTYPE_INT;

    $sql = SqlBuilder()->LoadSql($sql)->BuildSql($source_name, $company);
    $recordset = DbConnManager::GetDb('mpower')->Execute($sql);

    switch (count($recordset)) {
        case 0:
            return FALSE;
        case 1:
            return $recordset[0]->SourceID;
        default:
            throw new Exception('Multiple sources match: ' . $source_name);
    }
}

/**
 * Lookup the M-Power ValuationId by the name string
 *
 * @param string $valuation_name
 * @param int $company
 * @return object
 */
function LookupValuationByName($valuation_name, $company) {
    $sql = "SELECT ValID, VLevel
        FROM Valuations
        WHERE Label = ?
            AND CompanyID = ?
            AND Activated = 1";

    $valuation_name->type = DTYPE_STRING;
    $company->type = DTYPE_INT;

    $sql = SqlBuilder()->LoadSql($sql)->BuildSql($valuation_name, $company);
    $recordset = DbConnManager::GetDb('mpower')->Execute($sql);

    switch (count($recordset)) {
        case 0:
            return FALSE;
        case 1:
            return $recordset[0];
        default:
            throw new Exception('Multiple valuations match: ' . $valuation_name);
    }
}

/**
 * Get the single char data type used by M-Power
 * Input is usually the datatype from SQL Server
 *
 * @param string $type
 * @return char
 */
function GetMpDataType($type) {
    switch ($type) {
        case 'text':
        case 'char':
            return DTYPE_STRING;
        case 'bit':
            return DTYPE_BOOL;
        case 'datetime':
            return DTYPE_TIME;
        case 'real':
            return DTYPE_FLOAT;
        case 'numeric':
        case 'int':
            return DTYPE_INT;
        default:
            throw new Exception('GetMpDataType: Unexpected data type: ' . $type);
    }
}

function DumpVar($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}
?>
