<?php

class DateTimeUtil {
	
	
	private $hours = array();
	private $minutes = array();
	private $periods = array();
	
	public function __construct(){
		$this->setHours();
		$this->setMinutes();
		$this->setPeriods();
	}
	
	private function setHours(){
		for($i=1;$i<13;$i++){
			$this->hours[] = str_pad($i, 2, "0", STR_PAD_LEFT);
		}
	}
	private function setMinutes(){
		for($i=0;$i<60;$i++){
			if($i % 15 == 0){
				$this->minutes[] = str_pad($i, 2, "0", STR_PAD_LEFT);
			}
		}
	}
	private function setPeriods(){
		$this->periods = array('AM', 'PM');
	}

	public function getHours(){
		return $this->hours;
	}
	public function getMinutes(){
		return $this->minutes;
	}
	public function getPeriods(){
		return $this->periods;
	}
	
	
	private function buildTimeSelect($type, $id, $selected='', $disabled=false){
		
		$id = (!$id) ? $type : $id;
		
		switch($type){
			case 'hours':
				$list = $this->getHours();
				break;
			case 'minutes':
				$list = $this->getMinutes();
				break;
			case 'periods':
				$list = $this->getPeriods();
				break;
		}
		
		if($disabled){
			$disabled = 'disabled="disabled"';
		}
		
		
		$html = '<select name="'. $id .'" id="' . $id . '"' . $disabled . '>';
		foreach($list as $item){
			$select = ($item == $selected) ? ' selected="selected"' : '';
			$html .= '<option value="' . $item . '"' . $select . '>' . $item . '</option>';
		}
		$html .= '</select>';
		
		return $html;
	}
	
	public function buildHourSelect($id=null){
		return $this->buildTimeSelect('hours', $id, '12', true);	
	}
	public function buildMinuteSelect($id=null){
		return $this->buildTimeSelect('minutes', $id, '', true);
	}
	public function buildPeriodSelect($id=null){
		return $this->buildTimeSelect('periods', $id, 'PM', true);
	}
	
	
	
}