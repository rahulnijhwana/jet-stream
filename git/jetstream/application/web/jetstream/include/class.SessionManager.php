<?php

class SessionManager
{
	/**
	 * The single instance of the class
	 * @var object
	 */
	private static $instance;
	private static $session_name;
	public static $check_eula = TRUE;

	/**
	 * Private constructor to dissallow instantiaion
	 */
	private function __construct() {
	}

	/**
	 * Returns a single instance of the object
	 * @return object
	 */
	public static function GetInstance() {
		if (!isset(self::$instance)) {
			$object = __CLASS__;
			self::$instance = new $object();
		}
		return self::$instance;
	}

	public static function CreateUrl($page, $query_string = false) {
		$url = $page . '?SN=' . self::$session_name;
		if ($query_string) {
			$url .= '&amp;' . $query_string;
		}
		return $url;
	}

	public static function Init() {
		if (isset($_GET['SN'])) {
			self::$session_name = $_GET['SN'];
		} elseif (isset($_COOKIE['SN'])) {
			self::$session_name = $_COOKIE['SN'];
		} else {
			$session_name = uniqid('');
			self::$session_name = $session_name;
		}
		/* if the $check_eula is true, then it will not check for any eula value, while loading the application.
		This is needed to load the company/contact autocomplete field through ajax.
		Else it was not working in jetstream unless and until the mpower eula is accepted. */
		//self::$check_eula	= false;
		session_name(self::$session_name);
		@session_start();
		if (isset($_GET['pov'])) {
			$_SESSION['pov'] = (int) $_GET['pov'];
		}
		if (isset($_GET['combined'])) {
			$_SESSION['combined'] = ($_GET['combined'] == 'true') ? true : false;
		}
		if (isset($_GET['target']) && $_GET['target'] == 'false') {
			unset($_SESSION['target']);
		}
		
		$page = (isset($_SESSION['sp_page'])) ? $_SESSION['sp_page'] : 0;
		if (isset($_POST['paging'])) {
			switch ($_POST['paging']) {
				case '<<' :
					$page -= 10;
					break;
				case '<' :
					$page -= 1;
					break;
				case '>' :
					$page += 1;
					break;
				case '>>' :
					$page += 10;
					break;
			}
		}
		$_SESSION['sp_page'] = $page;
		
		return self::GetInstance();
	}

	public static function Destroy() {
		if (!self::$session_name) {
			self::Init();
		}
		if (isset($_COOKIE['SN'])) {
			setcookie($_COOKIE['SN'], "", time() - 3600, '/');
			setcookie('SN', "", time() - 3600);
		}
		session_unset();
		session_destroy();
		$_SESSION = array();
		return self::GetInstance();
	}

	//	public function CreateAuth() {
	//		$_SESSION['auth'] = $login->UserID . ':' . crypt($login->PersonID . $company_id . $_SERVER['HTTP_USER_AGENT']);
	//		$_SESSION['login_id'] = $login->PersonID;
	//	}
	

	public static function CreateToken($person, $company) {
		$_SESSION['token'] = crypt(trim($person) . trim($company) . trim($_SERVER['HTTP_USER_AGENT']));
		return self::GetInstance();
	}

	public static function ClearToken() {
		if (isset($_SESSION['token'])) unset($_SESSION['token']);
		return self::GetInstance();
	}

	public static function Validate() {
		global $app_slipstream;
				
		/* This validate function is called in every page.
		So the eula is chked, in every page. So that user can't load the application without accepting the eula.*/
		if (self::$check_eula != FALSE) {
			
			if(isset($app_slipstream) && ($app_slipstream == true)) {
				if(isset($_SESSION['SLIPSTREAM_EULA']) && ($_SESSION['SLIPSTREAM_EULA'] == false)) {					
					header('Location:'.WEB_PATH.'/index.php?company='.$_SESSION['USER']['COMPANY'].'&SN='.$_GET['SN']);
				}
			} else {
				if(isset($_SESSION['MPOWER_EULA']) && ($_SESSION['MPOWER_EULA'] == false)) {
					header('Location:'.WEB_PATH.'/index.php?company='.$_SESSION['USER']['COMPANY'].'&SN='.$_GET['SN']);
				}
			}
		}
		//print_r($_SESSION);//exit;
		if (isset($_SESSION['token']) && isset($_SESSION['login_id'])) {
			$token = $_SESSION['token'];
			if (crypt(trim($_SESSION['login_id']) . trim($_SESSION['company_id']) . trim($_SERVER['HTTP_USER_AGENT']), $token) == $token) {
				return true;
			}
		}
		
		//echo 'Session failed...<br>';
		//print_r($_SESSION);
		//header("Location: ".WEB_PATH."/index.php");
		echo '<script language="javascript">
		window.location.href=\''.WEB_PATH.'/index.php?smsg=Session failed...\';
		</script>';
		return false;
	}

}
