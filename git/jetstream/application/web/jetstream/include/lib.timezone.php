<?php

/**
 *Return the current timestamp according to logedin user's timezone
 */

function GetTimestampByTimezone() {
	$local_timezone = $_SESSION['USER']['TIMEZONE'];
	$system_timezone = date_default_timezone_get();
	date_default_timezone_set($local_timezone);
	$local_timearra = getdate();
	date_default_timezone_set($system_timezone);
	$local_timestamp = date("Y-m-d H:i:s", mktime($local_timearra['hours'], $local_timearra['minutes'], $local_timearra['seconds'], $local_timearra['mon'], $local_timearra['mday'], $local_timearra['year']));
	return $local_timestamp;
}

/**
 *Convert the time in GMT timestamp into  user's local time zone timestamp
 * @param time $gmttime
 * @param string $timezoneRequired
 * $gmttime should be in timestamp format like '02-06-2009 09:48:00.000'
 * $timezoneRequired sholud be a string like 'Asia/Calcutta' not 'IST' or 'America/Chicago' not 'CST'
 * return timestamp format like '02-06-2009 09:48:00' (m-d-Y H:i:s) Can also change this format 
 * $timestamp = $date->format("m-d-Y H:i:s"); decide the return format
 */

function ConvertGMTToLocalTimezone($gmttime, $timezoneRequired) {
	$system_timezone = date_default_timezone_get();
	date_default_timezone_set("GMT");
	$gmt = date("Y-m-d h:i:s A");
	$local_timezone = $timezoneRequired;
	
	if(trim($local_timezone) == '') {
		$local_timezone = $_SESSION['USER']['TIMEZONE'];
	}
	if(trim($local_timezone) == '') {
		$local_timezone = $_SESSION['USER']['BROWSER_TIMEZONE'];
	}
	if (!$local_timezone) {
		$local_timezone = 'America/Chicago';
	}
	
	date_default_timezone_set($local_timezone);
	$local = date("Y-m-d h:i:s A");
	date_default_timezone_set($system_timezone);
	$diff = (strtotime($local) - strtotime($gmt));
	$date = new DateTime($gmttime);
	$date->modify("+$diff seconds");
	$timestamp = $date->format("Y-m-d H:i:s");
	return $timestamp;
}

/**
 *Convert the time in user's local time zone timestamp into GMT timestamp   
 * @param time $gmttime
 * @param string $timezoneRequired
 * $gmttime should be in timestamp format like '02-06-2009 09:48:00.000'
 * $timezoneRequired sholud be a string like 'Asia/Calcutta' not 'IST' or 'America/Chicago' not 'CST'
 * return timestamp format like '02-06-2009 09:48:00' (m-d-Y H:i:s) Can also change this format 
 * $timestamp = $date->format("m-d-Y H:i:s"); decide the return format
 */
function ConvertLocalTimezoneToGMT($gmttime, $timezoneRequired) {
	$system_timezone = date_default_timezone_get();
	
	$local_timezone = $timezoneRequired;
	date_default_timezone_set($local_timezone);
	$local = date("Y-m-d h:i:s A");
	
	date_default_timezone_set("GMT");
	$gmt = date("Y-m-d h:i:s A");
	
	date_default_timezone_set($system_timezone);
	$diff = (strtotime($gmt) - strtotime($local));
	$date = new DateTime($gmttime);
	$date->modify("+$diff seconds");
	$timestamp = $date->format("Y-m-d H:i:s");
	return $timestamp;
}

/**
 *Convert the time in user's local time zone timestamp into another time zone timestamp 
 * @param time $gmttime
 * @param string $timezoneRequired
 * $gmttime should be in timestamp format like '02-06-2009 09:48:00.000'
 * $timezoneRequired sholud be a string like 'Asia/Calcutta' not 'IST' or 'America/Chicago' not 'CST'
 * return timestamp format like '02-06-2009 09:48:00' (m-d-Y H:i:s) Can also change this format 
 * $timestamp = $date->format("m-d-Y H:i:s"); decide the return format
 */
function ConvertOneTimezoneToAnotherTimezone($time, $currentTimezone, $timezoneRequired) {
	$system_timezone = date_default_timezone_get();
	
	$local_timezone = $currentTimezone;
	date_default_timezone_set($local_timezone);
	$local = date("Y-m-d h:i:s A");
	
	date_default_timezone_set("GMT");
	$gmt = date("Y-m-d h:i:s A");
	
	$require_timezone = $timezoneRequired;
	date_default_timezone_set($require_timezone);
	$required = date("Y-m-d h:i:s A");
	
	date_default_timezone_set($system_timezone);
	$diff1 = (strtotime($gmt) - strtotime($local));
	$diff2 = (strtotime($required) - strtotime($gmt));
	$date = new DateTime($time);
	$date->modify("+$diff1 seconds");
	$date->modify("+$diff2 seconds");
	$timestamp = $date->format("Y-m-d H:i:s");
	return $timestamp;
}

?>