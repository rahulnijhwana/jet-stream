<?php
require_once 'class.DbConnManager.php';
require_once 'class.SqlBuilder.php';

class AccountUtil {
	
	public static function getAccountName($accountId){
		$sql = "SELECT Text01 FROM Account WHERE AccountID = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $accountId));
		
		$account = DbConnManager::GetDb('mpower')->GetOne($sql);
		
		return $account['Text01'];
	}
	
}
