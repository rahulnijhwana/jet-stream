<?php
require_once 'class.DbConnManager.php';
require_once 'class.MpRecordset.php';
require_once 'class.SqlBuilder.php';
require_once 'class.JetCache.php';

/**
 * Caches the EventType table data to the session
 *
 */
class OptionLookup
{	protected static $lookup_data = array();
	protected static $cache_name = 'optionset';

	public static function Load($company_id, $optionset_id) {
		if (empty(self::$lookup_data[$company_id][$optionset_id])) {
			$cache_name = self::$cache_name . "_" . $optionset_id;
			if (!$load_array = JetCache::Load($company_id, $cache_name)) {
				$load_array = self::LoadFromDb($company_id, $optionset_id, $cache_name);
			}
			self::$lookup_data[$company_id][$optionset_id] = $load_array;
		}
		return self::$lookup_data[$company_id][$optionset_id];
	}

	/*
	 * Loads event_types from the database
	 * Can be called from anywhere to reload the event_types
	 */
	public static function LoadFromDb($company_id, $optionset_id, $cache_name) {
		$sql = 'SELECT * FROM [Option] WHERE OptionSetID = ?';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $optionset_id));
		$options = DbConnManager::GetDb('mpower')->Exec($sql);
		$optionset = array();
		foreach ($options as $option) {
			$optionset[$option['OptionID']] = $option;
		}
		JetCache::Save($company_id, $cache_name, $optionset);
		return $optionset;
	}

	public static function GetOptionName($company_id, $optionset_id, $option_id) {
		$optionset = self::Load($company_id, $optionset_id);
		$value = $optionset[$option_id]['OptionName'];
		return $value;
	}

	public static function GetOptionset($company_id, $optionset_id) {
		return self::Load($company_id, $optionset_id);
	}
} 
