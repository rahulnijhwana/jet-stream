<?php
require_once 'class.DbConnManager.php';
require_once 'class.SqlBuilder.php';

class CompanyUtil {
	
	public static function getCompany($companyId){
		$companySql = "SELECT Name FROM Company WHERE CompanyID = ?";
		$companySql = SqlBuilder()->LoadSql($companySql)->BuildSql(array(DTYPE_INT, $companyId));
		
		$company = DbConnManager::GetDb('mpower')->GetOne($companySql);
		
		return $company;
	}
	
	public static function isInactiveUser($person_id){
		
		$sql = "
			SELECT
				SupervisorID
			FROM
				People
			WHERE
				PersonID = ?";
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $person_id));
		$result = DbConnManager::GetDb('mpower')->GetOne($sql);

		return ($result['SupervisorID'] == -1) ? true : false;
	}
}
