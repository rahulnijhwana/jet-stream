<?php
//define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));
//require_once BASE_PATH . '/include/class.DbConnManager.php';
//require_once BASE_PATH . '/include/class.SqlBuilder.php';

/**
 * This class holds the authentication methods for mpower, jetsream and email services
 * @version 1.0 
 * @package authentication
 */
class Authentication 
{
	/**
	 * Returns the company id for the provided company directory
	 * @return company|false Company object or error
	 */
	function GetCompanyRecord($company_dir) {
		$company_sql = 'SELECT * FROM company LEFT OUTER JOIN resellers ON company.ResellerID = Resellers.ResellerID WHERE DirectoryName = ?';
		$company_sql = SqlBuilder()->LoadSql($company_sql)->BuildSql(array(DTYPE_STRING, $company_dir));

		// Run the SQL and get the matching record
		$company_record = DbConnManager::GetDb('mpower')->GetOne($company_sql);
		if (!$company_record instanceof MpRecord) {
			$company_record = false;
		}
			
		return $company_record;
	}	
	
	
	/**
	 * Authenticate the user
	 * @return login|false Login object or error
	 */
	function GetLoginRecord($company_id, $username, $password) {
		$login_sql = 'SELECT cast(logins.ID as real) as ID, logins.Password, logins.PasswordZ, logins.UserID, logins.PersonID,
							 people.SupervisorID, people.IsSalesPerson,people.Level, people.FirstName, people.LastName
						FROM logins
						LEFT JOIN people ON logins.PersonID = people.PersonID
					 	WHERE logins.CompanyID = ? AND logins.UserID = ?';
		$login_sql = SqlBuilder()->LoadSql($login_sql)->BuildSql(array(DTYPE_INT, $company_id), array(DTYPE_STRING, $username));

		$login = DbConnManager::GetDb('mpower')->GetOne($login_sql);

		if (!$login instanceof MpRecord || (($login->SupervisorID == -3 ) ? ($login->PasswordZ != $password) : $login->Password != crypt($password, $login->Password))) {
			$login = false;
		}
	
		return $login;
	}
}

/*
$company = Authentication::GetCompanyRecord('demo09');
var_dump($company);

$login = Authentication::GetLoginRecord(33, 'lfranks', 'drowssap');
var_dump($login);
*/