<?php
/**
 * @package database
 */
require_once('mpconstants.php');

/**
 * Singleton class to connect to the databases and to provide the
 * database object when needed
 */
class DbConnManager {
    /**
     * The single instance of the class
     * @var object
     */
    private static $instance;

    /**
     * The connected database objects
     * @var array
     */
    private static $databases = array();

    /**
     * Modifyable array of connection parameters for the Salesforce DB
     * This concept could easily be generalized for all databases
     * @var array
     */
    private static $sf_params = array();

    /**
     * Private constructor to dissallow instantiaion
     */
    private function __construct() {}

    /**
     * Returns a single instance of the object
     * @return object
     */
    public static function GetInstance(){
        if(!isset(self::$instance)){
            $object = __CLASS__;
            self::$instance = new $object;
        }
        return self::$instance;
    }

    /**
     * Returns the matching database object
     * @param string $name The name of the database
     * @return object
     */
    public static function GetDb ($name) {
        if (!key_exists($name, self::$databases)) {
            switch ($name) {
                case 'mpower':
					require_once 'class.DbConnMssql.php';
                    $db = new DbConnMssql;
                    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                        // Windows default settings
                        //$db->server = "75.101.239.183";
                        $db->server = MP_DATABASE_WINDOWS;
                    }
                    else {
                        // Unix Settings
                        //$db->server = "ASA-VERIO";
//                      $db->server = MP_DATABASE_WINDOWS; // Uncomment for Test server only
                        $db->server = MP_DATABASE_SERVER;
                    }
					
					$db->database_name = MP_DATABASE ; 
					$db->user = MP_DATABASEU;	
					$db->password = MP_DATABASEP;
//                  $db->user = 'mpasac';
//                  $db->password = 'qraal+zv';
//                  $db->database_name = 'test_new'; //'mpasac';//'test';

                    $db->DoConnect();
                    self::$databases[$name] = $db;
    
                    break;
                case 'salesforce':
					require_once 'class.DbConnSalesforce.php';
                    $db = new DbConnSalesforce;
                    foreach (self::$sf_params as $variable => $value) {
                        $db->$variable = $value;
                    }
                    // $db->url = $_GET['url'];
                    // $db->session = $_GET['session'];
                    // $db->username = 'pwillarson@asasales.com';
                    // $db->password = 'sfpassword';
					$db->DoConnect();
                    self::$databases[$name] = $db;
                    break;
                default:
                    throw new Exception('Unknown database specified.');
            }
        }
        return self::$databases[$name];
    }

    public static function SetSfParam($variable, $value) {
        self::$sf_params[$variable] = $value;
    }
}

?>
