<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/include/mpconstants.php';

/**
 * The SubAnswer database record
 * @package database
 * @subpackage record
 */
class SubAnswer extends MpRecord
{
    public function __toString() {
        return 'SubAnswer Record';
    }

    public function __set($variable, $value) {
        switch ($variable) {
            case 'SubID':
                if (!is_null($value)) {
                    parent::__set('cSubID', $value);
                }
                break;
//            case 'c_MsType':
//                if ($value == MSTYPE_YESNO || ) {
//                	$this->recordset->GetFieldDef('Answer')->transform = 'SubAnswerTransform';
//                }
//                if ($value == MSTYPE_CHECKBOX) {
//                	$this->recordset->GetFieldDef('Answer')->transform = 'SubAnswerTrueFalseTransform';
//                }
//                break;
            case 'Answer':
                if (array_key_exists('c_MsType', $this->data)) {
                    switch ($this->data['c_MsType']) {
                        case MSTYPE_YESNO:
                            if ($value == 'Yes') {
                                $value = '1';
                            }
                            if ($value == 'No') {
                                $value = '0';
                            }
                            break;
                        case MSTYPE_CHECKBOX:
                            if ($value == 'true') {
                                $value = '1';
                            }
                            if ($value == 'false') {
                                $value = '0';
                            }
                            break;
                    }
                }
                break;
        }

        parent::__set($variable, $value);
    }

    public function Initialize() {
        $this->db_table = 'SubAnswers';

        $this->recordset->GetFieldDef('SubID')->required = TRUE;
        $this->recordset->GetFieldDef('SubID')->key = TRUE;
        $this->recordset->GetFieldDef('DealID')->required = TRUE;
        $this->recordset->GetFieldDef('DealID')->key = TRUE;
        $this->recordset->GetFieldDef('Answer')->change_required = TRUE;

        // $this->SubID->required = TRUE;
        // $this->SubID->key = TRUE;
        // $this->DealID->required = TRUE;
        // $this->DealID->key = TRUE;
        // $this->Answer->change_required = TRUE;

        $ms_type = MSTYPE_UNKNOWN;

        if($this->data['YesNo'] == 1) {
            $ms_type = MSTYPE_YESNO;
        }
        else if($this->data['AlphaNum'] == 1) {
            $ms_type = MSTYPE_ALPHANUM;
        }
        else if($this->data['CheckBox'] == 1) {
            $ms_type = MSTYPE_CHECKBOX;
        }
        else if($this->data['Calendar'] == 1) {
            $ms_type = MSTYPE_CALENDAR;
        }
        else if($this->data['DropDown'] == 1) {
            $ms_type = MSTYPE_DROPDOWN;
        }

        $this->__set('c_MsType', $ms_type);

        parent::Initialize();

        return $this;
    }
}

?>
