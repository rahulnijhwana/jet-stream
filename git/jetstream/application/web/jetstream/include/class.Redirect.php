<?php
/**
 * Handles the redirection of users with mobile browsers
 * to m.teefury.com
 */
class Redirect {
	
	/**
	 * Redirect user to mobile site if using
	 * a mobile browser and they are not 
	 * explicitly requesting the full site
	 */
	public static function toMobile() {

		$browser = new Browser();
		if ($browser->isMobile()) {
			header("Location: " . MOBILE_LINK);
		}
	}

}
