<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';

class PriorityUtil {
	
	public static function all(){
		$sql = "SELECT * FROM Priority";
		$results = DbConnManager::GetDb('mpower')->Exec($sql);
		return $results;
	}
	
	public static function one($priorityId){
		$sql = "SELECT * FROM Priority WHERE PriorityID = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $priorityId));
		$result = DbConnManager::GetDb('mpower')->GetOne($sql);
		return $result;
	}
}