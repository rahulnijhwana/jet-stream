<?php
// require_once BASE_PATH . '/include/class.DbConnManager.php';
// require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';


// This class is just a wrapper shell for backward compatibility for ReportingTreeLookup

Class ReportingTree
{
	// protected $tree = array();
	public $paging;
	public $total_sp;
	private $target;
	
	// protected $tree;
	
    public function BuildTree() {
		return;
	}

    public function GetPersonByUsername($username) {
    	return ReportingTreeLookup::GetPersonByUsername($_SESSION['company_id'], $username);
    }
    
    public function GetPersonByName($first, $last) {
    	return ReportingTreeLookup::GetPersonByName($_SESSION['company_id'], $first, $last);
    }
    
	/**
	 * Provide an array of all the person data for a combined array
	 * 
	 * @return array
	 */
    public function GetCombinedArray() {
    	return ReportingTreeLookup::GetCombinedArray($_SESSION['company_id']);
    }
        
    public function GetTarget($get_all = false) {
    	$vars = ReportingTreeLookup::GetTarget($_SESSION['company_id'], $get_all);
    	if(is_array($vars) && count($vars) > 0) {
	    	foreach($vars as $key=>$var) {
	    		$this->$key = $var;
	    	}
    	}
    	return $this->target;
	}
    
    public function GetPath($person, $output = null) {
    	return ReportingTreeLookup::GetPath($_SESSION['company_id'], $person, $output);
    }
    
    public function GetRecord($person) {
    	return ReportingTreeLookup::GetRecord($_SESSION['company_id'], $person);
    }
    
    public function GetInfo($person, $type) {
    	return ReportingTreeLookup::GetInfo($_SESSION['company_id'], $person, $type);
    }

    public function GetRoots() {
    	return ReportingTreeLookup::GetRoots($_SESSION['company_id']);
    }        

	/**
	 * Returns a list of the start and their children that are salespeople
	 * Does not proceed into grandchildren
	 *
	 * @param int $start_id
	 * @return array
	 */
    public function GetDirectSp($start_id) {
    	return ReportingTreeLookup::GetDirectSp($_SESSION['company_id'], $start_id);
    }
    
    public function GetLimb($start_id) {
    	return ReportingTreeLookup::GetLimb($_SESSION['company_id'], $start_id);
    }

    public function SortPeople($list, $sort, $reverse = false) {
		return ReportingTreeLookup::SortPeople($_SESSION['company_id'], $list, $sort, $reverse);
    }
    
    public function GetDescendants($start_record, $sort = null) {
    	return ReportingTreeLookup::GetDescendants($_SESSION['company_id'], $start_record, $sort);
    }        

    public function IsManager($person_id) {
    	return ReportingTreeLookup::IsManager($_SESSION['company_id'], $person_id);
    }

    public function IsSeniorMgr($start_id) {
    	return ReportingTreeLookup::IsSeniorMgr($_SESSION['company_id'], $start_id);
    }

    public function IsDirectMgr($start_id) {
    	return ReportingTreeLookup::IsDirectMgr($_SESSION['company_id'], $start_id);
    }

    public function IsViewable($target) {
    	return ReportingTreeLookup::IsViewable($_SESSION['company_id'], $target);
    }
    
    public function GetCombined() {
    	return ReportingTreeLookup::GetCombined($_SESSION['company_id']);
    }

}
