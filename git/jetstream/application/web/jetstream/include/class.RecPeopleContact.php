<?php

require_once BASE_PATH . '/include/class.MpRecord.php';

class RecPeopleContact extends MpRecord
{

	public function Initialize() {
		$this->db_table = 'PeopleContact';
		$this->recordset->GetFieldDef('PeopleContactID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('ContactID')->required = TRUE;
		$this->recordset->GetFieldDef('PersonID')->required = TRUE;
		parent::Initialize();
	}
}
