<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/include/mpconstants.php';
require_once BASE_PATH . '/include/class.MapLookup.php';

/**
 * The DB fields initialization
 */
class RecAccount extends MpRecord
{

	public function Initialize() {
		$this->db_table = 'account';
		$this->recordset->GetFieldDef('AccountID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('CompanyID')->required = TRUE;

		parent::Initialize();
	}

	public function Save($simulate = false) {
		parent::Save($simulate);
		if (!$simulate) {
			
			$sql = "DELETE FROM DashboardSearch WHERE RecType = 1 and RecID = $this->AccountID";
			DbConnManager::GetDb('mpower')->Exec($sql);
			
			if (empty($this->Deleted)) {
				$active = ($this->Inactive == 0) ? 1 : 0;
				$an_field = MapLookup::GetAccountNameField($this->CompanyID, true);
				
				$search_text = $this->$an_field;
				
				$sql = "INSERT INTO DashboardSearch (CompanyID, RecType, RecID, SearchText, IsActive) 
					VALUES ($this->CompanyID, 1, $this->AccountID, ?, $active)";
				$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $search_text));
				DbConnManager::GetDb('mpower')->Exec($sql);
			}
		}
	}	

	public function GetName() {
		$layout = "{Text01}";
		// $layout = $_SESSION['AccountName'];
		$pattern = "|\{(\w*?)\}|"; // Find words encased in curly braces
		return preg_replace_callback($pattern, array('self', 'GetNameLookup'), $layout);
	}

	private function GetNameLookup($val) {
		return $this->{$val[1]}; // Go back and get the value of the word
	}
}

?>