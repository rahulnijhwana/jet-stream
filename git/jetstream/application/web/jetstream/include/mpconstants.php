<?php
/**
 * Contants for M-Power and Jetstream
 */
# Jetstream / M-Power Database information
# Test Server
//define ('MP_DATABASE', 'mpasac_test');
//define ('MP_DATABASE_WINDOWS', "50.19.85.234");

# Live Server
//define ('MP_DATABASE', 'mpasac');
//define ('MP_DATABASE_WINDOWS', "107.22.252.247");

/* 
 * So many define for BASE_PATH in each script
 * Monetize as one, besso
 */
//define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

// The three current environments
define('ENV_PRODUCTION', 'production');
define('ENV_STAGING', 'staging');
define('ENV_LOCAL', 'local');

// Current Environment (Set this to one of the above when changing environments)
//define('ENV', ENV_PRODUCTION); 
//define('ENV', ENV_STAGING); 
define('ENV', ENV_LOCAL);

/*
 * Environment Specific Details
 */
switch (ENV) {

	case ENV_PRODUCTION	:
		// M-Power Database informations 
		define ('MP_DATABASE_SERVER', 'ASA-VERIO');
		define ('MP_DATABASE', 'mpasac'); // live database is mpasac
		define ('MP_DATABASEU', 'mpasac' );
		define ('MP_DATABASEP', 'qraal+zv');
		define ('MP_DATABASES', 'ASA-VERIO');

		define ('MOBILE_LINK', 'https://www.mpasa.com/mobile/');

		break;

	case ENV_STAGING	:
		// M-Power Database informations
		define ('MP_DATABASE_SERVER', '50.19.85.234');
		define ('MP_DATABASE', 'mpasac'); // live database is mpasac
		define ('MP_DATABASEU', 'mpasac' );
		define ('MP_DATABASEP', 'qraal+zv');
		define ('MP_DATABASES', 'ASA-VERIO-STAGE');

		define ('MOBILE_LINK', 'https://www.mpasatest.com/mobile/');

		break;

	case ENV_LOCAL :
		// M-Power Database informations
		define ('MP_DATABASE_SERVER', '192.168.55.131');
		define ('MP_DATABASE', 'mpasac_test'); // live database is mpasac
		define ('MP_DATABASEU', 'sa' );
		define ('MP_DATABASEP', 'tech');
		define ('MP_DATABASES', '192.168.55.131');

		define ('MOBILE_LINK', 'http://localhost:8080/mobile/');

		break;
}

// M-Power Phases: To make the code more decipherable
define('MP_TARGET', 10);
define('MP_FM', 1);
define('MP_IP', 2);
define('MP_IPS', 3);
define('MP_DP', 4);
define('MP_DPS', 5);
define('MP_CLOSED', 6);
define('MP_REMOVED', 9);

# Milestone Types
define('MSTYPE_UNKNOWN', 0);
define('MSTYPE_YESNO', 1);
define('MSTYPE_ALPHANUM', 2);
define('MSTYPE_CHECKBOX', 3);
define('MSTYPE_CALENDAR', 4);
define('MSTYPE_DROPDOWN', 5);

# SQL Data Types
define('DTYPE_MIXED', 0);
define('DTYPE_CHAR', 1);
define('DTYPE_TIME', 2);
define('DTYPE_STRING', 3);
define('DTYPE_BOOL', 4);
define('DTYPE_FLOAT', 5);
define('DTYPE_INT', 6);
define('DTYPE_RECORDSET', 7);
define('DTYPE_SUBQUERY', 8);

# SPECIAL TYPE OF NOTE
define('NOTETYPE_INCOMING_CALL', 1);
define('NOTETYPE_OUTGOING_CALL', 2);
define('NOTETYPE_EMAIL', 3);
define('NOTETYPE_INCOMING_EMAIL', 4);
define('NOTETYPE_OUTGOING_EMAIL', 5);

define('NOTETYPE_INCOMING_EMAIL_TEXT', 'receiver');
define('NOTETYPE_OUTGOING_EMAIL_TEXT', 'sender');

# TYPE OF NOTE
define('NOTETYPE_COMPANY', 1);
define('NOTETYPE_CONTACT', 2);
define('NOTETYPE_EVENT', 3);
define('NOTETYPE_OPPORTUNITY', 4);
define('NOTETYPE_TASK', 5);

# Prefix of Type of Note, used in Note type filtration
define('NOTE_TYPE', 'NT');
define('NOTE_SPECIAL_TYPE', 'NST');

define('REPEAT_NONE', 0);
define('REPEAT_DAILY', 1);
define('REPEAT_WEEKLY', 2);
define('REPEAT_MONTHLY', 3);
define('REPEAT_MONTHLY_POS', 4);
define('REPEAT_YEARLY', 5);
define('REPEAT_YEARLY_POS', 6);

# Define opportunity event color
define('OPPORTUNITY_EVENT_COLOR', 'FFFFFF');

# Define sub menus of company and contact pages
define('LOG_INCOMING_CALL', 'Log Incoming Call');
define('LOG_OUTGOING_CALL', 'Log Outgoing Call');
define('MANAGE_ACC', 'Manage Account');
define('MANAGE_CON', 'Manage Contact');

# TYPE OF RECTYPE IN DASHBOARDSEARCH IN JETSTREAM
define('JS_COMPANY', 1);
define('JS_CONTACT', 2);

# Setting to run the php file in backend.
define('RESTORE_SEARCH_FILE', '/mindfire/v3/cron/RestoreSearchDataNew.php');
define('PHP_LOG_PATH', '/mindfire/v3/cron/cron_log.txt');
//define('RESTORE_SEARCH_FILE', 'D:/Projects/slip/cron/restoreSearchData.php');
//define('PHP_LOG_PATH',     		'D:/Projects/slip/cron/cron_log.txt');
define('PHP_PATH', 'php');

# TYPE OF ACCESS used for accounts and contacts.
# If the access is 0, then everyone can access it.
# If the access is 1, then unassigned users can access to view the limited fields.
# If the access is 2, then unassigned users can not access the accounts and contacts.
define('ACCESS_DEFAULT', 0);
define('ACCESS_UNASSIGNED_TRUE', 1);
define('ACCESS_UNASSIGNED_FALSE', 2);

# Ensures that the web path truncates the /legacy portion or the /ajax portion
$web_path = str_replace('\\', '/', dirname($_SERVER['SCRIPT_NAME']));
$web_path = array_shift(explode('/ajax', $web_path));
$web_path = array_shift(explode('/legacy', $web_path));
define('WEB_PATH', $web_path);

# Cache Path
$cache_path = dirname(__DIR__).'/newcache' . dirname($_SERVER['PHP_SELF']);

// TODO Move this to the login tasks so we don't check the path every load
if (!file_exists($cache_path)) {
	mkdir($cache_path, 0755, TRUE);
}

// TODO Remove ajax, legacy, etc subdirs or cache may be duplicated
define('CACHE_PATH', realpath($cache_path));

# To be used in some cases as an alternative to TRUE and FALSE
define('UNKNOWN', -1);
define('ACCOUNT', 1);
define('CONTACT', 2);
define('BOTH', 3);

define('MSSQL_DATE', 'Y-m-d\TH:i:s');

define('ALERT_TYPE_BASIC', 1);
define('RECORD_TYPE_NOTE', 1);
define('UTC_OFFSET', 21600);
