<?php
require_once 'lib.date.php';

define("MPF_TYPEMISMATCH", 1);
define("MPF_UNDEFINEDTYPE", 2);

class MpFieldDef
{
	//Metadata
	private $type = DTYPE_MIXED; // Type of data, see the DTYPE defines
	private $transform; // Name of a function to apply to the value when it is set
	

	private $required;
	private $auto_key;
	private $key;
	private $change_required;
	
	private $link;

	public function __set($variable, $value) {
		switch ($variable) {
			case 'link' :
				$this->link = $value;
				break;
			case 'type' :
				$this->type = $value;
				break;
			case 'auto_key' :
				if (!is_bool($value)) {
					throw new Exception('auto_key only accepts a boolean value.');
				}
				$this->auto_key = $value;
				break;
			case 'key' :
				if (!is_bool($value)) {
					throw new Exception('key only accepts a boolean value.');
				}
				$this->key = $value;
				break;
			case 'required' :
				if (!is_bool($value)) {
					throw new Exception('required only accepts a boolean value.');
				}
				$this->required = $value;
				break;
			case 'change_required' :
				if (!is_bool($value)) {
					throw new Exception('change_required only accepts a boolean value.');
				}
				$this->change_required = $value;
				break;
			case 'transform' :
				if (!function_exists($value)) {
					throw new Exception('Field transform defined that does not exist: ' . $value);
				}
				
				$this->transform = $value;
				break;
			default :
				debug_print_backtrace();
				throw new Exception('Illegal variable assignment: ' . $variable);
		}
	}

	public function __get($variable) {
		return $this->{$variable};
	}

	public function __toString() {
		return 'MpFieldDef';
	}

	public function Validate($value) {
		if ($this->transform) {
			$value = call_user_func($this->transform, $value);
		}
		
		if (is_string($value) && strlen($value) == 0) {
			$value = null;
		}
		
		if (!is_null($value)) {
			switch ($this->type) {
				case DTYPE_MIXED :
					break;
				case DTYPE_TIME :
					if ($value instanceof JetDateTime) {
						$value = $value->asMsSql();
					} else {
						$value = TimestampToMsSql(strtotime($value));
					}
					break;
				case DTYPE_SUBQUERY :
				case DTYPE_CHAR :
				case DTYPE_STRING :
					if (!is_string($value)) {
						$error = MPF_TYPEMISMATCH;
					}
					break;
				case DTYPE_BOOL :
					if ($value === '0' || $value === '1') {
						$value = (int) $value;
					}
					if (!is_bool($value) && $value !== 0 && $value !== 1) {
						$error = MPF_TYPEMISMATCH;
					}
					break;
				case DTYPE_FLOAT :
					if (!is_numeric($value)) {
						$error = MPF_TYPEMISMATCH;
					}
					break;
				case DTYPE_INT :
					if (!is_null($value) && is_string($value)) {
						$value = str_replace(array('$', ','), '', $value);
						if (substr($value, -3, 3) === '.00') {
							$value = substr($value, 0, -3);
						}
					}
					if (!is_int($value) && ((string) (int) $value !== (string) $value)) {
						$error = MPF_TYPEMISMATCH;
					}
					break;
				case DTYPE_RECORDSET :
					if (!$value instanceof MpRecordSet) {
						$error = MPF_TYPEMISMATCH;
					}
					break;
				default :
					$error = MPF_UNDEFINEDTYPE;
			}
		}
		if (isset($error)) {
			switch ($error) {
				case MPF_TYPEMISMATCH :
					if (!is_null($value)) {
						echo 'Expected type: ' . $this->type . '<br>';
						echo 'Value received: ' . $value . '<br>';
						var_dump($value);
						//var_dump($this);
						echo '<br>';
						throw new Exception("Data does not match defined type.");
					}
					break;
				case MPF_UNDEFINEDTYPE :
					throw new Exception("Unknown data type defined.");
					break;
			}
		}
		return $value;
	}

	public function Debug() {
		print_r($this);
	}
}

?>
