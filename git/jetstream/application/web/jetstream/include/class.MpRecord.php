<?php

class MpRecord implements IteratorAggregate, ArrayAccess
{
	protected $data = array();
	protected $dirty = array();
	protected $recordset;
	protected $database;
	protected $delete;
	protected $db_row;
	
	public function Destroy() {
		unset($this->recordset);
		unset($this->database);
	}

	public function __set($var, $val) {
		if (is_null($this->recordset)) {
			debug_print_backtrace();
		}
		$val = $this->recordset->Validate($var, $val);
		
		$obj_val = $this->$var;
		
		if (!key_exists($var, $this->data) || $obj_val != $val) {
			$this->dirty[$var] = TRUE;
			$this->data[$var] = $val;
			
			// If the table has a ModifyTime field, update it to the current server date time
			$this->dirty['ModifyTime'] = TRUE;
			$this->data['ModifyTime'] = TimestampToMsSql(time(), 's');
		}
	}

	public function __get($variable) {
		if (array_key_exists($variable, $this->data)) {
			return $this->data[$variable];
		}
		if (is_array($this->db_row) && array_key_exists($variable, $this->db_row)) {
			$this->dirty[$variable] = FALSE;
			$this->data[$variable] = $this->db_row[$variable];
			return $this->data[$variable];
		}
		//return;
	// echo "Unknown variable $variable...<br>";
	}

	public function SetDelete() {
		$this->delete = TRUE;
		return $this;
	}

	public function SetDbRow(array $row) {
		$this->db_row = $row;
		return $this;
	}

	public function ClearDelete() {
		$this->delete = FALSE;
		return $this;
	}

	public function GetField($variable) {
		return array('data' => $this->data[$variable], 'fielddef' => $this->recordset->GetFieldDef[$variable]);
	}

	public function GetDirtyData() {
		$output = array();
		foreach ($this->recordset->GetFieldDef() as $field_name => $field_def) {
			if ($this->IsDirty($field_name)) {
				$output[$field_name] = $this->data[$field_name];
			}
		}
		return $output;
	}
	
	public function __toString() {
		return 'MpRecord';
	}

	public function ClearDirty() {
		$this->dirty = array();
		return $this;
	}

	public function SetAllDirty() {
		foreach($this->recordset->GetFieldDef() as $def_name => $def) {
			if ($this->InRecord($def_name)) {
				// Ensures that the variable is pulled from the db vals
				$this->__get($def_name);
				$this->dirty[$def_name] = true;
			}
		}
	}
	
	public function IsDirty($field_name) {
		if (array_key_exists($field_name, $this->dirty) && $this->dirty[$field_name] == TRUE) {
			return true;
		} else {
			return false;
		}
	}

	public function InRecord($field_name, $include_empty = true) {
		if (!$include_empty) {
			return !empty($this->data[$field_name]) || !empty($this->db_row[$field_name]);
		}
		return isset($this->data[$field_name]) || isset($this->db_row[$field_name]);
		return (array_key_exists($field_name, $this->data)) ? true : false;
	}

	public function Debug($offset = '') {
		foreach ($this->data as $key => $value) {
			$dirty = (key_exists($key, $this->dirty) && $this->dirty[$key]) ? "(D) " : "    ";
			echo "{$offset}  {$dirty}[{$key}] => {$value}\n";
			if ($value instanceof MpRecordset) {
				$value->Debug($offset . '    ');
			}
		}
		if (is_array($this->db_row)) {
			print_r($this->db_row);
		}
	}

	public function GetDataset() {
		return $this->data;
	}

	public function GetFields() {
		return array_unique(array_merge(array_keys($this->data), array_keys($this->db_row)));
	
	}

	public function GetDbRow() {
		return $this->db_row;
	}

	public function GetDatabase() {
		return $this->database;
	}

	public function GetRecordset() {
		return $this->recordset;
	}

	public function SetDatabase(DbConn $database) {
		$this->database = $database;
	}

	public function SetRecordset(MpRecordset $recordset) {
		$this->recordset = $recordset;
	}

	public function Initialize() {
		foreach ($this->data as $field) {
			if ($field instanceof MpRecordset) {
				$field->Initialize();
			}
		}
		return $this;
	}

	/**
	 * Merges the current record with another MpRecord or with an array that has matching
	 * field names
	 *
	 * @param unknown_type $record
	 * @return MpRecord
	 */
	public function Merge($record) {
		foreach ($record as $field_name => $value) {
			if (key_exists($field_name, $this->data) && $this->data[$field_name] instanceof MpRecordset) {
				$this->data[$field_name]->Merge($value);
			} else {
				$this->__set($field_name, $value);
			}
		}
		return $this;
	}

	public function Save($simulate = false) {
		if (!$this->InRecord('db_table')) {
			$this->Debug();
			throw new Exception('Save failed because the database table was not defined.  Forgot to initialize record?');
		}
		$insert = false;
		$key_error = false;
		$fields = array();
		$key_fields = array();
		$insert_fields = array();
		
		foreach ($this->recordset->GetFieldDef() as $field_name => $field_def) {
			$field = $this->$field_name;
			
			// if (key_exists($field_name, $this->data)) {
			//	$field = $this->data[$field_name];
			// }
			

			// $field = (key_exists($field_name, $this->data)) ? $this->data[$field_name] : NULL;
			

			if ($field instanceof MpRecordSet) {
				$field->Save();
				continue;
			}
			
			$dirty = $this->IsDirty($field_name);
			
			if (($field_def->required && is_null($field)) || ($field_def->change_required && !$dirty)) {
				return $this;
			}
			
			if ($dirty && $field_def->type != DTYPE_MIXED) {
				$fields[$field_name] = array($field_def->type, $field);
			}
			
			if ($field_def->required && !$dirty) {
				$insert_fields[$field_name] = array($field_def->type, $field);
			}
			
			if ($field_def->key || $field_def->auto_key) {
				$key_fields[$field_name] = array($field_def->type, $field);
				if ($field_def->auto_key && $dirty) {
					
					$this->Debug();
					throw new Exception("Attempt to save a changed autonumber key field - $field_name");
				}
				if ($field_def->key && is_null($field)) {
					$key_error = TRUE;
				}
				if (is_null($field) || $dirty) {
					$insert = TRUE;
				}
			}
		}
		
		// Moved this out of the validation loop as it may not be an issue in cases that would
		// break out naturally, such as Milestones that wouldn't save anyway
		if ($key_error) {
			$this->Debug();
			throw new Exception('Attempt to save a record without all of the key fields populated.');
		}
		
		if (count($fields) > 0 || $this->delete) {
			if (count($key_fields) == 0) {
				throw new Exception('Cannot save a record without a key field.');
			}
			if ($this->delete) {
				$sql = 'DELETE FROM ' . $this->db_table . ' WHERE ' . implode(' AND ', AppendEach(array_keys($key_fields), " = ?"));
				$fields = $key_fields;
			} elseif ($insert) {
				$fields = array_merge($fields, $insert_fields);
				$sql = 'INSERT INTO ' . $this->db_table . ' (' . implode(', ', array_keys($fields)) . ') VALUES (' . implode(', ', array_fill(0, count($fields), '?')) . ')';
			} else {
				$sql = 'UPDATE ' . $this->db_table . ' SET ';
				$sql .= implode(', ', AppendEach(array_keys($fields), " = ?")) . ' WHERE ' . implode(' AND ', AppendEach(array_keys($key_fields), " = ?"));
				$fields = array_merge($fields, $key_fields);
			}
	
			$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($fields);

			if ($simulate) {
			
			//echo $sql . '<br>';
				
			$myFile = "testFile.txt";
			$fh = fopen($myFile, 'w') or die("can't open file");
			$stringData =json_encode($sql);
			fwrite($fh, $stringData);
			fclose($fh);
			} else {
				$this->database->Execute($sql);
				
				if ($insert) {
					$identity_record = $this->database->Execute("select IdentityInsert = @@identity");
					$identity = $identity_record[0]->IdentityInsert;
					foreach ($this->recordset->GetFieldDef() as $field_name => $field_def) {
						$field = (key_exists($field_name, $this->data)) ? $this->data[$field_name] : NULL;
						
						if ($field_def->auto_key) {
							$this->data[$field_name] = $identity;
						} elseif ($field instanceof MpRecordSet && $field_def->link) {
							$field->SetAll($field_def->link, $identity);
							$field->Save();
						}
					}
				}
				$this->ClearDirty()->ClearDelete();
			}
		}
		return $this;
	}

	// Required for IteratorAggregate
	public function getIterator() {
		return new ArrayIterator($this->data);
	}

	// *** Required ArrayAccess Functions
	

	public function offsetExists($offset) {
		return (isset($this->data[$offset])) ? TRUE : FALSE;
	}

	public function offsetGet($offset) {
		//return ($this->offsetExists($offset)) ? $this->data[$offset] : FALSE;
		return $this->__get($offset);
	}

	public function offsetSet($offset, $value) {
		$this->__set($offset, $value);
	}

	public function offsetUnset($offset) {
		unset($this->data[$offset]);
		unset($this->db_row[$offset]);
		unset($this->dirty[$offset]);
	}
	
// End required ArrayAccess functions ***
}

?>
