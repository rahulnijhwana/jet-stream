<?php
/**
 * @package database
 */
require_once 'class.MpRecordset.php';
require_once 'class.MpRecord.php';
// require_once BASE_PATH . '/include/class.Opp.php';
// require_once BASE_PATH . '/include/class.SubAnswer.php';
// require_once BASE_PATH . '/include/class.ProductXRef.php';
// require_once BASE_PATH . '/include/class.Product.php';

/**
 * Base Database connection Class also inlcudes the MS Sql connection
 * @package database
 * @subpackage connector
 */
abstract class DbConn
{
    /**
     * The database connection object
     * @var object
     */
    private $database;

    abstract public function DoConnect();

    abstract public function Execute($sql, $record_type = 'MpRec', $index = NULL);

//    public function Execute($sql, $record_type = 'MpRec') {
//        $this->Reset();
//        if (!is_object($this->database)) {
//            throw new Exception("Database object not set.");
//        }
//        $args = ArrayFlatten(func_get_args());
//        $record_object = array_shift($args);
//
//        return $this->DoExec();
//    }
}

?>
