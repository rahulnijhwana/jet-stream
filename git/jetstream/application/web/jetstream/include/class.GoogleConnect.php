<?php
require_once 'constants.php';
require_once JETSTREAM_ROOT . '/../libs/GoogleApi/src/Google_Client.php';
require_once JETSTREAM_ROOT . '/../libs/GoogleApi/src/io/Google_HttpRequest.php';
require_once JETSTREAM_ROOT . '/../libs/GoogleApi/src/contrib/Google_CalendarService.php';
require_once JETSTREAM_ROOT . '/../libs/GoogleApi/src/contrib/Google_MirrorService.php';
require_once JETSTREAM_ROOT . '/../libs/GoogleApi/src/contrib/Google_TasksService.php';
require_once JETSTREAM_ROOT . '/../libs/GoogleApi/src/contrib/Google_Oauth2Service.php';
require_once JETSTREAM_ROOT . '/../libs/GoogleApi/src/contrib/Google_PlusService.php';
//require_once JETSTREAM_ROOT . '/../include/class.SessionManager.php';
require_once JETSTREAM_ROOT . '/../include/class.DbConnManager.php';
require_once JETSTREAM_ROOT . '/../include/class.SqlBuilder.php';
require_once JETSTREAM_ROOT . '/../include/class.JetDateTime.php';

// Get all reporting tree
require_once JETSTREAM_ROOT . '/../slipstream/class.ModuleCalendar.php';
require_once JETSTREAM_ROOT . '/../include/class.ReportingTreeLookup.php';


class GoogleConnect {

    public $isUseGoogleSync;

    public $client;
    public $token;
    private $debug;

    // Services
    public $calendar;
    protected $request;
    public $contacts;
    public $tasks;
    public $oauth2;

    protected $plus;

    public $tasklists;

    /* TODO
     * It's controlled in Admin for each Account
     */
    protected $useGoogle = true;
    public $connected = false;
    protected $taskProcessed;
    protected $eventProcessed;
    protected $contactProcessed;

    /* 
     * for lessen google traffic, we store TaskListID in DB and reuse that
     * Let's rebuild it whenever new token issued by User
     */
    protected $listTaskListID;
    protected $calendarID;
    protected $contactGroupID;

    // TODO trial to use mirror service for contacts
    public $mirrorContacts;

    protected $user_timezone = 'America/Chicago';

    private $user_id;
    private $company_id;
    private $permitted_users = array();

    /* REVIEW
     * Separate each service initialization but GOOGLE_CLIENT
     */
    public function __construct($user, $sync = true, $service = '') {
		

        //$this->debug = true;

        //SessionManager::Init();
        //SessionManager::Validate();

        $this->client = new Google_Client();
        
        $this->client->setApplicationName(GOOGLE_CLIENT_NAME);

        $this->client->setClientId(GOOGLE_CLIENT_ID);
        $this->client->setClientSecret(GOOGLE_CLIENT_SECRET);
        $this->client->setRedirectUri(GOOGLE_CALLBACK_URL);
        $this->client->setScopes(array(
            //'https://apps-apis.google.com/a/feeds/groups/',
            //'https://apps-apis.google.com/a/feeds/alias/',
            //'https://apps-apis.google.com/a/feeds/user/',
            'https://www.google.com/m8/feeds/',
            'https://www.google.com/m8/feeds/user/',
            'https://www.google.com/m8/feeds/contacts',
            'https://www.google.com/m8/feeds/contacts/default/full',
            'https://www.google.com/m8/feeds/contacts/default/full/batch',
            'https://www.googleapis.com/auth/calendar',
            'https://www.googleapis.com/auth/tasks',
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/userinfo.email',
            //'https://www.googleapis.com/auth/plus.login',
            'https://mail.google.com/'
        ));

        // callback need the scope of API service
        $this->calendar = new Google_CalendarService($this->client);

        $this->contacts = $this->client;

        // TODO trial to use mirror service for contact
        $mirrorService = new Google_MirrorService($this->client);
        $this->mirrorContacts = $mirrorService->contacts;

        //echo '<pre>'; print_r($this->mirrorContacts); echo '</pre>'; exit;


        $taskService = new Google_TasksService($this->client);
        $this->tasks = $taskService->tasks;
        $this->tasklists = $taskService->tasklists;

        $this->oauth2 = new Google_Oauth2Service($this->client);
        $this->plus = new Google_PlusService($this->client);

        $this->user_timezone = self::getUserTimezone($user);

        if (!empty($_SESSION['USER']['USERID'])) {
            $this->user_id = $_SESSION['USER']['USERID'];
        } else {
            $this->user_id = $user;
        }

        //echo 'UID : ' . $this->user_id; exit;

        if (!empty($_SESSION['company_id'])) {
            $this->company_id = $_SESSION['company_id'];
        } else {
            $this->company_id = self::getCompanyId($user);
        }

        if ($this->checkConnect($user)) {
            $this->connected = true;
            //if ( $sync == true ) {
                //if ( $service == 'contacts' ) {
                    //$this->syncContacts($user);
                //} else {
                    //$this->syncAll($user);
                //}
            //}
        } else {
			$this->connected = false;
			//if($this->connect() !='token-in'){
				//header('Location: '.$this->connect());
				//}
        }
        
        
    }

    /* 
     * call sync for all each services
     * change it as AJAX for lazy load 
     */
    private function syncAll($user) {
		
        require_once JETSTREAM_ROOT . '/../include/class.GoogleCalendar.php';
        require_once JETSTREAM_ROOT . '/../include/class.GoogleTasks.php';
        require_once JETSTREAM_ROOT . '/../include/class.GoogleEmail.php';
        require_once JETSTREAM_ROOT . '/../include/class.GoogleContacts.php';

        // For sync, we should loop all sales of reporting tree
        $sync = true;
        $this->googleCalendar= new GoogleCalendar($this->calendar, $user, $sync);
        $this->googleTasks = new GoogleTasks($this->tasklists, $this->tasks, $user, $sync);
        $this->googleEmail = new GoogleEmail($this->client, $this->oauth2, $user);

        // Sync Contacts changed as 'batch'
        $this->googleContacts = new GoogleContacts($this->contacts, $user, $sync);
    }
    public function syncEmail($user){
		
			require_once JETSTREAM_ROOT . '/../include/class.GoogleCalendar.php';
			require_once JETSTREAM_ROOT . '/../include/class.GoogleTasks.php';
			require_once JETSTREAM_ROOT . '/../include/class.GoogleEmail.php';
			require_once JETSTREAM_ROOT . '/../include/class.GoogleContacts.php';
			$this->googleCalendar= new GoogleCalendar($this->calendar, $user, $sync);
			$this->googleTasks = new GoogleTasks($this->tasklists, $this->tasks, $user, $sync);
			$this->googleEmail = new GoogleEmail($this->client, $this->oauth2, $user);
			$this->googleContacts = new GoogleContacts($this->contacts, $user, $sync);
			return true;
			//$this->host = 'imap.gmail.com';
			//$this->port = 993;
			//$this->accessToken = $this->client->getAccessToken();
			//$user = $this->oauth2->userinfo->get();
			//$this->userEmail = filter_var($user['email'], FILTER_SANITIZE_EMAIL);
			//$sql = 'Update GoogleToken set email = ? where PersonId = ?';
			//$params[] = array(DTYPE_STRING, $user['email']);
			//$params[] = array(DTYPE_INT, $userid);
			//$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
			//$status = DbConnManager::GetDb('mpower')->Exec($sql);
			//if (!$status) {
				//return false;
			//}
				//return true;
		}


    private function syncContacts($user) {

        require_once JETSTREAM_ROOT . '/../include/class.GoogleEmail.php';
        require_once JETSTREAM_ROOT . '/../include/class.GoogleContacts.php';

        // For sync, we should loop all sales of reporting tree
        $sync = true;
        $this->googleEmail = new GoogleEmail($this->client, $this->oauth2, $user);
        $this->googleContacts = new GoogleContacts($this->contacts, $user, $sync);

    }

    /* Deprecated - move to each service classes */
    protected function getProcessed($userid) {
        //error_log('call getProcessed' );
        $personID = $userid;

        $sql = 'Select TOP 1 task_processed, event_processed, contact_processed, task_list_id, calendar_id, contact_group_id from GoogleToken where personId = ?';
        
        $params[] = array(DTYPE_INT, $personID);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);     
        $aProcessed = DbConnManager::GetDb('mpower')->Exec($sql);
        if (count($aProcessed) > 0) {

            $today = date("Ymd");           
            $utc_timezone = new DateTimeZone('UTC');
                $jetDate = new JetDateTime($today, $utc_timezone);
            $today = $jetDate->asMsSql('g');
            $this->taskProcessed = ('' != $aProcessed[0]['task_processed']) ? self::convertGoogleDateFormat($aProcessed[0]['task_processed']) : $today ;
            $this->eventProcessed = ('' != $aProcessed[0]['event_processed']) ? self::convertGoogleDateFormat($aProcessed[0]['event_processed']) : $today ;
            $this->contactProcessed = ('' != $aProcessed[0]['contact_processed']) ? self::convertGoogleDateFormat($aProcessed[0]['contact_processed']) : $today ;
            $this->listTaskListID = ('' != $aProcessed[0]['task_list_id']) ? $aProcessed[0]['task_list_id'] : '' ;
            $this->calendarID = ('' != $aProcessed[0]['calendar_id']) ? $aProcessed[0]['calendar_id'] : '' ;
            $this->contactGroupID = ('' != $aProcessed[0]['contact_group_id']) ? $aProcessed[0]['contact_group_id'] : '' ;
        }

        /* Run in GoogleContacts 
        if ( $this->contactGroupID == '' ) {
            $this->contactGroupID = $this->getContactGroup();
        }
        */
        
        return true;
    }

    public static function convertGoogleDateFormat($date, $type='g') {
        $date = str_replace(':000','',$date);
        $date = str_replace('.000','',$date);
        /*
        if (strstr($date,'000PM')) {
            $date = substr($date, 0, -6 );
            $oDate = new DateTime($date);
            $date = $oDate->format('Y-m-d H:i:s');
        }
        */

        $utc_timezone = new DateTimeZone('UTC');
        $jetDate = new JetDateTime($date, $utc_timezone);
        $googleDate = $jetDate->asMsSql($type);
        return $googleDate;
    }

    public static function convertJetstreamDateFormat($date, $userid) {

        $date = str_replace(':000','',$date);
        $date = str_replace('.000','',$date);

        /*
        $date = substr($date, 0, -6 );
        $oDate = new DateTime($date);
        $date = $oDate->format('Y-m-d H:i:s');
        */

        $userTimezone = self::getUserTimezone($userid);
        $timezone = new DateTimeZone($userTimezone);
        $date = new DateTime($date);
        $date->setTimezone($timezone);
        $format = JETSTREAM_DATE_FORMAT . ' ' . JETSTREAM_TIME_FORMAT;
        return $date->format($format);
    }

    /* 
     * it's used to check when user sync google so we need to sync from sotred date to +14 days
     * TODO 
     * not using yet
     */
    public function setProcessed($today = '', $service='calendar') {
        if ('' == $today) {
            $today = date("Ymd");           
            $utc_timezone = new DateTimeZone('UTC');
                $jetDate = new JetDateTime($today, $utc_timezone);
            $today = $jetDate->asMsSql('g');
        }
    
        //error_log('SetProcessed as : ' . $today);
        /***
        $this->taskProcessed = $today;
        $this->eventProcessed = $today;
        $this->contactProcessed = $today;
        ***/

        if (!empty($_SESSION['USER']['USERID'])) {
            $personID = $_SESSION['USER']['USERID'];
        } else {
            return false;
        } 

        // save the record
        $sql = 'Update GoogleToken 
               Set task_processed = ?,
                   event_processed = ?,
                   contact_processed = ?
             Where PersonID = ?';
        
        $params[] = array(DTYPE_STRING, $today);
        $params[] = array(DTYPE_STRING, $today);
        $params[] = array(DTYPE_STRING, $today);
        $params[] = array(DTYPE_INT, $personID);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);     
        $status = DbConnManager::GetDb('mpower')->Exec($sql);
        
        if (!$status) {
            return false;
        }
    }

    protected function setTaskListID($listTaskListID) {
        //error_log('Store TaskListID after Google : ' . $listTaskListID);
        if (!empty($_SESSION['USER']['USERID'])) {
            $personID = $_SESSION['USER']['USERID'];
        } else {
            return false;
        } 

        // save the record
        $sql = 'Update GoogleToken 
               Set task_list_id = ?
             Where PersonID = ?';
        
        $params[] = array(DTYPE_STRING, $listTaskListID);
        $params[] = array(DTYPE_INT, $personID);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);     
        $status = DbConnManager::GetDb('mpower')->Exec($sql);
        
        if (!$status) {
            return false;
        }
    }

    protected function setContactGroupID($contactGroupID) {
        if (!empty($_SESSION['USER']['USERID'])) {
            $personID = $_SESSION['USER']['USERID'];
        } else {
            return false;
        } 

        // save the record
        $sql = 'Update GoogleToken 
               Set contact_group_id = ?
             Where PersonID = ?';
        
        $params[] = array(DTYPE_STRING, $contactGroupID);
        $params[] = array(DTYPE_INT, $personID);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);     
        $status = DbConnManager::GetDb('mpower')->Exec($sql);
        
        if (!$status) {
            return false;
        }
    }

public function checkConnect($user) {
        /* check No DB record */

        $googleToken = $this->getToken($user);
        $this->token = $googleToken;
        if (!$googleToken) {
            return false;
        }
        
        if ($this->client->getAccessToken()) {
            if ($this->debug) {
                error_log('Google is connected');
            }
            return true;
        } else {

				if (count($googleToken) > 0) {
										/* JET-37 speed improvement attempt  */
							if(isset($_SESSION['USER']['google']['time_created']))
							{
							$t=time();
							$_SESSION['USER']['google']['time_created'] = json_decode($_SESSION['USER']['google']['token'])->created;
							$timediff=$t-$_SESSION['USER']['google']['time_created'];
							if($timediff>3600)
								{
									$refresh_token = $googleToken['refresh_token'];
									$this->client->refreshToken($refresh_token);
									$_SESSION['USER']['google']['token'] = $this->client->getAccessToken();
									$_SESSION['USER']['google']['time_created'] = json_decode($_SESSION['USER']['google']['token'])->created;
									return true;
								}else{
									$this->client->setAccessToken($_SESSION['USER']['google']['token']);
									$_SESSION['USER']['google']['token'] = $this->client->getAccessToken();
									return true;
									}
							}
			/* JET-37 */
			else{
					$refresh_token = $googleToken['refresh_token'];
					$this->client->refreshToken($refresh_token);
					
					$_SESSION['USER']['google']['token'] = $this->client->getAccessToken();
					
					$_SESSION['USER']['google']['time_created'] = json_decode($_SESSION['USER']['google']['token'])->created;

					
					return true;
				}
            } else {
                if ($this->debug) {
                    error_log('Google connect called in wrong place');
                }
                return false;
            }
                
        }
    }
/*        public function checkConnect($user) {

        $googleToken = $this->getToken($user);
        $this->token = $googleToken;
        if (!$googleToken) {
            return false;
        }
        
        if ($this->client->getAccessToken()) {
            if ($this->debug) {
                error_log('Google is connected');
            }
            return true;
        } else {
            if (count($googleToken) > 0) {
                $refresh_token = $googleToken['refresh_token'];
                $this->client->refreshToken($refresh_token);
                $_SESSION['USER']['google']['token'] = $this->client->getAccessToken();
                return true;
            } else {
                if ($this->debug) {
                    error_log('Google connect called in wrong place');
                }
                return false;
            }
                
        }
    }*/

    public function getToken($personID) {

        $sql = 'select TOP 1 * from GoogleToken
             where personId = ?';
        
        $params[] = array(DTYPE_INT, $personID);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);     
        $aToken = DbConnManager::GetDb('mpower')->Exec($sql);

        if (count($aToken) > 0) {
            $token = $aToken[0];
        } else {
            $token = '';
        }

        return $token;
    }

    /* storeToken is called when user 'Google Connect'
     * so getToekn arg1 should be Session User
     */
    private function storeToken() {
        
        $token = $this->getToken($this->user_id);
        if ($token) return false;

        if (!empty($_SESSION['USER']['google']['token'])) {
            $jsonToken = $_SESSION['USER']['google']['token'];
            $aToken = json_decode($jsonToken);
        } else {
            return false;
        }
        
        if (!empty($_SESSION['USER']['USERID'])) {
            $personID = $_SESSION['USER']['USERID'];
        } else {
            return false;
        } 

        // save the record
        $sql = 'Insert into GoogleToken (PersonID, access_token, token_type, expires_in, refresh_token, created, raw_token) Values (?, ?, ?, ?, ?, ?, ?)';
        
        $params[] = array(DTYPE_INT, $personID);
        $params[] = array(DTYPE_STRING, $aToken->access_token);
        $params[] = array(DTYPE_STRING, $aToken->token_type);
        $params[] = array(DTYPE_INT, $aToken->expires_in);
        $params[] = array(DTYPE_STRING, $aToken->refresh_token);
        $params[] = array(DTYPE_INT, $aToken->created);
        $params[] = array(DTYPE_STRING, $jsonToken);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);     
        $status = DbConnManager::GetDb('mpower')->Exec($sql);
        
        if (!$status) {
            return false;
        }
        return true; 
    }   

    public function connect() {
		
        if (isset($_GET['logout'])) {
            unset($_SESSION['USER']['google']['token']);
            return 'token-out';
        }
        if (isset($_GET['code'])) {
            $this->client->authenticate($_GET['code']);
            $_SESSION['USER']['google']['token'] = $this->client->getAccessToken();
            header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
        }

        if (isset($_SESSION['USER']['google']['token'])) {
            if ($this->debug) {
                error_log("Set Token in Google library\n");
                error_log(print_r($_SESSION['USER']['google']['token'],true)."\n\n");
            }
            $this->client->setAccessToken($_SESSION['USER']['google']['token']);
        } 

        if ( $this->client->getAccessToken() ) {
            //error_log('Connected : ' . $_SESSION['USER']['google']['token']);
            /* Oauth run 1) get all token including refresh_token and then 2) get access_token only so we should check last one to run sync contact just once */
            $jsonToken = $_SESSION['USER']['google']['token'];
            $aToken = json_decode($jsonToken);
            if ( !isset($aToken->refresh_token) ) {
                //error_log('call syncContactsAsBackground in GoogleConnect');
                $this->syncContactsAsBackground();
            }
            $this->storeToken();
            return 'token-in';
        } else {
            $authUrl = $this->client->createAuthUrl();
            return $authUrl;
        }
    }
    //public function connectnew($code = false) {
		//print_r($_POST);exit;
        ////if (isset($_GET['logout'])) {
            ////unset($_SESSION['USER']['google']['token']);
            ////return 'token-out';
        ////}
        //if (isset($code)) {
            //$this->client->authenticate($_GET['code']);
            //$_SESSION['USER']['google']['token'] = $this->client->getAccessToken();
            //header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
        //}

        //if (isset($_SESSION['USER']['google']['token'])) {
            //if ($this->debug) {
                //error_log("Set Token in Google library\n");
                //error_log(print_r($_SESSION['USER']['google']['token'],true)."\n\n");
            //}
            //$this->client->setAccessToken($_SESSION['USER']['google']['token']);
        //} 

        //if ( $this->client->getAccessToken() ) {
            ////error_log('Connected : ' . $_SESSION['USER']['google']['token']);
            ///* Oauth run 1) get all token including refresh_token and then 2) get access_token only so we should check last one to run sync contact just once */
            //$jsonToken = $_SESSION['USER']['google']['token'];
            //$aToken = json_decode($jsonToken);
            //if ( !isset($aToken->refresh_token) ) {
                ////error_log('call syncContactsAsBackground in GoogleConnect');
                //$this->syncContactsAsBackground();
            //}
            //$this->storeToken();
            //return 'token-in';
        //} else {
            //$authUrl = $this->client->createAuthUrl();
            //return $authUrl;
        //}
    //}
    
    /*Start JET-37*/
    public function getauthurl()
    {
		return $this->client->createAuthUrl();
	}
    /*End JET-37*/

    /* Run Sync Contacts as background process */
    private function syncContactsAsBackground() {
        //error_log('3Legs call the contact sync');
        if (!empty($_SESSION['USER']['USERID'])) {
            $userid = $_SESSION['USER']['USERID'];
        }
        if (!empty($_SESSION['USER']['COMPANYID'])) {
            $companyid = $_SESSION['USER']['COMPANYID'];
        }
        $googleScriptPath = realpath(dirname(__FILE__));
        $url = 'https://www.mpasatest.com/jetstream/ajax/ajax.googleContactBatchPush.php?userid=' . $userid . '&companyid=' . $companyid;
        error_log('call syncContactsAsBackground in GoogleConnect : ' . $url);
        $command = "/usr/bin/curl --insecure --request GET '$url'";

        shell_exec(sprintf('%s > /dev/null 2>&1 &', $command));

        return true;
    }

    public static function isUseGoogleSync($companyid) {

        $sql = "SELECT UseGoogleSync FROM Company WHERE CompanyID = ? ";
        $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyid));
        $company = DbConnManager::GetDb('mpower')->Exec($sql);

        $UseGoogleSync = (!empty($company) && count($company) > 0) ? $company[0]['UseGoogleSync'] : 0;
        ($UseGoogleSync != null) ? $UseGoogleSync : 0;
        return $UseGoogleSync;

    }

    public static function hasToken($personID) {
        $sql = 'select TOP 1 * from GoogleToken where personId = ?';

        $params[] = array(DTYPE_INT, $personID);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aToken = DbConnManager::GetDb('mpower')->Exec($sql);

        if (count($aToken) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function getCompanyId($user) {
        $sql = 'Select TOP 1 companyId from people where personId = ?';
        $params[] = array(DTYPE_INT, $user);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aCompany = DbConnManager::GetDb('mpower')->Exec($sql);
        return $aCompany[0]['companyId'];
    }

    public static function getTodayWithGoogleFormat( $format = "YmdHis" ) {
        $today = date($format);
        //echo '<pre>'; print_r($today); echo '</pre>';
        $utc_timezone = new DateTimeZone('UTC');
        $jetDate = new JetDateTime($today, $utc_timezone);
        $today = $jetDate->asMsSql('g');
        return $today;
    }

    public static function getTodayWithJetstreamFormat( $format = "YmdHis" ) {
        $today = date($format);
        //echo '<pre>'; print_r($today); echo '</pre>';
        $utc_timezone = new DateTimeZone('UTC');
        $jetDate = new JetDateTime($today, $utc_timezone);
        $today = $jetDate->asMsSql();
        return $today;
    }

    public static function getUserTimezone( $userid ) {
        $sql = 'Select tz.zname from people p 
                  join timezones tz on p.timezone = tz.zid 
                 where personId = ?';
        $params[] = array(DTYPE_INT, $userid);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aRow = DbConnManager::GetDb('mpower')->Exec($sql);
        return $aRow[0]['zname'];
    }

    /* Not used
    public static function getTimezoneCode( $user ) {
        $sql = 'Select p.timezone from people p 
                 where p.personId = ?';
        $params[] = array(DTYPE_INT, $user);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aRow = DbConnManager::GetDb('mpower')->Exec($sql);
        return $aRow[0]['timezone'];
    }
    */

    public static function getPermittedUsers($userid) {

        $sql = 'Select * from calendarPermission 
                 where personId = ?';
        $params[] = array(DTYPE_INT, $userid);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aRow = DbConnManager::GetDb('mpower')->Exec($sql);

        $aPermitted = array($userid);
        if ( count($aRow) > 0 ) {
            //echo '<pre>'; print_r($aRow); echo '</pre>'; exit;
            foreach ( $aRow as $row ) {
                $wPermission = $row['WritePermission'];
                $rPermission = $row['ReadPermission'];
                if ( $wPermission == 1 and $rPermission == 1 ) {
                    $aPermitted[] = $row['OwnerPersonID'];
                }
            }
        }

        return array_reverse($aPermitted);

    }

}
