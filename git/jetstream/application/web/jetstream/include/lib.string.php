<?php

/**
 * @package library
 */

/**
 * Trim and truncate a Salesforce ID to 15 chars
 * The last 3 characters are unncessary for looking up the info in
 * salesforce queries, and it appears they have some other meaning
 *
 * @param string $sf_id
 */
function SfFormatId($sf_id) {
	return substr(trim($sf_id), 0, 15);
}

function GetFirstNum($string) {
	preg_match('/\d+/', $string, $matches);
	return $matches[0];
}

function CurrencyFormat($amount, $precision = 2, $use_commas = true, $show_currency_symbol = false, $parentheses_for_negative_amounts = false) {
	/*
	**    An improvement to number_format.  Mainly to get rid of the annoying behaviour of negative zero amounts.
	*/
	$currencySymbol = '$ '; // Change this to use the organization's country's symbol in the future
	$amount = (float) $amount;
	// Get rid of negative zero
	$zero = round(0, $precision);
	if (round($amount, $precision) == $zero) $amount = $zero;
	
	if ($use_commas) {
		if ($parentheses_for_negative_amounts && ($amount < 0)) {
			$amount = '(' . number_format(abs($amount), $precision) . ')';
		} else {
			$amount = number_format($amount, $precision);
		}
	} else {
		if ($parentheses_for_negative_amounts && ($amount < 0)) {
			$amount = '(' . round(abs($amount), $precision) . ')';
		} else {
			$amount = round($amount, $precision);
		}
	}
	
	if ($show_currency_symbol) {
		$amount = $currencySymbol . $amount;
	}
	return $amount;
}

function SplitLongWords($string, $wrap_len = 10) {
	if ((empty($string)) || (strlen($string) < $wrap_len)) {
		return $string;
	}
	
	$words = explode(' ', $string);
	foreach ($words as &$word) {
		if (strlen($word) > $wrap_len) {
			$word = HtmlEntitySafeSplit($word, $wrap_len, '<wbr />');
		}
	}
	return implode(' ', $words);

}

function HtmlEntitySafeSplit($html, $size, $delim) {
	$pos = 0;
	for($i = 0; $i < strlen($html); $i++) {
		if ($pos >= $size && !$unsafe) {
			$out .= $delim;
			$unsafe = 0;
			$pos = 0;
		}
		$c = substr($html, $i, 1);
		if ($c == "&")
			$unsafe = 1;
		elseif ($c == ";")
			$unsafe = 0;
		elseif ($c == " ")
			$unsafe = 0;
		$out .= $c;
		$pos++;
	}
	return $out;
}

?>
