<?php
//phpinfo();
require_once 'constants.php';
require_once JETSTREAM_ROOT . '/../include/class.GoogleConnect.php';
require_once JETSTREAM_ROOT . '/../include/class.GoogleImap.php';
//require_once JETSTREAM_ROOT . '/../libs/Zend/Mail/Protocol/Imap.php';
//require_once JETSTREAM_ROOT . '/../libs/Zend/Mail/Storage/Imap.php';

class GoogleEmail {

    private $accessToken = '';
    private $userEmail = '';
    private $imap = '';
    private $host = '';
    private $port = '';

    private $receives = '';

    private $userid;
    private $client;
    private $oauth2;

    public function __construct($oClient, $oAuth2, $userid) {

        $this->userid = $userid;
        $this->client = $oClient;
        $this->oauth2 = $oAuth2;

        $this->host = 'imap.gmail.com';
        $this->port = 993;
        $this->accessToken = $this->client->getAccessToken();

        //TODO need exception checke later, besso
        $user = $this->oauth2->userinfo->get();
        $this->userEmail = filter_var($user['email'], FILTER_SANITIZE_EMAIL);;
        if ( $this->userEmail == '' ) {
            error_log('Google fail to get Email from oAuth2 service');
        }
        $this->insertUserEmail($this->userEmail);

    }

    public function insertUserEmail($email) {

        $sql = 'Update GoogleToken set email = ? where PersonId = ?';
        $params[] = array(DTYPE_STRING, $email);
        $params[] = array(DTYPE_INT, $this->userid);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $status = DbConnManager::GetDb('mpower')->Exec($sql);

        if (!$status) {
            return false;
        }
        return true;

    }
}
