<?php
/**
 * class.fieldsMap.php - contain the fieldMap class
 * @package database
 * include array class
 */
require_once 'class.DbConnManager.php';
require_once 'class.SqlBuilder.php';
require_once 'class.MpRecord.php';
require_once 'lib.array.php';
require_once 'class.JetCache.php';

class MapLookup
{
	private static $lookup_data = array();
	private static $cache_name = 'maps';

	public static function Load($company_id) {
	
		if (!in_array($company_id, self::$lookup_data)) {
			if (!$load_array = JetCache::Load($company_id, self::$cache_name)) {
				$load_array = self::LoadFromDb($company_id);
			}
			self::$lookup_data[$company_id] = $load_array;
		}
		return self::$lookup_data[$company_id];
	}

	/**
	 * Convert the contact layout into an array organized by position and section
	 *
	 */
	public static function LoadFromDb($company_id) {

		// Load the AccountMap
		$accountmap_sql = "SELECT * 
			FROM AccountMap  LEFT OUTER JOIN
			VcardKeywordMapping ON AccountMap.KeywordID = VcardKeywordMapping.KeywordID
			WHERE CompanyID = ? AND [Enable] = 1 AND Position > 0";
		
		$accountmap_sql = SqlBuilder()->LoadSql($accountmap_sql)->BuildSql(array(DTYPE_INT, $company_id));

		$accountmap_results = DbConnManager::GetDb('mpower')->Exec($accountmap_sql);
		
		self::CreateLayout($accountmap_results, $account_map, $account_layout, 'AccountMapID');
		$account_map['type'] = 'account';
		
		// Load the ContactMap
		$contactmap_sql = "SELECT * 
			FROM ContactMap LEFT OUTER JOIN
			VcardKeywordMapping ON ContactMap.KeywordID = VcardKeywordMapping.KeywordID
			WHERE CompanyID = ? AND [Enable] = 1 AND Position > 0";
		
		$contactmap_sql = SqlBuilder()->LoadSql($contactmap_sql)->BuildSql(array(DTYPE_INT, $company_id));
		$contactmap_results = DbConnManager::GetDb('mpower')->Exec($contactmap_sql);
		
		self::CreateLayout($contactmap_results, $contact_map, $contact_layout, 'ContactMapID');
		$contact_map['type'] = 'contact';
		
		// Load the section names and put them into the Layout
		$section_sql = "SELECT * FROM Section WHERE CompanyID = ?";
		$section_sql = SqlBuilder()->LoadSql($section_sql)->BuildSql(array(DTYPE_INT, $company_id));
		
		$sections = DbConnManager::GetDb('mpower')->Exec($section_sql);
		
		foreach ($sections as $section) {
			if (isset($account_layout[$section['SectionID']])) {
				$account_layout[$section['SectionID']] = array_merge($account_layout[$section['SectionID']], $section);
				$account_layout[$section['SectionID']]['SectionName'] = $section['AccountSectionName'];
			}
			if (isset($contact_layout[$section['SectionID']])) {
				$contact_layout[$section['SectionID']] = array_merge($contact_layout[$section['SectionID']], $section);
				$contact_layout[$section['SectionID']]['SectionName'] = $section['ContactSectionName'];
			}
		}
		usort($account_layout, 'CompareMapSection');
		usort($contact_layout, 'CompareMapSection');
		
		// Create an array with all four variables and store it in the Jetcache
		$save_array = array('account_map' => $account_map, 'contact_map' => $contact_map, 'account_layout' => $account_layout, 'contact_layout' => $contact_layout);
		JetCache::Save($company_id, self::$cache_name, $save_array);
		return $save_array;
	}

	private static function CreateLayout($results, &$map, &$layout, $id_field) {
		foreach ($results as $result) {
			// Force name fields to be required
			if (isset($result['IsCompanyName']) && $result['IsCompanyName'] == 1 || isset($result['IsFirstName']) && $result['IsFirstName'] == 1 || isset($result['IsLastName']) && $result['IsLastName'] == 1) {
				$result['IsRequired'] = 1;
			}
			$map['fields'][$result[$id_field]] = $result;
			$align = (empty($result['Align'])) ? 0 : (int) $result['Align'];
			$layout[$result['SectionID']]['rows'][(int) $result['Position']][$align] = $result[$id_field];
		}
		
		// Fill in all the missing rows in the sections
		foreach ($layout as & $section) {
			$full_section = array_fill(1, max(array_keys($section['rows'])), "");
			foreach ($section['rows'] as $key => $row) {
				$full_section[$key] = $row;
			}
			
			$section['rows'] = $full_section;
		}
	}
	
	public static function GetVcardFields($company_id, $field_type) {
		$map = self::Load($company_id);
		$vcard_fields = array();
		foreach($map[$field_type . '_map']['fields'] as $field_def) {
			if (!empty($field_def['Vcard'])) {
				$vcard_fields[$field_def['FieldName']] = $field_def['Vcard'];
			}
		}
		return $vcard_fields;
	}
	
	public static function GetFieldDef($company_id, $field_type, $field_id) {
		$map = self::Load($company_id);

		return $map[$field_type . '_map']['fields'][$field_id];
	}

	public static function GetFieldDefByFieldName($company_id, $field_name, $field_type = 'contact') {
		$map = self::Load($company_id);
		foreach ($map[$field_type . '_map']['fields'] as $fielddef) {
			if ($fielddef['FieldName'] == $field_name) {
				return $fielddef;
			}
		}
		return false;
	}
	
	public static function GetFieldDefByLabelName($company_id, $label_name, $field_type = 'contact') {
		$map = self::Load($company_id);
        //echo '<pre>'; print_r($map['account_map']); echo '</pre>'; exit;
		foreach ($map[$field_type . '_map']['fields'] as $fielddef) {
			if ($fielddef['LabelName'] == $label_name) {
				return $fielddef;
			}
		}
		return false;
	}
	
	public static function GetFieldDefByGoogleField($company_id, $label_name, $field_type = 'contact') {
		$map = self::Load($company_id);
       // echo '<pre>'; print_r($map['contact_map']); echo '</pre>'; exit;
		foreach ($map[$field_type . '_map']['fields'] as $fielddef) {
			if ($fielddef['googleField'] == $label_name) {
				return $fielddef;
			}
		}
		return false;
	}

		
	public static function GetFieldDefByDefaultLabelName($company_id, $label_name, $field_type = 'contact') {
		$map = self::Load($company_id);
		
		// Load map and layout by the appropriate type
		if ($field_type == 'contact') {
			$this_layout = $map['contact_layout'];
			$this_map = $map['contact_map'];
		}
		else {
			$this_layout = $map['account_layout'];
			$this_map = $map['account_map'];
		}

		// Set the default section ID
		$def_section = 0;
		foreach($this_layout as $section) {
			if ($section['SectionName'] == 'DefaultSection') {
				$def_section = $section['SectionID'];
			}
		}

		foreach ($map[$field_type . '_map']['fields'] as $fielddef) {
            if ($fielddef['LabelName'] == $label_name && $fielddef['SectionID'] == $def_section) {
		        return $fielddef;
		    }
		}
		return false;
	}
	
	public static function GetAccountLayout($company_id) {
		$map = self::Load($company_id);
		return $map['account_layout'];
	}

	public static function GetAccountMap($company_id, $sort = false) {
		$map = self::Load($company_id);
		if ($sort) {
			$output = $map['account_map'];
			masort($output['fields'], $sort);
			return $output;
		}
		return $map['account_map'];
	}

	public static function GetContactLayout($company_id) {
		$map = self::Load($company_id);
		return $map['contact_layout'];
	}

	public static function GetContactMap($company_id, $sort = false) {
		$map = self::Load($company_id);
		if ($sort) {
			$output = $map['contact_map'];
			masort($output['fields'], $sort);
			return $output;
		}
		return $map['contact_map'];
	}
	
	/*
	 * Get all the fields that are email addresses
	 * param $is_simple TRUE = array of field names, FALSE = assoc array, field names => field labels
	 */
	public static function GetContactEmailFields($company_id, $is_simple = TRUE) {
		$map = self::Load($company_id);
		
		$email_fields = array();
		foreach ($map['contact_map']['fields'] as $field) {
			if ($field['ValidationType'] == 5) {
				if ($is_simple) {
					$email_fields[] = $field['FieldName'];
				} else {
					$email_fields[$field['FieldName']] = $field['LabelName'];
				}
			}
		}
		return $email_fields;
	}

	/**
	 * function for getting account name field
	 * return account name field
	 * param $return_fieldname TRUE - return db field name, FALSE - return map id
	 */
	public static function GetAccountNameField($company_id, $return_fieldname = FALSE) {
		return self::GetDefinedField($company_id, 'account_map', 'IsCompanyName', 'company_name_field', $return_fieldname);
	}

	/**
	 * function for getting contact first name field
	 * return contact first name field
	 * param $return_fieldname TRUE - return db field name, FALSE - return map id
	 */
	public static function GetContactFirstNameField($company_id, $return_fieldname = FALSE) {
		return self::GetDefinedField($company_id, 'contact_map', 'IsFirstName', 'first_name_field', $return_fieldname);
	}
		
	/**
	 * function for getting contact last name field
	 * return contact last name field
	 * param $return_fieldname TRUE - return db field name, FALSE - return map id
	 */
	public static function GetContactLastNameField($company_id, $return_fieldname = FALSE) {
		return self::GetDefinedField($company_id, 'contact_map', 'IsLastName', 'last_name_field', $return_fieldname);
	}

	/** GMS 10/1/2010
	 * function for getting contact title field
	 * return contact title field
	 * param $return_fieldname TRUE - return db field name, FALSE - return map id
	 */
	public static function GetContactTitleField($company_id, $return_fieldname = FALSE) {
		/*
		 * Temporarily selecting by LabelName per Allan (10/26/2011)
		* //return self::GetDefinedField($company_id, 'contact_map', 'IsContactTitle', 'contact_title_field', $return_fieldname);
		*/
		$fieldDef = self::GetFieldDefByLabelName($company_id, 'Title');
		return $fieldDef['FieldName'];
	}
	
	/** Vern Gorman 08/20/2013
	 * function for getting contact City field
	 * return contact City field
	 * param $return_fieldname TRUE - return db field name, FALSE - return map id
	 */
	public static function GetContactCityField($company_id, $return_fieldname = FALSE) {
		$fieldDef = self::GetFieldDefByDefaultLabelName($company_id, 'City');
		return $fieldDef['FieldName'];
	}

	/** Vern Gorman 08/20/2013
	 * function for getting contact State field
	 * return contact State field
	 * param $return_fieldname TRUE - return db field name, FALSE - return map id
	 */
	public static function GetContactStateField($company_id, $return_fieldname = FALSE) {
		$fieldDef = self::GetFieldDefByDefaultLabelName($company_id, 'State');
		if (!$fieldDef) {
			$fieldDef = self::GetFieldDefByDefaultLabelName($company_id, 'St');
		}
		return $fieldDef['FieldName'];
	}
	
	/** Vern Gorman 08/20/2013
	 * function for getting account City field
	 * return account City field
	 * param $return_fieldname TRUE - return db field name, FALSE - return map id
	 */
	public static function GetAccountCityField($company_id, $return_fieldname = FALSE) {
		$fieldDef = self::GetFieldDefByDefaultLabelName($company_id, 'City', 'account');
		return $fieldDef['FieldName'];
	}

	/* get email by label 'Email - Primary' besso */
	public static function GetPrimaryEmailField($company_id, $return_fieldname = FALSE) {
		$fieldDef = self::GetFieldDefByDefaultLabelName($company_id, 'Email - Primary');
		return $fieldDef['FieldName'];
	}
	
	/* get email by label 'Phone - Main' besso */
	public static function GetMainPhoneField($company_id, $return_fieldname = FALSE) {
		$fieldDef = self::GetFieldDefByDefaultLabelName($company_id, 'Phone - Main');
		return $fieldDef['FieldName'];
	}

    public static function getLabelList($company_id, $label, $return_fieldname = FALSE) {
        $aFieldDef = array();
		$aFieldDef = self::GetFieldDefByDefaultLabelName($company_id, $label . '*');
        return $aFieldDef;
    }

	/* get email by label 'Phone - Direct' besso */
	public static function GetDirectPhoneField($company_id, $return_fieldname = FALSE) {
		$fieldDef = self::GetFieldDefByDefaultLabelName($company_id, 'Phone - Direct');
		return $fieldDef['FieldName'];
	}


	/** Vern Gorman 08/20/2013
	 * function for getting account State field
	 * return account State field
	 * param $return_fieldname TRUE - return db field name, FALSE - return map id
	 */
	public static function GetAccountStateField($company_id, $return_fieldname = FALSE) {
		$fieldDef = self::GetFieldDefByDefaultLabelName($company_id, 'State', 'account');
		if (!$fieldDef) {
			$fieldDef = self::GetFieldDefByDefaultLabelName($company_id, 'St', 'account');
		}
		return $fieldDef['FieldName'];
	}
	
	/**
	 * function for getting contact first email field
	 * return contact first email field
	 * param $return_fieldname TRUE - return db field name, FALSE - return map id
	 */
	public static function GetFirstEmailField($company_id, $return_fieldname = FALSE) {
		return self::GetDefinedField($company_id, 'contact_map', 'IsEmailAddress', 'first_email_field', $return_fieldname);
	}

	/**
	 * Loops through map to look for a particular flag and returns the field
	 */
	public function GetDefinedField($company_id, $map_name, $field_to_check, $quick_cache_name, $return_fieldname = FALSE) {

		/*
		echo '<pre>'; print_r($company_id); echo '</pre>';
		echo '<pre>'; print_r($map_name); echo '</pre>';
		echo '<pre>'; print_r($field_to_check); echo '</pre>';
		echo '<pre>'; print_r($quick_cache_name); echo '</pre>';
		echo '<pre>'; print_r($return_fieldname); echo '</pre>';
		exit;
		*/

		$map = self::Load($company_id);

		if (isset($map[$quick_cache_name])) {
			$field_id = $map[$quick_cache_name];
		} else {
			$found = FALSE;
			foreach ($map[$map_name]['fields'] as $field_id => $field) {
				if ($field[$field_to_check] == 1) {
					self::$lookup_data[$company_id][$quick_cache_name] = $field_id;
					
					JetCache::Save($company_id, self::$cache_name, self::$lookup_data[$company_id]);
					$found = TRUE;
					break;
				}
			}
			if (!$found) {
				return FALSE;
			}
		}
		if ($return_fieldname) {
			return $map[$map_name]['fields'][$field_id]['FieldName'];
		}
		return $field_id;
	}
}

function CompareMapSection($a, $b) {
	$a_index = (!empty($a["OrderIndex"])) ? $a["OrderIndex"] : 0;
	$b_index = (!empty($b["OrderIndex"])) ? $b["OrderIndex"] : 0;
	
	if ($a_index > $b_index) {
		return 1;
	} 
	if ($a_index < $b_index) {
		return -1;
	}

	return ($a["SectionID"] > $b["SectionID"]) ? 1 : -1;
}

function masort(&$data, $sortby) {
	static $sort_funcs = array();
	
	if (empty($sort_funcs[$sortby])) {
		$code = "\$c = 0;";
		foreach (split(',', $sortby) as $key) {
			$array = array_pop($data);
			array_push($data, $array);
			if (is_numeric($array[$key])) {
				$code .= "if ( \$c = ((\$a['$key'] == \$b['$key']) ? 0:((\$a['$key'] < \$b['$key']) ? -1 : 1 )) ) return \$c;";
			} else {
				$code .= "if ( (\$c = strcasecmp(\$a['$key'],\$b['$key'])) != 0 ) return \$c;\n";
			}
		}
		$code .= 'return $c;';
		$sort_func = $sort_funcs[$sortby] = create_function('$a, $b', $code);
	} else {
		$sort_func = $sort_funcs[$sortby];
	}
	$sort_func = $sort_funcs[$sortby];
	uasort($data, $sort_func);
}
