<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';

class DateUtil {
	
	public static function getCreatedDate($type, $id){
		
		$sql = "SELECT CreatedDate FROM " . $type . " WHERE " . $type . "ID = ?";
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id));
		$created = DbConnManager::GetDb('mpower')->GetOne($sql);
		
		return $created['CreatedDate'];
	}
	
	
	public static function setCreatedDate($type, $id){
		$sql = "UPDATE " . $type . " SET CreatedDate = '" . date('m/d/y') . "' WHERE " . $type . "ID = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id));
		DbConnManager::GetDb('mpower')->Exec($sql);
	}
	
}