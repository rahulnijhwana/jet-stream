<?php
require_once 'mpconstants.php';
require_once 'class.DbConnManager.php';
require_once 'class.SqlBuilder.php';
require_once 'class.DebugUtil.php';

class DataType
{	
	private $dataTypes;
	
	public function __construct($table)
	{
		$sql = "SELECT COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $table));
		$results = DbConnManager::GetDb('mpower')->Exec($sql);
		
		foreach($results as $result){
			$this->dataTypes[$result['COLUMN_NAME']] = $result['DATA_TYPE'];
		}
	}

	public function getDataType($col)
	{	
		switch($this->get($col))
		{	
			case 'varchar' :
				$dataType = DTYPE_STRING;
				break;
			case 'text' :
				$dataType = DTYPE_STRING;
				break;
			case 'int' :
				$dataType = DTYPE_INT;
				break;
			case 'bit' :
				$dataType = DTYPE_INT;
				break;
			case 'float' :
				$dataType = DTYPE_FLOAT;
				break;
			case 'datetime' :
				$dataType = DTYPE_STRING;
				break;
			default : 
				$dataType = DTYPE_STRING;
				break;
		}
		return $dataType;
	}

	private function get($col)
	{
		$exists = array_key_exists($col, $this->dataTypes);
		return $exists ? $this->dataTypes[$col] : 0;
	}
	
}