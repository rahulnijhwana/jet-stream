<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/include/mpconstants.php';

/**
 * The DB fields initialization
 */
class RecOpportunityProd extends MpRecord
{
	public function Initialize() {
		$this->db_table = 'ot_Opp_Prod';
		$this->recordset->GetFieldDef('OPID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('OpportunityID')->required = TRUE;
		$this->recordset->GetFieldDef('ProductID')->required = TRUE;
		
		parent::Initialize();
	}

	public function Save($simulate = false) {
		parent::Save($simulate);
	}
}

