<?php
class FormUtil {
	
	/**
	 * Sorts select list data
	 * @param array $options
	 */
	public static function sortOptions($data, $optionSetId){
		$options = array();
		
		# Clean out unnecessary data
		foreach ($data as $info){
			$options[$info['OptionID']] = $info['OptionName'];
		}
		
		# TODO Put this in a table and create a way to set this dynamically
		$exceptions = array(3370, 3383, 3379);
		
		if(!in_array($optionSetId, $exceptions)){
			# Sort by alpha
			asort($options);
			
			####################################################
			# Processing for State Lists
			####################################################
			# Put long strings at the bottom of the list
			# Used with Canadian Provinces
			foreach($options as $key=>$val) {
				if(strlen($val) > 2){
					$d[$key] = $val;
				}
				else {
					$c[$key] = $val;
				}
			}
			# Put the lists back together
			foreach($d as $k => $v){
				$c[$k] = $v;
			}
			#####################################################
			# Processing for Country Lists
			####################################################
			# United States should appear at the top of the list
			$options = array();
			foreach($c as $k => $v){
				if($v == 'United States'){
					$options[$k] = $v;
					unset($c[$k]);
				}
			}
			if(!empty($options)){
				foreach($c as $k => $v){
					$options[$k] = $v;
				}
			}
			else {
				$options = $c;
			}
		}
		return $options;
	}
	
}