<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/include/mpconstants.php';

/**
 * The DB fields initialization
 */
class RecEvent extends MpRecord
{
	public function Initialize() {
		$this->db_table = 'Event';
		$this->recordset->GetFieldDef('EventID')->auto_key = TRUE;
		// $this->recordset->GetFieldDef('UserID')->required = TRUE;
		$this->recordset->GetFieldDef('Subject')->required = TRUE;
		
		parent::Initialize();
	}

	public function Save($simulate = false) {
		parent::Save($simulate);
	}

	/*
	 * This pre-populates a set of standard variables for a systematic method of 
	 * rendering events properly
	 */	
	public function Process() {
		list($date, $time) = split(' ', $this['StartDate']);
	
		$this['start_date'] = strtotime($date);
		$this['start_time'] = strtotime($this['StartDate']);
		$this['end_time'] = $this['start_time'] + $this['DurationHours'] * 3600 + $this['DurationMinutes'] * 60;
		$now = getdate();
		$today_begin = mktime(0, 0, 0, $now['mon'], $now['mday'], $now['year']);
	
		// Pending = the scheduled time has past and it isn't closed
		if ($this['start_date'] < $today_begin && !$this['IsClosed']) {
			$this['pending'] = TRUE;
			if ($this['Type'] == 'TASK') {
				$this['start_time'] = time();
			}
		} else {
			$this['pending'] = FALSE;
			if ($this['Type'] == 'TASK' && $this['IsClosed']) {
				// For tasks, move the event up to the day it actually was closed
				$this['start_time'] = strtotime($this['DateModified']);
			}
		}
		
		// $this['is_viewable'] should be the test to display the event info or 
		// show it as 'Private'
		if ($this['UserID'] != $_SESSION['USER']['USERID'] && $this['PrivateEvent']) {
			$this['is_viewable'] = FALSE;
			// Blank all the event fields to ensure that data isn't accidently displayed
			$this['Contact'] = $this['FirstName'] = $this['LastName'] = '';
			$this['CompanyName'] = $this['Subject'] = $this['ContactID'] = '';
			$this['EventID'] = 0;
		} else {
			$this['is_viewable'] = TRUE;
			$this['Contact'] = $this['FirstName'] . ' ' . $this['LastName'];
		}
		
		// To use as array keys and comparison in ModuleCalendar
		$this['start'] = date('Ymd', $this['start_time']);
		$this['end'] = date('Ymd', $this['end_time']);
	
		// For display in tpl - these are the proper display formats
		// If we ever need to customize the way the calendar displays dates
		// and times, it can be done here in one fell swoop
		$this['date_string'] = date('n/j/Y', $this['start_time']);
		$this['start_time_string'] = DrawTime($this['start_time']);
		$this['end_time_string'] = DrawTime($this['end_time']);
		
		// Formatting the duration into a formatted string
		// Could move this into a DrawDuration function
		$duration = array();
		if (isset($this['DurationHours']) && $this['DurationHours'] > 0) {
			$duration[] = $this['DurationHours'] . (($this['DurationHours'] > 1) ? ' hrs' : ' hr');
		}
		if (isset($this['DurationMinutes']) && $this['DurationMinutes'] > 0) {
			$duration[] = $this['DurationMinutes'] . (($this['DurationMinutes'] > 1) ? ' mins' : ' min');
		}
		$this['duration_string'] = implode(' ', $duration);
	
		// Attach the user record from the tree object for easy access
		$this['user'] = $_SESSION['tree_obj']->GetRecord($this['UserID']);
	
		// Attach the eventtype record from the cache for easy access
		$this['eventtype'] = EventTypeLookup::GetRecord($this['EventTypeID']);
		$thiss[$key] = $this;
	}
}

