<?php
class BooleanUtil {
	
	public static function convert($str)
	{	
		$value = strtolower($str);
		
		if(
			$value === "y" ||
			$value === "yes" ||
			$value === "true"
		){
			return 1;
		}
		else {
			return 0;
		}
	}
	
	public static function isValid($str)
	{
		$value = strtolower($str);
		
		if(
			$value === "y" ||
			$value === "yes" ||
			$value === "true" ||
			$value === "n" ||
			$value === "no" ||
			$value === "false"
		){
			return true;
		}
		else {
			return false;
		}
	}
}