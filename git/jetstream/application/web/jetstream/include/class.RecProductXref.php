<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/include/mpconstants.php';

/**
 * The SubAnswer database record
 * @package database
 * @subpackage record
 */
class ProductXRef extends MpRecord
{
    public function __toString() {
        return 'ProductXRef Record';
    }

    public function __set($variable, $value) {
        parent::__set($variable, $value);
    }

    public function Initialize() {
        $this->db_table = 'Opp_Product_XRef';

        $this->recordset->GetFieldDef('ID')->auto_key = TRUE;
        $this->recordset->GetFieldDef('DealID')->required = TRUE;
        $this->recordset->GetFieldDef('ProductID')->required = TRUE;
//        $this->CMID->required = TRUE;

        parent::Initialize();
        return $this;
    }
}

?>
