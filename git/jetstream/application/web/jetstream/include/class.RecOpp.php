<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/include/lib.date.php';
require_once BASE_PATH . '/include/mpconstants.php';

/**
 * The Opportunity database record
 * @package database
 * @subpackage record
 */
class Opp extends MpRecord
{
    public function __toString() {
        return 'Opp Record';
    }

    public function __set($variable, $value) {
        parent::__set($variable, $value);

        switch ($variable) {
            case 'CTargetDate':
                parent::__set('TargetDate', TimestampToMsSql($value));
                break;
            case 'TargetDate':
                parent::__set('CTargetDate', strtotime($value));
                break;
            case 'CFirstMeeting':
                if ($value == 0) {
                    parent::__set('FirstMeeting', NULL);
                    parent::__set('FirstMeetingTime', NULL);
                    break;
                }
                parent::__set('FirstMeeting', TimestampToMsSql($value, "d"));
                parent::__set('FirstMeetingTime', TimestampToMsSql($value, "t"));
                break;
            case 'FirstMeeting':
            case 'FirstMeetingTime':
                if ($this->InRecord('FirstMeeting') && $this->InRecord('FirstMeetingTime')) {
                	parent::__set('CFirstMeeting', strtotime(date("F j Y", strtotime($this->FirstMeeting)) . " " . $this->FirstMeetingTime), 'c');
					parent::__set('CDaysSinceFM', floor((time() - $this->CFirstMeeting)/86400));
                } else {
                	parent::__set('CFirstMeeting', 0);
                }
                break;
            case 'CNextMeeting':
                if ($value == 0) {
                    parent::__set('NextMeeting', NULL);
                    parent::__set('NextMeetingTime', NULL);
                    break;
                }
                parent::__set('NextMeeting', TimestampToMsSql($value, "d"));
                parent::__set('NextMeetingTime', TimestampToMsSql($value, "t"));
                break;
            case 'NextMeeting':
            case 'NextMeetingTime':
                if ($this->InRecord('NextMeeting') && $this->InRecord('NextMeetingTime')) {
                    parent::__set('CNextMeeting', strtotime(date("F j Y", strtotime($this->NextMeeting)) . ' ' . $this->NextMeetingTime), 'c');
                }
                else {
                    parent::__set('CNextMeeting', 0);
                }
                break;
            case 'CActualCloseDate':
                parent::__set('ActualCloseDate', TimestampToMsSql($value));
                break;
            case 'ActualCloseDate':
                parent::__set('CActualCloseDate', strtotime($value));
                break;
        }
    }

    public function SelfCategorize() {
        $now = time();
        
        if ($this->InRecord('Category') && ($this->Category == MP_CLOSED || $this->Category == MP_REMOVED) && !$this->InRecord('CCmCategory')) {
            return;
        }

        $category = MP_TARGET; // if nothing, it's a target

        if ($this->InRecord('CCmCategory')) {
            $category = $this->CCmCategory;
        }

        if ($category != MP_CLOSED && $category != MP_REMOVED) {

            if ($this->InRecord('CFirstMeeting') && $this->CFirstMeeting > 0) {
                $category = MP_FM;

                if ($this->CFirstMeeting < $now) {
                    $category = MP_IPS;
                }
            }

            if ($this->InRecord('CNextMeeting') && $this->CNextMeeting > 0) {
                $category = MP_IP;
                if ($this->CNextMeeting < $now) {
                    $category = MP_IPS;
                }
            }
            if ($category == MP_IP || $category == MP_IPS) {
            	if ($this->Requirement1 == 1 && $this->Person == 1 && $this->Need == 1 && $this->Money == 1 && $this->Time == 1 && $this->Requirement2 == 1) {
                    $category = $category + 2;
                }
            }
        }
        $this->Category = $category;
    }

    public function Initialize() {
        $this->db_table = 'opportunities';
    	$this->recordset->GetFieldDef('DealID')->auto_key = TRUE;
        $this->recordset->GetFieldDef('CompanyID')->required = TRUE;
        $this->recordset->GetFieldDef('PersonID')->required = TRUE;

        parent::Initialize();
    }

    public function UpdateMilestoneTotals() {
        // One monstrous query to determine that determines how many milestone criteria are not met for each milestone
        // Using SQL was much faster than loading the data into PHP because of the number of fields necessary to calculate
        $sql = "select Location, count(*) as Unfinished
            from SubMilestones
            left outer join SubAnswers on SubMilestones.SubID = SubAnswers.SubID and DealID = ?
            where SubMilestones.companyid = ? and SubMilestones.Mandatory = 1 and SubMilestones.Enable = 1
            and (SubAnswers.Answer is NULL
                or (SubMilestones.YesNo = 1 and SubAnswers.Answer != cast(SubMilestones.RightAnswer as CHAR))
                or (SubMilestones.CheckBox = 1 and SubAnswers.Answer != '1')
                or (SubMilestones.DropDown = 1 and
                    (
                        (Answer = DropChoice1 and DChoiceOK1 = 0) or (Answer = DropChoice2 and DChoiceOK2 = 0)
                        or (Answer = DropChoice3 and DChoiceOK3 = 0) or (Answer = DropChoice4 and DChoiceOK4 = 0)
                        or (Answer = DropChoice5 and DChoiceOK5 = 0) or (Answer = DropChoice6 and DChoiceOK6 = 0)
                        or (Answer = DropChoice7 and DChoiceOK7 = 0) or (Answer = DropChoice8 and DChoiceOK8 = 0)
                        or (Answer = DropChoice9 and DChoiceOK9 = 0) or (Answer = DropChoice10 and DChoiceOK10 = 0)
                        or (Answer = DropChoice11 and DChoiceOK11 = 0) or (Answer = DropChoice12 and DChoiceOK12 = 0)
                        or (Answer = DropChoice13 and DChoiceOK13 = 0) or (Answer = DropChoice14 and DChoiceOK14 = 0)
                        or (Answer = DropChoice15 and DChoiceOK15 = 0) or (Answer = DropChoice16 and DChoiceOK16 = 0)
                        or (Answer = DropChoice17 and DChoiceOK17 = 0) or (Answer = DropChoice18 and DChoiceOK18 = 0)
                        or (Answer = DropChoice19 and DChoiceOK19 = 0) or (Answer = DropChoice20 and DChoiceOK20 = 0)
                        or (Answer not in (
                            DropChoice1, DropChoice2, DropChoice3, DropChoice4, DropChoice5,
                            DropChoice6, DropChoice7, DropChoice8, DropChoice9, DropChoice10,
                            DropChoice11, DropChoice12, DropChoice13, DropChoice14, DropChoice15,
                            DropChoice16, DropChoice17, DropChoice18, DropChoice19, DropChoice20
                        ))
                    )
                )
            )
            group by Location";


        $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->DealID), array(DTYPE_INT, $this->CompanyID));
        $recordset = DbConnManager::GetDb('mpower')->Execute($sql);

        $req = array_fill(0, 6, 1);

        foreach($recordset as $record) {
            $req[$record->Location] = 0;
        }

        $this->Requirement1 = $req[0];
        $this->Person = $req[1];
        $this->Need = $req[2];
        $this->Money = $req[3];
        $this->Time = $req[4];
        $this->Requirement2 = $req[5];
    }

    public function Save($simulate = false) {
        // Opps do not get deleted - instead, change their category to removed
        if (!is_null($this->delete) && $this->delete == TRUE) {
            $this->Category = 9;
            $this->ClearDelete();
        }

        parent::Save($simulate);
    }
}


?>
