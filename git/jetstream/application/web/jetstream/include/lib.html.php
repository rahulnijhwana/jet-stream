<?php

function CreatePage($page_title, $page_sections) {
	
	$css_links = '';
	$javascript = '';
	$javascript_init = '';

	if (count($page_sections)) {
		$css_files = array();
		$js_files = array();
		foreach ($page_sections as $section) {
			if ($section instanceof PageComponent) {

				if (is_array($section->GetCssFiles())) {
					foreach ($section->GetCssFiles() as $file) {
						$media = (is_array($file)) ? $file[0] : 'screen'; 
						$css_files[$media][] = (is_array($file)) ? $file[1] : $file;
					}
				}
				if(count($section->GetJsFiles()) > 0) {
					foreach ($section->GetJsFiles() as $file) {
						$js_files[] = $file;
					}
				}
				$javascript .= $section->GetJs();
				$javascript_init .= $section->GetJsInit();
			}
		}
		if (count($css_files) > 0) {
			foreach ($css_files as $media => $files) {
				$file_list = implode(',', array_unique($files));
				$css_links .= "<link href=\"css/$file_list\" rel=\"stylesheet\" type=\"text/css\" media=\"$media\">";
			}
		}
		if (count($js_files) > 0) {
			$js_files = implode(',', array_unique($js_files));
			$js_links = "<script type=\"text/javascript\" src=\"javascript/$js_files\"></script>";
		}
	}
	?>
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
	"http://www.w3.org/TR/html4/strict.dtd">
	<html>
		<head>
			<title><?php echo $page_title ?></title>
			<meta http-equiv="Content-Type" content="text/html;charset=utf-8">

			<script type="text/javascript">
			function init() {
				<?php echo $javascript_init ?>
			}
			window.onload = init;
			var GB_ROOT_DIR = "./greybox/";
			</script>
			
			<?php echo $css_links ?>
			<?php echo $js_links ?>
			
		</head>
		<body>
			<?php
			if (count($page_sections))  {
				foreach ($page_sections as $section) {
					if ($section instanceof PageComponent) {
						echo $section->Render();
					} else {
						echo $section;
					}
				}
			}
			
			if (!empty($javascript)) {
				echo "<script type=\"text/javascript\">\n <!--\n $javascript\n //-->\n </script>";
			}
			?>
		</body>
	</html>
<?php
}

function CreateTextDiv($text) {
	$output = '<div class="info">' . $text . '</div>';
	return $output;
}

?>