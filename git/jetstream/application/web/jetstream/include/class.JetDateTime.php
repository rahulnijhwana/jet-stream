<?php
class JetDateTime extends DateTime {
	public function asMsSql($type = 'b', $utc = 'true') {
		// Clone the object, so we can change the timezone without impacting the original object
		$cloned_copy = clone $this;
		if ($utc) {
			$utc_timezone = new DateTimeZone('UTC');
			$cloned_copy->setTimezone($utc_timezone);
		}
		
		switch ($type) {
			case 't' :
				$output = $cloned_copy->format('g:i A');
				break;
			case 's' :
				$output = $cloned_copy->format('Y-m-d G:i:s.000');
				break;
			// google format RFC 3339
			case 'g' :
				$output = $cloned_copy->format(GOOGLE_DATE_RFC3339);
				break;
			case 'google-recurrence' :
				$output = $cloned_copy->format(GOOGLE_RECURRENCE_RFC3339);
				break;
			default:
				$date = $cloned_copy->format('Y-m-d');
				$time = ($type != 'd') ? $cloned_copy->format('H:i:s') : '00:00:00';
				$output = $date . 'T' . $time;
		}
		
		return $output;
	}

	public function asScalIso8601() {
		$cloned_copy = clone $this;
		$utc_timezone = new DateTimeZone('UTC');
		$cloned_copy->setTimezone($utc_timezone);
				
		return $cloned_copy->format("Ymd\THis\Z");
	}
	
	function NormalDateFormat() {
		return $this->format('n/j/y');
	}
	
	function DrawTime() {
		if ($this->format('m') == 0) {
			$format = "ga";
		} else {
			$format = "g:ia";
		}
		return $this->format($format);
	}	

	public function addDate( $diff, $date = '' ,$format = 'Y-m-d', $diff_type = 'day' ) {
		$date = ( $date == '' ) ? date($format) : $date;
		$rtn = date( $format, strtotime( $date . ' ' . $diff . ' ' . $diff_type ) );
		return $rtn;
	}	

}
