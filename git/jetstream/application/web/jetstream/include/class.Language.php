<?php

require_once BASE_PATH . '/include/class.JetCache.php';

class Language
{
	/**
	 * The single instance of the class
	 * @var object
	 */
	private static $instance;

	/**
	 * Private constructor to dissallow instantiaion
	 */
	private function __construct() {
	}

	/**
	 * Returns a single instance of the object
	 * @return object
	 */
	public static function GetInstance() {
		if (!isset(self::$instance)) {
			$object = __CLASS__;
			self::$instance = new $object();
		}
		return self::$instance;
	}
	
	public static $language_terms = array();

	/* 
	 * Warning : The magic method __toString() must have public visibility and cannot be static
	 */
	public function __toString() {
		return ("Language Set");
	}

	/* 
	 * Warning : The magic method __get() must have public visibility and cannot be static
	 */
	public static function __get($name) {
		
		if (!self::$language_terms || count(self::$language_terms) == 0) {
			self::Init();
		}
		if (array_key_exists($name, self::$language_terms)) {
			$value = self::$language_terms[$name];
			$regex = "|\{(\w*?)\}|"; // Find words encased in curly braces
			return preg_replace_callback($regex, array('self', 'LookupRegEx'), $value);
		} else {
			return NULL;
		}
	}

	private static function LookupRegEx($val) {
		return self::__get($val[1]); // Go back and get the value of the word
	}

	public static function Term($val) {
		return self::__get($val); // Go back and get the value of the word
	}

	public static function LoadCache($company_id) {
		if (!is_null($company_id)) {
			self::$language_terms = JetCache::Load($company_id, 'language_terms');
		}
	}
	
	public static function Init($company = NULL, $lang = 'en') {
		if (!is_null($company)) {
			$company_id = $company->CompanyID;
			if (self::$language_terms = JetCache::Load($company_id, 'language_terms')) {
				return;
			}
		}
		
		//		if(isset($_SESSION['language_terms'])) {
		//			self::$language_terms = $_SESSION['language_terms'];
		//			return;
		//		}
		
		require BASE_PATH . '/lang/en_lang.php';
		if ($lang != 'en') {
			$english = $language_terms;
			require BASE_PATH . '/lang/' . $lang . '_lang.php';
			$language_terms = array_merge($english, $language_terms);
		}
		if (!is_null($company)) {
			foreach ($company->GetDbRow() as $variable => $value) {
				if (!empty($value) && isset($language_terms[$variable])) {
					$language_terms[$variable] = $value;
				}
			}
		}
		// $_SESSION['language_terms'] = self::$language_terms = $language_terms;
		self::$language_terms = $language_terms;
		if (!is_null($company) && !empty($company_id)) {
			JetCache::Save($company_id, 'language_terms', $language_terms);
		}
	}
}
