<?php
require_once 'constants.php';
require_once JETSTREAM_ROOT . '/../include/class.GoogleConnect.php';
require_once JETSTREAM_ROOT . '/../include/class.AccountUtil.php';
require_once JETSTREAM_ROOT . '/../include/class.ContactUtil.php';
require_once JETSTREAM_ROOT . '/../include/class.EventUtil.php';
require_once JETSTREAM_ROOT . '/../include/class.DbConnManager.php';
require_once JETSTREAM_ROOT . '/../include/class.SqlBuilder.php';
require_once JETSTREAM_ROOT . '/../slipstream/class.ModuleEventNew.php';

/* TODO Sync senarios
 * 1) get all targets which is not 'closed' and put that in google
 * 	use common insert method and update googleId after inserting google
 * 2) Crond(sync) get every not 'closed' list from google and array compare  with all Jet lists
 *	then update / insert both. heavy load
 *	think about best Algorithm how to merge different array or more efficient way
 * 3) tasks could be several in same day. so merge should be done under same day
 */
class GoogleTasks {

	private $sync = true;
	private $listTaskList;
	private $googleTaskLists;
	private $defaultBeforeDate = 90;
	private $syncClosedTask = false;	// sync Closed task also
    private $user_timezone;

    public $tasks;
    public $tasklists;

    private $user;
    private $taskProcessed;
    private $listTaskListID;
    private $companyId;

	public function __construct($oTaskLists, $oTask, $user, $sync=true) {

        $this->tasklists = $oTaskLists;
        $this->tasks = $oTask;
        $this->user = $user;
        $this->companyId = GoogleConnect::getCompanyId($user);

        if (!empty($_SESSION['USER']['BROWSER_TIMEZONE'])) {
            $this->user_timezone = $_SESSION['USER']['BROWSER_TIMEZONE'];
        } else {
            $this->user_timezone = GoogleConnect::getUserTimezone($user);
        }
		
		if ('' == $this->listTaskListID) {
			//$this->listTaskListID = 'MTE0NzAwNTAyMjcyOTQ5Mzk2MTI6MDow';
            try {
			    $this->listTaskListID = $this->getGoogleListTaskLists();
            } catch (Exception $e) {
                return false;
            }
		}

        if ( $sync ) {
            if ( !$this->hasProcessed() ) {
                $this->getProcessed();
                $this->syncTask();
            } else {
                $this->getProcessed();
                if ( $this->decideSync() ) {
                    error_log('sync task : ' . $this->taskProcessed);
                    $this->syncTask();
                } else {
                    error_log('sync task is not run for interval');
                }
            }
        }

	}


    private function decideSync() {

        if ( $this->taskProcessed == '' ) {
            return false;
        }

        $pTS = strtotime($this->taskProcessed);
        $nTS = strtotime(GoogleConnect::getTodayWithGoogleFormat());
        $diff = $nTS - $pTS;

        if ( $diff > GOOGLE_SYNC_INTERVAL ) {
            return true;
        } else {
            return false;
        }

    }

    private function hasProcessed() {
        $sql = 'Select TOP 1 task_processed, task_list_id from GoogleToken where personId = ?';
        $params[] = array(DTYPE_INT, $this->user);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aProcessed = DbConnManager::GetDb('mpower')->Exec($sql);
        return ( !empty($aProcessed[0]['task_processed']) ) ? true : false;
    }
     

    private function getProcessed() {

        $sql = 'Select TOP 1 task_processed, task_list_id from GoogleToken where personId = ?';
        $params[] = array(DTYPE_INT, $this->user);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aProcessed = DbConnManager::GetDb('mpower')->Exec($sql);
        if (count($aProcessed) > 0) {
            $today = date("YmdHis");
            $utc_timezone = new DateTimeZone('UTC');
            $jetDate = new JetDateTime($today, $utc_timezone);
            $today = $jetDate->asMsSql('g');
            $this->taskProcessed = ('' != $aProcessed[0]['task_processed']) ? GoogleConnect::convertGoogleDateFormat($aProcessed[0]['task_processed']) : $today ;
            $this->listTaskListID = ('' != $aProcessed[0]['task_list_id']) ? $aProcessed[0]['task_list_id'] : '' ;
        }

        return true;
    }

    private function setTaskListID($listTaskListID) {
        // save the record
        $sql = 'Update GoogleToken
            Set task_list_id = ?
            Where PersonID = ?';

        $params[] = array(DTYPE_STRING, $listTaskListID);
        $params[] = array(DTYPE_INT, $this->user);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $status = DbConnManager::GetDb('mpower')->Exec($sql);

        if (!$status) {
            return false;
        }
    }

	/* TODO
	 * User could have several tasklists and we use the 1st list of them
	 * If it's a kind of static, let's move to parent::__construct()  
	 * @return $listTaskListID like MTE0NzAwNTAyMjcyOTQ5Mzk2MTI6MDow
	 */
	/* TODO
	 * 'Error calling GET https://www.googleapis.com/tasks/v1/users/@me/lists: (403) Serving Limit Exceeded' 
	 * It's better to store the primary task list id, besso
	 */
	private function getGoogleListTaskLists() {
		$listTaskLists = $this->tasklists->listTasklists();
		$this->listTaskList = $listTaskLists['items'][0];
		$listTaskListID = $this->listTaskList['id'];
		if ('' == $listTaskListID) {
			return false;
		} else {
			// store in DB
			$this->setTaskListID($listTaskListID);
		}
		return $listTaskListID;
	}

	/* it's called when jetuser insert task 
	 * @param : $task of jetstream task 
	    [db_table] => Event
	    [ModifyTime] => 2014-04-02 14:25:38.000
	    [CompanyID] => 508
	    [AccountID] => 467553
	    [EventTypeID] => 1521
	    [RepeatType] => 0
	    [StartDateUTC] => Apr 2 2014 12:00AM
	    [EndDateUTC] => Apr 2 2014 11:59PM
	    [PriorityID] => 
	    [Subject] => sub
	    [Location] => loc
	    [Private] => 0
	    [RepeatInterval] => a:0:{}
	    [EventID] => 690095
	 */
	/* TODO
	 * Note is not in scope yet for insert. complicated, besso
	 */
	public function insertGoogleTask($jetstreamTask) {


		$jTask = $jetstreamTask->GetDataset();
		$googleIdnew = $this->getGoogleTaskId($jTask['EventID']);

		$taskId = $jTask['EventID'];
		
		$googleId = (isset($googleIdnew)) ? $googleIdnew : '' ;
		$clientName = '';	
		/*Start JET-37 For task speed Problem */
		if(isset($_SESSION['USER']['CONTACTID'])){
		$jTask['ContactID'] = $_SESSION['USER']['CONTACTID'];
		unset($_SESSION['USER']['CONTACTID']);
		}	
		/*End JET-37 For task speed Problem */
		
		if (isset($jTask['ContactID'])) {		
			$contact = ContactUtil::getContact($jTask['ContactID']);
			/*Start JET-37 */
			 $companyName = AccountUtil::getAccountName($contact['AccountID']);
   			 $companyName = strtoupper($companyName);
			$clientName = $contact['Text01'] . ' ' . $contact['Text02'].' - '. $companyName;
			
			/*End JET-37 */
			 //$companyName = AccountUtil::getAccountName($contact['AccountID']);
            //$companyName = strtoupper($companyName);
			//$clientName = $contact['Text01'] . ' ' . $contact['Text02'].' - '. $companyName;
			//print_r($clientName);die(fnln);
		} else if (isset($jTask['AccountID'])) {		
			$clientName = AccountUtil::getAccountName($jTask['AccountID']);
			//print_r($clientName); die(cn);

		//	$ContactID = EventUtil::getContact($jTask['EventID']);
		//	print_r($ContactID);die(hfg);
		
		//	@$contactName = $contact['Text01'] . ' ' . $contact['Text02'];

		}		

		if ('' != $clientName ) {
			$clientName = ' ('.$clientName.')';
		}

		$eventTypeName = EventUtil::getEventTypeName($jTask['EventTypeID']);

		$dueserver = new DateTime($jTask['StartDateUTC']);
		$duelocal = new DateTime($jTask['StartDateUTC']);

		$tz = new DateTimeZone($this->user_timezone);
		$duelocal->setTimezone($tz);
		$duelocal = $duelocal->format('Y-m-d H:i:s');
		
		$tk = new DateTimeZone('UTC');
		$dueserver->setTimezone($tk);
		$dueserver = $dueserver->format('Y-m-d H:i:s');
		
		$duelocal = new DateTime($duelocal);
		$dueserver = new DateTime($dueserver);
		
		$interval = $duelocal->diff($dueserver);
		
		$hour	=		$interval->h;
		$min = $interval->i;
		$sec   = 	$interval->s;

		$updated = new DateTime($jTask['ModifyTime']);
		$title = $jTask['Subject'] . $clientName;
		$notes = "Location : " . $jTask['Location'] . "\n";
		$notes.= "Event Type : " . $eventTypeName;
		$due = new DateTime($jTask['StartDateUTC']);
		

		$due->setTime(0, 0);
	
		 $due->add(new DateInterval('PT' . $hour . 'H' . $min . 'M' .$sec. 'S'));
		 $due = $due->format(DATE_RFC3339);
		/* Google task 
		 * https://developers.google.com/google-apps/tasks/v1/reference/tasks/insert
		 */
		/*
		$postBody = array(
  			//"kind" => "tasks#task",
			//"id" => string,
			//"etag" => etag,
			"title" => $title,
			"updated" => $updated,
			//"selfLink" => string,
			//"parent" => string,
			//"position" => string,
			"notes" => $note,
			//"status" => string,
			"due" => $due,
			//"completed" => datetime,
			//"deleted" => boolean,
			//"hidden" => boolean,
			//"type" => string,
			"description" => $description
		);
		*/

		if ('' == $googleId) {
			$newTask = new Google_Task();
			$newTask->setTitle($title);
			$newTask->setNotes($notes);
			$newTask->setDue($due);
			// TODO setDue https://code.google.com/p/google-api-php-client/issues/detail?id=191
			$newTask->setDue($due);
			$newTask->setUpdated($updated);
			$createdTask = $this->tasks->insert( $this->listTaskListID, $newTask );
			$eventId = $jTask['EventID'];	
			$createdGoogleId = $createdTask['id'];
			//exit('11');
			$this->updateGoogleTaskId($eventId, $createdGoogleId);
		} else {	// update
			$existTask = $this->tasks->get($this->listTaskListID, $googleId);
			$existTask = new Google_Task($existTask);
			$existTask->setTitle($title);
			$existTask->setNotes($notes);
			$existTask->setDue($due);
			$existTask->setUpdated($updated);

			$createdTask = $this->tasks->update( $this->listTaskListID, $googleId, $existTask );
			/*Start JET-37 For edit*/
            $sql = "Update Event set googleStatus = '1' Where googleId = '$googleId'";
            DbConnManager::GetDb('mpower')->Exec($sql);
            /*End JET-37 For edit*/


		}

		return true;
	}
	/*Start jet-37*/
private function getGoogleTaskId($id) {
        $sql = 'Select TOP 1 googleId from event
                Where EventID = ?';
        $params = array();
        $params[] = array(DTYPE_INT, $id);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aEvent = DbConnManager::GetDb('mpower')->Exec($sql);

        if (count($aEvent) > 0) {
            return $aEvent[0]['googleId'];
        }
        return false;
    }
    /*End jet-37*/
	private function updateGoogleTaskId($eventId, $googleTaskId) {

                if (empty($googleTaskId)) {
                        return false;
                }

                $params = array();
                // save the record
                $sql = 'Update Event set googleId = ?,googleStatus = ?
			 Where eventId = ?';
                $params[] = array(DTYPE_STRING, $googleTaskId);
                $params[] = array(DTYPE_INT, 1);
                $params[] = array(DTYPE_INT, $eventId);

		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);

                $status = DbConnManager::GetDb('mpower')->Exec($sql);

                if (!$status) {
                        return false;
                }
		return true;
	}

	public function setCompletedGoogleTask($googleId) {
		$existTask = $this->tasks->get($this->listTaskListID, $googleId);
                $existTask = new Google_Task($existTask);
                $existTask->setStatus('completed');
		/*
		$updated = date(DATE_RFC3339);
		$updated = new DateTime($jTask['ModifyTime']);
		$existTask->setUpdated(date(DATE_RFC3339));
		*/
                $createdTask = $this->tasks->update( $this->listTaskListID, $googleId, $existTask );
		return true;
	}

	public function setClosedGoogleTask($googleId) {
		//$existTask = $this->tasks->get($this->listTaskListID, $googleId);
                //$existTask = new Google_Task($existTask);
                $createdTask = $this->tasks->delete( $this->listTaskListID, $googleId );
		return true;
	}

	/* TEMP : Data from Google
	    [kind] => tasks#task
            [id] => MTE0NzAwNTAyMjcyOTQ5Mzk2MTI6MDoxMTkxMDA5NDE4
            [etag] => "AUaoCDpATbnuxaYAcC1ZMGf9bys/MTMwNDcxMTg2Nw"
            [title] => task1
            [updated] => 2014-03-19T17:51:15.000Z
            [selfLink] => https://www.googleapis.com/tasks/v1/lists/MTE0NzAwNTAyMjcyOTQ5Mzk2MTI6MDow/tasks/MTE0NzAwNTAyMjcyOTQ5Mzk2MTI6MDoxMTkxMDA5NDE4
            [position] => 00000000002147483647
            [status] => needsAction
            [due] => 2014-03-19T00:00:00.000Z
	 */
	/*
	 * @param $options Google optionParams 
	 * @return @taskLists
	 */
	public function getGoogleTaskLists($options = array()) {

		// No listTaskListID
		//$this->getGoogleListTaskLists();
		if ('' == $this->listTaskListID)  return false;
        //echo '<pre>'; print_r($this->listTaskListID); echo '</pre>';

		$taskLists = $this->tasks->listTasks($this->listTaskListID, $options);
        //echo '<pre>'; print_r($taskLists); echo '</pre>'; exit;

		if (!isset($taskLists['items']))	return false;
		$taskLists = $taskLists['items'];
        //echo '<pre>'; print_r($taskLists); echo '</pre>'; exit;
		return $taskLists;

	}

	/*
	 * it's called jetstream page is loaded to pull all tasks from Google
	 * TODO. Now only rule is DUE of task. Then updated date could be another rule, besso
	 */
	protected function syncTask() {

		$utc_timezone = new DateTimeZone('UTC');
		$jetDate = new JetDateTime($this->taskProcessed, $utc_timezone);
		$maxDate = $jetDate->addDate( GOOGLE_TO_DATE, $this->taskProcessed, GOOGLE_DATE_RFC3339 );

        $minDate = GoogleConnect::getTodayWithGoogleFormat('Ymd');
        //echo '<pre>'; print_r($minDate); echo '</pre>'; exit;
        //echo '<pre>'; print_r($maxDate); echo '</pre>'; exit;

		$options = array(
			'dueMin' => $minDate,
			'dueMax' => $maxDate
		);
		$taskLists = $this->getGoogleTaskLists($options);
        //echo '<pre>'; print_r($taskLists); echo '</pre>'; exit;

		if (empty($taskLists)) $taskLists = array();
		if ( count($taskLists) > 0 ) {
			foreach ($taskLists as $googleTask) {
				$this->insertJetTask($googleTask);
			}
		}

		/* TODO
		 * we should loop all jetstream tasks to delete all deleted google task
		 * This could be load , let's talk. besso
		 */
		$this->deleteJetTasks($taskLists);

		$this->setProcessed($today);
		return true;
	}

    private function setProcessed($today = '') {
        if ('' == $today) {
            $today = date("YmdHis");
            $utc_timezone = new DateTimeZone('UTC');
            $jetDate = new JetDateTime($today, $utc_timezone);
            $today = $jetDate->asMsSql('g');
        }

        // save the record
        $sql = 'Update GoogleToken
            Set task_processed = ?
            Where PersonID = ?';

        $params[] = array(DTYPE_STRING, $today);
        $params[] = array(DTYPE_INT, $this->user);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $status = DbConnManager::GetDb('mpower')->Exec($sql);

        if (!$status) {
            return false;
        }
    }

    private function deleteJetTasks($googleLists) {
        $jetLists = $this->getJetstreamTasks();
        //array_push($jetLists,array('id'=>'besso'));
        $googleCount = count($googleLists);
        $jetCount = count($jetLists);

        /*
        echo '<pre>'; print_r($googleCount); echo '</pre>';
        echo '<pre>'; print_r($jetCount); echo '</pre>'; exit;
        */

        if ($googleCount != $jetCount) {
            $gLists = array();
            foreach ($googleLists as $googleList ) {
                $gLists[] = $googleList['id'];
            }

			$jLists = array();
            foreach ($jetLists as $jetList) {
                $jLists[] = $jetList['id'];
            }

            $aDiff = array_diff($jLists,$gLists);
            if (count($aDiff) == ($jetCount - $googleCount)) {
                $this->closeJetTasks($aDiff);
            }
            return false;
        }
    }

        private function closeJetTasks($aTask) {
                foreach($aTask as $googleId) {
                        $sql = 'Update Event set Cancelled = 1, Closed = 1 where googleId = ?';
                        $params = array();
                        $params[] = array(DTYPE_STRING, (string)$googleId);
                        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
                        $status = DbConnManager::GetDb('mpower')->Exec($sql);
                }
                return true;
        }

        /* The date condition should be 00:00:00 of today */
        private function getJetstreamTasks() {
            $userID = $this->user;
            $sql = 'SELECT E.googleId as id FROM Event E
                      JOIN EventAttendee EA on EA.eventId = E.eventId
                     WHERE EA.personId = ?
                       AND EA.Creator = 1
                       AND E.Closed IS NULL
                       AND E.StartDateUTC >= ?';
            $params[] = array(DTYPE_INT, $userID);
            $date = GoogleConnect::getTodayWithJetstreamFormat("Ymd");
            $params[] = array(DTYPE_STRING, $date);
            $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
            $aJetstreamTask = DbConnManager::GetDb('mpower')->Exec($sql);
            return $aJetstreamTask;
        }

	private function existGoogleTask($googleId) {
		$sql = 'select EventID from Event where googleId = ?';
                $params[] = array(DTYPE_STRING, $googleId);
                $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
                $aEvent = DbConnManager::GetDb('mpower')->Exec($sql);

                if (count($aEvent) > 0) {
			return $aEvent[0]['EventID'];
		}
		return false;
	}

	/* used just update Duedate and status */
	private function getJetstreamTask( $eventId ) {
		$sql = 'select * from Event where eventId = ?';
                $params[] = array(DTYPE_INT, $eventId);
                $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
                $aEvent = DbConnManager::GetDb('mpower')->Exec($sql);

                if (count($aEvent) > 0) {
			return $aEvent[0];
		}
		return false;
	}

	/*
	 * only update Duedate and completed both
	 * the other stuff could screw existing Jetstream format
	 */
	private function updateJetTask($eventId, $googleTask ) {

		/* we dont need to get. Just update google setting
		$jetTask = $this->getJetstreamTask($eventId);
		$startDate = $jetTask['StartDateUTC'];
		$startDate = GoogleConnect::convertGoogleDateFormat($startDate);
		$status = $jetTask['status'];
		error_log("Start Date of $eventId : $startDate");
		error_log("Status of $eventId : $status");
		*/

		$googleDue = $googleTask['due'];
		$googleStatus = $googleTask['status'];
		$closed = ($googleStatus == GOOGLE_TASK_STATUS_COMPLETED) ? 1 : NULL;
		$sql = 'Update Event Set StartDateUTC = ?,
					 EndDateUTC = ?,
					 Closed = ?
			 Where EventID = ?';

        $params[] = array(DTYPE_STRING, $googleDue);
        $params[] = array(DTYPE_STRING, $googleDue);
        $params[] = array(DTYPE_INT, $closed);
        $params[] = array(DTYPE_INT, $eventId);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $status = DbConnManager::GetDb('mpower')->Exec($sql);

        if (!$status) {
                return false;
        }
        return true;

	}

	private function insertJetTask( $googleTask ) {

		$googleId = $googleTask['id'];
		$eventId = $this->existGoogleTask($googleId);
		$eventId = ($eventId > 0) ? $eventId : '';

        //echo $eventId; exit;
		
		if ($eventId > 0) {
			// TODO. it call update, lookup exist Edit process
			$this->updateJetTask($eventId, $googleTask);
			return false;
		}
        //echo '<pre>'; print_r($googleTask); echo '</pre>'; exit;

		$subject = $googleTask['title'];
		$updated = $googleTask['updated'];
		$status = $googleTask['status'];
		$due = (isset($googleTask['due'])) ? $googleTask['due'] : '' ;
		$notes = ($googleTask['notes'] != '') ? $googleTask['notes'] : '';
		
		/* 
         * TODO. We can't sync everythingbecause both structure be quite different, No update via Google Pull, besso
         * Note is not scope
		if ($eventId > 0) {
			$notes = '';
		}
		*/

		if ( '' == $subject || '' == $due ) {
			return false;
		}

        //echo '<pre>'; print_r($googleId); echo '</pre>';
        //echo '<pre>'; print_r($due); echo '</pre>'; exit;
		$oEventNew = new ModuleEventNew($this->companyId, $this->user, $this->user_timezone);
		$attendee_list = json_encode(array($this->user));
		$closed = ($status == 'needsAction') ? '' : 1;

		$req = array(
			'EventID' => $eventId, 
			'AccountID' => '',
			'ContactID' => $this->user, 
			'DealID' => '', 
			'Contact' => '', 
			'Subject' => $subject,
			'Location' => '',
			'EventType' => GOOGLE_TASK_TYPE_IN_JETSTREAM,
			'StartDate' => $due,
			'StartTime' => '', 
			'EndTime' => '', 
			'EndDate' => $due,
			'Repeat' => 0,
			'attendee_list' => $attendee_list,
			'CheckPrivate' => '', 
			'RepeatParent' => '', 
			'OrigStartDate' => '', 
			'Note' => $notes, 
			'priority' => '', 
			'add_event_alert_hidden' => '', 
			'email_event_alert' => '', 
			'standard_event_alert' => '', 
			'send_event_immediately' => '', 
			'delay_event_until' => '', 
			'event_hours' => '', 
			'event_minutes' => '', 
			'event_periods' => '', 
			'alert_users' => '', 
			'googleId' => $googleId,
			'ModifyDate' => updated,
			'Closed' => $closed,
			'repeat_detail' => array()
		);

        //echo '<pre>'; print_r($req); echo '</pre>'; exit;
		$status = $oEventNew->SaveForm($req, false, 'dashboard', false, true);
        /* TODO error handling */

		return true;
		
	}

	/*
	 * if sync is true, sync all google side tasks into Jetstrem DB
	 * it's batch task and we run it only One Account is created ar at project-first
	 * TODO this could be handled in other point, besso 
	 */
	public function syncTaskLists() {
		if (true == $this->sync) {
			if (!$this->syncTaskListsFromGoolgle()) {
				return false;
			}
			if (!$this->syncTaskListsFromJetstream()) {
				return false;
			}
		}
		return true;
	}

	/*
	 * Loop google task lists and sync with Jetstream Tasks
	 */
	private function getTaskListsFromGoolgle() {

		$this->googleTaskLists = $this->getGoogleTaskLists();	

		if (count($this->googleTaskLists) == 0)  return false;
		
		foreach ($this->googleTaskLists as $googleTaskList) {
			if (!$this->syncTaskFromGoogle($googleTaskList)) {
				continue;
			}
		}
		return true;
	}

	// not using
	private function syncTaskFromGoogle($task) {

		$due = $task['due'];

		/* Google default status : needsAction
		 * closed status : completed
		 */
		$status = $task['status'];

		$format = 'Ymd his Z';
		$googleStartDate = new DateTime($due);
		$googleStartDate = substr($googleStartDate->format($format),0,8);

		// we don't need to sync any closed task
		if ( false == $this->syncClosedTask && $status == 'completed' ) {
			return false;
		}

		$jetstreamTaskLists = $this->getJetstreamTaskLists();
		foreach ($jetstreamTaskLists as $jetTask) {
			
			$jetStartDate = $this->getUTCDate($jetTask['StartDate'], $jetTask['timezone'], $format );
			$jetStartDate = substr($jetStartDate,0,8);

			// google task date is same with jet task
			/* TODO what's the condition both are same */
			//$task['title'] == $jetTask['Subject']
			if ( $jetStartDate == $googleStartDate ) {

				// already it's synced google id then
				if ('' == $jetTask['googleId']) {
					//$this->insertJetTask($task);
					//$this->insertGoogleTask($jetTask);
				} else {
					if ($task['id'] != $jetTask['googleId']) {
						//$this->updateJetTask($task,$jetTask);
					}
					
				}
			} else {

			}
		}		

		return true;
	}

	public function getUTCDate($date, $timezone, $format = 'Ymd his Z') {
		$userTimezone = new DateTimeZone($timezone);
		$userDate = new DateTime($date,$userTimezone);
		//error_log('User Date : ' . $userDate->format($format));
		$utcTimezone = new DateTimeZone('UTC');
		$userDate->setTimeZone($utcTimezone);
		return $userDate->format($format);
	}

	private function syncTaskListsFromJetstream() {

		return true;
	}

	// not using
    private function getJetstreamTaskLists () {
                $account_name = $_SESSION['account_name'];
                $contact_first_name = $_SESSION['contact_first_name'];
                $contact_last_name = $_SESSION['contact_last_name'];
                $param = array();

                $sql = "SELECT E.EventID, E.UserID, E.Subject, CONVERT(varchar, E.StartDate, 101) AS StartDate, CONVERT(varchar, E.DateModified, 101) AS DateModified,
                C.ContactID, A.AccountID,A.$account_name as Company, ET.EventColor, C.$contact_first_name AS FirstName,
                C.$contact_last_name AS LastName, ET.EventName , ET.EventTypeID,
                P.Color as PersonColor, E.IsClosed,Pending=NULL,
		tz.zname as timezone, E.googleId 
		FROM Events E
                LEFT JOIN EventType ET on E.EventTypeID=ET.EventTypeID
                LEFT JOIN Contact C ON C.ContactID = E.ContactID
                LEFT JOIN Account A ON A.AccountID = C.AccountID
                LEFT JOIN People P ON P.PersonID=E.UserID
		JOIN timezones tz ON P.timezone = tz.zid
                where ET.Type='TASK' AND E.IsCancelled=0 AND P.CompanyID=A.CompanyID
		  and E.IsClosed = 0 AND E.UserID= ? ";

                $param[] = array(DTYPE_INT, $this->user);
                $sql_build = SqlBuilder()->LoadSql($sql);
                $sql = $sql_build->BuildSqlParam($param);
                $tasks = DbConnManager::GetDb('mpower')->Exec($sql);
                return $tasks;
        }

} 
