<?PHP
/**
 * Class for writing the log information to a log file. 
 */
class Log
{
	public $debug = false;
	private $log_file = '/webtemp/logs/EmailServiceLog';

	function __construct() {
	}
		
	/**
	 * writes to log file
	 * @param message string
	 */
	private function write($msg, $type='Info') {
		if (self::debug) {			
			$time = date('m-d-y H:i:s', strtotime('now'));
			file_put_contents($this->log_file, $time.' '.$type.' '.$msg."\n\n", FILE_APPEND);
		}
	}
}
