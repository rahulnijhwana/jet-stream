<?php
class DebugUtil{
	
	public static function show($array, $label=false){

		echo $label ? ('label: ' . $label . '<br>') : '';
		echo 'count: ' . count($array);
		echo '<pre>';
		print_r($array);
		echo '</pre>';
	
	}
}