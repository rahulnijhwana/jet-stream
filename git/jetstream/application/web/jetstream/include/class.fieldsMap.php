<?php
/**
 * class.fieldsMap.php - contain the fieldMap class
 * @package database
 * include array class
 */
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/include/lib.array.php';

class fieldsMap
{

	public $accountFields;
	public $contactFields;
	public $leftLables;
	public $rightLables;
	public $leftContactLables;
	public $rightContactLables;
	public $contactLables;
	public $leftAccountLables;
	public $rightAccountLables;
	public $accountLables;
	public $companyId;

	/**
	 * function for getting the account map fields for a company from db
	 * return array
	 */
	public function getAccountMapFieldsFromDB() {
		$accountMap = NULL;
		
		if(isset($this->companyId) && ($this->companyId > 0)) {		
			$sql = 'SELECT AM.AccountMapID, AM.FieldName, AM.LabelName, AM.IsCompanyName, AM.BasicSearch, AM.IsRequired,
				AM.ValidationType, AM.Position, AM.Align, AM.Enable, AM.IsPrivate, AM.SectionID, S.AccountSectionName AS SectionName 
				FROM AccountMap AM
				LEFT JOIN Section S ON AM.SectionID = S.SectionID
				WHERE AM.CompanyID= ? AND AM.Enable=1 ORDER BY AM.SectionID';
			$accountSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->companyId));
			$accountMap = DbConnManager::GetDb('mpower')->Exec($accountSql);	
		}
		return $accountMap;
	}
	
	/**
	 * function for getting the contact map fields for a company from db
	 * return array
	 */
	public function getContactMapFieldsFromDB() {
		$contactMap = NULL;
		
		if(isset($this->companyId) && ($this->companyId > 0)) {
			$sql = 'SELECT CM.ContactMapID, CM.FieldName, CM.LabelName, CM.BasicSearch, CM.IsRequired, CM.IsFirstName, CM.IsLastName, CM.ValidationType,
				CM.Position, CM.Align, CM.Enable, CM.SectionID, CM.IsPrivate, S.ContactSectionName AS SectionName 
				FROM ContactMap CM
				LEFT JOIN Section S ON CM.SectionID = S.SectionID
				WHERE CM.CompanyID= ? AND CM.Enable=1 ORDER BY CM.SectionID';
			$contactSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->companyId));
			$contactMap = DbConnManager::GetDb('mpower')->Exec($contactSql);
		}
		return $contactMap;
	}
	
	/**
	 * function for getting the account map fields from session
	 * return array
	 */
	public function getAccountFields() {
		$this->accountFields = $_SESSION['USER']['ACCOUNTMAP'];
		return $this->accountFields;
	}

	/**
	 * function for getting the contact map fields from session
	 * return array
	 */
	public function getContactFields() {
		$this->accountFields = $_SESSION['USER']['CONTACTMAP'];
		return $this->accountFields;
	}

	/**
	 * function for getting the account layout
	 * return array with leftPanel array and rightPanel array for account
	 */
	public function getAccountLayout() {
		$accountFields = $this->getAccountFields();
		$this->leftLables = array();
		$this->rightLables = array();

		foreach ($accountFields as $key => $value) {
			if ($accountFields[$key]['Align'] == 1) {
				$this->leftLables[] = array('AccountMapID' => $accountFields[$key]['AccountMapID'], 'FieldName' => $accountFields[$key]['FieldName'], 'LabelName' => $accountFields[$key]['LabelName'], 'IsRequired' => $accountFields[$key]['IsRequired'], 'ValidationType' => $accountFields[$key]['ValidationType'], 'Position' => $accountFields[$key]['Position']);
			} else {
				$this->rightLables[] = array('AccountMapID' => $accountFields[$key]['AccountMapID'], 'FieldName' => $accountFields[$key]['FieldName'], 'LabelName' => $accountFields[$key]['LabelName'], 'IsRequired' => $accountFields[$key]['IsRequired'], 'ValidationType' => $accountFields[$key]['ValidationType'], 'Position' => $accountFields[$key]['Position']);
			}
		}

		$this->leftAccountLables = ArraySort($this->leftLables, 'Position');
		$this->rightAccountLables = ArraySort($this->rightLables, 'Position');
		$this->accountLables = array('leftPanel' => $this->leftAccountLables, 'rightPanel' => $this->rightAccountLables);
		return $this->accountLables;
	}

	/**
	 * function for getting the contact layout
	 * return array with leftPanel array and rightPanel array for contact
	 */
	public function getContactLayout() {
		$contactFields = $this->getContactFields();
		$this->leftLables = array();
		$this->rightLables = array();

		foreach ($contactFields as $key => $value) {
			if ($contactFields[$key]['Align'] == 1) {
				$this->leftLables[] = array('ContactMapID' => $contactFields[$key]['ContactMapID'], 'FieldName' => $contactFields[$key]['FieldName'], 'LabelName' => $contactFields[$key]['LabelName'], 'IsRequired' => $contactFields[$key]['IsRequired'], 'ValidationType' => $contactFields[$key]['ValidationType'], 'Position' => $contactFields[$key]['Position']);
			} else {
				$this->rightLables[] = array('ContactMapID' => $contactFields[$key]['ContactMapID'], 'FieldName' => $contactFields[$key]['FieldName'], 'LabelName' => $contactFields[$key]['LabelName'], 'IsRequired' => $contactFields[$key]['IsRequired'], 'ValidationType' => $contactFields[$key]['ValidationType'], 'Position' => $contactFields[$key]['Position']);
			}
		}

		$this->leftContactLables = ArraySort($this->leftLables, 'Position');
		$this->rightContactLables = ArraySort($this->rightLables, 'Position');
		$this->contactLables = array('leftPanel' => $this->leftContactLables, 'rightPanel' => $this->rightContactLables);
		return $this->contactLables;
	}

	/**
	 * function for getting account name field
	 * return account name field
	 */
	public function getAccountNameField() {
		$accountFields = $this->getAccountFields();

		foreach ($accountFields as $key => $value) {
			if ($accountFields[$key]['IsCompanyName'] == 1) {
				$NameField = $accountFields[$key]['FieldName'];
			}
		}
		return $NameField;

	}

	/**
	 * function for getting contact first name field
	 * return contact first name field
	 */
	public function getContactFirstNameField() {
		$contactFields = $this->getContactFields();

		foreach ($contactFields as $key => $value) {
			if ($contactFields[$key]['IsFirstName'] == 1) {
				$FirstNameField = $contactFields[$key]['FieldName'];
			}
		}
		return $FirstNameField;
	}

	/**
	 * function for getting contact last name field
	 * return contact first name field
	 */
	public function getContactLastNameField() {
		$contactFields = $this->getContactFields();

		foreach ($contactFields as $key => $value) {
			if ($contactFields[$key]['IsLastName'] == 1) {
				$LastNameField = $contactFields[$key]['FieldName'];
			}
		}
		return $LastNameField;
	}

	/** GMS 9/12/2010
	 * function for getting contact title field
	 * return contact title field
	 */
	public function getContactTitleField() {
		$contactFields = $this->getContactFields();
		foreach ($contactFields as $key => $value) {
			if ($contactFields[$key]['IsContactTitle'] == 1) {
				$ContactTitleField = $contactFields[$key]['FieldName'];
			}
		}
		return $ContactTitleField;
	}

	public function parseContactFields() {
		$contactFields = $this->getContactFields();
		$newContactArray = array();
		//print_r($contactFields);
		foreach ($contactFields as $contact) {
			$newContactArray[$contact['FieldName']]['Text'] = $contact['LabelName'];
			$newContactArray[$contact['FieldName']]['ID'] = $contact['ContactMapID'];
			$newContactArray[$contact['FieldName']]['Section'] = $contact['SectionName'];
		}
		return $newContactArray;
	}

	public function parseAccountFields() {
		$accountFields = $this->getAccountFields();
		$newAccountArray = array();

		foreach ($accountFields as $account) {
			$newAccountArray[$account['FieldName']]['Text'] = $account['LabelName'];
			$newAccountArray[$account['FieldName']]['ID'] = $account['AccountMapID'];
			$newAccountArray[$account['FieldName']]['Section'] = $account['SectionName'];
		}
		return $newAccountArray;
	}

	/**
	 * function to get the contact email fields
	 *
	 */
	public function getContactEmailFields($is_simple = true) {
		$contactFields = $this->getContactFields();

		$emailFields = array();
		foreach ($contactFields as $key => $value) {
			if ($contactFields[$key]['ValidationType'] == 5 && $contactFields[$key]['Enable'] == 1) {
				//$emailFields[$key]['FieldName'] = $contactFields[$key]['FieldName'];
				//print_r($contactFields[$key]);
				if ($is_simple) {
					$emailFields[] = $contactFields[$key]['FieldName'];
				} else {
					$emailFields[$contactFields[$key]['FieldName']] = $contactFields[$key]['LabelName'];
				}
			}
		}
		return $emailFields;
	}

}
?>