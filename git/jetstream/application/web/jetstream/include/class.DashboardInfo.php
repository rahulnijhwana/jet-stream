<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.MpRecord.php';

/**
 * The DB fields initialization
 */
class DashboardInfo extends MpRecord
{

	public function Initialize() {
		$this->db_table = 'DashboardSearch';		
		$this->recordset->GetFieldDef('DashboardSearchID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('CompanyID')->required = TRUE;
		$this->recordset->GetFieldDef('RecType')->required = TRUE;
		$this->recordset->GetFieldDef('RecID')->required = TRUE;
		
		parent::Initialize();
	}
	
	public function Save($simulate = false) {
		parent::Save($simulate);
	}
}
?>