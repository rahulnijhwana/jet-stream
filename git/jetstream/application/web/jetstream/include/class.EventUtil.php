<?php
require_once 'class.DbConnManager.php';
require_once 'class.SqlBuilder.php';

class EventUtil {
	
	public static function getEventContact($eventId){
		$sql = "
			SELECT EventID, Event.AccountID, Text01, Text02
			FROM Event JOIN Contact ON Event.ContactID = Contact.ContactID
			WHERE EventID = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $eventId));
		
		$contact = DbConnManager::GetDb('mpower')->GetOne($sql);
		
		return $contact;
	}
	
	public static function getEventTypeName($event_type_id){
		$sql = "SELECT EventName FROM EventType WHERE EventTypeID = ?";
		$param = array(DTYPE_INT, $event_type_id);
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql($param);
		$result = DbConnManager::GetDb('mpower')->GetOne($sql);	
		
		return $result['EventName'];
	}
}
