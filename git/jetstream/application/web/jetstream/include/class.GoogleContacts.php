<?php
require_once 'constants.php';
require_once JETSTREAM_ROOT . '/../include/class.GoogleConnect.php';
require_once JETSTREAM_ROOT . '/../slipstream/class.Dashboard.php';
require_once JETSTREAM_ROOT . '/../slipstream/class.ModuleCompanyContactNew.php';
require_once JETSTREAM_ROOT . '/../slipstream/Inspekt.php';
require_once JETSTREAM_ROOT . '/../include/class.MapLookup.php';
require_once JETSTREAM_ROOT . '/../slipstream/class.CompanyContactNew.php';
require_once JETSTREAM_ROOT . '/../include/class.GoogleContactBatchPush.php';

class GoogleContacts {

    private $contactList = array();
    private $contactUserEmail = '';
    private $contacts;

    private $contactProcessed;
    private $contactGroupID;
    private $userid;
    private $companyId;

    private $debug = false;

    private $maps;
    private $aMapWithGoogle;

    public function __construct($oContact, $user, $sync=true) {

        if ( $_GET['debug'] == true ) {
            $this->debug = true;
        }

        if ( $this->debug ) {
            error_log('initialize class GoogleContacts');
        }

        $this->userid = $user;
        $this->companyId = GoogleConnect::getCompanyId($user);
        $this->contacts = $oContact;

        $this->aMapWithGoogle = unserialize( CONTACT_MAP );
        $this->getMapColumns();

        //$this->setProcessed();

        /* contact group id is the ID of name 'Jeststream' at google side
         * to lessen google request, store in googleToken table
         */
         //var_dump($this->contactGroupID);exit;
        if ('' == $this->contactGroupID) {
            $this->contactGroupID = $this->getContactGroup();
            //echo $this->contactGroupID; exit;
            if ( $this->contactGroupID == '' ) {
                error_log( 'Google contact id "Jetstream" is not found' );
                return false;
            }
        }

        $this->setContactEmail();

        if ( $sync ) {
            if ( $this->hasProcessed() == false ) {
                $this->getProcessed();
                $this->syncContact();
            } else {
                $this->getProcessed();
                if ( $this->decideSync() ) {
                    $this->syncContact();
                } else {
                    error_log('sync contacts is not run for interval');
                }
            }
        }

        $date = $this->contactProcessed;
        $date = str_replace(':000','',$date);
        $date = str_replace('.000','',$date);
        $this->contactProcessed = $date;
    }


    private function decideSync() {

        if ( $this->contactProcessed == '' ) {
            return false;
        }

        $pTS = strtotime($this->contactProcessed);
        $nTS = strtotime(GoogleConnect::getTodayWithGoogleFormat());
        $diff = $nTS - $pTS;

        /*
        echo '<pre>'; print_r($diff); echo '</pre>';
        echo '<pre>'; print_r($this->contactProcessed); echo '</pre>';
        echo '<pre>'; print_r(GoogleConnect::getTodayWithGoogleFormat()); echo '</pre>';
        exit;
        */

        if ( $diff > GOOGLE_SYNC_INTERVAL ) {
            return true;
        } else {
            return false;
        }
    }

    private function hasProcessed() {
        $sql = 'Select TOP 1 contact_processed from GoogleToken where personId = ?';
        $params[] = array(DTYPE_INT, $this->userid);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aProcessed = DbConnManager::GetDb('mpower')->Exec($sql);
        return ( !empty($aProcessed[0]['contact_processed']) ) ? true : false;
    }

    private function getProcessed() {

        $sql = 'Select TOP 1 contact_processed, contact_group_id from GoogleToken where personId = ?';
        $params[] = array(DTYPE_INT, $this->userid);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aProcessed = DbConnManager::GetDb('mpower')->Exec($sql);
        
        if (count($aProcessed) > 0) {
            $processed = $aProcessed[0]['contact_processed'];
            if ( $processed != '' ) {
                $this->contactProcessed = $processed;
            } else {
                $today = date("YmdHis", strtotime('-1 hour'));
                $utc_timezone = new DateTimeZone('UTC');
                $jetDate = new JetDateTime($today, $utc_timezone);
                $today = $jetDate->asMsSql('g');
                $this->contactProcessed = $today;
                //$this->contactGroupID = ('' != $aProcessed[0]['contact_group_id']) ? $aProcessed[0]['contact_group_id'] : '' ;
                // no contact_processed mean that it's first run 
                //error_log( 'run syncContact as first run' );
                //$this->syncContact();
            }
        }

        return true;
    }

    private function setProcessed($today = '') {
        if ('' == $today) {
            $today = date("YmdHis");
            $utc_timezone = new DateTimeZone('UTC');
            $jetDate = new JetDateTime($today, $utc_timezone);
            $today = $jetDate->asMsSql('g');
        }

        // save the record
        $sql = 'Update GoogleToken
            Set contact_processed = ?
            Where PersonID = ?';

        $params[] = array(DTYPE_STRING, $today);
        $params[] = array(DTYPE_INT, $this->userid);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $status = DbConnManager::GetDb('mpower')->Exec($sql);

        if (!$status) {
            return false;
        }
    }


    /* Too load task so changed to run as ajax
     * deprecated : it's monetized as one ajax call ajax.callGoogleSync.php 
     */
    /*
    public function callSyncContact() {
        $this->syncContact();
        echo json_encode(array('code'=>'200'));
    }
    */

    /* Get Jetstream group from google, if not create new Jetstream one */
    private function getContactGroup() {
		
		$sql = "Select Name from company where CompanyID = ? ";
        $params = array();
        $params[] = array(DTYPE_INT, $this->companyId);
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		$company = DbConnManager::GetDb('mpower')->Exec($sql);
		
        $req = new Google_HttpRequest("https://www.google.com/m8/feeds/groups/default/full");
        // Google Contacts way
        $val = $this->contacts->getIo()->authenticatedRequest($req);

        // The contacts api only returns XML responses.
        $response =$val->getResponseBody();
        $xml = simplexml_load_string($response); // Convert to an ARRAY

        /* get author email and define as contactEmail */
        if ( isset ( $xml->author ) ) {
            $this->contactEmail = $xml->author->email;
        }
        $GroupID = false;
        $aEntry = array();
        
        if ( isset ( $xml->entry ) ) {
            $aEntry = $xml->entry;
            /* check 'Jetstream' group */
            foreach ( $aEntry as $e ) {

              //  if ( 'Jetstream' == $e->title ) {
                if ( $company[0]['Name'] == $e->title ) {
                    /* ID is full url of contact group */
                    /* TODO no store in DB, lookup GoogleToken::contact_group_id */
                    $googleContactId = (string) $e->id;//(string) $xml->code[0]->lat;
                    $sql = 'Update googleToken set contact_group_id = ? Where personId = ? ';
					$params = array();
					$params[] = array(DTYPE_STRING, $googleContactId);
					$params[] = array(DTYPE_INT, $this->userid);
					$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
					$status = DbConnManager::GetDb('mpower')->Exec($sql);
					$GroupID = true;

                    return $e->id;
                    break;
                }
            }
            
        } else {
            return $this->createContactGroup();
        }
        if($GroupID == false){
			return $this->createContactGroup();
			}
    }

    private function createContactGroup() {
		$sql = "Select Name from company where CompanyID = ? ";
        $params = array();
        $params[] = array(DTYPE_INT, $this->companyId);
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		$company = DbConnManager::GetDb('mpower')->Exec($sql);
        $group = "
            <atom:entry xmlns:atom='http://www.w3.org/2005/Atom' xmlns:gd='http://schemas.google.com/g/2005'>
            <atom:category scheme='http://schemas.google.com/g/2005#kind' term='http://schemas.google.com/contact/2008#group'/>
            <atom:title type='text'>{$company[0]['Name']}</atom:title>
            <gd:extendedProperty name='more info about the group'>
            <info>Nice people.</info>
            </gd:extendedProperty>
            </atom:entry>";

        $len = strlen($group);
        $add = new Google_HttpRequest("https://www.google.com/m8/feeds/groups/".$this->contactEmail."/full/");

        //$add = new Google_HttpRequest("https://www.google.com/m8/feeds/groups/default/full/");
        $add->setRequestMethod("POST");
        $add->setPostBody($group);
        $add->setRequestHeaders(array('content-length' => $len, 'GData-Version'=> '3.0','content-type'=>'application/atom+xml; charset=UTF-8; type=feed'));
        $submit = $this->contacts->getIo()->authenticatedRequest($add);
        $sub_response = $submit->getResponseBody();

        $parsed = simplexml_load_string($sub_response);
        $aGroup = explode("base/",$parsed->id);

        $personId = $this->userid;

        $googleContactId = $aGroup[1];
		$urlEmail = urlencode($this->contactEmail);
       // $googleContactId = 'http://www.google.com/m8/feeds/groups/besso%40thinksolid.com/base/' . $googleContactId;
        $googleContactId = 'http://www.google.com/m8/feeds/groups/'.$urlEmail.'/base/' . $googleContactId;
        $sql = 'Update googleToken set contact_group_id = ? Where personId = ? ';
        $params = array();
        $params[] = array(DTYPE_STRING, $googleContactId);
        $params[] = array(DTYPE_INT, $personId);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $status = DbConnManager::GetDb('mpower')->Exec($sql);
        if (!$status) {
            return false;
        } else {
            return $googleContactId;
        }
    }

    private function syncContact() {
        // Get All contact lists
        $googleContacts = $this->getContactsList();

        if ( !$this->controlUpdateClientContacts( $googleContacts ) ) {
            error_log('Problem with updating Jet Contacts on syncing with Google');
            return false;
        }

        $this->setProcessed();
        return true;        
    }

    /* TODO existing code to full push and pull 
     * change to just check for google's only who has updated
     */
    /***
    private function syncContact() {

        // Get All contact lists
        $googleContacts = $this->getContactsList();
        $googleCount = count($googleContacts);
        
        $clientContacts = $this->getJetContacts();
        $clientCount = count($clientContacts);

        if ( !$this->controlUpdateClientContacts( $googleContacts ) ) {
            error_log('Problem with updating Jet Contacts on syncing with Google');
            return false;
        }

        // REVIEW. No update while pulling
        // for update any google change, we need to full loop of every contacts
        // Contact is always full scan so for pulling it's not proper 
        if ( $googleCount != $clientCount ) {
            // pull from Google
            if ( $googleCount > $clientCount ) {
                // TODO Blocker google contact response body return only title
                foreach ( $googleContacts as $g ) {
                    $gid = $g->id->{'$t'};
                    $aGid = explode('/',$gid);
                    $gid = $aGid[count($aGid)-1];

                    // check contactName
                    $contactName = $g->title->{'$t'};
                    
                    $isAlready = false;
                    foreach ( $clientContacts as $c ) {
                        if ( $c['googleContactId'] == $gid ) {
                            $isAlready = true;
                            break;
                        }

                        if ( trim($c['ContactName']) == trim($contactName) ) {
                            $isAlready = true;
                            break;
                        }

                    }

                    if ( $isAlready == true ) {
                        error_log('Google task id aleady exist : ' . $gid );
                        continue;
                    } else {
                        $this->insertClientContact($gid, $g);
                    }

                }
            } else {
                foreach ( $clientContacts as $c ) {
                    // Jetstream contact has not googleId
                    if ( '' == $c['googleContactId'] ) {
                        $contactId = $c['ContactID'];
                        $sql = "Select * from Contact Where ContactID = ?";
                        $params = array();
                        $params[] = array(DTYPE_INT, $contactId);
                        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
                        $aContact = DbConnManager::GetDb('mpower')->Exec($sql);
                        $rec = $aContact[0];
                        $this->insertGoogleContacts($rec, $contactId);
                    }
                }
            }
        }

        // TODO need error handling, besso
        // TODO need to pull from Google sync later, besso
        $today = '';
        $this->setProcessed($today);
        return true;
    }
    ***/

    public function setContactEmail() {
        $url = 'https://www.google.com/m8/feeds/contacts/default/full';
        $req = new Google_HttpRequest($url);
        $req->setRequestHeaders(array('GData-Version'=> '3.0','content-type'=>'application/atom+xml; charset=UTF-8; type=feed'));
        // Google Contacts way
        $val = $this->contacts->getIo()->authenticatedRequest($req);

        // The contacts api only returns XML responses.
        $response =$val->getResponseBody();
        $xml = simplexml_load_string($response); // Convert to an ARRAY
        $this->contactUserEmail = urlencode($xml->id); // email address
        unset($xml);
    }

    /* TODO
     * change to get only changed 
     *
     */
    public function getContactsList() {

        $groupUrl = $this->contactGroupID;

        //updated-min=2007-03-16T00:00:00
        if ( $this->contactProcessed != '' ) {
            $updatedMin = GoogleConnect::convertGoogleDateFormat($this->contactProcessed);
        }
        if ( $updatedMin == '' ) {
            $updatedMin = GoogleConnect::getTodayWithGoogleFormat();
        }

        $url = 'https://www.google.com/m8/feeds/contacts/default/full?alt=json&updated-min='. $updatedMin .'&group='. $groupUrl ;
        //$url = 'https://www.google.com/m8/feeds/contacts/default/full?alt=json&group='. $groupUrl ;

        $req = new Google_HttpRequest($url);
        $req->setRequestHeaders(array('GData-Version'=> '3.0','content-type'=>'application/atom+xml; charset=UTF-8; type=feed'));
        $val = $this->contacts->getIo()->authenticatedRequest($req);

        // The contacts api only returns XML responses.
        $response = $val->getResponseBody();
        $xml = json_decode($response)->feed;

        $aContact = array();
        if ( isset ( $xml->entry ) ) {
            $aContact = $xml->entry;
        }
        unset($xml);

        return $aContact;
    }

    /* TODO
     * we need to create new table to mapping any jetstream contact with google contact
     * because one jetstream id could be share with others
     * or Find out other table for mapping purpose, besso 
     */
    private function getJetContacts() {
        $userid = $this->userid;
        if (!empty($_SESSION['USER']['COMPANYID'])) {
            $companyid = $_SESSION['USER']['COMPANYID'];
        } else {
            $companyid = $this->getCompanyId($userid);
        }

        $search = array(
            'page_no' => '1',
            'records_per_page' => '1000',
            'assigned_search' => 0,
            'query_type' => 2,
            'search_string' => '',
            'inactive_type' => 0,
            'assign' => '',
            'sort_type' => '',
            'Advance' => '',
            'inclContacts' => '',
            'sort_dir' => ''
        );

        $oDashboard = new Dashboard($companyid, $userid);
        $oDashboard->Build($search);
        $aResult = $oDashboard->SearchResults();

        $aResponse = array();
        if ( count($aResult) > 0 ) {
            foreach($aResult as $r) {
                $sql = "Select GoogleContactId from GoogleContact Where PersonID = ? and ContactID = ?";
                $params = array();
                $params[] = array(DTYPE_INT, $this->userid);
                $params[] = array(DTYPE_INT, $r['ContactID']);
                $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
                $aGoogleContact = DbConnManager::GetDb('mpower')->Exec($sql);
                
                if ( count($aGoogleContact) > 0 ) {
                    $r['googleContactId'] = $aGoogleContact[0]['GoogleContactId'];
                } else {
                    $r['googleContactId'] = '';
                }

                $aResponse[] = $r;
            }
        }

        return $aResponse;
    }

    /*
     * it's also called by ajax.CompanyContact.php as event-driven for insert/update
     * TODO if user has enrolled one's token at serveral jetstream user, it could be double inserting to Google contact
     * TODO get column name of table using MapLookup
     * Google contact accept save username, besso
     */
    public function insertGoogleContacts($rec, $contactId) {

        $googleContactId = $this->getGoogleContactId($contactId);

        if ( $googleContactId != '' ) {
            $this->updateGoogleContacts($rec, $contactId, $googleContactId);
            return true;
        }

        $oBatchPush = new GoogleContactBatchPush( $this->contacts, $this->companyId, $this->userid, 'update');
        $oBatchPush->getMapColumns();
        $contactReource = $oBatchPush->getContacts($contactId);

        $aContact = mssql_fetch_array($contactReource, MSSQL_ASSOC); 
        
        $xml = $oBatchPush->generateGoogleXml($aContact);
        //print_r($xml);exit('pp');
        $len = strlen($xml);
        $add = new Google_HttpRequest("https://www.google.com/m8/feeds/contacts/".$this->contactUserEmail."/full/");
        $add->setRequestMethod("POST");
        $add->setPostBody($xml);
        $add->setRequestHeaders(array('content-length' => $len, 'GData-Version'=> '3.0','content-type'=>'application/atom+xml; charset=UTF-8; type=feed'));

        $submit = $this->contacts->getIo()->authenticatedRequest($add);
        $sub_response = $submit->getResponseBody();
       // var_dump($sub_response);exit;
        $parsed = @simplexml_load_string($sub_response); 
        
        $aClient = explode("base/",$parsed->id);

        $googleContactId = $aClient[1];
        //var_dump($googleContactId);exit;

        $sql = 'Insert GoogleContact (PersonID, ContactID, GoogleContactId) values (?, ?, ?)';
        $params = array();
        $params[] = array(DTYPE_INT, $this->userid);
        $params[] = array(DTYPE_INT, $contactId);
        $params[] = array(DTYPE_STRING, $googleContactId);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $status = DbConnManager::GetDb('mpower')->Exec($sql);
 
        if (!$status) {
            return false;
        }
    }
    /*Start JET-37 For Contact insert speed */
    public function insertGoogleContactsNew($rec, $contactId) {

		$googleContactId = $this->getGoogleContactId($contactId);
        if ( $googleContactId != '' ) {
            $this->updateGoogleContacts($rec, $contactId, $googleContactId);
            return true;
        }

        $oBatchPush = new GoogleContactBatchPush( $this->contacts, $this->companyId, $this->userid, 'update');
        $oBatchPush->getMapColumns();
        $contactReource = $oBatchPush->getContacts($contactId);

        $aContact = mssql_fetch_array($contactReource, MSSQL_ASSOC);
        
        /*Start Jet-37 for Add Current company in contact */
        if(isset($_SESSION['USER']['ACCOUNTNAME'])){
        $aContact['Group_Company'] = $_SESSION['USER']['ACCOUNTNAME'];
        unset($_SESSION['USER']['ACCOUNTNAME']);
		}
		/*End Jet-37 for Add Current company in contact */
		//var_dump($aContact);exit;
		
        $xml = $oBatchPush->generateGoogleXml($aContact);

        $len = strlen($xml);
        $add = new Google_HttpRequest("https://www.google.com/m8/feeds/contacts/".$this->contactUserEmail."/full/");
        $add->setRequestMethod("POST");
        $add->setPostBody($xml);
        $add->setRequestHeaders(array('content-length' => $len, 'GData-Version'=> '3.0','content-type'=>'application/atom+xml; charset=UTF-8; type=feed'));

        $submit = $this->contacts->getIo()->authenticatedRequest($add);
        $sub_response = $submit->getResponseBody();
      //  var_dump($this->contactUserEmail,$sub_response);exit('contact inserted');
        $parsed = @simplexml_load_string($sub_response); 
        
        $aClient = explode("base/",$parsed->id);

        $googleContactId = $aClient[1];
        //var_dump($googleContactId);exit;

        $sql = 'Insert GoogleContact (PersonID, ContactID, GoogleContactId) values (?, ?, ?)';
        $params = array();
        $params[] = array(DTYPE_INT, $this->userid);
        $params[] = array(DTYPE_INT, $contactId);
        $params[] = array(DTYPE_STRING, $googleContactId);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $status = DbConnManager::GetDb('mpower')->Exec($sql);
        
        $sql = 'Update Contact set GoogleStatus = 1 where ContactID = ?';
        $params = array();
        $params[] = array(DTYPE_INT, $contactId);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        DbConnManager::GetDb('mpower')->Exec($sql);
 
        if (!$status) {
            return false;
        }
    }
    /*End JET-37 For Contact insert speed */

    public function updateGoogleContacts($rec, $contactId, $googleContactId) {
        
         

        $oBatchPush = new GoogleContactBatchPush( $this->contacts, $this->companyId, $this->userid, 'update');
        $mapcol = $oBatchPush->getMapColumns();
        
            
        $contactReource = $oBatchPush->getContacts($contactId);
       
        $aContact = mssql_fetch_array($contactReource, MSSQL_ASSOC);
 //echo '<pre>'; print_r($rec); echo '</pre>';
            //echo '<pre>'; print_r($contactId); echo '</pre>';
            //echo '<pre>'; print_r($googleContactId); echo '</pre>';
            //echo '<pre>'; print_r($aContact); echo '</pre>';
         //exit;
        $xml = $oBatchPush->generateGoogleXml($aContact, 'update');

        try {
            $len = strlen($xml);
            $uri = 'https://www.google.com/m8/feeds/contacts/'.$this->contactUserEmail.'/full/'. $googleContactId;
            $add = new Google_HttpRequest($uri);
            $add->setRequestMethod("PUT");
            $add->setPostBody($xml);
            $add->setRequestHeaders(array('content-length' => $len, 'GData-Version'=> '3.0','content-type'=>'application/atom+xml; charset=UTF-8; type=feed', 'If-Match'=>'*'));

            $submit = $this->contacts->getIo()->authenticatedRequest($add);

            if ( $submit->getResponseHttpCode() == '404'  ) {
                error_log('Error while updating Contacts : ' . $this->contactUserEmail);
                error_log($submit->getResponseBody());
                return false;
            }

            $sub_response = $submit->getResponseBody();
            $parsed = simplexml_load_string($sub_response); 
            $aClient = explode("base/",$parsed->id);

            $googleContactId = $aClient[1];
        }catch(Exception $e) {
            error_log(print_r($e,true));
        }

        return true;
    }


    /* for Delete
     * There is no option to delete Jetstream Contacts
     * This method delete just Google contact based on Jet's delete event
     */
     
     /*Start Jet-37 Updated for contact Remove */
     
    public function deleteGoogleContact($contactId) {

      //  $sql = "Select googleContactId from Contact Where ContactID = ?";
        $sql = "Select GoogleContactId from GoogleContact Where ContactID = ?";
        $params = array();
        $params[] = array(DTYPE_INT, $contactId);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aContact = DbConnManager::GetDb('mpower')->Exec($sql);
       
        $rec = $aContact[0];
        $googleContactId = trim($rec['GoogleContactId']);

        if ( '' != $googleContactId ) {
            $url = 'https://www.google.com/m8/feeds/contacts/'.$this->contactUserEmail.'/full/' . $googleContactId;
            $req = new Google_HttpRequest($url);
            $req->setRequestMethod("DELETE");
            $req->setRequestHeaders(array('GData-Version'=> '3.0','content-type'=>'application/atom+xml; charset=UTF-8; type=feed', 'If-Match'=>'*'));
            try {
                // Google Contacts way
                $val = $this->contacts->getIo()->authenticatedRequest($req);
            } catch (Exception $e) {
                error_log($e);
                return false;
            }

            // The contacts api only returns XML responses.
            $response =$val->getResponseBody();
            
            $xml = simplexml_load_string($response); // Convert to an ARRAY
            unset($xml);
        }
    }
    
    /*Start Jet-37 Updated for contact Remove */

    private function getOption($id) {

        $sql = "Select OptionName from [Option] Where OptionId = ?";
        $params = array();
        $params[] = array(DTYPE_INT, $id);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aOption = DbConnManager::GetDb('mpower')->Exec($sql);
        $val = $aOption[0]['OptionName'];
        return $val;

    }

    private function existContact($firstname, $lastname) {

        $sql = "Select contactid from Contact Where text01 = ? and text02 = ? and companyid = ?";
        $params = array();
        $params[] = array(DTYPE_STRING, $firstname);
        $params[] = array(DTYPE_STRING, $lastname);
        $params[] = array(DTYPE_INT, $this->companyId ); 
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aRow = DbConnManager::GetDb('mpower')->Exec($sql);
        return ( count($aRow) > 0 ) ? true : false;
    }

    /* 
     * 1. should insert master table
     * 2. then insert googleContact mapping table
     */
    private function insertClientContact($gid, $g) {

        //echo '<pre>'; print_r($g); echo '</pre>';

        if ( $this->debug ) {
            error_log('insert contact with google id : ' . $gid);
        }

        $firstname = $g->{'gd$name'}->{'gd$givenName'}->{'$t'};
        $lastname  = $g->{'gd$name'}->{'gd$familyName'}->{'$t'};

        if ( $this->existContact($firstname, $lastname) ) {
            return false;
        }

        $_POST = array();
        $_POST = $this->generateFormData($g);
        //echo '<pre>'; print_r($_POST); echo '</pre>';
        $data = $_POST;

        $user_id = $this->userid;
        $company_id = $this->getCompanyId($user_id);

        // add new company
        if ( $data['AccountID'] == '' and $data['companyName'] != '' ) {
            $_POST = array(
                    'rec_type' => 'account',
                    'ID' => '',
                    'Text01' => $data['companyName']
                    );

            $rec = new ModuleCompanyContactNew($this->companyId, $this->userid);
            $input = Inspekt::makePostCage();
            $result = $rec->ValidateInput($input);
            //$result = $rec->ValidateInput($_POST);
        }

        if ( isset($result['status']['RECID']) && $result['status']['RECID'] > 0 ) {
            $data['accountid'] = $result['status']['RECID'];
            $_POST = $data;
        }

        $input = NULL;
        $rec = new ModuleCompanyContactNew($company_id, $user_id);
        $input = Inspekt::makePostCage(NULL,TRUE,FALSE);
        $result = $rec->ValidateInput($input);
        if ($result['status']['STATUS'] == 'OK') {

            $contactId = $result['status']['RECID'];
            $sql = 'Insert GoogleContact (PersonID, ContactID, GoogleContactId) values (?, ?, ?)';
            $params = array();
            $params[] = array(DTYPE_INT, $this->userid);
            $params[] = array(DTYPE_INT, $contactId);
            $params[] = array(DTYPE_STRING, $gid);
            $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
            $status = DbConnManager::GetDb('mpower')->Exec($sql);
        } else {
            error_log('Fail to insert jetstream with Google Contact : ' . $firstname . ' ' . $lastname);
            error_log(print_r($_POST,true));
            error_log(print_r($result['status'],true));

        }

        return true;
    }

    /* 
     * 1. should update master table
     * 2. then no need to update googleContact mapping table
     */
    private function updateClientContact($clientId, $gid, $g) {

        $_POST = $this->generateFormData($g);
        $_POST['ID'] = $clientId;

        $rec = new ModuleCompanyContactNew($this->companyId, $this->userid);
        $input = Inspekt::makePostCage();
        $result = $rec->ValidateInput($input);

        if (!$result['status']['STATUS'] == 'OK') {
            error_log('Error while updating Contact for syncing google with contactid : ' . $clientId);
            error_log( print_r($result['status'],true) );
        }

        // add new company
        if ( $_POST['accountid'] == '' and $_POST['companyName'] != '' ) {
            $_POST = array(
                'rec_type' => 'account',
                'Text01' => $_POST['companyName']
            );

            $rec = new ModuleCompanyContactNew($this->companyId, $this->userid);
            $input = Inspekt::makePostCage();
            $result = $rec->ValidateInput($input);
        }

        return false;
    }

    private function getCompanyId($user) {
        $sql = 'Select TOP 1 companyId from people where personId = ?';
        $params[] = array(DTYPE_INT, $user);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aCompany = DbConnManager::GetDb('mpower')->Exec($sql);
        return $aCompany[0]['companyId'];
    }

    public function getGoogleContactId($contactId) {
        $sql = 'Select TOP 1 googleContactId from GoogleContact where contactId = ? and personId = ?';
        $params[] = array(DTYPE_INT, $contactId);
        $params[] = array(DTYPE_INT, $this->userid);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aRow = DbConnManager::GetDb('mpower')->Exec($sql);
        return $aRow[0]['googleContactId'];
    }

    private function getContactIdWithGid($gid) {
        $sql = 'Select ContactId from GoogleContact where personId = ? and GoogleContactId = ?';
        $params[] = array(DTYPE_INT, $this->userid);
        $params[] = array(DTYPE_STRING, $gid);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aRow = DbConnManager::GetDb('mpower')->Exec($sql);
        //echo '<pre>'; print_r($aRow); echo '</pre>';
        return (!empty($aRow[0]['ContactId'])) ? $aRow[0]['ContactId'] : false;
    }

    private function controlUpdateClientContacts( $googleContacts ) {

        if ( count($googleContacts) == 0 )  return true;

        $i = 1;
        foreach($googleContacts as $g ) {

            $gid = $g->id->{'$t'};
            $aGid = explode('/',$gid);
            $gid = $aGid[count($aGid)-1];

            // No googleContactId in DB, it's target of insert 
            $clientId = $this->getContactIdWithGid($gid);

            if ( !$clientId ) {
                $this->insertClientContact($gid, $g);
                continue;
            }

            $updated = $g->updated->{'$t'};
            $updatedTimestamp = strtotime($updated);

            $utc_timezone = new DateTimeZone('UTC');

            $date = $this->contactProcessed;
            $date = str_replace(':000','',$date);
            $date = str_replace('.000','',$date);

            $jetDate = new JetDateTime($date, $utc_timezone);
            $processed = $jetDate->asMsSql('g'); 
            $processedTimestamp = strtotime($processed);

            $isUpdated = false;

            if ( $updatedTimestamp > $processedTimestamp ) {
                $isUpdated = true;
            }

            if ( $isUpdated ) {
                //echo '<pre>'; print_r($clientId); echo '</pre>';
                //echo '<pre>'; print_r($gid); echo '</pre>';
                if ( !$this->updateClientContact($clientId, $gid, $g) ) {
                    continue;
                }
            }
        }
        return true;
    }

    /* set $this->maps member */
    private function getMapColumns() {
        foreach ( $this->aMapWithGoogle as $type => $aSection ) {
            foreach ( $aSection as $section => $aLabel ) {
                foreach ( $aLabel as $gLabel => $cLabel ) {
                    $fieldname = MapLookup::GetFieldDefByLabelName($this->companyId, $cLabel, $type);
                    if ( $fieldname != '' ) {
                        $this->maps[$type][$section][$gLabel] = $fieldname['FieldName'];
                    }
                }
            }
        }
        return true;
    }

    private function generateFormData($g) {

        $title = $g->title->{'$t'};
        $aName = explode(' ', $title);

        $aAccount = array();
        $aContact = array();
        $firstname = $g->{'gd$name'}->{'gd$givenName'}->{'$t'};
        $firstnameColumn = $this->maps['contact']['Name']['Firstname'];
        $aContact[$firstnameColumn] = $firstname;

        $lastname  = $g->{'gd$name'}->{'gd$familyName'}->{'$t'};
        $lastnameColumn = $this->maps['contact']['Name']['Lastname'];
        $aContact[$lastnameColumn] = $lastname;

        $aEmail = $g->{'gd$email'};
        if ( count($aEmail) > 0 ) {
            foreach ($aEmail as $oEmail ) {
                $relNode = $oEmail->rel;
                $aRel = explode('#',$relNode);
                if ( $aRel[1] == 'home' ) {
                    $emailHome = $oEmail->address;
                    $emailHomeColumn = $this->maps['contact']['Email']['Home'];
                    $aContact[$emailHomeColumn] = $emailHome;
                }
                if ( $aRel[1] == 'work' ) {
                    $emailWork = $oEmail->address;
                    $emailWorkColumn = $this->maps['contact']['Email']['Work'];
                    $aContact[$emailWorkColumn] = $emailWork;
                }
            }
        }

        //echo '<pre>'; print_r($this->maps); echo '</pre>'; exit;
        $aNode = $g->{'gd$phoneNumber'};
        if ( count($aNode) > 0 ) {
            foreach ($aNode as $oNode ) {
                $relNode = $oNode->rel;
                $aRel = explode('#',$relNode);
                if ( $aRel[1] == 'mobile' ) {
                    $phoneMobile = $oNode->{'$t'};
                    $phoneMobileColumn = $this->maps['contact']['Phone']['Mobile'];
                    $aContact[$phoneMobileColumn] = $phoneMobile;
                }
                if ( $aRel[1] == 'home' ) {
                    $phoneHome = $oNode->{'$t'};
                    $phoneHomeColumn = $this->maps['contact']['Phone']['Home'];
                    $aContact[$phoneHomeColumn] = $phoneHome;
                }
                if ( $aRel[1] == 'work' ) {
                    $phoneWork = $oNode->{'$t'};
                    $phoneWorkColumn = $this->maps['contact']['Phone']['Work'];
                    $aContact[$phoneWorkColumn] = $phoneWork;
                }
                if ( $aRel[1] == 'main' ) {
                    $phoneMain = $oNode->{'$t'};
                    $phoneMainColumn = $this->maps['contact']['Phone']['Main'];
                    $aContact[$phoneMainColumn] = $phoneMain;
                }
                if ( $aRel[1] == 'home_fax' ) {
                    $phoneHomeFax = $oNode->{'$t'};
                    $phoneHomeFaxColumn = $this->maps['contact']['Phone']['Home_Fax'];
                    $aContact[$phoneHomeFaxColumn] = $phoneHomeFax;
                }

                if ( $aRel[1] == 'work_fax' ) {
                    $phoneWorkFax = $oNode->{'$t'};
                    $phoneWorkFaxColumn = $this->maps['contact']['Phone']['Work_Fax'];
                    $aContact[$phoneWorkFaxColumn] = $phoneWorkFax;
                }
            }
        }

        $aNode = $g->{'gd$structuredPostalAddress'};
        if ( count($aNode) > 0 ) {
            foreach ($aNode as $oNode) {
                // I found some rel was work, it depends on google usage, besso
                //$relNode = $oNode->rel;
                //$aRel = explode('#',$relNode);
                //if ( $aRel[1] == 'home' ) {

                    $address1 = $oNode->{'gd$street'}->{'$t'};
                    $address1Column = $this->maps['contact']['Address']['Street'];
                    $aContact[$address1Column] = $address1;

                    $address2 = $oNode->{'gd$neighborhood'}->{'$t'};
                    $address2Column = $this->maps['contact']['Address']['Neighborhood'];
                    $aContact[$address2Column] = $address2;

                    $zipcode = $oNode->{'gd$postcode'}->{'$t'};
                    $zipcodeColumn = $this->maps['contact']['Address']['PostalCode'];
                    $aContact[$zipcodeColumn] = $zipcode;

                    $city = $oNode->{'gd$city'}->{'$t'};
                    $cityColumn = $this->maps['contact']['Address']['City'];
                    $aContact[$cityColumn] = $city;

                    $stateName = $oNode->{'gd$region'}->{'$t'};
                    $state = $this->getOptionId($this->companyId,$stateName);
                    $stateColumn = $this->maps['contact']['Address']['Province'];
                    $aContact[$stateColumn] = $state;

                    $country  = $oNode->{'gd$country'}->{'$t'};
                    $countryColumn = $this->maps['account']['Address']['Country'];
                    $aAccount[$countryColumn] = $country;

                //}
            }
        }

        $aNode = $g->{'gd$organization'};
        if ( count($aNode) > 0 ) {
            foreach ($aNode as $oNode) {
                $companyName = $oNode->{'gd$orgName'}->{'$t'};
                if ( $companyName != '' ) {
                    $accountid = $this->getAccountId($companyName);
                }
                $companyNameColumn = $this->maps['account']['Group']['Company'];
                $aAccount[$companyNameColumn] = $companyName;

                $title = $oNode->{'gd$orgTitle'}->{'$t'};
                $titleColumn = $this->maps['contact']['Group']['Title'];
                $aContact[$titleColumn] = $title;
            }
        }

        $aNode = $g->{'gContact$website'};
        if ( count($aNode) > 0 ) {
            foreach ($aNode as $oNode) {
                $website = $oNode->href;
                $websiteColumn = $this->maps['contact']['Group']['Work_URL'];
                $aContact[$websiteColumn] = $website;
            }
        }

        $aContact['rec_type'] = 'contact';
        $aContact['AccountID'] = $accountid;
        $aContact['ID'] = '';
        $aContact['account'] = '';
        $aContact['companyName'] = $companyName;

        //echo '<pre>'; print_r($aContact); echo '</pre>'; exit;
                
        return $aContact;

    }

    private static function getOptionId($companyid, $name) {
        $name = strtoupper($name);
        $sql = "Select OptionId from [Option] Where CompanyId = ? and OptionName = ?";
        $params = array();
        $params[] = array(DTYPE_INT, $companyid);
        $params[] = array(DTYPE_STRING, $name);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aRow = DbConnManager::GetDb('mpower')->Exec($sql);
        $val = $aRow[0]['OptionId'];
        return $val;
    }
    
    private function getAccountId($name) {
        $sql = "Select AccountId from account Where Companyid = ". $this->companyId ." and Text01 like '%" . $name . "%'"; 
        /*
        $params = array();
        $params[] = array(DTYPE_INT, $this->companyId);
        $params[] = array(DTYPE_STRING, $name);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        */
        $aRow = DbConnManager::GetDb('mpower')->Exec($sql);
        $val = $aRow[0]['AccountId'];
        return $val;
    }
} 
