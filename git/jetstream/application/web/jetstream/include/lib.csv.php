<?php

function ReadCsvFile($file_path, $column_headings = true, $record_type = 'MpRecord', $index = null) {
	$row = 1;
	$recordset = new MpRecordset();

	$file_handle = fopen($file_path, "r");
	
	while ($values = fgetcsv($file_handle, 1000)) {
		if ($row == 1) {
			$keys = $values;  // Assumes that the first line of data are the keys
		}
		else {
			if ($column_headings) $row = array_combine($keys, $values);
			$record = new $record_type;
			$record->SetDbRow($row);
			if (is_null($index)) {
				$recordset[] = $record;
			}
			else {
				$recordset[$record->$index] = $record;
			}
		}
		$row++;
	}
	fclose($file_handle);
	return $recordset;
}

function ParseCsvFile($file, $columnheadings = false, $delimiter = ',', $enclosure = "\"") {
    $row = 1;
    $rows = array();
    $handle = fopen($file, 'r');

    while (($data = fgetcsv($handle, 1000, $delimiter, $enclosure )) !== FALSE) {

        if (!($columnheadings == false) && ($row == 1)) {
            $headingTexts = $data;
        }
        elseif (!($columnheadings == false)) {
            foreach ($data as $key => $value) {
                unset($data[$key]);
                $data[$headingTexts[$key]] = $value;
            }
            $rows[] = $data;
        }
        else {
            $rows[] = $data;
        }
        $row++;
    }

    fclose($handle);
    return $rows;
}

function ParseCsvFileByFirst($file, $columnheadings = false, $delimiter = ',', $enclosure = "\"") {
    $row = 0;
    $rows = array();
    $handle = fopen($file, 'r');

    while (($data = fgetcsv($handle, 1000, $delimiter, $enclosure )) !== FALSE) {

        if (!($columnheadings == false) && ($row == 0)) {
            $headingTexts = $data;
        }
        $col = 1;
        foreach ($data as $key => $value) {
            if ($col == 1) {
                unset($data[$key]);
                $row_id = $value;
            }
            elseif (!($columnheadings == false)) {
                unset($data[$key]);
                $data[$headingTexts[$key]] = $value;
                $col++;
            }
           $col++;
        }
        if ($row > 0 || ($columnheadings == false)) {
            $rows[$row_id] = $data;
        }
        $row++;
    }

    fclose($handle);
    return $rows;
}

?>
