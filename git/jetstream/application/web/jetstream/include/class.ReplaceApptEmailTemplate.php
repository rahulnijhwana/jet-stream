<?php
class  ReplaceEmailTemplate{
    
    function ReplaceEmailTemplateContact($companyid,$contactid,$contact_template){
               
        $contact_sql = "select * from contact where contactid = ".$contactid;
        $contact_rs = array();
        $contact_rs = DbConnManager::GetDb('mpower')->Exec($contact_sql);
         preg_match_all("/\{(.*?)\}/m",$contact_template,$match,PREG_SET_ORDER);
                $keyval=array();
                $replacearra=array();
                
                foreach($match as $field){                
                    $keyval[]=$field[1];
                    $replacearra[]=$field[0];
                }
                
                //echo '<pre>';print_r($replacearra);echo '</pre>';
                
            for($y=0;$y<count($keyval);$y++){
                $keydefaultarr = explode("|",$keyval[$y]);
                $keyname = trim($keydefaultarr[0]);
                $default = trim($keydefaultarr[1]);
                if($contact_rs[0][$keyname] == '' || $contact_rs[0][$keyname] == NULL){
                    $replace = $default;
                }
                else if (stristr($keyname,'Select') == true){
                    $option_value = '';
                    if($contact_rs[0][$keyname] != ''){
                        $option_sql = "select OptionName from [option] where OptionID = ".$contact_rs[0][$keyname];
                        $option_rs = DbConnManager::GetDb('mpower')->getOne($option_sql);
                        $option_value = $option_rs->OptionName;
                    }
                    $replace=$option_value;
                }
                else {
                    
                    $replace=$contact_rs[0][$keyname];
                }
                $contact_template = str_replace($replacearra[$y],$replace,$contact_template);
            }
       
        return $contact_template;
    }
    
    
    function ReplaceEmailTemplateCompany($companyid,$contactid,$company_template){
       
        
        $accountid_sql = "select AccountID from contact where ContactID = ".$contactid;
        $result = DbConnManager::GetDb('mpower')->getOne($accountid_sql);
        $accountid = $result->AccountID;
        
        $accountdetail_sql = "select * from Account where AccountID = ".$accountid;
        $accountdetail_rs = DbConnManager::GetDb('mpower')->Exec($accountdetail_sql);
        preg_match_all("/\{(.*?)\}/m",$company_template,$match,PREG_SET_ORDER);
                $keyval=array();
                $replacearra=array();
                
                foreach($match as $field){                
                    $keyval[]=$field[1];
                    $replacearra[]=$field[0];
                }
                
                //echo '<pre>';print_r($replacearra);echo '</pre>';
                
            for($y=0;$y<count($keyval);$y++){
                $keydefaultarr = explode("|",$keyval[$y]);
                $keyname = trim($keydefaultarr[0]);
                $default = trim($keydefaultarr[1]);
                if($accountdetail_rs[0][$keyname] == '' || $accountdetail_rs[0][$keyname] == NULL){
                    $replace = $default;
                }
                else if (stristr($keyname,'Select') == true){
                    $option_value = '';
                    if($accountdetail_rs[0][$keyname] != ''){
                        $option_sql = "select OptionName from [option] where OptionID = ".$accountdetail_rs[0][$keyname];
                        $option_rs = DbConnManager::GetDb('mpower')->getOne($option_sql);
                        $option_value = $option_rs->OptionName;
                    }
                    $replace=$option_value;
                }
                else {
                    
                    $replace=$accountdetail_rs[0][$keyname];
                }
                $company_template = str_replace($replacearra[$y],$replace,$company_template);
            }
            
       
        return $company_template;
    }

}
?>