<?php
require_once BASE_PATH . '/include/class.DebugUtil.php';

class AssignmentUtil
{	
	
	private $people = array();

	public function __construct(){
		$this->setPeople();
	}
	
	/**
	 * Returns the PersonID for the
	 * specified name in the format of
	 * 
	 * FirstName Lastname
	 * 
	 * @param String $name
	 */
	public function convert($name)
	{	
		$personID = array_search($name, $this->getPeople());
		
		return $personID ? $personID : 0;
	}

	private function setPeople()
	{	
		$stmt = 'SELECT FirstName, LastName, PersonID FROM People WHERE CompanyID = ? AND Deleted != 1  AND SupervisorID != -3';	
		$params = array(DTYPE_INT, $_SESSION['company_id']);
		
		$sql = SqlBuilder()->LoadSql($stmt)->BuildSql($params);
		$results = DbConnManager::GetDb('mpower')->Exec($sql);
		
		foreach($results as $result){
			$this->people[$result['PersonID']] = $result['FirstName'] . ' ' . $result['LastName'];
		}
	}
	
	private function getPeople()
	{
		return $this->people;
	}
}