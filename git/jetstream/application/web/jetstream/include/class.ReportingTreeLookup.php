<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.JetCache.php';

class ReportingTreeLookup
{
	private static $lookup_data = array();
	private static $cache_name = 'reporting_tree';

	public static function Load($company_id) {
		if (!in_array($company_id, self::$lookup_data)) {
			if (!$load_array = JetCache::Load($company_id, self::$cache_name)) {
				$load_array = self::LoadFromDb($company_id);
			}
			self::$lookup_data[$company_id] = $load_array;
		}
		return self::$lookup_data[$company_id];
	}

	/*
	 * Loads people tree from the database
	 * Can be called from anywhere to reload
	 */
	public static function LoadFromDb($company_id) {
		$sql = "SELECT PersonID, UserID, Color, FirstName
			, LastName, IsSalesperson, SupervisorID, GroupName
			, Rating, Level, StartDate, AvgVLevel
			, Email, AverageSalesCycle, FirstMeeting, AverageSale
			, ClosingRatio, TotalRevenues, TotalSales, UnreportedAvgSale
			, UnreportedSalesPerMonth, MasterAssignee, OnVacation
			, CONVERT(VARCHAR(10), VacationReturn, 101) AS VacationReturn
		FROM people left outer join
			(SELECT PersonID AS PID, AVG(CAST(Vlevel AS float)) AS AvgVLevel
			FROM opportunities
			WHERE companyid = ?
			AND vlevel > 0
			AND category NOT IN (9,6)
			GROUP BY personid) AS Valuations ON people.PersonID = Valuations.PID
		WHERE companyid = ? AND deleted = 0";

		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_id), array(DTYPE_INT, $company_id));
	
		$people = DbConnManager::GetDb('mpower')->Exec($sql);

		$parent_index = array();
		$tree = array();
		foreach ($people as $person) {
			if (isset($tree[$person['PersonID']])) {
				$tree[$person['PersonID']] = array_merge($person, $tree[$person['PersonID']]);
			} else {
				$tree[$person['PersonID']] = $person;
			}
			$tree[$person['SupervisorID']]['children'][] = $person['PersonID'];
		}
		
		JetCache::Save($company_id, self::$cache_name, $tree);
		return $tree;
	}

	public function GetRecord($company_id, $person_id) {
		$tree = self::Load($company_id);
		if (isset($tree[$person_id])) {
			return $tree[$person_id];
		}
		return FALSE;
	}

	public function GetRecords($company_id, $people) {
		$tree = self::Load($company_id);
		$output = array();
		/*Start : JET-37*/
		if (is_array($people))
		{
			foreach ($people as $person) {
				if (isset($tree[$person])) {
					$output[$tree[$person]['PersonID']] = $tree[$person];
				}
			}
		}
		/*End : JET-37 */
		return $output;
	}

	public function GetPersonByUsername($company_id, $username) {
		$tree = self::Load($company_id);
		foreach ($tree as $person) {
			if (isset($person['UserID']) && $person['UserID'] == $username) {
				return $person['PersonID'];
			}
		}
		return FALSE;
	}

	public function GetPersonByName($company_id, $first, $last) {
		$tree = self::Load($company_id);
		foreach ($tree as $person) {
			if (isset($person['FirstName']) && isset($person['LastName']) && $person['FirstName'] == $first && $person['LastName'] == $last) {
				return $person['PersonID'];
			}
		}
		return FALSE;
	}

	/**
	 * Provide an array of all the person data for a combined array
	 *
	 * @return array
	 */
	public function GetCombinedArray($company_id, $pov) {
		$tree = self::Load($company_id);
		$output_fields = array('PersonID', 'Color', 'FirstName', 'LastName', 'Rating', 'StartDate', 'AvgVLevel');
		$output = array();
		foreach (self::GetLimb($company_id, $pov) as $target) {
			$person = array();
			foreach ($output_fields as $field) {
				$person[$field] = $tree[$target][$field];
			}
			$output[] = $person;
		}
		return $output;
	}

	
	public function GetTarget($company_id, $get_all = FALSE) {
		$tree = self::Load($company_id);
		
		$rt_vars = array(); // Variables that are stored and updated in ReportingTree
		

		$rt_vars['manager'] = self::IsManager($company_id, $_SESSION['login_id']);
		//if (isset(self::target)) {
		//	return self::target;
		//}
		$rt_vars['paging'] = (basename($_SERVER['SCRIPT_NAME']) == 'dashboard.php') ? TRUE : FALSE;
		$reset_paging = FALSE;
		
		if (isset($_POST['sp_sort'])) {
			$sort = $_POST['sp_sort'];
			if (!isset($_SESSION['sp_sort']) || (isset($_SESSION['sp_sort']) && $_SESSION['sp_sort'] != $sort)) {
				$_SESSION['sp_sort'] = $sort;
				$reset_paging = TRUE;
			}
		}
		
		if (!$get_all && isset($_GET['target']) && self::IsViewable($company_id, $_GET['target'])) {
			$_SESSION['target'] = (int) $_GET['target'];
			$target = (int) $_GET['target'];
		} elseif (!$get_all && isset($_SESSION['target']) && $_SESSION['target'] && self::IsViewable($company_id, $_SESSION['target']) && basename($_SERVER['SCRIPT_NAME']) == 'dashboard.php') {
			$target = $_SESSION['target'];
		} elseif (!$rt_vars['manager']) {
			$target = (int) $_SESSION['pov'];
		} elseif ($_SESSION['combined']) {
			$target = self::GetLimb($company_id, $_SESSION['pov']);
		} else {
			$target = self::GetDirectSp($company_id, $_SESSION['pov']);
		}
		
		$rt_vars['total_sp'] = count($target);
		if ($get_all) {
			return $target;
		}
		
		if ($rt_vars['paging'] && is_array($target)) {
			$page = $_SESSION['sp_page'];
			if ($page > $rt_vars['total_sp']) $page = self::$total_sp - 10;
			if ($page < 0 || $reset_paging) $page = 0;
			$_SESSION['sp_page'] = $page;
			$rt_vars['target'] = array_slice($target, $page, 10);
		} else {
			$rt_vars['target'] = $target;
		}
		return $rt_vars;
	}

	public function GetPath($company_id, $person, $output = FALSE) {
		$tree = self::Load($company_id);
		$record = $tree[$person];
	
		if (!$output) {
			$output = $record['FirstName'] . ' ' . $record['LastName'];
		}
		if ($record['GroupName']) {
			$output = $record['GroupName'] . ': ' . $output;
		}
		// Recursion -
		if (isset($tree[$record['SupervisorID']]) && $record['SupervisorID'] > 0) {
			$output = self::GetPath($company_id, $record['SupervisorID'], $output);
		}
		return $output;
	}
	
	public function GetPathBackwards($company_id, $person, $output = FALSE, $ptr = 0) {
		$tree = self::Load($company_id);
		$record = $tree[$person];
	
		$output[$ptr] = $record['PersonID'];
		$ptr = $ptr + 1;
		
		// Recursion -
		if (isset($tree[$record['SupervisorID']]) && $record['SupervisorID'] > 0) {
			$output = self::GetPathBackwards($company_id, $record['SupervisorID'], $output, $ptr);
		}
		return $output;
	}
	
	public function GetInfo($company_id, $person, $type) {
		$tree = self::Load($company_id);
		if (isset($tree[$person][$type])) {
			return $tree[$person][$type];
		} else {
			return FALSE;
		}
	}

	public function GetRoots($company_id) {
		$tree = self::Load($company_id);
		$output = array();
		foreach ($tree as $key => $info) {
			if (!array_key_exists('parent', $info)) {
				$output[] = $key;
			}
		}
		return $output;
	}

	/**
	 * Returns a list of the start and their children that are salespeople
	 * Does not proceed into grandchildren
	 *
	 * @param int $start_id
	 * @return array
	 */
	public function GetDirectSp($company_id, $start_id) {
		$tree = self::Load($company_id);
		$sort = (isset($_SESSION['sp_sort'])) ? $_SESSION['sp_sort'] : 'FirstName';
		$reverse_sort = FALSE;
		if (substr($sort, -4) == '_asc') {
			$reverse_sort = FALSE;
			$sort = substr($sort, 0, -4);
		}
		if (substr($sort, -4) == '_dsc') {
			$reverse_sort = TRUE;
			$sort = substr($sort, 0, -4);
		}
		
		$start_record = $tree[$start_id];
		$output = array();
		$children = (isset($start_record['children'])) ? $start_record['children'] : array();
		$children[] = $start_id;
		foreach ($children as $child) {
			if ($tree[$child]['IsSalesperson'] == 1) {
				$output[] = $child;
			}
		}
		
		return self::SortPeople($company_id, $output, $sort, $reverse_sort);
	}
	
	/**
	 * Returns a list of the start children that are salespeople
	 * Does not proceed into grandchildren, not include itself
	 *
	 * @param int $start_id
	 * @return array
	 */
	public function GetDirectChildren($company_id, $start_id) {
		$tree = self::Load($company_id);
		$sort = (isset($_SESSION['sp_sort'])) ? $_SESSION['sp_sort'] : 'FirstName';
		$reverse_sort = FALSE;
		if (substr($sort, -4) == '_asc') {
			$reverse_sort = FALSE;
			$sort = substr($sort, 0, -4);
		}
		if (substr($sort, -4) == '_dsc') {
			$reverse_sort = TRUE;
			$sort = substr($sort, 0, -4);
		}
		
		$start_record = $tree[$start_id];
		$output = array();
		$children = (isset($start_record['children'])) ? $start_record['children'] : array();
		//$children[] = $start_id;
		foreach ($children as $child) {
			if ($tree[$child]['IsSalesperson'] == 1) {
				$output[] = $child;
			}
		}
		
		return self::SortPeople($company_id, $output, $sort, $reverse_sort);
	}
	
	/*
     * Returns a sorted list of the requetsed ID and any descendants
     * 
     */
	public function GetLimb($company_id, $start_id, $sort = 'FirstName') {
		$tree = self::Load($company_id);
		$sort = (isset($_SESSION['sp_sort'])) ? $_SESSION['sp_sort'] : $sort;
		$reverse_sort = FALSE;
		if (substr($sort, -4) == '_asc') {
			$reverse_sort = FALSE;
			$sort = substr($sort, 0, -4);
		}
		if (substr($sort, -4) == '_dsc') {
			$reverse_sort = TRUE;
			$sort = substr($sort, 0, -4);
		}
		
		$start_record = $tree[$start_id];
		$output = self::GetDescendants($company_id, $start_record, $sort);
		if ($start_record['IsSalesperson'] == 1) {
			$output[] = $start_id;
		}
		
		return self::SortPeople($company_id, $output, $sort, $reverse_sort);
	}

	/*
     * Returns an unsorted list of the requetsed ID and any descendants
     *
     */
	public function CleanGetLimb($company_id, $start_id) {
		$output = self::CleanGetDescendants($company_id, $start_id);
		$output[] = $start_id;
		return $output;
	}

	/*
	 * Recursively gets the IDs of all descendants into an array
	 */
	public function CleanGetDescendants($company_id, $start_id) {
		$tree = self::Load($company_id);
		$output = array();
		if (isset($tree[$start_id]['children'])) {
			foreach ($tree[$start_id]['children'] as $child) {
				$output[] = $child;
				// recursion -
				$output = array_merge($output, self::CleanGetDescendants($company_id, $child));
			}
		}
		return $output;
	}
	
	/*
	 * $sort can be only  'FirstName', 'LastName', 'Rating', 'StartDate'
	 *
	 */
	public function SortPeople($company_id, $list, $sort, $reverse = FALSE) {
		
		if (count($list) == 0 ) return ; 
	
		$tree = self::Load($company_id);
		foreach ($list as $person) {
			$first[$person] = (!empty($tree[$person]['FirstName'])) ? $tree[$person]['FirstName'] : '';
			$last[$person] = (!empty($tree[$person]['LastName'])) ?  $tree[$person]['LastName'] : '';
			$rating[$person] = (!empty($tree[$person]['Rating'])) ? $tree[$person]['Rating'] : '';
            if ( !empty($tree[$person]['StartDate'])) {
    			$start_date[$person] = date('Ymd', strtotime($tree[$person]['StartDate']));
            } else {
    			$start_date[$person] = date('Ymd');
            }
		}
		$sort_dir = ($reverse) ? SORT_DESC : SORT_ASC;
		
		switch ($sort) {
			case 'FirstName' :
				array_multisort($first, $sort_dir, $last, $sort_dir, $list);
				break;
			case 'LastName' :
				array_multisort($last, $sort_dir, $first, $sort_dir, $list);
				// echo "<pre>" . print_r($last, TRUE) . print_r($first, TRUE) . print_r($first, TRUE) . '</pre>';
				break;
			case 'Rating' :
				array_multisort($rating, $sort_dir, $first, SORT_ASC, $last, SORT_ASC, $list);
				break;
			case 'StartDate' :
				array_multisort($start_date, $sort_dir, $first, SORT_ASC, $last, SORT_ASC, $list);
				break;
		}
		
		return $list;
	}

	public function GetDescendants($company_id, $start_record, $sort = null) {
		$tree = self::Load($company_id);
		$output = array();
		if (isset($start_record['children'])) {
			foreach ($start_record['children'] as $child) {
				if ($tree[$child]['IsSalesperson'] == 1) {
					$output[] = $child;
				}
				$output = array_merge($output, self::GetDescendants($company_id, $tree[$child], $sort));
			}
		}
		return $output;
	}

	public function IsDescendant($company_id, $target, $manager_id) {
		if (in_array($target, self::CleanGetLimb($company_id, $manager_id))) {
			return TRUE;
		}
		return FALSE;
	}

	public function IsManager($company_id, $person_id) {
		$tree = self::Load($company_id);
		return ($tree[$person_id]['Level'] > 1) ? TRUE : FALSE;
	}

	public function IsMasterAssignee($company_id, $person_id) {
		$tree = self::Load($company_id);
		return ($tree[$person_id]['MasterAssignee'] == 1) ? TRUE : FALSE;
	}
	
	public function IsSeniorMgr($company_id, $start_id) {

		$tree = self::Load($company_id);

		$start_record = $tree[$start_id];
		if (isset($tree[$start_id]['children'])) {
			foreach ($tree[$start_id]['children'] as $child) {
				if (isset($tree[$child]['children']) && count($tree[$child]['children']) > 0) {
					return TRUE;
				}
			}
		}
		return FALSE;
	}

	public function IsDirectMgr($company_id, $start_id) {
		$tree = self::Load($company_id);
		$start_record = $tree[$start_id];
		foreach ($tree[$start_id]['children'] as $child) {
			if (isset($tree[$child]['IsSalesperson']) && $tree[$child]['IsSalesperson']) {
				return TRUE;
			}
		}
		return FALSE;
	}

	public function IsViewable($company_id, $target) {
		$tree = self::Load($company_id);
		if ($target == $_SESSION['login_id']) {
			return TRUE;
		} elseif (isset($tree[$target]) && $tree[$target]['SupervisorID'] > 0 && self::IsViewable($company_id, $tree[$target]['SupervisorID'])) {
			return TRUE;
		}
		return FALSE;
	}

	public function GetCombined($company_id) {
		$tree = self::Load($company_id);
		$output = array();
		foreach ($tree as $key => $info) {
			if (!array_key_exists('parent', $info)) {
				$output[] = $key;
			}
		}
		return $output;
	}

	/*
	 * Will get all the active users
	 */	
	public function ActiveOnly($company_id, $targets) {
		$tree = self::Load($company_id);
		$output = array();
		foreach ($targets as $target) {
			if (isset($tree[$target]) && isset($tree[$target]['SupervisorID']) && $tree[$target]['SupervisorID'] != -1 && $tree[$target]['SupervisorID'] != -3) {
		//	if ( $tree[$target]['SupervisorID'] != -1 && $tree[$target]['SupervisorID'] != -3) {
				$output[] = $target;
			}
		}
		return $output;
	}

	/*
	 * Will get all the inactive users
	 */	
	public function InActiveOnly($company_id) {
		$tree = self::Load($company_id);
		$output = array();
		foreach ($tree as $target) {
			if ( $target['SupervisorID'] == -1) {
				$output[] = $target['PersonID'];
			}
		}
		return $output;
	}



	public function GetAllActive($company_id) {
		$tree = self::Load($company_id);
		$output = array();
		foreach ($tree as $key => $info) {
			$output[] = $key;
		}
		return self::ActiveOnly($company_id, $output);
	}
	
	public function GetActiveLimb ($mode= 'active', $supervisorid){
		$nonactive = ReportingTreeLookup::InactiveOnly($this->company_id) ;
		$supervisor = ReportingTreeLookup::CleanGetLimb($this->company_id, -2);
		$tree = ReportingTreeLookup::CleanGetLimb($this->company_id, $supervisorid);
		
		if ($mode == 'Active') $list = array_intersect ($supervisor, $tree); 
		else $list = $nonactive;
		
		return $list ;
	
	
	}
	
	
}


