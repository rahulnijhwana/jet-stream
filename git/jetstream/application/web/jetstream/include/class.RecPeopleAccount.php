<?php

require_once BASE_PATH . '/include/class.MpRecord.php';

class RecPeopleAccount extends MpRecord
{

	public function Initialize() {
		$this->db_table = 'PeopleAccount';
		$this->recordset->GetFieldDef('PeopleAccountID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('AccountID')->required = TRUE;
		$this->recordset->GetFieldDef('PersonID')->required = TRUE;
		parent::Initialize();
	}
}
