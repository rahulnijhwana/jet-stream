<?php
/* 
 * This project has no one single configuration file
 * Use include/constants.php as one
 */
define('WWW_ROOT', $_SERVER['DOCUMENT_ROOT']);
$parentDir = dirname(__DIR__);
define('JETSTREAM_ROOT', $parentDir.'/www');

define('HTTP_HOST', $_SERVER['HTTP_HOST']);
define('JETSTREAM_HOST', HTTP_HOST . '/jetstream');

// Google Setting
define('GOOGLE_CLIENT_NAME','testapi');
define('GOOGLE_CLIENT_ID','298334446795-4jipi4hoaiphqddrv9nglp55kufjtl2m.apps.googleusercontent.com');
define('GOOGLE_CLIENT_SECRET','znBKCzD65sY9XLsyyu91zEaW');

/*
define('GOOGLE_CLIENT_NAME','Jetstream');
define('GOOGLE_CLIENT_ID','768171731291-sfb9i2vuo09dkb1m61va3qsuijmpb2j7.apps.googleusercontent.com');
define('GOOGLE_CLIENT_SECRET','Ssge2WGDQv_lyJjOgW53Jl7I');
*/

define('GOOGLE_CALLBACK_URL', 'http://' . JETSTREAM_HOST . '/googleConnect.php');

// set syncing hours limit as 30 min
//define('GOOGLE_SYNC_INTERVAL', 1800);
define('GOOGLE_SYNC_INTERVAL', 1);

define('GOOGLE_DATE_RFC3339',"Y-m-d\TH:i:s\Z");
define('GOOGLE_RECURRENCE_RFC3339',"Ymd\THis\Z");
define('GOOGLE_TO_DATE',"14");	// from now to 2weeks

// TODO. it could be temporary TBD
define('GOOGLE_TASK_TYPE_IN_JETSTREAM',1890);
define('GOOGLE_EVENT_TYPE_IN_JETSTREAM',1889);

define('GOOGLE_TASK_STATUS_NORMAL','needsAction');
define('GOOGLE_TASK_STATUS_COMPLETED','completed');

// Calendar
define('GOOGLE_CALENDAR_JETSTREAM','Jetstream');

define('GOOGLE_EVENT_STATUS_NORMAL','1');
define('GOOGLE_EVENT_STATUS_COMPLETED','2');

define('GOOGLE_EVENT_STATUS_CONFIRMED','confirmed');
define('GOOGLE_EVENT_STATUS_TENTATIVE','tentative');
define('GOOGLE_EVENT_STATUS_CANCELLED','cancelled');

define('GOOGLE_RECURRENCE_FREQ','FREQ');
define('GOOGLE_RECURRENCE_UNTIL','UNTIL');
define('GOOGLE_RECURRENCE_INTERVAL','INTERVAL');

define('JETSTREAM_DATE_FORMAT',"n/j/y");
define('JETSTREAM_TIME_FORMAT',"g:i a");



$aMapWithGoogle = array(

    'account' => array(
        'Phone' => array(
            //'Main' => 'Phone - Company Main',

            // matterhorn01 exist in contact
            //'Work Fax' => 'Fax - Main'

            // matterhorn01 no from company
            //'Work_Fax' => 'Main Fax'
        ),
        'Address' => array(
            'Country' => 'Country'         
        ),
        /* all inside contact
        'Address' => array(
            'Street' => 'Address 1',
            'PO_Box' => 'PO Box',          
            'Neighborhood' => 'Address 2',           
            'City' => 'City',            
            'Province' => 'State',           
            //'PostalCode' => 'Zip Code',            
            'PostalCode' => 'Zip',            
            'Country' => 'Country'         
        ),
        */
        'Group' => array(
            'Company' => 'Company Name',
            //'Work URL' => 'URL'
            // matterhorn1
            //'Work_URL' => 'Website'
        )
    ),

    'contact' => array(
        'Name' => array(
            'Firstname' => 'name-givenName',
            'Lastname' => 'name-familyName',
            'Fullname' => 'name-fullName',
            'Nickname' => 'name-nickname',
        ),
        'Email' => array(
          //  'Home' => 'Email - Home',
            'Home' => 'email-home',
            'Work' => 'email-work'
        ),
        'Phone' => array(
            'Main' => 'phoneNumber-main',//
            'Work' => 'phoneNumber-work',//
            'Mobile' => 'phoneNumber-mobile',//
            'Home' => 'phoneNumber-home',
            'Work_Fax' => 'phoneNumber-work_fax',
            'Home_Fax' => 'phoneNumber-home_fax'
        ),
        'Address' => array(
            'Street' => 'structuredPostalAddress-street',//Address 1
            'PO_Box' => 'structuredPostalAddress-pobox',         
            'Neighborhood' => 'structuredPostalAddress-neighborhood',//Address 2        
            'City' => 'structuredPostalAddress-city',            
            'Province' => 'structuredPostalAddress-region', //          
            'PostalCode' => 'structuredPostalAddress-postcode',            
            // only country from company
            'Country' => 'structuredPostalAddress-country'         
        ),
        'Group' => array(
            'Title' => 'organization-orgTitle',
            'Work_URL' => 'website-website',
            'Name' => 'organization-orgName'
        )
    )
);


define( 'CONTACT_MAP', serialize( $aMapWithGoogle) );
