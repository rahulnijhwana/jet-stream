<?php
require_once 'class.MapLookup.php';

/**
 * @package library
 */

/*
 * This file is reserved for database lookups that should not be cached but that happen
 * fairly regularly
 */

function GetContactName($company_id, $contact_id) {
	$contact_map_fname = MapLookup::GetContactFirstNameField($company_id, true);
	$contact_map_lname = MapLookup::GetContactLastNameField($company_id, true);
	$company_map_name = MapLookup::GetAccountNameField($company_id, true);
	
	$sql = "SELECT C.ContactID,
			C.$contact_map_fname AS FirstName,
			C.$contact_map_lname AS LastName,
			C.PrivateContact AS Private,
			A.AccountID,
			A.$company_map_name AS CompanyName
		FROM Contact C
			LEFT JOIN Account A ON A.AccountID = C.AccountID
		WHERE C.ContactID = ? AND C.CompanyID = ?";

	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($contact_id, $company_id)));
	$event_info = DbConnManager::GetDb('mpower')->Exec($sql);
	if (count($event_info) > 0) {
		return $event_info[0];
	} else {
		return false;
	}
}

function GetContactInfo($company_id, $contact_list) {
	$contact_map_fname = MapLookup::GetContactFirstNameField($company_id, true);
	$contact_map_lname = MapLookup::GetContactLastNameField($company_id, true);
	$company_map_name = MapLookup::GetAccountNameField($company_id, true);
	
	$sql = "SELECT C.ContactID,
			C.$contact_map_fname AS FirstName,
			C.$contact_map_lname AS LastName,
			A.AccountID,
			A.$company_map_name AS CompanyName
		FROM Contact C
			LEFT JOIN Account A ON A.AccountID = C.AccountID
		WHERE C.CompanyID = ? AND C.ContactID IN (" . ArrayQm($contact_list) . ")";

	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($company_id)), array(DTYPE_INT, $contact_list));
	$event_info = DbConnManager::GetDb('mpower')->Execute($sql, 'mprecord', 'ContactID');
	if (count($event_info) > 0) {
		return $event_info;
	} else {
		return false;
	}
}

