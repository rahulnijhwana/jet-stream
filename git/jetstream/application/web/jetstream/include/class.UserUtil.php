<?php
require_once 'class.DbConnManager.php';
require_once 'class.SqlBuilder.php';

class UserUtil {

	/**
	 * Get all active users for a given company
	 * 
	 * @param int $companyId
	 * @param bool $system : If set, return admin users
	 */
	public static function getUsers($companyId, $system=true){
		
		$limit = '';

		# Don't include MPower Administrators
		if(!$system){
			$limit = ' AND P.SupervisorID != -3 ';
		}
		
		$sql = "
			SELECT
				P.PersonID, 
				L.UserID, 
				L.LastUsed, 
				P.Color,
				P.FirstName, 
				P.LastName, 
				P.IsSalesperson, 
				P.SupervisorID, 
				P.GroupName, 
				P.Rating, 
				P.Level
			FROM
				Logins L 
			LEFT JOIN
				People P ON P.PersonID = L.PersonID
			WHERE
				P.companyid = ?
			AND
				P.deleted != 1
			AND
				P.SupervisorID != -1";
		if ('' != $limit) {
			$sql.= $limit;
		}
		$sql.= "
			ORDER BY 
				P.Level DESC, 
				P.FirstName ASC, 
				P.LastName ASC";

		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyId));
		$users = DbConnManager::GetDb('mpower')->Exec($sql);
		
		return $users;
	}
	
	public static function getAssignedUsers($recId, $recType){
		
		$table = '';
		
		
		switch($recType){
			case 'contact':
				$table = 'PeopleContact';
				$key = 'ContactID';
				break;
			case 'company':
				$table = 'PeopleAccount';
				$key = 'AccountID';
				break;
			case 'account':
				$table = 'PeopleAccount';
				$key = 'AccountID';
				break;
		}
		
		$assigned = array();
		$sql = "
			SELECT
				DISTINCT(PersonID)
			FROM " . $table . " WHERE " . $key . " = ? AND AssignedTo = 1";
		
		$params = array(DTYPE_INT, $recId);
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql($params);
		
		$people = DbConnManager::GetDb('mpower')->Exec($sql);
		foreach($people as $person){
			$assigned[] = $person['PersonID'];
		}
		return $assigned;
	}
	
	
	public static function getUserTree($companyId, $users=null){
		
		if(!$users){
			$users = self::getUsers($companyId, false);
		}
		
		$tree = array();
		foreach($users as $user){
			$tree[$user['Level']] = array();
		}
		foreach(array_keys($tree) as $level){
			$members = array();
			foreach($users as $user){
				if($user['Level'] == $level){
					$members[] = $user;
				}
			}
			$tree[$level] = $members;
		}
		return $tree;
	}

	public static function getChainOfCommand($companyID, $personID) {
		// This function will get all persons in the chain of command for the person specified.
		// Input: Company ID, User ID
		// Outgoing: Sorted array of users by level, first name and last name.
	
		$users = array(); // holds user chain
	
		// Get the current user record now so we can trace back their supervisors 
		// but do not load into the users array yet.
		$person_record = self::getPerson($personID);
		
		// Load all managers into a temporary array which will be in the wrong order
		$mgrs = array();
		$mgrID = $person_record['SupervisorID'];
		while (isset($mgrID) && $mgrID > 0) {
			$mgr_record = self::getPerson($mgrID);
			array_push($mgrs, $mgr_record);
			$mgrID = $mgr_record['SupervisorID'];
		}
		// Now swap the order of the array into the main users array
		for ($i=count($mgrs)-1; $i >= 0; $i--) {
			array_push($users, $mgrs[$i]);			
		}
		
		// Now load the current user into the users array
		array_push($users, $person_record);
	
		// Get any subordinates of the specified person
		$subordinates = array();
		$subordinates = self::getSubordinates($personID);
		
		$rec = count($subordinates);
		if ($rec) { // continue if they have any subordinates
			
			// Check each subordinate in the list to see if they have any subordinates of their own
//			foreach ($subordinates as $subordinate) {
//				array_push($users, $subordinate);	// Add the current subordinate to the user list
	
				// can anyone call the current subordinate boss?
//				$subID = $subordinate['PersonID'];
//				$tmp = array();
//				$tmp = self::getSubordinates($subID);
				
//				if (count($tmp)) {
					// add these to the subordinate list for future passes
//					foreach ($tmp as $staff) {
//						echo "<pre>".$staff['PersonID']."</pre>" ;
//						array_push($subordinates, $staff);
//					}
//				}
//			}

			for ($i=0; $i<$rec; $i++) {
				array_push($users, $subordinates[$i]);	// Add the current subordinate to the user list
				
				// can anyone call the current subordinate boss?
				$subID = $subordinates[$i]['PersonID'];
				$tmp = array();
				$tmp = self::getSubordinates($subID);
				//echo "<pre>subID $subID</pre>" ;
				
				if (count($tmp)) {
					// add these to the subordinate list for future passes					
					foreach ($tmp as $staff) {
						$subordinates[$rec] = $staff;
						$rec++;
					}
				}
			}
		}
	
		return $users;
	}
	
	public static function getFullName($personId){
		$sql = "
			SELECT
				FirstName,
				LastName
			FROM
				People
			WHERE
				PersonID = ?";
		
		$params = (array(DTYPE_INT, $personId));
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql($params);
		$results = DbConnManager::GetDb('mpower')->GetOne($sql);
		
		return $results['FirstName'] . ' ' . $results['LastName'];
	}
	
	/**
	 * Returns an array in the format of:
	 * ('person@domain.com' => 'First Last')
	 * for a given person
	 * 
	 * @param int $personId
	 */
	public static function getEmailAddress($personId){
		$sql = "
			SELECT
				FirstName,
				LastName,
				Email
			FROM
				People
			WHERE
				PersonID = ?";
		
		$params = (array(DTYPE_INT, $personId));
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql($params);
		$results = DbConnManager::GetDb('mpower')->GetOne($sql);
		
		$name = $results['FirstName'] . ' ' . $results['LastName'];
		$email = $results['Email'];
		
		return array($email => $name);
	}
	
	public static function getSubordinates($ID) {
	
		// Get a list of people that call the $personID boss
	
		$sql = "
			SELECT
				PersonID,
				Color,
				FirstName,
				LastName,
				IsSalesperson,
				SupervisorID,
				GroupName,
				Rating,
				Level
			FROM
				people
			WHERE
				SupervisorID = ?
			AND
				deleted != 1
			AND
				SupervisorID != -1
			AND
				SupervisorID != -3
			ORDER BY
				FirstName, 
				LastName";
	
		$params = (array(DTYPE_INT, $ID));
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql($params);
		$results = DbConnManager::GetDb('mpower')->Exec($sql);
		
		return $results;
	}
	
	public static function getPerson($ID) {
	
		// Get a person from the people table
	
		$sql = "
			SELECT
				PersonID,
				Color,
				FirstName,
				LastName,
				IsSalesperson,
				SupervisorID,
				GroupName,
				Rating,
				Level
			FROM
				people
			WHERE
				PersonID = ?";
	
		$params = (array(DTYPE_INT, $ID));
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql($params);
		$person = DbConnManager::GetDb('mpower')->GetOne($sql);
	
		return $person;
	}
		
}
