<?php
/**
 * This file is used to insert/update records to the DashboardSearch table.
 * @package database
 */

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
//require_once BASE_PATH . '/include/class.DashboardInfo.php';
require_once BASE_PATH . '/include/class.fieldsMap.php';

class DashboardSearchCreator {	
	public $company_id;
	public $rec_type;
	public $record_id;
	public $search_text;
	public $rec_active;
	public $comp_name;
	private $acc_row_num;
	private $con_row_num;
	
	function DashboardSearchCreator() {
		$this->record_id = -1;
	}
	
	/* This function inserts/updates the account/contact record in dashboardsearch table. */
	function StoreRecord($dashboard_id) {
		if(isset($this->company_id) && ($this->company_id > 0) && !empty($this->search_text)) {
			/*$sql = "SELECT * FROM DashboardSearch WHERE CompanyID =? AND RecType = ? AND RecID = ?";
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->company_id), array(DTYPE_INT, $this->rec_type), array(DTYPE_INT, $this->record_id));				
			$result = DbConnManager::GetDb('mpower')->Execute($sql, 'DashboardInfo');
			
			$search_info =  new DashboardInfo();
			$search_info->SetDatabase(DbConnManager::GetDb('mpower'));
			if(count($result) > 0) {
				$search_info = $result[0];
			} else {				
				$result[] = $search_info;
			}
			$search_info->CompanyID = $this->company_id;
			$search_info->RecType = $this->rec_type;
			$search_info->RecID = $this->record_id;
			
			$search_info->SearchText = $this->search_text;
			
			if($this->rec_active == 1) {
				$search_info->IsActive = $this->rec_active;			
			}
			$result->Initialize()->Save();*/

			if(isset($dashboard_id) && !is_null($dashboard_id)) {
				$sql = 'UPDATE DashboardSearch SET
					SearchText = \''.str_replace("'", "''", $this->search_text).'\'
					,IsActive = '.$this->rec_active.'
					,ModifiedTimestamp = getdate()
					WHERE DashboardSearchID = '.$dashboard_id;
			}
			else {
				$sql = 'INSERT INTO DashboardSearch(CompanyID, RecType, RecID, SearchText, IsActive)
					values('.$this->company_id.', '.$this->rec_type.', '.$this->record_id.', \''.str_replace("'", "''", $this->search_text).'\', '.$this->rec_active.')';
			}
			//echo "\n".$sql."\n";
			DbConnManager::GetDb('mpower')->Exec($sql);
		}
	}
	
	/* This function restroes account data in the dashboardsearch table. */
	function StoreAccountRecords() {
		$arr_account = array();
		
		if(!isset($this->company_id) || !($this->company_id > 0)) {						
			return false;
		} else {
			$fieldmap_obj = new fieldsMap();			
			$fieldmap_obj->companyId = $this->company_id;
			$mapped_accounts = $fieldmap_obj->getAccountMapFieldsFromDB();
			
			if(!is_null($mapped_accounts) && count($mapped_accounts) > 0) {
				foreach($mapped_accounts as $mapped_account) {			
					if((($mapped_account['BasicSearch'] == 1) && ($mapped_account['Enable'] == 1))
					|| (($mapped_account['IsCompanyName'] == 1) && ($mapped_account['Enable'] == 1))) {
						$arr_account[] = $mapped_account['FieldName'];
					}	
					//$arr_account[] = $mapped_account['FieldName'];
				}
			}
		}		
		
		//if(count($arr_account) > 0) {		
			$this->rec_type = JS_COMPANY;	
			
			if(isset($this->record_id) && ($this->record_id > 0)) {
				$this->StoreOneAccountRecord($arr_account);
			}	else {				
				$this->acc_row_num = 0;
				$sql = "SELECT count(AccountID) AS Cnt_Acc FROM Account WHERE CompanyID = ?";				
				$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->company_id));				
				$result = DbConnManager::GetDb('mpower')->Exec($sql);
				
				if($result[0]['Cnt_Acc'] > 0) {
					for($acc_cnt = 1; $acc_cnt <= $result[0]['Cnt_Acc']; $acc_cnt++) {
						$this->acc_row_num = $acc_cnt;
						$this->record_id = 0;
						$this->StoreOneAccountRecord($arr_account);
					}
				}
			}	
		//}
		return true;
	}
	
	function StoreOneAccountRecord($arr_account) {				
		if(!isset($this->company_id) || !($this->company_id > 0)) {						
			return false;
		}			
				
		if(isset($this->record_id) && ($this->record_id > 0)) {
			$sql = "SELECT AccountID, Inactive, DashboardSearchID, ".implode(', ',$arr_account)." FROM Account 
				LEFT JOIN DashboardSearch DS ON Account.AccountID = DS.RecID AND DS.RecType = 1
				WHERE Account.CompanyID = ? AND Account.AccountID = ? ";
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->company_id), array(DTYPE_INT, $this->record_id));
		}
		else if(isset($this->acc_row_num) && ($this->acc_row_num > 0)) {
			$sql = "select * from(SELECT CONVERT(int, ROW_NUMBER() OVER (ORDER BY AccountID)) AS RowNumber, AccountID, Inactive, ".implode(', ',$arr_account)."
				FROM Account where CompanyID=?) 
				AS myResult 
				LEFT JOIN DashboardSearch DS ON myResult.AccountID = DS.RecID AND DS.RecType = 1
				WHERE myResult.RowNumber=?";
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->company_id), array(DTYPE_INT, $this->acc_row_num));
			
			$this->acc_row_num = 0;
		}		
		
		$accounts_info = DbConnManager::GetDb('mpower')->Exec($sql);
		
		if(count($accounts_info) > 0) {		
				$acc_info = $accounts_info[0];
				$this->record_id = $acc_info['AccountID'];
				
				if(isset($acc_info['Inactive']) && ($acc_info['Inactive'] == 1)) {
					$this->rec_active = 0;
				}
				else {
					$this->rec_active = 1;
				}
				$search_fields_text = '';
				
				foreach($arr_account as $arr_account_field) {
					if(trim($search_fields_text) != '') {
						$search_fields_text .= ' ';							
					}
					if(trim($acc_info[$arr_account_field]) != '') {
						$search_fields_text .= $acc_info[$arr_account_field];							
					}
				}
				$this->search_text = trim($search_fields_text);
				//echo "\n".'Accountid--->'.$acc_info['AccountID'].'-->'.$this->search_text;
				if(isset($acc_info['DashboardSearchID']) && ($acc_info['DashboardSearchID'] > 0))
				{
					$this->StoreRecord($acc_info['DashboardSearchID']);
				}	
				else
				{
					$this->StoreRecord(null);
				}
		}
		return true;
	}
	
	/* This function restroes contact data in the dashboardsearch table. */
	function StoreContactRecords() {
		$arr_contact = array();		
		
		if(!isset($this->company_id) || !($this->company_id > 0)) {	
			return false;
		} else {
			$fieldmap_obj = new fieldsMap();
			$fieldmap_obj->companyId = $this->company_id;
			$mapped_contacts = $fieldmap_obj->getContactMapFieldsFromDB();
			
			if(!is_null($mapped_contacts) && count($mapped_contacts) > 0) {
				foreach($mapped_contacts as $mapped_contact) {		
					if((($mapped_contact['BasicSearch'] == 1) && ($mapped_contact['Enable'] == 1))
					|| (($mapped_contact['IsFirstName'] == 1) && ($mapped_contact['Enable'] == 1))
					|| (($mapped_contact['IsLastName'] == 1) && ($mapped_contact['Enable'] == 1))) {
						$arr_contact[] = $mapped_contact['FieldName'];					
					}
				}
			}
		}
		
		//if(count($arr_contact) > 0) {	
			$this->rec_type = JS_CONTACT;
					
			if(isset($this->record_id) && ($this->record_id > 0)) {
				$this->StoreOneContactRecord($arr_contact);
			}	else {
				$this->con_row_num = 0;
				$sql = "SELECT count(ContactID) AS Cnt_Con FROM Contact WHERE CompanyID = ?";				
				$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->company_id));			
				$result = DbConnManager::GetDb('mpower')->Exec($sql);
			
				if($result[0]['Cnt_Con'] > 0) {
					for($con_cnt = 1; $con_cnt <= $result[0]['Cnt_Con']; $con_cnt++) {
						$this->con_row_num = $con_cnt;
						$this->record_id = 0;
						$this->StoreOneContactRecord($arr_contact);
					}
				}
			}	
		//}
	}
	
	function StoreOneContactRecord($arr_contact) {
		if(isset($this->comp_name) && (trim($this->comp_name) != '')) {
			$comp_name_clause = "Account.".$this->comp_name." AS Comp_Name,";
		}
		
		if(isset($this->record_id) && ($this->record_id > 0)) {
			$sql = "SELECT  Contact.ContactID, Contact.Inactive, DashboardSearchID
				, Contact.".implode(', Contact.',$arr_contact)." FROM Contact 
				LEFT JOIN DashboardSearch DS ON Contact.ContactID = DS.RecID AND DS.RecType = 2				
				WHERE Contact.CompanyID = ? AND Contact.ContactID = ?";
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->company_id), array(DTYPE_INT, $this->record_id));
		} 
		else if(isset($this->con_row_num) && ($this->con_row_num > 0)) {			
			$sql = "select * from(SELECT CONVERT(int, ROW_NUMBER() OVER (ORDER BY ContactID)) AS RowNumber, Contact.ContactID, Contact.Inactive
				, Contact.".implode(', Contact.',$arr_contact)." FROM Contact 
				WHERE Contact.CompanyID = ?)  AS myResult 
				LEFT JOIN DashboardSearch DS ON myResult.ContactID = DS.RecID AND DS.RecType = 2
				where myResult.RowNumber=?";
				
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->company_id), array(DTYPE_INT, $this->con_row_num));
			
			$this->con_row_num = 0;
		}			
		$contacts_info = DbConnManager::GetDb('mpower')->Exec($sql);
		
		
		if(count($contacts_info) > 0) {
				$con_info = $contacts_info[0];					
				$this->record_id = $con_info['ContactID'];
				if(isset($con_info['Inactive']) && ($con_info['Inactive'] == 1)) {
					$this->rec_active = 0;
				}
				else {
					$this->rec_active = 1;
				}					
				$search_fields_text = '';
				
				foreach($arr_contact as $arr_contact_field) {
					if(trim($search_fields_text) != '') {
						$search_fields_text .= ' ';							
					}
					if(trim($con_info[$arr_contact_field]) != '') {
						$search_fields_text .= $con_info[$arr_contact_field];							
					}
				}
				$this->search_text = $search_fields_text;
				//echo "\n".'Contactid--->'.$con_info['ContactID'].'-->'.$this->search_text;
				if(isset($con_info['DashboardSearchID']) && ($con_info['DashboardSearchID'] > 0))
				{
					$this->StoreRecord($con_info['DashboardSearchID']);
				}
				else
				{
					$this->StoreRecord(null);
				}
		}
		return true;
	}
	
	/* This function restroes all account and contact data in the dashboardsearch table. */
	function StoreAllRecords() {
		$this->record_id = -1;
		$ret_acc_val = $this->StoreAccountRecords();
		$this->record_id = -1;
		$ret_con_val = $this->StoreContactRecords();
		
		if($ret_acc_val && $ret_con_val) {
			return true;
		} else {
			return false;
		}
	}
	
}