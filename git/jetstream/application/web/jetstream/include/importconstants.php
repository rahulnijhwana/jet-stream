<?php
define ('TYPE_COMPANY', 'Company');
define ('TABLE_COMPANY', 'Account');
define ('KEY_COMPANY', 'AccountID');

define ('TYPE_CONTACT', 'Contact');
define ('TABLE_CONTACT', 'Contact');
define ('KEY_CONTACT', 'ContactID');

define ('TYPE_ASSIGN', 'Assignment');

define ('TYPE_ASSIGN_COMPANY', 'Assign_Company');
define ('TABLE_ASSIGN_COMPANY', 'PeopleAccount');
define ('TABLE_ASSIGN_COMPANY_KEY', 'PeopleAccountID');

define ('TYPE_ASSIGN_CONTACT', 'Assign_Contact');
define ('TABLE_ASSIGN_CONTACT', 'PeopleContact');
define ('TABLE_ASSIGN_CONTACT_KEY', 'PeopleContactID');

define ('MAX_COLS', 50);
define ('MAX_ROWS', 9000);

define('FILE_UPLOAD_DIR', '/webtemp/attachments/filestore');
