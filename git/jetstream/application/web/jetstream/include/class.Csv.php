<?php
require_once BASE_PATH . '/libs/DataSource.php';
require_once BASE_PATH . '/include/class.DebugUtil.php';

class Csv extends File_CSV_DataSource {
	
	public function __construct($filename){
		$this->clean($filename);
		parent::__construct($filename);
	}
	
	public function saveAs($filename){
		
		$data = $this->getRawArray();
		
		$fp = fopen($filename, 'w');

		foreach ($data as $fields) {
			fputcsv($fp, $fields);
		}
		fclose($fp);
		
		return new Csv($filename);
	}
	
    public function setNewHeaders($list){
    	
        if (!$this->isSymmetric()) {
            return false;
        }
        if (!is_array($list)) {
            return false;
        }
        if (count($list) != count($this->headers)) {
            return false;
        }
        $this->headers = $list;
        
        return true;
    }
    /**
     * Replace backslashes with forward slashes
     * @param String $filename
     */
    private function clean($filename){
    	$str = file_get_contents($filename);
		$clean = preg_replace('/\\\\/', '/', $str);
		$fp = fopen($filename, 'w');
		fwrite($fp, $clean, strlen($clean));
    }
}
