<?php
require_once 'class.DbConnManager.php';
require_once 'class.SqlBuilder.php';

class ContactUtil {
	
	public static function getContact($contactId){
		$sql = "SELECT AccountID, Text01, Text02 FROM Contact WHERE ContactID = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $contactId));
		
		$contact = DbConnManager::GetDb('mpower')->GetOne($sql);
		
		return $contact;
	}
	
}
