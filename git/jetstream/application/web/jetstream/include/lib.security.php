<?php

require_once BASE_PATH . '/include/mpconstants.php';

session_start();

function SessionSet($var, $val, $time = 0) {
	$_SESSION[$var] = $val;
}

function SessionDel($name) {
	unset($_SESSION[$name]);
}

function SessionGet($name) {
	if (array_key_exists($name, $_SESSION)) {
		return $_SESSION[$name];
	} else {
		return false;
	}
}

function ValidateSession() {
	session_start();
	if (array_key_exists('auth', $_SESSION) && array_key_exists('company', $_SESSION) && array_key_exists('userid', $_SESSION)) {
		list($username, $token) = explode(':', $_SESSION['auth']);
		if (crypt($_SESSION['userid'] . $_SESSION['company'] . $_SERVER['HTTP_USER_AGENT'], $token) == $token) {
			return true;
		}
	}
	return false;
}

/**
 * Decrypts the encrypted string
 *
 * @param string $decodedStr
 * @return string
 */
function decryptUrl($decodedStr) {
	$seed_array = array('M','J','E','T','S','R','E','A','M');
	$decoded =  base64_decode($decodedStr);
	list($decoded,$letter) =  split("\+",$decoded);
	
	for ($i = 0; $i < count($seed_array); $i++) {
		if ($seed_array[$i] == $letter) break;
	}
	for ($j = 1; $j <= $i; $j++) {
		$decoded = base64_decode($decoded);
	}
	
	return $decoded;
}

/**
 * Encrypts the given string
 *
 * @param string $encodedStr
 * @return string
 */
function encryptUrl($encodedStr) {	
	$num = mt_rand(1, 6);
	
	for ($i=1;$i<=$num;$i++){
		$encodedStr = base64_encode($encodedStr);
	}
	
	$seed_array = array('M', 'J', 'E', 'T', 'S', 'R', 'E', 'A', 'M');
	$encodedStr = $encodedStr . "+" . $seed_array[$num];
	$encodedStr = base64_encode($encodedStr);
	return $encodedStr;
}

?>
