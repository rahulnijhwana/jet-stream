<?php
/**
 * @package database
 */

require_once 'lib.array.php';
require_once 'mpconstants.php';

function SqlBuilder() {
    return new SqlBuilder();
}

/**
 * Prepares and returns SQL statements
 */
class SqlBuilder
{
    protected $sql;
    protected $param_count;

    public function __construct($raw_sql = NULL) {
        if (!is_null($raw_sql)) {
            $this->LoadSql($raw_sql);
        }
        return $this;
    }

    /**
     * Load in the raw SQL statement
     * Values should be replaced with qustion marks
     *
     * @param string $sql
     */
    public function LoadSql($sql) {
        $this->param_count = substr_count($sql, '?');
        $this->sql = str_replace('?', '%s', $sql);
        return $this;
    }

	public function BuildInsert($table, array $field_list) {
		$insert_fields = array();
		foreach ($field_list as $field_name => $field) {
			$insert_fields[$field_name] = $this->EscapeValue($field[1], $field[0]);
		}
		$sql = 'insert into ' . $table . ' (' . implode(', ', array_keys($insert_fields)) . ') values (' . implode(', ', $insert_fields) . ')';
		return $sql;
	}

	public function BuildSql() {
		$params = func_get_args();
		return $this->BuildSqlParam($params);
	}
	
    /**
     * Merges the raw SQL statement with the variables
     * Returns the completed SQL statement
     *
     * @param string $types
     * @param array $param
     * @return string
     */
    public function BuildSqlParam($params) {
    	// $params = func_get_args();  // ArrayFlatten(func_get_args());

		$esc_params = array();
		foreach ($params as $param) {
			// print_r($param);
			if (is_array($param)) {
				$type = $param[0];
				$values = (is_array($param[1])) ? $param[1] : array($param[1]);
				foreach($values as $value) {
					$esc_params[] = $this->EscapeValue($value, $type);
				}
			}
		}

        if (count($esc_params) != $this->param_count) {
            echo $this->sql . '<pre>';
            print_r($params);
            print_r(func_get_args());
            echo '</pre>';
            throw new Exception('SQL parameter count mismatch:  Expected ' . $this->param_count . ', received ' . count($esc_params) . ".");
        }

        $sql = vsprintf($this->sql, $esc_params);
        return $sql;
    }

    /**
     * Ensures that the value matches the intended data type and escapes strings
     * This needs to be solid to avoid SQL injection
     *
     * @param string $type
     * @param string $value
     * @return string
     */
    private function EscapeValue($value, $type) {
        if (is_null($value)) {
            return 'NULL';
        }

        switch ($type) {
            case DTYPE_BOOL:  // Bool:
				if (!is_bool($value) && $value !== 0 && $value !== 1) {
                    var_dump($value);
                    echo $this->sql . '<br>';
                    throw new Exception("Expected type 'bool' for parameter.");
				}
				if (is_bool($value) && $value == false) $value = 0;
            	break;
            case DTYPE_INT:  // Integer
                if ((string)(int)$value !== (string)$value) {
                    var_dump($value);
                    echo $this->sql . '<br>';
                    throw new Exception("Expected type 'int' for parameter.");
                }
                break;
            case DTYPE_FLOAT:  // Double
                if (!is_numeric($value)) {
	                var_dump($value);
	                echo $this->sql . '<br>';
                    throw new Exception("Expected 'double' for parameter.");
                }
                break;
            case DTYPE_TIME:  // Date (should be passed a PHP date object)
                if (is_numeric($value)) {
					$value = TimestampToMsSql($value);
                }
            case DTYPE_STRING:  // String - ensure it is SQL injection safe
                $string = filter_var($value, FILTER_UNSAFE_RAW, FILTER_FLAG_ENCODE_HIGH);
                $string = preg_replace('/\x00/', '', $string);
                $string = str_replace("\0", "", $string);  // Remove null characters
                $string = str_replace("'", "''", $string);
                $string = str_replace("&#39;", "''", $string);
                $string = "'$string'";
                $string = stripslashes($string);
                $value = $string;
                // $string = str_replace("\0", "[NULL]", $string);
                break;
            default:
                echo $this->sql . '<br>';
                echo $value . '<br>';
                throw new Exception('Unknown SQL parameter type: ' . $type);
        }
        return $value;
    }
}

function ArrayQm(array $array) {
	return implode(', ', array_fill(0, count($array), '?'));
}

?>