ALTER TABLE PeopleAccount ADD AssignedTo bit NOT NULL DEFAULT (1);
ALTER TABLE PeopleContact ADD AssignedTo bit NOT NULL DEFAULT (1);