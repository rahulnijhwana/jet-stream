<?php
/* This script copies all tables from one db to another existing empty db. */
$myServer = "192.168.10.219";
$myUser = "sa";
$myPass = "sa99";

/* This is the existing db. */
$myDb = "mpower";

/* This is the name of the new db. Need to create the db before running this script. */
$mynewDb = "jetint";

$dbhandle = connect($myServer, $myUser, $myPass, $myDb);
if(!$dbhandle) {
	die('Connection Error');
}

/* Get the list of table names from the existing db. */
$sql = "select name from SYSOBJECTS where TYPE = 'U' order by NAME";
$result = mssql_query($sql) or die("MS-Query Error in select-query");
$tableArr = array();

if(mssql_num_rows($result) > 0) {
	while($row = mssql_fetch_assoc($result)) {
		$tableArr[] = $row['name'];
	}
}

// close the connection
mssql_close($dbhandle);

/* Copy the tables from one db to another db. */
foreach($tableArr as $table) {
	$dbhandle = connect($myServer, $myUser, $myPass, $mynewDb);
	
	$sql = 'SELECT * INTO '.$mynewDb.'.dbo.['.$table.'] FROM '.$myDb.'.dbo.['.$table.']';
	//echo $sql."\n";
	if(@mssql_query($sql)) {
		echo 'Success'.$table."\n";
	}
	mssql_close($dbhandle);
}

/* Function to connect to the db. */
function connect($myServer, $myUser, $myPass, $myDB) {
	
	// connection to the database
	$dbhandle = @mssql_connect($myServer, $myUser, $myPass);
	if($dbhandle) {
		// select a database to work with
		$selected = @mssql_select_db($myDB, $dbhandle);
		if($selected) {
			return $dbhandle;
		}
	}
	return false;
}
?>