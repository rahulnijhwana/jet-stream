SELECT *
FROM (
	SELECT ROW_NUMBER() OVER (ORDER BY SortName ASC) AS RowNumber, SearchQuery.*
	FROM (
		SELECT 'account_' + CAST(Account.AccountID AS varchar) AS UniqueID
			, NULL as ContactID
			, Account.AccountID
			, NULL as ContactName
			, '' + Account.Text01 + '' AS AccountName
			, '' + Account.Text01 + '' AS SortName
			, Account.PrivateAccount as Private
			, Account.Inactive
			, Account.Text01 AS CompText01
			, NULL as ConText01
			, NULL as ConText02
			, Account.Text04 AS City
			, opt.OptionName AS State
					
		FROM Account
		/* TODO
		 * what's Select04 and OPTION table, besso
		 */
		LEFT JOIN [Option] AS opt on Account.Select04 = opt.OptionID
		WHERE Account.CompanyID = 508 
		AND (Account.Deleted != 1)  AND (Account.Inactive != 1)  
		AND AccountID in (
			SELECT RecID 
			FROM DashboardSearch 
			WHERE RecType = 1
		 	AND CompanyID = 508)
		AND (PrivateAccount = 0 
			  	OR (PrivateAccount = 1
				AND 8449 in (
					SELECT PersonID 
					FROM PeopleAccount 
					WHERE AccountID = Account.AccountID AND AssignedTo = 1
					) 
				)
			)
		UNION 
			SELECT 'contact_' + CAST(Contact.ContactID AS varchar) AS UniqueID
			, Contact.ContactID
			, Account.AccountID
			, '' + Contact.Text01 + ' ' + Contact.Text02 + '' as ContactName
			, '' + Account.Text01 + '' as AccountName
			, '' + Contact.Text02 + ' ' + Contact.Text01 + '' + ' ' + '' + Account.Text01 + '' AS SortName
			, Contact.PrivateContact as Private
			, Contact.Inactive
			, Account.Text01 AS CompText01
			, Contact.Text01 AS ConText01
			, Contact.Text02 AS ConText02
			, Contact.Text11 AS City
			, opt.OptionName AS State
			FROM Contact
			LEFT JOIN Account on Contact.AccountID = Account.AccountID
			LEFT JOIN [Option] AS opt on Contact.Select10 = opt.OptionID
			WHERE Contact.CompanyID = 508 
			  AND (Contact.Deleted != 1)  
			  AND (Contact.Inactive != 1)  
			  AND ContactID in (
				SELECT RecID 
				FROM DashboardSearch 
				WHERE RecType = 2
					AND CompanyID = 508) AND (PrivateContact = 0 OR (PrivateContact = 1 
				AND 8449 in (
					SELECT PersonID 
					FROM PeopleContact 
					WHERE ContactID = Contact.ContactID AND AssignedTo = 1) )) ) AS SearchQuery
	) _myResults
WHERE 
	RowNumber BETWEEN 1 AND 25