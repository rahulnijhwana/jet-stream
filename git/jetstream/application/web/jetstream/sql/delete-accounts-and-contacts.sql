/*
1: Get the CompanyID
SELECT * FROM company WHERE DirectoryName = 'fsg01'
765

2: Verify the expected number of Contacts
SELECT COUNT(*) FROM Contact WHERE CompanyID = 765;

2.1: Clear out the Contact records once the count is verified
DELETE FROM Contact WHERE CompanyID = 765;

3: Verify the number of Account(Company) Records
SELECT COUNT(*) FROM Account WHERE CompanyID = 765;

3.1: Delete the matching Account(Company) Records
DELETE FROM Account WHERE CompanyID = 765;


Start:

SELECT COUNT(*) FROM Contact WHERE CompanyID = 765;
SELECT COUNT(*) FROM Account WHERE CompanyID = 765;

Total Expected: 7938 = (3950 Contact) + (3988 Account)

DELETE FROM Contact WHERE CompanyID = 765;
DELETE FROM Account WHERE CompanyID = 765;

End:
*/

SELECT COUNT(*) FROM Contact WHERE CompanyID = 765;
SELECT COUNT(*) FROM Account WHERE CompanyID = 765;

DELETE FROM Contact WHERE CompanyID = 765;
DELETE FROM Account WHERE CompanyID = 765;
