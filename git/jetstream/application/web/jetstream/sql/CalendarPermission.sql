USE [mpower]
GO
/****** Object:  Table [dbo].[CalendarPermission]    Script Date: 02/06/2009 19:25:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CalendarPermission](
	[CalendarPermissionID] [int] IDENTITY(1,1) NOT NULL,
	[PersonID] [int] NULL,
	[ReadPermission] [bit] NULL,
	[WritePermission] [bit] NULL,
	[OwnerPersonID] [int] NULL,
	[LockPermission] [bit] NULL,
 CONSTRAINT [PK_CalendarPermission] PRIMARY KEY CLUSTERED 
(
	[CalendarPermissionID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]