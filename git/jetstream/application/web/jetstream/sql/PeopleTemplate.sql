USE [mpower]
GO
/****** Object:  Table [dbo].[PeopleTemplate]    Script Date: 01/05/2009 21:59:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PeopleTemplate](
	[PeopleTemplateID] [int] IDENTITY(1,1) NOT NULL,
	[PersonID] [int] NOT NULL,
	[NoteTemplateID] [int] NOT NULL,
	[CompanyID] [int] NOT NULL,
 CONSTRAINT [PK_PeopleTemplate] PRIMARY KEY CLUSTERED 
(
	[PeopleTemplateID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
