USE [mpower]
GO
/****** Object:  Table [dbo].[NoteMap]    Script Date: 12/02/2008 18:03:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NoteMap](
	[NoteMapID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[FieldType] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LabelName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OptionType] [int] NULL,
	[Deleted] [bit] NOT NULL CONSTRAINT [DF_NoteMap_Deleted]  DEFAULT ((0)),
 CONSTRAINT [PK_NoteMap] PRIMARY KEY CLUSTERED 
(
	[NoteMapID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF