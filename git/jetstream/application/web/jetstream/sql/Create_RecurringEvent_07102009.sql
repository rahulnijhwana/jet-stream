/****** Object:  Table [dbo].[RecurringEvent]    Script Date: 07/10/2009 02:42:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RecurringEvent](
	[RecurringEventID] [int] IDENTITY(1,1) NOT NULL,
	[EventID] [int] NULL,
	[Type] [smallint] NULL,
	[StartTime] [varchar](50) NULL,
	[Duration] [varchar](50) NULL,
	[PersonID] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Interval] [varchar](250) NULL,
	[IsActive] [bit] NULL,
	[CompanyID] [int] NULL,
	[UserID] [int] NULL,
 CONSTRAINT [PK_RecurringEvents] PRIMARY KEY CLUSTERED 
(
	[RecurringEventID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
