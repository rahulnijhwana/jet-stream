USE [mpower]
GO
/****** Object:  Table [dbo].[Notes]    Script Date: 09/11/2008 17:26:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Notes](
	[NoteID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreationDate] [datetime] NULL,
	[Subject] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[NoteText] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ObjectType] [smallint] NOT NULL,
	[ObjectReferer] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF