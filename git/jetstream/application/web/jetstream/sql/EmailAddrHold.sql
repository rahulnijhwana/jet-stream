/****** Object:  Table [dbo].[EmailAddrHold]    Script Date: 11/19/2008 19:35:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmailAddrHold](
	[EmailAddrHoldID] [int] IDENTITY(1,1) NOT NULL,
	[PersonID] [int] NOT NULL,
	[EmailAddrDisplayName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EmailAddr] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsEmailAddrHold] [bit] NULL,
 CONSTRAINT [PK_EmailAddrHold] PRIMARY KEY CLUSTERED 
(
	[EmailAddrHoldID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF