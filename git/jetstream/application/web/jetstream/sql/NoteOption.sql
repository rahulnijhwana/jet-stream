USE [mpower]
GO
/****** Object:  Table [dbo].[NoteOption]    Script Date: 12/04/2008 20:01:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NoteOption](
	[NoteOptionID] [int] IDENTITY(1,1) NOT NULL,
	[OptionName] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NoteMapID] [int] NOT NULL,
 CONSTRAINT [PK_NoteOption] PRIMARY KEY CLUSTERED 
(
	[NoteOptionID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF