USE [mpower]
GO
/****** Object:  Table [dbo].[User]    Script Date: 07/08/2008 19:24:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastName] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UserName] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Password] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CompanyID] [int] NULL,
	[Email] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StartDate] [datetime] NULL,
	[IsSalesPerson] [int] NULL,
	[SupervisorID] [int] NULL,
	[Level] [int] NULL,
	[Deleted] [int] NULL,
	[DateDeleted] [datetime] NULL,
	[LastUsedIp] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastUsed] [smalldatetime] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF