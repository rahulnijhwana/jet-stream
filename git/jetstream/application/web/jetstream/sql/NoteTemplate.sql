USE [mpower]
GO
/****** Object:  Table [dbo].[NoteTemplate]    Script Date: 01/02/2009 19:01:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NoteTemplate](
	[NoteTemplateID] [int] IDENTITY(1,1) NOT NULL,
	[TemplateName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CompanyID] [int] NOT NULL,
	[FormType] [int] NOT NULL,
	[Deleted] [bit] NOT NULL CONSTRAINT [DF_NoteTemplate_Deleted]  DEFAULT ((0)),
 CONSTRAINT [PK_NoteTemplate] PRIMARY KEY CLUSTERED 
(
	[NoteTemplateID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF