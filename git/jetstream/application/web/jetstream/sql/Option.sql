USE [mpower]
GO
/****** Object:  Table [dbo].[Option]    Script Date: 09/12/2008 18:45:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Option](
	[OptionID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[AccountMapID] [int] NULL,
	[ContactMapID] [int] NULL,
	[OptionName] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OptionSetID] [int] NULL,
 CONSTRAINT [PK_AccountContactOption] PRIMARY KEY CLUSTERED 
(
	[OptionID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF