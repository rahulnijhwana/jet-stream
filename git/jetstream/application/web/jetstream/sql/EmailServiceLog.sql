SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmailServiceLog](
	[EmailServiceLogID] [bigint] IDENTITY(1,1) NOT NULL,
	[EmailServiceToken] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CompanyID] [int] NULL,
	[PersonID] [int] NULL,
	[EmailEntryID] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EmailUserRole] [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Timestamp] [datetime] NULL,
 CONSTRAINT [PK_EmailServiceLog] PRIMARY KEY CLUSTERED 
(
	[EmailServiceLogID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF