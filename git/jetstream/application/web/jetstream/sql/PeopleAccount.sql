USE [mpower]
GO
/****** Object:  Table [dbo].[PeopleAccount]    Script Date: 11/03/2008 18:32:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PeopleAccount](
	[PeopleAccountID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NOT NULL,
	[PersonID] [int] NOT NULL,
	[CreatedBy] [bit] NOT NULL CONSTRAINT [DF_PeopleAccount_CreatedBy]  DEFAULT ((0))
) ON [PRIMARY]
