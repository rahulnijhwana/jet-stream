USE [mpower]
GO
/****** Object:  Table [dbo].[ContactFields]    Script Date: 09/08/2008 19:26:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactFields](
	[FieldID] [int] IDENTITY(1,1) NOT NULL,
	[FieldName] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AccountID] [int] NOT NULL,
	[DataType] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_ContactFields] PRIMARY KEY CLUSTERED 
(
	[FieldID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF