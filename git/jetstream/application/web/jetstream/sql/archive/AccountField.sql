USE [mpower]
GO
/****** Object:  Table [dbo].[AccountField]    Script Date: 09/08/2008 20:03:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountField](
	[AccountFieldID] [int] IDENTITY(1,1) NOT NULL,
	[FieldName] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DataType] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_AccountFields] PRIMARY KEY CLUSTERED 
(
	[AccountFieldID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF