USE [mpower]
GO
/****** Object:  Table [dbo].[AccountFieldsMap]    Script Date: 09/08/2008 19:13:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountFieldsMap](
	[MapID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[FieldID] [int] NULL,
	[LabelName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_AccountFieldsMap] PRIMARY KEY CLUSTERED 
(
	[MapID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF