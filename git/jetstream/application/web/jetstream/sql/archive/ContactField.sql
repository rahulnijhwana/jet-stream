USE [mpower]
GO
/****** Object:  Table [dbo].[ContactField]    Script Date: 09/08/2008 20:06:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactField](
	[ContactFieldID] [int] IDENTITY(1,1) NOT NULL,
	[FieldName] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DataType] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_ContactFields] PRIMARY KEY CLUSTERED 
(
	[ContactFieldID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF