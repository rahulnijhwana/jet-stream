USE [mpower]
GO
/****** Object:  Table [dbo].[AccountFieldMap]    Script Date: 09/08/2008 20:04:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountFieldMap](
	[AccountFieldMapID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[AccountFieldID] [int] NOT NULL,
	[LabelName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_AccountFieldsMap] PRIMARY KEY CLUSTERED 
(
	[AccountFieldMapID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF