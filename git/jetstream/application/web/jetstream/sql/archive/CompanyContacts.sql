USE [mpower]
GO
/****** Object:  Table [dbo].[CompanyContacts]    Script Date: 09/08/2008 19:26:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompanyContacts](
	[ContactID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NOT NULL,
	[Name] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Attribute1] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Attribute2] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Attribute3] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Attribute4] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Attribute5] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Attribute6] [datetime] NULL,
	[Attribute7] [datetime] NULL,
	[Attribute8] [int] NULL,
	[Attribute9] [int] NULL,
	[Attribute10] [int] NULL,
 CONSTRAINT [PK_CompanyContacts] PRIMARY KEY CLUSTERED 
(
	[ContactID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF