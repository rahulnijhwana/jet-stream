USE [mpower]
GO
/****** Object:  Table [dbo].[ContactMapOutlook]    Script Date: 07/15/2008 19:53:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactMapOutlook](
	[MapID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[FieldID] [int] NULL,
	[Name] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Min] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Max] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Expression] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_ContactMapOutlook] PRIMARY KEY CLUSTERED 
(
	[MapID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF