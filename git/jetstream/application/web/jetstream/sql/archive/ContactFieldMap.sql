USE [mpower]
GO
/****** Object:  Table [dbo].[ContactFieldMap]    Script Date: 09/08/2008 20:05:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactFieldMap](
	[ContactFieldMapID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[ContactFieldID] [int] NOT NULL,
	[LabelName] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_ContactMap] PRIMARY KEY CLUSTERED 
(
	[ContactFieldMapID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF