USE [mpower]
GO
/****** Object:  Table [dbo].[CompanyAccountContactTemp]    Script Date: 01/20/2009 19:50:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompanyAccountContactTemp](
	[CompanyAccountContactTempID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[TemplateData] [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RecType] [int] NOT NULL,
 CONSTRAINT [PK_CompanyAccountContactTemp] PRIMARY KEY CLUSTERED 
(
	[CompanyAccountContactTempID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF