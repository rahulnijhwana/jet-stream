USE [mpasac]
GO
/****** Object:  StoredProcedure [dbo].[dosnaptest]    Script Date: 12/30/2008 21:46:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


CREATE                   PROCEDURE [dbo].[dosnaptest]

AS
declare @removedcount int, @stalledcount int, @dpcount int
declare @removedtotal float, @stalledtotal float, @dptotal float
declare @RemovedAvgAge float, @StalledAvgAge float, @DPAvgAge float
declare @age float
declare @FM int, @IP int, @SIP int, @DP int, @SDP int, @Closed int, @Removed int
declare @Category int, @ActualCloseDate smalldatetime, @RemoveDate smalldatetime
declare @FirstMeeting smalldatetime, @DPDate smalldatetime
declare @ActualDollarAmount money, @TotalSales money
declare @ThisPerson int
declare @now smalldatetime
declare @DealID int, @company int
declare @weekday varchar(15), @dayspan int, @CloseDiff int, @RemoveDiff int
declare @TotSched int
declare @SIPDate smalldatetime, @SDPDate smalldatetime
declare @RatingClosed int
declare @FM2 float, @FM3 float, @IP2 float, @IP3 float, @DP2 float, @DP3 float, @C2 float, @C3 float
declare @FMThreshold int, @IPThreshold int, @DPThreshold int, @CloseThreshold int
declare @FMValue int, @IPValue int, @DPValue int, @CValue int
declare @RankNew int, @RankExperienced int, @Rating int, @MonthsNew int, @StartDAte smalldatetime
declare @VLevel float, @ValCount float, @ValTotal float, @ValAvg float
-- rjp 8-24-07: use getutcdate
set @now = getutcdate();

set @weekday = datename(weekday, @now)

-- Don't run this on Sunday or Monday morning.
if @weekday = "Sunday" or @weekday = "Monday"
	return

-- Tally three days of closings and removals on the Tuesday run, one day otherwise.
if @weekday = "Tuesday"
	set @dayspan = 3
else
	set @dayspan = 1

-- only process companies that are flagged for this operation
declare Companies cursor
	for select CompanyID, MonthsConsideredNew from company where snapshot = 1

open Companies
fetch next from Companies into @company, @MonthsNew
while @@FETCH_STATUS = 0
	begin
	-- get the people for the company that have opportunities
	declare Opps cursor
		for select distinct PersonID from mpasac.opportunities where CompanyID = @company
	
	open Opps
	fetch next from Opps into @ThisPerson;
	while @@FETCH_STATUS = 0
		begin
		set @FM = 0
		set @IP = 0
		set @SIP = 0
		set @DP = 0
		set @SDP = 0
		set @Closed = 0
		set @RatingClosed = 0
		set @Removed = 0
		set @TotalSales = 0
		set @stalledcount = 0
		set @removedcount = 0
		set @stalledcount = 0
		set @dpcount = 0
		set @removedtotal = 0
		set @stalledtotal = 0
		set @dptotal = 0
		set @TotSched = 0
		set @FM2 = 0
		set @FM3 = 0
		set @IP2 = 0
		set @IP3 = 0
		set @DP2 = 0
		set @DP3 = 0
		set @C2 = 0
		set @C3 = 0
		set @ValCount = 0
		set @ValTotal = 0
		set @ValAvg = 0
		set @RemoveDiff = -1

		declare Thresholds cursor
			for select FMThreshold, IPThreshold, DPThreshold, CloseThreshold, StartDate
			from mpasac.people where PersonID = @ThisPerson
		open Thresholds
		fetch next from Thresholds into @FMThreshold, @IPThreshold, @DPThreshold, @CloseThreshold, @StartDate
		close Thresholds
		deallocate Thresholds

		-- get opportunities for this person
		declare OppDetail cursor
			for select DealID, Category, ActualCloseDate, RemoveDate, FirstMeeting, ActualDollarAmount, VLevel
			from mpasac.opportunities
			where CompanyID = @company
			and PersonID = @ThisPerson
			order by PersonID
		open OppDetail

		fetch next from OppDetail into @DealID, @Category, @ActualCloseDate, @RemoveDate, @FirstMeeting, @ActualDollarAmount, @VLevel
		while @@FETCH_STATUS = 0
			begin
			-- needed for determining if we look at close or removal records
			set @CloseDiff = datediff(day, @ActualCloseDate, @now)
			set @RemoveDiff = datediff(day, @RemoveDate, @now)

			if @Category = 1	-- first meeting
				begin
				set @FM = @FM + 1
				set @TotSched = @TotSched + 1
				end
			else if @Category = 2	-- information phase
				begin
				set @IP = @IP + 1
				set @TotSched = @TotSched + 1
				end
			else if @Category = 3	-- stalled information phase
				begin
				set @SIP = @SIP + 1
				declare CheckSIP cursor
					for select max(WhenChanged) from Log
					where DealID = @DealID
					and CompanyID = @company
					and PersonID = @ThisPerson
					and TransactionID = 1
				open CheckSIP
				fetch next from CheckSIP into @SIPDate
				close CheckSIP
				deallocate CheckSIP
				-- rjp 5-17-04: guard against NULL age due to missing log entry
				if @SIPDate is not null
					begin
					set @stalledcount = @stalledcount + 1
					-- rjp 5-19-04: diff against today, not FM
					set @age = datediff(day, @SIPDate, @now)
					set @stalledtotal = @stalledtotal + @age
					end
				end
			else if @Category = 4	-- decision point
				begin
				set @DP = @DP + 1
				-- find log entry to determine age
				declare CheckLog cursor
					for select WhenChanged from Log
					where DealID = @DealID
					and CompanyID = @company
					and PersonID = @ThisPerson
					and TransactionID = 1
					and LogInt = 4
				open CheckLog
				fetch next from CheckLog into @DPDate
				close CheckLog
				deallocate CheckLog
				-- rjp 5-17-04: no date? don't average.
				if @DPDate is not null
					begin
					set @dpcount = @dpcount + 1
					set @age = datediff(day, @DPDate, @now)
					set @dptotal = @dptotal + @age
					end
				set @TotSched = @TotSched + 1
				end
			else if @Category = 5	-- stalled decision point
				begin
				set @SDP = @SDP + 1
				declare CheckSDP cursor
					for select max(WhenChanged) from Log
					where DealID = @DealID
					and CompanyID = @company
					and TransactionID = 1
				open CheckSDP
				fetch next from CheckSDP into @SDPDate
				close CheckSDP
				deallocate CheckSDP
				-- rjp 5-17-04: guard against NULL results from missing log
				-- entries; don't figure them into the average.
				if @SDPDate is not null
					begin
					set @stalledcount = @stalledcount + 1
					-- rjp 5-19-04: diff against today, not FM
					set @age = datediff(day, @SDPDate, @now)
					set @stalledtotal = @stalledtotal + @age
					end
				end
			else if @Category = 6 and @CloseDiff > 0 and @CloseDiff <= @dayspan	-- closed
				begin
				set @Closed = @Closed + 1;
				set @TotalSales = @TotalSales + @ActualDollarAmount
				end
			-- 11/03/04  Avg removed age refers to avg age of opps removed TODAY
			-- not avg age of removed opps on board  Jim, per Al Lencioni
			else if @Category = 9 and @RemoveDiff > 0 and @RemoveDiff <= @dayspan	-- removed
				begin
				set @Removed = @Removed + 1
				set @removedcount = @removedcount + 1
				-- set @age = datediff(day, @RemoveDate, @now)
				set @age = datediff(day, @FirstMeeting, @now)
				set @removedtotal = @removedtotal + @age
				end

			if @Category = 6 and @CloseDiff <= 30
				begin
				set @RatingClosed = @RatingClosed + 1
				end

			if @VLevel > 0 and @VLevel < 256
				begin
				set @ValCount = @ValCount + 1
				set @ValTotal = @ValTotal + @VLevel
				end 

			fetch next from OppDetail into @DealID, @Category, @ActualCloseDate, @RemoveDate, @FirstMeeting, @ActualDollarAmount, @VLevel
			end
		close OppDetail
		deallocate OppDetail

		-- calc averages
		if @removedcount <> 0
			set @RemovedAvgAge = @removedtotal / @removedcount
		else
			set @RemovedAvgAge = null
	
		if @stalledcount <> 0
			set @StalledAvgAge = @stalledtotal / @stalledcount
		else
			set @StalledAvgAge = null
	
		if @dpcount <> 0
			set @DPAvgAge = @dptotal / @dpcount
		else
			set @DPAvgAge = null

		-- calc ratings
		set @FM2 = @FMThreshold * 0.66
		set @FM3 = @FMThreshold * 1.5
		set @IP2 = @IPThreshold * 0.66
		set @IP3 = @IPThreshold * 1.5
		set @DP2 = @DPThreshold * 0.66
		set @DP3 = @DPThreshold * 1.5
		set @C2 = @CloseThreshold * 0.66
		set @C3 = @CloseThreshold * 1.5

		if @FM = 0
			set @FMValue = 0
		else if @FM < @FM2
			set @FMValue = 1
		else if @FM < @FM3
			set @FMValue = 2
		else
			set @FMValue = 3

		if @IP = 0
			set @IPValue = 0
		else if @IP < @IP2
			set @IPValue = 1
		else if @IP < @IP3
			set @IPValue = 2
		else
			set @IPValue = 3

		if @DP = 0
			set @DPValue = 0
		else if @DP < @DP2
			set @DPValue = 1
		else if @DP < @DP3
			set @DPValue = 2
		else
			set @DPValue = 3

		if @RatingClosed = 0
			set @CValue = 0
		else if @RatingClosed < @C2
			set @CValue = 1
		else if @RatingClosed < @C3
			set @CValue = 2
		else
			set @CValue = 3

		declare Analysis cursor
			for select RankNew, RankExperienced from mpasac.analysis
			where FMValue = @FMValue and IPValue = @IPValue
			and DPValue = @DPValue and CloseValue = @CValue
		open Analysis
		fetch next from Analysis into @RankNew, @RankExperienced
		close Analysis
		deallocate Analysis

		-- what's our rating?
		if datediff(month, @StartDate, @now) <= @MonthsNew
			set @Rating = @RankNew
		else
			set @Rating = @RankExperienced

		-- what's our average valuation?
		if @ValCount > 0
			set @ValAvg = @ValTotal / @ValCount
		else
			set @ValAvg = null
		-- dump the data
		insert into snapback (CompanyID, SalesID, SnapDate, FM, IP, SIP, DP, SDP, Closed, TotalSales, Removed, RemovedAvgAge, StalledAvgAge, DPAvgAge, TotSched, TotStalled, Rating, ValAvg)
			values(@company, @ThisPerson, @now, @FM, @IP, @SIP, @DP, @SDP, @Closed, @TotalSales, @Removed, @RemovedAvgAge, @StalledAvgAge, @DPAvgAge, @TotSched, @stalledcount, @Rating, @ValAvg)

		-- next!
		fetch next from Opps into @ThisPerson
		end
	close Opps
	deallocate Opps
	fetch next from Companies into @company, @MonthsNew
	end
close Companies
deallocate Companies






