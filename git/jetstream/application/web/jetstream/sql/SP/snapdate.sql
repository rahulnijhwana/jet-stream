USE [mpasac]
GO
/****** Object:  StoredProcedure [dbo].[snapdate]    Script Date: 12/30/2008 21:40:18 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO









CREATE          PROCEDURE [dbo].[snapdate]
@when varchar(255)
AS
declare @removedcount int, @stalledcount int, @dpcount int
declare @removedtotal float, @stalledtotal float, @dptotal float
declare @RemovedAvgAge float, @StalledAvgAge float, @DPAvgAge float
declare @age float
declare @FM int, @IP int, @SIP int, @DP int, @SDP int, @Closed int, @Removed int
declare @Category int, @ActualCloseDate smalldatetime, @RemoveDate smalldatetime
declare @FirstMeeting smalldatetime, @DPDate smalldatetime
declare @ActualDollarAmount money, @TotalSales money
declare @ThisPerson int
declare @now smalldatetime
declare @DealID int, @company int
declare @weekday varchar(15), @dayspan int, @CloseDiff int, @RemoveDiff int
declare @TotSched int
declare @SIPDate smalldatetime, @SDPDate smalldatetime

set @now = cast(@when + ' 02:30:00' as smalldatetime);

set @weekday = datename(weekday, @now)

-- Don't run this on Sunday or Monday morning.
if @weekday = "Sunday" or @weekday = "Monday"
	return

-- Tally three days of closings and removals on the Tuesday run, one day otherwise.
if @weekday = "Tuesday"
	set @dayspan = 3
else
	set @dayspan = 1

-- only process companies that are flagged for this operation
declare Companies cursor
	for select CompanyID from company where snapshot = 1

open Companies
fetch next from Companies into @company
while @@FETCH_STATUS = 0
	begin
	-- get the people for the company that have opportunities
	declare Opps cursor
		for select distinct PersonID from mpasac.opportunities where CompanyID = @company
	
	open Opps
	fetch next from Opps into @ThisPerson;
	while @@FETCH_STATUS = 0
		begin
		set @FM = 0
		set @IP = 0
		set @SIP = 0
		set @DP = 0
		set @SDP = 0
		set @Closed = 0
		set @Removed = 0
		set @TotalSales = 0
		set @stalledcount = 0
		set @removedcount = 0
		set @stalledcount = 0
		set @dpcount = 0
		set @removedtotal = 0
		set @stalledtotal = 0
		set @dptotal = 0
		set @TotSched = 0

		-- get opportunities for this person
		declare OppDetail cursor
			for select DealID, Category, ActualCloseDate, RemoveDate, FirstMeeting, ActualDollarAmount
			from mpasac.opportunities
			where CompanyID = @company
			and PersonID = @ThisPerson
			order by PersonID
		open OppDetail

		fetch next from OppDetail into @DealID, @Category, @ActualCloseDate, @RemoveDate, @FirstMeeting, @ActualDollarAmount
		while @@FETCH_STATUS = 0
			begin
			-- needed for determining if we look at close or removal records
			set @CloseDiff = datediff(day, @ActualCloseDate, @now)
			set @RemoveDiff = datediff(day, @RemoveDate, @now)

			if @Category = 1	-- first meeting
				begin
				set @FM = @FM + 1
				set @TotSched = @TotSched + 1
				end
			else if @Category = 2	-- information phase
				begin
				set @IP = @IP + 1
				set @TotSched = @TotSched + 1
				end
			else if @Category = 3	-- stalled information phase
				begin
				set @SIP = @SIP + 1
				set @stalledcount = @stalledcount + 1
				declare CheckSIP cursor
					for select max(WhenChanged) from Log
					where DealID = @DealID
					and CompanyID = @company
					and PersonID = @ThisPerson
					and TransactionID = 1
				open CheckSIP
				fetch next from CheckSIP into @SIPDate
				close CheckSIP
				deallocate CheckSIP
				set @age = datediff(day, @FirstMeeting, @SIPDate)
				set @stalledtotal = @stalledtotal + @age
				end
			else if @Category = 4	-- decision point
				begin
				set @DP = @DP + 1
				set @dpcount = @dpcount + 1
				-- find log entry to determine age
				declare CheckLog cursor
					for select WhenChanged from Log
					where DealID = @DealID
					and CompanyID = @company
					and PersonID = @ThisPerson
					and TransactionID = 1
					and LogInt = 4
				open CheckLog
				fetch next from CheckLog into @DPDate
				close CheckLog
				deallocate CheckLog
				set @age = datediff(day, @DPDate, @now)
				set @dptotal = @dptotal + @age
				set @TotSched = @TotSched + 1
				end
			else if @Category = 5	-- stalled decision point
				begin
				set @SDP = @SDP + 1
				set @stalledcount = @stalledcount + 1
				declare CheckSDP cursor
					for select max(WhenChanged) from Log
					where DealID = @DealID
					and CompanyID = @company
					and TransactionID = 1
				open CheckSDP
				fetch next from CheckSDP into @SDPDate
				close CheckSDP
				deallocate CheckSDP
				set @age = datediff(day, @FirstMeeting, @SDPDate)
				set @stalledtotal = @stalledtotal + @age
				end
			else if @Category = 6 and @CloseDiff > 0 and @CloseDiff <= @dayspan	-- closed
				begin
				set @Closed = @Closed + 1;
				set @TotalSales = @TotalSales + @ActualDollarAmount
				end
			else if @Category = 9 and @RemoveDiff > 0 and @RemoveDiff <= @dayspan	-- removed
				begin
				set @Removed = @Removed + 1
				set @removedcount = @removedcount + 1
				set @age = datediff(day, @RemoveDate, @now)
				set @removedtotal = @removedtotal + @age
				end
			fetch next from OppDetail into @DealID, @Category, @ActualCloseDate, @RemoveDate, @FirstMeeting, @ActualDollarAmount
			end
		close OppDetail
		deallocate OppDetail

		-- calc averages
		if @removedcount <> 0
			set @RemovedAvgAge = @removedtotal / @removedcount
		else
			set @RemovedAvgAge = null
	
		if @stalledcount <> 0
			set @StalledAvgAge = @stalledtotal / @stalledcount
		else
			set @StalledAvgAge = null
	
		if @dpcount <> 0
			set @DPAvgAge = @dptotal / @dpcount
		else
			set @DPAvgAge = null
	
		-- dump the data
		insert into snapshot (CompanyID, SalesID, SnapDate, FM, IP, SIP, DP, SDP, Closed, TotalSales, Removed, RemovedAvgAge, StalledAvgAge, DPAvgAge, TotSched, TotStalled)
			values(@company, @ThisPerson, @now, @FM, @IP, @SIP, @DP, @SDP, @Closed, @TotalSales, @Removed, @RemovedAvgAge, @StalledAvgAge, @DPAvgAge, @TotSched, @stalledcount)

		-- next!
		fetch next from Opps into @ThisPerson
		end
	close Opps
	deallocate Opps
	fetch next from Companies into @company
	end
close Companies
deallocate Companies
print 'Run for ' + cast(@now as varchar) + ' finished ' + cast(getutcdate() as varchar)
