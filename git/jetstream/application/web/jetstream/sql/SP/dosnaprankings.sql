USE [mpasac]
GO
/****** Object:  StoredProcedure [dbo].[dosnaprankings]    Script Date: 12/30/2008 21:50:04 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


CREATE                      PROCEDURE [dbo].[dosnaprankings]

AS

declare @SalesID int
declare @SnapDate smalldatetime
declare @ClosedCount int
declare @FMThreshold int, @IPThreshold int, @DPThreshold int, @CloseThreshold int
declare @FM2 float, @FM3 float, @IP2 float, @IP3 float, @DP2 float, @DP3 float, @C2 float, @C3 float
declare @StartDAte smalldatetime
declare @FM int, @IP int, @DP int
declare @FMValue int, @IPValue int, @DPValue int, @CValue int
declare @RankNew int, @RankExperienced int, @Rating int
declare @CompanyID int
declare @MonthsNew int

declare Snap cursor
	for select SalesID, SnapDate, FM, IP, DP, CompanyID
	from mpasac.snapshot
open Snap
fetch next from Snap into @SalesID, @SnapDate, @FM, @IP, @DP, @CompanyID
while @@FETCH_STATUS = 0
	begin
		declare Closed cursor
			for select count(*) from mpasac.opportunities where PersonID=@SalesID and category=6 and 30>=datediff(day, ActualCloseDate, @SnapDate)
		open Closed
		fetch next from Closed into @ClosedCount
		if 0<>@@FETCH_STATUS
			set @ClosedCount=0
		close Closed
		deallocate Closed

		declare Thresholds cursor
			for select FMThreshold, IPThreshold, DPThreshold, CloseThreshold, StartDate
			from mpasac.people where PersonID = @SalesID
		open Thresholds
		fetch next from Thresholds into @FMThreshold, @IPThreshold, @DPThreshold, @CloseThreshold, @StartDate
		close Thresholds
		deallocate Thresholds

		-- calc ratings
		set @FM2 = @FMThreshold * 0.66
		set @FM3 = @FMThreshold * 1.5
		set @IP2 = @IPThreshold * 0.66
		set @IP3 = @IPThreshold * 1.5
		set @DP2 = @DPThreshold * 0.66
		set @DP3 = @DPThreshold * 1.5
		set @C2 = @CloseThreshold * 0.66
		set @C3 = @CloseThreshold * 1.5

		if @FM = 0
			set @FMValue = 0
		else if @FM < @FM2
			set @FMValue = 1
		else if @FM < @FM3
			set @FMValue = 2
		else
			set @FMValue = 3

		if @IP = 0
			set @IPValue = 0
		else if @IP < @IP2
			set @IPValue = 1
		else if @IP < @IP3
			set @IPValue = 2
		else
			set @IPValue = 3

		if @DP = 0
			set @DPValue = 0
		else if @DP < @DP2
			set @DPValue = 1
		else if @DP < @DP3
			set @DPValue = 2
		else

			set @DPValue = 3

		if @ClosedCount = 0
			set @CValue = 0
		else if @ClosedCount < @C2
			set @CValue = 1
		else if @ClosedCount < @C3
			set @CValue = 2
		else
			set @CValue = 3

		declare Analysis cursor
			for select RankNew, RankExperienced from mpasac.analysis
			where FMValue = @FMValue and IPValue = @IPValue
			and DPValue = @DPValue and CloseValue = @CValue
		open Analysis
		fetch next from Analysis into @RankNew, @RankExperienced
		if @@FETCH_STATUS<>0
			begin
			set @RankNew=0
			set @RankExperienced=0
			end
		close Analysis
		deallocate Analysis

		declare Companies cursor
		for select MonthsConsideredNew from company where CompanyID=@CompanyID 
		open Companies
		fetch next from Companies into @MonthsNew
		close Companies
		deallocate Companies		

		-- what's our rating?
		if datediff(month, @StartDate, @SnapDate) <= @MonthsNew
			set @Rating = @RankNew
		else
			set @Rating = @RankExperienced

		update mpasac.snapshot set Closed30=@ClosedCount,TempRating=@Rating where SalesId=@SalesID and SnapDate = @Snapdate 
	fetch next from Snap into @SalesID, @SnapDate, @FM, @IP, @DP, @CompanyID
	end
close Snap
deallocate Snap

