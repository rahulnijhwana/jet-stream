ALTER TABLE Account ADD PrivateAccount bit NOT NULL DEFAULT (0);
ALTER TABLE Contact ADD PrivateContact bit NOT NULL DEFAULT (0);
ALTER TABLE Events ADD PrivateEvent bit NOT NULL DEFAULT (0);
ALTER TABLE Notes ADD PrivateNote bit NOT NULL DEFAULT (0);