USE [mpower]
GO
/****** Object:  Table [dbo].[NoteLookup]    Script Date: 09/15/2008 11:37:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NoteLookup](
	[ObjectId] [smallint] NOT NULL,
	[ObjectName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF;

INSERT INTO [mpower].[dbo].[NoteLookup] ([ObjectId],[ObjectName]) VALUES (1, 'Account');
INSERT INTO [mpower].[dbo].[NoteLookup] ([ObjectId],[ObjectName]) VALUES (2, 'Contact');
INSERT INTO [mpower].[dbo].[NoteLookup] ([ObjectId],[ObjectName]) VALUES (3, 'Event');
INSERT INTO [mpower].[dbo].[NoteLookup] ([ObjectId],[ObjectName]) VALUES (4, 'Opportunity');