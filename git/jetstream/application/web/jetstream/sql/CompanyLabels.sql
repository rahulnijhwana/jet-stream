USE [mpower]
GO
/****** Object:  Table [dbo].[CompanyLabels]    Script Date: 09/03/2008 19:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompanyLabels](
	[CompanyLabelID] [int] IDENTITY(1,1) NOT NULL,
	[LabelID] [int] NOT NULL,
	[CompanyID] [int] NOT NULL,
	[CompanyLabelName] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Status] [int] NOT NULL,
	[Position] [int] NOT NULL,
	[Side] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF