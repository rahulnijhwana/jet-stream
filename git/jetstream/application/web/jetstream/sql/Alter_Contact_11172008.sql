ALTER TABLE Contact ADD CompanyID int NOT NULL;
ALTER TABLE Contact ALTER COLUMN AccountID int NULL;

Update Contact 
SET Contact.CompanyID = A.CompanyID
FROM Account A,Contact C
WHERE A.AccountID = C.AccountID;