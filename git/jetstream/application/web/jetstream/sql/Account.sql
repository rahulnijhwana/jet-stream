USE [mpower]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 09/12/2008 18:42:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account](
	[AccountID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[Text01] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Text02] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Text03] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Text04] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Text05] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Date01] [datetime] NULL,
	[Date02] [datetime] NULL,
	[Date03] [datetime] NULL,
	[Date04] [datetime] NULL,
	[Date05] [datetime] NULL,
	[Number01] [int] NULL,
	[Number02] [int] NULL,
	[Number03] [int] NULL,
	[Number04] [int] NULL,
	[Number05] [int] NULL,
	[Select01] [int] NULL,
	[Select02] [int] NULL,
	[select03] [int] NULL,
	[Select04] [int] NULL,
	[Select05] [int] NULL,
	[Select06] [int] NULL,
	[Select07] [int] NULL,
	[Select08] [int] NULL,
	[Select09] [int] NULL,
	[Select10] [int] NULL,
	[Bool01] [bit] NULL,
	[Bool02] [bit] NULL,
	[Bool03] [bit] NULL,
	[Bool04] [bit] NULL,
	[Bool05] [bit] NULL,
 CONSTRAINT [PK_account] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF