USE [mpower]
GO
/****** Object:  Table [dbo].[Validation]    Script Date: 09/12/2008 20:15:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Validation](
	[ValidationID] [int] IDENTITY(1,1) NOT NULL,
	[ValidationName] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_Validation] PRIMARY KEY CLUSTERED 
(
	[ValidationID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF