/****** Object:  Table [dbo].[PeopleEmailAddr]    Script Date: 11/18/2008 20:32:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PeopleEmailAddr](
	[PeopleEmailAddrID] [int] IDENTITY(1,1) NOT NULL,
	[PersonID] [int] NOT NULL,
	[EmailAddr] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsPrimaryEmailAddr] [bit] NULL,
 CONSTRAINT [PK_PeopleEmailAddr] PRIMARY KEY CLUSTERED 
(
	[PeopleEmailAddrID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF