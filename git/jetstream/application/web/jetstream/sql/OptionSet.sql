USE [mpower]
GO
/****** Object:  Table [dbo].[OptionSet]    Script Date: 10/01/2008 23:40:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OptionSet](
	[OptionSetID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[OptionSetName] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AccountMapID] [int] NULL,
	[ContactMapID] [int] NULL,
 CONSTRAINT [PK_OptionSet] PRIMARY KEY CLUSTERED 
(
	[OptionSetID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF