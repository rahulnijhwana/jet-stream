USE [mpower]
GO
/****** Object:  Table [dbo].[PeopleContact]    Script Date: 11/03/2008 18:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PeopleContact](
	[PeopleContactID] [int] IDENTITY(1,1) NOT NULL,
	[ContactID] [int] NOT NULL,
	[PersonID] [int] NOT NULL,
	[CreatedBy] [bit] NOT NULL CONSTRAINT [DF_PeopleContact_CreatedBy]  DEFAULT ((0))
) ON [PRIMARY]
