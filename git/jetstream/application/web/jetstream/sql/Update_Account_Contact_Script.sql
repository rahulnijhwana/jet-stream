UPDATE AccountMap SET IsPrivate = 1 where (IsCompanyName = 0 OR IsCompanyName IS NULL);

UPDATE ContactMap SET IsPrivate = 1 where (IsFirstName IS NULL AND IsLastName IS NULL) OR (IsFirstName = 0 AND IsLastName = 0);
