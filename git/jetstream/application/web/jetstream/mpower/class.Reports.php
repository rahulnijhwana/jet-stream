<?php

require_once(BASE_PATH . '/mpower/class.PageComponent.php');
require_once(BASE_PATH . '/include/class.SessionManager.php');

class Reports extends PageComponent 
{
	public $target;
	
	public function Render() { 
		return '<iframe src ="' . SessionManager::CreateUrl('legacy/board/which_report.php') . '" style="width:100%;height:2000px;"></iframe>';
	}

}

?>