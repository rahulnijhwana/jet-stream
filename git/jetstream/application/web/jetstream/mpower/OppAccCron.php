<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/slipstream/class.Status.php';
require_once BASE_PATH . '/slipstream/class.Logger.php';

$path = realpath(dirname(__FILE__) . '/Log/');
$file = $path . date('mdyy');

function updateContact($row, $accountId) {
	$contact = $row['Contact'];
	$contactId = NULL;
	$dealId = $row['DealID'];
	if ($contact != NULL) {
		$name = splitName($contact);
		$sql = 'select ContactID from CompanyContactOutlook where FirstName=? and MiddleName=? and LastName=? and AccountID=?';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $name['FirstName']), array(DTYPE_STRING, $name['MiddleName']), array(DTYPE_STRING, $name['LastName']), array(DTYPE_INT, $accountId));
		$rs = DbConnManager::GetDb('mpower')->Exec($sql);
		if (count($rs) > 0)
			$contactId = $rs[0]['ContactID'];
		else {
			$sql = 'insert into CompanyContactOutlook(AccountID,FirstName,MiddleName,LastName) values(?,?,?,?)';
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $accountId), array(DTYPE_STRING, $name['FirstName']), array(DTYPE_STRING, $name['MiddleName']), array(DTYPE_STRING, $name['LastName']));
			$rs = DbConnManager::GetDb('mpower')->Exec($sql);
			$sql = 'select @@IDENTITY as ContactID';
			$rs = DbConnManager::GetDb('mpower')->Exec($sql);
			if ($rs[0]['ContactID'] != NULL) {
				$contactId = $rs[0]['ContactID'];
			}
		}
		if ($contactId != NULL) {
			$sql = 'update opportunities set ContactID=? where DealID=?';
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $contactId), array(DTYPE_INT, $dealId));
			$rs = DbConnManager::GetDb('mpower')->Exec($sql);
		}
	}

}

function updateAccount($row) {
	$company = $row['Company'];
	$companyId = $row['CompanyID'];
	//$division=$row['Division']; skipped not reqd any more
	$dealId = $row['DealID'];
	$accountId = NULL;
	$sql = 'select AccountID from account where [Name]=? and CompanyID=?'; //check if account is already there or not
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $company), array(DTYPE_INT, $companyId));
	$rs = DbConnManager::GetDb('mpower')->Exec($sql);
	if (count($rs) > 0)
		$accountId = $rs[0]['AccountID'];
	else {
		$sql = 'insert into account([Name],CompanyID) values(?,?)';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $company), array(DTYPE_INT, $companyId));
		$rs = DbConnManager::GetDb('mpower')->Exec($sql);
		$sql = 'select @@IDENTITY as AccountID';
		$rs = DbConnManager::GetDb('mpower')->Exec($sql);
		if ($rs[0]['AccountID'] != NULL) {
			$accountId = $rs[0]['AccountID'];
		}
	}
	if ($accountId != NULL) {
		$sql = 'update opportunities set AccountID=? where DealID=?';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $accountId), array(DTYPE_INT, $dealId));
		$rs = DbConnManager::GetDb('mpower')->Exec($sql);
	}
	return $accountId;
}

function getAllCompany() {
	$sql = 'SELECT * 
			 FROM 
			 (
			 	SELECT ROW_NUMBER() OVER (ORDER BY CompanyID) AS RowNumber, SearchQuery.*
			 	 FROM 
			 	 (		 	 	
			  			select O.DealID,O.Company,O.CompanyID,O.Contact,A.AccountID from opportunities O
						Left Join Account A
						on O.AccountID=A.AccountID
						where A.AccountID is NULL and O.Company is not null		  		
			  	) 
			  	AS SearchQuery
			  ) _myResults 
			 WHERE RowNumber BETWEEN 1 AND 1000';
	$rs = DbConnManager::GetDb('mpower')->Exec($sql);
	if (count($rs) > 0)
		return $rs;
	else
		return false;
}

function splitName($name) {
	$firstname = NULL;
	$middlename = NULL;
	$lastname = NULL;
	if ($name != NULL && trim($name) != '') {
		$name = explode(' ', $name);
		if (count($name) == 1)
			$firstname = str_replace("'", "''", $name[0]);
		elseif (count($name) == 2) {
			$firstname = str_replace("'", "''", $name[0]);
			$lastname = str_replace("'", "''", $name[1]);
		} elseif (count($name) >= 3) {
			$firstname = str_replace("'", "''", $name[0]);
			$middlename = str_replace("'", "''", $name[1]);
			$lastname = '';
			for($i = 2; $i < count($name); $i++) {
				$lastname = $lastname . ' ' . $name[$i];
			}
			$lastname = trim($lastname);
			$lastname = str_replace("'", "''", $lastname);
		}
	}
	return (array('FirstName' => $firstname, 'MiddleName' => $middlename, 'LastName' => $lastname));
}

function Logger($fileName) {
	$file = '';
	if (!$file = fopen($fileName, 'a')) {
		return false;
	}
	return $file;
}

function writeToLog($file, $data) {
	if (!$file) return false;
	if (fwrite($file, $data) === FALSE)
		return false;
	else
		return true;
}

function closeLogger($file) {
	fclose($file);
}

?>