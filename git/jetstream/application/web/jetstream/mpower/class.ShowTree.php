<?php

require_once(BASE_PATH . '/mpower/class.PageComponent.php');


class ShowTree extends PageComponent 
{
	public function Render() {
		
		return "<p>Please select the board you would like to view:</p><div style='border:1px solid #aaa;margin:10px 50px;padding:30px;'>" . $this->DrawPerson($_SESSION['login_id']) . '</div>';
	}

	private function DrawPerson($person, $indent = 0) {
		$rec = $_SESSION['tree_obj']->GetRecord($person);

		$combined = ($_SESSION['tree_obj']->IsSeniorMgr($person)) ? ' <a href="' . SessionManager::CreateUrl('dashboard.php', 'pov=' . $person . '&combined=true') . '" style="font-size:.8em;">[Combined View]</a>' : '';

		$output = '';
		if ($rec['Level'] > 1) {

			$output = $rec['FirstName'] . ' ' . $rec['LastName'];
			if ($_SESSION['tree_obj']->IsDirectMgr($person)) {
				$output = '<a href="' . SessionManager::CreateUrl('dashboard.php', 'pov=' . $person . '&combined=false') . '">' . $output . '</a>';
			}

			$output = '<div style="text-align:left;"><img src="images/person_sm.png" style="margin-left:' . $indent * 23 . 'px;">' . $output . $combined . '</div>';
		}
		if(isset($rec['children'])) {
			foreach($rec['children'] as $child) {
				$output .= $this->DrawPerson($child, $indent + 1);
			}
		}
		return $output;
	}

	
}
