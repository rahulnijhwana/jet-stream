<?php

require_once BASE_PATH . '/mpower/class.PageComponent.php';
require_once BASE_PATH . '/include/lib.csv.php';
require_once BASE_PATH . '/include/class.ReportingTree.php';

class ImportTargets extends PageComponent
{
	private $target_path = '/tmp/';
	private $translated_fields = array();
	
	private $import_fields = array('Imp_SpLastName'
			, 'Imp_SpFirstName'
			, 'Imp_Company'
			, 'Imp_Source'
			, 'Imp_Valuation'
			, 'Imp_Division'
			, 'Imp_Contact'
			, 'Imp_Source2'
			, 'Imp_PersonID'
		);
	
		
	public function GetJs() {
		return '$(document).ready(function(){
			$("div.info").fadeIn("normal")
			});';
	}

	public function GetJsFiles() {
		return array('jquery.js');
	}

	public function Render() {
		$output = '<h2 style="margin-top:0">M-Power<span style="font-size: 50%;vertical-align: sub;">&trade;</span> ' . Language::Term('ImportTargets') . '</h2>';

		if (isset($_FILES['TargetFile'])) {
			/* Add the original filename to our target path.
			Result is "uploads/filename.extension" */
			$this->target_path .= basename($_FILES['TargetFile']['name']);

			if(move_uploaded_file($_FILES['TargetFile']['tmp_name'], $this->target_path)) {
				$output .= $this->Transform(ReadCsvFile($this->target_path));
			}
			return CreateTextDiv($output);
		} elseif (isset($_POST['proceed']) && !isset($_POST['cancel']) && isset($_SESSION['last_import'])) {
			$output .= $this->Transform(ReadCsvFile($_SESSION['last_import']), true);
		}

		$output .= '<p>The <b>' . Language::Term('ImportTargets') . '</b> utility is used to assign target companies to salespeople. You can import the list created in any spreadsheet program, such as Microsoft Excel.</p>
		<p>Each row must contain either <b>' . Language::Term('Imp_PersonID') . '</b> or <b>' . Language::Term('Imp_SpFirstName') . ' and ' . Language::Term('Imp_SpLastName') . '</b>; it is acceptable for some rows to have one and some rows the other.  Each row must also include a <b>' . Language::Term('Imp_Company') . '</b>.  All other columns can be omitted or left blank.</p>
		<p>The first row of the table must contain the following column titles, spelled exactly as you see below with no extra spaces or carriage returns.  The columns can be listed in any order, but the data must be in the column directly below the appropriate header.</p>';
		
		$field_names = '';
		$field_options = '';
		foreach($this->import_fields as $field) {
			$field_name = Language::Term($field);
			$field_names .= "<li>$field_name</li>";
		}
		$output .= "<ul>$field_names</ul>";

		$output .= '<p>The import file must be saved as a CSV file.  To do this from Microsoft Excel, with the file open to the sheet with your targets, select Save As -> Other Formats, then under Save as Type, choose CSV (Comma delimited)(*.csv).</p>';

		$output .= 'Import File <i>(Must be CSV Format)</i><br>
			<form enctype="multipart/form-data" method="POST"><input type="file" name="TargetFile" size="40"/><br>
			<input type="submit" value="Import it"></form>';

		return CreateTextDiv($output);

	}

	/**
     * Transform the CSV into MPower Opp records
     *
     * @return object
     */
	private function Transform($csv_recordset, $do_save) {
		$company = $_SESSION['company_obj']['CompanyID'];

		// Only need to do this if the company uses source and the csv contains source
		// Build a Source lookup table
		$source_terms = $csv_recordset->GetField(Language::Term('Imp_Source'));
		if (count($source_terms) > 0) {
			$source_sql = 'SELECT SourceID, Name FROM Sources WHERE Name IN (' . implode(', ', array_fill(0, count($source_terms), '?')) . ') AND CompanyID = ? AND Deleted = 0';
			$source_sql = SqlBuilder()->LoadSql($source_sql)->BuildSql(array(DTYPE_STRING, $source_terms), array(DTYPE_INT, $company));
			$source_lookup = DbConnManager::GetDb('mpower')->Execute($source_sql, 'MpRecord', 'Name');
		}

		// Only need to do this if the company uses source2 and the csv contains source2
		// Build a Source2 lookup table
		$source2_terms = $csv_recordset->GetField(Language::Term('Imp_Source2'));
		if (count($source2_terms) > 0) {
			$source2_sql = 'SELECT Source2ID, Name FROM Sources2 WHERE Name IN (' . implode(', ', array_fill(0, count($source2_terms), '?')) . ') AND CompanyID = ? AND Deleted = 0';
			$source2_sql = SqlBuilder()->LoadSql($source2_sql)->BuildSql(array(DTYPE_STRING, $source2_terms), array(DTYPE_INT, $company));
			$source2_lookup = DbConnManager::GetDb('mpower')->Execute($source2_sql, 'MpRecord', 'Name');
		}

		// Only need to do this if the company uses valuation and the csv contains valuation
		// Build a Valuation lookup table
		$valuation_terms = $csv_recordset->GetField(Language::Term('Imp_Valuation'));
		if (count($valuation_terms) > 0) {
			$valuation_sql = 'SELECT ValID, VLevel FROM Valuations WHERE VLevel IN (' . implode(', ', array_fill(0, count($valuation_terms), '?')) . ') AND CompanyID = ? AND Activated = 1';
			$valuation_sql = SqlBuilder()->LoadSql($valuation_sql)->BuildSql(array(DTYPE_INT, $valuation_terms), array(DTYPE_INT, $company));
			$valuation_lookup = DbConnManager::GetDb('mpower')->Execute($valuation_sql, 'MpRecord', 'VLevel');
		}

		$new_recordset_sql = 'SELECT * FROM opportunities WHERE 0 = 1';
		$new_recordset = DbConnManager::GetDb('mpower')->Execute($new_recordset_sql);
		
		//$new_recordset = new MpRecordset();
		
		$row = 1;

		$errors = array();
		$column_errors = array();
		$field_errors = array();
		foreach($this->import_fields as $mp_field) {
			$this->translated_fields[] = Language::Term($mp_field);
		}
		$succ_count = $warn_count = $fail_count = 0;
		$fail_text = $warn_text = '';
		
		// Loop through the Salesforce Opps/Leads
		foreach($csv_recordset as $csv_record) {
			$column_errors = array_unique(array_merge($column_errors, array_diff($csv_record->GetFields(), $this->translated_fields)));
			$row++;
			$errors[$row]['fatal'] = false;
			$errors[$row]['field'] = array();

			$opp = new Opp();  // Opportunity that will be used to store the converted salesforce data
			$new_recordset[] = $opp;
			$opp->TargetDate = TimestampToMsSql(time());
			$opp->SetDatabase(DbConnManager::GetDb('mpower'));
			$opp->CompanyID = $company;

			$person = false;
			// Need to have a methodology to look up salesperson by name or userid
			// $person = LookupPersonIdByCmid($csv_record->OwnerId);
			$pers_text = 'None found';
			if ($csv_record->InRecord(Language::Term('Imp_PersonID'), false)) {
				$person = $_SESSION['tree_obj']->GetPersonByUsername($csv_record->{Language::Term('Imp_PersonID')});
				$pers_text = 'UserID: ' . $csv_record->{Language::Term('Imp_PersonID')};
			} elseif ($csv_record->InRecord(Language::Term('Imp_SpFirstName'), false) && $csv_record->InRecord(Language::Term('Imp_SpFirstName'), false)) {
				$person = $_SESSION['tree_obj']->GetPersonByName($csv_record->{Language::Term('Imp_SpFirstName')}, $csv_record->{Language::Term('Imp_SpLastName')});
				$pers_text = 'Name: ' . $csv_record->{Language::Term('Imp_SpFirstName')} . ' ' . $csv_record->{Language::Term('Imp_SpLastName')};
			}
			
			if (!empty($person)) {
				$opp->PersonID = $person;
			} else {
				$errors[$row]['fatal'] = true;
				$errors[$row]['field'][] = "Unable to identify salesperson ($pers_text) <i style=\"color:red;\">Unrecoverable</i>";
			}

			if ($csv_record->InRecord(Language::Term('Imp_Company'), false)) {
				$opp->Company = $csv_record->{Language::Term('Imp_Company')};
			} else {
				$errors[$row]['fatal'] = true;
				$errors[$row]['field'][] = "No " . Language::Term('Imp_Company') . " found. <i style=\"color:red;\">Unrecoverable</i>";
			}

			if ($csv_record->InRecord(Language::Term('Imp_Division'), false)) {
				$opp->Division = $csv_record->{Language::Term('Imp_Division')};
			}

			if ($csv_record->InRecord(Language::Term('Imp_Contact'), false)) {
				$opp->Contact = $csv_record->{Language::Term('Imp_Contact')};
			}
			
			$opp->Category = MP_TARGET;

			if ($csv_record->InRecord(Language::Term('Imp_Source'), false)) {
				$source = $csv_record->{Language::Term('Imp_Source')};
				if ($source_lookup->InRecordset($source)) {
					$opp->SourceID = $source_lookup[$source]->SourceID;
				} else {
					$errors[$row]['field'][] = "Invalid " . Language::Term('Imp_Source') . " ($source).";
				}
			}

			if ($csv_record->InRecord(Language::Term('Imp_Source2'), false)) {
				$source2 = $csv_record->{Language::Term('Imp_Source2')};
				if ($source2_lookup->InRecordset($source2)) {
					$opp->Source2ID = $source2_lookup[$source2]->Source2ID;
				} else {
					$errors[$row]['field'][] = "Invalid " . Language::Term('Imp_Source2') . " ($source2).";
				}
			}

			if ($csv_record->InRecord(Language::Term('Imp_Valuation'), false)) {
				$valuation = $csv_record->{Language::Term('Imp_Valuation')};
				if ($valuation_lookup->InRecordset($valuation)) {
					$opp->ValID = $valuation_lookup[$valuation]->ValID;
					$opp->VLevel = $valuation_lookup[$valuation]->VLevel;
				} else {
					$errors[$row]['field'][] = "Invalid " . Language::Term('Imp_Valuation') . " ($valuation).";
				}
			}
			$err_text = '';
			if ($errors[$row]['fatal'] == true) {
				$new_recordset->Pop();
				$fail_count++;
				foreach($errors[$row]['field'] as $error) {
					$err_text .= "<li>" . $error . "</li>";
				}

				$fail_text .= "<li>Row $row <ul>$err_text</ul></li>";
			} elseif (count($errors[$row]['field']) > 0) {
				$warn_count++;
				foreach($errors[$row]['field'] as $error) {
					$err_text .= "<li>" . $error . "</li>";
				}
				$warn_text .= "<li>Row $row <ul>$err_text</ul></li>";

			} else {
				$succ_count++;
			}
		}

		if ($do_save) {
			$new_recordset->Initialize()->Save();
			$output = '<p><b>Import complete:</b> ';
			$output .= $succ_count + $warn_count . ' row(s) loaded';
			if ($fail_count > 0) $output .= ', ' . $fail_count . ' row(s) ignored</p>';
			$output .= '<hr>';
			unset($_SESSION['last_import']);
			return $output;			
		}

		$output = 'Your file has been checked by the system:<br>';
		$output .= 'Good Rows: ' . $succ_count;
		$output .= ' / Rows with Warnings: ' . $warn_count;
		$output .= ' / Rows Failed:  ' . $fail_count . '<br>';
		$_SESSION['last_import'] = $this->target_path;
		$output .= '<form method="POST">';
		if ($succ_count > 0 || $warn_count > 0) {
				$output .= '<input type="submit" name="proceed" value="Proceed with Import">';
		}
		$output .= '<input type="submit" name="cancel" value="Cancel and Start Over"></form>';
		
		if (count($column_errors) > 0) {
			$error_text = implode('</li><li>', $column_errors);
			$output .= "<p>The following column(s) was not recognized: <ul><li>$error_text</li></ul></p>";
		}
		
		if (!empty($fail_text)) {
			$output .= "<p>The following rows cannot be imported and produced the following errors:<ul>$fail_text</ul></p>";
		}
		if (!empty($warn_text)) {
			$output .= "<p>The following rows can be imported, but produced the following warnings:<ul>$warn_text</ul></p>";
		}
		return $output;
	}

}
