<?php
require_once(BASE_PATH . '/mpower/class.PageComponent.php');
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';

class Salespeople extends PageComponent 
{
	private $target;
	public $type;
	public $url;
	public $checked;
	public $manager;
	//private $total_sp;
	
	public function GetJsInit() {
		if ($this->type == 'calendar') {
			return 'FilterCal();';
		} else {
			return false;
		}
	}
	
	public function GetJs() {
		$output = '';
		if ($this->type == 'calendar') {
			$output = "function FilterCal() {
				var checkboxes = document.getElementsByName(\"person_chkbx\");
				if (checkboxes.length == 0) return;
				var checked = '';
				var checked_qty = 0;
				for (var i = 0; i < checkboxes.length; i++) {
					var display = 'none';
					if (checkboxes[i].checked == true) {
						display = '';
						checked += ' ' + checkboxes[i].getAttribute('id').substring(2);
						checked_qty += 1;
					}
					var this_id = checkboxes[i].id;
		
					var appts = document.getElementsByTagName('table');
					for(var j = 0; j < appts.length; j++) {
						if(appts[j].getAttribute('name') ==  this_id + 'appt'){
							appts[j].style.display = display;
						}
					}
				}
				
				var form = document.forms['date_picker'];
				var el = document.createElement('input');
				el.type = 'hidden';
				el.name = 'checked';
				el.value = checked;
				form.appendChild(el);
				
				if (checked_qty == 0) {
					document.getElementById(\"cal_warning\").style.display = '';
					document.getElementById(\"chkbx_all\").checked = false;
				} else {
					document.getElementById(\"cal_warning\").style.display = 'none';
					if (checked_qty == checkboxes.length) {
						document.getElementById(\"chkbx_all\").checked = true;
					}
				}
			}

			function CheckAllCal() {
				var checked = document.getElementById(\"chkbx_all\").checked;
				document.getElementById(\"chkbx_all\").checked = (checked) ? true : false;
				var checkboxes = document.getElementsByName(\"person_chkbx\");
				for (var i = 0; i < checkboxes.length; i++) {
					checkboxes[i].checked = checked;
				}
				FilterCal();
			}";
		}
		$output .= "function ToggleRatings() {
				document.getElementById(\"ratings\").value = document.getElementById(\"ratings_cb\").checked ? 'on' : 'off';
	}";		
		return $output;
	}
	
	public function GetCssFiles() {
		return array('opp.css', 'salespeople.css', array('print', 'opp_print.css'), array('print', 'salespeople_print.css'));
	}

	public function Render() {
		//$sp_section .= '<table width="100%" class="dbl" style="clear:both;"><tr>';
		//$sp_section .= '</tr></table>';
		$target = $_SESSION['tree_obj']->GetTarget(false, true);
		if (is_int($target)) {
			$output = '<td>' . $this->PersonButton($_SESSION['tree_obj']->GetRecord($target), true) . '</td>';
			// $back_cell = '<td style="width:1px;"><a href="' . SessionManager::CreateUrl($this->url, 'target=false') . '">Back</a></td>';
			// if ($this->manager) {
			// 	$output = $back_cell . $output . $back_cell;
			//}
		} elseif (is_array($target)) {
			$output = '';
			$output = $this->TopControls();
			// Contortions to get numbers for the salespeople.  Starts at 0
			$people_num = array_flip(array_values($target));	
			$sp_count = 0;
			foreach($target as $salesman) {
				if ($sp_count == 8 && !$_SESSION['tree_obj']->paging) {
					$sp_count = 0;
					$output .= '</tr><tr>';
				}
				$output .= '<td style="width:10%;">';
				$output .= $this->PersonButton($_SESSION['tree_obj']->GetRecord($salesman), false, $people_num[$salesman] + 1);
				$output .= '</td>';
				$sp_count++;
			}
			if (count($target) < 6) {
				for ($x = 0; $x < 6 - count($target); $x++) {
					$output .= '<td style="width:10%;"></td>';
				}
			}
		} else {
			$output = "<td>Error:  No salesman to display</td>";
		}
		return '<table width="100%" class="dbl" style="clear:both;"><form method="POST"><tr>' . $output . '</tr></form></table>';
	}

	private function TopControls() {
		$page = $_SESSION['sp_page'];
		$total_sp = $_SESSION['tree_obj']->total_sp;
		$paging_text = '<div style="font-size:.9em;padding:0px 5px 0px 5px; margin:0 20px 0px 0;vertical-align:middle;">';
		if ($_SESSION['tree_obj']->paging) {
			$start = $page + 1;
			$end = ($page + 10 > $total_sp) ? $total_sp : $page + 10;
			if ($start != 1 || $end < $total_sp) {			
				$paging_text .= '<div style="float:left;">';
	
				if ($page > 0) {
					$paging_text .= '<input name="paging" type="submit" value="<<">';
					$paging_text .= '<input name="paging" type="submit" value="<"> ';
				}			
				$paging_text .= "Salespeople $start - $end of $total_sp";
				if ($end < $total_sp) {
					$paging_text .= ' <input name="paging" type="submit" value=">">';
					$paging_text .= '<input name="paging" type="submit" value=">>">';
				}
				$paging_text .= '</div>';
			}
		}
		if ($this->type == 'calendar') {

			$checked_text = '';
			$paging_text .= '<div style="float:left;">';
			$paging_text .= '<input id="chkbx_all" type="checkbox" style="vertical-align:middle;" onclick="CheckAllCal();"> ' . Language::__get('AllSalespeople') . '</input>';
			$paging_text .= '</div>';
			
		}
		
		$sort_options = array('FirstName', 'LastName', 'Rating_asc', 'Rating_dsc', 'StartDate_asc', 'StartDate_dsc');

		$sp_sort = (isset($_SESSION['sp_sort'])) ? $sp_sort = $_SESSION['sp_sort'] : '';
		$sort_option_text = '';
		foreach($sort_options as $opt) {
			$selected = ($sp_sort == $opt) ? 'selected' : '';
			$sort_option_text .= "<option value =\"$opt\" $selected>" . Language::__get($opt) . "</option>";
		}
		
		if (isset($_POST['ratings'])) {
		 	$_SESSION['ratings'] = ($_POST['ratings'] == 'on') ? true : false;
		}

		if (isset($_SESSION['ratings']) && $_SESSION['ratings']) {
			$checked = 'checked';
			$rating_val = 'on';
		} else {
			$checked = '';
			$rating_val = 'off';
		}
		// $checked = (isset($_SESSION['ratings']) && $_SESSION['ratings']) ? 'checked' : '';
		
		// echo 'Post Ratings: ' . $_POST['ratings'] . '<br>';
		
		$output = $paging_text . '
					<div style="float:right; text-align:right; margin-left:5px;">
						 &nbsp;&nbsp;&nbsp;' . Language::__get('DisplayRatings') . ' <input id="ratings_cb" type="checkbox" onclick="ToggleRatings();this.form.submit();" ' . $checked . '>
					</div>
					<div style="float:right; text-align:right;">' . Language::__get('SortSalespeople') . ' <select name="sp_sort" style="font-size:.9em;vertical-align:middle;" onchange="this.form.submit();"> 
							' . $sort_option_text . '
						</select>
					</div>
				</div><input type="hidden" id="ratings" name="ratings" value="' . $rating_val . '">';

		return $output;
	}
	
	private function PersonButton($person, $detail = false, $person_num = 0) {
		$ratings = (isset($_SESSION['ratings'])) ? $_SESSION['ratings'] : false;

			// $href = '';
		$left = '';
		$right = '';
		$middle = '';
		$onclick = '';
		$rating = (isset($person['Rating']) && $person['Rating'] > 0) ? $person['Rating'] : 'Unk.';
		$avg_vlevel = (isset($person['AvgVLevel']) && $person['AvgVLevel'] > 0) ? $person['AvgVLevel'] : 'Unk.';
		$class = ' oppdefault';
		
		$personid = $person['PersonID'];
		if(empty($person['PersonID'])) $personid = -1;

		$multi_class = 'person_multi';
		if ($detail) {
			$multi_class = 'person';
			$middle = '<span class="persondetail">'.$person['FirstName'] . ' ' . $person['LastName'].'</span>';
			if ($_SESSION['tree_obj']->IsManager($_SESSION['login_id'])) {
				// $coaching = SessionManager::CreateUrl('legacy/board/analysis.php','target='.$personid);
				// $trends = SessionManager::CreateUrl('legacy/board/select_trend.php','target='.$personid);

				// $left = "<a href=\"$coaching\" rel=\"gb_pageset[editopp]\">Coaching</a><br><a href=\"$trends\" rel=\"gb_pageset[editopp]\">Trends</a>";
				if ($_SESSION['company_obj']['ValuationUsed'] == 1) {
					$left = sprintf("<p>Avg Valuation: %1.1f</p>", $avg_vlevel);
				}

				if ($ratings) {
					$right .= "<p>Rating: $rating</p>";
				}
			}
		} else {
			$name = ($this->url == 'dashboard.php') ? $person['FirstName'] . '<br>' . $person['LastName'] : $person['FirstName'] . ' ' . $person['LastName'];

			//$name = $person['FirstName'] . '<br>' . $person['LastName'];
			if(empty($person['FirstName']) && empty($person['LastName'])) $name = 'All';

			$href = SessionManager::CreateUrl($this->url, 'target=' . $personid);
			$onclick = "onclick=\"window.location.href='" . $href .  "';\"";
			
			if ($this->type == 'calendar') {
				$checked_text = '';
				
				if (count($this->checked) == 0 || in_array($personid, $this->checked)) {
					$checked_text = 'checked';
				}
				$onclick = '';
				$left = "<div class=\"appt_person\">$person_num</div>";
				$middle = $name;
				$right = '<input name="person_chkbx" id="sp' . $personid . '" type="checkbox" ' . $checked_text . ' onclick="FilterCal();">';
			} elseif ($ratings) {
				$left = $name;
				$right = $rating;
			} else {
				$middle = $name;
			}
		}
	
		$background = $person['Color'];

		if($this->url == 'dashboard.php' && !$detail) $class = '';
		$multi_class .= $class;

		$firstline = "<table class=\"opp $multi_class\" style=\"background-color:$background;\" $onclick>";
		$lastline = '</table>';

		if($this->url == 'legacy/board/analysis.php' || $this->url == 'legacy/board/select_trend.php') {
			$firstline = "<a href=\"$href\" title=\"\" rel=\"gb_pageset[editopp]\"><table class=\"person opp\" style=\"background-color:$background;\" >";
			$lastline = '</table></a>';		
		} 
		
		$output= "$firstline
			<tr>
				<td class=\"tl\">$left</td>
				<td class=\"tc\">$middle</td>
				<td class=\"tr\">$right</td>
			</tr>
			<tr><td class=\"bl\"></td><td class=\"bc\"></td><td class=\"br\"></td></tr>
			$lastline";

		return $output;
	}

}
