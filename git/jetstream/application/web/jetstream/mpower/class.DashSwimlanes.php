<?php

require_once BASE_PATH . '/mpower/class.PageComponent.php';
require_once BASE_PATH . '/include/lib.date.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';

class DashSwimlanes extends PageComponent 
{
	public $target;
	
	public function GetCssFiles() {
		return array('opp.css', 'dash.css', array('print', 'opp_print.css'), array('print', 'dash_print.css'));
	}


	public function GetJsFiles() {
		return array('dash_column_sizer.js');
	}
	
	public function GetJs() {
		if (is_int($this->target)) {
			$javascript = "var dash_type='detail';";
		} else {
			$javascript = "var dash_type='group';";
		}
		
		return $javascript;
	}

	public function GetJsInit() {
		return "DashColumnSizer.init();";
	}

	function HeaderButton($text, $category, $stalled = false, $closedtimestamp = 0)	{
		$class = '';
		if ($stalled) {
			$class = 'stalled_hdr';
		}

		$headerfline = '';
		$headerlline = '';
		$target = (isset($_GET['target'])) ? (int)$_GET['target'] : 0;
		$timestamp = '';

		if($category == 6) $timestamp = '&timestamp='.$closedtimestamp;

		if (isset($target) && $target > 0) {
			$hash = $category .'_'. $target .'_';
			$url = 'legacy/board/spreadsheet.php?SN=' . $_REQUEST['SN'] . '&time='.time().$timestamp.'&hash='.$hash;

			$headerfline = '<a href="'.$url.'" title="" rel="gb_pageset[spreadsheets]">';
			$headerlline = '</a>';
		} else {
			if (!$_SESSION['tree_obj']->IsManager($_SESSION['login_id'])) {		
				$hash = $category .'_'. $_SESSION['login_id'] .'_';
				$url = 'legacy/board/spreadsheet.php?SN=' . $_REQUEST['SN'] . '&time='.time().$timestamp.'&hash='.$hash;
				
				$headerfline = '<a href="'.$url.'" title="" rel="gb_pageset[spreadsheets]">';
				$headerlline = '</a>';			
			} else {
				$class .= ' oppdefault';
			}
		}

		$output="$headerfline<table class=\"opp opp_h $class\">
			<tr><td class=\"tl\">$text</td><td class=\"tr\"></td></tr>
			<tr><td class=\"bl\"></td><td class=\"br\"></td></tr>
		</table>$headerlline";
	
		return $output;
	}


	function DrawDbCol($contents) {
		// $detail = is_int($this->target);
		$content_count = '';
		if (is_array($contents)) {
			$content_count = count($contents);
			$contents = implode($contents);
		}

		$html = "<td class=\"dash_i\"><div class=\"colcount\">$content_count</div>$contents</td>"; 
		return $html;
	}

	function TargetButton($opp,	$background, $detail = true)	{
		$alert_effective_time = mktime(0, 0, 0, date('m'), date('d') - $_SESSION['company_obj']['MissedMeetingGracePeriod'], date('y'));
		
		$alert = (($alert_effective_time > MsSqlToTimestamp($opp->FirstMeeting) && $opp->Category	== 1) ||
		($alert_effective_time	> MsSqlToTimestamp($opp->NextMeeting) &&
		($opp->Category	== 2 ||	$opp->Category == 4))) ? ' dash_alert' : '';

		// echo date('y-m-d', $alert_effective_time);
		// echo ' : ' . date('y-m-d', strtotime($opp->FirstMeeting));
		// echo ' : ' . date('y-m-d', strtotime($opp->NextMeeting));
		// echo ' : ' . $opp->Category . ' : ' . $alert . '<br>';
		
		$ms_table =	'';
		if ($opp->Transactional == 1) {
			$ms_table = '<div class="trans">' . Language::Term('TransAbbr') . '</div>';
			if ($detail) $ms_table = '<b>' . Language::Term('Translabel') . '</b>';
		} elseif ($opp->Category == 2	|| $opp->Category == 3)	{
			$ms_table = '';
			$filled_ms = '<td class="msc"><span>x</span></td>';
			$empty_ms = '<td></td>';
			if ($_SESSION['company_obj']['Requirement1used'] == 1) {
				$ms_table .= ($opp->Requirement1 == 1) ? $filled_ms : $empty_ms;
			}

			$ms_table .= ($opp->Person == 1) ? $filled_ms : $empty_ms;
			$ms_table .= ($opp->Need == 1) ? $filled_ms : $empty_ms;
			$ms_table .= ($opp->Money == 1) ? $filled_ms : $empty_ms;
			$ms_table .= ($opp->Time == 1) ? $filled_ms : $empty_ms;

			if ($_SESSION['company_obj']['Requirement2used'] == 1) {
				$ms_table .= ($opp->Requirement2 == 1) ? $filled_ms : $empty_ms;
			}

		} elseif ($opp->Category ==	4 || $opp->Category	== 5 ||	$opp->Category == 6) {
			$ms_table =	'<td class="msc"></td>';
		}

		$salescycle = '';
		if ($opp->Category > 1 && $opp->Category < 6 &&	$opp->SalesCycleLength != NULL)	{
			$red_percent = GetStdFromPercent($opp->RedAlertPercentage);
			$yellow_percent	= GetStdFromPercent($opp->YellowAlertPercentage);
			if($opp->SalesCycleLength *	$red_percent + $opp->StandardDeviation < (time() - MsSqlToTimestamp($opp->FirstMeeting))/86400) {
				$salescycle	= '<td class="alert_r"></td>';
			} elseif($opp->SalesCycleLength	+ $yellow_percent *	$opp->StandardDeviation	< (time() -	MsSqlToTimestamp($opp->FirstMeeting))/86400) {
				$salescycle	= '<td class="alert_y"></td>';
			}
		}

		$ms_table_template = '<table class="milestone"><tr>%s</tr></table>';
		if ($opp->Transactional != 1) {
			if ($ms_table != '') {
				$ms_table = vsprintf($ms_table_template, $ms_table);
			}
		}
		if ($salescycle != '') {
			$salescycle = vsprintf($ms_table_template, $salescycle);
		}

		if($opp->IsCancelled == 1) {
			$color_border = 'border:2px solid yellow;';
			$cancel_title = 'title= "Latest meeting is cancelled"';
		}
		else {
			$color_border = '';
			$cancel_title = '';
		}

		if (!$detail) {

			$timestamp = '';	
			if($opp->Category == 6) $timestamp = '&timestamp='.MsSqlToTimestamp($opp->ActualCloseDate);

			$hash = $opp->Category .'_'. $opp->PersonID .'_'. $opp->DealID;
			$url = 'legacy/board/spreadsheet.php?SN=' . $_REQUEST['SN'] . '&amp;time='.time().$timestamp.'&amp;hash='.$hash;

			$output	= <<<END_HTML
				<a href="$url" title="" rel="gb_pageset[spreadsheets]"><table class="opp opp_grp $alert" style="background-color:$background;$color_border" $cancel_title > 
					<tr> 
						<td class="tl">$ms_table$salescycle</td> 
						<td class="tr" ></td> 
					</tr> 
					<tr><td class="bl"></td><td class="br"></td></tr> 
				</table></a>
END_HTML;
	
		} else {
	
			$vlevel = ($opp->VLevel > 0) ? $opp->VLevel : '&nbsp;';
			$company_name = (is_null($opp->Company)) ? '&nbsp;' : $opp->Company;
			$division_name = (is_null($opp->Division)) ? '&nbsp;' : $opp->Division;
			$contact_name = (is_null($opp->Contact)) ? '&nbsp;' : $opp->Contact;
			
			$target_date = '&nbsp;';
			if ($opp->Category == 10 || $opp->Category == 3 || $opp->Category == 5) {
				if ($opp->Category == 10) {
					if (!empty($opp->TargetDate)) {
						$display_date = MsSqlToTimestamp($opp->TargetDate);
					} else {
						$display_date = MsSqlToTimestamp($opp->CreateDate);
					}
				} elseif (!is_null($opp->NextMeeting)) {
					$display_date = MsSqlToTimestamp($opp->NextMeeting);
				} else {
					$display_date = MsSqlToTimestamp($opp->FirstMeeting);
				}
				if (MsSqlToTimestamp($opp->AssignedDate) > $display_date) {
					$display_date = MsSqlToTimestamp($opp->AssignedDate);
				}
				$display_date = mktime(0,0,0,date('m', $display_date),date('d', $display_date), date('Y', $display_date));
				$daysago = floor((time() - $display_date)/86400);
				if (!is_null($opp->PrevDaysOld) && $opp->PrevDaysOld > 0) {
					$daysago .= '+' . $opp->PrevDaysOld;
				}
				$date = date("m/d/y", $display_date);
				$daysTerm = ($opp->Category == 10) ? Language::Term('Added') : Language::Term('Stalled');
				$target_date = "$daysTerm $date ($daysago)";
			}
			
			$retarget = '';
			if ($opp->Category == 10) {
				if ($opp->Renewed == 1 || !is_null($opp->AutoReviveDate)) {
					$retarget = '<b>' . Language::Term('ReviveLabel') . '</b>';	
				}
			}
	
		$hash =  $opp->DealID .'_'. $opp->PersonID .'_'.  $opp->VLevel;
		$url = 'legacy/shared/edit_opp2.php?SN=' . $_REQUEST['SN'] . '&amp;detailview=1&amp;time=' . time() . '&amp;reqOpId=' . $opp->DealID . '&amp;reqPersonId=' . $opp->PersonID . '#' . $hash;
		
		$output = <<<END_HTML
	
		<table class="opp opp_dtl $alert" style="background-color:$background;$color_border" onclick="var win = Windowing.openSizedWindow('$url', 630, 1000);" $cancel_title> 
			<tr> 
				<td class="tl"> 
					$company_name<br> 
					$division_name<br> 
					$contact_name<br> 
					$target_date
				</td> 
				<td class="tr"> 
					$vlevel<br> 
					$retarget<br>
					<div style="float:right;">$ms_table
					$salescycle</div>
				</td> 
			</tr> 
			<tr><td class="bl"></td><td class="br"></td></tr> 
		</table>
	
END_HTML;

		}
	
		
		return $output;
	}
	
	public function Render() { 
		$detail = is_int($this->target);
		$lang = Language::GetInstance();
		$company = $_SESSION['company_obj'];
		$lang->LoadCache($company['CompanyID']);
		// $people_list = ($detail) ? $this->target : array_slice($this->target, $_SESSION['sp_page'], 10);

		//$people_list = $this->target;
		$people_list = is_null($this->target) ? array() : $this->target;
		
		if (!$detail) {
			$opp_fields = 'opportunities.DealID, opportunities.PersonID, opportunities.Category, opportunities.ActualCloseDate, opportunities.FirstMeeting, opportunities.NextMeeting, opportunities.Requirement1, opportunities.Person, opportunities.Need, opportunities.Money, opportunities.Time, opportunities.Requirement2, opportunities.Transactional, opportunities.VLevel';
		} else {
			$people_list = array($people_list);
			$opp_fields = 'opportunities.*';
		}
		
				// , DATEADD(d, Company.MissedMeetingGracePeriod, FirstMeeting) AS FMAlert
				// , DATEADD(d, Company.MissedMeetingGracePeriod, NextMeeting) AS NMAlert

				// LEFT JOIN Company ON Opportunities.CompanyID = Company.CompanyID

				// Load	the	Main and Stalled Grid
		$opp_sql = "SELECT $opp_fields				
				, Account." . $_SESSION['account_name'] . " Company
				, (Contact." . $_SESSION['contact_first_name'] . " + ' ' + Contact." . $_SESSION['contact_last_name'] . ") AS Contact 
				, salescycles.SalesCycleLength, salescycles.StandardDeviation
				, products.YellowAlertPercentage,	products.RedAlertPercentage
				, (SELECT TOP 1 WhenChanged FROM log 
					WHERE log.DealID = opportunities.DealID and TransactionID = 10 
					ORDER BY WhenChanged DESC
					) AssignedDate,
				opportunities.LatestEventID, E.IsCancelled
			FROM opportunities 
				LEFT JOIN Account ON opportunities.AccountID = account.AccountID
				LEFT JOIN Contact ON opportunities.ContactID = Contact.ContactID
				LEFT OUTER JOIN	Opp_Product_XRef ON	opportunities.DealID = Opp_Product_XRef.DealID
				LEFT OUTER JOIN	salescycles	ON salescycles.ProductID = Opp_Product_XRef.ProductID 
					AND	salescycles.PersonID = opportunities.PersonID
				LEFT OUTER JOIN	products ON	salescycles.ProductID =	products.ProductID
				LEFT JOIN Events AS E ON opportunities.LatestEventID = E.EventID
			WHERE opportunities.PersonID IN	(" . ArrayQm($people_list) . ")
				AND	(category IN (10, 1, 2,	3, 4, 5) OR	(category =	6 AND ActualCloseDate >= ?))
			ORDER BY Account." . $_SESSION['account_name'] . ",	opportunities.DealID, SalesCycleLength DESC";
		
		$opp_sql = SqlBuilder()->LoadSql($opp_sql)->BuildSql(array(DTYPE_INT, $people_list), array(DTYPE_TIME, MonthAgoDate(3)));

		//echo $opp_sql . '<br>';
		
		$opp_rs	= DbConnManager::GetDb('mpower')->Execute($opp_sql);
		
		//timehack('opportunities');
		
		$column_html = array();
		$closed_label = array();
		$closed_interval = array();
		
		for	($interval = 0;	$interval <	3; $interval++)	{
			if ($company['ClosePeriod'] == 'm') {
				$closed_interval[$interval] = MonthAgoDate($interval);
				$month_name = date('F', $closed_interval[$interval]);
				$closed_label[$interval] = str_replace('[Month]', $lang->$month_name, $lang->ClosedInMonth);				
			} elseif ($company['ClosePeriod'] == 'q') {
				$closed_interval[$interval] = QuarterAgoDate($interval);
				$quarter_name = QuarterName($closed_interval[$interval], 'Quarter', false);
				$quarter_year = date('Y', $closed_interval[$interval]);
				$closed_label[$interval] = str_replace('[Year]', $quarter_year, str_replace('[Quarter]', $lang->$quarter_name, $lang->ClosedInQuarter));
			}
		}
		
		// Generate	the	HTML for each swimlane
		$prev_deal = 0;
//		$color_table = array();
		
		// $valuations = array();
		foreach($opp_rs as $opp) {
			if ($opp->DealID ==	$prev_deal)	{
				continue;
			} else {
				$prev_deal = $opp->DealID;
			}
			$column	= $opp->Category;
			$sub_column	= array_search($opp->PersonID, array_values($people_list));
			if ($column	== MP_CLOSED) {
				for	($interval = 0;	$interval <	3; $interval++)	{
					if (MsSqlToTimestamp($opp->ActualCloseDate) >= $closed_interval[$interval]) {
						$column	.= '-' . $interval;
						break;
					}
				}
			} //else {
				//if($opp->VLevel > 0) {
				//	$valuations[$opp->PersonID][] = $opp->VLevel;
				//}
			//}
			$color = $_SESSION['tree_obj']->GetInfo($opp->PersonID, 'Color');
			$column_html[$column][$sub_column][] = $this->TargetButton($opp, $color, $detail);
		}
		$total_columns = ($detail) ? 1 : (count($people_list) > 6) ? count($people_list) : 6;
		// foreach($valuations as $val) {
		// 	echo array_sum($val) / count($val) . '<br>';
		//}

		$targetcategory = 10;
		$fmcategory = 1;
		$ipcategory = 2;
		$ipscategory = 3;
		$dpcategory = 4;
		$dpscategory = 5;
		$closedcategory = 6;
		$dremovedcategory = 9;
		?>
		<div align="center" style="border:0px solid coral">
		<table id="dashb" class="dash">
			<tr>
				<td class="dash">
					<table class="dash">
						<tr>
							<td colspan="<?php echo $total_columns?>"><?php echo $this->HeaderButton($lang->TLabel, $targetcategory); ?></td>
						</tr>
						<tr id="ph10">
						<?php 
						for ($x = 0; $x < $total_columns; $x++) {
							$col = null;
							if (isset($column_html[MP_TARGET]) && array_key_exists($x, $column_html[MP_TARGET])) {
								$col = $column_html[MP_TARGET][$x];
							}
							echo $this->DrawDbCol($col);
						}
						?>
						</tr>
					</table>
				</td>
				<td class="divider">
					<img src="images/spacer.gif" alt="spacer" style="width:5px; height:0;">
				</td>
				<td class="dash">
					<table class="dash">
						<tr>
							<td colspan="<?php echo $total_columns?>"><?php echo $this->HeaderButton($lang->FMLabel, $fmcategory); ?></td>
						</tr>
						<tr id="ph1">					
						<?php 
						for ($x = 0; $x < $total_columns; $x++) {
							$col = null;
							if (isset($column_html[MP_FM]) && array_key_exists($x, $column_html[MP_FM])) {
								$col = $column_html[MP_FM][$x];
							}
							echo $this->DrawDbCol($col);
						}
						?>
						</tr>
					</table>
				</td>
				<td class="divider">
					<img src="images/spacer.gif" alt="spacer" style="width:5px; height:0;">
				</td>
				<td class="dash">
					<table class="dash">
						<tr>
							<td colspan="<?php echo $total_columns?>"><?php echo $this->HeaderButton($lang->IPLabel, $ipcategory); ?></td>
							<td  class="divider" rowspan=4>
								<img src="images/spacer.gif" alt="spacer" style="width:5px; height:0;">
							</td>
							<td colspan="<?php echo $total_columns?>"><?php echo $this->HeaderButton($lang->DPLabel, $dpcategory); ?></td>
						</tr>
						<tr id="ph2">					
						<?php 
						for ($x = 0; $x < $total_columns; $x++) {
							$col = null;
							if (isset($column_html[MP_IP]) && array_key_exists($x, $column_html[MP_IP])) {
								$col = $column_html[MP_IP][$x];
							}
							echo $this->DrawDbCol($col);
						}
						for ($x = 0; $x < $total_columns; $x++) {
							$col = null;
							if (isset($column_html[MP_DP]) && isset($column_html[MP_DP]) && array_key_exists($x, $column_html[MP_DP])) {
								$col = $column_html[MP_DP][$x];
							}
							echo $this->DrawDbCol($col);
						}
						?>
						</tr>
						<tr>
							<td colspan="<?php echo $total_columns?>">
								<img src="images/spacer.gif" alt="spacer" style="width:1px; height:80px;">
								<?php echo $this->HeaderButton($lang->SLabel, $ipscategory, true); ?>
							</td>
							<td colspan="<?php echo $total_columns?>">
								<img src="images/spacer.gif" alt="spacer" style="width:1px; height:80px;">
								<?php echo $this->HeaderButton($lang->SLabel, $dpscategory,true); ?>
							</td>
						</tr>
						<tr id="ph3">					
						<?php 
						for ($x = 0; $x < $total_columns; $x++) {
							$col = null;
							if (isset($column_html[MP_IPS]) && array_key_exists($x, $column_html[MP_IPS])) {
								$col = $column_html[MP_IPS][$x];
							}
							echo $this->DrawDbCol($col);
						}
						for ($x = 0; $x < $total_columns; $x++) {
							$col = null;
							if (isset($column_html[MP_DPS]) && array_key_exists($x, $column_html[MP_DPS])) {
								$col = $column_html[MP_DPS][$x];
							}
							echo $this->DrawDbCol($col);
						}
						?>
						</tr>
					</table>
				</td>
				<td class="divider">
					<img src="images/spacer.gif" alt="spacer" style="width:5px; height:0;">
				</td>
				<td class="dash">
					<table class="dash">
						<tr>
							<td colspan="<?php echo $total_columns?>"><?php echo $this->HeaderButton($closed_label[0], $closedcategory, false, $closed_interval[0]); ?></td>
						</tr>
						<tr id="ph6.1">					
						<?php 
						for ($x = 0; $x < $total_columns; $x++) {
							$col = null;
							if (isset($column_html[MP_CLOSED . '-0']) && array_key_exists($x, $column_html[MP_CLOSED . '-0'])) {
								$col = $column_html[MP_CLOSED . '-0'][$x];
							}
							echo $this->DrawDbCol($col);
						}
						?>
						</tr>
						<tr>
							<td colspan="<?php echo $total_columns?>">
								<img src="images/spacer.gif" alt="spacer" style="width:1px; height:80px;">
								<?php echo $this->HeaderButton($closed_label[1], $closedcategory, false, $closed_interval[1]); ?>
							</td>
						</tr>
						<tr id="ph6.2">
						<?php 
						for ($x = 0; $x < $total_columns; $x++) {
							$col = null;
							if (isset($column_html[MP_CLOSED . '-1']) && array_key_exists($x, $column_html[MP_CLOSED . '-1'])) {
								$col = $column_html[MP_CLOSED . '-1'][$x];
							}
							echo $this->DrawDbCol($col);
						}
						?>
						</tr>
						<tr>
							<td colspan="<?php echo $total_columns?>">
								<img src="images/spacer.gif" alt="spacer" style="width:1px; height:80px;">
								<?php echo $this->HeaderButton($closed_label[2], $closedcategory, false, $closed_interval[2]); ?>
							</td>
						</tr>
						<tr id="ph6.3">					
						<?php 
						for ($x = 0; $x < $total_columns; $x++) {
							$col = null;
							if (isset($column_html[MP_CLOSED . '-2']) && array_key_exists($x, $column_html[MP_CLOSED . '-2'])) {
								$col = $column_html[MP_CLOSED . '-2'][$x];
							}
							echo $this->DrawDbCol($col);
						}
						?>
						</tr>
					</table>
				</td>
			</tr>
		</table>	
		</div>
		<?php	
	}

}

function GetStdFromPercent($percent) {
	// NY - I'm not sure how these numbers were arrived at
	$std_conv =	array(
		0.0000, // 50
		0.0180, // 51
		0.0461, // 52
		0.0681, // 53
		0.0962, // 54
		0.1183, // 55
		0.1464, // 56
		0.1686, // 57
		0.1968, // 58
		0.2190, // 59
		0.2474, // 60
		0.2697, // 61
		0.2983, // 62
		0.3269, // 63
		0.3494, // 64
		0.3783, // 65
		0.4072, // 66
		0.4364, // 67
		0.4592, // 68
		0.4885, // 69
		0.5181, // 70
		0.5477, // 71
		0.5776, // 72
		0.6076, // 73
		0.6378, // 74
		0.6683, // 75
		0.6989, // 76
		0.7297, // 77
		0.7677, // 78
		0.7990, // 79
		0.8377, // 80
		0.8694, // 81
		0.9088, // 82
		0.9485, // 83
		0.9887, // 84
		1.0292, // 85
		1.0779, // 86
		1.1192, // 87
		1.1690, // 88
		1.2193, // 89
		1.2785, // 90
		1.3385, // 91
		1.3993, // 92
		1.4694, // 93
		1.5494, // 94
		1.6395, // 95
		1.7492, // 96
		1.8794, // 97
		2.0497, // 98
		2.3199  // 99
	);

	$percent = $percent	- 50;

	if ($percent < 0) {
		$percent = 0;
	}
	else {
		if ($percent > 49) $percent	= 49;
	}
	return $std_conv[$percent];
}
