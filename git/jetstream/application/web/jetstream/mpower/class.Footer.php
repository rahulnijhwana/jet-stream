<?php

require_once(BASE_PATH . '/mpower/class.PageComponent.php');

class Footer extends PageComponent 
{
	public function Render() {
		$output = '<div id="footer">' . Language::__get('Trademark') . '</div>';
		return $output;
	}
}
