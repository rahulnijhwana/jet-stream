<?php
require_once(BASE_PATH . '/mpower/class.PageComponent.php');
require_once(BASE_PATH . '/include/class.DbConnManager.php');
require_once(BASE_PATH . '/include/class.SqlBuilder.php');

class ManagerAdmin extends PageComponent 
{	
	private $users_array;
	private $total_period;
	private $company_seasonality;
	public $year;
	
	function __construct() {
		$this->year = isset($_POST['Year']) ? (int) $_POST['Year'] : date('Y');
	}

	public function GetJsInit() {
		return;
	}
	
	public function GetJs() {		
		$current_= date('n');
		$output = "var sn = '{$_GET['SN']}';\n";
		$output .= "var total_period = '$this->total_period';\n";
		return $output;
	}
	
	public function GetCssFiles() {
		return array('manager_admin.css', array('print', 'manager_admin_print.css'));
	}

	public function GetJsFiles() {
		return array('jquery.js', 'manager_admin.js', 'color_picker.js');
	}
	
	public function GetCompanySeasonality() {	
		$this->company_seasonality = array_fill(1, 13, '');
		
		$sql = "SELECT SeasonalityEnabled
				FROM Seasonality WHERE CompanyID = ? AND PersonID IS NULL AND Year = ? AND PersonID IS NULL AND Seasonality IS NULL";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['mpower_companyid']), array(DTYPE_INT, $this->year));
		$seasonality_enabled_rs = DbConnManager::GetDb('mpower')->getOne($sql);
		
		if ($seasonality_enabled_rs['SeasonalityEnabled']) {
			$seasonality_sql = "SELECT Period, Seasonality FROM Seasonality WHERE CompanyID = ? AND PersonID IS NULL AND Year = ?";
			$seasonality_sql = SqlBuilder()->LoadSql($seasonality_sql)->BuildSql(array(DTYPE_INT, $_SESSION['mpower_companyid']), array(DTYPE_INT, $this->year));
			$seasonality_rs = DbConnManager::GetDb('mpower')->Execute($seasonality_sql);
			
			if ($seasonality_rs) {
				foreach ($seasonality_rs as $seasonality) {
					$this->company_seasonality[$seasonality->Period] = $seasonality->Seasonality;
				}		
			}
		}
	}

	public function GetInfo($targets) {
	
		$sql = "SELECT P.PersonID, Color, FirstName, LastName, FMThreshold, IPThreshold, DPThreshold, CloseThreshold, 
						Deleted, CompanySetGoal, SeasonalityEnabled  
				FROM people P
				LEFT JOIN GOAL G ON P.PersonID = G.PersonID AND Year = ?
				LEFT JOIN Seasonality S ON S.PersonID = P.PersonID AND S.Year = ? AND S.Period IS NULL and S.Seasonality IS NULL
				WHERE P.companyid = ? AND P.PersonID IN ($targets) ORDER BY FirstName, LastName";
				
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->year), array(DTYPE_INT, $this->year), array(DTYPE_INT, $_SESSION['mpower_companyid']));
		//echo $sql;
		$users_rs = DbConnManager::GetDb('mpower')->Execute($sql);		
		
		foreach ($users_rs as $users) {
			$this->users_array[$users->PersonID]['Color'] = $users->Color;
			$this->users_array[$users->PersonID]['FirstName'] = $users->FirstName;
			$this->users_array[$users->PersonID]['LastName'] = $users->LastName;
			$this->users_array[$users->PersonID]['FMThreshold'] = $users->FMThreshold;
			$this->users_array[$users->PersonID]['IPThreshold'] = $users->IPThreshold;
			$this->users_array[$users->PersonID]['DPThreshold'] = $users->DPThreshold;
			$this->users_array[$users->PersonID]['CloseThreshold'] = $users->CloseThreshold;
			$this->users_array[$users->PersonID]['CompanySetGoal'] = $users->CompanySetGoal;
			$this->users_array[$users->PersonID]['Deleted'] = $users->Deleted;
			
			$this->users_array[$users->PersonID]['Seasonality'] = $users->SeasonalityEnabled;
				
			$period = $this->company_seasonality;
			if ($users->SeasonalityEnabled) {
				$seasonality_sql = "SELECT Period, Seasonality FROM Seasonality WHERE PersonID = ? AND Year = ?";
				$seasonality_sql = SqlBuilder()->LoadSql($seasonality_sql)->BuildSql(array(DTYPE_INT, $users->PersonID), array(DTYPE_INT, $this->year));
				$seasonality_rs = DbConnManager::GetDb('mpower')->Execute($seasonality_sql);
				foreach ($seasonality_rs as $seasonality) {		
					$period[$seasonality->Period] = $seasonality->Seasonality;
				}			
			}
			$this->users_array[$users->PersonID]['Period'] = $period;
		} 
	}
	
	public function GetTargets($targets) {
	
		$sql = "SELECT PersonID	FROM people WHERE (SupervisorID IN ($targets) OR PersonID in ($targets)) AND IsSalesPerson = 1";			
		$users_rs = DbConnManager::GetDb('mpower')->Execute($sql);		
		
		$users_list = array();
		foreach ($users_rs as $users) {
			$users_list[] = $users->PersonID;
		}

		return $users_list;
	}	
	
	function FormatCurrency($amount) {
		return '$'.number_format($amount);
	}
		
	function GetCompanyPeriods() {
		
		$sql = "SELECT count(Period) as TotalPeriod
				FROM FiscalYear
				WHERE CompanyID = ? AND Year = ? AND FiscalYearEndDate is NULL AND PeriodEndDate is NOT NULL";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['mpower_companyid']), array(DTYPE_INT, $this->year));
		$company = DbConnManager::GetDb('mpower')->getOne($sql);
		$this->total_period = $company->TotalPeriod;
	}

	function getMpowerYears() {
	
		$sql = "select min(year(createdate)) MinYear from opportunities where companyid = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['mpower_companyid']));
		$opp = DbConnManager::GetDb('mpower')->getOne($sql);
		$min_year = $opp->MinYear;
		
		$years = range($min_year, date('Y') +  1);
		return $years;
	}

	public function Render() {
		echo '<!--test-->';
		$this->GetCompanySeasonality();
		$this->GetCompanyPeriods();
		
		$user = $_SESSION['login_id'];
		$user_list = '';
		
		if ($_SESSION['tree_obj']->IsSeniorMgr($user)) {
			$managers_list = $this->GetTargets($user);			
			$managers = implode(',', $managers_list);			
			$salespersons_list = $this->GetTargets($managers);			
			$salespersons = implode(',', $salespersons_list);			
			$user_list = $managers . ',' . $salespersons;			
			
		} else {
			$salespersons_list = $this->GetTargets($user);			
			$salespersons = implode(',', $salespersons_list);			
			$user_list	= $salespersons;
		}		
		
		$this->GetInfo($user_list);
		$manager_admin = SessionManager::CreateUrl('manager_admin.php');
		
		$forcast_body = '<div><form name="myform" id="myform" action="' . $manager_admin . '" method="post">';
		$forcast_body .= '<div align="left"><b>Seasonality </b></div>';
		$forcast_body .= '<div align="right"><a href="#Edit" onClick="edit()">Edit</a></div>';
		$forcast_body .= '<div align="left"><b>Current Year: ' . $this->year . '</b></div>';
		
		$year_list = $this->getMpowerYears();
		
		$year_select = '<div align="right" style="margin-right:40px;">
							Please select a Year
										<select name="Year" id="year" style="width:100px;">
											<option value="-1"></option>';
		$year_selerct_option = '';
		foreach($year_list as $year) {
			if($year == $this->year) {
				$year_selerct_option .= '<option value="' . $year . '" selected>' . $year . '</option>';
			} else {
				$year_selerct_option .= '<option value="' . $year . '">' . $year . '</option>';
			}
			
		}
		$year_select .= $year_selerct_option;
		$year_select .= '</select><input type="submit" name="GoBtn" id="GoBtn" value="Go"></div>';
		$forcast_body .= $year_select;											
		
		$forcast_body .= '<table border="0" cellpadding="4" cellspacing="4">';			
		
		if ($this->total_period == 12) {
			$seasonality = '<th>Jan</th><th>Feb</th><th>Mar</th><th>Apr</th><th>May</th><th>Jun</th><th>Jul</th><th>Aug</th><th>Sep</th><th>Oct</th><th>Nov</th><th>Dec</th>';						
		} else {
			$seasonality = '<th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th><th>7</th><th>8</th><th>9</th><th>10</th><th>11</th><th>12</th><th>13</th>';
		}

		$forcast_body .= '<thead><tr align="center"><th colspan="3"></th><th colspan="4" class="thresholds">Thresholds</th><th></th><th align="left" colspan="13" class="seasonality">Seasonality</th></tr>
							<tr align="center"><th>Enable</th><th>Salesperson</th><th>Goal</th>
							<th class="thresholds">FM</th><th class="thresholds">IP</th><th class="thresholds">DP</th>
							<th class="thresholds">C</th><th>Color</th><th>&nbsp;</th>'.$seasonality.'</tr></thead><tbody>';	
		$content = '';	
		$id = 1;
		foreach ($this->users_array as $key=>$personid) {
		
			$name = $personid['FirstName'] . ' ' . $personid['LastName'];
			$checked = '';			
			if ($personid['Deleted'] == 0) $checked = 'checked="true"';
			
			$content .= '<tr align="center"><td><input type="hidden" name="person[]" id="person_' . $id . '" ' . $checked . ' value="' . $key . '" />
						<input type="checkbox" name="user[]" id="user_' . $id . '" ' . $checked . ' value="' . $key . '" /></td><td align=left>' . $name . '</td>';			
			$content .= '<td><input size="10" maxlength="20" type="text" onBlur="checkGoal(\'CompanySetGoal_' . $id . '\')" name="CompanySetGoal[]" id="CompanySetGoal_' . $id . '" value="' . $this->FormatCurrency($personid['CompanySetGoal']) . '" /></td>';
			$content .= '<td class="thresholds"><input type="text" name="FMThreshold[]" id="FMThreshold_' . $id . '" value="' . $personid['FMThreshold'] . '" size="2" maxlength="3" /></td>';
			$content .= '<td class="thresholds"><input type="text" name="IPThreshold[]" id="IPThreshold_' . $id . '" value="' . $personid['IPThreshold'] . '" size="2" maxlength="3" /></td>';
			$content .= '<td class="thresholds"><input type="text" name="DPThreshold[]" id="DPThreshold_' . $id . '" value="' . $personid['DPThreshold'] . '" size="2" maxlength="3" /></td>';
			$content .= '<td class="thresholds"><input type="text" name="CloseThreshold[]" id="CloseThreshold_'.$id.'" value="' . $personid['CloseThreshold'] . '" maxlength="3" size="2" /></td>';
			$content .= '<td>
						<input type="hidden" id="pick_' . $id . 'field" size="7" name="Color[]" value="' . $personid['Color'] . '" onchange="relateColor(\'pick_' . $id . '\', this.value);">
						<div onclick="javascript:pickColor(\''.$id.'\');" id="pick_'.$id.'" class="color_picker" style="background-color:' . $personid['Color'] . '">&nbsp;</a>
						<div id="pick_' . $id . '_div"></div></td>';						
			
			$seasonality_checked = ($personid['Seasonality'] == 1) ? 'checked' : '';
			$display = ($personid['Seasonality'] == 1) ? '' : ' disabled ';
			
			$content .= '<td><input type="checkbox" name="SeasonalityCheck[]" onClick="SeasonalityCheck(this.id, ' . $id . ', true )" id="Sesonality_check_' . $id . '" value="' . $key . '" ' . $seasonality_checked . ' /></td>';
			$content .= '<td class="sesonality"><input type="text" name="Sesonality[]" onBlur="calculateSeasonality()" onKeyUp="calculateSeasonality()" id="Sesonality_' . $id . '_1" value="' . $personid['Period'][1] . '" size="1" maxlength="3" ' . $display . ' /></td>';
			$content .= '<td class="sesonality"><input type="text" name="Sesonality[]" onBlur="calculateSeasonality()" onKeyUp="calculateSeasonality()" id="Sesonality_' . $id . '_2" value="' . $personid['Period'][2] . '" size="1" maxlength="3" ' . $display . ' /></td>';
			$content .= '<td class="sesonality"><input type="text" name="Sesonality[]" onBlur="calculateSeasonality()" onKeyUp="calculateSeasonality()" id="Sesonality_' . $id . '_3" value="' . $personid['Period'][3] . '" size="1" maxlength="3" ' . $display . ' /></td>';
			$content .= '<td class="sesonality"><input type="text" name="Sesonality[]" onBlur="calculateSeasonality()" onKeyUp="calculateSeasonality()" id="Sesonality_' . $id . '_4" value="' . $personid['Period'][4] . '" size="1" maxlength="3" ' . $display . ' /></td>';
			$content .= '<td class="sesonality"><input type="text" name="Sesonality[]" onBlur="calculateSeasonality()" onKeyUp="calculateSeasonality()" id="Sesonality_' . $id . '_5" value="' . $personid['Period'][5] . '" size="1" maxlength="3" ' . $display . ' /></td>';
			$content .= '<td class="sesonality"><input type="text" name="Sesonality[]" onBlur="calculateSeasonality()" onKeyUp="calculateSeasonality()" id="Sesonality_' . $id . '_6" value="' . $personid['Period'][6] . '" size="1" maxlength="3" ' . $display . ' /></td>';
			$content .= '<td class="sesonality"><input type="text" name="Sesonality[]" onBlur="calculateSeasonality()" onKeyUp="calculateSeasonality()" id="Sesonality_' . $id . '_7" value="' . $personid['Period'][7] . '" size="1" maxlength="3" ' . $display . ' /></td>';
			$content .= '<td class="sesonality"><input type="text" name="Sesonality[]" onBlur="calculateSeasonality()" onKeyUp="calculateSeasonality()" id="Sesonality_' . $id . '_8" value="' . $personid['Period'][8] . '" size="1" maxlength="3" ' . $display . ' /></td>';
			$content .= '<td class="sesonality"><input type="text" name="Sesonality[]" onBlur="calculateSeasonality()" onKeyUp="calculateSeasonality()" id="Sesonality_' . $id . '_9" value="' . $personid['Period'][9] . '" size="1" maxlength="3" ' . $display . ' /></td>';
			$content .= '<td class="sesonality"><input type="text" name="Sesonality[]" onBlur="calculateSeasonality()" onKeyUp="calculateSeasonality()" id="Sesonality_' . $id . '_10" value="' . $personid['Period'][10] . '" size="1" maxlength="3" ' . $display . ' /></td>';
			$content .= '<td class="sesonality"><input type="text" name="Sesonality[]" onBlur="calculateSeasonality()" onKeyUp="calculateSeasonality()" id="Sesonality_' . $id . '_11" value="' . $personid['Period'][11] . '" size="1" maxlength="3" ' . $display . ' /></td>';
			$content .= '<td class="sesonality"><input type="text" name="Sesonality[]" onBlur="calculateSeasonality()" onKeyUp="calculateSeasonality()" id="Sesonality_' . $id . '_12" value="' . $personid['Period'][12] . '" size="1" maxlength="3" ' . $display . ' /></td>';
			$extra_period = 'style="display:none"';
			
			if ($this->total_period == 13) $extra_period = '';
			
			$content .= '<td class="sesonality" id="extra_period_' . $id . '" ' .  $extra_period . '><input type="text" name="Sesonality[]" onBlur="calculateSeasonality()" onKeyUp="calculateSeasonality()" id="Sesonality_' . $id . '_13" value="' . $personid['Period'][13] . '" size="1" maxlength="3" /></td>';
			$content .= '<td class="sesonality" id="equal_sign_' . $id . '"  ' . $display . ' >=</td>';
			$content .= '<td class="sesonality"><span id="Sesonality_total_' . $id . '"  ' . $display . ' ></span></td>';
			$content .= '</tr>';
			$id++;
		}
		
		$content .= '<tr><td colspan="2" id="edit_buttons">
					<input type="button" name="submit" id="submit" value="Submit" onClick="doSubmit();" />
					<input type="reset" name="cancel" id="cancel" value="Cancel"  onClick="doCancel();"/>
					</td>
					<td colspan="18">&nbsp;</td></tr>';

		$forcast_body .= $content.'</tbody></table>';
		$forcast_body .= '</br>' . $this->pendingGoal($user_list) ;
		$forcast_body .= '</form>';
		$forcast_body .= '</div>';
		$forcast_body .= '<br>';
		
		return $forcast_body;
	}
		
	
	
	// add another division that add the fucntion of adding in Goal 
	// naming the division Goal 
	// additional content xxxGOALxxx
	function pendingGoal ($targets) {

		$sql = "SELECT PendingReport  FROM company WHERE CompanyID = ? ";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['mpower_companyid']));
		$users_rs = DbConnManager::GetDb('mpower')->GetOne($sql);	
		
		if ( $users_rs->PendingReport != 1 ) {
			return ; 
		}
		
		$fiscal_month = $this->getFiscalMonth();
		
		$sql = "SELECT P.PersonID, R.goal, R.TotalGoal, P.FirstName + ' '+ P.LastName as Name, R.PendingID, R.Year
				FROM People P 
					left OUTER JOIN Pending R ON P.PersonID = R.PersonID and Year = ? 
				WHERE P.PersonID in ($targets) ORDER BY Name";
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->year) );
		$users_rs = DbConnManager::GetDb('mpower')->Execute($sql);	

		$forcast_body1 =  '<div name="goals">';
		$forcast_body1 .= "<input type='hidden' name='pendingReport' value='1'>";   
		$forcast_body1 .= '<table border="0" cellpadding="4" cellspacing="4">';
		$months_list = array (  0,'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' );
		$month = $fiscal_month;
		$seasonality  = '' ;
		$temp_year = $this->year ; 
		for ($i = 0 ; $i < 12 ; $i++){
			$temp_year = (string) $temp_year ; 
			$seasonality .= "<th>" . $months_list[$month]. " '". $temp_year[2] . $temp_year[3] . "</th>" ; 
			$month++ ; 
			if ( $month > 12){ 
				$month = 1;
				$temp_year = (int) $temp_year + 1 ; 
			}
		}
		$forcast_body1 .= '<thead><tr align="center"><th colspan="3"></th><th colspan="4" class="thresholds">Pending Goals</th><th></th><th align="left" colspan="13" class="seasonality">&nbsp;</th></tr>
						<tr align="center"><th width="50px">&nbsp;</th><th>Salesperson</th>'.$seasonality.'<th>Total</th></tr></thead><tbody>';	
		$content1 = '';	
		$id = 1;
		$breakdown_list = array();
		foreach ($users_rs as $users) {
			$content1 .= '<tr align="center"><td>&nbsp;</td><td align=left>' . $users['Name'] . '<input type=hidden name="PendingID['. $users['PersonID'].']" value="' ;

			if ($users['PendingID'])$content1 .= $users['PendingID']; 
			else $content .= "0";
			
			$content1.= '"></td>';			
			$breakdown_list = explode ( ';', $users['goal'] );
			$month = $fiscal_month ; 
			for( $i= 1 ; $i <= 12 ; $i++){
				$content1 .= '<td class="pending_goal"><input type="text" name="goal_'. $users['PersonID'].'['. $month .']" id="goal_'. $users['PersonID'].'['. $month .']" '; 
					
				$content1 .= ' value="'.$this->FormatCurrency($this->unformat_money($breakdown_list[$month- 1])) . '" size="6" maxlength="8" onChange="javascript:updateValues(\''.$users['PersonID'].'\')" onFocus="javascript:onFocusDollar(this)" onBlur="javascript:onBlurDollar(this)" onKeyPress="javascript:return moneyonly(this, event)"/></td>';			
				$month++ ; 
				if ( $month > 12 ) $month = 1 ; 
			}
			$content1 .= '<td><input size="10" maxlength="20" type="text" name="total_'. $users['PersonID'] .'" value="' . $this->FormatCurrency($this->unformat_money($users['TotalGoal'])) . '" disable /></td>';			
			$content1.= '</tr>';
		}
/*		$content1 .= '<tr><td colspan=15 id="edit_buttons"> 
					<input type="button" name="submit" id="submit" value="Submit" onClick="doSubmit();" />
					<input type="reset" name="cancel" id="cancel" value="Cancel"  onClick="doCancel();"/>
					</td></tr>' ; 
*/
		$forcast_body1 .= $content1 .'</tbody></table></div>';			
		return  $forcast_body1 ; 

	}
function unformat_money( $money) {
	$money = str_replace("$", '', $money); 
	$money = str_replace(",", '', $money); 
	return $money;
}

function getFiscalMonth() {

	$months_list = array ( '0', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' );

	$strsql = "SELECT CompanyID, FiscalYearEndDate FROM FiscalYear where companyID = ? and FiscalYearEndDate is not NULL" ; 
	$strsql = SqlBuilder()->LoadSql($strsql)->BuildSql(array(DTYPE_INT, $_SESSION['mpower_companyid']));
	$users_rs = DbConnManager::GetDb('mpower')->Exec($strsql);	
	
	$part = explode(" ", $users_rs->FiscalYearEndDate );
	if ( count($part) > 2 ) {
		for ($v = 0 ; $v < 12 ; $v++){
			if ( $part[0] == $months_list[$v] ) return ($v + 1); 	
		}	
	}
	else {
		$parts = explode("/", $part[0]) ;
		return ( (int) $parts[0] + 1 ) ; 
	}
	return 0  ; 
	}
	
}
