<?php
require_once BASE_PATH . '/mpower/class.PageComponent.php';
require_once BASE_PATH . '/include/lib.date.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';

class Calendar extends PageComponent
{
	public $cal_start;
	public $week_start_day;
	public $target;

	public function GetCssFiles() {
		return array('opp.css', 'calendar.css','calendar_print.css', array('print','opp_print.css'), array('print','calendar_print.css'));
	}

	public function GetJsInit() {
		return "var date_select = document.getElementById('cal_start');
			if (date_select.selectedIndex != 0) {
				document.getElementById('cal_left').style.visibility = '';
			}
			if (date_select.selectedIndex != date_select.length - 1) {
				document.getElementById('cal_right').style.visibility = '';
			}";
	}

	public function Render() {
		echo $this->MakeDateSelector();
		echo '<div id="cal_warning" style="display:none;font-weight:bold;color:#B70000">No salesperson selected</div><br>';

		// First day should be a Monday.  Last day is a Friday  Calendar always shows 5 weeks
		// Sunday and Saturday columns are hidden unless there are appointments on those days
		$cal_start = strtotime($this->cal_start);

		$cal_length = 35;  //days

		// Make sure that the calendar always starts on a Sunday
		$cal_start  = mktime(0, 0, 0, date("m", $cal_start),  date("d", $cal_start) - date('w', $cal_start), date("Y", $cal_start));
		$cal_end  = mktime(0, 0, 0, date("m", $cal_start),  date("d", $cal_start) + $cal_length, date("Y", $cal_start));

		$detail = is_int($this->target);
		$lang = Language::GetInstance();

		$people_list = $this->target;

		if ($detail) {
			$people_list = array($people_list);
		}

		// Contortions to get numbers for the salespeople.  Starts at 0
		$people_num = array_flip(array_values($people_list));

		$phase_translation = array(10=>'T', 1=>'FM', 2=>'IP', 3=>'IPS', 4=>'DP', 5=>'DPS', 6=>'C', 9=>'R');

		$meeting_sql = "SELECT PersonID, Category, VLevel, account." . $_SESSION['account_name'] . " as Company, Division, FirstMeeting + FirstMeetingTime as Meeting, DealID
			FROM opportunities
			LEFT JOIN	account ON	opportunities.AccountID = account.AccountID
			WHERE opportunities.PersonID IN	(" . ArrayQm($people_list) . ") AND
			Category = 1 AND FirstMeeting >= ? AND FirstMeeting <= ?
			UNION ALL
			SELECT  PersonID, Category, VLevel, account." . $_SESSION['account_name'] . " as Company, Division, NextMeeting + NextMeetingTime as Meeting, DealID
			FROM opportunities
			LEFT JOIN	account ON	opportunities.AccountID = account.AccountID
			WHERE opportunities.PersonID IN	(" . ArrayQm($people_list) . ") AND
			Category in (2, 4) AND NextMeeting >= ? AND NextMeeting <= ?
			order by Meeting, PersonID";

		$meeting_sql = SqlBuilder()->LoadSql($meeting_sql)->BuildSql(array(DTYPE_INT, $people_list), array(DTYPE_TIME, date('m-d-Y', $cal_start)), array(DTYPE_TIME, date('m-d-Y', $cal_end)), array(DTYPE_INT, $people_list),  array(DTYPE_TIME, date('m-d-Y', $cal_start)), array(DTYPE_TIME, date('m-d-Y', $cal_end)));
		$meeting_rs	= DbConnManager::GetDb('mpower')->Execute($meeting_sql);

		$calendar_days = array();

		$week_start = 1;
		$week_end = 5;
		$next_meeting_conflict = false;

		for ($meeting_num = 0; $meeting_num < count($meeting_rs); $meeting_num++) {
			$meeting = $meeting_rs[$meeting_num];

			$appointment = array();

			$appt_date = strtotime($meeting->Meeting);

			$opp_name = $meeting->Company;
			if (!is_null($meeting->Division)) {
				$opp_name .= ' - ' . $meeting->Division;
			}

			$appointment['conflict'] = false;

			if ($next_meeting_conflict) {
				$appointment['conflict'] = true;
				$next_meeting_conflict = false;
			}
			if ($meeting_num + 1 < count($meeting_rs)) {
				$next_meeting = $meeting_rs[$meeting_num + 1];
				if ($meeting->PersonID ==  $next_meeting->PersonID && strtotime($meeting->Meeting) ==  strtotime($next_meeting->Meeting)) {
					$appointment['conflict'] = true;
					$next_meeting_conflict = true;
				}
			}

			$appointment['person'] = $meeting->PersonID;
			$appointment['time'] = date('g:i A', $appt_date);
			$appointment['opp_name'] = $opp_name;
			$appointment['phase'] = $lang->{$phase_translation[ $meeting->Category] . 'Abbr'};
			$appointment['valuation'] = ($meeting->VLevel > 0) ? 'V' . $meeting->VLevel : null;
			$appointment['DealID'] = $meeting->DealID;
			$appointment['hash'] = $meeting->DealID . '_' . $meeting->PersonID . '_' . $meeting->VLevel;

			$calendar_days[date('Ymd', $appt_date)][] = $appointment;

			if (date("w", $appt_date) == 0) {
				$week_start = 0;
			}
			if (date("w", $appt_date) == 6) {
				$week_end = 6;
			}
		}
		echo '<table id="calendar">';
		$start_day = $cal_start;
		for($day = 0; $day < $cal_length; $day++) {
			$current_day  = mktime(0, 0, 0, date("m", $start_day), date("d", $start_day) + $day, date("Y", $start_day));
			if (date("w", $current_day) == $week_start) {
				echo "<tr>";
			}
			if (date("w", $current_day) >= $week_start && date("w", $current_day) <= $week_end) {
				echo "<td class=\"cal_day\"><div class=\"day_label\">" . Language::__get(date('l', $current_day)) . ',&nbsp;' . date('n/j/Y', $current_day) . '</div>';

				if (isset($calendar_days[date('Ymd', $current_day)])) {
					foreach($calendar_days[date('Ymd', $current_day)] as $appointment) {
						echo $this->MakeApptOpp($people_num[$appointment['person']] + 1, $appointment, $_SESSION['tree_obj']->GetInfo($appointment['person'], 'Color'), $detail);
					}
				}

				echo "</td>";
			}
			if (date("w", $current_day) == $week_end) {
				echo "</tr>";
			}
		}
		echo "</table></div>";
	}

	private function MakeDateSelector() {
		if (isset($_POST['cal_start'])) {
			$this->cal_start = $_POST['cal_start'];
		} else {
			$this->cal_start = date('Ymd');
		}
		$choices = array(date('Ymd') => Language::__get('5weeks'));

		$start_month = mktime(0, 0, 0, date("m"), 1, date("Y"));
		for ($x = 0; $x < 6; $x++) {
			$current_month  = mktime(0, 0, 0, date("m", $start_month) + $x, 1, date("Y", $start_month));
			$choices[date('Ymd', $current_month)] = Language::__get(date('F', $current_month)) . date(' Y', $current_month);
		}

		ksort($choices);
		$output = '<div align="center" style="border:0px solid coral">
			<form name="date_picker" method="POST" class="date_picker">
				<input id="cal_left" type="image" src="images/left_arrow.png" style="margin-bottom:-6px;visibility:hidden;"  onclick="document.getElementById(\'cal_start\').selectedIndex -= 1">
				<span style="display:inline;padding:0 20px 0 20px;">
					<select id="cal_start" name="cal_start" onchange="this.form.submit();">';
		foreach($choices as $key => $choice) {
			$selected = '';
			if ($key == $this->cal_start) {
				$selected = 'selected';
			}
			$output .= "<option value=\"$key\" $selected>$choice</option>";
		}
		$output .= '</select>
				</span>
				<input id="cal_right" type="image" src="images/right_arrow.png" style="margin-bottom:-6px;visibility:hidden;" onclick="document.getElementById(\'cal_start\').selectedIndex += 1">
			</form>';
		return $output;
	}

	private function MakeApptOpp($person_num, $appointment, $background, $detail) {
		$body_text = $appointment['time'] . '<br>' .
			$appointment['opp_name'] . '<br>(' . $appointment['phase'];
		if ($appointment['valuation']) {
			$body_text .= ', ' . $appointment['valuation'];
		}
		$body_text .= ')';
		$personid = $appointment['person'];
		$left = '';
		$right = '';

		$conflict = ($appointment['conflict']) ? 'conflict' : '';

		if ($detail) {
			$person_num = 'Dup';
			$conflict = '';
		}

		if ($detail && !$appointment['conflict']) {
			$right = $body_text;
		} else {
			$left = "<div class=\"appt_person $conflict\">$person_num</div>";
			$right = $body_text;
		}

		$url = 'legacy/shared/edit_opp2.php?SN=' . $_REQUEST['SN'] . '&detailview=1&time=' . strtotime("now") . '&reqOpId='.$appointment['DealID'].'&reqPersonId='.$appointment['person'].'#'.$appointment['hash'];

		$cal_opp =<<<END_HTML
			<table name="sp{$personid}appt" class="opp opp_cal" style="background:$background;" onclick="var win = Windowing.openSizedWindow('$url', 630, 1000);">
				<tr>
					<td class="tl">$left</td>
					<td class="tr">$right</td>
				</tr>
				<tr>
					<td class="bl"></td>
					<td class="br"></td>
				</tr>
			</table>
END_HTML;

		return $cal_opp;
	}
}
