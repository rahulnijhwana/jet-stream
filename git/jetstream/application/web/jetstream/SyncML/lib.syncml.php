<?php
require '../include/class.DbConnManager.php';
require '../include/class.SqlBuilder.php';
require '../include/class.RecSyncmlDevice.php';
require '../include/lib.date.php';
require '../include/class.MapLookup.php';
require '../include/class.OptionLookup.php';
require '../slipstream/class.Contact.php';
require '../include/class.RecEvent.php';
require '../include/class.RecAccount.php';
require '../slipstream/class.ModuleCompanyContactNew.php';
require '../include/class.JetDateTime.php';

require 'vcard.php';

function AuthenticateCredentials($cred) {
	list($user_info, $password) = explode(':', base64_decode($cred));
	list($company_name, $user_name) = explode('/', $user_info);
	
	if (empty($password) || empty($company_name) || empty($user_name)) {
		return false;
	}
	
	$login_sql = 'SELECT PersonID, Password, Company.CompanyID 
		FROM Logins JOIN Company ON Logins.CompanyID = Company.CompanyID
		WHERE Company.DirectoryName = ? AND UserID = ?';
	
	$login_sql = SqlBuilder()->LoadSql($login_sql)->BuildSql(array(DTYPE_STRING, $company_name), array(DTYPE_STRING, $user_name));
	$login = DbConnManager::GetDb('mpower')->GetOne($login_sql);
	
	if (!$login || $login->Password != crypt($password, $login->Password)) {
		return false;
	}
	
	return array('CompanyID' => $login->CompanyID, 'PersonID' => $login->PersonID);
}

function LoadTargetConfig($uri) {
	$uri = (string) $uri;

	// Load the record from the database
	$sql = "SELECT * FROM SyncmlDevice WHERE DeviceID = ?";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $uri));
	
	$recordset = DbConnManager::GetDb('mpower')->Execute($sql, 'RecSyncmlDevice');
	
	if (count($recordset) == 0) {
		$record = new RecSyncmlDevice();
		$recordset[] = $record;
		$record->SetDatabase(DbConnManager::GetDb('mpower'));
		$record->Initialize();
		$record->DeviceID = $uri;
		$record->Save();
	} else {
		$record = $recordset[0];
	}

	return $record;
}

/* 
 * Process records sent from the client
 */
function DoSync($rec_type, $company_id, $person_id, $target, $device_id, $sync_data) {
	
	$luid_list = array_keys($sync_data);
	$results = array();
	
	if ($rec_type == 'Contact') {
		$sql = "SELECT Contact.*, Luid FROM Contact 
			INNER JOIN SyncmlMapContact ON Contact.ContactID = SyncmlMapContact.ContactID
			WHERE Contact.CompanyID = ? AND SyncmlDeviceID = ? AND Luid IN (" . ArrayQm($luid_list) . ")";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($company_id, $device_id)), array(DTYPE_STRING, $luid_list));
		$recordset = DbConnManager::GetDb('mpower')->Execute($sql, 'Contact', 'Luid');

		// Extending it to allow it to also sync to accounts
		$account_sql = "SELECT Account.*, Luid FROM Account 
			INNER JOIN SyncmlMapContact ON Account.AccountID = SyncmlMapContact.AccountID
			WHERE Account.CompanyID = ? AND SyncmlDeviceID = ? AND Luid IN (" . ArrayQm($luid_list) . ")";
		$account_sql = SqlBuilder()->LoadSql($account_sql)->BuildSql(array(DTYPE_INT, array($company_id, $device_id)), array(DTYPE_STRING, $luid_list));
		$account_recordset = DbConnManager::GetDb('mpower')->Execute($account_sql, 'Account', 'Luid');
	
		$account_recordset->Initialize();
		
		$company_name_list = array();
		
	} elseif ($rec_type == 'Event' || $rec_type == 'Task') {
		$sql = "SELECT * FROM Event
			INNER JOIN SyncmlMapEvent ON Event.EventID = SyncmlMapEvent.EventID
			WHERE Event.CompanyID = ? AND SyncmlDeviceID = ? AND Luid IN (" . ArrayQm($luid_list) . ")";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($company_id, $device_id)), array(DTYPE_STRING, $luid_list));
		
		$recordset = DbConnManager::GetDb('mpower')->Execute($sql, 'RecEvent', 'Luid');
	} else {
		return false;
	}
	
	$recordset->Initialize();

	foreach($sync_data as $luid => $item_data) {
		if ($recordset->InRecordset($luid)) {
			$record = $recordset[$luid]; 
		} elseif (isset($account_recordset) && $account_recordset->InRecordset($luid)) {
			$record = $account_recordset[$luid];
		} else {
			$record = false;
		}
		switch ($item_data[0]) {
			case 'Replace' :
				// If this incomming item matches an Account, but has contact info, then we need to treat it as a new record
				if ($record instanceof Account) {
					$contact_rec_valid = ValidateContact($company_id, $item_data[1]);
					if ($contact_rec_valid == 'contact') {
						$record = false;
					}
				}
				if ($record != false) {
					break;
				}
			case 'Add' :
				// Create a new Record
				if (!$record) {
					// Determine if the record should create a contact or account
					if ($rec_type == 'Contact') {
						$contact_rec_valid = ValidateContact($company_id, $item_data[1]);
						if ($contact_rec_valid == 'contact') {
							// Create a new contact record
							$record = new Contact();
							$recordset[] = $record;
						} elseif ($contact_rec_valid == 'account') {
							// Create a new contact record
							$record = new Account();
							$account_recordset[] = $record;
						} else {
							// This does not validate as a contact or account, so skip it
							continue 2;
						}
					} else {
						$record = new RecEvent();
						$recordset[] = $record;
					}
					
					$record->SetDatabase(DbConnManager::GetDb('mpower'));
					$record->CompanyID = $company_id;
					$record->NewLuid = $luid;
				} elseif ($rec_type == 'Contact' && ($record->Inactive == 1 || $record->Deleted == 1)) {
					// The record they are trying to add is shown as having been deleted - reactivate the deleted rec and treat
					// it like a replace
					$action = 'Replace';
					$record->Inactive = 0;
					$record->Deleted = 0;
					SyncmlMapUndelete($rec_type, $device_id, $luid);
				} elseif (($rec_type == 'Event' || $rec_type == 'Task') && $recordset->Cancelled == 1) {
					$record->Cancelled = 0;
					SyncmlMapUndelete($rec_type, $device_id, $luid);
				} else {
					// The luid requested to add is already in the map and has not been deleted
					//return false;
					continue 2;
				}
				break;
			case 'Delete' :
				if (!$record) {
					// We don't have the record they are trying to delete on the server, which is okay by the protocol
					// Don't do anything
					continue 2;
				}
				if ($rec_type == 'Contact') {
					$record->Inactive = 1;
				} elseif ($rec_type == 'Event' || $rec_type == 'Task') {
					$record->Cancelled = 1;
				}				
				SyncmlMapDelete($rec_type, $device_id, $luid);
				break;
		}
		
		$record->Initialize();
		
		// Update the record
		if ($item_data[0] == 'Add' || $item_data[0] == 'Replace') {
			if ($rec_type == 'Contact') {
				MergeVcardIntoContact($company_id, $record, $item_data[1]);
			} elseif ($rec_type == 'Event') {
				MergeScalIntoEvent($record, $item_data[1]);
			} elseif ($rec_type == 'Task') {
				MergeStaskIntoEvent($record, $item_data[1]);
			}
		}
	}
	
	if ($rec_type == 'Contact') {
		$contact_object = new ModuleCompanyContactNew($company_id, $person_id);
		
		// Process to get company info for contacts:
		// Loop through contact records and get the account names
		// SQL request Jetstream account matches
		// Loop through contact records and create records for missing account names, attach matches
		
		// Do a search for all the company names, and update the individual contact records.  If the company doesn't exist, create it.
		// Get all the company names from the updated records
		$company_names = array();
		foreach ($recordset as $record) {
			if ($record->InRecord('CompanyName', false)) {
				$company_names[] = $record->CompanyName;
			}
		}
		if (count($company_names) > 0) {
			// Search the account tables for the names
			$sql = "SELECT AccountID, CompanyID, " . MapLookup::GetAccountNameField($company_id, true) . " FROM Account WHERE CompanyID = ? 
				AND " . MapLookup::GetAccountNameField($company_id, true) . " IN (" . ArrayQm($company_names) . ") 
				AND (Inactive = 0 OR Inactive IS NULL) AND (Deleted = 0 OR Deleted IS NULL)";
			
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_id), array(DTYPE_STRING, $company_names));
			$accounts = DbConnManager::GetDb('mpower')->Execute($sql, 'RecAccount', MapLookup::GetAccountNameField($company_id, true));
			
			foreach ($recordset as $record) {
				if ($record->InRecord('CompanyName', false)) {
					if ($accounts->InRecordset($record->CompanyName)) {
						$record->AccountID = $accounts[$record->CompanyName]['AccountID'];
					} else {
						$new_account = new RecAccount();
						$accounts[$record->CompanyName] = $new_account;
						$new_account->SetDatabase(DbConnManager::GetDb('mpower'));
						$new_account->Initialize();
						$new_account->CompanyID = $company_id;
						$new_account->{MapLookup::GetAccountNameField($company_id, true)} = $record->CompanyName;
						$new_account->Save();
						// $new_account->Debug();
						$record->AccountID = $new_account['AccountID'];
						$contact_object->Assign('account', $new_account['AccountID'], $person_id);
					}
				}
			}
			// $accounts->Debug();
		}
	}
	
	$recordset->Save();

	
	//	file_put_contents("c:/webtemp/syncmlllllll.txt", "\n Here we go accountid {$record->AccountID} contactid {$record->ContactID}\n ", FILE_APPEND);
	foreach ($recordset as $record) {
		if ($record->InRecord('NewLuid')) {
			//if (empty($record->EventID)) continue;
			//if (!empty($record->EventID)){
			
			if ($record->EventID){
				
				$msg = 'gets here (9.2) : rec_type : ' . $rec_type;
				file_put_contents("/webtemp/logs/log.txt", "$msg\n", FILE_APPEND);
				
				if ($rec_type == 'Event' || $rec_type == 'Task') {
					$sql = "INSERT INTO EventAttendee (EventID, PersonID, Creator, Deleted) values ($record->EventID, $person_id, 1, 0)";
					DbConnManager::GetDb('mpower')->Exec($sql);
					SyncmlMapInsert('Event', $device_id, $record->NewLuid, $record->EventID);
				} else {
					SyncmlMapInsert($rec_type, $device_id, $record->NewLuid, $record->ContactID);
					$contact_object->Assign('contact', $record->ContactID, $person_id);
				}
			}
		} else {
			if ($rec_type == 'Event' || $rec_type == 'Task') {
				SyncmlMapUpdateSyncTime('Event', $device_id, $record->Luid);
			} else {
				SyncmlMapUpdateSyncTime($rec_type, $device_id, $record->Luid);
			}
		}
	}

	if ($rec_type == 'Contact') {
		$account_recordset->Save();

		foreach ($account_recordset as $record) {
			if ($record->InRecord('NewLuid')) {
				SyncmlMapInsert('Account', $device_id, $record->NewLuid, $record->AccountID);
				$contact_object->Assign('account', $record->AccountID, $person_id);
			} else {
				SyncmlMapUpdateSyncTime($rec_type, $device_id, $record->Luid);
			}
		}
	}
	
	
	
	$recordset->Destroy();
}

function GetVcardKeywordMapping($company_id, $fields = false) {
	static $vcard_keywords = array();
	static $vcard_fields = array();
		
	if (count($vcard_keywords) == 0) {

		$vcard_sql = "SELECT KeywordId, Vcard FROM VcardKeywordMapping ";
		
		$vcard_results = DbConnManager::GetDb('mpower')->Exec($vcard_sql);
		foreach ($vcard_results as $key) {
			if (!empty($key['KeywordId'])) {
				$vcard_keywords[$key['KeywordId']] = $key['Vcard'];
				//$vcard_values[$key['Vcard']] = $key['Vcard'];
			}
		}

		$contact_map = MapLookup::GetContactMap($company_id);
		foreach ($contact_map['fields'] as $field) {
			if (!empty($field['KeywordId'])) {
				if( array_key_exists( $field['KeywordId'], $vcard_keywords ) ) {
					$vcard_fields[$field['FieldName']] = $vcard_keywords[$field['KeywordId']];
				}
			}
		}
	}
		
	if ($fields) {
		return $vcard_fields;
	} else {
		return $vcard_keywords;
	}
}

function ValidateContact($company_id, $vcard_text) {
	$vcard = new VCard();
	$vcard->parse(explode(chr(10), $vcard_text));
	
	$fn_fielddef = MapLookup::GetFieldDef($company_id, 'contact', MapLookup::GetContactFirstNameField($company_id));
	$ln_fielddef = MapLookup::GetFieldDef($company_id, 'contact', MapLookup::GetContactLastNameField($company_id));
	
	$fn_val = GetVcardBreakout($fn_fielddef['Vcard'], $vcard);
	$ln_val = GetVcardBreakout($ln_fielddef['Vcard'], $vcard);
	
	if (!empty($fn_val) ||  !empty($ln_val)) {
		return 'contact';
	}

	$an_fielddef = MapLookup::GetFieldDef($company_id, 'account', MapLookup::GetAccountNameField($company_id));
	
	$an_val = GetVcardBreakout($an_fielddef['Vcard'], $vcard);
	
	if (!empty($an_val)) {
		return 'account';
	}

	return false;
}

function GetVcardBreakout($vcard_mapping, $vcard) {
	$type = $pos = $sub_pos = null;
		
	sscanf($vcard_mapping, '%[^:]:%[^.].%d', $type, $pos, $sub_pos);

	// Check to see if the parameter is in the current vcard
	$vcard_entry = $vcard->GetProperty($type);

	if (!empty($vcard_entry)) {
		$vcard_vals = $vcard_entry->getComponents();
		$my_val = $vcard_vals[((!empty($pos)) ? $pos - 1 : 0)];
		if (!empty($sub_pos)) {
			$tmp = explode(chr(13) . chr(10) , $my_val);
			$my_val = (!empty($tmp[$sub_pos - 1])) ? $my_val = $tmp[$sub_pos - 1] : '';
		}

		return $my_val;
	}
	return false;
}

function MergeVcardIntoContact($companyid, &$contact, $vcard_text) {
	if ($contact instanceof Contact) {
		$contact_map = MapLookup::GetContactMap($companyid);
		
		// Create a mapping to capture the company name value
		$contact_map['fields'][] = array('FieldName' => 'CompanyName', 'Vcard' => 'ORG:1');
	} elseif ($contact instanceof Account) {
		$contact_map = MapLookup::GetAccountMap($companyid);
	}
	
	$vcard = new VCard();
	$vcard->parse(explode(chr(10), $vcard_text));

	foreach ($contact_map['fields'] as $field) {
		if (!empty($field['Vcard'])) {
			$temp_val = GetVcardBreakout($field['Vcard'], $vcard);
			if ($temp_val != false) {
				// Set the contact value to the vcard value
				if (substr($field['FieldName'], 0, 4) == 'Sele') {
					$lookup_val = $temp_val;
					$temp_val = '';
					foreach(OptionLookup::GetOptionset($companyid, $field['OptionSetID']) as $option) {
						if ($option['OptionName'] == $lookup_val) {
							$temp_val = $option['OptionID'];
							break;
						}
					}
				} elseif (isset($field['ValidationType']) && $field['ValidationType'] == 1) {
					// Validation Type 1 is for phone numbers
					$temp_val = FormatPhone($temp_val);
				} 
				
				$contact->{$field['FieldName']} = $temp_val;
			}
		}
	}
	
	$temp_private = GetVcardBreakout ('CLASS', $vcard); 
	if ($temp_private == 'PRIVATE') $contact->{'PrivateContact'} = 1 ;
	
}




define ('OL_RECURS_DAILY', 0);
define ('OL_RECURS_WEEKLY', 1);
define ('OL_RECURS_MONTHLY', 2);
define ('OL_RECURS_MONTHLY_NTH', 3);
define ('OL_RECURS_YEARLY', 5);
define ('OL_RECURS_YEARLY_NTH', 6);

function MergeScalIntoEvent(&$event, $scal_text) {
	$repeat_conversion = array(REPEAT_DAILY => OL_RECURS_DAILY, 
		REPEAT_WEEKLY => OL_RECURS_WEEKLY, 
		REPEAT_MONTHLY => OL_RECURS_MONTHLY, 
		REPEAT_MONTHLY_POS => OL_RECURS_MONTHLY_NTH, 
		REPEAT_YEARLY => OL_RECURS_YEARLY, 
		REPEAT_YEARLY_POS => OL_RECURS_YEARLY_NTH);	
	
	// scal = Sync4j Calendar Rec
	// Sync4j is stored in basic XML
	$scal = simplexml_load_string($scal_text);
	
	$event->Subject = (string)$scal->Subject;
	$event->Location = (string)$scal->Location;

	$start_date = FromScalIso8601($scal->Start);
	$event->StartDateUTC = $start_date->asMsSql();
	$end_date = FromScalIso8601($scal->End);
	$event->EndDateUTC = $end_date->asMsSql();

	if ($scal->IsRecurring == 1) {
		$temp = array_flip($repeat_conversion);
		
		$event->RepeatType = $temp[(string) $scal->RecurrenceType];
		
		$repeat_info = array();
		switch($scal->RecurrenceType) {
			case OL_RECURS_DAILY:
				$repeat_info[] = (string)$scal->Interval;
				break;

			case OL_RECURS_WEEKLY:
				$repeat_info[] = (string)$scal->Interval;
				$ol_days = array(1, 2, 4, 8, 16, 32, 64);
				$repeat_days = array();
				foreach ($ol_days as $day => $day_val) {
					if ($day_val & (int) $scal->DayOfWeekMask) {
						$repeat_days[] = $day;
					}
				}
				$repeat_info[] = $repeat_days;
				break;

			case OL_RECURS_MONTHLY:
				$repeat_info[] = (string) $scal->Interval;
				$repeat_info[] = (string) $scal->DayOfMonth;
				break;

			case OL_RECURS_MONTHLY_NTH:
				$repeat_info[] = (string) $scal->Interval;
				$repeat_info[] = (string) $scal->Instance;
				$repeat_info[] = strpos(strrev(decbin($scal->DayOfWeekMask)), '1');
				break;

			case OL_RECURS_YEARLY:
				$repeat_info[] = (string) $scal->Interval;
				$repeat_info[] = (string) $scal->MonthOfYear;
				$repeat_info[] = (string) $scal->DayOfMonth;
				break;

			case OL_RECURS_YEARLY_NTH:
				$repeat_info[] = (string) $scal->Interval;
				$repeat_info[] = (string) $scal->Instance;
				$repeat_info[] = strpos(strrev(decbin($scal->DayOfWeekMask)), '1');
				$repeat_info[] = (string) $scal->MonthOfYear;
				break;
		}
		$event->RepeatInterval = serialize($repeat_info);
		$utc_timezone = new DateTimeZone('UTC');
		$repeat_end_date = new JetDateTime($scal->PatternEndDate, $utc_timezone);
		$event->RepeatEndDateUTC = $repeat_end_date->asMsSql();
	} else {
		$event->RepeatType = 0;
	}
	
	$current_event_type = $event->EventTypeID;
	if (empty($current_event_type)) {
		$event->EventTypeID = 1335;
	}
}

function MergeStaskIntoEvent(&$event, $stask_text) {
	$utc_timezone = new DateTimeZone('UTC');
	
	// scal = Sync4j Calendar Rec
	// Sync4j is stored in basic XML
	
	$stask = simplexml_load_string($stask_text);
	
	$msg = 'gets here (4.1)  xml: ' . $stask_text;
	file_put_contents("/webtemp/logs/log.txt", "$msg\n", FILE_APPEND);

	$event->Subject = (string) $stask->Subject;
	
	
	$start_date_str = (string) $stask->StartDate;
	if (empty($start_date_str)) {
		$start_date_str = (string) $stask->DueDate;
	}
	if (empty($start_date_str)) {
		$start_date_str = date('Y-m-d');
	}
	$start_date = FromScalIso8601($start_date_str);
	$event->StartDateUTC = $start_date->asMsSql();
	$event->RepeatType = 0;

	$current_event_type = $event->EventTypeID;
	if (empty($current_event_type)) {
		$event->EventTypeID = 1336;
	}
}

function GetServerChanges($sync_request, $company_id, $person_id, $device_id, $last_sync_timestamp) {
	$last_sync_timestamp = '';
	
	// echo "$sync_request, $company_id, $person_id, $device_id, $last_sync_timestamp<br>";
	$change_list = array ('Add' => Array (), 'Replace' => Array (), 'Delete' => Array ());
	// return $change_list;
	
	switch ($sync_request) {
		case 'Contact':
		case 'card' :
			// $vcard_values = GetVcardKeywordMapping($company_id);
			// $vcard_fields = GetVcardKeywordMapping($company_id, true);
			$vcard_fields = MapLookup::GetVcardFields($company_id, 'contact');
			
			$sql = "SELECT Contact.ContactID, SyncmlMapContact.Luid, CONVERT(VARCHAR(19), ModifyTime, 120) as ModifyTime, 
				(SELECT " . MapLookup::GetAccountNameField($company_id, true) . " FROM Account WHERE AccountID = Contact.AccountID) as AccountName, " .
				implode(", " , array_keys($vcard_fields)) . " FROM Contact 
				LEFT JOIN SyncmlMapContact ON Contact.ContactID = SyncmlMapContact.ContactID AND SyncmlDeviceID = ?
				WHERE CompanyID = ? AND (Inactive = 0 OR Inactive IS NULL) AND (Contact.Deleted = 0 OR Contact.Deleted IS NULL)
				AND Contact.ContactID IN (SELECT ContactID FROM PeopleContact WHERE PersonID = ?) AND
				(SyncTime IS NULL OR ModifyTime IS NULL OR SyncTime <> ModifyTime)";
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($device_id, $company_id, $person_id)));
			$results = DbConnManager::GetDb('mpower')->Execute($sql);

			$account_ids = array();
			foreach ($results as $result) {
				if (!in_array($result->AccountID, $account_ids)) {
					$account_ids[] = $result->AccountID;
				}
				
				if (!$result->InRecord('Luid', false)) {
					$change_list['Add'][$result->ContactID] = array('Data' => ContactToVcard($company_id, $result, $vcard_fields));
				} else {
					$change_list['Replace'][$result->ContactID] = array('Luid' => $result->Luid, 'Data' => ContactToVcard($company_id, $result, $vcard_fields));
					SyncmlMapUpdateSyncTime('Contact', $device_id, $result->Luid);
				}
			}
			
			$account_vcard_fields = MapLookup::GetVcardFields($company_id, 'account');
			
			$account_sql = "SELECT Account.AccountID, SyncmlMapContact.Luid, CONVERT(VARCHAR(19), ModifyTime, 120) as ModifyTime, " .
				implode(", " , array_keys($account_vcard_fields)) . " FROM Account
				LEFT JOIN SyncmlMapContact ON Account.AccountID = SyncmlMapContact.AccountID AND SyncmlDeviceID = ?
				WHERE CompanyID = ? AND (Inactive = 0 OR Inactive IS NULL) AND 
				(Account.Deleted = 0 OR Account.Deleted IS NULL) AND 
				Account.AccountID IN (SELECT AccountID FROM PeopleAccount WHERE PersonID = ?) AND
				(SELECT COUNT(*) FROM Contact WHERE AccountID = Account.AccountID) = 0 AND
				(SyncTime IS NULL OR ModifyTime IS NULL OR SyncTime <> ModifyTime)";
			$account_sql = SqlBuilder()->LoadSql($account_sql)->BuildSql(array(DTYPE_INT, array($device_id, $company_id, $person_id)));
			
			$account_results = DbConnManager::GetDb('mpower')->Execute($account_sql);
				
			foreach ($account_results as $result) {
				if (!$result->InRecord('Luid', false)) {
					$change_list['Add']['a' . $result->AccountID] = array('Data' => ContactToVcard($company_id, $result, $account_vcard_fields, true));
				} else {
					$change_list['Replace']['a' . $result->ContactID] = array('Luid' => $result->Luid, 'Data' => ContactToVcard($company_id, $result, $account_vcard_fields, true));
					SyncmlMapUpdateSyncTime('Contact', $device_id, $result->Luid);
				}
			}
			
			$sql = "SELECT Luid 
				FROM (SELECT ContactID FROM Contact WHERE CompanyID = ? AND (Inactive = 0 OR Inactive IS NULL) AND (Deleted = 0 OR Deleted IS NULL) 
				AND ContactID in (select contactid from peoplecontact where personid = ?)) AS Contact
				FULL OUTER JOIN SyncmlMapContact ON Contact.ContactID = SyncmlMapContact.ContactID WHERE SyncmlDeviceID = ?
				AND Contact.ContactID IS NULL
				INTERSECT
				SELECT Luid 
				FROM (SELECT AccountID FROM Account WHERE CompanyID = ? AND (Inactive = 0 OR Inactive IS NULL) AND (Deleted = 0 OR Deleted IS NULL) 
				AND AccountID in (SELECT AccountID FROM PeopleAccount where PersonID = ?)) AS Account
				FULL OUTER JOIN SyncmlMapContact ON Account.AccountID = SyncmlMapContact.AccountID WHERE SyncmlDeviceID = ?
				AND Account.AccountID IS NULL";
			
			
			
//			$sql = "SELECT Contact.ContactID, Luid 
//				FROM (SELECT ContactID FROM Contact WHERE CompanyID = ? AND (Inactive = 0 OR Inactive IS NULL) AND (Deleted = 0 OR Deleted IS NULL) 
//				AND ContactID in (select contactid from peoplecontact where personid = ?)) AS Contact
//				FULL OUTER JOIN SyncmlMapContact ON Contact.ContactID = SyncmlMapContact.ContactID WHERE SyncmlDeviceID = ? 
//				AND Contact.ContactID IS NULL";
			
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($company_id, $person_id, $device_id, $company_id, $person_id, $device_id)));
			
			$results = DbConnManager::GetDb('mpower')->Execute($sql);
			
			foreach ($results as $result) {
				$change_list['Delete'][] = array('Luid' => $result->Luid);
			}
			
			return $change_list;
		case 'stask' :
		case 'scal' :
			// Get all the data for the events that have been added and replaced
			$sql = "SELECT Event.EventID, Subject, Location, StartDateUTC, EndDateUTC, 
				SyncmlMapEvent.Luid, RepeatType, RepeatInterval, RepeatEndDateUTC, Event.Closed, EventType.EventName,
				SyncmlMapEvent.Luid, CONVERT(VARCHAR(19), ModifyTime, 120) as ModifyTime,
				(SELECT " . MapLookup::GetContactFirstNameField($company_id, true) . " + ' ' + " . MapLookup::GetContactLastNameField($company_id, true) . " FROM Contact WHERE ContactID = Event.ContactID) as ContactName,
				(SELECT Account." . MapLookup::GetAccountNameField($company_id, true) . " FROM Account LEFT JOIN Contact ON Contact.AccountID = Account.AccountID WHERE ContactID = Event.ContactID) as AccountName
				FROM Event LEFT JOIN SyncmlMapEvent ON Event.EventID = SyncmlMapEvent.EventID AND SyncmlDeviceID = ?
				LEFT JOIN EventAttendee ON Event.EventID = EventAttendee.EventID
				LEFT JOIN EventType ON Event.EventTypeID = EventType.EventTypeID
				WHERE Event.CompanyID = ? AND EventAttendee.PersonID = ? AND (Cancelled = 0 OR Cancelled IS NULL) AND
				(SyncTime IS NULL OR ModifyTime IS NULL OR SyncTime <> ModifyTime)";
			
			if ($sync_request == 'scal') {
				$sql .= " AND EventType.Type = 'EVENT' AND StartDateUTC >= DATEADD(d, -30, GETDATE())";
			} elseif ($sync_request == 'stask') {
				$sql .= " AND EventType.Type = 'TASK' AND ((Closed = 0 OR Closed IS NULL) OR StartDateUTC >= DATEADD(d, -30, GETDATE()))";
			}

			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($device_id, $company_id, $person_id)));
			$results = DbConnManager::GetDb('mpower')->Execute($sql);
		
			foreach ($results as $result) {
				if ($sync_request == 'scal') {
					$data = EventToScal($result);
				} elseif ($sync_request == 'stask') {
					$data = EventToStask($result);
				}
				
				if (!$result->InRecord('Luid', false)) {
					$change_list['Add'][$result->EventID] = array('Data' => $data);
				} else {
					$change_list['Replace'][$result->EventID] = array('Luid' => $result->Luid, 'Data' => $data);
				}
			}

			$sql = "SELECT Event.EventID, Luid
				FROM (
				SELECT Event.EventID FROM Event JOIN EventAttendee ON Event.EventID = EventAttendee.EventID 
				WHERE CompanyID = ? AND PersonID = ? AND (Cancelled = 0 OR Cancelled IS NULL)
				) Event
				FULL OUTER JOIN SyncmlMapEvent ON Event.EventID = SyncmlMapEvent.EventID
				WHERE SyncmlDeviceID = ? AND Event.EventID IS NULL";
			
			
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($company_id, $person_id, $device_id)));
			
			$results = DbConnManager::GetDb('mpower')->Execute($sql);
			
			foreach ($results as $result) {
				$change_list['Delete'][$result->EventID] = array('Luid' => $result->Luid);
			}
			
			return $change_list;
	}
}

function ContactToVcard($company_id, $contact, $vcard_fields, $is_account = false) {
	$vcard_elements = array('N' => 5, 'ADR' => 7, 'EMAIL' => 2, 'TEL' => 1, 'TITLE' => 1, 'ORG' => 3, 'URL' => 1);
	
	if (!$is_account) {
		$vcard_entries = array(
			'ORG' => array(1 => array($contact->AccountName), ''), 
			'X-FUNAMBOL-ORGANIZATIONALID' => array(1 => array(base64_encode('JET' . $contact->ContactID)))
		);
	}
	
	foreach ($vcard_fields as $field_name => $vcard_mapping) {
		$db_value = ($contact->InRecord($field_name, false)) ? $contact->$field_name : '';
			
		$type = $pos = $sub_pos = null;
		sscanf($vcard_mapping, '%[^:]:%[^.].%d', $type, $pos, $sub_pos);
		
		$vcard_breakout = explode(';', $type);
		if (!isset($vcard_entries[$type]) && isset($vcard_elements[$vcard_breakout[0]]) && $vcard_elements[$vcard_breakout[0]] > 1) {
			$vcard_entries[$type] = array_fill(1, $vcard_elements[$vcard_breakout[0]], '');
		}
		if (substr($field_name, 0, 4) == 'Sele') {
			if ($fielddef = MapLookup::GetFieldDefByFieldName($company_id, $field_name, ($is_account) ? 'account' : 'contact')) {
				$vcard_entries[$type][$pos][$sub_pos] = OptionLookup::GetOptionName($company_id, $fielddef['OptionSetID'], $db_value);
			}
		} else {
			$vcard_entries[$type][$pos][$sub_pos] = $contact->$field_name;
		}
	}
	
	$vcard = "BEGIN:VCARD\nVERSION:2.1\n";
	$vcard .= "TEL;PREF;VOICE:\n";
	foreach ($vcard_entries as $entry_id => &$entry) {
		if (is_array($entry)) {
			ksort($entry);
			foreach ($entry as &$sub_entry) {
				if (is_array($sub_entry)) {
					ksort($sub_entry);
					$sub_entry = implode('=0D=0A', $sub_entry);
				}
			}
			$entry = implode(';', $entry);
		}
		$vcard_breakout = explode(';', $entry_id);
		if($vcard_breakout[0] == 'ADR') {
			$entry_id .= ';ENCODING=QUOTED-PRINTABLE;CHARSET=UTF-8';
		}
		$vcard .= "$entry_id:$entry\n"; 
	}
	$vcard .= "END:VCARD\n";
	
	return $vcard;
}

function DayArrayToDayMap($day_array) {
	if (!is_array($day_array)) {
		$day_array = array($day_array);
	}
	$ol_days = array(1, 2, 4, 8, 16, 32, 64);
	$result = 0;
	foreach($day_array as $day) {
		$result += $ol_days[$day];
	}	
	return $result;
}

function EventToScal($event) {
	$utc_timezone = new DateTimeZone('UTC');
	$scal = simplexml_load_string('<?xml version="1.0" encoding="UTF-8"?><appointment><SIFVersion>1.1</SIFVersion></appointment>');
	
	$body_text = "Jetstream Appointment Info\n---------------------\n";

	if ($event->InRecord('ContactName', false)) {
		$body_text .= "With $event->ContactName";
	}
	if ($event->InRecord('AccountName', false)) {
		$body_text .= " of $event->AccountName\n";
	}
	if ($event->InRecord('EventName', false)) {
		$body_text .= "Event Type: $event->EventName\n";
	}
	$scal->Appointment->Body = $body_text;

	$scal->Appointment->Subject = $event->Subject;
	$scal->Appointment->Location = $event->Location;
	
	$start_date = new JetDateTime($event->StartDateUTC, $utc_timezone);
	$scal->Appointment->Start = $start_date->format('Ymd\THis\Z');
	$end_date = new JetDateTime($event->EndDateUTC, $utc_timezone);
	$scal->Appointment->End = $end_date->format('Ymd\THis\Z');

	// If the event is in the past, then tell it not to show a reminder
	if ($start_date < new JetDateTime('now', $utc_timezone)) {
		$scal->ReminderSet = 0;
	}
	
	
	// Convert the repeating into scal equivalent
	if ($event->RepeatType > 0) {
		$repeat_conversion = array(REPEAT_DAILY => OL_RECURS_DAILY, 
			REPEAT_WEEKLY => OL_RECURS_WEEKLY, 
			REPEAT_MONTHLY => OL_RECURS_MONTHLY, 
			REPEAT_MONTHLY_POS => OL_RECURS_MONTHLY_NTH, 
			REPEAT_YEARLY => OL_RECURS_YEARLY, 
			REPEAT_YEARLY_POS => OL_RECURS_YEARLY_NTH);
		
		$scal->IsRecurring = 1;
		$scal->RecurrenceType = $repeat_conversion[$event->RepeatType];
		
		if ($event->InRecord('RepeatEndDateUTC', false)) {
			$repeat_end_date = new JetDateTime($event->RepeatEndDateUTC);
			$scal->PatternEndDate = $repeat_end_date->format('Ymd\THis');
		} else {
			$scal->NoEndDate = 1;
		}
		
		$repeat_detail = unserialize($event->RepeatInterval);
		
		switch ($event->RepeatType) {
			case REPEAT_DAILY :
				$scal->Interval = $repeat_detail[0];
				break;
				
			case REPEAT_WEEKLY :
				list ($freq, $days) = $repeat_detail;
				$scal->Interval = $freq;
				$scal->DayOfWeekMask = DayArrayToDayMap($days);
				break;
				
			case REPEAT_MONTHLY :
				list ($freq, $month_day) = $repeat_detail;
				$scal->Interval = $freq;
				$scal->DayOfMonth = $month_day;
				break;
				
			case REPEAT_MONTHLY_POS :
				list ($freq, $pos, $pos_type) = $repeat_detail;
				$scal->Interval = $freq;
				$scal->Instance = $pos;
				$scal->DayOfWeekMask = DayArrayToDayMap($pos_type);
				break;
				
			case REPEAT_YEARLY :
				list ($freq, $month, $month_day) = $repeat_detail;
				$scal->Interval = $freq;
				$scal->MonthOfYear = $month;
				$scal->DayOfMonth = $month_day;
				break;
				
			case REPEAT_YEARLY_POS :
				list ($freq, $pos, $pos_type, $pos_month) = $repeat_detail;
				$scal->Interval = $freq;
				$scal->Instance = $pos;
				$scal->DayOfWeekMask = DayArrayToDayMap($pos_type);
				$scal->MonthOfYear = $pos_month;
				break;
		}
	}
	
	return base64_encode($scal->asXML());
}

function EventToStask($event) {
	$utc_timezone = new DateTimeZone('UTC');
	
	$stask = simplexml_load_string('<?xml version="1.0" encoding="UTF-8"?><task><SIFVersion>1.1</SIFVersion></task>');
	$stask->Body = 'Task imported from a Jetstream event';
	$stask->Subject = $event->Subject;
	$start_date = new JetDateTime($event->StartDateUTC, $utc_timezone);
	$stask->StartDate = $start_date->format('Y-m-d');
	$stask->Complete = $event->Closed;
	return base64_encode($stask->asXML());
}

function SyncmlMapInsert($map_type, $device_id, $luid, $guid) {
	if ($map_type == 'Task') {
		$map_type = 'Event';
	}

	$syncml_map_table = $map_type;
	
	if ($map_type == 'Account') {
		$syncml_map_table = 'Contact';
	}
	
	$sql = "DELETE FROM SyncmlMap{$syncml_map_table} WHERE SyncmlDeviceID = ? AND Luid = ?;
	INSERT INTO SyncmlMap{$syncml_map_table} (SyncmlDeviceID, Luid, {$map_type}ID, SyncTime) 
	SELECT ?, ?, ?, ModifyTime FROM {$map_type} WHERE {$map_type}ID = ?";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $device_id), array(DTYPE_STRING, $luid), array(DTYPE_INT, $device_id), array(DTYPE_STRING, $luid), array(DTYPE_INT, $guid), array(DTYPE_INT, $guid));

	DbConnManager::GetDb('mpower')->Exec($sql);
}

function SyncmlMapUpdateSyncTime($map_type, $device_id, $luid) {
	if ($map_type == 'Task') {
		$map_type = 'Event';
	}
	
	$sql = "Update sm SET SyncTime = rec_table.ModifyTime FROM SyncmlMap{$map_type} sm LEFT JOIN {$map_type} rec_table ON sm.{$map_type}ID = rec_table.{$map_type}ID WHERE SyncmlDeviceID = ? AND LUID = ?";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $device_id), array(DTYPE_STRING, $luid));
	
	DbConnManager::GetDb('mpower')->Exec($sql);
}

function SyncmlMapDelete($map_type, $device_id, $luid) {
	if ($map_type == 'Task') {
		$map_type = 'Event';
	}
	
	$sql ="UPDATE SyncmlMap{$map_type} SET Deleted = 1 WHERE SyncmlDeviceID = ? AND LUID = ?";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $device_id), array(DTYPE_STRING, $luid));
	DbConnManager::GetDb('mpower')->Exec($sql);
}

function SyncmlMapUndelete($map_type, $device_id, $luid) {
	if ($map_type == 'Task') {
		$map_type = 'Event';
	}
	
	$sql ="UPDATE SyncmlMap{$map_type} SET Deleted = NULL WHERE SyncmlDeviceID = ? AND LUID = ?";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $device_id), array(DTYPE_STRING, $luid));
	DbConnManager::GetDb('mpower')->Exec($sql);
}

function UpdateSyncmlDevice($device_id, $sync_anchor, $request_type) {
	$sql = "UPDATE SyncmlDevice SET SyncAnchor$request_type = ?, LastSync$request_type = ? , PersonID = ? WHERE SyncmlDeviceID = ?";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $sync_anchor), array(DTYPE_STRING, TimestampToMsSql(time(), 's')), array(DTYPE_INT, $_SESSION['Security']['PersonID']) , array(DTYPE_STRING, $device_id));
	DbConnManager::GetDb('mpower')->Exec($sql);
}

function FromScalIso8601($date_string) {
	$utc_timezone = new DateTimeZone('UTC');
	return new JetDateTime($date_string, $utc_timezone);
}

function ToScalIso8601(JetDateTime $date) {
	$utc_timezone = new DateTimeZone('UTC');
	$date->setTimezone($utc_timezone);
	$date->format("Ymd\THis\Z");
}

function GetCmdId($reset = false) {
	static $cmd_id = 0;
	if ($reset) $cmd_id = 0;
	$cmd_id++;
	return $cmd_id;
}

function DecodeItem($data, $format) {
	switch ($format) {
		case 'bin':
			return $data;
		case 'b64':
			return base64_decode($data);
		default :
			WriteDebug("Decode requested for unknown format ($format) [lib.Syncml.php:DecodeItem]");
	}
}

function WriteDebug($text) {
	if (DEBUG) {
		file_put_contents("c:\webtemp\logs\syncml.xml", "$text\n", FILE_APPEND);
//		file_put_contents("/webtemp/logs/syncml.xml", "$text\n", FILE_APPEND);
	}
}

function FormatPhone($value) {
	$digits = preg_replace('/\D/', '', $value);
	// $digits = $input->getDigits($field['FieldName']);
	switch (strlen($digits)) {
		case 7 : // 7 digits with the first digit not a 1 is a local exchange American phone number
			if ($digits[0] != '1') {
				$value = substr($digits, 0, 3) . '-' . substr($digits, 3);
			}
			break;
		case 11 : // If it is 11 digits and the first digit is one, remove the first digit and treat it like a 10 digit
			if ($digits[0] != 1) break;
			$digits = substr($digits, 1);
		case 10 : // 10 digits with the first digit not a 1 is a standard American number
			if ($digits[0] == 1) break;
			$value = '(' . substr($digits, 0, 3) . ') ' . substr($digits, 3, 3) . '-' . substr($digits, 6);
			break;
	}
	return $value;
}

function Assign($type, $id, $user) {
	if ( CheckAssignments( $type, $id, $user) > 0 ) return; 
	if ($type == 'contact') {
		$assign_sql = "insert into PeopleContact (ContactID, PersonID, CreatedBy, AssignedTo) 
			values (?, ?, 1, 1)";
	} else {
		$assign_sql = "insert into PeopleAccount (AccountID, PersonID, CreatedBy, AssignedTo)
			values (?, ?, 1, 1)";
	}
	$assign_sql = SqlBuilder()->LoadSql($assign_sql)->BuildSql(array(DTYPE_INT, array($id, $user)));
	// echo $assign_sql . "\n";

	DbConnManager::GetDb('mpower')->DoInsert($assign_sql);
}

function CheckAssignments ($type, $id, $user) {
	$table = ($type == 'contact') ? 'PeopleContact' : 'PeopleAccount'; 
	if ($type == 'contact') {
		$check_sql = "select count(*) AS CNT from $table where ContactID = ? and PersonID = ? " ; 
	} else {
		$check_sql = "select count(*) AS CNT from $table where AccountID = ? and PersonID = ? " ; 
	}
	
	$check_sql = SqlBuilder()->LoadSql($check_sql)->BuildSql(array(DTYPE_INT, array($id, $user)));
	$value = DbConnManager::GetDb('mpower')->GetOne($check_sql);
	if ($value->CNT > 0) return 1 ; 
	else return 0 ; 
}