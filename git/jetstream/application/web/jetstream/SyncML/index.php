<?php
define('DEBUG', false);

set_time_limit(240);
define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

require 'lib.syncml.php';


define('SYNCML_ALERT_TWOWAY', 200);
define('SYNCML_ALERT_SLOW_SYNC', 201);

define('SYNCML_OK', 200);
define('SYNCML_ACCEPTED_FOR_PROCESSING', 202);
define('SYNCML_ITEM_NOT_DELETED', 211);
define('SYNCML_AUTHENTICATION_ACCEPTED', 212);
define('SYNCML_UNAUTHORIZED', 401);
define('SYNCML_COMMAND_NOT_ALLOWED', 405);
define('SYNCML_OPTIONAL_FEATURE_NOT_SUPPORTED', 406);
define('SYNCML_AUTHENTICATION_REQUIRED', 407);

$ref_translations = array('card' => 'Contact', 'scal' => 'Event', 'stask' => 'Task');

header("Content-Type: application/vnd.syncml+xml");
session_start();
// MysqlConnect();

$xml_dir = "./";

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
	return;
}

$request = simplexml_load_file('php://input');
$response = simplexml_load_file($xml_dir . 'syncml_1.2_template.xml');

WriteDebug("Request Package:");
WriteDebug($request->asXML());

$msg_id = $request->SyncHdr->MsgID;

if (empty($_SESSION['TargetConfig'])) {
	$_SESSION['TargetConfig'] = LoadTargetConfig($request->SyncHdr->Source->LocURI);
	if ($msg_id == 1 && empty($_SESSION['TargetConfig']['Capabilities'])) {
		// We should perform a get at this point and update Capabilities
	}
}

// ***** Create Header *****
// Populate the response header
$response->SyncHdr->SessionID = $request->SyncHdr->SessionID;
$response->SyncHdr->MsgID = $msg_id;
// Target and source are inverse of what comes from the client
$response->SyncHdr->Target->LocURI = $request->SyncHdr->Source->LocURI;
$response->SyncHdr->Source->LocURI = $request->SyncHdr->Target->LocURI;
// Use RespID to connect the next response to the session
$response->SyncHdr->RespURI = $request->SyncHdr->Target->LocURI . "?" . session_name() . "=" . session_id();

// ***** Create Response to Client's Header *****
// Respond to the client's header with a status
$header_status = $response->SyncBody->addChild('Status');
$header_status->CmdID = GetCmdId();
$header_status->MsgRef = $msg_id;
$header_status->CmdRef = 0; // 0 is always the CmdRef for the header
$header_status->Cmd = 'SyncHdr';
$header_status->TargetRef = $request->SyncHdr->Target->LocURI;
$header_status->SourceRef = $request->SyncHdr->Source->LocURI;

if (empty($_SESSION['AuthResult']) || $_SESSION['AuthResult'] != SYNCML_AUTHENTICATION_ACCEPTED) {
	if (!empty($request->SyncHdr->Cred)) {
		if ($_SESSION['Security'] = AuthenticateCredentials($request->SyncHdr->Cred->Data)) {
			$header_status->Data = $_SESSION['AuthResult'] = SYNCML_AUTHENTICATION_ACCEPTED;
		} else {
			$header_status->Data = $_SESSION['AuthResult'] = SYNCML_UNAUTHORIZED;
		}
	} else {
		// They are not authorized and they have sent a message with no credentials
		// Initiate challenge
		$header_status->Chal->Meta->Type = 'syncml:auth-basic';
		$header_status->Chal->Meta->Type->AddAttribute('xmlns', 'syncml:metinf');
		$header_status->Chal->Meta->Format = 'b64';
		$header_status->Chal->Meta->Format->AddAttribute('xmlns', 'syncml:metinf');
		$header_status->Data = $_SESSION['AuthResult'] = SYNCML_AUTHENTICATION_REQUIRED;
	}
} else {
	// We have a previously good authentication, so send OK - good request to the header
	$header_status->Data = SYNCML_OK;
}

if ($_SESSION['AuthResult'] == SYNCML_AUTHENTICATION_ACCEPTED) {
	// Traverse the request body, process each request and create appropriate responses
	foreach ($request->SyncBody->children() as $body_child_type => $body_child) {
		switch ($body_child_type) {
			// SyncML defines the following "request" commands: 
			case 'Alert' :
				/*
				 * Allows the originator to notify the recipient. The notification
				 * can be used as an application-to-application message or a message
				 * intended for display through the recipient's user interface.
				 */
				if ($body_child->Data == SYNCML_ALERT_TWOWAY) {
					// Build the Status as we process the alert
					$status = $response->SyncBody->addChild('Status');
					$status->CmdID = GetCmdId();
					$status->MsgRef = $msg_id;
					$status->CmdRef = $body_child->CmdID;
					$status->Cmd = 'Alert';
					
					$target = $status->TargetRef = (string) $body_child->Item->Target->LocURI;
					
					$server_anchor = $_SESSION['sync_requests'][$target]['Last'] = $_SESSION['TargetConfig']['SyncAnchor' . $ref_translations[$target]];
					$device_anchor = (string) $body_child->Item->Meta->Anchor->Last;
					
					if ($server_anchor == $device_anchor) {
						$status->Data = $_SESSION['sync_requests'][$target]['Type'] = SYNCML_ALERT_TWOWAY;
					} else {
						$status->Data = $_SESSION['sync_requests'][$target]['Type'] = SYNCML_ALERT_SLOW_SYNC;
					}
						
					$status->SourceRef = $_SESSION['sync_requests'][$target]['Source'] = (string) $body_child->Item->Source->LocURI;

					$status->Item->Data->Anchor->Next = $_SESSION['sync_requests'][$target]['Next'] = (string) $body_child->Item->Meta->Anchor->Next;
					
					// $_SESSION['sync_requests'][$target]['Last'] = (string) $body_child->Item->Meta->Anchor->Last;
				}
				break;
			case 'Atomic' :
				/*
				 * Allows the originator to notify the recipient. The notification can
				 * be used as an application-to-application message or a message intended
				 * for display through the recipient's user interface.
				 */
				break;
			case 'Copy' :
				/*
				 * Allows the originator to ask that a data element or data elements
				 * accessible to the recipient be copied.
				 */
				break;
			case 'Exec' :
				/*
				 * Allows the originator to ask that a named or supplied executable is
				 * invoked by the recipient.  (Seems like a bad idea...)
				 */
				break;
			case 'Get' :
				/*
				 * Get. Allows the originator to ask for a data element or data elements 
				 * from the recipient. A get can include the resetting of any meta-information
				 * that the recipient maintains about the data element or collection.
				 */
				$status = $response->SyncBody->addChild('Results');
				$status->CmdID = GetCmdId();
				$status->MsgRef = $msg_id;
				$status->CmdRef = $body_child->CmdID;
				foreach ($body_child->Item as $request_item) {
					$response_item = $status->addChild('Item');
					$response_item->addChild('Source')->addChild('LocURI', $request_item->Target->LocURI);
					switch ($request_item->Target->LocURI) {
						case './devinf12' :
							$data = $response_item->addChild('Data');
							$dom_data_node = dom_import_simplexml($data);
							$dom_dev_inf = DomDocument::load($xml_dir . 'server_devinf.xml');
							$dom_data_node->appendChild($dom_data_node->ownerDocument->importNode($dom_dev_inf->documentElement, true));
							break;
						default :
							$response_item->addChild('Data', SYNCML_OPTIONAL_FEATURE_NOT_SUPPORTED);
					}
				}
				break;
			case 'Map' :
				/*
				 * Allows the originator to ask the recipient to update the identifier
				 * mapping between two data collections.  
				 */
				$map_type = $ref_translations[(string) $body_child->Target->LocURI];
				foreach ($body_child->MapItem as $map_item) {
					$guid = $map_item->Target->LocURI;
					$this_map_type = $map_type;
					if ($map_type == 'Contact' && substr($guid, 0, 1) == 'a') {
						$this_map_type = 'Account';
						$guid = (int) substr($guid, 1);
						
					} 
					SyncmlMapInsert($this_map_type, $_SESSION['TargetConfig']['SyncmlDeviceID'], $map_item->Source->LocURI, $guid);
				}
				break;
			case 'Move' :
				/* 
				 * Allows the originator to ask the recipient to update the identifier
				 * mapping between two data collections.  
				 */
				break;
			case 'Put' :
				/*
				 * Allows the original to put a data element or data elements on to the 
				 * recipient.
				 */
				$status = $response->SyncBody->addChild('Status');
				$status->addChild('CmdID', GetCmdId());
				$status->addChild('MsgRef', $request->SyncHdr->MsgID);
				$status->addChild('CmdRef', $body_child->CmdID);
				$status->addChild('Cmd', 'Put');
				$status->addChild('SourceRef', $body_child->Item->Source->LocURI);
				$status->addChild('Data', SYNCML_OK);
				break;
			case 'Search' :
				/*
				 * Allows the originator to ask that the supplied query be executed 
				 * against a data element or data elements accessible to the recipient.
				 */
				break;
			case 'Sequence' :
				/*
				 * Allows the originator to indicate that a set of commands is to be 
				 * performed in the specified sequence.
				 */
				break;
			case 'Sync' :
				$sync_happend = true;
				
				/*
				 * Allows the originator to specify that the included commands be treated
				 * as part of the synchronization of two data collections
				 */

				// A sync first sends back a status of Sync sent, then follows with a sync of server changes
				$target = (string) $body_child->Target->LocURI; // This is the type of record we are receiving
				
				$status = $response->SyncBody->addChild('Status');
				$status->CmdID = GetCmdId();
				$status->MsgRef = $msg_id;
				$status->Cmd = 'Sync';
				$status->CmdRef = $body_child->CmdID;
				$status->TargetRef = $target;
				$status->SourceRef = $body_child->Source->LocURI;
				
				
				// Bundle the sync request for processing
				$sync_data = array();
				foreach ($body_child as $action => $action_data) {
					foreach ($action_data->Item as $item) {
						$item_data = ($action == 'Delete') ? true : (!empty($item->Meta->Format)) ? DecodeItem($item->Data, $item->Meta->Format) : $item->Data;
						$sync_data[(string) $item->Source->LocURI] = array($action, (string) $item_data);
					}
				}
				if (count($sync_data) > 0) {
					DoSync($ref_translations[$target], $_SESSION['Security']['CompanyID'], $_SESSION['Security']['PersonID'], $target, $_SESSION['TargetConfig']['SyncmlDeviceID'], $sync_data);
				}
				
				$status->Data = SYNCML_OK;
				
				break;
			// SyncML defines the following "response" commands:
			case 'Status' :
				/*
				 * Indicates the completion status of an operation or that an error 
				 * occurred while processing a previous request.  
				 */
				foreach ($body_child->Item as $status_item) {
					if ($body_child->Cmd == 'Delete' && isset($_SESSION['PreviousCmds'][(string) $body_child->CmdRef])) {
						SyncmlMapDelete($_SESSION['PreviousCmds'][(string) $body_child->CmdRef][0], $_SESSION['TargetConfig']['SyncmlDeviceID'], $status_item->Source->LocURI);
					}
					if ($body_child->Cmd == 'Replace') {
						SyncmlMapUpdateSyncTime($_SESSION['PreviousCmds'][(string) $body_child->CmdRef][0], $_SESSION['TargetConfig']['SyncmlDeviceID'], $status_item->Source->LocURI);
					}
				}
				
				break;
			case 'Results' :
				/*
				 * Used to return the data results of either a Get or Search SyncML Command.
				 */
				break;
		}
	}
	
	if ($msg_id == 1) {
		// In package 1 we need to alert back our sync intentions
		foreach ($_SESSION['sync_requests'] as $sync_request => $sync_request_data) {
			$alert = $response->SyncBody->addChild('Alert');
			$alert->CmdID = GetCmdId();
			$alert->Data = $sync_request_data['Type'];
			$alert->Item->Target->LocURI = $sync_request_data['Source'];
			$alert->Item->Source->LocURI = $sync_request;
			$alert->Item->Meta->Anchor->Last = $sync_request_data['Last'];
			$alert->Item->Meta->Anchor->Next = $sync_request_data['Next'];
		}
	}
	
	if ($msg_id > 1) {
		// In package 2+ we send our Sync packages
		
		// PreviousCmds stores info on recent sync requests in order to process returned statuses
		$_SESSION['PreviousCmds'] = array();


		// ContinueNext stores the unsent results from the previous packet - if there are unsent results, then send those, otherwise, get any unsent results
		if (empty($_SESSION['ContinueNext'])) {
			$sync_request = false;
			// Loop through the sync requests and determine which are available to be sent - wait until they are done syncing before sending any
			// SLOWSYNC records
			
			foreach($_SESSION['sync_requests'] as $sync_request_temp => $sync_request_data) {
				if ($sync_request_data['Type'] == SYNCML_ALERT_TWOWAY || empty($sync_happend)) {
					$sync_request = $sync_request_temp;
					$sync_request_data = $_SESSION['sync_requests'][$sync_request];
					$changes = GetServerChanges($sync_request, $_SESSION['Security']['CompanyID'], $_SESSION['Security']['PersonID'], $_SESSION['TargetConfig']['SyncmlDeviceID'], $_SESSION['TargetConfig']['LastSync' . $ref_translations[$sync_request]]);
					unset($_SESSION['sync_requests'][$sync_request]);
					break;
				}
			}
			
//			if (!empty($_SESSION['sync_requests'])) {
//				list($sync_request) = array_keys($_SESSION['sync_requests']);
//				$sync_request_data = $_SESSION['sync_requests'][$sync_request];
//				unset($_SESSION['sync_requests'][$sync_request]);
//				
//				$changes = GetServerChanges($sync_request, $_SESSION['Security']['CompanyID'], $_SESSION['Security']['PersonID'], $_SESSION['TargetConfig']['SyncmlDeviceID'], $_SESSION['TargetConfig']['LastSync' . $ref_translations[$sync_request]]);
//			} else {
//				$sync_request = false;
//			}
		} else {
			$sync_request = $_SESSION['ContinueNext']['SyncRequest'];
			$sync_request_data = $_SESSION['ContinueNext']['SyncRequestData'];
			$changes = $_SESSION['ContinueNext']['Changes'];
			unset ($_SESSION['ContinueNext']);
		}

		$section_finished = true;
		if ($sync_request) {
			
			$sync_formatting = array('scal' => array('Type' => 'text/x-s4j-sife', 'Format' => 'b64'), 'card' => array('Type' => 'text/x-vcard'), 'stask' => array('Type' => 'text/x-s4j-sift', 'Format' => 'b64'));
			
			// foreach ($_SESSION['sync_requests'] as $sync_request => $sync_request_data) {
	
			$sync = $response->SyncBody->addChild('Sync');
			$sync->CmdID = GetCmdId();
			$sync->addChild('MsgRef', $msg_id);
			$sync->Target->LocURI = $sync_request_data['Source'];
			$sync->Source->LocURI = $sync_request;
	
			$sync->NumberOfChanges = count($changes['Add']) + count($changes['Replace']) + count($changes['Delete']);
			
			$xml_max_size = 250000;
			$xml_size_estimates = array('Section' => 105, 'Add' => 120, 'Replace' => 163, 'Delete' => 95);
			$xml_estimate = strlen($response->asXML());
			
			foreach (array ('Add', 'Replace', 'Delete') as $action) {
				if (count($changes[$action]) == 0) {
					continue;
				}
	
				$xml_estimate += $xml_size_estimates['Section'];
				$section = $sync->addChild($action);
	
				if (!empty($sync_formatting[$sync_request]['Type'])) {
					$section->addChild('Meta')->addChild('Type', $sync_formatting[$sync_request]['Type'])->AddAttribute('xmlns', 'syncml:metinf');
				}

				$section->CmdID = GetCmdId();
				$_SESSION['PreviousCmds'][(string) $section->CmdID] = array($ref_translations[$sync_request], $action);
				
				foreach ($changes[$action] as $guid => $data) {
					$xml_estimate += $xml_size_estimates[$action];
					if ($action == 'Add' || $action == 'Replace') {
						$xml_estimate += strlen($data['Data']);
					}
					
					if ($xml_estimate > $xml_max_size * .95) {
						$section_finished = false;
						break;
					}
					
					// $section->addChild('Meta')->addChild('Type', 'text/x-vcard')->AddAttribute('xmlns', 'syncml:metinf');
					
					
					$item = $section->AddChild('Item');
					if ($action == 'Add') {
						$item->Source->LocURI = $guid;
					} else {
						$item->Source->LocURI = $data['Luid'];
					}
					
					if ($action == 'Add' || $action == 'Replace') {
						if (!empty($sync_formatting[$sync_request]['Format'])) {
							$item->addChild('Meta')->addChild('Format', $sync_formatting[$sync_request]['Format'])->AddAttribute('xmlns', 'syncml:metinf');
						}
						
						
						$data_node = $item->addChild('Data');
						$dom_data_node = dom_import_simplexml($data_node);
						$cdata = $dom_data_node->ownerDocument->createCDATASection($data['Data']);
						$dom_data_node->appendChild($cdata);
					}
					unset($changes[$action][$guid]);
				}
				if (!$section_finished) {
					$_SESSION['ContinueNext']['SyncRequest'] = $sync_request;
					$_SESSION['ContinueNext']['SyncRequestData'] = $sync_request_data;
					$_SESSION['ContinueNext']['Changes'] = $changes;
					break;
				}
			}
			if ($section_finished && $sync_request != false) {
				// Update the section last sync time
				UpdateSyncmlDevice($_SESSION['TargetConfig']['SyncmlDeviceID'], $request->SyncHdr->SessionID, $ref_translations[$sync_request]);
				
				// Perhaps it would be better to wait for the response from the client?
			
				
			}
		}
				
		if (empty($_SESSION['ContinueNext']) && count($_SESSION['sync_requests']) == 0) {
			$response->SyncBody->addChild('Final');
		}
		// }
	}
	
	
	/*
	 * Upon sync successful completion, update the device record to indicate:
	 * SyncAnchor - used to validate that server and client consider the same sync as the last successful one
	 * LastSyncTime - used as comparison of file change times
	 */

	// UpdateSyncmlDevice($_SESSION['TargetConfig']['SyncmlDeviceID'], $request->SyncHdr->SessionID);
}

WriteDebug("Response Package:");
WriteDebug($response->asXML());

echo $response->asXML();
