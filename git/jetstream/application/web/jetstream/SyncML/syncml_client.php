<?php

set_time_limit(600);

define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

require 'lib.syncml.php';

define('DEBUG', true);

define('SYNCML_ALERT_TWOWAY', 200);
define('SYNCML_ALERT_SLOW_SYNC', 201);

define('SYNCML_OK', 200);
define('SYNCML_ACCEPTED_FOR_PROCESSING', 202);
define('SYNCML_ITEM_NOT_DELETED', 211);
define('SYNCML_AUTHENTICATION_ACCEPTED', 212);
define('SYNCML_UNAUTHORIZED', 401);
define('SYNCML_COMMAND_NOT_ALLOWED', 405);
define('SYNCML_OPTIONAL_FEATURE_NOT_SUPPORTED', 406);
define('SYNCML_AUTHENTICATION_REQUIRED', 407);

define('XML_DIR', './');

// Load the record from the database
$sql = "SELECT SyncmlDevice.*, People.CompanyID FROM SyncmlDevice LEFT JOIN People on SyncmlDevice.PersonID = People.PersonID WHERE SyncmlDeviceID = 4";
//$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $uri));

$recordset = DbConnManager::GetDb('mpower')->Exec($sql);


foreach($recordset as $target_config) {
	$sync = new SyncML();
	$sync->SetTargetConfig($target_config);
	$response = false;
	while ($sync->msg_id < 10) {
		echo $sync->msg_id . "\n";
		
		// Loop
		// Process Response, create next request
		$request = $sync->ProcessResponse($response);
		WriteDebug(date('h:i:s A'));
		WriteDebug("Request $sync->msg_id : $sync->reply_url");
		WriteDebug($request);
		
		$ch = curl_init();
		
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array ("Content-Type: application/vnd.syncml+xml"));
	    curl_setopt($ch, CURLOPT_URL, $sync->reply_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
		
		$response = curl_exec($ch);
		curl_close($ch);
		
		WriteDebug(date('h:i:s A'));
		WriteDebug("Response $sync->msg_id");
		WriteDebug($response);
		// End Loop
	}
}

class SyncML {
	public $target_uri;
	public $source_uri;
	public $reply_url;
	public $msg_id = 0;
	public $sync_requests;
	public $target_config;
	public $continue_next;
	public $previous_cmds;
	
	private $ref_translations = array ('card' => 'Contact', 'scal' => 'Event', 'stask' => 'Task');
	
	public $company_id;
	public $person_id;
	
	public function __construct() {
		$this->session_id = time();
	}
	
	public function SetTargetConfig($config) {
		$this->target_config = $config;
		
		// $this->source_uri = 'jet-' . uniqid();
		$this->source_uri = 'jet-' . $config['SyncmlDeviceID'];
		$this->person_id = $config['PersonID'];
		$this->company_id = $config['CompanyID'];
		$this->target_uri = $config['RemoteURL'];
		$this->remote_user = $config['RemoteUserID'];
		$this->remote_pw = $config['RemotePw'];

		$this->sync_requests['Contact']['Last'] = $config['SyncAnchorContact'];
		$this->sync_requests['Contact']['Next'] = $this->session_id;
		$this->sync_requests['Contact']['Type'] = SYNCML_ALERT_TWOWAY;
		$this->sync_requests['Contact']['Source'] = 'Contact';
		$this->sync_requests['Contact']['Target'] = 'ncon';
		
	}
	
	public function ProcessResponse($response) {
//		if (empty($this->target_config)) {
//			$this->target_config = LoadTargetConfig($this->source_uri);
//		}
		
		if (!empty($response)) {
			$response = new SimpleXMLElement($response);
		}
		
		$this->msg_id++;
		
		//		if (!$response) {
		//			$response = $this;
		//		}
		

		$request = simplexml_load_file(XML_DIR . 'syncml_1.2_template.xml');
		if (is_object($response) && !empty($response->SyncHdr->RespURI)) {
			$this->reply_url = $response->SyncHdr->RespURI;
		} else {
			$this->reply_url = $this->target_uri;
		}
		
		// ***** Create Header *****
		// Populate the response header
		$request->SyncHdr->SessionID = $this->session_id;
		$request->SyncHdr->MsgID = $this->msg_id;

		$request->SyncHdr->Target->LocURI = $this->target_uri;
		$request->SyncHdr->Source->LocURI = $this->source_uri;
		
		if (is_object($response) && !empty($response->SyncHdr)) {
			$msg_ref = $response->SyncHdr->MsgID;
			// ***** Create Response to Client's Header *****
			// Respond to the client's header with a status
			$header_status = $request->SyncBody->addChild('Status');
			$header_status->CmdID = GetCmdId(true);
			$header_status->MsgRef = $msg_ref;
			$header_status->CmdRef = 0; // 0 is always the CmdRef for the header
			$header_status->Cmd = 'SyncHdr';
			$header_status->TargetRef = $response->SyncHdr->Target->LocURI;
			$header_status->SourceRef = $response->SyncHdr->Source->LocURI;
			$header_status->Data = SYNCML_OK;
		}
		
		if (is_object($response) && !empty($response->SyncBody)) {
			foreach ($response->SyncBody->children() as $body_child_type => $body_child) {
				switch ($body_child_type) {
					// SyncML defines the following "request" commands: 
					case 'Alert' :
						/*
						 * Allows the originator to notify the recipient. The notification
						 * can be used as an application-to-application message or a message
						 * intended for display through the recipient's user interface.
						 */
						if ($body_child->Data == SYNCML_ALERT_TWOWAY || $body_child->Data == SYNCML_ALERT_SLOW_SYNC) {
							// Build the Status as we process the alert
							$status = $request->SyncBody->addChild('Status');
							$status->CmdID = GetCmdId();
							$status->MsgRef = $msg_ref;
							$status->CmdRef = $body_child->CmdID;
							$status->Cmd = 'Alert';
							
							// $target = $status->TargetRef = (string) $body_child->Item->Target->LocURI;
							// $status->SourceRef = $this->sync_requests[$target]['Source'] = (string) $body_child->Item->Source->LocURI;
							
							// $this->sync_requests[$target]['Source'] = $status->TargetRef = (string) $body_child->Item->Source->LocURI;
							// $status->SourceRef = $target = (string) $body_child->Item->Target->LocURI;

							$status->TargetRef = (string) $body_child->Item->Source->LocURI;
							$status->SourceRef = $target = (string) $body_child->Item->Target->LocURI;
							
							$server_anchor = $this->target_config['SyncAnchor' . $target];
							//$server_anchor = $this->sync_requests[$target]['Last'] = $this->target_config['SyncAnchor' . $target];
							$device_anchor = (string) $body_child->Item->Meta->Anchor->Last;
							
							// if ($server_anchor == $device_anchor) {
								// $status->Data = $this->sync_requests[$target]['Type'] = SYNCML_ALERT_TWOWAY;
							// } else {
							//	$status->Data = $this->sync_requests[$target]['Type'] = SYNCML_ALERT_SLOW_SYNC;
							//}
							
							$status->Data = 200;
							$status->Item->Data->Anchor->Next = (string) $body_child->Item->Meta->Anchor->Next;
							// $status->Item->Data->Anchor->Next = $this->sync_requests[$target]['Next'] = (string) $body_child->Item->Meta->Anchor->Next;
							$status->Item->Data->Anchor->AddAttribute('xmlns', 'syncml:metinf');
							
						// $this->sync_requests[$target]['Last'] = (string) $body_child->Item->Meta->Anchor->Last;
						}
						break;
					case 'Get' :
						/*
						 * Get. Allows the originator to ask for a data element or data elements 
						 * from the recipient. A get can include the resetting of any meta-information
						 * that the recipient maintains about the data element or collection.
						 */
						
						$status = $request->SyncBody->addChild('Status');
						$status->CmdID = GetCmdId();
						$status->MsgRef = $msg_ref;
						$status->CmdRef = $body_child->CmdID;
						$status->Cmd = 'Get';
						// $status->TargetRef = './devinf12';
						$status->Data = 200;

						$status = $request->SyncBody->addChild('Results');
						$status->CmdID = GetCmdId();
						$status->MsgRef = $msg_ref;
						$status->CmdRef = $body_child->CmdID;
						$status->Meta->Type = 'application/vnd.syncml-devinf+xml';
						$status->Meta->Type->AddAttribute('xmlns', 'syncml:metinf');
						
						foreach ($body_child->Item as $request_item) {
							$response_item = $status->addChild('Item');
							$response_item->Source->LocURI = $request_item->Target->LocURI;
							// $response_item->Source->LocName = $request_item->Target->LocName;
							switch ($request_item->Target->LocURI) {
								case './devinf12' :
									$data = $response_item->addChild('Data');
									$dom_data_node = dom_import_simplexml($data);
									$dom_dev_inf = DomDocument::load(XML_DIR . 'client_devinf.xml');
									$dom_data_node->appendChild($dom_data_node->ownerDocument->importNode($dom_dev_inf->documentElement, true));
									$data->DevInf->DevID = $this->source_uri;
									break;
								default :
									$response_item->Data = SYNCML_OPTIONAL_FEATURE_NOT_SUPPORTED;
							}
						}
						break;
					case 'Map' :
						/*
						 * Allows the originator to ask the recipient to update the identifier
						 * mapping between two data collections.  
						 */
						$map_type = $this->ref_translations[(string) $body_child->Target->LocURI];
						foreach ($body_child->MapItem as $map_item) {
							SyncmlMapInsert($map_type, $this->target_config['SyncmlDeviceID'], $map_item->Source->LocURI, $map_item->Target->LocURI);
						}
						break;
					case 'Put' :
						/*
						 * Allows the original to put a data element or data elements on to the 
						 * recipient.
						 */
						$status = $request->SyncBody->addChild('Status');
						$status->addChild('CmdID', GetCmdId());
						$status->addChild('MsgRef', $msg_ref);
						$status->addChild('CmdRef', $body_child->CmdID);
						$status->addChild('Cmd', 'Put');
						$status->addChild('SourceRef', $body_child->Item->Source->LocURI);
						$status->addChild('Data', SYNCML_OK);
						break;
					case 'Sync' :
						/*
						 * Allows the originator to specify that the included commands be treated
						 * as part of the synchronization of two data collections
						 */
						
						$target = (string) $body_child->Target->LocURI; // This is the type of record we are receiving
						
						$status = $request->SyncBody->addChild('Status');
						$status->CmdID = GetCmdId();
						$status->MsgRef = $msg_ref;
						$status->CmdRef = $body_child->CmdID;
						$status->Cmd = 'Sync';
						$status->TargetRef = $body_child->Source->LocURI;
						$status->SourceRef = $body_child->Target->LocURI;
						$status->Data = SYNCML_OK;

						$mappings = array();
						// Bundle the sync request for processing
						foreach ($body_child as $action => $action_data) {
							if ($action == 'Add') {
								$status = $request->SyncBody->addChild('Status');
								$status->CmdID = GetCmdId();
								$status->MsgRef = $msg_ref;
								$status->CmdRef = $action_data->CmdID;
								$status->Cmd = $action;
								$status->Data = 201;
							}
							
							$sync_data = array ();
							$cmd_ref = $action_data->CmdID;
							foreach ($action_data->Item as $item) {
							// print_r($item);
								$item_data = ($action == 'Delete') ? true : (!empty($item->Meta->Format)) ? DecodeItem($item->Data, $item->Meta->Format) : $item->Data;
								$sync_data[(string) $item->Source->LocURI] = array ($action, (string) $item_data);

								$status_item = $status->addChild('Item');
								$status_item->Source->LocURI = $item->Source->LocURI;
							}
							if (count($sync_data) > 0) {
								$sync_results = ClientDoSync($target, $this->company_id, $this->person_id, $target, $this->target_config['SyncmlDeviceID'], $sync_data);
								// $status = $request->SyncBody->addChild('Status');
								// On an add we need to send back mappings
								
								
								if ($action == 'Add') {
									$mappings = $mappings + $sync_results['map'];
								}
							}
						}
						
						if (count($mappings) > 0) {
							print_r($mappings);
							$mapping = $request->SyncBody->addChild('Map');
							$mapping->CmdID = GetCmdId();
							$mapping->Target->LocURI = $body_child->Source->LocURI;
							$mapping->Source->LocURI = $body_child->Target->LocURI;
							foreach ($mappings as $luid => $guid) {
								$map_item = $mapping->addChild('MapItem');
								$map_item->Target->LocURI = $luid;
								$map_item->Source->LocURI = $guid;
							}
						}
						
						break;
					// SyncML defines the following "response" commands:
					case 'Status' :
						/*
						 * Indicates the completion status of an operation or that an error 
						 * occurred while processing a previous request.  
						 */
						if ($body_child->Cmd == 'Delete' && isset($this->previous_cmds[(string) $body_child->CmdRef])) {
							foreach ($body_child->Item as $deleted_item) {
								SyncmlMapDelete($this->previous_cmds[(string) $body_child->CmdRef][0], $this->target_config['SyncmlDeviceID'], $deleted_item->Source->LocURI);
							}
						}
						
						break;
					case 'Results' :
						
						/*
						 * Used to return the data results of either a Get or Search SyncML Command.
						 */
						$status = $request->SyncBody->addChild('Status');
						$status->CmdID = GetCmdId();
						$status->MsgRef = $msg_ref;
						$status->CmdRef = $body_child->CmdID;
						$status->Cmd = 'Results';
						$status->Data = '200';
						
						break;
				}
			}
		}
		
		if ($this->msg_id == 1) {
			$cred = $request->SyncHdr->AddChild('Cred');
			$cred->Meta->Format = 'b64';
			$cred->Meta->Format->AddAttribute('xmlns', 'syncml:metinf');
			$cred->Meta->Type = 'syncml:auth-basic';
			$cred->Meta->Type->AddAttribute('xmlns', 'syncml:metinf');
			$cred->Data = base64_encode( $this->remote_user . ':' . $this->remote_pw);

			// In package 1 we need to alert back our sync intentions
			foreach ($this->sync_requests as $sync_request => $sync_request_data) {
				$alert = $request->SyncBody->addChild('Alert');
				$alert->CmdID = GetCmdId();
				$alert->Data = $sync_request_data['Type'];
				$alert->Item->Target->LocURI = $sync_request_data['Target'];
				$alert->Item->Source->LocURI = $sync_request_data['Source'];
				$alert->Item->Meta->Anchor->Last = $sync_request_data['Last'];
				$alert->Item->Meta->Anchor->Next = $sync_request_data['Next'];
				$alert->Item->Meta->Anchor->AddAttribute('xmlns', 'syncml:metinf');
			}

			$request->SyncBody->addChild('Final');
			
//			$status = $request->SyncBody->addChild('Put');
//			$status->CmdID = GetCmdId();
//			$status->Meta->Type = 'application/vnd.syncml-devinf+xml';
//			$status->Meta->Type->AddAttribute('xmlns', 'syncml:metinf');
//			$response_item = $status->addChild('Item');
//			$response_item->Source->LocURI = './devinf12';
//			$response_item->Source->LocName = './devinf12';
//			$data = $response_item->addChild('Data');
//			$dom_data_node = dom_import_simplexml($data);
//			$dom_dev_inf = DomDocument::load(XML_DIR . 'client_devinf.xml');
//			$dom_data_node->appendChild($dom_data_node->ownerDocument->importNode($dom_dev_inf->documentElement, true));
//			$data->DevInf->DevID = $this->source_uri;
			
//			$get = $request->SyncBody->AddChild('Get');
//			$get->CmdID = GetCmdId();
//			$get->Meta->Type = 'application/vnd.syncml-devinf+xml';
//			$get->Meta->Type->AddAttribute('xmlns', 'syncml:metinf');
//			$get->Item->Target->LocURI = './devinf12';
//			$get->Item->Target->LocName = './devinf12';
//			$request->SyncBody->Get->CmdID = 2;
			
		}
		
		if ($this->msg_id > 1) {
			// WriteDebug("Continue Next:" . print_r($this->continue_next, true));
			// WriteDebug("Sync Requests:" . print_r($this->sync_requests, true));
			// In package 2+ we send our Sync packages
			

			// PreviousCmds stores info on recent sync requests in order to process returned statuses
			$this->previous_cmds = array ();
			
			// ContinueNext stores the unsent results from the previous packet - if there are unsent results, then send those, otherwise, get any unsent results
			if (empty($this->continue_next)) {
				$sync_request = false;
				// Loop through the sync requests and determine which are available to be sent - wait until they are done syncing before sending any
				// SLOWSYNC records
				foreach ($this->sync_requests as $sync_request_temp => $sync_request_data) {
//					if ($sync_request_data['Type'] == SYNCML_ALERT_TWOWAY || empty($sync_happend)) {
						$sync_request = $sync_request_temp;
						$sync_request_data = $this->sync_requests[$sync_request];

						$changes = GetServerChanges($sync_request, $this->company_id, $this->person_id, $this->target_config['SyncmlDeviceID'], $this->target_config['LastSync' . $sync_request]);
						unset($this->sync_requests[$sync_request]);
						break;
//					}
				}
				
			//			if (!empty($this->sync_requests)) {
			//				list($sync_request) = array_keys($this->sync_requests);
			//				$sync_request_data = $this->sync_requests[$sync_request];
			//				unset($this->sync_requests[$sync_request]);
			//				
			//				$changes = GetServerChanges($sync_request, $this->company_id, $this->person_id, $this->target_config['SyncmlDeviceID'], $this->target_config['LastSync' . $sync_request]);
			//			} else {
			//				$sync_request = false;
			//			}
			} else {
				$sync_request = $this->continue_next['SyncRequest'];
				$sync_request_data = $this->continue_next['SyncRequestData'];
				$changes = $this->continue_next['Changes'];
				unset($this->continue_next);
			}
			
			$section_finished = true;
			if ($sync_request) {
				$sync_formatting = array ('scal' => array ('Type' => 'text/x-s4j-sife', 'Format' => 'b64'), 'Contact' => array ('Type' => 'text/x-vcard'), 'stask' => array ('Type' => 'text/x-s4j-sift', 'Format' => 'b64'));
				
				// foreach ($this->sync_requests as $sync_request => $sync_request_data) {
				

				$sync = $request->SyncBody->addChild('Sync');
				$sync->CmdID = GetCmdId();
				//$sync->addChild('MsgRef', $msg_ref);
				$sync->Target->LocURI = $sync_request_data['Target'];
				$sync->Source->LocURI = $sync_request_data['Source'];
				
				// $sync->NumberOfChanges = count($changes['Add']) + count($changes['Replace']) + count($changes['Delete']);
				
				$xml_max_size = 250000;
				$xml_size_estimates = array ('Section' => 105, 'Add' => 120, 'Replace' => 163, 'Delete' => 95);
				$xml_estimate = strlen($response->asXML());
				
				foreach (array ('Add', 'Replace', 'Delete') as $action) {
					if (count($changes[$action]) == 0) {
						continue;
					}
					
					$xml_estimate += $xml_size_estimates['Section'];
					$section = $sync->addChild($action);
					$section->CmdID = GetCmdId();
					
					if (!empty($sync_formatting[$sync_request]['Type'])) {
						$section->addChild('Meta')->addChild('Type', $sync_formatting[$sync_request]['Type'])->AddAttribute('xmlns', 'syncml:metinf');
					}
					
					$this->previous_cmds[(string) $section->CmdID] = array ($sync_request, $action);
					
					foreach ($changes[$action] as $guid => $data) {
						$xml_estimate += $xml_size_estimates[$action];
						if ($action == 'Add' || $action == 'Replace') {
							$xml_estimate += strlen($data['Data']);
						}
						
						if ($xml_estimate > $xml_max_size * .95) {
							$section_finished = false;
							break;
						}
						
						// $section->addChild('Meta')->addChild('Type', 'text/x-vcard')->AddAttribute('xmlns', 'syncml:metinf');
						

						$item = $section->AddChild('Item');
						if ($action == 'Add') {
							$item->Source->LocURI = $guid;
						} else {
							$item->Source->LocURI = $data['Luid'];
						}
						
						if ($action == 'Add' || $action == 'Replace') {
							if (!empty($sync_formatting[$sync_request]['Format'])) {
								$item->addChild('Meta')->addChild('Format', $sync_formatting[$sync_request]['Format'])->AddAttribute('xmlns', 'syncml:metinf');
							}
							
							$data_node = $item->addChild('Data');
							$dom_data_node = dom_import_simplexml($data_node);
							$cdata = $dom_data_node->ownerDocument->createCDATASection($data['Data']);
							$dom_data_node->appendChild($cdata);
						}
						unset($changes[$action][$guid]);
					}
					if (!$section_finished) {
						$this->continue_next['SyncRequest'] = $sync_request;
						$this->continue_next['SyncRequestData'] = $sync_request_data;
						$this->continue_next['Changes'] = $changes;
						break;
					}
				}
				if ($section_finished && $sync_request != false) {
					// Update the section last sync time
					UpdateSyncmlDevice($this->target_config['SyncmlDeviceID'], $request->SyncHdr->SessionID, $sync_request);
					
					// Perhaps it would be better to wait for the response from the client?
				

				}
			}
			 
			if (empty($this->continue_next) && count($this->sync_requests) == 0) {
				if (!empty($this->TwoTwoTwoSent)) {
					$request->SyncBody->addChild('Final');
				} elseif (!empty($this->FinalSent)) {
					$alert = $request->SyncBody->addChild('Alert');
					$alert->CmdID = GetCmdId();
					$alert->Data = 222;
					$alert->Item->Target->LocURI = 'ncon';
					$alert->Item->Source->LocURI = 'Contact';
					$this->TwoTwoTwoSent = true;
				} else {
					$request->SyncBody->addChild('Final');
					$this->FinalSent = true;
				}
			}
			
		/*
		 * Upon sync successful completion, update the device record to indicate:
		 * SyncAnchor - used to validate that server and client consider the same sync as the last successful one
		 * LastSyncTime - used as comparison of file change times
		 */
		
		// UpdateSyncmlDevice($this->target_config['SyncmlDeviceID'], $request->SyncHdr->SessionID);
		

		}
		return $request->asxml();
	}
}


// function ClientDoSync($rec_type, $company_id, $person_id, $target, $device_id, $sync_data, $action) {
function ClientDoSync($rec_type, $company_id, $person_id, $target, $device_id, $sync_data) {
	$luid_list = array_keys($sync_data);
	
	if ($rec_type == 'Contact') {
		$sql = "SELECT Contact.*, Luid FROM Contact 
			INNER JOIN SyncmlMapContact ON Contact.ContactID = SyncmlMapContact.ContactID
			WHERE Contact.CompanyID = ? AND SyncmlDeviceID = ? AND Luid IN (" . ArrayQm($luid_list) . ")";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($company_id, $device_id)), array(DTYPE_STRING, $luid_list));
		
		WriteDebug($sql);
		$recordset = DbConnManager::GetDb('mpower')->Execute($sql, 'Contact', 'Luid');
		
		$company_name_list = array();
		
	} elseif ($rec_type == 'Event' || $rec_type == 'Task') {
		$sql = "SELECT * FROM Event
			INNER JOIN SyncmlMapEvent ON Event.EventID = SyncmlMapEvent.EventID
			WHERE Event.CompanyID = ? AND SyncmlDeviceID = ? AND Luid IN (" . ArrayQm($luid_list) . ")";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($company_id, $device_id)), array(DTYPE_STRING, $luid_list));
		$recordset = DbConnManager::GetDb('mpower')->Execute($sql, 'RecEvent', 'Luid');
	} else {
		return false;
	}
	
	$recordset->Initialize();

	foreach($sync_data as $luid => & $item_data) {
		if ($recordset->InRecordset($luid)) {
			$record = $recordset[$luid];
		} else {
			$record = false;
		}
		$results = array();
		$action = $item_data[0];

		// WriteDebug(print_r($item_data, true));
		switch ($action) {
			case 'Add' :
				// Create a new Record
				// $recordset = DbConnManager::GetDb('mpower')->Execute("SELECT * FROM Contact WHERE 1 = 0", 'Contact');
				
				if (!$record) {
					// Create a new contact record
					$record_class = ($rec_type == 'Contact') ? 'Contact' : 'RecEvent';
					$record = new $record_class();
					$recordset[] = $record;
					$record->SetDatabase(DbConnManager::GetDb('mpower'));
					$record->CompanyID = $company_id;
					$record->NewLuid = $luid;
					$result[$luid] = 200;
				} elseif ($rec_type == 'Contact' && ($record->Inactive == 1 || $record->Deleted == 1)) {
					// The record they are trying to add is shown as having been deleted - reactivate the deleted rec and treat
					// it like a replace
					$action = $item_data[0] = 'Replace';
					$record->Inactive = 0;
					$record->Deleted = 0;
					$result[$luid] = 418;
					SyncmlMapUndelete($rec_type, $device_id, $luid);
				} elseif (($rec_type == 'Event' || $rec_type == 'Task') && $recordset->Cancelled == 1) {
					$record->Cancelled = 0;
					SyncmlMapUndelete($rec_type, $device_id, $luid);
				} else {
					// The luid requested to add is already in the map and has not been deleted
					//return false;
					continue 2;
				}
				break;
			case 'Delete' :
				if (!$record) {
					// We don't have the record they are trying to delete on the server, which is okay by the protocol
					// Don't do anything
					$result[$luid] = 211;
					continue 2;
				}
				if ($rec_type == 'Contact') {
					$record->Inactive = 1;
				} elseif ($rec_type == 'Event' || $rec_type == 'Task') {
					$record->Cancelled = 1;
				}				
				$result[$luid] = 200;
				
				SyncmlMapDelete($rec_type, $device_id, $luid);
				break;
			case 'Replace' :
				if (!$record) {
					// They have requested to replace a contact that isn't in the map
					// return false;
					$result[$luid] = 404;
					continue 2;
				}
				break;
		}
		
		$record->Initialize();
		
		// Update the record
		if ($action == 'Add' || $action == 'Replace') {
			if ($rec_type == 'Contact') {
				MergeVcardIntoContact($company_id, $record, $item_data[1]);
			} elseif ($rec_type == 'Event') {
				MergeScalIntoEvent($record, $item_data[1]);
			} elseif ($rec_type == 'Task') {
				MergeStaskIntoEvent($record, $item_data[1]);
			}
		}
	}
	
	if ($rec_type == 'Contact') {
		// Process to get company info for contacts:
		// Loop through contact records and get the account names
		// SQL request Jetstream account matches
		// Loop through contact records and create records for missing account names, attach matches
		
		
		// Do a search for all the company names, and update the individual contact records.  If the company doesn't exist, create it.
		// Get all the company names from the updated records
		$company_names = array();
		foreach ($recordset as $record) {
			// if (isset($record->CompanyName) && !empty($record->CompanyName)) {
				$company_names[] = $record->CompanyName;
			// }
		}
		// WriteDebug(print_r($company_names, true));
		if (count($company_names) > 0) {
			// Search the account tables for the names
			$sql = "SELECT AccountID, CompanyID, " . MapLookup::GetAccountNameField($company_id, true) . " FROM Account WHERE CompanyID = ? 
				AND " . MapLookup::GetAccountNameField($company_id, true) . " IN (" . ArrayQm($company_names) . ") 
				AND (Inactive = 0 OR Inactive IS NULL) AND (Deleted = 0 OR Deleted IS NULL)";
			
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_id), array(DTYPE_STRING, $company_names));
			// WriteDebug($sql);
			$accounts = DbConnManager::GetDb('mpower')->Execute($sql, 'RecAccount', MapLookup::GetAccountNameField($company_id, true));
			
			foreach ($recordset as $record) {
				// WriteDebug($record->Text01 . ' ' . $record->Text02 . ' ' . $record->CompanyName);
				//if (isset($record->CompanyName) && !empty($record->CompanyName)) {
					if ($accounts->InRecordset($record->CompanyName)) {
						$record->AccountID = $accounts[$record->CompanyName]['AccountID'];
					} else {
						$new_account = new RecAccount();
						$accounts[$record->CompanyName] = $new_account;
						$new_account->SetDatabase(DbConnManager::GetDb('mpower'));
						$new_account->Initialize();
						$new_account->CompanyID = $company_id;
						$new_account->{MapLookup::GetAccountNameField($company_id, true)} = $record->CompanyName;
						$new_account->Save();
						// $new_account->Debug();
						$record->AccountID = $new_account['AccountID'];
					}
				//}
			}
			
			// $accounts->Debug();
		}
	}
	$recordset->Save();

	foreach ($recordset as $record) {
		if ($record->InRecord('NewLuid')) {
			if ($rec_type == 'Event' || $rec_type == 'Task') {
				$sql = "INSERT INTO EventAttendee (EventID, PersonID, Creator, Deleted) values ($record->EventID, $person_id, 1, 0)";
				DbConnManager::GetDb('mpower')->Exec($sql);
				SyncmlMapInsert('Event', $device_id, $luid, $record->EventID);
			} else {
				SyncmlMapInsert($rec_type, $device_id, $luid, $record->ContactID);
			}
		}
	}

	// Loop through the original requests and create a mapping list
	foreach($sync_data as $luid => $item_data) {
		if ($item_data[0] == 'Add') {
			$result['map'][$luid] = $recordset[$luid]->ContactID;
		}
	}

	// print_r($map);
	
	$recordset->Destroy();
	return $result;
}







