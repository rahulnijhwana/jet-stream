<?php /* Smarty version 2.6.18, created on 2015-02-09 12:40:14
         compiled from shell_sidebar.tpl */ ?>
<div class="module">
	<div class="module_title">
		<div class="module_title_text">
			<?php echo $this->_tpl_vars['module']->title; ?>

		</div>
		<div class="module_title_icons">
			<?php $_from = $this->_tpl_vars['module']->buttons; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['buttons'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['buttons']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['button']):
        $this->_foreach['buttons']['iteration']++;
?><a <?php if ($this->_tpl_vars['button']['id']): ?>id="<?php echo $this->_tpl_vars['button']['id']; ?>
"<?php endif; ?> title="<?php echo $this->_tpl_vars['button']['tooltip']; ?>
" href="<?php echo $this->_tpl_vars['button']['action']; ?>
"><?php echo $this->_tpl_vars['button']['tooltip']; ?>
</a><?php if (! ($this->_foreach['buttons']['iteration'] == $this->_foreach['buttons']['total'])): ?> | <?php endif; ?><?php endforeach; endif; unset($_from); ?>
		</div>
	</div>
	<?php $_from = $this->_tpl_vars['module']->template_files; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['template_file']):
?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['template_file'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<?php endforeach; endif; unset($_from); ?>
</div>