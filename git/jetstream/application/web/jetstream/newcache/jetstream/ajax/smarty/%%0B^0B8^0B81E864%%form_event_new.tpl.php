<?php /* Smarty version 2.6.18, created on 2015-02-10 05:15:44
         compiled from form_event_new.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'FormatDate', 'form_event_new.tpl', 17, false),array('modifier', 'GetOptionsTime', 'form_event_new.tpl', 95, false),array('modifier', 'GetOptionsNumRng', 'form_event_new.tpl', 119, false),array('modifier', 'GetOptionsMonths', 'form_event_new.tpl', 141, false),array('modifier', 'json_encode', 'form_event_new.tpl', 337, false),array('function', 'html_options', 'form_event_new.tpl', 114, false),array('function', 'html_checkboxes', 'form_event_new.tpl', 124, false),)), $this); ?>
<div id="eventMiniCalendar" style="float: right; border: 0px solid black; padding: 5px; width:282px; overflow:auto">
	<div class="loading">
		<img class="spinner" src="<?php echo @WEB_PATH; ?>
/images/slipstream_spinner.gif" />
	</div>
</div>
<input type="hidden" name="EventID" value="<?php echo $this->_tpl_vars['module']->event_info['EventID']; ?>
">
<input type="hidden" name="AccountID" value="<?php echo $this->_tpl_vars['module']->event_info['AccountID']; ?>
">
<input type="hidden" name="ContactID" value="<?php echo $this->_tpl_vars['module']->event_info['ContactID']; ?>
">
<input type="hidden" name="DealID" value="<?php echo $this->_tpl_vars['module']->event_info['DealID']; ?>
">
<?php if (! $this->_tpl_vars['module']->event_info['EventID'] && $this->_tpl_vars['module']->event_info['RepeatParent']): ?>
<input type="hidden" name="RepeatParent" value="<?php echo $this->_tpl_vars['module']->event_info['RepeatParent']; ?>
">
<input type="hidden" name="OrigStartDate" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['module']->event_info['StartTimeStamp'])) ? $this->_run_mod_handler('FormatDate', true, $_tmp, 'trunc') : FormatDate($_tmp, 'trunc')); ?>
">
<?php endif; ?>
<div id="contactForm" style="width: 495px; float: left; margin: 5px 0px;">
	<div class="event_form">
		<table>
			<tr>
				<td colspan="2">
					<?php if ($this->_tpl_vars['module']->event_info['AccountID']): ?>
						<span class="acc_con_label">Company</span> <span class="form_required">*</span><br />
							<input type="text" id="account" name="account" class="full" value="<?php echo $this->_tpl_vars['module']->event_info['AccountName']; ?>
" onkeyup="javascript:ClearAccount();" onchange="javascript:ClearAccount();" />
							<div class="form_error" id="err_Account"></div>
						<!--span class="acc_con_label">Contact</span> <span class="form_required">*</span><br />
						<input type="text" id="Contact" name="Contact" class="full" value="<?php echo $this->_tpl_vars['module']->event_info['Contact']['FirstName']; ?>
<?php if ($this->_tpl_vars['module']->event_info['Contact']['FirstName'] && $this->_tpl_vars['module']->event_info['Contact']['LastName']): ?> <?php endif; ?><?php echo $this->_tpl_vars['module']->event_info['Contact']['LastName']; ?>
<?php if ($this->_tpl_vars['module']->event_info['Contact']['CompanyName']): ?> (<?php echo $this->_tpl_vars['module']->event_info['Contact']['CompanyName']; ?>
)<?php endif; ?>" onkeyup="javascript:ClearContact();" onchange="javascript:ClearContact();" /-->
						<div class="form_error" id="err_Contact"></div>

					<?php else: ?>
						<span class="acc_con_label">Contact</span> <span class="form_required">*</span><br />
						<?php if ($this->_tpl_vars['module']->event_info['ContactID'] == -1): ?>
							Opportunity Contact
						<?php else: ?>
							<input type="text" id="Contact" name="Contact" class="full" value="<?php echo $this->_tpl_vars['module']->event_info['Contact']['FirstName']; ?>
<?php if ($this->_tpl_vars['module']->event_info['Contact']['FirstName'] && $this->_tpl_vars['module']->event_info['Contact']['LastName']): ?> <?php endif; ?><?php echo $this->_tpl_vars['module']->event_info['Contact']['LastName']; ?>
<?php if ($this->_tpl_vars['module']->event_info['Contact']['CompanyName']): ?> (<?php echo $this->_tpl_vars['module']->event_info['Contact']['CompanyName']; ?>
)<?php endif; ?>" onkeyup="javascript:ClearContact();" onchange="javascript:ClearContact();" />
							<div class="form_error" id="err_Contact"></div>
						<?php endif; ?>
					<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<span class="acc_con_label">Subject</span> <span class="form_required">*</span><br />
					<input type="text" name="Subject" class="full" value="<?php echo $this->_tpl_vars['module']->event_info['Subject']; ?>
" />
					<div class="form_error" id="err_Subject"></div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<span class="acc_con_label">Location</span><br />
					<textarea name="Location" rows="3" class="full"><?php echo $this->_tpl_vars['module']->event_info['Location']; ?>
</textarea>
					<div class="form_error" id="err_Location"></div>
				</td>
			</tr>
			<tr>
				<td>
					<span class="acc_con_label">Type</span> <span class="form_required">*</span><br />				
					<select name="EventType" id="EventType" class="clsSelect" onchange="DoChangeType(this.value);">
						<option value="" event_type=""></option>
							<?php if (! $this->_tpl_vars['module']->event_info['AccountID']): ?>
							<optgroup label="Events (Date with Time)">
								<?php $_from = $this->_tpl_vars['module']->GetEventTypes('EVENT'); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['EventType']):
?>
									<option style="background-color: <?php echo $this->_tpl_vars['EventType']['EventColor']; ?>
" value="<?php echo $this->_tpl_vars['EventType']['EventTypeID']; ?>
" <?php if ($this->_tpl_vars['module']->event_info['EventTypeID'] == $this->_tpl_vars['EventType']['EventTypeID']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['EventType']['EventName']; ?>
</option>
								<?php endforeach; endif; unset($_from); ?>
							</optgroup>
							<?php endif; ?>
							<?php if (! $this->_tpl_vars['module']->event_info['DealID']): ?>
								<optgroup label="Tasks (Date Only)">
									<?php $_from = $this->_tpl_vars['module']->GetEventTypes('TASK'); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['EventType']):
?>
										<option style="background-color:<?php echo $this->_tpl_vars['EventType']['EventColor']; ?>
" value="<?php echo $this->_tpl_vars['EventType']['EventTypeID']; ?>
" <?php if ($this->_tpl_vars['module']->event_info['EventTypeID'] == $this->_tpl_vars['EventType']['EventTypeID']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['EventType']['EventName']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</optgroup>
							<?php endif; ?>
					</select>						
					<div class="form_error" id="err_EventType"></div>
				</td>
				<td>
					<span class="acc_con_label" id="label_priority">Task Priority</span><br />
					<select name="priority" id="priority">
						<option value="">-</option>
						<?php $_from = $this->_tpl_vars['module']->priorities; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['priority']):
?>
							<option value="<?php echo $this->_tpl_vars['priority']['PriorityID']; ?>
" <?php if ($this->_tpl_vars['module']->event_info['PriorityID'] == $this->_tpl_vars['priority']['PriorityID']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['priority']['Priority']; ?>
 - (<?php echo $this->_tpl_vars['priority']['Label']; ?>
)</option>
						<?php endforeach; endif; unset($_from); ?>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<span class="acc_con_label">When</span> <span class="form_required">*</span><br />
	 				<input type="text" name="StartDate" id="StartDate" class="datepicker" size="6" value="<?php if ($this->_tpl_vars['module']->event_info['StartTimeStamp']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['module']->event_info['StartTimeStamp'])) ? $this->_run_mod_handler('FormatDate', true, $_tmp, 'date') : FormatDate($_tmp, 'date')); ?>
<?php endif; ?>" />
					<span id="event_datetime">
					<select name="StartTime">
						<?php if (! $this->_tpl_vars['module']->event_info['BlankTime']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['module']->event_info['StartTimeStamp'])) ? $this->_run_mod_handler('GetOptionsTime', true, $_tmp) : GetOptionsTime($_tmp)); ?>
<?php endif; ?>
						<?php if ($this->_tpl_vars['module']->event_info['BlankTime']): ?><?php echo ((is_array($_tmp='')) ? $this->_run_mod_handler('GetOptionsTime', true, $_tmp) : GetOptionsTime($_tmp)); ?>
<?php endif; ?>
					</select>
					to
					<select name="EndTime">
						<?php if (! $this->_tpl_vars['module']->event_info['BlankTime']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['module']->event_info['EndTimeStamp'])) ? $this->_run_mod_handler('GetOptionsTime', true, $_tmp) : GetOptionsTime($_tmp)); ?>
<?php endif; ?>
						<?php if ($this->_tpl_vars['module']->event_info['BlankTime']): ?><?php echo ((is_array($_tmp='')) ? $this->_run_mod_handler('GetOptionsTime', true, $_tmp) : GetOptionsTime($_tmp)); ?>
<?php endif; ?>
					</select>
					<input type="text" name="EndDate" id="EndDate" class="datepicker" size="6" value="<?php if ($this->_tpl_vars['module']->event_info['EndTimeStamp']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['module']->event_info['EndTimeStamp'])) ? $this->_run_mod_handler('FormatDate', true, $_tmp, 'date') : FormatDate($_tmp, 'date')); ?>
<?php endif; ?>" />
					</span>
					<div class="form_error" id="err_When"></div>
				</td>
			</tr>
						<?php if (! $this->_tpl_vars['module']->event_info['RepeatParent'] && ! ( $this->_tpl_vars['module']->event_info['DealID'] == -1 || $this->_tpl_vars['module']->event_info['DealID'] > 0 )): ?>
			<tr>
				<td id="event_repeat" colspan="2">
					<span class="acc_con_label">Repeats</span><br />
					<select name="Repeat" onchange="DoChangeRepeat(this.value);">
						<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['module']->repeat_types,'selected' => $this->_tpl_vars['module']->event_info['repeat_selector']), $this);?>

					</select>
					<fieldset id="repeat_choices" class="hidden" style="border:1px solid #E6E6E6;">
						<legend>Repeat Frequency</legend>
						<div id="repeat_1">
							<p>Every <select name="daily_freq"><?php echo GetOptionsNumRng($this->_tpl_vars['module']->repeat_interval['daily_freq'], 1, 31); ?>
</select> day(s)</p>
						</div>
						<div id="repeat_2">
							<p>Every <select name="weekly_freq"><?php echo GetOptionsNumRng($this->_tpl_vars['module']->repeat_interval['weekly_freq'], 1, 52); ?>
</select> week(s) on:</p>
							<p>
								<?php echo smarty_function_html_checkboxes(array('name' => 'weekly_day','options' => $this->_tpl_vars['module']->weekdays,'selected' => $this->_tpl_vars['module']->repeat_interval['weekly_days'],'separator' => '<br />'), $this);?>

							</p>
						</div>
						<div id="repeat_3">
							<p>Every <select name="monthly_freq"><?php echo GetOptionsNumRng($this->_tpl_vars['module']->repeat_interval['monthly_freq'], 1, 12); ?>
</select> month</p>
							<p><input type="radio" name="monthly_type" value="3" <?php if ($this->_tpl_vars['module']->event_info['RepeatType'] == 3): ?>checked<?php endif; ?> />On the <select name="monthly_daynum"><?php echo GetOptionsNumRng($this->_tpl_vars['module']->repeat_interval['monthly_day'], 1, 31, true); ?>
</select></p> 
							<p><input type="radio" name="monthly_type" value="4" <?php if ($this->_tpl_vars['module']->event_info['RepeatType'] == 4): ?>checked<?php endif; ?> />On the <select name="monthly_pos">
								<option />
								<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['module']->pos_locs,'selected' => $this->_tpl_vars['module']->repeat_interval['monthly_pos']), $this);?>

							</select>
							<select name="monthly_pos_type">
								<option />
								<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['module']->pos_types,'selected' => $this->_tpl_vars['module']->repeat_interval['monthly_pos_type']), $this);?>

							</select></p>
						</div>
						<div id="repeat_5">
							<p>Every <select name="yearly_freq"><?php echo GetOptionsNumRng($this->_tpl_vars['module']->repeat_interval['yearly_freq'], 1, 4); ?>
</select> year(s)</p>
							<p><input type="radio" name="yearly_type" value="5" <?php if ($this->_tpl_vars['module']->event_info['RepeatType'] == 5): ?>checked<?php endif; ?> />On <select name="yearly_monthnum"><option value="" /><?php echo GetOptionsMonths($this->_tpl_vars['module']->repeat_interval['yearly_month']); ?>
</select> <select name="yearly_daynum"><?php echo GetOptionsNumRng($this->_tpl_vars['module']->repeat_interval['yearly_month_day'], 1, 31, true); ?>
</select></p>
							<p><input type="radio" name="yearly_type" value="6" <?php if ($this->_tpl_vars['module']->event_info['RepeatType'] == 6): ?>checked<?php endif; ?> />On the <select name="yearly_pos">
								<option />
								<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['module']->pos_locs,'selected' => $this->_tpl_vars['module']->repeat_interval['yearly_pos']), $this);?>

							</select> <select name="yearly_pos_type">
								<option />
								<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['module']->pos_types,'selected' => $this->_tpl_vars['module']->repeat_interval['yearly_pos_type']), $this);?>

							</select> of <select name="yearly_pos_month"><option value="" /><?php echo GetOptionsMonths($this->_tpl_vars['module']->repeat_interval['yearly_pos_month']); ?>
</select></p>
						</div>
						<div class="form_error" id="err_RepeatFreq"></div>
					</fieldset>
					<fieldset id="repeat_span" class="hidden" style="border:1px solid #E6E6E6;">
						<legend>Repeat Until</legend>
						<input type="radio" name="repeat_enddate_type" value="0" <?php if (! $this->_tpl_vars['module']->event_info['RepeatEndDateUTC']): ?>checked<?php endif; ?> />No end date<br />
						<input type="radio" name="repeat_enddate_type" value="1" <?php if ($this->_tpl_vars['module']->event_info['RepeatEndDateUTC']): ?>checked<?php endif; ?> />End by <input type="text" name="repeat_enddate" class="datepicker" size="6" value="<?php if ($this->_tpl_vars['module']->event_info['RepeatEndDateUTC']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['module']->event_info['RepeatEndTimeStamp'])) ? $this->_run_mod_handler('FormatDate', true, $_tmp, 'date', 1) : FormatDate($_tmp, 'date', 1)); ?>
<?php endif; ?>" />
						<div class="form_error" id="err_RepeatUntil"></div>
					</fieldset>
				</td>
			</tr>
			<?php endif; ?>
			<?php if (permitted_people): ?>
			<tr>
				<td colspan="2">
					<span class="acc_con_label">Primary Attendee</span><br />
					<select name="creator" onChange="RemoveAttendee(this.value);UpdateAttendeeSelect();">
					<?php $_from = $this->_tpl_vars['module']->permitted_people; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['pp_id'] => $this->_tpl_vars['pp_name']):
?>
						
						<option value="<?php echo $this->_tpl_vars['pp_id']; ?>
" <?php if ($this->_tpl_vars['pp_id'] == $this->_tpl_vars['module']->creator): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['pp_name']; ?>
</option>
					<?php endforeach; endif; unset($_from); ?>
					</select>
				</td>
			</tr>
			<?php endif; ?>
			<tr id="attendee_section">
				<td colspan="2">
					<span class="acc_con_label">Other Attendees</span><br />
					<div id="add_attendee_div">
						<select name="add_attendee" id="add_attendee"></select>
						<input type="button" value="Add" onclick="AddAttendee();">
						<input type="hidden" name="attendee_list" />
					</div>
					<div id="attendees">
					<?php $_from = $this->_tpl_vars['module']->attendees; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['attendee']):
?>
						<?php if (! ( permitted_people && $this->_tpl_vars['attendee']['Creator'] )): ?>
							<div id="attendee_<?php echo $this->_tpl_vars['attendee']['PersonID']; ?>
"><?php echo $this->_tpl_vars['attendee']['name']; ?>
 <?php if ($this->_tpl_vars['attendee']['Creator']): ?>(Creator)<?php else: ?>&nbsp;&nbsp;&nbsp;<a style="font-weight:bold;color:red;" onClick="RemoveAttendee(<?php echo $this->_tpl_vars['attendee']['PersonID']; ?>
)">X</a><?php endif; ?></div>
						<?php endif; ?>
					<?php endforeach; endif; unset($_from); ?>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<span class="acc_con_label" id="label_event_private">Private</span><br />
					<input name="CheckPrivate" id="eventCheckPrivate" type="checkbox" value="true" <?php if ($this->_tpl_vars['module']->event_info['Private'] == 1): ?>checked<?php endif; ?> />
					<div class="form_error" id="err_CheckPrivate"></div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<span class="acc_con_label">Note</span><br />
					<textarea name="Note" class="full" cols="35" rows="8" id="Notes" ><?php echo $this->_tpl_vars['module']->event_info['Notes']; ?>
</textarea>				
					<div class="form_error" id="err_Notes"></div>
				</td>
			</tr>
		</table>
	</div>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "snippet_event_alert.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<div id="event_bottom" class="clearing"></div>
</div>


<?php echo '
<script language="javascript">
var people_list;
var type_list;
var webpath = \''; ?>
<?php echo @WEB_PATH; ?>
<?php echo '\';
var prev_start = \'\';

$(document).ready(function() {

	var sPath = window.location.pathname;
	var sPage = sPath.substring(sPath.lastIndexOf(\'/\') + 1);

	if(sPage == \'edit_opp2.php\'){
		$("#event_alert_wrapper").hide();
	}


	try {
		showNoteAlertHelp();
	}
	catch(err) {}

	$("#expand_event_alert").click(function(){

		$("#add_event_alert").slideToggle();

		if($("#eventCheckPrivate").attr(\'disabled\')){
			$("#eventCheckPrivate").removeAttr(\'disabled\');
			$("#label_event_private").css(\'color\', \'#5C80A5\');
    		$(this).attr(\'value\', \'Add Alert\');
    		$.scrollTo("#overlay", {duration : 2000});
    		$("#add_event_alert_hidden").attr(\'value\', \'\');
		}
        else {
        	$.scrollTo("#event_bottom", {duration : 1000});
        	$("#eventCheckPrivate").attr(\'disabled\', \'disabled\');
			$("#label_event_private").css(\'color\', \'#cacaca\');
    		$(this).attr(\'value\', \'Remove Alert\');
    		$("#add_event_alert_hidden").attr(\'value\', \'1\');
        }
		
	});

	$("#show_event_detail").click(function(){
		$(".event_form").slideToggle();
	});
	
	$(".event_level").click(function(){
		
		id = ($(this).attr(\'id\'));
		
		if($("#level_event_checked").attr(\'value\') != 1){
			$(\'.\'+id).attr(\'checked\', true);
			$("#level_event_checked").attr(\'value\', 1);
		}
		else {
			$(\'.\'+id).removeAttr(\'checked\');
			$("#level_event_checked").attr(\'value\', 0);
		}
	});

	$("#select_event_level").click(function(){
		$("#level_event_msg").show();
		$(".header").fadeOut();
		$(".header").fadeIn();
		$(".header").fadeOut();
		$(".header").fadeIn();
	});

	$("#delay_event_until_day").datepicker({
		showOn: "button",
		buttonImage: "images/calendar.gif",
		buttonImageOnly: true,
		disabled: true
	});
	
	$("#select_event_all").click(function(){
		toggleEventAllSelected();
	});

	$("#select_event_assigned").click(function(){
		toggleEventAssignedSelected();
	});

	$("#send_event_immediately_no").click(function(){
		$(".ui-datepicker-trigger").attr(\'title\', \'Select a Date\');
		$(".ui-datepicker-trigger").show();
		$("#delay_event_until_day" ).datepicker(\'enable\');
		$("#delay_event_until_day" ).attr(\'value\',\'\');
		$("#event_hours").removeAttr(\'disabled\');
		$("#event_minutes").removeAttr(\'disabled\');
		$("#event_periods").removeAttr(\'disabled\');
	});

	
	$("#send_event_immediately_yes").click(function(){
		$("#delay_event_until_day").attr(\'value\', \'\');
		$("#delay_event_until_day").datepicker(\'disable\');
		$("#event_hours").attr(\'disabled\', \'disabled\');
		$("#event_minutes").attr(\'disabled\', \'disabled\');
		$("#event_periods").attr(\'disabled\', \'disabled\');
		$("#spn_event_delay_until_error").text(\'\');
		
	});
	
	$("#EventType").change(function(){
		$("#err_EventType").hide();
	});

	$("#standard_event_alert").change(function(){
		$("#err_Delivery").hide();
	});

	$("#email_event_alert").change(function(){
		$("#err_Delivery").hide();
	});

	$("#delay_event_until_day").change(function(){
		$("#err_Delay").hide();
	});


	
	var target = 0;
	'; ?>

	people_list = <?php echo json_encode($this->_tpl_vars['module']->GetPermittedPeople()); ?>
;
	type_list = <?php echo json_encode($this->_tpl_vars['module']->GetEventTypes()); ?>
;
	<?php echo '

	var start_date = $("input[name=\'StartDate\']").val();
	if (start_date != \'\') {
		prev_start = $.datepicker.parseDate(\'m/d/y\', start_date);
	}

	try {
		// Get the selected user from edit-opportunity 
		target = ssSalesPerson.getSelectedVal();				
	}
	catch(err){}
			
	$("#eventMiniCalendar").load("'; ?>
<?php echo @WEB_PATH; ?>
<?php echo '/ajax/ajax.EventMiniCalendar.php?target=" + target);

	DoChangeRepeat($("select[name=\'Repeat\']").val());
	RemoveAttendee($("select[name=\'creator\']").val());
	UpdateAttendeeSelect();


	
	$("input[name=\'StartDate\']").datepicker({
		showOn :"both",
		buttonImage : webpath + "/images/calendar.gif",
		buttonImageOnly :true,
		dateFormat: \'m/d/y\',
		onSelect: function(dateText, inst) {
			var start_date = $(this);
			var start_parsed = $.datepicker.parseDate(\'m/d/y\', start_date.val());

			
			var end_date = $("input[name=\'EndDate\']");
			var repeat_end = $("input[name=\'RepeatEndDate\']");
			if (end_date.val() == \'\') {
				end_date.val(start_date.val());
			} else if (prev_start != \'\') {
				var end_parsed = $.datepicker.parseDate(\'m/d/y\', end_date.val());
				var new_end = new Date();
				new_end.setTime(end_parsed.getTime() + start_parsed.getTime() - prev_start.getTime() + 3600000);
				end_date.val($.datepicker.formatDate(\'m/d/y\', new_end));
			} 
			if (repeat_end.val() != undefined && start_parsed > $.datepicker.parseDate(\'m/d/y\', repeat_end.val())) {
				repeat_end.val(start_date.val());
			}
			prev_start = $.datepicker.parseDate(\'m/d/y\', start_date.val());
			end_date.datepicker(\'option\', \'minDate\', start_parsed);
			repeat_end.datepicker(\'option\', \'minDate\', start_parsed);
		}

	});

	$("input[name=\'EndDate\']").datepicker({
		showOn :"both",
		buttonImage : webpath + "/images/calendar.gif",
		buttonImageOnly :true,
		dateFormat: \'m/d/y\'
	});

	$("input[name=\'repeat_enddate\']").datepicker({
		showOn :"both",
		buttonImage : webpath + "/images/calendar.gif",
		buttonImageOnly :true,
		dateFormat: \'m/d/y\'
	});

	$(".ui-datepicker-trigger").attr(\'title\', \'Select a Date\');
	/*
	$(".ui-datepicker-trigger").css( "width", "24px");
	$(".ui-datepicker-trigger").css( "height", "23px");
	*/
	if ($("select[name=\'EventType\']").val() != \'\') {
		DoChangeType($("select[name=\'EventType\']").val());
	}
	
	$("#delay_event_until_day" ).datepicker(\'disable\');

});

function toggleEventAllSelected(){
	
	var current_state = $("#all_event_checked").attr(\'value\');

	if(current_state != 1){
		$(".all_users").attr(\'checked\', true);
		$("#all_event_checked").attr(\'value\', 1);
	}
	else {
		resetEventSelected();
	}
}

function toggleEventAssignedSelected(){

	var current_state = $("#assigned_event_checked").attr(\'value\');
	
	if(current_state != 1){
		$(".assigned_users").attr(\'checked\', true);
		$("#assigned_event_checked").attr(\'value\', 1);
	}
	else {
		resetEventSelected();	
	}
}



function resetEventSelected(){
	
	$(".all_users").removeAttr(\'checked\');
	$("#all_event_checked").attr(\'value\', 0);
	
	$(".assigned_users").removeAttr(\'checked\');
	$("#assigned_event_checked").attr(\'value\', 0);
}




function AddAttendee() {
	var to_add = $("select[name=\'add_attendee\']").find(\':selected\').val();
	if (to_add != "" && to_add != "All") {
		$("#attendees").append(\'<div id="attendee_\' + to_add + \'">\' + people_list[to_add] + \'&nbsp;&nbsp;&nbsp;<a style="font-weight:bold;color:red;" onClick="RemoveAttendee(\' + to_add + \')">X</a></div>\');
		UpdateAttendeeSelect();
	}else if (to_add == "All") {
		$("#add_attendee option:not(option:first, option:last)").each(function(){
		$("#attendees").append(\'<div id="attendee_\' + $(this).val() + \'">\' + people_list[$(this).val()] + \'&nbsp;&nbsp;&nbsp;<a style="font-weight:bold;color:red;" onClick="RemoveAttendee(\' + $(this).val()+ \')">X</a></div>\');		
		});	
		UpdateAttendeeSelect();
	}
}

function RemoveAttendee(attendee_id) {
	$("#attendee_" + attendee_id).remove();
	UpdateAttendeeSelect();
}

function UpdateAttendeeSelect() {
	var attendee_select = $("select[name=\'add_attendee\']");
	attendee_select.children().remove();
	attendee_select.append(new Option(\'\', \'\', true));

	// Get the list of the current attendees
	current_array = new Array();
	current_count = 0;
	if ($("select[name=\'creator\']")) {
		current_array[current_count] = $("select[name=\'creator\']").val();
		current_count++;
	}
	$("div[id^=\'attendee_\']").each(function() {
		current_array[current_count] = this.id.substring(9);
		current_count++;
	});

	$("input[name=\'attendee_list\']").val(JSON.stringify(current_array));
	
	for (var person in people_list) {
		var found = false;
		for(var i = 0; i < current_array.length; i++) {
			if(current_array[i] == person) {
				var found = true;
			}
		}
		if (found) continue;
		if(person > 0)
		attendee_select.append("<option value=\'" + person + "\'>" + people_list[person] + "</option>");
	}
	attendee_select.append("<option value=\'All\'>" + "All" + "</option>");
	
}

function DoChangeRepeat(value) {
	$(\'#repeat_choices div\').hide();
	$(\'#event_repeat fieldset\').hide();
	if (value > 0) {
		$(\'#event_repeat fieldset\').show();
		var field = \'repeat_\' + value;
		$(\'#\' + field).show();
	}
}

function DoChangeType(value) {
	if (type_list[value][\'Type\'] == \'TASK\') {
		$(\'#event_datetime\').hide();
		$(\'#event_repeat\').hide();
		$(\'#attendee_section\').hide();
		$("div[id^=\'attendee_\']").remove();
		UpdateAttendeeSelect();
		$("#label_priority").show();
		$("#priority").show();
	}
	else {
		$(\'#event_datetime\').show();
		$(\'#event_repeat\').show();
		$(\'#attendee_section\').show();
		$("#label_priority").hide();
		$("#priority").hide();
	}
}

</script>

'; ?>

