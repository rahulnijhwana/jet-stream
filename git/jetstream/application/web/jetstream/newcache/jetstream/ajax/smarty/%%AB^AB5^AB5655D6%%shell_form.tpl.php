<?php /* Smarty version 2.6.18, created on 2015-02-10 05:15:44
         compiled from shell_form.tpl */ ?>
<div id="<?php echo $this->_tpl_vars['module']->form_id; ?>
" style="display:none;">
	<form method="post" onSubmit="return SubmitForm(this)" action="<?php echo @WEB_PATH; ?>
/ajax/<?php echo $this->_tpl_vars['module']->submit_to; ?>
?SN=<?php echo $_COOKIE['SN']; ?>
" style="padding:0;margin:0;">
		<div style="text-align:center;">Fields marked with <span class="form_required">*</span> are mandatory.</div>
		<?php echo $this->_tpl_vars['module']->DrawForm(); ?>

		<?php $_from = $this->_tpl_vars['module']->template_files; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['template_file']):
?>
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['template_file'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<?php endforeach; endif; unset($_from); ?>
		<!--<div style="text-align:center;">
			<input type="submit" value="Save" />
			<input type="button" value="Cancel" onclick="javascript:CancelForm(this.form);" />
		</div>-->
		<div style="clear:both;"></div>
		<div id="divTopRight" class="float_save"  style="position:absolute;">
			<div class="float_save_banner" style="z-index:5000;">
				<div style="text-align:center;">
					<input type="submit" value="Save" class="float_save_save" />	
					<input type="button" value="Cancel" class="float_save_save" onclick="javascript:CancelForm(this.form);" />					
				</div>
			</div>
		</div>
		
	</form>
</div>
<script type="text/javascript">
	<?php echo '
	var ns = (navigator.appName.indexOf("Netscape") != -1);
	var d = document;
	var px = document.layers ? "" : "px";

	function JSFX_FloatDiv(id, sx, sy)
	{
		var el=d.getElementById?d.getElementById(id):d.all?d.all[id]:d.layers[id];
		var px = document.layers ? "" : "px";
		window[id + "_obj"] = el;
		if(d.layers)el.style=el;
		el.cx = el.sx = sx;el.cy = el.sy = sy;
		el.sP=function(x,y){this.style.left=x+px;this.style.top=y+px;};

		el.floatIt=function()
		{
			var pX, pY;
			pX = (this.sx >= 0) ? 0 : ns ? innerWidth : 
			document.documentElement && document.documentElement.clientWidth ? 
			document.documentElement.clientWidth : document.body.clientWidth;
			pY = ns ? pageYOffset : document.documentElement && document.documentElement.scrollTop ? 
			document.documentElement.scrollTop : document.body.scrollTop;
			if(this.sy<0) 
			pY += ns ? innerHeight : document.documentElement && document.documentElement.clientHeight ? 
			document.documentElement.clientHeight : document.body.clientHeight;
			this.cx += (pX + this.sx - this.cx)/8;this.cy += (pY + this.sy - this.cy)/8;
			this.sP(this.cx, this.cy);
			setTimeout(this.id + "_obj.floatIt()", 40);
		}
		return el;
	}

	$(document).ready(function() {
		var right_pos = ($.browser.msie) ? 796 : 820;
		JSFX_FloatDiv("divTopRight", right_pos, 10).floatIt();
	});
	'; ?>

</script>