<?php /* Smarty version 2.6.18, created on 2015-02-09 12:40:14
         compiled from snippet_mini_calendar.tpl */ ?>
<div class="toolTipDiv" id="EventTypeLegend" style="display:none;">
	<?php echo $this->_tpl_vars['module']->toolTip; ?>

</div>

<div class="minical_title_container">
	<div class="minical_title">
	
		<a href="javascript:getMonth('prev'<?php if ($this->_tpl_vars['module']->target != ''): ?>,<?php echo $this->_tpl_vars['module']->target; ?>
<?php endif; ?>);"><img class="previousMonth" src="<?php echo @WEB_PATH; ?>
/images/left-arrow-blue.png" /></a>
        <?php if ($this->_tpl_vars['module']->eventMiniCalendar): ?>
            <a style="margin:30px;" href="#"><?php echo $this->_tpl_vars['module']->month; ?>
 <?php echo $this->_tpl_vars['module']->year; ?>
</a>
        <?php else: ?>
            <a style="margin:30px;" href="<?php echo @WEB_PATH; ?>
/slipstream.php?action=monthly&day=<?php echo $this->_tpl_vars['module']->month_int; ?>
/1/<?php echo $this->_tpl_vars['module']->year; ?>
<?php if ($this->_tpl_vars['module']->target != ''): ?>&target=<?php echo $this->_tpl_vars['module']->target; ?>
<?php endif; ?>"><?php echo $this->_tpl_vars['module']->month; ?>
 <?php echo $this->_tpl_vars['module']->year; ?>
</a>
        <?php endif; ?>
		<a href="javascript:getMonth('next'<?php if ($this->_tpl_vars['module']->target != ''): ?>,<?php echo $this->_tpl_vars['module']->target; ?>
<?php endif; ?>);"><img class="nextMonth" src="<?php echo @WEB_PATH; ?>
/images/right-arrow-blue.png" /></a>
	</div>
</div>

<table class="minicalendar">
		<tr>
		<th>&nbsp;</th><th>Su</th><th>Mo</th><th>Tu</th><th>We</th><th>Th</th><th>Fr</th><th>Sa</th>
<?php $this->assign('count', 7); ?>

<?php $_from = $this->_tpl_vars['module']->days; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['day'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['day']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key_outer'] => $this->_tpl_vars['day']):
        $this->_foreach['day']['iteration']++;
?>
	<?php if ($this->_tpl_vars['count'] == 7): ?>
		<?php $this->assign('count', 0); ?>
	</tr>
	<tr>
		<th>			
			<?php if ($this->_tpl_vars['module']->eventMiniCalendar): ?>
				W<?php echo $this->_tpl_vars['day']['week']; ?>

			<?php else: ?>
			<a href="slipstream.php?action=weekly&day=<?php echo $this->_tpl_vars['day']['date']; ?>
<?php if ($this->_tpl_vars['module']->target): ?>&target=<?php echo $this->_tpl_vars['module']->target; ?>
<?php endif; ?>">
				W<?php echo $this->_tpl_vars['day']['week']; ?>

			</a>
			<?php endif; ?>
		</th>
	<?php endif; ?>
		<td class="<?php if ($this->_tpl_vars['day']['type'] != 'current'): ?>othermonth<?php endif; ?> <?php if ($this->_tpl_vars['day']['today']): ?>today<?php endif; ?>" <?php if (! $this->_tpl_vars['module']->eventMiniCalendar): ?>onclick="location.href='<?php echo @WEB_PATH; ?>
/slipstream.php?action=daily&day=<?php echo $this->_tpl_vars['day']['date']; ?>
'"<?php endif; ?>>
			<div class="miniday"><?php echo $this->_tpl_vars['day']['day']; ?>
</div>			
			<?php $_from = $this->_tpl_vars['day']['events']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['event']):
?>				
				<div id="<?php echo @WEB_PATH; ?>
/ajax/ajax.EventTooltip.php?ref=<?php echo $this->_tpl_vars['event']['id_enc']; ?>
" class="<?php if ($this->_tpl_vars['event']['pending']): ?>minipendingevent<?php else: ?>minievent<?php endif; ?> preview" style="background-color:#<?php echo $this->_tpl_vars['event']['eventtype']['EventColor']; ?>
;<?php if ($this->_tpl_vars['event']['is_edit_functional']): ?>cursor:pointer;<?php else: ?>cursor:default;<?php endif; ?>" <?php if ($this->_tpl_vars['event']['is_edit_functional'] && ! $this->_tpl_vars['module']->eventMiniCalendar): ?><?php if ($this->_tpl_vars['event']['RepeatParent']): ?>onclick='javascript:NoBubble(event);LoadRptngEvnt(<?php echo $this->_tpl_vars['event']['RepeatParent']; ?>
, "<?php echo $this->_tpl_vars['event']['id_enc']; ?>
")'<?php else: ?>onclick="javascript:NoBubble(event, '<?php echo @WEB_PATH; ?>
/slipstream.php?action=event&eventId=<?php echo $this->_tpl_vars['event']['EventID']; ?>
')"<?php endif; ?><?php endif; ?>></div>
			<?php endforeach; endif; unset($_from); ?>
		</td>
		<?php $this->assign('count', $this->_tpl_vars['count']+1); ?>		
<?php endforeach; endif; unset($_from); ?>
	</tr>
</table>
<script language="javascript">
<?php echo '
	$(document).ready(function() {
		ResetCalTooltips();
		$(\'#event_legend\').cluetip({
			attribute: \'href\',
			activation: \'click\',
			width: 272,
			leftOffset: 180,
			sticky: true,
			closePosition: \'title\',
			closeText: \'<img src="images/img_close.png" style="width: 16px; height: 16px" alt="close" />\',
			arrows: false
		});
	});
		var webpath = \''; ?>
<?php echo @WEB_PATH; ?>
<?php echo '\';
'; ?>

</script>