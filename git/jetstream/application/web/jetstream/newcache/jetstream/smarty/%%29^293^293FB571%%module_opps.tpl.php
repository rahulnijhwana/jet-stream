<?php /* Smarty version 2.6.18, created on 2015-02-09 18:10:17
         compiled from module_opps.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'SplitLongWords', 'module_opps.tpl', 14, false),array('modifier', 'trim', 'module_opps.tpl', 30, false),)), $this); ?>
<div class="module_container">

<?php if ($this->_tpl_vars['module']->opps): ?>
	<?php $_from = $this->_tpl_vars['module']->opps; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
	<div class="opp" style="background-color:<?php echo $this->_tpl_vars['item']['Color']; ?>
">
		<div class="caption">
			Salesperson : <?php echo $this->_tpl_vars['item']['Salesperson']; ?>

		</div>

		<?php if (( $this->_tpl_vars['module']->type == 'Active' )): ?> 
		
			<div class="info" <?php if ($this->_tpl_vars['item']['Edit']): ?>style="cursor:pointer" onclick="javascript:openOppPopup(<?php echo $this->_tpl_vars['item']['DealID']; ?>
, <?php echo $this->_tpl_vars['item']['PersonID']; ?>
);"<?php endif; ?>>				
				Location: <?php echo $this->_tpl_vars['item']['Category']; ?>
<br/>				
				Company: <a onclick="javascript:showPopup=false;" href="?action=company&accountId=<?php echo $this->_tpl_vars['item']['CompanyID']; ?>
"><?php echo SplitLongWords($this->_tpl_vars['item']['Company']); ?>
</a><br/>
				<?php if ($this->_tpl_vars['item']['Contact'] != ' '): ?>
				Contact: <a onclick="javascript:showPopup=false;" href="?action=contact&contactId=<?php echo $this->_tpl_vars['item']['ContactID']; ?>
"><?php echo SplitLongWords($this->_tpl_vars['item']['Contact']); ?>
</a><br/>
				<?php endif; ?>
				<?php if ($this->_tpl_vars['item']['Offerings'] != ""): ?>
				Offerings: <?php echo SplitLongWords($this->_tpl_vars['item']['Offerings']); ?>
<br/>
				<?php endif; ?>				
				<?php if ($this->_tpl_vars['item']['MeetingLabel'] != ""): ?>
				<?php echo $this->_tpl_vars['item']['MeetingLabel']; ?>
: <?php echo $this->_tpl_vars['item']['NextMeeting']; ?>
<br/>
				<?php endif; ?>
				
			</div>	
		<?php else: ?>
			<div class="info" <?php if ($this->_tpl_vars['item']['Edit']): ?>style="cursor:pointer" onclick="javascript:openOppPopup(<?php echo $this->_tpl_vars['item']['DealID']; ?>
, <?php echo $this->_tpl_vars['item']['PersonID']; ?>
);"<?php endif; ?>>
				Closed: <?php echo $this->_tpl_vars['item']['ClosedDate']; ?>
<br/>				
				Company: <a onclick="javascript:showPopup=false;" href="?action=company&accountId=<?php echo $this->_tpl_vars['item']['CompanyID']; ?>
"><?php echo SplitLongWords($this->_tpl_vars['item']['Company']); ?>
</a><br/>
				<?php if (((is_array($_tmp=$this->_tpl_vars['item']['Contact'])) ? $this->_run_mod_handler('trim', true, $_tmp) : trim($_tmp)) != ""): ?>
				Contact: <a onclick="javascript:showPopup=false;" href="?action=contact&contactId=<?php echo $this->_tpl_vars['item']['ContactID']; ?>
"><?php echo SplitLongWords($this->_tpl_vars['item']['Contact']); ?>
</a><br/>
				<?php endif; ?>
				Offerings: <?php echo SplitLongWords($this->_tpl_vars['item']['Offerings']); ?>
<br/>
				Total $: <?php echo $this->_tpl_vars['item']['Total']; ?>

			</div>
		<?php endif; ?>	
		
		
	</div>	
	 <?php endforeach; endif; unset($_from); ?>
<?php else: ?>
	<div class="BlackBold">
		<?php echo $this->_tpl_vars['module']->msg; ?>

	</div>
<?php endif; ?>	
</div>
<script language="javascript" type="text/javascript">
	<?php echo '
	var showPopup = true;
	//var showPopup = false;
	function openOppPopup(dealId, personId) {
		if(showPopup) {
			var win = Windowing.openSizedWindow(\'legacy/shared/edit_opp2.php?SN='; ?>
<?php echo $_COOKIE['SN']; ?>
<?php echo '&detailview=1&reqOpId=\' + dealId + \'&reqPersonId=\' + personId + \'#\' + dealId + \'_\' + personId, 630, 1000);
		}		
	}
	
	function addOpp(dealId, personId, acc, contact) {
			var account = acc.replace("&","<-and->" );
			var win = Windowing.openSizedWindow(\'legacy/shared/edit_opp2.php?SN='; ?>
<?php echo $_COOKIE['SN']; ?>
<?php echo '&detailview=1\' + \'&account=\' + account + \'&contact=\' + contact + \'&reqOpId=\' + dealId + \'&reqPersonId=\' + personId + \'#\' + dealId + \'_\' + personId , 630, 1000);
	}	
	'; ?>

</script>