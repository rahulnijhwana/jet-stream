<?php /* Smarty version 2.6.18, created on 2015-02-09 18:10:07
         compiled from module_simple_search.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'module_simple_search.tpl', 6, false),)), $this); ?>
<div id="dashboard">
	<form onsubmit="javascript:AjaxLoadPage(1);return false;" id="SlipstreamSearch" action="#" method="post">
		<div id="search">
			<div id="search_div">
				<input type="text" name="SearchString" id="SearchString" size="32" value="<?php if ($this->_tpl_vars['module']->search['search_string']): ?><?php echo $this->_tpl_vars['module']->search['search_string']; ?>
<?php endif; ?>" /> <input type="submit" name="SearchButton" value="Search" />
				in <select onchange="javascript:AjaxLoadPage(1);" name="SearchType" id="SearchType"><?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['module']->search_types,'selected' => $this->_tpl_vars['module']->search['query_type']), $this);?>
</select>
			</div>
			<div id="per_page">
				Show <select onchange="javascript:AjaxLoadPage(1);"  name="RecordsPerPage" id="RecordsPerPage" ><?php echo smarty_function_html_options(array('output' => $this->_tpl_vars['module']->records_per_page,'values' => $this->_tpl_vars['module']->records_per_page,'selected' => $this->_tpl_vars['module']->search['records_per_page']), $this);?>
</select>
			</div>
			<div id="search_filters">
				<div id="search_filters_checkboxes">
					<?php if ($this->_tpl_vars['module']->suppress): ?>
						<div style="display:none"><input type="checkbox" name="AssignedSearch" id="AssignedSearch" value="1" checked="checked" /></div>
					<?php else: ?>
						<p><input type="checkbox" name="AssignedSearch" id="AssignedSearch" value="1" onchange="javascript:AjaxLoadPage(1);" <?php if ($this->_tpl_vars['module']->search['assigned_search']): ?>checked<?php endif; ?> /><label for="AssignedSearch">Assigned to me</label></p>
					<?php endif; ?>
					<p>
						<input type="checkbox" id="selectInactive" name="selectInactive" value="1" onclick="javascript:AjaxLoadPage(1);" <?php if ($this->_tpl_vars['module']->search['inactive_type']): ?>checked<?php endif; ?> />
						<label for="selectInactive">Include inactive</label>
					</p>
				</div>
				<div id="merge">
					<?php if ($_SESSION['USER']['LEVEL'] != 1): ?><p><input id="btnMerge" type="button" value="Merge Records" onclick="javascript:doMerge();" style="display:none;"/><label for="chkMergeRecord">Merge</label><input type="checkbox" id="chkMergeRecord" onclick="javascript:toggleMergeButton(this.checked)"/></p><?php endif; ?>
				</div>
				<div style="clear: both"></div>
			</div>
		</div>
		<div style="clear: both"></div>
	</form>
	<div id="dashboardsearch">
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "snippet_dashboard_search_list.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	</div>
</div>