<?php /* Smarty version 2.6.18, created on 2015-02-09 18:10:16
         compiled from module_company_contact_new.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'regex_replace', 'module_company_contact_new.tpl', 52, false),array('modifier', 'default', 'module_company_contact_new.tpl', 54, false),)), $this); ?>
<div id='contactView'>
	<?php $_from = $this->_tpl_vars['module']->layout; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['section_id'] => $this->_tpl_vars['section']):
?>
	<?php if ($this->_tpl_vars['section']['SectionName'] != 'DefaultSection'): ?>
	<div class="section_head" onclick="javascript:ToggleHidden('section<?php echo $this->_tpl_vars['section_id']; ?>
');">
		<img id="toggle_section<?php echo $this->_tpl_vars['section_id']; ?>
" src="images/plus.gif"/> <?php echo $this->_tpl_vars['section']['SectionName']; ?>

	</div>
	<?php endif; ?>
	<div class="jet_datagrid <?php if ($this->_tpl_vars['section']['SectionName'] != 'DefaultSection'): ?>hidden<?php endif; ?>" id="section<?php echo $this->_tpl_vars['section_id']; ?>
">
		<table style="width: 100%">
			<?php if ($this->_tpl_vars['section']['SectionName'] == 'DefaultSection'): ?>
			<?php if ($this->_tpl_vars['module']->created): ?>
				<tr>
					<td class="label">&nbsp;</td>
					<td>&nbsp;</td>
					<td class="label">Original Creation Date</td>
					<td>
						<?php echo $this->_tpl_vars['module']->created; ?>

					</td>
				</tr> 
			<?php endif; ?>
			<tr>
				<?php if ($this->_tpl_vars['module']->type == 'contact'): ?>
				<td class="label">Company</td>
				<td class="left">
					<?php if ($this->_tpl_vars['module']->map['account']['name']): ?>
						<a href="?action=company&accountId=<?php echo $this->_tpl_vars['module']->map['account']['id']; ?>
"><?php echo $this->_tpl_vars['module']->map['account']['name']; ?>
</a>
					<?php else: ?>
						&nbsp;
					<?php endif; ?>
				</td>
				<?php else: ?>
				<td class="label">&nbsp;</td>
				<td class="left">&nbsp;</td>
				<?php endif; ?>
				<td class="label">Assigned to</td>
				<td class="right">
					<?php $_from = $this->_tpl_vars['module']->map['security']['assigned']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['person']):
?>
						<?php echo $this->_tpl_vars['person']['FirstName']; ?>
 <?php echo $this->_tpl_vars['person']['LastName']; ?>
<br />
					<?php endforeach; else: ?>
						Nobody
					<?php endif; unset($_from); ?>
				</td>
			</tr>
			<?php endif; ?>
			<?php $_from = $this->_tpl_vars['section']['rows']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['row']):
?>
			<?php if (((is_array($_tmp=$this->_tpl_vars['module']->map['fields'][$this->_tpl_vars['row']['1']]['FieldName'])) ? $this->_run_mod_handler('regex_replace', true, $_tmp, "/[\d]/", "") : smarty_modifier_regex_replace($_tmp, "/[\d]/", "")) == 'LongText'): ?>
				<tr>
					<td class="label"><?php echo ((is_array($_tmp=@$this->_tpl_vars['module']->map['fields'][$this->_tpl_vars['row']['1']]['LabelName'])) ? $this->_run_mod_handler('default', true, $_tmp, "&nbsp;") : smarty_modifier_default($_tmp, "&nbsp;")); ?>
</td>
					<td class="left" colspan="3" style="word-wrap: break-word;"><p><?php echo ((is_array($_tmp=@$this->_tpl_vars['module']->FormatForDisplay($this->_tpl_vars['module']->map['fields'][$this->_tpl_vars['row']['1']]))) ? $this->_run_mod_handler('default', true, $_tmp, "&nbsp;") : smarty_modifier_default($_tmp, "&nbsp;")); ?>
</p></td>
				</tr>			
			<?php else: ?>
				<tr>
					<td class="label"><?php echo ((is_array($_tmp=@$this->_tpl_vars['module']->map['fields'][$this->_tpl_vars['row']['1']]['LabelName'])) ? $this->_run_mod_handler('default', true, $_tmp, "&nbsp;") : smarty_modifier_default($_tmp, "&nbsp;")); ?>
</td>
					<td class="left"><?php echo ((is_array($_tmp=@$this->_tpl_vars['module']->FormatForDisplay($this->_tpl_vars['module']->map['fields'][$this->_tpl_vars['row']['1']]))) ? $this->_run_mod_handler('default', true, $_tmp, "&nbsp;") : smarty_modifier_default($_tmp, "&nbsp;")); ?>
</td>
					<td class="label"><?php echo ((is_array($_tmp=@$this->_tpl_vars['module']->map['fields'][$this->_tpl_vars['row']['0']]['LabelName'])) ? $this->_run_mod_handler('default', true, $_tmp, "&nbsp;") : smarty_modifier_default($_tmp, "&nbsp;")); ?>
</td>
					<td class="right"><?php echo ((is_array($_tmp=@$this->_tpl_vars['module']->FormatForDisplay($this->_tpl_vars['module']->map['fields'][$this->_tpl_vars['row']['0']]))) ? $this->_run_mod_handler('default', true, $_tmp, "&nbsp;") : smarty_modifier_default($_tmp, "&nbsp;")); ?>
</td>
				</tr>
			<?php endif; ?>
			<?php endforeach; endif; unset($_from); ?>
		</table>	
	</div>
	<?php endforeach; endif; unset($_from); ?>
</div>
<?php if ($this->_tpl_vars['module']->type == 'contact'): ?>
	<?php $this->assign('account_id', $this->_tpl_vars['module']->map['account']['id']); ?>
<?php else: ?>
	<?php $this->assign('account_id', $this->_tpl_vars['module']->map['security']['AccountID']); ?>
<?php endif; ?>
	
<?php if (! $this->_tpl_vars['module']->limit_access && ! $this->_tpl_vars['module']->partial && $this->_tpl_vars['account_id']): ?>
	<script type="text/javascript">
		var this_account_id = <?php echo $this->_tpl_vars['account_id']; ?>
;
	</script>
<?php endif; ?>