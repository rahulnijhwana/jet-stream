<?php /* Smarty version 2.6.18, created on 2015-02-09 15:58:10
         compiled from shell_jetstream.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'shell_jetstream.tpl', 49, false),)), $this); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<link rel="shortcut icon" href="/favicon.ico" />

		<title>Jetstream&trade; <?php echo $this->_tpl_vars['page']->title; ?>
</title>
		<?php if ($this->_tpl_vars['page']->core_css): ?>
			<link rel="stylesheet" type="text/css" href="css/<?php echo $this->_tpl_vars['page']->GetImploded('core_css'); ?>
" />
		<?php endif; ?>
		<?php if ($this->_tpl_vars['page']->page_css): ?>
			<link rel="stylesheet" type="text/css" href="css/<?php echo $this->_tpl_vars['page']->GetImploded('page_css'); ?>
" />
		<?php endif; ?>
		<?php if ($this->_tpl_vars['page']->print_css): ?>
			<link rel="stylesheet" type="text/css" media="print" href="css/<?php echo $this->_tpl_vars['page']->GetImploded('print_css'); ?>
" />
		<?php endif; ?>
						<script src="javascript/jquery.js" type="text/javascript"></script>
		
				<?php if ($this->_tpl_vars['page']->core_javascript): ?>
			<script type="text/javascript" src="javascript/<?php echo $this->_tpl_vars['page']->GetImploded('core_javascript'); ?>
"></script>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['page']->page_javascript): ?>
			<script type="text/javascript" src="javascript/<?php echo $this->_tpl_vars['page']->GetImploded('page_javascript'); ?>
"></script>
		<?php endif; ?>
		<?php echo '
			<script type="text/javascript">
			$(document).ready(function(){
				try{
					cssdropdown.startchrome("chromemenu");
				}
				catch(e){
					alert(e);
				}
			});
			</script>
		'; ?>

		
		<script type="text/javascript" src="legacy/javascript/windowing.js"></script>
	</head>
	<body>
		<div id="pagewrapper">
			<!--  <div id="header">
				<div class="info">
										<?php echo ((is_array($_tmp=time())) ? $this->_run_mod_handler('date_format', true, $_tmp, "%A, %B %e, %Y") : smarty_modifier_date_format($_tmp, "%A, %B %e, %Y")); ?>

				</div>
			</div> -->

		<div id="header2" style="z-index:1000;width:100%;background-color:#545556">
			<div class="top_bar" id="top">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="top_bar"> 
					<tr>
						<td ><div style="float:left;padding:3px 0 0 7px;">Welcome, <?php echo $_SESSION['USER']['FIRSTNAME']; ?>
 <?php echo $_SESSION['USER']['LASTNAME']; ?>
 </div></td>
						<td align="center"> <?php echo $_SESSION['company_obj']['HeaderText']; ?>
  </td>
						<td align="right">
							<div id="quick_search_wrapper">
								<form action="?action=dashboard" onSubmit="javascript:return DoQuickSearch();" method="get">
									<input title="Quick Search" name="quicksearch" id="quicksearch" class="quicksearch" />
									<input type="image" src="images/search_button.png" class="quicksearch_button" />
								</form>
							</div>
							<div id="zone">
								<?php if ($_SESSION['USER']['LOCATION']): ?>
									<a id="change_location" title="Click to change your location" href="javascript:void(0);"><?php echo $_SESSION['USER']['LOCATION']; ?>
</a>
								<?php endif; ?>
							</div>
						</td>
					</tr>
				</table>
			</div>

						<a name="top"></a>
						<div class="chromestyle" id="chromemenu">
				<ul>
					<li><a id="firstChild" href='?action=dashboard'>Dashboard</a></li>
					<li><a href='#' rel='add_sub_menu'>Add</a></li>
					<li><a href='?action=daily'>Calendar</a></li>
					<li><a href='?action=notes&load=first'>Notes</a></li>
					<!-- <li><a href='?action=email_hold'>Email Hold</a></li> -->
					<?php if ($this->_tpl_vars['page']->show_pending_emails): ?>
						<li><a id="pending_email" class="pending_email" href='#' rel='email_sub_menu'>Email</a></li>
					<?php else: ?>
						<li><a href='#' rel='email_sub_menu'>Email</a></li>
					<?php endif; ?>
					<?php if ($_SESSION['call_report'] == 1): ?>
						<li><a href='?action=call_report'>Call Report</a></li>
					<?php endif; ?>
					<?php if ($_SESSION['company_obj']['UseOppTracker']): ?>
						<li><a href='?action=opptracker&type=user'>Opp Tracker</a></li>
					<?php endif; ?>
					<?php if ($this->_tpl_vars['page']->show_pending_events): ?>
						<li><a id="pending_event" class="pending_event" href="?action=pending_events">Uncompleted</a></li>	
					<?php endif; ?>
					<?php if ($_SESSION['mpower'] && ! ( $_SESSION['USER']['LEVEL'] == 1 && $_SESSION['USER']['ISSALESPERSON'] == 0 )): ?>
						<li><a href='<?php echo $_SESSION['mpower_link']; ?>
?SN=<?php echo $_COOKIE['SN']; ?>
'><img class="mpower_button" src="images/mpower_button.png" /></a></li>
					<?php endif; ?>
					<?php if ($_SESSION['company_obj']['MyGoalMine']): ?>
						<li><a href="http://www.my-goalmine.com/login.aspx" target="_blank"><img class="my_goalmine_button" src="images/mygoalmine.png" /></a></li>
					<?php endif; ?>
					<li><a href='#' rel='setup_sub_menu'>Tools</a></li>
					<li><a href='logout.php'>Logout</a></li>
				</ul>
			</div>
			<div id="email_sub_menu" class="dropmenudiv">
				<?php if ($this->_tpl_vars['page']->email_privacy_enabled): ?>
					<a href='?action=email_incoming' rel='email_sub_menu'>Incoming</a>
					<a href='?action=email_outgoing' rel='email_sub_menu'>Outgoing</a>
				<?php endif; ?>
				<a href='?action=email_hold' rel='email_sub_menu'>Unassigned</a>
			</div>
			<div id="add_sub_menu" class="dropmenudiv">
				<a href='javascript:ShowAjaxForm("CompanyContactNew&action=add&type=account");' rel='setup_sub_menu'>Company</a>
				<a href='javascript:ShowAjaxForm("CompanyContactNew&action=add&type=contact");' rel='setup_sub_menu' title="If attached to a Company, Add Company first">Contact</a>
			</div>
			<div id="setup_sub_menu" class="dropmenudiv">
				<!-- <a href='?action=template' rel='setup_sub_menu'>Email Template</a> -->
				<a href='?action=permission' rel='setup_sub_menu'>Calendar Permissions</a>
				<?php if ($_SESSION['USER']['IMAP_ENABLED'] == 1): ?>
					<a href='?action=imap_settings' rel='setup_sub_menu'>Email Sync Settings</a>
				<?php endif; ?>
				<?php if ($_SESSION['USER']['LEVEL'] >= 2): ?>
					<a href='?action=vacation' rel='setup_sub_menu'>Vacation/Leave</a>
				<?php endif; ?>
				<?php if ($_SESSION['USER']['ALLOW_UPLOADS'] == 1): ?>
					<a href='?action=dataimport'>Import</a>
				<?php endif; ?>
				<?php if ($_SESSION['company_obj']['Jetfile']): ?>
					<a href='?action=filesearch'>Library</a>
				<?php endif; ?>
				<a href='?action=google_sync'>Google Sync</a>
				<!--<?php if ($this->_tpl_vars['page']->isUseGoogleSync): ?>

				    <?php if ($this->_tpl_vars['page']->hasGoogleToken): ?>
                        <?php if ($this->_tpl_vars['page']->ableToGoogleDisconnect): ?>
				            <a href='#' id='google-disconnect'>Google Disconnect</a>
                        <?php endif; ?>
				    <?php else: ?>
				        <a href='#' id='google-connect'>Google Connect</a>
				    <?php endif; ?>

				<?php endif; ?>-->

			</div>

		</div>
		<div id="buffer"></div>

						<div id="contentwrapper">
				<div id="container">
					<div id="content">
						<div id="subnav">
							<ul id="subnav_items">
							<?php $_from = $this->_tpl_vars['page']->subnav; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['subnav'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['subnav']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['label'] => $this->_tpl_vars['link']):
        $this->_foreach['subnav']['iteration']++;
?>
								<li <?php if (($this->_foreach['subnav']['iteration'] <= 1)): ?>class="first"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['link']; ?>
"><?php echo $this->_tpl_vars['label']; ?>
</a></li>
							<?php endforeach; endif; unset($_from); ?>
							</ul>
						</div>
						<h1 id='main_title' <?php if (! $this->_tpl_vars['page']->content_title): ?>style="display:none;"<?php endif; ?>><?php echo $this->_tpl_vars['page']->content_title; ?>
</h1>
						<h2 id='sub_title' <?php if (! $this->_tpl_vars['page']->content_sub_title): ?>style="display:none;"<?php endif; ?>><?php echo $this->_tpl_vars['page']->content_sub_title; ?>
</h2>
						<div id="content_modules">
														<?php $_from = $this->_tpl_vars['page']->content_html; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['content']):
?>
								<?php echo $this->_tpl_vars['content']; ?>

							<?php endforeach; endif; unset($_from); ?>
						</div>
					</div>
				</div>
				<div id="sidebar">
				<div style="margin-bottom:10px;"><img src="images/jetstream_logo_new.jpg" /></div>
					<div id="sidebar_modules">
												<?php $_from = $this->_tpl_vars['page']->sidebar_html; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['sidebar']):
?>
						<?php echo $this->_tpl_vars['sidebar']; ?>

						<?php endforeach; endif; unset($_from); ?>
					</div>
				</div>
				<div class="clearing">&nbsp;</div>
			</div>
		</div>
		<div id="footer">
			Jetstream is a trademark of JetstreamCRM.com
		</div>
		<div id="note_alerts" style="display: none;"></div>
		<div id="confirm_location" style="display: none;"></div>
		<span><input type="hidden" name="first_alert" id="first_alert" value="1" /></span>
	</body>
</html>