<?php /* Smarty version 2.6.18, created on 2015-02-09 18:10:12
         compiled from form_add_note.tpl */ ?>
<div id="addNote" style="display:none;">
	<form method="post" id="addNewNote" name="addNewNote">	
	<fieldset style="border: 1px solid #fff;" align="center">
	<legend><span class="title">Add Note</span></legend>

	<table border="0" cellpadding='5' cellspacing='0' align='center' width="95%" id="tblAddNote">		
		<tr>
			<td width="10%" class="BlackBold" align="right">
				<span class="clsRequire">*</span>&nbsp;Subject
			</td>
			<td colspan="2" width="90%" valign="top" class="inputSection" align="left">
				<input type="text" name="NoteSubject" id="NoteSubject" class="clsTextBox" maxlength="50" />		
				<br/><span id="spn_NoteSubject" class="clsError"></span>
			</td>
		</tr>
		<tr>
			<?php if ($this->_tpl_vars['module']->has_template == 1): ?>
				<td width='100%' colspan="3" valign='top' id="noteTemplateInnerTd">
				<?php echo '
				<script type="text/javascript">						
					createNoteTemplate('; ?>
<?php echo $this->_tpl_vars['module']->note_template_fields; ?>
<?php echo ');
				</script>
				'; ?>

					<br/><span id='spn_NoteText' class='clsError'></span>
				</td>
			<?php else: ?>
				<td width='10%' valign='top' class='BlackBold' align='right' valign="top">
					<span class='clsRequire'>*</span>&nbsp;Text
				</td>
				<td colspan="2" width='90%' class='inputSection' align='left'  valign="top" >
					<textarea name='NoteText' id='NoteText' class='clsTextBox' cols='70' rows="15"></textarea>
					<br/><span id='spn_NoteText' class='clsError'></span>
				</td>
			<?php endif; ?>				
		</tr>
		<tr>
			<td width='10%' class='BlackBold' align='right' valign="top">
				<span id="label_private">Private</span>
			</td>
			<td width='90%' class='smlBlack' align='left' valign="top">				
				<?php if ($this->_tpl_vars['module']->default_private == 1): ?>
					<img src="images/checkbox_checked.png">
				<?php else: ?>
					<input name="CheckPrivate" id="CheckPrivate" type="checkbox" value="true" />
				<?php endif; ?>
			</td>
		</tr>
		<tr <?php if (! $_SESSION['note_alert']): ?>style="display: none"<?php endif; ?>>
			<td width='10%' class='BlackBold' align='right' valign="top">
				<span id="label_note_alert">Note Alert</span>
			</td>
			<td width='90%' class='smlBlack' align='left' valign="top">
				<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "snippet_note_alert.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			</td>
		</tr>
		<tr>
			<td colspan="3" class="smlBlackBold">
				<div id="save_note_buttons">
					<div id="save_button_wrapper">
						<input type="button" class="grayButton" id="btnAddNote" name="btnAddNote" value="Save" />
						<input type="button" class="grayButton" value="Cancel" name="cancel" id="buttonCancel" />
					</div>
					<div id="saving">
						<img src="images/spinner.gif" alt="saving" />
					</div>
				</div>
			</td>
		</tr>
	</table>
	<input type="hidden" id="all_checked" name="all_checked" />
	<input type="hidden" id="assigned_checked" name="assigned_checked" />
	<input type="hidden" id="level_checked" name="level_checked" />
	<input type="hidden" id="sn" name="sn" value="<?php echo $_COOKIE['SN']; ?>
" />
	
	<input type="hidden" id="NoteObjType" name="NoteObjType" value="" />
	<input type="hidden" id="NoteObjVal" name="NoteObjVal" value="" />
	<input type="hidden" id="NoteSplType" name="NoteSplType" value="" />
	</fieldset>
	</form>	
</div>
<div class="clsSucc" id="noteSuccMsg" align="center" width="100%"></div><br/>