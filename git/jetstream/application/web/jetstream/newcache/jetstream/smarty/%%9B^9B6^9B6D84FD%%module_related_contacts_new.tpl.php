<?php /* Smarty version 2.6.18, created on 2015-02-09 18:10:17
         compiled from module_related_contacts_new.tpl */ ?>
<div class="module_container" id="divContactList">
	<?php if ($this->_tpl_vars['module']->contacts): ?>
		<table>
			<tr>
				<td class="label">Name</td>
				<td class="label">Title</td>
			</tr>
			<?php $_from = $this->_tpl_vars['module']->contacts; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['contact']):
?>
				<?php if (! $this->_tpl_vars['contact']['Inactive']): ?>
					<tr>
						<td>						
							<p style="margin: 0px; padding: 0px;">
								<a href="slipstream.php?action=contact&contactId=<?php echo $this->_tpl_vars['contact']['ContactID']; ?>
">
									<?php echo $this->_tpl_vars['contact']['Name']; ?>

								</a>
							</p>
						</td>
						<td>
							<?php echo $this->_tpl_vars['contact']['Title']; ?>

						</td>
					</tr>
				<?php endif; ?>
			<?php endforeach; endif; unset($_from); ?>
		</table>
	<?php else: ?>
		<?php echo $this->_tpl_vars['module']->msg; ?>

	<?php endif; ?>
</div>