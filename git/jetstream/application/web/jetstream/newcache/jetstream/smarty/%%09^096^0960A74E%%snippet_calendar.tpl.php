<?php /* Smarty version 2.6.18, created on 2015-02-09 18:10:07
         compiled from snippet_calendar.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'snippet_calendar.tpl', 12, false),array('modifier', 'count', 'snippet_calendar.tpl', 18, false),array('modifier', 'strtotime', 'snippet_calendar.tpl', 23, false),array('modifier', 'SplitLongWords', 'snippet_calendar.tpl', 80, false),)), $this); ?>
<div>
	<div class="week_cal_header">
		<a href="javascript:GetCalendar('<?php echo $this->_tpl_vars['module']->type; ?>
', '<?php echo $this->_tpl_vars['module']->previous; ?>
', '<?php echo $this->_tpl_vars['module']->selected_sales_person; ?>
'<?php if ($this->_tpl_vars['module']->type == 'daily' && $this->_tpl_vars['module']->merged_view == 1 && $this->_tpl_vars['module']->is_manager): ?>,1<?php endif; ?>)"><img src="images/left-arrow-blue.png"/></a><span class="week_cal_header_text"><?php echo $this->_tpl_vars['module']->display_date; ?>
</span><a href="javascript:GetCalendar('<?php echo $this->_tpl_vars['module']->type; ?>
', '<?php echo $this->_tpl_vars['module']->next; ?>
', '<?php echo $this->_tpl_vars['module']->selected_sales_person; ?>
'<?php if ($this->_tpl_vars['module']->type == 'daily' && $this->_tpl_vars['module']->merged_view == 1 && $this->_tpl_vars['module']->is_manager): ?>,1<?php endif; ?>)"><img src="images/right-arrow-blue.png"/></a> 
	</div>
	
<?php if ($this->_tpl_vars['module']->toggle_view == 'box'): ?>	
	<table class="cal_task">
		<tr>	
	<?php $_from = $this->_tpl_vars['module']->days; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>						
		<?php if ($this->_tpl_vars['module']->type == 'weekly'): ?>
			<td style="width:14.3%;text-align:center;vertical-align:top;font-size:.9em;">
				<a href="javascript:GetCalendar('daily', '<?php echo $this->_tpl_vars['item']['date']; ?>
', '<?php echo $this->_tpl_vars['module']->selected_sales_person; ?>
')"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']['Day'])) ? $this->_run_mod_handler('replace', true, $_tmp, ' ', '<br>') : smarty_modifier_replace($_tmp, ' ', '<br>')); ?>
</a>
		<?php else: ?>
				<td style="text-align:center;">
					<a href="#"><?php echo $this->_tpl_vars['item']['DayName']; ?>
</a>
		<?php endif; ?>
				<?php if (count($this->_tpl_vars['item']['tasks'])): ?>
			<div title="Tasks for <?php echo $this->_tpl_vars['item']['date_string']; ?>
" id="ajax/ajax.GetTask.php?date=<?php echo $this->_tpl_vars['item']['date']; ?>
" class="task_preview" style="border:1px solid #CCDDEE;text-align:center;"> 
				<?php if ($this->_tpl_vars['module']->type == 'weekly'): ?>
					<?php if ($this->_tpl_vars['item']['today']): ?>
						<?php echo $this->_tpl_vars['item']['closed_task_count']; ?>
/<?php echo count($this->_tpl_vars['item']['tasks']); ?>
 Task<?php if (count($this->_tpl_vars['item']['tasks']) > 1): ?>s<?php endif; ?>
					<?php elseif (strtotime($this->_tpl_vars['item']['date']) <= time()): ?>	
						<?php echo count($this->_tpl_vars['item']['tasks']); ?>
 Task<?php if (count($this->_tpl_vars['item']['tasks']) > 1): ?>s<?php endif; ?> Comp
					<?php else: ?>
						<?php echo count($this->_tpl_vars['item']['tasks']); ?>
 Task<?php if (count($this->_tpl_vars['item']['tasks']) > 1): ?>s<?php endif; ?>
					<?php endif; ?>
				<?php else: ?>
					<?php if ($this->_tpl_vars['item']['today']): ?>
						<?php echo $this->_tpl_vars['item']['closed_task_count']; ?>
 of <?php echo count($this->_tpl_vars['item']['tasks']); ?>
 Task<?php if (count($this->_tpl_vars['item']['tasks']) > 1): ?>s<?php endif; ?> Completed
					<?php elseif (strtotime($this->_tpl_vars['item']['date']) <= time()): ?>
						<?php echo count($this->_tpl_vars['item']['tasks']); ?>
 Task<?php if (count($this->_tpl_vars['item']['tasks']) > 1): ?>s<?php endif; ?> Completed
					<?php else: ?>
						<?php echo count($this->_tpl_vars['item']['tasks']); ?>
 Task<?php if (count($this->_tpl_vars['item']['tasks']) > 1): ?>s<?php endif; ?>
					<?php endif; ?>
				<?php endif; ?>
			</div>
			<div class="task_list" id="list_<?php echo $this->_tpl_vars['key']; ?>
" style="display:none;position:absolute;font-size:1.1em;line-height:1.3em;" />
			</div>
		<?php endif; ?>		
							
			</td>
			
	<?php endforeach; endif; unset($_from); ?>
		</tr>
	</table>
<?php endif; ?>

</div>
<div id="week_cal" class="<?php if ($this->_tpl_vars['module']->toggle_view == 'box'): ?>week_cal<?php else: ?>week_cal_list<?php endif; ?>">
<?php if ($this->_tpl_vars['module']->toggle_view == 'box'): ?>
	<table class="calendar">
		<tr>
			<td class="time">
				<?php $_from = $this->_tpl_vars['module']->hours; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
					<div class="time_label"><?php echo $this->_tpl_vars['item']; ?>
</div>
				<?php endforeach; endif; unset($_from); ?>			
			</td>
			<?php $_from = $this->_tpl_vars['module']->days; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['days']):
?>
			<td <?php if ($this->_tpl_vars['days']['today']): ?>class="today"<?php endif; ?> onclick="javascript:ShowAjaxForm('EventNew&contactid=0&action=add&StartDate='+GetCalendarTime(event, document, '<?php echo $this->_tpl_vars['days']['date']; ?>
', 0)+'&EndDate='+GetCalendarTime(event, document, '<?php echo $this->_tpl_vars['days']['date']; ?>
', 1));window.scrollTo(0,0);">
				<div class="event_con">
					<!--
					top = [15min height] * [# of 15mins] - 1 
					height = [15min height] * [# of 15mins] - 1 + 2px for IE 
					currently 15min height = 11px
					-->					
					<?php $_from = $this->_tpl_vars['days']['events']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
					<div id="ajax/ajax.EventTooltip.php?ref=<?php echo $this->_tpl_vars['item']['id_enc']; ?>
" class="event_pos preview" style="top:<?php echo $this->_tpl_vars['item']['VerticalOffset']*11-1; ?>
px;left:<?php echo $this->_tpl_vars['item']['LeftPosition']; ?>
%;width:<?php echo $this->_tpl_vars['item']['Width']; ?>
%;">
						<div class="event <?php if ($this->_tpl_vars['item']['pending']): ?>pendingevent<?php endif; ?>" style="background-color:#<?php echo $this->_tpl_vars['item']['eventtype']['EventColor']; ?>
;border-color: <?php echo $this->_tpl_vars['item']['user']['Color']; ?>
;height:<?php echo $this->_tpl_vars['item']['Height']*11-3; ?>
px;-height:<?php echo $this->_tpl_vars['item']['Height']*11+2; ?>
px;<?php if ($this->_tpl_vars['item']['PrivateEvent'] == 1): ?>cursor:default;<?php endif; ?>" <?php if ($this->_tpl_vars['item']['is_viewable']): ?><?php if ($this->_tpl_vars['item']['RepeatParent']): ?>onclick='javascript:LoadRptngEvnt(<?php echo $this->_tpl_vars['item']['RepeatParent']; ?>
, "<?php echo $this->_tpl_vars['item']['id_enc']; ?>
")'<?php else: ?>onclick="javascript:location.href='slipstream.php?action=event&eventId=<?php echo $this->_tpl_vars['item']['EventID']; ?>
&ref=weekly'"<?php endif; ?><?php endif; ?>>
							<?php if ($this->_tpl_vars['item']['Height'] > 1): ?>
							<div class="event_head <?php if ($this->_tpl_vars['item']['Closed']): ?>closedevent<?php endif; ?>" style="background:<?php echo $this->_tpl_vars['item']['user']['Color']; ?>
;">
								<?php echo $this->_tpl_vars['item']['start_time_string']; ?>
 - <?php echo $this->_tpl_vars['item']['end_time_string']; ?>
 (<?php echo $this->_tpl_vars['item']['duration_string']; ?>
)
							</div>
							<?php endif; ?>
							<div>
								<?php if ($this->_tpl_vars['item']['Height'] > 2): ?>
								<span <?php if ($this->_tpl_vars['item']['Closed']): ?>class="closedevent"<?php endif; ?> style="white-space:nowrap;overflow:hidden;">
									<?php if ($this->_tpl_vars['item']['is_viewable']): ?>
										<p class="subtitle"><?php echo $this->_tpl_vars['item']['eventtype']['EventName']; ?>
</p>
										<span>Contact</span> <a onClick='NoBubble(event, "?action=contact&contactId=<?php echo $this->_tpl_vars['item']['ContactID']; ?>
");'><?php echo SplitLongWords($this->_tpl_vars['item']['Contact']['FirstName']); ?>
 <?php echo SplitLongWords($this->_tpl_vars['item']['Contact']['LastName']); ?>
</a><br>
										<span>Company</span> <a onClick='NoBubble(event, "?action=company&accountId=<?php echo $this->_tpl_vars['item']['Contact']['AccountID']; ?>
");'><?php echo $this->_tpl_vars['item']['Contact']['CompanyName']; ?>
</a><br>
										<span>Subject</span> <?php echo $this->_tpl_vars['item']['Subject']; ?>

									<?php else: ?>
										<span>User</span> <?php echo SplitLongWords($this->_tpl_vars['item']['user']['FirstName']); ?>
 <?php echo SplitLongWords($this->_tpl_vars['item']['user']['LastName']); ?>
<br>
										Personal
									<?php endif; ?>
								</span>
								<?php endif; ?>
							</div>
						</div>						
					</div>
					<?php endforeach; endif; unset($_from); ?>
				</div>
				&nbsp;
			</td>
			<?php endforeach; endif; unset($_from); ?>
		</tr>
	</table>
<?php else: ?>
	<?php $_from = $this->_tpl_vars['module']->days; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['days']):
?>
		<?php if ($this->_tpl_vars['module']->type == 'weekly'): ?>
			<div style="margin: 20px 0 5px 0; font-size: 1.4em; font-variant: small-caps;">
				<a style="color:black; font-weight:normal;" href="javascript:GetCalendar('daily', '<?php echo $this->_tpl_vars['days']['date']; ?>
', '<?php echo $this->_tpl_vars['module']->selected_sales_person; ?>
')">
					<?php echo $this->_tpl_vars['days']['LongDay']; ?>

				</a>
			</div>
		<?php endif; ?>
		<div style="border:1px solid #DDD; padding:10px;<?php if ($this->_tpl_vars['days']['today']): ?>background:#F7EC8B;<?php endif; ?>">		
		<?php if ($this->_tpl_vars['days']['events']): ?>
			<?php $_from = $this->_tpl_vars['days']['events']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['events'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['events']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
        $this->_foreach['events']['iteration']++;
?>
				<div class="event <?php if ($this->_tpl_vars['item']['pending']): ?>pendingevent<?php endif; ?>" style="background-color:#<?php echo $this->_tpl_vars['item']['eventtype']['EventColor']; ?>
;border-color:<?php echo $this->_tpl_vars['item']['user']['Color']; ?>
;<?php if (! ($this->_foreach['events']['iteration'] == $this->_foreach['events']['total'])): ?>margin-bottom:10px;<?php endif; ?>" >
					<div class="event_head <?php if ($this->_tpl_vars['item']['Closed']): ?>closedevent<?php endif; ?>" style="background:<?php echo $this->_tpl_vars['item']['user']['Color']; ?>
;" style="cursor:default;">
						<?php echo $this->_tpl_vars['item']['start_time_string']; ?>
 - <?php echo $this->_tpl_vars['item']['end_time_string']; ?>
 (<?php echo $this->_tpl_vars['item']['duration_string']; ?>
)
					</div>
					<div <?php if ($this->_tpl_vars['item']['Closed']): ?>class="closedevent"<?php endif; ?> <?php if ($this->_tpl_vars['item']['PrivateEvent']): ?>style="cursor:default;"<?php else: ?><?php if ($this->_tpl_vars['item']['RepeatType'] > 0): ?>onclick='LoadRptngEvnt(<?php echo $this->_tpl_vars['item']['EventID']; ?>
, "<?php echo $this->_tpl_vars['item']['id_enc']; ?>
")'<?php else: ?>onclick="javascript:location.href='slipstream.php?action=event&eventId=<?php echo $this->_tpl_vars['item']['EventID']; ?>
<?php endif; ?><?php if ($this->_tpl_vars['module']->target != ''): ?>&target=<?php echo $this->_tpl_vars['module']->target; ?>
<?php endif; ?><?php endif; ?>'">
						<?php if ($this->_tpl_vars['item']['is_viewable']): ?>
							<?php echo SplitLongWords($this->_tpl_vars['item']['eventtype']['EventName']); ?>
<br>
							<span>User</span> <?php echo SplitLongWords($this->_tpl_vars['item']['user']['FirstName']); ?>
 <?php echo SplitLongWords($this->_tpl_vars['item']['user']['LastName']); ?>
<br>
							<span>Contact</span> <a href="?action=contact&contactId=<?php echo $this->_tpl_vars['item']['ContactID']; ?>
"><?php echo SplitLongWords($this->_tpl_vars['item']['Contact']['FirstName']); ?>
 <?php echo SplitLongWords($this->_tpl_vars['item']['Contact']['LastName']); ?>
</a><br>
							<span>Company</span> <a href="?action=company&accountId=<?php echo $this->_tpl_vars['item']['Contact']['AccountID']; ?>
"><?php echo SplitLongWords($this->_tpl_vars['item']['Contact']['CompanyName']); ?>
</a><br>
							<span>Subject</span> <?php echo SplitLongWords($this->_tpl_vars['item']['Subject']); ?>

							<?php if ($this->_tpl_vars['item']['Location']): ?><br />
								<span>Location</span>
							<?php echo SplitLongWords($this->_tpl_vars['item']['Location']); ?>
<?php endif; ?>
						<?php else: ?>
							<span>User</span> <?php echo SplitLongWords($this->_tpl_vars['item']['SalesPersonFirstName']); ?>
 <?php echo SplitLongWords($this->_tpl_vars['item']['SalesPersonLastName']); ?>
<br>
							Personal
						<?php endif; ?>
					</div>
				</div>
			<?php endforeach; endif; unset($_from); ?>
		<?php else: ?>
			No events scheduled <br/>
		<?php endif; ?>
			</div>
	<?php endforeach; endif; unset($_from); ?>
<?php endif; ?>
</div>
<script>
	<?php echo '
	$(document).ready(function() {
		ResetCalTooltips();
		$(\'#week_cal\').attr(\'scrollTop\', 332);
	});
	'; ?>

</script>