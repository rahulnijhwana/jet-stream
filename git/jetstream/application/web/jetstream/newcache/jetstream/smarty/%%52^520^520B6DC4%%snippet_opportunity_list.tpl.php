<?php /* Smarty version 2.6.18, created on 2015-02-09 18:10:17
         compiled from snippet_opportunity_list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'snippet_opportunity_list.tpl', 8, false),array('modifier', 'SplitLongWords', 'snippet_opportunity_list.tpl', 14, false),)), $this); ?>
<?php if ($this->_tpl_vars['module']->opportunities): ?>
	<?php $_from = $this->_tpl_vars['module']->opportunities; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['opportunities'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['opportunities']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
        $this->_foreach['opportunities']['iteration']++;
?>
		<div class="event <?php if ($this->_tpl_vars['item']['pending'] == 1): ?>pendingevent<?php endif; ?>" style="width:250px;
			<?php if ($this->_tpl_vars['item']['eventtype']['EventColor'] != ''): ?>background-color:#<?php echo $this->_tpl_vars['item']['eventtype']['EventColor']; ?>
;<?php endif; ?><?php if (! ($this->_foreach['opportunities']['iteration'] == $this->_foreach['opportunities']['total'])): ?>margin-bottom:10px;<?php endif; ?><?php if ($this->_tpl_vars['item']['user']['Color'] == '808080'): ?>border:1px solid #<?php echo $this->_tpl_vars['item']['user']['Color']; ?>
; <?php else: ?> border:1px solid <?php echo $this->_tpl_vars['item']['user']['Color']; ?>
; <?php endif; ?>"
			onclick="javascript:location.href='slipstream.php?action=opportunity&opportunityId=<?php echo $this->_tpl_vars['item']['OpportunityID']; ?>
'">
			<div class="event_head <?php if ($this->_tpl_vars['item']['Closed'] == 1): ?>closedevent<?php endif; ?>" style="background:<?php echo $this->_tpl_vars['item']['user']['Color']; ?>
;">				
					Start Date: <?php echo $this->_tpl_vars['item']['StartDate']; ?>
<br>
					<?php if ($this->_tpl_vars['item']['CloseDate'] != ''): ?>Close Date: <?php echo ((is_array($_tmp=$this->_tpl_vars['item']['CloseDate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m/%d/%y") : smarty_modifier_date_format($_tmp, "%m/%d/%y")); ?>
<?php else: ?>Expected Close Date: <?php echo ((is_array($_tmp=$this->_tpl_vars['item']['ExpCloseDate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%m/%d/%y") : smarty_modifier_date_format($_tmp, "%m/%d/%y")); ?>
<?php endif; ?>
			</div>
			<div class="openevent">
				<table class="sidebar_tbl">
					<tr>
						<td class="label"><span>Opportunity</span></td>
						<td><?php echo SplitLongWords($this->_tpl_vars['item']['OpportunityName']); ?>
</td>
					</tr>
					<tr>
						<td class="label"><span>Salesperson</span></td>
						<td>
							<?php echo SplitLongWords($this->_tpl_vars['item']['salesperson']['FirstName']); ?>
 <?php echo SplitLongWords($this->_tpl_vars['item']['salesperson']['LastName']); ?>

						</td>
					</tr>
					<tr>
						<td class="label"><span>Contact</span></td>
						<td><a onclick='javascript:NoBubble(event,"?action=contact&contactId=<?php echo $this->_tpl_vars['item']['ContactID']; ?>
");'><?php echo SplitLongWords($this->_tpl_vars['item']['Contact']['FirstName']); ?>
 <?php echo SplitLongWords($this->_tpl_vars['item']['Contact']['LastName']); ?>
</a></td>
					</tr>
					<tr>
						<td class="label"><span>Company</span></td>
						<td><a onclick='javascript:NoBubble(event,"?action=company&accountId=<?php echo $this->_tpl_vars['item']['Contact']['AccountID']; ?>
");'><?php echo SplitLongWords($this->_tpl_vars['item']['Contact']['CompanyName']); ?>
</a></td>
					</tr>
				</table>
			</div>
		</div>
	<?php endforeach; endif; unset($_from); ?>
<?php else: ?>
	<span class='BlackBold'><?php echo $this->_tpl_vars['module']->msg; ?>
</span>
<?php endif; ?>