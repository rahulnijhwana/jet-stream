<?php /* Smarty version 2.6.18, created on 2015-02-09 18:10:12
         compiled from snippet_note_list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'snippet_note_list.tpl', 13, false),array('modifier', 'trim', 'snippet_note_list.tpl', 16, false),)), $this); ?>
<div id="pager">
	<?php echo $this->_tpl_vars['module']->notesPerPageSelect; ?>

	<span id="loadingDiv" style="float: left; margin-right: 5px; display: none;">
		<img width="20" height="20" src="./images/slipstream_spinner.gif" alt="Loading...." />
	</span>
	<span style="float:right;" id="spanPageLinkTop">
	  <?php echo $this->_tpl_vars['module']->page_links; ?>

	</span>
	<span style="float:right;" id="pageInfo"><?php echo $this->_tpl_vars['module']->page_info; ?>
</span>
</div>
<div class="clearing"></div>
<?php unset($this->_sections['note']);
$this->_sections['note']['name'] = 'note';
$this->_sections['note']['loop'] = is_array($_loop=$this->_tpl_vars['module']->note_list) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['note']['show'] = true;
$this->_sections['note']['max'] = $this->_sections['note']['loop'];
$this->_sections['note']['step'] = 1;
$this->_sections['note']['start'] = $this->_sections['note']['step'] > 0 ? 0 : $this->_sections['note']['loop']-1;
if ($this->_sections['note']['show']) {
    $this->_sections['note']['total'] = $this->_sections['note']['loop'];
    if ($this->_sections['note']['total'] == 0)
        $this->_sections['note']['show'] = false;
} else
    $this->_sections['note']['total'] = 0;
if ($this->_sections['note']['show']):

            for ($this->_sections['note']['index'] = $this->_sections['note']['start'], $this->_sections['note']['iteration'] = 1;
                 $this->_sections['note']['iteration'] <= $this->_sections['note']['total'];
                 $this->_sections['note']['index'] += $this->_sections['note']['step'], $this->_sections['note']['iteration']++):
$this->_sections['note']['rownum'] = $this->_sections['note']['iteration'];
$this->_sections['note']['index_prev'] = $this->_sections['note']['index'] - $this->_sections['note']['step'];
$this->_sections['note']['index_next'] = $this->_sections['note']['index'] + $this->_sections['note']['step'];
$this->_sections['note']['first']      = ($this->_sections['note']['iteration'] == 1);
$this->_sections['note']['last']       = ($this->_sections['note']['iteration'] == $this->_sections['note']['total']);
?>
	<div class="<?php echo smarty_function_cycle(array('values' => "note, note_alt"), $this);?>
">
		<div style="display: block; float: right;">
			<table>
			<?php if (((is_array($_tmp=$this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['Creator'])) ? $this->_run_mod_handler('trim', true, $_tmp) : trim($_tmp)) != ""): ?>
				<tr>
					<td class="right_sub">
						User
					</td>
					<td>
						<?php echo $this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['Creator']; ?>

					</td>
				</tr> 
			<?php endif; ?>
			<?php if ($this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['Type'] != ""): ?>
				<tr>
					<td class="right_sub">Type</td>
					<td>
						<?php if ($this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['Type'] == 'Email' && $this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['EmailContentID'] > 0): ?>
							<a href="ajax/ajax.getEmailByID.php?email=<?php echo $this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['EmailContentID']; ?>
" target="blank">Email</a>
						<?php else: ?>
							<?php echo $this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['Type']; ?>

						<?php endif; ?>
					</td>
				</tr>
			<?php endif; ?>		
			<?php if (isset ( $this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['Private'] )): ?>
				<?php if ($this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['Private'] == '1'): ?>
					<tr>
						<td class="right_sub">
							Private
						</td>
						<td>
							<img src="images/checkbox_checked.png" />
						</td>
					</tr>
				<?php endif; ?>
			<?php endif; ?>		
			</table>
		</div>
		<?php if ($this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['HasAlert']): ?>
			<div class="alert">
				<img src="images/alert.png" />
			</div>
		<?php endif; ?>
		<h3 class="subject<?php if ($this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['HasAlert']): ?> alert_subject<?php endif; ?>"><?php echo $this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['Subject']; ?>
</h3>
		<div class="clear_left"></div>
		<p><b>Date</b> <?php echo $this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['TimeStamp']; ?>
</p>


		<?php if ($this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['Company']): ?>
			<p><b>Company</b> <a href="?action=company&accountId=<?php echo $this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['AccountID']; ?>
"><?php echo $this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['Company']; ?>
</a></p>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['ContactID'] != ""): ?>
			<p><b>Contact</b> <a href="?action=contact&contactId=<?php echo $this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['ContactID']; ?>
"><?php echo $this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['Contact']; ?>
</a></p>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['Opp'] != ""): ?>
			<p><b>Opportunity</b> <?php echo $this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['Opp']; ?>
</p>
		<?php endif; ?>
		
		<?php if ($this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['EmailContentID'] > 0): ?>
		
		<p><b>From</b> <a href="mailto:<?php echo $this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['FromEmail']; ?>
"><?php echo $this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['From']; ?>
</a><img src="images/email.gif" style="margin-left: 5px;"/></p>
		<p><b>To</b> <?php echo $this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['To']; ?>
</p>
		<?php if (((is_array($_tmp=$this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['Cc'])) ? $this->_run_mod_handler('trim', true, $_tmp) : trim($_tmp)) != ''): ?>
			<p><b>Cc</b> <?php echo $this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['Cc']; ?>
</p> 
		<?php endif; ?>
		
		<div>
		<?php if ($this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['Attachment']): ?>
			<div class="attachment_wrapper">
				<div class="attachment_img">
					<img src="images/attachment.png" width="36" height="36" alt="Attachments" />
				</div>
				<div class="attachments">
					<span class="attachment_links"><?php echo $this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['Attachment']; ?>
</span>
				</div>
			</div>
		<?php endif; ?>
		</div>
		<div class="clearing"></div>		
		<div style="border: 1px solid #cacaca; padding:0;">
			<iframe src="ajax/ajax.getEmailByID.php?email=<?php echo $this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['EmailContentID']; ?>
" style="width:100%;" frameborder="0" /></iframe>
		</div>
		<?php else: ?>
		<div class="clearing"></div>		
		<div>
			<p class="body"><?php echo $this->_tpl_vars['module']->note_list[$this->_sections['note']['index']]['Body']; ?>
</p>
		</div>
		<?php endif; ?>
	</div>
<?php endfor; endif; ?>
<?php if ($this->_tpl_vars['module']->no_records): ?><?php echo $this->_tpl_vars['module']->no_records; ?>
<?php endif; ?>
<span style="float:right;" id="spanPageLinkBottom">
  <?php echo $this->_tpl_vars['module']->page_links; ?>

</span>
<div class="clearing"></div>
<input type="hidden" name="totRec" id="totRec" value="<?php echo $this->_tpl_vars['module']->tot_rec; ?>
">

<script language="javascript" type="text/javascript">
<?php echo '
// Hide and show forces a redraw to fix display issues in IE6
$(\'iframe\').load(function(){
	$(\'iframe\').hide();
	$(\'iframe\').show();

});

$(document).ready(function() {
	$(\'.body\').each(function(i) {
		obj = $(this);
		if (obj.height() > 200) {
			obj.css(\'height\', \'200px\');
			obj.css(\'overflow\', \'auto\');
		}
	});

});

'; ?>

</script>
