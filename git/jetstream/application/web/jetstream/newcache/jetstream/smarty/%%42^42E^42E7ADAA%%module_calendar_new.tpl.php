<?php /* Smarty version 2.6.18, created on 2015-02-09 18:10:07
         compiled from module_calendar_new.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'module_calendar_new.tpl', 2, false),)), $this); ?>
<div style="width:100%;">
	<?php if (count($this->_tpl_vars['module']->sales_people) > 1): ?>
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "snippet_merge_calendar_form.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<?php endif; ?>	
	<?php if (( $this->_tpl_vars['module']->type == 'monthly' || $this->_tpl_vars['module']->type == 'fiveweek' )): ?> 
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "snippet_calendar_monthly.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<?php else: ?>
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "snippet_calendar.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<?php endif; ?>
</div>
<script language="javascript">
	var cal_type = '<?php echo $this->_tpl_vars['module']->type; ?>
';	
	var cal_date = '<?php echo $this->_tpl_vars['module']->day; ?>
';
</script>