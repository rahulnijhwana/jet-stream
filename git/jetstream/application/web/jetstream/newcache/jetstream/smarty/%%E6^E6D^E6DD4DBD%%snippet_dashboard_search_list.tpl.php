<?php /* Smarty version 2.6.18, created on 2015-02-09 18:10:07
         compiled from snippet_dashboard_search_list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'snippet_dashboard_search_list.tpl', 14, false),array('modifier', 'trim', 'snippet_dashboard_search_list.tpl', 19, false),array('modifier', 'SplitLongWords', 'snippet_dashboard_search_list.tpl', 28, false),)), $this); ?>
<div style="text-align:right;display:hidden">
	<?php echo $this->_tpl_vars['module']->SearchResultsInfo(); ?>
 <?php echo $this->_tpl_vars['module']->getDashboardPages(); ?>

</div>
<?php if (! $this->_tpl_vars['module']->search_results): ?>
	<?php if ($this->_tpl_vars['module']->last_search): ?>
	<div id="searchresults">
		<div class="smlRedBold" style="text-align:center;">No matching records found</div>
	</div>
	<?php endif; ?>
<?php else: ?>
	<table id="searchresults" width="100%">
		<?php $_from = $this->_tpl_vars['module']->search_results; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['result']):
?>

		<tr style="clear:both;" class="<?php echo smarty_function_cycle(array('values' => "alt_row,row"), $this);?>
<?php if ($this->_tpl_vars['result']['Private']): ?>private<?php endif; ?><?php if ($this->_tpl_vars['result']['Inactive']): ?>inactive<?php endif; ?>" id="div_<?php if ($this->_tpl_vars['result']['ContactID']): ?><?php echo $this->_tpl_vars['result']['ContactID']; ?>
<?php else: ?><?php echo $this->_tpl_vars['result']['AccountID']; ?>
<?php endif; ?>">

			<td class="toggle">
				<a href="javascript: toggleInfo('<?php echo $this->_tpl_vars['result']['UniqueID']; ?>
')"><img id="toggle_<?php echo $this->_tpl_vars['result']['UniqueID']; ?>
" src="images/plus.gif"/></a>
			<?php if ($this->_tpl_vars['result']['ContactID']): ?>
				<input type="checkbox" class="mergeClass" onclick="javascript:setAttr(this.id,'chkContact');" id="chk_<?php echo $this->_tpl_vars['result']['ContactID']; ?>
" cid="<?php echo $this->_tpl_vars['result']['ContactID']; ?>
" accid="<?php echo $this->_tpl_vars['result']['AccountID']; ?>
" cname="<?php echo ((is_array($_tmp=$this->_tpl_vars['result']['ContactName'])) ? $this->_run_mod_handler('trim', true, $_tmp) : trim($_tmp)); ?>
" accname="<?php echo ((is_array($_tmp=$this->_tpl_vars['result']['AccountName'])) ? $this->_run_mod_handler('trim', true, $_tmp) : trim($_tmp)); ?>
" style="display:none;"/>
			<?php else: ?>
				<input type="checkbox" class="mergeClass" onclick="javascript:setAttr(this.id,'chkAccount');" id="chk_<?php echo $this->_tpl_vars['result']['AccountID']; ?>
" accid="<?php echo $this->_tpl_vars['result']['AccountID']; ?>
" accname="<?php echo ((is_array($_tmp=$this->_tpl_vars['result']['AccountName'])) ? $this->_run_mod_handler('trim', true, $_tmp) : trim($_tmp)); ?>
" style="display:none;"/>
			<?php endif; ?>
			</td>

			<td>
			<?php if ($this->_tpl_vars['result']['ContactID']): ?>
				<a href="javascript: toggleInfo('<?php echo $this->_tpl_vars['result']['UniqueID']; ?>
')"><b class="contact_title">Contact</b></a> 
				<a href="?action=contact&contactId=<?php echo $this->_tpl_vars['result']['ContactID']; ?>
"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['result']['ContactName'])) ? $this->_run_mod_handler('trim', true, $_tmp) : trim($_tmp)))) ? $this->_run_mod_handler('SplitLongWords', true, $_tmp) : SplitLongWords($_tmp)); ?>
</a> 
				<?php if (((is_array($_tmp=$this->_tpl_vars['result']['AccountName'])) ? $this->_run_mod_handler('trim', true, $_tmp) : trim($_tmp))): ?>(<a href="?action=company&accountId=<?php echo $this->_tpl_vars['result']['AccountID']; ?>
"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['result']['AccountName'])) ? $this->_run_mod_handler('trim', true, $_tmp) : trim($_tmp)))) ? $this->_run_mod_handler('SplitLongWords', true, $_tmp) : SplitLongWords($_tmp)); ?>
</a>)<?php endif; ?>
			<?php else: ?>
				<?php if ($this->_tpl_vars['result']['AssignedToMe'] || $this->_tpl_vars['limit_account_access'] == 0 || $this->_tpl_vars['is_manager']): ?>
					<b class="company_title">Company</b> 
					<a href="?action=company&accountId=<?php echo $this->_tpl_vars['result']['AccountID']; ?>
"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['result']['AccountName'])) ? $this->_run_mod_handler('trim', true, $_tmp) : trim($_tmp)))) ? $this->_run_mod_handler('SplitLongWords', true, $_tmp) : SplitLongWords($_tmp)); ?>
 </a>
				<?php else: ?>
					<b class="company_title">Company</b> 
					<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['result']['AccountName'])) ? $this->_run_mod_handler('trim', true, $_tmp) : trim($_tmp)))) ? $this->_run_mod_handler('SplitLongWords', true, $_tmp) : SplitLongWords($_tmp)); ?>

				<?php endif; ?>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['result']['Private']): ?> [Private]<?php endif; ?>
			<?php if ($this->_tpl_vars['result']['Inactive']): ?> [Inactive]<?php endif; ?>
			</td>			

			<td><strong><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['result']['City'])) ? $this->_run_mod_handler('trim', true, $_tmp) : trim($_tmp)))) ? $this->_run_mod_handler('SplitLongWords', true, $_tmp) : SplitLongWords($_tmp)); ?>
</strong></td>
			
			<td><strong><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['result']['State'])) ? $this->_run_mod_handler('trim', true, $_tmp) : trim($_tmp)))) ? $this->_run_mod_handler('SplitLongWords', true, $_tmp) : SplitLongWords($_tmp)); ?>
</strong></td>

		</tr>

		<tr>
			<td class="detail"colspan="4" id="<?php echo $this->_tpl_vars['result']['UniqueID']; ?>
">
				<img class="spinner" src="images/slipstream_spinner.gif" />
			</td>
		</tr>
		
		<?php endforeach; endif; unset($_from); ?>
	</table>
<?php endif; ?>

<div style="text-align:right;display:hidden">
	<?php echo $this->_tpl_vars['module']->SearchResultsInfo(); ?>
 <?php echo $this->_tpl_vars['module']->getDashboardPages(); ?>

</div>