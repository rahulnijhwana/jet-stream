<?php /* Smarty version 2.6.18, created on 2015-02-09 18:10:12
         compiled from snippet_event.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'FormatDate', 'snippet_event.tpl', 131, false),array('modifier', 'FormatDuration', 'snippet_event.tpl', 131, false),array('modifier', 'lower', 'snippet_event.tpl', 159, false),array('modifier', 'capitalize', 'snippet_event.tpl', 159, false),)), $this); ?>
<div class="jet_datagrid" align="center">
<table>
	<?php if ($this->_tpl_vars['module']->event_info['AccountID']): ?>
	<tr>
		<td class="label3">Company</td>
		<td class="data <?php if ($this->_tpl_vars['module']->event_info['Closed'] == 1): ?>closedevent<?php endif; ?>">
			<!-- toggle later, besso
			<div style="padding:1px 10px 0 0px; float:left;">
				<a href="javascript: ToggleContactInfo()"><img id="toggle_contact" src="images/plus.gif"/></a>
			</div>
			-->
			<a href="slipstream.php?action=company&accountId=<?php echo $this->_tpl_vars['module']->event_info['AccountID']; ?>
"><?php echo $this->_tpl_vars['module']->event_info['AccountName']; ?>
</a>
		</td>
	</tr>
	<tr id="event_contact_cells" style="display:none;">
		<td colspan="2" style="background: #DDD; padding: 0px 10px 10px 10px;">

			<div id="event_contact_info" style="margin-top: 10px; background-color: #F8F8F8;">
				<div class="loading" style="border:0px;"><img class="spinner" src="images/slipstream_spinner.gif" /></div>
				<script type="text/javascript">
					<?php echo '
					
					var contact_loaded = false;
					
					function ToggleContactInfo() {
						if($(\'#event_contact_cells\').css(\'display\') == \'none\' || $(\'#event_contact_cells\').css(\'display\') == \'\') {
							$(\'#event_contact_cells\').show();
							$(\'#toggle_contact\').attr(\'src\', \'images/minus.gif\');
							if (!contact_loaded) {
								contact_loaded = true;
								LoadContact();
							}
						} else {
							$(\'#event_contact_cells\').hide();
							$(\'#toggle_contact\').attr(\'src\', \'images/plus.gif\');
						}
					}		
					
					function LoadContact() {
						$.ajax({
							url: "ajax/ajax.searchdetail.php",
							data: ("request_id=account_'; ?>
<?php echo $this->_tpl_vars['module']->event_info['AccountID']; ?>
<?php echo '"),
							type: "POST",
							dataType: "html",
							success: function(data){
								$("#event_contact_info").html(data);
							}
						});
					}
					'; ?>

				</script>
			</div>

		</td>
	</tr>
	<?php else: ?>
	<tr>
		<td class="label3">Contact</td>
		<td class="data <?php if ($this->_tpl_vars['module']->event_info['Closed'] == 1): ?>closedevent<?php endif; ?>">

			<?php if (! $this->_tpl_vars['module']->event_info['Contact']['noLink']): ?>
			<div style="padding:1px 10px 0 0px; float:left;">
				<a href="javascript: ToggleContactInfo()"><img id="toggle_contact" src="images/plus.gif"/></a>
			</div>
			<?php endif; ?>

			<?php if (! $this->_tpl_vars['module']->event_info['Contact']['noLink']): ?>
			<a style="float:left;" href="slipstream.php?action=contact&contactId=<?php echo $this->_tpl_vars['module']->event_info['ContactID']; ?>
"><?php echo $this->_tpl_vars['module']->event_info['Contact']['FirstName']; ?>
 <?php echo $this->_tpl_vars['module']->event_info['Contact']['LastName']; ?>
</a>
			<?php else: ?>
                <?php if (@GOOGLE_EVENT_TYPE_IN_JETSTREAM == $this->_tpl_vars['module']->event_info['EventTypeID']): ?>  
                    Please choose valid Contact
                <?php elseif (@GOOGLE_TASK_TYPE_IN_JETSTREAM == $this->_tpl_vars['module']->event_info['EventTypeID']): ?>
                    Please choose valid Contact or Company
                <?php else: ?>
			        <?php echo $this->_tpl_vars['module']->event_info['Contact']['FirstName']; ?>
 <?php echo $this->_tpl_vars['module']->event_info['Contact']['LastName']; ?>

                <?php endif; ?>
			<?php endif; ?>

			&nbsp;<?php if ($this->_tpl_vars['module']->event_info['Contact']['AccountID']): ?>(<a href="slipstream.php?action=company&accountId=<?php echo $this->_tpl_vars['module']->event_info['Contact']['AccountID']; ?>
"><?php echo $this->_tpl_vars['module']->event_info['Contact']['CompanyName']; ?>
</a>)<?php endif; ?>
		</td>
	</tr>
	<tr id="event_contact_cells" style="display:none;">
		<td colspan="2" style="background: #DDD; padding: 0px 10px 10px 10px;">

			<div id="event_contact_info" style="margin-top: 10px; background-color: #F8F8F8;">
				<div class="loading" style="border:0px;"><img class="spinner" src="images/slipstream_spinner.gif" /></div>
				<script type="text/javascript">
					<?php echo '
					
					var contact_loaded = false;
					
					function ToggleContactInfo() {
						if($(\'#event_contact_cells\').css(\'display\') == \'none\' || $(\'#event_contact_cells\').css(\'display\') == \'\') {
							$(\'#event_contact_cells\').show();
							$(\'#toggle_contact\').attr(\'src\', \'images/minus.gif\');
							if (!contact_loaded) {
								contact_loaded = true;
								LoadContact();
							}
						} else {
							$(\'#event_contact_cells\').hide();
							$(\'#toggle_contact\').attr(\'src\', \'images/plus.gif\');
						}
					}		
					
					function LoadContact() {
						$.ajax({
							url: "ajax/ajax.searchdetail.php",
							data: ("request_id=contact_'; ?>
<?php echo $this->_tpl_vars['module']->event_info['ContactID']; ?>
<?php echo '"),
							type: "POST",
							dataType: "html",
							success: function(data){
								$("#event_contact_info").html(data);
							}
						});
					}
					'; ?>

				</script>
			</div>

		</td>
	</tr>
	<?php endif; ?>
	<tr>
		<td class="label3">When</td>
		<td class="left <?php if ($this->_tpl_vars['module']->event_info['Closed'] == 1): ?>closedevent<?php endif; ?>" colspan="3">
			<?php if ($this->_tpl_vars['module']->event_info['EventType']['Type'] == 'TASK'): ?>
				<span style="font-size: 1.2em; font-weight: bold; display: block; margin: 4px 0px 4px 0px; <?php if ($this->_tpl_vars['module']->event_info['pending']): ?> color: red;<?php endif; ?>"><?php echo $this->_tpl_vars['module']->event_str; ?>
</span>
			<?php else: ?>
			<?php if ($this->_tpl_vars['module']->event_info['RepeatType'] == 0): ?>
				<!-- <span style="font-size:1.2em;font-weight:bold;"><?php echo ((is_array($_tmp=$this->_tpl_vars['module']->event_info['StartTimeStamp'])) ? $this->_run_mod_handler('FormatDate', true, $_tmp) : FormatDate($_tmp)); ?>
 to <?php echo ((is_array($_tmp=$this->_tpl_vars['module']->event_info['EndTimeStamp'])) ? $this->_run_mod_handler('FormatDate', true, $_tmp) : FormatDate($_tmp)); ?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['module']->event_info['Duration'])) ? $this->_run_mod_handler('FormatDuration', true, $_tmp) : FormatDuration($_tmp)); ?>
)</span> -->
				<span style="font-size: 1.2em; font-weight: bold; display: block; margin: 4px 0px 4px 0px; <?php if ($this->_tpl_vars['module']->event_info['pending']): ?> color: red;<?php endif; ?>"><?php echo $this->_tpl_vars['module']->event_str; ?>
</span>
			<?php if ($this->_tpl_vars['module']->event_info['RepeatParent']): ?>
				<p class="repeat_label">This event is part of a <a href="slipstream.php?action=event&eventId=<?php echo $this->_tpl_vars['module']->event_info['RepeatParent']; ?>
">repeating series</a></p>
			<?php endif; ?>
			<?php else: ?>
				<?php echo $this->_tpl_vars['module']->GetRepeatReadable(); ?>

			<?php endif; ?>
			<?php endif; ?>
		</td>
	</tr>
	<tr>
		<td class="label3">Attendees</td>
		<td class="left <?php if ($this->_tpl_vars['module']->event_info['Closed'] == 1): ?>closedevent<?php endif; ?>" <?php if ($this->_tpl_vars['module']->event_info['DealID'] > 0): ?>colspan="3"<?php endif; ?>>
			<?php $_from = $this->_tpl_vars['module']->attendees; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['people'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['people']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['attendee']):
        $this->_foreach['people']['iteration']++;
?>
				<?php if (! ($this->_foreach['people']['iteration'] <= 1)): ?><br><?php endif; ?>
				<?php echo $this->_tpl_vars['attendee']['name']; ?>

			<?php endforeach; endif; unset($_from); ?>
		</td>
	</tr>
	<tr>
		<td class="label3">Type</td>
		<td <?php if ($this->_tpl_vars['module']->event_info['Closed'] == 1): ?>class="closedevent"<?php endif; ?>>
            <?php if (@GOOGLE_EVENT_TYPE_IN_JETSTREAM == $this->_tpl_vars['module']->event_info['EventTypeID']): ?>
                Please choose valid Event Type
            <?php elseif (@GOOGLE_TASK_TYPE_IN_JETSTREAM == $this->_tpl_vars['module']->event_info['EventTypeID']): ?>
                Please choose valid Task Type
            <?php else: ?>
			    <?php echo $this->_tpl_vars['module']->event_info['EventType']['EventName']; ?>
 (<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['module']->event_info['EventType']['Type'])) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp)))) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
)
            <?php endif; ?>
		</td>
	</tr>
	<tr>
		<td class="label3">Subject</td>
		<td <?php if ($this->_tpl_vars['module']->event_info['Closed'] == 1): ?>class="closedevent"<?php endif; ?>>
			<?php echo $this->_tpl_vars['module']->event_info['Subject']; ?>

		</td>
	</tr>
	<?php if ($this->_tpl_vars['module']->event_info['Location']): ?>
	<tr>
		<td class="label3">Location</td>
		<td <?php if ($this->_tpl_vars['module']->event_info['Closed'] == 1): ?>class="closedevent"<?php endif; ?>>
			<?php echo $this->_tpl_vars['module']->event_info['Location']; ?>

		</td>
	</tr>
	<?php endif; ?>
	<?php if ($this->_tpl_vars['module']->event_info['PriorityID']): ?>
	<tr>
		<td class="label3">Priority</td>
		<td <?php if ($this->_tpl_vars['module']->event_info['Closed'] == 1): ?>class="closedevent"<?php endif; ?>>
			<?php echo $this->_tpl_vars['module']->event_info['PriorityLabel']; ?>

		</td>
	</tr>
	<?php endif; ?>
	<?php if (! $this->_tpl_vars['module']->event_info['DealID']): ?>
		<?php if ($this->_tpl_vars['module']->event_info['Private'] == '1'): ?>

			<tr>
				<td class="label3">Private</td>
				<td class="right">			
					<img src="images/checkbox_checked.png">
				</td>
			</tr>
		<?php endif; ?>
	<?php endif; ?>
</table>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "form_close_event.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<script language="javascript" type="text/javascript">
	<?php echo '	
	function openOppPopup(dealId, personId) {
		var win = Windowing.openSizedWindow(\'legacy/shared/edit_opp2.php?SN='; ?>
<?php echo $_COOKIE['SN']; ?>
<?php echo '&detailview=1&reqOpId=\' + dealId + \'&reqPersonId=\' + personId + \'#\' + dealId + \'_\' + personId, 630, 1000);		
	}
	'; ?>

</script>	
