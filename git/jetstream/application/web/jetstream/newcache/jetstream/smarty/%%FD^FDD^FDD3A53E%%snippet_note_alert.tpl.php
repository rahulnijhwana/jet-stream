<?php /* Smarty version 2.6.18, created on 2015-02-09 18:10:12
         compiled from snippet_note_alert.tpl */ ?>
<div>
	<div class="img_wrapper">
		<div id="add_alert_wrapper">
			<input type="button" id="expand_alert" value="Add Alert" />
		</div>
		<div class="help_icon">
			<a href="javascript: void(0);" class="note_alert_help" rel="ajax/ajax.contextHelp.php">
				<img src="images/help_red.png" alt="What is this?" style="width: 24px; height: 24px;" />
			</a>
		</div>
		<div class="clearing"></div>
	</div>
	<div id="add_alert">
		<div class="alert_options">
			<span class="subtitle">1. How do you want to send the Alert?</span>
			<table class="user_tree">
				<tr>
					<td>
						<p class="delivery_methods">
							<label for="standard_alert">
								<input type="checkbox" name="standard_alert" id="standard_alert" checked="checked" />
								On Screen
							</label>
						</p>
					</td>
					<td>
						<p class="delivery_methods">
							<label for="email_alert">
								<input type="checkbox" name="email_alert" id="email_alert" />
								Email
							</label>
						</p>
					</td>
				</tr>
			</table>
			<span id="spn_delivery_error" class="clsError"></span>
		</div>
		<div class="alert_options">
			<span class="subtitle">2. When should we send it?</span>
			<table class="user_tree">
				<tr>
					<td>
						<p class="timing">
							<label for="send_immediately_yes">
								<input type="radio" name="send_immediately" id="send_immediately_yes" checked="checked" value="1" />
								Immediately
							</label>
						</p>
					</td>
					<td>
						<p class="timing">
							<label for="send_immediately_no">
								<input type="radio" name="send_immediately" id="send_immediately_no" value="0" /> 
								Delay until
							</label>
						</p>
						<input type="text" name="delay_until" id="delay_until_day" class="datepicker" size="12" value="" disabled="disabled" />
						<span class="time_select"><?php echo $this->_tpl_vars['module']->hourSelect; ?>
 : <?php echo $this->_tpl_vars['module']->minuteSelect; ?>
 <?php echo $this->_tpl_vars['module']->periodSelect; ?>
</span> 
					</td>
				</tr>
			</table>
			<span id="spn_delay_until_error" class="clsError"></span>
		</div>
		<div class="alert_options">
			<span class="subtitle">3. Who should we send it to?</span>
			<p>
				<span class="links">
					<a href="javascript:void(0);" id="select_all">Select All</a> |
					<a href="javascript:void(0);" id="select_assigned">Select Assigned</a> |
					<a href="javascript:void(0);" id="select_level">Select Level</a>
				</span>
			</p>
			<span id="spn_select_user_error" class="clsError"></span>
			<span id="level_msg">Click on the column headers below to select users by level.</span>
			<table class="user_tree">
				<tr class="header">
				<?php $_from = $this->_tpl_vars['module']->tree; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['level'] => $this->_tpl_vars['users']):
?>
					<td>
						<a href="javascript:void(0);" id="level_<?php echo $this->_tpl_vars['level']; ?>
" class="level"><?php echo $this->_tpl_vars['level']; ?>
</a>
					</td>
				<?php endforeach; endif; unset($_from); ?>
				</tr>
				<tr>
				<?php $_from = $this->_tpl_vars['module']->tree; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['level'] => $this->_tpl_vars['users']):
?>
						<td class="user_level">
							<ul class="simple">
								<?php $_from = $this->_tpl_vars['users']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['user']):
?>
									<li <?php if ($this->_tpl_vars['user']['AssignedTo']): ?>class="heavy"<?php endif; ?>>
										<p class="timing">
											<label>
												<input type="checkbox" name="alert_user[]" value="<?php echo $this->_tpl_vars['user']['PersonID']; ?>
" class="all_users <?php if ($this->_tpl_vars['user']['AssignedTo']): ?> assigned_users<?php endif; ?> level_<?php echo $this->_tpl_vars['user']['Level']; ?>
" />
												<?php echo $this->_tpl_vars['user']['FirstName']; ?>
 <?php echo $this->_tpl_vars['user']['LastName']; ?>

											</label>
										</p>
										<div class="clearing"></div>
									</li> 
								<?php endforeach; endif; unset($_from); ?>
							</ul>
						</td>
				<?php endforeach; endif; unset($_from); ?>
				</tr>
			</table>
		</div>
	</div>
</div>