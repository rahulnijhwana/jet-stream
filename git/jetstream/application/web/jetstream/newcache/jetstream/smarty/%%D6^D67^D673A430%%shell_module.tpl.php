<?php /* Smarty version 2.6.18, created on 2015-02-09 15:58:09
         compiled from shell_module.tpl */ ?>
<div <?php if ($this->_tpl_vars['module']->id): ?>id="<?php echo $this->_tpl_vars['module']->id; ?>
"<?php endif; ?> <?php if ($this->_tpl_vars['module']->formatting): ?>class="<?php echo $this->_tpl_vars['module']->formatting; ?>
"<?php endif; ?>>
	<div class="content_head">
		<div class="content_head_text">
			<?php if ($this->_tpl_vars['module']->id == 'dashboard_simple_search'): ?>
				<a href="javascript: void(0);" id="toggle_dashboard_search_link"><img id="toggle_dashboard_search" src="images/minus-med.gif" /></a>
			<?php endif; ?>
			<?php echo $this->_tpl_vars['module']->title; ?>

		</div>
		<div class="content_head_icons">
			<?php $_from = $this->_tpl_vars['module']->buttons; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['buttons'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['buttons']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['button']):
        $this->_foreach['buttons']['iteration']++;
?><a <?php if ($this->_tpl_vars['button']['id']): ?>id="<?php echo $this->_tpl_vars['button']['id']; ?>
"<?php endif; ?> title="<?php echo $this->_tpl_vars['button']['tooltip']; ?>
" href="<?php echo $this->_tpl_vars['button']['action']; ?>
" target="<?php echo $this->_tpl_vars['button']['target']; ?>
"><?php echo $this->_tpl_vars['button']['tooltip']; ?>
</a><?php if (! ($this->_foreach['buttons']['iteration'] == $this->_foreach['buttons']['total'])): ?> | <?php endif; ?><?php endforeach; endif; unset($_from); ?>
		</div>
		<?php if ($this->_tpl_vars['module']->menu): ?>
			<ul class="sf-menu-module">
				<?php $_from = $this->_tpl_vars['module']->menu; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['menu_item']):
?>
				<li<?php if ($this->_tpl_vars['menu_item']['selected']): ?> class="selected"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['menu_item']['action']; ?>
"><?php echo $this->_tpl_vars['menu_item']['label']; ?>
</a></li>
				<?php endforeach; endif; unset($_from); ?>
			</ul>
		<?php endif; ?>
	</div>
	<div class="content_body">
		<?php $_from = $this->_tpl_vars['module']->template_files; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['template_file']):
?>
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['template_file'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<?php endforeach; endif; unset($_from); ?>
	</div>
</div>