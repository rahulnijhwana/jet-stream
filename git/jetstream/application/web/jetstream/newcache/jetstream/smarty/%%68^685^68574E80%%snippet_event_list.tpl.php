<?php /* Smarty version 2.6.18, created on 2015-02-09 18:10:07
         compiled from snippet_event_list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'SplitLongWords', 'snippet_event_list.tpl', 17, false),)), $this); ?>
<?php if ($this->_tpl_vars['module']->events): ?>
	<?php $_from = $this->_tpl_vars['module']->events; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['events'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['events']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
        $this->_foreach['events']['iteration']++;
?>
		<div class="event <?php if ($this->_tpl_vars['item']['pending'] == 1): ?>pendingevent<?php endif; ?>" style="width:98%;
			<?php if ($this->_tpl_vars['item']['eventtype']['EventColor'] != ''): ?>background-color:#<?php echo $this->_tpl_vars['item']['eventtype']['EventColor']; ?>
;<?php endif; ?><?php if (! ($this->_foreach['events']['iteration'] == $this->_foreach['events']['total'])): ?>margin-bottom:10px;<?php endif; ?><?php if ($this->_tpl_vars['item']['user']['Color'] == '808080'): ?>border:1px solid #<?php echo $this->_tpl_vars['item']['user']['Color']; ?>
; <?php else: ?> border:1px solid <?php echo $this->_tpl_vars['item']['user']['Color']; ?>
; <?php endif; ?>"
			<?php if ($this->_tpl_vars['item']['is_viewable']): ?>
				<?php if ($this->_tpl_vars['item']['RepeatParent']): ?>
					onclick="javascript:LoadRptngEvnt('<?php echo $this->_tpl_vars['item']['RepeatParent']; ?>
', '<?php echo $this->_tpl_vars['item']['id_enc']; ?>
')"
				<?php else: ?>
					onclick="javascript:location.href='slipstream.php?action=event&eventId=<?php echo $this->_tpl_vars['item']['EventID']; ?>
'"
				<?php endif; ?>
			<?php endif; ?>>
			<div class="event_head <?php if ($this->_tpl_vars['item']['Closed'] == 1): ?>closedevent<?php endif; ?>" style="background:<?php echo $this->_tpl_vars['item']['user']['Color']; ?>
;">				
				<?php if ($this->_tpl_vars['item']['eventtype']['Type'] == 'TASK'): ?>
					<div class="task_head_wrapper">
						<div class="task_left">
							<?php echo $this->_tpl_vars['item']['orig_date_string']; ?>
<br>
							<?php echo SplitLongWords($this->_tpl_vars['item']['eventtype']['EventName']); ?>
<br>
							<!-- <input type="button" value="Complete" /> -->
						</div>
						<div class="task_icon">
							<?php echo $this->_tpl_vars['item']['Priority']; ?>

						</div>
					<div class="clear"></div>
					</div>
				<?php else: ?>
					<?php echo $this->_tpl_vars['item']['date_string']; ?>
<br>
					<?php echo $this->_tpl_vars['item']['start_time_string']; ?>
 - <?php echo $this->_tpl_vars['item']['end_time_string']; ?>
 (<?php echo $this->_tpl_vars['item']['duration_string']; ?>
)
				<?php endif; ?>
			</div>
			<div <?php if ($this->_tpl_vars['item']['Closed']): ?>class="closedevent"<?php else: ?>class="openevent"<?php endif; ?>>
				<?php if ($this->_tpl_vars['item']['is_viewable']): ?>
					<?php if ($this->_tpl_vars['item']['eventtype']['Type'] != 'TASK'): ?>
						<p class="subtitle"><?php echo SplitLongWords($this->_tpl_vars['item']['eventtype']['EventName']); ?>
</p>
					<?php endif; ?>
					<table class="sidebar_tbl">
						<tr>
							<td class="label"><span>User</span></td>
							<td><?php echo SplitLongWords($this->_tpl_vars['item']['user']['FirstName']); ?>
 <?php echo SplitLongWords($this->_tpl_vars['item']['user']['LastName']); ?>
</td>
						</tr>
						<?php if ($this->_tpl_vars['item']['AccountID']): ?>
						<tr>
							<td class="label"><span>Company</span></td>
							<td>
								<a onclick='javascript:NoBubble(event,"?action=company&accountId=<?php echo $this->_tpl_vars['item']['AccountID']; ?>
");'>
									<?php echo $this->_tpl_vars['item']['AccountName']; ?>

								</a>
							</td>
						</tr>
						<?php elseif ($this->_tpl_vars['item']['Contact']['FirstName']): ?>
						<tr>
							<td class="label"><span>Contact</span></td>
							<td>
								<a onclick='javascript:NoBubble(event,"?action=contact&contactId=<?php echo $this->_tpl_vars['item']['ContactID']; ?>
");'>
									<?php echo SplitLongWords($this->_tpl_vars['item']['Contact']['FirstName']); ?>
 <?php echo SplitLongWords($this->_tpl_vars['item']['Contact']['LastName']); ?>

								</a>
							</td>
						</tr>
						<?php endif; ?>

						<?php if ($this->_tpl_vars['item']['Contact']['CompanyName']): ?>
						<tr>
							<td class="label"><span>Company</span></td>
							<td>
								<a onclick='javascript:NoBubble(event,"?action=company&accountId=<?php echo $this->_tpl_vars['item']['Contact']['AccountID']; ?>
");'>
									<?php echo SplitLongWords($this->_tpl_vars['item']['Contact']['CompanyName']); ?>

								</a>
							</td>
						</tr>
						<?php endif; ?>
						<tr>
							<td class="label"><span>Subject</span></td>
							<td><?php echo SplitLongWords($this->_tpl_vars['item']['Subject']); ?>
</td>
						</tr>
						<?php if ($this->_tpl_vars['item']['Location']): ?>
							<tr>
								<td class="label"><span>Location</span></td>
								<td><?php echo SplitLongWords($this->_tpl_vars['item']['Location']); ?>
</td>
							</tr>
						<?php endif; ?>
					</table>
				<?php else: ?>
					<span class="label">User</span> <?php echo SplitLongWords($this->_tpl_vars['item']['user']['FirstName']); ?>
 <?php echo SplitLongWords($this->_tpl_vars['item']['user']['LastName']); ?>
<br />
					Personal
				<?php endif; ?>
			</div>
		</div>
	<?php endforeach; endif; unset($_from); ?>
<?php else: ?>
	<span class='BlackBold'><?php echo $this->_tpl_vars['module']->msg; ?>
</span>
<?php endif; ?>