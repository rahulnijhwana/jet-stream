<?php
if (!defined('BASE_PATH')) {
	define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));
}

require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';
require_once BASE_PATH . '/include/class.RecEvent.php';
require_once 'class.Event.php';
require_once BASE_PATH . '/include/class.RecEventAttendee.php';
require_once "class.EventTypeLookup.php";
require_once BASE_PATH . '/include/lib.dblookup.php';
require_once "class.JetstreamModule.php";

require_once 'class.Note.php';

define ('ADD', 0);
define ('EDIT', 1);


class ModuleOppTrackerNew extends JetstreamModule
{
	protected $javascript_files = array('ui.datepicker.js', 'jquery.cluetip.js');
	protected $css_files = array('event.css', 'ui.datepicker.css', 'mini_calendar.css', 'jquery.cluetip.css');
	
	private $company_id;
	private $user_id;
	public $title = 'Opportunity Information';
	public $event_info;
	public $attendees;
	
	public $repeat_types = array('Does not repeat', 'Daily', 'Weekly', 'Monthly', 5 => 'Yearly');
	public $weekdays = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
	public $pos_locs = array(1 => 'First', 'Second', 'Third', 'Fourth', 'Last');
	public $pos_types = array(7 => 'day', 8 => 'weekday', 9 => 'weekend day', 0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday');
							
	public $form_id;
	public $submit_to = "ajax.Event.php";
	
	function __construct($company_id = -1, $user_id = -1) {
		$this->company_id = ($company_id == -1) ? $_SESSION['company_obj']['CompanyID'] : $company_id;
		$this->user_id = ($user_id == -1) ? $_SESSION['USER']['USERID'] : $user_id;
	}

	public function Build($rec_type, $rec_id, $action = 'display', $limit_access = false, $repeat_instance = false, $default_data = false) {
		// Load data, populate new record
		if ($action != 'add') {
			$event_sql = "SELECT * FROM Event WHERE EventID = ?";
			$event_sql = SqlBuilder()->LoadSql($event_sql)->BuildSql(array(DTYPE_INT, array($rec_id)));
			
			// $record_sql = "SELECT * FROM Event WHERE EventID = ? AND CompanyID = ?";
			// $record_sql = SqlBuilder()->LoadSql($record_sql)->BuildSql(array(DTYPE_INT, array($id, $this->company_id)));
			

			$recordset = DbConnManager::GetDb('mpower')->Execute($event_sql, 'RecEvent');
			
			$recordset->Initialize();
			$this->event_info = $recordset[0];
			
			// Default data is used because edit opp stores form value until the opp is saved
			// This way if they re-edit an event, they can see their original edits
			if ($default_data) {
				foreach ($default_data as $field => $value) {
					$this->event_info[$field] = $value;
				}
			}
			
			// Get any needed lookup data
			$this->event_info['EventType'] = EventTypeLookup::GetRecord($this->company_id, $this->event_info['EventTypeID']);
			$this->event_info['User'] = ReportingTreeLookup::GetRecord($this->company_id, $this->event_info['UserID']);
			
			// echo $this->event_info['EventID'] . ' ' .  $this->event_info['RepeatParent'] . '<br>';
			
			if ($repeat_instance && (!$this->event_info->InRecord('RepeatParent') || $this->event_info['EventID'] == $this->event_info['RepeatParent'])) {
				// The user has asked to load an instance of a repeating event that isn't a separate ID
				// So clone the series for the instance instance date
				$this->event_info = CloneEvent($this->event_info, $repeat_instance);
				$this->event_info['RepeatType'] = REPEAT_NONE;
				$this->event_info['RepeatParent'] = $rec_id;
				unset($this->event_info['EventID']);
			}
			
			// Translate data for display
			$this->event_info['StartTimeStamp'] = strtotime($this->event_info['StartDate']);
			$this->event_info['EndTimeStamp'] = strtotime($this->event_info['EndDate']);
			$this->event_info['RepeatEndTimeStamp'] = strtotime($this->event_info['RepeatEndDate']);
			$this->event_info['Duration'] = $this->event_info['EndTimeStamp'] - $this->event_info['StartTimeStamp'];
			
			// Ensure that the page chooses the month or year section when they choose either sub-section
			$this->event_info['repeat_selector'] = ($this->event_info['RepeatType'] == 4 || $this->event_info['RepeatType'] == 6) ? $this->event_info['RepeatType'] - 1 : $this->event_info['RepeatType'];
			
			// Extract the repeat interval data
			$repeat_detail = unserialize($this->event_info['RepeatInterval']);
			$repeat_interval = array();
			switch($this->event_info['RepeatType']) {
				case REPEAT_NONE :
					break;
				case REPEAT_DAILY :
					$repeat_interval['daily_freq'] = $repeat_detail[0];
					break;
				case REPEAT_WEEKLY :
					list($repeat_interval['weekly_freq'], $repeat_interval['weekly_days']) = $repeat_detail;
					break;
				case REPEAT_MONTHLY :
					list($repeat_interval['monthly_freq'], $repeat_interval['monthly_day']) = $repeat_detail;
					break;
				case REPEAT_MONTHLY_POS :
					list($repeat_interval['monthly_freq'], $repeat_interval['monthly_pos'], $repeat_interval['monthly_pos_type']) = $repeat_detail;
					break;
				case REPEAT_YEARLY :
					list($repeat_interval['yearly_freq'], $repeat_interval['yearly_month'], $repeat_interval['yearly_month_day']) = $repeat_detail;
					break;
				case REPEAT_YEARLY_POS :
					list($repeat_interval['yearly_freq'], $repeat_interval['yearly_pos'], $repeat_interval['yearly_pos_type'], $repeat_interval['yearly_pos_month']) = $repeat_detail;
					break;
			}
			$this->repeat_interval = $repeat_interval;

			$attendee_sql = 'SELECT * FROM EventAttendee WHERE EventID = ? AND (Deleted != 1 OR Deleted IS NULL)';
			
			if (is_object($this->event_info) && $this->event_info->InRecord('EventID')) {
				$attendee_source = $this->event_info['EventID'];
			} else {
				$attendee_source = $this->event_info['RepeatParent'];
			}
			
			$attendee_sql = SqlBuilder()->LoadSql($attendee_sql)->BuildSql(array(DTYPE_INT, $attendee_source));
			$this->attendees = DbConnManager::GetDb('mpower')->Execute($attendee_sql, 'RecEventAttendee', 'PersonID');
			$this->attendees->Initialize();
		} else {
			date_default_timezone_set($_SESSION['USER']['BROWSER_TIMEZONE']);
			// date_default_timezone_set($_SESSION['USER']['TIMEZONE']);
			
			// $date_array = getdate();
			// $this->event_info['StartTimeStamp'] = mktime($date_array['hours'] + 1, 0, 0, $date_array['mon'], $date_array['mday'], $date_array['year']);
			// $this->event_info['EndTimeStamp'] = strtotime("+1 hour", $this->event_info['StartTimeStamp']);
			
			//	echo 'Rec Type: ' . $rec_type . "\n";
			//	print_r($default_data);

			
			if ($rec_type == 'contact') {
				$this->event_info['ContactID'] = $rec_id;
			} elseif ($rec_type == 'event') {
				if (!empty($rec_id)) {
					$this->event_info['EventID'] = $rec_id;
				}
				if ($default_data) {
					foreach ($default_data as $field => $value) {
						
						$this->event_info[$field] = $value;
						if ($field == 'StartDate') {
							$this->event_info['StartTimeStamp'] = strtotime($value);
						}
						if ($field == 'EndDate') {
							$this->event_info['EndTimeStamp'] = strtotime($value);
						}
						
					}
				}
			}
			
			$this->attendees[] = array('PersonID' => $this->user_id, 'Creator' => 1, 'Accepted' => 1);
		}

		// print_r($this->attendees);
		foreach ($this->attendees as & $attendee) {
			if ($attendee['Creator'] == 1) {
				$this->creator = $attendee['PersonID'];
				
				// echo($this->creator) . '<br>';
			}
			$person = ReportingTreeLookup::GetRecord($this->company_id, $attendee['PersonID']);
			$attendee['name'] = $person['FirstName'] . ' ' . $person['LastName'];
		}
		
		if (empty($this->creator)) {
			$this->creator = $this->user_id;
		}
		
		if ($this->event_info['ContactID']) {
			$this->event_info['Contact'] = GetContactName($this->company_id, $this->event_info['ContactID']);
		}
			
		// echo '<pre>' . print_r($this->attendees, true) . '</pre>';
		
		
		// echo '<pre>' . print_r($this->event_info, true) . '</pre>';
		

		// $this->event_info['StartDisplay'] = date('n/j/y g:i a', $start_time);
		

		// Ensure user is okay to view this record
		

		switch ($action) {
			case 'add' :
			case 'edit' :
				$this->permitted_people = $this->GetPermittedPeople();
				$this->form_id = 'id' . uniqid();
				$this->template_files = array('form_event_new.tpl');
				break;
			default :
				// echo '<pre>' . print_r($this->data['EventID'], true) . '</pre>';
				$this->AddTemplate('module_opp_info.tpl');
			
				// Add the appropriate buttons to the module
				if ($this->event_info['Cancelled'] != 1 && $this->event_info['Closed'] != 1) {
					if ($this->event_info->InRecord('DealID') && ($this->event_info['DealID'] > 0)) {
						$this->AddButton('', "javascript:openOppPopup({$this->event_info['DealID']}, {$this->user_id})", 'Edit');
						$this->AddButton('', "javascript:openOppPopup({$this->event_info['DealID']}, {$this->user_id})", 'Complete');
						$this->AddButton('', "javascript:openOppPopup({$this->event_info['DealID']}, {$this->user_id})", 'Remove');
					} else {
						if ($this->event_info->InRecord('EventID')) {
							$event_ref = "eventid={$this->event_info['EventID']}";
							$event_encoded = '';
						} else {
							$button_id = '';
							$event_encoded = base64_encode(json_encode(array('i' => $this->event_info['RepeatParent'], 'd' => UnixToTruncatedDate($this->event_info['StartTimeStamp']), 'u' => $this->user_id)));
							$event_ref = "rep=" . $event_encoded;
						}
						
						$this->AddButton('', "javascript:ShowAjaxForm('EventNew&$event_ref&action=edit');", 'Edit');
						if ($this->event_info['RepeatType'] == 0 || !empty($this->event_info['RepeatParent'])) {
							$this->AddButton('', "javascript:CompletePendingEvent('{$this->event_info['EventID']}', '$event_encoded');", 'Complete');
						}
						$this->AddButton('', "javascript:CompletePendingEvent('{$this->event_info['EventID']}', '$event_encoded', 'remove');", 'Remove');
						
//						$this->AddButton('', "javascript:closePendingEvent($event_id, '', '', 'EVENT', true, '$date_form');", 'Complete');
//						$this->AddButton('', "javascript:cancelPendingEvent($event_id, 'EVENT', '$date_form');", 'Remove');
					}
				} else {
					$this->formatting = 'content_inactive';
				}
		}
	}

	public function SaveForm($form_values, $only_return_values = false, $source_page = '', $ignore_missing = false) {

		require_once 'class.Status.php';
		
		$status = new Status();
		
		$record = new RecEvent();

		
		// Determine what the ID is for this event (-1 for a new one)
		if (empty($form_values['EventID'])) {
			$form_type = ADD;
			$event_id = -1;
		} elseif ($form_values['EventID'] < 0) {
			$form_type = ADD;
			$event_id = $form_values['EventID'];
		} else {
			$form_type = EDIT;
			$event_id = $form_values['EventID'];
		}

		// Load the record from the database
		$sql = 'SELECT * FROM Event WHERE EventID = ?';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $event_id));
		$recordset = DbConnManager::GetDb('mpower')->Execute($sql, 'RecEvent');
		
		if ($form_type == ADD) {
			$record = new RecEvent();
			$recordset[] = $record;
			$record->SetDatabase(DbConnManager::GetDb('mpower'));
		} else {
			$record = $recordset[0];
		}
		$record->Initialize();
		
		if (!empty($form_values['DealID'])) {
			$record->DealID = $form_values['DealID'];
		}
		
		// Get the fields from the from and set the record values to them
		$record->CompanyID = $this->company_id;
		if (!empty($form_values['ContactID']) && ($form_values['ContactID'] > 0 || $form_values['ContactID'] == -1)) {
			$record->ContactID = $form_values['ContactID'];
		} else {
			$status->addError('You must select a contact', 'Contact');
			
		}
		if (!empty($form_values['EventType']) && $form_values['EventType'] > 0) {
			$record->EventTypeID = $form_values['EventType'];
			// Since date validaiton is type dependent, do it here, since we know we have a
			// valid type
			
			$event_type = EventTypeLookup::GetRecord($this->company_id, $form_values['EventType']);
			if ($event_type['Type'] == 'TASK') {
				$record->RepeatType = REPEAT_NONE;
				if (!empty($form_values['StartDate'])) {
					$startdate = strtotime($form_values['StartDate']);
					$record->StartDate = TimestampToMsSql($startdate);
				} else {
					$status->addError('You must enter a date for this task', 'When');
				}
			} elseif ($event_type['Type'] == 'EVENT') {
				if (!empty($form_values['StartDate']) && !empty($form_values['StartTime']) && !empty($form_values['EndDate']) && !empty($form_values['EndTime'])) {
					$startdate = strtotime($form_values['StartDate'] . ' ' . $form_values['StartTime']);
					$enddate = strtotime($form_values['EndDate'] . ' ' . $form_values['EndTime']);
					if ($startdate < $enddate) {
						$record->StartDate = TimestampToMsSql($startdate);
						$record->EndDate = TimestampToMsSql($enddate);
					} else {
						$status->addError('The end date/time must be later than the start date/time', 'When');
					}
				} else {
					$status->addError('All the date/time fields must be filled', 'When');
				}
			}
		} else {
			$status->addError('You must select an event type', 'EventType');
		}
		if (!empty($form_values['Subject']) && strlen($form_values['Subject']) > 0) {
			$record->Subject = $form_values['Subject'];
		} else {
			$status->addError('You must enter a subject', 'Subject');
		}
		
		$record->Location = $form_values['Location'];
		$record->Private = ($form_values['CheckPrivate'] == 'true') ? 1 : 0;
		
		// Handle repeat
		if (!empty($form_values['RepeatParent'])) {
			$record->RepeatParent = $form_values['RepeatParent'];
		}
		
		if (empty($form_values['Repeat'])) {
			$record->RepeatType = REPEAT_NONE;
		} else {
			$record->RepeatType = $form_values['Repeat'];
		}
		
		if ($record->RepeatType == REPEAT_MONTHLY) {
			if (!empty($form_values['repeat_detail']['monthly_type'])) {
				$record->RepeatType = $form_values['repeat_detail']['monthly_type'];
			} else {
				$status->addError('You must select a radio button for the type of monthly repeat', 'RepeatFreq');
			}
		}
		if ($record->RepeatType == REPEAT_YEARLY) {
			if (!empty($form_values['repeat_detail']['yearly_type'])) {
				$record->RepeatType = $form_values['repeat_detail']['yearly_type'];
			} else {
				$status->addError('You must select a radio button for the type of yearly repeat', 'RepeatFreq');
			}
		}
		
		$form_rpt_dtl = $form_values['repeat_detail'];
		$repeat_detail = array();
		if ($record->RepeatType > 0) {
			switch ($record->RepeatType) {
				case REPEAT_DAILY :
					$repeat_detail[] = $form_rpt_dtl['daily_freq'];
					break;
				case REPEAT_WEEKLY :
					$repeat_detail[] = $form_rpt_dtl['weekly_freq'];
					$repeat_detail[] = $form_rpt_dtl['weekly_day'];
					break;
				case REPEAT_MONTHLY :
					$repeat_detail[] = $form_rpt_dtl['monthly_freq'];
					$repeat_detail[] = $form_rpt_dtl['monthly_daynum'];
					break;
				case REPEAT_MONTHLY_POS :
					$repeat_detail[] = $form_rpt_dtl['monthly_freq'];
					$repeat_detail[] = $form_rpt_dtl['monthly_pos'];
					$repeat_detail[] = $form_rpt_dtl['monthly_pos_type'];
					break;
				case REPEAT_YEARLY :
					$repeat_detail[] = $form_rpt_dtl['yearly_freq'];
					$repeat_detail[] = $form_rpt_dtl['yearly_monthnum'];
					$repeat_detail[] = $form_rpt_dtl['yearly_daynum'];
					break;
				case REPEAT_YEARLY_POS :
					$repeat_detail[] = $form_rpt_dtl['yearly_freq'];
					$repeat_detail[] = $form_rpt_dtl['yearly_pos'];
					$repeat_detail[] = $form_rpt_dtl['yearly_pos_type'];
					$repeat_detail[] = $form_rpt_dtl['yearly_pos_month'];
					break;
			}
			foreach($repeat_detail as $detail_item) {
				if (empty($detail_item)) {
					$status->addError('All values for the selected repeat must be entered', 'RepeatFreq');
				}
			}
			if ($form_values['repeat_enddate_type'] == 1) {
				if (!empty($form_values['repeat_enddate'])) {
					$record->RepeatEndDate = TimestampToMsSql(strtotime($form_values['repeat_enddate']));
				} else {
					$status->addError('You must enter an end date, or choose "No end date"', 'RepeatUntil');
				}
			} else {
				$record->RepeatEndDate = NULL;
			}
		}
		$record->RepeatInterval = serialize($repeat_detail);

		$attendees = json_decode($form_values['attendee_list']);

		// TODO Validate that the attendee list before saving the event

		$results['status'] = $status->GetStatus();
		
		if ($results['status']['STATUS'] == 'OK') {
			// For edit opp forms, we only want to return an array of values - so in this case, we
			// need to skip the save process and create an array of the dirty record vals
			if ($only_return_values) {
				$data = $record->GetDirtyData();
				if (!empty($form_values['Note'])) {
					$data['Note'] = $form_values['Note'];
				}
				if (isset($data['StartDate'])) {
					$start_date = strtotime($data['StartDate']);
					$data['meetingDate'] = date('m/d/y', $start_date);
					$data['meetingTime'] = date('g:i A', $start_date);
				}
				$data['attendees'] = $attendees;

				$results['status']['RECID'] = $event_id;
				$results['event_data'] = $data;
				return $results;
			}
			
			$record->Save();
			
			// Handle attendee_list
	
			// Load all attendees of this record, index by PersonID
			$attendee_sql = 'SELECT * FROM EventAttendee WHERE EventID = ?';
			$attendee_sql = SqlBuilder()->LoadSql($attendee_sql)->BuildSql(array(DTYPE_INT, $record->EventID));
			$attendee_set = DbConnManager::GetDb('mpower')->Execute($attendee_sql, 'RecEventAttendee', 'PersonID');
			
			// Set all their delete flags (will be cleared as we process each rec from the web page)
			$attendee_set->SetAll('Deleted', true);
			
			// First attendee is creator
			$first = true;
			foreach($attendees as $attendee) {
				if (!$attendee_set->InRecordset($attendee)) {
					$attendee_rec = new RecEventAttendee();
					$attendee_set[$attendee] = $attendee_rec;
					$attendee_rec->SetDatabase(DbConnManager::GetDb('mpower'));
					//$attendee_set->Initialize();
					$attendee_rec->EventID = $record->EventID;
					$attendee_rec->PersonID = $attendee;
				}
				if ($first) {
					$attendee_set[$attendee]->Creator = 1;
					$first = false;
				} else {
					$attendee_set[$attendee]->Creator = 0;
				}
				$attendee_set[$attendee]->Deleted = false;
			}
			$attendee_set->Initialize()->Save();
	
			// If this is saving a clone of a repeating event, then we need to add an exception on the original date to the repeat parent
			// otherwise, we will end up with duplicate entries in the repeat series
			if (!empty($form_values['OrigStartDate'])) {
				$excep_sql = "SELECT RepeatExceptions FROM Event WHERE EventID = ?";
				$excep_sql = SqlBuilder()->LoadSql($excep_sql)->BuildSql(array(DTYPE_INT, $record->RepeatParent));
				$excep_set = DbConnManager::GetDb('mpower')->GetOne($excep_sql);
				$exception_string = $excep_set->RepeatExceptions;
				if (!empty($exception_string)) {
					$exceptions = explode(',', $exception_string);
				} else {
					$exceptions = array();
				}
				$exceptions[] = $form_values['OrigStartDate'];
				$excep_upd_sql = "UPDATE Event SET RepeatExceptions = ? WHERE EventID = ?";
				$excep_upd_sql = SqlBuilder()->LoadSql($excep_upd_sql)->BuildSql(array(DTYPE_STRING, implode(',', $exceptions)), array(DTYPE_STRING, $record->RepeatParent));
				DbConnManager::GetDb('mpower')->GetOne($excep_upd_sql);
			}
			
			if (!empty($form_values['Note'])) {
				// Handle note
				$note = new Note();
				$note->noteCreator = $this->user_id;
				$note->noteCreationDate = date("Y-m-d H:i:s");
				$note->noteSubject = "Event Created";
				$note->noteText = nl2br($form_values['Note']);
				$note->noteObjectType = NOTETYPE_EVENT;
				$note->noteObject = $record->EventID;
				$result_note = $note->createNote();
			}
			
			$results['status']['RECID'] = $record->EventID;
			if ($source_page == 'pending_events') {
				$results['dest'] = "?action=pending_events";
			} else {
				$results['dest'] = "?action=event&eventId=" . $record->EventID;
			}
		}
		return $results;
	}

	public function DrawForm() {
		return;
	}

	public function GetEventTypes() {
		$event_types = EventTypeLookup::GetAllRecords($this->company_id);
		if (!empty($this->event_info['DealID'])) {
			foreach($event_types as $id => $event_type) {
				if ($event_type['Type'] != 'EVENT') {
					unset($event_types[$id]);
				}
			}
		}
		return $event_types;
	}

	public function GetPeople() {
		$people = ReportingTreeLookup::GetAllActive($this->company_id);
		$people = ReportingTreeLookup::SortPeople($this->company_id, $people, 'LastName');
		$people_list = array();
		foreach($people as $person) {
			$person_rec = ReportingTreeLookup::GetRecord($this->company_id, $person);
			$people_list[$person] = "{$person_rec['FirstName']} {$person_rec['LastName']}";
		}
		return $people_list;
	}
	
	public function GetPermittedPeople() {
		$sql = "SELECT PersonID FROM People WHERE
			? IN (SupervisorID, PersonID)
			UNION
			SELECT OwnerPersonID as PersonID
			FROM CalendarPermission INNER JOIN People
				ON CalendarPermission.OwnerPersonID = People.PersonID
			WHERE CalendarPermission.PersonID = ?
				AND CalendarPermission.WritePermission = 1
				AND People.SupervisorID NOT IN (-1, -3)";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($this->user_id, $this->user_id)));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		$people = array();
		foreach ($result as $person) {
			$people[] = $person['PersonID'];
		}
		$people = ReportingTreeLookup::SortPeople($this->company_id, $people, 'LastName');
		$people_list = array();
		foreach($people as $person) {
			$person_rec = ReportingTreeLookup::GetRecord($this->company_id, $person);
			$people_list[$person] = "{$person_rec['FirstName']} {$person_rec['LastName']}";
		}
		return $people_list;
	}
	
	public function GetRepeatReadable() {
		$output = "Repeats ";
		$output .= " at " . FormatDate($this->event_info['StartTimeStamp'], 'time') . ' to ' . FormatDate($this->event_info['EndTimeStamp'], 'time');
		switch($this->event_info['RepeatType']) {
			case REPEAT_DAILY :
				$output .= " every {$this->repeat_interval['daily_freq']} day(s)";
				break;
			case REPEAT_WEEKLY :
				$output .= " every {$this->repeat_interval['weekly_freq']} week(s) on ";
				$output_days = array();
				foreach ($this->repeat_interval['weekly_days'] as $day) {
					$output_days[] = $this->weekdays[$day];
				}
				$output .= implode(', ', $output_days);
				break;
			case REPEAT_MONTHLY :
				$output .= " every {$this->repeat_interval['monthly_freq']} month(s) on the " . GetOrdinal($this->repeat_interval['monthly_day']);
				break;
			case REPEAT_MONTHLY_POS :
				$output .= " every {$this->repeat_interval['monthly_freq']} month(s) on the " . ($this->pos_locs[$this->repeat_interval['monthly_pos']]) . " " . $this->pos_types[$this->repeat_interval['monthly_pos_type']];
				break;
			case REPEAT_YEARLY :
				$output .= " every {$this->repeat_interval['yearly_freq']} year(s) on the " . GetOrdinal($this->repeat_interval['yearly_month_day']) . " of " . date('F', mktime(0, 0, 0, $this->repeat_interval['yearly_month'], 1));
				break;
			case REPEAT_YEARLY_POS :
				$output .= " every {$this->repeat_interval['yearly_freq']} year(s) on the " . ($this->pos_locs[$this->repeat_interval['yearly_pos']]) . " " . $this->pos_types[$this->repeat_interval['yearly_pos_type']] . " of " . date('F', mktime(0, 0, 0, $this->repeat_interval['yearly_pos_month'], 1));;
				break;
		}
		$output .= "<br>Starting " . FormatDate($this->event_info['StartTimeStamp'], 'date');

		if (!empty($this->event_info['RepeatEndDate'])) {
			$output .= ", ending " . FormatDate($this->event_info['RepeatEndTimeStamp'], 'date');
		} else {
			$output .= " with no end date";
		}

		// $output .= "<br>Next appears on ";
		
		return $output;
	}
}

function GetOptionsTime($selected) {
	if (empty($selected)) {
		$selected_time = -1;
	} else {
		$selected_array = getdate($selected);
		$selected_time = mktime($selected_array['hours'], $selected_array['minutes'], 0);
	}
	
	$day_length = 86400; // 24 hours * 60 minutes * 60 seconds
	$increment = 900; // 15 minutes * 60 seconds
	

	$options = '';
	
	$start = mktime(0, 0, 0);
	
	for($timehack = $start; $timehack < $start + $day_length; $timehack += $increment) {
		if ($timehack == $selected_time) {
			$selected_text = "selected";
		} else {
			$selected_text = '';
		}
		$date_string = date("g:i a", $timehack);
		$options .= "<option id='$date_string' $selected_text>$date_string</option>\n";
		if ($selected_time == -1 && date("g:i a", $timehack) == '12:00 pm') {
			$options .= "<option selected />";
		}
	}
	
	return $options;
}

function FormatDate($date, $show = "full") {
	switch ($show) {
		case "full" :
			return date('n/j/y g:i a', $date);
		case "date" :
			return date('n/j/y', $date);
		case "time" :
			return date('g:i a', $date);
		case "trunc" :
			return date('Ymd', $date);
	}
}

function GetOptionsNumRng($selected, $start, $end, $ordinal = false) {
	$options = '<option />';
	for ($num = $start; $num <= $end; $num++) {
		$selected_text = ($num == $selected) ? "selected" : "";
		$disp_num = ($ordinal) ? GetOrdinal($num) : $num;
		$options .= "<option value='$num' $selected_text>$disp_num</option>";
	}
	return $options;
}

function GetOptionsMonths($selected) {
	$options = '<option />';
	for ($month = 1; $month < 13; $month++) {
		$selected_text = ($month == $selected) ? "selected" : "";
		$options .= "<option value='$month' $selected_text>" . date('F', mktime(0, 0, 0, $month, 1)) . "</option>";
	}
	return $options;
}

function GetOrdinal($number) {
	$ext = 'th';
	if (abs($number) % 100 < 4 || abs($number) % 100 > 20) {
		switch (abs($number) % 10) {
			case 1 :
				$ext = 'st';
				break;
			case 2 :
				$ext = 'nd';
				break;
			case 3 :
				$ext = 'rd';
				break;
		}
	}
	return $number . $ext;
}

