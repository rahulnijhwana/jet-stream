<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/slipstream/class.Status.php';
require_once BASE_PATH . '/include/class.fieldsMap.php';
require_once BASE_PATH . '/include/class.ReportingTree.php';
require_once BASE_PATH . '/slipstream/class.Util.php';
require_once BASE_PATH . '/slipstream/class.RecurringEventInfo.php';
require_once BASE_PATH . '/slipstream/class.OppInfo.php';
require_once BASE_PATH . '/slipstream/class.Contact.php';
require_once BASE_PATH . '/slipstream/class.Opps.php';
require_once BASE_PATH . '/slipstream/lib.php';
require_once BASE_PATH . '/slipstream/class.EventTypeLookup.php';

/**
 * The DB fields initialization
 */
class RecurringEvent
{
	public $eventId = 0;
	public $recurId = 0;
	public $startDate;
	public $endDate;
	public $interval;
	public $recurType; 	//daily, weekly, monthly, yearly
	public $companyId;
	public $userId;
	public $recurAction;
	public $isActive = FALSE;


	function __construct($company_id, $user_id) {
		$this->companyId = $company_id;
		$this->userId = $user_id;
	}


	/**
	 * function SaveRecurringEvent
	 */
	function SaveRecurringEvent() {
		try {
			/**
			 * create Status() class object
			 */
			$status = new Status();

			/**
			 * Validates the event fields.
			 */
			$status = $this->DoValidateEvent();			

			/**
			 * If the stauts is Ok then save the event data to the db.
			 * Else return the error.
			 */
			if ($status->status == 'OK') {

				$sql = "SELECT * FROM RecurringEvent WHERE RecurringEventID =?";
				$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->recurId));
				$result = DbConnManager::GetDb('mpower')->Execute($sql, 'RecurringEventInfo');

				$recur_event_info = new RecurringEventInfo();

				if ($this->recurAction == 'add') {
					$recur_event_info->SetDatabase(DbConnManager::GetDb('mpower'));
					$result[] = $recur_event_info;
				} else {
					$recur_event_info = $result[0];
					$recur_event_info->RecurringEventID = $this->recurId;

				}
				$result->Initialize();


				$recur_event_info->Type = $this->recurType;
				$recur_event_info->StartDate = $this->startDate;
				$recur_event_info->EndDate = $this->endDate;
				$recur_event_info->Interval = $this->interval;

                $recur_event_info->CompanyID = $this->companyId;
				$recur_event_info->UserID = $this->userId;

				$recur_event_info->IsActive = $this->isActive;
				$result->Save();
				
				$recur_id = $recur_event_info->RecurringEventID;
				$status->setFormattedVar($recur_id);


			}
		} catch (Exception $e) {
			$status->addError($e->getMessage());
		}


		return $status->getStatus();
	}


	/**
	 * function doValidateEvent
	 * Validates the event fields.
	 */
	function DoValidateEvent() {
		/**
		 * create Status() class object
		 * create Util() class object
		 */
		$status = new Status();
		$obj_util = new Util();


		/**
		 * Start date is a required field.
		 * Start date should be equal to or greater than the current date.
		 */
		if (!isset($this->startDate) || trim($this->startDate) == "") {
			$status->addError('Enter the Start Date', 'StartDate');
		} elseif (!$obj_util->isValidDate($this->startDate, '/')) {
			$status->addError('Invalid charcters', 'StartDate');
		} else {
			$entered_st_date = strtotime(foramtDateTime($this->startDate, '', 'date'));
			$cur_date = strtotime(date('Y-m-d'));			
			
			/*if ($entered_st_date < $cur_date) {
				$status->addError('Enter upcoming date. Current date is ' . date('Y-m-d'), 'StartDate');
			}*/
		}

		return $status;
	}

	
	public function UpdateEventStatus($event_id, $date, $action) {
		
		$cancel = ($action  == 'cancel') ? 1 : 0;
		$close = ($action  == 'close') ? 1 : 0;
		
		/*$sql = "Insert into events(ContactID, UserID, EventTypeID, Subject, DateEntered, DateModified, 
				DurationHours, DurationMinutes, StartDate, PrivateEvent, IsCancelled, IsClosed,
				LastModifiedUserID, RecurringEventID) 

				select ContactID, UserID, EventTypeID, Subject, DateEntered, DateModified, 
				DurationHours, DurationMinutes, ? as StartDate, PrivateEvent, ?, ?,
				LastModifiedUserID, RecurringEventID from events where eventid = ?";*/
		
		// Select the default event
		$sql_event = 'Select ContactID, UserID, EventTypeID, Subject, DateEntered, DateModified, 
				DurationHours, DurationMinutes, StartDate, PrivateEvent, 
				LastModifiedUserID, RecurringEventID from events where eventid = ?';
		$params[] = array(DTYPE_INT, $event_id);
		$sql_event = SqlBuilder()->LoadSql($sql_event)->BuildSqlParam($params);
		$event_info = DbConnManager::GetDb('mpower')->Exec($sql_event);	
		$params = array();
		$event_info = $event_info[0];
		
		// save the record
		$sql = 'Insert into events(ContactID, UserID, EventTypeID, Subject, DateEntered, DateModified, 
				DurationHours, DurationMinutes, StartDate, PrivateEvent, IsCancelled, IsClosed,
				RecurringEventID) Values(?, ?, ?, ?, ?, ?, 
				?, ?, ?, ?, ?, ?, ?)';
		
		$params[] = array(DTYPE_INT, $event_info['ContactID']);
		$params[] = array(DTYPE_INT, $event_info['UserID']);
		$params[] = array(DTYPE_INT, $event_info['EventTypeID']);
		$params[] = array(DTYPE_STRING, $event_info['Subject']);
		$params[] = array(DTYPE_STRING, $event_info['DateEntered']);
		$params[] = array(DTYPE_STRING, $event_info['DateModified']);		
		$params[] = array(DTYPE_INT, $event_info['DurationHours']);
		$params[] = array(DTYPE_INT, $event_info['DurationMinutes']);
		
		
		$event_ts = strtotime($event_info['StartDate']);		
		$ts = strtotime($date);
		$date = date('Y-m-d', $ts);		
		$params[] = array(DTYPE_STRING, $date.' '.date('h:i:s', $event_ts).'.000' );
		//$params[] = array(DTYPE_STRING, $event_info['StartDate']);	
		// date format : 2009-07-21 20:40:00.000		
		
		$params[] = array(DTYPE_INT, $event_info['PrivateEvent']); 
		$params[] = array(DTYPE_INT, $cancel);
		$params[] = array(DTYPE_INT, $close);
		$params[] = array(DTYPE_INT, $event_info['RecurringEventID']);		
		
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);		
		$status = DbConnManager::GetDb('mpower')->Exec($sql);
		
		$sql = "SELECT SCOPE_IDENTITY() as idno;";
		$last_inert_id = DbConnManager::GetDb('mpower')->Exec($sql);		
		$last_inert_id = $last_inert_id[0]['idno'];
		
		return array('Status'=>$status, 'ID'=>$last_inert_id);			
	}
	

   function GetRecurringEventsByDateRange($startDate, $endDate, $target, $type = 'BOTH') {

		$sql = "SELECT * FROM RecurringEvent
				WHERE CompanyID = ? AND
					(EndDate >= ? OR EndDate IS NULL) AND StartDate <= ? AND (IsActive = 1)";


		$params[] = array(DTYPE_INT, $this->companyId);
		$params[] = array(DTYPE_STRING, $startDate);
		$params[] = array(DTYPE_STRING, $endDate);


		if (is_array($target) && count($target) > 0) {
			$sql .= " AND UserID in (" . ArrayQm($target) . ")";
			$params[] = array(DTYPE_INT, $target);
		} else {
			$sql .= " AND UserID = ?";
			if (!is_array($target) && $target != '') {
				$params[] = array(DTYPE_INT, $target);
			} else {
				$params[] = array(DTYPE_INT, $this->userId);
			}
		}

		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		$recurring_events = DbConnManager::GetDb('mpower')->Exec($sql);

        $event_list = array();

        foreach($recurring_events as $recurring_event) {

			// get the event id
			$sql_event = "SELECT *, CONVERT(VARCHAR(19), StartDate, 120) Edate FROM Events WHERE RecurringEventID = ?";
			$sql_event = SqlBuilder()->LoadSql($sql_event)->BuildSql(array(DTYPE_INT, $recurring_event['RecurringEventID']));
			$event_info = DbConnManager::GetDb('mpower')->Exec($sql_event);
					
			
			$deleted_event_id_list = array();			
			
			foreach($event_info as $event_temp) {
				//if($event_temp['IsCancelled'] == 1) {
					$temp_ts = strtotime($event_temp['Edate']);					
					$deleted_event_id_list[] = date('Y-m-d', $temp_ts);					
				//}				
			}			
			

			//Load the event
			$event_id = (isset($event_info[0]['EventID'])) ? $event_info[0]['EventID'] : false;
			$event_obj = new Event();
			$event = $event_obj->GetEventById($event_id, false);

            $type = $recurring_event['Type'];
            $start_date = $recurring_event['StartDate'];
            $end_date = $recurring_event['EndDate'];

            // check which date is the earliest
            if(!empty($end_date)) {
                $end_date = (strtotime($end_date) - strtotime($endDate) > 0) ? $endDate : $end_date;
            } else {
                $end_date = $endDate;
            }

            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
			$recurring_type = (int) $recurring_event['Type'];
			$day_list = array();
			
			switch($recurring_type) {              
				case 2:
				case 3:
				case 4:				
				case 5: // weekly
					$interval = unserialize($recurring_event['Interval']);

					if(is_array($interval) && count($interval) > 0) {
						$interval = $interval;
						$day_list = $this->getDateRange($start_date, $end_date, $interval);
					} else {					
						$day_list = $this->getMonthRange($start_date, $end_date, 7, 1);						
					}
					
					break;				
				case 1: // daily					
				case 6: // monthly					
				case 7: // yearly
					$interval = (int) $recurring_event['Interval'];		
					$day_list = $this->getMonthRange($start_date, $end_date, $interval, $recurring_type);	
					break;
            }		
			
            foreach($day_list as $day) {
				$event_temp = $event[0];
				$ts = strtotime($event_temp['StartDate']);
                $event_temp['StartDate'] = $day.' '.date('H:i', $ts);
				$event_temp['VirtualEvent'] = 1;				
				$event_temp['IsCancelled'] = 0;
				$event_temp['IsClosed'] = 0;
				
				if(!in_array($day, $deleted_event_id_list)) {					
					$event_list[] = $event_temp;
				}
            }

        }

		return $event_list;
	}


	function getMonthRange($start_date, $end_date, $interval, $opt=6) {
		$day_list = array();
		
		$ts_start = strtotime($start_date);
		$ts_end = strtotime($end_date);		
		
		while ($ts_start <= $ts_end) {	

			if($ts_start <= $ts_end) {
				$day_list[] = date('Y-m-d', $ts_start);			
			} 
			
			$month = ($opt == 6) ? date('m', $ts_start) + $interval : date('m', $ts_start);			
			$day = ($opt == 1) ? date('d', $ts_start) + $interval : date('d', $ts_start);			
			$year = ($opt == 7) ? date('y', $ts_start) + $interval : date('y', $ts_start);
			
			$ts_start = mktime(0, 0, 0, $month, $day, $year);
		}
		
		return $day_list;
	}

    function getDateRange($start_date, $end_date, $week_days) {
        
		$day_list = array();		
		$week_start_date = GetWeekStartDate($start_date);		
		
		if(is_array($week_days) && !empty($week_days)) {
			foreach($week_days as $week_day) {
				$week_day = ($week_day == 0) ? 7 : $week_day;				
				$ts = strtotime($week_start_date);
				$ts = mktime(0, 0, 0, date('m', $ts), date('d', $ts) + $week_day, date('y', $ts));				
				$start_date_temp = date('Y-m-d', $ts);				
								
				$day_list_a = $this->getMonthRange($start_date_temp, $end_date, 7, 1);				
				$day_list = array_merge($day_list, $day_list_a);				
			}
		}		
		
		
		// filter the days
		$month_day_list = array();
		$start_date_ts = strtotime($start_date);
		
		foreach ($day_list as $day) {
			$day_ts = strtotime($day);
			if($day_ts >= $start_date_ts) {
				$month_day_list[] = date('Y-m-d', $day_ts);
			}
		}	
		
		return $month_day_list;
    }



    public function DaysDiff($dateBegin, $dateEnd) {
        $days_diff = floor(strtotime($dateEnd) - strtotime($dateBegin)) / 86400;
        return $days_diff;
    }


}
