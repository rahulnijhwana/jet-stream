<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once 'class.Status.php';

class NoteTemplate extends MpRecord
{

	public function __toString()
	{
		return 'Note Template Record';
	}

	public function Initialize()
	{
		$this->db_table = 'NoteTemplate';
		$this->recordset->GetFieldDef('NoteTemplateID')->auto_key = TRUE;
		parent::Initialize();
	}
	
	public function saveNoteTemplate($compId,$TempName,$formType){
		if($formType == 0)
			$formType = -1;
		$status = new Status();
		$sql = "SELECT * FROM NoteTemplate WHERE NoteTemplateID = -1";
		$rs = DbConnManager::GetDb('mpower')->Execute($sql);
		$this->SetDatabase(DbConnManager::GetDb('mpower'));
		$this->recordset = $rs;
		$this->Initialize();
		$this->CompanyID = $compId;
		$this->TemplateName = $TempName;
		$this->FormType = $formType;
		$this->Save();
		
		$selectSql = "Select NoteTemplateID,TemplateName,FormType from NoteTemplate where NoteTemplateID =(select Max(NoteTemplateID) from NoteTemplate where CompanyID=? AND FormType=? AND TemplateName=?)";
		$sqlBuild = SqlBuilder()->LoadSql($selectSql);
		$selectSql = $sqlBuild->BuildSql(array(DTYPE_INT, $compId),array(DTYPE_STRING, $formType),array(DTYPE_STRING, $TempName));
		$noteTemplate = DbConnManager::GetDb('mpower')->Exec($selectSql);

		return $noteTemplate;
	}
	
	function getNoteTemplatesByFormTypeAndCompId($companyId,$formType)
	{
		if($formType == NULL || $formType == 0)
			$sql = "select NoteTemplateID,TemplateName,FormType from NoteTemplate where CompanyID = ? AND Deleted = 0";
		else
			$sql = "select NoteTemplateID,TemplateName,FormType from NoteTemplate where CompanyID = ? AND Deleted = 0 AND FormType =".$formType;
			
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId));
		$noteTemp = DbConnManager::GetDb('mpower')->Exec($sql);
		
		for($i=0;$i<count($noteTemp);$i++){
			if($noteTemp[$i]['FormType'] == -1)
				$noteTemp[$i]['FormType'] = 'Default';
			else if($noteTemp[$i]['FormType'] == 1)
				$noteTemp[$i]['FormType'] = 'Account';
			else if($noteTemp[$i]['FormType'] == 2)
				$noteTemp[$i]['FormType'] = 'Contact';
			else if($noteTemp[$i]['FormType'] == 3)
				$noteTemp[$i]['FormType'] = 'Event';
		}
		return $noteTemp;
	}
	
	function deleteNoteTemplateByTempId($tempId){
		$status = new Status();
		$sql = "SELECT * FROM NoteTemplate WHERE NoteTemplateID = ?";
		$noteTempSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $tempId));
		$result = DbConnManager::GetDb('mpower')->Execute($noteTempSql, 'NoteTemplate');
		if (count($result) == 1) {
			$objNoteTemp = $result[0];
			$objNoteTemp->Initialize();
			$objNoteTemp->Deleted = true;
			$result->Save();
		}
		return $status;
	}
	
	function getNoteTemplateFields($compId,$tempId){
		$sql = "select NoteTemplateID,TemplateName,FormType from NoteTemplate where CompanyID = ? AND NoteTemplateID = ? ";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $compId),array(DTYPE_INT, $tempId));
		$noteTemp = DbConnManager::GetDb('mpower')->Exec($sql);
		return $noteTemp;
	}
	
	function updateNoteTemplate($tempId,$tempName){
		$status = new Status();
		$sql = "SELECT * FROM NoteTemplate WHERE NoteTemplateID = ? ";
		$noteTempSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $tempId));
		$result = DbConnManager::GetDb('mpower')->Execute($noteTempSql, 'NoteTemplate');
		if (count($result) == 1) {
			$noteTemp = $result[0];
			$noteTemp->Initialize();
			$noteTemp->TemplateName = $tempName;
			$result->Save();
		}
		return $status;
	}
}

?>