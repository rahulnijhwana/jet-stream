<?php
require_once BASE_PATH . '/slipstream/class.Opportunity.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.Contact.php';
require_once BASE_PATH . '/slipstream/class.Account.php';

class ModuleOpportunitySidebar extends JetstreamModule
{
	public $title = 'Opportunities';
	protected $javascript_files = array('overlay.js', 'jquery.bgiframe.js', 'jquery.autocomplete.js', 'event.js', 'stringexplode.js', 'ui.datepicker.js', 'jquery.cluetip.js', 'jquery.hoverIntent.js');
	protected $css_files = array('event.css', 'ui.datepicker.css', 'overlay.css', 'jquery.autocomplete.css', 'mini_calendar.css', 'jquery.cluetip.css');
	protected $buttons = array();
	public $sidebar = TRUE;
	public $target;
	public $msg;
	
	public $id = 'opportunity_sidebar';
	
	public $type;
	public $rec_id;
	public $company_id;
	public $user_id;
	public $empty_module;
	public $is_restricted = UNKNOWN;
	public $inactive_contact;
	public $usage;

	public function Init() {
		if (!isset($this->type)) {
			if (isset($_GET['contactId'])) {
				$this->usage = 'CONTACT';
			} elseif (isset($_GET['accountId'])) {
				$this->usage = 'ACCOUNT';
			}
		}

		if (!isset($this->rec_id)) {
			if (isset($_GET['contactId'])) {
				$this->rec_id = $_GET['contactId'];
			} elseif (isset($_GET['accountId'])) {
				$this->rec_id = $_GET['accountId'];
			}
		}
		
		if (!isset($this->type)) {
				$this->type = $_SESSION['type'];
		}
		
		if (!isset($this->usage)) {
				$this->usage = $_SESSION['usage'];
		}
		if($this->usage == 'ACCOUNT'){
		$filter = array('type' => ACCOUNT, 'rec_id' => $this->rec_id);
		}
		if($this->usage == 'CONTACT'){
		$filter = array('type' => CONTACT, 'rec_id' => $this->rec_id);
		}
		
		
		$this->user_id = $_SESSION['USER']['USERID'];
		$this->company_id = $_SESSION['company_obj']['CompanyID'];
	}

	public function Draw() {
		$this->AddTemplate('module_opportunity.tpl');
		
		$this->msg = 'None found';
		
		$obj_opportunity = new Opportunity();
		
		$today = date("m/d/Y");
		$today_dt_time = date("Y-m-d H:i:s");
		$this->curDate = 'var cur_date = \'' . json_encode($today) . '\';';
		
		$objContact = new Contact();
		$objAccount = new Account();
		if (!$this->empty_module && $this->is_restricted == UNKNOWN) {
			switch ($this->type) {
				case 'contact' :
					$this->is_restricted = $objContact->IsRestricted($this->rec_id, $this->user_id, $this->company_id);
					break;
				case 'account' :
					$this->is_restricted = $objAccount->IsRestricted($this->rec_id, $this->user_id, $this->company_id);
					break;
				default :
					$this->is_restricted = FALSE;
			}
		}
		
		if ($this->is_restricted === TRUE) {
			$this->msg = "You are not allowed to view upcoming opportunities.";
			return;
		}
		$filter = Array();
		if($this->usage == 'ACCOUNT'){
		//$this->AddButton('add20.png', "#top\" onclick=\"javascript:ShowAjaxForm('OpportunityNew&contactid={$this->rec_id}&action=add');", 'Add Opportunity');
		$filter = array('type' => ACCOUNT, 'rec_id' => $this->rec_id);
		}
		
		if($this->usage == 'CONTACT'){
		//$this->AddButton('add20.png', "#top\" onclick=\"javascript:ShowAjaxForm('OpportunityNew&contactid={$this->rec_id}&action=add');", 'Add Opportunity');
		$filter = array('type' => CONTACT, 'rec_id' => $this->rec_id);
		}
		
		switch ($this->type) {			
			case 'Active' :	
				$this->title = 'Active Opps';
				if($this->usage == 'CONTACT'){
				$this->AddButton('add20.png', "#top\" onclick=\"javascript:ShowAjaxForm('OpportunityNew&contactid={$this->rec_id}&action=add');", 'Add');				
				}
				if (!$this->empty_module) {
					$this->opportunities = $obj_opportunity->GetOpportunitiesByDateRange(date("m/d/Y", strtotime("-6 months", time())), date("m/d/Y", strtotime("+6 months", time())), -1, 'Active', $filter, 50, $this->usage);
				}
				break;
			
			case 'Won' :
				$this->title = 'Won Opps';
				if (!$this->empty_module) {
					$this->opportunities = $obj_opportunity->GetOpportunitiesByDateRange(date("m/d/Y", strtotime("-6 months", time())), date("m/d/Y", strtotime("+6 months", time())), -1, 'Won', $filter, 50, $this->usage);
				}
				break;
			
			case 'Lost' :
				$this->title = 'Lost Opps';
				if (!$this->empty_module) {
					$this->opportunities = $obj_opportunity->GetOpportunitiesByDateRange(date("m/d/Y", strtotime("-6 months", time())), date("m/d/Y", strtotime("+6 months", time())), -1, 'Lost', $filter, 50, $this->usage);
				}				
				break;
			default :
				$this->title = 'Active Opps';
				if (!$this->empty_module) {
					$this->opportunities = $obj_opportunity->GetOpportunitiesByDateRange(date("m/d/Y", strtotime("-6 months", time())), date("m/d/Y", strtotime("+6 months", time())), -1, 'Active', $filter, 50, $this->usage);
				}	
				break;	
		}
	}
}

?>