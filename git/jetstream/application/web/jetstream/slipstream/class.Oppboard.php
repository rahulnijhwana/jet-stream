<?php
/**
 * This file contains the note class.
 * @package database
 */

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';
require_once BASE_PATH . '/include/class.MapLookup.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

/**
 * The DB fields initialization
 */
class Oppboard extends JetstreamModule
{
	public $company_id;
	public $user_id;
	public $search;  // The search parameters
	public $query_type;
	public $is_manager;
	public $search_results;
		
	function __construct($company_id = -1, $user_id = -1) {
		$this->company_id = ($company_id == -1 && isset($_SESSION['company_obj']['CompanyID'])) ? $_SESSION['company_obj']['CompanyID'] : $company_id;
		$this->user_id = ($user_id == -1 && isset($_SESSION['USER']['USERID'])) ? $_SESSION['USER']['USERID'] : $user_id;
	}

	public function Build($search) {
		$this->search = $search;
		$this->search_results = $this->SearchResults();
	}

	public function SearchResults() {
		$search_sql = "SELECT OP.OpportunityID, OP.ContactID,OP.CompanyID ,SalesPersonID,P.FirstName, P.LastName,OP.UserID,OpportunityName,DateEntered,DateModified,OP.StartDate,ExpCloseDate,CloseDate,LastModifiedUserID, SalesCycleLocID,OP.ResultID ResultID,ResultName, R.status as status,SCLname,SCLPercent,C.Text01,C.Text02,A.Text01 CompanyName,case R.status when 1 then CloseDate when 2 then CloseDate else ExpCloseDate end as theDate,case R.status when 2 then ActTotal else EstTotal end as theValue FROM ot_opportunities OP left join ot_Results R on OP.ResultID=R.ResultID left join ot_SalesCycleLocation S on OP.SalesCycleLocID=S.SCLID, Contact C, Account A, People P where OP.ContactID=C.ContactID and C.AccountID=A.AccountID and P.PersonId=SalesPersonID and OP.CompanyID=?";
		$search_sql = SqlBuilder()->LoadSql($search_sql)->BuildSql(array(DTYPE_INT, $this->company_id));	
		$where_clause='';

		if(!isset($this->search['salesperson_list'])) {
			$this->search['salesperson_list'] = ReportingTreeLookup::GetLimb($this->company_id, $this->user_id);
		}
		$_SESSION['salesperson_list']= $this->search['salesperson_list'];
		$_SESSION['isManager']= count($_SESSION['salesperson_list']) > 1? true : false;

		if(count($this->search['salesperson_list'])>0){
			$salesperson_list = " AND (";		
			for($i=0; $i<count($this->search['salesperson_list']);$i++){
				if($i != count($this->search['salesperson_list'])-1)
				$salesperson_list .= "SalesPersonID=".$this->search['salesperson_list'][$i]." OR ";
				else
				$salesperson_list .= "SalesPersonID=".$this->search['salesperson_list'][$i].") ";
			}
			$where_clause.=$salesperson_list;
			$search_sql .= $where_clause;
		}else{
			return null;
		}
		
		$search_criteria = array();
		if(isset($this->search['search_string']) && $this->search['search_string'] != '') {
			$arr_search_string = $this->createMultiwordForSearch($this->search['search_string']);
			foreach($arr_search_string as $s_string) {
				$search_criteria[] = $s_string;
			}
		}		
		
		$param = Array();		
		if (isset($this->search['query_type']) && ($this->search['query_type'] ==4)) {
	
			if (count($search_criteria) > 0) {
				// Format the terms as the CONTAINS function requires
				array_walk($search_criteria, create_function('&$term', '$term = "%$term%";'));
				$search_terms = implode(' AND ', $search_criteria);
			} else {
				$search_terms = FALSE;
			}

			/*
			PHP Warning
			Cannot use a CONTAINS or FREETEXT predicate on table or indexed view 'ot_opportunities' 
			because it is not full-text indexed. (severity 16) 
			=> change CONTAINS with 'like operation', CONTAIN need fulltext index then this table has few rows
			*/
			if ($search_terms) {
				//$where_clause .= " AND CONTAINS(OpportunityName, ?)";
				$where_clause .= " AND OpportunityName like ?";
				$param[] = array(DTYPE_STRING, $search_terms);
			}
			$search_sql .= $where_clause;		
			$search_sql = SqlBuilder()->LoadSql($search_sql)->BuildSqlParam($param);

		}
		if (isset($this->search['query_type']) && ($this->search['query_type'] ==CONTACT)) {
	
			if (count($search_criteria) > 0) {
			$search_terms1 = ' (C.Text01 LIKE ';
			$count = 0;
			foreach($search_criteria as $term){
				$search_terms1 .= "'%".$term."%'";
				$count++;
				if($count != count($search_criteria))
				$search_terms1 .= ' OR  C.Text01 LIKE ';
			}
			$search_terms1 .= ')';
			
			$search_terms2 = ' (C.Text02 LIKE ';
			$count = 0;
			foreach($search_criteria as $term){
				$search_terms2 .= "'%".$term."%'";
				$count++;
				if($count != count($search_criteria))
				$search_terms2 .= ' OR  C.Text02 LIKE ';
			}
			$search_terms2 .= ')';
			} else {
				$search_terms1 = FALSE;
			}	
			if ($search_terms1) {				
				$where_clause = ' AND ('.$search_terms1 . ' OR '. $search_terms2.')';
				$param[] = array(DTYPE_STRING, $search_terms);
			}
			$search_sql .= $where_clause;		
		}
		if (isset($this->search['query_type']) && ($this->search['query_type'] ==ACCOUNT)) {
			if (count($search_criteria) > 0) {
			$search_terms = ' AND (A.Text01 LIKE ';
			$count = 0;
			foreach($search_criteria as $term){
				$search_terms .= "'%".$term."%'";
				$count++;
				if($count != count($search_criteria))
				$search_terms .= ' OR  A.Text01 LIKE ';
			}
					
			}else {
				$search_terms = FALSE;
			}
		
			if ($search_terms) {
				$where_clause = $search_terms .')';
				$param[] = array(DTYPE_STRING, $search_terms);
			}
			$search_sql .= $where_clause;		
		}
		
		$start_date = '';
		if(isset($this->search['age_range'])){
			if($this->search['age_range'] != 'All')
				$start_date = date("m/d/Y", strtotime("-".$this->search['age_range'], time()));
		}
		else {$start_date = date("m/d/Y", strtotime("-180 days", time()));
		$this->search['age_range'] = '180 days';
		}
		
		
		if($start_date != ''){
		$start = getdate(strtotime($start_date));
		$start = mktime(0, 0, -1, $start['mon'], $start['mday'] + 1, $start['year']);
		$start_date = date("m/d/Y H:i:s", $start);
		$search_sql .=  " AND OP.StartDate >= '".$start_date."'";;		
		//echo $start_date."testsetset: ".$search_sql ;
		//$search_sql = "SELECT OP.OpportunityID, OP.ContactID,OP.CompanyID ,SalesPersonID,P.FirstName, P.LastName,OP.UserID,OpportunityName,DateEntered,DateModified,OP.StartDate,ExpCloseDate,CloseDate,LastModifiedUserID, SalesCycleLocID,OP.ResultID ResultID,ResultName, R.status as status,SCLname,SCLPercent,C.Text01,C.Text02,A.Text01 CompanyName,case R.status when 2 then CloseDate else ExpCloseDate end as theDate,case R.status when 2 then ActTotal else EstTotal end as theValue FROM ot_opportunities OP left join ot_Results R on OP.ResultID=R.ResultID left join ot_SalesCycleLocation S on OP.SalesCycleLocID=S.SCLID, Contact C, Account A, People P where OP.ContactID=C.ContactID and C.AccountID=A.AccountID and P.PersonId=SalesPersonID and OP.CompanyID=508  AND OP.StartDate >= ? ";
		//$search_sql = SqlBuilder()->LoadSql($search_sql)->BuildSql(array(DTYPE_STRING, $start_date));			
		//echo "<br />testsetset1: ".$search_sql ;
		}
		
		if(isset($this->search['active_opp']) && $this->search['active_opp']===0){
			$search_sql .=  " AND OP.ResultID is not null ";
		}
		
		if(!isset($this->search['won_opp']) || $this->search['won_opp']===0){
			$search_sql .=  " AND (status is null or status !=2) ";
		}
		
		if(!isset($this->search['lost_opp']) || $this->search['lost_opp']===0){
			$search_sql .=  " AND (status is null or status !=1) ";
		}
		
		
		
		if(isset($this->search['sort_type']) && $this->search['sort_type'] != '') {
			
			if(isset($_SESSION[$this->search['sort_type'].'sort_by']) && $_SESSION[$this->search['sort_type'].'sort_by']=='DESC')
			$_SESSION[$this->search['sort_type'].'sort_by']= 'ASC';
			else $_SESSION[$this->search['sort_type'].'sort_by']='DESC';						

			$this->search['sort_type'] = str_replace('_','.',$this->search['sort_type']);
			if(!isset($this->search['sort_dir'])) $this->search['sort_dir']='ASC';
			
			if(!isset($_SESSION['sort_items'])) {$_SESSION['sort_items'] = $this->search['sort_type'];}
			else{
			$_SESSION['sort_items'] = str_replace(" ASC",'',$_SESSION['sort_items']);
			$_SESSION['sort_items'] = str_replace(" DESC",'',$_SESSION['sort_items']);
			$_SESSION['sort_items'] = $this->search['sort_type'].' '.$this->search['sort_dir'].','. str_replace(','.$this->search['sort_type'],'',$_SESSION['sort_items']);
			$_SESSION['sort_items'] = str_replace(','.$this->search['sort_type'],'',$_SESSION['sort_items']);
			}
			$_SESSION['sort_items'] = rtrim($_SESSION['sort_items'], ',');
						
			$search_sql .= " ORDER BY ".$_SESSION['sort_items'];

		} else {
			$search_sql .= " ORDER BY StartDate";
		}		
									
		$search_result = DbConnManager::GetDb('mpower')->Exec($search_sql);		
		
		return $search_result;
	}
	
	/**
	 * Devides the entire word to multiple word.
	 */
	public function createMultiwordForSearch($search_string) {
		$search_string = trim($search_string);
		$arr_search_string = array();
		
		$explode_dash_string = explode(' ', $search_string);
		
		if (count($explode_dash_string) > 0) {
			foreach ($explode_dash_string as $single_string) {
				//$arr_search_string[] = 	$single_string;
				$explode_comma_string = explode(',', $single_string);
				
				if (count($explode_comma_string) > 0) {
					foreach ($explode_comma_string as $single_comma_string) {
						$arr_search_string[] = $single_comma_string;
					}
				}
			}
		} else {
			$explode_comma_string = explode(',', $search_string);
			
			if (count($explode_comma_string) > 0) {
				foreach ($explode_comma_string as $single_comma_string) {
					$arr_search_string[] = $single_comma_string;
				}
			}
		}
		return $arr_search_string;
	}
}
