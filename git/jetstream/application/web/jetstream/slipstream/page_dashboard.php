<?php
/*
 * Jetstream dashboard main
 */
 
require_once BASE_PATH . '/slipstream/class.JetstreamPage.php';
require_once BASE_PATH . '/slipstream/class.ModuleCalendar.php';
require_once BASE_PATH . '/slipstream/class.ModuleMiniCalendar.php';
require_once BASE_PATH . '/slipstream/class.ModuleUpcomingEvents.php';
require_once BASE_PATH . '/slipstream/class.ModuleSavedSearch.php';
require_once BASE_PATH . '/include/lib.string.php';

$page = new JetstreamPage();

$module = new Dashboard();
$module->title = 'Search Jetstream Records';
$module->AddMenuItem('Basic', "javascript:showSearch('basic');",'selected');
$module->AddMenuItem('Advanced', "javascript:showSearch('superadvance');");
$module->AddTemplate('module_simple_search.tpl');
$module->AddTemplate('snippet_merge_record.tpl');
$module->AddJavascript('jquery.js', 'slipstream_search.js', 'jquery.form.js');
$module->AddCss('search.css');
$module->search_types = array(3 => 'Company + Contact', 1 => 'Company', 2 => 'Contact');
$module->records_per_page = array(25, 50, 75, 100);

$module->id = 'dashboard_simple_search';//stuff will break if you change this

if(!$_SESSION['USER']['SPECIAL_PERMISSIONS_ID']){
	/*
	 * Exception for companies who's level 1 users only see records assigned to them
	 */
	$onlyAssigned = array(
		636,
		612,
		619,
		557,
		508	
	);
	if($_SESSION['USER']['LEVEL'] == 1 && in_array($_SESSION['company_id'], $onlyAssigned)){
		$assigned_search = 1;
		$suppress = true;
	}
	else {
		$assigned_search = 0;
		$suppress = false;
	}
	$module->suppress = $suppress;
}

if (isset($_GET['quicksearch'])) {
	$_SESSION['LastSearch'] = array(
		'search_string' => filter_input(INPUT_GET, 'quicksearch'),
		'page_no' => 1,
	    'records_per_page' => 25,
	    'assigned_search' => $assigned_search,
	    'query_type' => 3,
	    'inactive_type' => 0,
	    'advanced_filters' => array(),
	    'sort_type' => '',
	    'sort_dir' => ''
	);
	$module->Build($_SESSION['LastSearch']);
}
elseif(isset($_SESSION['LastSearch'])) {
	$_SESSION['LastSearch']['advanced_filters'] = array();
	$_SESSION['LastSearch']['sort_type'] = '';
	$_SESSION['LastSearch']['sort_dir'] = '';
	
	$module->Build($_SESSION['LastSearch']);
}

$page->AddModule($module);

$page->AddModule(new ModuleCalendar());

if (!in_array($_SESSION['USER']['USERID'], array(8717, 8718, 8809, 8872, 8986))) {
	$page->AddModule(new ModuleMiniCalendar());

	$page->AddModule(new ModuleUpcomingEvents());
	
	$tasks = new ModuleUpcomingEvents();
	$tasks->type = 'tasks';
	$page->AddModule($tasks);
}


$page->Render();
?>
<script type="text/javascript">
	function checkGConnect(){
	
		$.ajax({
				   type: "POST",
				   url: "ajax/ajax.checkGConnect.php",
				   success: function(msg){
					console.log(msg);
				   }
			});
	
	}
checkGConnect();
</script>
