<?php
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
/**
 * Class for Notes Sort module
 */
class ModuleNotesSort extends JetstreamModule
{
	public $title = 'Sort Notes';
	public $sidebar = true;	
	public $sort = array('Date', 'Company', 'Contact', 'Salesperson', 'Opportunity');

	
	public function Init() {	
	
		/**
		 * include tpl file for HTML section.
		 */
		$this->AddTemplate('module_notes_sort.tpl');
	}	
}

?>