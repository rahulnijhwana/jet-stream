<?php

require_once BASE_PATH . '/smarty/Smarty.class.php';
require_once BASE_PATH . '/include/mpconstants.php';

//date_default_timezone_set('America/Chicago');
if(isset($_SESSION['USER']['BROWSER_TIMEZONE']) && (trim($_SESSION['USER']['BROWSER_TIMEZONE']) != ''))
{
	date_default_timezone_set($_SESSION['USER']['BROWSER_TIMEZONE']);
}
else
{
	date_default_timezone_set('GMT');
}

$smarty = new Smarty();

$smarty->template_dir = BASE_PATH . '/slipstream/smarty/templates_new';

$smarty->compile_dir = CACHE_PATH . '/smarty';

if (!file_exists($smarty->compile_dir)) {
	mkdir($smarty->compile_dir, 0755, TRUE);
}
$app_slipstream = true;
$smarty->config_dir = BASE_PATH . '/slipstream/smarty/configs';
