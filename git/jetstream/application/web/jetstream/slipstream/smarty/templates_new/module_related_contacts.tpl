<div class="module_container" id="divContactList">
	{if $module->contacts && !$module->is_restricted}
		{foreach from=$module->contacts key=key item=item}	
			<p style="margin:0px;padding:0 0 5px 0;{if $item.IsInactive eq 1}display:none;{/if}"><a href="slipstream.php?action=contact&contactId={$item.ContactId}">{$item.Name|@SplitLongWords}</a>{if $item.IsInactive eq 1}&nbsp;[Inactive]{/if}</p>
		{/foreach}
	{else}
		<span class='BlackBold'>{$module->msg}</span>
	{/if}
</div>
