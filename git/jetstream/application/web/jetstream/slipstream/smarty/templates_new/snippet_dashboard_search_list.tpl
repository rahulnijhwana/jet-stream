<div style="text-align:right;display:hidden">
	{$module->SearchResultsInfo()} {$module->getDashboardPages()}
</div>
{if !$module->search_results}
	{if $module->last_search}
	<div id="searchresults">
		<div class="smlRedBold" style="text-align:center;">No matching records found</div>
	</div>
	{/if}
{else}
	<table id="searchresults" width="100%">
		{foreach from=$module->search_results item=result}

		<tr style="clear:both;" class="{cycle values="alt_row,row"}{if $result.Private}private{/if}{if $result.Inactive}inactive{/if}" id="div_{if $result.ContactID}{$result.ContactID}{else}{$result.AccountID}{/if}">

			<td class="toggle">
				<a href="javascript: toggleInfo('{$result.UniqueID}')"><img id="toggle_{$result.UniqueID}" src="images/plus.gif"/></a>
			{if $result.ContactID}
				<input type="checkbox" class="mergeClass" onclick="javascript:setAttr(this.id,'chkContact');" id="chk_{$result.ContactID}" cid="{$result.ContactID}" accid="{$result.AccountID}" cname="{$result.ContactName|trim}" accname="{$result.AccountName|trim}" style="display:none;"/>
			{else}
				<input type="checkbox" class="mergeClass" onclick="javascript:setAttr(this.id,'chkAccount');" id="chk_{$result.AccountID}" accid="{$result.AccountID}" accname="{$result.AccountName|trim}" style="display:none;"/>
			{/if}
			</td>

			<td>
			{if $result.ContactID}
				<a href="javascript: toggleInfo('{$result.UniqueID}')"><b class="contact_title">Contact</b></a> 
				<a href="?action=contact&contactId={$result.ContactID}">{$result.ContactName|trim|SplitLongWords}</a> 
				{if $result.AccountName|trim}(<a href="?action=company&accountId={$result.AccountID}">{$result.AccountName|trim|SplitLongWords}</a>){/if}
			{else}
				{if $result.AssignedToMe || $limit_account_access eq 0 || $is_manager}
					<b class="company_title">Company</b> 
					<a href="?action=company&accountId={$result.AccountID}">{$result.AccountName|trim|SplitLongWords} </a>
				{else}
					<b class="company_title">Company</b> 
					{$result.AccountName|trim|SplitLongWords}
				{/if}
			{/if}
			{if $result.Private} [Private]{/if}
			{if $result.Inactive} [Inactive]{/if}
			</td>			

			<td><strong>{$result.City|trim|SplitLongWords}</strong></td>
			
			<td><strong>{$result.State|trim|SplitLongWords}</strong></td>

		</tr>

		<tr>
			<td class="detail"colspan="4" id="{$result.UniqueID}">
				<img class="spinner" src="images/slipstream_spinner.gif" />
			</td>
		</tr>
		
		{/foreach}
	</table>
{/if}

<div style="text-align:right;display:hidden">
	{$module->SearchResultsInfo()} {$module->getDashboardPages()}
</div>