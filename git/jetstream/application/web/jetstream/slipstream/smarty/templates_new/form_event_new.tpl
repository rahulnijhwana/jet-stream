{*
<pre>
{$module->event_info|@print_r:TRUE}
</pre>
*}
<div id="eventMiniCalendar" style="float: right; border: 0px solid black; padding: 5px; width:282px; overflow:auto">
	<div class="loading">
		<img class="spinner" src="{$smarty.const.WEB_PATH}/images/slipstream_spinner.gif" />
	</div>
</div>
<input type="hidden" name="EventID" value="{$module->event_info.EventID}">
<input type="hidden" name="AccountID" value="{$module->event_info.AccountID}">
<input type="hidden" name="ContactID" value="{$module->event_info.ContactID}">
<input type="hidden" name="DealID" value="{$module->event_info.DealID}">
{if !$module->event_info.EventID && $module->event_info.RepeatParent}
<input type="hidden" name="RepeatParent" value="{$module->event_info.RepeatParent}">
<input type="hidden" name="OrigStartDate" value="{$module->event_info.StartTimeStamp|FormatDate:'trunc'}">
{/if}
<div id="contactForm" style="width: 495px; float: left; margin: 5px 0px;">
	<div class="event_form">
		<table>
			<tr>
				<td colspan="2">
					{if $module->event_info.AccountID}
						<span class="acc_con_label">Company</span> <span class="form_required">*</span><br />
							<input type="text" id="account" name="account" class="full" value="{$module->event_info.AccountName}" onkeyup="javascript:ClearAccount();" onchange="javascript:ClearAccount();" />
							<div class="form_error" id="err_Account"></div>
						<!--span class="acc_con_label">Contact</span> <span class="form_required">*</span><br />
						<input type="text" id="Contact" name="Contact" class="full" value="{$module->event_info.Contact.FirstName}{if $module->event_info.Contact.FirstName and $module->event_info.Contact.LastName} {/if}{$module->event_info.Contact.LastName}{if $module->event_info.Contact.CompanyName} ({$module->event_info.Contact.CompanyName}){/if}" onkeyup="javascript:ClearContact();" onchange="javascript:ClearContact();" /-->
						<div class="form_error" id="err_Contact"></div>

					{else}
						<span class="acc_con_label">Contact</span> <span class="form_required">*</span><br />
						{if $module->event_info.ContactID == -1}
							Opportunity Contact
						{else}
							<input type="text" id="Contact" name="Contact" class="full" value="{$module->event_info.Contact.FirstName}{if $module->event_info.Contact.FirstName and $module->event_info.Contact.LastName} {/if}{$module->event_info.Contact.LastName}{if $module->event_info.Contact.CompanyName} ({$module->event_info.Contact.CompanyName}){/if}" onkeyup="javascript:ClearContact();" onchange="javascript:ClearContact();" />
							<div class="form_error" id="err_Contact"></div>
						{/if}
					{/if}
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<span class="acc_con_label">Subject</span> <span class="form_required">*</span><br />
					<input type="text" name="Subject" class="full" value="{$module->event_info.Subject}" />
					<div class="form_error" id="err_Subject"></div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<span class="acc_con_label">Location</span><br />
					<textarea name="Location" rows="3" class="full">{$module->event_info.Location}</textarea>
					<div class="form_error" id="err_Location"></div>
				</td>
			</tr>
			<tr>
				<td>
					<span class="acc_con_label">Type</span> <span class="form_required">*</span><br />				
					<select name="EventType" id="EventType" class="clsSelect" onchange="DoChangeType(this.value);">
						<option value="" event_type=""></option>
							{if !$module->event_info.AccountID}
							<optgroup label="Events (Date with Time)">
								{foreach from=$module->GetEventTypes('EVENT') item=EventType}
									<option style="background-color: {$EventType.EventColor}" value="{$EventType.EventTypeID}" {if $module->event_info.EventTypeID eq $EventType.EventTypeID}selected{/if}>{$EventType.EventName}</option>
								{/foreach}
							</optgroup>
							{/if}
							{if !$module->event_info.DealID}
								<optgroup label="Tasks (Date Only)">
									{foreach from=$module->GetEventTypes('TASK') item=EventType}
										<option style="background-color:{$EventType.EventColor}" value="{$EventType.EventTypeID}" {if $module->event_info.EventTypeID eq $EventType.EventTypeID}selected{/if}>{$EventType.EventName}</option>
									{/foreach}
								</optgroup>
							{/if}
					</select>						
					<div class="form_error" id="err_EventType"></div>
				</td>
				<td>
					<span class="acc_con_label" id="label_priority">Task Priority</span><br />
					<select name="priority" id="priority">
						<option value="">-</option>
						{foreach from=$module->priorities item=priority}
							<option value="{$priority.PriorityID}" {if $module->event_info.PriorityID eq $priority.PriorityID}selected{/if}>{$priority.Priority} - ({$priority.Label})</option>
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<span class="acc_con_label">When</span> <span class="form_required">*</span><br />
	 				<input type="text" name="StartDate" id="StartDate" class="datepicker" size="6" value="{if $module->event_info.StartTimeStamp}{$module->event_info.StartTimeStamp|FormatDate:'date'}{/if}" />
					<span id="event_datetime">
					<select name="StartTime">
						{if !$module->event_info.BlankTime}{$module->event_info.StartTimeStamp|GetOptionsTime}{/if}
						{if $module->event_info.BlankTime}{''|GetOptionsTime}{/if}
					</select>
					to
					<select name="EndTime">
						{if !$module->event_info.BlankTime}{$module->event_info.EndTimeStamp|GetOptionsTime}{/if}
						{if $module->event_info.BlankTime}{''|GetOptionsTime}{/if}
					</select>
					<input type="text" name="EndDate" id="EndDate" class="datepicker" size="6" value="{if $module->event_info.EndTimeStamp}{$module->event_info.EndTimeStamp|FormatDate:'date'}{/if}" />
					</span>
					<div class="form_error" id="err_When"></div>
				</td>
			</tr>
			{* Don't show the repeat choices if this is a member of a repeat group *}
			{if !$module->event_info.RepeatParent && !($module->event_info.DealID eq -1 || $module->event_info.DealID gt 0)}
			<tr>
				<td id="event_repeat" colspan="2">
					<span class="acc_con_label">Repeats</span><br />
					<select name="Repeat" onchange="DoChangeRepeat(this.value);">
						{html_options options=$module->repeat_types selected=$module->event_info.repeat_selector}
					</select>
					<fieldset id="repeat_choices" class="hidden" style="border:1px solid #E6E6E6;">
						<legend>Repeat Frequency</legend>
						<div id="repeat_1">
							<p>Every <select name="daily_freq">{$module->repeat_interval.daily_freq|@GetOptionsNumRng:1:31}</select> day(s)</p>
						</div>
						<div id="repeat_2">
							<p>Every <select name="weekly_freq">{$module->repeat_interval.weekly_freq|@GetOptionsNumRng:1:52}</select> week(s) on:</p>
							<p>
								{html_checkboxes name='weekly_day' options=$module->weekdays selected=$module->repeat_interval.weekly_days separator='<br />'}
							</p>
						</div>
						<div id="repeat_3">
							<p>Every <select name="monthly_freq">{$module->repeat_interval.monthly_freq|@GetOptionsNumRng:1:12}</select> month</p>
							<p><input type="radio" name="monthly_type" value="3" {if $module->event_info.RepeatType eq 3}checked{/if} />On the <select name="monthly_daynum">{$module->repeat_interval.monthly_day|@GetOptionsNumRng:1:31:true}</select></p> 
							<p><input type="radio" name="monthly_type" value="4" {if $module->event_info.RepeatType eq 4}checked{/if} />On the <select name="monthly_pos">
								<option />
								{html_options options=$module->pos_locs selected=$module->repeat_interval.monthly_pos}
							</select>
							<select name="monthly_pos_type">
								<option />
								{html_options options=$module->pos_types selected=$module->repeat_interval.monthly_pos_type}
							</select></p>
						</div>
						<div id="repeat_5">
							<p>Every <select name="yearly_freq">{$module->repeat_interval.yearly_freq|@GetOptionsNumRng:1:4}</select> year(s)</p>
							<p><input type="radio" name="yearly_type" value="5" {if $module->event_info.RepeatType eq 5}checked{/if} />On <select name="yearly_monthnum"><option value="" />{$module->repeat_interval.yearly_month|@GetOptionsMonths}</select> <select name="yearly_daynum">{$module->repeat_interval.yearly_month_day|@GetOptionsNumRng:1:31:true}</select></p>
							<p><input type="radio" name="yearly_type" value="6" {if $module->event_info.RepeatType eq 6}checked{/if} />On the <select name="yearly_pos">
								<option />
								{html_options options=$module->pos_locs selected=$module->repeat_interval.yearly_pos}
							</select> <select name="yearly_pos_type">
								<option />
								{html_options options=$module->pos_types selected=$module->repeat_interval.yearly_pos_type}
							</select> of <select name="yearly_pos_month"><option value="" />{$module->repeat_interval.yearly_pos_month|@GetOptionsMonths}</select></p>
						</div>
						<div class="form_error" id="err_RepeatFreq"></div>
					</fieldset>
					<fieldset id="repeat_span" class="hidden" style="border:1px solid #E6E6E6;">
						<legend>Repeat Until</legend>
						<input type="radio" name="repeat_enddate_type" value="0" {if !$module->event_info.RepeatEndDateUTC}checked{/if} />No end date<br />
						<input type="radio" name="repeat_enddate_type" value="1" {if $module->event_info.RepeatEndDateUTC}checked{/if} />End by <input type="text" name="repeat_enddate" class="datepicker" size="6" value="{if $module->event_info.RepeatEndDateUTC}{$module->event_info.RepeatEndTimeStamp|FormatDate:'date':1}{/if}" />
						<div class="form_error" id="err_RepeatUntil"></div>
					</fieldset>
				</td>
			</tr>
			{/if}
			{if permitted_people}
			<tr>
				<td colspan="2">
					<span class="acc_con_label">Primary Attendee</span><br />
					<select name="creator" onChange="RemoveAttendee(this.value);UpdateAttendeeSelect();">
					{foreach from=$module->permitted_people key=pp_id item=pp_name}
						
						<option value="{$pp_id}" {if $pp_id eq $module->creator}selected{/if}>{$pp_name}</option>
					{/foreach}
					</select>
				</td>
			</tr>
			{/if}
			<tr id="attendee_section">
				<td colspan="2">
					<span class="acc_con_label">Other Attendees</span><br />
					<div id="add_attendee_div">
						<select name="add_attendee" id="add_attendee"></select>
						<input type="button" value="Add" onclick="AddAttendee();">
						<input type="hidden" name="attendee_list" />
					</div>
					<div id="attendees">
					{foreach from=$module->attendees item=attendee}
						{if !(permitted_people && $attendee.Creator)}
							<div id="attendee_{$attendee.PersonID}">{$attendee.name} {if $attendee.Creator}(Creator){else}&nbsp;&nbsp;&nbsp;<a style="font-weight:bold;color:red;" onClick="RemoveAttendee({$attendee.PersonID})">X</a>{/if}</div>
						{/if}
					{/foreach}
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<span class="acc_con_label" id="label_event_private">Private</span><br />
					<input name="CheckPrivate" id="eventCheckPrivate" type="checkbox" value="true" {if $module->event_info.Private eq 1}checked{/if} />
					<div class="form_error" id="err_CheckPrivate"></div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<span class="acc_con_label">Note</span><br />
					<textarea name="Note" class="full" cols="35" rows="8" id="Notes" >{$module->event_info.Notes}</textarea>				
					<div class="form_error" id="err_Notes"></div>
				</td>
			</tr>
		</table>
	</div>
	{include file="snippet_event_alert.tpl"}
	<div id="event_bottom" class="clearing"></div>
</div>


{literal}
<script language="javascript">
var people_list;
var type_list;
var webpath = '{/literal}{$smarty.const.WEB_PATH}{literal}';
var prev_start = '';

$(document).ready(function() {

	var sPath = window.location.pathname;
	var sPage = sPath.substring(sPath.lastIndexOf('/') + 1);

	if(sPage == 'edit_opp2.php'){
		$("#event_alert_wrapper").hide();
	}


	try {
		showNoteAlertHelp();
	}
	catch(err) {}

	$("#expand_event_alert").click(function(){

		$("#add_event_alert").slideToggle();

		if($("#eventCheckPrivate").attr('disabled')){
			$("#eventCheckPrivate").removeAttr('disabled');
			$("#label_event_private").css('color', '#5C80A5');
    		$(this).attr('value', 'Add Alert');
    		$.scrollTo("#overlay", {duration : 2000});
    		$("#add_event_alert_hidden").attr('value', '');
		}
        else {
        	$.scrollTo("#event_bottom", {duration : 1000});
        	$("#eventCheckPrivate").attr('disabled', 'disabled');
			$("#label_event_private").css('color', '#cacaca');
    		$(this).attr('value', 'Remove Alert');
    		$("#add_event_alert_hidden").attr('value', '1');
        }
		
	});

	$("#show_event_detail").click(function(){
		$(".event_form").slideToggle();
	});
	
	$(".event_level").click(function(){
		
		id = ($(this).attr('id'));
		
		if($("#level_event_checked").attr('value') != 1){
			$('.'+id).attr('checked', true);
			$("#level_event_checked").attr('value', 1);
		}
		else {
			$('.'+id).removeAttr('checked');
			$("#level_event_checked").attr('value', 0);
		}
	});

	$("#select_event_level").click(function(){
		$("#level_event_msg").show();
		$(".header").fadeOut();
		$(".header").fadeIn();
		$(".header").fadeOut();
		$(".header").fadeIn();
	});

	$("#delay_event_until_day").datepicker({
		showOn: "button",
		buttonImage: "images/calendar.gif",
		buttonImageOnly: true,
		disabled: true
	});
	
	$("#select_event_all").click(function(){
		toggleEventAllSelected();
	});

	$("#select_event_assigned").click(function(){
		toggleEventAssignedSelected();
	});

	$("#send_event_immediately_no").click(function(){
		$(".ui-datepicker-trigger").attr('title', 'Select a Date');
		$(".ui-datepicker-trigger").show();
		$("#delay_event_until_day" ).datepicker('enable');
		$("#delay_event_until_day" ).attr('value','');
		$("#event_hours").removeAttr('disabled');
		$("#event_minutes").removeAttr('disabled');
		$("#event_periods").removeAttr('disabled');
	});

	
	$("#send_event_immediately_yes").click(function(){
		$("#delay_event_until_day").attr('value', '');
		$("#delay_event_until_day").datepicker('disable');
		$("#event_hours").attr('disabled', 'disabled');
		$("#event_minutes").attr('disabled', 'disabled');
		$("#event_periods").attr('disabled', 'disabled');
		$("#spn_event_delay_until_error").text('');
		
	});
	
	$("#EventType").change(function(){
		$("#err_EventType").hide();
	});

	$("#standard_event_alert").change(function(){
		$("#err_Delivery").hide();
	});

	$("#email_event_alert").change(function(){
		$("#err_Delivery").hide();
	});

	$("#delay_event_until_day").change(function(){
		$("#err_Delay").hide();
	});


	
	var target = 0;
	{/literal}
	people_list = {$module->GetPermittedPeople()|@json_encode};
	type_list = {$module->GetEventTypes()|@json_encode};
	{literal}

	var start_date = $("input[name='StartDate']").val();
	if (start_date != '') {
		prev_start = $.datepicker.parseDate('m/d/y', start_date);
	}

	try {
		// Get the selected user from edit-opportunity 
		target = ssSalesPerson.getSelectedVal();				
	}
	catch(err){}
			
	$("#eventMiniCalendar").load("{/literal}{$smarty.const.WEB_PATH}{literal}/ajax/ajax.EventMiniCalendar.php?target=" + target);

	DoChangeRepeat($("select[name='Repeat']").val());
	RemoveAttendee($("select[name='creator']").val());
	UpdateAttendeeSelect();


	
	$("input[name='StartDate']").datepicker({
		showOn :"both",
		buttonImage : webpath + "/images/calendar.gif",
		buttonImageOnly :true,
		dateFormat: 'm/d/y',
		onSelect: function(dateText, inst) {
			var start_date = $(this);
			var start_parsed = $.datepicker.parseDate('m/d/y', start_date.val());

			
			var end_date = $("input[name='EndDate']");
			var repeat_end = $("input[name='RepeatEndDate']");
			if (end_date.val() == '') {
				end_date.val(start_date.val());
			} else if (prev_start != '') {
				var end_parsed = $.datepicker.parseDate('m/d/y', end_date.val());
				var new_end = new Date();
				new_end.setTime(end_parsed.getTime() + start_parsed.getTime() - prev_start.getTime() + 3600000);
				end_date.val($.datepicker.formatDate('m/d/y', new_end));
			} 
			if (repeat_end.val() != undefined && start_parsed > $.datepicker.parseDate('m/d/y', repeat_end.val())) {
				repeat_end.val(start_date.val());
			}
			prev_start = $.datepicker.parseDate('m/d/y', start_date.val());
			end_date.datepicker('option', 'minDate', start_parsed);
			repeat_end.datepicker('option', 'minDate', start_parsed);
		}

	});

	$("input[name='EndDate']").datepicker({
		showOn :"both",
		buttonImage : webpath + "/images/calendar.gif",
		buttonImageOnly :true,
		dateFormat: 'm/d/y'
	});

	$("input[name='repeat_enddate']").datepicker({
		showOn :"both",
		buttonImage : webpath + "/images/calendar.gif",
		buttonImageOnly :true,
		dateFormat: 'm/d/y'
	});

	$(".ui-datepicker-trigger").attr('title', 'Select a Date');
	/*
	$(".ui-datepicker-trigger").css( "width", "24px");
	$(".ui-datepicker-trigger").css( "height", "23px");
	*/
	if ($("select[name='EventType']").val() != '') {
		DoChangeType($("select[name='EventType']").val());
	}
	
	$("#delay_event_until_day" ).datepicker('disable');

});

function toggleEventAllSelected(){
	
	var current_state = $("#all_event_checked").attr('value');

	if(current_state != 1){
		$(".all_users").attr('checked', true);
		$("#all_event_checked").attr('value', 1);
	}
	else {
		resetEventSelected();
	}
}

function toggleEventAssignedSelected(){

	var current_state = $("#assigned_event_checked").attr('value');
	
	if(current_state != 1){
		$(".assigned_users").attr('checked', true);
		$("#assigned_event_checked").attr('value', 1);
	}
	else {
		resetEventSelected();	
	}
}



function resetEventSelected(){
	
	$(".all_users").removeAttr('checked');
	$("#all_event_checked").attr('value', 0);
	
	$(".assigned_users").removeAttr('checked');
	$("#assigned_event_checked").attr('value', 0);
}




function AddAttendee() {
	var to_add = $("select[name='add_attendee']").find(':selected').val();
	if (to_add != "" && to_add != "All") {
		$("#attendees").append('<div id="attendee_' + to_add + '">' + people_list[to_add] + '&nbsp;&nbsp;&nbsp;<a style="font-weight:bold;color:red;" onClick="RemoveAttendee(' + to_add + ')">X</a></div>');
		UpdateAttendeeSelect();
	}else if (to_add == "All") {
		$("#add_attendee option:not(option:first, option:last)").each(function(){
		$("#attendees").append('<div id="attendee_' + $(this).val() + '">' + people_list[$(this).val()] + '&nbsp;&nbsp;&nbsp;<a style="font-weight:bold;color:red;" onClick="RemoveAttendee(' + $(this).val()+ ')">X</a></div>');		
		});	
		UpdateAttendeeSelect();
	}
}

function RemoveAttendee(attendee_id) {
	$("#attendee_" + attendee_id).remove();
	UpdateAttendeeSelect();
}

function UpdateAttendeeSelect() {
	var attendee_select = $("select[name='add_attendee']");
	attendee_select.children().remove();
	attendee_select.append(new Option('', '', true));

	// Get the list of the current attendees
	current_array = new Array();
	current_count = 0;
	if ($("select[name='creator']")) {
		current_array[current_count] = $("select[name='creator']").val();
		current_count++;
	}
	$("div[id^='attendee_']").each(function() {
		current_array[current_count] = this.id.substring(9);
		current_count++;
	});

	$("input[name='attendee_list']").val(JSON.stringify(current_array));
	
	for (var person in people_list) {
		var found = false;
		for(var i = 0; i < current_array.length; i++) {
			if(current_array[i] == person) {
				var found = true;
			}
		}
		if (found) continue;
		if(person > 0)
		attendee_select.append("<option value='" + person + "'>" + people_list[person] + "</option>");
	}
	attendee_select.append("<option value='All'>" + "All" + "</option>");
	
}

function DoChangeRepeat(value) {
	$('#repeat_choices div').hide();
	$('#event_repeat fieldset').hide();
	if (value > 0) {
		$('#event_repeat fieldset').show();
		var field = 'repeat_' + value;
		$('#' + field).show();
	}
}

function DoChangeType(value) {
	if (type_list[value]['Type'] == 'TASK') {
		$('#event_datetime').hide();
		$('#event_repeat').hide();
		$('#attendee_section').hide();
		$("div[id^='attendee_']").remove();
		UpdateAttendeeSelect();
		$("#label_priority").show();
		$("#priority").show();
	}
	else {
		$('#event_datetime').show();
		$('#event_repeat').show();
		$('#attendee_section').show();
		$("#label_priority").hide();
		$("#priority").hide();
	}
}

</script>

{/literal}

