{literal}
<script type="text/javascript"><!--
	$(function() {
		$('input').filter('.vacationreturn').datepicker({
			showOn: 'button',
			buttonImage: './images/cal.jpg',
			buttonImageOnly: true
		});
	});
--></script>
{/literal}

<div id="divEditor">
	<form method="post" name="frmVacation">
		Check the boxes next to the users that are on Vacation/Leave, then enter date they will return  (if known).<br><br>
		<table class="event">
			<tr>
				<th style="width:200px;">Users</th>
				<th class="centered" style="width:50px;">On Leave</th>
				<th class="centered" style="width:150px;">Return Date</th>
				<th></th>
			</tr>
		{foreach from=$module->user_details key=user_id item=user}
			<tr>
				<td>{$user.FirstName} {$user.LastName}</td>
				<td class="centered"><input type="checkbox" person_id="{$user.PersonID}" class="onvacation" {if $user.OnVacation}checked{/if}/></td>
				<td class="centered"><input type="text" class="vacationreturn" id="vacreturn{$user.PersonID}" name="vacationreturn" person_id="{$user.PersonID}" value="{$user.VacationReturn}" size="10" ></td>
			</tr>
		{/foreach}
		</table>
		<br/>
		<br/>
		<input type="hidden" id="testHidden" value="" />
		<input type="button" id="btnSaveEditor" value="Save" onclick="javascript:saveVacationSettings('{$module->user_id}');"/>
	</form>
</div>