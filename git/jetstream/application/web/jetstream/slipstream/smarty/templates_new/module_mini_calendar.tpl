{*Hidden fields for month and year for dashboard calendar section*}
<input type="hidden" name="dashMonth" id="dashMonth" value="{$module->month_int}">
<input type="hidden" name="dashYear" id="dashYear" value="{$module->year}">

{*Dashboard page mini calendar section*}
<div id="miniCalendar">
	{include file="snippet_mini_calendar.tpl"}
</div>