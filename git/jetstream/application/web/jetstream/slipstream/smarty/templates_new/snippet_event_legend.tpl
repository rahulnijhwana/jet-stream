<div>
<table>
	{foreach from=$event_types item=event_type}
		{if !$event_type.Deleted}
		<tr>
			<td>
				<div class="minievent" style="background-color:#{$event_type.EventColor};" >&nbsp;</div>
			</td>
			<td>&nbsp;</td>
			<td>{$event_type.EventName}</td>
		</tr>
		{/if}
	{/foreach}
</table>
</div>
