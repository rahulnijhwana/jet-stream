{*
<pre>
{$module->layout|@print_r:TRUE}
{$module->map|@print_r:TRUE}
</pre>
*}
<div id='contactView'>
	{foreach from=$module->layout key=section_id item=section}
	{if $section.SectionName neq 'DefaultSection'}
	<div class="section_head" onclick="javascript:ToggleHidden('section{$section_id}');">
		<img id="toggle_section{$section_id}" src="images/plus.gif"/> {$section.SectionName}
	</div>
	{/if}
	<div class="jet_datagrid {if $section.SectionName neq 'DefaultSection'}hidden{/if}" id="section{$section_id}">
		<table style="width: 100%">
			{if $section.SectionName eq 'DefaultSection'}
			{if $module->created}
				<tr>
					<td class="label">&nbsp;</td>
					<td>&nbsp;</td>
					<td class="label">Original Creation Date</td>
					<td>
						{$module->created}
					</td>
				</tr> 
			{/if}
			<tr>
				{if $module->type eq 'contact'}
				<td class="label">Company</td>
				<td class="left">
					{if $module->map.account.name}
						<a href="?action=company&accountId={$module->map.account.id}">{$module->map.account.name}</a>
					{else}
						&nbsp;
					{/if}
				</td>
				{else}
				<td class="label">&nbsp;</td>
				<td class="left">&nbsp;</td>
				{/if}
				<td class="label">Assigned to</td>
				<td class="right">
					{foreach from=$module->map.security.assigned item=person}
						{$person.FirstName} {$person.LastName}<br />
					{foreachelse}
						Nobody
					{/foreach}
				</td>
			</tr>
			{/if}
			{foreach from=$section.rows item=row}
			{if $module->map.fields[$row.1].FieldName|regex_replace:"/[\d]/":"" eq "LongText" }
				<tr>
					<td class="label">{$module->map.fields[$row.1].LabelName|default:"&nbsp;"}</td>
					<td class="left" colspan="3" style="word-wrap: break-word;"><p>{$module->FormatForDisplay($module->map.fields[$row.1])|default:"&nbsp;"}</p></td>
				</tr>			
			{else}
				<tr>
					<td class="label">{$module->map.fields[$row.1].LabelName|default:"&nbsp;"}</td>
					<td class="left">{$module->FormatForDisplay($module->map.fields[$row.1])|default:"&nbsp;"}</td>
					<td class="label">{$module->map.fields[$row.0].LabelName|default:"&nbsp;"}</td>
					<td class="right">{$module->FormatForDisplay($module->map.fields[$row.0])|default:"&nbsp;"}</td>
				</tr>
			{/if}
			{/foreach}
		</table>	
	</div>
	{/foreach}
</div>
{if $module->type eq 'contact'}
	{assign var='account_id' value=$module->map.account.id}
{else}
	{assign var='account_id' value=$module->map.security.AccountID}
{/if}
	
{if !$module->limit_access && !$module->partial && $account_id}
	<script type="text/javascript">
		var this_account_id = {$account_id};
	</script>
{/if}
