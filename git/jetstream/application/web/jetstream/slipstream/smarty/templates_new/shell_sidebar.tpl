<div class="module">
	<div class="module_title">
		<div class="module_title_text">
			{$module->title}
		</div>
		<div class="module_title_icons">
			{foreach from=$module->buttons name=buttons item=button}<a {if $button.id}id="{$button.id}"{/if} title="{$button.tooltip}" href="{$button.action}">{$button.tooltip}</a>{if !$smarty.foreach.buttons.last} | {/if}{/foreach}
		</div>
	</div>
	{foreach from=$module->template_files item=template_file}
	{include file=$template_file}
	{/foreach}
</div>
