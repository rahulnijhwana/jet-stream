	<div class="clear" style="width:80%;" > 
			<div style="border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 2px;padding: 5px;">		
		<div style="text-align:center; font-weight:bold;font-size:1.2em;">
			 <br />PIPELINE REPORTS &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
		</div>			
		<br /><br />
		<table align="center" width="100%">
			
					<tr>
						<td style="padding: 5px 5px 5px 5px;" align="left">
							<span style="text-align:center; font-weight:bold;font-size:1.1em;"> &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ACTIVE OPPORTUNITY VALUES</span>
						</td>	
						<td style="padding: 5px 5px 5px 5px;" align="left">
							<span style="text-align:center; font-weight:bold;font-size:1.1em;"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; FORECASTED REVENUES</span>
						</td>		
					</tr>
					
					<tr>
						<td style="padding: 5px 5px 5px 5px;" align="center">
							<a class="button" href="#" onclick="javascript:CreateReport('export', 'all', 'all', 'all', 'all')" > <span style="width:25em; text-align:left;">BY CUSTOMER (BY SALES CYCLE STAGE)</span></a>
						</td>
						<td style="padding: 5px 5px 5px 5px;" align="center">
							<a class="button" href="#" onclick="javascript:CreateReport('export', 'true', 'all', 'all', 'all')" > <span style="width:25em; text-align:left;">BY CUSTOMER (BY SALES CYCLE STAGE)</span></a>
						</td>		
					</tr>
					<tr>
						<td style="padding: 5px 5px 5px 5px;">
							<a class="button" href="#" onclick="javascript:CreateReport('OppCalendarRPT', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">BY CUSTOMER (BY MONTH)</span></a>
						</td>
						<td style="padding: 5px 5px 5px 5px;">
							<a class="button" href="#" onclick="javascript:CreateReport('OppCalendarRPT', 'true', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">BY CUSTOMER (BY MONTH)</span></a>
						</td>		
					</tr>
					<tr>
						<td style="padding: 5px 5px 5px 5px;">
							<a class="button" href="#" onclick="javascript:CreateReport('OppCalendarRPTOpps', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">BY CUSTOMER / DETAIL (BY MONTH)</span></a>
						</td>	
						<td style="padding: 5px 5px 5px 5px;">
							<a class="button" href="#" onclick="javascript:CreateReport('OppCalendarRPTOpps', 'true', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">BY CUSTOMER / DETAIL (BY MONTH)</span></a>
						</td>		
					</tr>
					<tr>
						<td style="padding: 5px 5px 5px 5px;">
							<a class="button" href="#" onclick="javascript:CreateReport('OppCalendarRPTProds', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">BY PRODUCT (BY MONTH)</span></a>
						</td>	
						<td style="padding: 5px 5px 5px 5px;">
							<a class="button" href="#" onclick="javascript:CreateReport('OppCalendarRPTProds', 'true', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">BY PRODUCT (BY MONTH)</span></a>
						</td>		
					</tr>
			</table>
			
			<!--
				<a class="button" href="#" onclick="javascript:CreateReport('export', 'all', 'all', 'all', 'all')" > <span style="width:25em; text-align:left;"> Weighted Forecast Report</span></a><br /><br /><br /><br />			
				<a class="button" href="#" onclick="javascript:CreateReport('OppCalendarRPT', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">Anticipated Revenue by Customer</span></a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a class="button" href="#" onclick="javascript:CreateReport('OppCalendarRPT', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">Anticipated Revenue by Customer</span></a><br /><br /><br /><br />
				<a class="button" href="#" onclick="javascript:CreateReport('OppCalendarRPTOpps', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">Anticipated Revenue by Customer & Opp</span></a> <a class="button" href="#" onclick="javascript:CreateReport('OppCalendarRPTOpps', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">Anticipated Revenue by Customer & Opp</span></a><br /><br /><br /><br />
				<a class="button" href="#" onclick="javascript:CreateReport('OppCalendarRPTProds', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">Anticipated Revenue by Customer & Product</span></a> <a class="button" href="#" onclick="javascript:CreateReport('OppCalendarRPTProds', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">Anticipated Revenue by Customer & Product</span></a><br /><br />
				<p />&nbsp;
			-->	
			</div>
			<!--
			<div style="border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 2px;padding: 5px;">		
				
				<a class="button" href="#" onclick="javascript:CreateReport('ClosedCustRPT', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">Closed Revenue by Customer</span></a><br /><br />
				<a class="button" href="#" onclick="javascript:CreateReport('ClosedOppRPT', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">Closed Revenue by Customer & Opp</span></a> <br /><br />
				<a class="button" href="#" onclick="javascript:CreateReport('ClosedProdRPT', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">Closed Revenue by Customer & Product</span></a>
				<p />&nbsp;
			</div>
			-->
			
	</div>
	
	<br />
	<div class="clear" style="width:80%;" > 
	
			<div style="border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 2px;padding: 5px;">	
				<br />
			<div style="text-align:center; font-weight:bold;font-size:1.2em;">
				  CLOSED REPORTS &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
			</div>			
		<br /><br />
		<table align="center" width="100%">
		
					<tr>
						<td style="padding: 5px 5px 5px 5px;" align="left">
							<span style="text-align:center; font-weight:bold;font-size:1.1em;"> &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CLOSED OPPORTUNITY VALUES</span>
						</td>	
						<td style="padding: 5px 5px 5px 5px;" align="left">
							&nbsp;
						</td>		
					</tr>
		
					<tr>
						<td style="padding: 5px 5px 5px 5px;" align="center">
							<a class="button" href="#" onclick="javascript:CreateReport('ClosedCustRPT', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">BY CUSTOMER (BY MONTH)</span></a>						
						</td>
						<td style="padding: 5px 5px 5px 5px;" align="center">
							&nbsp;<!--<a class="button" href="#" onclick="javascript:CreateReport('export', 'all', 'all', 'all', 'all')" > <span style="width:25em; text-align:left;"> Weighted Forecast Report</span></a> -->
						</td>		
					</tr>
					<tr>
						<td style="padding: 5px 5px 5px 5px;">
							<a class="button" href="#" onclick="javascript:CreateReport('ClosedOppRPT', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">BY CUSTOMER / DETAIL (BY MONTH)</span></a> <br /><br />
						</td>
						<td style="padding: 5px 5px 5px 5px;">
							&nbsp;
						</td>		
					</tr>
					<tr>
						<td style="padding: 5px 5px 5px 5px;">
							<a class="button" href="#" onclick="javascript:CreateReport('ClosedProdRPT', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">BY PRODUCT  (BY MONTH)</span></a>
						</td>	
						<td style="padding: 5px 5px 5px 5px;">
							&nbsp;
						</td>		
					</tr>					
			</table>
			
			<!--
				<a class="button" href="#" onclick="javascript:CreateReport('export', 'all', 'all', 'all', 'all')" > <span style="width:25em; text-align:left;"> Weighted Forecast Report</span></a><br /><br /><br /><br />			
				<a class="button" href="#" onclick="javascript:CreateReport('OppCalendarRPT', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">Anticipated Revenue by Customer</span></a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a class="button" href="#" onclick="javascript:CreateReport('OppCalendarRPT', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">Anticipated Revenue by Customer</span></a><br /><br /><br /><br />
				<a class="button" href="#" onclick="javascript:CreateReport('OppCalendarRPTOpps', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">Anticipated Revenue by Customer & Opp</span></a> <a class="button" href="#" onclick="javascript:CreateReport('OppCalendarRPTOpps', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">Anticipated Revenue by Customer & Opp</span></a><br /><br /><br /><br />
				<a class="button" href="#" onclick="javascript:CreateReport('OppCalendarRPTProds', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">Anticipated Revenue by Customer & Product</span></a> <a class="button" href="#" onclick="javascript:CreateReport('OppCalendarRPTProds', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">Anticipated Revenue by Customer & Product</span></a><br /><br />
				<p />&nbsp;
			-->	
			</div>
			<!--
			<div style="border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 2px;padding: 5px;">		
				
				<a class="button" href="#" onclick="javascript:CreateReport('ClosedCustRPT', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">Closed Revenue by Customer</span></a><br /><br />
				<a class="button" href="#" onclick="javascript:CreateReport('ClosedOppRPT', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">Closed Revenue by Customer & Opp</span></a> <br /><br />
				<a class="button" href="#" onclick="javascript:CreateReport('ClosedProdRPT', 'all', 'all', 'all', 'all')" ><span style="width:25em; text-align:left;">Closed Revenue by Customer & Product</span></a>
				<p />&nbsp;
			</div>
			-->
			
	</div>