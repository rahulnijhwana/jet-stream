<div class="printborder mainGrid">

<div><a href="slipstream.php?action=weekly&day={$module->previous}"><<</a>&nbsp;&nbsp;<a href="slipstream.php?action=weekly&day={$module->next}">>></a>&nbsp; {$module->display_date}</div>

<div id="colheaders" style="border:0px solid black; margin-left:35px; margin-right:30px; ">
	{foreach from=$module->days key=key item=item}					
		<div style="width: 14.2857%; left: {$key*14.2857}%;" class="chead cheadToday">
		<span><a class="lkh" href="slipstream.php?action=daily&day={$item.Date}">{$item.Day}</a></span></div>
	{/foreach}
</div>


<div>&nbsp;</div>

<div style="overflow-x: hidden; overflow-y: scroll; height: 679px;" id="gridcontainer">

<table style="table-layout: fixed; min-width: 100%;" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td id="rowheadcell" style="width: 40px;">
			<div id="rowheaders" style="height: 144ex; top: 0pt; left: 0pt;">
		
			{foreach from=$module->hours key=key item=item}					
				<div class="rhead" style="height: 6ex; top: {$key*6}ex;">
				<div class="rheadtext">{$item}</div>
				</div>						
			{/foreach}			
			
			</div>
			</td>
			<td id="gridcontainercell" style="width: auto;">
			<div id="grid" class="grid" style="height: 144ex; ">			
			
			<div style="position: absolute; left: {$module->day_of_week*14.2857}%; top: 0px; width: 14.2012%; height: 850px; z-index: 0;">
			<div class="currentDayDec"></div>
			</div>
						
			
			{section name=hrule start=0 loop=147 step=3}				
				{if $smarty.section.hrule.index % 2 eq 0}							
					<div id="{$smarty.section.hrule.index|replace:".":"_"}" class="hrule hruleeven" style="top: {$smarty.section.hrule.index}ex; z-index: 1;"></div>
				{else}
					<div id="{$smarty.section.hrule.index-1.5|replace:".":"_"}" class="hrule hruleodd" style="top: {$smarty.section.hrule.index-1.5}ex; z-index: 1;"></div>
					<div id="{$smarty.section.hrule.index|replace:".":"_"}" class="hrule hruleodd" style="top: {$smarty.section.hrule.index}ex; z-index: 1;"></div>
					<div id="{$smarty.section.hrule.index+1.5|replace:".":"_"}" class="hrule hruleodd" style="top: {$smarty.section.hrule.index+1.5}ex; z-index: 1;"></div>
				{/if}			
			{/section}
			
			<div class="vrule gutter" id="c0" style="width: 1px; left: 0%; height: 144ex; z-index: 1;"></div>
			<div class="vrule gutter" id="c1" style="width: 1px; left: 14.2857%; height: 144ex; z-index: 1;"></div>
			<div class="vrule gutter" id="c2" style="width: 1px; left: 28.5714%; height: 144ex; z-index: 1;"></div>
			<div class="vrule gutter" id="c3" style="width: 1px; left: 42.8571%; height: 144ex; z-index: 1;"></div>
			<div class="vrule gutter" id="c4" style="width: 1px; left: 57.1429%; height: 144ex; z-index: 1;"></div>
			<div class="vrule gutter" id="c5" style="width: 1px; left: 71.4286%; height: 144ex; z-index: 1;"></div>
			<div class="vrule gutter" id="c6" style="width: 1px; left: 85.7143%; height: 144ex; z-index: 1;"></div>
			
			
			<div id="eventowner" style="z-index: 2;">
			{foreach from=$module->events key=key item=item}
				<div class="chip"  id="event-{$item.DivID}" style="cursor: pointer;position: absolute; top:{$item.StartHour*35.522}px; left: {$item.LeftPosition}%; width: {$item.Width}%; z-index:{$item.ZIndex}" onclick="javascript:location.href='slipstream.php?action=event&eventId={$key}&ref=weekly'">
				<div class="t1" style="background-color: {$item.Color};">&nbsp;</div>
				<div class="t2" style="background-color: {$item.Color};">&nbsp;</div>
				<div class="chipbody edit" style="background-color: {$item.BodyColor};">
				<dl id="body-event-{$item.DivID}" style="border-color: {$item.Color}; height:{$item.Duration*33.5}px;">
					<dt style="background-color: {$item.Color};"><b><span class="timelabel">{$item.TimeLabel}</span> </b></dt><dd>
					<div>
						{$item.EventName}<br>
						{$item.Date}<br>
						Contact: <a href="?action=contact&contactId={$item.ContactID}">{$item.Contact}</a><br>
						Company: <a href="?action=company&accountId={$item.AccountID}">{$item.CompanyName}</a><br>
						Subject: {$item.Subject}
					</div>	
				</dl>
				</div>
				<div class="b2" style="border-color: {$item.Color}; background-color: {$item.BodyColor};">&nbsp;</div>
				<div class="b1" style="background-color: {$item.Color};">&nbsp;</div>
				</div>					
			{/foreach}			
						
			</div>
			</td>
		</tr>
	</tbody>
</table>

</div>
</div>
