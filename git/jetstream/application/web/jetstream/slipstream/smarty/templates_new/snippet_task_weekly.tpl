{if $cur_task_list|@count >0}
	{foreach from=$cur_task_list name=task key=key item=item}					
		<div class="event" style="margin:10px;width:250px;background-color:#{$item.EventColor};{if !$smarty.foreach.task.last}margin-bottom:10px;{/if}{if $item.Pending eq 0}border:1px solid {$item.PersonColor};{else}border:1px solid red;{/if}" onmousemove="javascript:$(this).parent().show();" onclick="javascript:window.location.href='?action=task&eventId={$item.EventID}';">
			<div class="event_head {if $item.IsClosed eq 1}closedevent{/if}" style="background:{$item.PersonColor};">
				{$item.EventName}	
			</div>
			<div>		
				<span {if $item.IsClosed eq 1}class="closedevent"{/if}>Contact: <a href="?action=contact&contactId={$item.ContactID}">{$item.FirstName|@SplitLongWords} {$item.LastName|@SplitLongWords}</a></span><br>
				<span {if $item.IsClosed eq 1}class="closedevent"{/if}>Company: <a href="?action=company&accountId={$item.AccountID}">{$item.Company|@SplitLongWords}</a></span><br>
				<span {if $item.IsClosed eq 1}class="closedevent"{/if}>Subject: {$item.Subject|@SplitLongWords}</span>		
			</div>
		</div>	
	{/foreach}
{/if}