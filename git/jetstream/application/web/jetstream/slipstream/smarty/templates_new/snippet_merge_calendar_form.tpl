<div id="secMergeCal" style="display:inline;width:100%;" show="1">
	{assign var="all_checked" value=1}
	{foreach from=$module->sales_people item=item key=key name=person}
		{if !@in_array($item.PersonID,$module->target)} 
			{assign var="all_checked" value=0}
		{/if}
		<table class="opp" id="sp_{$item.PersonID}" style="float:left;width:14%;background-color:{$item.Color}; margin-left:2px;">
			<tr>
				<td class="tl"></td>
				<td class="tc" nowrap="nowrap" align="center"><b>{$item.FirstName}<br/> {$item.LastName}<b></td>
				<td class="tr">
				<input type="checkbox" onclick="javascript:AddMergeClass($(this));"  person_id="{$item.PersonID}" id="chk_{$item.PersonID}" {if @in_array($item.PersonID,$module->target)}checked class="mergeCal"{/if}/>
				</td>
			</tr>
			<tr>
				<td class="bl"></td>
				<td class="bc"></td>
				<td class="br"></td>
			</tr>
		</table>					
	{/foreach}			
	<input type="hidden" id="cal_type" name="cal_type" value="{$module->type}" />
</div>
<div class="clearing"></div>
<div id="all_users">
	<p>
		<label for="chkbx_all">
			<input type="checkbox" onclick="CheckAllCal();" id="chkbx_all" {if $all_checked}checked{/if} />
			All Users
		</label>
	</p>
</div>
<div style="clear:both;margin-bottom:20px;"></div>
