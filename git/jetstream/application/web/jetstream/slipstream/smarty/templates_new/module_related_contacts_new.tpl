<div class="module_container" id="divContactList">
	{if $module->contacts}
		<table>
			<tr>
				<td class="label">Name</td>
				<td class="label">Title</td>
			</tr>
			{foreach from=$module->contacts item=contact}
				{if !$contact.Inactive}
					<tr>
						<td>						
							<p style="margin: 0px; padding: 0px;">
								<a href="slipstream.php?action=contact&contactId={$contact.ContactID}">
									{$contact.Name}
								</a>
							</p>
						</td>
						<td>
							{$contact.Title}
						</td>
					</tr>
				{/if}
			{/foreach}
		</table>
	{else}
		{$module->msg}
	{/if}
</div>
