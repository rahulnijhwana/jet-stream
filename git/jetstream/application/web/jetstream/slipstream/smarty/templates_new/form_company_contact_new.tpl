{*
<pre>
{$module->layout|@print_r:TRUE}
{$module->map|@print_r:TRUE}
</pre>
*}
{assign var='fields' value=$module->map.fields}
<div id="contactForm">
	<input type="hidden" name="rec_type" value="{$module->map.type}" />
	{if $module->type eq 'contact'}
		{assign var='id' value=$module->map.security.ContactID}
		<input type="hidden" name="AccountID" value="{$module->map.account.id}" />
	{else}
		{assign var='id' value=$module->map.security.AccountID}
	{/if}
	<input type="hidden" name="ID" value="{$id}" />
	{foreach from=$module->layout key=section_id item=section}
		{if $section.SectionName neq 'DefaultSection'}
		<div class="section_head" onclick="javascript:ToggleHidden('this_section{$section_id}');" > 
			<img id="toggle_this_section{$section_id}" src="images/plus.gif"/> {$section.SectionName}
		</div>
		<div class="acc_con_form" id="this_section{$section_id}" style="display:none;">
		{else}
		<div class="section_head" onclick="javascript:ToggleHidden('this_section{$section_id}');" > 
			<img id="toggle_this_section{$section_id}" src="images/minus.gif"/> {$section.SectionName}
		</div>
		<div class="acc_con_form" id="this_section{$section_id}">
		{/if}
			<table>
			{if $section.SectionName eq 'DefaultSection' && $module->type eq 'contact'} 
			<tr>
				{* --- Company Row on Contact Form --- *}
				<td class="half"><span class="acc_con_label">Company</span><br />
					{if $id && $module->map.account.name}
						<span class="datatext">{$module->map.account.name}</span>
					{else}
						<input id="account" class="ac_input full" type="text" name="account" autocomplete="off" value="{$module->map.account.name}" />
					{/if}
				</td>
				<td class="half"><span class="acc_con_label">Private</span><br />
					{if ($module->map.security.PublicItems eq 0)}
						<input id="private" type="checkbox" name="private" value="true" {if $module->map.security.Private}checked{/if} />
					{else}
						Public and cannot be made private
					{/if}
				</td>
			</tr>
			{/if}
			{foreach from=$section.rows item=row}
			{if $module->map.fields[$row.1].FieldName|regex_replace:"/[\d]/":"" eq "LongText" }
				<tr>
					<td colspan=2>
						<span class="acc_con_label">{$fields[$row.1].LabelName|default:"&nbsp;"}</span> {if $fields[$row.1].IsRequired}<span class="form_required">*</span>{/if}<br />
						{$module->FormatForForm($fields[$row.1])|default:"&nbsp;"}
					</td>
				</tr>		
			{else}
				<tr>
					<td class="half">
						<span class="acc_con_label">{$fields[$row.1].LabelName|default:"&nbsp;"}</span> {if $fields[$row.1].IsRequired}<span class="form_required">*</span>{/if}<br />
						{$module->FormatForForm($fields[$row.1])|default:"&nbsp;"}
					</td>
					<td class="half">
						<span class="acc_con_label">{$fields[$row.0].LabelName|default:"&nbsp;"}</span> {if $fields[$row.0].IsRequired}<span class="form_required">*</span>{/if}<br />
						{$module->FormatForForm($fields[$row.0])|default:"&nbsp;"}
					</td>
				</tr>
			{/if}	

			{/foreach}
			</table>	
		</div>
	{/foreach}
</div>		
