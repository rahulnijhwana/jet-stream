<input type="hidden" name="OpportunityID" value="{$module->opportunity_info.OpportunityID}" />
<input type="hidden" name="ContactID" value="{$module->opportunity_info.ContactID}" />
<input type="hidden" name="product_list" />
<input type="hidden" name="est_qty_list" />
<input type="hidden" name="est_val_list" />
<input type="hidden" name="act_qty_list" />
<input type="hidden" name="act_val_list" />
<input type="hidden" name="est_total" value="{$module->opportunity_info.EstTotal}" />
<input type="hidden" name="act_total" value="{$module->opportunity_info.ActTotal}" />

<div id="contactForm">
	<div class="acc_con_form">
		<table>
			<tr>
				<td class="label">
					<span class="acc_con_label">Opportunity Name</span>
					<span class="form_required">*</span>
				</td>
				<td class="data">
					<input type="text" class="full" name="OpportunityName" value="{$module->opportunity_info.OpportunityName}" />
					<div class="form_error" id="err_OpportunityName"></div>
				</td>
			</tr>
			<tr>
				<td class="label">
					<span class="acc_con_label">Contact</span>
				</td>
				<td class="data">
					{if $module->event_info.ContactID == -1}
						Opportunity Contact
					{else}
						<input id="Contact" type="text" class="full" name="Contact" size="40" autocomplete="off" value="{$module->opportunity_info.Contact.FirstName}{if $module->opportunity_info.Contact.FirstName and $module->opportunity_info.Contact.LastName} {/if}{$module->opportunity_info.Contact.LastName}{if $module->opportunity_info.Contact.CompanyName} ({$module->opportunity_info.Contact.CompanyName}){/if}" onkeyup="javascript:ClearContact();" onchange="javascript:ClearContact();" />
						<div class="form_error" id="err_Contact"></div>
					{/if}
				</td>
			</tr>
			{if permitted_people}
				<tr>
					<td class="label">
						<span class="acc_con_label">Salesperson</span>
					</td>
					<td class="data">
						<select name="SalesPersonID" class="full">
							{foreach from=$module->permitted_people key=pp_id item=pp_name}
								<option value="{$pp_id}" {if $pp_id eq $module->creator}selected{/if}>{$pp_name}</option>
							{/foreach}
						</select>
						<div class="form_error" id="err_SalesPersonID"></div>
					</td>
				</tr>
			{/if}
			<tr>
				<td class="label">
					<span class="acc_con_label">Sales Cycle Stage</span>
					<span class="form_required">*</span>
				</td>
				<td class="data">
					<select class="full" name="salesloc" id="salesloc" onchange="UpdatePercentSelect(this.value);">
						<option>Select a Sales Cycle Stage</option>
							{foreach from=$module->loc_names key=SCLID item=SCLName}
								<option value="{$SCLID}" {if $SCLID eq $module->opportunity_info.SalesCycleLocID}selected{/if}>{$SCLName}</option>
							{/foreach}
					</select>
					<div class="form_error" id="err_SalesCycleLocID"></div>
				</td>
			</tr>
			<tr>
				<td class="label">
					<span class="acc_con_label">Result</span>
				</td>
				<td class="data">
					<select id="result" name="result" class="full" onchange="UpdateClose(this.value);">
						<option value="0">If "Won/Lost", select result</option>
						{foreach from=$module->results key=resultID item=resultName}
							<option value="{$resultID}" {if $resultID eq $module->opportunity_info.ResultID}selected{/if}>{$resultName}</option>
						{/foreach}
					</select>
					<div class="form_error" id="err_resultID"></div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="center">
						<div class="inline">
							<span class="acc_con_label">Creation Date</span>
							<span class="form_required">*</span>
							<input type="text" name="StartDate" class="datepicker" size="6" value="{if $module->opportunity_info.StartTimeStamp}{$module->opportunity_info.StartTimeStamp|FormatDate:'date'}{/if}" />
							<div class="form_error" id="err_When"></div>
						</div>
						<div class="inline">
							<span class="acc_con_label" align="left">Exp. Close Date</span>
							<span class="form_required">*</span>
							<input type="text" name="ExpCloseDate" class="datepicker" size="6" value="{if $module->opportunity_info.EndTimeStamp}{$module->opportunity_info.EndTimeStamp|FormatDate:'date'}{/if}" />
							<div class="form_error" id="err_ExpCloseDate"></div>
						</div>
						<div class="inline" id="closedate_div" style="display: none;">
							<span class="acc_con_label" align="left">Close Date</span>
							<input type="text" name="CloseDate" class="datepicker" size="6" value="{if $module->opportunity_info.CloseDateStamp}{$module->opportunity_info.CloseDateStamp|FormatDate:'date'}{/if}" onchange="checkCreationDate();" />
						</div>
						<div class="form_error" id="err_CloseDate"></div>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="center">
						<div class="inline">
							<div id="age" name="age" class="progress">
								Age: {if $module->opportunity_info.Duration || $module->opportunity_info.Duration eq 0}{$module->opportunity_info.Duration} day(s){/if}
							</div>
						</div>
						<div class="inline">
							<div id="Percentage" name="Percentage" class="progress">
								{if $module->opportunity_info.SCLPercent}{$module->opportunity_info.SCLPercent}{else}0{/if}% to Close
							</div>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<table id="tabOfferings"></table> 
					<div class="totals">
						<span class="est_total">Est. Total: <span id="total">$0.00</span>
						</span><span class="actual_total">Actual Total: <span id="actual_total">$0.00</span> </span>
					</div> 
				</td>
			</tr>
			<tr>
				<td colspan="2" id="note_wrapper">
					<span class="acc_con_label">Note</span>
					<textarea name="Note" class="full" rows="10" id="Notes">{$module->opportunity_info.Notes}</textarea>
					<div class="form_error" id="err_Notes"></div>
				</td>
			</tr>
		</table>
	</div>
	{include file="snippet_event_alert.tpl"}
</div>


{literal}
<style type="text/css">
.actual_product {
	color: green;
	display: none;
}

.totals {
	position: relative;
}

.est_total {
	position: relative;
	left: 318px;
	top: 5px;
}

.actual_total {
	position: relative;
	left: 545px;
	top: -10px;
	display: none;
	color: green;
}

.evenrow {
	background-color: #FFDBC2;
}
</style>

<script language="javascript">
var webpath = '{/literal}{$smarty.const.WEB_PATH}{literal}';
//alert(webpath);
//defining arrays that hold the product information, value, and quautity
var products = Array();//the productIds and productNames, get from database and the Module
var values;//the values of each product, get from database and the Module, identified by productID
var est_prod_quantities = Array();//holding the quantities of each selected Estimated product, identified by productId
var est_quantities = Array();//holding the quantities of each selected Estimated product, identified by row id
var est_prod_values = Array();//holding the price of each selected Esitmated product, identified by productId
var est_values = Array(); //holding the price of each selected Estimated product, identified by row id

var act_prod_quantities = Array();//holding the quantities of each Actual selected product, identified by productId
var act_quantities = Array();//holding the quantities of each selected Actual product, identified by row id
var act_prod_values = Array();//holding the price of each selected Actual product, identified by productId
var act_values = Array(); //holding the price of each selected Actual product, identified by row id

var selected_prod_array = Array(); //the row id numbers of selected products, identified by productId
var selected_row_prodId_array = Array();//the productIds of all the rows, identified by row id

var dummy_array = Array();//to make up the css ids with its length to differentiate adding or removing rows

var loc_names;//the location Ids and location Names
var loc_percents;//the location Ids and locatioin percentages

var result_status = Array();//telling if a result is a won or lost, status=2 is won,else is lost

var percent1 = 0;
var oppid=0;


var dat = new Date();
var curday = dat.getDate();
var curmon = dat.getMonth()+1;
var curyear = dat.getFullYear();
var shortyear = (''+curyear).substring(2);

var CloseDateDB = '';

$(document).ready(function() {

	$("#event_alert_wrapper").hide();
	
	try {
		showNoteAlertHelp();
	}
	catch(err) {}
	

	$("#expand_event_alert").click(function(){

		$("#add_event_alert").slideToggle();

		if($("#eventCheckPrivate").attr('disabled')){
			$("#eventCheckPrivate").removeAttr('disabled');
			$("#label_event_private").css('color', '#5C80A5');
    		$(this).attr('value', 'Add Alert');
    		$.scrollTo("#overlay", {duration : 2000});
    		$("#add_event_alert_hidden").attr('value', '');
		}
        else {
        	$.scrollTo("#add_event_alert_wrapper", {duration : 1000});
        	$("#eventCheckPrivate").attr('disabled', 'disabled');
			$("#label_event_private").css('color', '#cacaca');
    		$(this).attr('value', 'Remove Alert');
    		$("#add_event_alert_hidden").attr('value', '1');
        }
		
	});

	$("#show_event_detail").click(function(){
		$(".event_form").slideToggle();
	});
	
	$(".event_level").click(function(){
		
		id = ($(this).attr('id'));
		
		if($("#level_event_checked").attr('value') != 1){
			$('.'+id).attr('checked', true);
			$("#level_event_checked").attr('value', 1);
		}
		else {
			$('.'+id).removeAttr('checked');
			$("#level_event_checked").attr('value', 0);
		}
	});

	$("#select_event_level").click(function(){
		$("#level_event_msg").show();
		$(".header").fadeOut();
		$(".header").fadeIn();
		$(".header").fadeOut();
		$(".header").fadeIn();
	});

	$("#delay_event_until_day").datepicker({
		showOn: "button",
		buttonImage: "images/calendar.gif",
		buttonImageOnly: true,
		disabled: true
	});
	
	$("#select_event_all").click(function(){
		toggleEventAllSelected();
	});

	$("#select_event_assigned").click(function(){
		toggleEventAssignedSelected();
	});

	$("#send_event_immediately_no").click(function(){
		$(".ui-datepicker-trigger").attr('title', 'Select a Date');
		$(".ui-datepicker-trigger").show();
		$("#delay_event_until_day" ).datepicker('enable');
		$("#event_hours").removeAttr('disabled');
		$("#event_minutes").removeAttr('disabled');
		$("#event_periods").removeAttr('disabled');
	});

	
	$("#send_event_immediately_yes").click(function(){
		$("#delay_event_until_day").attr('value', '');
		$("#delay_event_until_day").datepicker('disable');
		$("#event_hours").attr('disabled', 'disabled');
		$("#event_minutes").attr('disabled', 'disabled');
		$("#event_periods").attr('disabled', 'disabled');
		$("#spn_event_delay_until_error").text('');
		
	});
	
	$("#EventType").change(function(){
		$("#err_EventType").hide();
	});

	$("#standard_event_alert").change(function(){
		$("#err_Delivery").hide();
	});

	$("#email_event_alert").change(function(){
		$("#err_Delivery").hide();
	});

	$("#delay_event_until_day").change(function(){
		$("#err_Delay").hide();
	});

	
	var target = 0;
	
	{/literal}
	products = {$module->product_names|@json_encode};
	values = {$module->product_prices|@json_encode};	
	
	loc_names = {$module->loc_names|@json_encode};
	loc_percents = {$module->loc_percents|@json_encode};
	
	result_status = {$module->result_status|@json_encode};
	
	percent1 = '{$module->opportunity_info.SCLPercent}';
	oppid='{$module->opportunity_info.OpportunityID}';
	
	CloseDateDB = "{if $module->opportunity_info.CloseDateStamp}{$module->opportunity_info.CloseDateStamp|FormatDate:'date'}{/if}"
	//alert(CloseDateDB);
	{literal}
	//alert(percent1);
	var start_date = $("input[name='StartDate']").val();
	if (start_date != '') {
		//prev_start = $.datepicker.parseDate('m/d/y', start_date);
	}
	
	//$("#eventMiniCalendar").load("{/literal}{$smarty.const.WEB_PATH}{literal}/ajax/ajax.EventMiniCalendar.php?target=" + target);

	//RemoveProduct($("select[name='SalesPersonID']").val());
	//UpdateProductSelect();
	
	$("input[name='StartDate']").datepicker({
		showOn :"both",
		buttonImage : webpath + "/images/calendar.gif",
		buttonImageOnly :true,
		dateFormat: 'm/d/y',
		onSelect: function(dateText, inst) {
			var start_date = $(this);
			var start_parsed = $.datepicker.parseDate('m/d/y', start_date.val());
			
			var end_date = $("input[name='ExpCloseDate']");
			/*
			if (end_date.val() == '') {
				end_date.val(start_date.val());
			} else if (prev_start != '') {
				var end_parsed = $.datepicker.parseDate('m/d/y', end_date.val());
				var new_end = new Date();
				new_end.setTime(end_parsed.getTime() + start_parsed.getTime() - prev_start.getTime() + 3600000);
				end_date.val($.datepicker.formatDate('m/d/y', new_end));
			} 
			*/
			//prev_start = $.datepicker.parseDate('m/d/y', start_date.val());
			end_date.datepicker('option', 'minDate', start_parsed);		
			calage();
		}
	});

	$("input[name='ExpCloseDate']").datepicker({
		showOn :"both",
		buttonImage : webpath + "/images/calendar.gif",
		buttonImageOnly :true,
		dateFormat: 'm/d/y'
	});
	
	$("input[name='CloseDate']").datepicker({
		showOn :"both",
		buttonImage : webpath + "/images/calendar.gif",
		buttonImageOnly :true,
		dateFormat: 'm/d/y'
	});
	
	addProdTitle();
	if(oppid !== 0 && oppid!='')
		addExistingProdRow();
		addBlankProdRow();	
	
	if(percent1 == '100'){
		changecss('.actual_product', 'display', 'block');
		changecss('.actual_total', 'display', 'block');	
		document.getElementById('closedate_div').style.display= 'inline';
	}
	
	if(document.getElementsByName('StartDate')[0].value == ''){
		document.getElementsByName('StartDate')[0].value = curmon+'/'+curday+'/'+shortyear;
		calage();
	}
	//UpdateProductSelect();

	if($("#salesloc").val() == 8){
		$('select[name*="add_product"]').hide();
	}

});

function toggleEventAllSelected(){
	
	var current_state = $("#all_event_checked").attr('value');

	if(current_state != 1){
		$(".all_users").attr('checked', true);
		$("#all_event_checked").attr('value', 1);
	}
	else {
		resetEventSelected();
	}
}

function toggleEventAssignedSelected(){

	var current_state = $("#assigned_event_checked").attr('value');
	
	if(current_state != 1){
		$(".assigned_users").attr('checked', true);
		$("#assigned_event_checked").attr('value', 1);
	}
	else {
		resetEventSelected();	
	}
}



function resetEventSelected(){
	
	$(".all_users").removeAttr('checked');
	$("#all_event_checked").attr('value', 0);
	
	$(".assigned_users").removeAttr('checked');
	$("#assigned_event_checked").attr('value', 0);
}

function addProdTitle()
{			
	//alert(document.getElementById('Percentage').value);
	var tabOff = document.getElementById('tabOfferings');
	var htr = tabOff.insertRow(tabOff.rows.length);		

		headerTitles = new Array(
		'<u>Products</u>',
		'<u>Estimated</u>',
		'<div class="actual_product"><u>Actual</u></div>',
		' '
		);
	
		
	var td_colSpan = new Array(1,3,3,1);
	for (var i=0;i<headerTitles.length; ++i)
	{
		var htd = htr.insertCell(htr.cells.length);
		htd.className = "acc_con_label";
		//htd.style.fontSize="11px";
		htd.style.fontFamily="Arial";
		htd.style.fontWeight="bold";
		htd.style.textAlign = "center";
		htd.style.border =  "1px";
		htd.colSpan = td_colSpan[i];
		htd.innerHTML = headerTitles[i];
	}	
	
	htr = tabOff.insertRow(tabOff.rows.length);	

	headerTitles = new Array(
		'Product Name',
		'Value',
		'Qty',
		'Ext.',
		'<div class="actual_product">Value</div>',
		'<div class="actual_product">Qty</div>',
		'<div class="actual_product">Ext.</div>',
		'Del'
	);

	for (var i=0;i<headerTitles.length; ++i) {
		
		var htd = htr.insertCell(htr.cells.length);
		htd.className = "acc_con_label";
		//htd.style.fontSize="11px";
		htd.style.fontFamily="Arial";
		htd.style.fontWeight="bold";
		htd.style.textAlign = "center";
		htd.style.border =  "0";
		htd.innerHTML = headerTitles[i];
	}
}

function addBlankProdRow()
{			
	var tabOff = document.getElementById('tabOfferings');
	var htr = tabOff.insertRow(tabOff.rows.length);		
	var tr_number = dummy_array.length;
	htr.id='prod_tr_' + dummy_array.length;

	var prodSelect = '<div id="products_'+dummy_array.length+'">' 
				+ '</div><div id="add_product_div_'+dummy_array.length+'"><select name="add_product" onChange="AddProduct();" STYLE="width: 220px"></select></div>';
				
	var valInput = '<input type="text" name="Value'+dummy_array.length+'" id="Value'+dummy_array.length+'" size="6" class="clsTextBox" style="text-align: right; display:none;" value="$0.00" onblur="onChangeEstVal(this.value,'+tr_number+')"/>';	
	var qtyInput = '<input type="text" name="Quantity'+dummy_array.length+'" id="Quantity'+dummy_array.length+'"  size="2" class="clsTextBox" style="text-align: right; width: 42px; display:none;" value="1" onblur="onChangeEstQty(this.value,'+tr_number+')"/>';	
	//var extInput = '<input type="text" name="Ext'+dummy_array.length+'" id="Ext'+dummy_array.length+'"  size="6" class="clsTextBox" style="text-align:right;background-color:#DDDDDD;color:black;" value="$0.00" READONLY  onfocus="this.blur()"/>';
	var extInput = '<div name="Ext'+dummy_array.length+'" id="Ext'+dummy_array.length+'" align="right"></div>';

	var Act_valInput = '<div class="actual_product"><input type="text" name="Act_Value'+dummy_array.length+'" id="Act_Value'+dummy_array.length+'" size="6" class="clsTextBox" style="text-align:right; display: none;" value="$0.00"  onblur="onChangeActVal(this.value,'+tr_number+')" /></div>';	
	var Act_qtyInput = '<div class="actual_product"><input type="text" name="Act_Quantity'+dummy_array.length+'" id="Act_Quantity'+dummy_array.length+'"  size="2" class="clsTextBox" style="text-align:right; width: 42px; display:none;" value="1" onblur="onChangeActQty(this.value,'+tr_number+')"/></div>';	
	//var Act_extInput = '<div class="actual_product"><input type="text" name="Act_Ext'+dummy_array.length+'" id="Act_Ext'+dummy_array.length+'"  size="6" class="clsTextBox" style="text-align:right;background-color:#DDDDDD;color:black;" value="$0.00" READONLY onfocus="this.blur()" /></div>';
	var Act_extInput = '<div class="actual_product"><div name="Act_Ext'+dummy_array.length+'" id="Act_Ext'+dummy_array.length+'" style="text-align: right; display: none;">$0.00</div></div>';

	//var deleteX	= '<a style="font-weight:bold;color:red;" onClick="RemoveProduct({$ProductID})">X</a>';
	var deleteDiv = '<div id="deleteProduct'+dummy_array.length+'" align="center" vertical-align="middle"></div>';

	var newProd = new Array(
		prodSelect,
		valInput,
		qtyInput,
		extInput,
		Act_valInput,
		Act_qtyInput,
		Act_extInput,
		deleteDiv
	);

	for (var i=0;i<newProd.length; ++i) {
		
		var htd = htr.insertCell(htr.cells.length);
		htd.className = "acc_con_label";
		//htd.style.fontSize = "10px";
		htd.style.align = "right";	
		htd.style.verticalAlign = "middle";		
		htd.style.border =  "0";		
		//htd.style.borderSpacing =  "0";		
		//htd.style.backgroundColor= dummy_array.length%2==0?"#FCF6CF":"";
		htd.innerHTML = newProd[i];
	}
	
	if(document.getElementById('Percentage').innerHTML != '100%') {
		changecss('.actual_product', 'display', 'none');
		changecss('.actual_total', 'display', 'none');
	}
	else{
		changecss('.actual_product', 'display', 'block');
		changecss('.actual_total', 'display', 'block');
	}
	
	//g_counter=g_counter+1;
	UpdateProductSelect();
}

function AddProduct() {
	var to_add = $("select[name='add_product']").find(':selected').val();
	if (to_add != "") {

		$("#add_product_div_"+dummy_array.length).remove();
		//$("#products_"+dummy_array.length).append('<div id="product_' + to_add + '">' + products[to_add] + '&nbsp;&nbsp;&nbsp;<a style="font-weight:bold;color:red;" onClick="RemoveProduct(' + to_add + ')">X</a></div>');
		$("#products_"+dummy_array.length).append('<div id="product_' + to_add + '">' + products[to_add] + '</div>');
		$("#deleteProduct"+dummy_array.length).append('<a style="font-weight:bold;color:red;" onClick="RemoveProduct(' + to_add + ')">X</a>');
		document.getElementById('Value'+dummy_array.length).value = format_money_cents(values[to_add]);
		document.getElementById('Quantity'+dummy_array.length).value = unformat_qty(values[to_add]);

		document.getElementById('Value'+dummy_array.length).style.display="block";
		document.getElementById('Quantity'+dummy_array.length).style.display="block";

		document.getElementById('Act_Value'+dummy_array.length).style.display="block";
		document.getElementById('Act_Quantity'+dummy_array.length).style.display="block";
		document.getElementById('Act_Ext'+dummy_array.length).style.display="block";
		
		//document.getElementById('Ext'+dummy_array.length).value = format_money_cents(values[to_add] * document.getElementById('Quantity'+dummy_array.length).value);
		document.getElementById('Ext'+dummy_array.length).innerHTML = format_money_cents(values[to_add] * document.getElementById('Quantity'+dummy_array.length).value);
		//for actual product info
		document.getElementById('Act_Value'+dummy_array.length).value = format_money_cents(values[to_add]);
		document.getElementById('Act_Ext'+dummy_array.length).innerHTML = format_money_cents(values[to_add] * document.getElementById('Quantity'+dummy_array.length).value);

		
		selected_prod_array[to_add]=dummy_array.length;
		selected_row_prodId_array[dummy_array.length] = to_add;
		
		est_quantities[dummy_array.length]=1;
		est_prod_quantities[to_add] = 1;
		
		act_quantities[dummy_array.length]=1;
		act_prod_quantities[to_add] = 1;
		
		est_values[dummy_array.length]=values[to_add];
		est_prod_values[to_add] = values[to_add];
		
		act_values[dummy_array.length]=values[to_add];
		act_prod_values[to_add] = values[to_add];
		
		document.getElementById('total').innerHTML = GetTotalValue();	
		document.getElementById('actual_total').innerHTML = GetActTotalValue();	
		
		$("input[name='est_total']").val(unformat_money(GetTotalValue()));
		$("input[name='act_total']").val(unformat_money(GetActTotalValue()));
		

		//alert(dummy_array.length);	
		dummy_array[dummy_array.length]=1;
		//alert(g_counter);
		addBlankProdRow();
		UpdateProductSelect();
		
	}
}

function RemoveProduct(product_id) {

	//$("#product_" + product_id).remove();		
	$("#prod_tr_"+selected_prod_array[product_id]).remove();		
	UpdateProductSelect();	
	document.getElementById('total').innerHTML = GetTotalValue();	
	document.getElementById('actual_total').innerHTML = GetTotalValue();
	
	$("input[name='est_total']").val(unformat_money(GetTotalValue()));
	$("input[name='act_total']").val(unformat_money(GetActTotalValue()));
}

function UpdateProductSelect() {
	var product_select = $("select[name='add_product']");
	product_select.children().remove();
	//product_select.append(new Option('Please select your products', '', true));

	// Get the list of the current Products
	current_array = new Array();
	current_count = 0;
	//populate the product_list to be passed back to DB
	$("div[id^='product_']").each(function() {
		current_array[current_count] = this.id.substring(8);
		//alert(current_array[current_count]);
		current_count++;
	});
	
	$("input[name='product_list']").val(JSON.stringify(current_array));
	//populate the est_qty_list to be passed back to DB
		qty_array = new Array();
		qty_count = 0;
	
		$("input[id^='Quantity']").each(function() {
			qty_array[qty_count] = unformat_qty(this.value);
			//alert(qty_array[qty_count]);
			qty_count++;
		});
	qty_array.splice(qty_count-1,1);
	$("input[name='est_qty_list']").val(JSON.stringify(qty_array));
	//populate the act_qty_list to be passed back to DB
		qty_array = new Array();
		qty_count = 0;
	
		$("input[id^='Act_Quantity']").each(function() {
			qty_array[qty_count] = unformat_qty(this.value);
			//alert(qty_array[qty_count]);
			qty_count++;
		});
	qty_array.splice(qty_count-1,1);
	$("input[name='act_qty_list']").val(JSON.stringify(qty_array));
	
	//populate the est_val_list to be passed back to DB
		val_array = new Array();
		val_count = 0;
	
		$("input[id^='Value']").each(function() {
			val_array[val_count] = unformat_money(this.value);
			//alert(val_array[val_count]);
			val_count++;
		});
		val_array.splice(val_count-1,1);
		$("input[name='est_val_list']").val(JSON.stringify(val_array));
	//populate the act_qty_list to be passed back to DB
		val_array = new Array();
		val_count = 0;
	
		$("input[id^='Act_Value']").each(function() {
			val_array[val_count] = unformat_money(this.value);
			//alert(val_array[val_count]);
			val_count++;
		});
		val_array.splice(val_count-1,1);
		$("input[name='act_val_list']").val(JSON.stringify(val_array));
		
	//end populate input fields
	
	
	var product_count = 0;
	for (var product in products) {
		product_count++;
	}

	if(product_count > current_count){	
		product_select.append(new Option('Select a product', '', true));
	}
	else {
		//alert(products.length + " and " + current_count);
		product_select.append(new Option('All have been selected', '', true));
	}
	
	for (var product in products) {
		//alert(product);
		//alert(current_array);
		var found = false;
		for(var i = 0; i < current_array.length; i++) {
			if(current_array[i] == product) {
				var found = true;
			}
		}
		if (found) continue;		

		product_select.append("<option value='" + product + "' >" + products[product] + "</option>");
	}
	var tr_row = 0;
	$("tr[id^='prod_tr_']").each(function() {
			if(tr_row%2 == 0) $(this).addClass('evenrow');
			else $(this).removeClass('evenrow');
			tr_row++;
		});
	$("input[name='est_total']").val(unformat_money(GetTotalValue()));
	$("input[name='act_total']").val(unformat_money(GetActTotalValue()));
}

function onChangeEstQty(qty,row)
{	

	qty = unformat_qty(qty);

	if(!is_valid_number(qty))
		{
			alert('Please enter a valid number');
			document.getElementById('Quantity'+row).value = est_quantities[row]; 		
			document.getElementById('Quantity'+row).focus();
			return;
		}
	if(qty == 0)
		{
			alert('Please enter a valid number greater than 0. Or if you want to remove this product, please click the red X besides the product name.');
			document.getElementById('Quantity'+row).value = est_quantities[row]; 	
			document.getElementById('Quantity'+row).focus();
			return;
		}	
	if(row == dummy_array.length){
			alert('Please select a product first');			
			return;
		}	
		var theValue = est_values[row];//values[selected_row_prodId_array[row]];
		//alert(row+": "+theValue);		

		est_quantities[row]=qty;
		est_prod_quantities[selected_row_prodId_array[row]] = qty;
		//alert(prod_quantities[selected_row_prodId_array[row]]);
		
		document.getElementById('Quantity'+row).innerHTML = qty;
		document.getElementById('Ext'+row).innerHTML = format_money_cents(qty * unformat_money(theValue));
		document.getElementById('total').innerHTML = GetTotalValue();
		//for actual products
		if(document.getElementById('Percentage').value != '100%'){
			act_quantities[row]=qty;
			act_prod_quantities[selected_row_prodId_array[row]] = qty;
			document.getElementById('Act_Quantity'+row).value = qty;
			document.getElementById('Act_Ext'+row).innerHTML = format_money_cents(qty * unformat_money(theValue));
			document.getElementById('actual_total').innerHTML = GetActTotalValue();
		}
		qty_array = new Array();
		qty_count = 0;
	
		$("input[id^='Quantity']").each(function() {
			qty_array[qty_count] = unformat_qty(this.value);
			//alert(qty_array[qty_count]);
			qty_count++;
		});
		qty_array.splice(qty_count-1,1);
		$("input[name='est_qty_list']").val(JSON.stringify(qty_array));
		if(document.getElementById('Percentage').value != '100%'){
			$("input[name='act_qty_list']").val(JSON.stringify(qty_array));
		}
		
		$("input[name='est_total']").val(unformat_money(GetTotalValue()));
		$("input[name='act_total']").val(unformat_money(GetActTotalValue()));
}

function onChangeActQty(qty,row)
{

	qty = unformat_qty(qty);

	if(!is_valid_number(qty))
		{
			alert('Please enter a valid number');
			document.getElementById('Act_Quantity'+row).value = act_quantities[row]; 		
			document.getElementById('Act_Quantity'+row).focus();
			return;
		}
	if(qty == 0)
		{
			alert('Please enter a valid number greater than 0. Or if you want to remove this product, please click the red X besides the product name.');
			document.getElementById('Act_Quantity'+row).value = act_quantities[row]; 	
			document.getElementById('Act_Quantity'+row).focus();
			return;
		}	
	if(row == dummy_array.length){
			alert('Please select a product first');			
			return;
		}	
		var theValue = act_values[row];//values[selected_row_prodId_array[row]];
		//alert(row+": "+theValue);		
		qty = unformat_qty(qty);
		act_quantities[row]=qty;
		act_prod_quantities[selected_row_prodId_array[row]] = qty;
		//alert(prod_quantities[selected_row_prodId_array[row]]);
		//document.getElementById('total').innerHTML = GetTotalValue();		
		
		//for estimated products if it's not 100% to close
		if(document.getElementById('Percentage').value != '100%'){
		document.getElementById('Ext'+row).innerHTML = format_money_cents(qty * unformat_money(theValue));
		document.getElementById('total').innerHTML = GetTotalValue();
		}
				
		document.getElementById('Act_Quantity'+row).value = qty;
		document.getElementById('Act_Ext'+row).innerHTML = format_money_cents(qty * unformat_money(theValue));
		document.getElementById('actual_total').innerHTML = GetActTotalValue();
		
		qty_array = new Array();
		qty_count = 0;
	
		$("input[id^='Act_Quantity']").each(function() {
			qty_array[qty_count] = unformat_qty(this.value);
			//alert(qty_array[qty_count]);
			qty_count++;
		});
		qty_array.splice(qty_count-1,1);
		$("input[name='act_qty_list']").val(JSON.stringify(qty_array));
		
		$("input[name='est_total']").val(unformat_money(GetTotalValue()));
		$("input[name='act_total']").val(unformat_money(GetActTotalValue()));
}

function onChangeEstVal(val,row) {
	
	val = unformat_money(val);
	
	if(!is_valid_number(val)){
			alert('Please enter a valid number');
			document.getElementById('Value'+row).value = est_values[row]; 		
			document.getElementById('Value'+row).focus();
			return;
	}
	if(val == 0){
			alert('Please enter a valid number greater than 0. Or if you want to remove this product, please click the red X besides the product name.');
			document.getElementById('Value'+row).value = est_values[row]; 	
			document.getElementById('Value'+row).focus();
			return;
	}	
	if(row == dummy_array.length){
			alert('Please select a product first');			
			return;
	}
	var theQty = est_quantities[row];

	
	est_values[row]=val;
	est_prod_values[selected_row_prodId_array[row]] = val;
	
	document.getElementById('Ext'+row).value = format_money_cents(theQty * unformat_money(val));
	document.getElementById('Ext'+row).innerHTML = format_money_cents(theQty * unformat_money(val));
	//(VJG 5/8/2013) document.getElementById('Value'+row).value = format_money_cents(theQty * unformat_money(val));
	document.getElementById('Value'+row).value = format_money_cents(unformat_money(val)); //(VJG 5/8/2013 Added without theQty)
	document.getElementById('total').innerHTML = GetTotalValue();

	//for actual products
	if(document.getElementById('Percentage').value != '100%'){
		act_values[row]=val;
		act_prod_values[selected_row_prodId_array[row]] = val; //format_money_cents(val);
		document.getElementById('Act_Value'+row).value = format_money_cents(val);
		document.getElementById('Act_Ext'+row).value = format_money_cents(theQty * unformat_money(val));
		document.getElementById('actual_total').innerHTML = GetTotalValue();
	}
	val_array = new Array();
	val_count = 0;

	$("input[id^='Value']").each(function() {
		val_array[val_count] = unformat_money(this.value);
		val_count++;
	});
	val_array.splice(val_count-1,1);
	$("input[name='est_val_list']").val(JSON.stringify(val_array));
	if(document.getElementById('Percentage').value != '100%'){
		$("input[name='act_val_list']").val(JSON.stringify(val_array));
	}
	$("input[name='est_total']").val(unformat_money(GetTotalValue()));
	$("input[name='act_total']").val(unformat_money(GetActTotalValue()));
}

function onChangeActVal(val,row)
{
	val = unformat_money(val);
	
	if(!is_valid_number(val))
		{
			alert('Please enter a valid number');
			document.getElementById('Act_Value'+row).value = act_values[row]; 		
			document.getElementById('Act_Value'+row).focus();
			return;
		}
	if(val == 0)
		{
			alert('Please enter a valid number greater than 0. Or if you want to remove this product, please click the red X besides the product name.');
			document.getElementById('Act_Value'+row).value = act_values[row]; 	
			document.getElementById('Act_Value'+row).focus();
			return;
		}	
	if(row == dummy_array.length){
			alert('Please select a product first');			
			return;
		}	
		var theQty = act_quantities[row];
		//alert(row+": "+theQty);		
		act_values[row]=val;
		act_prod_values[selected_row_prodId_array[row]] = val;
		//alert(prod_quantities[selected_row_prodId_array[row]]);
		//document.getElementById('total').innerHTML = GetTotalValue();		
		
		document.getElementById('Act_Ext'+row).value = format_money_cents(theQty * unformat_money(val));
		document.getElementById('actual_total').innerHTML = GetActTotalValue();
		
		//for actual products
		/*
		if(document.getElementById('Percentage').value != '100%'){
		act_values[row]=val;
		act_prod_values[selected_row_prodId_array[row]] = val;
		document.getElementById('Act_Value'+row).value = val;
		document.getElementById('Act_Ext'+row).value = format_money_cents(theQty * unformat_money(val));
		document.getElementById('actual_total').innerHTML = GetTotalValue();
		}
		*/
		val_array = new Array();
		val_count = 0;
	
		$("input[id^='Act_Value']").each(function() {
			val_array[val_count] = unformat_money(this.value);
			//alert(val_array[val_count]);
			val_count++;
		});
		val_array.splice(val_count-1,1);
		$("input[name='act_val_list']").val(JSON.stringify(val_array));
		
		$("input[name='est_total']").val(unformat_money(GetTotalValue()));
		$("input[name='act_total']").val(unformat_money(GetActTotalValue()));
}

function GetTotalValue() {	
	var total = 0;
	var current_count=0;
	var current_array = Array();
	//alert("Here...");
	$("div[id^='product_']").each(function() {
		current_array[current_count] = this.id.substring(8);
		//alert(current_array[current_count]);
		current_count++;
	});
	
	for (var product in products) {
		//alert(product);
		//alert(current_array);
		var found = false;
		for(var i = 0; i < current_array.length; i++) {
			if(current_array[i] == product) {
				var found = true;
			}
		}
		if (found) {
			total = total + est_prod_quantities[product] * est_prod_values[product];
			//alert(prod_quantities[product] +"*"+values[product]+"="+total);
		}
		else continue;		
	}
	return format_money_cents(total);
}

function GetActTotalValue() {	
	var total = 0;
	var current_count=0;
	var current_array = Array();
	$("div[id^='product_']").each(function() {
		current_array[current_count] = this.id.substring(8);
		//alert(current_array[current_count]);
		current_count++;
	});
	
	for (var product in products) {
		//alert(product);
		//alert(current_array);
		var found = false;
		for(var i = 0; i < current_array.length; i++) {
			if(current_array[i] == product) {
				var found = true;
			}
		}
		if (found) {
			total = total + act_prod_quantities[product] * act_prod_values[product];
			//alert(act_prod_quantities[product] +"*"+act_prod_values[product]+"="+total);
		}
		else continue;		
	}
	//alert(total);
	return format_money_cents(total);
}
/*
var dat = new Date();
var curday = dat.getDate();
var curmon = dat.getMonth()+1;
var curyear = dat.getFullYear();
var shortyear = (''+curyear).substring(2);
*/
function UpdatePercentSelect(id) {
	if(loc_percents[id] != undefined){
		document.getElementById('Percentage').innerHTML = loc_percents[id]+'%';
	}		
	else {
		document.getElementById('Percentage').innerHTML= '';		
	}
	if(document.getElementById('Percentage').innerHTML == '100%'){
		changecss('.actual_product', 'display', 'block');
		changecss('.actual_total', 'display', 'block');
		document.getElementById('result').options[1].selected =  true;

		if(document.getElementsByName('CloseDate')[0].value == ''){
			document.getElementsByName('CloseDate')[0].value = curmon+'/'+curday+'/'+shortyear;
			document.getElementById('closedate_div').style.display= 'inline';
		}
	}
	else {
		changecss('.actual_product', 'display', 'none');
		changecss('.actual_total', 'display', 'none');

		document.getElementsByName('add_product')[0].style.display = 'block';
		document.getElementById('result').options[0].selected =  true;

		if(document.getElementsByName('CloseDate')[0].value != ''){
			document.getElementsByName('CloseDate')[0].value = '';
		}
		document.getElementById('closedate_div').style.display= 'none';
	}
}

function UpdateClose(id) {
	//alert("here:"+result_status[id]);
	if(id == '0'){	
		if(document.getElementById('Percentage').innerHTML== '100%') {
			document.getElementById('salesloc').options[0].selected =  true;
			document.getElementById('Percentage').innerHTML= '';	
		}
		document.getElementsByName('CloseDate')[0].value = '';

		changecss('.actual_product', 'display', 'none');	
		changecss('.actual_total', 'display', 'none');	
		//changecss('#closedate', 'display', 'none');	
		document.getElementById('closedate_div').style.display= 'none';

	}
	//else if(id == document.getElementById("result").options[1].value){
	else if(result_status[id] == 2){	
		var numberOfLocs = document.getElementById('salesloc').length;
		//alert(numberOfLocs);	
		document.getElementById('salesloc').options[numberOfLocs-1].selected =  true;
		document.getElementById('Percentage').innerHTML = '100%';	
		if(document.getElementsByName('CloseDate')[0].value == ''){
			document.getElementsByName('CloseDate')[0].value = curmon+'/'+curday+'/'+shortyear;		
		}
		changecss('.actual_product', 'display', 'block');	
		changecss('.actual_total', 'display', 'block');	
		//changecss('#closedate', 'display', 'block');	
		document.getElementById('closedate_div').style.display= 'inline';
	}
	else if(document.getElementById('Percentage').innerHTML== '100%') {
		document.getElementById('salesloc').options[0].selected =  true;
		document.getElementById('Percentage').innerHTML= '';	
		document.getElementsByName('CloseDate')[0].value = '';
		changecss('.actual_product', 'display', 'none');	
		changecss('.actual_total', 'display', 'none');	
		//document.getElementById('closedate').style.display= 'none';
		if(document.getElementsByName('CloseDate')[0].value == ''){	
			document.getElementsByName('CloseDate')[0].value = curmon+'/'+curday+'/'+shortyear;
		}
		document.getElementById('closedate_div').style.display= 'inline';
	}
	else{
		if(document.getElementsByName('CloseDate')[0].value == ''){	
			document.getElementsByName('CloseDate')[0].value = curmon+'/'+curday+'/'+shortyear;
		}
		document.getElementById('closedate_div').style.display= 'inline';
	}
}

function checkCreationDate(){
	var start = document.getElementsByName('StartDate')[0].value;
	var start_string_array=start.split("/");
	var startday = start_string_array[1];
	var startmon = start_string_array[0];
	var startyear = '20'+start_string_array[2];
	var startTimeStamp = new Date(startyear,startmon-1,startday);
	
	var close = document.getElementsByName('CloseDate')[0].value;
	var close_string_array=close.split("/");
	var closeday = close_string_array[1];
	var closemon = close_string_array[0];
	var closeyear = '20'+close_string_array[2];
	var closeTimeStamp = new Date(closeyear,closemon-1,closeday);
	
	if(startTimeStamp > closeTimeStamp) {alert("The Close Date ("+close + ") can't be earlier than the Creation Date (" + start+")");
		if(CloseDateDB != '') {
			document.getElementsByName('CloseDate')[0].value = CloseDateDB;
		}
		else{
			document.getElementsByName('CloseDate')[0].value = curmon+'/'+curday+'/'+shortyear;
		}
	}
	//return false;
}

function addExistingProdRow() {
var prod_counter=0;
{/literal}
	{foreach from=$module->opp_prods key=product_id item=detail}
		{if $product_id != 'est_total' && $product_id != 'act_total'}
			var tr_number = prod_counter;
			est_quantities[prod_counter]={$detail.est_qty};
			est_prod_quantities[{$product_id}] = {$detail.est_qty};

			est_values[prod_counter]={$detail.est_price};
			est_prod_values[{$product_id}] = {$detail.est_price};

			act_quantities[prod_counter]={$detail.act_qty};
			act_prod_quantities[{$product_id}] = {$detail.act_qty};

			act_values[prod_counter]={$detail.act_price};
			act_prod_values[{$product_id}] = {$detail.act_price};
			
			selected_row_prodId_array[prod_counter] = {$product_id};
			selected_prod_array[{$product_id}]=prod_counter;
		
			var tabOff = document.getElementById('tabOfferings');
			var htr = tabOff.insertRow(tabOff.rows.length);	
			
			htr.id='prod_tr_' + prod_counter;					

			var prodSelect =  '<div id="products_'+prod_counter+'">' 
						+ '<div id="product_' + {$product_id} + '">' + '{$detail.name}' + '</div></div>';				
			var valInput = '<input type="text" name="Value'+prod_counter+'" id="Value'+prod_counter+'" size="6" class="clsTextBox" style="text-align:right;" value="${$detail.est_price|number_format:2:".":","}" onblur="onChangeEstVal(this.value,'+tr_number+')"/>';			
			var qtyInput = '<input type="text" name="Quantity'+prod_counter+'" id="Quantity'+prod_counter+'"  size="2" class="clsTextBox" style="text-align:right;width:42px" value="{$detail.est_qty}" onblur="onChangeEstQty(this.value,'+tr_number+')"/>';	
			//var extInput = '<input type="text" name="Ext'+prod_counter+'" id="Ext'+prod_counter+'"  size="6" class="clsTextBox" style="text-align:right;background-color:#DDDDDD;color:black;" value="${$detail.est_ext|number_format:2:".":","}" READONLY/>';
			var extInput = '<div name="Ext'+prod_counter+'" id="Ext'+prod_counter+'" align="right">${$detail.est_ext|number_format:2:".":","}</div>';
			var Act_valInput = '<div class="actual_product"><input type="text" name="Act_Value'+prod_counter+'" id="Act_Value'+prod_counter+'" size="6" class="clsTextBox" style="text-align:right;" value="${$detail.act_price|number_format:2:".":","}"  onblur="onChangeActVal(this.value,'+tr_number+')" /></div>';
			var Act_qtyInput = '<div class="actual_product"><input type="text" name="Act_Quantity'+prod_counter+'" id="Act_Quantity'+prod_counter+'"  size="2" class="clsTextBox" style="text-align:right; width:42px" value="{$detail.act_qty}" onblur="onChangeActQty(this.value,'+tr_number+')"/></div>';
			//var Act_extInput = '<div class="actual_product"><input type="text" name="Act_Ext'+prod_counter+'" id="Act_Ext'+prod_counter+'"  size="6" class="clsTextBox" style="text-align:right;background-color:#DDDDDD;color:black;" value="${$detail.act_ext|number_format:2:".":","}" READONLY/></div>';
			var Act_extInput = '<div class="actual_product"><div name="Act_Ext'+prod_counter+'" id="Act_Ext'+prod_counter+'" align="right">${$detail.act_ext|number_format:2:".":","}</div></div>';
			var deleteDiv = '<div id="deleteProduct'+prod_counter+'" align="center" vertical-align="middle"><a style="font-weight:bold;color:red;" onClick="RemoveProduct(' + {$product_id} + ')">X</a></div>';
			
			var newProd = new Array(
				prodSelect,
				valInput,
				qtyInput,
				extInput,
				Act_valInput,
				Act_qtyInput,
				Act_extInput,
				deleteDiv
				);
			{literal}	
			for (var i=0;i<newProd.length; ++i)
			{
				var htd = htr.insertCell(htr.cells.length);
				htd.className = "acc_con_label";
				//htd.style.fontSize="10px";
				htd.style.align = "right";	
				htd.style.verticalAlign = "middle";			
				htd.style.border =  "0";
				htd.innerHTML = newProd[i];
			}
			{/literal}
		selected_prod_array[{$product_id}]=dummy_array.length;		
		dummy_array[dummy_array.length]=1;	
		prod_counter++;
		{/if}
	{/foreach}
{literal}
	
	if(prod_counter > 0){
		document.getElementById('total').innerHTML = GetTotalValue();
		document.getElementById('actual_total').innerHTML = GetActTotalValue();
		//if( percent1 == '100'){
		//	alert ("Here:"+percent1);
		//	changecss('.actual_product', 'display', 'block');
		//	changecss('.actual_total', 'display', 'block');		
		//}
	}

}


/*************************************************************************
*util functions to format money
**************************************************************************/

function format_money_cents(amt)
{
	amt = amt.toString();
	if (amt == '&nbsp;')
		return amt;
		
	var ind=amt.indexOf('.');
	var cents = '';
	if (ind >= 0)
	{
		cents = amt.substr(ind+1);
		// (VJG 5/8/2013 There needs to be something before the decimal)
				
		if(ind == 0)
		{
			amt = '0';
		}
		else
		{	
			amt = amt.substr(0, ind);
		}
		if(cents.length < 2)
			cents += 0;
		else if (cents.length > 2)
			cents = cents.substr(0, 2);
	}
	else
		cents = '00';
	var neg=amt.charAt(0)=='-';
	if (neg) amt=amt.substr(1);

	var ret = '';
	// The loop stops one short of the length to chop off the decimal
	for (var i = 0; i < amt.length; ++i)
	{
		if (i && (i % 3) == (amt.length % 3))
			ret += ',';
		ret += amt.charAt(i);
	}

	if (neg) ret='-'+ret;
	if (ret) ret += '.' + cents;
	
		if (ret == '')
			return ret;
		else
			return '$' + ret;	
}

function unformat_money(amt)
{
	amt = amt.toString();
	amt = amt.replace(/\$/g, '');
	amt = amt.replace(/,/g, '');
	amt = amt.replace(/^0+/, '');
	return amt;
}

function unformat_qty(Qty)
{
	Qty = Qty.toString();
	Qty = Qty.replace(/^0+/, '');
	return Qty;
}

function is_valid_number(val)
{

    var test = parseFloat(unformat_money(val), 10);
    if (isNaN(test))
        return false;
	
    var validChars = '$0123456789,.';
    for (var i = 0; i < test.length; ++i)
    {
        var ch = val.charAt(i);
        if (validChars.indexOf(ch) == -1) return false;
    }

    return true;
}

/******************************************
*functions to calculate age by a given date
*******************************************/
/*
var dat = new Date();
var curday = dat.getDate();
var curmon = dat.getMonth()+1;
var curyear = dat.getFullYear();
*/
function DaysInMonth(Y, M) {
    with (new Date(Y, M, 1, 12)) {
        setDate(0);
        return getDate();
    }
}
function datediff(date1, date2) {
    var y1 = date1.getFullYear(), m1 = date1.getMonth(), d1 = date1.getDate(),
	 y2 = date2.getFullYear(), m2 = date2.getMonth(), d2 = date2.getDate();

    if (d1 < d2) {
        m1--;
        d1 += DaysInMonth(y2, m2);
    }
    if (m1 < m2) {
        y1--;
        m1 += 12;
    }
    return [y1 - y2, m1 - m2, d1 - d2];
}

function calage()
{
//var calday = document.birthday.day.options[document.birthday.day.selectedIndex].value;
//var calmon = document.birthday.month.options[document.birthday.month.selectedIndex].value;
//var calyear = document.birthday.year.options[document.birthday.year.selectedIndex].value;
var whole_date = document.getElementsByName('StartDate')[0].value;
var day_string_array=whole_date.split("/");

var calday = day_string_array[1];
var calmon = day_string_array[0];
var calyear = '20'+day_string_array[2];

//alert(calmon+"/"+calday+"/"+calyear);

	if(curday == "" || curmon=="" || curyear=="" || calday=="" || calmon=="" || calyear=="")
	{
		alert("please fill all the values and click go -");
	}	
	else
	{
		var curd = new Date(curyear,curmon-1,curday);
		var cald = new Date(calyear,calmon-1,calday);
		
		var diff =  Date.UTC(curyear,curmon,curday,0,0,0) - Date.UTC(calyear,calmon,calday,0,0,0);

		var dife = datediff(curd,cald);
		//document.birthday.age.value=dife[0]+" years, "+dife[1]+" months, and "+dife[2]+" days";
		var monleft = (dife[0]*12)+dife[1];
		var secleft = diff/1000/60;
		var hrsleft = secleft/60;
		var daysleft = hrsleft/24;
		document.getElementById('age').innerHTML=daysleft+" day(s)";	
		//document.birthday.months.value=monleft+" Month since your birth";	
		//document.birthday.daa.value=daysleft+" days since your birth";	
		//document.birthday.hours.value=hrsleft+" hours since your birth";
		//document.birthday.min.value=secleft+" minutes since your birth";
		//var as = parseInt(calyear)+dife[0]+1;
		//var diff =  Date.UTC(as,calmon,calday,0,0,0) - Date.UTC(curyear,curmon,curday,0,0,0);
		//var datee = diff/1000/60/60/24;
		//document.birthday.nbday.value=datee+" day(s)";	
	}
}

//util to change css class rules
function changecss(theclass,element,value) {
	var cssRules;
	if (document.all) {
		cssRules = 'rules';
	}
	else if (document.getElementById) {
		cssRules = 'cssRules';
	}
	for (var S = 0; S < document.styleSheets.length; S++){
		for (var R = 0; R < document.styleSheets[S][cssRules].length; R++) {
			if (document.styleSheets[S][cssRules][R].selectorText == theclass) {
			document.styleSheets[S][cssRules][R].style[element] = value;
			}
		}
	}
} 
</script>

{/literal}


