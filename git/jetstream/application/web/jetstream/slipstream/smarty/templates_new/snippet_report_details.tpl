<table class="tablesorter" id="{$module->table_id}" cellspacing="1" cellpadding="0" border="0">
	<thead>
		<tr>
			<!-- <th>#</th> -->
			<th title="Company">Company</th>
			<th title="Contact">Contact</th>
			{if $module->show_user_in_report}
				<th title="User">User</th>
			{/if}
			<th title="Completed">Completed<br/>{$module->completed_event_counts}/{$module->total_event_counts}</th>
			<th title="Event Type">Event Type</th>
			<!-- <th title="Date Meeting Created">Created On</th> -->
			<th title="Original Meeting Scheduled">Scheduled On</th>
			<th title="Date Meeting Completed In Jetstream">Completed On</th>
			<th title="Subject">Subject</th>
			<th title="Meeting Notes">Notes</th>
		</tr>
	</thead>
	{if $module->event_list|@count > 0}
	{foreach from=$module->event_list name=event_list key=keyevent item=itemevent}
	<tr>
		<!-- <td align="center">{$smarty.foreach.event_list.iteration}</td> -->
		<td>{$itemevent.CompanyName}</td>
		<td>{$itemevent.ContactName}</td>
		{if $module->show_user_in_report}
			<td>{$itemevent.UserName}</td>
		{/if}
		<td class="closed_img">{if $itemevent.IsClosed eq 1}<img src="./images/checkbox_checked.png" />{else}<img src="./images/checkbox_unchecked.png" />{/if}</td>
		<td>{if $itemevent.EventName eq 1}Incoming Call{elseif $itemevent.EventName eq 2}Outgoing Call{else}{$itemevent.EventName}{/if}</td>
		<!-- <td>{$itemevent.DateEntered}</td> -->
		<td>{$itemevent.StartDate}</td>
		<td>{if $itemevent.IsClosed eq 1}{$itemevent.DateModified}{else}&nbsp;{/if}</td>
		<td>
			{if $itemevent.EventName eq 1 || $itemevent.EventName eq 2}
				{$itemevent.Subject}
			{else}
				<a href="?action=event&eventId={$itemevent.EventID}">{$itemevent.Subject}</a>
			{/if}
		</td>
		<td>
		{if $itemevent.Notes|strlen gt 350}
			<p id="less_{$module->table_id}_{$keyevent}"  class="body">{$itemevent.Notes|substr:0:350}..<a href="javascript:More('{$module->table_id}_{$keyevent}')">more</a></p>
			<p id="more_{$module->table_id}_{$keyevent}" class="body" style="display:none">{$itemevent.Notes}..<a href="javascript:More('{$module->table_id}_{$keyevent}')">less</a></p>
		{else}
			<p class="body">{$itemevent.Notes}</p>
		{/if}
		</td>
	</tr>
	{/foreach}
	{else}
	<tr><td colspan="10" align="center">No Records Found</td></tr>
	{/if}
</table>
