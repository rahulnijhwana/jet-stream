{* config_load file=slipstream.conf section="setup" *}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<link rel="shortcut icon" href="/favicon.ico" />

		<title>Jetstream&trade; {$page->title}</title>
		{if $page->core_css}
			<link rel="stylesheet" type="text/css" href="css/{$page->GetImploded('core_css')}" />
		{/if}
		{if $page->page_css}
			<link rel="stylesheet" type="text/css" href="css/{$page->GetImploded('page_css')}" />
		{/if}
		{if $page->print_css}
			<link rel="stylesheet" type="text/css" media="print" href="css/{$page->GetImploded('print_css')}" />
		{/if}
		{* Experimental - loading jquery from Google APIs - jquery must be loaded upfront for inline javascript *}
		{* <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" type="text/javascript"></script> *}
		<script src="javascript/jquery.js" type="text/javascript"></script>
		
		{* All javascript loading needs to be upfront, or else it can cause problems with IE6 *}
		{if $page->core_javascript}
			<script type="text/javascript" src="javascript/{$page->GetImploded('core_javascript')}"></script>
		{/if}
		{if $page->page_javascript}
			<script type="text/javascript" src="javascript/{$page->GetImploded('page_javascript')}"></script>
		{/if}
		{literal}
			<script type="text/javascript">
			$(document).ready(function(){
				try{
					cssdropdown.startchrome("chromemenu");
				}
				catch(e){
					alert(e);
				}
			});
			</script>
		{/literal}
		
		<script type="text/javascript" src="legacy/javascript/windowing.js"></script>
	</head>
	<body>
		<div id="pagewrapper">
			<!--  <div id="header">
				<div class="info">
					{*Call the Date structure*}
					{$smarty.now|date_format:"%A, %B %e, %Y"}
				</div>
			</div> -->

		<div id="header2" style="z-index:1000;width:100%;background-color:#545556">
			<div class="top_bar" id="top">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="top_bar"> 
					<tr>
						<td ><div style="float:left;padding:3px 0 0 7px;">Welcome, {$smarty.session.USER.FIRSTNAME} {$smarty.session.USER.LASTNAME} </div></td>
						<td align="center"> {$smarty.session.company_obj.HeaderText}  </td>
						<td align="right">
							<div id="quick_search_wrapper">
								<form action="?action=dashboard" onSubmit="javascript:return DoQuickSearch();" method="get">
									<input title="Quick Search" name="quicksearch" id="quicksearch" class="quicksearch" />
									<input type="image" src="images/search_button.png" class="quicksearch_button" />
								</form>
							</div>
							<div id="zone">
								{if $smarty.session.USER.LOCATION}
									<a id="change_location" title="Click to change your location" href="javascript:void(0);">{$smarty.session.USER.LOCATION}</a>
								{/if}
							</div>
						</td>
					</tr>
				</table>
			</div>

			{* Anchor for page top. *}
			<a name="top"></a>
			{* Nav Bar *}
			<div class="chromestyle" id="chromemenu">
				<ul>
					<li><a id="firstChild" href='?action=dashboard'>Dashboard</a></li>
					<li><a href='#' rel='add_sub_menu'>Add</a></li>
					<li><a href='?action=daily'>Calendar</a></li>
					<li><a href='?action=notes&load=first'>Notes</a></li>
					<!-- <li><a href='?action=email_hold'>Email Hold</a></li> -->
					{if $page->show_pending_emails}
						<li><a id="pending_email" class="pending_email" href='#' rel='email_sub_menu'>Email</a></li>
					{else}
						<li><a href='#' rel='email_sub_menu'>Email</a></li>
					{/if}
					{if $smarty.session.call_report == 1}
						<li><a href='?action=call_report'>Call Report</a></li>
					{/if}
					{if $smarty.session.company_obj.UseOppTracker}
						<li><a href='?action=opptracker&type=user'>Opp Tracker</a></li>
					{/if}
					{if $page->show_pending_events}
						<li><a id="pending_event" class="pending_event" href="?action=pending_events">Uncompleted</a></li>	
					{/if}
					{if $smarty.session.mpower && !($smarty.session.USER.LEVEL == 1 && $smarty.session.USER.ISSALESPERSON == 0) }
						<li><a href='{$smarty.session.mpower_link}?SN={$smarty.cookies.SN}'><img class="mpower_button" src="images/mpower_button.png" /></a></li>
					{/if}
					{if $smarty.session.company_obj.MyGoalMine}
						<li><a href="http://www.my-goalmine.com/login.aspx" target="_blank"><img class="my_goalmine_button" src="images/mygoalmine.png" /></a></li>
					{/if}
					<li><a href='#' rel='setup_sub_menu'>Tools</a></li>
					<li><a href='logout.php'>Logout</a></li>
				</ul>
			</div>
			<div id="email_sub_menu" class="dropmenudiv">
				{if $page->email_privacy_enabled}
					<a href='?action=email_incoming' rel='email_sub_menu'>Incoming</a>
					<a href='?action=email_outgoing' rel='email_sub_menu'>Outgoing</a>
				{/if}
				<a href='?action=email_hold' rel='email_sub_menu'>Unassigned</a>
			</div>
			<div id="add_sub_menu" class="dropmenudiv">
				<a href='javascript:ShowAjaxForm("CompanyContactNew&action=add&type=account");' rel='setup_sub_menu'>Company</a>
				<a href='javascript:ShowAjaxForm("CompanyContactNew&action=add&type=contact");' rel='setup_sub_menu' title="If attached to a Company, Add Company first">Contact</a>
			</div>
			<div id="setup_sub_menu" class="dropmenudiv">
				<!-- <a href='?action=template' rel='setup_sub_menu'>Email Template</a> -->
				<a href='?action=permission' rel='setup_sub_menu'>Calendar Permissions</a>
				<!-- jet-37-->
				<!--
				{if $smarty.session.USER.IMAP_ENABLED == 1}
					<a href='?action=imap_settings' rel='setup_sub_menu'>Email Sync Settings</a>
				{/if}
				-->
				<!-- jet-37-->
				{if $smarty.session.USER.LEVEL >= 2}
					<a href='?action=vacation' rel='setup_sub_menu'>Vacation/Leave</a>
				{/if}
				{if $smarty.session.USER.ALLOW_UPLOADS == 1}
					<a href='?action=dataimport'>Import</a>
				{/if}
				{if $smarty.session.company_obj.Jetfile}
					<a href='?action=filesearch'>Library</a>
				{/if}
				<!------Start Jet-37------->
				{if $page->is_UseGoogleSync}
				<a href='?action=google_sync'>Google Sync</a>
				{/if}
				<!------Start Jet-37------->
				<!--{if $page->isUseGoogleSync}

				    {if $page->hasGoogleToken}
                        {if $page->ableToGoogleDisconnect }
				            <a href='#' id='google-disconnect'>Google Disconnect</a>
                        {/if}
				    {else}
				        <a href='#' id='google-connect'>Google Connect</a>
				    {/if}

				{/if}-->

			</div>

		</div>
		<div id="buffer"></div>

			{* Content *}
			<div id="contentwrapper">
				<div id="container">
					<div id="content">
						<div id="subnav">
							<ul id="subnav_items">
							{foreach from=$page->subnav key=label item=link name=subnav}
								<li {if $smarty.foreach.subnav.first}class="first"{/if}><a href="{$link}">{$label}</a></li>
							{/foreach}
							</ul>
						</div>
						<h1 id='main_title' {if !$page->content_title}style="display:none;"{/if}>{$page->content_title}</h1>
						<h2 id='sub_title' {if !$page->content_sub_title}style="display:none;"{/if}>{$page->content_sub_title}</h2>
						<div id="content_modules">
							{* Content Modules here *}
							{foreach from=$page->content_html item=content}
								{$content}
							{/foreach}
						</div>
					</div>
				</div>
				<div id="sidebar">
				<div style="margin-bottom:10px;"><img src="images/jetstream_logo_new.jpg" /></div>
					<div id="sidebar_modules">
						{* Right Modules here *}
						{foreach from=$page->sidebar_html item=sidebar}
						{$sidebar}
						{/foreach}
					</div>
				</div>
				<div class="clearing">&nbsp;</div>
			</div>
		</div>
		<div id="footer">
			Jetstream is a trademark of JetstreamCRM.com
		</div>
		<div id="note_alerts" style="display: none;"></div>
		<div id="confirm_location" style="display: none;"></div>
		<span><input type="hidden" name="first_alert" id="first_alert" value="1" /></span>
	</body>
</html>
