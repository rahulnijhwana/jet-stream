<span id="cancel_link"></span>
<form name="saveEvent" id="saveEvent" method="post" action='#'>
	<hr height="1" color="#ffffff">
	<fieldset style="border: 1px solid rgb(148, 178, 231);" align="center">
	<legend><span class="BlackBold">{$module->form_title}</span></legend>
	
	<div style="float:left;width:300px;">
	<table border="0" width="85%" cellpadding="5" cellspacing="0" align="center">
		<tr height="50">
			<td width='15%'></td>
			<td width='1%'></td>
			<td width='84%' valign='bottom' align='left'>Fields marked with <span class='clsRequire'>*</span> are mandatory</td>
		</tr>
		<tr height=25" id="contact_row">
			<td width='15%' valign='top' class='BlackBold' align='left' > Contact</td>
			<td width='1%' class="BlackBold" valign="top" align='left'>:</td>
			<td width='84%' valign='top' class='smlBlack' align='left'>
				<input type="text" name="Contact" class="clsTextBox" size="30" id="Contact" value="{$module->ContactName}" onkeyup="javascript:ClearContact();" onchange="javascript:ClearContact();" />				
			</td>
		</tr>
		<tr height=25">
			<td width='15%' valign='top' class='BlackBold' align='left' ><nobr><span class='clsRequire'>*</span> Subject</nobr></td>
			<td width='1%' class="BlackBold" valign="top" align='left'>:</td>
			<td width='84%' valign='top' class='smlBlack' align='left'>
				<input type="text" name="Subject" class="clsTextBox" size="40" id="Subject" value="{$module->event_info.Subject}" />
				<br/><span id='spn_Subject' class='clsError'></span>
			</td>
		</tr>
		<tr height=25" id="event_type_row">
			<td width='15%' valign='top' class='BlackBold' align='left' ><nobr><span class='clsRequire'>*</span> Event Type</nobr></td>
			<td width='1%' class="BlackBold" valign="top" align='left'>:</td>
			<td width='84%' valign='top' class='smlBlack' align='left'>
				<select name="Type" class="clsSelect" id="Type" onchange="javascript:toggleEventRow();">
					<option value="" event_type=""></option>					
					{html_options options=$module->GetEventTypes() selected=$module->event_info.FormEventTypeID}
				</select>						
				<br/><span id='spn_Type' class='clsError'></span>
			</td>
		</tr>
		<tr id="user_row" height=25" {if $module->permitted_users|@count eq 0} style='display:none;'{/if}>
			<td width='15%' valign='top' class='BlackBold' align='left' ><nobr><span class='clsRequire'>*</span> User</nobr></td>
			<td width='1%' class="BlackBold" valign="top" align='left'>:</td>
			<td width='84%' valign='top' class='smlBlack' align='left'>								
				<select name="EventUser" class="clsSelect" id='EventUser'>				
					{html_options options=$module->permitted_users selected=$module->event_info.user.PersonID}								
				</select>						
				<br/><span id='spn' class='clsError'></span>
			</td>
		</tr>		
		<tr height=25">
			<td width='15%' valign='top' class='BlackBold' align='left' ><nobr><span class='clsRequire'>*</span> Start Date</nobr></td>
			<td width='1%' class="BlackBold" valign="top" align='left'>:</td>
			<td width='84%' valign='top' class='smlBlack' align='left'>
				<input type="text" name="StartDate" class="datepicker" size="30" id="StartDate" value="{$module->event_info.date_form}" />
				<br/><span id='spn_StartDate' class='clsError'></span>				
			</td>
		</tr>
		<tr height=25" class="EventRows" {if $module->event_info.Type eq 'TASK'}style="display:none;"{/if}>
			<td width='15%' valign='top' class='BlackBold' align='left' ><nobr><span class='clsRequire'>*</span> Start Time</nobr></td>
			<td width='1%' class="BlackBold" valign="top" align='left'>:</td>
			<td width='84%' valign='top' class='smlBlack' align='left'>				
					<input type="text" name="ClockPick" id="ClockPick" value="{if $module->event_info.EventCategory neq 'TASK'}{$module->event_info.start_time_form}{/if}" />
					<img src="{$smarty.const.WEB_PATH}/images/clock.gif" alt="Clock" class="ClockPicker" />
					<br/><span id='spn_ClockPick' class='clsError'></span>				

				<!--<input type="text" name="ClockPick" id="ClockPick" value="{$module->event_info.StartTime}" />
				<br/><span id='spn_ClockPick' class='clsError'></span>		-->		
			</td>
		</tr>
		<tr height=25" class="EventRows" {if $module->event_info.Type eq 'TASK'}style="display:none;"{/if}>
			<td width='15%' valign='top' class='BlackBold' align='left' ><nobr><span class='clsRequire'>*</span> Duration</nobr></td>
			<td width='1%' class="BlackBold" valign="top" align='left'>:</td>
			<td width='84%' valign='top' class='smlBlack' align='left'>
				{$module->duration_form}<input type="text" name="ClockDuration" id="ClockDuration" value="{$module->event_info.duration_form}" onKeyUp="javascript:validateDuration()" />
				<img src="{$smarty.const.WEB_PATH}/images/clock.gif" alt="Duration" class="ClockDur" />
				<br />
				<span id='spn_ClockDuration' class='clsError'></span>							
			</td>
		</tr>
		<!--<tr class="EventRows">
			<td></td>
			<td></td>
			<td>
				<div id="durationDiv" style="position:absolute;display:none;" >					
					<select size="5" name="durations" id="durations" style="width:100px;" onchange="javascript:selectDuration();"  >
						{html_options output=$module->durationArr values=$module->durationArr}						
					</select>
				</div>	
			</td>
		</tr>-->
		{if $module->eventOppId eq ""}
			{if $module->show_private}
			<tr height=25">
				<td width='15%' valign='top' class='BlackBold' align='left' >Private</td>
				<td width='1%' class="BlackBold" valign="top" align='left'>:</td>
				<td width='84%' valign='top' class='smlBlack' align='left'>
					<span id="PrivateContact"><input name="CheckPrivate" id="CheckPrivate" type="checkbox" value="true" {$module->private} /></span>
				</td>
			</tr>
			{elseif $module->disable_private neq ""}
			<tr height=25">
				<td width='15%' valign='top' class='BlackBold' align='left' >Private</td>
				<td width='1%' class="BlackBold" valign="top" align='left'>:</td>
				<td width='84%' valign='top' class='smlBlack' align='left'>
				<span id="PrivateContact">{$module->disable_private}</span>
				</td>
			</tr>
			{/if}
		{/if}
		
		
		<tr height=25" rowspan="2" style="background:#C3D9FF none repeat scroll 0 0;" id="repeate_row">
			<td valign='top' class='BlackBold' align='left' ><h4>Repeats</h4></td>
			<td class="BlackBold" align='left'></td>
			<td class='smlBlack' align='left'>
				<div class="repeat">
				<input type="hidden" name="recurring_event_id" id="recurring_event_id" value="0"/>
				<select onchange="recur_mc()" id="RecurPeriod" name="RecurPeriod">
					<option value="0" selected="selected">Does not repeat</option>
					<option value="1">Daily</option>
					<option value="2">Every weekday (Mon-Fri)</option>
					<option value="3">Every Mon., Wed., and Fri.</option>
					<option value="4">Every Tues., and Thurs.</option>
					<option value="5">Weekly</option>
					<option value="6">Monthly</option>
					<option value="7">Yearly</option>
				</select>
				</div>				
				
				
				<div id="repeat-box" style="display:none">
				<div id="recur-info">
				Repeat every:
					<select id="RecurInterval" name="RecurInterval" onChange="update_interval()">
						<option selected="selected" value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option><option value="4">4</option>
						<option value="5">5</option><option value="6">6</option>
						<option value="7">7</option><option value="8">8</option>
						<option value="9">9</option><option value="10">10</option>
						<option value="11">11</option><option value="12">12</option>
						<option value="13">13</option><option value="14">14</option>
						<option value="15">15</option><option value="16">16</option>
						<option value="17">17</option><option value="18">18</option>
						<option value="19">19</option><option value="20">20</option>
						<option value="21">21</option><option value="22">22</option>
						<option value="23">23</option><option value="24">24</option>
						<option value="25">25</option><option value="26">26</option>
						<option value="27">27</option><option value="28">28</option>
						<option value="29">29</option><option value="30">30</option>
					</select>&nbsp;				
					
					<span id="recur-pdesc">day/days</span>	

					<div id="week-days">
						<input type="checkbox" name="weekDays[]" id="weekDays0" value="0">Su
						<input type="checkbox" name="weekDays[]" id="weekDays1" value="1">Mo
						<input type="checkbox" name="weekDays[]" id="weekDays2" value="2">Tu
						<input type="checkbox" name="weekDays[]" id="weekDays3" value="3">We
						<input type="checkbox" name="weekDays[]" id="weekDays4" value="4">Th
						<input type="checkbox" name="weekDays[]" id="weekDays5" value="5">Fr
						<input type="checkbox" name="weekDays[]" id="weekDays6" value="6">Sa
					</div>	
				</div>				
				
				<div class="recur-secondary">
					<table cellspacing="0" cellpadding="0">
					<tbody>
						
						<tr valign="top">
						<td colspan="3">Range:</td></tr>						
						<tr valign="middle">
						<td id="r-recur-left" style="padding-left: 1em;">
						<h5 class="nobr">
						<label for="hr-rstart">Starts:</label></h5>
						</td>
						<td class="recurfoot">
						<input type="text" size="12" id="RecurStartDate" name="RecurStartDate" value="{$module->event_info.date_form}" autocomplete="off" class="datepicker"/></td>
						</tr>
						<tr>
						<td align="right" style="padding-left: 1ex;">Ends:</td>
						<td rowspan="2">					
						
						<div>
						<table cellspacing="0" cellpadding="0" width="100%">
						<tbody>
						<tr valign="middle">
							<td align="left">
							<input type="radio" id="RecurNoEnd" onclick="range(0)" name="RecurEndOpion" checked="checked" value="0"/> 
							<label>Never</label>
							<br/>
							<input type="radio" id="RecurUntil" name="RecurEndOpion" onclick="range(1)" value="1"/> 
							<label>Until</label>
							<span id="RecurLabelUntil" style="display:none">								
							<input type="text" class="datepicker" id="RecurEndDate" name="RecurEndDate" size="12" autocomplete="off" dir=""/>
							</span>
							
							</td>								
						</tr>
						</tbody>							
						</table></div>							
						
						</td>
						</tr>
						
						<tr><td colspan="2"/></tr>
					</tbody>
					</table>
				</div>
					
			</td>
		</tr>		
		
		
		{if not $module->eventId}
		<tr height=25" id="description_tr">
			<td width='15%' valign='top' class='BlackBold' align='left' >Description</td>
			<td width='1%' class="BlackBold" valign="top" align='left'>:</td>
			<td width='84%' valign='top' class='smlBlack' align='left'>
				<textarea name="Notes" class="clsTextBox" cols="35" rows="8" id="Notes" >{$module->event_info.Notes}</textarea>				
				<br/><span id='spn_Notes' class='clsError'></span>
			</td>
		</tr>
		{/if}
		
		<tr height=25" id="button_tr">
			<td width='15%' valign='top' class='BlackBold' align='left' ></td>
			<td width='1%' class="BlackBold" align='left'></td>
			<td width='84%' valign='top' class='smlBlack' align='left' >
				<input type="hidden" name="EventID" id="EventID" value="{$module->eventId}">
				<input type="hidden" name="ContactID" id="ContactID" value="{$module->contactId}">
				<input type="hidden" name="OppID" id="OppID" value="{$module->eventOppId}">
				<!--<input type="hidden" name="Time" id="Time" value="' +count+'">
				<input type="hidden" name="PageDate" id="PageDate" value="' +date+'">-->				
				<input type="button" name="btnSave" id="btnSave" class='grayButton' value="Save" onclick="javascript:saveEventToDb('{$smarty.cookies.SN}');" />&nbsp;
				<input type="button" name="btnCancel" id="btnCancel" class='grayButton' value="Cancel" onclick="javascript:cancelEvent({if $module->eventId neq ""}'edit'{else}'add'{/if});"/>							

			</td>
		</tr>
		<tr>
			<td colspan="3">
				{if $module->event_info.EventType eq "0"}
					Contact information for events tied to opportunities must be changed in Edit Opportunity in M-Power. Click save or cancel and follow the link to the Edit Opportunity.
				{/if}
			</td>
		</tr>
	</table>	
	</div>
	
	<div id="eventMiniCalendar" align="center" style="float:right;border:0px solid black; padding:10px; height:400px; width:300px;overflow:auto">
			<div class="loading"><img class="spinner" src="{$smarty.const.WEB_PATH}/images/slipstream_spinner.gif" \></div>
	</div>
	
	</fieldset>
	<hr height="1" color="#ffffff">	
</form>
<script language="javascript">
	{$module->curDate}
    var BASE_URL = '{$smarty.const.WEB_PATH}';
    var recurring = false;
    
	{if $module->event_recurring_info}    
	eval('var recurring = {$module->event_recurring_info};');
	{/if}
	
	{literal}
        var EventMiniCalendar = true;

		try	{		
			curDate = eval(cur_date);
		}
		catch(e) {
			curDate = JSON.decode(cur_date);
		}
		
		$(document).ready(function() {
			var target = 0;
			try {
				// Get the selected user from edit-opportunity 
				target = ssSalesPerson.getSelectedVal();				
			} catch(err) {
			}		
		
			$("#eventMiniCalendar").load("{/literal}{$smarty.const.WEB_PATH}{literal}/ajax/ajax.EventMiniCalendar.php?target=" + target);
		
			if ($("#StartDate").length) {
				$("#StartDate").datepicker( {
					showOn :"both",
					buttonImage :"./images/calendar.gif",
					buttonImageOnly :true
				});
			}		
		
			if ($("#ClockPick").length) {
				$("#ClockPick").clockpick( {
					starthour :0,
					endhour :23,
					minutedivisions :4,
					showminutes :true,
					useBgiframe :true
				});			
			}
							
			if ($("#ClockDuration").length) {
				$("#ClockDuration").clockpick( {
					starthour :0,
					endhour :24,
					minutedivisions :4,
					showminutes :true,
					military :true,
					useBgiframe :true
				});
			}		
			
			if ($(".ClockPicker").length) {
				$(".ClockPicker").clockpick( {
					starthour :0,
					endhour :23,
					minutedivisions :4,
					showminutes :true,
					useBgiframe :true
				}, function(time) {
					$("#ClockPick").val(time);
				});
			}
			
			
			if ($(".ClockDur").length) {
				$(".ClockDur").clockpick( {
					starthour :0,
					endhour :24,
					minutedivisions :4,
					showminutes :true,
					military :true,
					useBgiframe :true
				}, function(time) {
					$("#ClockDuration").val(time);
				});
			}
			
			// load event data
			load_recurring_event(recurring);
		});		
		
		function load_recurring_event(recurring) {
			if(!recurring) return;			
			$('#RecurPeriod').val(recurring.Type);			
			
			if(typeof(recurring.Interval) == 'object' && recurring.Interval.length > 0) {
				for(var i in recurring.Interval) {
					$('#weekDays' + recurring.Interval[i]).attr('checked', true);					
				}			
			} else {
				$('#RecurInterval').val(recurring.Interval);
			}
			
			$('#recurring_event_id').val(recurring.RecurringEventID);
			
			$('#RecurStartDate').val(recurring.StartDate);
			if(recurring.EndDate != '') {
				range(1);
				$('#RecurEndDate').val(recurring.EndDate);
				$('#RecurUntil').attr('checked', true);
			}
			
			
			
			recur_mc();
		}
		
		function update_interval() {
		
			var recur_selected = $('#RecurPeriod').val();
			var recur_interval = $('#RecurInterval').val();
			var interval_desc = '';
			
			switch(recur_selected) {				
				case '1':  // Daily
					var interval_desc = (recur_interval > 1) ? 'days' : 'day';
					break;					
				case '5':	// Weekly
					var interval_desc = (recur_interval > 1) ? 'weeks' : 'week';
					break;				
				case '6':	// Monthly
					var interval_desc = (recur_interval > 1) ? 'months' : 'month';
					break;
				case '7':	// Yearly
					var interval_desc = (recur_interval > 1) ? 'years' : 'year';
					break;					
			}
			
			$('#recur-pdesc').html(interval_desc);	
		}
		
		function recur_mc() {
		
			var recur_selected = $('#RecurPeriod').val();
			var recur_interval = $('#RecurInterval').val();
			
			if ($("#RecurStartDate").length) {
				$("#RecurStartDate").datepicker( {
					showOn :"both",
					buttonImage :"./images/calendar.gif",
					buttonImageOnly :true
				});
			}
			
			if ($("#RecurEndDate").length) {
				$("#RecurEndDate").datepicker( {
					showOn :"both",
					buttonImage :"./images/calendar.gif",
					buttonImageOnly :true
				});
			}
			
			switch(recur_selected) {
					
				case '2':  // Weekday
					clear_week_conf();
					var arr = new Array(1, 2, 3, 4, 5);
					set_week_conf(arr);
					$('#recur-info').hide();
					$('#repeat-box').slideDown();
					break;
					
				case '3':	// Monday, Wednessday, friday
					clear_week_conf();
					var arr = new Array(1, 3, 5);
					set_week_conf(arr);			
					$('#recur-info').hide();
					$('#repeat-box').slideDown();
					break;
					
				case '4':	// Tuesday, Thursday
					clear_week_conf();
					var arr = new Array(2, 4);
					set_week_conf(arr);						
					$('#recur-info').hide();
					$('#repeat-box').slideDown();
					break;
				case '5':	// Weekly
					$('#RecurInterval').hide();
					$('#recur-pdesc').hide();
					$('#week-days').show();		
					$('#recur-info').show();					
					$('#repeat-box').slideDown();
					break;	
					
				case '1':  // Daily
				case '6':	// Monthly	
				case '7':	// Yearly	
					clear_week_conf();
					$('#recur-pdesc').show();
					$('#week-days').hide();
					$('#RecurInterval').show();
					$('#recur-info').show();
					$('#recur-secondary').show();
					$('#repeat-box').slideDown();
					break;
					
				default :
					$('#repeat-box').slideUp();
					break;
			}
			
			update_interval();
		}
		
		
		function set_week_conf(arr) {
			for(var i in arr) {
				$('#weekDays' + arr[i]).attr('checked', true);
			}
		}
		
		
		function clear_week_conf(arr) {
			for(var i=0; i <=6; i++) {
				$('#weekDays' + i).attr('checked', false);
			}
		}
		
		function range(option) {
		
			if(option) {
				$('#RecurLabelUntil').show();
			} else {
				$('#RecurLabelUntil').hide();
				$('#RecurEndDate').val('');
			}
			
		}
		
	{/literal}	
</script>
