{*
<pre>
{$module->opp_prods|@print_r:TRUE}
</pre>
*}
<div id="contactView">
	<div>
		<table>
			<tr>
				<td class="label">Product Name</td>
				<td class="label_center">Estimated Value</td>
				<td class="label_center" >Estimated Qty</td>
				<td class="label_center">Estimated Ext.</td>
				{if $module->opportunity_info.SCLPercent eq 100}
					<td class="label_center">Actual Value</td>
					<td class="label_center">Actual Qty</td>
					<td class="label_center">Actual Ext.</td>
				{/if}
			</tr>
			{foreach from=$module->opp_prods key=product_id item=detail}
				{if $product_id != 'est_total' && $product_id != 'act_total'}
					<tr>
						<td class="subtitle">{$detail.name}</td>
						<td class="data_center">${$detail.est_price|number_format:2:".":","}</td>
						<td class="data_center">{$detail.est_qty}</td>
						<td class="data_center">${$detail.est_ext|number_format:2:".":","}</td>
						{if $module->opportunity_info.SCLPercent eq 100}
							<td class="actual_opp_prod">${$detail.act_price|number_format:2:".":","}</td>
							<td class="actual_opp_prod">{$detail.act_qty}</td>
							<td class="actual_opp_prod">${$detail.act_ext|number_format:2:".":","}</td>
						{/if}
					</tr>
				{elseif $product_id == 'est_total'}
					<tr>
						<td class="label_right" colspan="3" valign="middle">Estimated Total</td>
						<td class="data_center">${$detail|number_format:2:".":","}</td>
				{elseif $product_id == 'act_total'}			
						{if $module->opportunity_info.SCLPercent eq 100}
							<td class="label_right" colspan="2">
								Actual Total
							</td>
							<td class="actual_opp_prod">${$detail|number_format:2:".":","}</td>
						{/if}
					</tr>
				{/if}
					
			{/foreach}
		</table>	
	</div>
</div>
