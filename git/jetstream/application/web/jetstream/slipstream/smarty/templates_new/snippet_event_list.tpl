{if $module->events}
	{foreach from=$module->events name=events key=key item=item}
		<div class="event {if $item.pending eq 1}pendingevent{/if}" style="width:98%;
			{if $item.eventtype.EventColor neq ''}background-color:#{$item.eventtype.EventColor};{/if}{if !$smarty.foreach.events.last}margin-bottom:10px;{/if}{if $item.user.Color eq '808080'}border:1px solid #{$item.user.Color}; {else} border:1px solid {$item.user.Color}; {/if}"
			{if $item.is_viewable}
				{if $item.RepeatParent}
					onclick="javascript:LoadRptngEvnt('{$item.RepeatParent}', '{$item.id_enc}')"
				{else}
					onclick="javascript:location.href='slipstream.php?action=event&eventId={$item.EventID}'"
				{/if}
			{/if}>
			<div class="event_head {if $item.Closed eq 1}closedevent{/if}" style="background:{$item.user.Color};">				
				{if $item.eventtype.Type eq 'TASK'}
					<div class="task_head_wrapper">
						<div class="task_left">
							{$item.orig_date_string}<br>
							{$item.eventtype.EventName|@SplitLongWords}<br>
							<!-- <input type="button" value="Complete" /> -->
						</div>
						<div class="task_icon">
							{$item.Priority}
						</div>
					<div class="clear"></div>
					</div>
				{else}
					{$item.date_string}<br>
					{$item.start_time_string} - {$item.end_time_string} ({$item.duration_string})
				{/if}
			</div>
			<div {if $item.Closed}class="closedevent"{else}class="openevent"{/if}>
				{if $item.is_viewable}
					{if $item.eventtype.Type neq'TASK'}
						<p class="subtitle">{$item.eventtype.EventName|@SplitLongWords}</p>
					{/if}
					<table class="sidebar_tbl">
						<tr>
							<td class="label"><span>User</span></td>
							<td>{$item.user.FirstName|@SplitLongWords} {$item.user.LastName|@SplitLongWords}</td>
						</tr>
						{if $item.AccountID}
						<tr>
							<td class="label"><span>Company</span></td>
							<td>
								<a onclick='javascript:NoBubble(event,"?action=company&accountId={$item.AccountID}");'>
									{$item.AccountName}
								</a>
							</td>
						</tr>
						{elseif $item.Contact.FirstName}
						<tr>
							<td class="label"><span>Contact</span></td>
							<td>
								<a onclick='javascript:NoBubble(event,"?action=contact&contactId={$item.ContactID}");'>
									{$item.Contact.FirstName|@SplitLongWords} {$item.Contact.LastName|@SplitLongWords}
								</a>
							</td>
						</tr>
						{/if}

						{if $item.Contact.CompanyName}
						<tr>
							<td class="label"><span>Company</span></td>
							<td>
								<a onclick='javascript:NoBubble(event,"?action=company&accountId={$item.Contact.AccountID}");'>
									{$item.Contact.CompanyName|@SplitLongWords}
								</a>
							</td>
						</tr>
						{/if}
						<tr>
							<td class="label"><span>Subject</span></td>
							<td>{$item.Subject|@SplitLongWords}</td>
						</tr>
						{if $item.Location}
							<tr>
								<td class="label"><span>Location</span></td>
								<td>{$item.Location|@SplitLongWords}</td>
							</tr>
						{/if}
					</table>
				{else}
					<span class="label">User</span> {$item.user.FirstName|@SplitLongWords} {$item.user.LastName|@SplitLongWords}<br />
					Personal
				{/if}
			</div>
		</div>
	{/foreach}
{else}
	<span class='BlackBold'>{$module->msg}</span>
{/if}
