<div id="dashboard">
	<form onsubmit="javascript:AjaxLoadPage(1);return false;" id="SlipstreamSearch" action="#" method="post">
		<div id="search">
			<div id="search_div">
				<input type="text" name="SearchString" id="SearchString" size="32" value="{if $module->search.search_string}{$module->search.search_string}{/if}" /> <input type="submit" name="SearchButton" value="Search" />
				in <select onchange="javascript:AjaxLoadPage(1);" name="SearchType" id="SearchType">{html_options options=$module->search_types selected=$module->search.query_type}</select>
			</div>
			<div id="per_page">
				Show <select onchange="javascript:AjaxLoadPage(1);"  name="RecordsPerPage" id="RecordsPerPage" >{html_options output=$module->records_per_page values=$module->records_per_page selected=$module->search.records_per_page}</select>
			</div>
			<div id="search_filters">
				<div id="search_filters_checkboxes">
					{if $module->suppress}
						<div style="display:none"><input type="checkbox" name="AssignedSearch" id="AssignedSearch" value="1" checked="checked" /></div>
					{else}
						<p><input type="checkbox" name="AssignedSearch" id="AssignedSearch" value="1" onchange="javascript:AjaxLoadPage(1);" {if $module->search.assigned_search}checked{/if} /><label for="AssignedSearch">Assigned to me</label></p>
					{/if}
					<p>
						<input type="checkbox" id="selectInactive" name="selectInactive" value="1" onclick="javascript:AjaxLoadPage(1);" {if $module->search.inactive_type}checked{/if} />
						<label for="selectInactive">Include inactive</label>
					</p>
				</div>
				<div id="merge">
					{if $smarty.session.USER.LEVEL neq 1}<p><input id="btnMerge" type="button" value="Merge Records" onclick="javascript:doMerge();" style="display:none;"/><label for="chkMergeRecord">Merge</label><input type="checkbox" id="chkMergeRecord" onclick="javascript:toggleMergeButton(this.checked)"/></p>{/if}
				</div>
				<div style="clear: both"></div>
			</div>
		</div>
		<div style="clear: both"></div>
	</form>
	<div id="dashboardsearch">
	{include file="snippet_dashboard_search_list.tpl"}
	</div>
</div>