<div class="jet_datagrid" align="center">
	<table>
		<tr>
			<td class="label">Opportunity Name</td>
			<td colspan="3">
				{$module->opportunity_info.OpportunityName}
			</td>
		</tr>
		<tr>
			<td class="label">Salesperson</td>
			<td colspan="3">
				{$module->opportunity_info.salesperson.FirstName} {$module->opportunity_info.salesperson.LastName}
			</td>
		</tr>	
		<tr>
			<td class="label">Contact</td>
			<td colspan="3" {if $module->opportunity_info.Closed eq 1}class="closedevent"{/if}>
				<div style="padding:1px 10px 0px 0px; float: left;">
					<a href="javascript: ToggleContactInfo()">
						<img id="toggle_contact" src="images/plus.gif"/>
					</a>
				</div>
				<a style="float:left;" href="slipstream.php?action=contact&contactId={$module->opportunity_info.ContactID}">
					{$module->opportunity_info.Contact.FirstName} {$module->opportunity_info.Contact.LastName}
				</a>
				&nbsp;{if $module->opportunity_info.Contact.AccountID}(<a href="slipstream.php?action=company&accountId={$module->opportunity_info.Contact.AccountID}">{$module->opportunity_info.Contact.CompanyName}</a>){/if}
			</td>
		</tr>
		<td class="label">Sales Cycle Stage</td>
			<td class="left {if $module->opportunity_info.Closed eq 1}closedevent{/if}" colspan="3">
				<span class="heavy">{$module->opportunity_info.SCLname}</span> 			
			</td>
		</tr>
		{if $module->opportunity_info.ResultName}
			<tr>
				<td class="label">Result</td>
				<td class="left {if $module->opportunity_info.Closed eq 1}closedevent{/if}" colspan="3">
					<span class="heavy">{$module->opportunity_info.ResultName}</span> 			
				</td>
			</tr>
		{/if}
		<tr id="event_contact_cells" style="display: none;">
			<td colspan="4" style="background: #DDD; padding: 0px 10px 10px 10px;">

				<div id="event_contact_info" style="margin-top: 10px; background-color: #F8F8F8;">
					<div class="loading" style="border:0px;">
						<img class="spinner" src="images/slipstream_spinner.gif" />
					</div>
					<script type="text/javascript">
						{literal}
						
						var contact_loaded = false;
						
						function ToggleContactInfo() {
							if($('#event_contact_cells').css('display') == 'none' || $('#event_contact_cells').css('display') == '') {
								$('#event_contact_cells').show();
								$('#toggle_contact').attr('src', 'images/minus.gif');
								if (!contact_loaded) {
									contact_loaded = true;
									LoadContact();
								}
							} else {
								$('#event_contact_cells').hide();
								//css('display', 'none');
								$('#toggle_contact').attr('src', 'images/plus.gif');
							}
						}		
						
						function LoadContact() {
							$.ajax({
								url: "ajax/ajax.searchdetail.php",
								data: ("request_id=contact_{/literal}{$module->opportunity_info.ContactID}{literal}"),
								type: "POST",
								dataType: "html",
								success: function(data){
									$("#event_contact_info").html(data);
								}
							});
						}
						{/literal}
					</script>
				</div>
			</td>
		</tr>
		<tr>
			<td class="label">Creation Date</td>
			<td class="left {if $module->opportunity_info.Closed eq 1}closedevent{/if}" colspan="1">
				<span class="heavy">{$module->opportunity_info.Start}</span> 
			</td>
			<td class="label">Opportunity Age</td>
			<td class="right">			
				<span class="heavy">{$module->opportunity_info.Duration} {if $module->opportunity_info.Duration eq "1"}day{else}days{/if}</span> 
			</td>
		</tr>
		<tr>
			<td class="label">Exp. Close Date</td>
			<td class="right">	
				<span class="heavy">{$module->opportunity_info.ExpCloseDate1}</span> 
			</td>
			<td class="label">% to Close</td>
			<td class="right">			
				<span class="heavy">{$module->opportunity_info.SCLPercent}%</span> 
			</td>		
		</tr>	
		{if $module->opportunity_info.CloseDate}
			<tr>
				<td class="label">Close Date</td>
				<td class="right" colspan="3">			
					<span class="heavy">{$module->opportunity_info.CloseDate1}</span> 
				</td>		
			</tr>
		{/if}
	</table>		

	<div id="opp_prod_info" style="margin-top: 10px; background-color: #F8F8F8;">
		<div class="loading" style="border:0px;">
			<img class="spinner" src="images/slipstream_spinner.gif" />
		</div>
		<script type="text/javascript">
			{literal}
			
			var product_loaded = false;
			
			function ToggleProductInfo() {
				if(true) {
					$('#opp_prod_cells').show();
					$('#toggle_product').attr('src', 'images/minus.gif');
					if (!product_loaded) {
						product_loaded = true;
						LoadProduct();
					}
				}
				else {
					$('#opp_prod_cells').hide();
					$('#toggle_product').attr('src', 'images/plus.gif');
				}
			}
			function LoadProduct() {
				$.ajax({
					url: "ajax/ajax.opp_prod_detail.php",
					data: ("request_id=opportunity_{/literal}{$module->opportunity_info.OpportunityID}{literal}"),
					type: "POST",
					dataType: "html",
					success: function(data){
						$("#opp_prod_info").html(data);
					}
				});
			}
			{/literal}
		</script>
	</div>
</div>


<script language="javascript" type="text/javascript">
	{literal}	
	$(document).ready(function() {
		ToggleProductInfo();
	})
	{/literal}
</script>	

