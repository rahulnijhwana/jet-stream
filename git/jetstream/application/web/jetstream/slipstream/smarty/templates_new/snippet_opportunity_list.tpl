{if $module->opportunities}
	{foreach from=$module->opportunities name=opportunities key=key item=item}
		<div class="event {if $item.pending eq 1}pendingevent{/if}" style="width:250px;
			{if $item.eventtype.EventColor neq ''}background-color:#{$item.eventtype.EventColor};{/if}{if !$smarty.foreach.opportunities.last}margin-bottom:10px;{/if}{if $item.user.Color eq '808080'}border:1px solid #{$item.user.Color}; {else} border:1px solid {$item.user.Color}; {/if}"
			onclick="javascript:location.href='slipstream.php?action=opportunity&opportunityId={$item.OpportunityID}'">
			<div class="event_head {if $item.Closed eq 1}closedevent{/if}" style="background:{$item.user.Color};">				
					Start Date: {$item.StartDate}<br>
					{if $item.CloseDate != ''}Close Date: {$item.CloseDate|date_format:"%m/%d/%y"}{else}Expected Close Date: {$item.ExpCloseDate|date_format:"%m/%d/%y"}{/if}
			</div>
			<div class="openevent">
				<table class="sidebar_tbl">
					<tr>
						<td class="label"><span>Opportunity</span></td>
						<td>{$item.OpportunityName|@SplitLongWords}</td>
					</tr>
					<tr>
						<td class="label"><span>Salesperson</span></td>
						<td>
							{$item.salesperson.FirstName|@SplitLongWords} {$item.salesperson.LastName|@SplitLongWords}
						</td>
					</tr>
					<tr>
						<td class="label"><span>Contact</span></td>
						<td><a onclick='javascript:NoBubble(event,"?action=contact&contactId={$item.ContactID}");'>{$item.Contact.FirstName|@SplitLongWords} {$item.Contact.LastName|@SplitLongWords}</a></td>
					</tr>
					<tr>
						<td class="label"><span>Company</span></td>
						<td><a onclick='javascript:NoBubble(event,"?action=company&accountId={$item.Contact.AccountID}");'>{$item.Contact.CompanyName|@SplitLongWords}</a></td>
					</tr>
				</table>
			</div>
		</div>
	{/foreach}
{else}
	<span class='BlackBold'>{$module->msg}</span>
{/if}
