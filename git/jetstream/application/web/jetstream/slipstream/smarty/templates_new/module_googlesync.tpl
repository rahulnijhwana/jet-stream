<div id="wrap">
	<div class="module_container_overflow">

			<h3 id="emaildiv">{ $module->email}</h3>
			<input type="hidden" value="$module->email" id="syncEmailId">
			<button id="rmbutton" onclick="removeSyncMail();" >Remove Account</button>
			<button id="chbutton" onclick="changeSyncMail('{$module->newurl}');" >Change Account</button>
			<button id="addbutton" onclick="addSyncMail('{$module->authurl}');" >Add Account</button>
	</div>
</div>
{literal}
<script type="text/javascript">

if(document.getElementById("emaildiv").innerHTML != ""){
	document.getElementById("addbutton").style.display = "none";
}else{
	document.getElementById("rmbutton").style.display = "none";
	document.getElementById("chbutton").style.display = "none";
}
function removeSyncMail(){

var cid = document.getElementById("syncEmailId").value;

	$.ajax({
			url: '/jetstream/ajax/ajax.GoogleDisconnect.php',
			data: cid,
			method: 'post',
			success: function(res){
				document.getElementById("emaildiv").innerHTML = "";
				document.getElementById("rmbutton").style.display = "none";
				document.getElementById("chbutton").style.display = "none";
				document.getElementById("addbutton").style.display = "block";
				}
		});


}
function changeSyncMail(newurl){
var title = 'Change your Gmail id for Google Sync';
	$.ajax({
			url: '/jetstream/ajax/ajax.GoogleDisconnect.php',
			success: function(res){
				popupCenter(newurl,title, 600, 500);
				}
		});
		
}
function addSyncMail(url){
		if(url){
				var title = 'Add your Gmail id for Google Sync';
				popupCenter(url,title, 600, 500);
			}
			else{
				return false;
			}
}
function popupCenter(url, title, w, h) {
	
        var left = (window.screen.width/2)-(w/2);
        var top = (window.screen.height/2)-(h/2);
        newwindow = window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
        
}

</script>
{/literal}
