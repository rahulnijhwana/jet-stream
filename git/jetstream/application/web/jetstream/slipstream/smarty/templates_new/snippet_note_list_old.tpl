<span style="float:right;" id="spanPageLinkTop">
  {$module->page_links}
</span>
<span style="float:right;" id="pageInfo">{$module->page_info}</span>
{section name=note loop=$module->note_list}
	<div class="{cycle values="note, note_alt"}">
		<p><b>Date:</b> {$module->note_list[note].TimeStamp}</p>				
		
		{if $module->note_list[note].Creator|trim neq ""}
		<p><b>User:</b> {$module->note_list[note].Creator}</p> 
		{/if}

		{if $module->note_list[note].Company}
			<p><b>Company:</b> <a href="?action=company&accountId={$module->note_list[note].AccountID}">{$module->note_list[note].Company}</a></p>
		{/if}
		{if $module->note_list[note].ContactID neq ""}
			<p><b>Contact:</b> <a href="?action=contact&contactId={$module->note_list[note].ContactID}">{$module->note_list[note].Contact}</a></p>
		{/if}
		{if $module->note_list[note].Opp neq ""}
			<p><b>Opp:</b> {$module->note_list[note].Opp}</p>
		{/if}
		{if $module->note_list[note].Type neq ""}
			<p><b>Type:</b> {$module->note_list[note].Type}</p>
		{/if}		
		
		{if isset($module->note_list[note].Private)}
			{if $module->note_list[note].Private eq "1"}
				<p><b>Private:</b> <img src="images/checkbox_checked.png"></p>
			{else if $module->note_list[note].Private eq "0"}
				<p><b>Private:</b> <img src="images/checkbox_unchecked.png"></p>
			{/if}
		{/if}		
		
		<p><b>Subject:</b> {$module->note_list[note].Subject}</p>

		
		
		{if $module->note_list[note].EmailContentID > 0}
		
		<p><b>From:</b> <a href="mailto:{$module->note_list[note].FromEmail}">{$module->note_list[note].From}</a><img src="images/email.gif" style="margin-left: 5px;"/></p>
		<p><b>To:</b> {$module->note_list[note].To}</p>
		{if $module->note_list[note].Cc|trim neq ''}
			<p><b>Cc:</b> {$module->note_list[note].Cc}</p> 
		{/if}
		
		<div>
		{if $module->note_list[note].Attachment}
			<p><b>Attached:</b> {$module->note_list[note].Attachment}</p>
		{/if}
		</div>
		
		<div><p><a href="ajax/ajax.getEmailByID.php?email={$module->note_list[note].EmailContentID}" target="blank">Complete View</a></p></div>
		<div style="border:1px solid #DDD;padding:0;">
		<iframe src="ajax/ajax.getEmailByID.php?email={$module->note_list[note].EmailContentID}" style="width:100%;" frameborder="0" /></iframe>
		</div>
		{else}
		<div class="scroll">
		{if $module->note_list[note].Body|strlen gt 350}			
			<p id="email_less_{$smarty.section.note.index}"  class="body">{$module->note_list[note].Body|substr:0:350}..<a href="javascript:More('{$smarty.section.note.index}')">more</a></p>
			<p id="email_more_{$smarty.section.note.index}" class="body" style="display:none">{$module->note_list[note].Body}..<a href="javascript:More('{$smarty.section.note.index}')">less</a></p>
			
		{else}
			<p class="body">{$module->note_list[note].Body}</p>
		{/if}
		</div>
		{/if}
		
	</div>
{/section}
{if $module->no_records}{$module->no_records}{/if}
<span style="float:right;" id="spanPageLinkBottom">
  {$module->page_links}
</span>
<input type="hidden" name="totRec" id="totRec" value="{$module->tot_rec}">

<script language="javascript" type="text/javascript">
{literal}


function More(id) {
	$('#email_less_' + id).toggle();
	$('#email_more_' + id).toggle();
}
// Hide and show forces a redraw to fix display issues in IE6
$('iframe').load(function(){$('iframe').hide();$('iframe').show();});

{/literal}
</script>

