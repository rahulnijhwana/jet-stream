<div id="addNote" style="display:none;">
	<form method="post" id="addNewNote" name="addNewNote">	
	<fieldset style="border: 1px solid #fff;" align="center">
	<legend><span class="title">Add Note</span></legend>

	<table border="0" cellpadding='5' cellspacing='0' align='center' width="95%" id="tblAddNote">		
		<tr>
			<td width="10%" class="BlackBold" align="right">
				<span class="clsRequire">*</span>&nbsp;Subject
			</td>
			<td colspan="2" width="90%" valign="top" class="inputSection" align="left">
				<input type="text" name="NoteSubject" id="NoteSubject" class="clsTextBox" maxlength="50" />		
				<br/><span id="spn_NoteSubject" class="clsError"></span>
			</td>
		</tr>
		<tr>
			{if $module->has_template eq 1}
				<td width='100%' colspan="3" valign='top' id="noteTemplateInnerTd">
				{literal}
				<script type="text/javascript">						
					createNoteTemplate({/literal}{$module->note_template_fields}{literal});
				</script>
				{/literal}
					<br/><span id='spn_NoteText' class='clsError'></span>
				</td>
			{else}
				<td width='10%' valign='top' class='BlackBold' align='right' valign="top">
					<span class='clsRequire'>*</span>&nbsp;Text
				</td>
				<td colspan="2" width='90%' class='inputSection' align='left'  valign="top" >
					<textarea name='NoteText' id='NoteText' class='clsTextBox' cols='70' rows="15"></textarea>
					<br/><span id='spn_NoteText' class='clsError'></span>
				</td>
			{/if}				
		</tr>
		<tr>
			<td width='10%' class='BlackBold' align='right' valign="top">
				<span id="label_private">Private</span>
			</td>
			<td width='90%' class='smlBlack' align='left' valign="top">				
				{if $module->default_private eq 1}
					<img src="images/checkbox_checked.png">
				{else}
					<input name="CheckPrivate" id="CheckPrivate" type="checkbox" value="true" />
				{/if}
			</td>
		</tr>
		<tr {if !$smarty.session.note_alert}style="display: none"{/if}>
			<td width='10%' class='BlackBold' align='right' valign="top">
				<span id="label_note_alert">Note Alert</span>
			</td>
			<td width='90%' class='smlBlack' align='left' valign="top">
				{include file="snippet_note_alert.tpl"}
			</td>
		</tr>
		<tr>
			<td colspan="3" class="smlBlackBold">
				<div id="save_note_buttons">
					<div id="save_button_wrapper">
						<input type="button" class="grayButton" id="btnAddNote" name="btnAddNote" value="Save" />
						<input type="button" class="grayButton" value="Cancel" name="cancel" id="buttonCancel" />
					</div>
					<div id="saving">
						<img src="images/spinner.gif" alt="saving" />
					</div>
				</div>
			</td>
		</tr>
	</table>
	<input type="hidden" id="all_checked" name="all_checked" />
	<input type="hidden" id="assigned_checked" name="assigned_checked" />
	<input type="hidden" id="level_checked" name="level_checked" />
	<input type="hidden" id="sn" name="sn" value="{$smarty.cookies.SN}" />
	
	<input type="hidden" id="NoteObjType" name="NoteObjType" value="" />
	<input type="hidden" id="NoteObjVal" name="NoteObjVal" value="" />
	<input type="hidden" id="NoteSplType" name="NoteSplType" value="" />
	</fieldset>
	</form>	
</div>
<div class="clsSucc" id="noteSuccMsg" align="center" width="100%"></div><br/>
