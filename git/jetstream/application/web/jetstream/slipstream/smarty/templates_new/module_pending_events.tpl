{if $module->pending_events}
	{foreach from=$module->pending_events name=events key=key item=item}		
		<div class="event" style="background-color:#{$item.eventtype.EventColor};border-color:{$item.user.Color};{if !$smarty.foreach.events.last}margin-bottom:10px;{/if}"  
		onclick="javascript:location.href='slipstream.php?action=event&{if $item.RepeatParent}rep={$item.id_enc}{else}eventId={$item.EventID}{/if}{if $module->target neq ''}&target={$module->target}{/if}'">
			<div class="event_head" style="cursor:default;background:{$item.user.Color};">
				<span class="text">
				{$item.date_string}<br>
				{$item.start_time_string} - {$item.end_time_string} ({$item.duration_string})
				</span>
				<span class="buttons">
					{if !($item.DealID)}
						{if $item.is_edit_functional}
							<a onclick="event.cancelBubble = true;javascript:ShowAjaxForm('EventNew&{if $item.RepeatParent}rep={$item.id_enc}{else}eventid={$item.EventID}{/if}&action=edit');event.cancelBubble = true;">Reschedule</a> |
							<a onclick="event.cancelBubble = true;javascript:CompletePendingEvent({if $item.RepeatParent}'', '{$item.id_enc}'{else}'{$item.EventID}', ''{/if});">Complete</a>
						{else}
							<a onclick="event.cancelBubble = true;javascript:CompletePendingEvent({if $item.RepeatParent}'', '{$item.id_enc}'{else}'{$item.EventID}', ''{/if});event.cancelBubble = true;">Complete</a>
						{/if}
					
						| <a onclick="javascript:CompletePendingEvent({if $item.RepeatParent}'', '{$item.id_enc}'{else}'{$item.EventID}', ''{/if}, 'remove');event.cancelBubble = true;">Remove</a>
					{else}
						<a onclick="event.cancelBubble = true;" target="" href="javascript:openOppPopup({$item.DealID}, {$item.user.PersonID})" title="Reschedule">Reschedule</a> |
						<a onclick="event.cancelBubble = true;" target="" href="javascript:openOppPopup({$item.DealID}, {$item.user.PersonID})" title="Complete">Complete</a> |
						<a onclick="event.cancelBubble = true;" target="" href="javascript:openOppPopup({$item.DealID}, {$item.user.PersonID})" title="Complete">Remove</a>
					{/if}
				</span>
			</div>
			<div class="openevent">
				<p class="subtitle">{$item.eventtype.EventName|@SplitLongWords}</p>
				<table class="sidebar_tbl">
					<tr>
						<td class="label"><span>User</span></td>
						<td>{$item.user.FirstName} {$item.user.LastName}</td>
					</tr>
					<tr>
						<td class="label"><span>Contact</span></td>
						<td>
							<a onclick='javascript:NoBubble(event,"?action=contact&contactId={$item.ContactID}");'>
								{$item.Contact.FirstName} {$item.Contact.LastName}
							</a>
						</td>
					</tr>
					<tr>
						<td class="label"><span>Company</span></td>
						<td>
							<a onclick='javascript:NoBubble(event,"?action=company&accountId={$item.Contact.AccountID}");'>
								{$item.Contact.CompanyName|@SplitLongWords}
							</a>
						</td>
					</tr>
					<tr>
						<td class="label"><span>Subject</span></td>
						<td>{$item.Subject|@SplitLongWords}</td>
					</tr>
					{if $item.Location}
						<tr>
							<td class="label"><span>Location</span></td>
							<td>{$item.Location|@SplitLongWords}</td>
						</tr>
					{/if}
				</table>
			</div>
		</div>
	{/foreach}
	{include file="form_close_event.tpl"}
{else}
	<span class='BlackBold'>None found</span>
{/if}

<script language="javascript" type="text/javascript">
{literal}
function openOppPopup(dealId, personId) {	
	var win = Windowing.openSizedWindow('legacy/shared/edit_opp2.php?SN={/literal}{$smarty.cookies.SN}{literal}&detailview=1&reqOpId=' + dealId + '&reqPersonId=' + personId + '#' + dealId + '_' + personId, 630, 1000);
}
{/literal}
</script>
