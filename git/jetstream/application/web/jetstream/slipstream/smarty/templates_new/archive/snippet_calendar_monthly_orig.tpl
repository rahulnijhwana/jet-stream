<div style="text-align:center;margin-bottom:10px;">
	<a href="slipstream.php?action=monthly&day={$module->previous}{if $module->target!=''}&target={$module->target}{/if}"><img src="images/left-arrow-blue.png"/></a><span style="margin:0 50px;font-size:1.4em;font-variant:small-caps;">{$module->display_date}</span><a href="slipstream.php?action=monthly&day={$module->next}{if $module->target!=''}&target={$module->target}{/if}"><img src="images/right-arrow-blue.png"/></a> 
</div>

<div id="colheaders">
<div style="width: 14.2857%; left: 0%;" class="chead"><a href="#">Sun</a></div>
<div style="width: 14.2857%; left: 14.2857%;" class="chead"><a href="#">Mon</a></div>
<div style="width: 14.2857%; left: 28.5714%;" class="chead"><a href="#">Tue</a></div>
<div style="width: 14.2857%; left: 42.8571%;" class="chead"><a href="#">Wed</a></div>
<div style="width: 14.2857%; left: 57.1429%;" class="chead"><a href="#">Thu</a></div>
<div style="width: 14.2857%; left: 71.4286%;" class="chead"><a href="#">Fri</a></div>
<div style="width: 14.2857%; left: 85.7143%;" class="chead"><a href="#">Sat</a></div>
</div> 

<div style="overflow: hidden; height: {$module->cal_height}; border:1px solid #cde" id="gridcontainer">
	<div id="calowner">
	<div id="grid" class="grid" style="height: 100%; cursor: default;">

		<div id="decowner">		

		{foreach from=$module->days key=key item=item}
		
		<div style="position: absolute; left: {$item.left}%; top: {$item.top}%; width: 14.2591%; height: 130px; z-index: 1;">
		<div class="dayOfMonth dayInMonth"><span>{$item.day}</span>&nbsp;</div>
		<br>
		{if ($item.currentDay)}
			<div class="month_today">
				<div style="padding:4px; font-size:10px;">
					<div class="month_event">{$item.events}</div>
				</div>
			</div>	
		{else}
			
			<div {if $item.Type neq 'current'} class="othermonth" {/if}>			
				<div style="padding:4px; font-size:10px;">
					<div class="month_event">{$item.events}</div>
				</div>	
			</div>
		{/if}	
			
		</div>
		{/foreach}
	
	</div>

	<div class="hrule hruleMonth" style="top: 20%; z-index: 1;"></div>
	<div class="hrule hruleMonth" style="top: 40%; z-index: 1;"></div>
	<div class="hrule hruleMonth" style="top: 60%; z-index: 1;"></div>
	<div class="hrule hruleMonth" style="top: 80%; z-index: 1;"></div>
	<div class="vrule nogutter" style="width: 1px; left: 0%; height: 100%; z-index: 1;"></div>
	<div class="vrule nogutter" style="width: 1px; left: 14.2857%; height: 100%; z-index: 1;"></div>
	<div class="vrule nogutter" style="width: 1px; left: 28.5714%; height: 100%; z-index: 1;"></div>
	<div class="vrule nogutter" style="width: 1px; left: 42.8571%; height: 100%; z-index: 1;"></div>
	<div class="vrule nogutter" style="width: 1px; left: 57.1429%; height: 100%; z-index: 1;"></div>
	<div class="vrule nogutter" style="width: 1px; left: 71.4286%; height: 100%; z-index: 1;"></div>
	<div class="vrule nogutter" style="width: 1px; left: 85.7143%; height: 100%; z-index: 1;"></div>
	</div>
	</div>
</div>

