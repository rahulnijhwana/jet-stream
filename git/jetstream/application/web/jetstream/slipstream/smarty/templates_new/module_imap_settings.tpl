<div id="wrap">
	<div class="module_container_overflow">
		{if $module->isEnabled}
			<div id="stage">
				<h3>Email Sync Accounts</h3>
				{if $module->settings}
					<table id="accounts">
						<tr class="header">
							<td>Name</td>
							<td>Type</td>
							<td>Username/Email</td>
							<td>Last Sync</td>
							<td class="center">Enabled</td>
						</tr>
						{foreach from=$module->settings item=settings}
							<tr>
								<td>
									<a id="{$settings.ImapUserSettingsID}:{$settings.ImapUsername}" class="show" href="#">{$settings.UserLabel}</a>
								</td>
								<td>
									{$settings.ServerLabel}
								</td>
								<td>
									<span >{$settings.ImapUsername}</span>
								</td>
								<td>
									{$settings.LastSync}
								</td>
								<td class="center">
									{if $settings.Enabled}
										<img src="images/checkbox_checked.png" alt="Enabled" />
									{else}
										<img src="images/checkbox_unchecked.png" alt="Disabled" />
									{/if}
								</td>
							</tr>
						{/foreach}
					</table>
					<span class="message">Click the account name to change your username or password.</span>
				{/if}
			</div>
		{/if}
	</div>
</div>
<div id="loginForm" style="display:none">
	<form id="imap_settings" method="post">
		<a href="#" id="close_form"><image class="close" src="images/img_close.png" alt="Cancel" /></a>
		<div style="clear: both"></div>
		<p><label>Username</label><input class="long" type="text" name="imap_username" id="imap_username" /></p> 
		<p><label>Password</label><input class="long" type="password" name="imap_pass" id="imap_pass" /></p>
		<div id="save_button_wrapper">
			<input type="button" id="save_button" value="Save" />
		</div>
		<div id="saving">
			<img src="images/spinner.gif" alt="saving" />
		</div>
		<input type="hidden" name="settings_id" id="settings_id" />
	</form>
</div> 
