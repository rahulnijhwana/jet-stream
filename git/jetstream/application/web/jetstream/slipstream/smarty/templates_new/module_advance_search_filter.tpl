<div class="module_container_overflow">
	<div>
		<form id="advanced_filter">
			<select name="template_advfilter" onchange="javascript:ChangeAdvFilter(this);" style="display:none;">
				<option value="">Choose which field to filter</option>
				<optgroup label='--- ASSIGN FIELDS ---'>
					<option value="Active_Id">Active User</option>
					<option value="Inactive_Id">Inactive User</option>
				<optgroup label='--- COMPANY FIELDS ---'>
				{foreach from=$module->filter_list.account item=section}
					<optgroup label='{$section.SectionName}'>
					{foreach from=$section.fields key=id item=field}
						<option value="account_{$id}">{$field}</option>
					{/foreach}
					</optgroup>
				{/foreach}
				</optgroup>
				<optgroup label='--- CONTACT FIELDS ---'>
				{foreach from=$module->filter_list.contact item=section}
					<optgroup label='{$section.SectionName}'>
					{foreach from=$section.fields key=id item=field}
						<option value="contact_{$id}">{$field}</option>
					{/foreach}
					</optgroup>
				{/foreach}
				</optgroup>
			</select>
			{foreach from=$module->presets key=field item=value}
			<div id="advfiltercont_{$field}" style="margin: 20px 0px;">
				<select name="advfilter_{$field}"><option value='{$field}'></option></select>
				<div id="advfilterinputcont_{$field}">
				{$module->CreateFilterEntry($field, $field, $value)}
				</div>
			</div>			
			{/foreach}
		</form>
	</div>
</div>

<script language="javascript">
	var filter_count = 0;
	{literal}

	$(document).ready(function() {
		CreateAdvFilter();
		UpdateAdvFilter();
	});
	
	function CreateAdvFilter() {
		$('#advanced_filter').append('<div id="advfiltercont_' + filter_count + '" style="margin:20px 0px"></div>');
		$("select[name='template_advfilter']").clone().attr("name", "advfilter_" + filter_count).prependTo('#advfiltercont_' + filter_count);
		filter_count++;
	}

	function ChangeAdvFilter(obj) {
		var id = obj.name.substring(10);
		var value = obj.value;
		// 1.  Clear out any existing input for the current field
		$('#advfilterinputcont_' + id).remove();

		// 1.5.  Get updated search results if input was in previous result
		// 2.  Get AJAX data input
		if (value != '') {
			$.post("ajax/ajax.adv_search_filter.php", {field:value, form_id:id}, function(data) {
				var input_div = '<div id="advfilterinputcont_' + id + '">' + data + '</div>';
				$('#advfiltercont_' + id).append(input_div);
			}, "html");
		}
				
		// 3.  Add a blank filter (if all filters are used)
		var blank_request_available = false;
		$("select[name^='advfilter_']").each(function() {
			if ($(this).val() == '') {
				blank_request_available = true;
				return false;
			}
		});
		if (!blank_request_available) {
			CreateAdvFilter();
		}

		// 4.  Update filter selects
		UpdateAdvFilter();
	}

	function ChangeSearchTerm() {
		// 3.  Get updated search results
	}
	
	function UpdateAdvFilter() {
		$("select[name^='advfilter_']").each(function() {
			select = $(this);
			var value = select.attr('value');
			var id = select.attr("name").substring(10);
			select.remove();
			$("select[name='template_advfilter']").clone().attr("name", "advfilter_" + id).prependTo('#advfiltercont_' + id).val(value).show();
		});
		
		var selects = $("select[name^='advfilter_']");
		selects.each(function() {
			outside_field = $(this).attr('name');
			outside_option = $(this).val();
			if (outside_option != '') {
				selects.each(function() {
					if ($(this).attr('name') != outside_field) {
						$(this).find("option[value='" + outside_option + "']").remove();
					}
				});
			}
		});
	}
	{/literal}
</script>
