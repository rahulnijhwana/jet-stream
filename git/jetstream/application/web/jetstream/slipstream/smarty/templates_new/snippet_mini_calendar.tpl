<div class="toolTipDiv" id="EventTypeLegend" style="display:none;">
	{$module->toolTip}
</div>

<div class="minical_title_container">
	<div class="minical_title">
	
		<a href="javascript:getMonth('prev'{if $module->target neq ''},{$module->target}{/if});"><img class="previousMonth" src="{$smarty.const.WEB_PATH}/images/left-arrow-blue.png" /></a>
        {if $module->eventMiniCalendar}
            <a style="margin:30px;" href="#">{$module->month} {$module->year}</a>
        {else}
            <a style="margin:30px;" href="{$smarty.const.WEB_PATH}/slipstream.php?action=monthly&day={$module->month_int}/1/{$module->year}{if $module->target neq ''}&target={$module->target}{/if}">{$module->month} {$module->year}</a>
        {/if}
		<a href="javascript:getMonth('next'{if $module->target neq ''},{$module->target}{/if});"><img class="nextMonth" src="{$smarty.const.WEB_PATH}/images/right-arrow-blue.png" /></a>
	</div>
</div>

<table class="minicalendar">
	{* Display Sunday to Saturday. *}
	<tr>
		<th>&nbsp;</th><th>Su</th><th>Mo</th><th>Tu</th><th>We</th><th>Th</th><th>Fr</th><th>Sa</th>
{assign var="count" value=7}
{* Loops through the calender dates. *}

{foreach from=$module->days name=day item=day key=key_outer}
	{if $count eq 7}
		{assign var="count" value=0}
	</tr>
	<tr>
		<th>			
			{if $module->eventMiniCalendar}
				W{$day.week}
			{else}
			<a href="slipstream.php?action=weekly&day={$day.date}{if $module->target}&target={$module->target}{/if}">
				W{$day.week}
			</a>
			{/if}
		</th>
	{/if}
		<td class="{if $day.type neq "current"}othermonth{/if} {if $day.today}today{/if}" {if !$module->eventMiniCalendar}onclick="location.href='{$smarty.const.WEB_PATH}/slipstream.php?action=daily&day={$day.date}'"{/if}>
			<div class="miniday">{$day.day}</div>			
			{foreach from=$day.events key=key item=event}				
				<div id="{$smarty.const.WEB_PATH}/ajax/ajax.EventTooltip.php?ref={$event.id_enc}" class="{if $event.pending}minipendingevent{else}minievent{/if} preview" style="background-color:#{$event.eventtype.EventColor};{if $event.is_edit_functional}cursor:pointer;{else}cursor:default;{/if}" {if $event.is_edit_functional && !$module->eventMiniCalendar}{if $event.RepeatParent}onclick='javascript:NoBubble(event);LoadRptngEvnt({$event.RepeatParent}, "{$event.id_enc}")'{else}onclick="javascript:NoBubble(event, '{$smarty.const.WEB_PATH}/slipstream.php?action=event&eventId={$event.EventID}')"{/if}{/if}></div>
			{/foreach}
		</td>
		{assign var="count" value=$count+1}		
{/foreach}
	</tr>
</table>
<script language="javascript">
{literal}
	$(document).ready(function() {
		ResetCalTooltips();
		$('#event_legend').cluetip({
			attribute: 'href',
			activation: 'click',
			width: 272,
			leftOffset: 180,
			sticky: true,
			closePosition: 'title',
			closeText: '<img src="images/img_close.png" style="width: 16px; height: 16px" alt="close" />',
			arrows: false
		});
	});
		var webpath = '{/literal}{$smarty.const.WEB_PATH}{literal}';
{/literal}
</script>
