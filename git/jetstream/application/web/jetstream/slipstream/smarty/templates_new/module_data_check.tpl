<form id="check" name="check" method="post">
	<div class="module_container_overflow" id="import">
		<div id="help">
			<h3 class="indentHeading">These duplicate {$module->type} records were found.</h3>
			<ul>
				<li>
					All of these {$module->type} records were found in the system already. 
				</li>
				<li>
					Please review the list below and select the records that you wish to retain.
				</li>
			</ul>
		</div>
		<div id="searchresults">
		<p style="display:inline-block; float: left; margin-left: 10px; font-weight: bold; font-style: italic;">Matches found: {$module->matchCount}</p>
			<div style="margin-left: 232px;">
				<!-- <input type="checkbox" name="sel_all" id="sel_all" value="true" label="Select All" onclick="selectAllCheckBox();" /> Select All -->
				<p class="useAll"><input type="radio" name="sel_all" id="sel_all" value="true" title="Use All New" onclick="selectAllRadio();" /> use all new</p>
				<p class="useAll"><input type="radio" name="sel_all" id="sel_all" value="false" title="Use All Current" onclick="selectAllRadio();" /> use all current</p>
			</div>
				{section name=record loop=$module->unique}
					<div style="clear:both; padding:2px;" class="{cycle values="alt_row, row"} id="div_{$module->unique[record].key}">
						<div class="match">Match found on input file row {$module->unique[record].idx+2}<p>{$module->unique[record].matches}<p></div>
						<div style="float:left; padding:0px; margin:3px;">
							<!-- <input type="checkbox" name="unique[]" value="{$module->unique[record].idx}:{$module->unique[record].key}"/> -->
							<div class="useNew"><input type="radio" class="new required" name="unique[{$module->unique[record].idx}]" id="{$module->unique[record].idx}:0" value="{$module->unique[record].idx}:0"  /><label for="{$module->unique[record].idx}:0"> use new </label></div>
							<div class="useCurrent"><input type="radio" class="current required" name="unique[{$module->unique[record].idx}]" id="{$module->unique[record].idx}:{$module->unique[record].key}" value="{$module->unique[record].idx}:{$module->unique[record].key}"  /><label for="{$module->unique[record].idx}:{$module->unique[record].key}">  use current </label></div>
						</div>
						<div style="margin-top: 14px;" class="toggle"><a href="javascript: toggleInfo('{$module->idLabel}{$module->unique[record].key}')"><img id="toggle_{$module->idLabel}{$module->unique[record].key}" src="images/plus.gif"/></a></div>
						<div style="overflow-x: hidden; margin-top: 8px;">
							<b class="{$module->titleClass}">{$module->type}: </b>&nbsp<div style="display:inline-block; width: 400px; color:#0000A0; padding:1px; font-weight:bold; overflow-y: hidden; margin-right:5px;">{$module->unique[record].0|trim|SplitLongWords}{if $module->isContact} {$module->unique[record].1|trim|SplitLongWords} ({$module->unique[record].CompanyName|trim|SplitLongWords}){/if}</div>
						</div>
						<div class="detail" id="{$module->idLabel}{$module->unique[record].key}"><img class="spinner" src="images/slipstream_spinner.gif" /></div>
					</div>
				{/section}
			<div id="buttons">Click next to continue...  <input type="submit" id="submitCheck" value="Next &gt;&gt;" />
			</div>
		</div>
	</div>

	{section name=hidden loop=$module->existing}
		<input type="hidden" name="all[]" value="{$module->existing[hidden].idx}:{$module->existing[hidden].key}"/>
	{/section}
	<input type="hidden" name="step" value="{$module->step}" />
	<input type="hidden" name="type" value="{$module->type}" />
	<input type="hidden" name="csv" value="{$module->csv}" />	

</form>
<script>
{literal}
$("#submitCheck").click(function(event){
	if($("#check").validate().form()){
		BusyOn();
	}
});
{/literal}
</script>