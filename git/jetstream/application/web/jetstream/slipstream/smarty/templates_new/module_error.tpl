<p style="font-size:1.3em;font-weight:bold;">
An error has occurred:  
<span style="color:#890B0C;">{$module->error}</span>
<span style="font-size:.7em;color:#890B0C;">({$module->error_detail})</span>
</p>
<p>{$module->error_text}</p>
<p>This issue has been logged and will be reviewed by support staff.</p>
<p>If this error persists, please contact Jetstream support at (773) 714-8100.</p>
