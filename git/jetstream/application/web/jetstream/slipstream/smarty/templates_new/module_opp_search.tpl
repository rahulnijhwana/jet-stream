<div id="dashboard">
	<form onsubmit="javascript:AjaxLoadPage(1);return false;" id="SlipstreamSearch" action="#" method="post">
		&nbsp; <input type="text" name="SearchString" id="SearchString" size="32" value="{if $module->search.search_string}{$module->search.search_string}{/if}" /> <input type="submit" name="SearchButton" value="Search" />
		Search in: <select onchange="javascript:AjaxLoadPage(1);" name="SearchType" id="SearchType">{html_options options=$module->search_types selected=$module->search.search_type}</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<br /><br />&nbsp; Show Opportunities less than <select onchange="javascript:AjaxLoadPage(1);"  name="daysRange" id="daysRange" >{html_options output=$module->age_range values=$module->age_range selected=$module->search.age_range}</select> old &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="checkbox" onclick="javascript:AjaxLoadPage(1);" value="1" name="includeActive" id="includeActive" {if (!$module->search.won_opp and !$module->search.lost_opp ) or $module->search.active_opp eq 1}checked{/if}>&nbsp; Include Active Opportunities  &nbsp; &nbsp; <input type="checkbox" onclick="javascript:AjaxLoadPage(1);" value="1" name="includeWon" id="includeWon" {if $module->search.won_opp}checked{/if}>&nbsp; Include Won Opportunities  &nbsp; &nbsp; <input type="checkbox" onclick="javascript:AjaxLoadPage(1);" value="1" name="includeLost" id="includeLost" {if $module->search.lost_opp}checked{/if}>&nbsp; Include Lost Opportunities 
		<input type="hidden" id="SortType" name="SortType" value="" />
		<input type="hidden" id="SortAs" name="SortAs" value="" />
		<br><br>
		<div style="border: solid 1px #CCCCCC;">
			<div class="section_head" onclick="javascript:ToggleHidden('PipelineReports');" > 
				<img id="toggle_PipelineReports" src="images/plus.gif"/> Pipeline Reports
			</div>
			<div id="PipelineReports" style="display:none; margin: 10px 0px 10px 20px; ">
				 Active Opportunity Values by : 
				 <select onChange="if(this.value != '') CreateReport(this.value);">
					<option value="" ></option> 
					<option value="1">Customer (/Sales Cycle Stage)</option> 
					<option value="2">Customer (/Month)</option> 
					<option value="3">Customer / Detail (/Month)</option> 
					<option value="4">Product (/Month)</option> 
				 </select>
				 &nbsp; &nbsp; &nbsp;
				 Forecasted Revenues by : 
				 <select onChange="if(this.value != '') CreateReport(this.value)">
					<option value="" selected></option> 
					<option value="5">Customer (/Sales Cycle Stage)</option> 
					<option value="6">Customer (/Month)</option> 
					<option value="7">Customer / Detail (/Month)</option> 
					<option value="8">Product (/Month)</option> 
				 </select>	
			</div>
			
			<div class="section_head" onclick="javascript:ToggleHidden('ClosedReports');"> 
				<img id="toggle_ClosedReports" src="images/plus.gif"/> Closed Reports
			</div>
			<div id="ClosedReports" style="display:none; margin: 10px 0px 10px 20px; ">
				 Closed Opportunity Values by : 
				 <select onChange="if(this.value != '') CreateReport(this.value)">
					<option value="" selected></option> 
					<option value="9">Customer (/Month)</option> 
					<option value="10">Customer / Detail (/Month)</option> 
					<option value="11">Product (/Month)</option> 
				 </select>
			</div>
		</div>
		<p id="opp_tracker" style="display: none">OppTracker</p>
	</form>	
	<div id="dashboardsearch">
	{include file="snippet_opp_search_list.tpl"}
	</div>	
	
</div>