<div id='dashboard'>
<form onsubmit="javascript:AjaxLoadPage(1);return false;" id="SlipstreamSearch" action="#" method="post" name="SlipstreamSearch">

	{* OPTIONS *}
	<div class="advance_section_head" onclick="javascript:ToggleHidden('division_0');"> 
		<img id="toggle_division_0" src="images/minus.gif"/> Options
	</div>
	<div id="division_0" class="advance_datagrid_b">
		<table>
			<tr>
				<td>
					Show <select onchange="javascript:AjaxLoadPage(1);"  name="RecordsPerPage" id="RecordsPerPage" >
						{html_options output=$module->records_per_page values=$module->records_per_page selected=$module->search.records_per_page }
					</select> records /page. &nbsp;
					{if $module->limitAccess == 0 }
						Assigned to: 
						<select name="assignto">
						<option></option>
						<option value="Unassign" {if $module->assignto == "Unassign"}selected{/if}>Not Assigned</option>
						<optgroup label="Active">
						{$module->createAssignList('Active')}
						<optgroup label="Inactive">
						{$module->createAssignList('Inactive')}
						</select>
					{else}
						Assigned to: 
						<select name="assignto">
						<option></option>
						<option value="Unassign" {if $module->assignto == "Unassign"}selected{/if}>Not Assigned</option>
						<optgroup label="Active">
						{$module->createAssignList('Active', $module->user_id)}
						<optgroup label="Inactive">
						{$module->createAssignList('Inactive', $module->user_id)}
						</select>
					{/if}
				&nbsp;
				Search in: 
				<select onchange="javascript:HideSearchType(){*AjaxLoadPage(1)*};" name="SearchType" id="SearchType">
					{html_options options=$module->search_types selected=$module->query}
				</select>
				&nbsp;
				Creation Date:
				{$module->PrintForm($module->created , 'created', $module->created.value)}
			</td></tr>
			<tr><td>
				<input type="checkbox" id="selectInactive" name="selectInactive" value="1" onclick="javascript:AjaxLoadPage(1);" {if $module->search.inactive_type}checked{/if} /> 
				Include Inactive 
				{if $module->limitAccess == 1}
					<input type="hidden" name="AssignedSearch" id="AssignedSearch" value="1"> 
				{else}
					<input type="checkbox" name="AssignedSearch" id="AssignedSearch" value="1" onchange="javascript:AjaxLoadPage(1);" {if $module->search.assigned_search}checked{/if} />
					Only search records assigned to me	
				{/if}&nbsp;
				{if $smarty.session.USER.LEVEL neq 1}
					<input type="checkbox" id="chkMergeRecord" onclick="javascript:toggleMergeButton(this.checked)"/> Merge
				{else}&nbsp;
				{/if}
			</td></tr>
		</table>
		<input type="hidden" name="Advance" value="1" >
		<input type="hidden" name="Saved_Id" value="{$module->saveSearchID|default:0}" id="Saved_Id">
		<input type="hidden" name="Saved_Name" value="{$module->saveSearchName}" id="Saved_Name">
	</div>
	{* /OPTIONS *}

	{assign var=lcv value=1 }

	{foreach from=$module->layouts key=division item=layout}
	<div class="advance_section_head" onclick="javascript:ToggleHidden('division_{$division}');" id="bar_{$division}">
		<img id="toggle_division_{$division}" src="images/minus.gif"/> {$division|capitalize}
	</div>

	<div id="division_{$division}">
		{foreach from=$layout key=section item=row}
		<div class="advance_section_head_b"  onclick="javascript:ToggleHidden('sect{$lcv}');">
			<img id="toggle_sect{$lcv}" src="images/{$module->Display_Div($division,$section, 1)}"/>  
			{if $section == 'DefaultSection'}
				Default Section
			{else }
				{$section}
			{/if}
			
		</div>

		<div id="sect{$lcv}" class="advance_datagrid hidden" style="display:{$module->Display_Div($division,$section, 0)};">
	
			<table>
				{foreach from=$row key=column item=values}
				<tr>
					<td class="label">{$values.1.labelName|default:"&nbsp;"}</td>
					<td class="left">{$module->PrintForm($values.1 , $division, $values.1.value)|default:"&nbsp;"}</td>
					<td class="label">{$values.0.labelName|default:"&nbsp;"}</td>
					<td class="right">{$module->PrintForm($values.0 , $division, $values.0.value)|default:"&nbsp;"}</td>
				</tr>
				{/foreach}
			</table>
		</div>
		{assign var=lcv value=$lcv+1 }
		{/foreach}
	</div>
	{/foreach}

	<div class="clear" style="width:100%;margin:10px;" > 
		<div style="float:left;">
			<a class="button" href="#" onclick="javascript:this.blur();AjaxLoadPage(1);ShowHide('Expt', 'show');return false;"><span> Search </span></a>
			<a class="button" href="#" onclick="javascript:this.blur();SaveSlipstreamSearchPrompt()"><span> Save </span></a>
			<a class="button" href="#" onclick="javascript:this.blur();AdvanceSearchReset();ShowHide('Expt', 'hide')"><span> Clear </span></a>
		</div>
		{if $module->allowExport == 1}
			<div style="float:right;" style="display:{if $module->search_results.0}block{else}none{/if};" id="Expt">
				<a class="button" id="btnMerge" href="#" onclick="javascript:this.blur();doMerge()" style="visibility:hidden;"><span> Merge </span></a>
				<a class="button" href="#" onclick="javascript:ExportForm()" ><span> Export Selected Fields</span></a>
				<a class="button" href="#" onclick="javascript:SaveGroupField('export', 'all', 'all', 'all', 'all')" ><span> Export All Fields</span></a>
			</div>
		{/if}
	</div>

	<div id="dashboardsearch">{include file="snippet_dashboard_search_list.tpl"}</div>
	</form>
</div>
