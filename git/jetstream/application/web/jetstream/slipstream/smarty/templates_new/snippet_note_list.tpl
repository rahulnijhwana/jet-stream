<div id="pager">
	{$module->notesPerPageSelect}
	<span id="loadingDiv" style="float: left; margin-right: 5px; display: none;">
		<img width="20" height="20" src="./images/slipstream_spinner.gif" alt="Loading...." />
	</span>
	<span style="float:right;" id="spanPageLinkTop">
	  {$module->page_links}
	</span>
	<span style="float:right;" id="pageInfo">{$module->page_info}</span>
</div>
<div class="clearing"></div>
{section name=note loop=$module->note_list}
	<div class="{cycle values="note, note_alt"}">
		<div style="display: block; float: right;">
			<table>
			{if $module->note_list[note].Creator|trim neq ""}
				<tr>
					<td class="right_sub">
						User
					</td>
					<td>
						{$module->note_list[note].Creator}
					</td>
				</tr> 
			{/if}
			{if $module->note_list[note].Type neq ""}
				<tr>
					<td class="right_sub">Type</td>
					<td>
						{if $module->note_list[note].Type eq 'Email' && $module->note_list[note].EmailContentID > 0}
							<a href="ajax/ajax.getEmailByID.php?email={$module->note_list[note].EmailContentID}" target="blank">Email</a>
						{else}
							{$module->note_list[note].Type}
						{/if}
					</td>
				</tr>
			{/if}		
			{if isset($module->note_list[note].Private)}
				{if $module->note_list[note].Private eq "1"}
					<tr>
						<td class="right_sub">
							Private
						</td>
						<td>
							<img src="images/checkbox_checked.png" />
						</td>
					</tr>
				{/if}
			{/if}		
			</table>
		</div>
		{if $module->note_list[note].HasAlert}
			<div class="alert">
				<img src="images/alert.png" />
			</div>
		{/if}
		<h3 class="subject{if $module->note_list[note].HasAlert} alert_subject{/if}">{$module->note_list[note].Subject}</h3>
		<div class="clear_left"></div>
		<p><b>Date</b> {$module->note_list[note].TimeStamp}</p>


		{if $module->note_list[note].Company}
			<p><b>Company</b> <a href="?action=company&accountId={$module->note_list[note].AccountID}">{$module->note_list[note].Company}</a></p>
		{/if}
		{if $module->note_list[note].ContactID neq ""}
			<p><b>Contact</b> <a href="?action=contact&contactId={$module->note_list[note].ContactID}">{$module->note_list[note].Contact}</a></p>
		{/if}
		{if $module->note_list[note].Opp neq ""}
			<p><b>Opportunity</b> {$module->note_list[note].Opp}</p>
		{/if}
		
		{if $module->note_list[note].EmailContentID > 0}
		
		<p><b>From</b> <a href="mailto:{$module->note_list[note].FromEmail}">{$module->note_list[note].From}</a><img src="images/email.gif" style="margin-left: 5px;"/></p>
		<p><b>To</b> {$module->note_list[note].To}</p>
		{if $module->note_list[note].Cc|trim neq ''}
			<p><b>Cc</b> {$module->note_list[note].Cc}</p> 
		{/if}
		
		<div>
		{if $module->note_list[note].Attachment}
			<div class="attachment_wrapper">
				<div class="attachment_img">
					<img src="images/attachment.png" width="36" height="36" alt="Attachments" />
				</div>
				<div class="attachments">
					<span class="attachment_links">{$module->note_list[note].Attachment}</span>
				</div>
			</div>
		{/if}
		</div>
		<div class="clearing"></div>		
		<div style="border: 1px solid #cacaca; padding:0;">
			<iframe src="ajax/ajax.getEmailByID.php?email={$module->note_list[note].EmailContentID}" style="width:100%;" frameborder="0" /></iframe>
		</div>
		{else}
		<div class="clearing"></div>		
		<div>
			<p class="body">{$module->note_list[note].Body}</p>
		</div>
		{/if}
	</div>
{/section}
{if $module->no_records}{$module->no_records}{/if}
<span style="float:right;" id="spanPageLinkBottom">
  {$module->page_links}
</span>
<div class="clearing"></div>
<input type="hidden" name="totRec" id="totRec" value="{$module->tot_rec}">

<script language="javascript" type="text/javascript">
{literal}
// Hide and show forces a redraw to fix display issues in IE6
$('iframe').load(function(){
	$('iframe').hide();
	$('iframe').show();

});

$(document).ready(function() {
	$('.body').each(function(i) {
		obj = $(this);
		if (obj.height() > 200) {
			obj.css('height', '200px');
			obj.css('overflow', 'auto');
		}
	});

});

{/literal}
</script>

