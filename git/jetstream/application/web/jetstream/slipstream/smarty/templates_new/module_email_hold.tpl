
<div id="hold" class="BlackBold">
	{if $module->email_hold}
		{foreach from=$module->email_hold key=key item=email}
			<p id="email_hold_{$key}"> 
				<span class="display_name">{$email.DisplayName}{if $email.DisplayName neq $email.EmailAddress}</span>
				<span class="email"> ({$email.EmailAddress})</span>{/if}<br>
				<span style="font-size:.8em">
				<a href="javascript:CreateNewContact('CompanyContactNew&action=add&type=contact&form=true&referer=email_hold&email={$email.EmailAddress}&last_name={$email.LastName}&first_name={$email.FirstName}','','edit', '{$key}');">New Contact</a> | 
				<a href="javascript:ContactHintForm('{$email.FirstName}', '{$email.LastName}', '{$email.EmailAddress}', '{$key}');">Assign to Contact</a> | 
				<a href="javascript:IgnoreEmailAddress('email_hold_{$key}', '{$email.EmailAddress}');">Ignore</a> | 
				<a href="javascript:DisplayEmails('{$email.FirstName}', '{$email.LastName}', '{$email.EmailAddress}', '{$key}');">View Email</a>			
				</span>
			</p>
		{/foreach}
		<div id="contact_hint" style="display:none"></div>
		
	{else}
		<div class="BlackBold">
			None found.
		</div>
	{/if}	
</div>
