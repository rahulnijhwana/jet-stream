
<div id="pager">
	{$module->notesPerPageSelect}
	<span id="loadingDiv" style="float: left; margin-right: 5px; display: none;">
		<img width="20" height="20" src="./images/slipstream_spinner.gif" alt="Loading...." />
	</span>
	<span style="float:right;" id="spanPageLinkTop">
	  {$module->page_links}
	</span>
	<span style="float:right;" id="pageInfo">{$module->page_info}</span>
</div>
<div class="clearing"></div>

<div id="hold" class="BlackBold">
	{if $module->email_incoming}
		{foreach from=$module->email_incoming key=key item=email}
			<div class="email_incoming_{$key}">
				<div class="{cycle values="note, note_alt"}"{if $email.Pending} style="border-color:red"{/if}>
					<h3 class="subject{if $email.HasAlert} alert_subject{/if}">{$email.Subject}</h3>
					<div class="clear_left"></div>
					{if $email.EmailContentID > 0}
						<p><b>Date</b> {$email.Date}</p>
						<p><b>From</b> <a href="mailto:{$email.FromEmail}">{$email.FromName}</a><img src="images/email.gif" style="margin-left: 5px;"/></p>
						<p><b>To</b> {$email.RecipientNames}</p>
						{if $email.CcNames|trim neq ''}
							<p><b>Cc</b> {$email.CcNames}</p> 
						{/if}
						<div>
						{if $email.Attachment}
							<div class="attachment_wrapper">
								<div class="attachment_img">
									<img src="images/attachment.png" width="36" height="36" alt="Attachments" />
								</div>
								<div class="attachments">
									<span class="attachment_links">{$email.Attachment}</span>
								</div>
							</div>
						{/if}
						<div class="clearing"></div>
						</div>
						
						<div style="border: 1px solid #cacaca; padding:0;">
							<iframe src="ajax/ajax.getEmailByID.php?email={$email.EmailContentID}" style="width:100%;" frameborder="0" /></iframe>
						</div>
						<div style="float:left;">
							<a class="button" href="#" onclick="javascript:MarkNotePrivate('email_incoming_{$key}', {$email.EmailContentID}, false);"><span> Mark Public </span></a>
							<a class="button" href="#" onclick="javascript:MarkNotePrivate('email_incoming_{$key}', {$email.EmailContentID}, true);"><span> Mark Private </span></a>
						</div><br>
<!--						<span style="font-size:.9em">
							<a href="javascript:MarkNotePrivate('email_incoming_{$key}', {$email.NoteID}, false);">Mark Public</a> | 
							<a href="javascript:MarkNotePrivate('email_incoming_{$key}', {$email.NoteID}, true);">Mark Private</a>
						</span>
-->	
					{else}
						<div class="clearing"></div>		
						<div>
							<p class="body">{$email.Body}</p>
						</div>
					{/if}
				</div>
			</div>			
		{/foreach}

<!--		{foreach from=$module->email_incoming key=key item=email}
			<p id="email_incoming_{$key}"> 
				From: <span class="display_name">{$email.FromName}
				{if $email.FromName neq $email.FromEmail}</span><span class="email"> ({$email.FromEmail})</span>{/if}
				<br>
				To: <span class="display_name">{$email.RecipientNames}{if $email.RecipientNames neq $email.RecipientEmails}</span>
				<span class="email"> ({$email.RecipientEmails})</span>{/if}
				<br>
				Subject: <span class="display_name">{$email.Subject}</span>
				<br>
				<span style="font-size:.8em">
				<a href="javascript:MarkNotePrivate('{$email.NoteID}', 'False');">Mark Public</a> | 
				<a href="javascript:MarkNotePrivate('{$email.NoteID}', 'True');">Mark Private</a> |

				<a href="javascript:CreateNewContact('CompanyContactNew&action=add&type=contact&form=true&referer=email_incoming&email={$email.EmailAddress}&last_name={$email.LastName}&first_name={$email.FirstName}','','edit', '{$key}');">New Contact</a> | 
				<a href="javascript:ContactHintForm('{$email.FirstName}', '{$email.LastName}', '{$email.EmailAddress}', '{$key}');">Assign to Contact</a> | 
				<a href="javascript:IgnoreEmailAddress('email_hold_{$key}', '{$email.EmailAddress}');">Ignore</a> | 
				<a href="javascript:DisplayEmails('{$email.FirstName}', '{$email.LastName}', '{$email.EmailAddress}', '{$key}');">View Email</a>			
				</span>
			</p>
		{/foreach}
-->
		<div id="contact_hint" style="display: none"></div>
	{else}
		<div class="BlackBold">
			None found.
		</div>
	{/if}	
</div>
