<div id="divEditor">
	<form method="post" name="frmCalendar">
		Check the boxes next to the users you wish to grant access to your calendar.
		
		<ul>
		<li>Read grants only the ability to view your calendar.</li>
		<li>Write grants the ability to add, change, or remove appointments on your calendar.</li>
		</ul>
				
		<table class="event">
			<tr>
				<th style="width:200px;">Users</th>
				<th class="centered" style="width:50px;">Read</th>
				<th class="centered" style="width:50px;">Write</th>
				<th></th>
			</tr>
		{foreach from=$module->user_details key=user_id item=user}
			<tr>
				<td>{$user.FirstName} {$user.LastName}</td>
				<td class="centered"><input type="checkbox" person_id="{$user.PersonID}" class="read" {if $module->cal_perms[$user.PersonID].ReadPermission}checked{/if} {if $module->cal_perms[$user.PersonID].LockPermission}disabled{/if}/></td>
				<td class="centered"><input type="checkbox" person_id="{$user.PersonID}" class="write" {if $module->cal_perms[$user.PersonID].WritePermission}checked{/if} {if $module->cal_perms[$user.PersonID].LockPermission}disabled{/if}/></td>
				<td style="border:0px;">{if $module->cal_perms[$user.PersonID].LockPermission}<span style="font-weight:bold;font-size:10px;">Locked</span>{/if}</td>
			</tr>
		{/foreach}
		</table>
		<br/>
		<br/>
		<input type="hidden" id="testHidden" value="" />
		<input type="button" id="btnSaveEditor" value="Save" onclick="javascript:saveCalendarPermission('{$module->user_id}');"/>
	</form>
</div>