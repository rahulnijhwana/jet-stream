<div style="text-align:center;margin-bottom:10px;">
	<a href="javascript:GetCalendar('monthly', '{$module->previous}', '{$module->selected_sales_person}')"><img src="images/left-arrow-blue.png"/></a><span class="week_cal_header_text">{$module->display_date}</span><a href="javascript:GetCalendar('monthly', '{$module->next}', '{$module->selected_sales_person}',1)"><img src="images/right-arrow-blue.png"/></a> 
</div>

<table class="monthly_calendar">
	{* Display Sunday to Saturday. *}
	<tr>
		<th>&nbsp;</th><th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th>
{* Assign 0 to count variable. *}		
{assign var="count" value=7}
{* Loops through the calender dates. *}
{foreach from=$module->days name=day item=day key=key_outer}
	{if $count eq 7}
	</tr>
	<tr>
		<th>
			<a href="javascript:GetCalendar('weekly', '{$day.date}', '{$module->selected_sales_person}',1)">W{$day.week}</a>			
		</th>
			{assign var="count" value=0}
	{/if}
		<td class="{if $day.type neq "current"}othermonth{/if} {if $day.today}today{/if}" onclick="javascript:ShowAjaxForm('EventNew&contactid=0&action=add&StartDate={$day.date}&EndDate={$day.date}&BlankTime=True');window.scrollTo(0,0);">
			<div class="monthly_day">{$day.day}</div>
			{if $day.tasks|@count gt 0}
			<div id="a_{$key_outer}" class="monthly_event_container">
				<div title="Tasks for {$day.date_string}" id="ajax/ajax.GetTask.php?date={$day.date}" class="monthly_event task_preview" style="border:2px solid #CCDDEE;background-color:#FFF;text-align:center;">
					<div style="padding:2px">
					{if $day.today}
						{$day.closed_task_count}/{$day.tasks|@count} Task{if $day.tasks|@count gt 1}s{/if}
					{elseif $day.date|@strtotime le $smarty.now}	
						{$day.tasks|@count} Task{if $day.tasks|@count gt 1}s{/if} Comp
					{else}
						{$day.tasks|@count} Task{if $day.tasks|@count gt 1}s{/if}
					{/if}
					</div>
				</div>
				&nbsp;
			</div>
			<div class="task_list" id="list_{$key_outer}" style="display:none;margin-top:-2px;">
			</div>
			{/if}
			{foreach from=$day.events key=key item=event}
			<div class="{if $event.pending}monthly_event_container_pending{else}monthly_event_container{/if}">
				<div id="ajax/ajax.EventTooltip.php?ref={$event.id_enc}" class="monthly_event preview {if $event.pending}pendingevent{elseif $event.Closed eq 1}closedevent{/if}" style="background-color:#{$event.eventtype.EventColor};border-color:{$event.user.Color};{if $event.PrivateEvent}cursor:default;{/if}" {if $event.is_viewable}{if $event.RepeatParent}onclick='javascript:NoBubble(event);LoadRptngEvnt({$event.RepeatParent}, "{$event.id_enc}")'{else}onclick="javascript:NoBubble(event, 'slipstream.php?action=event&eventId={$event.EventID}');"{/if}{/if}>
					<div class="monthly_lf_color" style="background-color:{$event.user.Color};">&nbsp;</div>
					<div style="padding:2px;">
						<nobr>
							{$event.start_time_string}
							{if $event.is_viewable}
								{$event.eventtype.EventName}							
							{else}
								Personal
							{/if}						
						</nobr>
					</div>
				</div>
			</div>
			
			&nbsp;
			{/foreach}
		</td>
		{assign var="count" value=$count+1}		
{/foreach}
	</tr>
</table>
<script>
	{literal}
	$(document).ready(function() {
		ResetCalTooltips();
		$('#week_cal').attr('scrollTop', 332);
	});
	{/literal}
</script>
