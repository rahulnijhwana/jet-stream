<div class="jet_datagrid" align="center">
<table>
	{if $module->event_info.AccountID }
	<tr>
		<td class="label3">Company</td>
		<td class="data {if $module->event_info.Closed eq 1}closedevent{/if}">
			<!-- toggle later, besso
			<div style="padding:1px 10px 0 0px; float:left;">
				<a href="javascript: ToggleContactInfo()"><img id="toggle_contact" src="images/plus.gif"/></a>
			</div>
			-->
			<a href="slipstream.php?action=company&accountId={$module->event_info.AccountID}">{$module->event_info.AccountName}</a>
		</td>
	</tr>
	<tr id="event_contact_cells" style="display:none;">
		<td colspan="2" style="background: #DDD; padding: 0px 10px 10px 10px;">

			<div id="event_contact_info" style="margin-top: 10px; background-color: #F8F8F8;">
				<div class="loading" style="border:0px;"><img class="spinner" src="images/slipstream_spinner.gif" /></div>
				<script type="text/javascript">
					{literal}
					
					var contact_loaded = false;
					
					function ToggleContactInfo() {
						if($('#event_contact_cells').css('display') == 'none' || $('#event_contact_cells').css('display') == '') {
							$('#event_contact_cells').show();
							$('#toggle_contact').attr('src', 'images/minus.gif');
							if (!contact_loaded) {
								contact_loaded = true;
								LoadContact();
							}
						} else {
							$('#event_contact_cells').hide();
							$('#toggle_contact').attr('src', 'images/plus.gif');
						}
					}		
					
					function LoadContact() {
						$.ajax({
							url: "ajax/ajax.searchdetail.php",
							data: ("request_id=account_{/literal}{$module->event_info.AccountID}{literal}"),
							type: "POST",
							dataType: "html",
							success: function(data){
								$("#event_contact_info").html(data);
							}
						});
					}
					{/literal}
				</script>
			</div>

		</td>
	</tr>
	{else}
	<tr>
		<td class="label3">Contact</td>
		<td class="data {if $module->event_info.Closed eq 1}closedevent{/if}">

			{if !$module->event_info.Contact.noLink }
			<div style="padding:1px 10px 0 0px; float:left;">
				<a href="javascript: ToggleContactInfo()"><img id="toggle_contact" src="images/plus.gif"/></a>
			</div>
			{/if}

			{if !$module->event_info.Contact.noLink }
			<a style="float:left;" href="slipstream.php?action=contact&contactId={$module->event_info.ContactID}">{$module->event_info.Contact.FirstName} {$module->event_info.Contact.LastName}</a>
			{else}
                {if $smarty.const.GOOGLE_EVENT_TYPE_IN_JETSTREAM == $module->event_info.EventTypeID}  
                    Please choose valid Contact
                {elseif $smarty.const.GOOGLE_TASK_TYPE_IN_JETSTREAM == $module->event_info.EventTypeID}
                    Please choose valid Contact or Company
                {else}
			        {$module->event_info.Contact.FirstName} {$module->event_info.Contact.LastName}
                {/if}
			{/if}

			&nbsp;{if $module->event_info.Contact.AccountID}(<a href="slipstream.php?action=company&accountId={$module->event_info.Contact.AccountID}">{$module->event_info.Contact.CompanyName}</a>){/if}
		</td>
	</tr>
	<tr id="event_contact_cells" style="display:none;">
		<td colspan="2" style="background: #DDD; padding: 0px 10px 10px 10px;">

			<div id="event_contact_info" style="margin-top: 10px; background-color: #F8F8F8;">
				<div class="loading" style="border:0px;"><img class="spinner" src="images/slipstream_spinner.gif" /></div>
				<script type="text/javascript">
					{literal}
					
					var contact_loaded = false;
					
					function ToggleContactInfo() {
						if($('#event_contact_cells').css('display') == 'none' || $('#event_contact_cells').css('display') == '') {
							$('#event_contact_cells').show();
							$('#toggle_contact').attr('src', 'images/minus.gif');
							if (!contact_loaded) {
								contact_loaded = true;
								LoadContact();
							}
						} else {
							$('#event_contact_cells').hide();
							$('#toggle_contact').attr('src', 'images/plus.gif');
						}
					}		
					
					function LoadContact() {
						$.ajax({
							url: "ajax/ajax.searchdetail.php",
							data: ("request_id=contact_{/literal}{$module->event_info.ContactID}{literal}"),
							type: "POST",
							dataType: "html",
							success: function(data){
								$("#event_contact_info").html(data);
							}
						});
					}
					{/literal}
				</script>
			</div>

		</td>
	</tr>
	{/if}
	<tr>
		<td class="label3">When</td>
		<td class="left {if $module->event_info.Closed eq 1}closedevent{/if}" colspan="3">
			{if $module->event_info.EventType.Type == 'TASK'}
				<span style="font-size: 1.2em; font-weight: bold; display: block; margin: 4px 0px 4px 0px; {if $module->event_info.pending} color: red;{/if}">{$module->event_str}</span>
			{else}
			{if $module->event_info.RepeatType eq 0}
				<!-- <span style="font-size:1.2em;font-weight:bold;">{$module->event_info.StartTimeStamp|FormatDate} to {$module->event_info.EndTimeStamp|FormatDate} ({$module->event_info.Duration|FormatDuration})</span> -->
				<span style="font-size: 1.2em; font-weight: bold; display: block; margin: 4px 0px 4px 0px; {if $module->event_info.pending} color: red;{/if}">{$module->event_str}</span>
			{if $module->event_info.RepeatParent}
				<p class="repeat_label">This event is part of a <a href="slipstream.php?action=event&eventId={$module->event_info.RepeatParent}">repeating series</a></p>
			{/if}
			{else}
				{$module->GetRepeatReadable()}
			{/if}
			{/if}
		</td>
	</tr>
	<tr>
		<td class="label3">Attendees</td>
		<td class="left {if $module->event_info.Closed eq 1}closedevent{/if}" {if $module->event_info.DealID gt 0}colspan="3"{/if}>
			{foreach from=$module->attendees item=attendee name=people}
				{if !$smarty.foreach.people.first}<br>{/if}
				{$attendee.name}
			{/foreach}
		</td>
	</tr>
	<tr>
		<td class="label3">Type</td>
		<td {if $module->event_info.Closed eq 1}class="closedevent"{/if}>
            {if $smarty.const.GOOGLE_EVENT_TYPE_IN_JETSTREAM == $module->event_info.EventTypeID}
                Please choose valid Event Type
            {elseif $smarty.const.GOOGLE_TASK_TYPE_IN_JETSTREAM == $module->event_info.EventTypeID}
                Please choose valid Task Type
            {else}
			    {$module->event_info.EventType.EventName} ({$module->event_info.EventType.Type|lower|capitalize})
            {/if}
		</td>
	</tr>
	<tr>
		<td class="label3">Subject</td>
		<td {if $module->event_info.Closed eq 1}class="closedevent"{/if}>
			{$module->event_info.Subject}
		</td>
	</tr>
	{if $module->event_info.Location}
	<tr>
		<td class="label3">Location</td>
		<td {if $module->event_info.Closed eq 1}class="closedevent"{/if}>
			{$module->event_info.Location}
		</td>
	</tr>
	{/if}
	{if $module->event_info.PriorityID}
	<tr>
		<td class="label3">Priority</td>
		<td {if $module->event_info.Closed eq 1}class="closedevent"{/if}>
			{$module->event_info.PriorityLabel}
		</td>
	</tr>
	{/if}
	{if !$module->event_info.DealID}
		{if $module->event_info.Private eq "1"}

			<tr>
				<td class="label3">Private</td>
				<td class="right">			
					<img src="images/checkbox_checked.png">
				</td>
			</tr>
		{/if}
	{/if}
</table>
</div>
{include file="form_close_event.tpl"}

<script language="javascript" type="text/javascript">
	{literal}	
	function openOppPopup(dealId, personId) {
		var win = Windowing.openSizedWindow('legacy/shared/edit_opp2.php?SN={/literal}{$smarty.cookies.SN}{literal}&detailview=1&reqOpId=' + dealId + '&reqPersonId=' + personId + '#' + dealId + '_' + personId, 630, 1000);		
	}
	{/literal}
</script>	

