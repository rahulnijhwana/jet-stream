{*
<pre>
{$module->event_info|@print_r:TRUE}
</pre>
*}
<!--div id="eventMiniCalendar" style="float:right;border:0px solid black; padding:5px; width:282px;overflow:auto">
	<div class="loading"><img class="spinner" src="{$smarty.const.WEB_PATH}/images/slipstream_spinner.gif" \></div>
</div-->

<input type="hidden" name="EventID" value="{$module->event_info.EventID}">
<input type="hidden" name="ContactID" value="{$module->event_info.ContactID}">
<input type="hidden" name="DealID" value="{$module->event_info.DealID}">
{if !$module->event_info.EventID && $module->event_info.RepeatParent}
<input type="hidden" name="RepeatParent" value="{$module->event_info.RepeatParent}">
<input type="hidden" name="OrigStartDate" value="{$module->event_info.StartTimeStamp|FormatDate:'trunc'}">
{/if}
<div id="contactForm" style="width:345px;float:left;">
	<div class="acc_con_form">
	<table>
		<tr>
			<td><span class="acc_con_label">Opportunity Name</span> <span class="form_required">*</span><br />
				<input type="text" name="Subject" size="40" class="clsTextBox" value="{$module->event_info.Subject}" />
				<div class="form_error" id="err_Subject"></div>
			</td>
		</tr>
		
		<tr>
			<td><span class="acc_con_label">Contact</span><span class="form_required">*</span><br />			
				<input type="text" name="Contact" size="40" value="{$module->event_info.Contact.FirstName} {$module->event_info.Contact.LastName}" onkeyup="javascript:ClearContact();" onchange="javascript:ClearContact();" />
				<div class="form_error" id="err_Contact"></div>
			</td>
		</tr>
		
		<tr>		
			<td><span class="acc_con_label">Company</span> <span class="form_required">*</span><br />
				<input type="text" name="Contact" size="40" value="{if $module->event_info.Contact.CompanyName}{$module->event_info.Contact.CompanyName}{/if}" onkeyup="javascript:ClearContact();" onchange="javascript:ClearContact();" />
				<div class="form_error" id="err_Contact"></div>
			</td>
		</tr>
		
		{if permitted_people}
		<tr>
			<td>
				<span class="acc_con_label">Salesperson</span><br />
				<select name="creator" onChange="RemoveAttendee(this.value);UpdateAttendeeSelect();">
				{foreach from=$module->permitted_people key=pp_id item=pp_name}
					
					<option value="{$pp_id}" {if $pp_id eq $module->creator}selected{/if}>{$pp_name}</option>
				{/foreach}
				</select>
			</td>
		</tr>
		{/if}		
						
		<tr>
			<td>
				<span class="acc_con_label">Opportunity Creation Date</span> <span class="form_required">*</span><br />
 				<input type="text" name="StartDate" class="datepicker" size="6" value="{if $module->event_info.StartTimeStamp}{$module->event_info.StartTimeStamp|FormatDate:'date'}{/if}" />
				<div class="form_error" id="err_When"></div>
			</td>
			<td>
				<span class="acc_con_label">Exp. Close Date</span><br />				
				<input type="text" name="EndDate" class="datepicker" size="6" value="{if $module->event_info.EndTimeStamp}{$module->event_info.EndTimeStamp|FormatDate:'date'}{/if}" />
				</span>
				<div class="form_error" id="err_When"></div>
			</td>
		</tr>
		
		<tr>
			<td>
				<span class="acc_con_label">Products</span><br />
				<select name="creator" onChange="RemoveAttendee(this.value);UpdateAttendeeSelect();">
				<option>Select a product</option>
				<option>Product 1</option>
				<option>Product 2</option>
				<option>Product 3</option>
				<option>Product 4</option>
				<option>Product 5</option>
				<option>Product 6</option>
				</select>
			</td>
			<td><span class="acc_con_label">Value</span> 				
				<div class="form_error" id="err_Subject"></div>
			</td>
			<td><span class="acc_con_label">Quantity</span> 				
				<div class="form_error" id="err_Subject"></div>
			</td>
			<td align="right"><span class="acc_con_label" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ext.</span> 				
				<div class="form_error" id="err_Subject"></div>
			</td>
		</tr>
		
		<tr>
			<td>
				<span class="acc_con_label">Sales Cycle Loc.</span><br />
				<select name="creator" onChange="RemoveAttendee(this.value);UpdateAttendeeSelect();">
				<option>Select a Sales Cycle Location</option>
				<option>Lead</option>
				<option>Met Once</option>
				<option>Discovery</option>
				<option>Specifications</option>
				<option>Negotiations</option>
				<option>Internal Approval</option>
				<option>Almost Closed</option>
				<option>Closed</option>
				</select>
			</td>
			<td width="50">
				<span class="acc_con_label">% to Close</span> 								
				<select name="creator" onChange="RemoveAttendee(this.value);UpdateAttendeeSelect();">
				<option>Select % to Close</option>
				<option>2%</option>
				<option>10%</option>
				<option>25%</option>
				<option>35%</option>
				<option>50%</option>
				<option>70%</option>
				<option>85%</option>
				<option>100%</option>
				</select>
			</td>
			
		</tr>
				
		<tr>
			<td><span class="acc_con_label">Note</span><br />
				<textarea name="Note" class="clsTextBox" cols="35" rows="8" id="Notes" >{$module->event_info.Notes}</textarea>				
				<div class="form_error" id="err_Notes"></div>
			</td>
		</tr>
	</table>
</div>
</div>


{literal}
<script language="javascript">
var people_list;
var type_list;
var webpath = '{/literal}{$smarty.const.WEB_PATH}{literal}';
var prev_start = '';

$(document).ready(function() {
	var target = 0;
	{/literal}
	people_list = {$module->GetPeople()|@json_encode};
	type_list = {$module->GetEventTypes()|@json_encode};
	{literal}

	var start_date = $("input[name='StartDate']").val();
	if (start_date != '') {
		prev_start = $.datepicker.parseDate('m/d/y', start_date);
	}

	try {
		// Get the selected user from edit-opportunity 
		target = ssSalesPerson.getSelectedVal();				
	} catch(err) {
	}		
	$("#eventMiniCalendar").load("{/literal}{$smarty.const.WEB_PATH}{literal}/ajax/ajax.EventMiniCalendar.php?target=" + target);

	DoChangeRepeat($("select[name='Repeat']").val());
	RemoveAttendee($("select[name='creator']").val());
	UpdateAttendeeSelect();
	
	$("input[name='StartDate']").datepicker({
		showOn :"both",
		buttonImage : webpath + "/images/calendar.gif",
		buttonImageOnly :true,
		dateFormat: 'm/d/y',
		onSelect: function(dateText, inst) {
			var start_date = $(this);
			var start_parsed = $.datepicker.parseDate('m/d/y', start_date.val());

			
			var end_date = $("input[name='EndDate']");
			var repeat_end = $("input[name='RepeatEndDate']");
			if (end_date.val() == '') {
				end_date.val(start_date.val());
			} else if (prev_start != '') {
				var end_parsed = $.datepicker.parseDate('m/d/y', end_date.val());
				var new_end = new Date();
				new_end.setTime(end_parsed.getTime() + start_parsed.getTime() - prev_start.getTime() + 3600000);
				end_date.val($.datepicker.formatDate('m/d/y', new_end));
			} 
			if (repeat_end.val() != undefined && start_parsed > $.datepicker.parseDate('m/d/y', repeat_end.val())) {
				repeat_end.val(start_date.val());
			}
			prev_start = $.datepicker.parseDate('m/d/y', start_date.val());
			end_date.datepicker('option', 'minDate', start_parsed);
			repeat_end.datepicker('option', 'minDate', start_parsed);
		}
	});

	$("input[name='EndDate']").datepicker({
		showOn :"both",
		buttonImage : webpath + "/images/calendar.gif",
		buttonImageOnly :true,
		dateFormat: 'm/d/y'
	});

	$("input[name='repeat_enddate']").datepicker({
		showOn :"both",
		buttonImage : webpath + "/images/calendar.gif",
		buttonImageOnly :true,
		dateFormat: 'm/d/y'
	});

	if ($("select[name='EventType']").val() != '') {
		DoChangeType($("select[name='EventType']").val());
	}
});


function AddAttendee() {
	var to_add = $("select[name='add_attendee']").find(':selected').val();
	if (to_add != "") {
		$("#attendees").append('<div id="attendee_' + to_add + '">' + people_list[to_add] + '&nbsp;&nbsp;&nbsp;<a style="font-weight:bold;color:red;" onClick="RemoveAttendee(' + to_add + ')">X</a></div>');
		UpdateAttendeeSelect();
	}
}

function RemoveAttendee(attendee_id) {
	$("#attendee_" + attendee_id).remove();
	UpdateAttendeeSelect();
}

function UpdateAttendeeSelect() {
	var attendee_select = $("select[name='add_attendee']");
	attendee_select.children().remove();
	attendee_select.append(new Option('', '', true));

	// Get the list of the current attendees
	current_array = new Array();
	current_count = 0;
	if ($("select[name='creator']")) {
//		alert($("select[name='creator']").val());
		current_array[current_count] = $("select[name='creator']").val();
		current_count++;
	}
	$("div[id^='attendee_']").each(function() {
		current_array[current_count] = this.id.substring(9);
		current_count++;
	});

	$("input[name='attendee_list']").val(JSON.stringify(current_array));
	
	for (var person in people_list) {
		// alert(person);
		// alert(current_array);
		var found = false;
		for(var i = 0; i < current_array.length; i++) {
			if(current_array[i] == person) {
				var found = true;
			}
		}
		if (found) continue;

		attendee_select.append("<option value='" + person + "'>" + people_list[person] + "</option>");
	}
}

function DoChangeRepeat(value) {
	$('#repeat_choices div').hide();
	$('#event_repeat fieldset').hide();
	if (value > 0) {
		$('#event_repeat fieldset').show();
		var field = 'repeat_' + value;
		$('#' + field).show();
	}
}

function DoChangeType(value) {
	/*
	if (type_list[value]['Type'] == 'TASK') {
		$('#event_datetime').hide();
		$('#event_repeat').hide();
		$('#attendee_section').hide();
		$("div[id^='attendee_']").remove();
		UpdateAttendeeSelect();		
	} else {
		$('#event_datetime').show();
		$('#event_repeat').show();
		$('#attendee_section').show();
	}
	*/
}

</script>

{/literal}
