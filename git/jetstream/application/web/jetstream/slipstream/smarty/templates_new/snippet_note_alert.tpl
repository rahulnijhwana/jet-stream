<div>
	<div class="img_wrapper">
		<div id="add_alert_wrapper">
			<input type="button" id="expand_alert" value="Add Alert" />
		</div>
		<div class="help_icon">
			<a href="javascript: void(0);" class="note_alert_help" rel="ajax/ajax.contextHelp.php">
				<img src="images/help_red.png" alt="What is this?" style="width: 24px; height: 24px;" />
			</a>
		</div>
		<div class="clearing"></div>
	</div>
	<div id="add_alert">
		<div class="alert_options">
			<span class="subtitle">1. How do you want to send the Alert?</span>
			<table class="user_tree">
				<tr>
					<td>
						<p class="delivery_methods">
							<label for="standard_alert">
								<input type="checkbox" name="standard_alert" id="standard_alert" checked="checked" />
								On Screen
							</label>
						</p>
					</td>
					<td>
						<p class="delivery_methods">
							<label for="email_alert">
								<input type="checkbox" name="email_alert" id="email_alert" />
								Email
							</label>
						</p>
					</td>
				</tr>
			</table>
			<span id="spn_delivery_error" class="clsError"></span>
		</div>
		<div class="alert_options">
			<span class="subtitle">2. When should we send it?</span>
			<table class="user_tree">
				<tr>
					<td>
						<p class="timing">
							<label for="send_immediately_yes">
								<input type="radio" name="send_immediately" id="send_immediately_yes" checked="checked" value="1" />
								Immediately
							</label>
						</p>
					</td>
					<td>
						<p class="timing">
							<label for="send_immediately_no">
								<input type="radio" name="send_immediately" id="send_immediately_no" value="0" /> 
								Delay until
							</label>
						</p>
						<input type="text" name="delay_until" id="delay_until_day" class="datepicker" size="12" value="" disabled="disabled" />
						<span class="time_select">{$module->hourSelect} : {$module->minuteSelect} {$module->periodSelect}</span> 
					</td>
				</tr>
			</table>
			<span id="spn_delay_until_error" class="clsError"></span>
		</div>
		<div class="alert_options">
			<span class="subtitle">3. Who should we send it to?</span>
			<p>
				<span class="links">
					<a href="javascript:void(0);" id="select_all">Select All</a> |
					<a href="javascript:void(0);" id="select_assigned">Select Assigned</a> |
					<a href="javascript:void(0);" id="select_level">Select Level</a>
				</span>
			</p>
			<span id="spn_select_user_error" class="clsError"></span>
			<span id="level_msg">Click on the column headers below to select users by level.</span>
			<table class="user_tree">
				<tr class="header">
				{foreach from=$module->tree key=level item=users}
					<td>
						<a href="javascript:void(0);" id="level_{$level}" class="level">{$level}</a>
					</td>
				{/foreach}
				</tr>
				<tr>
				{foreach from=$module->tree key=level item=users}
						<td class="user_level">
							<ul class="simple">
								{foreach from=$users item=user}
									<li {if $user.AssignedTo}class="heavy"{/if}>
										<p class="timing">
											<label>
												<input type="checkbox" name="alert_user[]" value="{$user.PersonID}" class="all_users {if $user.AssignedTo} assigned_users{/if} level_{$user.Level}" />
												{$user.FirstName} {$user.LastName}
											</label>
										</p>
										<div class="clearing"></div>
									</li> 
								{/foreach}
							</ul>
						</td>
				{/foreach}
				</tr>
			</table>
		</div>
	</div>
</div>
