<div id="event_alert_wrapper">
	<div class="img_wrapper">
		<div id="add_event_alert_wrapper">
			<input type="button" id="expand_event_alert" value="Add Alert" />
		</div>
		<div class="help_icon">
			<a href="javascript: void(0);" class="note_alert_help" rel="ajax/ajax.contextHelp.php">
				<img src="images/help_red.png" alt="What is this?" style="width: 24px; height: 24px;" />
			</a>
		</div>
		<div class="clearing"></div>
	</div>
	<div class="form_error" id="err_Testing"></div>

    {*	
	{if $module->event_info.Contact.FirstName != '' or $module->event_info.Contact.LastName != ''}
    *}

	<div id="add_event_alert">
		<div class="alert_options">
			<span class="subtitle">1. How do you want to send the Alert?</span>
			<table class="user_tree">
				<tr>
					<td>
						<p class="delivery_methods">
							<label for="standard_event_alert">
								<input type="checkbox" name="standard_event_alert" id="standard_event_alert" checked="checked" />
								On Screen
							</label>
						</p>
					</td>
					<td>
						<p class="delivery_methods">
							<label for="email_event_alert">
								<input type="checkbox" name="email_event_alert" id="email_event_alert" />
								Email
							</label>
						</p>
					</td>
				</tr>
			</table>
			<div class="form_error" id="err_Delivery"></div>
		</div>
		<div class="alert_options">
			<span class="subtitle">2. When should we send it?</span>
			<table class="user_tree">
				<tr>
					<td class="left_cell">
						<p class="timing">
							<label for="send_event_immediately_yes">
								<input type="radio" name="send_event_immediately" id="send_event_immediately_yes" checked="checked" value="1" />
								Now
							</label>
						</p>
					</td>
					<td class="right_cell">
						<p class="timing">
							<label for="send_event_immediately_no">
								<input type="radio" name="send_event_immediately" id="send_event_immediately_no" value="0" /> 
								Delay until
							</label>
						</p>
						<input type="text" name="delay_event_until" id="delay_event_until_day" class="datepicker" size="12" value="" disabled="disabled" />
						<span class="time_select">{$module->hourSelect}{$module->minuteSelect}{$module->periodSelect}</span> 
					</td>
				</tr>
			</table>
			<div class="form_error" id="err_Delay"></div>
		</div>
		<div class="alert_options">
			<span class="subtitle">3. Who should we send it to?</span>
			<p>
				<span class="links">
					<a href="javascript:void(0);" id="select_event_all">Select All</a> |
					<a href="javascript:void(0);" id="select_event_assigned">Select Assigned</a> |
					<a href="javascript:void(0);" id="select_event_level">Select Level</a>
				</span>
			</p>
			<div class="form_error" id="err_Users"></div>
			<span id="level_event_msg">Click on the column headers below to select users by level.</span>
			<table class="user_tree">
				<tr class="header">
				{foreach from=$module->tree key=level item=users}
					<td>
						<a href="javascript:void(0);" id="event_level_{$level}" class="event_level">{$level}</a>
					</td>
				{/foreach}
				</tr>
				<tr>
				{foreach from=$module->tree key=level item=users}
						<td class="user_level">
							<ul class="simple">
								{foreach from=$users item=user}
									<li {if $user.AssignedTo}class="heavy"{/if}>
										<p class="timing">
											<label>
												<input type="checkbox" name="alert_user[]" value="{$user.PersonID}" class="all_users {if $user.AssignedTo} assigned_users{/if} event_level_{$user.Level}" />
												{$user.FirstName} {$user.LastName}
											</label>
										</p>
										<div class="clearing"></div>
									</li> 
								{/foreach}
							</ul>
						</td>
				{/foreach}
				</tr>
			</table>
		</div>
	</div>
    {*
	{else}
		<p>You must save the event with a contact before the alert settings are made available.
	{/if}
    *}
	
</div>
<input type="hidden" id="all_event_checked" name="all_event_checked" />
<input type="hidden" id="assigned_event_checked" name="assigned_event_checked" />
<input type="hidden" id="level_event_checked" name="level_event_checked" />
<input type="hidden" id="add_event_alert_hidden" name="add_event_alert_hidden" />
<input type="hidden" id="eventNoteSplType" name="eventNoteSplType" value="" />
