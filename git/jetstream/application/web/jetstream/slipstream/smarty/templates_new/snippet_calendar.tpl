<div>
	<div class="week_cal_header">
		<a href="javascript:GetCalendar('{$module->type}', '{$module->previous}', '{$module->selected_sales_person}'{if $module->type eq 'daily' && $module->merged_view eq 1 && $module->is_manager},1{/if})"><img src="images/left-arrow-blue.png"/></a><span class="week_cal_header_text">{$module->display_date}</span><a href="javascript:GetCalendar('{$module->type}', '{$module->next}', '{$module->selected_sales_person}'{if $module->type eq 'daily' && $module->merged_view eq 1 && $module->is_manager},1{/if})"><img src="images/right-arrow-blue.png"/></a> 
	</div>
	
{if $module->toggle_view eq 'box'}	
	<table class="cal_task">
		<tr>	
	{foreach from=$module->days key=key item=item}						
		{if $module->type=="weekly"}
			<td style="width:14.3%;text-align:center;vertical-align:top;font-size:.9em;">
				<a href="javascript:GetCalendar('daily', '{$item.date}', '{$module->selected_sales_person}')">{$item.Day|replace:' ':'<br>'}</a>
		{else}
				<td style="text-align:center;">
					<a href="#">{$item.DayName}</a>
		{/if}
		{*------------Task Section--------*}
		{if $item.tasks|@count}
			<div title="Tasks for {$item.date_string}" id="ajax/ajax.GetTask.php?date={$item.date}" class="task_preview" style="border:1px solid #CCDDEE;text-align:center;"> 
				{if $module->type == "weekly"}
					{if $item.today}
						{$item.closed_task_count}/{$item.tasks|@count} Task{if $item.tasks|@count gt 1}s{/if}
					{elseif $item.date|@strtotime le $smarty.now}	
						{$item.tasks|@count} Task{if $item.tasks|@count gt 1}s{/if} Comp
					{else}
						{$item.tasks|@count} Task{if $item.tasks|@count gt 1}s{/if}
					{/if}
				{else}
					{if $item.today}
						{$item.closed_task_count} of {$item.tasks|@count} Task{if $item.tasks|@count gt 1}s{/if} Completed
					{elseif $item.date|@strtotime le $smarty.now}
						{$item.tasks|@count} Task{if $item.tasks|@count gt 1}s{/if} Completed
					{else}
						{$item.tasks|@count} Task{if $item.tasks|@count gt 1}s{/if}
					{/if}
				{/if}
			</div>
			<div class="task_list" id="list_{$key}" style="display:none;position:absolute;font-size:1.1em;line-height:1.3em;" />
			</div>
		{/if}		
		{*--------------------*}					
			</td>
			
	{/foreach}
		</tr>
	</table>
{/if}

</div>
<div id="week_cal" class="{if $module->toggle_view eq 'box'}week_cal{else}week_cal_list{/if}">
{if $module->toggle_view eq 'box'}
	<table class="calendar">
		<tr>
			<td class="time">
				{foreach from=$module->hours item=item}
					<div class="time_label">{$item}</div>
				{/foreach}			
			</td>
			{foreach from=$module->days item=days}
			<td {if $days.today}class="today"{/if} onclick="javascript:ShowAjaxForm('EventNew&contactid=0&action=add&StartDate='+GetCalendarTime(event, document, '{$days.date}', 0)+'&EndDate='+GetCalendarTime(event, document, '{$days.date}', 1));window.scrollTo(0,0);">
				<div class="event_con">
					<!--
					top = [15min height] * [# of 15mins] - 1 
					height = [15min height] * [# of 15mins] - 1 + 2px for IE 
					currently 15min height = 11px
					-->					
					{foreach from=$days.events key=key item=item}
					<div id="ajax/ajax.EventTooltip.php?ref={$item.id_enc}" class="event_pos preview" style="top:{$item.VerticalOffset*11-1}px;left:{$item.LeftPosition}%;width:{$item.Width}%;">
						<div class="event {if $item.pending}pendingevent{/if}" style="background-color:#{$item.eventtype.EventColor};border-color: {$item.user.Color};height:{$item.Height*11-3}px;-height:{$item.Height*11+2}px;{if $item.PrivateEvent eq 1}cursor:default;{/if}" {if $item.is_viewable}{if $item.RepeatParent}onclick='javascript:LoadRptngEvnt({$item.RepeatParent}, "{$item.id_enc}")'{else}onclick="javascript:location.href='slipstream.php?action=event&eventId={$item.EventID}&ref=weekly'"{/if}{/if}>
							{if $item.Height > 1}
							<div class="event_head {if $item.Closed}closedevent{/if}" style="background:{$item.user.Color};">
								{$item.start_time_string} - {$item.end_time_string} ({$item.duration_string})
							</div>
							{/if}
							<div>
								{if $item.Height > 2}
								<span {if $item.Closed}class="closedevent"{/if} style="white-space:nowrap;overflow:hidden;">
									{if $item.is_viewable}
										<p class="subtitle">{$item.eventtype.EventName}</p>
										<span>Contact</span> <a onClick='NoBubble(event, "?action=contact&contactId={$item.ContactID}");'>{$item.Contact.FirstName|@SplitLongWords} {$item.Contact.LastName|@SplitLongWords}</a><br>
										<span>Company</span> <a onClick='NoBubble(event, "?action=company&accountId={$item.Contact.AccountID}");'>{$item.Contact.CompanyName}</a><br>
										<span>Subject</span> {$item.Subject}
									{else}
										<span>User</span> {$item.user.FirstName|@SplitLongWords} {$item.user.LastName|@SplitLongWords}<br>
										Personal
									{/if}
								</span>
								{/if}
							</div>
						</div>						
					</div>
					{/foreach}
				</div>
				&nbsp;
			</td>
			{/foreach}
		</tr>
	</table>
{else}
	{foreach from=$module->days item=days}
		{if $module->type=="weekly"}
			<div style="margin: 20px 0 5px 0; font-size: 1.4em; font-variant: small-caps;">
				<a style="color:black; font-weight:normal;" href="javascript:GetCalendar('daily', '{$days.date}', '{$module->selected_sales_person}')">
					{$days.LongDay}
				</a>
			</div>
		{/if}
		<div style="border:1px solid #DDD; padding:10px;{if $days.today}background:#F7EC8B;{/if}">		
		{if $days.events}
			{foreach from=$days.events name=events key=key item=item}
				<div class="event {if $item.pending}pendingevent{/if}" style="background-color:#{$item.eventtype.EventColor};border-color:{$item.user.Color};{if !$smarty.foreach.events.last}margin-bottom:10px;{/if}" >
					<div class="event_head {if $item.Closed}closedevent{/if}" style="background:{$item.user.Color};" style="cursor:default;">
						{$item.start_time_string} - {$item.end_time_string} ({$item.duration_string})
					</div>
					<div {if $item.Closed}class="closedevent"{/if} {if $item.PrivateEvent}style="cursor:default;"{else}{if $item.RepeatType gt 0}onclick='LoadRptngEvnt({$item.EventID}, "{$item.id_enc}")'{else}onclick="javascript:location.href='slipstream.php?action=event&eventId={$item.EventID}{/if}{if $module->target neq ''}&target={$module->target}{/if}{/if}'">
						{if $item.is_viewable}
							{$item.eventtype.EventName|@SplitLongWords}<br>
							<span>User</span> {$item.user.FirstName|@SplitLongWords} {$item.user.LastName|@SplitLongWords}<br>
							<span>Contact</span> <a href="?action=contact&contactId={$item.ContactID}">{$item.Contact.FirstName|@SplitLongWords} {$item.Contact.LastName|@SplitLongWords}</a><br>
							<span>Company</span> <a href="?action=company&accountId={$item.Contact.AccountID}">{$item.Contact.CompanyName|@SplitLongWords}</a><br>
							<span>Subject</span> {$item.Subject|@SplitLongWords}
							{if $item.Location}<br />
								<span>Location</span>
							{$item.Location|@SplitLongWords}{/if}
						{else}
							<span>User</span> {$item.SalesPersonFirstName|@SplitLongWords} {$item.SalesPersonLastName|@SplitLongWords}<br>
							Personal
						{/if}
					</div>
				</div>
			{/foreach}
		{else}
			No events scheduled <br/>
		{/if}
			</div>
	{/foreach}
{/if}
</div>
<script>
	{literal}
	$(document).ready(function() {
		ResetCalTooltips();
		$('#week_cal').attr('scrollTop', 332);
	});
	{/literal}
</script>
