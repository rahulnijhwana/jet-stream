<div>
	<div id="SavedList">
		<div style="width:100%">
			<table style="width:100%"><tr>
			<td style="text-align:left;v-align:bottom;">Click One of the following to load:</td>
			<td style="text-align:right;v-align:top;">{*<img src="images/refresh.jpg" onclick="javascript:this.blur();AjaxLoadSlipstreamSearch('loadList', 0 , '')">*} </td>
			</tr></table>
		</div>
		<div style="padding-top:5px;padding-bottom:10px;">
			<div class="module_container_overflow" style="width:100%;height:100px;border:1px solid #D6E0D6;background-color:#F0F5FA;">
				<table style="width:100%" >
				{if count($module->filter_list) gt 0}
					{foreach from=$module->filter_list item=SaveCriteria}
						<tr>
							<td style="width:20%"> &nbsp; &nbsp; - </td>
							<td style="width:60%;text-align:left;"> <a href="javascript:AjaxLoadSlipstreamSearch('loadSearch',{$SaveCriteria.Saved_ID}, '{$SaveCriteria.Name}')">{$SaveCriteria.Name}</a>  </td>
							<td style="width:20%"> &nbsp; &nbsp; <a href="javascript:AjaxLoadSlipstreamSearch('deleteList',{$SaveCriteria.Saved_ID}, '{$SaveCriteria.Name}')"><span style="color:red;"> X </span></a> </td>
						</tr>				
					{/foreach}
				{else}
					<tr>
						<td> &nbsp; &nbsp; - </td>
						<td> &nbsp; &nbsp; Empty Set  </td>
						<td> &nbsp; &nbsp;   </td>
						<td> &nbsp; &nbsp; </td>
					</tr>
				{/if}
				</table> 
			</div>
		</div>
		
	</div>
</div>

<script language="javascript">

	var filter_count = 0;
	{literal}

	$(document).ready(function() {
		CreateAdvFilter();
		UpdateAdvFilter();
	});
	function CreateAdvFilter() {
		$('#advanced_filter').append('<div id="advfiltercont_' + filter_count + '" style="margin:20px 0px"></div>');
		$("select[name='template_advfilter']").clone().attr("name", "advfilter_" + filter_count).prependTo('#advfiltercont_' + filter_count);
		filter_count++;
	}

	function ChangeAdvFilter(obj) {
		var id = obj.name.substring(10);
		var value = obj.value;
		// 1.  Clear out any existing input for the current field
		$('#advfilterinputcont_' + id).remove();

		// 1.5.  Get updated search results if input was in previous result
		// 2.  Get AJAX data input
		if (value != '') {
			$.post("ajax/ajax.adv_search_filter.php", {field:value, form_id:id}, function(data) {
				var input_div = '<div id="advfilterinputcont_' + id + '">' + data + '</div>';
				$('#advfiltercont_' + id).append(input_div);
			}, "html");
		}
				
		// 3.  Add a blank filter (if all filters are used)
		var blank_request_available = false;
		$("select[name^='advfilter_']").each(function() {
			if ($(this).val() == '') {
				blank_request_available = true;
				return false;
			}
		});
		if (!blank_request_available) {
			CreateAdvFilter();
		}

		// 4.  Update filter selects
		UpdateAdvFilter();
	}

	function ChangeSearchTerm() {
		// 3.  Get updated search results
	}
	
	function UpdateAdvFilter() {
		$("select[name^='advfilter_']").each(function() {
			select = $(this);
			var value = select.attr('value');
			var id = select.attr("name").substring(10);
			select.remove();
			$("select[name='template_advfilter']").clone().attr("name", "advfilter_" + id).prependTo('#advfiltercont_' + id).val(value).show();
		});
		
		var selects = $("select[name^='advfilter_']");
		selects.each(function() {
			outside_field = $(this).attr('name');
			outside_option = $(this).val();
			if (outside_option != '') {
				selects.each(function() {
					if ($(this).attr('name') != outside_field) {
						$(this).find("option[value='" + outside_option + "']").remove();
					}
				});
			}
		});
	}	

	
	{/literal}


</script>
