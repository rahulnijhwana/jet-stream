{if $module->target!='' && $module->merged_view==0}<strong>{$module->active_sales_person}</strong>{/if}
<div style="width:100%;">
	{if $module->is_manager}
		{include file="snippet_merge_calendar_form.tpl"}
	{/if}	
	{if ($module->type == 'monthly') } 
		{include file="snippet_calendar_monthly.tpl"}
	{else}
		{include file="snippet_calendar_weekly.tpl"}
	{/if}
</div>
<script language="javascript">
	var cal_type = '{$module->type}';
</script>