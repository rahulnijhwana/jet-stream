<div style="width:100%;">
	{if $module->sales_people|@count > 1}
		{include file="snippet_merge_calendar_form.tpl"}
	{/if}	
	{if ($module->type eq 'monthly' || $module->type eq 'fiveweek') } 
		{include file="snippet_calendar_monthly.tpl"}
	{else}
		{include file="snippet_calendar.tpl"}
	{/if}
</div>
<script language="javascript">
	var cal_type = '{$module->type}';	
	var cal_date = '{$module->day}';
</script>