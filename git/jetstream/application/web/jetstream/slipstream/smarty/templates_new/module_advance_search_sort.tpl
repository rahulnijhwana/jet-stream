<div class="module_container">
	<select name="SortType" id="SortType" class="clsTextBox" style="width:150px;" onchange="javascript:callDashBoardSearch();">
	{html_options output=$module->sort[1] values=$module->sort[0] selected=$module->sort_type}
	</select>
	
	<select name="SortAs" id="SortAs" class="clsTextBox" onchange="javascript:callDashBoardSearch();">
		<option value="0" {if $module->sort_dir eq 'ASC'}selected{/if}>Ascending</option>	
		<option value="1" {if $module->sort_dir eq 'DESC'}selected{/if}>Descending</option>	
	</select>
</div>