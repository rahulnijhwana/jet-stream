<div class="module_container_overflow" id="divDataUpload">
	<h1 class="errorMsg">A fatal error has occurred.</h1>
	{foreach from=$module->errors item=error}
		<p class="uploadError">{$error}</p>
	{/foreach}
	<br>
	<p>Please contact ASA for assistance at (773) 714-8100 or check your data and try again.</p>
</div>