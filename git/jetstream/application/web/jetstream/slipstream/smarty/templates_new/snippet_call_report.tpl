<div id="call_report_filter_wrapper">
	<p class="subtitle">Show calls and events for a date range</p>
	<div id="call_report_range_select">
		<input type="text" name="ReportStartDate" class="datepicker" size="20" id="ReportStartDate" value="" />
		<span class="to">to</span>
		<input type="text" name="ReportEndDate" class="datepicker" size="20" id="ReportEndDate" value="" />
		<input type="submit" value="Show Report" name="SearchButton" id="SearchButton" onclick="javascript: CallReportSearch();" />
	</div>
	<div id="call_report_user">
		{if $module->show_user_in_report}
			Filter By User: <select name="report_user" class="clsSelect" id="report_user" onchange="javascript: FilterReport();">
				<option value="">All</option>	
				{foreach from=$module->sales_people item=item key=key name=person}
				<option value="{$item.PersonID}">{$item.FirstName|cat:' '|cat:$item.LastName}</option>
				{/foreach}
			</select>
		{/if}
	</div>
</div>
<div class="clearing"></div>
<div id="call_report_toggles_wrapper">
	<div class="call_report_toggle_wrapper">
		<p>
			<label for="call_report_filter_tasks">
				<input type="checkbox" id="call_report_filter_tasks" {if $all_checked}checked{/if} />
				Show Uncompleted Tasks
			</label>
		</p>
	</div>
	<div class="call_report_toggle_wrapper">
		<p>
			<label for="call_report_filter_events">
			 <!--start:Jet-3, default value to unchecked  -->      
                         	<input type="checkbox" id="call_report_filter_events"  />
			<!--end:Jet-3 -->	
                               Show Uncompleted Events
			</label>
		</p>
	</div>
	<div class="clearing"></div>
</div>
<input type="hidden" name="report_type" id="report_type"  value="" />
<div id="div_report_content">
	<p class="subtitle">At a glance</p>
	<div id="div_todays_report">
		<div class="toggle">
			<div class="call_report_label_wrapper">
				<a class="label" href="javascript: toggleCallReport('todays_report', '{$module->today}', '{$module->end_of_today}')">
					<div class="call_report_label">
						<img id="toggle_todays_report" class="toggle_default_report" src="images/plus.gif" />
						<span class="contact_title">Today</span>
					</div>
					<div class="call_report_range">{$module->display_today}</div>
					<div class="clearing"></div>
				</a>
			</div>
			<div id="todays_report" style="display:none; text-align: center;" class="default_report"><img class="spinner" src="images/slipstream_spinner.gif" /></div>
		</div>
	</div>
	<div style="clear: both;" id="div_yesterdays_report" style="width: 100%">
		<div class="toggle">
			<div class="call_report_label_wrapper">
				<a class="label" href="javascript: toggleCallReport('yesterdays_report', '{$module->yesterday}', '{$module->end_of_yesterday}')">
					<div class="call_report_label">
						<img id="toggle_yesterdays_report" class="toggle_default_report" src="images/plus.gif" />
						<span class="contact_title">Yesterday</span>
					</div>
					<div class="call_report_range">{$module->display_yesterday}</div>
					<div class="clearing"></div>
				</a>
			</div>
			<div id="yesterdays_report" style="display:none; text-align: center;" class="default_report"><img class="spinner" src="images/slipstream_spinner.gif" /></div>
		</div>
	</div>
	<div style="clear:both;" id="div_week1_report" style="width:100%">
		<div class="toggle">
			<div class="call_report_label_wrapper">
				<a class="label" href="javascript: toggleCallReport('week1_report', '{$module->cur_week_start}', '{$module->cur_week_end}')">
					<div class="call_report_label">
						<img id="toggle_week1_report" class="toggle_default_report" src="images/plus.gif" />
						<span class="contact_title">This Week</span>
					</div>
					<div class="call_report_range">{$module->cur_week_start} - {$module->cur_week_end}</div>
					<div class="clearing"></div>
				</a>
			</div>
			<div id="week1_report" style="display:none; text-align: center;" class="default_report"><img class="spinner" src="images/slipstream_spinner.gif" /></div>
		</div>
	</div>
	<div style="clear:both;" id="div_week3_report" style="width:100%">
		<div class="toggle">
			<div class="call_report_label_wrapper">
				<a class="label" href="javascript: toggleCallReport('week3_report', '{$module->prev_week_start}', '{$module->prev_week_end}')">
					<div class="call_report_label">
						<img id="toggle_week3_report" class="toggle_default_report" src="images/plus.gif"/>
						<span class="contact_title">Previous Week:</span>
					</div>
					<div class="call_report_range">{$module->prev_week_start} - {$module->prev_week_end}</div>
					<div class="clearing"></div>
				</a>
			</div>
			<div id="week3_report" style="display:none; text-align: center;" class="default_report"><img class="spinner" src="images/slipstream_spinner.gif" ></div>
		</div>
	</div>
	<div style="clear:both;" id="div_mon1_report" style="width:100%">
		<div class="toggle">
			<div class="call_report_label_wrapper">
				<a class="label" href="javascript: toggleCallReport('mon1_report', '{$module->cur_mon_start}', '{$module->cur_mon_end}')">
					<div class="call_report_label">
						<img id="toggle_mon1_report" class="toggle_default_report" src="images/plus.gif" />
						<span class="contact_title">This Month</span>
					</div>
					<div class="call_report_range">{$module->cur_mon_start} - {$module->cur_mon_end}</div>
					<div class="clearing"></div>
				</a>
			</div>
			<div id="mon1_report" style="display:none; text-align: center;" class="default_report"><img class="spinner" src="images/slipstream_spinner.gif" /></div>
		</div>
	</div>
	<div style="clear:both;" id="div_mon3_report" style="width:100%">
		<div class="toggle">
			<div class="call_report_label_wrapper">
				<a class="label" href="javascript: toggleCallReport('mon3_report', '{$module->prev_mon_start}', '{$module->prev_mon_end}')">
					<div class="call_report_label">
						<img id="toggle_mon3_report" class="toggle_default_report" src="images/plus.gif" />
						<span class="contact_title">Previous Month</span>
					</div>
					<div class="call_report_range">{$module->prev_mon_start} - {$module->prev_mon_end}</div>
					<div class="clearing"></div>
				</a>
			</div>
			<div id="mon3_report" style="display:none; text-align: center;" class="default_report"><img class="spinner" src="images/slipstream_spinner.gif" /></div>
		</div>
	</div>
	<!--
	<br />
	<div style="clear:both;" id="div_week2_report" style="width:100%">
		<div class="toggle"><a href="javascript: toggleCallReport('week2_report', '{$module->next_week_start}', '{$module->next_week_end}')"><img id="toggle_week2_report" class="toggle_default_report" src="images/plus.gif"/></a>
		<b class="contact_title">Next Week:</b> {$module->next_week_start} - {$module->next_week_end}</div>
		<div id="week2_report" style="display:none;" class="default_report"><img class="spinner" src="images/slipstream_spinner.gif" ></div>
	</div>
	<br />

	<div style="clear:both;" id="div_mon2_report" style="width:100%">
		<div class="toggle"><a href="javascript: toggleCallReport('mon2_report', '{$module->next_mon_start}', '{$module->next_mon_end}')"><img id="toggle_mon2_report" class="toggle_default_report" src="images/plus.gif"/></a>
		<b class="contact_title">Next Month:</b> {$module->next_mon_start} - {$module->next_mon_end}</div>
		<div id="mon2_report" style="display:none;" class="default_report"><img class="spinner" src="images/slipstream_spinner.gif" ></div>
	</div>
	<br />
	-->
</div>

<script language="javascript" type="text/javascript">
{literal}


function More(id) {
	$('#less_' + id).toggle();
	$('#more_' + id).toggle();
}
{/literal}
</script>
