<div style="text-align:right;display:hidden">
{$module->SearchResultsInfo()} {$module->getDashboardPages()}
</div>
<div id="searchresults">
{if !$module->search_results}
	<div class="smlRedBold" style="text-align:center;">No matching file found</div>
{else}
	<div style="padding-left:20px;">

	
	<table cellpadding=0 cellspacing=0 border=0 id="mytable" class="filetablesorter"> 
		<thead>
			<th>&nbsp;</td>
			<th align=center>File Name</th>
			<th align=center>Related To</th>
			<th align=center>Attached By</th>
			<th align=center>Created On</th>
			<th align=center>Size</th>
		</tr>
		</thead>
		<tbody>
	{foreach from=$module->search_results item=result}
		<tr>	
			<td> <img src="images/{$result.Icon}.png"> </td>
			<td> <a href="javascript:DownloadFile('file', {$result.JetFileID}, '', '{$result.Typeid}')" alt="Download $result.LabelName"> {$result.LabelName}</a> </td>
			<td> 	
				{if $result.Type eq 'Account' } <a href="slipstream.php?action=account&accountId={$result.Typeid}">{$result.AccountName}</a> 
				{elseif $result.Type eq 'Contact' } <a href="slipstream.php?action=contact&contactId={$result.Typeid}">{$result.ContactName }</a>
				{elseif $result.Type eq 'Company' } Library {* $result.CompanyName *}
				{else} Error
				{/if}
				</td>
			<td> {$result.LoginName} </td>
			<td> {$result.CreatedOn} </td>
			<td align=right>{if $result.Size}{$result.Size}{else}0{/if}</td>	
		</tr>
	{/foreach}
	</tbody>
	</table>
	</div>
{/if}

</div>
<div style="text-align:right;">{$module->SearchResultsInfo()} {$module->getDashboardPages()}</div>
<script type="text/javascript">
{literal}
$(document).ready(function() 
    { 	
		$("#mytable").tablesorter({sortList:[[5,0],[2,1]], widgets: ['zebra']});
    } 
); 
{/literal}
</script>
