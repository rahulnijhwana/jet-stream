<div id="{$module->GetId()}">
	<div class="loading"><img class="spinner" src="images/slipstream_spinner.gif" \></div>
	<script type="text/javascript">
		{literal}
		$(document).ready(function() { 
			$.ajax({
				url: "ajax/ajax.GetModule.php",
				data: ({/literal}{$module->GetParams()}{literal}),
				type: "GET",
				dataType: "html",
				success: function(data){
					$("#{/literal}{$module->GetId()}{literal}").before(data).remove(); 
				}
			});
		});
		{/literal}
	</script>
</div>

