<div class="module_container" id="divJetFile">
	<script type="text/javascript">
	{literal}
		$(function(){
			$('#fileTree').fileTree({
				root: 0,
				script: 'ajax/ajax.FileTree.php',
				folderEvent: 'click',
				expandSpeed: 750,
				collapseSpeed: 750,
				action: {/literal}'{$module->rec_type|default:'Contact'}'{literal},
				id: {/literal}{$module->rec_id|default:'0'}{literal}
			},
			function(file) { 
				DownloadFile('open', file, {/literal}'{$module->file_path}'{literal}, {/literal}'{$module->rec_id}'{literal});
			});
		});
	{/literal}	
	</script>
	<div id="fileTree" class="filetree"></div>
</div>
