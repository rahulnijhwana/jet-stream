
<br />
<div id='jet_datagrid'>

	<table style="border:1px solid #CCCCCC; border-collapse: separate; border-spacing: 1px;">
	<tr>
		<th style="background:#FFCCA8;padding:3px 4px 4px 3px;border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 1px;"><div onClick="document.getElementById('SortType').value='OpportunityName';document.getElementById('SortAs').value='{$smarty.session.OpportunityNamesort_by}';javascript:AjaxLoadPage('1');" style="cursor:pointer;text-decoration: underline;font-size: 1.1em;fort-weight:bolder;">Opportunity Name</div></td>
		{if $smarty.session.isManager}<th style="background:#FFCCA8;padding:3px 4px 4px 3px;border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 1px;"><div onClick="document.getElementById('SortType').value='P_LastName';document.getElementById('SortAs').value='{$smarty.session.P_LastNamesort_by}';javascript:AjaxLoadPage('1');" style="cursor:pointer;text-decoration: underline;font-size: 1.1em;fort-weight:bolder;">Salesperson</div></td>{/if}	
		<th style="background:#FFCCA8;padding:3px 2px 2px 2px;border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 1px;"><div onClick="document.getElementById('SortType').value='A_Text01';document.getElementById('SortAs').value='{$smarty.session.A_Text01sort_by}';javascript:AjaxLoadPage('1');" style="cursor:pointer;text-decoration: underline;font-size: 1.1em;fort-weight:bolder;">Company</div></td>
		<th style="background:#FFCCA8;padding:3px 2px 2px 2px;border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 1px;"><div onClick="document.getElementById('SortType').value='C_Text02';document.getElementById('SortAs').value='{$smarty.session.C_Text02sort_by}';javascript:AjaxLoadPage('1');" style="cursor:pointer;text-decoration: underline;font-size: 1.1em;fort-weight:bolder;">Contact</div></td>
		<th style="background:#FFCCA8;padding:3px 2px 2px 2px;border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 1px;"><div onClick="document.getElementById('SortType').value='StartDate';document.getElementById('SortAs').value='{$smarty.session.StartDatesort_by}';javascript:AjaxLoadPage('1');" style="cursor:pointer;text-decoration: underline;font-size: 1.1em;fort-weight:bolder;">Creation Date</div></td>
		<th style="background:#FFCCA8;padding:3px 2px 2px 2px;border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 1px;"><div onClick="document.getElementById('SortType').value='SCLPercent';document.getElementById('SortAs').value='{$smarty.session.SCLPercentsort_by}';javascript:AjaxLoadPage('1');" style="cursor:pointer;text-decoration: underline;font-size: 1.1em;fort-weight:bolder;">Sale Cycle Stage(% to Close)</div></td>
		<th style="background:#FFCCA8;padding:3px 2px 2px 2px;border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 1px;"><div onClick="document.getElementById('SortType').value='theDate';document.getElementById('SortAs').value='{$smarty.session.theDatesort_by}';javascript:AjaxLoadPage('1');" style="cursor:pointer;text-decoration: underline;font-size: 1.1em;fort-weight:bolder;">Close Date/Exp. Close Date</div></td>
		<th style="background:#FFCCA8;padding:3px 2px 2px 2px;border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 1px;"><div onClick="document.getElementById('SortType').value='theValue';document.getElementById('SortAs').value='{$smarty.session.theValuesort_by}';javascript:AjaxLoadPage('1');" style="cursor:pointer;text-decoration: underline;font-size: 1.1em;fort-weight:bolder;">Product Value</div></td>

		<!--	<th style="background:#66FFFF;padding:2px 2px 2px 2px;"><div onClick="document.getElementById('SortType').value='R.SortingOrder';javascript:AjaxLoadPage('1');" style="cursor:pointer;text-decoration: underline;">Result</div></td>-->
		<!--<td class="label">Est. Total</td>
		<td class="label">Act. Total</td>-->

	</tr>

{if !$module->search_results}
	<div class="smlRedBold" style="text-align:center;">No matching records found</div>
{else}	

	{foreach from=$module->search_results item=result}
	<tr class="label" {if $result.SCLPercent neq 100 and $result.status neq 1 and $result.theDate|date_format:"%y%m/%d" < $smarty.now|date_format:"%y%m/%d"}style="background-color:#F7EC8B;"{/if}>
		<td style="padding:5px 2px 2px 5px;border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 1px;" width="20%"><a href="slipstream.php?action=opportunity&opportunityId={$result.OpportunityID}">{$result.OpportunityName|wordwrap:16:"\n":true}</a></td>
		{if $smarty.session.isManager}<td style="padding:2px 2px 2px 5px;border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 1px;" width="15%" >{$result.FirstName} {$result.LastName}</td>{/if}
		<td style="padding:2px 2px 2px 5px;border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 1px;" width="15%" >{$result.CompanyName|wordwrap:16:"\n":true}</td>
		<td style="padding:2px 2px 2px 5px;border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 1px;" width="10%" >{$result.Text01} {$result.Text02}</td>
		<td style="padding:2px 2px 2px 5px; text-align:center;border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 1px;" >{$result.StartDate|date_format:"%m/%d/%y"}</td>				
		<td style="padding:2px 2px 2px 5px;border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 1px;" >{if $result.status eq 1 and $result.ResultName}<span style="color: red;">{$result.ResultName}</span> -- {$result.SCLname}({$result.SCLPercent}%){elseif $result.status eq 2 and $result.ResultName}{$result.ResultName}{else} {$result.SCLname}({$result.SCLPercent}%){/if}</td>		
		<td style="padding:2px 2px 2px 5px; text-align:center;border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 1px;" >{$result.theDate|date_format:"%m/%d/%y"}</td>	
		<td style="padding:2px 2px 2px 5px; text-align:right;border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 1px;" >{if $result.status eq 1 and $result.theValue}<span style="text-decoration: line-through;">${$result.theValue|number_format:2:".":","}</span>{elseif $result.theValue}${$result.theValue|number_format:2:".":","}{else}-{/if}</td>		
	</tr>
	{/foreach}
{/if}
</table>	


</div>
{literal}
<style type="text/css">
.evenrow {
background-color: #FFDBC2; border:1px solid #DDDDDD; border-collapse: separate; border-spacing: 1px;
}
</style> 

<script language="javascript">

	$(document).ready(function() {

	var tr_row = 0;
	$("tr[class^='label']").each(function() {
			if(tr_row%2 == 1) $(this).addClass('evenrow');
			else $(this).removeClass('evenrow');
			tr_row++;
		});
	})

</script>

{/literal}
