<div class="module_container">

{if $module->opps}
	{foreach from=$module->opps key=key item=item}
	<div class="opp" style="background-color:{$item.Color}">
		<div class="caption">
			Salesperson : {$item.Salesperson}
		</div>

		{if ($module->type == 'Active') } 
		
			<div class="info" {if $item.Edit}style="cursor:pointer" onclick="javascript:openOppPopup({$item.DealID}, {$item.PersonID});"{/if}>				
				Location: {$item.Category}<br/>				
				Company: <a onclick="javascript:showPopup=false;" href="?action=company&accountId={$item.CompanyID}">{$item.Company|@SplitLongWords}</a><br/>
				{if $item.Contact neq " "}
				Contact: <a onclick="javascript:showPopup=false;" href="?action=contact&contactId={$item.ContactID}">{$item.Contact|@SplitLongWords}</a><br/>
				{/if}
				{if $item.Offerings neq ""}
				Offerings: {$item.Offerings|@SplitLongWords}<br/>
				{/if}				
				{if $item.MeetingLabel neq ""}
				{$item.MeetingLabel}: {$item.NextMeeting}<br/>
				{/if}
				
			</div>	
		{else}
			<div class="info" {if $item.Edit}style="cursor:pointer" onclick="javascript:openOppPopup({$item.DealID}, {$item.PersonID});"{/if}>
				Closed: {$item.ClosedDate}<br/>				
				Company: <a onclick="javascript:showPopup=false;" href="?action=company&accountId={$item.CompanyID}">{$item.Company|@SplitLongWords}</a><br/>
				{if $item.Contact|trim neq ""}
				Contact: <a onclick="javascript:showPopup=false;" href="?action=contact&contactId={$item.ContactID}">{$item.Contact|@SplitLongWords}</a><br/>
				{/if}
				Offerings: {$item.Offerings|@SplitLongWords}<br/>
				Total $: {$item.Total}
			</div>
		{/if}	
		
		
	</div>	
	 {/foreach}
{else}
	<div class="BlackBold">
		{$module->msg}
	</div>
{/if}	
</div>
<script language="javascript" type="text/javascript">
	{literal}
	var showPopup = true;
	//var showPopup = false;
	function openOppPopup(dealId, personId) {
		if(showPopup) {
			var win = Windowing.openSizedWindow('legacy/shared/edit_opp2.php?SN={/literal}{$smarty.cookies.SN}{literal}&detailview=1&reqOpId=' + dealId + '&reqPersonId=' + personId + '#' + dealId + '_' + personId, 630, 1000);
		}		
	}
	
	function addOpp(dealId, personId, acc, contact) {
			var account = acc.replace("&","<-and->" );
			var win = Windowing.openSizedWindow('legacy/shared/edit_opp2.php?SN={/literal}{$smarty.cookies.SN}{literal}&detailview=1' + '&account=' + account + '&contact=' + contact + '&reqOpId=' + dealId + '&reqPersonId=' + personId + '#' + dealId + '_' + personId , 630, 1000);
	}	
	{/literal}
</script>
