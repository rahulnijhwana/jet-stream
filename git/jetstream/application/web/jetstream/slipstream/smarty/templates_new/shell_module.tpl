{* Encase below to add shadows
<div class="content_head_wrap">
</div>
<div class="content_body_wrap">
</div>
<div class="content_foot_wrap">
	<div class="content_foot">
		<div class="content_foot_left">
		</div>
	</div>
</div>
*}
<div {if $module->id}id="{$module->id}"{/if} {if $module->formatting}class="{$module->formatting}"{/if}>
	<div class="content_head">
		<div class="content_head_text">
			{if $module->id == 'dashboard_simple_search'}
				<a href="javascript: void(0);" id="toggle_dashboard_search_link"><img id="toggle_dashboard_search" src="images/minus-med.gif" /></a>
			{/if}
			{$module->title}
		</div>
		<div class="content_head_icons">
			{foreach from=$module->buttons name=buttons item=button}<a {if $button.id}id="{$button.id}"{/if} title="{$button.tooltip}" href="{$button.action}" target="{$button.target}">{$button.tooltip}</a>{if !$smarty.foreach.buttons.last} | {/if}{/foreach}
		</div>
		{if $module->menu}
			<ul class="sf-menu-module">
				{foreach from=$module->menu item=menu_item}
				<li{if $menu_item.selected} class="selected"{/if}><a href="{$menu_item.action}">{$menu_item.label}</a></li>
				{/foreach}
			</ul>
		{/if}
	</div>
	<div class="content_body">
		{foreach from=$module->template_files item=template_file}
		{include file=$template_file}
		{/foreach}
	</div>
</div>
