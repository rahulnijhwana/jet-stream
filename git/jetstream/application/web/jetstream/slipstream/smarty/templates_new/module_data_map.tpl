<div id="wrap">
	<form name="map" id="map" method="post">
		<div class="module_container_overflow" id="divDataUpload">
			<div id="stage">
				<table id="map">
					<tr>
						<th>Your Column Name</th><th style="width:25px"></th><th>Map To...</th>
					</tr>
					{foreach from=$rowheaders item=rowheader}
						<tr>
							<td style="text-align:right;padding-bottom:5px;">{$rowheader}</td><td style='background-image:url(images/ImgRightArrow.gif);background-repeat: no-repeat;' ></td>
							<td><div class="wrap" id="{$rowheader}"> {html_options name=$rowheader options=$myOptions selected=$dataImport->getSelectedValue($rowheader)}</div></td>
						</tr>
					{/foreach}
				</table>
				<p><input type="button" id="submitMap" value="Next >>"  /></p>
			</div>
		</div>
		<div id="results"></div>
		<input type="hidden" id="step" name="step" value="check" />
		<input type="hidden" id="csv" name="csv" value="{$csv}" />
	</form>
</div>