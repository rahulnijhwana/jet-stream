{literal}
<script type="text/javascript">
	$(document).ready(function() {$("#datefrom").datepicker( { showOn :'both',	buttonImage :'./images/cal.jpg',buttonImageOnly :true})});
	$(document).ready(function() {$("#dateto").datepicker( { showOn :'both',	buttonImage :'./images/cal.jpg',buttonImageOnly :true})});
</script>
{/literal}
<div id='dashboard'>
<form onsubmit="javascript:AjaxFileLoadPage(1);return false;" id="SlipstreamSearch" action="#" method="post" name="SlipstreamSearch">

	{* OPTIONS *}
	<div class="file_section_head" > 
		<img id="toggle_division_0" src="images/minus.gif"/> File Parameter
	</div>
	<div id="division_0" class="file_datagrid_b" >
		<table>
			<tr colspan=2 >
				<td> <input type="text" size=30 name="filename" > <input type=submit value=Search> <a href="javascript:FileSearchReset()"> <button>Reset</button></a>  
				</td>
			</tr>
			<tr colspan=2 >
				<td> Includes files attached to: 
				{*	<input type="checkbox" name=SearchType[] value="All" checked> All *}
					<input type="checkbox" name=SearchType[] value="Account" checked> Accounts 
					<input type="checkbox" name=SearchType[] value="Contact" checked> Contacts
					<input type="checkbox" name=SearchType[] value="Company" checked> Libraries 
				{*	<input type="checkbox" name=SearchType[] value="Event" checked> Events *}
				{*	<input type="checkbox" name=SearchType[] value="Notes" checked> Notes  *}
					
				</td>
			</tr>			
			<tr >
				<td>{* Search in  
						<select name="SearchType">
							<option value=ALL>ALL</option>
							<option value=Account>Account</option>
							<option value=Contact>Contact</option>
							<option value=Event>Event</option>
							<option value=Notes>Notes</option>
						</select>
				&nbsp;*}
				Created between <input type="text" id="datefrom" name="datefrom">  and <input type="text" name="dateto" id="dateto">  
				</td>
			</tr>
		</table>
	</div>
	{* /OPTIONS *}
	
	{* OPTIONS *}
	<div class="file_section_head" onclick="javascript:ToggleHidden('division_1');"  > 
		<img id="toggle_division_1" src="images/minus.gif"/> Tags
	</div>
	<div id="division_1" class="file_datagrid_b" >
		{if $module->tags|@count gt 0 }
			<table>
			{foreach from=$module->tags key=division item=layout}	
				{if $division % 7 eq 0  }
					<tr>
				{/if}	
					<td><input type="checkbox" name=tags[] value="{$layout.n}"> {$layout.labelName} </td>
					
				{if $division % 7 eq 6  }
					</tr>
				{/if}
				
			{/foreach}
				{if $division % 7 neq 6  }
					</tr>
				{/if}
			</table>
		{else}
			No Tags have been set please go to the Admin Console to add Tags.
		{/if}
		
	</div>
	{* /OPTIONS *}	
	<br>

	<div id="dashboardsearch">{include file="snippet_file_search_list.tpl"}</div>
	</form>
</div>
