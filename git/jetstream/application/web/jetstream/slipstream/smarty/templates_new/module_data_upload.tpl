<div class="module_container_overflow" id="divDataUpload">
	<div id="help">
		<h3 class="indentHeading">Import your existing company and contact data.</h3>
		<h3 class="indent">Rules</h3>
		<ol>
			<li>Data must be imported from a comma seperated values (CSV) file</li>
			<li>File must contain column headings</li>
			<li>Each column heading must be unique</li>
		</ol>
		<div id="upload">
			<form method="post" enctype="multipart/form-data">
				<fieldset>
					<legend>Upload File</legend>
					<input type="hidden" id="step" name="step" value="map" />
					<input type="file" id="csv" name="csv" />
					<input type="submit" id="enter" value="Upload CSV File" onclick="BusyOn()" />
				</fieldset>
			</form>
			{foreach from=$errors item=error}
				<p class="uploadError">{$error}</p>
			{/foreach}
		</div>
	</div>
</div>