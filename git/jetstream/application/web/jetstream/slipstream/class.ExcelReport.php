<?php
/*
 *	Will export .xls, .xlsx, and .csv 
 *  
 *  User needs to set some of the values 
 *  $properties ( Creator, LastModifiedBy, Title, Subject, Description, Keywords, Category)  []
 *  $header (header, footer)[**]
 *  $values () [**]
 *	$page 
 *	$headerFooter
 *  $filename
 *
 *	[*] - mandatory , [] - optional 
 */

require_once BASE_PATH .'/libs/PHPExcel.php';
require_once BASE_PATH .'/libs/PHPExcel/IOFactory.php';

class export
{
	public $properties = array(); 
	public $headers = array();
	public $datas = array();
	public $headerFooter = array(); 
	public $page = array(); 
	public $filename = array(); 
	public $report_type = ''; 
	public $weighted = ''; 
	public $sp_row_height;

	function xlscolumn($i){
		$i = $i - 1 ; 
		$remainder = $i % 26 ; 
		$dividend = ( $i - $remainder )/ 26  ; 	
		return ($dividend > 0 ) ? chr($dividend-1  +65 ) . chr($remainder +65 ) : chr($remainder +65);
	}

	public function xlsx() {

		$x = 0 ;
		$y = 0 ;
		$objPHPExcel = new PHPExcel();
		
		// Setting properties
		// Example 
		//$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
		//					 ->setLastModifiedBy("Maarten Balliauw")
		//					 ->setTitle("Office 2007 XLSX Test Document")
		//					 ->setSubject("Office 2007 XLSX Test Document")
		//					 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
		//					 ->setKeywords("office 2007 openxml php")
		//					 ->setCategory("Test result file");		
		//
		if ( count($this->properties) > 0 ) {
			if ( !empty($this->properties['Creator']) ) $objPHPExcel->getProperties()->setCreator($this->properties['Creator']);
			if ( !empty($this->properties['LastModifiedBy']) ) $objPHPExcel->getProperties()->setLastModifiedBy($this->properties['LastModifiedBy']);
			if ( !empty($this->properties['Title']) ) $objPHPExcel->getProperties()->setTitle($this->properties['Title']);
			if ( !empty($this->properties['Subject']) ) $objPHPExcel->getProperties()->setSubject($this->properties['Subject']);
			if ( !empty($this->properties['Description']) ) $objPHPExcel->getProperties()->setDescription($this->properties['Description']);
			if ( !empty($this->properties['Keywords']) ) $objPHPExcel->getProperties()->setKeywords($this->properties['Keywords']);
			if ( !empty($this->properties['Category']) ) $objPHPExcel->getProperties()->setCategory($this->properties['Category']);
		}

		// Not implemented Yet
		// Set page orientation and size
		//	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		//	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		// default paper A4 orientation POTRAIT or LANDSCAPE
		if ( count($this->page) > 0 ){
			$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		}
		
		//	Set header and footer. When no different headers for odd/even are used, odd header is assumed.
		//	$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&G&C&HPlease treat this document as confidential!');
		//	$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');
	
		if ( count($this->headerFooter) > 0 ) {
			if ( !empty($this->headerFooter['Header']) ) $objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddHeader($this->headerFooter['Header']);
			if ( !empty($this->headerFooter['Footer']) ) $objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter($this->headerFooter['Footer']);
		}

		//	Not Implemented Yet 
		// Add a drawing to the header
/*		$objDrawing = new PHPExcel_Worksheet_HeaderFooterDrawing();
		$objDrawing->setName('PHPExcel logo');
		$objDrawing->setPath('./images/phpexcel_logo.gif');
		$objDrawing->setHeight(36);
		$objPHPExcel->getActiveSheet()->getHeaderFooter()->addImage($objDrawing, PHPExcel_Worksheet_HeaderFooter::IMAGE_HEADER_LEFT);
		//
		*/
		
		
		// Add Headers 
		if ( !empty ($this->headers)){
			$x++;
			
			for ($y =0 ; $y < count($this->headers) ; $y++){
				$objPHPExcel->getActiveSheet()->setCellValue( $this->xlscolumn($y+1).$x, $this->headers[$y]) ; 
			}
		}
		else {
			echo "ERROR: header is empty";
			exit; 
		}
		// Add data
		if ( !empty($this->headers) ){
			foreach ($this->datas as $value){
				$x++;
				for ($y =0 ; $y < count($value) ; $y++){
					$objPHPExcel->getActiveSheet()->setCellValue( $this->xlscolumn($y+1).$x, $value[$y]) ; 
				}
			}
		}
		else {
			echo "ERROR: Value is empty";
			exit; 
		}
		
		//applying format	
		//Format Sales Process Report
		if($this->report_type == 'SalesProcess'){
			$max_column_number = count($this->datas[2]) + 1;
			$max_column_name = $this->xlscolumn($max_column_number);

			$max_row_number = count($this->datas) + 1;

			$max_column_name1 = $max_column_name.'1';
			$max_column2 = $max_column_name . '2';
			$max_column_name3 = $max_column_name.'3';
			$max_column_name4 = $max_column_name.'4';
			$max_column_name5 = $max_column_name.'5';
			$max_column_name6 = $max_column_name.'6';
			$max_column_name7 = $max_column_name.'7';
			//$max_column_name14 = $max_column_name.'14';
			$max_column_row = $max_column_name . ''.$max_row_number;
			$A_max_row = 'A'.$max_row_number;
			$max_column_row_1 = $max_column_name . ''.($max_row_number-1);
			$A_max_row_1 = 'A'.($max_row_number-1);

			$objPHPExcel->getActiveSheet()->mergeCells("A1:$max_column_name1");
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(45);

			$objPHPExcel->getActiveSheet()->getStyle("A1:$max_column_name1")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A1:$max_column_name1")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A1:$max_column_name1")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			//$objPHPExcel->getActiveSheet()->getStyle("A2:$max_column2")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A2:$max_column2")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A2:$max_column2")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A3:$max_column_name3")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A3:$max_column_name3")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A3:$max_column_name3")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight($this->sp_row_height);
			$objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setWrapText(true);

			$objPHPExcel->getActiveSheet()->mergeCells("A2:$max_column2");
			$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->mergeCells("A3:$max_column_name3");
			$objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->getStyle('A1:A3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('99CCFF');

			$objPHPExcel->getActiveSheet()->getStyle("B4:$max_column_name4")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->getStyle("A5:$max_column_name5")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("B5:$max_column_name5")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			if($this->weighted != 'true'){
				$objPHPExcel->getActiveSheet()->getStyle("B6:$max_column_row")->getNumberFormat()->setFormatCode('$#,##0.00');
				$objPHPExcel->getActiveSheet()->getStyle("B6:$max_column_row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			}
			else{
				$objPHPExcel->getActiveSheet()->getStyle("B7:$max_column_row")->getNumberFormat()->setFormatCode('$#,##0.00');
				$objPHPExcel->getActiveSheet()->getStyle("B7:$max_column_row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->getActiveSheet()->getStyle("B6:$max_column_name6")->getNumberFormat()->setFormatCode('#%');
				$objPHPExcel->getActiveSheet()->getStyle("B6:$max_column_name6")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			}
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
			for($i=1; $i <= $max_column_number; $i++){
				$columnName = $this->xlscolumn($i+1);
				$objPHPExcel->getActiveSheet()->getColumnDimension($columnName)->setWidth(16);
			}

			$objPHPExcel->getActiveSheet()->getStyle("A1:$A_max_row")->getFont()->setBold(true);

			//coloring and bordering
			$max_column_row_4 = $max_column_name . ''.($max_row_number-4);
			$A_max_row_4 = 'A'.($max_row_number-4);
			$max_column_row_3 = $max_column_name . ''.($max_row_number-3);
			$A_max_row_3 = 'A'.($max_row_number-3);
			$B_max_row_3 = 'B'.($max_row_number-3);

			$max_column_row_2 = $max_column_name . ''.($max_row_number-2);
			$B_max_row_2 = 'B'.($max_row_number-2);
			$A_max_row_2 = 'A'.($max_row_number-2);
			if($this->weighted != true){
				for($i = 4; $i < $max_row_number+1; $i++){
					$A_row = 'A'.$i;
					$column_row_maxColumn = $max_column_name.''.$i;

					$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
					$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

					if($i == $max_row_number || $i == $max_row_number -4 || $i == $max_row_number -5){
						$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
						$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
					}else if($i == 5){
						$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
					}

					if($i <= 5 || $i >= $max_row_number-3) $color = 'CCFFFF'; else $color = 'FFFFCC';
					if($i % 2 == 1){
						$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("$color");
					}
				}
					
				for($i=1; $i < $max_column_number; $i++){
					$columnName = $this->xlscolumn($i+1);
					for($j = 4; $j < $max_row_number+1; $j++){
						$cell = $columnName.''.$j;
						//echo $cell;
						if($i == 1){
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						}else if($i == $max_column_number-1){
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
						}
						else{
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						}
					}
				}
				$objPHPExcel->getActiveSheet()->getStyle("$A_max_row_1:$max_column_row_1")->getNumberFormat()->setFormatCode('#%');
				$B_max_row_1 = 'B'.($max_row_number-1);
				$objPHPExcel->getActiveSheet()->getStyle("$B_max_row_1:$max_column_row_1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
					
					
				$objPHPExcel->getActiveSheet()->mergeCells("$A_max_row_4:$max_column_row_4");
				$objPHPExcel->getActiveSheet()->getStyle("$A_max_row_4")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->getActiveSheet()->getStyle("$A_max_row_4")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$objPHPExcel->getActiveSheet()->getStyle("$A_max_row_4")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('99CCFF');
				$forecastRow = $max_row_number-4;
				$objPHPExcel->getActiveSheet()->getRowDimension("$forecastRow")->setRowHeight(33);
				$objPHPExcel->getActiveSheet()->getStyle("$A_max_row_4")->getFont()->setSize(14);
					
				$objPHPExcel->getActiveSheet()->getStyle("$B_max_row_3:$max_column_row_3")->getNumberFormat()->setFormatCode('#');
				$objPHPExcel->getActiveSheet()->getStyle("$B_max_row_3:$max_column_row_3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->getActiveSheet()->getStyle("$B_max_row_2:$max_column_row_2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->getActiveSheet()->getStyle("$B_max_row_2:$max_column_row_2")->getFont()->setBold(true);
			}else{
				for($i = 5; $i < $max_row_number+1; $i++){
					$A_row = 'A'.$i;
					$column_row_maxColumn = $max_column_name.''.$i;

					$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
					$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

					if($i == $max_row_number  || $i == $max_row_number -2 ){
						$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
						$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
					}else if($i == 6){
						$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
					}

					if($i <= 6 || $i >= $max_row_number-1) $color = 'CCFFFF'; else $color = 'FFFFCC';
					if($i % 2 == 1){
						$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("$color");
					}
				}
				for($i=1; $i < $max_column_number; $i++){
					$columnName = $this->xlscolumn($i+1);
					for($j = 4; $j < $max_row_number+1; $j++){
						$cell = $columnName.''.$j;
						//echo $cell;
						if($i == 1){
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						}else if($i == $max_column_number-1){
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
						}
						else{
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						}
					}
				}
				$objPHPExcel->getActiveSheet()->mergeCells("$A_max_row_1:$max_column_row_1");
				$objPHPExcel->getActiveSheet()->getStyle("$A_max_row_1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->getActiveSheet()->getStyle("$A_max_row_1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$objPHPExcel->getActiveSheet()->getStyle("$A_max_row_1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('99CCFF');
				$forecastRow = $max_row_number-1;
				$objPHPExcel->getActiveSheet()->getRowDimension("$forecastRow")->setRowHeight(33);
				$objPHPExcel->getActiveSheet()->getStyle("$A_max_row_1")->getFont()->setSize(14);
					
			}
		}
		// Format Sales Calendar Report
		else if($this->report_type == 'SalesCalendar'){
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
			$max_row_number = count($this->datas) + 1;
			//echo "max row number: ".$max_row_number;
			$max_column_number = count($this->datas[2]);
			for($i=1; $i <= $max_column_number; $i++){
				$columnName = $this->xlscolumn($i+1);
				$objPHPExcel->getActiveSheet()->getColumnDimension($columnName)->setWidth(12);
			}
			//$max_column_name = $this->xlscolumn(count($this->datas[0])+1);
			$max_column_name = $this->xlscolumn($max_column_number);
			$max_column1 = $max_column_name . '1';
			$max_column2 = $max_column_name . '2';
			$max_column3 = $max_column_name . '3';
			$max_column4 = $max_column_name . '4';
			$max_column_row = $max_column_name . ''.$max_row_number;
			$A_max_row = 'A'.$max_row_number;

			$objPHPExcel->getActiveSheet()->mergeCells("A1:$max_column1");
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle("A1:$max_column1")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A1:$max_column1")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A1:$max_column1")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			//$objPHPExcel->getActiveSheet()->getStyle("A2:$max_column2")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A2:$max_column2")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A2:$max_column2")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A3:$max_column3")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A3:$max_column3")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A3:$max_column3")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			
			$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight($this->sp_row_height);
			$objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setWrapText(true);

			//$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('99CCFF');

			$objPHPExcel->getActiveSheet()->mergeCells("A2:$max_column2");
			$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->mergeCells("A3:$max_column3");
			$objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->getStyle('A1:A3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('99CCFF');
			$objPHPExcel->getActiveSheet()->getStyle("A4:$max_column4")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF99');
			$objPHPExcel->getActiveSheet()->getStyle("A4:$max_column4")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A4:$max_column4")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A4:$max_column4")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("B4:$max_column4")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle("B4:$max_column4")->getFont()->setBold(true);

			$objPHPExcel->getActiveSheet()->getStyle("$A_max_row:$max_column_row")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF99');
			$objPHPExcel->getActiveSheet()->getStyle("$A_max_row:$max_column_row")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("$A_max_row:$max_column_row")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("$A_max_row:$max_column_row")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("$A_max_row:$max_column_row")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

			$objPHPExcel->getActiveSheet()->getStyle("B1:$max_column1")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("B4:$max_column_row")->getNumberFormat()->setFormatCode('$#,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle("B4:$max_column_row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("A4:$A_max_row")->getFont()->setBold(true);

			for($i = 5; $i < $max_row_number; $i++){
				$A_row = 'A'.$i;
				$column_row_maxColumn = $max_column_name.''.$i;
					
				$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
				$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
					
				if($i % 2 == 0){
					$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFFFCC');
				}
			}

			for($i=1; $i < $max_column_number; $i++){
				$columnName = $this->xlscolumn($i+1);
					
				for($j = 3; $j < $max_row_number+1; $j++){
					$cell = $columnName.''.$j;
					//echo $cell;
					if($i == 1){
						$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
						$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					}else if($i == $max_column_number-1){
						$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
					}
					else{
						$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					}
						
					if($objPHPExcel->getActiveSheet()->getCell($cell)->getValue() == 'Sub-Total'){
						$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setBold(true);
					}

				}
					
					
			}

		}
		// Format Sales Calendar Report -  oportunity detail
		else if($this->report_type == 'SalesCalendarOpp'){
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);

			$max_row_number = count($this->datas) + 1;
			//echo "max row number: ".$max_row_number;
			$max_column_number = count($this->datas[2]);
			for($i=2; $i <= $max_column_number; $i++){
				$columnName = $this->xlscolumn($i+1);
				$objPHPExcel->getActiveSheet()->getColumnDimension($columnName)->setWidth(12);
			}
			//$max_column_name = $this->xlscolumn(count($this->datas[0])+1);
			$max_column_name = $this->xlscolumn($max_column_number);
			$max_column1 = $max_column_name . '1';
			$max_column2 = $max_column_name . '2';
			$max_column3 = $max_column_name . '3';
			$max_column4 = $max_column_name . '4';
			$max_column_row = $max_column_name . ''.$max_row_number;
			$max_column_row_1 = $max_column_name . ''.($max_row_number-1);
			$A_max_row = 'A'.$max_row_number;
			$A_max_row_1 = 'A'.($max_row_number-1);
			$B_max_row = 'B'.$max_row_number;

			//$objPHPExcel->getActiveSheet()->getStyle("B4:$B_max_row")->getAlignment()->setWrapText(true);

			$objPHPExcel->getActiveSheet()->mergeCells("A1:$max_column1");
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle("A1:$max_column1")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A1:$max_column1")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A1:$max_column1")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			//$objPHPExcel->getActiveSheet()->getStyle("A2:$max_column2")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A2:$max_column2")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A2:$max_column2")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

			$objPHPExcel->getActiveSheet()->getStyle("A3:$max_column3")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A3:$max_column3")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A3:$max_column3")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			
			$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight($this->sp_row_height);
			$objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setWrapText(true);

			//$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('99CCFF');

			$objPHPExcel->getActiveSheet()->mergeCells("A2:$max_column2");
			$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->mergeCells("A3:$max_column3");
			$objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->getStyle('A1:A3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFCC99');
			$objPHPExcel->getActiveSheet()->getStyle("A4:$max_column4")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF99');
			$objPHPExcel->getActiveSheet()->getStyle("A4:$max_column4")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A4:$max_column4")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A4:$max_column4")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("B4:$max_column4")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle("A4:$max_column4")->getFont()->setBold(true);

			$objPHPExcel->getActiveSheet()->getStyle("$A_max_row:$max_column_row")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF99');
			$objPHPExcel->getActiveSheet()->getStyle("$A_max_row:$max_column_row")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("$A_max_row:$max_column_row")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("$A_max_row:$max_column_row")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("$A_max_row:$max_column_row")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

			//$objPHPExcel->getActiveSheet()->getStyle("$A_max_row_1:$max_column_row_1")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

			$objPHPExcel->getActiveSheet()->getStyle("B1:$max_column1")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("C5:$max_column_row")->getNumberFormat()->setFormatCode('$#,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle("C5:$max_column_row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()->getStyle("A5:$max_column_row")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("A5:$A_max_row")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("$A_max_row:$max_column_row")->getFont()->setBold(true);

			for($i = 5; $i < $max_row_number; $i++){
				$A_row = 'A'.$i;
				$column_row_maxColumn = $max_column_name.''.$i;
					
				$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
				$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
					
				if($i % 2 == 0){
					$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFFFCC');
				}
			}

			for($i=1; $i < $max_column_number; $i++){
				$columnName = $this->xlscolumn($i+1);
				for($j = 4; $j < $max_row_number+1; $j++){
					$cell = $columnName.''.$j;
					//echo $cell;
					/*
					 if($i == 1 || $i == 2){
					 $objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
					 $objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					 }else if($i == $max_column_number-1){
					 $objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
					 $objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
					 }
					 else{
					 $objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					 $objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					 }
					 */
					$cell_A = 'A'.$j;
					$cell_B = 'B'.$j;
					//echo $cell;
					if($objPHPExcel->getActiveSheet()->getCell("$cell_B")->getValue() != '' ){
						if($i == 1 || $i == 2){
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						}else if($i == $max_column_number-1){
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
						}
						else{
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						}
					}
					else{
						$objPHPExcel->getActiveSheet()->getStyle("$cell_A")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
						$objPHPExcel->getActiveSheet()->getStyle("$cell_A")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
						$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
						$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
					}

					if($objPHPExcel->getActiveSheet()->getCell($cell)->getValue() == 'Sub-Total'){
						$objPHPExcel->getActiveSheet()->getStyle($cell)->getFont()->setBold(true);
					}
				}
					
			}
			//$objPHPExcel->getActiveSheet()->getStyle("$A_max_row_1:$max_column_row_1")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

		}// Format Sales Calendar Report -  product detail
		else if($this->report_type == 'SalesCalendarProd'){
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);

			$max_row_number = count($this->datas) + 1;
			//echo "max row number: ".$max_row_number;
			$max_column_number = count($this->datas[2]);
			for($i=3; $i <= $max_column_number; $i++){
				$columnName = $this->xlscolumn($i+1);
				$objPHPExcel->getActiveSheet()->getColumnDimension($columnName)->setWidth(12);
			}
			//$max_column_name = $this->xlscolumn(count($this->datas[0])+1);
			$max_column_name = $this->xlscolumn($max_column_number);
			$max_column1 = $max_column_name . '1';
			$max_column2 = $max_column_name . '2';
			$max_column3 = $max_column_name . '3';
			$max_column4 = $max_column_name . '4';
			$max_column_row = $max_column_name . ''.$max_row_number;
			$max_column_row_1 = $max_column_name . ''.($max_row_number-1);
			$A_max_row = 'A'.$max_row_number;
			$A_max_row_1 = 'A'.($max_row_number-1);
			$B_max_row = 'B'.$max_row_number;
			$C_max_row = 'C'.$max_row_number;

			//$objPHPExcel->getActiveSheet()->getStyle("A4:$A_max_row")->getAlignment()->setWrapText(true);
			//$objPHPExcel->getActiveSheet()->getStyle("B4:$B_max_row")->getAlignment()->setWrapText(true);
			//$objPHPExcel->getActiveSheet()->getStyle("C4:$C_max_row")->getAlignment()->setWrapText(true);

			$objPHPExcel->getActiveSheet()->mergeCells("A1:$max_column1");
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle("A1:$max_column1")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A1:$max_column1")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A1:$max_column1")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			//$objPHPExcel->getActiveSheet()->getStyle("A2:$max_column2")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A2:$max_column2")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A2:$max_column2")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

			$objPHPExcel->getActiveSheet()->getStyle("A3:$max_column3")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A3:$max_column3")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A3:$max_column3")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

			//$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('99CCFF');

			$objPHPExcel->getActiveSheet()->mergeCells("A2:$max_column2");
			$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->mergeCells("A3:$max_column3");
			$objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight($this->sp_row_height);
			$objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setWrapText(true);

			$objPHPExcel->getActiveSheet()->getStyle('A1:A3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFCC99');
			$objPHPExcel->getActiveSheet()->getStyle("A4:$max_column4")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF99');
			$objPHPExcel->getActiveSheet()->getStyle("A4:$max_column4")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A4:$max_column4")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A4:$max_column4")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("A4:$max_column4")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle("A4:$max_column4")->getFont()->setBold(true);

			$objPHPExcel->getActiveSheet()->getStyle("$A_max_row:$max_column_row")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF99');
			$objPHPExcel->getActiveSheet()->getStyle("$A_max_row:$max_column_row")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("$A_max_row:$max_column_row")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("$A_max_row:$max_column_row")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
			$objPHPExcel->getActiveSheet()->getStyle("$A_max_row:$max_column_row")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

			//$objPHPExcel->getActiveSheet()->getStyle("$A_max_row_1:$max_column_row_1")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

			$objPHPExcel->getActiveSheet()->getStyle("B1:$max_column1")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("D5:$max_column_row")->getNumberFormat()->setFormatCode('$#,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle("D5:$max_column_row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()->getStyle("A5:$max_column_row")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("A4:$A_max_row")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("$A_max_row:$max_column_row")->getFont()->setBold(true);
			for($i = 5; $i < $max_row_number; $i++){
				$A_row = 'A'.$i;
				$column_row_maxColumn = $max_column_name.''.$i;
					
				$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
				$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
					
				if($i % 2 == 0){
					$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFFFCC');
				}
					
				if($objPHPExcel->getActiveSheet()->getCell("$A_row")->getValue() == 'Total'){
					$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
					$objPHPExcel->getActiveSheet()->getStyle("$A_row:$column_row_maxColumn")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
				}
			}

			for($i=1; $i < $max_column_number; $i++){
				$columnName = $this->xlscolumn($i+1);
				for($j = 4; $j < $max_row_number+1; $j++){
					$cell = $columnName.''.$j;
					$cell_A = 'A'.$j;
					$cell_B = 'B'.$j;
					//echo $cell;
					if($objPHPExcel->getActiveSheet()->getCell("$cell_A")->getValue() != '' || $objPHPExcel->getActiveSheet()->getCell("$cell_B")->getValue() != '' ){
						if($i == 1 || $i == 2 || $i == 3){
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						}else if($i == $max_column_number-1){
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
						}
						else{
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						}
					}
					else{
						$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
						$objPHPExcel->getActiveSheet()->getStyle("$cell_A")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
						$objPHPExcel->getActiveSheet()->getStyle("$cell_A")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
						$objPHPExcel->getActiveSheet()->getStyle("$cell")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
					}

				}
			}
			//$objPHPExcel->getActiveSheet()->getStyle("$A_max_row_1:$max_column_row_1")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

		}

		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath('../../www/images/jetstream_logo_new.jpg');
		//$objDrawing->setHeight(20);
		$objDrawing->setWidth(180);
		$objDrawing->setCoordinates('A1');
		$objDrawing->setOffsetX(4);
		$objDrawing->setOffsetY(4);
		$objDrawing->getShadow()->setVisible(true);
		$objDrawing->getShadow()->setDirection(45);

		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());


		// Rename sheet
		$objPHPExcel->getActiveSheet()->setTitle('Sales Report');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');

		if (!empty($this->filename))header('Content-Disposition: attachment;filename="'.$this->filename[0].'.xls"');
		else header('Content-Disposition: attachment;filename="jetstream.xls"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
			
		exit;
	}
}