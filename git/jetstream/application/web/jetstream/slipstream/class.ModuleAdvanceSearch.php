<?php
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.ModuleSaveAdvanceSearch.php';
require_once BASE_PATH . '/include/class.fieldsMap.php';
require_once BASE_PATH . '/include/class.OptionLookup.php';
require_once BASE_PATH . '/include/class.MapLookup.php';
require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';
require_once BASE_PATH . '/include/class.DebugUtil.php';
require_once BASE_PATH . '/include/class.FormUtil.php';

class ModuleAdvanceSearch extends JetstreamModule {
	
	public $title = 'Super Advance Search Records';
	public $sidebar = false;
	public $filter_list;
	public $company_id;
	public $user_id;
	public $layouts; 
	public $saveSearchID = 0; 
	public $saveSearchName = ''; 
	public $lastSearch = array (); 
	public $dispDiv = array ();
	public $records_per_page = array(25, 50, 75, 100);
	public $search_types = array(3 => 'Company & Contact', 1 => 'Company', 2 => 'Contact');
	public $search_results ; 
	public $limitAccess;
	public $assignto;
	public $query; 
	public $search; 
	private $dashboard;
	public $created;
	public $allowExport;

	function __construct($company_id = -1, $user_id = -1) {
		$this->company_id = ($company_id == -1) ? (int) $_SESSION['USER']['COMPANYID'] : (int) $company_id;
		$this->user_id = ($user_id == -1) ? (int) $_SESSION['USER']['USERID'] : (int) $user_id;
		$this->limitAccess = $this->GetLimitAccess(); 
	}
	
	public function Init() {
		
		if ($this->saveSearchID){
			$saveValues = $this->getSavedFilter($this->saveSearchID, 1);
			$_SESSION['LastSearch'] = $saveValues;
		}
		
		if(!empty($_SESSION['LastSearch'])) {
			$module = new Dashboard();
			$this->lastSearch = $_SESSION['LastSearch']['advanced_filters']; 
			$this->search_results = $module->Build2($_SESSION['LastSearch']); 
			$this->assignto = $_SESSION['LastSearch']['assign'];
			$this->query = $_SESSION['LastSearch']['query_type'];
			$this->created = $_SESSION['LastSearch']['created'];
			$this->search['page_no'] = $_SESSION['LastSearch']['page_no'];
			$this->search['records_per_page'] = $_SESSION['LastSearch']['records_per_page'];
			$this->search['inactive_type'] = $_SESSION['LastSearch']['inactive_type'];
		}
		
		$this->dashboard = (isset($module)) ? $module : '';
		$this->Build(); 
	}

/* data return array is  
 * $searchLayouts['account'][sectionName][rowNumber][position]( array [labelName], [fieldName], [select], [value] ) 
 *	Array ( [account] => 
 *		Array ( [DefaultSection] => 
 *			Array ( [0] => Array ( 
 *						[1] => Array ( [labelName] => Company Name [fieldName] => Text01 [select] => [value] => SomeValue ) 
 *						[0] => Array ( [labelName] => Main Phone [fieldName] => Text07 [select] => [value] => SomeValue ) ) 
 *					[1] => Array ( 
 *						[1] => Array ( [labelName] => Address 1 [fieldName] => Text02 [select] => [value] => SomeValue ) 
 *						[0] => Array ( [labelName] => Toll Free Telephone [fieldName] => Text16 [select] => [value] => SomeValue ) ) 
 * also populates the dispDiv array to determine whhether it is hidden show or which image  sample below 
 * Array (  [account] => defaultSection => array( img => plus.gif , stats => hidden ) ) 
 */
	public function Build() { // build the array of information needed 

		$this->allowExport = $_SESSION['USER']['ALLOW_EXPORT'] ? true : false;
		
		$searchLayouts = array(); 
		$display = array(); 

		// check for any values that are passed in either from the cookie or from setting saveSearchId 
		if (isset($this->saveSearchID) && $this->saveSearchID != 0){// grab save search criteria
			$saveValues = $this->getSavedFilter($this->saveSearchID, 1);
		}
		else if(count($this->lastSearch > 0)){
			$saveValues = $this->lastSearch; 
		}
		else {
			$saveValues = array(); 
		}
		
		$ContactLayouts = MapLookup::GetContactLayout($this->company_id);
		$AccountLayouts = MapLookup::GetAccountLayout($this->company_id);
		$ContactMaps = MapLookup::GetContactMap($this->company_id);
		$AccountMaps = MapLookup::GetAccountMap($this->company_id);
		
		foreach($AccountLayouts as &$AccountLayout){
			$show = 0; 
			$lines = array (); 
			foreach($AccountLayout['rows'] as &$row){
				$line = array();
				foreach(array(1,0) as $position){
					
					if(isset($row[$position]) && !empty($row[$position])){
						
						$line[$position] = array (
													'labelName' => $AccountMaps['fields'][$row[$position]]['LabelName'],
													'fieldName' => $AccountMaps['fields'][$row[$position]]['FieldName'],
													'params' => $AccountMaps['fields'][$row[$position]]['Parameters']
						); 
						
						if(!isset($saveValues['company'][$AccountMaps['fields'][$row[$position]]['FieldName']])){
							$saveValues['company'][$AccountMaps['fields'][$row[$position]]['FieldName']] = ''; 
						}
						$line[$position]['select'] = ( preg_match("/^Select/", $AccountMaps['fields'][$row[$position]]['FieldName'])) ? $this->getSelectHTML($AccountMaps['fields'][$row[$position]]['OptionSetID'], $saveValues['company'][$AccountMaps['fields'][$row[$position]]['FieldName']]) :  false; 						 
						
						if($this->saveSearchID > 0){
							$line[$position]['value'] = $saveValues['account'][$AccountMaps['fields'][$row[$position]]['FieldName']]; 
						}
						else if(count($this->lastSearch) > 0){
							
							$line[$position]['value'] = ( !empty($this->lastSearch['company'][$AccountMaps['fields'][$row[$position]]['FieldName']])) ?  $this->lastSearch['company'][$AccountMaps['fields'][$row[$position]]['FieldName']] : '';  
						}
						else {
							$line[$position]['value'] = ''; 
						}
						$show = (!empty($line[$position]['value']) ) ? ($show+1) : $show; 
					}
				}
				$lines[] = $line; 
			}
			$searchLayouts['company'][$AccountLayout['SectionName']] = $lines;
			$display['company'][$AccountLayout['SectionName']] = $show; 
		}
		foreach($ContactLayouts as &$ContactLayout) {
			$show = 0;
			$lines = array(); 
			foreach($ContactLayout['rows'] as &$row) {
				$line = array(); 
				foreach(array(1,0) as $position){
					if(isset($row[$position]) && !empty($row[$position])){
						
						$line[$position] = array (
												'labelName' => $ContactMaps['fields'][$row[$position]]['LabelName'],
												'fieldName' => $ContactMaps['fields'][$row[$position]]['FieldName'],
												'params' => $ContactMaps['fields'][$row[$position]]['Parameters']
						); 
						if(!isset($saveValues['contact'][$ContactMaps['fields'][$row[$position]]['FieldName']])){
							$saveValues['contact'][$ContactMaps['fields'][$row[$position]]['FieldName']] = ''; 
						}
						$line[$position]['select'] = ( preg_match ("/^Select/", $ContactMaps['fields'][$row[$position]]['FieldName'])) ? $this->getSelectHTML($ContactMaps['fields'][$row[$position]]['OptionSetID'], $saveValues['contact'][$ContactMaps['fields'][$row[$position]]['FieldName']]) :  false; 
						$line[$position]['value'] =	(($this->saveSearchID > 0) || count($this->lastSearch) > 0  ) ? $saveValues['contact'][$ContactMaps['fields'][$row[$position]]['FieldName']]: '';
						
						$show = (!empty ($saveValues['contact'][$ContactMaps['fields'][$row[$position]]['FieldName']]) ) ? ($show+1) : $show; 
					}
				}
				$lines[] = $line; 
			}
			$searchLayouts['contact'][$ContactLayout['SectionName']] = $lines;
			$display['contact'][$ContactLayout['SectionName']] = $show; 
		}
		
		if(isset($saveValues['created'])){
			$this->created['value'] = $saveValues['created']['Date'];
		}
		
		$this->layouts = $searchLayouts; 
		$this->dispDiv = $display;
		
		$this->created['fieldName'] = 'Date';
		$this->created['labelName'] = 'Original Creation Date';
	}

	public function	getSavedFilter($saveSearchID, $type=0){
	
		$raw_array = ModuleSaveAdvanceSearch::getSavedSearch($saveSearchID); 
		
		if(!$raw_array){
			return array();  
		}
		if($type == 1){

			$saved = array();
			$saved['advanced_filters'] = array('company' =>  json_decode($raw_array['Account'], true),
												'contact' => json_decode($raw_array['Contact'],true));
			
			$options = json_decode($raw_array['Options'], true);
			$saved['page_no'] = $options['page_no'] == '' ? 1 : $options['page_no'];
			$saved['records_per_page'] = $options['RecordsPerPage'];
			$saved['assigned_search'] = $options['AssignedSearch'];
			$saved['query_type'] = $options['query_type'] == null ? 3 : $options['query_type'];
			$saved['search_string'] = ''; 
			$saved['inactive_type'] = $options['SelectInactive'] == '' ? 0 : $options['SelectIntactive'];
			$saved['assign'] = $options['assignto'];
			$saved['sort_type'] = $options['sort_type'];
			$saved['Advance'] = 1;
			$saved['inclContacts'] = '';
			
		}
		else {
			$options = json_decode($raw_array['Options'], true); 
			$saved['page_no'] = 1;
			$saved['records_per_page'] = 25;
			$saved['assigned_search'] = 0;
			$saved['query_type'] = 3; 
			$saved['search_string'] = ''; 
			$saved['inactive_type'] = 0;
			$saved['assign'] = '';
			$saved['sort_type'] = 3; 
			$saved['Advance'] = 1;
			$saved['inclContacts'] = '';
			$saved['advanced_filters'] = array('company' =>  json_decode($raw_array['Account'], true),
												'contact' => json_decode($raw_array['Contact'],true));
			$saved['sort_dir'] = 'ASC';
		}
		return $saved;  
	}

	public function getSelectHTML($OptionSetID, $selected){
		$data = OptionLookUp::GetOptionset($this->company_id, $OptionSetID);
		$options = FormUtil::sortOptions($data, $OptionSetID);
		
		$output = "<option></option>"; 
		foreach($options as $key => $value){
			if(is_array($selected)){
				$select = (in_array($key, $selected)) ? 'selected' : ''; 
			}
			else {
				$select = (($selected == $key)) ? 'selected' : ''; 
			}
			$output .= "<option value=\"$key\"". $select ." >". $value ."</option>"; 
		}
		
		return $output; 
	}
	
	private function createSelectMonth($name, $select = ''){

		if ($name == '') return "ERROR: no name"; 
		
		$output = '<select name="' . $name . '" ><option></option>';
		for($month = 1; $month < 13; $month++){
			$selected = ($month == $select) ? 'selected' : '';	
			$output .= "<option value=" . $month . " " . $selected . ">" . substr((date('F', mktime(0, 0, 0, $month, 1))), 0,3) . "</option>";
		}
		return $output . '</select>'; 
	}

	private function createSelectDay($name, $select = '' ){
		if ($name == '') return "ERROR: no name"; 
		
		$output = '<select name="' . $name . '" ><option></option>';
		for($day = 1; $day <= 31; $day++) {
			$selected = ($day == $select) ? 'selected' : '';	
			$output .= "<option value=" . $day . " " . $selected . ">" . $day . "</option>";
		}
		return $output . '</select>'; 
	
	}
	
	public function printForm($form, $division, $value) {
		
		if(count($form) < 1) return; 
		
		$output = ''; 
		
		if(isset($form['select']) && $form['select'] != ''){
			
			$empty = "<option value='EMPTY'> Empty </option>";
			$output.= " <select id=".$division .'_'.$form['fieldName']. " name=".$division .'_'.$form['fieldName']. "[] multiple size=10>". $empty . $form['select'] .  "</select>" ;
			
			$output .=  '<script type="text/javascript">
							$(document).ready( function() {
								$("#'.$division .'_'.$form['fieldName'].'").multiSelect({ oneOrMoreSelected: \'*\' });
							});	
						</script>';  

			return $output;
		}
		switch (substr($form['fieldName'], 0, 4)) {
			
			case 'Date' :
				
				$params = ($form['params'] != null) ? explode("|", $form['params']) : array('','');
				
				/*
				 * Dates will be in the form:
				 * 1. YYYY-MM-DD
				 * 2. Array(
				 * 		start:month => 1,
				 * 		start:day => 1,
				 * 		end:month => 1,
				 * 		end:day => 1
				 * 	);
				 */
								
				if ($params[6] == 1){//?!!!WTF (I'm guessing this means month/day range?)

					$output .= $this->createSelectMonth($division .'_start:month_'.$form['fieldName'], $value['start:month']);
					$output .= $this->createSelectDay($division .'_start:day_'.$form['fieldName'], $value['start:day']);
					
					$output .= ' to '. $this->createSelectMonth($division .'_end:month_'.$form['fieldName'], $value['end:month']);
					$output .= $this->createSelectDay($division .'_end:day_'.$form['fieldName'], $value['end:day']);
				}
				else {
					$output .= '<input type="text" size="8" id="' .$division ."_start_" . $form['fieldName'] . '" name="' .$division ."_start_" . $form['fieldName'] . '" Range="' . $year_range . '" value="' . ReformatDate($value['start']) . '"size=6 />';
					$output .= ' to <input type="text" size="8" id="' .$division ."_end_" . $form['fieldName'] . '" name="' .$division ."_end_" . $form['fieldName'] . '" Range="' . $year_range . '" value="' . ReformatDate($value['end']) . '"size=6 />';
					
					$output .= "
						<script type=\"text/javascript\">
						$(document).ready(function() {
							$(\"#" .$division ."_end_" . $form['fieldName'] . "\").datepicker( {
								showOn :'both',
								buttonImage :'./images/cal.jpg',
								buttonImageOnly :true
							});
							$(\"#" .$division ."_start_" . $form['fieldName'] . "\").datepicker( {
								showOn :'both',
								buttonImage :'./images/cal.jpg',
								buttonImageOnly :true
							});
						});
					</script>";
				}
				break; 
			case 'Bool' : 
				$output .= '<select name=' . $division ."_" . $form['fieldName'] .">";
				$output .= '<option value=""></option>';
				$output .= '<option value="1" '; 
				$output .= ($value == '1') ? 'checked ' : ''; 
				$output .= '>Checked</option>';
				$output .= '<option value="0" ';
				$output .= ($value == '0') ? ' ' : ''; 
				$output .= '>Not Checked</option>';
				$output .= '</select>';
				break;
			case 'Numb' : 
				$output .= "between <input type=text name=" . $division . '_' . $form['fieldName'] .'-start value="' .$value.'" onkeypress="javascript:return numbersonly(this, event)">';
				$output .= " and <input type=text name=" . $division . '_' . $form['fieldName'] .'-end value="' .$value.'" onkeypress="javascript:return numbersonly(this, event)">';
				break;
			default :
				$output .= '<input type="text" name="' . $division . '_' . $form['fieldName'] .'" value="' .$value.'" />';
				break;
		}
	
		return $output;
	
	}
	
	private function FormatJsYearRng($start_year, $end_year) {
		if ($start_year == '' || $end_year == '') return FALSE;
		
		if ($start_year == 'CurrentYear') $start_year = date('Y');
		
		if ($end_year == 'CurrentYear') $end_year = date('Y');
		
		if ($end_year < $start_year) return FALSE;
		
		$year_range = ($start_year - date('Y')) . ':' . ($end_year - date('Y'));
		
		return $year_range;
	}
	
	public function SearchResultsInfo(){
		if(!empty($this->dashboard))	return $this->dashboard->SearchResultsInfo(); 
		else return; 
	}
	
	public function getDashboardPages(){
		if(!empty($this->dashboard))	return $this->dashboard->getDashboardPages(); 		
		else return;
	}
	
	public function createAssignList($type, $default_value=0){

		if($default_value != 0){
			$lists = ReportingTreeLookup::GetActiveLimb($type, $default_value);
		}
		else if($type == 'Active'){
			$lists = ReportingTreeLookup::CleanGetLimb($this->company_id, -2);
		}
		else {
			$lists = ReportingTreeLookup::CleanGetLimb($this->company_id, -1);
		}
		
		$output = '';
		$lists = ReportingTreeLookup::SortPeople($this->company_id, $lists, 'FirstName' , $reverse = false); 

		if(count($lists) == 0) return;
		
		foreach($lists as $v) {
			
			$value = ReportingTreeLookup::GetRecord($this->company_id, $v);
			
			if($value['PersonID'] != 0){
				$selected = ($value['PersonID'] == $_SESSION['LastSearch']['assign']) ? 'selected' : '';
				//$selected = ($value['PersonID'] == $default_value) ? 'selected' : '';
				$output .= "<option value='{$value['PersonID']}' $selected>{$value['FirstName']} {$value['LastName']}</option>";	
			}
		}
		return $output; 	
	}


// returns an image plus.gif or minus.gif .. AND it also returns hidden or show  
 
	public function Display_Div($division, $section, $image){
		$total = 0;
		$hidden = 0; 
		if ($section == '' ){
			foreach( $this->dispDiv[$division] as $key =>$value ){
				$total = $value + $total; 
			} 
			$hidden = ( $total > 0 ) ? 1 : 0;
		}
		else{
			$hidden = ($this->dispDiv[$division][$section] > 0 ) ? 1 : 0; 
		}		
		
		if ( $image == 1 ) return ( $hidden > 0) ? 'minus.gif' :'plus.gif'; 
		else return ($hidden > 0 ) ? 'block' : 'hidden';
		
	}

	function GetLimitAccess(){
		
		if($_SESSION['USER']['SPECIAL_PERMISSIONS_ID'] > 0){
			return false;
		}
		
		$sql = "SELECT LimitAccessToUnassignedUsers, supervisorid FROM company, people WHERE company.CompanyID = ? and people.PersonID = ?"; 
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql( array(DTYPE_INT, $this->company_id), array(DTYPE_INT, $this->user_id) );
		
		$list = DbConnManager::GetDb('mpower')->Exec($sql);
		if ( $list[0]['LimitAccessToUnassignedUsers'] == 1 ){
			return 1; 
		}
		else {
			return 0; 
		}
	
	}
}
?>
