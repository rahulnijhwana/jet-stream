<?php
/**
 * include dashboard class to create the filter array
 */
//require_once BASE_PATH . '/slipstream/class.Note.php';
require_once BASE_PATH . '/include/class.fieldsMap.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/include/class.OptionLookup.php';
require_once BASE_PATH . '/include/class.MapLookup.php';
require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';

/**
 * Class for Advace Search Filter module
 */
class ModuleAdvanceSearchFilter extends JetstreamModule
{
	public $title = 'Filter Search Results';
	public $sidebar = true;
	public $filter_list;
	public $company_id;
	public $user_id;

	/**
	 * Constructor of the Advace Search Filter class.
	 */
	function __construct($company_id = -1, $user_id = -1) {
		$this->company_id = ($company_id == -1) ? (int) $_SESSION['USER']['COMPANYID'] : (int) $company_id;
		$this->user_id = ($user_id == -1) ? (int) $_SESSION['USER']['USERID'] : (int) $user_id;
	}

	public function Init() {
		$this->filter_list = $this->CreateFilterArray();

		$this->AddTemplate('module_advance_search_filter.tpl');
	
	}

	function CreateFilterArray() {
		$filter_array = array();
		
		
		$account_layout = MapLookup::GetAccountLayout($this->company_id);
		$account_map = MapLookup::GetAccountMap($this->company_id, 'SectionID,TabOrder');
	
		foreach($account_map['fields'] as $field_id => $field) {
			if ($account_layout[$field['SectionID']]['SectionName'] == 'DefaultSection') {
				$section_id = 0;
			} else {
				$section_id = $field['SectionID'];
			}
			
			$filter_array['account'][$section_id]['SectionName'] = $account_layout[$field['SectionID']]['SectionName'];
			$filter_array['account'][$section_id]['fields'][$field['FieldName']] = $field['LabelName'];
		}
		
		$contact_layout = MapLookup::GetContactLayout($this->company_id);
		$contact_map = MapLookup::GetContactMap($this->company_id, 'SectionID,TabOrder');
		foreach($contact_map['fields'] as $field_id => $field) {
			if ($contact_layout[$field['SectionID']]['SectionName'] == 'DefaultSection') {
				$section_id = 0;
			} else {
				$section_id = $field['SectionID'];
			}
			
			$filter_array['contact'][$section_id]['SectionName'] = $contact_layout[$field['SectionID']]['SectionName'];
			$filter_array['contact'][$section_id]['fields'][$field['FieldName']] = $field['LabelName'];
		}
	
		return $filter_array;
	}

	
	public function CreateFilterEntry($field_from_form, $form_id, $default_value = false) {
		
		list($type, $field_name) = split('_', $field_from_form);
		$form_input_name = "advfilterinput_{$type}_{$field_name}";
		
		if ($field_name == 'Id'){
			//$target = ReportingTreeLookup::CleanGetLimb($this->company_id, -2); 
				
			if ( $type == 'Active' ) $lists = ReportingTreeLookup::CleanGetLimb($this->company_id, -2);
			else $lists = ReportingTreeLookup::CleanGetLimb($this->company_id, -1);
			$output = "<select onChange='javascript:callDashBoardSearch();' class='clsTextBox' name='$form_input_name' >";
			$output .= '<option value="" >&nbsp;</option>';
			$lists = ReportingTreeLookup::SortPeople($this->company_id, $lists, 'FirstName' , $reverse = FALSE) ; 

			if ( count($lists) == 0 ) return $output. '<option value="" > -- </option></select>';
			foreach ($lists as $v) {
				$value = ReportingTreeLookup::GetRecord($this->company_id, $v);
				$selected = ($value['PersonID'] == $default_value) ? 'selected' : '';
				$output .= "<option value='{$value['PersonID']}' $selected>{$value['FirstName']} {$value['LastName']}</option>";
			}
			$output .= '</select>';
			
		
			return $output ; 
		}
		
		switch (strtolower(substr($field_name, 0, 4))) {
			case 'date' :
				$output  = "<input onKeyUp='javascript:callDashBoardSearch();' onChange='javascript:callDashBoardSearch();' class='clsTextBox' size='10' name='{$form_input_name}_StartDate' type='text' />";
				$output .= ' to ';
				$output .= "<input onKeyUp='javascript:callDashBoardSearch();' onChange='javascript:callDashBoardSearch();' class='clsTextBox' size='10' name='{$form_input_name}_EndDate'   type='text' />";
				break;
			
			case 'sele' :
				if ($type == 'account') {
					$map = MapLookup::GetAccountMap($this->company_id);
				} else {
					$map = MapLookup::GetContactMap($this->company_id);
				}
				
				foreach($map['fields'] as $field) {
					if ($field['FieldName'] == $field_name)	{
						$field_def = $field;
						break;
					}
				}
				$option_list = OptionLookup::GetOptionset($this->company_id, $field_def['OptionSetID']);
	
				$output = "<select onChange='javascript:callDashBoardSearch();' class='clsTextBox' name='$form_input_name'>";
				$output .= '<option value=""></option>';
				foreach ($option_list as $option) {
					$selected = ($option['OptionID'] == $default_value) ? 'selected' : '';
					$output .= "<option value='{$option['OptionID']}' $selected>{$option['OptionName']}</option>";
				}
				$output .= '</select>';
				break;
			
			case 'bool' :
				$output = "<select onChange='javascript:callDashBoardSearch();' class='clsTextBox' name='$form_input_name'>";
				$output .= '<option value=""></option>';
				$output .= '<option value="1">True</option>';
				$output .= '<option value="0">False</option>';
				$output .= '</select>';
				break;
			
			default :
				$output = "<input onKeyUp='javascript:callDashBoardSearch();' type='text' name='$form_input_name' value='$default_value' class='clsTextBox' />";
		}
		return $output;
	}
	
	private function getFilterOptionList($mapId, $recType) {
		$searchField = ($recType == 'CN') ? 'ContactMapID' : 'AccountMapID';
		
		$option_list = array();
		foreach ($this->option_set as $option) {
			if ($option[$searchField] == $mapId) {
				$option_list[] = $option;
			}
		}
		
		return $option_list;
	}
}
?>