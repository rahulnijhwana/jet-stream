<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.MpRecord.php';
// require_once BASE_PATH . '/include/mpconstants.php';

/**
 * The DB fields initialization
 */
class EventInfo extends MpRecord
{

	public function Initialize() {
		$this->db_table = 'Events';
		$this->recordset->GetFieldDef('EventID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('UserID')->required = TRUE;
		$this->recordset->GetFieldDef('Subject')->required = TRUE;
		
		parent::Initialize();
	}

	public function Save($simulate = false) {
		parent::Save($simulate);
	}

}

?>