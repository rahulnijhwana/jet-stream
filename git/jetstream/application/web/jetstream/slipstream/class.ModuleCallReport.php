<?php
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.Event.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.JetDateTime.php';
require_once BASE_PATH . '/include/class.fieldsMap.php';
require_once BASE_PATH . '/include/class.ReportingTree.php';
require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';
require_once BASE_PATH . '/include/class.DebugUtil.php';
require_once BASE_PATH . '/include/class.AccountUtil.php';
require_once BASE_PATH . '/include/class.EventUtil.php';
require_once BASE_PATH . '/include/class.UserUtil.php';
require_once BASE_PATH . '/include/class.ContactUtil.php';

class ModuleCallReport extends JetstreamModule
{
	public $title = 'Event Call Report';
	protected $javascript_files = array('jquery.js','call_report.js','jquery.tablesorter.js', 'ui.datepicker.js');
	protected $css_files = array('table_sorter.css', 'ui.datepicker.css', 'call_report.css');
	public $sidebar = FALSE;
	public $sales_people = array();
	public $sales_people_ids = array();
	public $sel_report_user = '';
	public $show_user_in_report = false;
	public $report_event_ids = array();
	
	public function InitAjax() {
		$this->sidebar = False;
	}
	
	public function getUsersArr() {		
		// Storing the permitted users in the session saves a DB call and the user sorting
		if (isset($_SESSION['call_report_users'])) {
			$call_users = $_SESSION['call_report_users'];
		}
		else {
			// Get and sort the IDs for the people this user can view on the cal
			// This may be too much - includes all descendants (GetLimb) for the current user
			// May need to be updated to only children later
			$call_users = array_unique(ReportingTreeLookup::CleanGetLimb($_SESSION['company_id'], $_SESSION['login_id']));
			$call_users = ReportingTreeLookup::SortPeople($_SESSION['company_id'], $call_users, 'LastName');
			$_SESSION['cal_permitted_users'] = $call_users;
		}
		$this->sales_people_ids = $call_users;
		// Get the full person records from the reporting tree
		$this->sales_people = ReportingTreeLookup::GetRecords($_SESSION['company_id'], $call_users);
	}
	public function Init() {	
		$this->getUsersArr();
		
		//if(isset($_SESSION['USER']['ISSALESPERSON']) && ($_SESSION['USER']['ISSALESPERSON'] == 1)) {
		//	$this->show_user_in_report = false;
		//}
		//else {
			$this->show_user_in_report = true;
		//}
		
		
		$this->today = date('m/d/Y H:i:s', mktime(0, 0, 0, date('m'), date('d'), date('Y')));
		$this->end_of_today = date('m/d/Y H:i:s', mktime(0, 0, -1, date('m'), date('d') + 1, date('Y')));
		$this->display_today = substr($this->today, 0, 10);
		
		$this->yesterday = date('m/d/Y H:i:s', mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')));
		$this->end_of_yesterday = date('m/d/Y H:i:s', mktime(0, 0, -1, date('m'), date('d'), date('Y')));
		$this->display_yesterday = substr($this->yesterday, 0, 10);
		
		//$this->tomorrow = date('m/d/Y', mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')));
		//$this->end_of_tomorrow = date('m/d/Y H:i:s', mktime(0, 0, -1, date('m'), date('d'), date('Y')));
		
		$week_date = date('d') - date('w');
		$week_mon = date('m');
		$week_year = date('Y');
		
		$this->cur_week_start = date('m/d/Y', mktime(0, 0, 0, $week_mon, $week_date, $week_year));
		$this->cur_week_end = date('m/d/Y', mktime(0, 0, 0, $week_mon, $week_date + 6, $week_year));
		//echo $this->cur_week_start.'---->'.$this->cur_week_end.'<br>';
		
		$this->next_week_start = date('m/d/Y', mktime(0, 0, 0, $week_mon, $week_date + 7, $week_year));
		$this->next_week_end = date('m/d/Y', mktime(0, 0, 0, $week_mon, $week_date + 13, $week_year));
		//echo $this->next_week_start.'---->'.$this->next_week_end.'<br>';
		
		$this->prev_week_start = date('m/d/Y', mktime(0, 0, 0, $week_mon, $week_date - 7, $week_year));
		$this->prev_week_end = date('m/d/Y', mktime(0, 0, 0, $week_mon, $week_date - 1, $week_year));
		//echo $this->prev_week_start.'---->'.$this->prev_week_end.'<br>';
		
		$mon_date = 1;
		$mon_mon = date('m');
		$mon_year = date('Y');
		$this->cur_mon_start = date('m/d/Y', mktime(0, 0, 0, $mon_mon, $mon_date, $mon_year));
		$this->cur_mon_end = date('m/d/Y', mktime(0, 0, 0, $mon_mon + 1, $mon_date - 1, $mon_year));
		//echo $this->cur_mon_start.'--->'.$this->cur_mon_end.'<br>';

		$this->prev_mon_start = date('m/d/Y', mktime(0, 0, 0, $mon_mon - 1, $mon_date, $mon_year));
		$this->prev_mon_end = date('m/d/Y', mktime(0, 0, 0, $mon_mon, $mon_date - 1, $mon_year));
		//echo $this->prev_mon_start.'--->'.$this->prev_mon_end.'<br>';
		
		$this->next_mon_start = date('m/d/Y', mktime(0, 0, 0, $mon_mon + 1, $mon_date, $mon_year));
		$this->next_mon_end = date('m/d/Y', mktime(0, 0, 0, $mon_mon + 2, $mon_date - 1, $mon_year));
		//echo $this->next_mon_start.'--->'.$this->next_mon_end.'<br>';
				
		$this->date_prevto_3months = date('m/d/Y', mktime(0, 0, 0, date('m')-3, date('d'), date('Y')));	
		
		$this->AddTemplate('snippet_call_report.tpl');
		$this->AddButton('print24.png', 'javascript:window.print();', 'Print');
	}
	public function CreateReport($dt1, $dt2, $uncompleted_tasks, $uncompleted_events) {
		if($this->sel_report_user > 0) {
			$this->sales_people_ids = array($this->sel_report_user);
		}
		else {
			$this->getUsersArr();
		}
		
		//if(isset($_SESSION['USER']['ISSALESPERSON']) && ($_SESSION['USER']['ISSALESPERSON'] == 1)) {
			//$this->show_user_in_report = false;
		//}
		//else {
			$this->show_user_in_report = true;
		//}
		
		$field_map_obj = new fieldsMap();
		$contact_map_fname = $field_map_obj->getContactFirstNameField();
		$contact_map_lname = $field_map_obj->getContactLastNameField();
		$company_map_name = $field_map_obj->getAccountNameField();
		$report_events = $this->GetReportEventTypes();
		$tot_event_list = array();
		$event_list = array();
		
		if(count($report_events) > 0)	{
			foreach($report_events AS $key=>$val)	{
				$this->report_event_ids[] = $val['ID'];
			}
		}
		
		if(count($this->report_event_ids) > 0) {

			$params = array(
				array(DTYPE_INT, $_SESSION['company_obj']['CompanyID']),
				array(DTYPE_INT, $_SESSION['login_id']),
				array(DTYPE_TIME, $dt1),
				array(DTYPE_TIME, $dt2),
				array(DTYPE_TIME, $dt1),
				array(DTYPE_TIME, $dt2)
			);
			
			$filter = "";
			if($uncompleted_tasks){
				$filter .= " OR (ET.Type = 'TASK' AND (E.Closed = 0 OR E.Closed IS NULL) AND E.StartDateUTC < ?)";
				array_push($params, array(DTYPE_TIME, $dt2));
			}
			if($uncompleted_events){
				$filter .= " OR (ET.Type = 'EVENT' AND (E.Closed = 0 OR E.Closed IS NULL) AND (E.StartDateUTC BETWEEN ? AND ?))";
				array_push($params, array(DTYPE_TIME, $dt1));
				array_push($params, array(DTYPE_TIME, $dt2));
			}
			else {
				$filter .= " OR (ET.Type = 'EVENT' AND (E.Closed = 1) AND (E.StartDateUTC BETWEEN ? AND ?))";
				array_push($params, array(DTYPE_TIME, $dt1));
				array_push($params, array(DTYPE_TIME, $dt2));
			}
			
			/*
			 * Get Non-Repeating Events
			 */
             /*start:JET-4,  Call Report for Multiple Event Attendees*/
             $sqlGetCurrentUserEvent = "SELECT EventID FROM EventAttendee where PersonID = {$_SESSION['login_id']} ";
             $rs_sqlGetCurrentUserEvent = SqlBuilder()->LoadSql($sqlGetCurrentUserEvent)->BuildSqlParam(array());
             $currentUserEvents = DbConnManager::GetDb('mpower')->Exec($rs_sqlGetCurrentUserEvent);
             $userEvents =  array();
             foreach ($currentUserEvents as $currentUserEvent) {
                 $userEvents[] .= $currentUserEvent['EventID'];
              }


			$sql = "
				SELECT
					E.EventID, E.Closed AS IsClosed, ET.EventName, NULL AS DateEntered, ET.EventTypeID, EA.EventAttendeeID,
					CONVERT(varchar(10), E.StartDateUTC, 101) AS StartDate,
				
					CONVERT(varchar(10), E.ModifyTime, 101) AS DateModified, E.Subject, A.Text01 AS CompanyName, 
								C.Text01+' '+C.Text02 AS ContactName, NS.NoteText AS Notes,
								P.FirstName+' '+P.LastName AS UserName 
				FROM
					Event E
				JOIN
					EventAttendee EA ON E.EventID = EA.EventID
				JOIN
					EventType ET ON E.EventTypeID = ET.EventTypeID
				
				
				LEFT JOIN Contact C ON C.ContactID = E.ContactID 
				LEFT JOIN Account A ON A.AccountID = C.AccountID 
				LEFT JOIN People P ON P.PersonID = EA.PersonID
				
				LEFT JOIN (SELECT MAX(NoteID) AS NoteID, ObjectReferer, ObjectType
								FROM Notes GROUP BY ObjectReferer,ObjectType
								) N ON N.ObjectReferer = E.EventID AND N.ObjectType=".NOTETYPE_EMAIL."
								LEFT JOIN Notes NS ON NS.NoteID = N.NoteID
				WHERE
					E.CompanyID = ? AND
					E.RepeatType = 0 AND
					E.EventTypeID IN(".implode(',', $this->report_event_ids).")";
                    
                    $sql .= " AND E.EventID IN(".implode(',', $userEvents).")";                    
                    /* Jet-4 show all user in a event */
                    
             //        if ($this->sel_report_user > 1) {
                       $sql.= " AND
					   EA.PersonID IN(".implode(',', $this->sales_people_ids).")"; 
               //     }

                    $sql .= " AND
					(E.Cancelled = 0 OR E.Cancelled IS NULL) AND
					(E.Private = 0 OR (E.Private = 1 AND EA.PersonID = ?)) AND
					(
						
							(
								(ET.Type = 'TASK' AND (E.StartDate BETWEEN ? AND ?) AND E.Closed = 0) OR
								(ET.Type = 'TASK' AND (E.ModifyDate BETWEEN ? AND ?) AND E.Closed = 1)
							)
							
							" . $filter . "
							
					)";
			
			
			/*end:JET-4*/
			
			$rs_sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
			
			/*
			echo '<pre>';
			echo $rs_sql;
			echo '</pre>';
            */			
			$event_list = DbConnManager::GetDb('mpower')->Exec($rs_sql);
			
		}
		
		/*
		 * Get Repeating Events
		 */
		$repeating = $this->getRepeatingEvents($dt1, $dt2);
		
		
		/*
		 * Get Call List
		 */
		$contact_call_list = array();
		$contact_sql_call = "SELECT 1 AS IsClosed, N.NoteSpecialType AS EventName, CONVERT(varchar(10),N.CreationDate, 101) AS  DateEntered, 
			CONVERT(varchar(10),N.CreationDate, 101) AS StartDate, 
			CONVERT(varchar(10),N.CreationDate, 101) AS DateModified, N.Subject, A.$company_map_name AS CompanyName, 
			C.$contact_map_fname+' '+C.$contact_map_lname AS ContactName, N.NoteText AS Notes ,
			P.FirstName+' '+P.LastName AS UserName
			FROM Notes N
			LEFT JOIN Contact C ON C.ContactID = N.ObjectReferer 
			LEFT JOIN Account A ON A.AccountID = C.AccountID 
			LEFT JOIN People P ON P.PersonID = N.CreatedBy
			WHERE N.ObjectType = ".NOTETYPE_CONTACT." AND N.NoteSpecialType IN(".NOTETYPE_INCOMING_CALL.",".NOTETYPE_OUTGOING_CALL.")
			AND N.CreatedBy IN(".implode(',', $this->sales_people_ids).")
			AND (N.PrivateNote = 0 OR (N.PrivateNote = 1 AND N.CreatedBy = ?)) 
			AND N.CreationDate BETWEEN ? AND ?
			";
			
		$contact_rs_sql_call = SqlBuilder()->LoadSql($contact_sql_call)->BuildSql(array(DTYPE_INT, $_SESSION['login_id']), array(DTYPE_TIME, $dt1), array(DTYPE_TIME, $dt2));
		
		/*
		echo '<pre>';
		echo $rs_sql_call;
		echo '</pre>';
		*/
		$contact_call_list = DbConnManager::GetDb('mpower')->Exec($contact_rs_sql_call);

		$account_call_list = array();
		$account_sql_call = "SELECT 1 AS IsClosed, N.NoteSpecialType AS EventName, CONVERT(varchar(10),N.CreationDate, 101) AS  DateEntered, 
			CONVERT(varchar(10),N.CreationDate, 101) AS StartDate, 
			CONVERT(varchar(10),N.CreationDate, 101) AS DateModified, N.Subject, A.$company_map_name AS CompanyName, 
			NULL AS ContactName, N.NoteText AS Notes ,
			P.FirstName+' '+P.LastName AS UserName
			FROM Notes N
			LEFT JOIN Account A ON A.AccountID = N.ObjectReferer 
			LEFT JOIN People P ON P.PersonID = N.CreatedBy
			WHERE N.ObjectType = ".NOTETYPE_COMPANY." AND N.NoteSpecialType IN(".NOTETYPE_INCOMING_CALL.",".NOTETYPE_OUTGOING_CALL.")
			AND N.CreatedBy IN(".implode(',', $this->sales_people_ids).")
			AND (N.PrivateNote = 0 OR (N.PrivateNote = 1 AND N.CreatedBy = ?)) 
			AND N.CreationDate BETWEEN ? AND ?
			";
			
		$account_rs_sql_call = SqlBuilder()->LoadSql($account_sql_call)->BuildSql(array(DTYPE_INT, $_SESSION['login_id']), array(DTYPE_TIME, $dt1), array(DTYPE_TIME, $dt2));
		
		/*
		echo '<pre>';
		echo $account_rs_sql_call;
		echo '</pre>';
		*/
		$account_call_list = DbConnManager::GetDb('mpower')->Exec($account_rs_sql_call);
		
		
		$call_list = array_merge($contact_call_list, $account_call_list);
		$tot_event_list = array_merge($event_list, $call_list);
		$complete_list = array_merge($tot_event_list, $repeating);
		
		return $complete_list;
	}
	
	public function GetReportEventTypes() {
		$sql = "SELECT EventTypeID AS ID, EventName AS Name FROM EventType 
			WHERE CompanyID = ? AND UseEventInCallReport=1";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']));
		$event_types = DbConnManager::GetDb('mpower')->Exec($sql);	
		/*
		echo '<pre>';
		print_r($event_types);
		echo '</pre>';
		*/
		return $event_types;
	}
	
	public function getRepeatingEvents($start, $end){
		
		$tz_str = $_SESSION['USER']['BROWSER_TIMEZONE'];
		$user_tz = new DateTimeZone($tz_str);
		$start_date = new JetDateTime($start, $user_tz);
		$end_date = new JetDateTime($end, $user_tz);
		
		/*
		 * Get All Repeating Events for Company
		 */
		
		$sql = "
			SELECT * FROM Event E JOIN EventAttendee EA
			ON E.EventID = EA.EventID
			WHERE
				CompanyID = ? AND
				RepeatType > 0 AND
				(Cancelled = 0 OR Cancelled IS NULL) AND
				E.EventTypeID IN(".implode(',', $this->report_event_ids).") AND
				EA.PersonID IN(".implode(',', $this->sales_people_ids).") AND
				(RepeatEndDateUTC > ? OR RepeatEndDateUTC IS NULL)
			";
		$params = array(
			array(DTYPE_INT, $_SESSION['USER']['COMPANYID']),
			array(DTYPE_TIME, $start)
		);
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		
		$repeating_event_result = DbConnManager::GetDb('mpower')->Exec($sql);	
		
		if(count($repeating_event_result) > 0){
			$event = new Event();
			$repeating_events = $event->BreakoutRepeating($repeating_event_result, $start_date, $end_date);
		
		}
		$events = array();
		foreach($repeating_events as $repeating_event){
			$event = array();
			$event['EventID'] = $repeating_event['EventID'];
			$event['Closed'] = $repeating_event['Closed'];
			$event['EventName'] = EventUtil::getEventTypeName($repeating_event['EventTypeID']);
			$event['DateEntered'] =  null;
			$event['EventTypeID'] = $repeating_event['EventTypeID'];
			$event['EventAttendeeID'] = $repeating_event['EventAttendeeID'];
			$event['DateModified'] = null;
			$start_stamp = strtotime($repeating_event['StartDateUTC']);
			$event['StartDate'] = date('m/d/Y', $start_stamp);
			$event['Subject'] = $repeating_event['Subject'];
			$contact = ContactUtil::getContact($repeating_event['ContactID']);
			$event['ContactName'] = $contact['Text01'] . ' ' . $contact['Text02'];
			$event['CompanyName'] = AccountUtil::getAccountName($contact['AccountID']);
			$event['UserName'] = UserUtil::getFullName($repeating_event['PersonID']);
			$event['Notes'] = null;
			$events[] = $event;
		}
		return $events;
	}
}
