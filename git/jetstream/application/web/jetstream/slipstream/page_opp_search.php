<?php
require_once BASE_PATH . '/slipstream/class.JetstreamPage.php';
require_once BASE_PATH . '/include/lib.string.php';
require_once BASE_PATH . '/slipstream/class.Oppboard.php';
require_once BASE_PATH . '/slipstream/class.ModuleOppReport.php';
require_once BASE_PATH . '/slipstream/class.ModuleOppTreeSidebar.php';

$page = new JetstreamPage();

// echo '<pre>' . print_r($_SESSION, TRUE) . '</pre>';

$module = new Oppboard();
$module->title = 'Opportunities';
$module->AddTemplate('module_opp_search.tpl');
$module->AddJavascript('ui.datepicker.js','jquery.js', 'opportunity_search.js', 'jquery.form.js');
$module->AddCss('ui.datepicker.css','search.css', 'jet_datagrid.css');
$module->search_types = array(4 => 'Opportunity Name', 1 => 'Company', 2 => 'Contact');
$module->age_range = array('30 days', '60 days', '90 days', '180 days', '1 year','All');

if(isset($_SESSION['LastOppSearch'])) {
	$module->Build($_SESSION['LastOppSearch']);	
}else {
$_SESSION['LastOppSearch']['sort_type'] = '';
$_SESSION['LastOppSearch']['sort_dir'] = '';
$module->Build($_SESSION['LastOppSearch']);
}

$page->AddModule($module);
/*
//opp report
/*
$module = new ModuleOppReport();
$module->title = 'Opportunity Reports';
$module->AddTemplate('module_opp_report.tpl');
$module->AddJavascript('jquery.js', 'opportunity_search.js', 'jquery.form.js');
$module->AddCss('search.css');

$page->AddModule($module);
*/

$page->AddModule(new ModuleOppTreeSidebar($company_id, $user_id));

include 'class.ModuleOpportunitySidebar.php';
$_SESSION['usage'] = "USER";
$_SESSION['type'] = 'Active';
$page->AddModule(new ModuleOpportunitySidebar($company_id, $user_id));
$_SESSION['type'] = 'Won';
$page->AddModule(new ModuleOpportunitySidebar($company_id, $user_id));
$_SESSION['type'] = 'Lost';
$page->AddModule(new ModuleOpportunitySidebar($company_id, $user_id));

$page->Render();
