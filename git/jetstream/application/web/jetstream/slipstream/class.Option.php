<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/slipstream/class.Status.php';

class Option extends MpRecord
{

	public function __toString()
	{
	        return 'Option';
	}
	public function Initialize()
	{
		$this->db_table = '[Option]';
		$this->recordset->GetFieldDef('OptionID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('CompanyID')->required = TRUE;
		$this->recordset->GetFieldDef('OptionName')->required = TRUE;
		$this->recordset->GetFieldDef('OptionSetID')->required = FALSE;
		parent::Initialize();
	}
	
	function insertOptions($companyId,$optionName,$optionSetId)
	{
		$status=new Status();
		$sql = "SELECT * FROM [Option] WHERE OptionID =-1";
		$rs = DbConnManager::GetDb('mpower') -> Execute($sql);
		$this->SetDatabase(DbConnManager::GetDb('mpower'));
		$this->recordset = $rs;
		$this->Initialize();
		$this->CompanyID=$companyId;
		$this->OptionSetID = $optionSetId;
		$this->OptionName = trim($optionName);
		$this->Save();
		return $status;
	}
	
	function updateOptions($optionId,$optionName)
	{
		$status=new Status();
		$sql = "SELECT * FROM [Option] WHERE OptionID = ?";
		$optionSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $optionId));
		$result = DbConnManager::GetDb('mpower')->Execute($optionSql,'Option');
		if (count($result) == 1) {
			$objOption = $result[0];
			$objOption->Initialize();
			$objOption->OptionName = trim($optionName);
			$result->Save();
		}
		return $status;
	}
	
	function getOptionsByOptionSetId($companyId,$optionSetId)
	{
		$sql="select OptionName,OptionID,CompanyID from [Option] where (CompanyID = ? OR CompanyID = -1) and OptionSetID = ?";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId),array(DTYPE_INT, $optionSetId));
		$options = DbConnManager::GetDb('mpower')->Exec($sql);
		return $options;
	}
	
	function deleteOptionsByOptionId($optionId)
	{
		$sql="delete from [Option] where OptionID = ?";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $optionId));
		$options = DbConnManager::GetDb('mpower')->Exec($sql);
	}
	
	function deleteOptionsByOptionSetId($optionSetId)
	{
		$sql="delete from [Option] where OptionSetID = ?";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $optionSetId));
		$options = DbConnManager::GetDb('mpower')->Exec($sql);
	}
}



?>