<?php
if (!defined('BASE_PATH')) {
	define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));
}

require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';
require_once BASE_PATH . '/include/class.RecOpportunity.php';
require_once BASE_PATH . '/include/class.RecOpportunityProd.php';
require_once BASE_PATH . '/include/lib.dblookup.php';
require_once 'class.Opportunity.php';
require_once 'class.JetstreamModule.php';
require_once 'class.Note.php';
require_once 'class.CompanyContact.php';

define ('ADD', 0);
define ('EDIT', 1);


class ModuleOpportunityNew extends JetstreamModule
{
	protected $javascript_files = array('ui.datepicker.js', 'jquery.cluetip.js', 'opportunity.js');
	protected $css_files = array('event.css', 'ui.datepicker.css', 'mini_calendar.css', 'jquery.cluetip.css', 'opps.css');

	private $company_id;
	private $user_id;
	public $title = 'Opportunity Information';
	public $opportunity_info;
	public $attendees;

	public $repeat_types = array('Does not repeat', 'Daily', 'Weekly', 'Monthly', 5 => 'Yearly');
	public $weekdays = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
	public $pos_locs = array(1 => 'First', 'Second', 'Third', 'Fourth', 'Last');
	public $pos_types = array(7 => 'day', 8 => 'weekday', 9 => 'weekend day', 0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday');

	public $form_id;
	public $submit_to = "ajax.Opportunity.php";

	public $products = array();
	public $product_names = array();
	public $product_prices = array();

	public $locs = array();
	public $loc_names = array();
	public $loc_percents = array();
	
	public $result_status = array();
	public $results = array();

	function __construct($company_id = -1, $user_id = -1) {
		$this->company_id = ($company_id == -1) ? $_SESSION['company_obj']['CompanyID'] : $company_id;
		$this->user_id = ($user_id == -1) ? $_SESSION['USER']['USERID'] : $user_id;
	}

	public function Build($rec_type, $rec_id, $action = 'display', $limit_access = false, $repeat_instance = false, $default_data = false) {
		// Load data, populate new record
		if ($action != 'add') {
			//$opportunity_sql = "SELECT * FROM ot_opportunities WHERE OpportunityID = ?";
			//$opportunity_sql = "SELECT ContactID,OP.CompanyID  CompanyID,SalesPersonID,UserID,OpportunityName,DateEntered,DateModified,StartDate,ExpCloseDate,CloseDate,LastModifiedUserID,PrivateOpportunity, DealID, IsCancelled,IsClosed,SalesCycleLocID,OP.ResultID ResultID,ResultName,SCLname,SCLPercent FROM ot_opportunities OP, ot_Results R, ot_SalesCycleLocation S WHERE OP.OpportunityID = ? and OP.ResultID=R.ResultID and OP.SalesCycleLocID=S.SCLID";
			$opportunity_sql = "SELECT OP.OpportunityID, ContactID,OP.CompanyID ,SalesPersonID,UserID,OpportunityName,DateEntered,DateModified,StartDate,ExpCloseDate,CloseDate,LastModifiedUserID,PrivateOpportunity,DealID, IsCancelled,IsClosed,SalesCycleLocID,OP.ResultID ResultID,ResultName,SCLname,SCLPercent, EstTotal, ActTotal FROM ot_opportunities OP left join ot_Results R on OP.ResultID=R.ResultID left join ot_SalesCycleLocation S on OP.SalesCycleLocID=S.SCLID WHERE OP.OpportunityID = ?";

			$opportunity_sql = SqlBuilder()->LoadSql($opportunity_sql)->BuildSql(array(DTYPE_INT, array($rec_id)));
			
			//$this->opportunity_info['OpportunityID'] = $rec_id;
			$this->OpportunityID = $rec_id;
			
			//$recordset = DbConnManager::GetDb('mpower')->Execute($opportunity_sql, 'RecOpportunity');
			$recordset = DbConnManager::GetDb('mpower')->Execute($opportunity_sql);

			$recordset->Initialize();
			$this->opportunity_info = $recordset[0];

			// Default data is used because edit opp stores form value until the opp is saved
			// This way if they re-edit an event, they can see their original edits
			if ($default_data) {
				foreach ($default_data as $field => $value) {
					$this->opportunity_info[$field] = $value;
				}
			}

			// Get any needed lookup data
			//$this->opportunity_info['OpportunityType'] = OpportunityTypeLookup::GetRecord($this->company_id, $this->opportunity_info['OpportunityTypeID']);
			$this->opportunity_info['User'] = ReportingTreeLookup::GetRecord($this->company_id, $this->opportunity_info['UserID']);
			$this->opportunity_info['salesperson'] = ReportingTreeLookup::GetRecord($this->company_id, $this->opportunity_info['SalesPersonID']);
			
			// Translate data for display
			$this->opportunity_info['StartTimeStamp'] = strtotime($this->opportunity_info['StartDate']);
			$this->opportunity_info['Start'] = date('m/d/y',$this->opportunity_info['StartTimeStamp']);
			//echo $this->opportunity_info['ExpCloseDate'];
			//if (!empty($this->opportunity_info['ExpCloseDate'])) {
			if($this->opportunity_info['ExpCloseDate'] != ''){
			$this->opportunity_info['EndTimeStamp'] = strtotime($this->opportunity_info['ExpCloseDate']);	
			//echo $this->opportunity_info['EndTimeStamp'];
			$this->opportunity_info['ExpCloseDate1'] =  date('m/d/y',$this->opportunity_info['EndTimeStamp']);		
			}
			if($this->opportunity_info['CloseDate']){
			$this->opportunity_info['CloseDateStamp'] = strtotime($this->opportunity_info['CloseDate']);	
			$this->opportunity_info['CloseDate1'] =  date('m/d/y',$this->opportunity_info['CloseDateStamp']);	
			}
			//$this->opportunity_info['Duration'] = $this->opportunity_info['EndTimeStamp'] - $this->opportunity_info['StartTimeStamp'];
			$now = getdate();
			$today_begin = mktime(0, 0, 0, $now['mon'], $now['mday'], $now['year']);
			if($this->opportunity_info['CloseDate']){
			$this->opportunity_info['Duration'] =  $this->opportunity_info['CloseDateStamp'] - $this->opportunity_info['StartTimeStamp'];
			}else{
			$this->opportunity_info['Duration'] =  $today_begin - $this->opportunity_info['StartTimeStamp'];
			}
			$this->opportunity_info['Duration'] = ceil($this->opportunity_info['Duration'] / (24*60*60));			
		} else {
			date_default_timezone_set($_SESSION['USER']['BROWSER_TIMEZONE']);

			if ($rec_type == 'contact') {
				$this->opportunity_info['ContactID'] = $rec_id;
			} elseif ($rec_type == 'opportunity') {
				if (!empty($rec_id)) {
					$this->opportunity_info['OppportunityID'] = $rec_id;
				}
				if ($default_data) {
					foreach ($default_data as $field => $value) {

						$this->opportunity_info[$field] = $value;
						if ($field == 'StartDate') {
							$this->opportunity_info['StartTimeStamp'] = strtotime($value);
						}
						if ($field == 'ExpCloseDate') {
							$this->opportunity_info['ExpCloseDate'] = strtotime($value);
						}
					}
				}
			}
		}
		
		if (empty($this->creator)) {
			$this->creator = $this->opportunity_info['SalesPersonID'];
		}
		
		if ($this->opportunity_info['ContactID']) {
			$this->opportunity_info['Contact'] = GetContactName($this->company_id, $this->opportunity_info['ContactID']);
		}


		switch ($action) {
			case 'add' :
			case 'edit' :

				$this->permitted_people = $this->GetPermittedPeople();
				$this->products = $this->GetProducts();
				
				$combo[] = array();
				foreach($this->products as $key => $product){
					$combo = explode('||',$product);				
					$this->product_names[$key] = $combo[0];
					$this->product_prices[$key] = $combo[1];
				}				
				
				$this->locs = $this->GetSalesCycleLocations();
				$loc_combo[] = array();

				foreach($this->locs as $key => $loc){
					$loc_combo = explode('||',$loc);
					$this->loc_names[$key] = $loc_combo[0];
					$this->loc_percents[$key] = $loc_combo[1];
				}
				
				$this->DBresults = $this->GetDBResults();
				$result_combo[] = array();
				
				foreach($this->DBresults as $key => $result){
					$result_combo = explode('||',$result);
					$this->results[$key] = $result_combo[0];
					$this->result_status[$key] = $result_combo[1];
				}
				
				$this->opp_prods = $this->GetOppProds();
				//echo 'here: '.$this->opp_prods['est_total'];
				$this->form_id = 'id' . uniqid();
				$this->template_files = array('form_opportunity_new.tpl');
				
				break;
				
			default :
				$this->opp_prods = $this->GetOppProds();
				//echo 'here: '.$this->opp_prods['est_total'];
				if($action=='default_only') {				
				$this->AddTemplate('module_opp_prod.tpl'); 
				}
				else $this->AddTemplate('module_opportunity_info.tpl');

				// Add the appropriate buttons to the module
				if ($this->opportunity_info['Cancelled'] != 1 && $this->opportunity_info['Closed'] != 1) {
					//if ($this->opportunity_info->InRecord('OpportunityID')) {
							$opportunity_ref = "opportunityid={$this->opportunity_info['OpportunityID']}";
							$opportunity_encoded = '';
					//	} 
					if($this->opportunity_info['SalesPersonID'] == $_SESSION['login_id'] || in_array($this->opportunity_info['SalesPersonID'], ReportingTreeLookup::GetLimb($this->company_id, $this->user_id)))
						$this->AddButton('', "javascript:ShowAjaxForm('OpportunityNew&$opportunity_ref&action=edit');", 'Edit');
						//$this->AddButton('', "javascript:CompletePendingOpportunity('{$this->opportunity_info['OpportunityID']}', '$opportunity_encoded');", 'Complete');
						//$this->AddButton('', "javascript:CompletePendingOpportunity('{$this->opportunity_info['OpportunityID']}', '$opportunity_encoded', 'remove');", 'Remove');

					//}
				} else {
					$this->formatting = 'content_inactive';
				}
		}
	}

	public function SaveForm($form_values, $only_return_values = false, $source_page = '', $ignore_missing = false) {

		require_once 'class.Status.php';

		$status = new Status();
		$record = new RecOpportunity();

		// Determine what the ID is for this opportunity (-1 for a new one)
		if (empty($form_values['OpportunityID'])) {
			$form_type = ADD;
			$opportunity_id = -1;
		} elseif ($form_values['OpportunityID'] < 0) {
			$form_type = ADD;
			$opportunity_id = $form_values['OpportunityID'];
		} else {
			$form_type = EDIT;
			$opportunity_id = $form_values['OpportunityID'];
		}
		//echo 'here:'.$form_type;
		// Load the record from the database
		$sql = 'SELECT * FROM ot_Opportunities WHERE OpportunityID = ?';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $opportunity_id));
		$recordset = DbConnManager::GetDb('mpower')->Execute($sql, 'RecOpportunity');

		if ($form_type == ADD) {
			$record = new RecOpportunity();
			$recordset[] = $record;
			$record->SetDatabase(DbConnManager::GetDb('mpower'));
		} else {
			$record = $recordset[0];
		}
		$record->Initialize();
		/*
		if (!empty($form_values['DealID'])) {
			$record->DealID = $form_values['DealID'];
		}
		*/
		// Get the fields from the form and set the record values to them
		$record->CompanyID = $this->company_id;
		if (!empty($form_values['ContactID']) && ($form_values['ContactID'] > 0 || $form_values['ContactID'] == -1)) {//how can the ContactID be -1?
			$record->ContactID = $form_values['ContactID'];
		} else {
			$status->addError('You must select a contact', 'Contact');
		}

		if (!empty($form_values['OpportunityName'])) {
			$record->OpportunityName = $form_values['OpportunityName'];
		} else {
			$status->addError('You must enter an Opportunity Name', 'OpportunityName');
			$record->OpportunityName = "NA";
		}

		$record->UserID = $this->user_id;
		$record->LastModifiedUserID = $this->user_id;
		$record->DateEntered = date("Y-m-d H:i:s");
		$record->DateModified = date("Y-m-d H:i:s");

		if (!empty($form_values['SalesPersonID'])) {
			$record->SalesPersonID = $form_values['SalesPersonID'];
			$record->UserID = $form_values['SalesPersonID'];
		} else {
			$status->addError('You must select a salesperson', 'SalesPersonID');
			$record->SalesPersonID = -1;
		}

		if (!empty($form_values['StartDate'])) {
					$startdate = strtotime($form_values['StartDate']);
					$record->StartDate = TimestampToMsSql($startdate);
				} else {
					$status->addError('You must enter a date for this opportunity', 'When');
				}
		if (!empty($form_values['ExpCloseDate'])) {
					$enddate = strtotime($form_values['ExpCloseDate']);
					$record->ExpCloseDate = TimestampToMsSql($enddate);
				} else {
					$status->addError('You must enter an Expected Close Date', 'ExpCloseDate');
				}

		if (!empty($form_values['CloseDate'])) {
					$closedate = strtotime($form_values['CloseDate']);
					$record->CloseDate = TimestampToMsSql($closedate);
				}else{
					$record->CloseDate = '';
				}
		
		if (!empty($form_values['salesloc'])) {
			$record->SalesCycleLocID = $form_values['salesloc'];
		} else {
			$status->addError('You must select Sales Cycle Location', 'SalesCycleLocID');
			$record->SalesCycleLocID = "0";
		}

		if (!empty($form_values['result'])) {
			$record->ResultID = $form_values['result'];
		}else {
			$record->ResultID = '';
			}
		
		$products = json_decode($form_values['product_list']);
		$est_quantities = array();
		$est_quantities = json_decode($form_values['est_qty_list']);
		$est_prices = array();
		$est_prices = json_decode($form_values['est_val_list']);
		$act_quantities = array();
		//$act_quantities = json_decode($form_values['act_qty_list']);
		$act_prices = array();
		//$act_prices = json_decode($form_values['act_val_list']);
		//if (!empty($form_values['CloseDate'])) {
			$act_quantities = json_decode($form_values['act_qty_list']);
			$act_prices = json_decode($form_values['act_val_list']);
		//	} 

		/*
			$myFile = "testFile.txt";
			$fh = fopen($myFile, 'w') or die("can't open file");
			$stringData ='product: '.$products[1];
			fwrite($fh, $stringData);
			fclose($fh);
		*/
		//echo $products[1];
		$record->EstTotal = $form_values['est_total'];
		$record->ActTotal = $form_values['act_total'];
						
		$results['status'] = $status->GetStatus();
		if ($results['status']['STATUS'] == 'OK') {
			$record->Save();
		
			// Handle product_list
			// Load all products of this record, index by OpportunityID

			$product_sql = 'SELECT * FROM ot_Opp_Prod OP WHERE OpportunityID = ?  and (OP.Deleted != 1 OR OP.Deleted IS NULL)';
			$product_sql = SqlBuilder()->LoadSql($product_sql)->BuildSql(array(DTYPE_INT, $record->OpportunityID));
			$product_set = DbConnManager::GetDb('mpower')->Execute($product_sql, 'RecOpportunityProd', 'OPID');

			$product_set->SetAll('Deleted', true);
			$array_counter = 0;
			$est_total = 0;
			$act_total = 0;
			
			foreach($products as $product) {

				if (!$product_set->InRecordset($product)) {
					$product_rec = new RecOpportunityProd();
					$product_set[$product] = $product_rec;
					$product_rec->SetDatabase(DbConnManager::GetDb('mpower'));
					$product_rec->OpportunityID = $record->OpportunityID;
					$product_rec->ProductID = $product;
					$product_rec->Est_Quantity = $est_quantities[$array_counter];
					$product_rec->Est_Price = $est_prices[$array_counter];
					$product_rec->Act_Quantity = $act_quantities[$array_counter];
					$product_rec->Act_Price = $act_prices[$array_counter];
					
					//$est_total += $est_quantities[$array_counter] * $est_prices[$array_counter];					
					//$act_total += $act_quantities[$array_counter] * $act_prices[$array_counter];
			
					$array_counter++;
				}
				$product_set[$product]->Deleted = false;
			}
			$product_set->Initialize()->Save();
			
			//$record->EstTotal = $est_total;
			//$record->ActTotal = $act_total;
			/*
			$results['status'] = $status->GetStatus();
			if ($results['status']['STATUS'] == 'OK') {
			$record->Save();
			*/
			//$product_set->Initialize()->Save();
			
			if (!empty($form_values['Note'])) {
				// Handle note
				$note = new Note();
				$note->noteCreator = $this->user_id;
				$note->noteCreationDate = date("Y-m-d H:i:s");
				$note->noteSubject = "Opportunity Created";
				$note->noteText = nl2br($form_values['Note']);				
				$note->noteObjectType = NOTETYPE_OPPORTUNITY;
				$note->noteObject = $record->OpportunityID;
				$result_note = $note->createNote();
			}
			
			$results['status']['RECID'] = $record->OpportunityID;
			$results['dest'] = "?action=opportunity&opportunityId=" . $record->OpportunityID;
			//$results['dest'] = "?action=contact&contactId=" . $record->ContactID;
		}
		return $results;
	}

	public function DrawForm() {
		return;
	}

	public function GetEventTypes() {
		$event_types = OpportunityTypeLookup::GetAllRecords($this->company_id);
		if (!empty($this->opportunity_info['DealID'])) {
			foreach($event_types as $id => $event_type) {
				if ($event_type['Type'] != 'EVENT') {
					unset($event_types[$id]);
				}
			}
		}
		return $event_types;
	}

	public function GetPeople() {
		$people = ReportingTreeLookup::GetAllActive($this->company_id);
		$people = ReportingTreeLookup::SortPeople($this->company_id, $people, 'LastName');
		$people_list = array();
		foreach($people as $person) {
			$person_rec = ReportingTreeLookup::GetRecord($this->company_id, $person);
			$people_list[$person] = "{$person_rec['FirstName']} {$person_rec['LastName']}";
		}
		return $people_list;
	}

	/*
	public function GetPermittedPeople() {
		$sql = "SELECT PersonID FROM People WHERE
			? IN (SupervisorID, PersonID)
			UNION
			SELECT OwnerPersonID as PersonID
			FROM CalendarPermission INNER JOIN People
				ON CalendarPermission.OwnerPersonID = People.PersonID
			WHERE CalendarPermission.PersonID = ?
				AND CalendarPermission.WritePermission = 1
				AND People.SupervisorID NOT IN (-1, -3)";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($this->user_id, $this->user_id)));

		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		$people = array();
		foreach ($result as $person) {
			$people[] = $person['PersonID'];
		}
		$people = ReportingTreeLookup::SortPeople($this->company_id, $people, 'LastName');
		$people_list = array();
		foreach($people as $person) {
			$person_rec = ReportingTreeLookup::GetRecord($this->company_id, $person);
			$people_list[$person] = "{$person_rec['FirstName']} {$person_rec['LastName']}";
		}
		
		return $people_list;
	}
	*/
	
	public function GetPermittedPeople(){
		
		$companyContact = new CompanyContact();
		$person_id = $_SESSION['USER']['USERID'];
		$person_records = $_SESSION['tree_obj']->GetRecord($person_id);
		
		$children = array();
		$permitted = array();

		$permitted[$person_id] = $person_records['FirstName'] . ' ' . $person_records['LastName'];
					
		$child_records = $companyContact->getChildRecords($person_id);
		$children = $companyContact->getChildren($child_records);
		
		$tmp = $companyContact->clean($children);	
		$children = $tmp;
		foreach($children as $child){
			if($child['IsSalesperson'] == 1){
				$permitted[$child['PersonID']] = $child['FirstName'] . ' ' . $child['LastName'];
			}
		}
		return $permitted;
	}

	public function GetProducts() {
		$sql = "SELECT ProductID, ProductName, Price FROM ot_Products WHERE IsEnabled=1 AND 
			CompanyID = ? ORDER BY SortingOrder";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($this->company_id)));

		/*
		$myFile = "testFile.txt";
			$fh = fopen($myFile, 'w') or die("can't open file");
			$stringData =$sql;
			fwrite($fh, $stringData);
			fclose($fh);
		*/

		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		//$products = array();
		//$product_names = array();
		//$product_prices = array();
		foreach ($result as $product) {
			$products[] = $product['ProductID'];
			$product_names[$product['ProductID']]=$product['ProductName'];
			$product_prices[$product['ProductID']]=$product['Price'];
		}

		$product_list = array();
		foreach($products as $product) {
			$product_list[$product] = $product_names[$product].'||'.$product_prices[$product];
		}
		/*
		foreach ($result as $product) {
			$product_list[$product['ProductID']] ['ID']=$product['ProductID'];
			$product_list[$product['ProductID']] ['Name']=$product['ProductName'];
			$product_list[$product['ProductID']] ['Price']=$product['Price'];
		}
		*/
		//$this->$product_prices = $product_prices;

		return $product_list;
	}
/*
	public function GetOppProds() {
		$sql = "SELECT OpportunityID, ProductID, Est_Quantity, Est_Price, Act_Quantity, Act_Price FROM ot_Opp_Prod WHERE
			OpportunityID = ? ";
			if(empty($this->OpportunityID)) $this->OpportunityID=0;
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($this->OpportunityID)));

		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		//$products = array();
		//$product_names = array();
		//$product_prices = array();
		$opp_prod_list = array();
		foreach ($result as $product) {
			$opp_prod_list[$product['ProductID']] = $product['Quantity'];
		}
		return $opp_prod_list;
	}
*/
	public function GetOppProds() {
		$sql = "SELECT OpportunityID, OP.ProductID, Est_Quantity, Est_Price, Act_Quantity, Act_Price, ProductName FROM ot_Opp_Prod OP, ot_Products P WHERE
			OpportunityID = ?  and OP.ProductID=P.ProductID and (OP.Deleted != 1 OR OP.Deleted IS NULL)";
			if(empty($this->OpportunityID)) $this->OpportunityID=0;
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($this->OpportunityID)));
		//echo $this->OpportunityID;
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		
		$opp_prod_list = array();
		$est_total = 0;
		$act_total = 0;
		foreach ($result as $product) {
			$opp_prod_list[$product['ProductID']]['name'] = $product['ProductName'];
			$opp_prod_list[$product['ProductID']]['est_qty'] = $product['Est_Quantity'];
			$opp_prod_list[$product['ProductID']]['est_price'] = $product['Est_Price'];
			$opp_prod_list[$product['ProductID']]['est_ext'] = $product['Est_Quantity'] * $product['Est_Price'];
			$est_total += $opp_prod_list[$product['ProductID']]['est_ext'];
			$opp_prod_list[$product['ProductID']]['act_qty'] = $product['Act_Quantity'];
			$opp_prod_list[$product['ProductID']]['act_price'] = $product['Act_Price'];
			$opp_prod_list[$product['ProductID']]['act_ext'] = $product['Act_Quantity'] * $product['Act_Price'];
			$act_total += $opp_prod_list[$product['ProductID']]['act_ext'];
		}
		//echo $opp_prod_list['est_total'];
		$opp_prod_list['est_total']=$est_total;
		$opp_prod_list['act_total']=$act_total;
		return $opp_prod_list;
	}

	public function GetSalesCycleLocations() {
		$sql = "SELECT SCLID, SCLName, SCLPercent FROM ot_SalesCycleLocation WHERE InUse=1 AND 
			CompanyID = ? ORDER BY SortingOrder";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($this->company_id)));

		/*
		$myFile = "testFile.txt";
			$fh = fopen($myFile, 'w') or die("can't open file");
			$stringData =$sql;
			fwrite($fh, $stringData);
			fclose($fh);
		*/
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		foreach ($result as $loc) {
			$locs[] = $loc['SCLID'];
			$loc_names[$loc['SCLID']]=$loc['SCLName'];
			$loc_percents[$loc['SCLID']]=$loc['SCLPercent'];
		}

		$loc_list = array();
		foreach($locs as $loc) {
			$loc_list[$loc] = $loc_names[$loc].'||'.$loc_percents[$loc];
		}

		return $loc_list;
	}

	public function GetDBResults() {
		$sql = "SELECT ResultID, ResultName, status FROM ot_Results WHERE InUse=1 AND 
			CompanyID = ? ORDER BY SortingOrder";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($this->company_id)));

		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		$DBresults = array();
		foreach ($result as $aresult) {
			$DBresults[$aresult['ResultID']] = $aresult['ResultName'].'||'.$aresult['status'];
		}
		return $DBresults;
	}
	
	
	
	public function GetRepeatReadable() {
		$output = "Repeats ";
		$output .= " at " . FormatDate($this->opportunity_info['StartTimeStamp'], 'time') . ' to ' . FormatDate($this->opportunity_info['EndTimeStamp'], 'time');
		switch($this->opportunity_info['RepeatType']) {
			case REPEAT_DAILY :
				$output .= " every {$this->repeat_interval['daily_freq']} day(s)";
				break;
			case REPEAT_WEEKLY :
				$output .= " every {$this->repeat_interval['weekly_freq']} week(s) on ";
				$output_days = array();
				foreach ($this->repeat_interval['weekly_days'] as $day) {
					$output_days[] = $this->weekdays[$day];
				}
				$output .= implode(', ', $output_days);
				break;
			case REPEAT_MONTHLY :
				$output .= " every {$this->repeat_interval['monthly_freq']} month(s) on the " . GetOrdinal($this->repeat_interval['monthly_day']);
				break;
			case REPEAT_MONTHLY_POS :
				$output .= " every {$this->repeat_interval['monthly_freq']} month(s) on the " . ($this->pos_locs[$this->repeat_interval['monthly_pos']]) . " " . $this->pos_types[$this->repeat_interval['monthly_pos_type']];
				break;
			case REPEAT_YEARLY :
				$output .= " every {$this->repeat_interval['yearly_freq']} year(s) on the " . GetOrdinal($this->repeat_interval['yearly_month_day']) . " of " . date('F', mktime(0, 0, 0, $this->repeat_interval['yearly_month'], 1));
				break;
			case REPEAT_YEARLY_POS :
				$output .= " every {$this->repeat_interval['yearly_freq']} year(s) on the " . ($this->pos_locs[$this->repeat_interval['yearly_pos']]) . " " . $this->pos_types[$this->repeat_interval['yearly_pos_type']] . " of " . date('F', mktime(0, 0, 0, $this->repeat_interval['yearly_pos_month'], 1));;
				break;
		}
		$output .= "<br>Starting " . FormatDate($this->opportunity_info['StartTimeStamp'], 'date');

		if (!empty($this->opportunity_info['RepeatEndDate'])) {
			$output .= ", ending " . FormatDate($this->opportunity_info['RepeatEndTimeStamp'], 'date');
		} else {
			$output .= " with no end date";
		}

		// $output .= "<br>Next appears on ";

		return $output;
	}
}

function GetOptionsTime($selected) {
	if (empty($selected)) {
		$selected_time = -1;
	} else {
		$selected_array = getdate($selected);
		$selected_time = mktime($selected_array['hours'], $selected_array['minutes'], 0);
	}

	$day_length = 86400; // 24 hours * 60 minutes * 60 seconds
	$increment = 900; // 15 minutes * 60 seconds


	$options = '';

	$start = mktime(0, 0, 0);

	for($timehack = $start; $timehack < $start + $day_length; $timehack += $increment) {
		if ($timehack == $selected_time) {
			$selected_text = "selected";
		} else {
			$selected_text = '';
		}
		$date_string = date("g:i a", $timehack);
		$options .= "<option id='$date_string' $selected_text>$date_string</option>\n";
		if ($selected_time == -1 && date("g:i a", $timehack) == '12:00 pm') {
			$options .= "<option selected />";
		}
	}

	return $options;
}

function FormatDate($date, $show = "full") {
	switch ($show) {
		case "full" :
			return date('n/j/y g:i a', $date);
		case "date" :
			return date('n/j/y', $date);
		case "time" :
			return date('g:i a', $date);
		case "trunc" :
			return date('Ymd', $date);
	}
}

function GetOptionsNumRng($selected, $start, $end, $ordinal = false) {
	$options = '<option />';
	for ($num = $start; $num <= $end; $num++) {
		$selected_text = ($num == $selected) ? "selected" : "";
		$disp_num = ($ordinal) ? GetOrdinal($num) : $num;
		$options .= "<option value='$num' $selected_text>$disp_num</option>";
	}
	return $options;
}

function GetOptionsMonths($selected) {
	$options = '<option />';
	for ($month = 1; $month < 13; $month++) {
		$selected_text = ($month == $selected) ? "selected" : "";
		$options .= "<option value='$month' $selected_text>" . date('F', mktime(0, 0, 0, $month, 1)) . "</option>";
	}
	return $options;
}

function GetOrdinal($number) {
	$ext = 'th';
	if (abs($number) % 100 < 4 || abs($number) % 100 > 20) {
		switch (abs($number) % 10) {
			case 1 :
				$ext = 'st';
				break;
			case 2 :
				$ext = 'nd';
				break;
			case 3 :
				$ext = 'rd';
				break;
		}
	}
	return $number . $ext;
}

