<?php
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/page_import_event.php';

/**
 * Class for Data Import module
 */
class ModuleDataCheck extends JetstreamModule
{
	public $title = 'Import';
	public $sidebar = false;
	protected $css_files = array('import.css', 'search.css');
	protected $javascript_files = array('jquery.js', 'import_data_check.js','slipstream_search.js', 'jquery.form.js', 'jquery.validate.js', 'jquery.blockUI.js');
	
	public $type;
	public $existing;
	public $step;
	public $titleClass;
	public $idLabel;
	public $matchCount;
	public $isContact = false;
	public $complete = false;
	
	public function Init()
	{
		if(!$this->complete){
			if($this->existing){
				$this->AddTemplate('module_data_check.tpl');
			}
			else {
				$this->AddTemplate('module_data_check_empty.tpl');
			}
		}
		else {
			$this->AddTemplate('module_data_import_complete.tpl');
		}
	}
}