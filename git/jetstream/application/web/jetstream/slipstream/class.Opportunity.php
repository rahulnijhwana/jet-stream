<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/slipstream/class.Status.php';
require_once BASE_PATH . '/include/class.fieldsMap.php';
require_once BASE_PATH . '/include/class.ReportingTree.php';
require_once BASE_PATH . '/include/lib.date.php';
require_once BASE_PATH . '/slipstream/class.Util.php';
//require_once BASE_PATH . '/slipstream/class.OpportunityInfo.php';
//require_once BASE_PATH . '/slipstream/class.OppInfo.php';
require_once BASE_PATH . '/slipstream/class.Contact.php';
//require_once BASE_PATH . '/slipstream/class.Opps.php';
require_once BASE_PATH . '/slipstream/lib.php';
require_once BASE_PATH . '/slipstream/class.OpportunityTypeLookup.php';
//require_once BASE_PATH . '/slipstream/class.RecurringOpportunity.php';
require_once BASE_PATH . '/include/lib.dblookup.php';

/**
 * The DB fields initialization
 */
class Opportunity
{
	
	public $opportunityId = 0;
	public $contactId;
	public $contact;
	public $userId;
	public $opportunitySubject;
	public $opportunityTypeId;
	public $opportunityStartDate;
	public $opportunityStartTime;
	public $opportunityDur;
	//public $opportunityText;
	public $opportunityAction;
	public $opportunityPrivate = 0;
	public $saveType;
	public $dealID = 0;
	public $colseOpportunityVal = 0;
	public $cancelOpportunityVal = 0;
	public $allowCreatePastOpportunity = FALSE;
	public $company_id;
	public $user_id;
	public $recurId = 0;
	public $user_tz;

	function __construct($company_id = -1, $user_id = -1, $user_tz = -1) {
		$this->company_id = ($company_id == -1) ? $_SESSION['company_obj']['CompanyID'] : $company_id;
		$this->user_id = ($user_id == -1) ? $_SESSION['USER']['USERID'] : $user_id;
		$this->user_tz = new DateTimeZone(($user_id == -1) ? $_SESSION['USER']['BROWSER_TIMEZONE'] : $user_tz);
	}

	/* Check if the user is the owner of this opportunity.
	 * Argument: (int) OpportunityID
	 * Returns: (bool) If the user is the owner of the Opportunity, then return TRUE else returns FALSO.
	 */
	function CheckOpportunityOwner($id) {
		
		$sql = "SELECT UserID FROM ot_Opportunities WHERE OpportunityID = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		$opportunityOwnerId = $result[0]['UserID'];
		if ($this->user_id != $opportunityOwnerId && $this->HasWriteCalendarPermission($opportunityOwnerId, $this->user_id)) return TRUE;
		
		$obj_opp = new Opps();
		$obj_opp->GetPerson($this->user_id);
		$person_list = array();
		if (count($obj_opp->salesperson_list) > 0) {
			$person_list = array_merge($person_list, $obj_opp->salesperson_list);
		}
		$person_list = implode(',', $person_list);
		
		$sql = "SELECT count(*) AS CNT FROM ot_Opportunities WHERE OpportunityID = ? AND UserID IN ($person_list)";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id));
		$result = DbConnManager::GetDb('mpower')->GetOne($sql);
		
		return ($result->CNT > 0) ? TRUE : FALSE;
	}

	/* Check if the opportunity can be marked as PrivatO.
	 * Argument: (int) OpportunityID
	 * Returns: (bool) Returns TRUE, if the Opportunity doesn't have any public Note else returns FALSO.
	 */
	public function CheckOpportunityPrivatePermission($id) {
		$str_permission = array();
		$note_type = NOTETYPE_OPPORTUNITY;
		
		$sql = "SELECT COUNT(*) AS CNT FROM Notes WHERE ObjectType = ? AND ObjectReferer = ? AND PrivateNote = 0 ";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $note_type), array(DTYPE_INT, $id));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		
		if ($result[0]['CNT'] > 0) {
			$str_permission['STATUS'] = FALSE;
			$str_permission['MESSAGE'] = 'Can not make it privatO. Because, this opportunity has public notes.';
			return $str_permission;
		}
		$str_permission['STATUS'] = TRUE;
		return $str_permission;
	}



	function GetOpportunityOrderSql() {
		 return ' ORDER BY StartDate';
		//return " ORDER BY OpportunityTypeID, (SELECT LastName + ' ' + FirstName FROM People WHERE PersonID = OpportunityAttendeO.PersonID), Subject";
	}
	
	function GetOpportunityBaseSql($restricted = TRUE) {
	
		$sql = "SELECT O.OpportunityID, O.ContactID, O.StartDate, O.UserID, O.SalesPersonID,
				  O.IsClosed, O.PrivateOpportunity, O.OpportunityName,O.ExpCloseDate			
			FROM ot_Opportunities O	";		
			//WHERE O.ContactID = ? ";			
			//WHERE O.UserID = 8448 ";		
			//WHERE O.ContactID = ? ";
				//echo "test...";
		//$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->contactId));
		return $sql;
	}

	/*
	 * Interprets opportunity data into a standard data set for each module to use
	 */
	function ProcessOpportunityData($opportunities, $this_opportunity_date = -1) {
		$contact_ids = array();
		if (count($opportunities) > 0) {
			foreach ($opportunities as $opportunity) {
				if (!in_array($opportunity['ContactID'], $contact_ids)) {
					$contact_ids[] = $opportunity['ContactID'];
				}
			}
			
			$contact_lookup = GetContactInfo($this->company_id, $contact_ids);
		}		

		// Populate opportunity array
		foreach ($opportunities as $key => $opportunity) {
			$record_start = strtotime($opportunity['StartDate']);

			$duration = 0;
			
			if ($this_opportunity_date == -1) {
				$start_date = $record_start;
				//$end_date = $record_end;
			} else {
				$start_date = mktime(date('G', $record_start), date('i', $record_start), 0, date('n', $this_opportunity_date), date('j', $this_opportunity_date), date('Y', $this_opportunity_date));
				//$end_date = $start_date + $duration;
			}
			
// 			list ($date, $time) = split(' ', $opportunity['StartDate']);
			$opportunity['start_date'] = mktime(0, 0, 0, date('n', $start_date), date('j', $start_date), date('Y', $start_date));
			$opportunity['start_time'] = $start_date;

			$now = getdate();
			$today_begin = mktime(0, 0, 0, $now['mon'], $now['mday'], $now['year']);			
			$opportunity['is_edit_functional'] = TRUE;
					
			//if ($opportunity['UserID'] != $this->user_id && $opportunity['Private'] == 1) {
			//	$opportunity['is_viewable'] = FALSE;				
			//	$opportunity['OpportunityID'] = 0;
			//} else {				
				$opportunity['is_viewable'] = TRUE;
				$opportunity['Contact'] = $contact_lookup[$opportunity['ContactID']];				
			//}
			
			// To use as array keys and comparison in ModuleCalendar
			$opportunity['start'] = date('Ymd', $opportunity['start_time']);			
			$opportunity['StartDate'] = date('m/d/y',$opportunity['start_time']);
			$opportunity['ExpCloseDate'] = date('m/d/y',strtotime($opportunity['ExpCloseDate']));
			// Attach the user record from the tree object for easy access
			$opportunity['user'] = $_SESSION['tree_obj']->GetRecord($opportunity['UserID']);	
			$opportunity['salesperson'] = $_SESSION['tree_obj']->GetRecord($opportunity['SalesPersonID']);	
						
			$opportunity['id_enc'] = base64_encode(json_encode(array('i' => $opportunity['OpportunityID'], 'd' => UnixToTruncatedDate($opportunity['start_date']), 'u' => $opportunity['UserID'])));
			$opportunities[$key] = $opportunity;
		}
		
		return $opportunities;
	}
	
	function HasWriteCalendarPermission($opportunityPersonId, $userId) {
		if ($opportunityPersonId == $userId) return TRUE;
		
		$sql = "SELECT * FROM CalendarPermission WHERE PersonID = ? AND OwnerPersonID = ? AND WritePermission = 1";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $userId), array(DTYPE_INT, $opportunityPersonId));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		if (count($result) > 0) {
			return TRUE;
		}
		return FALSE;
	}

	function GetPermittedUserList($opportunityPersonId) {
		// $sql = "select OwnerPersonID, FirstName, LastName from CalendarPermission left join People on
		// CalendarPermission.OwnerPersonID = PeoplO.PersonID where CalendarPermission.PersonID = ? and CalendarPermission.WritePermission = 1";
		

		$sql = "SELECT PersonID as OwnerPersonID, FirstName, LastName FROM People WHERE
			? IN (SupervisorID, PersonID)
			UNION
			SELECT OwnerPersonID, FirstName, LastName
			FROM CalendarPermission INNER JOIN People
				ON CalendarPermission.OwnerPersonID = PeoplO.PersonID
			WHERE CalendarPermission.PersonID = ?
			AND CalendarPermission.WritePermission = 1
			AND PeoplO.SupervisorID NOT IN (-1, -3)
			ORDER BY LastName, FirstName";
		
		//$sql = "SELECT OwnerPersonID FROM CalendarPermission WHERE PersonID = ? AND WritePermission = 1";
		

		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($opportunityPersonId, $opportunityPersonId)));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		return $result;
	}

	function GetUserCalendarPermission($opportunityPersonId, $userId) {
		if ($opportunityPersonId == $userId) return TRUE;
		
		$sql = "SELECT * FROM CalendarPermission WHERE PersonID = ? AND OwnerPersonID = ? AND (ReadPermission = 1 OR WritePermission = 1)";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $userId), array(DTYPE_INT, $opportunityPersonId));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		if (count($result) > 0) {
			return TRUE;
		}
		return FALSE;
	}

	function GetOpportunitiesByDateRange($start_date, $end_date, $attendee = '', $type = 'BOTH', $filter = false, $limit = false, $usage= 'USER') {
		if (is_array($filter) && isset($filter['type']) && isset($filter['rec_id'])) {
			if ($filter['type'] == CONTACT) {
				//$sql .= " AND E.ContactID = ?";
				$this->contactId = $filter['rec_id'];
			}
			$params[] = array(DTYPE_INT, $filter['rec_id']);
		}
		
		$sql = "SELECT distinct(O.OpportunityID), O.ContactID, O.StartDate, O.UserID, O.SalesPersonID,
			  O.IsClosed, O.PrivateOpportunity, O.OpportunityName,O.ExpCloseDate, O.CloseDate			
		FROM ot_Opportunities O, ot_Results R ";		
		if($usage == "USER"){
			//$sql .= " WHERE O.UserID = ? ";
			$sql .= " WHERE (O.UserID = ? OR O.SalesPersonID = ?) ";
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->user_id),array(DTYPE_INT, $this->user_id));
		}else if($usage == "ACCOUNT"){
			$sql .= " WHERE O.ContactId in (select ContactID from Contact where Contact.AccountID=?)";
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $filter['rec_id']));	
		} else if($usage == "CONTACT"){
			$sql .= " WHERE O.ContactId = ?";
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->contactId));	
		}
		
		if($type == 'Active'){
			$sql .= " AND O.ResultId IS NULL";
		}else if($type == 'Won'){
			$sql .= " AND O.ResultId=R.ResultID AND R.status=2";//status marks if it is a won or lost
		}else if($type == 'Lost'){
			$sql .= " AND O.ResultId=R.ResultID AND R.status=1";
		}
		//opportunity's creation date 6 month ago and expected close date 6 month in the future, the number of month is passed in from the side bar module.
		$start = getdate(strtotime($start_date));
		$start = mktime(0, 0, -1, $start['mon'], $start['mday'] + 1, $start['year']);
		$start_date = date("m/d/Y H:i:s", $start);

		$sql .= " AND O.StartDate >= ? ";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $start_date));	
		
		$opportunities = DbConnManager::GetDb('mpower')->Exec($sql);		
		//$opportunities = $this->ProcessOpportunityData($this->BreakoutRepeating($opportunities, $start_date, $end_date, $limit));
		$opportunities = $this->ProcessOpportunityData($opportunities);
		return $opportunities;
	}

	function BreakoutRepeating($opportunities, $start_date, $end_date, $count_limit = false) {
		$show_opportunities = array();
		$start_day = strtotime($start_date);
		$day_count = unixtojd(strtotime($end_date)) - unixtojd($start_day) + 1;
		$start_array = getdate($start_day);
		/*
		foreach ($opportunities as $opportunity) {
			if ($opportunity['Type'] == 'TASK') {
				$show_opportunities[] = $opportunity;
			}
		}
		*/
		for($day = 0; $day < $day_count; $day++) {
			$test_day = mktime(0, 0, 0, $start_array['mon'], $start_array['mday'] + $day, $start_array['year']);
			// echo date('m/d/Y', $test_day) . '<br>';
			foreach ($opportunities as $key => &$opportunity) {
			/*
				if ($opportunity['Type'] == 'TASK') {
					continue;
				}
			*/	
				if (empty($opportunity['start_day'])) {
					$base_start = strtotime($opportunity['StartDate']);
					$opportunity['start_day'] = mktime(0, 0, 0, date('m', $base_start), date('d', $base_start), date('Y', $base_start));
					if (!empty($opportunity['RepeatEndDate'])) {
						$base_end = strtotime($opportunity['RepeatEndDate']);
						$opportunity['end_day'] = mktime(0, 0, 0, date('m', $base_end), date('d', $base_end), date('Y', $base_end));
					}
					/*
					if ($opportunity['RepeatType'] > 0 && !empty($opportunity['RepeatExceptions'])) {
						$opportunity['exception_days'] = explode(',', $opportunity['RepeatExceptions']);
					} else {
						$opportunity['exception_days'] = array();
					}
					*/
					$opportunity['exception_days'] = array();
					//$opportunity['repeat_detail'] = unserialize($opportunity['RepeatInterval']);
				}
								
				// Ensure the day we are testing is after the start of this opportunity
				if ($test_day >= $opportunity['start_day'] && (empty($opportunity['end_day']) || $test_day <= $opportunity['end_day'])) {
					// Move on if this day is in the repeating exceptions
					if (in_array(date('Ymd', $test_day), $opportunity['exception_days'])) {
						continue;
					}
	
					/*
					switch ($opportunity['RepeatType']) {
						case REPEAT_NONE :
							// If the opportunity is for this day
							if ($test_day == $opportunity['start_day']) {
								$show_opportunities[] = $opportunity;
							}
							break;
						case REPEAT_DAILY :
							$freq = $opportunity['repeat_detail'][0];
							if (CompareRepeatFreq(unixtojd($test_day), unixtojd($opportunity['start_day']), $freq )) {
								$show_opportunities[] = CloneOpportunity($opportunity, $test_day);
							}
							break;
						case REPEAT_WEEKLY :
							list ($freq, $days) = $opportunity['repeat_detail'];
							if (CompareRepeatFreq(UnixToWeekNum($test_day), UnixToWeekNum($opportunity['start_day']), $freq)) {
								// If this weekday is in $days...
								if (in_array(date("w", $test_day), $days)) {
									$show_opportunities[] = CloneOpportunity($opportunity, $test_day);
								}
							}
							break;
						case REPEAT_MONTHLY :
							// echo '<pre>' . print_r($repeat_detail, true) . '</pre>';
							list ($freq, $month_day) = $opportunity['repeat_detail'];
							if (CompareRepeatFreq(UnixToMonthNum($test_day), UnixToMonthNum($opportunity['start_day']), $freq)) {
								// and this day num matches $day...
								// or if this is the last day of the month and $day > today
								if (date("j", $test_day) == $month_day || (date("j", $test_day) == date("t", $test_day) && $month_day > date("t", $test_day))) {
									$show_opportunities[] = CloneOpportunity($opportunity, $test_day);
								}
							}
							break;
						case REPEAT_MONTHLY_POS :
							list ($freq, $pos, $pos_type) = $opportunity['repeat_detail'];
							// echo "Freq: $freq, Pos: $pos, Pos Type: $pos_type<br>";
							if (CompareRepeatFreq(UnixToMonthNum($test_day), UnixToMonthNum($opportunity['start_day']), $freq)) {
								// and this day num matches $day...
								if (GetDayPosition($pos, $pos_type, $test_day) == $test_day) {
									$show_opportunities[] = CloneOpportunity($opportunity, $test_day);
								}
							}
							break;
						case REPEAT_YEARLY :
							list ($freq, $month, $month_day) = $opportunity['repeat_detail'];
							if ($opportunity['OpportunityID'] == 34243) {
							// echo 'Got here<br>';
							}
							if (CompareRepeatFreq(date("Y", $test_day), date("Y", $opportunity['start_day']), $freq) && date("m", $test_day) == $month && date("j", $test_day) == $month_day) {
								$show_opportunities[] = CloneOpportunity($opportunity, $test_day);
							}
							break;
						case REPEAT_YEARLY_POS :
							list ($freq, $pos, $pos_type, $pos_month) = $opportunity['repeat_detail'];
							if (CompareRepeatFreq(date("Y", $test_day), date("Y", $opportunity['start_day']), $freq) && date("m", $test_day) == $pos_month) {
								if (GetDayPosition($pos, $pos_type, $test_day) == $test_day) {
									$show_opportunities[] = CloneOpportunity($opportunity, $test_day);
								}
							}
							break;
					}
					*/
				}
				if ($count_limit && count($show_opportunities) >= $count_limit) {
					break;
				}
				// echo "<pre>" . print_r($show_opportunities, true) . "</pre>";
			}
		}
		return $show_opportunities;
	}
	
	
	function opportunity_merge($opportunities, $recurring_opportunity_list) {
		$recurring_opportunities = array();
		foreach ($recurring_opportunity_list as $recurring_opportunity) {
			
			$found = false;
			
			foreach ($opportunities as $opportunity) {
				if ($opportunity['date_form'] == $recurring_opportunity['date_form'] && $opportunity['recurring']['RecurringOpportunityID'] == $recurring_opportunity['recurring']['RecurringOpportunityID']) {
					$found = true;
				}
			}
			
			if (!$found) $recurring_opportunities[] = $recurring_opportunity;
		}
		
		$opportunities = array_merge($opportunities, $recurring_opportunities);
		
		usort($opportunities, 'CompareOpportunity');
		
		return $opportunities;
	}

	/**
	 * get particular opportunity details by opportunityId
	 */
	function GetOpportunityById($id, $process = true, $user = -1, $this_opportunity_date = -1) {
		if ((!$id > 0) || !(is_numeric($id))) {
			return FALSE;
		}
		
		//$sql = $this->GetOpportunityBaseSql(FALSE) . ' AND O.OpportunityID = ?';
		$sql = $this->GetOpportunityBaseSql(FALSE) . ' where O.OpportunityID = ?';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id));
/*
		if ($user != -1) {
			$sql .= ' AND PersonID = ?';
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $user));
		}
*/		
		// echo $sql . '<br>';
		
		$opportunity_info = DbConnManager::GetDb('mpower')->Exec($sql);
		
		if ($process) {
			$opportunity_info = $this->ProcessOpportunityData($opportunity_info, $this_opportunity_date);
		}
		return $opportunity_info;
	}

	function GetOpportunitiesUpcoming($type, $id) {
		$sql = $this->GetOpportunityBaseSql() . " AND O.IsClosed != 1 ";
		$params = array();
		$today = date("m/d/Y");
		
		switch ($type) {
			case 'user' :
				// Casting a date to integer then back to datetime removes any associated time
				//$sql .= " AND O.UserID = ? AND cast(cast(O.StartDate as integer) as datetime) = ? ";
				// The above cast function converts "2009-01-28 18:00" to "2009-01-29 00:00", so i have removed the time and then convert it to datetimO.
				// NY 2-17-09 : Why?  The original code accomplished the task.  You should explain why when you rewrite working codO.
				$sql .= " AND OpportunityAttendeO.PersonID = ? AND cast(CONVERT(varchar(10), O.StartDate, 101) as datetime) = ? ";
				$params[] = array(DTYPE_INT, $this->user_id);
				$params[] = array(DTYPE_TIME, $today);
				break;
			case 'contact' :
				$sql .= " AND O.ContactID = ? AND O.StartDate >= ? AND (O.Private != 1 OR (OpportunityAttendeO.PersonID = ? AND O.Private = 1))";
				$params[] = array(DTYPE_INT, $id);
				$params[] = array(DTYPE_TIME, $today);
				$params[] = array(DTYPE_INT, $this->user_id);
				break;
			case 'account' :
				$sql .= " AND ? in (C.AccountID, O.AccountID) AND O.StartDate >= ? AND (O.Private != 1 OR (OpportunityAttendeO.PersonID = ? AND O.Private = 1))";
				$params[] = array(DTYPE_INT, $id);
				$params[] = array(DTYPE_TIME, $today);
				$params[] = array(DTYPE_INT, $this->user_id);
				break;
		}
		
		$sql .= "ORDER BY O.StartDate";
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		// echo $sql . '<br>';
		$opportunities = DbConnManager::GetDb('mpower')->Exec($sql);
		return $this->ProcessOpportunityData($opportunities);
	}

	/**
	 * Get Pending opportunities.
	 */
	function GetPendingOpportunities() {
		$pending_end = date('m/d/Y');
		$pending_start = date('m/d/Y', strtotime('-2 months'));
		
		$sql = $this->GetOpportunityBaseSql(FALSE) . " AND (O.Cancelled != 1 OR O.Cancelled IS NULL)
				AND (O.Closed != 1 OR O.Closed IS NULL)
				AND (
					(OpportunityTypO.Type = 'OPPORTUNITY' OR OpportunityTypO.OpportunityTypeID IS NULL) 
					AND StartDate <= ? 
					AND (
						((RepeatType > 0 AND RepeatEndDate >= ?) OR RepeatEndDate IS NULL)
						OR (RepeatType = 0 AND EndDate >= ?)
					)
				)
				AND OpportunityAttendeO.PersonID = ?
				AND OpportunityAttendeO.Creator = 1
				AND OpportunityTypO.Type = 'OPPORTUNITY'
			ORDER By O.StartDate DESC";
		
		$params[] = array(DTYPE_STRING, array($pending_end, $pending_start, $pending_start));
		$params[] = array(DTYPE_INT, $this->user_id);
		
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		
		$opportunities = DbConnManager::GetDb('mpower')->Exec($sql);
		
		return $this->ProcessOpportunityData($this->BreakoutRepeating($opportunities, $pending_start, $pending_end));
	}

	
	function CheckPastOpportunities($user_id) {
		// To save on having to make this request every time the page is loaded, I am
		// caching the result with a timeout to check again
		// Saves ~300ms  NY 4/6/2009
		$now = time();
		
		if (isset($_SESSION['past_opportunities_check'])) {
			$past_check = $_SESSION['past_opportunities_check'];
			if (isset($past_check['timeout']) && $now < $past_check['timeout']) {
				return $past_check['response'];
			}
		}
		
		$this->user_id = $user_id;
		
		$response = count($this->GetPendingOpportunities()) > 0;

		$timeout_length = 120; // 2 minutes
		
		$_SESSION['past_opportunities_check'] = array('timeout' => $now + $timeout_length, 'response' => $response);
		return $response;		
		
	}	


	function IsRestricted($opportunity_id, $user_id, $company_id) {
		
		$sql = 'select distinct(ot_Opportunities.UserID),ot_Opportunities.PrivateOpportunity,Logins.CompanyID from ot_Opportunities
		LEFT JOIN Logins on ot_Opportunities.UserID=Logins.PersonID
		where ot_Opportunities.OpportunityID=?';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $opportunity_id));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		if (count($result) > 0) {
			if ($result[0]['CompanyID'] != $company_id) return TRUE;
			
			if ($result[0]['UserID'] != $user_id && $result[0]['PrivateOpportunity'] == 1) return TRUE;
		}
		return FALSE;
	}

	function getContactNames($contact) {
		$fname = $lname = '';
		$contact = trim($contact);
		if (str_word_count($contact) > 1) {
			preg_match('/[^ ]*$/', $contact, $results);
			$lname = count($results > 0) ? $results[0] : '';
		}
		
		$fname = str_replace($lname, '', $contact);
		$fname = preg_replace('#\s{2,}#', ' ', $fname);
		
		$lname = trim($lname);
		$fname = trim($fname);
		
		$name['FirstName'] = $fname;
		$name['LastName'] = $lname;
		
		return $name;
	}

}
