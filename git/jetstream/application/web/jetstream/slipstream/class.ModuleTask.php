<?php
require_once BASE_PATH . '/slipstream/class.Event.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/include/class.ReportingTree.php';

class ModuleTask extends JetstreamModule
{
	protected $css_files = array('event.css');
	public $start_date;
	public $end_date;
	public $task_list;
	public $target;
	public $task_date;
	public $cur_task_list;

	function ModuleTask() {
	}

	public function Init() {
		if ($this->target == '')
			$this->target = explode(',', $_SESSION['login_id']);
		else
			$this->target = explode(',', $this->target);
		
		if ((strtotime($this->start_date) < strtotime(date("m/d/Y"))) && (strtotime($this->end_date) < strtotime(date("m/d/Y"))))
			$this->task_list = $this->GetTask($_SESSION['USER']['COMPANYID'], $this->target, $this->start_date, $this->end_date);
		else
			$this->task_list = $this->GetTask($_SESSION['USER']['COMPANYID'], $this->target, $this->start_date, $this->end_date, true);
	
	}

	public function GetTask($company_id, $target, $start_date, $end_date, $pending_task = false) {
		$account_name = $_SESSION['account_name'];
		$contact_first_name = $_SESSION['contact_first_name'];
		$contact_last_name = $_SESSION['contact_last_name'];
		$param = array();
		$sql = "SELECT E.EventID, E.UserID, E.Subject, CONVERT(varchar, E.StartDate, 101) AS StartDate, CONVERT(varchar, E.DateModified, 101) AS DateModified, 
		C.ContactID, A.AccountID,A.$account_name as Company, ET.EventColor, C.$contact_first_name AS FirstName,
		C.$contact_last_name AS LastName, ET.EventName , ET.EventTypeID, 
		P.Color as PersonColor, E.IsClosed,Pending=NULL FROM Events E 
		LEFT JOIN EventType ET on E.EventTypeID=ET.EventTypeID 
		LEFT JOIN Contact C ON C.ContactID = E.ContactID 
		LEFT JOIN Account A ON A.AccountID = C.AccountID 
		LEFT JOIN People P ON P.PersonID=E.UserID 
		where ET.Type='TASK' AND E.IsCancelled=0 AND P.CompanyID=?";
		$param[] = array(DTYPE_INT, $company_id);
		$sql .= " AND 
		(
			( (E.StartDate between ? AND ?) AND IsClosed in(0,1) ) 
				OR ";
		if ($start_date == $end_date)
			$sql .= "( CONVERT(varchar, E.DateModified, 101) between ? AND ? AND IsClosed = 1) ";
		else
			$sql .= "( E.DateModified between ? AND ? AND IsClosed = 1) ";
		if ($pending_task) $sql .= "	OR (E.StartDate <= ? AND IsClosed = 0) ";
		$sql .= " ) ";
		
		$param[] = array(DTYPE_STRING, $start_date);
		$param[] = array(DTYPE_STRING, $end_date);
		$param[] = array(DTYPE_STRING, $start_date);
		$param[] = array(DTYPE_STRING, $end_date);
		if ($pending_task) $param[] = array(DTYPE_STRING, $end_date);
		
		if (!$_SESSION['USER']['ISSALESPERSON']) {
			$sql .= " AND ( ";
			foreach ($target as $key => $t) {
				if ($key == 0)
					$sql .= " E.UserID=? ";
				else
					$sql .= " OR E.UserID=?";
				
				$param[] = array(DTYPE_INT, $t);
			}
			$sql .= " )";
		} else {
			$sql .= ' AND E.UserID=?';
			$param[] = array(DTYPE_INT, $_SESSION['USER']['USERID']);
		}
		$sql_build = SqlBuilder()->LoadSql($sql);
		$sql = $sql_build->BuildSqlParam($param);
		$task = DbConnManager::GetDb('mpower')->Exec($sql);
		$task_list = array();
		if (count($task) > 0) {
			foreach ($task as $t) {
				// 1-7-08 NY : WTF?  This section is terribly written, very difficult to follow
				// 1-7-08 NY : Unclosed events in the future were not appearing on their day
				if (strtotime($t['StartDate']) > strtotime(date("m/d/Y")) && $t['IsClosed'] == 0) {
					$t['Pending'] = 0;
					$task_list[$t['StartDate']][] = $t;
				} else if ($t['DateModified'] != NULL && $t['IsClosed'] == 1) {
					$t['Pending'] = 0;
					$task_list[$t['DateModified']][] = $t;
				} else if ($t['DateModified'] != NULL && $t['IsClosed'] == 0) {
					if ($t['StartDate'] == date("m/d/Y"))
						$t['Pending'] = 0;
					else
						$t['Pending'] = 1;
					
					$task_list[date("m/d/Y")][] = $t;
				} elseif ($t['DateModified'] == NULL && $t['IsClosed'] == 0 && (strtotime($t['StartDate']) <= strtotime(date("m/d/Y")))) {
					if (strtotime($t['StartDate']) < strtotime(date("m/d/Y")))
						$t['Pending'] = 1;
					else
						$t['Pending'] = 0;
					
					$task_list[date("m/d/Y")][] = $t;
				} elseif ($t['DateModified'] == NULL && $t['IsClosed'] == 0 && (strtotime($t['StartDate']) > strtotime(date("m/d/Y")))) {
					$t['Pending'] = 0;
					$task_list[$t['StartDate']][] = $t;
				}
			}
		}
		//echo $sql;
		// echo '<pre>' . print_r($task_list, TRUE) . '</pre>';
		return $task_list;
	}

	function GetTaskCount($task_list, $task_date, $closed_task = 0) {
		if (!isset($task_list[$task_date])) return 0;
		if ($closed_task == 0) {
			return count($task_list[$task_date]);
		} else {
			$ctr = 0;
			foreach ($task_list[$task_date] as $task) {
				if ($task['IsClosed'] == 1) $ctr++;
			}
			return $ctr;
		}
	
	}

}

?>
