<?php
/**
 * @package database
 * include class file
 */

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
include_once BASE_PATH . '/include/class.fieldsMap.php';

/**
 * function getCategory	
 * Argument1: categoryid
 * Returns the category from the language file.
 */
function getCategory($cat) {
	$att_categories = array(
			1 => 'FMLabel'
			, 'IPLabel'
			, 'IPSLabel'
			, 'DPLabel'
			, 'DPSLabel'
			, 'CLabel'
			, 9 => 'RLabel'
			, 'TLabel');
			
	$catTerm = Language::__get($att_categories[$cat]);
	return $catTerm;
}

/**
 * function foramtDateTime	
 * Argument1: american format date.(m/d/Y)
 * Argument1: time in am / pm.(h:m AM/PM)
 */
function foramtDateTime($date, $time, $type='', $ampm=0) {
	$strDate = '';
	
	if($type != 'time') {
		if($date != '') {
			$startDateArr = explode('/', $date);			
			$strDate .= $startDateArr[2].'-'.$startDateArr[0].'-'.$startDateArr[1];
			//$strDate .= date('Y-m-d', strtotime($date)); 
		}
		else {
			$strDate .= date('Y-m-d');
		}
	}
	
	if($type != 'date') {
		if($time != '') {
			$timeArr = explode(':', $time);
			$stHour = $timeArr[0];	
			$timeArrArr = explode(' ', $timeArr[1]);		
			$stMin = $timeArrArr[0];
			$stType = $timeArrArr[1];
			
			if(trim(strtolower($stType)) == 'pm') {
				$stHourNew = 12 + $stHour;
			} else {
				$stHourNew = $stHour;
			}			
			
			if($ampm == 1) {
				$strDate .= ' '.$stHour.':'.$stMin.':00 '.$stType;				
			}
			else {				
				$strDate .= ' '.$stHourNew.':'.$stMin.':00.000';
			}
		}	
		else {
			$strDate .= ' 00:00:00.000';
		}
	}
	return $strDate;
}

/*function getAccountIdByCompanyId($companyId = '0') {
	$sql = "SELECT AccountID from account where CompanyId=" . $companyId;
	$accounts = DbConnManager::GetDb('mpower')->Exec($sql);
	return $accounts;

}

function getAccountNameUserId($userId = '0') {
	$sql = "SELECT AccountID,Name from account where AccountID in(select AccountID from UserAccount where UserID=?)";
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $userId));
	$accounts = DbConnManager::GetDb('mpower')->Exec($sql);
	return $accounts;
}

function getContactNameByCompanyId($company_id = '0') {

	$sql = 'select ContactID,AccountID,FirstName,LastName from CompanyContactOutlook where AccountID in(select AccountID from Account where CompanyID=?)';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $company_id));
	$contacts = DbConnManager::GetDb('mpower')->Exec($sql);
	return $contacts;
}

function getContactInfoByAccountIdAndFields($account_id = '0', $fields) {
	$sql = 'SELECT ' . $fields . ' FROM Contact WHERE AccountID= ?';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $account_id));
	$contacts = DbConnManager::GetDb('mpower')->Exec($sql);
	return $contacts;
}

function getFirstNameLastNameField($companyId) {
	$sql = 'SELECT FieldName FROM ContactMap WHERE IsFirstName = 1 OR IsLastName = 1 AND CompanyID = ?';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId));
	$fields = DbConnManager::GetDb('mpower')->Exec($sql);
	return $fields;
}

function getCompanyNameById($companyId = '0') {
	$sql = 'select Name from company where CompanyID=?';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId));
	$result = DbConnManager::GetDb('mpower')->GetOne($sql);
	if (!empty($result))
		return $result->Name;
	else
		return 'Unable to Find';
}

function getEventsByTime($userId, $companyId, $time) {
	$sql = 'SELECT * FROM Events WHERE UserID=? AND ? between Events.StartDate and DateAdd([minute],DurationMinutes,DateAdd([hour],DurationHours,StartDate)) AND DateAdd([minute],DurationMinutes,DateAdd([hour],DurationHours,StartDate)) != ? AND Events.StartDate != ?';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $userId), array(DTYPE_STRING, $time), array(DTYPE_STRING, $time), array(DTYPE_STRING, $time));
	$events = DbConnManager::GetDb('mpower')->Exec($sql);
	return $events;
}

function getOtherEventsByEventId($userId, $companyId, $time, $eventId) {
	$sql = 'SELECT * FROM Events WHERE UserID=? AND ? between Events.StartDate and DateAdd([minute],DurationMinutes,DateAdd([hour],DurationHours,StartDate)) AND EventID!=?';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $userId), array(DTYPE_STRING, $time), array(DTYPE_STRING, $eventId));
	$events = DbConnManager::GetDb('mpower')->Exec($sql);
	return $events;
}

function getEventsInfoByTodayDate($userId, $companyId, $startdate) {
	$sql = 'SELECT [EventID],[UserID],Events.[EventTypeID],[Subject],[Description],[DateEntered],[DateModified],[DurationHours],[DurationMinutes],[StartDate],CONVERT(VARCHAR(10), StartDate, 101) as NewStartDate,CONVERT(varchar, StartDate, 8) as StartTime,[EndDate],[Status],[EventName],[EventColor] FROM Events,EventType where Events.EventTypeID=EventType.EventTypeID and Events.UserID=? and EventType.CompanyID=? and CONVERT(VARCHAR(10), StartDate, 101)=? ORDER BY Events.EventID DESC';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $userId), array(DTYPE_INT, $companyId), array(DTYPE_STRING, $startdate));
	$events = DbConnManager::GetDb('mpower')->Exec($sql);
	return $events;
}
function getEventsInfoByContactIds($userId, $companyId, $contactIds, $today) {
	$sql = 'SELECT [EventID],[UserID],Events.[EventTypeID],[ContactID],[Subject],[Description],[DateEntered],[DateModified],[DurationHours],[DurationMinutes],[StartDate],CONVERT(VARCHAR(10), StartDate, 101) as NewStartDate,CONVERT(varchar, StartDate, 8) as StartTime,[EndDate],[Status],[EventName],[EventColor] FROM Events,EventType where Events.EventTypeID=EventType.EventTypeID and Events.UserID=? and EventType.CompanyID=? and ContactID IN (?) and CONVERT(VARCHAR(10), StartDate, 101)>=?';
	$sqlBuild = SqlBuilder()->LoadSql($sql);

	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $userId), array(DTYPE_INT, $companyId), array(DTYPE_INT, -1), array(DTYPE_STRING, $today));
	$sql = str_replace('-1', $contactIds, $sql);
	$events = DbConnManager::GetDb('mpower')->Exec($sql);
	return $events;
}
function getFieldNameFromAccountMapByCompId($companyId,$criteria)
{
	$searchCriteria = $criteria.'%';
	$sql="SELECT FieldName FROM AccountMap where CompanyID= ? and FieldName Like ? Order By FieldName";
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId),array(DTYPE_STRING, $searchCriteria));
	$fieldNames = DbConnManager::GetDb('mpower')->Exec($sql);
	return $fieldNames;
}

function getEventsByContactId($companyId = '0', $contactId) {
	$sql = 'SELECT [EventID],[UserID],Events.[EventTypeID],[Subject],[Description],[DateEntered],[DateModified],[DurationHours],[DurationMinutes],CONVERT(VARCHAR(10), StartDate, 101) as StartDate,[EndDate],[Status],[EventName],[EventColor] FROM Events,EventType where Events.EventTypeID=EventType.EventTypeID and EventType.CompanyID=? and Events.ContactID=?';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId), array(DTYPE_INT, $contactId));
	$events = DbConnManager::GetDb('mpower')->Exec($sql);
	return $events;
}


function getAccountNameByAccountId($accountId = '0') {
	$sql = "SELECT AccountID,Text01 as Name from account where AccountID=?";
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $accountId));
	$accounts = DbConnManager::GetDb('mpower')->Exec($sql);
	return $accounts;
}
*/

function getContactInfoByAccountId($account_id = '0') {
	$sql = 'SELECT * FROM Contact WHERE AccountID= ?';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $account_id));
	$contacts = DbConnManager::GetDb('mpower')->Exec($sql);
	return $contacts;
}

function getContactInfoByIdAndFields($contact_id = '0', $fields) {
	$sql = 'SELECT ' . $fields . ' FROM Contact WHERE ContactID= ?';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $contact_id));
	$contacts = DbConnManager::GetDb('mpower')->Exec($sql);
	return $contacts;
}

/**	function getContactInfoById($contactId = '0') {
		$sql='select AccountID,Address,City,State,Zip,Country,PhoneBusiness,PhoneHome,PhoneCell,PhoneFax,Email,Website,Source,FirstName,LastName,MiddleName from CompanyContactOutlook where ContactID=?';
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $contactId));
		$contact = DbConnManager::GetDb('mpower')->Exec($sql);
		return $contact;
	}
	function getContactInfo()
	{
		$sql='select ContactID,AccountID,Address,City,State,Zip,Country,PhoneBusiness,PhoneHome,PhoneCell,PhoneFax,Email,Website,Source,FirstName,LastName,MiddleName from CompanyContactOutlook';
		$contact = DbConnManager::GetDb('mpower')->Exec($sql);
		return $contact;
	}
 **/

/**
 * get event type information by companyId
 */
function getEventTypeByCompanyId($companyId = '0') {
	$sql = 'select EventTypeID, EventName from EventType where CompanyID=?';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId));
	$eventType = DbConnManager::GetDb('mpower')->Exec($sql);
	return $eventType;
}

/**
 * get all events by companyId
 */
/*function getEvents($companyId = '0', $userId) {
	$sql = 'SELECT [EventID],[UserID],Events.[EventTypeID],[Subject],[Description],[DateEntered],[DateModified],[DurationHours],[DurationMinutes],CONVERT(VARCHAR(10), StartDate, 101) as StartDate,[EndDate],[Status],[EventName],[EventColor] FROM Events,EventType where Events.EventTypeID=EventType.EventTypeID and EventType.CompanyID=? and Events.UserID=?';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId), array(DTYPE_INT, $userId));
	$events = DbConnManager::GetDb('mpower')->Exec($sql);
	return $events;
}*/

/**
 * get events for a month
 */
function getMonthEvents($companyId, $userId, $curMonth, $curYear) {
	if (strlen($curMonth) == 1) {
		$cMonth = "0" . $curMonth;
	} else {
		$cMonth = $curMonth;
	}
	$chkDate = $cMonth . "/%/" . $curYear;
	
	$sql = 'SELECT [EventID],[UserID],Events.[EventTypeID],[Subject],[Description],[DateEntered],[DateModified],[DurationHours],[DurationMinutes],CONVERT(VARCHAR(10), StartDate, 101) as StartDate,[EndDate],[Status],[EventName],[EventColor] FROM Events,EventType where Events.EventTypeID=EventType.EventTypeID and EventType.CompanyID=? and Events.UserID=? AND CONVERT(VARCHAR(10), StartDate, 101) LIKE ?';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId), array(DTYPE_INT, $userId), array(DTYPE_STRING, $chkDate));
	$events = DbConnManager::GetDb('mpower')->Exec($sql);
	return $events;
}

/**
 * get all upcoming events by accountId
 */
function getUpcomingEventsByContactId($companyId = '0', $contactId, $today) {
	$sql = 'SELECT [EventID],[ContactID],[UserID],Events.[EventTypeID],[Subject],[Description],[DateEntered],[DateModified],[DurationHours],[DurationMinutes],CONVERT(VARCHAR(10), StartDate, 101) as StartDate,[EndDate],[Status],[EventName],[EventColor] FROM Events,EventType where Events.EventTypeID=EventType.EventTypeID and EventType.CompanyID=? and Events.ContactID=? and StartDate >= ?';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId), array(DTYPE_INT, $contactId), array(DTYPE_STRING, $today));
	$events = DbConnManager::GetDb('mpower')->Exec($sql);
	return $events;
}

/**
 * get particular event details by eventId
 */
function getEventsInfoByEventId($eventId = '0') {
	$sql = 'SELECT [EventID],[ContactID],[UserID],Events.[EventTypeID],[Subject],[Description],[DateEntered],[DateModified],[DurationHours],[DurationMinutes],CONVERT(VARCHAR(10), StartDate, 101) as StartDate,CONVERT(varchar, StartDate, 8) as StartTime,[EndDate],[Status],[EventName],[EventColor] FROM Events,EventType where Events.EventTypeID=EventType.EventTypeID and Events.EventID=?';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $eventId));
	$events = DbConnManager::GetDb('mpower')->Exec($sql);
	return $events;
}

/**
 * get events by startdate
 */
function getEventsInfoByStartDate($userId, $companyId, $startdate) {
	$sql = 'SELECT [EventID],[ContactID],[UserID],Events.[EventTypeID],[Subject],[Description],[DateEntered],[DateModified],[DurationHours],[DurationMinutes],CONVERT(VARCHAR(10), StartDate, 101) as StartDate,CONVERT(varchar, StartDate, 8) as StartTime,DateAdd([minute],DurationMinutes,DateAdd([Hour],DurationHours,StartDate)) as EndDate,[Status],[EventName],[EventColor] FROM Events,EventType where Events.EventTypeID=EventType.EventTypeID and Events.UserID=? and EventType.CompanyID=? and CONVERT(VARCHAR(10), StartDate, 101)=? ORDER BY CONVERT(varchar, StartDate, 8) ASC';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $userId), array(DTYPE_INT, $companyId), array(DTYPE_STRING, $startdate));
	$events = DbConnManager::GetDb('mpower')->Exec($sql);
	return $events;
}

/**
 * get events by two date range
 */
function getEventsInfoByDates($userId, $companyId, $fdate, $ldate) {
	$sql = 'SELECT [EventID],[ContactID],[UserID],Events.[EventTypeID],[Subject],[Description],[DateEntered],[DateModified],[DurationHours],[DurationMinutes],CONVERT(VARCHAR(10), StartDate, 101) as StartDate,CONVERT(varchar, StartDate, 8) as StartTime,DateAdd([minute],DurationMinutes,DateAdd([Hour],DurationHours,StartDate)) as EndDate,[Status],[EventName],[EventColor] FROM Events,EventType where Events.EventTypeID=EventType.EventTypeID and Events.UserID=? and EventType.CompanyID=? and (CONVERT(VARCHAR(10), StartDate, 101)>= ?) AND (CONVERT(VARCHAR(10), StartDate, 101)<= ?) ORDER BY CONVERT(varchar, StartDate, 8) ASC';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $userId), array(DTYPE_INT, $companyId), array(DTYPE_STRING, $fdate), array(DTYPE_STRING, $ldate));
	$events = DbConnManager::GetDb('mpower')->Exec($sql);
	return $events;
}

/**
 * get event types
 */
function getEventType() {
	$sql = 'SELECT [EventTypeID],[EventName],[EventColor] FROM EventType';
	$eventType = DbConnManager::GetDb('mpower')->Exec($sql);
	return $eventType;
}

/**
 * get validation types
 */
function getValidationTypeById($validationId) {
	$sql = 'SELECT [ValidationName] FROM Validation WHERE ValidationID = ?';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $validationId));
	$vName = DbConnManager::GetDb('mpower')->GetOne($sql);
	return $vName;
}

/**
 * get option values by AccountMapID and CompanyID
 */
function getOptionByAccountMapId($accountmapId, $companyId) {
	$sql = 'SELECT * FROM [Option] WHERE AccountMapID=? AND COmpanyID=?';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $accountmapId), array(DTYPE_INT, $companyId));
	$optionValue = DbConnManager::GetDb('mpower')->Exec($sql);
	return $optionValue;
}

/**
 * get option name by OptionID
 */
function getOptionNameById($optionId, $companyId) {
	$sql = 'SELECT OptionName FROM [Option] WHERE OptionID=? AND CompanyID=?';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $optionId), array(DTYPE_INT, $companyId));
	$optionValue = DbConnManager::GetDb('mpower')->GetOne($sql);
	return $optionValue;
}

/**
 * function for set HTML view data
 * @param array $leftPanelData
 * @param array $rightPanelData 
 * return HTML structure view data
 */
function getViewData($leftPanelData, $rightPanelData) {
	$view_data = "<table class='jet_datagrid'>";
	//	$view_data .= "<colgroup span='4'><col class='label'><col class='left'><col class='label'><col class='right'></colgroup>";
	$right = current($rightPanelData);
	$left = current($leftPanelData);
	while ($right || $left) {
		$view_data .= "<tr><td class='label'>" . ((isset($left['LabelName'])) ? $left['LabelName'] . ':' : '&nbsp;') . "</td>";
		$view_data .= "<td class='left'>";
		if (isset($left['OptionValue'])) {
			$view_data .= $left['OptionValue'];
		} else {
			$view_data .= $left['Value'];
		}
		$view_data .= "</td>";
		
		$view_data .= "<td class='label'>" . ((isset($right['LabelName'])) ? $right['LabelName'] . ':' : '&nbsp;') . "</td>";
		$view_data .= "<td class='right'>";
		if (isset($right['OptionValue'])) {
			$view_data .= $right['OptionValue'];
		} else {
			$view_data .= $right['Value'];
		}
		$view_data .= "</td>";
		$view_data .= "</tr>";
		$right = next($rightPanelData);
		$left = next($leftPanelData);
	}
	
	$view_data .= "</table>";
	return $view_data;
}

/**
 * function for set HTML edit data
 * @param array $leftPanelData
 * @param array $rightPanelData
 * @param array $selectOptions
 * return HTML structure edit data
 */
function getLBEditData($leftPanelData, $rightPanelData, $selectOptions) {
	$viewLBData = "<table class='jet_datagrid'>";
	$right = current($rightPanelData);
	$left = current($leftPanelData);
	while ($right || $left) {
		$chkLeftField = ($left['IsRequired'] == 1) ? '<span class="smlRedBold">*</span>' : '';
		$lLblName = str_replace(" ", "_", $left['LabelName']);
		
		$viewLBData .= "<tr><td class='label'>" . $chkLeftField . ((isset($left['LabelName'])) ? $left['LabelName'] . ':' : '&nbsp;') . "</td>";
		$viewLBData .= "<td class='left'>";
		
		if (isset($left['LabelName'])) {
			if (substr($left['FieldName'], 0, 4) == 'Date') {
				$viewLBData .= "<input type='text' class='datepicker' size='30' name='LB" . $lLblName . "' id='LB" . $lLblName . "' value='" . $left['Value'] . "'/>";
			} elseif (substr($left['FieldName'], 0, 6) == 'Select') {
				$accMapID = $left['AccountMapID'];
				$viewLBData .= "<select name='LB" . $lLblName . "' id='LB" . $lLblName . "'>";
				$option = current($selectOptions[$accMapID]);
				while ($option) {
					$selected = ($option['OptionID'] == $left['OptionID']) ? 'selected' : '';
					$viewLBData .= "<option value='" . $option['OptionID'] . "' " . $selected . ">" . $option['OptionName'] . "</option>";
					$option = next($selectOptions[$accMapID]);
				}
				$viewLBData .= "</select>";
			} else {
				$viewLBData .= "<input type='text' class='clsTextBox' size='30' name='LB" . $lLblName . "' id='LB" . $lLblName . "' value='" . $left['Value'] . "'/>";
			}
			$viewLBData .= "<span class='clsError' id='spn_LB" . $lLblName . "'></span></div><br>";
		}
		
		$viewLBData .= "</td>";
		
		$chkRightField = ($right['IsRequired'] == 1) ? '<span class="smlRedBold">*</span>' : '';
		$rLblName = str_replace(" ", "_", $right['LabelName']);
		
		$viewLBData .= "<td class='label'>" . $chkRightField . ((isset($right['LabelName'])) ? $right['LabelName'] . ':' : '&nbsp;') . "</td>";
		$viewLBData .= "<td class='left'>";
		
		if (isset($right['LabelName'])) {
			if (substr($right['FieldName'], 0, 4) == 'Date') {
				$viewLBData .= "<input type='text' class='datepicker' size='30' name='LB" . $rLblName . "' id='LB" . $rLblName . "' value='" . $right['Value'] . "'/>";
			} elseif (substr($right['FieldName'], 0, 6) == 'Select') {
				$accMapID = $right['AccountMapID'];
				$viewLBData .= "<select name='LB" . $rLblName . "' id='LB" . $rLblName . "' class='clsTextBox'>";
				$option = current($selectOptions[$accMapID]);
				while ($option) {
					$selected = ($option['OptionID'] == $right['OptionID']) ? 'selected' : '';
					$viewLBData .= "<option value='" . $option['OptionID'] . "' " . $selected . ">" . $option['OptionName'] . "</option>";
					$option = next($selectOptions[$accMapID]);
				}
				$viewLBData .= "</select>";
			} else {
				$viewLBData .= "<input type='text' class='clsTextBox' size='30' name='LB" . $rLblName . "' id='LB" . $rLblName . "' value='" . $right['Value'] . "'/>";
			}
			$viewLBData .= "<span class='clsError' id='spn_LB" . $rLblName . "'></span></div><br>";
		}
		
		$viewLBData .= "</td>";
		$viewLBData .= "</tr>";
		$right = next($rightPanelData);
		$left = next($leftPanelData);
	}
	
	$viewLBData .= "</table>";
	return $viewLBData;
}

/**
 * function for add new record in HTML structure
 * @param array $leftPanelData
 * @param array $rightPanelData 
 * return HTML structure add new record
 */
function addNewData($leftLables, $rightLables, $selectOptions) {
	$addNewData = "<table class='jet_datagrid'>";
	$left = current($leftLables);
	$right = current($rightLables);
	while ($right || $left) {
		$chkLeftField = ($left['IsRequired'] == 1) ? '<span class="smlRedBold">*</span>' : '';
		$lLblName = str_replace(" ", "_", $left['LabelName']);
		
		$addNewData .= "<tr><td class='label' align='left' valign='top'>" . $chkLeftField . ((isset($left['LabelName'])) ? $left['LabelName'] . ':' : '&nbsp;') . "</td>";
		$addNewData .= "<td class='left'>";
		if (isset($left['LabelName'])) {
			if (substr($left['FieldName'], 0, 4) == 'Date') {
				$addNewData .= "<input type='text' class='datepicker' size='30' name='" . $lLblName . "' id='" . $lLblName . "'/>";
			} elseif (substr($left['FieldName'], 0, 6) == 'Select') {
				$accMapID = $left['AccountMapID'];
				$addNewData .= "<select name='" . $lLblName . "' id='" . $lLblName . "' class='clsTextBox' style='width:210px;'>";
				$option = current($selectOptions[$accMapID]);
				while ($option) {
					$addNewData .= "<option value='" . $option['OptionID'] . "' >" . $option['OptionName'] . "</option>";
					$option = next($selectOptions[$accMapID]);
				}
				$addNewData .= "</select>";
			} else {
				$addNewData .= "<input type='text' class='clsTextBox' size='30' name='" . $lLblName . "' id='" . $lLblName . "'/>";
			}
			$addNewData .= "<br><span class='clsError' id='spn_" . $lLblName . "'></span></div><br>";
		}
		
		$addNewData .= "</td>";
		
		$chkRightField = ($right['IsRequired'] == 1) ? '<span class="smlRedBold">*</span>' : '';
		$rLblName = str_replace(" ", "_", $right['LabelName']);
		
		$addNewData .= "<td class='label' align='left' valign='top'>" . $chkRightField . ((isset($right['LabelName'])) ? $right['LabelName'] . ':' : '&nbsp;') . "</td>";
		$addNewData .= "<td class='left'>";
		
		if (isset($right['LabelName'])) {
			if (substr($right['FieldName'], 0, 4) == 'Date') {
				$addNewData .= "<input type='text' class='datepicker' size='30' name='" . $rLblName . "' id='" . $rLblName . "'/>";
			} elseif (substr($right['FieldName'], 0, 6) == 'Select') {
				$accMapID = $right['AccountMapID'];
				$addNewData .= "<select name='" . $rLblName . "' id='" . $rLblName . "' class='clsTextBox' style='width:210px;'>";
				$option = current($selectOptions[$accMapID]);
				while ($option) {
					$addNewData .= "<option value='" . $option['OptionID'] . "'>" . $option['OptionName'] . "</option>";
					$option = next($selectOptions[$accMapID]);
				}
				$addNewData .= "</select>";
			} else {
				$addNewData .= "<input type='text' class='clsTextBox' size='30' name='" . $rLblName . "' id='" . $rLblName . "'/>";
			}
			$addNewData .= "<br><span class='clsError' id='spn_" . $rLblName . "'></span></div><br>";
		}
		
		$addNewData .= "</td>";
		$addNewData .= "</tr>";
		$right = next($rightLables);
		$left = next($leftLables);
	}
	
	$addNewData .= "</table>";
	return $addNewData;
}

/**
 * function for the field validation for new record insert
 * return the validation status
 * @param $newData - set new datas for insert
 * @param $sessLabels - set lables for company
 */

function doValidateNewRecord($newData, $sessLables) {
	
	$status = new Status();
	$objUtil = new Util();
	foreach ($sessLables as $key => $value) {
		if ($sessLables[$key]['IsRequired'] == 1) {
			$lblName = str_replace(" ", "_", $sessLables[$key]['LabelName']);
			$valType = $sessLables[$key]['ValidationType'];
			$valName = getValidationTypeById($valType);
			$validation = $valName['ValidationName'];
			
			if (!isset($newData[$lblName]) || trim($newData[$lblName]) == "") {
				$status->addError('Can\'t be left blank.', $lblName);
			}
			if (isset($newData[$lblName]) && trim($newData[$lblName]) != '' && $validation != '' && !$objUtil->$validation($newData[$lblName])) {
				$status->addError('Enter valid data.', $lblName);
			}
		}
	}
	return $status;
}

/**
 * Function for the field validation for update record
 * return the validation status
 * @param $newData - set new datas for insert
 * @param $sessLabels - set lables for company
 */

function doValidateUpdateRecord($newData, $sessLables) {
	
	$status = new Status();
	$objUtil = new Util();
	foreach ($sessLables as $key => $value) {
		if ($sessLables[$key]['IsRequired'] == 1) {
			$FlblName = str_replace(" ", "_", $sessLables[$key]['LabelName']);
			$lblName = "LB" . $FlblName;
			$valType = $sessLables[$key]['ValidationType'];
			$valName = getValidationTypeById($valType);
			$validation = $valName['ValidationName'];
			
			if (!isset($newData[$lblName]) || trim($newData[$lblName]) == "") {
				$status->addError('Can\'t be left blank.', $lblName);
			}
			if (isset($newData[$lblName]) && trim($newData[$lblName]) != '' && $validation != '' && !$objUtil->$validation($newData[$lblName])) {
				$status->addError('Enter valid data.', $lblName);
			}
		}
	}
	return $status;
}

/**
 * Function to get Contact First Name
 * @param int $contactId - pass the contact id value
 * create fieldsMap() class object to get the First Name field
 */
function getContactFirstName($contactId) {
	
	$objFieldMap = new fieldsMap();
	$NameFields = $objFieldMap->getContactFirstNameField();
	
	$sqlName = "SELECT " . $NameFields . " AS CName FROM Contact WHERE ContactID=?";
	$sqlBuildName = SqlBuilder()->LoadSql($sqlName);
	$sqlName = $sqlBuildName->BuildSql(array(DTYPE_INT, $contactId));
	$ContactName = DbConnManager::GetDb('mpower')->GetOne($sqlName);
	$NameValue = $ContactName['CName'];
	return $NameValue;
}

/**
 * Function to get Contact Last Name
 * @param int $contactId - pass the contact id value
 * create fieldsMap() class object to get the Last Name field 
 */
function getContactLastName($contactId) {
	
	$objFieldMap = new fieldsMap();
	$NameFields = $objFieldMap->getContactLastNameField();
	
	$sqlName = "SELECT " . $NameFields . " AS CName FROM Contact WHERE ContactID=?";
	$sqlBuildName = SqlBuilder()->LoadSql($sqlName);
	$sqlName = $sqlBuildName->BuildSql(array(DTYPE_INT, $contactId));
	$ContactName = DbConnManager::GetDb('mpower')->GetOne($sqlName);
	$NameValue = $ContactName['CName'];
	return $NameValue;
}

/**
 * Function to get Account Name
 * @param int $accountId - pass the account id value
 * create fieldsMap() class object to get the Account Name field
 */
function getAccountName($accountId) {
	$objFieldMap = new fieldsMap();
	$NameField = $objFieldMap->getAccountNameField();
	
	$sqlAccountName = "SELECT " . $NameField . " FROM Account WHERE AccountID=?";
	$sqlBuildAccountName = SqlBuilder()->LoadSql($sqlAccountName);
	$sqlAccountName = $sqlBuildAccountName->BuildSql(array(DTYPE_INT, $accountId));
	$accountName = DbConnManager::GetDb('mpower')->GetOne($sqlAccountName);
	$accountNameValue = $accountName[$NameField];
	return $accountNameValue;

}
?>