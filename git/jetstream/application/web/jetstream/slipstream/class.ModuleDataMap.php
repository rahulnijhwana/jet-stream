<?php
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';


class ModuleDataMap extends JetstreamModule
{
	public $title = 'Import';
	public $sidebar = false;
	
	protected $css_files = array('import.css', 'search.css');
	protected $javascript_files = array('jquery.js', 'jquery.blockUI.js', 'slipstream_search.js', 'jquery.form.js', 'jquery.validate.js', 'import_data_map.js');
	
	public function Init() {		
		/**
		 * include tpl file for HTML section.
		 */

		$this->AddTemplate('module_data_map.tpl');
	}
	
}