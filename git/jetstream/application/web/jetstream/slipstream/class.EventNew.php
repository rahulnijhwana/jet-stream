<?php
// require_once BASE_PATH . '/include/class.DbConnManager.php';
// require_once BASE_PATH . '/include/class.SqlBuilder.php';

require_once BASE_PATH . '../include/class.RecEvent.php';

class EventNew
{
	public $company_id;
	public $user_id;
	public $limit_company_access;

	function __construct($company_id = -1, $user_id = -1, $limit_company_access = -1) {
		$this->company_id = ($company_id == -1 && isset($_SESSION['company_obj']['CompanyID'])) ? $_SESSION['company_obj']['CompanyID'] : $company_id;
		$this->limit_company_access = ($limit_company_access == -1) ? (isset($_SESSION['company_obj']['LimitAccessToUnassignedUsers']) && ($_SESSION['company_obj']['LimitAccessToUnassignedUsers'])) : $limit_company_access;
		$this->user_id = ($user_id == -1 && isset($_SESSION['USER']['USERID'])) ? $_SESSION['USER']['USERID'] : $user_id;
	}

	function GetEvent($id) {
			$record_sql = "SELECT * FROM Events WHERE EventID = ?";
			$record_sql = SqlBuilder()->LoadSql($record_sql)->BuildSql(array(DTYPE_INT, array($id)));

			// $record_sql = "SELECT * FROM Event WHERE EventID = ? AND CompanyID = ?";
			// $record_sql = SqlBuilder()->LoadSql($record_sql)->BuildSql(array(DTYPE_INT, array($id, $this->company_id)));

			// echo $record_sql . '<br>';
			$recordset = DbConnManager::GetDb('mpower')->Execute($record_sql, 'RecEvent');
			
			$recordset->Initialize();
			
			return $recordset;
	}
}

