<?php

require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.EmailIncoming.php';

class ModuleEmailIncoming extends JetstreamModule
{
	public $title = 'Email Incoming';
	protected $javascript_files = array('contact.js', 'ui.datepicker.js', 'jquery.cookie.js', 'overlay.js', 'email_hold.js');
	protected $css_files = array('jet_datagrid.css', 'ui.datepicker.css', 'overlay.css', 'note_print.css','email_hold.css', 'note.css');	
	
	public $email_incoming;
	public $contact_hint_list;
	public $contact_email_addresses;	

	public function Init() {
		
		/**
		 * Create EmailIncoming() class object.
		 */
		$obj_email_incoming = new EmailIncoming();

//echo '<pre>'; print_r($obj_email_incoming); echo '</pre>'; exit;
	

		$this->email_incoming = $obj_email_incoming->GetEmailIncoming($_SESSION['login_id']);
		
		/**
		 * include tpl file for HTML section.
		 */
		$this->AddTemplate('module_email_incoming.tpl');
//		$this->AddTemplate('snippet_note_list.tpl');
	}
}