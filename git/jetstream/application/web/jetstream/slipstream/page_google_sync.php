<?php

require_once BASE_PATH . '/slipstream/class.JetstreamPage.php';
/*
 * Module class is actual controller for one module
 */

require_once BASE_PATH . '/slipstream/class.ModuleGoogleSync.php';
require 'smarty/configs/slipstream.php';

$page = new JetstreamPage();

//$company_id = $_SESSION['company_id'];
$company_id = $_SESSION['company_obj']['CompanyID'];
$user_id = $_SESSION['USER']['USERID'];

$moduleGoogleSync = new ModuleGoogleSync($company_id,$user_id);

$data = $moduleGoogleSync->isGoogleSync($user_id);
		
$page->AddModule($moduleGoogleSync);
$page->Render();
