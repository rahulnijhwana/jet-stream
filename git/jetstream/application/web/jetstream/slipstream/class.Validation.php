<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once 'class.Status.php';

class Validation
{
	/*
		ValidationType and validationId are predeifined.

	*/


	function getValidationIdByName($fieldType){
		switch($fieldType){
			case "Phone":
				$validationId = '1';
				break;

			case "Number":
				$validationId = '2';
				break;

			case "Text":
				$validationId = '3';
				break;

			case "Url":
				$validationId = '4';
				break;

			case "Email":
				$validationId = '5';
				break;
			case "LongText":
				$validationId = '6';
				break;

		}
		return $validationId;
	}

	function getFieldTypeByValidationId($validationId){
	$fieldType = 'Text';
		switch($validationId){
			case "1":
				$fieldType = 'Phone';
				break;

			case "2":
				$fieldType = 'Number';
				break;

			case "3":
				$fieldType = 'Text';
				break;

			case "4":
				$fieldType = 'Url';
				break;

			case "5":
				$fieldType = 'Email';
				break;

				$fieldType = 'LongText';
			case "6":
				break;

		}
		return $fieldType;
	}
}



?>