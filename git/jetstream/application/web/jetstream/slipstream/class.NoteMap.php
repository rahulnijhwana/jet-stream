<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once 'class.Status.php';

class NoteMap extends MpRecord
{

	public function __toString()
	{
		return 'Note Map Record';
	}

	public function Initialize()
	{
		$this->db_table = 'NoteMap';
		$this->recordset->GetFieldDef('NoteMapID')->auto_key = TRUE;
		parent::Initialize();
	}
	
	public function saveNoteFields($compId,$fieldType,$fieldName,$tempId){
		$status = new Status();
		$sql = "SELECT * FROM NoteMap WHERE NoteMapID = -1";
		$rs = DbConnManager::GetDb('mpower')->Execute($sql);
		$this->SetDatabase(DbConnManager::GetDb('mpower'));
		$this->recordset = $rs;
		$this->Initialize();
		$this->CompanyID = $compId;
		$this->FieldType = $fieldType;
		$this->LabelName = $fieldName;
		$this->NoteTemplateID = $tempId;
		$this->Save();

		$selectSql = "select Max(NoteMapID) as NoteMapID from NoteMap where CompanyID=? AND FieldType=? AND LabelName=?";
		$sqlBuild = SqlBuilder()->LoadSql($selectSql);
		$selectSql = $sqlBuild->BuildSql(array(DTYPE_INT, $compId),array(DTYPE_STRING, $fieldType),array(DTYPE_STRING, $fieldName));
		$noteMapID = DbConnManager::GetDb('mpower')->Exec($selectSql);
		
		return $noteMapID;
	}
	
	function getAllNoteFieldsByCompanyId($companyId)
	{
		$sql = "select NoteMapID,FieldType,LabelName from NoteMap where CompanyID = ? AND Deleted = 0 AND FormType = -1 ";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId));
		$noteFields = DbConnManager::GetDb('mpower')->Exec($sql);
		return $noteFields;
	}
	
	function getNoteFieldDetailsByFieldId($noteFieldId,$companyId){
		$noteOptions = array();
		$sql = "select NoteMapID,FieldType,LabelName from NoteMap where CompanyID = ? AND NoteMapID = ? ";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId),array(DTYPE_INT, $noteFieldId));
		$noteFields = DbConnManager::GetDb('mpower')->Exec($sql);
		
		if($noteFields[0]['FieldType'] == 'DropDown' || $noteFields[0]['FieldType'] == 'RadioButton'){
			$sql = "select * from NoteOption where NoteMapID = ?";
			$sqlBuild = SqlBuilder()->LoadSql($sql);
			$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $noteFieldId));
			$noteOptions = DbConnManager::GetDb('mpower')->Exec($sql);
		}
		$noteFields[0]['NoteOptions'] = $noteOptions;
		return $noteFields;
	}
	
	function updateNoteFieldDetailsByMapId($noteMapId,$companyId,$labelName,$fieldType){
		$status = new Status();
		$sql = "SELECT * FROM NoteMap WHERE NoteMapID = ? AND CompanyID = ?";
		$noteMapSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $noteMapId),array(DTYPE_INT, $companyId));
		$result = DbConnManager::GetDb('mpower')->Execute($noteMapSql, 'NoteMap');
		if (count($result) == 1) {
			$objNoteMap = $result[0];
			$objNoteMap->Initialize();
			$objNoteMap->LabelName = $labelName;
			$objNoteMap->FieldType = $fieldType;
			$result->Save();
		}
		return $status;
	}
	
	function deleteNoteFieldByFieldId($noteMapId,$companyId){
		$status = new Status();
		$sql = "SELECT * FROM NoteMap WHERE NoteMapID = ? AND CompanyID = ?";
		$noteMapSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $noteMapId),array(DTYPE_INT, $companyId));
		$result = DbConnManager::GetDb('mpower')->Execute($noteMapSql, 'NoteMap');
		if (count($result) == 1) {
			$objNoteMap = $result[0];
			$objNoteMap->Initialize();
			$objNoteMap->Deleted = true;
			$result->Save();
		}
		return $status;
	}
	
	function getNoteFieldByFormTypeAndCompId($formType,$companyId)
	{
		$sql = "select NoteMapID,FieldType,LabelName from NoteMap where CompanyID = ? AND Deleted = 0 AND FormType = ? ";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId),array(DTYPE_INT, $formType));
		$noteFields = DbConnManager::GetDb('mpower')->Exec($sql);
		return $noteFields;
	}
	
	function getAllFieldsByTemplateId($tempId,$companyId)
	{
		$sql = "select NoteMapID,FieldType,LabelName from NoteMap where CompanyID = ? AND Deleted = 0 AND NoteTemplateID = ? ";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId),array(DTYPE_INT, $tempId));
		$noteFields = DbConnManager::GetDb('mpower')->Exec($sql);
		return $noteFields;
	}
	
	
}

?>