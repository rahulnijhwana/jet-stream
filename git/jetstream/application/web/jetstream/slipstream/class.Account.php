<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once 'class.Status.php';

class Account extends MpRecord
{

	public function __toString()
	{
		return 'Account';
	}
	public function Initialize()
	{
		$this->db_table = 'Account';
		$this->recordset->GetFieldDef('AccountID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('CompanyID')->required = TRUE;
		parent::Initialize();
	}

	public function Save($simulate = false) {
		parent::Save($simulate);
		if (!$simulate) {
			
			$sql = "DELETE FROM DashboardSearch WHERE RecType = 1 and RecID = $this->AccountID";
			DbConnManager::GetDb('mpower')->Exec($sql);
			
			if (empty($this->Deleted)) {
				$active = ($this->Inactive == 0) ? 1 : 0;
				$an_field = MapLookup::GetAccountNameField($this->CompanyID, true);
				
				$search_text = $this->$an_field;
				
				$sql = "INSERT INTO DashboardSearch (CompanyID, RecType, RecID, SearchText, IsActive) 
					VALUES ($this->CompanyID, 1, $this->AccountID, ?, $active)";
				$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $search_text));
				DbConnManager::GetDb('mpower')->Exec($sql);
			}
		}
	}	
	
	function getFieldsFromAccount($type)
	{
		$searchCriteria = $type.'%';
		$sql="select COLUMN_NAME as FieldName from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='Account' and COLUMN_NAME like ?";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_STRING, $searchCriteria));
		$fieldNames = DbConnManager::GetDb('mpower')->Exec($sql);
		return $fieldNames;
	}

	function IsInactiveAccount($accountId){
		$sql = "select Inactive from Account where AccountID = ? ";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $accountId));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		return (isset($result[0]['Inactive']) && $result[0]['Inactive'] == 1) ? TRUE : FALSE;
	}
	
	function activateAccount($accountId)
	{
		$status = new Status();
		$sql = "SELECT * FROM Account WHERE AccountID = ?";
		$accountSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $accountId));
		$result = DbConnManager::GetDb('mpower')->Execute($accountSql, 'Account');
		if (count($result) == 1) {
			$objAccount = $result[0];
			$objAccount->Initialize();
			$objAccount->Inactive = 0;
			$result->Save();
		}
		$sql = "update Contact set Inactive = 0 WHERE AccountID = ?";
		$contactSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $accountId));
		$conResult = DbConnManager::GetDb('mpower')->Execute($contactSql, 'Contact');
		return $status;
	}
	function IsRestricted($account_id,$user_id,$company_id)
	{

		$sql="SELECT CompanyID from Account where AccountID=?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $account_id));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		if(count($result)>0)
		{
			if($result[0]['CompanyID']!=$company_id)
				return true;
		}		
		$sql="SELECT Account.PrivateAccount, PeopleAccount.PersonID FROM Account LEFT OUTER JOIN PeopleAccount ON Account.AccountID = PeopleAccount.AccountID WHERE (Account.CompanyID = ?) AND (Account.AccountID = ?)";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_id),array(DTYPE_INT, $account_id));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		//echo $contact_id;
		//print_r($result);
		if(count($result)>0)
		{
			if($result[0]['PrivateAccount']==1 && $result[0]['PersonID']!=$user_id)
				return true;
			else
				return false;
		}		
		return false;
		
	}	

}



?>