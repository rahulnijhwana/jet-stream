<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.MpRecordset.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.JetCache.php';

/**
 * Caches the EventType table data to the session
 *
 */
class EventTypeLookup
{
	protected static $lookup_data = array();
	protected static $cache_name = 'eventtype';

	public static function Load($company_id) {
		if (!in_array($company_id, self::$lookup_data)) {
			if (!$load_array = JetCache::Load($company_id, self::$cache_name)) {
				$load_array = self::LoadFromDb($company_id);
			}
			self::$lookup_data[$company_id] = $load_array;
		}
		return self::$lookup_data[$company_id];
	}

	/*
	 * Loads event_types from the database
	 * Can be called from anywhere to reload the event_types
	 */
	public static function LoadFromDb($company_id) {
		$sql = 'SELECT * FROM EventType WHERE CompanyID = ?';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_id));
		$eventtypes_result = DbConnManager::GetDb('mpower')->Exec($sql, 'MPRecord', 'EventTypeID');
		$eventtypes = array();
		foreach ($eventtypes_result as $eventtype) {
			$eventtypes[$eventtype['EventTypeID']] = $eventtype;
		}
		JetCache::Save($company_id, self::$cache_name, $eventtypes);
		return $eventtypes;
	}

	/*
	 * Get a single record by it's eventtypeid
	 */
	public static function GetRecord($company_id, $id) {
		$eventtypes = self::Load($company_id);
		return (isset($eventtypes[$id])) ? $eventtypes[$id] : FALSE;
	}

	/*
	 * Returns the entire eventtype recordset
	 */
	public static function GetAllRecords($company_id, $include_deleted = false) {
		$event_types = self::Load($company_id);
		if ($include_deleted) {
			return $event_types;
		}
		$output = array();
		foreach ($event_types as $event_type_id => $event_type) {
			if (!$event_type['Deleted']) {
				$output[$event_type_id] = $event_type;
			}
		}
		return $output;
	}
	
	/*
	 * Returns all the entries (filtered) for use in select boxes
	 */
	public static function GetSelection($company_id, $event_opp_type) {
		$eventtypes = self::Load($company_id);
		$eventtype_list = array();
		foreach ($eventtypes as $eventtype) {
			if ($eventtype['Deleted'] != 1) {				
				if($event_opp_type) {
					if(strtolower($eventtype['Type']) == 'event') {
						$eventtype_list[$eventtype['Type'].'_'.$eventtype['EventTypeID']] = $eventtype['EventName'];
					}
				} else {
					$eventtype_list[$eventtype['Type'].'_'.$eventtype['EventTypeID']] = $eventtype['EventName'];
				}
			}
		}
		return $eventtype_list;
	}

}
