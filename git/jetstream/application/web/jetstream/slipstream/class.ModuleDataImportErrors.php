<?php
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';

/**
 * Class for Data Import module
 */
class ModuleDataImportErrors extends JetstreamModule
{
	public $title = 'Import Errors';
	public $sidebar = false;
	protected $css_files = array('import.css');
	
	
	public function Init() {		
		/**
		 * include tpl file for HTML section.
		 */

		$this->AddTemplate('module_data_import_errors.tpl');
	}
	
}