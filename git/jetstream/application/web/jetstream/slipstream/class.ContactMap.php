<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once 'class.Status.php';

class ContactMap extends MpRecord
{

	public function __toString() {
		return 'Contact Map Record';
	}

	public function Initialize() {
		$this->db_table = 'ContactMap';
		$this->recordset->GetFieldDef('ContactMapID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('CompanyID')->required = TRUE;
		$this->recordset->GetFieldDef('LabelName')->required = TRUE;
		$this->recordset->GetFieldDef('FieldName')->required = TRUE;
		// $this->recordset->GetFieldDef('IsFirstName')->required = FALSE;
		// $this->recordset->GetFieldDef('IsRequired')->required = FALSE;
		// $this->recordset->GetFieldDef('OptionType')->required = FALSE;
		// $this->recordset->GetFieldDef('ValidationType')->required = FALSE;
		// $this->recordset->GetFieldDef('IsLastName')->required = FALSE;
		// $this->recordset->GetFieldDef('Align')->required = FALSE;
		// $this->recordset->GetFieldDef('Enable')->required = FALSE;
		// $this->recordset->GetFieldDef('KeywordId')->required = TRUE;
		parent::Initialize();
	}
/*Jet-37*/
	function insertContactFieldsToMap($companyId, $fieldName, $labelName, $isMandatory, $optionType, $validationType, $isFirstName, $isLastName, $align, $optionSetId, $fieldAccountMap, $keywordId, $basicSearch, $parameters, $isPrivateField,$isPrivateField=false,$googleField) {
		
		$status = new Status();
		
		$sql = "SELECT * FROM ContactMap WHERE ContactMapID =-1";
		$rs = DbConnManager::GetDb('mpower')->Execute($sql);
		$this->SetDatabase(DbConnManager::GetDb('mpower'));
		$this->recordset = $rs;
		$this->Initialize();
		$this->CompanyID = $companyId;
		$this->FieldName = $fieldName;
		$this->LabelName = $labelName;
		$this->IsRequired = $isMandatory;
		$this->OptionType = $optionType;
		$this->ValidationType = $validationType;
		$this->IsFirstName = $isFirstName;
		$this->IsLastName = $isLastName;
		$this->Align = $align;
		$this->OptionSetID = $optionSetId;
		$this->AccountMapID = $fieldAccountMap;
		$this->BasicSearch = $basicSearch;
		$this->Parameters = $parameters;
		$this->IsPrivate = $isPrivateField;
		$this->KeywordId = $keywordId;
		/*Jet-37*/
		$this->googleField = $googleField;
		/*Jet-37*/
// GMS 9/13/2010
		$this->IsContactTitle = $isContactTitle;
		$this->Save();
		return $status;
	}
/*Jet-37*/
	function getContactMapIdByCompanyAndField($companyId, $fieldName) {
		$sql = "select ContactMapID from ContactMap where CompanyID = ? and FieldName = ?";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId), array(DTYPE_STRING, $fieldName));
		$mapId = DbConnManager::GetDb('mpower')->Exec($sql);
		return $mapId;
	}
// GMS 9/13/2010
/*Jet-37*/
	function updateContactMapById($mapId, $labelName, $isRequired, $isFirstName, $isLastName, $fieldAccountMapId, $keywordId, $basicSearch, $tabOrder, $isPrivateField, $isContactTitle,$googleField) {
		$status = new Status();
		$sql = "SELECT * FROM ContactMap WHERE ContactMapID = ?";
		$accountMapSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $mapId));
		$result = DbConnManager::GetDb('mpower')->Execute($accountMapSql, 'ContactMap');
		
		if (count($result) == 1) {	
			$objContact = $result[0];
			$objContact->Initialize();
			$objContact->LabelName = $labelName;
		/*Jet-37*/
			$objContact->googleField = $googleField;
			/*Jet-37*/
			$objContact->IsFirstName = $isFirstName;
			$objContact->IsLastName = $isLastName;
			$objContact->IsRequired = $isRequired;
			$objContact->AccountMapID = $fieldAccountMapId;
			$objContact->BasicSearch = $basicSearch;
			$objContact->TabOrder = $tabOrder;
			$objContact->IsPrivate = $isPrivateField;
			$objContact->KeywordId = $keywordId;
// GMS 9/13/2010
			$objContact->IsContactTitle = $isContactTitle;	
			$result->Save();
		}
		return $status;
	
	}
/*Jet-37*/
	function getContactMapById($companyId) {
		$sql = 'SELECT ContactMapID,FieldName,LabelName,BasicSearch,IsRequired,IsFirstName,IsLastName,ValidationType,Position,Align,IsPrivate,SectionID, KeywordId FROM ContactMap WHERE CompanyID= ?';
		$contactSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $companyId));
		$contactMap = DbConnManager::GetDb('mpower')->Exec($contactSql);
		
		$_SESSION['USER']['CONTACTMAP'] = $contactMap;
		return $contactMap;
	}

	//Fetch different field values and validationName from ContactMap and Validation table for company
	function getFieldsFromContactMapByCompId($companyId, $criteria) {
		$result = array();
		$searchCriteria = $criteria . '%';
		$sql = "SELECT ContactMapID,FieldName,LabelName,IsFirstName,IsLastName,IsRequired,OptionType,ValidationType,Align,Enable,OptionSetID,SectionID,Position,AccountMapID, TabOrder, IsPrivate, KeywordId FROM ContactMap where CompanyID= ? and FieldName Like ? Order By FieldName";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId), array(DTYPE_STRING, $searchCriteria));
		$fieldNames = DbConnManager::GetDb('mpower')->Exec($sql);
		for($i = 0; $i < count($fieldNames); $i++) {
			$validationType = $fieldNames[$i]['ValidationType'];
			
			if ($validationType != '' || $validationType != NULL) {
				
				$sql = "SELECT ValidationName FROM Validation where ValidationID= ? ";
				$sqlBuild = SqlBuilder()->LoadSql($sql);
				$sql = $sqlBuild->BuildSql(array(DTYPE_STRING, $validationType));
				$validationName = DbConnManager::GetDb('mpower')->Exec($sql);
				if (isset($validationName[0]['ValidationName'])) {
					$fieldNames[$i]['ValidationName'] = $validationName[0]['ValidationName'];
				} else {
					$fieldNames[$i]['ValidationName'] = '-1';
				}
			} else {
				$fieldNames[$i]['ValidationName'] = '-1';
			}
		}
		if (count($fieldNames) > 0) $result = $fieldNames;
		
		return $result;
	}

	function updateFieldPositionByContactMapId($align, $position, $sectionId, $contactMapId, $enable, $tabOrder) {
		$status = new Status();
		$sql = "SELECT * FROM ContactMap WHERE ContactMapID = ?";
		$accountMapSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $contactMapId));
		$result = DbConnManager::GetDb('mpower')->Execute($accountMapSql, 'ContactMap');
		if (count($result) == 1) {
			$objContact = $result[0];
			$objContact->Initialize();
			$objContact->Align = $align;
			$objContact->Position = $position;
			$objContact->SectionID = $sectionId;
			$objContact->Enable = $enable;
			if ($tabOrder) $objContact->TabOrder = NULL;
			
			$result->Save();
		}
		return $status;
	}
// GMS 10/1/2010
/*Start Jet-37*/
	function getContactMapDetailsById($contactMapId) {
		$sql = 'SELECT ContactMapID,FieldName,LabelName,ValidationType,OptionSetID,BasicSearch,IsFirstName,IsLastName,AccountMapID,OptionType,IsRequired,TabOrder,Parameters, IsPrivate, KeywordId, IsContactTitle,googleField,(Select googleFieldName from googleFields where googleField = ContactMap.googleField) as googleFieldName FROM ContactMap WHERE ContactMapID= ?';
		$contactSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $contactMapId));
		$contactMap = DbConnManager::GetDb('mpower')->Exec($contactSql);
		return $contactMap;
	}
/*End Jet-37*/
	function updateOptionSetIdOfContactMapFields($contactMapId) {
		$status = new Status();
		$sql = "SELECT * FROM ContactMap WHERE ContactMapID = ?";
		$contactMapSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $contactMapId));
		$result = DbConnManager::GetDb('mpower')->Execute($contactMapSql, 'ContactMap');
		if (count($result) == 1) {
			$objContact = $result[0];
			$objContact->Initialize();
			$objContact->OptionSetID = NULL;
			$result->Save();
		}
		return $status;
	}

	function getMapIdBySectionId($sectionId) {
		$sql = 'SELECT ContactMapID,LabelName,FieldName FROM ContactMap WHERE SectionID = ? ';
		$contactSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $sectionId));
		$contactMap = DbConnManager::GetDb('mpower')->Exec($contactSql);
		return $contactMap;
	}

	function updateContactMapSectionId($mapId) {
		$status = new Status();
		$sql = "SELECT * FROM ContactMap WHERE ContactMapID = ?";
		$contactMapSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $mapId));
		$result = DbConnManager::GetDb('mpower')->Execute($contactMapSql, 'ContactMap');
		if (count($result) == 1) {
			$objContact = $result[0];
			$objContact->Initialize();
			$objContact->SectionID = NULL;
			$result->Save();
		}
		return $status;
	
	}

	function updateContactFieldsTabOrderById($mapId, $tabOrder) {
		$status = new Status();
		$sql = "SELECT * FROM ContactMap WHERE ContactMapID = ?";
		$contactMapSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $mapId));
		$result = DbConnManager::GetDb('mpower')->Execute($contactMapSql, 'ContactMap');
		if (count($result) == 1) {
			$objContact = $result[0];
			$objContact->Initialize();
			$objContact->TabOrder = $tabOrder;
			$result->Save();
		}
		return $tabOrder;
	}

	function checkIsFirstNameSet($companyId, $mapId) {
		$sql = 'SELECT IsFirstName,ContactMapID FROM ContactMap WHERE CompanyID = ? ';
		$contactSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyId));
		$contactMap = DbConnManager::GetDb('mpower')->Exec($contactSql);
		foreach ($contactMap as $contact) {
			if ($contact['IsFirstName'] == 1 && $contact['ContactMapID'] != $mapId) return true;
		}
		return false;
	}

	function checkIsLastNameSet($companyId, $mapId) {
		$sql = 'SELECT IsLastName,ContactMapID FROM ContactMap WHERE CompanyID = ? ';
		$contactSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyId));
		$contactMap = DbConnManager::GetDb('mpower')->Exec($contactSql);
		foreach ($contactMap as $contact) {
			if ($contact['IsLastName'] == 1 && $contact['ContactMapID'] != $mapId) return true;
		}
		return false;
	}
// GMS 10/1/2010
	function checkIsContactTitleSet($companyId, $mapId) {
		$sql = 'SELECT IsContactTitle,ContactMapID FROM ContactMap WHERE CompanyID = ? ';
		$contactSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyId));
		$contactMap = DbConnManager::GetDb('mpower')->Exec($contactSql);
		foreach ($contactMap as $contact) {
			if ($contact['IsContactTitle'] == 1 && $contact['ContactMapID'] != $mapId) return true;
		}
		return false;
	}
	
	function getMaxContactFieldValues($companyId) {
		$sql = 'SELECT LabelName,ContactMapID FROM ContactMap WHERE ContactMapID = (Select Max(ContactMapID) from ContactMap where CompanyID = ?)';
		$contactSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyId));
		$contactMap = DbConnManager::GetDb('mpower')->Exec($contactSql);
		return $contactMap;
	}

	function getMapIdAndFieldNameByCompanyId($companyId) {
		$sql = 'SELECT LabelName,ContactMapID FROM ContactMap WHERE CompanyID = ?';
		$contactSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyId));
		$contactMap = DbConnManager::GetDb('mpower')->Exec($contactSql);
		return $contactMap;
	}
	
	function getFieldsFromVcardKeywordMap( $companyId, $criteria )
	{
		$result = array( );
		
		$sql = "SELECT KeywordId, Keyword FROM VcardKeywordMapping";
		if( $criteria == 1 )
		{
			$sql.= " WHERE KeywordId NOT IN ( SELECT KeywordId FROM ContactMap WHERE CompanyID = ? ) ORDER BY KeywordId ASC";
		
			$keywordSql = SqlBuilder( )->LoadSql( $sql )->BuildSql( array( DTYPE_INT, $companyId ) );
			$sql = $keywordSql;
		}
		else
		{
			$sql .= " ORDER BY KeywordId ASC";
		}
		$keywords = DbConnManager::GetDb('mpower')->Exec( $sql );
		
		//print_r( $keywords ); exit( );
		return $keywords;
	}
	
	function getDeleteFields($companyId) {
		$sql = 'SELECT LabelName,ContactMapID FROM ContactMap WHERE CompanyID = ? AND SectionID IS NULL';
		$contactSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyId));
		$contactMap = DbConnManager::GetDb('mpower')->Exec($contactSql);
		return $contactMap;
	}

	function deleteContactFieldById($contactMapId) {
		$status = new Status();
		$sql = 'DELETE FROM ContactMap WHERE ContactMapID = ?';
		$contactSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $contactMapId));
		$contactMap = DbConnManager::GetDb('mpower')->Exec($contactSql);
		return $status;
	}

	function getFieldTypeAndParametersByMapId($mapId) {
		$sql = 'SELECT FieldName,Parameters,ContactMapId as MapId FROM ContactMap WHERE ContactMapID = ?';
		$contactSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $mapId));
		$contactMap = DbConnManager::GetDb('mpower')->Exec($contactSql);
		return $contactMap;
	}

	function updateFieldParametersByMapId($mapId, $parameters) {
		$status = new Status();
		$sql = "update ContactMap set Parameters = ? WHERE ContactMapID = ?";
		$contactSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $parameters), array(DTYPE_INT, $mapId));
		$contactMap = DbConnManager::GetDb('mpower')->Execute($contactSql, 'ContactMap');
		return $status;
	}

	function updateValidationIdByMapId($mapId, $validationId) {
		$status = new Status();
		$sql = "update ContactMap set ValidationType = ? WHERE ContactMapID = ?";
		$contactSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $validationId), array(DTYPE_INT, $mapId));
		$contactMap = DbConnManager::GetDb('mpower')->Execute($contactSql, 'ContactMap');
		return $status;
	}

	function updateOptionTypeByMapId($mapId, $optionType) {
		$status = new Status();
		$sql = "update ContactMap set OptionType = ? WHERE ContactMapID = ?";
		$contactSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $optionType), array(DTYPE_INT, $mapId));
		$contactMap = DbConnManager::GetDb('mpower')->Execute($contactSql, 'ContactMap');
		return $status;
	}

	function countContactFieldsBySectionAlign($compId, $secId, $align, $position) {
		$sql = "Select count(*) as count from ContactMap where CompanyId = ? AND SectionID = ? AND Align = ? AND Position = ?";
		$contactSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $compId), array(DTYPE_INT, $secId), array(DTYPE_INT, $align), array(DTYPE_INT, $position));
		$fieldCount = DbConnManager::GetDb('mpower')->Exec($contactSql);
		return $fieldCount[0];
	}

	function getContactMaxPositionBySecId($secId, $compId) {
		$sql = "select MAX(Position) as MaxPosition from ContactMap where CompanyID = ? AND SectionID = ?";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $compId), array(DTYPE_INT, $secId));
		$position = DbConnManager::GetDb('mpower')->Exec($sql);
		return $position[0]['MaxPosition'];
	}
}

?>
