<html>
	<body>
		<div style="font-family:Arial,sans-serif;font-size:1.2em;width:100%;text-align:center;padding-top:100px;">
			Please wait for a moment while Jetstream performs a one time update of your data.  Thank you for your patience.  If you see this message more than once, please contact ASA at (773) 714-8100.
		</div>
	</body>
</html>

<?php
if (isset($_GET['upd'])) {
	require_once BASE_PATH . '/slipstream/class.ModuleEventNew.php';
	require_once BASE_PATH . '/include/class.DbConnManager.php';
	require_once BASE_PATH . '/include/class.SqlBuilder.php';
	
	set_time_limit(180);
	
	$event = new ModuleEventNew();
	$event->ConvertEventTimes();
	
	$sql = "UPDATE People SET UtcUpdate = 1 WHERE PersonID = ?";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['USERID']));
	DbConnManager::GetDb('mpower')->Exec($sql);
	
	$_SESSION['USER']['UTC_UPDATE'] = 1;
	
	echo '<script language="Javascript">location.href="slipstream.php"</script>';
} else {
	echo '<script language="Javascript">location.href="slipstream.php?upd=utc"</script>';
}
	