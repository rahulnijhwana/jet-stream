<?php

if (!defined('BASE_PATH')) {
	define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));
}

require_once BASE_PATH . '/include/class.MapLookup.php';
require_once BASE_PATH . '/include/class.OptionLookup.php';
require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';
require_once BASE_PATH . '/include/class.DateUtil.php';
require_once BASE_PATH . '/include/class.FormUtil.php';
require_once 'class.Assignment.php';
require_once 'class.JetstreamModule.php';
require_once 'class.CompanyContactNew.php';

class ModuleCompanyContactNew extends JetstreamModule
{
	protected $css_files = array('acc_con_form.css', 'ui.datepicker.css');
	protected $javascript_files = array('contact.js', 'ui.datepicker.js', 'jquery.cookie.js');
	protected $template_files;
	public $sidebar = FALSE;
	public $layout;
	public $map;
	public $type; // contact or account
	public $subnav = array();
	public $limit_access = TRUE;
	public $limit_validation = FALSE;
	public $created;
	public $id = "comp_cont";
	public $submit_to = "ajax.CompanyContact.php";

	private $partial = false;
	
	public $company_id;
	public $user_id;

	function __construct($company_id = -1, $user_id = -1) {
		$this->company_id = ($company_id == -1) ? $_SESSION['company_obj']['CompanyID'] : $company_id;
		$this->user_id = ($user_id == -1) ? $_SESSION['USER']['USERID'] : $user_id;
	}

	public function Init() {
		return;
	}

	public function Draw() {
		return;
	}

	public function DrawForm() {
		return;
	}

	public function Build($rec_type, $rec_id, $action = 'display', $limit_access = FALSE, $prepop = FALSE, $get_values = array() ) {

        /*
		echo '<pre>'; print_r($rec_type); echo '</pre>';
		echo '<pre>'; print_r($rec_id); echo '</pre>';
		echo '<pre>'; print_r($action); echo '</pre>';
		echo '<pre>'; print_r($limit_access); echo '</pre>';
		echo '<pre>'; print_r($prepop); echo '</pre>';
		echo '<pre>'; print_r($get_values); echo '</pre>'; exit;
        */

		$record = new CompanyContactNew($this->company_id, $this->user_id);
		$this->created = ReformatDate(DateUtil::getCreatedDate($rec_type, $rec_id));
		
		// Set the record pointer for the current record
		$search_results = isset($_SESSION['search_results']) ? $_SESSION['search_results']: '' ;
		$rec_ptr = 0;
		$xptr = 0;
		if ('' != $search_results ) {
			foreach ($search_results as $result) {
				if ($result['UniqueID']==$rec_type.'_'.$rec_id) {
					//$rec_ptr = $result['RowNumber']-1;  
					$rec_ptr = $xptr;
				}
				$xptr++;
			}
		}
		
		//echo "<pre>Record pointer is ".$rec_ptr."</pre>";
		//echo "<pre>";print_r($search_results);"</pre>" ;
		
		/*
		 * If a user expands an Account record from the dashboard, an ajax
		 * call will be passing 'default_only' to only display the 'DefaultSection'
		 * This will result in a partial load, thus we do not want to set the
		 * window.this_account_id variable (see module_company_contact_new.tpl bottom)
		 */
		$this->partial = ($action == 'default_only') ? true : false;

		if ($rec_type == 'contact') {
			
			$this->type = 'contact';
			$this->title = 'Contact Information';
			$this->layout = MapLookup::GetContactLayout($this->company_id);
			// If they only want the default section do surgery on the layout
			// before requesting data so the response will be quicker
										
			if ($action == 'default_only'){
				$this->DefaultOnly();
			}
			
			if ($action == 'add') {
				$this->map = MapLookup::GetContactMap($this->company_id);
				if ($prepop) {
					// The request is to create a contact based on a company record, so
					// we must pre-populate the contact record based on the company fields
					$this->map = $record->PopulateMapValues($rec_id, $this->map, TRUE);
				}
				$this->map = $this->PopulateDefaults($this->map);
			
				if ( count($get_values) > 0 ){
					$this->map = $this->PopulateGetValues($this->map, $get_values) ; 
				} 
			}
			else {
					
				$this->map = $record->PopulateMapValues($rec_id, MapLookup::GetContactMap($this->company_id));

				$this->content_title = $this->map['fields'][MapLookup::GetContactFirstNameField($this->company_id)]['value'] . " " . $this->map['fields'][MapLookup::GetContactLastNameField($this->company_id)]['value'];
	
				if (isset($this->map['account']['name'])) {
					$this->content_sub_title = $this->map['account']['name'];
				}
			}
		} elseif ($rec_type == 'accountcontact') {
			$this->type = $rec_type;
			$this->title = 'Contact Information';
			$this->layout = MapLookup::GetContactLayout($this->company_id);
			
			// If they only want the default section do surgery on the layout
			// before requesting data so the response will be quicker
			
			if ($action == 'default_only'){
				$this->DefaultOnly();
			}
			
			if ($action == 'add') {
				$this->map = MapLookup::GetContactMap($this->company_id);
				if ($prepop) {
					// The request is to create a contact based on a company record, so
					// we must pre-populate the contact record based on the company fields
					$this->map = $record->PopulateMapValues($rec_id, $this->map, TRUE);
				}
				$this->map = $this->PopulateDefaults($this->map);
			
				if ( count($get_values) > 0 ){
					$this->map = $this->PopulateGetValues($this->map, $get_values) ; 
				} 
			}
			else {
				$this->map = $record->PopulateMapValues($rec_id, MapLookup::GetContactMap($this->company_id));
				
				$this->content_title = $this->map['fields'][MapLookup::GetContactFirstNameField($this->company_id)]['value'] . " " . $this->map['fields'][MapLookup::GetContactLastNameField($this->company_id)]['value'];
				if (isset($this->map['account']['name'])) {
					$this->content_sub_title = $this->map['account']['name'];
				}
			}
		} elseif ($rec_type == 'account') {
	
			$this->type = 'account';
			$this->title = 'Company Information';
			$this->layout = MapLookup::GetAccountLayout($this->company_id);

			// If they only want the default section do surgery on the layout
			// before requesting data so the response will be quicker
			if ($action == 'default_only') $this->DefaultOnly();
			
			switch ($action) {
				case 'add' :
					$this->map = MapLookup::GetAccountMap($this->company_id);
					break;
				default :
					$this->map = $record->PopulateMapValues($rec_id, MapLookup::GetAccountMap($this->company_id));
			
					$this->content_title = $this->map['fields'][MapLookup::GetAccountNameField($this->company_id)]['value'];
			}
		}
		else {
			return; // Fail because we don't have a type or ID to hint at what to do
		}

				
		// Quick method of removing restricted sections from the layout for the current user
		$this->RestrictSections();
		
		if ($action != 'add') {
			if ($this->map['security']['Private']) {
				$this->content_title .= ' (Private)';
				$this->formatting = 'content_private';
			}
			if ($this->map['security']['Inactive']) {
				$this->content_title .= ' (Inactive)';
				$this->formatting = 'content_inactive';
			}
		}
		
		if (ReportingTreeLookup::IsManager($this->company_id, $this->user_id)) {
			$limit_access = FALSE;
		}
		
		if (isset($this->map['security']['is_mine']) && $this->map['security']['is_mine']) {
			$limit_access = FALSE;
		}
		
		// Don't limit the access to unassigned users if the security is turned off.
		if (!(isset($record->limit_company_access) && $record->limit_company_access)) {
			$limit_access = FALSE;
		}
		
		if ($action == 'add' && $limit_access) {
			// The user is clicked "add" while looking at a record that
			// they are not assigned to, so get a blank map and allow access
			$this->map = MapLookup::GetContactMap($this->company_id);
			$limit_access = FALSE;
		}

	
		switch ($action) {
			case 'edit' :
			case 'add' :
				
				if ($limit_access) {
					// Something weird has happened for them to get here - trying to
					// edit an account that they do not have access to
					echo "Restricted";
					exit();
				}
				$this->template_files = array('form_company_contact_new.tpl');
				break;
				
			default :
				
				if ($limit_access) {
					// Remove all the private fields from the layout because the user has only basic access
					$this->RemovePrivateFields();
				}
				
				$this->template_files = array('module_company_contact_new.tpl');
				
				if ($this->type == 'contact') {
					$button_id = 'contactId=' . $rec_id;
					$this->account_id = $this->map['account']['id'];
					$note_type = NOTETYPE_CONTACT;
				}
				else {
					$button_id = 'accountId=' . $rec_id;
					$this->account_id = $this->map['security']['AccountID'];
					$note_type = NOTETYPE_COMPANY;
				}
				
				// These links will allow navigation through the search list (VJG - 10/09/13)
				if ($rec_ptr > 0) {
					// If we are not at the beginning of any page, we will need a previous link
					$btn_ptr = $rec_ptr-1;
					if (substr($search_results[$btn_ptr]['UniqueID'],0,1)=='a'){
						$nav_address = '?action=company&accountId='.$search_results[$btn_ptr]['AccountID'];
					}
					else {
						$nav_address = '?action=contact&contactId='.$search_results[$btn_ptr]['ContactID'];
					}
					$this->AddButton('', $nav_address, 'Previous', '', '_self');
				}
				else {
					// We are looking at the first item on a page.  If it's not the first page, we need a previous link
					if ($_SESSION['search_curpage'] > 1) {
						//Create a new action on the slipstream class that will move us back a page
						if (substr($search_results[$btn_ptr]['UniqueID'],0,1)=='a'){
							$nav_address = '?action=pcompany';
						}
						else {
							$nav_address = '?action=pcontact';
						}
						$this->AddButton('', $nav_address, 'Previous', '', '_self');
					}
				}

				if ($rec_ptr < count($search_results)-1) {
					// Next if we are not at the end
					$btn_ptr = $rec_ptr+1;
					if (substr($search_results[$btn_ptr]['UniqueID'],0,1)=='a'){
						$nav_address = '?action=company&accountId='.$search_results[$btn_ptr]['AccountID'];
					}
					else {
						$nav_address = '?action=contact&contactId='.$search_results[$btn_ptr]['ContactID'];
					}
					$this->AddButton('', $nav_address, 'Next', '', '_self');
				}
				else {
					if ($_SESSION['search_curpage'] < $_SESSION['search_maxpage']) {
						//Create a new action on the slipstream class that will move us forward a page
						if (substr($search_results[$btn_ptr]['UniqueID'],0,1)=='a'){
							$nav_address = '?action=ncompany';
						}
						else {
							$nav_address = '?action=ncontact';
						}
						$this->AddButton('', $nav_address, 'Next', '', '_self');						
					}
				}				
				
				if (!$limit_access) {
					if ($this->map['security']['Inactive'] == 1) {
						$this->AddButton('', "javascript:MarkActive('$this->type', $rec_id, 'active');", 'Mark Active');
					}
					else {
						$this->subnav[LOG_INCOMING_CALL] = 'javascript:addNewNoteForm(' . $note_type . ', \'' . $rec_id . '\', ' . NOTETYPE_INCOMING_CALL . ');';
						$this->subnav[LOG_OUTGOING_CALL] = 'javascript:addNewNoteForm(' . $note_type . ', \'' . $rec_id . '\', ' . NOTETYPE_OUTGOING_CALL . ');';
						
						$this->AddButton('', "javascript:ShowAjaxForm('CompanyContactNew&$button_id&action=edit');", 'Edit');
						$this->AddButton('', "javascript:MarkActive('$this->type', $rec_id, 'inactive');", 'Mark Inactive');
						
						// display a link to google maps with query string for the contact
						$map_address = $this->GetMapAddress();
						if (!empty($map_address)){
							$this->AddButton('', $map_address, 'Map', '', '_blank');
						}
					}
				}
				$this->AddButton('', 'javascript:window.print();', 'Print');
				$assignment = new Assignment();
				
				if($assignment->canAssign($this->company_id, $this->user_id, $rec_id, $rec_type)){
					$this->subnav['Assign'] = "javascript:showManageForm('$this->type', $rec_id);";
				}				
		}

		$this->form_id = 'id' . uniqid();
		$this->limit_access = $limit_access;

	}	

	// Get the address map for google map
	public function GetMapAddress() {
		$address = '';
		$sql = "SELECT * FROM AddressMap WHERE CompanyID = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->company_id));
		$address_map = DbConnManager::GetDb('mpower')->Exec($sql);
		if (count($address_map) > 0) {
			$street = (!empty($this->map['fields'][$address_map[0]['Street']]['value']) )? $this->map['fields'][$address_map[0]['Street']]['value'] : '';
			$city = (!empty($this->map['fields'][$address_map[0]['City']]['value'])) ? $this->map['fields'][$address_map[0]['City']]['value'] : '';
			$state = (!empty($this->map['fields'][$address_map[0]['State']]['value'])) ? $this->map['fields'][$address_map[0]['State']]['value'] : '';
		
			if (!empty($street) || !empty($city) || !empty($state)) {
				$address = 'http://maps.google.com/?q=' . $street . ',' . $city . ',' . $state;
			}
		}
		return $address;
	}

	public function CreateModuleFromExisting() {
		$this->Build($this->type, $this->rec_id);
	}
	
	public function CreateModuleFromPrevNext($atype,$aid) {
		$this->Build($atype, $aid);
	}
	
	private function DefaultOnly() {
		foreach ($this->layout as $section_id => &$section) {
			if ($section['SectionName'] != 'DefaultSection') {
				unset($this->layout[$section_id]);
			}
		}
	}

	private function RestrictSections() {
		foreach ($this->layout as $section_id => &$section) {
			if (!empty($section['Restriction'])) {
				if (!in_array($this->user_id, explode(',', $section['Restriction']))) {
					unset($this->layout[$section_id]);
				}
			}
		}
	}
	
	/*
	 * Load defaults into fields that are set to use them and are blank
	 */
	private function PopulateDefaults($map) {
		//print_r($this->map['fields']);
		foreach ($map['fields'] as &$field) {
			if (!empty($field['value'])) {
				continue;
			}
			$params = ($field['Parameters'] != NULL) ? explode("|", $field['Parameters']) : FALSE;
			switch (substr($field['FieldName'], 0, 4)) {
				case 'Date' :
					// For a date, param 4 indicates to default as now
					if (isset($params[4]) && $params[4] == 1) {
						$field['value'] = date('m/d/Y');
					}
					break;
			}
		}
		return $map;
	}

	/*
	 * Load values from GET into fields 
	 */
	private function PopulateGetValues($map, $getValues) {
		
		$FirstNameField = MapLookup::GetContactFirstNameField($this->company_id, false);
		$LastNameField = MapLookup::GetContactLastNameField($this->company_id , false);
		$EmailField = MapLookup::GetFirstEmailField($this->company_id, false);
		
		$map['fields'][$FirstNameField]['value'] = $getValues['first_name'];
		$map['fields'][$LastNameField]['value'] = $getValues['last_name'];
		$map['fields'][$EmailField]['value'] = $getValues['email'];
		return $map;
	}	

	private function RemovePrivateFields() {
		$trailing_row = array();
		foreach ($this->layout as $section_id => &$section) {
			foreach ($section['rows'] as $row_id => &$row) {
				if (is_array($row)) {
					foreach ($row as $field_id => $field) {
						if ($this->map['fields'][$field]['IsPrivate'] == 1) {
							unset($row[$field_id]);
						}
					}
					if (count($row) == 0) {
						unset($section['rows'][$row_id]);
					} else {
						$trailing_row = array();
					}
				} else {
					$trailing_row[] = $row_id;
				}
			}
			foreach ($trailing_row as $row_id) {
				unset($section['rows'][$row_id]);
			}
			if (count($section['rows']) == 0) {
				unset($this->layout[$section_id]);
			}
		}
	}

	public function FormatForDisplay($fielddef) {
		if (empty($fielddef['value'])) return;
		$field_value = $fielddef['value'];
		$val_type = (!empty($fielddef['ValidationType'])) ? $fielddef['ValidationType'] : 0;
		$params = ($fielddef['Parameters'] != NULL) ? explode("|", $fielddef['Parameters']) : NULL;
		
		$output = '';
		
		switch (substr($fielddef['FieldName'], 0, 4)) {
			case 'Long' :
				//$output = SplitLongWords($field_value);
				$output = $field_value;
				break;
			case 'Text' :
				switch ($val_type) {
					case 4 : // URL
						if (trim($field_value) != '') {
							if (!strstr($field_value, "://")) $field_value = "http://" . $field_value;
							$output = "<a href=\"{$field_value}\" target=\"_blank\" title=\"" . $field_value ."\">" . SplitLongWords($this->ParseUrl($field_value)) . "</a><img style=\"margin-left:5px;\" src=\"images/external_link.gif\" />";
						}
						break;
					case 5 : // Email
						if (trim($field_value) != '') {
							#########################################
							# Mailto Action
							# Currently only gmail is implemented
							#   
							#	Standard : 0
							#      Gmail : 1
							#
							# Link to compose mail at mail.google.com
							if($_SESSION['USER']['MAILTO_ACTION'] == 1){
								//http://mail.google.com/mail/?view=cm&fs=1&ui=1&to=&su=test
								$output = "<a href=\"http://mail.google.com/mail/?view=cm&fs=1&ui=1&to={$field_value}\" target=\"_blank\">" . SplitLongWords($field_value) . "</a><img style=\"margin-left:5px;\" src=\"images/email.gif\" />";
							}
							else {
								$output = "<a href=\"mailto:{$field_value}?{$mail_data}\">" . SplitLongWords($field_value) . "</a><img style=\"margin-left:5px;\" src=\"images/email.gif\" />";
							}
						}
						break;
					default :
						if(is_numeric($field_value) && $fielddef['ValidationType'] == 1){
							switch (strlen($field_value)) {
								case 7 : // 7 digits with the first digit not a 1 is a local exchange American phone number
									if ($field_value[0] != '1') {
										$value = substr($field_value, 0, 3) . '-' . substr($field_value, 3);
									}
									else {//invalid. just display raw data
										$value = $field_value;
									}
									$output .= $value;
									break;
								case 11 : // If it is 11 digits and the first digit is one, remove the first digit and treat it like a 10 digit
									if ($field_value[0] != 1) break;
									$field_value = substr($field_value, 1);
								case 10 : // 10 digits with the first digit not a 1 is a standard American number
									if ($field_value[0] == 1) break;
									$output = '(' . substr($field_value, 0, 3) . ') ' . substr($field_value, 3, 3) . '-' . substr($field_value, 6);
									break;
								default :
									$output .= SplitLongWords($field_value);
							}
						}
						else {
							$output .= SplitLongWords($field_value);
						}
						
				}
				break;
			case 'Numb' :
				if (isset($params[3])) {
					if ($params[3] == 'Currency') {
						setlocale(LC_MONETARY, 'en_US');
						$output = money_format('%+n', $field_value);
					} else if ($params[3] == 'Percent') {
						$output = $field_value . ' % ';
					} else {
						$output = $field_value;
					}
				} else {
					$output = $field_value;
				}
				break;
			case 'Date' :
				if (isset($params[6]) && $params[6] == 1) {
					$output = date("F d", MsSqlToTimestamp($field_value));
				} else {
					$output = ReformatDate($field_value);
				}
				break;
			case 'Bool' :
				// Added code for Limiting access.
				if ($field_value != 'none') {
					$output = ($field_value == 1) ? '<img src="images/checkbox_checked.png">' : '<img src="images/checkbox_unchecked.png">';
				}
				break;
			case 'Sele' :
				$output = OptionLookup::GetOptionName($this->company_id, $fielddef['OptionSetID'], $field_value);
				break;
			default :
				$output = 'Unknown field type...';
		}
		return $output;
	}

	public function FormatForForm($fielddef) {
		if (empty($fielddef)) return;
		
		// If this is a manager edit only field and this is not a manager,
		// then just return the field as display
		if (isset($fielddef['MgrEditOnly']) && $fielddef['MgrEditOnly'] && !ReportingTreeLookup::IsManager($this->company_id, $this->user_id)) {
			return $this->FormatForDisplay($fielddef);
		}
		
		$field_value = (isset($fielddef['value'])) ? $fielddef['value'] : FALSE;
		$val_type = (!empty($fielddef['ValidationType'])) ? $fielddef['ValidationType'] : 0;
		
		if ($fielddef['Parameters'] != null && $fielddef['Parameters'] != '') {
			/*
			 * Additional field parameters are set with |(Pipe)
			 * as the delimiter in the following order:
			 * Width,Height,Length,Format,Decimal,IsPastDate,
			 * IsMonthDay,HasYearRange,StartYear,EndYear. 
			 */
			$param = $fielddef['Parameters'];
			$params = ($fielddef['Parameters'] != NULL) ? explode("|", $fielddef['Parameters']) : NULL;
		}
		
		$output = '';

		switch (substr($fielddef['FieldName'], 0, 4)) {
			case 'Date' :
				$date_field_num = substr($fielddef['FieldName'], 4);
			
				if (is_null($params)) {
				 
					$output .= '<input type="text" size="12" name="' . $fielddef['FieldName'] . '" Range="" value="' . ReformatDate($field_value) . '" tabindex="' . $fielddef['TabOrder'] . '"/>';
				}
				else {
					//Check if the date field is of type Month/Day only.
					
					if ($params[6] == 1) {
						if ($field_value) $date_array = getdate(MsSqlToTimestamp($field_value));
						$output .= '<select class="basic" name="dateMonthField' . $date_field_num . '" tabindex="' . $fielddef['TabOrder'] . '"><option></option>';
						for($month = 1; $month < 13; $month++) {
							if ($field_value) {
								$selected = ($month == $date_array['mon']) ? 'selected' : '';
							}
							$output .= "<option value=" . $month . " " . $selected . ">" . date('F', mktime(0, 0, 0, $month, 1)) . "</option>";
						}
						$output .= '</select>';
						
						$output .= '<select class="basic" name="dateDayField' . $date_field_num . '" tabindex="' . $fielddef['TabOrder'] . '"><option></option>';
						for($day = 1; $day < 32; $day++) {
							if ($field_value != null || strlen($field_value) != 0) $selected = ($day == $date_array['mday']) ? 'selected' : '';
							$output .= "<option value=" . $day . " " . $selected . ">" . $day . "</option>";
						}
						$output .= '</select>';
					} elseif ($params[7] == 1) { //Check if the date field has a year range.
						
						$start_year = $params[8];
						$end_year = $params[9];
						$year_range = $this->FormatJsYearRng($start_year, $end_year);
						$output .= '<input type="text" size="12" name="' . $fielddef['FieldName'] . '" Range="' . $year_range . '" value="' . ReformatDate($field_value) . '" tabindex="' . $fielddef['TabOrder'] . '"/>';
					
					} else {
						$output .= '<input type="text" size="12" name="' . $fielddef['FieldName'] . '" value="' . ReformatDate($field_value) . '" tabindex="' . $fielddef['TabOrder'] . '"/>';
					}
				}
				break;
			case 'Sele' :
				
				$data = OptionLookup::GetOptionset($this->company_id, $fielddef['OptionSetID']);
				$options = FormUtil::sortOptions($data, $fielddef['OptionSetID']);
				
				if ($fielddef['OptionType'] == 1) {
					$output .= '<select class="full" name="' . $fielddef['FieldName'] . '" tabindex="' . $fielddef['TabOrder'] . '"><option></option>';
					
					foreach ($options as $k => $v) {
						$selected = ($k == $field_value) ? 'selected' : '';
						$output .= "<option value='" . $k . "' " . $selected . ">" . $v . "</option>";
					}
					$output .= '</select>';
				}
				elseif ($fielddef['OptionType'] == 2) {
					$output .= '<fieldset class="radio">';
					foreach ($options as $k => $v) {
						$selected = ($k == $field_value) ? 'checked' : '';
						$output .= '<div><input name="' . $fielddef['FieldName'] . '" type="radio" value="' . $k . '" ' . $selected . '>' . $v . '</div>';
					}
					$output .= '</fieldset>';
				}
				break;
			case 'Bool' :
				$checked = ($field_value == 1) ? 'checked' : '';
				$output .= '<div><input name="' . $fielddef['FieldName'] . '" type="checkbox" value="true" ' . $checked . ' tabindex="' . $fielddef['TabOrder'] . '"></div>';
				break;
			case 'Numb' :
				if ($fielddef['Parameters'] == NULL || $fielddef['Parameters'] == '') {
					$output .= '<input type="text" class="basic" size="30" name="' . $fielddef['FieldName'] . '" value="' . $field_value . '" tabindex="' . $fielddef['TabOrder'] . '"/>';
				}
				else {
					$output .= '<input type="text" class="basic" size="' . $params[0] . '" maxlength="' . $params[2] . '" name="' . $fielddef['FieldName'] . '" value="' . $field_value . '" tabindex="' . $fielddef['TabOrder'] . '"/>';
				}
				break;
			case 'Long' :
				if ($params[2] == '')
					$max_length = 0;
				else
					$max_length = $params[2];
//				$output .= '<textarea rows="' . $params[1] . '" cols="' . $params[0] . '" ' . $company_field_id . ' name="' . $fielddef['FieldName'] . '" tabindex="' . $fielddef['TabOrder'] . '" onkeyup="textCounter(this,' . $max_length . ');">' . $field_value . '</textarea>';
				$output .= '<textarea style="width:98%;margin-right:10px;" rows="' . 5 . '" ' . $company_field_id . ' name="' . $fielddef['FieldName'] . '" tabindex="' . $fielddef['TabOrder'] . '" onkeyup="textCounter(this,' . $max_length . ');">' . $field_value . '</textarea>';
				break;
			default :
				if ($fielddef['IsCompanyName'] == 1) {
					if ($this->form_type == 'edit') {
						$company_field_id = 'id="edit_company_account"';
					} else {
						// $company_field_id = 'id="company_account"';
						$company_field_id = '';
					}
				} else {
					$company_field_id = '';
				}
				if ($fielddef['Parameters'] == NULL || $fielddef['Parameters'] == '') {
					$output .= '<input type="text" class="full" ' . $company_field_id . ' name="' . $fielddef['FieldName'] . '" value="' . $field_value . '" tabindex="' . $fielddef['TabOrder'] . '"/>';
				}
				else { //This field has got parameters.
					if ($params[2] == '')
						$max_length = 0;
					else
						$max_length = $params[2];
					
					if ($params[0] != '' && $params[1] != '' && $params[1] != '1') {
						$output .= '<textarea rows="' . $params[1] . '" cols="' . $params[0] . '" ' . $company_field_id . ' name="' . $fielddef['FieldName'] . '" tabindex="' . $fielddef['TabOrder'] . '" onkeyup="textCounter(this,' . $max_length . ');">' . $field_value . '</textarea>';
					} else {
						$output .= '<input type="text" class="full" size="' . $params[0] . '" maxlength="' . $params[2] . '" ' . $company_field_id . ' name="' . $fielddef['FieldName'] . '" value="' . $field_value . '" tabindex="' . $fielddef['TabOrder'] . '"/>';
					}
				}
		}
		$output .= '<div class="form_error" id="err_' . $fielddef['FieldName'] . '"></div>';
		return $output;
	}

	// Gets the record from the database and runs through the map fields and validates them
	public function ValidateInput($input) {
		require_once 'class.Status.php';
		
		define('ADD_NEW_REC', -1);
		
		if ($input->getAlpha('rec_type') == 'contact') {
			$this->type = 'contact';
			$map = MapLookup::GetContactMap($this->company_id);
			$contact = TRUE;
			$fields = array('ContactID', 'CompanyID', 'AccountID', 'PrivateContact', 'Inactive', 'SourceID', 'ImportID', 'ModifyTime');
		} elseif ($input->getAlpha('rec_type') == 'account') {
			$this->type = 'account';
			$map = MapLookup::GetAccountMap($this->company_id);
			$contact = FALSE;
			$fields = array('AccountID', 'CompanyID', 'PrivateAccount', 'Inactive', 'SourceID', 'ImportID');
		} else {
			return FALSE;
		}
		
		$id = $input->getDigits('ID');
		
		// Will load a blank recordset for an add
		if (!$id) {
			$id = ADD_NEW_REC;
			// We need to populate the defaults from the function because we
			// can't trust the data from the form
			$map = $this->PopulateDefaults($map);
		}
		
		$this->rec_id = $id;
		
		$record = new CompanyContactNew($this->company_id, $this->user_id);
		$recordset = $record->GetRecord($id, $map, $fields);

		if ($id != ADD_NEW_REC) {
			$db_record = $recordset[0];
			/*Start Jet-37 */
			$sql = "Update Contact Set googleStatus = NULL Where ContactID = '$id'";
			$r = DbConnManager::GetDb('mpower')->Exec($sql);
			/*Start Jet-37 */

		} else {
			// Adding a new record
			// Create a new record, place it in the recordset, preset values, and initialize
			$db_record = ($contact) ? new Contact() : new Account();
			$recordset[] = $db_record;
			$db_record->SetDatabase(DbConnManager::GetDb('mpower'));
			$db_record->CompanyID = $this->company_id;
			$recordset->Initialize();
		}
		
		//var_dump($db_record);exit;
		$status = new Status();
		
		// Handle private checkbox
		$private = ($input->getAlpha('private') == 'true') ? 1 : 0;
		
		$do_create_account = FALSE;
		if ($contact) {
			// AccountID is a special case for contacts
			$account_id = $input->getDigits('AccountID');
			$account = $input->noTags('account');
			if (empty($account_id) && !empty($account)) {
				// They have entered a company name that isn't in the pull down
				// We will need to create a placeholder if this record is accepted
				$do_create_account = TRUE;
			} elseif ($db_record->AccountID != $account_id) {
				// Account has changed for this contact
				$db_record->AccountID = $account_id;
			}
			
			if ($db_record->PrivateContact != $private) {
				$db_record->PrivateContact = $private;
			}
		} // else { // Accounts are not allowed to be private
		//if ($db_record->PrivateAccount != $private) {
		//	$db_record->PrivateAccount = $private;
		//}
		//}
		

		// the following fields are used in for import api
		if ($input->noTags('SourceID')) {
			$db_record->SourceID = $input->noTags('SourceID');
		}
		
		if ($input->noTags('ImportID')) {
			$db_record->ImportID = $input->noTags('ImportID');
		}
		
		// $value = ($input->getAlpha($field['FieldName']) == 'true') ? 1 : 0;
		

		foreach ($map['fields'] as &$field) {
			
			$value = (isset($field['value'])) ? $field['value'] : NULL;

			// If this is a manager edit only field and this user is not a manager, do not 
			// process the field, or the data will be erased (or someone could insert bad field info)
			if ((isset($field['MgrEditOnly']) && $field['MgrEditOnly'] && !ReportingTreeLookup::IsManager($this->company_id, $this->user_id))) {
				
				if($id != ADD_NEW_REC){
					$field['value'] = $db_record->$field['FieldName'];
					$value = $db_record->$field['FieldName'];
				}
			}
			else {
				$val_type = (!empty($field['ValidationType'])) ? $field['ValidationType'] : 0;
				$params = ($field['Parameters'] != NULL) ? explode("|", $field['Parameters']) : FALSE;
			
				switch (substr($field['FieldName'], 0, 4)) {
					case 'Long' :
						
						$value = trim(nl2br($input->noTags($field['FieldName'])));
						break;
					case 'Text' :
						$value = trim($input->noTags($field['FieldName']));
						if (empty($value)) {
							$value = NULL;
							break;
						}
						switch ($val_type) {
							case 1 :
								$digits = $input->getDigits($field['FieldName']);
								switch (strlen($digits)) {
									case 7 : // 7 digits with the first digit not a 1 is a local exchange American phone number
										if ($digits[0] != '1') {
											$value = substr($digits, 0, 3) . '-' . substr($digits, 3);
										}
										break;
									case 11 : // If it is 11 digits and the first digit is one, remove the first digit and treat it like a 10 digit
										if ($digits[0] != 1) break;
										$digits = substr($digits, 1);
									case 10 : // 10 digits with the first digit not a 1 is a standard American number
										if ($digits[0] == 1) break;
										$value = '(' . substr($digits, 0, 3) . ') ' . substr($digits, 3, 3) . '-' . substr($digits, 6);
										break;
								}
								break;
							case 4 : // URL
								if (!strstr($value, '://')) {
									$value = "http://$value";
								}
								if (!Inspekt::isUri($value)) {
									$status->addError('Invalid URL', $field['FieldName']);
								}
								break;
							case 5 : // Email
								if (!$input->testEmail($field['FieldName'])) {
									$status->addError('Invalid Email Address', $field['FieldName']);
								}
								break;
							default :
						}
						break;
					case 'Numb' :
						$value = str_replace(array('$', ",", "%"), "", $input->noTags($field['FieldName']));
						if (empty($value)) {
							$value = NULL;
							break;
						}
						if (!is_numeric($value)) {
							$value = NULL;
							$status->addError('Must be a number value', $field['FieldName']);
							break;
						}
						if (isset($params[4]) && $params[4] == 1) {
							$arr = explode('.', $value);
							if (count($arr) > 1) { //it has a decimal .
								if (strlen($arr[1]) > $params[4]) $status->addError('Cannot have more than ' . $params[4] . ' decimal places.', $field['FieldName']);
							}
						}
						break;
					case 'Date' :
						// Param 6 indicates to look for a day/month fields only
						if (isset($params[6]) && $params[6] == 1) {
							$date_field_num = substr($field['FieldName'], 4);
							$monthFieldName = 'dateMonthField' . $date_field_num;
							$dayFieldName = 'dateDayField' . $date_field_num;
							$month = $input->getDigits($monthFieldName);
							$day = $input->getDigits($dayFieldName);
							if ($month > 0 && $day > 0) {
								$value = TimestampToMsSql(mktime(0, 0, 0, $month, $day, 2008));
							}
							break;
						}
						
						// Get the date and standardize the field delims, then explode into array
						$date = $input->noTags($field['FieldName']);
						$date = str_replace('-', ' ', $date);
						$date = str_replace('/', ' ', $date);
						$exploded_date = explode(" ", $date);
						
						if (count($exploded_date) < 3) {
							$value = NULL;
							break;
						}
						list ($month, $day, $year) = $exploded_date;
						if ($year == 0) $year = 2000;
						
						if (is_numeric($month) && is_numeric($day) && is_numeric($year) && checkdate($month, $day, $year)) {
							$temp_value = mktime(0, 0, 0, $month, $day, $year);
							$value = TimestampToMsSql($temp_value);

							// Param 5 = before today
							if ($params[5] != '' && $params[5] == 1) {
								$today = time();
								if ($temp_value > $today) {
									$status->addError('Must be before today.', $field['FieldName']);
								}
							}
							if (isset($params[7]) && $params[7] == 1) {
								$start_year = ($params[8] == 'CurrentYear') ? date("Y") : $params[8];
								$end_year = ($params[9] == 'CurrentYear') ? date("Y") : $params[9];
								$temp_value_year = date("Y", $temp_value);
								
								if (!($temp_value_year >= $start_year && $temp_value_year <= $end_year)) {
									$status->addError('Must be between ' . $start_year . ' and ' . $end_year, $field['FieldName']);
								}
							}
						} else {
							$status->addError('Invalid date', $field['FieldName']);
						}
						break;
					case 'Bool' :
						$value = ($input->getAlpha($field['FieldName']) == 'true') ? 1 : 0;
						break;
					case 'Sele' :
						// TODO:  Ensure that the choice is a valid selection
						$result = $input->getInt($field['FieldName']);
						if (empty($result)) {
							$value = NULL;
						} else {
							$value = $result;
						}
						break;
					default :
						$status->addError('Unknown field type.', $fields['FieldName']);
				}
				if ($field['IsRequired'] && empty($value) && !$this->limit_validation) {
					$status->addError($field['LabelName'] . ' is a mandatory field.', $field['FieldName']);
				}
			}
			// Store the value back into the map, so we can produce a module from the data
			// without reloading
			$field['value'] = $value;
			
			$fieldname = $field['FieldName'];
			if ($db_record->$fieldname != $value) {
				$db_record->$fieldname = $value;
			}
		}

		$results['status'] = $status->GetStatus();
		if ($results['status']['STATUS'] == 'OK') {
			if ($do_create_account) {
				$db_record->AccountID = $this->CreateMinimalAccount($this->company_id, $this->user_id, $account);
			}
			//$db_record->googleStatus = '0';
			$recordset->Save();
			if ($id == ADD_NEW_REC) {
				
				$id = $this->rec_id = ($map['type'] == 'contact') ? $db_record->ContactID : $db_record->AccountID;
				
				// Create an assignment record
				$this->Assign($map['type'], $id, $this->user_id);
				
				/*Start Jet-37 Removed from here for edit */
				//$results['dest'] = "?action=" . (($map['type'] == 'contact') ? 'contact' : 'company') . "&{$map['type']}Id=" . (($map['type'] == 'contact') ? $db_record->ContactID : $db_record->AccountID);
				/*End Jet-37 Removed from here for edit */
				
				DateUtil::setCreatedDate($map['type'], $this->rec_id);
				
				
				// echo 'In validate: ' . $this->rec_id . "\n";
				$results['add'] = TRUE;
			}

			
			$results['status']['RECID'] = $id;
			
			/*Start Jet-37 Added here */
			$results['dest'] = "?action=" . (($map['type'] == 'contact') ? 'contact' : 'company') . "&{$map['type']}Id=" . (($map['type'] == 'contact') ? $db_record->ContactID : $db_record->AccountID);
			/*End Jet-37 Added here */
			
			// Create or update a matching record in the DashboardSearchCreator
			$obj_search = new DashboardSearchCreator();
			$obj_search->company_id = $this->company_id;
			$obj_search->record_id = $id;
			
			if ($contact) {
				$obj_search->StoreContactRecords();
			} else {
				$obj_search->StoreAccountRecords();
			}
			
		// $db_record->Debug();
		}
		return $results;
	}
	
	/**
	 * Display only:
	 * http://www.domain.com/...
	 * @param String $url
	 */
	function ParseUrl($url){
		
		$parts = parse_url($url);
		$display = $parts['scheme'] . '://' . $parts['host'];
		
		if(strlen($parts['path']) > 0){
			$display .= '...';
		}
		return $display;
	}

	function Assign($type, $id, $user) {
		if ( $this->CheckAssignments( $type, $id, $user) > 0 ) return; 
		if ($type == 'contact') {
			$assign_sql = "insert into PeopleContact (ContactID, PersonID, CreatedBy, AssignedTo) 
				values (?, ?, 1, 1)";
		} else {
			$assign_sql = "insert into PeopleAccount (AccountID, PersonID, CreatedBy, AssignedTo)
				values (?, ?, 1, 1)";
		}
		$assign_sql = SqlBuilder()->LoadSql($assign_sql)->BuildSql(array(DTYPE_INT, array($id, $user)));
		// echo $assign_sql . "\n";

		DbConnManager::GetDb('mpower')->DoInsert($assign_sql);
	}

	private function CheckAssignments ($type, $id, $user) {
		$table = ($type == 'contact') ? 'PeopleContact' : 'PeopleAccount'; 
		if ($type == 'contact') {
			$check_sql = "select count(*) AS CNT from $table where ContactID = ? and PersonID = ? " ; 
		} else {
			$check_sql = "select count(*) AS CNT from $table where AccountID = ? and PersonID = ? " ; 
		}
		
		$check_sql = SqlBuilder()->LoadSql($check_sql)->BuildSql(array(DTYPE_INT, array($id, $user)));
		$value = DbConnManager::GetDb('mpower')->GetOne($check_sql);
		if ($value->CNT > 0) return 1 ; 
		else return 0 ; 
	}

	private function CreateMinimalAccount($company, $user, $account_name) {
		$name_field = MapLookup::GetAccountNameField($this->company_id, TRUE);
		$created = date("m/d/y");
		
		$sql = "INSERT INTO Account (CompanyId, $name_field, CreatedDate) values (?, ?, ?)";
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company), array(DTYPE_STRING, $account_name), array(DTYPE_STRING, $created));

		$id = DbConnManager::GetDb('mpower')->DoInsert($sql);
		
		$this->Assign('account', $id, $user);
		
		// Create or update a matching record in the DashboardSearchCreator
		$obj_search = new DashboardSearchCreator();
		$obj_search->company_id = $company;
		$obj_search->record_id = $id;
		$obj_search->StoreAccountRecords();
		
		return $id;
	}

	// TODO: Ensure any call to inactive is permitted for this user
	function Activate($type, $id, $action) {
		$status = new Status();
		
		if ($action == 'active') {
			$is_inactive = 0;
		} elseif ($action == 'inactive') {
			$is_inactive = 1;
		} else {
			return;
		}
		
		if ($type == 'contact') {
			$sql = "UPDATE Contact SET Inactive = $is_inactive WHERE ContactID = ?";
		} elseif ($type == 'account') {
			$sql = "UPDATE Account SET Inactive = $is_inactive WHERE AccountID = ?";
		} else {
			return;
		}
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id));
		
		DbConnManager::GetDb('mpower')->Exec($sql);
		
		return $status;
	}

	private function FormatJsYearRng($start_year, $end_year) {
		if ($start_year == '' || $end_year == '') return FALSE;
		
		if ($start_year == 'CurrentYear') $start_year = date('Y');
		
		if ($end_year == 'CurrentYear') $end_year = date('Y');
		
		if ($end_year < $start_year) return FALSE;
		
		$year_range = ($start_year - date('Y')) . ':' . ($end_year - date('Y'));
		
		return $year_range;
	}

	public function IsRestricted() {
		return $this->limit_access;
	}

}

