<?php
require_once BASE_PATH . '/slipstream/class.Calendar.php';
require_once BASE_PATH . '/slipstream/lib.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.ModuleEvents.php';
require_once BASE_PATH . '/include/class.ReportingTree.php';
require_once BASE_PATH . '/include/mpconstants.php';

class ModuleMiniCalendar extends JetstreamModule
{
	public $title = 'Mini-Calendar';

	protected $javascript_files = array('jquery.cluetip.js', 'jquery.hoverIntent.js');
	protected $css_files = array('mini_calendar.css', 'tooltip.css', 'event.css', 'jquery.cluetip.css');
	protected $template_files = array('module_mini_calendar.tpl');
	protected $buttons = array();
	public $sidebar = true;
	public $target;

	function __construct($target = 0) {
		/*
		 * If the values are set in the cookie then get it from the cookie.
		 * Else Use the current date.
		 * Numeric representation of the month, with leading zeros.
		 * Numeric representation of the year.
		 * Full textual representation of the month.
		 * Day of the month without leading zeros.
		 */
		if (isset($_COOKIE['MiniCalMonth'])) {
			$cur_month = $_COOKIE['MiniCalMonth'];
			$cur_year = $_COOKIE['MiniCalYear'];
			$cur_date_time = mktime(0, 0, 0, $cur_month, 1, $cur_year);

			$this->month_int = date('m', $cur_date_time);
			$this->year = date('Y', $cur_date_time);
			$this->month = date('F', $cur_date_time);
			$this->today = date('m/d/Y');

			if (($this->month_int == date('m')) && ($this->year == date('Y'))) {
				$this->show_today = false;
			} else {
				$this->show_today = true;
			}
		} else {
			$this->month_int = date('m');
			$this->year = date('Y');
			$this->month = date('F');
			$this->today = date('m/d/Y');
		}


		$this->target = ($target == 0) ? $_SESSION['USER']['USERID'] : $target;
	}

	public function Init() {

	}

	public function Draw() {
		/*
		 * Creates the object of Calendar class.
		 * Get the array having each date of the particular month and year, also with the blank dates
		 */
		$obj_calendar = new Calendar();
		//		$mini_calendar = $obj_calendar->showCalendar($this->month_int, $this->year, $week_arr);


		//		$this->calendar = $mini_calendar;
		//		$this->weeks = $week_arr;
		//		$this->AddButton('info20.png', 'javascript:showToolTip();', 'Legendx');


		$this->AddButton('info20.png', 'ajax/EventLegend.php', 'Event Legend', 'event_legend');

		/*
		 * Creates the object of Event class.
		 * Get the array having each date's Events of the particular month and year.
		 * Get the eventtype legend..
		 */

		//$this->target = (isset($_REQUEST['target']) && is_numeric($_REQUEST['target']) && $_REQUEST['target'] > 0) ? $_REQUEST['target'] : '';

		$obj_event = new Event();

		$this->days = $obj_calendar->showCalendar($this->month_int, $this->year);

		$month_start_date = reset($this->days);
		$first_date = $month_start_date['date'];

		$month_end_date = end($this->days);
		$last_date = $month_end_date['date'];
		$events = $obj_event->GetEventsByDateRange($first_date, $last_date, $this->target, 'EVENT');
		// $events = $obj_event->GetWeeklyEvents($first_date, $last_date, $_SESSION['tree_obj']->IsManager($_SESSION['login_id']), $this->target);


		foreach ($events as $event_id => $event) {
			for ($event_day = $event['start_date']->format('Ymd'); $event_day <= $event['end_date']->format('Ymd'); $event_day++) {
				// Skip this one if this is the end day and the end time falls on midnight
				if ($event_day == $event['end'] && $event['end_date']->format('Hi') == '0000') continue;
				if (isset($this->days[$event_day])) $this->days[$event_day]['events'][$event_id] = $event;
			}
		}
		$today = date('Ymd');
		if (isset($this->days[$today])) {
			$this->days[$today]['today'] = TRUE;
		}

	// $event_legend = $obj_event->ShowEventTypeLegend();
	// $this->toolTip = $event_legend;
	}

}
