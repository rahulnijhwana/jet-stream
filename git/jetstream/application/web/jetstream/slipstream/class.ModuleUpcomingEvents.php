<?php
require_once BASE_PATH . '/slipstream/class.Event.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.Contact.php';
require_once BASE_PATH . '/slipstream/class.Account.php';

class ModuleUpcomingEvents extends JetstreamModule
{
	public $title = 'Upcoming Events';
	protected $javascript_files = array('overlay.js', 'jquery.bgiframe.js', 'jquery.autocomplete.js', 'event.js', 'stringexplode.js', 'ui.datepicker.js', 'jquery.cluetip.js', 'jquery.hoverIntent.js');
	protected $css_files = array('event.css', 'ui.datepicker.css', 'overlay.css', 'jquery.autocomplete.css', 'mini_calendar.css', 'jquery.cluetip.css');
	protected $buttons = array();
	public $sidebar = TRUE;
	public $target;
	public $msg;
	
	public $id = 'upcoming_events';
	
	public $type;
	public $rec_id;
	public $company_id;
	public $user_id;
	public $empty_module;
	public $is_restricted = UNKNOWN;
	public $inactive_contact;

	public function Init() {
	
		if (!isset($this->type)) {
			if (isset($_GET['contactId'])) {
				$this->type = 'contact';
			} elseif (isset($_GET['accountId'])) {
				$this->type = 'account';
			}
		}

		if (!isset($this->rec_id)) {
			if (isset($_GET['companyId'])) {
				$this->rec_id = $_GET['companyId'];
			} elseif (isset($_GET['contactId'])) {
				$this->rec_id = $_GET['contactId'];
			} elseif (isset($_GET['accountId'])) {
				$this->rec_id = $_GET['accountId'];
			}
		}
		
		$this->user_id = $_SESSION['USER']['USERID'];
		$this->company_id = $_SESSION['company_obj']['CompanyID'];
	}

	public function Draw() {
							
		$this->AddTemplate('module_events.tpl');
		$this->msg = 'None found';
		
		$obj_event = new Event();
		
		$today = date("m/d/Y");
		$days_for = date('m/d/Y', strtotime('+10 days', strtotime($today)));
		$days_for1 = date('m/d/Y H:i:s', strtotime('+1 days', strtotime($today))-1);
		$today_dt_time = date("Y-m-d H:i:s");
		$this->curDate = 'var cur_date = \'' . json_encode($today) . '\';';
		$this->durationArr = $obj_event->GetDurationArray(24);
		
		$objContact = new Contact();
		$objAccount = new Account();
		
		if (!$this->empty_module && $this->is_restricted == UNKNOWN) {
			switch ($this->type) {
				case 'contact' :
					$this->is_restricted = $objContact->IsRestricted($this->rec_id, $this->user_id, $this->company_id);
					break;
				case 'account' :
					$this->is_restricted = $objAccount->IsRestricted($this->rec_id, $this->user_id, $this->company_id);
					break;
				default :
					$this->is_restricted = FALSE;
			}
		}
		
		if ($this->is_restricted === TRUE) {
			$this->msg = "You are not allowed to view upcoming events.";
			return;
		}

		switch ($this->type) {
			case 'account_tasks' :
				$filter = array('type' => ACCOUNT, 'rec_id' => $this->rec_id);
			case 'contact_tasks' :
				$query_type = 'TASK';
				$this->title = 'Upcoming Tasks';
				if (!$objContact->IsInactiveContact($this->rec_id)) {
					if ($this->type == 'account_tasks') {
						$this->AddButton('add20.png', "#top\" onclick=\"javascript:ShowAjaxForm('EventNew&companyid={$this->rec_id}&action=add');", 'Add');
					} else {
						$this->AddButton('add20.png', "#top\" onclick=\"javascript:ShowAjaxForm('EventNew&contactid={$this->rec_id}&action=add');", 'Add');
					}
				}
			case 'contact' :
				if (!isset($filter)) {
					$filter = array('type' => CONTACT, 'rec_id' => $this->rec_id);
				}
				
				if (!$objContact->IsInactiveContact($this->rec_id) && !isset($query_type)) {
					$this->AddButton('add20.png', "#top\" onclick=\"javascript:ShowAjaxForm('EventNew&contactid={$this->rec_id}&action=add');", 'Add');
				}
			case 'account' :
				if (!isset($query_type)) {
					$query_type = 'EVENT';
				}
				if (!isset($filter)) {
					$filter = array('type' => ACCOUNT, 'rec_id' => $this->rec_id);
				}
				if (!$this->empty_module) {

					$this->events = $obj_event->GetEventsByDateRange("01/01/1970", date("m/d/Y", strtotime("+12 months", time())), -1, $query_type, $filter, false);
					// $this->events = $obj_event->GetEventsUpcoming($this->type, $this->rec_id);
				}
				break;
				
			case 'tasks' :
				$this->title = 'Today\'s Tasks';
				$this->events = $obj_event->GetEventsByDateRange($today,$days_for1, '', 'TASK');
				break;
			
			default :
				$this->title = 'Today\'s Appointments';
				$this->events = $obj_event->GetEventsByDateRange($today,$days_for1, '', 'EVENT');
				
		}
	}
}
