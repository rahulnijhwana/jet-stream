<?php
require_once BASE_PATH . '/slipstream/class.JetstreamPage.php';
require_once BASE_PATH . '/slipstream/class.ModuleCalendar.php';
require_once BASE_PATH . '/slipstream/class.ModuleMiniCalendar.php';
require_once BASE_PATH . '/slipstream/class.ModuleUpcomingEvents.php';
require_once BASE_PATH . '/slipstream/class.ModuleAdvanceSearch.php';
require_once BASE_PATH . '/slipstream/class.ModuleAdvanceSearchSort.php';
require_once BASE_PATH . '/slipstream/class.ModuleSaveAdvanceSearch.php';
require_once BASE_PATH . '/slipstream/class.ModuleSavedSearch.php';
require_once BASE_PATH . '/include/lib.string.php';

$page = new JetstreamPage();

// Use a generic module because the dashboard search is such a simple page
$module = new ModuleAdvanceSearch(); //Dashboard();
$module->title = 'Advanced Search Jetstream Records';
$module->AddMenuItem('Basic', "javascript:showSearch('basic');",'');
$module->AddMenuItem('Advanced', "javascript:showSearch('superadvance');",'selected');
$module->AddTemplate('module_advance_search.tpl');
$module->AddTemplate('snippet_merge_record.tpl');
$module->AddJavascript('jquery.js', 'slipstream_search.js', 'slipstream_jetmenu.js', 'jquery.form.js', 'ui.datepicker.js', 'ui.core.js', 'jquery.multiSelect.js');
$module->AddCss('search.css', 'ui.datepicker.css', 'jquery.multiSelect.css', 'advance_datagrid.css');
$module->search_types = array(3 => 'Company & Contact', 1 => 'Company', 2 => 'Contact');
$module->records_per_page = array(25, 50, 75, 100);

/* Set the search parameters from cookie. or $_GET*/

if (isset($_GET['quicksearch'])) {
	$_SESSION['LastSearch'] = array(
		'search_string' => filter_input(INPUT_GET, 'quicksearch'),
		'page_no' => 1,
	    'records_per_page' => 25,
	    'assigned_search' => 0,
	    'query_type' => 3,
	    'inactive_type' => 0,
	    'advanced_filters' => Array(),
	    'sort_type' => '',
	    'sort_dir' => ''
	);
	$module->Build($_SESSION['LastSearch']);
} 

$page->AddModule($module);

$page->AddModule(new ModuleCalendar());
$page->AddModule(new ModuleSaveAdvanceSearch());
$page->AddModule(new ModuleMiniCalendar());
$page->AddModule(new ModuleUpcomingEvents());

$tasks = new ModuleUpcomingEvents();
$tasks->type = 'tasks';
$page->AddModule($tasks);

$page->Render();
