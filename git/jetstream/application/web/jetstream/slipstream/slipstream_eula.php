<?php
// Jetstream EULA content
//SessionManager::Init();
$_SESSION['REDIRECT_PAGENAME'] = 'slipstream.php?SN='.$_GET['SN'];
?>
<div style="width:95%;padding-left:10px;">
<center><img src="images/login_jetstream.png"></center>
<p>
BY CLICKING THE "I ACCEPT" BUTTON DISPLAYED AS PART OF THE PROCESS BY WHICH
YOU ARE AUTHORIZED TO HAVE ACCESS TO JETSTREAM�, YOU AGREE TO THE FOLLOWING TERMS AND CONDITIONS. THESE TERMS AND CONDITIONS CONSTITUTE YOUR AGREEMENT FOR THE BENEFIT OF JetstreamCRM.com, LLC ("COMPANY"). IF YOU DO NOT AGREE TO THE FOLLOWING TERMS AND CONDITIONS, PLEASE CLICK THE "I DECLINE" BUTTON, AND YOU WILL NOT BE AUTHORIZED TO HAVE ACCESS TO JETSTREAM�.
</p>

<p>
Your access to Jetstream� ("Services") is provided pursuant to another agreement with the entity or person for whose benefit you will have access to the Services. The entity or person entering into that other agreement with Company is referred to as "Licensee." Licensee has control of the process by which you are given a User-ID and a password to enable you to access the Services. If you are an employee, Licensee may be your employer. If you are a contractor, consultant, or sales representative, Licensee may be the company on whose behalf you are
acting. If you are using the Services for yourself individually, you are the Licensee. In any case, Licensee must have entered into a "License Agreement" with Company, and your access to the Services is subject to the terms of the License Agreement. You are sometimes referred to in this Agreement as an "End User," and you and other End Users authorized to use the Services pursuant to the License Agreement are sometimes referred to collectively in this Agreement as "End Users."
</p>
	
<p>
1. YOUR USE OF THE SERVICES. You are authorized as an End User to use the Services to use, copy, store, transmit and display Licensee Data, only for the business purposes of Licensee, and only from one access device at a time. Any other use, resale or commercial exploitation of the
Services by you is expressly prohibited. Without limiting the generality of the foregoing, you agree not to reverse engineer the Services. You agree not to make copies of the user interfaces or other elements of the Services, or otherwise infringe on the intellectual property rights of Company. You shall not create Internet "links" to the Services or "frame" or "mirror" any page or
other element of the Services on any other server or Internet-based device. Licensee is responsible for your use of the Services in accordance with the terms and conditions of the License Agreement.
</p>

<p>
2. OTHER RESPONSIBILITIES. You shall abide by all applicable local, state, national and foreign laws, treaties and regulations in connection with your use of the Services. You shall not provide your User-ID and password to any other person, including any other End User. You shall report to Licensee any unauthorized use of your password or account or any other known or suspected breach of security of the Services. You shall not impersonate another End User or provide false identity information to gain access to or use the Services.
</p>

<p>
3. TERMINATION. Your access to the Services will automatically terminate upon any termination of the License Agreement. Company also reserves the right to terminate your access to the Services if you materially breach this Agreement. Any unauthorized use of Company Technology or Services will be deemed a material breach of this Agreement. 
</p>

<p>
4. OWNERSHIP. Company retains all right, title and interest in and to the Company Technology and the Services, including any enhancements, modifications or upgrades thereto, and all related intellectual property rights, and any suggestions, ideas, enhancement requests, feedback,
recommendations or other information provided by you, Licensee or any other party relating to the Company Technology or the Services. 
</p>

<p>
5. THIRD-PARTY WEBSITES. Company does not endorse any sites on the Internet that may be linked through the Services. Company may provide such links to Licensee only as a matter of convenience, and in no event shall Company or its licensors be responsible for any content, products, or other materials on or available from such sites. 
</p>

<p>
6. DISCLAIMERS OF WARRANTIES AND LIABILITIES. THE SERVICES WILL BE MADE AVAILABLE TO YOU SUBJECT TO THE DISCLAIMERS OF WARRANTY AND LIABILITY THAT ARE SET FORTH IN THE LICENSE AGREEMENT. WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, EXCEPT AS EXPRESSLY SET FORTH IN THE LICENSE AGREEMENT, COMPANY AND ITS LICENSORS MAKE NO REPRESENTATION, WARRANTY, OR GUARANTY AS TO THE RELIABILITY, TIMELINESS, QUALITY, SUITABILITY, TRUTH, AVAILABILITY, ACCURACY OR COMPLETENESS OF THE SERVICES OR COMPANY TECHNOLOGY.
</p>

<p>
7. JURISDICTION. The laws of the state of Illinois shall govern this Agreement without reference to conflict of laws principles. For any disputes arising out of this Agreement, the parties consent to the personal and exclusive jurisdiction of, and venue in, the State or Federal Courts within Cook County, Illinois.
</p>

<p>
8. DEFINITIONS. As used in this Agreement: "Licensee Data" shall mean any data, information, or material provided or submitted by Licensee to Company in the course of utilizing the Services. "Company Technology" shall mean all of Company�s proprietary technology (including software, hardware, products, processes, algorithms, user interfaces, intellectual property rights, know-how, techniques, designs, and other tangible or intangible technical material or information made available to Licensee by Company in providing the Services). "Services" shall mean the service of
making available to Licensee, on an Application Service Provider (ASP) basis, Jetstream � via designated website or IP address or ancillary services rendered to Licensee by Company. 
</p>
<center>
<form name="acceptance_form" method="post">
	<input type="submit" name="accept_btn" value="I accept" />
	&nbsp;&nbsp;<input type="button" name="cancel_accept_btn" value="I decline" onclick="javascript:window.location.href='logout.php';" />
</form>
</center>
<div>