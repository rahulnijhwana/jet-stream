<?php
require_once BASE_PATH . '/slipstream/class.DataImport.php';
require_once BASE_PATH . '/include/class.DataType.php';

class DataImportUtil
{
	private $table = "";
	private $databaseField = "";
	private $labelName = "";
	private $columnName = "";
	private $index = null;
	
	public function __construct($index=null, $col=null, $str=null)
	{	
		if(is_string($str) && !is_null($index)){
			$this->init($index, $col, $str);
		}
	}
	
	private function init($index, $col, $str)
	{
		$values = split(':', $str);
				
		$this->setTable($values[0]);
		$this->setDatabaseField($values[1]);
		$this->setLabelName($values[2]);
		$this->setColumnName(str_replace("_"," ",$col));
		$this->setIndex($index);
	}
	
	public function setIndex($index)
	{
		$this->index = $index;
	}
	
	public function setTable($table)
	{
		$this->table = $table;
	}
	
	public function setDatabaseField($databaseField)
	{
		$this->databaseField = $databaseField;
	}
	
	public function setLabelName($labelName)
	{
		$this->labelName = $labelName;
	}
	
	public function setColumnName($columnName){
		$this->columnName = $columnName;
	}
	
	public function getTable()
	{
		return $this->table;
	}
	
	public function getDatabaseField()
	{
		return $this->databaseField;
	}
	
	public function getIndex()
	{
		return $this->index;
	}
	
	public function getLabelName()
	{
		return $this->labelName;
	}

	public function getColumnName()
	{
		return $this->columnName;
	}	
	
	public static function buildArray($options)
	{
		$dataImportUtilArray = array();
		$index = 0;
		
		foreach($options as $table => $str){
			array_push($dataImportUtilArray, new DataImportUtil($index, $table, $str));
			$index++;
		}
		return $dataImportUtilArray;
	}
}