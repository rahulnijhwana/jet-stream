<?php
require_once BASE_PATH . '/slipstream/class.JetstreamPage.php';
require_once BASE_PATH . '/slipstream/class.ModuleOpportunityNew.php';
require_once BASE_PATH . '/include/lib.date.php';
require_once BASE_PATH . '/slipstream/class.ModuleNotes.php';
require_once BASE_PATH . '/slipstream/class.ModuleMiniCalendar.php';

$page = new JetstreamPage();

include 'class.ModuleCompanyContactNew.php';

$company_id = $_SESSION['company_obj']['CompanyID'];
$user_id = $_SESSION['USER']['USERID'];

if (isset($_GET['opportunityId'])) {
	$opportunity_id = filter_input(INPUT_GET, 'opportunityId');
}

$opportunity = new ModuleOpportunityNew($company_id, $user_id);
$opportunity->Build('opportunity', $opportunity_id, 'display', true);

$page->AddModule($opportunity);

$page->AddModule(new ModuleNotes());

$page->AddAjaxModule(new ModuleMiniCalendar());

$page->Render();