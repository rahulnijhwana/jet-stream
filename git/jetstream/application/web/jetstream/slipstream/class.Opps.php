<?php
require_once BASE_PATH . '/include/class.Language.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/slipstream/class.Status.php';


/**
 * The opportunities class
 */
class Opps
{
	public $contactId;
	public $accountId;	
	public $type;
	public $salesperson_list;
	public $company_id;
	public $user_id;
	
	/**
	 * Constructor of the opps class.
	 */
	function Opps($company_id, $user_id, $type = 'Active') {		
		$this->type = $type;
		$this->accountId = 0;
		$this->contactId = 0;
		$this->company_id = $company_id;
		$this->user_id = $user_id;
	}
	
	public function GetPerson($person) {
		
		$rec = $_SESSION['tree_obj']->GetRecord($person);
		$this->salesperson_list[] = $person;

		if(isset($rec['children'])) {
			foreach($rec['children'] as $child) {
				$this->GetPerson($child);
			}
		}		
	}	

	/**
	 * get all active or closed opportunities by accountId or contactId
	 */
	function getOpps() {
		
		$accountMapCName = MapLookup::GetAccountNameField($this->company_id, TRUE);
		$contactMapFName = MapLookup::GetContactFirstNameField($this->company_id, TRUE);
		$contactMapLName = MapLookup::GetContactLastNameField($this->company_id, TRUE);

		$this->GetPerson($this->user_id);
		
		if(!in_array($this->user_id, $this->salesperson_list)) {
			array_push($this->salesperson_list, $this->user_id);
		}

		$phases = array(10 => 'TLabel', 1 => 'FMLabel', 2 => 'IPLabel', 3 => 'IPSLabel', 4 => 'DPLabel', 5 => 'DPSLabel', 6 =>'CLabel');				
		
		$sql = 'SELECT O.DealID, P.Color, CONVERT(VARCHAR(20), O.ActualCloseDate, 1) ActualCloseDate, O.ActualDollarAmount, 
					   CONVERT(VARCHAR(20), O.NextMeeting, 1) NextMeeting, CONVERT(VARCHAR(20), O.FirstMeeting, 1) FirstMeeting, 
					   CONVERT(VARCHAR(20), O.LastMoved, 1) LastMoved, DATEDIFF(day, O.LastMoved, getdate()) DaysOld,
					   P.PersonID, P.FirstName, P.LastName, (C.' . $contactMapFName . ' + \' \' + C.' . $contactMapLName . ') Contact,
					   O.Category, A.' . $accountMapCName . ' Company, C.ContactID, A.AccountID
				FROM opportunities O 
				LEFT JOIN people P ON P.PersonID = O.PersonID
				LEFT JOIN Account A ON A.AccountID = O.AccountID
				LEFT JOIN Contact C ON C.ContactID = O.ContactID';			

		$opp_type_check =  ($this->type == 'Active') ? '((O.Category BETWEEN 1 AND 5) OR O.Category = 10)' : 'O.Category = 6';
		
		if ($this->accountId == 0) {
			$sql .= ' WHERE O.ContactID = ? AND ' . $opp_type_check;
			
			$sqlBuild = SqlBuilder()->LoadSql($sql);
			$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $this->contactId));
		} else {			
			$sql .= ' WHERE O.AccountID = ? AND ' . $opp_type_check;
				
			$sqlBuild = SqlBuilder()->LoadSql($sql);
			$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $this->accountId));
		}		
		
		//echo $sql . '<br>';
		
		$opps_rs = DbConnManager::GetDb('mpower')->Execute($sql);		
		$opps = array();
		
		if (count($opps_rs) > 0) {
			foreach ($opps_rs as $key => $opp) {
				
				$sql = 'SELECT P.Abbr FROM Opp_Product_XRef O
						LEFT JOIN Products P ON P.ProductID = O.ProductID
						WHERE O.DealID = ?';
				$sql = $sqlBuild->LoadSql($sql)->BuildSql(array(DTYPE_INT, $opp->DealID));
				$offerings_rs = DbConnManager::GetDb('mpower')->Execute($sql);
				$offerings = array();
				
				if (count($offerings_rs) > 0) {
					foreach ($offerings_rs as $offering) {
						$offerings[] = trim($offering->Abbr);
					}
				}
				
				$opps[$key]['DealID'] = $opp->DealID;
				$opps[$key]['PersonID'] = $opp->PersonID;				
				$opps[$key]['Color'] = $opp->Color;				
				$opps[$key]['Company'] = $opp->Company;	
				$opps[$key]['CompanyID'] = $opp->AccountID;			
				$opps[$key]['Category'] = Language::__get($phases[$opp->Category]);			
				$opps[$key]['Salesperson'] = $opp->FirstName . ' ' . $opp->LastName;
				$opps[$key]['Contact'] = $opp->Contact;	
				$opps[$key]['ContactID'] = $opp->ContactID;
				$opps[$key]['Edit'] = (in_array($opp->PersonID, $this->salesperson_list)) ? true : false;					
				
				if ($opp->Category == 3 or $opp->Category == 5) {					
					$opps[$key]['MeetingLabel'] = '';
					//$next_meeting  = "$opp->LastMoved ($opp->DaysOld)";
					$next_meeting = '';
				} 
				else if($opp->Category == 1) {
					$opps[$key]['MeetingLabel'] = 'First Meeting';
					$next_meeting = $opp->FirstMeeting;
				}
				else {
					$opps[$key]['MeetingLabel'] = 'Next Meeting';
					//$next_meeting = (!empty($opp->NextMeeting)) ? $opp->NextMeeting : $opp->FirstMeeting;
					$next_meeting = $opp->NextMeeting;
				}			
				
				$opps[$key]['NextMeeting'] = ReformatDate($next_meeting);
				$opps[$key]['Offerings'] = implode(', ', $offerings);
				$opps[$key]['Total'] = '$' . number_format($opp->ActualDollarAmount);
				$opps[$key]['ClosedDate'] = ReformatDate($opp->ActualCloseDate);								
			}
		}
		
		return $opps;
	}	
}

?>