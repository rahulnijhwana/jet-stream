<?php
require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';
require_once BASE_PATH . '/slipstream/class.CompanyContact.php';
require_once BASE_PATH . '/slipstream/class.RestrictCompanyContact.php';
require_once BASE_PATH . '/include/class.DebugUtil.php';
require_once BASE_PATH . '/include/class.CompanyUtil.php';

class Assignment {
	
	/**
	 * Check for the ability to assign a record
	 * @param int $company_id
	 * @param int $user_id
	 * @param int $record_id
	 * @param String $record_type
	 */
	public function canAssign($company_id, $user_id, $record_id, $record_type){
		
		if(ReportingTreeLookup::IsMasterAssignee($company_id, $user_id)){
			return true;
		}
		if(ReportingTreeLookup::IsManager($company_id, $user_id)){
			
			$restrict = new RestrictCompanyContact();
			$ref = 0;
			$assigned = $restrict->GetRecordAssignedPeople($record_id, $record_type, $ref);

			/*
			 * We need to be able to reassign users if they are assigned to inactive users
			 */
			if($this->isAssignedToInactiveUser($company_id, $record_id, $record_type, $assigned)){
				return true;
			}
			
			$company_contact = new CompanyContact();
			$child_records = $company_contact->getChildRecords($user_id);
			$children = $company_contact->getChildren($child_records);
			$tmp = $company_contact->clean($children);
			$children = $tmp;
			
			$child_ids = array();
			array_push($child_ids, $user_id);
			foreach($children as $child){
				foreach($child as $k => $v){
					if($k == 'PersonID'){
						array_push($child_ids, $v);
					}
				}
			}
			
			foreach($assigned as $assignee){
				if(!in_array($assignee, $child_ids)){
					return false;
				}
			}
			return true;
		}
		/*
		 * Level one users cannot assign
		 */
		return false;
	}
	
	/**
	 * Check to see if a record is assigned to an
	 * inactive Jetstream user
	 * @param int $company_id
	 * @param int $rec_id
	 * @param int $rec_type
	 * @param array $assigned_people
	 */
	function isAssignedToInactiveUser($company_id, $rec_id, $rec_type, $assigned_people){
		
		$is_assigned = false;
		
		foreach($assigned_people as $person_id){
			if(CompanyUtil::isInactiveUser($person_id)){
				$is_assigned = true;
				break;
			}
		}
		return $is_assigned;
	}
}