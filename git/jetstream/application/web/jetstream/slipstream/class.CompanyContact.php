<?php

/**
 * class.Company.php - contain Company class
 * @package database
 * include DB API classes
 * include Util class for validation
 * include Status class for status return
 */

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/include/class.RecContact.php';
require_once BASE_PATH . '/include/class.RecAccount.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/lib.date.php';
require_once BASE_PATH . '/slipstream/class.Status.php';
require_once BASE_PATH . '/slipstream/class.Util.php';
require_once BASE_PATH . '/slipstream/Inspekt.php';
require_once BASE_PATH . '/include/class.RecPeopleContact.php';
require_once BASE_PATH . '/include/class.RecPeopleAccount.php';
require_once BASE_PATH . '/include/class.fieldsMap.php';
require_once BASE_PATH . '/slipstream/class.Contact.php';
require_once BASE_PATH . '/slipstream/class.Account.php';
require_once BASE_PATH . '/include/class.ReportingTree.php';
require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';
require_once BASE_PATH . '/slipstream/class.RestrictCompanyContact.php';
require_once BASE_PATH . '/include/class.DashboardSearchCreatorNew.php';

/**
 * Company class declaration
 * return the status OK or FALSE
 * declare createCompany() function
 * @param $newData - set new datas for insert
 * @param $sessLabels - set lables for company
 * @param $type - define the type of save insert or update
 */

class CompanyContact
{
	private $record;
	private $map;
	private $record_type;
	private $record_owner;
	public $children;
	public $assigned_people_arr;
	public $title;
	public $first_name;
	public $email;
	public $last_name;
	public $form_type;
	public $is_assigned_to_cur_user = false;

	public function SetType($type) {
		if (isset($type) && ($type == 'contact' || $type == 'company')) {
			$this->record_type = $type;
		}
		return $this;
	}

	/* This function saves the account or contact in the database. */
	public function ProcessForm() {
		$status = new Status();
		$status = $this->validateFormFields($_POST, $status);
		
		if (isset($_POST['check_private']) && ($_POST['check_private'])) {
			$input_private = 1;
		} else {
			$input_private = 0;
		}
		if (isset($_POST['inactive_check']) && ($_POST['inactive_check'])) {
			$input_inactive = 1;
		} else {
			$input_inactive = 0;
		}
		
		if (isset($_POST['account']) && (trim($_POST['account']) != '')) {
			$account = trim($_POST['account']);
		} else {
			$account = '';
		}
		$input = Inspekt::makePostCage();
		$results = array();
		
		if ($input->testOneOf('rec_type', array('contact', 'company'))) {
			switch ($input->getAlpha('rec_type')) {
				case 'contact' :
					$this->record_type = 'contact';
					break;
				case 'company' :
					$this->record_type = 'company';
					break;
				default :
					throw new Exception("Bad Request - Invalid Rec Type");
			}
		} else {
			throw new Exception("Bad Request - Unknown Rec Type");
		}
		
		if ($input->testInt('ID')) {
			$results['action'] = 'update';
			$this->Load($this->record_type, $input->getInt('ID'));
			// TODO:  Ensure that record load loads only from this company's data set
		} else {
			$results['action'] = 'insert';
			$this->Create($this->record_type);
		}
		
		$this->record->Initialize();
		
		$util = new Util();
		//Validate Field values
		

		if ($this->record_type == 'contact') {
			if ($input->testInt('AccountID')) {
				$this->record['AccountID'] = $input->getInt('AccountID');
				// TODO Test that the account belongs to the company and the user can write to it
			} else {
				// This thing isn't going to save because it can't attach it to an account
			}
		}
		
		// Iterate through the field map section by section, row by row, and field by field
		foreach ($this->map as $section) {
			if (!isset($section['fieldmap']) || count($section['fieldmap']) == 0) {
				continue;
			}
			foreach ($section['fieldmap'] as $row) {
				foreach ($row as $field) {
					$val = $input->noTags($field['FieldName']);
					if (empty($val)) {
						// Empty form fields are the equivalent of NULL DB records
						$this->record[$field['FieldName']] = NULL;
						continue;
					}
					
					// Scrub Data based on field type
					switch (substr($field['FieldName'], 0, 4)) {
						case 'Bool' : // Booleans
							// if ($field_val == 'true') {
							if ($input->getAlpha($field['FieldName']) == 'true') {
								$this->record[$field['FieldName']] = 1;
							} else {
								$this->record[$field['FieldName']] = 0;
							}
							break;
						
						case 'Numb' : // Numbers make this thing numb
							$result = str_replace(array('$', ",", "%"), "", $input->noTags($field['FieldName']));
							if (is_numeric($result)) {
								if (empty($result)) {
									$this->record[$field['FieldName']] = NULL;
								} else {
									$this->record[$field['FieldName']] = $result;
								}
							} else {
								$status->addError('Must be a number value', $field['FieldName']);
							}
							break;
						
						case 'Sele' : // Dropdowns
							// TODO:  Ensure that the choice is a valid selection
							$result = $input->getInt($field['FieldName']);
							if (empty($result)) {
								$this->record[$field['FieldName']] = NULL;
							} else {
								$this->record[$field['FieldName']] = $result;
							}
							break;
						
						case 'Date' :
							// TODO Ensure that dates follow a local standard.
							$date = $input->noTags($field['FieldName']);
							$date = str_replace('-', ' ', $date);
							$date = str_replace('/', ' ', $date);
							
							list ($month, $day, $year) = explode(" ", $date);
							
							if (is_numeric($month) && is_numeric($day) && is_numeric($year) && checkdate($month, $day, $year)) {
								$this->record[$field['FieldName']] = TimestampToMsSql(mktime(0, 0, 0, $month, $day, $year));
							} else {
								$status->addError('Invalid date', $field['FieldName']);
							}
							break;
						
						case 'Text' :
							$val_type = (isset($field['ValidationType']) && !empty($field['ValidationType'])) ? $field['ValidationType'] : 0;
							switch ($val_type) {
								case 1 :
									$phone = $input->noTags($field['FieldName']);
									$digits = $input->getDigits($field['FieldName']);
									switch (strlen($digits)) {
										case 7 :
											if ($digits[0] != '1') {
												$phone = substr($digits, 0, 3) . '-' . substr($digits, 3);
											}
											break;
										
										case 11 :
											if ($digits[0] != 1) break;
											$digits = substr($digits, 1);
										
										case 10 :
											if ($digits[0] == 1) break;
											$phone = '(' . substr($digits, 0, 3) . ') ' . substr($digits, 3, 3) . '-' . substr($digits, 6);
											break;
									}
									$this->record[$field['FieldName']] = $phone;
									break;
								case 4 : // URL
									$val = $input->noTags($field['FieldName']);
									if (!strstr($val, '://')) {
										$val = "http://$val";
									}
									if (!Inspekt::isUri($val)) {
										$status->addError('Invalid URL', $field['FieldName']);
									} else {
										$this->record[$field['FieldName']] = $val;
									}
									break;
								case 5 : // Email
									if (!$input->testEmail($field['FieldName'])) {
										$status->addError('Invalid Email Address', $field['FieldName']);
									} else {
										$this->record[$field['FieldName']] = $input->noTags($field['FieldName']);
									}
									break;
								default :
									$this->record[$field['FieldName']] = $input->noTags($field['FieldName']);
							}
							break;
					}
				}
			}
		}
		// TODO:  For new records, validate that the record's unique data doesn't already exist
		

		// If the contact is an individual contact then while editing it can be assigned to an existing or a new account.
		if (($this->record_type == 'contact') && trim($account) != '') {
			$account_sql = "SELECT AccountID
				FROM Account
				WHERE " . $_SESSION['account_name'] . " = ? AND CompanyID = ? AND Deleted != 1";
			
			$account_sql = SqlBuilder()->LoadSql($account_sql)->BuildSql(array(DTYPE_STRING, $account), array(DTYPE_INT, $_SESSION['USER']['COMPANYID']));
			
			$account_result = DbConnManager::GetDb('mpower')->Exec($account_sql);
			
			if (isset($account_result[0]['AccountID']) && ($account_result[0]['AccountID'] > 0)) {
				$accountId = $account_result[0]['AccountID'];
			} else {
				$account_info = new RecAccount();
				$rs_sql = "SELECT * FROM Account WHERE 0 = 1";
				$rs = DbConnManager::GetDb('mpower')->Execute($rs_sql);
				$rs[] = $account_info;
				
				$account_info->SetDatabase(DbConnManager::GetDb('mpower'));
				$account_info->CompanyID = $_SESSION['company_obj']['CompanyID'];
				$account_info->$_SESSION['account_name'] = $account;
				$rs->Initialize()->Save();
				$accountId = $account_info->AccountID;
				
				/* When a new company name is entered while adding/updating the contact from the contact form
				, it also updates the dashboard search table.*/
				$obj_search = new DashboardSearchCreator();
				$obj_search->company_id = $_SESSION['company_obj']['CompanyID'];
				$obj_search->record_id = $accountId;
				$obj_search->StoreAccountRecords();
			}
			$this->record['AccountID'] = $accountId;
			$this->account_id = $accountId;
		}
		
		// Set the private contact/account.
		($this->record_type == 'contact') ? ($private_field = "PrivateContact") : ($private_field = "");
		$id_field = ($this->record_type == 'contact') ? 'ContactID' : 'AccountID';
		
		if ($results['action'] == 'insert') {
			if ($this->record_type == 'contact') {
				$this->record[$private_field] = $input_private;
			}
		} else if ($results['action'] == 'update') {
			if (($this->record_type == 'contact') && $this->CheckOwner($this->record[$id_field], $this->record_type)) {
				$this->record[$private_field] = $input_private;
			}
			if ($input_inactive && $this->hasActiveOpp($input->getInt('ID'))) {
				$status->addError('Cannot inactivate this ' . $this->record_type . '.', 'inactive_check');
			} else {
				$this->record['Inactive'] = $input_inactive;
			}
		}
		
		$results['status'] = $status->GetStatus();
		
		if ($results['status']['STATUS'] == 'OK') {
			$this->record->Save();
						
			/* When a account/contact is created newly or updated,
			then it also updates the dashboard search table. */
			$obj_search = new DashboardSearchCreator();
			$obj_search->company_id = $_SESSION['company_obj']['CompanyID'];
			$obj_search->record_id = $this->record[$id_field];
			
			if ($this->record_type == 'contact') {
				$obj_search->StoreContactRecords();
			} else {
				$obj_search->StoreAccountRecords();
			}
			
			if ($results['action'] == 'insert') {
				// Set the owner of the contact/account.
				$id_field = ($this->record_type == 'contact') ? 'ContactID' : 'AccountID';
				
				$this->CreateOwner($this->record_type);
				$this->record_owner->Initialize();
				
				if (isset($_SESSION['USER']['USERID']) && ($_SESSION['USER']['USERID'] > 0)) {
					$this->record_owner[$id_field] = $this->record[$id_field];
					$this->record_owner['PersonID'] = $_SESSION['USER']['USERID'];
					$this->record_owner['CreatedBy'] = 1;
					$this->record_owner->Save();
				}
				$results['dest'] = "?action={$this->record_type}&" . (($this->record_type == 'contact') ? 'contact' : 'account') . "Id={$this->record[$id_field]}";
			} else {
				$results['title'] = $this->GetName();
				$results['view'] = $this->DrawView();
				if (($this->record_type == 'contact') && trim($account) != '') {
					$results['account'] = '<a href="?action=company&accountId=' . $accountId . '">' . $account . '</a>';
				}
				$results['dest'] = "?action={$this->record_type}&" . (($this->record_type == 'contact') ? 'contact' : 'account') . "Id={$input->getInt(ID)}";
			}
			$results['active_status'] = $this->record['Inactive'];
			($this->record_type == 'contact') ? ($results['private_status'] = $input_private) : ($results['private_status'] = 0);
			
			if ($this->record_type == 'company' && $input->getInt('ID')) {
				//print_r('--------->'.$input->getInt('ID'));
				$sql = 'update contact set Inactive=' . $input_inactive . ' where AccountID=?';
				$param[] = array(DTYPE_INT, $input->getInt('ID'));
				$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($param);
				$rs = DbConnManager::GetDb('mpower')->Execute($sql);
			}			
			
			if ($results['action'] == 'insert' && $this->record_type == 'contact') {
				
				$field_map_obj = new fieldsMap();
				$email_list = $field_map_obj->getContactEmailFields();
				//$contact_email_field = $email_list[0];
				$contact_id = $this->record['ContactID'];
				require_once (BASE_PATH . '/slipstream/class.Note.php');
				require_once (BASE_PATH . '/include/mpconstants.php');
				
				// Need to move this to a new method. 
				if(!empty($email_list)){
					foreach($email_list as $contact_email_field) {					
						if (!empty($contact_email_field)) {
							$email_address = $this->record[$contact_email_field];
							
							// Create Notes
							// if the contact is the sender then create a note for the contact immedately.
							// if the contact is receiver, check if the sender is a salesperson then create a note for the contact.
							$sql = "SELECT EmailAddresses.EmailContentID, EmailAddresses.EmailAddressType, emailcontent.ReceivedDate  
									FROM emailcontent, EmailAddresses 
									WHERE emailcontent.CompanyID = ? AND EmailAddresses.EmailcontentID = emailcontent.EmailcontentID AND EmailAddresses.EmailAddress = ?";
							$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']), array(DTYPE_STRING, $email_address));
							$email_addresses = DbConnManager::GetDb('mpower')->GetSqlResult($sql);
														
							if (!empty($email_addresses)) {
								//foreach ($email_addresses as $email) {
								while ($email = mssql_fetch_assoc($email_addresses)) {
									$email_content_id = $email['EmailContentID'];
									
									$objNote = new Note();
									$objNote->noteCreator = ($email['EmailAddressType'] == 'From') ? 0 : $_SESSION['USER']['USERID'];										
									$objNote->noteCreationDate = $email['ReceivedDate'];
									$objNote->noteSubject = 'Email';
									$objNote->noteText = 'Email';
									$objNote->noteObjectType = NOTETYPE_CONTACT;
									$objNote->noteObject = $contact_id;
									$objNote->noteSplType = NOTETYPE_EMAIL;
									$objNote->noteEmailContentID = $email_content_id;
									$objNote->notePrivate = 0;
									$result = $objNote->createNote();
								}
							}
							
							if (!empty($email_address)) {
								$sql = 'UPDATE EmailAddresses SET ContactID = ? FROM emailcontent
										WHERE emailcontent.CompanyID = ? AND EmailAddresses.EmailcontentID = emailcontent.EmailcontentID AND 
												EmailAddresses.EmailAddress = ?';
								
								$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $contact_id), array(DTYPE_INT, $_SESSION['USER']['COMPANYID']), array(DTYPE_STRING, $email_address));
								
								$data = DbConnManager::GetDb('mpower')->Exec($sql);
							}
						}
					}	
				}
			}
		
		}
		
		return $results;
	}

	public function hasActiveOpp($rId) {
		
		if ($this->record_type == 'company') {
			$sql = 'select * from Opportunities WHERE AccountID=? AND (Category BETWEEN 1 AND 5 OR Category = 10)';
			$param[] = array(DTYPE_INT, $rId);
			$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($param);
			$rs = DbConnManager::GetDb('mpower')->Execute($sql);
			if (count($rs) > 0) return true;
		} else {
			$sql = 'select * from Opportunities WHERE ContactID=? AND (Category BETWEEN 1 AND 5 OR Category = 10)';
			$param[] = array(DTYPE_INT, $rId);
			$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($param);
			$rs = DbConnManager::GetDb('mpower')->Execute($sql);
			if (count($rs) > 0) return true;
		}
		return false;
	}

	public function IsInactive() {
		return ($this->record['Inactive'] == 1) ? TRUE : FALSE;
	}

	public function IsPrivate() {
		if ($this->record->InRecord('PrivateContact') && $this->record['PrivateContact'] == 1) return TRUE;
		if ($this->record->InRecord('PrivateAccount') && $this->record['PrivateAccount'] == 1) return TRUE;
		return FALSE;
	}

	public function GetName() {
		if ($this->record_type == 'contact') {
			$contact_map_fname = $_SESSION['contact_first_name'];
			$contact_map_lname = $_SESSION['contact_last_name'];
			
			return $this->record[$contact_map_fname] . ' ' . $this->record[$contact_map_lname];
		}
		$company_map_name = $_SESSION['account_name'];
		return $this->record[$company_map_name];
	}

	public function GetAccount() {
		return $this->account_id;
		// return ($this->record['AccountID']);
	}

	public function SetAccount($value) {
		$this->account_id = $value;
		// $this->record['AccountID'] = $value;
		return $this;
	}

	public function Create($object_type) {
		if ($object_type == 'contact') {
			$this->record = new RecContact();
			$rs_sql = "SELECT * FROM Contact WHERE 0 = 1";
			$map = 'CONTACTMAP';
		} elseif ($object_type == 'company') {
			$this->record = new RecAccount();
			$rs_sql = "SELECT * FROM Account WHERE 0 = 1";
			$map = 'ACCOUNTMAP';
		}
		
		$this->record_type = $object_type;
		
		$rs = DbConnManager::GetDb('mpower')->Execute($rs_sql);
		$rs[] = $this->record;
		$this->record->SetDatabase(DbConnManager::GetDb('mpower'));
		
		$this->record['CompanyID'] = $_SESSION['company_obj']['CompanyID'];
		$this->map = $this->GetMapArray($map);
	}

	public function CreateOwner($object_type) {
		if ($object_type == 'contact') {
			$this->record_owner = new RecPeopleContact();
			$rs_sql = "SELECT * FROM PeopleContact WHERE 0 = 1";
		} elseif ($object_type == 'company') {
			$this->record_owner = new RecPeopleAccount();
			$rs_sql = "SELECT * FROM PeopleAccount WHERE 0 = 1";
		}
		
		$rs = DbConnManager::GetDb('mpower')->Execute($rs_sql);
		$rs[] = $this->record_owner;
		$this->record_owner->SetDatabase(DbConnManager::GetDb('mpower'));
	}

	public function createFetchSql($tableName) {
		$sqlStr = '';
		$sql = "select COLUMN_NAME as FieldName from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = ?";
		$param[] = array(DTYPE_STRING, $tableName);
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($param);
		$tableFields = DbConnManager::GetDb('mpower')->Exec($sql);
		if (count($tableFields) > 0) {
			foreach ($tableFields as $key => $fields) {
				if (substr($fields['FieldName'], 0, 4) == 'Date') {
					if ($key != (count($tableFields) - 1))
						$sqlStr .= "CONVERT(CHAR(10)," . $fields['FieldName'] . ",105) as " . $fields['FieldName'] . ",";
					else
						$sqlStr .= "CONVERT(CHAR(10)," . $fields['FieldName'] . ",105) as " . $fields['FieldName'];
				} else {
					if ($key != (count($tableFields) - 1))
						$sqlStr .= "[" . $fields['FieldName'] . "],";
					else
						$sqlStr .= "[" . $fields['FieldName'] . "]";
				}
			}
		}
		return $sqlStr;
	}

	public function Load($object_type, $value, $lookup_field = "id") {
		$param[] = array(DTYPE_INT, $value);
		if ($object_type == 'contact') {
			$record = 'RecContact';
			switch ($lookup_field) {
				case "id" :
					//$sql = "SELECT * FROM Contact WHERE ContactID = ?";
					$sql = "Select " . $this->createFetchSql('Contact') . " FROM Contact WHERE ContactID = ? AND Deleted != 1";
			}
		} else {
			$record = 'RecAccount';
			switch ($lookup_field) {
				case "id" :
					//$sql = "SELECT * FROM Account WHERE AccountID = ?";
					$sql = "Select " . $this->createFetchSql('Account') . " FROM Account WHERE AccountID = ? AND Deleted != 1";
			}
		}
		$this->record_type = $object_type;
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($param);
		$this->record = DbConnManager::GetDb('mpower')->GetOne($sql, $record);
		$this->map = $this->GetMapArray($object_type);
		$this->account_id = $this->record['AccountID'];
	}

	public function GetMapArray($type) {
		// TODO Move this contact/account map structure to the SESSION so it doesn't have to be reloaded from the database each time
		

		if ($type == 'ACCOUNTMAP' || $type == 'account' || $type == 'company') {
			$type == 'ACCOUNTMAP';
			$section_sql = "SELECT * FROM Section WHERE CompanyID = ? AND AccountSectionName IS NOT NULL ORDER BY SectionID";
			$section_name_field = 'AccountSectionName';
			$map_sql = "SELECT * FROM AccountMap WHERE CompanyID = ? AND SectionID IS NOT NULL AND Position IS NOT NULL ORDER BY SectionID, Position, Align";
		} elseif ($type == 'CONTACTMAP' || $type == 'contact') {
			$type == 'CONTACTMAP';
			$section_sql = "SELECT * FROM Section WHERE CompanyID = ? AND ContactSectionName IS NOT NULL ORDER BY SectionID";
			$section_name_field = 'ContactSectionName';
			$map_sql = "SELECT * FROM ContactMap WHERE CompanyID = ? AND SectionID IS NOT NULL AND Position IS NOT NULL ORDER BY SectionID, Position, Align";
		}
		
		$param = array(DTYPE_INT, $_SESSION['company_obj']['CompanyID']);
		
		$section_sql = SqlBuilder()->LoadSql($section_sql)->BuildSql($param);
		
		$section_result = DbConnManager::GetDb('mpower')->Exec($section_sql);
		
		$map_sql = SqlBuilder()->LoadSql($map_sql)->BuildSql($param);
		
		$map_result = DbConnManager::GetDb('mpower')->Exec($map_sql);
		
		$section_map = array();
		foreach ($section_result as $section) {
			$section_map[$section['SectionID']] = array('name' => $section[$section_name_field]);
		}
		
		foreach ($map_result as $field) {
			if (isset($section_map[$field['SectionID']])) {
				$position = $field['Position'];
				$align = ($field['Align'] != 0 && $field['Align'] != 1) ? 1 : $field['Align'];
				if (is_numeric($position) && ($position > 0) && !isset($section_map[$field['SectionID']]['fieldmap'][$position][$align])) {
					$section_map[$field['SectionID']]['fieldmap'][$position][$align] = $field;
				}
			
			}
		}
		
		ksort($section_map);
		return $section_map;
	}

	public function CheckOwner($id, $type) {
		($type == 'contact') ? ($table_name = 'PeopleContact') : ($table_name = 'PeopleAccount');
		($type == 'contact') ? ($field_name = 'ContactID') : ($field_name = 'AccountID');
		$sql = "SELECT count(*) AS CNT FROM $table_name WHERE $field_name = ? AND PersonID = ? AND CreatedBy = 1";
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id), array(DTYPE_INT, $_SESSION['USER']['USERID']));
		//echo $sql;
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		
		if ($result[0]['CNT'] == 1) {
			return true;
		}
		return false;
	}

	public function IsRestricted($type = 'contact') {
		
		$objContact = new Contact();
		$objAccount = new Account();
		if ($type == 'contact')
			return $objContact->IsRestricted($this->record['ContactID'], $_SESSION['USER']['USERID'], $_SESSION['company_obj']['CompanyID']);
		elseif ($type == 'company')
			return $objAccount->IsRestricted($this->record['AccountID'], $_SESSION['USER']['USERID'], $_SESSION['company_obj']['CompanyID']);
		else
			return false;
	}

	public function CheckPrivatePermission($id, $type) {
		$str_permission = array();
		($type == 'contact') ? ($table_name = 'PeopleContact') : ($table_name = 'PeopleAccount');
		($type == 'contact') ? ($field_name = 'ContactID') : ($field_name = 'AccountID');
		$sql = "SELECT count(*) AS CNT FROM $table_name WHERE $field_name = ? AND PersonID = ?";
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id), array(DTYPE_INT, $_SESSION['USER']['USERID']));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		
		if ($result[0]['CNT'] > 1) {
			$str_permission['STATUS'] = false;
			$str_permission['MESSAGE'] = 'Can not make it private. Because, this ' . $type . ' has been assigned to more than one person.';
			return $str_permission;
		}
		
		if ($type != 'contact') {
			$sql = "SELECT COUNT(*) AS CNT FROM Contact WHERE ContactID = ? AND PrivateContact = 0 ";
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id));
			$result = DbConnManager::GetDb('mpower')->Exec($sql);
			
			if ($result[0]['CNT'] > 0) {
				$str_permission['STATUS'] = false;
				$str_permission['MESSAGE'] = 'Can not make it private. Because, this ' . $type . ' has public contacts.';
				return $str_permission;
			}
			
			$note_type = NOTETYPE_COMPANY;
		} else {
			$note_type = NOTETYPE_CONTACT;
			$sql = "SELECT COUNT(*) AS CNT FROM Events WHERE ContactID = ? AND PrivateEvent = 0 ";
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id));
			$result = DbConnManager::GetDb('mpower')->Exec($sql);
			
			if ($result[0]['CNT'] > 0) {
				$str_permission['STATUS'] = false;
				$str_permission['MESSAGE'] = 'Can not make it private. Because, this ' . $type . ' has public events.';
				return $str_permission;
			}
		}
		
		$sql = "SELECT COUNT(*) AS CNT FROM Opportunities WHERE $field_name = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		
		if ($result[0]['CNT'] > 0) {
			$str_permission['STATUS'] = false;
			$str_permission['MESSAGE'] = 'Can not make it private. Because, this ' . $type . ' has opportunities.';
			return $str_permission;
		}
		
		$sql = "SELECT COUNT(*) AS CNT FROM Notes WHERE ObjectType = ? AND ObjectReferer = ? AND PrivateNote = 0 ";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $note_type), array(DTYPE_INT, $id));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		
		if ($result[0]['CNT'] > 0) {
			$str_permission['STATUS'] = false;
			$str_permission['MESSAGE'] = 'Can not make it private. Because, this ' . $type . ' has public notes.';
			return $str_permission;
		}
		$str_permission['STATUS'] = true;
		return $str_permission;
	}

	public function DrawView($form = false, $main_only = false) {
		if (!isset($this->map)) $this->map = $this->GetMapArray($this->record_type);
		
		$output = '';
		if (!$form && ($this->IsRestricted('contact') || $this->IsRestricted('company'))) return 'You are not allowed to view information';

		if ($form) {
			$output .= '<input type="hidden" name="rec_type" value="' . $this->record_type . '" />';
			/*if(isset($_REQUEST['accountId']) && ($_REQUEST['accountId'] > 0)) {
				$this->account_id = $_REQUEST['accountId'];
			}*/
			if ($this->record_type == 'contact') {
				$output .= '<input type="hidden" name="AccountID" value="' . $this->account_id . '" />';
				// $output .= '<input type="hidden" name="AccountID" value="' . $this->record['AccountID'] . '" />';
			}
			
			$id_field = ($this->record_type == 'contact') ? 'ContactID' : 'AccountID';
			if (!empty($this->record) && $this->record->InRecord($id_field) && (!isset($_GET['formaction']) || ($_GET['formaction'] != 'add'))) {
				$output .= '<input type="hidden" name="ID" value="' . $this->record[$id_field] . '" />';
				$this->form_type = 'edit';
				$output .= '<input type="hidden" name="FormType" value="' . $this->form_type . '" />';
			} else {
				$output .= '<input type="hidden" name="ID" value="" />';
				$this->form_type = 'add';
				$output .= '<input type="hidden" name="FormType" value="' . $this->form_type . '" />';
			}
		}
		
		$first = true;
		$company_shown = false;
		$private_shown = false;
		$inactive_shown = false;
		
		$mail_data = '';
		if ($this->record_type == 'contact' && !$form) {
			//$mail_data='body=JETSTREAM-'. base64_encode('contact&'.$_SESSION['USER']['USERID'].'&'.$this->record->ContactID.'&'.$_SESSION['USER']['COMPANYID']);
			

			$field_map_obj = new fieldsMap();
			$list = $field_map_obj->parseContactFields();
			
			$qstr = '';
			foreach ($list as $key => $value) {
				if (empty($qstr))
					$qstr = $key . '::::' . $this->record[$key];
				else
					$qstr .= '%0A' . $key . '::::' . $this->record[$key];
			}
			
			$mail_data = 'body=JETSTREAM-' . $qstr;
		
		} elseif ($this->record_type == 'company' && !$form) {
			//$mail_data='body=JETSTREAM-'. base64_encode('account&'.$_SESSION['USER']['USERID'].'&'.$this->record->AccountID.'&'.$_SESSION['USER']['COMPANYID']);
			$field_map_obj = new fieldsMap();
			$list = $field_map_obj->parseAccountFields();
			
			$qstr = '';
			foreach ($list as $key => $value) {
				if (empty($qstr))
					$qstr = $key . '::::' . $this->record[$key];
				else
					$qstr .= '%0A' . $key . '::::' . $this->record[$key];
			}
			
			$mail_data = 'body=JETSTREAM-' . $qstr;
		}
		
		foreach ($this->map as $section_id => $section) {
			if (!isset($section['fieldmap']) || count($section['fieldmap']) == 0) {
				continue; // Don't draw empty sections
			}
			
			$section_id = 'section' . $section_id;
			if ($first) {
				$hidden_class = '';
				$first = false;
			} else {
				if ($main_only) break;
				if ($form) {
					$hidden_class = '';
					$section_head_class = 'section_head section_form';
					$output .= '<div class="section_head section_form">';
				} else {
					$section_head_class = 'section_head';
					if (!isset($_COOKIE[$section_id])) {
						$hidden_class = ' hidden';
						$toggle_image = 'plus.gif';
					} else {
						$hidden_class = '';
						$toggle_image = 'minus.gif';
					}
					$output .= '<div class="section_head" onclick="javascript:ToggleHidden(\'' . $section_id . '\');"><img id="toggle_' . $section_id . '" src="images/' . $toggle_image . '">';
				}
				$output .= $section['name'] . '</div>';
			}
			
			$output .= '<div id="' . $section_id . '" class="jet_datagrid' . $hidden_class . '"><table>';
			
			$id_field = ($this->record_type == 'contact') ? 'ContactID' : 'AccountID';
			$this->checkRestriction($this->record[$id_field], $this->record_type);
			
			if ($this->record_type == 'contact' && !$company_shown) {
				//if($form && !($this->account_id > 0)) {
				if ($form) {
					if (isset($_REQUEST['accountId']) && ($_REQUEST['accountId'] > 0)) {
						$comp_name = $this->GetCompanyName($_REQUEST['accountId'], false);
						$this->account_id = $_REQUEST['accountId'];
					} else if (isset($_REQUEST['contactId']) && ($_REQUEST['contactId'] > 0)) {
						$comp_name = $this->GetCompanyName($this->account_id, false);
					} else {
						$comp_name = '';
					}
					
					// Added code for Limiting access.
					if (!($this->is_assigned_to_cur_user) && $_SESSION['limit_account_access'] == ACCESS_UNASSIGNED_FALSE) {
						$comp_name = '';
					}
					$output .= '<tr><td class="label">Company:</td><td><span id="account_span"><input type="text" name="account" id="account" value="' . $comp_name . '" /></span></td><td>&nbsp;</td><td>&nbsp;</td></tr>';
					$company_shown = true;
				} else {
					$assigned_salesperson = $this->GetAssignedPeople($this->record['ContactID'], 'contact', $owner);
					$salesperson_list = array();
					//print_r($assigned_salesperson);
					if (count($assigned_salesperson) > 0) {
						foreach ($assigned_salesperson as $person_id) {
							if ($owner == $person_id) {
								$name = $_SESSION['tree_obj']->GetInfo($person_id, 'FirstName') . ' ' . $_SESSION['tree_obj']->GetInfo($person_id, 'LastName');
							} else {
								$name = $_SESSION['tree_obj']->GetInfo($person_id, 'FirstName') . ' ' . $_SESSION['tree_obj']->GetInfo($person_id, 'LastName');
							}
							$salesperson_list[] = $name;
						}
					}
					
					// Added code for Limiting access.
					if (!$this->is_assigned_to_cur_user) {
						if ($_SESSION['limit_account_access'] == ACCESS_UNASSIGNED_FALSE) {
							$comp_name = '';
						} else {
							$comp_name = $this->GetCompanyName($this->account_id, true);
						}
						$salesperson_list = array();
					} else {
						$comp_name = $this->GetCompanyName($this->account_id, true);
					}
					$output .= '<tr><td class="label">Company:</td><td>' . $comp_name . '&nbsp;</td><td class="label">Assigned to:</td><td>' . implode('<br>', $salesperson_list) . '&nbsp;</td></tr>';
					$company_shown = true;
				}
			}
			
			if (in_array($this->record_type, array('company', 'account')) && !$form) {
				$assigned_salesperson = $this->GetAssignedPeople($this->record['AccountID'], 'company', $owner);
				$salesperson_list = array();
				if (count($assigned_salesperson) > 0) {
					foreach ($assigned_salesperson as $person_id) {
						if ($owner == $person_id) {
							$name = $_SESSION['tree_obj']->GetInfo($person_id, 'FirstName') . ' ' . $_SESSION['tree_obj']->GetInfo($person_id, 'LastName');
						} else {
							$name = $_SESSION['tree_obj']->GetInfo($person_id, 'FirstName') . ' ' . $_SESSION['tree_obj']->GetInfo($person_id, 'LastName');
						}
						$salesperson_list[] = $name;
					}
				}
				$output .= '<tr><td class="label">&nbsp;</td><td>&nbsp;</td><td class="label">Assigned to:</td><td>' . implode('<br>', $salesperson_list) . '&nbsp;</td></tr>';
			
			}
			
			if (!$this->is_assigned_to_cur_user) {
				if ($this->record_type == 'contact') {
					$public_fields = $_SESSION['public_contact_fields'];
				} else {
					$public_fields = $_SESSION['public_account_fields'];
				}
			}
			
			foreach ($section['fieldmap'] as $position => $row) {
				// if (!$form && $position == 0) continue;
				

				for($x = 1; $x >= 0; $x--) {
					// Added code for Limiting access.
					if (!$this->is_assigned_to_cur_user && $this->record->InRecord($id_field)) {
						if ((count($public_fields) > 0) && !in_array($row[$x]['FieldName'], $public_fields)) {
							if ($x == 1) {
								$row_output = "<tr><td colspan='2'>&nbsp;</td>";
							} elseif ($row_output) {
								$row_output = FALSE;
							} else {
								$output .= "<td colspan='2'>&nbsp;</td>";
							}
							continue;
						}
					}
					if ($x == 1) {
						$output .= '<tr>';
					}
					if ($row_output) {
						$output .= $row_output;
						$row_output = FALSE;
					}
					
					$reqField = ($row[$x]['IsRequired'] == 1 && $form) ? '<span class="form_error">*</span>' : '';
					
					$output .= '<td class="label">' . $reqField . ' ' . ((isset($row[$x])) ? $row[$x]['LabelName'] . ':' : '&nbsp;') . '</td>';
					
					$output .= '<td class="' . (($x == 1) ? 'left' : 'right') . '">';
					
					$field_value = '';
					
					/*if (isset($this->record)) {
						if (isset($row[$x]) && ($this->record->InRecord($row[$x]['FieldName'], false))) {
							$field_value = $this->record[$row[$x]['FieldName']];
						}
					//} elseif ($form && $this->record_type == 'contact' && !empty($row[$x]['AccountMapID']) && isset($this->account_id)) {
					} */
					if (isset($this->record) && !isset($_REQUEST['formaction']) && isset($row[$x]) && ($this->record->InRecord($row[$x]['FieldName'], false))) {
						$field_value = $this->record[$row[$x]['FieldName']];
					} elseif ($form && $this->record_type == 'contact' && !empty($row[$x]['AccountMapID']) && isset($this->account_id)) {
						// This is a form for a new record - see if there is a related account
						// field, and if so, get the value, if there is one
						

						// Load the related Account Record
						if (!isset($this->account_rec)) $this->GetAccountRec($this->account_id);
						
						// TODO A more consistent way to store this alternate AccountMap index
						if (!isset($account_map)) {
							$account_map_sql = "SELECT * FROM AccountMap WHERE CompanyID = ? AND SectionID IS NOT NULL AND Position IS NOT NULL";
							$account_map_sql = SqlBuilder()->LoadSql($account_map_sql)->BuildSql(array(DTYPE_INT, $_SESSION['company_obj']['CompanyID']));
							$account_map = DbConnManager::GetDb('mpower')->Execute($account_map_sql, 'MpRecord', 'AccountMapID');
						}
						// if (isset($this->account_rec[$account_map[$row[$x]['AccountMapID']]['FieldName']]) && !empty($this->account_rec[$account_map[$row[$x]['AccountMapID']]['FieldName']])) {
						if ($this->account_rec->InRecord($account_map[$row[$x]['AccountMapID']]['FieldName'], TRUE)) {
							$field_value = $this->account_rec[$account_map[$row[$x]['AccountMapID']]['FieldName']];
						}
					}
					
					if ($form && isset($row[$x])) {
						if ($row[$x]['IsFirstName'] == 1 && !empty($this->first_name)) {
							$output .= $this->DrawFormField($row[$x], $this->first_name);
						} else if ($row[$x]['IsLastName'] == 1 && !empty($this->last_name)) {
							$output .= $this->DrawFormField($row[$x], $this->last_name);
						} else if ($row[$x]['ValidationType'] == 5 && !empty($this->email)) {
							$output .= $this->DrawFormField($row[$x], $this->email);
							$this->email = '';
						} else {
							$output .= $this->DrawFormField($row[$x], $field_value);
						}
					} elseif (!$form && isset($row[$x]) && (!empty($field_value) || substr($row[$x]['FieldName'], 0, 4) == 'Bool')) {
						
						// Added code for Limiting access.
						if (!$this->is_assigned_to_cur_user) {
							if ($this->record_type == 'contact') {
								//$output .= 'hi<br><br>'.$row[$x]['FieldName'].'-->'.print_r($_SESSION['public_contact_fields'], true);
								if ((count($_SESSION['public_contact_fields']) > 0) && !in_array($row[$x]['FieldName'], $_SESSION['public_contact_fields'])) {
									continue;
									if (substr($row[$x]['FieldName'], 0, 4) == 'Bool') {
										$field_value = 'none';
									} else {
										$field_value = '';
									}
								}
							} else {
								if ((count($_SESSION['public_account_fields']) > 0) && !in_array($row[$x]['FieldName'], $_SESSION['public_account_fields'])) {
									continue;
									if (substr($row[$x]['FieldName'], 0, 4) == 'Bool') {
										$field_value = 'none';
									} else {
										$field_value = '';
									}
								}
							}
						
						}
						if ($row[$x]['Parameters'] != null) {
							$paramArr = explode("|", $row[$x]['Parameters']);
						}
						switch (substr($row[$x]['FieldName'], 0, 4)) {
							case 'Numb' :
								setlocale(LC_MONETARY, 'en_US');
								
								if ($row[$x]['Parameters'] != null && $row[$x]['FieldName'] != '') {
									if ($paramArr[3] == 'Currency')
										$output .= money_format('%+n', $field_value);
									else if ($paramArr[3] == 'Percent')
										$output .= $field_value . ' % ';
									else
										$output .= $field_value;
								} else {
									$output .= $field_value;
								}
								
								break;
							case 'Sele' :
								$output .= $this->GetOptionVal($field_value);
								break;
							case 'Bool' :
								// Added code for Limiting access.
								if ($field_value != 'none') {
									$output .= ($field_value == 1) ? '<img src="images/checkbox_checked.png">' : '<img src="images/checkbox_unchecked.png">';
								} else {
									$output .= '&nbsp;';
								}
								break;
							case 'Date' :
								if ($row[$x]['Parameters'] != null) {
									if ($paramArr[6] == 1) {
										$output .= date("F d", strtotime($field_value));
									} else
										$output .= ReformatDate($field_value);
								} else {
									$output .= ReformatDate($field_value);
								}
								break;
							case 'Text' :
								// $field_value = $this->WrapText($field_value);
								$val_type = (isset($row[$x]['ValidationType']) && !empty($row[$x]['ValidationType'])) ? $row[$x]['ValidationType'] : 0;
								switch ($val_type) {
									case 4 : // URL
										if (trim($field_value) != '') {
											if (!strstr($field_value, "://")) $field_value = "http://" . $field_value;
											$output .= "<a href=\"{$field_value}\" target=\"_blank\">" . SplitLongWords($field_value) . "</a><img style=\"margin-left:5px;\" src=\"images/external_link.gif\" />";
										}
										break;
									case 5 : // Email
										if (trim($field_value) != '') {
											$output .= "<a href=\"mailto:{$field_value}?{$mail_data}\">" . SplitLongWords($field_value) . "</a><img style=\"margin-left:5px;\" src=\"images/email.gif\" />";
										}
										break;
									default :
										//$output .= SplitLongWords($field_value);
										$output .= $field_value;
								}
								break;
							default :
								$output .= 'Unknown field type...';
						}
						if (trim($field_value) == '') {
							$output .= '&nbsp;';
						}
					} else {
						$output .= '&nbsp;';
					}
					$output .= '</td>';
					if ($x == 0) {
						$output .= '</tr>';
					}
				}
			}
			
			if (($this->record_type == 'contact') && !$private_shown) {
				$id_field = ($this->record_type == 'contact') ? 'ContactID' : 'AccountID';
				($this->record_type == 'contact') ? ($private_field = "PrivateContact") : ($private_field = "PrivateAccount");
				$field_value = $this->record[$private_field];
				$checked = ($field_value == 1) ? ('checked') : ('');
				
				if ($form) {
					if (!isset($this->record[$id_field]) || ($this->record[$id_field] == 0) || empty($this->record[$id_field])) {
						$output .= '<tr><td class="label">Private:</td><td colspan="3"><div><input name="check_private" type="checkbox" value="true" ></div></td></tr>';
					} else if ($this->CheckOwner($this->record[$id_field], $this->record_type)) {
						if ($field_value == 1) {
							$output .= '<tr><td class="label">Private:</td><td colspan="3"><div><input name="check_private" type="checkbox" value="true" ' . $checked . '></div></td></tr>';
						} else {
							$make_public = $this->CheckPrivatePermission($this->record[$id_field], $this->record_type);
							if ($make_public['STATUS']) {
								$output .= '<tr><td class="label">Private:</td><td colspan="3"><div><input name="check_private" type="checkbox" value="true" ></div></td></tr>';
							} else {
								$output .= '<tr><td class="label">Private:</td><td colspan="3"><div><img src="images/checkbox_unchecked.png"><br/>
									' . $make_public['MESSAGE'] . '</div></td></tr>';
							}
						}
					}
				} else if ($this->CheckOwner($this->record[$id_field], $this->record_type)) {
					$output .= '<tr><td class="label">Private:</td><td colspan="3">' . (($field_value == 1) ? '<img src="images/checkbox_checked.png">' : '<img src="images/checkbox_unchecked.png">') . '</td></tr>';
				}
				$private_shown = true;
			}
			
			if (!$inactive_shown) {
				$field_value = $this->record['Inactive'];
				$checked_inactive = ($field_value == 1) ? ('checked') : ('');
				
				if ($form) {
					if (isset($this->record[$id_field]) && ($this->record[$id_field] > 0)) {
						$output .= '<tr><td class="label">Inactive:</td><td colspan="3"><div><input type="checkbox" name="inactive_check" id="inactive_check" value="true" ' . $checked_inactive . ' /></div>
						<div class="form_error" id="err_inactive_check"></div></td></tr>';
					}
				}
				/*else {
					if($this->record_type!='company')
					$output .= '<tr><td class="label">Inactive:</td><td colspan="3">' .(($field_value == 1) ? '<img src="images/checkbox_checked.png">' : '<img src="images/checkbox_unchecked.png">') .'</td></tr>';
				}*/
				$inactive_shown = true;
			}
			
			$output .= '</table></div>';
		}
		return $output;
	}

	private function GetAccountRec($account_id) {
		$sql = SqlBuilder()->LoadSql("SELECT * FROM Account WHERE AccountID = ?")->BuildSql(array(DTYPE_INT, $account_id));
		
		$this->account_rec = DbConnManager::GetDb('mpower')->GetOne($sql, 'RecAccount');
		
		return $output;
	}

	public function GetCompanyName($account_id, $anchor = false) {
        $company_map_name = $_SESSION['account_name'];
		$sql = SqlBuilder()->LoadSql("SELECT $company_map_name FROM Account WHERE AccountID = ?")->BuildSql(array(DTYPE_INT, $account_id));
    	$result = DbConnManager::GetDb('mpower')->GetOne($sql);
		$output = $result[$company_map_name];
		
		if ($anchor) $output = "<a href=\"?action=company&accountId=$account_id\">" . SplitLongWords($output) . "</a>";
		return $output;
	}

	private function GetOptionList($set_id) {
		$option_sql = SqlBuilder()->LoadSql("SELECT * FROM [Option] WHERE OptionSetID = ?")->BuildSql(array(DTYPE_INT, $set_id));
		$option_list = DbConnManager::GetDb('mpower')->Exec($option_sql);
		return $option_list;
	}

	/*
	private function GetOptionList($map_id) {
		$search_field = ($this->record_type == 'contact') ? 'ContactMapID' : 'AccountMapID';
		$option_sql = SqlBuilder()->LoadSql("SELECT * FROM [Option] WHERE $search_field = ?")->BuildSql(array(DTYPE_INT, $map_id));
		$option_list = DbConnManager::GetDb('mpower')->Exec($option_sql);
		return $option_list;
	}
*/
	public function GetOptionVal($option) {
		if (empty($option)) return;
		$option_sql = SqlBuilder()->LoadSql("SELECT * FROM [Option] WHERE OptionID = ?")->BuildSql(array(DTYPE_INT, $option));
		$option_rec = DbConnManager::GetDb('mpower')->GetOne($option_sql);
		return $option_rec->OptionName;
	}

	private function DrawFormField($field, $field_value) {
		$output = '';
		// $field_value = (!empty($this->record)) ? $this->record[$field['FieldName']] : '';
		

		if ($field['Parameters'] != null && $field['Parameters'] != '') {
			
			/*Additional field parameters are set with |(Pipe) as the delimiter in the following order:
		 Width,Height,Length,Format,Decimal,IsPastDate,IsMonthDay,HasYearRange,StartYear,EndYear.*/
			$param = $field['Parameters'];
			$paramArr = explode("|", $param);
		}
		
		switch (substr($field['FieldName'], 0, 4)) {
			case 'Date' :
				// TODO Standardize method of presenting dates
				

				if ($field['Parameters'] == NULL || $field['Parameters'] == '') {
					$output .= '<input type="text" size="12" name="' . $field['FieldName'] . '" Range="" value="' . ReformatDate($field_value) . '" tabindex="' . $field['TabOrder'] . '"/>';
				} else {
					//Check if the date field is of type Month/Day only.
					if ($paramArr[6] != '' && $paramArr[6] == 1) {
						$hasMonthDay = true;
						$fieldNo = substr($field['FieldName'], 4);
						$util = new Util();
						$monthArr = $util->getMonthList();
						$output .= '<select name="dateMonthField' . $fieldNo . '" tabindex="' . $field['TabOrder'] . '"><option></option>';
						foreach ($monthArr as $key => $month) {
							if ($field_value != null || strlen($field_value) != 0) $selected = ($key == date('m', strtotime($field_value))) ? 'selected' : '';
							
							$output .= "<option value=" . $key . " " . $selected . ">" . $month . "</option>";
						}
						$output .= '</select>&nbsp;';
						
						$output .= '<select name="dateDayField' . $fieldNo . '" tabindex="' . $field['TabOrder'] . '"><option></option>';
						for($k = 1; $k < 32; $k++) {
							if ($field_value != null || strlen($field_value) != 0) $selected = ($k == date('j', strtotime($field_value))) ? 'selected' : '';
							$output .= "<option value=" . $k . " " . $selected . ">" . $k . "</option>";
						}
						$output .= '</select>';
						
					//$output .= '<input type="hidden" size="12" name="' . $field['FieldName'] . '" Range="" value="'.date("m/d/y").'" />';
					} //Check if the date field has a year range.
					

					else if ($paramArr[7] != '' && $paramArr[7] == 1) {
						$startYear = $paramArr[8];
						$endYear = $paramArr[9];
						$fieldNo = substr($field['FieldName'], 4);
						$util = new Util();
						$yearRange = $util->getJqueryDateYearRange($startYear, $endYear);
						$output .= '<input type="text" size="12" name="' . $field['FieldName'] . '" Range="' . $yearRange . '" value="' . ReformatDate($field_value) . '" tabindex="' . $field['TabOrder'] . '"/>';
						//$output .= '<input type="hidden" size="12" name="' . $field['FieldName'] . '"  value="'.$yearRange.'" />';
					} 

					else {
						$output .= '<input type="text" size="12" name="' . $field['FieldName'] . '" Range="" value="' . ReformatDate($field_value) . '" tabindex="' . $field['TabOrder'] . '"/>';
					}
				}
				break;
			
			case 'Sele' :
				$options = $this->GetOptionList($field['OptionSetID']);
				if ($field['OptionType'] == 1) {
					$output .= '<select name="' . $field['FieldName'] . '" tabindex="' . $field['TabOrder'] . '"><option></option>';
					
					foreach ($options as $option) {
						// foreach ($this->GetOptionList((($this->record_type == 'contact') ? $field['ContactMapID'] : $field['AccountMapID'])) as $option) {
						$selected = ($option['OptionID'] == $field_value) ? 'selected' : '';
						$output .= "<option value='" . $option['OptionID'] . "' " . $selected . ">" . $option['OptionName'] . "</option>";
					}
					$output .= '</select>';
				} elseif ($field['OptionType'] == 2) {
					foreach ($options as $option) {
						// foreach ($this->GetOptionList((($this->record_type == 'contact') ? $field['ContactMapID'] : $field['AccountMapID'])) as $option) {
						$selected = ($option['OptionID'] == $field_value) ? 'checked' : '';
						$output .= '<div><input name="' . $field['FieldName'] . '" type="radio" value="' . $option['OptionID'] . '" ' . $selected . '>' . $option['OptionName'] . '</div>';
					}
				}
				break;
			
			case 'Bool' :
				$checked = ($field_value == 1) ? 'checked' : '';
				$output .= '<div><input name="' . $field['FieldName'] . '" type="checkbox" value="true" ' . $checked . ' tabindex="' . $field['TabOrder'] . '"></div>';
				break;
			
			case 'Numb' :
				
				if ($field['Parameters'] == NULL || $field['Parameters'] == '') {
					$output .= '<input type="text" size="30" name="' . $field['FieldName'] . '" value="' . $field_value . '" tabindex="' . $field['TabOrder'] . '"/>';
				} else {
					$output .= '<input type="text" size="' . $paramArr[0] . '" maxlength="' . $paramArr[2] . '" name="' . $field['FieldName'] . '" value="' . $field_value . '" tabindex="' . $field['TabOrder'] . '"/>';
				}
				break;
			
			default :
				if ($field['IsCompanyName'] == 1) {
					if ($this->form_type == 'edit') {
						$company_field_id = 'id="edit_company_account"';
					} else {
						$company_field_id = 'id="company_account"';
					}
				} else {
					$company_field_id = '';
				}
				if ($field['Parameters'] == NULL || $field['Parameters'] == '') {
					$output .= '<input type="text" size="30" ' . $company_field_id . ' name="' . $field['FieldName'] . '" value="' . $field_value . '" tabindex="' . $field['TabOrder'] . '"/>';
				} else { //This field has got parameters.
					if ($paramArr[2] == '')
						$max_length = 0;
					else
						$max_length = $paramArr[2];
					
					if ($paramArr[0] != '' && $paramArr[1] != '' && $paramArr[1] != '1') {
						$output .= '<textarea rows="' . $paramArr[1] . '" cols="' . $paramArr[0] . '" ' . $company_field_id . ' name="' . $field['FieldName'] . '" tabindex="' . $field['TabOrder'] . '" onkeyup="textCounter(this,' . $max_length . ');">' . $field_value . '</textarea>';
					} else {
						$output .= '<input type="text" size="' . $paramArr[0] . '" maxlength="' . $paramArr[2] . '" ' . $company_field_id . ' name="' . $field['FieldName'] . '" value="' . $field_value . '" tabindex="' . $field['TabOrder'] . '"/>';
					}
				}
		}
		
		$output .= '<div class="form_error" id="err_' . $field['FieldName'] . '"></div>';
		return $output;
	}

	public function ShowManageRec($id, $field_type) {
		if ($_SESSION['USER']['LEVEL'] == 1) {
			return false;
		}
		if ($field_type == 'contact') {
			$table_name = ($field_type == 'contact') ? ('Contact') : ('Account');
			$pk_field = ($field_type == 'contact') ? ('ContactID') : ('AccountID');
			$private_field = ($field_type == 'contact') ? ('PrivateContact') : ('PrivateAccount');
			
			$sql = SqlBuilder()->LoadSql("SELECT count(*) AS RECCNT FROM $table_name WHERE $pk_field = ? AND $private_field != 1")->BuildSql(array(DTYPE_INT, $id));
			$result = DbConnManager::GetDb('mpower')->Exec($sql);
			
			if ($result[0]['RECCNT'] == 0) {
				return false;
			}
		}
		return true;
	}

	public function GetAssignedPeople($id, $field_type, &$owner) {
		$arr_assigned_people = array();
		$obj_restriction = new RestrictCompanyContact();
		$arr_assigned_people = $obj_restriction->GetRecordAssignedPeople($id, $field_type, $owner);
		return $arr_assigned_people;
	}

	public function DrawManageForm($record_id, $type) {
		$record_id = (int) $record_id;
		
		$output = '<form onsubmit="javascript:assignPeople(\'' . $type . '\');return false;" name="assignForm" id="assignForm" method="post" action="#">';
		$output .= '<h2 style="text-indent:0px;">Assign ' . ucfirst($type) . '</h2>';
		$output .= '<table><tr>';
		$output_table = '';

		$next_tr = 0;
		$show_btn = false;
		$check_sel_all = false;
		
		$assigned_people = $this->GetAssignedPeople($record_id, $type, $owner);
		$output_table .= $this->createSections($_SESSION['login_id'], $assigned_people);
		
		if (isset($check_sel_all) && ($check_sel_all)) {
			$checked = 'checked';
		} else {
			$checked = '';
		}
		$output_first_tr = '<td><input type="checkbox" name="sel_all" id="sel_all" value="true" onclick="selectAllCheckBox();" ' . $checked . ' /> Select All</td></tr><tr><td>';
		$output .= $output_first_tr . $output_table;
		$output .= '</tr><tr><td>&nbsp;</td></tr></table>';
		
		if (trim($output_table) != '') {
			$output .= '<center><input type="submit"  id="btnAssign" name="btnAssign" value="Assign" />&nbsp;
				<input type="button" value="Cancel" name="cancel" size="30" onclick="javascript:closeManageForm();"/>
				</center>';
			
			$output .= '<input type="hidden" name="ID" value="' . $record_id . '" />';
			
			  
			foreach($this->children as $child){
				$output .= '<input type="hidden" name="children[]" id="' . $child . '" value="' . $child . '" />';
			}
			
		}
		$output .= '</form>';
		return $output;
	}

	public function createSections($person_id, $assigned_people) {
		
		$person_records = $_SESSION['tree_obj']->GetRecord($person_id);
		$isMasterAssignee = ReportingTreeLookup::IsMasterAssignee($_SESSION['company_id'], $person_id);
		$children = array();	
		$level = $person_records['Level']; // Set default level - used for section breaking

		// If user is Master Assignee, just show all employees minus m-power admins
		// 06/26/2013 - Vern Gorman - Blue Star Technology
		if($isMasterAssignee){
			$users = $this->getAllCompanyUsers($_SESSION['company_id']);
			foreach($users as $user){
				$blevel = $user['Level'];
				if ($blevel > $level) {
					$level = $blevel; // track highest level for section breaking
				}
				array_push($children, $user);				
			}
		}
		else {
			$checked = (in_array($person_records['PersonID'], $assigned_people)) ? 'checked':'';
			$output = '<input type="checkbox" name="people[]" id="' . $person_records['PersonID'] . '" value="' . $person_records['PersonID'] . '" ' . $checked . ' />';
			$output .= $person_records['FirstName'] . ' ' . $person_records['LastName'];
			
			$supervisorName =  $this->getSupervisorName($person_records['SupervisorID']);
			if(strlen($supervisorName) > 0){
				$output .= ' <em>(' . $this->getSupervisorName($person_records['SupervisorID']) . ')<em>';
			}
			
			$child_records = $this->getChildRecords($person_id);
			$children = $this->getChildren($child_records);

			$tmp = $this->clean($children);	// remove duplicates and sort
			$children = $tmp;
			$this->setChildIds($children);
		}
				
		// Loop on the children sorted by level, supervisor ID, first and last name
		foreach($children as $child){
			// Break on the level starting at the top most level.
			if($child['Level'] != $level){
				$level = $child['Level'];
				$output .= '</td><td>';
			}

			if (in_array($child['PersonID'], $assigned_people)) {
				$output .= '<input type="checkbox" name="people[]" id="' . $child['PersonID'] . '" value="' . $child['PersonID'] . '" checked ';				
			} else {
				$output .= '<input type="checkbox" name="people[]" id="' . $child['PersonID'] . '" value="' . $child['PersonID'] . '" ';
			}

			$output .= '/>';
			$output .= '<label for="' . $child['PersonID'] . '">' . $child['FirstName'] . ' ' . $child['LastName'];
			$supervisorName =  $this->getSupervisorName($child['SupervisorID']);
			if(strlen($supervisorName) > 0){
				$output .= ' <em>(' . $supervisorName . ')</em>';
			}
			$output .= '</label><br>';
			//$output .= '<label style="border: 1px solid #000; width:25px; padding 3px; background-color:' . $this->getSupervisorColor($child['SupervisorID']) . ';">&nbsp;&nbsp;&nbsp;</label><label for="' . $child['PersonID'] . '" style="width: 100px; border: 1px solid #000; background-color:' . $child['Color'] . ';">' . $start_tag . $child['FirstName'] . ' ' . $child['LastName']  . $end_tag . '</label><br>';

		}
		
		return $output;
	}
	
	/**
	 * Get an array of child records for a given person
	 * @param array $child_ids
	 * @param array $children
	 */
	public function getChildren($child_ids, $children = array()){

		foreach($child_ids as $child_id){
			$child = $_SESSION['tree_obj']->GetRecord($child_id);
			if ($child) {
				array_push($children, $child); // Save the people record for the current sales person
	
				if(count($child['children']) > 0){
					// The subordinate has their own subordinates so call this function recursively to load them up
					$tmp = array();
					$tmp = $this->getChildren($child['children']);					
					$children = array_merge($children, $tmp);
				}
			}
		}
		return $children;
	}
	
	private function setChildIds($children){
		$tmp = array();
		foreach($children as $k => $v){
			array_push($tmp, $v['PersonID']);
		}
		$this->children = $tmp;
	}
	
	/**
	 * Remove duplicate records from an array
	 * Necessary due to recursion used in getChildren
	 * Return value is also sorted by Name
	 * @param array $arr
	 */
	public function clean($arr){
		
		
		$name = array();
		$level = array();
		
		foreach($arr as $k => $v){
			$firstName[$k] = $v['FirstName'];
			$lastName[$k] = $v['LastName'];
			$supervisorID[$k] = $v['SupervisorID'];
			$level[$k] = $v['Level'];
		}
		array_multisort($level, SORT_DESC, $supervisorID, SORT_DESC, $firstName, SORT_ASC, $lastName, SORT_ASC, $arr);
		
		return $arr;
	}
	
	public function getSupervisorName($supervisor_id) {
		
		if($supervisor_id <= 0){
			return;
		}
		
		$sql = SqlBuilder()->LoadSql("SELECT FirstName, LastName, Level FROM people WHERE PersonID = ? ")->BuildSql(array(DTYPE_INT, $supervisor_id));
		$result = DbConnManager::GetDb('mpower')->GetOne($sql);

		$levelSql = SqlBuilder()->LoadSql("SELECT COUNT(*) AS count FROM people WHERE Level = ? AND CompanyID = ? ")->BuildSql(array(DTYPE_INT, $result['Level']), array(DTYPE_INT, $_SESSION['company_id']));
		$levelResult = DbConnManager::GetDb('mpower')->GetOne($levelSql);
		
		return ($levelResult['count'] > 1) ? $result['FirstName'] . ' ' . $result['LastName'] : '';
	}
	/*
	public function getSupervisorColor($supervisor_id) {
		
		if($supervisor_id <= 0){
			return;
		}

		$sql = SqlBuilder()->LoadSql("SELECT Color FROM people WHERE PersonID = ? ")->BuildSql(array(DTYPE_INT, $supervisor_id));
		$result = DbConnManager::GetDb('mpower')->GetOne($sql);

		return $result['Color'];
	}
	*/
	
	
	public function getChildRecords($person_id) {
		
		$children = array();

		$sql = SqlBuilder()->LoadSql("SELECT PersonID FROM people WHERE SupervisorID = ? ")->BuildSql(array(DTYPE_INT, $person_id));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);

		foreach($result as $child) {
			$children[] = $child['PersonID'];
		}
		return $children;
	}
	
/*
 * Made obsolete with getAllCompanyUsers function
 * 
	private function getTopLevelUsers($company_id){
	
		$users = array();
	
		//$sql = SqlBuilder()->LoadSql("SELECT PersonID FROM people WHERE Level IN (SELECT MAX(Level) AS Level FROM people WHERE CompanyID = ?) AND CompanyID = ?")->BuildSql(array(DTYPE_INT, $company_id), array(DTYPE_INT, $company_id));
		$sql = SqlBuilder()->LoadSql("SELECT PersonID FROM people WHERE CompanyID = ? AND SupervisorID = -2 AND Deleted = 0")->BuildSql(array(DTYPE_INT, $company_id));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		
		foreach($result as $user) {
			$users[] = $user['PersonID'];
		}
		return $users;
	}
*/	
	
	private function getAllCompanyUsers($company_id){
		// Return a list of all active company users minus m-power admins
		// 06/26/2013 - Vern Gorman - Blue Star Technology
		
		$sql = SqlBuilder()->LoadSql("SELECT PersonID, FirstName, LastName, CompanyID, Level, SupervisorID, Deleted, MasterAssignee FROM people WHERE CompanyID = ? and deleted = 0 and SupervisorID != -3 ORDER BY Level DESC, SupervisorID DESC, FirstName, LastName")->BuildSql(array(DTYPE_INT, $company_id));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		
		return $result;
	}

	public function getAllSubordinates($person_id, &$sub_ordinates) {
		$person_records = $_SESSION['tree_obj']->GetRecord($person_id);
		$sub_ordinates[] = $person_records['PersonID'];
		
		if (isset($person_records['children']) && (count($person_records['children']) > 0)) {
			foreach ($person_records['children'] as $child_id) {
				$person = $_SESSION['tree_obj']->GetRecord($child_id);
				$sub_ordinates[] = $person['PersonID'];
				
			/*if($person['Level'] > 1) {
					$this->getAllSubordinates($person['PersonID'], $sub_ordinates);
				}*/
			}
		}
	}

	public function AssignPeople($record_id, $type) {
		
		$type = trim($type);
		$sub_ordinates = array();
		
		if(is_array($this->assigned_people_arr) && count($this->assigned_people_arr) > 0) {
			$new_assigned_people = $this->assigned_people_arr;
		}
		else {
			$new_assigned_people = array();
		}
		
		// NY 12-30-2008 : WTF - Call-time pass-by-reference has been deprecated!
		// $this->getAllSubordinates($_SESSION['login_id'], &$sub_ordinates);
		//$this->getAllSubordinates($_SESSION['login_id'], $sub_ordinates);
		$sub_ordinates = $this->children;

		$owner = '';
		$all_assigned_people = array();		
		
		$old_assigned_people = $this->GetAssignedPeople($record_id, $type, $owner);
		
		if(!is_array($old_assigned_people) || (count($old_assigned_people) == 0))	{
			$old_assigned_people = array();
		}
		
		$all_assigned_people = array_merge($old_assigned_people, $new_assigned_people);
		//print_r($all_assigned_people);
				
		/* Set the target array with the used id and its subordinates. */
		if(is_array($sub_ordinates)) {
			$target = $sub_ordinates;
		}
		else if(is_int($sub_ordinates))	{
			$target = array($sub_ordinates);
		}
		else {
			$target = array();
		}

		$table_name = ($type == 'contact') ? ('PeopleContact') : ('PeopleAccount');
		$ref_field = ($type == 'contact') ? ('ContactID') : ('AccountID');

		if(count($all_assigned_people) > 0)	{
			foreach($all_assigned_people as $each_people)	{
				/* If the people is assigned newly and it is one of the subordinates, then set assign_to field to one.
				If the people is assigned previously, which is not markes as unassigned 
				and doesn't belongs to any subordinates now, then also assign_to field will be set as 1.
				Else the assign to field field will be set as 0. */
				if(in_array($each_people, $new_assigned_people) 
					&& (in_array($each_people, $target) || in_array($each_people, $old_assigned_people)))	{
					$assign_to = 1;					
				}
				else {
					$assign_to = 0;
				}
				$sql_base = "IF EXISTS (SELECT * FROM $table_name WHERE $ref_field = ? AND PersonID = ?)
    			UPDATE $table_name
	      		SET AssignedTo = ?
	     			WHERE $ref_field = ? AND PersonID = ? 
	     			ELSE
	    			INSERT INTO $table_name
	        	($ref_field
	        	,PersonID
	        	,CreatedBy
	        	,AssignedTo)
	     			VALUES (?, ?, 0, 1)";
		    
		   		$build_arr = array();
		    	$build_arr[] = array(DTYPE_INT, $record_id);
				$build_arr[] = array(DTYPE_INT, $each_people);
				$build_arr[] = array(DTYPE_INT, $assign_to);
				$build_arr[] = array(DTYPE_INT, $record_id);
				$build_arr[] = array(DTYPE_INT, $each_people);
				$build_arr[] = array(DTYPE_INT, $record_id);
				$build_arr[] = array(DTYPE_INT, $each_people);
				

				$sql = SqlBuilder()->LoadSql($sql_base)->BuildSqlParam($build_arr);
				
				DbConnManager::GetDb('mpower')->Exec($sql);	

			}
			
		}
		if (isset($_SESSION['restricted_' . $type][$record_id])) {
			unset($_SESSION['restricted_' . $type][$record_id]);
		}
		return true;
	}

	function getAccountContact($contact_id) {
		$sql_base = "SELECT C.ContactID, (C.$_SESSION[contact_first_name] + ' ' + C.$_SESSION[contact_last_name]) AS Contact,
			A.$_SESSION[account_name] AS AccountName, C.PrivateContact
			FROM Contact C
			LEFT JOIN Account A on C.AccountID = A.AccountID
			WHERE C.CompanyID = ?	AND C.ContactID = ? ";
		
		$sql_base = SqlBuilder()->LoadSql($sql_base)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']), array(DTYPE_INT, $contact_id));
		
		$result = DbConnManager::GetDb('mpower')->Exec($sql_base);
		$contact = $result[0]['Contact'];
		
		if (trim($result[0]['AccountName']) != '') {
			$contact .= ' (' . $result[0]['AccountName'] . ')';
		}
		$ret_array['Contact'] = $contact;
		$ret_array['PrivateContact'] = $result[0]['PrivateContact'];
		
		return $ret_array;
	}

	function validateFormFields($data, $status) {
		$allFields = array();
		if (isset($data['rec_type']) && $data['rec_type'] == 'contact') {
			$con_sql = 'Select FieldName,LabelName,IsRequired,Parameters from ContactMap where CompanyID = ? AND Enable = 1';
			$con_sql = SqlBuilder()->LoadSql($con_sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']));
			$con_result = DbConnManager::GetDb('mpower')->Exec($con_sql);
			$allFields = $con_result;
		} else if (isset($data['rec_type']) && $data['rec_type'] == 'company') {
			$acc_sql = 'Select FieldName,LabelName,IsRequired,Parameters from AccountMap where CompanyID = ? AND Enable = 1';
			$acc_sql = SqlBuilder()->LoadSql($acc_sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']));
			$acc_result = DbConnManager::GetDb('mpower')->Exec($acc_sql);
			$allFields = $acc_result;
		}
		
		foreach ($allFields as $fields) {
			$fieldName = $fields['FieldName'];
			/*Additional field parameters are set with |(Pipe) as the delimiter in the following order:
			 Width,Height,Length,Format,Decimal,IsPastDate,IsMonthDay,HasYearRange,StartYear,EndYear.*/
			$param = $fields['Parameters'];
			if ($param != null) {
				$paramArr = explode("|", $param);
			}
			
			//Check Radio Buttons
			if (substr($fieldName, 0, 4) == 'Sele' && $fields['IsRequired'] == 1 && !isset($data[$fieldName])) $status->addError($fields['LabelName'] . ' is a mandatory field.', $fields['FieldName']);
			//Check all other fields.
			if (isset($data[$fieldName]) && trim($data[$fieldName]) == '') {
				
				if ($fields['IsRequired'] == 1) $status->addError($fields['LabelName'] . ' is a mandatory field.', $fields['FieldName']);
			} else {
				if (substr($fieldName, 0, 4) == 'Numb') {
					if ($paramArr[4] != '' && $paramArr[4] == 1) {
						$arr = explode('.', $data[$fieldName]);
						if (count($arr) > 1) //it has a decimal .
{
							if (strlen($arr[1]) > $paramArr[4]) $status->addError('Sorry you cannot have more than ' . $paramArr[4] . ' values after decimal.', $fields['FieldName']);
						}
					}
				} else if (substr($fieldName, 0, 4) == 'Date') {
					if ($paramArr[5] != '' && $paramArr[5] == 1) {
						$today = date("m/d/Y");
						$util = new Util();
						$dateDif = $util->dateDiff("/", $data[$fieldName], $today);
						//print_r ($dateDif);exit;
						if ($dateDif >= 0) $status->addError('Sorry the date should be a previous date than' . $today . '.', $fields['FieldName']);
					} else if ($paramArr[7] != '' && $paramArr[7] == 1) {
						$startYear = $paramArr[8];
						$endYear = $paramArr[9];
						if ($startYear == 'CurrentYear') $startYear = date("Y");
						if ($endYear == 'CurrentYear') $endYear = date("Y");
						
						//strtotime($data[$fieldName]);
						if (!(date("Y", strtotime($data[$fieldName])) >= $startYear) || !(date("Y", strtotime($data[$fieldName])) <= $endYear)) {
							$status->addError('Sorry the date should be between ' . $startYear . ' and ' . $endYear, $fields['FieldName']);
						}
					} else if ($paramArr[6] != '' && $paramArr[6] == 1) {
						$fieldNo = substr($fieldName, 4);
						//print_r($fieldNo);exit;
						$monthFieldName = 'dateMonthField' . $fieldNo;
						$dayFieldName = 'dateDayField' . $fieldNo;
						if (isset($data[$monthFieldName]) && isset($data[$dayFieldName]) && strlen($data[$monthFieldName]) != 0 && strlen($data[$dayFieldName]) != 0) {
							$calDate = $data[$monthFieldName] . '/' . $data[$dayFieldName] . '/' . date("Y");
							
							$_POST[$fieldName] = $calDate;
						
						} else {
							$_POST[$fieldName] = '';
						}
					}
				}
			}
		}
		return $status;
	}

	public function checkRestriction($rec_id, $rec_type) {
		if (isset($_SESSION['restricted_' . $rec_type][$rec_id])) {
			$this->is_assigned_to_cur_user = $_SESSION['restricted_' . $rec_type][$rec_id];
			//var_dump($this->is_assigned_to_cur_user);
		} else {
			$obj_restriction = new RestrictCompanyContact();
			$this->is_assigned_to_cur_user = $obj_restriction->checkRecordRestriction($_SESSION['company_obj']['LimitAccessToUnassignedUsers'], $rec_id, $rec_type);
		}
		return $this->is_assigned_to_cur_user;
	}
}
