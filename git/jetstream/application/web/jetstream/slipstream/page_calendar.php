<?php
require_once BASE_PATH . '/slipstream/class.JetstreamPage.php';
require_once BASE_PATH . '/slipstream/class.ModuleCalendar.php';
require_once BASE_PATH . '/slipstream/class.ModuleMiniCalendar.php';
require_once BASE_PATH . '/slipstream/class.ModuleUpcomingEvents.php';

$page = new JetstreamPage();

// Use a generic module because the dashboard search is such a simple page

$cal = new ModuleCalendar();

if (!empty($_GET['action']) && !empty($_GET['day'])) {
	setcookie('Cal_Type', $_GET['action']);
	setcookie('Cal_Day', $_GET['day']);
	
	$cal->Init($_GET['action'], $_GET['day']);
}

$page->AddModule($cal);

if (!in_array($_SESSION['USER']['USERID'], array(8717, 8718, 8809, 8872, 8986))) {
	$page->AddModule(new ModuleMiniCalendar());
	$page->AddModule(new ModuleUpcomingEvents());
	
	$tasks = new ModuleUpcomingEvents('today_tasks');
	$tasks->type = 'tasks';
	$page->AddModule($tasks);
}

$page->Render();
