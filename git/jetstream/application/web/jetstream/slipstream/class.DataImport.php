<?php
require_once BASE_PATH . '/libs/DataSource.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.DataType.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/importconstants.php';
require_once BASE_PATH . '/include/class.Csv.php';
require_once BASE_PATH . '/include/class.DashboardSearchCreatorNew.php';
require_once BASE_PATH . '/slipstream/class.OptionUtil.php';
require_once BASE_PATH . '/include/class.DebugUtil.php';
require_once BASE_PATH . '/include/class.BooleanUtil.php';
require_once BASE_PATH . '/include/class.RecordUtil.php';
require_once BASE_PATH . '/include/class.AssignmentUtil.php';

class DataImport {

	private $errors = array();
	private $uniqueFileName;
	private $companyId;
	
	private $accountMapFields = array();
	private $contactMapFields = array();
	
	public $associated;
	public $requiredFields;
	public $selectedValues;
	public $existingCompanies;
	
	public $options = array();


	public function __construct()
	{
		$this->companyId = $_SESSION['company_id'];
		
		$this->setMapFields();
		$this->setOptions();
		
		$this->upload();
	}

	private function setUniqueFilename()
	{
		# unique file name to save upload as
		$unique = microtime(false);
		$unique = str_replace(" ", "", $unique);
		$unique = str_replace(".", "", $unique);

		$this->uniqueFileName = $unique;
	}

	public function getUniqueFilename()
	{
		return $this->uniqueFileName;
	}

	private function setErrors($errors)
	{
		$this->errors = $errors;
	}

	public function getErrors()
	{
		return $this->errors;
	}
	
	private function setOptions()
	{
		$this->options = $this->buildOptions();
	}

	/**
	 * Upload csv data, rename file and move to filestore
	 */
	public function upload()
	{
		$this->setUniqueFilename();

		# path to save file as
		$dest = FILE_UPLOAD_DIR . '/' . $this->getUniqueFilename();

		# validate and upload file
		$allowed = array("text/csv",
				"text/comma-separated-values", 
				"application/csv", 
				"application/excel", 
				"application/vnd.ms-excel", 
				"application/vnd.msexcel", 
				"text/anytext",
				"application/octet-stream"
		);
		if(empty($_FILES['csv']['type'])){
			$errors[] = 'Please select a file to upload by clicking "Browse"';
		}
		else if (!in_array($_FILES['csv']['type'], $allowed)){
			$errors[] = 'File type not allowed';
		}
		else {
			if(!move_uploaded_file($_FILES['csv']['tmp_name'], $dest)){
				array_push($errors, 'Error moving file');
			}
		}
		
		/*
		 * Check for anything over the maximum supported col/row values
		 */
		$csv = new Csv($dest);
		
		if($csv->countHeaders() >= MAX_COLS){
			$errors[] = "A maximum column count of " . MAX_COLS . " is supported at this time. Please split your file or contact ASA for assistance.";
		}
		if($csv->countRows() >= MAX_ROWS){
			$errors[] = "A maximum row count of " . MAX_ROWS . " is supported at this time. Please split your file or contact ASA for assistance.";
		}
		
		$this->setErrors($errors);
	}

	public function buildOptions()
	{	
		/*
		 * Contact Options
		 */
		$contactMapQuery = "SELECT LabelName, FieldName, ContactSectionName FROM ContactMap, Section WHERE ContactMap.SectionID = Section.SectionID AND ContactMap.CompanyID = " . $this->companyId;
		$contactMapResults = DbConnManager::GetDb('mpower')->Exec($contactMapQuery);

		$options = array();

		foreach($contactMapResults as $result){
			$options[TABLE_CONTACT . ':' . $result['FieldName'] . ':' . $result['LabelName']] = TYPE_CONTACT . ' (' . $result['ContactSectionName'] . ') - ' . $result['LabelName'];
		}

		/*
		 * Company Options
		 */
		$accountMapQuery = "SELECT LabelName, FieldName, AccountSectionName FROM AccountMap, Section WHERE AccountMap.SectionID = Section.SectionID AND AccountMap.CompanyID = " . $this->companyId;
		$accountMapResults = DbConnManager::GetDb('mpower')->Exec($accountMapQuery);

		foreach($accountMapResults as $result){
			$options[TABLE_COMPANY . ':'. $result['FieldName'] . ':' . $result['LabelName']] = TYPE_COMPANY . ' (' . $result['AccountSectionName'] . ') - ' . $result['LabelName'];
		}
		
		/*
		 * Assignment Options
		 */
		$options[TABLE_ASSIGN_COMPANY . ':PersonID:Name'] = TYPE_ASSIGN . ' - Assign To';
		
		/*
		 * Static options always appear at top of list
		 */
		$staticOptions[''] = 'Select Field';
		$staticOptions['0'] = 'DO NOT MAP';

		$sortedOptions = $staticOptions;
		asort($options);
		
		foreach($options as $k => $v){
			$sortedOptions[$k] = $v;
		}
		
		$this->options = $sortedOptions;
		
		return $sortedOptions;
	}
	
	public function checkForErrors(Csv $dataSource, $dataImportUtilArray)
	{
		/*
		 * Reset errors
		 */
		$this->setErrors( array() );
	
		/*
		 * Check for required Company/Account fields
		 */
		$requiredCompanyFields = $this->getRequiredFields(TYPE_COMPANY);
		$companyTable = $this->getTable(TYPE_COMPANY);
		$found = false;
		
		foreach($dataImportUtilArray as $dataImportUtil){
			if($dataImportUtil->getTable() == $companyTable['name']){
				if(in_array($dataImportUtil->getDatabaseField(), $requiredCompanyFields)){
					$found = true;
				}
			}
		}
		if(!$found){
			//$this->errors[] = 'Required "Company" columns not found.<br>';
			//return false;
		}

		/*
		 * Check for required Contact fields
		 */
		$requiredContactFields = $this->getRequiredFields(TYPE_CONTACT);
		$contactTable = $this->getTable(TYPE_CONTACT);
		$found = false;
		
		foreach($dataImportUtilArray as $dataImportUtil){
			if($dataImportUtil->getTable() ==  $contactTable['name']){
				if(in_array($dataImportUtil->getDatabaseField(), $requiredContactFields)){
					$found = true;
				}
			}
		}
		if(!$found){
			$this->errors[] = 'Required "Contact" columns not found.<br>';
			return false;
		}
		
		/*
		 * Check for duplicate selections
		 */
		$iterator = 0;
		$selections = array();
		foreach($dataImportUtilArray as $dataImportUtil){
			$selections[$dataImportUtil->getTable() . ':' . $dataImportUtil->getDatabaseField()]= $iterator;
			$iterator++;
		}
		$newSelections = array_flip($selections);
		
		if(count($dataImportUtilArray) > count($newSelections)){
			$this->errors[] = 'Duplicate selections found. All selections must be unique<br>';
			return false;
		}
		
		/*
		 * Check datatypes
		 */
		
		$rowCount = $dataSource->countRows();
		$headerCount = $dataSource->countHeaders();
		
		for($i=0;$i<$headerCount;$i++){
			
			$colType = '';
			$colData = $dataSource->getColumn($dataImportUtilArray[$i]->getColumnName());
			
			$field = $dataImportUtilArray[$i]->getDatabaseField();
			$table = $dataImportUtilArray[$i]->getTable();
			$column = $dataImportUtilArray[$i]->getColumnName();
			
			if( stristr($field, "select") ){
				$colType = 'select';
				$optionUtil = new OptionUtil($table, $field);
			}
			else if( stristr($field, "bool") ){
				$colType = 'bool';
			}
			else if( stristr($field, "number") ){
				$colType = 'number';
			}
			
			//echo 'colName : '  . $column . ' field : ' . $field . ' colType : ' . $colType . '<br>';
			
			switch($colType)
			{
				case 'select' :
					
					$line = 1;
					foreach($colData as $value){
						$line++;
						if($value != ''){
							if(!$optionUtil->convert($value)){
								$this->errors[] = 'Option not found for column "' . $column . '" on line ' . $line . '<br>';
								return false;
							}
						}
					}
					break;
				
				case 'bool' :

					$line = 1;
					foreach($colData as $value){
						$line++;
						if($value != ''){
							if(!BooleanUtil::isValid($value)){
								$this->errors[] = 'Invalid yes/no field in column "' . $column . '" on line ' . $line . '<br>';
								return false;
							}
						}
					}
					break;
					
				case 'number' :
					
					$line = 1;
					foreach($colData as $value){
						//strip commas
						$value = str_replace(',', '', $value);
						
						$line ++;
						
						if($value != ''){
							if(!is_numeric($value)){
								$this->errors[] = 'Invalid number field in column "' . $column . '" on line ' . $line . '<br>';
								return false;
							}
						}
					}
					break;
			}
		}
		return true;
	}
	
	public function populate($contactIds)
	{
		$this->setAssociatedFields();

		$accountFields = array_keys($this->associated);
		$contactFields = array_values($this->associated);
		
		$accountStr = implode(',', $accountFields);
		$contactStr = implode(' = ?, ', $contactFields);
		
		$contactStr .= ' = ? ';
		
		
		$dataType = new DataType('Contact');
		
		foreach($contactIds as $contactId){
			
			
			if(strlen($contactId) > 0){
				
				$contactSql = 'SELECT AccountID, ContactID FROM Contact WHERE ContactID = ?';
				
				$contactParams = array(DTYPE_INT, (Integer)$contactId);
				$contactSql = SqlBuilder()->LoadSql($contactSql)->BuildSql($contactParams);
				$contactResult = DbConnManager::GetDb('mpower')->GetOne($contactSql);
			
				if(is_numeric($contactResult['AccountID'])){
					$accountSql = 'SELECT ';
					$accountSql .= $accountStr;
					$accountSql .= ' FROM Account WHERE AccountID = ?';
			
					$accountParams = array(DTYPE_INT, $contactResult['AccountID']);
					$accountSql = SqlBuilder()->LoadSql($accountSql)->BuildSql($accountParams);
					$accountResults = DbConnManager::GetDb('mpower')->Exec($accountSql);
	
					$updateParams = array();
					
					foreach($contactFields as $contactField){
						$updateParams[] = array($dataType->getDataType($contactField), $accountResults[0][array_search($contactField, $this->associated)]);
					}
					$updateParams[] = array(DTYPE_INT, $contactId);
					
					$updateSql = 'UPDATE Contact SET ';
					$updateSql .= $contactStr;
					$updateSql .= ' WHERE ContactID = ?';
				
					$updateSql = SqlBuilder()->LoadSql($updateSql)->BuildSqlParam($updateParams);
					
					DbConnManager::GetDb('mpower')->Exec($updateSql);
				}
				
			}
		}
	}
	
	public function setSelectedValues()
	{
		$this->selectedValues = $_SESSION['raw_options'];
		
	}
	
	public function getSelectedValue($str){
		return $this->selectedValues[$str];
	}
	
	public function loadFailedAssignmentFile()
	{
		$file = '0620783001304028060.assign_company.new';
		$dataSourceAssignCompany = new Csv(FILE_UPLOAD_DIR . '/' . $file);
		$this->buildInserts(TYPE_ASSIGN_COMPANY, $dataSourceAssignCompany, null, false);
	}
	
	public function setAssociatedFields()
	{
		$this->associated = array();
		
		$sql = '
			SELECT
				ContactMap.FieldName AS ContactFieldName,
				AccountMap.FieldName AS AccountFieldName
			FROM
				ContactMap,
				AccountMap
			WHERE
				ContactMap.AccountMapID = AccountMap.AccountMapID AND AccountMap.CompanyID = ?';

		$params = array(DTYPE_INT, $this->companyId);
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql($params);
			
		$results = DbConnManager::GetDb('mpower')->Exec($sql);
		
		foreach($results as $result){
			$this->associated[$result['AccountFieldName']] = $result['ContactFieldName'];
		}
	}
	
	public function isNewCompany($type)
	{
		$table = $this->getTable($type);
		$sql = "SELECT COUNT(*) AS count FROM " . $table['name'] . " WHERE CompanyID = ?";
		$param = array(DTYPE_INT, $this->companyId);
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql($param);
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		
		$count = $result[0]['count'];
		return $count < 10 ? true : false;
	}
	
	public function checkForExistingRecords($type, $dataSource)
	{
		$found = array();

		/*
		 * Skip exist check for new companies on first import
		 */
		if($this->isNewCompany($type)){
			return $found;
		}
	
		$checkColumns = array();
		$checkFields = $this->getCheckFields($type);

		$table = $this->getTable($type);
		$returnFields = $this->getReturnFields($type);
		
		$select = 'SELECT ' . $table['key'] . ', ';
		if($type == TYPE_CONTACT){
			$select .= 'AccountID, ';	
		}
		foreach($returnFields as $returnField){
			$select .= $returnField . ', ';
		}
		$select = substr($select, 0, -2);
		$from = ' FROM ' . $table['name'];

		$headers = $dataSource->getHeaders();

		foreach($headers as $header) {
			if(in_array($header, $checkFields)){
				$checkColumns[] = $header;
			}
		}

		$fields = array();
		foreach($checkColumns as $col){
				
			$fields[$col] = $dataSource->getColumn($col);
				
			foreach($fields[$col] as $field){
				$where[$col][] = $col . ' = ?';
				$values[$col][] = $field;
			}
		}

		$queries = $this->buildCheckQueries($type, $dataSource, $where, $checkColumns);

		/*
		 echo '<pre>';
		 print_r($queries);
		 echo '</pre>';

		 echo '<pre>';
		 print_r($values);
		 echo '</pre>';
		*/
		
		if($type == TYPE_CONTACT){
			$accountIds = $dataSource->getColumn('AccountID');
		}
	
		$rowCount = $dataSource->countRows();
		for($i=0;$i<$rowCount;$i++){

			/*
			 * Skip exist check for newly imported account records
			 */
			if($type == TYPE_CONTACT){
				if(!in_array($accountIds[$i], $this->existingCompanies)){
					continue;
				}
			}
			if(!$this->isRowEmpty($type, $dataSource, $i)){
					
				$where = ' WHERE ' . $queries[$i];
	
				$params = array();
				foreach($values as $k => $v){
					$params[] = array(DTYPE_STRING, $v[$i]);
				}
				$params[] = array(DTYPE_INT, $this->companyId);
	
				$sql = SqlBuilder()->LoadSql($select . $from . $where . ' AND CompanyID = ?')->BuildSqlParam($params);
					
				//echo $sql . '<br>';
				$results = DbConnManager::GetDb('mpower')->Exec($sql);
				
				if(count($results) > 0){
	
					$output = array();
					$output['idx'] = $i;
	
					$highest = 0;
	
					foreach($results as $result){
							
						$result['matchCount'] = 0;
						$matched = array();
						
						foreach($checkFields as $checkField){
							
							$matched[] = $checkField;
							
							if($result[$checkField] == $values[$checkField][$i]){
								//$matched[] = $checkField;
								$result['matchCount']++;
							}
						}
							
						if($result['matchCount'] > $highest){
							$highest = $result['matchCount'];
							$matches = $matched;
														
							$best = $result;
						}
					}
						
					$output['key'] = $best[$table['key']];
						
					foreach($returnFields as $returnField){
						$output[] = $best[$returnField];
					}
					
					$output['matches'] = $this->buildMatchString($type, $matches);
					if($type == TYPE_CONTACT){
						$output['CompanyName'] = $this->getAccountName($result['AccountID']);
					}
					
					$found[] = $output;
	
				}
			}
		}
		return $found;

	}
	
	private function getAccountName($id)
	{
		$params[] = array(DTYPE_INT, $id);
		$sql = SqlBuilder()->LoadSql('SELECT Text01 CompanyName FROM Account WHERE AccountID = ?')->BuildSqlParam($params);					
		$results = DbConnManager::GetDb('mpower')->GetOne($sql);
		
		return $results['CompanyName'];
	}
	
	private function buildMatchString($type, $matches)
	{

		if($type == TYPE_CONTACT){
			array_shift($matches);
		}
		
		$count = count($matches);
		$mapFields = $this->getMapFields($type);
				
		foreach($matches as $match){
			$labels[] = $mapFields[$match];
		}
		
		return implode(' and ', $labels);
	}
	
	public function getUniqueValues($records)
	{	
		$unique = array();
		
		foreach($records as $record){
			if(!$this->keyExists($record['key'], $unique)){
				$unique[] = $record;
			}
		}
		return $unique;
	}
	
	private function keyExists($key, $arr)
	{
		foreach($arr as $a){
			if(in_array($key, $a)){
				return true;
			}
		}
		return false;
	}

	public function buildAssignments($dataSource)
	{
		$assignmentUtil = new AssignmentUtil();
		$userID = $_SESSION['USER']['USERID'];
		
		$ids = array();
		$by = array();
		$assignedTo = array();
		
		$rows = $dataSource->getColumn('PersonID');

		foreach($rows as $name){
			$id = $assignmentUtil->convert($name);
			$by = ($id == $userID) ? 1 : 0;
			
			$createdBy[] = $by;
			$ids[] = $id;
			$assignedTo[] = 1;
		}
		$dataSource->fillColumn('PersonID', $ids);
		$dataSource->appendColumn('CreatedBy', $createdBy);
		$dataSource->appendColumn('AssignedTo', $assignedTo);
		
		return $dataSource;
		
	}
	
	public function buildCsv($type, $filename, $dataImportUtilArray)
	{
		$csv = new Csv(FILE_UPLOAD_DIR . '/' . $filename);
		$table = $this->getTable($type);
		$csv->symmetrize();
		
		$headers = $csv->getHeaders();
		$headerCount = $csv->countHeaders();

		for($i=0;$i<$headerCount;$i++){
			
			if($dataImportUtilArray[$i]->getTable() == $table['name']){
				/*
				 * Setup new column headers. Map field names are used
				 */
				$newHeaders[] = $dataImportUtilArray[$i]->getDatabaseField();
				
				/*
				 * Convert select field values to the correct option values stored in the database
				 */
				if(stristr($dataImportUtilArray[$i]->getDatabaseField(), "select")){
						
					$converted = array();
						
					$values = $csv->getColumn($headers[$i]);
					$optionUtil = new OptionUtil($dataImportUtilArray[$i]->getTable(), $dataImportUtilArray[$i]->getDatabaseField());
						
					foreach($values as $value){
						$converted[] = $optionUtil->convert($value);
					}
					$csv->fillColumn($headers[$i], $converted);
				}
				/*
				 * Convert yes/no to boolean for storage
				 */
				else if(stristr($dataImportUtilArray[$i]->getDatabaseField(), "bool")){
						
					$converted = array();
						
					$values = $csv->getColumn($headers[$i]);
						
					foreach($values as $value){
						$converted[] = BooleanUtil::convert($value);
					}
					$csv->fillColumn($headers[$i], $converted);
				}
				else if(stristr($dataImportUtilArray[$i]->getDatabaseField(), "number")){
						
					$converted = array();
						
					$values = $csv->getColumn($headers[$i]);
						
					foreach($values as $value){
						if($value == ''){
							$converted[] = 0;
						}
						else {
							$converted[] = str_replace(',', '', $value);
						}
					}
					$csv->fillColumn($headers[$i], $converted);
				}
			}
			else {
				$csv->removeColumn($headers[$i]);
			}
		}

		$csv->setNewHeaders($newHeaders);

		$filename .= '.' . strtolower($type);
		$newCsv = $csv->saveAs(FILE_UPLOAD_DIR . '/' . $filename);

		return $newCsv;
	}

	/**
	 * Determine if Csv contains columns for the given type
	 * @param String $type
	 * @param Csv $csv
	 * @param DataImportUtil $dataImportUtilArray
	 */
	public function hasColumns($type, $csv, $dataImportUtilArray)
	{
		$table = $this->getTable($type);
		$headerCount = $csv->countHeaders();
		for($i=0;$i<$headerCount;$i++){
			if($dataImportUtilArray[$i]->getTable() == $table['name']){
				return true;
			}
		}
		return false;
	}

	private function isRowEmpty($type, $dataSource, $index)
	{
		$reqFields = $this->getRequiredFields($type);
		$headers = $dataSource->getHeaders();
		
		for($i=0;$i<$dataSource->countHeaders();$i++){
			if(in_array($headers[$i], $reqFields)){
				$cell = $dataSource->getCell($index, $i);
				$value = trim($cell);
				
				if(!empty($value)){
					return false;
				}
			}
		}
		return true;
	}

	public function buildInserts($type, $dataSource, $existing, $includeCompanyID=true)
	{
		/*
		 * Build the initial sql insert statement
		 */
		$table = $this->getTable($type);

		$insert = 'INSERT INTO ' . $table['name'] . ' (';

		$cols = $dataSource->getHeaders();
		
		if($includeCompanyID){
			$cols[] = 'CompanyID';
		}
		
		$params = '';
		for($i=0;$i<$dataSource->countHeaders();$i++){
			$params .= '?,';
		}

		$stmt = $insert . implode(',', $cols) . ') VALUES (';
		$stmt .= $params;
		if($includeCompanyID){
			$stmt .= '?';	
		}
		else {
			$stmt = substr($stmt,0,-1);
		}
		$stmt .= ')';

		$dataType = new DataType($table['name']);

		$headers = $dataSource->getHeaders();
		$reqFields = $this->getRequiredFields($type);

		$check = array();
		$insertedRecords = array();

		//echo 'count: ' . $dataSource->countRows();
		$rowCount = $dataSource->countRows();
		
		for($i=0;$i<$rowCount;$i++){			
			/*
			 * Determine if values have already been inserted
			 */
			for($j=0;$j<$dataSource->countHeaders();$j++){
				if(in_array($headers[$j], $reqFields)){
					$check[$headers[$j]] = $dataSource->getCell($i,$j);
				}
			}
			$recordUtil = new RecordUtil();
			$inserted = $recordUtil->isInserted($check, $insertedRecords);
			/*
			echo '<pre>';
			echo '**********************START***************************<br>';
			echo 'i:' . $i . '<br>';
			*/
			/*
			 * If values have already been inserted during the import,
			 * obtain the key value for this row
			 */
			if($this->isRowEmpty($type, $dataSource, $i)){
				$ids[$i] = null;
				//echo 'empty<br>';
			}
			else if($inserted){
				$ids[$i] = $recordUtil->getKey();
				//echo 'inserted<br>';
			}
			else if(in_array($i, array_keys($existing))){
				$ids[$i] = $existing[$i];
				//echo 'existing<br>';
			}
			else {
				//echo 'new<br>';
				
				/*
				 * Create sql statement and insert this row.
				 * Return the inserted id
				 */
				$params = array();

				for($j=0;$j<$dataSource->countHeaders();$j++){
					
					$value = $dataSource->getCell($i,$j);
					
					/*
					 * Allow contacts without an association to an account.
					 * empty strings must be converted to null to keep the
					 * SqlBuilder happy
					 */
					if($cols[$j] == 'AccountID' && $value == ''){
						$value = null;
					}

					$params[] = array($dataType->getDataType($cols[$j]), $value);
				}
				
				if($includeCompanyID){
					$params[] = array(DTYPE_INT, $this->companyId);
				}
				
				
				$sql = SqlBuilder()->LoadSql($stmt)->BuildSqlParam($params);

				//echo '<br> ' . $sql . '<br>';
				
				$ids[$i] = DbConnManager::GetDb('mpower')->DoInsert($sql);
				
				//echo 'id: ' . $ids[$i] . '<br>';
				/*
				 * Add each successfully inserted record to an array
				 * to prevent duplicate records
				 *
				 * Each subsequent iteration will check to see if
				 * the same values have already been inserted during
				 * the import
				 */
				$insertedRecord = new RecordUtil($ids[$i], $check);
				$insertedRecords[] = $insertedRecord;
				
				if($includeCompanyID){
					/*
					* Add DashboardSearch records used for search
					* functionality
					* TODO (needs to be encapsulated better)
					*/
					$dashboardSearch = new DashboardSearchCreator();
					$dashboardSearch->company_id = $this->companyId;
					$dashboardSearch->record_id = $ids[$i];
	
					if($type == TYPE_CONTACT) {
						$dashboardSearch->StoreContactRecords();
					}
					else if($type == TYPE_COMPANY) {
						$dashboardSearch->StoreAccountRecords();
					}
				}
				
				
				
			}
			//echo '***********************END****************************<br>';
			//echo '</pre>';
			
		}
		return $ids;
	}

	private function buildCheckQueries($type, $dataSource, $where, $checkColumns)
	{
		$rowCount = $dataSource->countRows();

		for($i=0;$i<$rowCount;$i++){
				
			$query = '(';
				
			if(in_array('AccountID', $checkColumns)){
				//$query .= '(';
			}
			for($j=0;$j<count($checkColumns);$j++){

				$query .= $where[$checkColumns[$j]][$i];

				if($j<count($checkColumns)-1){
					//if(strstr($checkColumns[$j+1], "ID")){
					//	$query .= ') AND ';
					//}
					//else {
					//	$query .= ' OR ';
					//}
					$query .= ' AND ';
				}
			}
				
			$query .= ')';
				
			$queries[] = $query;
		}
		return $queries;
	}

	public function getTable($type)
	{
		$table = array();

		switch($type)
		{
			case TYPE_COMPANY :

				$table['name'] = TABLE_COMPANY;
				$table['key'] = KEY_COMPANY;

				break;
					
			case TYPE_CONTACT :

				$table['name'] = TABLE_CONTACT;
				$table['key'] = KEY_CONTACT;

				break;

			case TYPE_ASSIGN_COMPANY :
					
				$table['name'] = TABLE_ASSIGN_COMPANY;
				$table['key'] = TABLE_ASSIGN_COMPANY_KEY;

				break;

			case TYPE_ASSIGN_CONTACT :

				$table['name'] = TABLE_ASSIGN_CONTACT;
				$table['key'] = TABLE_ASSIGN_CONTACT_KEY;

				break;
		}
		
		return $table;
	}

	private function getReturnFields($type)
	{
		$fields = array();

		if($type == 'Company'){
			$fields['Name'] = "Text01";
			$fields['Address'] = "Text02";
		}
		if($type == 'Contact'){
			$fields['First Name'] = "Text01";
			$fields['Last Name'] = "Text02";
		}
		return $fields;
	}

	/**
	 * Get the fields to use for existing check by type
	 * @param String $type
	 */
	private function getCheckFields($type)
	{
		$fields = array();

		if(($type == TYPE_ASSIGN_COMPANY) || ($type == TYPE_ASSIGN_CONTACT)){
			$fields[] = "AccountID";
			$fields[] = "PersonID";
			$fields[] = "CreatedBy";
			$fields[] = "AssignedTo";

		}
		else {
			
			if($type == TYPE_COMPANY){
				
				$mapQuery = "SELECT FieldName FROM AccountMap WHERE CompanyID = " . $this->companyId . " AND (IsCompanyName = 1 OR IsZipCode = 1)";
				$mapResults = DbConnManager::GetDb('mpower')->Exec($mapQuery);
				
			}
			else if($type == TYPE_CONTACT){
				
				$fields[] = "AccountID";
				
				$mapQuery = "SELECT FieldName FROM ContactMap WHERE CompanyID = " . $this->companyId . " AND (IsFirstName = 1 OR IsLastName = 1)";
				$mapResults = DbConnManager::GetDb('mpower')->Exec($mapQuery);
				
			}
			
			foreach($mapResults as $mapResult){
				$fields[] = $mapResult['FieldName'];
			}
			
		}
		return $fields;
	}

	private function getRequiredFields($type)
	{
		$fields = array();

		if($type == TYPE_COMPANY){
			$fields[] = "Text01";
		}
		else if($type == TYPE_CONTACT){
			$fields[] = "Text01";
			$fields[] = "Text02";
		}
		else if($type == TYPE_ASSIGN_COMPANY){
			$fields[] = "AccountID";
			$fields[] = "PersonID";
		}
		else if($type == TYPE_ASSIGN_CONTACT){
			$fields[] = "ContactID";
		}
		return $fields;
	}
	
	private function setMapFields()
	{
		$accountMapQuery = "SELECT LabelName, FieldName FROM AccountMap WHERE CompanyID = " . $this->companyId;
		$accountMapResults = DbConnManager::GetDb('mpower')->Exec($accountMapQuery);
		
		foreach($accountMapResults as $accountMapResult){
			$this->accountMapFields[$accountMapResult['FieldName']] = $accountMapResult['LabelName'];
		}
		
		$contactMapQuery = "SELECT LabelName, FieldName FROM ContactMap WHERE CompanyID = " . $this->companyId;
		$contactMapResults = DbConnManager::GetDb('mpower')->Exec($contactMapQuery);
		
		foreach($contactMapResults as $contactMapResult){
			$this->contactMapFields[$contactMapResult['FieldName']] = $contactMapResult['LabelName'];
		}
	}
	
	private function getMapFields($type)
	{
		switch($type)
		{
			case TYPE_COMPANY :
				return $this->accountMapFields;
				break;
			
			case TYPE_CONTACT :
				return $this->contactMapFields;
				break;
		}
	}

	/**
	 * Existing values come through POST as id:index
	 * Parse and return an array
	 * @param array $existing
	 */
	public function parseExisting($all, $unique)
	{	
		$existing = array();		   
		foreach($all as $a){
			$tmp = split(':', $a);
			$existing[$tmp[0]] = $tmp[1];
		}

		$selected = array();
		foreach($unique as $ex){
			$tmp = split(':', $ex);
			if($tmp[1] != 0){
				$selected[$tmp[1]] = $tmp[0];
			}
		}
		
		$values = array();
		
		$arr = array();
		$arr = array_flip($selected);
		
		foreach($arr as $a){
			foreach($existing as $k => $v){
				if($a == $v){
					$values[$k] = $v;
				}
			}
		}
		return $values;
	}
}
