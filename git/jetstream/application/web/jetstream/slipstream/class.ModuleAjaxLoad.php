<?php
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.Event.php';

class ModuleAjaxLoad extends JetstreamModule
{
	protected $module;
	protected $params = array();
	public $template_files = array('shell_ajaxload.tpl');

	public function __get($name) {
		switch ($name) {
			case 'sidebar':
				return $this->module->sidebar;
			case 'css_files':
				return $this->module->css_files;
			case 'print_css_files':
				return $this->module->print_css_files;
			case 'javascript_files':
				return $this->module->javascript_files;
			default:
				if (isset($this->name)) return $this->name;
				return;
		}
	}
	
	public function Init() {
		$init = $this->module->InitAjax();
		if (is_array($init)) {
			$this->params += $init;
		}
		$this->sidebar = $this->module->sidebar;
		$this->params += $_GET;
		$this->AddParam('module', get_class($this->module));
	}

	public function SetModule(JetstreamModule $module) {
		$this->module = $module;
	}
	
	public function AddParam($param, $value) {
		$this->params[$param] = $value;
	}

	public function GetParams() {
		return json_encode($this->params);
	}

	public function GetId() {
		if (!isset($this->id)) {
			$this->id = uniqid('id');
		}
		return ($this->id);
	}
}



?>