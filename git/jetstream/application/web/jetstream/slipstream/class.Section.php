<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once 'class.Status.php';

class Section extends MpRecord
{

	public function __toString()
	{
		return 'Section';
	}
	public function Initialize()
	{
		$this->db_table = 'Section';
		$this->recordset->GetFieldDef('SectionID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('CompanyID')->required = TRUE;
		parent::Initialize();
	}

	function saveSection($companyId,$accountSectionName,$contactSectionName) {
		$status = new Status();
		$sql = "SELECT * FROM Section WHERE SectionID = -1";
		$rs = DbConnManager::GetDb('mpower')->Execute($sql);
		$this->SetDatabase(DbConnManager::GetDb('mpower'));
		$this->recordset = $rs;
		$this->Initialize();
		$this->CompanyID = $companyId;
		$this->AccountSectionName = $accountSectionName;
		$this->ContactSectionName = $contactSectionName;
		$this->Save();
		return $status;
	}

	function getAccountSectionByCompanyId($companyId){
		$sql = "select SectionID,AccountSectionName from Section where CompanyID = ? and AccountSectionName is not NULL";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId));
		$sections = DbConnManager::GetDb('mpower')->Exec($sql);
		return $sections;
	}

	function getContactSectionByCompanyId($companyId){
		$sql = "select SectionID,ContactSectionName from Section where CompanyID = ? and ContactSectionName is not NULL";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId));
		$sections = DbConnManager::GetDb('mpower')->Exec($sql);
		return $sections;
	}
	function updateAccountSectionById($accountSectionName,$sectionId){

		$status=new Status();
		$sql = "SELECT * FROM Section WHERE SectionID = ?";
		$sectionSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $sectionId));
		$result = DbConnManager::GetDb('mpower')->Execute($sectionSql,'Section');
		if (count($result) == 1) {
			$objSection = $result[0];
			$objSection->Initialize();
			$objSection->AccountSectionName = $accountSectionName;
			$result->Save();
		}
		return $status;

	}

	function updateContactSectionById($contactSectionName,$sectionId){

		$status=new Status();
		$sql = "SELECT * FROM Section WHERE SectionID = ?";
		$sectionSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $sectionId));
		$result = DbConnManager::GetDb('mpower')->Execute($sectionSql,'Section');
		if (count($result) == 1) {
			$objSection = $result[0];
			$objSection->Initialize();
			$objSection->ContactSectionName = $contactSectionName;
			$result->Save();
		}
		return $status;
	}

	function deleteSectionById($sectionId){
		$status=new Status();
		$sql="delete from Section where SectionID = ?";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $sectionId));
		$options = DbConnManager::GetDb('mpower')->Exec($sql);
		return $status;
	}
	
	function getMaxSecIdBySecName($compId,$accSecName,$conSecName){
		if($accSecName == NULL && $conSecName == NULL)
			return;
			
		else if($conSecName == NULL)
			$sql = "select MAX(SectionID) as SectionID from Section where CompanyID = ? AND AccountSectionName Like '".$accSecName."'";
		else if($accSecName == NULL)
			$sql = "select MAX(SectionID) as SectionID from Section where CompanyID = ? AND ContactSectionName Like '".$conSecName."'";

		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $compId));
		$sectionId = DbConnManager::GetDb('mpower')->Exec($sql);
		
		return $sectionId;
	}
}



?>