<?php

require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.EmailOutgoing.php';

class ModuleEmailOutgoing extends JetstreamModule
{
	public $title = 'Email Outgoing';
	protected $javascript_files = array('contact.js', 'ui.datepicker.js', 'jquery.cookie.js', 'overlay.js', 'email_hold.js');
	protected $css_files = array('jet_datagrid.css', 'ui.datepicker.css', 'overlay.css', 'note_print.css','email_hold.css', 'note.css');	
	
	public $email_outgoing;
	public $contact_hint_list;
	public $contact_email_addresses;	

	public function Init() {
		
		/**
		 * Create EmailIncoming() class object.
		 */
		$obj_email_outgoing = new EmailOutgoing();
		$this->email_outgoing = $obj_email_outgoing->GetEmailOutgoing($_SESSION['login_id']);
		
		/**
		 * include tpl file for HTML section.
		 */
		$this->AddTemplate('module_email_outgoing.tpl');
//		$this->AddTemplate('snippet_note_list.tpl');
	}
}