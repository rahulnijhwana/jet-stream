<?php
/*
 * include the class file
 * include the DB API query file
 * include the country list
 */
require_once BASE_PATH . '/include/class.RecContact.php';
require_once BASE_PATH . '/slipstream/lib.php';

// Smarty Append to css_include for any css files you need included in the page
$smarty->append('css_include', array('jquery-calendar.css' => 1, 'flora.datepicker.css' => 1, 'note.css' => 1), true);

$smarty->append('js_include', array('jquery.js' => 1, 'note.js' => 1, 'json.js' => 1, 'jquery-calendar.min.js' => 1), true);

// Append the tpl file to the appropriate array - content or modules.  The key will appear as
// the title of the content/module box
// See the search_test.tpl file for info on building the template
//$smarty->append('content', array('Contact' => 'contact_info.tpl'), true);


$smarty->append('content', array('Search Notes' => array('template' => 'notes.tpl', 'buttons' => array(array('tooltip' => 'Print Notes', 'image' => 'print24.png', 'dest' => 'javascript:printNotes();'), array('tooltip' => 'Export Notes', 'image' => 'export24.png', 'dest' => 'javascript:exportNotes();')))), true);

?>