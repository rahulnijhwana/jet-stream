<?php
include_once BASE_PATH . '/slipstream/class.Company.php';
include_once BASE_PATH . '/slipstream/class.Contact.php';
include_once BASE_PATH . '/slipstream/class.Calendar.php';
include_once BASE_PATH . '/slipstream/lib.php';

// Append the tpl file to the appropriate array - content or modules.  The key will appear as
// the title of the content/module box
// See the search_test.tpl file for info on building the template
$smarty->append('content', array('Search Slipstream Records' => 'search_test.tpl'), true);

// Ensure that any needed javascript files, CSS files, or inline javascript is appended to the 
// appropraite Smarty array:
// js_include:  Javascript files
// css_include:  Stylesheets
// js_inline:  Javascript functions and snippets
$smarty->append('js_include', array('jquery.js' => 1, 'json.js' => 1, 'dom-drag.js' => 1, 'slipstream_search.js' => 1), true);

// The body of the code is just as before - assign smarty variables as normal for the tpl file
// BE CAREFUL to assign Smarty variables so that they don't interfere with other modules
$objCompany = new Company();
$search_type = array(1 => 'Company', 'Contact', 'Company & Contact', 'Notes Subject', 'Notes Content');
$smarty->assign('SEARCHTYPE', $search_type);

if (isset($_GET['txtSearchString']) && isset($_GET['cbSearchType'])) {
	if ($_GET['cbSearchType'] == 1) {
		$result = getAccountDetailsByAccountName($_SESSION['USER']['COMPANYID'], $_GET['txtSearchString'], 1);
		$smarty->assign('COMPANY', $result['SELECT']);
		$smarty->assign('SEARCHSEL', 1);
		$smarty->assign('TOTAL_PAGE', $result['TOTAL_PAGE']);
		$smarty->assign('SEARCH_STRING', $_GET['txtSearchString']);
		$smarty->assign('NUM_ROWS', 10);
		$smarty->assign('CUR_PAGE', 1);
		$smarty->assign('RECORDCOUNT', $result['RECORDCOUNT']);
	}
	if ($_GET['cbSearchType'] == 2) {
		$result = getContactDetailsByContactName($_SESSION['USER']['COMPANYID'], $_GET['txtSearchString']);
		$smarty->assign('CONTACT', $result);
		$smarty->assign('SEARCHSEL', 2);
	}
	if ($_GET['cbSearchType'] == 3) {
		$result = getAccountDetailsByAccountName($_SESSION['USER']['COMPANYID'], $_GET['txtSearchString']);
		$smarty->assign('COMPANY', $result['SELECT']);
		
		$result = getContactDetailsByContactName($_SESSION['USER']['COMPANYID'], $_GET['txtSearchString']);
		$smarty->assign('CONTACT', $result);
		$smarty->assign('SEARCHSEL', 3);
	}
}

?>