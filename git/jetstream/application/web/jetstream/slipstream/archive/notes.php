<?php
// This main file brings together all the modules for the page and does the final display of
// the shell - it also assigns titles.
// The order that content files and modules are appended determine their order on the page -
// first on top and so forth.
require_once BASE_PATH . '/include/class.RecContact.php';
require_once BASE_PATH . '/slipstream/lib.php';

// Title is appended to Slipstream name and is not required
$smarty->assign('page_title', 'Note');

if(isset($_GET['Export'])) {
	//include BASE_PATH . '/slipstream/export_notes.php';
	include BASE_PATH . '/slipstream/export_xlsnotes.php';
}
else if(isset($_GET['Print'])) {
	include BASE_PATH . '/slipstream/print_notes.php';	
	
	$smarty->append('content', array('Print Notes' => array('template' => 'note_list.tpl')), true);
	// The last thing you do is include shell.php - see shell.php for further explanation and examples
	include BASE_PATH . '/slipstream/shell.php';	
	exit;
}

// Content
include BASE_PATH . '/slipstream/search_notes.php';
//include BASE_PATH . '/slipstream/contact_notes.php';

// Modules
include BASE_PATH . '/slipstream/sort_notes.php';
include BASE_PATH . '/slipstream/filter_notes.php';

// The last thing you do is include shell.php - see shell.php for further explanation and examples
include BASE_PATH . '/slipstream/shell.php';
?>