<?php
/**
 * include the class file
 * include the DB API query file
 * include the country list
 */
// include_once BASE_PATH . '/include/class.fieldsMap.php';
// include_once BASE_PATH . '/slipstream/class.Company.php';
// include_once BASE_PATH . '/slipstream/lib.php';

/**
 * create fieldMap() class object
 */
$objFieldMap = new fieldsMap();

/**
 * include css files
 */
$smarty->append('css_include', array('active_opportunity.css' => 1), true);

/**
 * include the tpl file
 */
$smarty->append('modules', array('Active Opportunities' => 'active_opportunity.tpl'), true);

$sqlBuild = SqlBuilder();
$opps = array();
$active_opps = array();

$first_name = $objFieldMap->getContactFirstNameField();
$last_name = $objFieldMap->getContactLastNameField();

if (isset($_GET['accountId'])) {
	$sql = 'SELECT O.DealID, P.Color, CONVERT(VARCHAR(20), O.NextMeeting, 1) NextMeeting, CONVERT(VARCHAR(20), O.FirstMeeting, 1) FirstMeeting, O.PoNumber, P.FirstName, P.LastName, (C.' . $first_name . ' + \' \' + C.' . $last_name . ') Contact 
			FROM opportunities O 
			LEFT JOIN people P ON P.PersonID = O.PersonID
			LEFT JOIN Contact C ON C.ContactID = O.ContactID
			WHERE O.AccountID = ? AND ((O.Category BETWEEN 1 AND 5) OR O.Category = 10)';
	$sql = $sqlBuild->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_GET['accountId']));
	$opps = DbConnManager::GetDb('mpower')->Execute($sql);
}

if (isset($_GET['contactId'])) {
	$sql = 'SELECT O.DealID, P.Color, CONVERT(VARCHAR(20), O.NextMeeting, 1) NextMeeting, CONVERT(VARCHAR(20), O.FirstMeeting, 1) FirstMeeting, O.PoNumber, P.FirstName, P.LastName, (C.' . $first_name . ' + \' \' + C.' . $last_name . ') Contact 
			FROM opportunities O 
			LEFT JOIN people P ON P.PersonID = O.PersonID
			LEFT JOIN Contact C ON C.ContactID = O.ContactID
			WHERE O.ContactID = ? AND ((O.Category BETWEEN 1 AND 5) OR O.Category = 10)';
	$sql = $sqlBuild->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_GET['contactId']));
	$opps = DbConnManager::GetDb('mpower')->Execute($sql);
}

if (count($opps) > 0) {
	foreach ($opps as $key => $opp) {
		
		$sql = 'SELECT P.Abbr FROM Opp_Product_XRef O
				LEFT JOIN Products P ON P.ProductID = O.ProductID
				WHERE O.DealID = ?';
		$sql = $sqlBuild->LoadSql($sql)->BuildSql(array(DTYPE_INT, $opp->DealID));
		$offerings_rs = DbConnManager::GetDb('mpower')->Execute($sql);
		
		$offerings = array();
		
		if (count($opps) > 0) {
			foreach ($offerings_rs as $offering) {
				$offerings[] = $offering->Abbr;
			}
		}
		
		$active_opps[$key]['DealID'] = $opp->DealID;
		$active_opps[$key]['Color'] = $opp->Color;
		$active_opps[$key]['Salesperson'] = $opp->FirstName . ' ' . $opp->LastName;
		$active_opps[$key]['PO'] = $opp->PoNumber;
		$active_opps[$key]['Contact'] = $opp->Contact;
		$active_opps[$key]['LastMeeting'] = (!empty($opp->NextMeeting)) ? $opp->NextMeeting : $opp->FirstMeeting;
		$active_opps[$key]['Offerings'] = implode(',', $offerings);
	}
}

$smarty->assign('ACTIVEOPP', $active_opps);

?>