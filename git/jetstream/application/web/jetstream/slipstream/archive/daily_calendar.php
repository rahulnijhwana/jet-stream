<?php
// This main file brings together all the modules for the page and does the final display of
// the shell - it also assigns titles.
// The order that content files and modules are appended determine their order on the page - 
// first on top and so forth.


// Title is appended to Slipstream name and is not required
$smarty->assign('page_title', 'Daily Calendar');

// Example of how to set the content title - used it to display company or contact name, for instance
// Get the content title
if (isset($_GET['date']) && $_GET['date'] != "") {
	$date = $_GET['date'];
	$pDate = date("n/j/Y", strtotime($date));
	$smarty->assign('PAGEDATE', $date);
	$dateFormat = date("l, F j, Y", strtotime($date));
	$smarty->assign('DATETITLE', $dateFormat);
} else {
	$date = date("d-m-Y");
	$pDate = date("n/j/Y", strtotime($date));
	$smarty->assign('PAGEDATE', $date);
	$dateFormat = date("l, F j, Y", strtotime($date));
	$smarty->assign('DATETITLE', $dateFormat);
}
$title = "<div id='tblHeaderTitle'><a href=# class=lrgBlackBold onclick='javascript:getEvent(\"prev\")'>&lt;</a>&nbsp;" . $dateFormat . "&nbsp;<a href=# class=lrgBlackBold onclick='javascript:getEvent(\"next\")'>&gt;</a></div>";
$startDate = date("m/d/Y", strtotime($date));
$smarty->assign('content_title', $title);

// The subnavigation is for links specific to the page you are currently on - for instance, 'Send Email'
// could appear on the Subnavigation bar for a contact...  It would make 
//$smarty->append('subnav', array('Send Email' => 'http://www.fieldmuseum.org', 'Initiate Self Destruct Sequence' => 'http://en.wikipedia.org/wiki/Self-destruct'), true);


// Just perform includes of the content and modules - see search_test.php for more info on
// how to build the content/module
include BASE_PATH . '/slipstream/daily_calendar_events.php';

// The last thing you do is include shell.php - see shell.php for further explanation and examples
include BASE_PATH . '/slipstream/shell.php';

?>