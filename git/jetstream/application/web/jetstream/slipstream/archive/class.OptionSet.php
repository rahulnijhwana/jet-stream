<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
include_once (BASE_PATH . '/slipstream/class.Option.php');
require_once 'class.Status.php';

class OptionSet extends MpRecord
{

	public function __toString()
	{
	        return 'Option Set';
	}
	public function Initialize()
	{
		$this->db_table = 'OptionSet';
		$this->recordset->GetFieldDef('OptionSetID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('CompanyID')->required = TRUE;
		$this->recordset->GetFieldDef('OptionSetName')->required = TRUE;
		parent::Initialize();
	}
	function insertOptionSet($companyId,$accountMapId,$contactMapId,$optionSetName){

		$status=new Status();
		$sql = "SELECT * FROM OptionSet WHERE OptionSetID =-1";
		$rs = DbConnManager::GetDb('mpower') -> Execute($sql);
		$this->SetDatabase(DbConnManager::GetDb('mpower'));
		$this->recordset = $rs;
		$this->Initialize();
		$this->CompanyID=$companyId;
		$this->OptionSetName=$optionSetName;
		$this->AccountMapID=$accountMapId;
		$this->ContactMapID=$contactMapId;
		$this->Save();
		return $status;
	}

	function getOptionSetByCompanyId($companyId)
	{
		$optionObj = new Option();
		$sql="select OptionSetID,OptionSetName,ContactMapID,AccountMapID from OptionSet where CompanyID = ? ";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId));
		$options = DbConnManager::GetDb('mpower')->Exec($sql);
		$optionSet = array();
		for($i=0;$i<count($options);$i++){
			if($options[$i]['AccountMapID'] != NULL){
				$optionValues = $optionObj->getOptionsByAccountMapId($companyId,$options[$i]['AccountMapID']);
			}else if($options[$i]['ContactMapID'] != NULL){
				$optionValues = $optionObj->getOptionsByContactMapId($companyId,$options[$i]['ContactMapID']);
			}
			$optionSet[] =  array('OptionSetName'=>$options[$i]['OptionSetName'],'OptionSetId'=>$options[$i]['OptionSetID'],'OptionSetValues'=>$optionValues);
		}
		return $optionSet;
	}
	function getOptionsSetDetailsByOptionSetId($optionSetId)
	{
		$sql="select OptionSetName,AccountMapID,ContactMapID from OptionSet where OptionSetID = ?";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $optionSetId));
		$optionsSet = DbConnManager::GetDb('mpower')->Exec($sql);
		return $optionsSet;
	}

}



?>