<?php
/**
 * include the class file
 * include the DB API query file
 */
// include_once BASE_PATH . '/include/class.fieldsMap.php';
require_once BASE_PATH . '/slipstream/class.Contact.php';
// require_once BASE_PATH . '/slipstream/lib.php';

$smarty->append('css_include', array('jet_datagrid.css' => 1, 'flora.datepicker.css' => 1), true);
$smarty->append('js_include', array('jquery.js' => 1, 'contact.js' => 1, 'jquery.blockUI.js' => 1, 'jquery.form.js' => 1, 'stringexplode.js' => 1, 'disable.js' => 1, 'json.js' => 1, 'jquery-calendar.min.js' => 1), true);
$smarty->append('content', array('Contact Information' => array('template' => 'contact_view.tpl', 'buttons' => array(array('tooltip' => 'Edit', 'image' => 'edit24.png', 'dest' => 'javascript:editContactInfo()')))), true);

$contact = new Contact();

$contact->Load($_GET['contactId']);

$smarty->assign('content_title', $contact->GetName());
$smarty->assign('VIEWDATA', $contact->DrawView());
$smarty->assign('EDITDATA', $contact->DrawView(true));


//
///**
// * create fieldMap() class object
// */
//$objFieldMap = new fieldsMap();
//
///**
// * get the contact layout
// * retrive left panel and right panel for layout structure
// */
//$contactLbls = $objFieldMap->getContactFields();
//$ContactLables = $objFieldMap->getContactLayout();
//$leftLables = $ContactLables['leftPanel'];
//$rightLables = $ContactLables['rightPanel'];
//
///**
// * assign add new contact form
// */
//$options = array();
//
//foreach ($contactLbls as $key => $value) {
//	if (substr($contactLbls[$key]['FieldName'], 0, 6) == 'Select') {
//		
//		$conMapID = $contactLbls[$key]['contactLblsID'];
//		$options[$accMapID] = getOptionByAccountMapId($conMapID, $_SESSION['USER']['COMPANYID']);
//	}
//}
////$smarty->assign('OPTIONSVALUE', $options);
//$smarty->assign('ADDNEWDATA', addNewData($leftLables, $rightLables, $options));
//
///**
// * get contact id
// * set view data for contact by contact id and set edit for contact by contact id
// */
//if (isset($_GET['contactId']) && is_numeric($_GET['contactId'])) {
//	
//	$a = 'ContactID,AccountID,';
//	foreach ($contactLbls as $map) {
//		$a .= $map['FieldName'] . ",";
//	
//	}
//	$fields = substr($a, 0, -1);
//	$contactInfo = getContactInfoByIdAndFields($_GET['contactId'], $fields);
//	
//	$contactDetails = array();
//	$contactDetailsLeft = array();
//	$contactDetailsRight = array();
//	
//	for($i = 0; $i < count($contactLbls); $i++) {
//		if ($contactLbls[$i]['Align'] == 1) {
//			
//			/**
//			 * store data for left panel
//			 * checking for select field, if select field then get value from Option table
//			 */
//			if (substr($contactLbls[$i]['FieldName'], 0, 6) == 'Select') {
//				
//				$optionID = $contactInfo[0][$contactLbls[$i]['FieldName']];
//				$optName = getOptionNameById($optionID, $_SESSION['USER']['COMPANYID']);
//				$optVal = $optName['OptionName'];
//				
//				$contactDetailsLeft[] = array('contactLblsID' => $contactLbls[$i]['ContactMapID'], 'FieldName' => $contactLbls[$i]['FieldName'], 'LabelName' => $contactLbls[$i]['LabelName'], 'Value' => $contactInfo[0][$contactLbls[$i]['FieldName']], 'IsRequired' => $contactLbls[$i]['IsRequired'], 'Position' => $contactLbls[$i]['Position'], 'OptionValue' => $optVal, 'OptionID' => $optionID);
//			
//			} else {
//				
//				$contactDetailsLeft[] = array('contactLblsID' => $contactLbls[$i]['ContactMapID'], 'FieldName' => $contactLbls[$i]['FieldName'], 'LabelName' => $contactLbls[$i]['LabelName'], 'Value' => $contactInfo[0][$contactLbls[$i]['FieldName']], 'IsRequired' => $contactLbls[$i]['IsRequired'], 'Position' => $contactLbls[$i]['Position']);
//			
//			}
//		
//		} else {
//			
//			/**
//			 * store data for right panel
//			 * checking for select field, if select field then get value from Option table
//			 */
//			if (substr($contactLbls[$i]['FieldName'], 0, 6) == 'Select') {
//				
//				$optionID = $contactInfo[0][$contactLbls[$i]['FieldName']];
//				$optName = getOptionNameById($optionID, $_SESSION['USER']['COMPANYID']);
//				$optVal = $optName['OptionName'];
//				
//				$contactDetailsRight[] = array('contactLblsID' => $contactLbls[$i]['ContactMapID'], 'FieldName' => $contactLbls[$i]['FieldName'], 'LabelName' => $contactLbls[$i]['LabelName'], 'Value' => $contactInfo[0][$contactLbls[$i]['FieldName']], 'IsRequired' => $contactLbls[$i]['IsRequired'], 'Position' => $contactLbls[$i]['Position'], 'OptionValue' => $optVal, 'OptionID' => $optionID);
//			
//			} else {
//				
//				$contactDetailsRight[] = array('contactLblsID' => $contactLbls[$i]['ContactMapID'], 'FieldName' => $contactLbls[$i]['FieldName'], 'LabelName' => $contactLbls[$i]['LabelName'], 'Value' => $contactInfo[0][$contactLbls[$i]['FieldName']], 'IsRequired' => $contactLbls[$i]['IsRequired'], 'Position' => $contactLbls[$i]['Position']);
//			
//			}
//		}
//	}
//	
//	/**
//	 * assign ACCOUNTID
//	 * assign CONTACTID
//	 * assign ACCOUNTNAME
//	 * assign contact First Name
//	 * assign contact Last Name	 
//	 */
//	$smarty->assign('ACCOUNTID', $contactInfo[0]['AccountID']);
//	$smarty->assign('CONTACTID', $_GET['contactId']);
//	$FirstName = getContactFirstName($_GET['contactId']);
//	$LastName = getContactLastName($_GET['contactId']);
//	$smarty->assign('FIRSTNAME', $FirstName);
//	$smarty->assign('LASTNAME', $LastName);
//	
//	$accName = getAccountName($contactInfo[0]['AccountID']);
//	$smarty->assign('ACCOUNTNAME', $accName);
//	
//	/**
//	 * assign HTML view contact data
//	 * assign HTML form for edit contact data
//	 */
//	$dataLeft = ArraySort($contactDetailsLeft, 'Position');
//	$dataRight = ArraySort($contactDetailsRight, 'Position');
//	
//	$smarty->assign('VIEWDATA', getViewData($dataLeft, $dataRight));
//	$smarty->assign('VIEWEDITDATA', getLBEditData($dataLeft, $dataRight, $options));
//}
//
///**
// * add new contact section
// * assign array for HOURS and MINUTES for event add and edit form
// * assign event types 
// * set contact id
// */
//$hrs = array();
//for($i = 1; $i <= 12; $i++) {
//	if ($i < 10) {
//		$hrs[] = "0" . $i;
//	} else {
//		$hrs[] = $i;
//	}
//
//}
//$hr = array();
//foreach ($hrs as $key => $value) {
//	$hr[$value] = $value;
//}
//$smarty->assign('HOURS', $hr);
//
//$mins = array();
//for($i = 0; $i <= 45; $i = $i + 15) {
//	if ($i == 0) {
//		$mins[] = "0" . $i;
//	} else {
//		$mins[] = $i;
//	}
//}
//$min = array();
//foreach ($mins as $key => $value) {
//	$min[$value] = $value;
//}
//$smarty->assign('MINUTES', $min);
//
//$eventType = getEventTypeByCompanyId($_SESSION['USER']['COMPANYID']);
//$arrEvent = array();
//foreach ($eventType as $key => $val) {
//	$keyID = $val['EventTypeID'];
//	$arrEvent[$keyID] = $val['EventName'];
//}
//$smarty->assign('EVENTTYPE', $arrEvent);
//
//if (isset($_GET['contactId']) && is_numeric($_GET['contactId'])) {
//	$smarty->assign('CONTACTID', $_GET['contactId']);
//}
?>