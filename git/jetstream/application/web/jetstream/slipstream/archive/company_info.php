<?php
include_once BASE_PATH . '/slipstream/class.Company.php';

$smarty->append('css_include', array('jet_datagrid.css' => 1, 'flora.datepicker.css' => 1), true);
$smarty->append('js_include', array('jquery.js' => 1, 'company.js' => 1, 'jquery.blockUI.js' => 1, 'jquery.form.js' => 1, 'stringexplode.js' => 1, 'disable.js' => 1, 'json.js' => 1, 'jquery-calendar.min.js' => 1), true);
$smarty->append('content', array('Company Information' => array('template' => 'company_view.tpl', 'buttons' => array(array('tooltip' => 'Edit', 'image' => 'edit24.png', 'dest' => 'javascript:editAccountInfo()')))), true);

$company = new Company();

$company->Load($_GET['accountId']);

$smarty->assign('content_title', $company->GetName());
$smarty->assign('VIEWDATA', $company->DrawView());
$smarty->assign('EDITDATA', $company->DrawView(true));

//
///**
// * get the contact layout
// * retrive left panel and right panel for layout structure
// */
//$ContactLables = $objFieldMap->getContactLayout();
//$leftCLables = $ContactLables['leftPanel'];
//$rightCLables = $ContactLables['rightPanel'];
//
///**
// * get the company fields
// * retrive left panel and right panel for layout structure
// */
//$accountLbls = $objFieldMap->getAccountFields();
//$AccountLables = $objFieldMap->getAccountLayout();
//$leftLables = $AccountLables['leftPanel'];
//$rightLables = $AccountLables['rightPanel'];
//
///**
// * set the option values for the Select fields.
// * assign HTML form for add new account
// * assign HTML form for add new contact 
// */
//$options = array();
//
//foreach ($accountLbls as $key => $value) {
//	if (substr($accountLbls[$key]['FieldName'], 0, 6) == 'Select') {
//		
//		$accMapID = $accountLbls[$key]['AccountMapID'];
//		$options[$accMapID] = getOptionByAccountMapId($accMapID, $_SESSION['USER']['COMPANYID']);
//	}
//}
//$smarty->assign('OPTIONSVALUE', $options);
//$smarty->assign('ADDNEWDATA', addNewData($leftLables, $rightLables, $options));
//$smarty->assign('ADDNEWCONTACTDATA', addNewData($leftCLables, $rightCLables, $options));
//
///**
// * get account id
// * set view data for account by account id and set edit for account by account id
// */
//if (isset($_GET['accountId'])) {
//	$accLables = '';
//	
//	foreach ($accountLbls as $map) {
//		$accLables .= $map['FieldName'] . ",";
//	
//	}
//	
//	/**
//	 * getting the value from account table for the session fields
//	 */
//	$fields = substr($accLables, 0, -1);
//	$sql = 'SELECT ? FROM Account WHERE AccountID= ?';
//	$accountSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, -1), array(DTYPE_INT, $_GET['accountId']));
//	$accountSql = str_replace('-1', $fields, $accountSql);
//	$accountInfo = DbConnManager::GetDb('mpower')->Exec($accountSql);
//	
//	$accountDetails = array();
//	$accountDetailsLeft = array();
//	$accountDetailsRight = array();
//	
//	for($i = 0; $i < count($accountLbls); $i++) {
//		if ($accountLbls[$i]['Align'] == 1) {
//			
//			/**
//			 * store data for left panel
//			 * checking for select field, if select field is present then get value from Option table
//			 */
//			if (substr($accountLbls[$i]['FieldName'], 0, 6) == 'Select') {
//				
//				$optionID = $accountInfo[0][$accountLbls[$i]['FieldName']];
//				$optName = getOptionNameById($optionID, $_SESSION['USER']['COMPANYID']);
//				$optVal = $optName['OptionName'];
//				
//				$accountDetailsLeft[] = array('AccountMapID' => $accountLbls[$i]['AccountMapID'], 'FieldName' => $accountLbls[$i]['FieldName'], 'LabelName' => $accountLbls[$i]['LabelName'], 'Value' => $accountInfo[0][$accountLbls[$i]['FieldName']], 'IsRequired' => $accountLbls[$i]['IsRequired'], 'Position' => $accountLbls[$i]['Position'], 'OptionValue' => $optVal, 'OptionID' => $optionID);
//			
//			} else {
//				
//				$accountDetailsLeft[] = array('AccountMapID' => $accountLbls[$i]['AccountMapID'], 'FieldName' => $accountLbls[$i]['FieldName'], 'LabelName' => $accountLbls[$i]['LabelName'], 'Value' => $accountInfo[0][$accountLbls[$i]['FieldName']], 'IsRequired' => $accountLbls[$i]['IsRequired'], 'Position' => $accountLbls[$i]['Position']);
//			
//			}
//		
//		} else {
//			
//			/**
//			 * store data for right panel
//			 * checking for select field, if select field is present then get value from Option table
//			 */
//			if (substr($accountLbls[$i]['FieldName'], 0, 6) == 'Select') {
//				
//				$optionID = $accountInfo[0][$accountLbls[$i]['FieldName']];
//				$optName = getOptionNameById($optionID, $_SESSION['USER']['COMPANYID']);
//				$optVal = $optName['OptionName'];
//				
//				$accountDetailsRight[] = array('AccountMapID' => $accountLbls[$i]['AccountMapID'], 'FieldName' => $accountLbls[$i]['FieldName'], 'LabelName' => $accountLbls[$i]['LabelName'], 'Value' => $accountInfo[0][$accountLbls[$i]['FieldName']], 'IsRequired' => $accountLbls[$i]['IsRequired'], 'Position' => $accountLbls[$i]['Position'], 'OptionValue' => $optVal, 'OptionID' => $optionID);
//			
//			} else {
//				
//				$accountDetailsRight[] = array('AccountMapID' => $accountLbls[$i]['AccountMapID'], 'FieldName' => $accountLbls[$i]['FieldName'], 'LabelName' => $accountLbls[$i]['LabelName'], 'Value' => $accountInfo[0][$accountLbls[$i]['FieldName']], 'IsRequired' => $accountLbls[$i]['IsRequired'], 'Position' => $accountLbls[$i]['Position']);
//			
//			}
//		}
//	}
//	
//	/**
//	 * assign ACCOUNTID
//	 * assign ACCOUNTNAME
//	 */
//	$smarty->assign('ACCOUNTID', $_GET['accountId']);
//	$accName = getAccountName($_GET['accountId']);
//	$smarty->assign('ACCOUNTNAME', $accName);
//	
//	/**
//	 * assign HTML view account data
//	 * assign HTML form for edit account data
//	 */
//	$dataLeft = ArraySort($accountDetailsLeft, 'Position');
//	$dataRight = ArraySort($accountDetailsRight, 'Position');
//	$smarty->assign('VIEWDATA', getViewData($dataLeft, $dataRight));
//	$smarty->assign('VIEWEDITDATA', getLBEditData($dataLeft, $dataRight, $options));


// }
?>