<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once 'class.Status.php';

class ContactMap extends MpRecord
{

	public function __toString()
	{
	        return 'Contact Map Record';
	}
	public function Initialize()
	{
		$this->db_table = 'ContactMap';
		$this->recordset->GetFieldDef('ContactMapID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('CompanyID')->required = TRUE;
		$this->recordset->GetFieldDef('LabelName')->required = TRUE;
		$this->recordset->GetFieldDef('FieldName')->required = TRUE;
		$this->recordset->GetFieldDef('IsFirstName')->required = FALSE;
		$this->recordset->GetFieldDef('IsRequired')->required = FALSE;
		$this->recordset->GetFieldDef('OptionType')->required = FALSE;
		$this->recordset->GetFieldDef('ValidationType')->required = FALSE;
		$this->recordset->GetFieldDef('IsLastName')->required = FALSE;
		$this->recordset->GetFieldDef('IsEmailAddress')->required = FALSE;
		$this->recordset->GetFieldDef('Align')->required = FALSE;
		$this->recordset->GetFieldDef('Enable')->required = FALSE;
		parent::Initialize();
	}
	function insertContactFieldsToMap($companyId,$fieldName,$labelName,$isMandatory,$optionType,$validationType,$isFirstName,$isLastName,$isEmail,$align,$optionSetId){

		$status=new Status();
		$sql = "SELECT * FROM ContactMap WHERE ContactMapID =-1";
		$rs = DbConnManager::GetDb('mpower') -> Execute($sql);
		$this->SetDatabase(DbConnManager::GetDb('mpower'));
		$this->recordset = $rs;
		$this->Initialize();
		$this->CompanyID=$companyId;
		$this->FieldName=$fieldName;
		$this->LabelName=$labelName;
		$this->IsRequired=$isMandatory;
		$this->OptionType=$optionType;
		$this->ValidationType=$validationType;
		$this->IsFirstName=$isFirstName;
		$this->IsLastName=$isLastName;
		$this->IsEmailAddress=$isEmail;
		$this->Align=$align;
		$this->OptionSetID=$optionSetId;
		$this->Save();
		return $status;
	}
	function getContactMapIdByCompanyAndField($companyId,$fieldName){
		$sql="select ContactMapID from ContactMap where CompanyID = ? and FieldName = ?";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId),array(DTYPE_STRING, $fieldName));
		$mapId = DbConnManager::GetDb('mpower')->Exec($sql);
		return $mapId;
	}
	function updateContactMapById($mapId,$googleField,$labelName,$isRequired,$validationType,$isFirstName,$isLastName,$isEmail){
		$status=new Status();
		$sql = "SELECT * FROM ContactMap WHERE ContactMapID = ?";
		$accountMapSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $mapId));
		$result = DbConnManager::GetDb('mpower')->Execute($accountMapSql,'ContactMap');
		if (count($result) == 1) {
			$objContact = $result[0];
			$objContact->Initialize();
			$objContact->LabelName = $labelName;
			$objContact->googleField = $googleField;
			$objContact->IsFirstName = $isFirstName;
			$objContact->IsLastName = $isLastName;
			$objContact->IsEmailAddress = $isEmail;
			$objContact->IsRequired = $isRequired;
			$objContact->ValidationType = $validationType;
			$result->Save();
		}
		return $status;

	}
	function updateAlignAndPositionOfContactFields($align,$position,$labelName,$companyId,$enable){
			$status=new Status();
			$sql = "SELECT * FROM ContactMap WHERE CompanyID = ? and LabelName = ?";
			$contactMapSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyId),array(DTYPE_STRING, $labelName));
			$result = DbConnManager::GetDb('mpower')->Execute($contactMapSql,'ContactMap');
			if (count($result) == 1) {
				$objAccount = $result[0];
				$objAccount->Initialize();
				$objAccount->Position = $position;
				$objAccount->Align = $align;
				$objAccount->Enable = $enable;
				$result->Save();
			}
			return $status;
	}


	function getContactMapById($companyId) {
		$sql = 'SELECT ContactMapID,FieldName,LabelName, BasicSearch,IsRequired,IsFirstName,IsLastName,ValidationType,Position,Align FROM ContactMap WHERE CompanyID= ?';
		$contactSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $companyId));
		$contactMap = DbConnManager::GetDb('mpower')->Exec($contactSql);

		$_SESSION['USER']['CONTACTMAP'] = $contactMap;
		//return $accountMap;
	}

	//Fetch different field values and validationName from ContactMap and Validation table for company
	function getFieldsFromContactMapByCompId($companyId,$criteria)
	{
		$searchCriteria = $criteria.'%';
		$sql="SELECT ContactMapID,FieldName,LabelName,IsFirstName,IsLastName,IsEmailAddress,IsRequired,OptionType,ValidationType,Align,Enable,OptionSetID FROM ContactMap where CompanyID= ? and FieldName Like ? Order By FieldName";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId),array(DTYPE_STRING, $searchCriteria));
		$fieldNames = DbConnManager::GetDb('mpower')->Exec($sql);
		for($i=0;$i<count($fieldNames);$i++){
			$validationType = $fieldNames[$i]['ValidationType'];

			if($validationType  != '' || $validationType !=NULL){

				$sql="SELECT ValidationName FROM Validation where ValidationID= ? ";
				$sqlBuild = SqlBuilder()->LoadSql($sql);
				$sql = $sqlBuild->BuildSql(array(DTYPE_STRING, $validationType));
				$validationName = DbConnManager::GetDb('mpower')->Exec($sql);
				$fieldNames[$i]['ValidationName'] = $validationName[0]['ValidationName'];
			}else{
				$fieldNames[$i]['ValidationName'] = '-1';
			}
		}
		return $fieldNames;
	}

}



?>
