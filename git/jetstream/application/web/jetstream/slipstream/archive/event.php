<?php
/**
 * include the lib file that contains all common functions.
 */
include BASE_PATH . '/slipstream/lib.php';

/**
 * Title is appended to Slipstream name and is not required
 */
$smarty->assign('page_title', 'Event');

/**
 * Content
 */
include BASE_PATH . '/slipstream/event_info.php';
include BASE_PATH . '/slipstream/module_notes.php';

/**
	* Modules
	*/
//include BASE_PATH . '/slipstream/dashboard_calendar.php';
//include BASE_PATH . '/slipstream/dashboard_appointment.php';


/**
 * Modules are max 290px wide and are included in exactly the same way as the content.  
 * Simple inclusion of two of them here.
 */
//$smarty->append('modules', array('Module 1' => 'module.tpl', 'Module 2' => 'module.tpl'), true);

/**
 * The last thing you do is include shell.php - see shell.php for further explanation and examples
 */
include BASE_PATH . '/slipstream/shell.php';

?>