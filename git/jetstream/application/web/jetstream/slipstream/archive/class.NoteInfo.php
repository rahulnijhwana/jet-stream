<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/include/mpconstants.php';

/**
 * The DB fields initialization
 */
class NoteInfo extends MpRecord
{

	public function Initialize() {
		$this->db_table = 'Notes';
		$this->recordset->GetFieldDef('NoteID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('CreatedBy')->required = TRUE;
		$this->recordset->GetFieldDef('Subject')->required = TRUE;
		$this->recordset->GetFieldDef('NoteText')->required = TRUE;
		$this->recordset->GetFieldDef('ObjectType')->required = TRUE;
		$this->recordset->GetFieldDef('ObjectReferer')->required = TRUE;
		
		parent::Initialize();
	}
}

?>