<?php
// This main file brings together all the modules for the page and does the final display of
// the shell - it also assigns titles.
// The order that content files and modules are appended determine their order on the page -
// first on top and so forth.
include BASE_PATH . '/slipstream/class.Company.php';

//$objContact = new Company();

// This line produces a notice when going in via 'Add Company'


//if (isset($_GET['accountId'])) {
//	$accountInfo = $objContact->getAccountInfoById($_GET['accountId']);
//}

// Title is appended to Slipstream name and is not required
$smarty->assign('page_title', 'Company');

// Example of how to set the content title - used it to display company or contact name, for instance
//if (isset($accountInfo) != "") {
//	$smarty->assign('content_title', $accountInfo['Name']);
//}

// The subnavigation is for links specific to the page you are currently on - for instance, 'Send Email'
// could appear on the Subnavigation bar for a contact
// $smarty->append('subnav', array('Send Email' => 'http://www.fieldmuseum.org', 'Initiate Self Destruct Sequence' => 'http://en.wikipedia.org/wiki/Self-destruct'), true);


// Just perform includes of the content and modules - see search_test.php for more info on
// how to build the content/module


// Content
include BASE_PATH . '/slipstream/company_info.php';
include BASE_PATH . '/slipstream/module_notes.php';

// Modules
include BASE_PATH . '/slipstream/company_contacts.php';
include BASE_PATH . '/slipstream/module_events.php';
include BASE_PATH . '/slipstream/active_opportunity.php';
include BASE_PATH . '/slipstream/closed_opportunity.php';
//include BASE_PATH . '/slipstream/purchase_orders.php';




// The following is used to show what the result of including a second
// content seciton will do, but this should be done in an include
// $smarty->append('content', array('Module 2' => 'content.tpl'), true);


// Modules are max 290px wide and are included in exactly the same way as the content.  Simple inclusion
// of two of them here


// The last thing you do is include shell.php - see shell.php for further explanation and examples
include BASE_PATH . '/slipstream/shell.php';

?>