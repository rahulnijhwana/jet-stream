<?php
/**
 * include the class event file.
 */
include_once BASE_PATH . '/slipstream/class.Event.php';

/**
 * include css files.
 */
$smarty->append('css_include', array('jquery-calendar.css' => 1, 'flora.datepicker.css' => 1), true);

/**
 * include javascript files.
 */
$smarty->append('js_include', array('jquery.js' => 1, 'jquery.clockpick.1.2.4.js' => 1, 'event.js' => 1, 'jquery.blockUI.js' => 1, 'jquery.form.js' => 1, 'json.js' => 1, 'jquery-calendar.min.js' => 1), true);

if (isset($_GET['eventId']) && $_GET['eventId'] > 0 && (is_numeric($_GET['eventId']))) {	
	/**
	 * include tpl file for HTML section.
	 */
	$smarty->append('content', array('Event' => array('template' => 'event_info.tpl', 'buttons' => array(array('tooltip' => 'Edit', 'image' => 'edit24.png', 'dest' => 'javascript:editEvent()')))), true);	

	/**
	 * Get the company id from session.
	 * Get the event id.
	 */
	$companyId = $_SESSION['USER']['COMPANYID'];
	$eventId = $_GET['eventId'];
	
	/**
	 * create Event() class object.
	 * Get the event types using company id.
	 * Get the eventinfo for the selected event.	
	 * Set Content title
	 * Set contact id 
	 */
	$objEvent = new Event();
	$objEvent->eventId = $eventId;
	$arrEvent = $objEvent->getEventTypeByCompanyId($companyId);
	$eventDetails = $objEvent->getEventInfo();
	$title = "<div id='tblHeaderTitle'>" . $eventInfo[0]['EventName'] . "</div>";
	$smarty->assign('content_title', $title);
	
	$smarty->assign('FORM_TITLE', 'Edit Event');
	$smarty->assign('EVENTTYPE', $arrEvent);	
	$smarty->assign('EVENTINFO', $eventDetails);
	$smarty->assign('ContactID', $eventDetails['ContactID']);
	$smarty->assign('EVENTID', $eventId);
	

}
else {
	/**
	 * include tpl file if there is no event id.
	 * Set Content title
	 */
	$smarty->append('content', array('Event' => array('template' => 'event_info.tpl', 'buttons' => array(array('tooltip' => 'Edit', 'image' => 'edit24.png', 'dest' => '#')))), true);
	$title = "<div id='tblHeaderTitle'></div>";
	$smarty->assign('content_title', $title);
}

?>