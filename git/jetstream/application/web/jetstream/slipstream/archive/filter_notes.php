<?php
/*
 * include the class file
 * include the DB API query file
 * include the country list
 */
// include_once BASE_PATH . '/slipstream/class.Company.php';
// include_once BASE_PATH . '/slipstream/lib.php';

// Append the tpl file to the appropriate array - content or modules.  The key will appear as
// the title of the content/module box
// See the search_test.tpl file for info on building the template
$smarty->append('modules', array('Filter Notes' => 'filter_notes.tpl'), true);

$objNote = new Note();
$smarty->assign('STARTDATE', date('m/d/Y'));
$smarty->assign('ENDDATE', date('m/d/Y'));

$smarty->assign('FILTERLISTARR', 'var filterList = '.json_encode($objNote->createFilterArray()));
?>