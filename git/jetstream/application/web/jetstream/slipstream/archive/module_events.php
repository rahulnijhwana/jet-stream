<?php
/**
 * include the class event file.
 */
include_once BASE_PATH . '/slipstream/class.Event.php';

/**
 * Include css files.
 */
$smarty->append('css_include', array('jquery-calendar.css' => 1, 'flora.datepicker.css' => 1), true);

/**
 * Include javascript files.
 */
//$smarty->append('js_include', array('jquery.js' => 1, 'event.js' => 1, 'jquery.blockUI.js' => 1, 'jquery.form.js' => 1, 'stringexplode.js' => 1, 'disable.js' => 1, 'json.js' => 1, 'jquery-calendar.min.js' => 1), true);
$smarty->append('js_include', array('jquery.js' => 1, 'jquery.clockpick.1.2.4.js' => 1, 'event.js' => 1, 'jquery.blockUI.js' => 1, 'jquery.form.js' => 1, 'json.js' => 1, 'jquery-calendar.min.js' => 1), true);

/**
 * Create Event() class object.
 */
$objEvent = new Event();

/**
 * Get the company id from session.
 * Get the current date.
 */
$companyId = $_SESSION['USER']['COMPANYID'];
$today = date("m/d/Y");
$todayDtTime = date("Y-m-d h:i:s");

/**
 * Get the events for the account.
 */
if(isset($_GET['action']) && (trim($_GET['action']) == 'company')) {
	if(isset($_GET['accountId']) && ($_GET['accountId'] > 0) && is_numeric($_GET['accountId'])) {
		/**
		 * include tpl file for HTML section.
		 */
		$smarty->append('modules', array('Upcoming Events' => 'module_events.tpl'), true);
		
		/**
		 * Get the events for the selected account.
		 */
		$accountId = $_GET['accountId'];
		$events = $objEvent->getUpcomingEvents($companyId, 0, $accountId, $todayDtTime);
		$smarty->assign('EVENTS',$events);
	}
	else {
		/**
		 * include tpl file for HTML section when company id does not exist.
		 */
		$smarty->append('modules', array('Upcoming Events' => 'module_events.tpl'), true);
	}
}
/**
 * Get the events for the contact.
 */
else if(isset($_GET['action']) && (trim($_GET['action']) == 'contact')) {
	if (isset($_GET['contactId']) && $_GET['contactId'] != "") {	
		/**
		 * include tpl file for HTML section according to contact id
		 */
		$smarty->append('modules', array('Upcoming Events' => array('template' => 'module_events.tpl', 'buttons' => array(array('tooltip' => 'Add', 'image' => 'add20.png', 'dest' => 'javascript:addEvent();', 'div' => 'eventsAddLink')))), true);
				
		/**
		 * Get the events for the selected contact.
		 */
	 	$contactId = $_GET['contactId'];
	 	$events = $objEvent->getUpcomingEvents($companyId, $contactId, 0, $todayDtTime);
		$smarty->assign('EVENTS',$events);
		
		/**
		 * Add new contact section
		 * Get the event types using company id.
		 * Set the start date as current date. 
		 * Set contact id
		 */	
		$arrEvent = $objEvent->getEventTypeByCompanyId($companyId);
		$dateArr['StartDate'] = $today;		
		$smarty->assign('EVENTTYPE', $arrEvent);	
		$smarty->assign('EVENTINFO', $dateArr);		
		$smarty->assign('FORM_TITLE', 'Add Event');
		
		if (isset($contactId) && is_numeric($contactId)) {
			$smarty->assign('ContactID', $contactId);
		}
		
	}
	else {
		/**
		 * Include tpl file for HTML section when contact id does not exist.
		 */
		$smarty->append('modules', array('Upcoming Events' => 'module_events.tpl'), true);
	}
}
/**
 * Get the current date's events displayed in the dashboard page.
 */
else {
	$smarty->append('modules', array('Today\'s Appointments' => 'module_events.tpl'), true);	
	$events = $objEvent->getUpcomingEvents($companyId, 0, 0, $today, 'current');
	$smarty->assign('EVENTS',$events);
}
?>