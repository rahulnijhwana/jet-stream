<?php
// This main file brings together all the modules for the page and does the final display of
// the shell - it also assigns titles.
// The order that content files and modules are appended determine their order on the page - 
// first on top and so forth.


// Title is appended to Slipstream name and is not required
$smarty->assign('page_title', 'Weekly Calendar');

// Example of how to set the content title - used it to display company or contact name, for instance
// Get the content title
// If first date, last date, month and year is set in URL
if (isset($_GET['fdate']) && $_GET['fdate'] != "") {
	$firstDate = $_GET['fdate'];
}
if (isset($_GET['ldate']) && $_GET['ldate'] != "") {
	$lastDate = $_GET['ldate'];
}
if (isset($_GET['month']) && $_GET['month'] != "") {
	$month = $_GET['month'];
}
if (isset($_GET['year']) && $_GET['year'] != "") {
	$year = $_GET['year'];
}

// If first date and last date is not set in URL get the default week for current date
if (!isset($_GET['fdate']) && !isset($_GET['ldate'])) {
	$arrDay = array('Sunday' => 1, 'Monday' => 2, 'Tuesday' => 3, 'Wednesday' => 4, 'Thursday' => 5, 'Friday' => 6, 'Saturday' => 7);
	$today = date("l");
	$todayDate = date("d");
	$dayNo = $arrDay[$today];
	$firstDateInterval = ($dayNo - 1);
	$lastDateInterval = (7 - $dayNo);
	
	$firstDate = ($todayDate - $firstDateInterval);
	$lastDate = ($todayDate + $lastDateInterval);	
	$month = date('m');;
	$year = date('Y');
	$initialDate = date('Y-m-d', mktime(0, 0, 0, $month, $todayDate - $firstDateInterval, $year));
	$finalDate = date('Y-m-d', mktime(0, 0, 0, $month, $todayDate + $lastDateInterval, $year));
	//$year . "-" . $month . "-" . $firstDate;
	//$finalDate = $year . "-" . $month . "-" . $lastDate;
} 
else {
	$initialDate = $year . "-" . $month . "-" . $firstDate;
	$finalDate = $year . "-" . $month . "-" . $lastDate;
}
$initialDateFormat = date("F j, Y", strtotime($initialDate));
$finalDateFormat = date("F j, Y", strtotime($finalDate));
$dateTitle = $initialDateFormat . " - " . $finalDateFormat;

$title = "<div id='tblHeaderTitle'><a href=# class=lrgBlackBold onclick='javascript:getEvent(\"prev\")'>&lt;</a>&nbsp;" . $dateTitle . "&nbsp;<a href=# class=lrgBlackBold onclick='javascript:getEvent(\"next\")'>&gt;</a></div>";
//$startDate = date("m/d/Y", strtotime($date));
$smarty->assign('content_title', $title);

// Just perform includes of the content and modules - see search_test.php for more info on
// how to build the content/module
include BASE_PATH . '/slipstream/weekly_calendar_events.php';

// The last thing you do is include shell.php - see shell.php for further explanation and examples
include BASE_PATH . '/slipstream/shell.php';

?>