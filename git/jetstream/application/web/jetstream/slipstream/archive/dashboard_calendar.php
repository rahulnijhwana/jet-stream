<?
require_once BASE_PATH . '/slipstream/class.Calendar.php';
require_once BASE_PATH . '/slipstream/lib.php';

$smarty->append('modules', array('Event Calendar' => 'dashboard_calendar.tpl'), true);
$smarty->append('css_include', array('mini_calendar.css' => 1), true);
$smarty->append('js_include', array('jquery.js' => 1, 'dashboard_calendar.js' => 1, 'json.js' => 1), true);

/*
 * Define Calendar Section
 */
$today = date("n_o_F_j");
$expDate = explode("_", $today);
$tmonth = $expDate['0'];
$year = $expDate['1'];
$strMonth = $expDate['2'];
$todayDate = $expDate['3'];
$totWeek = 2 + ceil((date("t", mktime(0, 0, 0, $tmonth, 1, $year)) - (8 - date("w", mktime(0, 0, 0, $tmonth, 1, $year)))) / 7);
//$smarty->assign('WEEKS',$totWeek);


$smarty->assign('YEAR', $year);
$smarty->assign('MONTH', $strMonth);
$smarty->assign('TODAY', $todayDate);
$smarty->assign('MONTHINT', $tmonth);

$myCelandar = new Calendar();
$dashCalendar = $myCelandar->showCalendar($tmonth, $year);
$smarty->assign('CALENDAR', $dashCalendar);

/*
 * Assign week section to the Calendar
 */
$count = 0;
$arr = array();
for($i = 0, $j = 1; $i < count($dashCalendar); $i++) {
	if ($dashCalendar[$i] != '-') {
		$arr[$j][] = $dashCalendar[$i];
	} else {
		$arr[$j][] = '-';
	}
	if (count($arr[$j]) == 7) {
		$j++;
	}
}

function isNum($variable) {
	if (is_int($variable)) {
		return $variable;
	}
}
$weekArr = array();
$j = 1;
foreach ($arr as $key => $value) {
	$weekArr[$j] = array_filter($value, "isNum");
	$j++;
}
//print_r($weekArr);
$smarty->assign('WEEKDATE', $weekArr);
$smarty->assign('WEEKS', count($weekArr));

/**
 * get all events for current month
 */
$events = getMonthEvents($_SESSION['USER']['COMPANYID'], $_SESSION['USER']['USERID'], $tmonth, $year);

$eventArr = array();
foreach ($events as $key => $val) {
	$sDT = $val['StartDate'];
	
	$mdy = explode("/", $sDT);
	if (isset($mdy[0])) {
		$month = $mdy[0];
	}
	if (isset($mdy[1])) {
		$date = $mdy[1];
	}
	if (isset($mdy[2])) {
		$year = $mdy[2];
	}
	if ($month == $tmonth) {
		$eventArr[$date][] = array('Date' => $date, 'EventName' => $val['EventName'], 'EventColor' => $val['EventColor']);
		//$eventArr[$date][] = array('Date'=>$date);
	}
}
$smarty->assign('DATEEVENTS', $eventArr);

$eventDate = array();
foreach ($eventArr as $key => $value) {
	$eventDate[(integer) $key] = 'true';
}
$smarty->assign('CALENDAREVENT', $eventDate);
?>