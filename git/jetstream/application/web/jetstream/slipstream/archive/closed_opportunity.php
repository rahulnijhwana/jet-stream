<?php
/**
 * include the class file
 * include the DB API query file
 * include the country list
 */
// require_once BASE_PATH . '/include/class.fieldsMap.php';
// require_once BASE_PATH . '/slipstream/class.Company.php';
// require_once BASE_PATH . '/slipstream/lib.php';

/**
 * create fieldMap() class object
 */
$objFieldMap = new fieldsMap();

/**
 * include css files
 */
$smarty->append('css_include', array('closed_opportunity.css' => 1), true);

/**
 * include the tpl file
 */
$smarty->append('modules', array('Closed Opportunities' => 'closed_opportunity.tpl'), true);

$sqlBuild = SqlBuilder();
$opps = array();
$closed_opps = array();
$first_name = $objFieldMap->getContactFirstNameField();
$last_name = $objFieldMap->getContactLastNameField();

if (isset($_GET['accountId'])) {
	$sql = 'SELECT O.DealID, P.Color, CONVERT(VARCHAR(20), O.ActualCloseDate, 1) ActualCloseDate, O.ActualDollarAmount, P.FirstName, P.LastName, (C.' . $first_name . ' + \' \' + C.' . $last_name . ') Contact 
			FROM opportunities O 
			LEFT JOIN people P ON P.PersonID = O.PersonID
			LEFT JOIN Contact C ON C.ContactID = O.ContactID
			WHERE O.AccountID = ? AND O.Category = 6';
	$sql = $sqlBuild->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_GET['accountId']));
	$opps = DbConnManager::GetDb('mpower')->Execute($sql);
}

if (isset($_GET['contactId'])) {
	$sql = 'SELECT O.DealID, P.Color, CONVERT(VARCHAR(20), O.ActualCloseDate, 1) ActualCloseDate, O.ActualDollarAmount, P.FirstName, P.LastName, (C.' . $first_name . ' + \' \' + C.' . $last_name . ') Contact 
			FROM opportunities O 
			LEFT JOIN people P ON P.PersonID = O.PersonID
			LEFT JOIN Contact C ON C.ContactID = O.ContactID
			WHERE O.ContactID = ? AND O.Category = 6';
	$sql = $sqlBuild->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_GET['contactId']));
	$opps = DbConnManager::GetDb('mpower')->Execute($sql);
}

if (count($opps) > 0) {
	foreach ($opps as $key => $opp) {
		
		$sql = 'SELECT P.Abbr FROM Opp_Product_XRef O
				LEFT JOIN Products P ON P.ProductID = O.ProductID
				WHERE O.DealID = ?';
		$sql = $sqlBuild->LoadSql($sql)->BuildSql(array(DTYPE_INT, $opp->DealID));
		$offerings_rs = DbConnManager::GetDb('mpower')->Execute($sql);
		
		$offerings = array();
		
		if (count($opps) > 0) {
			foreach ($offerings_rs as $offering) {
				$offerings[] = $offering->Abbr;
			}
		}
		
		$closed_opps[$key]['DealID'] = $opp->DealID;
		$closed_opps[$key]['Color'] = $opp->Color;
		$closed_opps[$key]['Salesperson'] = $opp->FirstName . ' ' . $opp->LastName;
		$closed_opps[$key]['Total'] = '$' . number_format($opp->ActualDollarAmount);
		$closed_opps[$key]['Contact'] = $opp->Contact;
		$closed_opps[$key]['ClosedDate'] = $opp->ActualCloseDate;
		$closed_opps[$key]['Offerings'] = implode(',', $offerings);
	}
}

$smarty->assign('CLOSEDOPP', $closed_opps);

?>