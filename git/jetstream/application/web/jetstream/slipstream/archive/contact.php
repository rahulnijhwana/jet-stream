<?php
require_once BASE_PATH . '/include/class.RecContact.php';
require_once BASE_PATH . '/slipstream/lib.php';

$smarty->assign('page_title', 'Contact');

//if (isset($_GET['contactId'])) {
//	if (isset($_GET['contactId']) && is_numeric($_GET['contactId'])) {
//		//To be modified
//	//$results=getContactInfoById($_GET['contactId']);
//	//$smarty->assign('CONTACTINFO',$results[0]);
//	//$title = "<div id='tblCompanyHeader'>".$results[0]['FirstName']." ".$results[0]['MiddleName']." ".$results[0]['LastName']."</div>";
//	}
//} elseif (!isset($_GET['contactId'])) {
//	$title = "<div id='tblCompanyHeader'></div>";
//}


//$smarty->assign('content_title', $title);


$smarty->append('subnav', array('Log Incoming Call' => '#', 'Log Outgoing Call' => '#', 'Email Contact' => '#'), true);

// Content
include BASE_PATH . '/slipstream/contact_info.php';
include BASE_PATH . '/slipstream/module_notes.php';

// Modules
include BASE_PATH . '/slipstream/contact_lists.php';
include BASE_PATH . '/slipstream/module_events.php';
include BASE_PATH . '/slipstream/active_opportunity.php';
include BASE_PATH . '/slipstream/closed_opportunity.php';

// The last thing you do is include shell.php - see shell.php for further explanation and examples
include BASE_PATH . '/slipstream/shell.php';
