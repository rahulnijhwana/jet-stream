<?php
/*
 * include the class file
 * include the DB API query file
 */
include_once BASE_PATH . '/slipstream/class.Note.php';
include_once BASE_PATH . '/slipstream/lib.php';

// Append the tpl file to the appropriate array - content or modules.  The key will appear as
// the title of the content/module box
// See the search_test.tpl file for info on building the template
$smarty->append('modules', array('Sort Notes' => 'sort_notes.tpl'), true);

/* These are the content of the sort selectbox. */
$sortArr = array('Date', 'Company', 'Contact', 'Salesperson', 'Opportunity');
$smarty->assign('SORT', $sortArr);
?>