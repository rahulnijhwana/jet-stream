<?php

class Status
{
	var $status;
	var $error;
	var $message;
	var $getFormattedVar;

	function __construct() {
		$this->status = "OK";
		$this->error = array();
		$this->message = array();
		$this->getFormattedVar = "";
		$num_args = func_num_args();

		switch ($num_args) {
			case 1 :
				$this->status = func_get_arg(0);
				break;
			case 2 :
				$this->status = func_get_arg(0);
				$this->error[] = func_get_arg(1);
				break;
		}
	}

	function addError($tStatus, $idx = '') {

		if (is_object($tStatus)) {
			$this->getFormattedVar = $tStatus->getFormattedVar;
			foreach ($tStatus->error as $key => $value) {
				$this->status = "FALSE";

				if ($idx != '')
					$this->error[$idx] = $value;
				else
					$this->error[] = $value;
			}
		} elseif (is_string($tStatus)) {
			$this->status = "FALSE";
			if ($idx != '')
				$this->error[$idx] = $tStatus;
			else
				$this->error[] = $tStatus;
		}
	}

	function addMessage($tStatus, $idx = '') {
		if (is_object($tStatus)) {
			$this->getFormattedVar = $tStatus->getFormattedVar;
			if (count($tStatus->message) > 0) {
				foreach ($tStatus->message as $key => $value) {
					if ($idx != '')
						$this->message[$idx] = $value;
					else
						$this->message[] = $value;
				}
			} else {
				if ($idx != '')
					$this->message[$idx] = $tStatus->message;
				else
					$this->message[] = $tStatus->message;
			}
		} elseif (is_string($tStatus)) {
			if ($idx != '')
				$this->message[$idx] = $tStatus;
			else
				$this->message[] = $tStatus;
		}
	}

	function getStatus() {
		return array("STATUS" => $this->status, "ERROR" => $this->error, "MESSAGE" => $this->message, "GETFORMATTEDVAR" => $this->getFormattedVar);
	}

	function setFormattedVar($val) {
		$this->getFormattedVar = $val;
	}

	function setStatus($st) {
		if ($st == "OK") {
			$this->error = array();
			$this->status = "OK";
		} else {
			$this->status = "FALSE";
		}
	}

	function flushMessage() {
		$this->message = array();
	}

	function flushError() {
		$this->error = array();
	}

	function flushAll() {
		$this->message = array();
		$this->error = array();

	}
}
;
?>