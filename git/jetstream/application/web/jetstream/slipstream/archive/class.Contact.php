<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once 'class.Status.php';

class Contact extends MpRecord
{

	public function __toString()
	{
		return 'Contact';
	}
	public function Initialize()
	{
		$this->db_table = 'Contact';
		$this->recordset->GetFieldDef('ContactID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('CompanyID')->required = TRUE;
		parent::Initialize();
	}

	function getFieldsFromContact($type)
	{
		$searchCriteria = $type.'%';
		$sql="select COLUMN_NAME as FieldName from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='Contact' and COLUMN_NAME like ?";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_STRING, $searchCriteria));
		$fieldNames = DbConnManager::GetDb('mpower')->Exec($sql);
		return $fieldNames;
	}
}



?>