<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/include/mpconstants.php';

/**
 * The DB fields initialization
 */
class UserAccountInfo extends MpRecord
{

	public function Initialize() {
		$this->db_table = 'UserAccount';
		$this->recordset->GetFieldDef('UserAccountID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('UserID')->required = TRUE;
		$this->recordset->GetFieldDef('AccountID')->required = TRUE;
		
		parent::Initialize();
	}

	public function Save($simulate = false) {
		parent::Save($simulate);
	}

}

?>