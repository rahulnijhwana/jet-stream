<?php

// Do a Smarty Append to js_include for any external Javascript files you need included in the page
// A trick is used to make the name of the file the key in order to automatically remove duplicates
$smarty->append('js_include', array('jquery.js' => 1
	, 'hoverIntent.js' => 1
	, 'superfish.js' => 1
	, 'dom-drag.js' => 1
	, 'jquery.bgiframe.js' => 1
	, 'jquery.dimensions.js' => 1
	, 'add_company.js' => 1
	, 'jquery.blockUI.js' => 1
), true);

// Do a Smarty Append to css_include for any css files you need included in the page
// A trick is used to make the name of the file the key in order to automatically remove duplicates
$smarty->append('css_include', array('slipstream.css' => 1, 'slider.css' => 1, 'clockpick.1.2.4.css' => 1), true);

// In page javascript can be built in PHP and added to inline Javascript function
$js = "jQuery(function(){
			jQuery('ul.sf-menu').superfish();
		});";

$smarty->append('js_inline', $js);

// Display the shell and you are finished.  The shell will put the other modules together in the
// appropriate places
$smarty->display('shell.tpl');
