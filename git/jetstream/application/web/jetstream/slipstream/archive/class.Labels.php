<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/slipstream/class.Status.php';

class Labels extends MpRecord
{

	public function __toString() {
		return 'Label Record';
	}

	public function Initialize() {
		$this->db_table = 'Labels';
		$this->recordset->GetFieldDef('LabelID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('LabelName')->required = TRUE;
		
		parent::Initialize();
	}

	function getLabelNameById($labelId) {
		$sql = 'SELECT LabelName from Labels where LabelID = ? order by LabelID';
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $labelId));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		return $result;
	}
}

?>