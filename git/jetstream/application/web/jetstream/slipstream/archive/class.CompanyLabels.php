<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once 'class.Status.php';

class CompanyLabels extends MpRecord
{

	public function __toString() {
		return 'Company Lable Record';
	}

	public function Initialize() {
		$this->db_table = 'CompanyLabels';
		$this->recordset->GetFieldDef('CompanyLabelID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('LabelID')->required = TRUE;
		$this->recordset->GetFieldDef('CompanyID')->required = TRUE;
		$this->recordset->GetFieldDef('CompanyLabelName')->required = TRUE;
		$this->recordset->GetFieldDef('Status')->required = TRUE;
		$this->recordset->GetFieldDef('Position')->required = TRUE;
		$this->recordset->GetFieldDef('Side')->required = TRUE;
		parent::Initialize();
	}

	function updateLabel($LabelID, $CompanyLabelName, $CompanyID, $selected, $position, $side) {
		$status = new Status();
		$sql = "SELECT * FROM CompanyLabels WHERE CompanyID =? and LabelID=?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $CompanyID), array(DTYPE_INT, $LabelID));
		$rs = DbConnManager::GetDb('mpower')->Execute($sql, 'CompanyLabels');
		if (count($rs) == 1) {
			$objL = $rs[0];
			$objL->Initialize();
			$objL->CompanyID = $CompanyID;
			$objL->CompanyLabelName = $CompanyLabelName;
			$objL->LabelID = $LabelID;
			$objL->Status = $selected;
			$objL->Position = $position;
			$objL->Side = $side;
			$rs->Save();
		}
		return $status;
	}

	function getCompanyLabelsByCompanyId($companyId) {
		$sql = 'SELECT * from CompanyLabels where CompanyID = ? order by Position';
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		return $result;
	}

	function initializeCompanyLabel($labelId, $labelName, $companyId, $side) {
		$status = new Status();
		$sql = "SELECT * FROM CompanyLabels WHERE CompanyLabelID = -1";
		$rs = DbConnManager::GetDb('mpower')->Execute($sql);
		$this->SetDatabase(DbConnManager::GetDb('mpower'));
		$this->recordset = $rs;
		$this->Initialize();
		$this->CompanyID = $companyId;
		$this->CompanyLabelName = $labelName;
		$this->LabelID = $labelId;
		$this->Position = $labelId;
		$this->Status = '0';
		$this->Side = $side;
		$this->Save();
		return $status;
	}

	function getCompanyLabelsByCompanyIdAndStatus($companyId, $status) {
		$sql = 'SELECT * from CompanyLabels where CompanyID = ? and Status=? order by Position';
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId), array(DTYPE_INT, $status));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		return $result;
	}
}

?>