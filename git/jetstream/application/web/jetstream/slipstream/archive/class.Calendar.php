<?php
/**
 * class.Calendar.php - contain Calendar class
 * @package database
 * class files include
 */
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
include_once BASE_PATH . '/include/class.fieldsMap.php';
require_once BASE_PATH . '/slipstream/class.Util.php';
require_once BASE_PATH . '/slipstream/class.Status.php';

// if (!isset($_SESSION)) session_start();

class Calendar
{
	
	var $objFieldMap;
	
	/**
	 * function for creating the fieldsMap class object
	 */
	function __construct() {
		$this->objFieldMap = new fieldsMap();
	}
	
	/**
	 * function for the calendar display section
	 * @param int $month - pass the month value
	 * @param int $year - pass the year value
	 * return an array having each date of the month for that particular year, also with the blank dates
	 */
	public function showCalendar($month, $year) {
		$date = strtotime("$year/$month/1");
		$day = date("D", $date);
		$m = date("F", $date);
		$totaldays = date("t", $date);
		
		if ($day == "Sun") $st = 1;
		if ($day == "Mon") $st = 2;
		if ($day == "Tue") $st = 3;
		if ($day == "Wed") $st = 4;
		if ($day == "Thu") $st = 5;
		if ($day == "Fri") $st = 6;
		if ($day == "Sat") $st = 7;
		
		if ($st >= 6 && $totaldays == 31) {
			$tl = 42;
		} elseif ($st == 7 && $totaldays == 30) {
			$tl = 42;
		} else {
			$tl = 35;
		}
		
		$d = 1;
		
		$dt = array();
		for($i = 1; $i <= $tl; $i++) {
			
			if ($i >= $st && $d <= $totaldays) {
				$dt[] = $d;
				$d++;
			} else {
				$dt[] = "-";
			}
		}
		return $dt;
	}
	
	/**
	 * function to build an array having key as ContactID and value as Contact First Name
	 * @param array $passVal - pass the array from where it collects the ContactID
	 * @param string $ContactIDField - pass the key name for ContactID in above passing array $passVal
	 */
	public function fetchContacts($passVal, $ContactIDField) {
		$contactArr = array();
		foreach($passVal as $key=>$value) {
			$ContactID = $passVal[$key][$ContactIDField];
			
			$contactArr[$ContactID] = getContactFirstName($ContactID);
		}
		return $contactArr;
	}
}
?>
