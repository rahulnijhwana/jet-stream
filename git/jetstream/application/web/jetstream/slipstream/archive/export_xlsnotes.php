<?php
/*
 * include the class file
 * include the DB API query file
 */
include_once BASE_PATH . '/slipstream/class.Note.php';
include_once BASE_PATH . '/slipstream/class.Xls.php';
include_once BASE_PATH . '/slipstream/lib.php';

$objNote = new Note();
$objXls = new Xls();

if(trim($_REQUEST['Keyword']) != '') {
	$objNote->searchString = $_REQUEST['Keyword'];
}

$filterFields = $_REQUEST['filterOpt'];
$filterArray = array();

foreach($filterFields as $filterKey => $filterVal) {
	$newFilterKey = $filterKey + 1;

	if(($filterVal != '') && ($filterVal != 'D0') && ($filterVal != 'D1')) {
		if(isset($_REQUEST['NText'.$newFilterKey]) && (trim($_REQUEST['NText'.$newFilterKey]) != '')) {
			$filterArray[$filterVal] = $_REQUEST['NText'.$newFilterKey];
		}
	}
	else if($filterVal == 'D0') {
		$filterArray['D0_SalesPerson'] = $_REQUEST['NText'.$newFilterKey];
	}
	else if($filterVal == 'D1') {
		$filterArray['D1_StartDate'] = $_REQUEST['StartDate'.$newFilterKey];
		$filterArray['D1_EndDate'] = $_REQUEST['EndDate'.$newFilterKey];
	}
}

$objNote->noteExport = true;
$objNote->sortType = $_REQUEST['SortType'];
$objNote->sortAs = $_REQUEST['SortAs'];
$objNote->noteFilterArray = $filterArray;
$noteList = $objNote->searchNoteList();

if(count($noteList) > 0) {
	$output = '';
	$count = 1;
	$outputLabel = '<tr valign="top" bgcolor="#99CCFF" style="height:30px;font-size:12px;">';
	$bgcolor = "WHITE";

	foreach($noteList as $noteKey => $noteArr) {
		$outputRow = '<tr valign="top" style="background-color:#FFFFCC;">';

		foreach($noteArr as $noteLabel => $noteVal) {
			if($noteKey == 0) {
				if(strtolower(trim($noteLabel)) == 'timestamp') {
					$noteLabel = 'Date';
				}
				elseif(strtolower(trim($noteLabel)) == 'attachment') {
					$noteLabel = 'Attached';
				}

				if(($noteLabel != 'AddressType') && ($noteLabel != 'NewFields')) {
					$outputLabel .= '<td align="center" valign="middle">' . $objNote->escapeXlsChar($noteLabel) . '</td>';
				}
			}			

			if($noteLabel == 'Creator') {
				if(trim($noteArr['AddressType']) != '') {
					$noteVal = $noteArr['AddressType'] . ': ' . $noteVal;
				}
			}
			if(($noteLabel != 'AddressType') && ($noteLabel != 'NewFields')) {
				if($noteLabel == 'Date') {
					$outputRow .= '<td>' . $objNote->escapeXlsChar($noteVal) . '</td>';
				}
				else {
					$outputRow .= '<td>' . $objNote->escapeXlsChar($noteVal) . '</td>';
				}
			}
			
			/* Tese are the extra fields those are used by the user to filter the search results. */
			if($noteLabel == 'NewFields') {
				$newNoteFields = '';
				
				foreach($noteArr['NewFields'] as $newFieldsLabel => $newFieldsVal) {
					if($noteKey == 0) {
						$outputLabel .= '<td align="center" valign="middle">' . $objNote->escapeXlsChar($newFieldsLabel) . '</td>';
					}
					$newNoteFields .= '<td>' . $objNote->escapeXlsChar($newFieldsVal) . '</td>';
				}
				$outputRow .= $newNoteFields;
			}

		}
		$output .= $outputRow . "</tr>";
	}
	$finalOutput = $outputLabel."</tr>".$output;
}
else {
	$finalOutput = 'No Search Results Found.';
}

$excelString = $objXls->excelHeader();
$excelString .= $finalOutput;
$excelString .= $objXls->excelFooter();

//echo $excelString;
ob_clean();
ob_start();
$length = strlen($output);
$fileName = 'Notes.xls';
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=$fileName");
header('Content-Length: '.$length);
print($excelString);
ob_end_flush();
echo '<script language="javascript">hideLoadSymbol();</script>';
exit;
?>