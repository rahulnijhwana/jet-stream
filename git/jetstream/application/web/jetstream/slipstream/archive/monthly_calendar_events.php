<?php
/**
 * include the class file
 * include the DB API query file
 * include the country list
 */
include_once BASE_PATH . '/slipstream/class.Calendar.php';
include_once BASE_PATH . '/slipstream/lib.php';

/**
 * include css files
 */
 $smarty->append('css_include', array('weekly_calendar.css' => 1, 'csstooltip.css' => 1), true);

/**
 * include javascript files
 */
$smarty->append('js_include', array('jquery.js' => 1, 'monthly_calendar.js' => 1, 'json.js' => 1), true);

/**
 * include tpl file for HTML display section
 */
$smarty->append('content', array('Monthly Calendar' => 'monthly_calendar_events.tpl'), true);

/**
 * define Calendar Section
 */
if (isset($_GET['month']) != '' & isset($_GET['year']) != '') {
	$currentDate = date("m-d-Y");
	$expCurrentDate = explode("-", $currentDate);
	
	$pMonth = $_GET['month'];
	$pYear = $_GET['year'];
	$today = date("n_o_F_j", strtotime('1-' . $pMonth . '-' . $pYear));
	$expDate = explode("_", $today);
	$tmonth = $expDate['0'];
	$year = $expDate['1'];
	$strMonth = $expDate['2'];
	if ($expCurrentDate[0] == $tmonth) {
		$todayDate = $expCurrentDate[1];
	} else {
		$todayDate = '0';
	}
} else {
	$today = date("n_o_F_j");
	$expDate = explode("_", $today);
	$tmonth = $expDate['0'];
	$year = $expDate['1'];
	$strMonth = $expDate['2'];
	$todayDate = $expDate['3'];
}

$smarty->assign('YEAR', $year);
$smarty->assign('MONTH', $strMonth);
$smarty->assign('TODAY', $todayDate);
$smarty->assign('MONTHINT', $tmonth);

/**
 * create Calendar() class object
 * assign CALENDAR
 */
$objCalendar = new Calendar();
$monthlyCalendar = $objCalendar->showCalendar($tmonth, $year);
$smarty->assign('CALENDAR', $monthlyCalendar);

/**
 * assign week section to the Calendar
 */
$count = 0;
$arr = array();
for($i = 0, $j = 1; $i < count($monthlyCalendar); $i++) {
	if ($monthlyCalendar[$i] != '-') {
		$arr[$j][] = $monthlyCalendar[$i];
	} else {
		$arr[$j][] = '-';
	}
	if (count($arr[$j]) == 7) {
		$j++;
	}
}

function isNum($variable) {
	if (is_int($variable)) {
		return $variable;
	}
}
$weekArr = array();
$j = 1;
foreach ($arr as $key => $value) {
	$weekArr[$j] = array_filter($value, "isNum");
	$j++;
}
$smarty->assign('WEEKDATE', $weekArr);
$smarty->assign('WEEKS', count($weekArr));

/**
 * get events for a month
 */
$events = getMonthEvents($_SESSION['USER']['COMPANYID'], $_SESSION['USER']['USERID'], $tmonth, $year);
$eventArr = array();
foreach ($events as $key => $val) {
	$sDT = $val['StartDate'];
	
	$mdy = explode("/", $sDT);
	if (isset($mdy[0])) {
		$month = $mdy[0];
	}
	if (isset($mdy[1])) {
		$date = $mdy[1];
	}
	if (isset($mdy[2])) {
		$year = $mdy[2];
	}
	if ($month == $tmonth) {
		$eventArr[$date][] = array('Date' => $date, 'EventName' => $val['EventName'], 'EventColor' => $val['EventColor'], 'EventID' => $val['EventID'], 'StartDate' => date("l, F j, Y", strtotime($val['StartDate'])));
	}
}
$smarty->assign('DATEEVENTS', $eventArr);

$eventDate = array();
foreach ($eventArr as $key => $value) {
	$eventDate[(integer) $key] = 'true';
}
$smarty->assign('CALENDAREVENT', $eventDate);

?>