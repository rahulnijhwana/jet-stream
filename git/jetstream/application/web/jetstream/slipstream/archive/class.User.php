<?php
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/slipstream/class.Status.php';
require_once BASE_PATH . '/slipstream/class.AccountMap.php';
require_once BASE_PATH . '/slipstream/class.ContactMap.php';
require_once BASE_PATH . '/slipstream/class.Util.php';

class User extends MpRecord
{

	public function Initialize() {
		$this->db_table = '[User]';
		$this->recordset->GetFieldDef('UserID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('UserName')->required = TRUE;
		parent::Initialize();
	}

	function createUser($userValues,$companyId)
	{
		$status=$this->doValidateNewUser($userValues,$companyId);
		if($status->status=='OK')
		{
			try
			{
				$encodedPassword = $this->encryptString($userValues['Password']);
				$result = $this->getLevelDetailsByCompanyIdAndLevelId($companyId,$userValues['userType']);
				$levelNumber = $result['LevelNumber'];

				if($levelNumber == '' || $levelNumber == 'null')
					$levelNumber = -1;
				if(isset ($userValues['cbAssign']) && $userValues['cbAssign'] != -1)
					$supervisorId = $userValues['cbAssign'];
				else if(isset ($userValues['userType']) && $userValues['userType'] == -3)
					$supervisorId = -3;
				else
					$supervisorId = -1;
				$supervisorId=($supervisorId == '')?-1:$supervisorId;
				$isSalesPerson=($levelNumber == 1)?1:0;
				$sql = "SELECT * FROM [User] WHERE UserID =-1";
				$rs = DbConnManager::GetDb('mpower') -> Execute($sql);
				$this->SetDatabase(DbConnManager::GetDb('mpower'));
				$rs[] = $this;
				$this->CompanyID=$companyId;
				$this->FirstName=$userValues['FirstName'];
				$this->LastName=$userValues['LastName'];
				$this->UserName=$userValues['UserID'];
				$this->Password=$encodedPassword;
				$this->Email=$userValues['Email'];
				$this->StartDate=$userValues['StartDate'];
				$this->IsSalesPerson=$isSalesPerson;
				$this->SupervisorID=$supervisorId;
				$this->Level=$levelNumber;
				$rs->Initialize()->Save();
			}
			catch(Exception $e)
			{
				$status->addError($e->getMessage());
			}
		}
			return $status->getStatus();
	}

	function ifUserExists($userName, $companyId, $userId = '') {
		//echo $userName;
		if ($userId == '') {
			$sql = "select count(*) as USER_COUNT from [User] where UserName=? and CompanyID=?";
			$userSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $userName), array(DTYPE_INT, $companyId));
			$result = DbConnManager::GetDb('mpower')->Execute($userSql);
			return $result[0]->USER_COUNT;
		} else {
			$sql = "select count(*) as USER_COUNT from [User] where UserName=? and CompanyID=? and UserID !=?";
			$userSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $userName), array(DTYPE_INT, $companyId), array(DTYPE_INT, $userId));
			$result = DbConnManager::GetDb('mpower')->Execute($userSql);
			return $result[0]->USER_COUNT;
		}
	}

	function doValidateNewUser($userValues,$companyId='',$actionType='NEW')
	{
		$status=new Status;
		$objUtil=new Util;

		if(!isset($userValues['FirstName']) || trim($userValues['FirstName'])=="")
			$status->addError('First Name can\'t be left blank','FirstName');
		else if(!$objUtil->isAlphaNumeric($userValues['FirstName'],array("\'","#","&")))
			$status->addError('Invalid charcters for First Name','FirstName');

		if(!isset($userValues['LastName']) || trim($userValues['LastName'])=="")
			$status->addError('Last Name can\'t be left blank','LastName');
		else if(!$objUtil->isAlphaNumeric($userValues['LastName'],array("\'","#","&")))
			$status->addError('Invalid charcters for Last Name','LastName');

		if(!isset($userValues['UserID']) || trim($userValues['UserID'])=="")
			$status->addError('User Name can\'t be left blank','UserID');

		if($actionType=='NEW')
		{
			if(trim($userValues['Password'])=="")
				$status->addError('Password can\'t be left blank','Password');
			else
				$status->addMessage('','Password');
			if($userValues['Password']!='' &&  ($userValues['Password'] != $userValues['RePassword']))
				$status->addError('Password and Re Password should be same','RePassword');
		}
		else
		{
			if($userValues['Password']!='' &&  ($userValues['Password'] != $userValues['RePassword']))
				$status->addError('Password and Re Password should be same','RePassword');
		}

		if(!isset($userValues['Email']) || trim($userValues['Email'])=="")
			$status->addError('Email can\'t be left blank','Email');
		elseif(!$objUtil->isValidEmail($userValues['Email']))
			$status->addError('Enter a valid Email Id','Email');

		if(!isset($userValues['StartDate']) || trim($userValues['StartDate'])=="")
			$status->addError('Start date can\'t be left blank','StartDate');

		if(!isset($userValues['userType']) || trim($userValues['userType'])=="" || trim($userValues['userType'])== 0)
			$status->addError('Please select type of User','userType');

		if($actionType=='NEW')
		{
			if($this->ifUserExists($userValues['UserID'],$companyId))
				$status->addError('User Exist!','UserID');
		}
		else
		{
			if($this->ifUserExists($userValues['UserID'],$companyId,$userValues['txt_userid']))
				$status->addError('User Exist!','UserID');
		}

		return $status;
	}

	function isLogin() {
		if (!isset($_SESSION['USER'])) return 'false';
	}

	function doValidateLogin($_POST) {
		$status = new Status();
		if (!isset($_POST['editUserId']) || trim($_POST['editUserId']) == '') {
			$status->addError('Enter User Id', 'editUserId');
		}
		if (!isset($_POST['editPassword']) || trim($_POST['editPassword']) == "") {
			$status->addError('Enter Password', 'editPassword');
		}
		if ($status->status == 'OK') {
			$encodedPassword = $this->encryptString($_POST['editPassword']);
			$sql = "select count(*) as UserCount from [User] where UserName= ?";
			$userSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $_POST['editUserId']));
			$result = DbConnManager::GetDb('mpower')->GetOne($userSql);
			if ($result->UserCount == 0) {
				$status->addError("Please check UserId.User<font color='blue'> " . $_POST['editUserId'] . "</font> doesn't exist.", "showError");
				return $status;
			} else {
				$sql = "select * from [User] where UserName= ?";
				$loginSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $_POST['editUserId']));
				$login = DbConnManager::GetDb('mpower')->GetOne($loginSql);
				$decodedPassword = $this->decryptString($login->Password);
			}

			if (isset($decodedPassword) && $decodedPassword != sha1($_POST['editPassword'])) {
				$status->addError('Please check UserId and Password Combination', 'showError');
			} else {
				$_SESSION['USER']['USERNAME'] = $login->UserName;
				$_SESSION['USER']['USERID'] = $login->UserID;
				$_SESSION['USER']['COMPANYID'] = $login->CompanyID;
				$_SESSION['USER']['SUPERVISORID'] = $login->SupervisorID;
				$_SESSION['USER']['ISSALESPERSON'] = $login->IsSalesPerson;
				$_SESSION['USER']['FIRSTNAME'] = $login->FirstName;
				$_SESSION['USER']['LASTNAME'] = $login->LastName;
				if ($login->IsSalesPerson == 1)
					$_SESSION['USER']['TYPE'] = 'SALESPERSON';
				elseif ($login->IsSalesPerson != 1 && $login->SupervisorID == -3)
					$_SESSION['USER']['TYPE'] = 'ADMIN';
				else
					$_SESSION['USER']['TYPE'] = 'SALESMANAGER';

				$objContact = new ContactMap();
				$objAccount = new AccountMap();
				$objAccount->getAccountMapById($login->CompanyID);
				$objContact->getContactMapById($login->CompanyID);
			}
		}
		return $status;
	}

	function getAllUserByCompanyId($company_id = 0, $page_id = 0, $record_count = 0) {
		if ($page_id != 0 && $record_count != 0) {

			$from_record = $page_id * $record_count - ($record_count - 1);
			$to_record = $from_record + $record_count - 1;
			$sql = "SELECT * FROM(SELECT ROW_NUMBER() OVER (ORDER BY UserName) AS RowNumber,UserID,FirstName,LastName,UserName,Email,IsSalesPerson,Level,SupervisorID FROM [User] where CompanyID =?) _myResults where RowNumber between ? and ?";
			$sqlBuild = SqlBuilder()->LoadSql($sql);
			$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $company_id), array(DTYPE_INT, $from_record), array(DTYPE_INT, $to_record));
		} else {
			$sql = "SELECT * FROM(SELECT ROW_NUMBER() OVER (ORDER BY UserName) AS RowNumber,UserID,FirstName,LastName,UserName,Email,IsSalesPerson,Level,SupervisorID FROM [User] where CompanyID =?) _myResults";
			$sqlBuild = SqlBuilder()->LoadSql($sql);
			$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $company_id));
		}
		$user = DbConnManager::GetDb('mpower')->Exec($sql);
		return $user;
	}

	function getAllUserByCompanyIdRecordCount($company_id) {
		$sql = 'select count(*) as TotalRecord from [User] where CompanyID=?';
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $company_id));
		$rec_count = DbConnManager::GetDb('mpower')->Exec($sql);
		return $rec_count;
	}

	function getUserByPrimaryKey($primaryKey = 0) {
		$sql = "SELECT [User].SupervisorID FROM [User] WHERE UserID =" . $primaryKey;
		$id = DbConnManager::GetDb('mpower')->Exec($sql);
		$supervisorId = $id[0]['SupervisorID'];

		if ($supervisorId != -3) {
			$sql = "SELECT [User].CompanyID,UserID,FirstName,LastName,UserName,Email,IsSalesPerson,SupervisorID,CONVERT(VARCHAR(10), StartDate, 101) as StartDate,Levels.LevelID  FROM [User],Levels where [User].CompanyID=Levels.CompanyID and  UserID =" . $primaryKey . "  and Levels.LevelNumber=(select Level from [User] where UserID=" . $primaryKey . ")";
			$user = DbConnManager::GetDb('mpower')->Exec($sql);
			$result = array();
			if (count($user) > 0) $result = $this->getAllParentUserByLevelId($user[0]['CompanyID'], $user[0]['LevelID']);
		} else {
			$sql = "SELECT [User].CompanyID,UserID,FirstName,LastName,UserName,Email,IsSalesPerson,SupervisorID,CONVERT(VARCHAR(10), StartDate, 101) as StartDate,Level as LevelID FROM [User] where UserID =" . $primaryKey;
			$user = DbConnManager::GetDb('mpower')->Exec($sql);
			$result = array();
			if (count($user) > 0) $result = array();
		}
		return array('USERDATA' => $user[0], 'LEVEL' => $result);
	}

	function deleteUser($primaryKey = 0) {
		$sql = "Delete FROM [User] where UserID =" . $primaryKey;
		$user = DbConnManager::GetDb('mpower')->Exec($sql);
		return $user[0];
	}

	function updateUser($userValues,$companyId)
	{

		$status=new Status;
		$status=$this->doValidateNewUser($userValues,$companyId,'UPDATE');
		if($status->status=='OK')
		{
			$result = $this->getLevelDetailsByCompanyIdAndLevelId($companyId,$userValues['userType']);
			$levelNumber = $result['LevelNumber'];
			if($levelNumber == '' || $levelNumber == 'null')
				$levelNumber = -1;

			if(isset ($userValues['cbAssign']) && $userValues['cbAssign'] != -1)
				$supervisorId = $userValues['cbAssign'];

			else if(isset ($userValues['userType']) && $userValues['userType'] == -3)
				$supervisorId = -3;
			else
				$supervisorId = -1;

			$supervisorId=($supervisorId == '')?-1:$supervisorId;
			$isSalesPerson=($levelNumber == 1)?1:0;

			$sql = "SELECT * FROM [User] WHERE UserID =". $userValues['txt_userid'];
			$rs = DbConnManager::GetDb('mpower') -> Execute($sql, 'User');
			if (count($rs) == 1)
			{
				$objUser = $rs[0];
				$objUser->Initialize();
				$objUser->FirstName=$userValues['FirstName'];
				$objUser->LastName=$userValues['LastName'];
				$objUser->UserName=$userValues['UserID'];
				$objUser->Email=$userValues['Email'];
				$objUser->StartDate=$userValues['StartDate'];
				$objUser->SupervisorID=$supervisorId;
				$objUser->Level=$levelNumber;
				$objUser->IsSalesPerson=$isSalesPerson;
				if($userValues['Password'] != '' || $userValues['Password'] != null)
					$objUtil=new Util;
					$encodedPassword = $this->encryptString($userValues['Password']);
					$objUser->Password = $encodedPassword;
				$rs->Save();
			}
		}
		return $status->getStatus();
	}

	function getUserTypeByCompanyId($companyId) {
		$sql = 'SELECT LevelNumber,LevelID,Name from levels where CompanyID = ? order by levelNumber';
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		return $result;
	}

	function getAllParentUserByLevelId($companyId, $levelId) {

		$sql = "select [User].UserID,[User].FirstName,[User].Level,Levels.Name from [User]," . "Levels where [User].Level=Levels.LevelNumber" . " and [User].CompanyID=Levels.CompanyID and " . " [User].CompanyID=? and " . " Levels.LevelNumber >(" . " select Levels.LevelNumber from Levels where LevelID=?)";

		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId), array(DTYPE_INT, $levelId));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		$parentUser = array();
		if (count($result) > 0) {

			for($i = 0; $i < count($result); $i++)
				$parentUser[$result[$i]['Level']] = $result[$i]['Name'];
		}

		$parentUserData = array();
		foreach ($parentUser as $key => $value) {
			for($i = 0; $i < count($result); $i++) {
				if ($key == $result[$i]['Level']) $parentUserData[$result[$i]['Level']][] = array('USERID' => $result[$i]['UserID'], 'NAME' => $result[$i]['FirstName']);
			}
		}

		return array('STATUS' => 'OK', 'PARENT' => $parentUser, 'DATA' => $parentUserData);
	}

	function getLevelDetailsByCompanyIdAndLevelId($companyId, $levelId) {
		$sql = 'SELECT LevelNumber,Name from levels where CompanyID = ? and LevelID = ? order by levelNumber';
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId), array(DTYPE_INT, $levelId));
		$result = DbConnManager::GetDb('mpower')->GetOne($sql);
		return $result;
	}

	function encryptString($str) {
		$objUtil = new Util();
		$encryptedStr = sha1($str);
		$encodedStr = $objUtil->encode($encryptedStr);
		return $encodedStr;
	}

	function decryptString($encryptedStr) {
		$objUtil = new Util();
		$decryptedStr = $objUtil->decode($encryptedStr);
		return $decryptedStr;
	}

	function getLabels() {
		$sql = 'SELECT * from Labels order by LabelID';
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		return $result;
	}

	function getUserFullName($primaryKey = 0) {
		$sql = "SELECT [User].FirstName, [User].LastName FROM [User] WHERE UserID =" . $primaryKey;
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		if(count($result) != 0) {
			$userFullName = $result[0]['FirstName'] . ' ' . $result[0]['LastName'];
			return $userFullName;
		}
	}

}

?>
