<?php
/*
 * include the class file
 * include the DB API query file
 */
include_once BASE_PATH . '/slipstream/class.Note.php';
include_once BASE_PATH . '/slipstream/lib.php';

// Smarty Append to css_include for any css files you need included in the page
$smarty->append('css_include', array('note.css' => 1), true);

// Smarty Append to css_include for any print css files you need included in the page
$smarty->append('css_print_include', array('note_print.css' => 1), true);

$objNote = new Note();

if(trim($_REQUEST['Keyword']) != '') {
	$objNote->searchString = $_REQUEST['Keyword'];
}

$filterFields = $_REQUEST['filterOpt'];
$filterArray = array();

foreach($filterFields as $filterKey => $filterVal) {
	$newFilterKey = $filterKey + 1;

	if(($filterVal != '') && ($filterVal != 'D0') && ($filterVal != 'D1')) {
		if(isset($_REQUEST['NText'.$newFilterKey]) && (trim($_REQUEST['NText'.$newFilterKey]) != '')) {
			$filterArray[$filterVal] = $_REQUEST['NText'.$newFilterKey];
		}
	}
	else if($filterVal == 'D0') {
		$filterArray['D0_SalesPerson'] = $_REQUEST['NText'.$newFilterKey];
	}
	else if($filterVal == 'D1') {
		$filterArray['D1_StartDate'] = $_REQUEST['StartDate'.$newFilterKey];
		$filterArray['D1_EndDate'] = $_REQUEST['EndDate'.$newFilterKey];
	}
}

$objNote->notePrint = true;
$objNote->sortType = $_REQUEST['SortType'];
$objNote->sortAs = $_REQUEST['SortAs'];
$objNote->noteFilterArray = $filterArray;
$noteList = $objNote->searchNoteList();

if(isset($noteList['result']) && ($noteList['result'] == 'false')) {
	$smarty->assign('NORECORDS', $noteList['error']);
}
else {
	if(is_array($noteList) && (count($noteList) > 0)) {
		$smarty->assign('NOTELIST', $noteList);
		$smarty->assign('PAGELINKS', $pageLinks);
		$smarty->assign('PAGEINFO', $pageInfo);
		$smarty->assign('TOTREC', $$objNote->totalRecords);
		$smarty->assign('NORECORDS', '');
	}
	else {
		$smarty->assign('NORECORDS', 'No notes match the current search term and filters.');
	}
}
?>