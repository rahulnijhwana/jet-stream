<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once 'class.Status.php';

class Validation extends MpRecord
{

	public function __toString()
	{
		return 'Validation';
	}
	public function Initialize()
	{
		$this->db_table = 'Validation';
		$this->recordset->GetFieldDef('ValidationID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('ValidationName')->required = TRUE;
		parent::Initialize();
	}
	
	function getValidationIdByName($validationName){
		$sql="select ValidationID from Validation where ValidationName LIKE ? ";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_STRING, $validationName));
		$validation = DbConnManager::GetDb('mpower')->Exec($sql);
		return $validation;
	}
}



?>