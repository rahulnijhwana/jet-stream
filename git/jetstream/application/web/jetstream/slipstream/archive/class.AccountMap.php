<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once 'class.Status.php';

class AccountMap extends MpRecord
{

	public function __toString()
	{
		return 'Account Map Record';
	}

	public function Initialize()
	{
		$this->db_table = 'AccountMap';
		$this->recordset->GetFieldDef('AccountMapID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('CompanyID')->required = TRUE;
		$this->recordset->GetFieldDef('LabelName')->required = TRUE;
		$this->recordset->GetFieldDef('FieldName')->required = TRUE;
		parent::Initialize();
	}

	function insertAccountFieldsToMap($companyId, $fieldName, $labelName, $isCompany, $isMandatory, $optionType, $validationType, $align,$optionSetId)
	{
		$status = new Status();
		$sql = "SELECT * FROM AccountMap WHERE AccountMapID = -1";
		$rs = DbConnManager::GetDb('mpower')->Execute($sql);
		$this->SetDatabase(DbConnManager::GetDb('mpower'));
		$this->recordset = $rs;
		$this->Initialize();
		$this->CompanyID = $companyId;
		$this->FieldName = $fieldName;
		$this->LabelName = $labelName;
		$this->IsCompanyName = $isCompany;
		$this->IsRequired = $isMandatory;
		$this->OptionType = $optionType;
		$this->ValidationType = $validationType;
		$this->Align = $align;
		$this->OptionSetID = $optionSetId;
		$this->Enable = 0;
		$this->Save();
		return $status;
	}

	function getAccountMapIdByCompanyAndField($companyId, $fieldName)
	{
		$sql = "select AccountMapID from AccountMap where CompanyID = ? and FieldName = ?";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId), array(DTYPE_STRING, $fieldName));
		$mapId = DbConnManager::GetDb('mpower')->Exec($sql);
		return $mapId;
	}

	function updateAccountMapById($mapId, $labelName, $isRequired, $isCompany, $validationType)
	{
		$status = new Status();
		$sql = "SELECT * FROM AccountMap WHERE AccountMapID = ?";
		$accountMapSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $mapId));
		$result = DbConnManager::GetDb('mpower')->Execute($accountMapSql, 'AccountMap');
		if (count($result) == 1) {
			$objAccount = $result[0];
			$objAccount->Initialize();
			$objAccount->LabelName = $labelName;
			$objAccount->IsCompanyName = $isCompany;
			$objAccount->IsRequired = $isRequired;
			$objAccount->ValidationType = $validationType;
			$result->Save();
		}
		return $status;

	}

	function updateAlignAndPositionOfAccountFields($align, $position, $labelName, $companyId, $enable)
	{
		$status = new Status();
		$sql = "SELECT * FROM AccountMap WHERE CompanyID = ? and LabelName = ?";
		$accountMapSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyId), array(DTYPE_STRING, $labelName));
		$result = DbConnManager::GetDb('mpower')->Execute($accountMapSql, 'AccountMap');
		if (count($result) == 1) {
			$objAccount = $result[0];
			$objAccount->Initialize();
			$objAccount->Position = $position;
			$objAccount->Align = $align;
			$objAccount->Enable = $enable;
			$result->Save();
		}
		return $status;
	}

	function getAccountMapById($companyId)
	{
		$sql = 'SELECT AccountMapID,FieldName,LabelName,IsCompanyName,BasicSearch,IsRequired,ValidationType,Position,Align FROM AccountMap WHERE CompanyID= ?';
		$accountSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $companyId));
		$accountMap = DbConnManager::GetDb('mpower')->Exec($accountSql);

		$_SESSION['USER']['ACCOUNTMAP'] = $accountMap;
		//return $accountMap;
	}

	//Fetch different field values and validationName from AccountMap and Validation table for company
	function getFieldsFromAccountMapByCompId($companyId,$criteria)
	{
		$searchCriteria = $criteria.'%';
		$sql="SELECT AccountMapID,FieldName,LabelName,IsRequired,IsCompanyName,OptionType,ValidationType,Align,Enable,OptionSetID,SectionID,Position FROM AccountMap where CompanyID= ? and FieldName Like ? Order By Position DESC";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId),array(DTYPE_STRING, $searchCriteria));
		$fieldNames = DbConnManager::GetDb('mpower')->Exec($sql);
		for($i=0;$i<count($fieldNames);$i++){
			$validationType = $fieldNames[$i]['ValidationType'];

			if($validationType  != '' || $validationType !=NULL){

				$sql="SELECT ValidationName FROM Validation where ValidationID= ? ";
				$sqlBuild = SqlBuilder()->LoadSql($sql);
				$sql = $sqlBuild->BuildSql(array(DTYPE_STRING, $validationType));
				$validationName = DbConnManager::GetDb('mpower')->Exec($sql);
				$fieldNames[$i]['ValidationName'] = $validationName[0]['ValidationName'];
			}else{
				$fieldNames[$i]['ValidationName'] = '-1';
			}
		}
		return $fieldNames;
	}

	function updateFieldPositionByAccountMapId($align,$position,$sectionId,$accountMapId)
	{
		$status = new Status();
		$sql = "SELECT * FROM AccountMap WHERE AccountMapID = ?";
		$accountMapSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $accountMapId));
		$result = DbConnManager::GetDb('mpower')->Execute($accountMapSql, 'AccountMap');
		if (count($result) == 1) {
			$objAccount = $result[0];
			$objAccount->Initialize();
			$objAccount->Align = $align;
			$objAccount->Position = $position;
			$objAccount->SectionID = $sectionId;
			$result->Save();
		}
		return $status;
	}
}

?>