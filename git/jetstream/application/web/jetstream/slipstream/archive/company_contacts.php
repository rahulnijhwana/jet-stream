<?php
/**
 * include the class file
 * include the DB API query file
 * include the country list
 */
include_once BASE_PATH . '/include/class.fieldsMap.php';
include_once BASE_PATH . '/slipstream/class.Company.php';
include_once BASE_PATH . '/slipstream/lib.php';

/**
 * create fieldMap() class object
 */
$objFieldMap = new fieldsMap();

/**
 * set all contacts details by accountid
 * include the tpl file according to accountid
 */
if (isset($_GET['accountId'])) {
	$smarty->assign('ACCOUNTID', $_GET['accountId']);
	
	$contactLables = 'ContactID,';
	$contactMap = $objFieldMap->getAccountFields();
	
	foreach ($contactMap as $map) {
		$contactLables .= $map['FieldName'] . ",";
	}
	
	/**
	 * getting contact information by accountid
	 */
	$fields = substr($contactLables, 0, -1);
	$sql = 'SELECT ? FROM Contact WHERE AccountID= ?';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, -1), array(DTYPE_INT, $_GET['accountId']));
	$sql = str_replace('-1', $fields, $sql);
	$contactInfo = DbConnManager::GetDb('mpower')->Exec($sql);
	
	$contactDetails = array();

	for($i = 0; $i < count($contactInfo); $i++) {
		$ContactID = $contactInfo[$i]['ContactID'];
		$FirstName = getContactFirstName($ContactID);
		$LastName = getContactLastName($ContactID);
		$contactDetails[] = array('ContactId' => $ContactID, 'FirstName' => $FirstName, 'LastName' => $LastName);
	}
	
	$smarty->assign('CONTACTS', $contactDetails);
		
	$smarty->append('modules', array('Contacts' => array('template' => 'company_contacts.tpl', 'buttons' => array(array('tooltip' => 'Add', 'image' => 'add20.png', 'dest' => 'javascript:addContact('.$_GET['accountId'].');')))), true);

} else {
	$smarty->append('modules', array('Contacts' => array('template' => 'company_contacts.tpl', 'buttons' => array(array('tooltip' => 'Add', 'image' => 'add20.png', 'dest' => '', 'div' => 'addContactImage')))), true);
}
?>