<?php

/**
 * class.Company.php - contain Company class
 * @package database
 * include DB API classes
 * include Util class for validation
 * include Status class for status return
 */
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/include/class.RecContact.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

/**
 * Company class declaration
 * return the status OK or FALSE
 * declare createCompany() function
 * @param $newData - set new datas for insert
 * @param $sessLabels - set lables for company
 * @param $type - define the type of save insert or update
 */

class Contact
{
	var $contact_rec;
	var $contact_map;

	public function ProcessForm() {
		if (isset($_POST['ContactID'])) {
			$this->Load($_POST['ContactID']);
		} else {
			$this->contact_rec = new RecContact();
			$rs_sql = "SELECT * FROM Contact WHERE 0 = 1";
			$rs = DbConnManager::GetDb('mpower')->Execute($rs_sql);
			$rs[] = $this->contact_rec;
			$this->contact_rec->SetDatabase(DbConnManager::GetDb('mpower'));
			$this->contact_rec['CompanyID'] = $_SESSION['company_obj']['CompanyID'];
		}
		
		$this->contact_rec->Initialize();
		
		foreach ($_POST as $field => $value) {
			$this->contact_rec[$field] = $value;
		}
		$this->contact_rec->Save();
	}

	public function GetName() {
		return ($this->contact_rec['Text01']);
	}

	public function Load($value, $type = "id") {
		$param[] = array(DTYPE_INT, $value);
		switch ($type) {
			case "id" :
				$contact_sql = "SELECT * FROM Contact WHERE ContactID = ?";
		}
		$account_sql = SqlBuilder()->LoadSql($contact_sql)->BuildSqlParam($param);
		$this->contact_rec = DbConnManager::GetDb('mpower')->GetOne($account_sql, 'RecContact');
	
	}

	public function DrawView($form = false) {
		if (empty($this->contact_map)) $this->contact_map = SessionManager::GetMapArray('CONTACTMAP');
		
		$output = '';
		
		if ($form && isset($this->contact_rec)) {
			$output .= '<input type="hidden" name="ContactID" value="' . $this->contact_rec['ContactID'] . '" />';
		}
		
		$output .= '<table class="jet_datagrid">';
		foreach ($this->contact_map as $position => $row) {
			if (!$form && $position == 0) continue;
			$output .= '<tr>';
			for($x = 1; $x < 3; $x++) {
				$output .= '<td class="label">' . ((isset($row[$x])) ? $row[$x]['LabelName'] . ':' : '&nbsp;') . '</td>';
				
				$output .= '<td class="' . (($x == 1) ? 'left' : 'right') . '">';
				if (isset($row[$x])) {
					if ($form) {
						$output .= $this->DrawFormField($row[$x]);
					} else {
						$output .= (substr($row[$x]['FieldName'], 0, 6) == 'Select') ? $this->GetOptionVal($this->contact_rec[$row[$x]['FieldName']]) : $this->contact_rec[$row[$x]['FieldName']];
					}
				} else {
					$output .= '&nbsp;';
				}
				$output .= '</td>';
			}
			$output .= '</tr>';
		}
		$output .= '</table>';
		return $output;
	}

	private function GetOptionList($map_id) {
		$option_sql = SqlBuilder()->LoadSql("SELECT * FROM [Option] WHERE ContactMapID = ?")->BuildSql(array(DTYPE_INT, $map_id));
		$option_list = DbConnManager::GetDb('mpower')->Exec($option_sql);
		return $option_list;
	}

	private function GetOptionVal($option) {
		$option_sql = SqlBuilder()->LoadSql("SELECT * FROM [Option] WHERE OptionID = ?")->BuildSql(array(DTYPE_INT, $option));
		$option_rec = DbConnManager::GetDb('mpower')->GetOne($option_sql);
		return $option_rec->OptionName;
	}

	private function DrawFormField($field) {
		$output = '';
		if (substr($field['FieldName'], 0, 4) == 'Date') {
			$output .= '<input type="text" class="datepicker" size="30" name="' . $field['FieldName'] . '" value="' . $this->contact_rec[$field['FieldName']] . '"/>';
		} elseif (substr($field['FieldName'], 0, 6) == 'Select') {
			$output .= '<select name="' . $field['FieldName'] . '"><option></option>';
			foreach ($this->GetOptionList($field['ContactMapID']) as $option) {
				$selected = ($option['OptionID'] == $this->contact_rec[$field['FieldName']]) ? 'selected' : '';
				$output .= "<option value='" . $option['OptionID'] . "' " . $selected . ">" . $option['OptionName'] . "</option>";
			}
			$output .= '</select>';
		} else {
			$output .= '<input type="text" size="30" name="' . $field['FieldName'] . '" value="' . $this->contact_rec[$field['FieldName']] . '"/>';
		}
		
		$output .= '<span class="clsError" id="spn_' . $field['FieldName'] . '"></span><br>';
		return $output;
	}
}








///**
// * class.Contact.php - contain Company class
// * @package database
// * include DB API classes
// * include Util class for validation
// * include Status class for status return
// */
//require_once BASE_PATH . '/include/class.DbConnManager.php';
//require_once BASE_PATH . '/include/class.MpRecord.php';
//require_once BASE_PATH . '/slipstream/class.Status.php';
//require_once BASE_PATH . '/slipstream/class.Util.php';
//
///**
// * Contact class declaration
// * return the status OK or FALSE
// * declare InsertContactInfo() function
// * @param $newData - set new datas for insert
// * @param $sessLabels - set lables for company
// * @param $type - define the type of save insert or update
// */
//class Contact extends MpRecord
//{
//
//	public function __toString() {
//		return 'Contact Record';
//	}
//
//	public function Initialize() {
//		$this->db_table = 'Contact';
//		$this->recordset->GetFieldDef('ContactID')->auto_key = TRUE;
//		$this->recordset->GetFieldDef('AccountID')->required = TRUE;
//		parent::Initialize();
//	}
//	
//	function InsertContactInfo($newData, $sessLables, $type) {
//		try {
//			$status = new Status();
//			if($type == 'update') {
//				$sql = "SELECT * FROM Contact WHERE ContactID =" . $_POST['editContactId'];
//				$rs = DbConnManager::GetDb('mpower')->Execute($sql, 'Contact');
//				if (count($rs) == 1) {
//					$objContact = $rs[0];
//					$objContact->Initialize();
//					
//					$saveData = '';
//					foreach ($sessLables as $map) {
//						$a = $map['LabelName'];
//						$label = str_replace(' ', '_', $a);
//						$objContact->$map['FieldName'] = $newData['LB' . $label];	
//					}
//
//					$rs->Save();
//					$status->setFormattedVar('UPDATE_'.$newData['editContactId']);
//				}
//			} elseif($type == 'insert') {
//				$sql = "SELECT * FROM Contact WHERE 0 = 1";
//				$result = DbConnManager::GetDb('mpower')->Execute($sql, 'Contact');
//
//				$contact_info = new Contact();
//				$contact_info->SetDatabase(DbConnManager::GetDb('mpower'));
//				$result[] = $contact_info;
//				$result->Initialize();
//				$contact_info->AccountID = $newData['AccountID'];
//				foreach ($sessLables as $map) {
//					$lableName = $map['LabelName'];
//					$label = str_replace(' ', '_', $lableName);
//
//					$contact_info->$map['FieldName'] = $newData[$label];
//				}
//				$result->Save();
//				$contactID = $contact_info->ContactID;
//				$status->setFormattedVar('INSERT_' . $contactID);
//			}
//		} catch (Exception $e) {
//			$status->addError($e->getMessage());
//		}
//		return $status;
//	}
//}
?>
