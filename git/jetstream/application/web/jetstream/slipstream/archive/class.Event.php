<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.RecAccount.php';
require_once BASE_PATH . '/slipstream/class.Status.php';
require_once BASE_PATH . '/slipstream/class.Util.php';
require_once BASE_PATH . '/slipstream/class.EventInfo.php';
require_once BASE_PATH . '/include/class.fieldsMap.php';
require_once BASE_PATH . '/slipstream/lib.php';

/**
 * The DB fields initialization
 */
class Event
{
	
	public $eventId;
	public $contactId;
	public $userId;	
	public $eventSubject;
	public $eventTypeId;	
	public $eventStartDate;
	public $eventStartTime;
	public $eventDur;
	public $eventText;
	public $eventAction;
	
	/**
	 * Constructor of the Note class.
	 */
	function Event() {
		/* Set the EventId to 0. */
		$this->eventId = 0;
	}
	
	/**
	 * function saveEvent
	 */
	function saveEvent() {		
		try {
			/**
		   * create Status() class object
		   */
			$status = new Status();
			
			/**
		   * Validates the event fields.
		   */
			$status = $this->doValidateEvent();
			
			/**
		   * If the stauts is Ok then save the event data to the db.
		   * Else return the error.
		   */		 	
			if ($status->status == 'OK') {
				$sql = "SELECT * FROM Events WHERE EventID =?";
				$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->eventId));
				$result = DbConnManager::GetDb('mpower')->Execute($sql, 'EventInfo');				
				//echo $sql;
				$event_info = new EventInfo();				
				
				if($this->eventAction == 'add') {
					$event_info->SetDatabase(DbConnManager::GetDb('mpower'));
					$result[] = $event_info;
				} else {
					$event_info = $result[0];
				}				
				$result->Initialize();
								
				if($this->eventAction == 'add') {
					$event_info->ContactID = $this->contactId;
					$event_info->UserID = $this->userId;
					$event_info->DateEntered = TimestampToMsSql(time());
				}
				else {
					$event_info->DateModified = TimestampToMsSql(time());
					$event_info->ContactID = $this->contactId;
					$event_info->UserID = $this->userId;
				}
				
				$event_info->Subject = $this->eventSubject;
				$event_info->Description = $this->eventText;
				$event_info->EventTypeID = $this->eventTypeId;
				
				$startDate = foramtDateTime($this->eventStartDate, $this->eventStartTime, '', 1);				
				$event_info->StartDate = $startDate;
				
				if($this->eventDur != '') {
					$durArr = explode(':', $this->eventDur);
					$event_info->DurationHours = $durArr[0];
					if($durArr[1] > 0) {
						$event_info->DurationMinutes = $durArr[1];
					}					
				}				
				$result->Save();
				$EventID = $event_info->EventID;
				
				$status->setFormattedVar($this->eventId);
			}
		} catch (Exception $e) {
			$status->addError($e->getMessage());
		}
		return $status->getStatus();	
	}
	

	/**
	 * function doValidateEvent	
	 * Validates the event fields.
	 */
	function doValidateEvent() {
		/**
	   * create Status() class object
	   * create Util() class object
	   */
		$status = new Status();
		$objUtil = new Util();
		
		/**
		 * Event subject is a required field.
		 */
		if (!isset($this->eventSubject) || trim($this->eventSubject) == "") {
			$status->addError('Enter Subject', 'Subject');
		} elseif (!$objUtil->isAlphaNumeric($this->eventSubject, array("\'", "#", "&", " "))) {
			$status->addError('Invalid charcters for Subject', 'Subject');
		}
		
		/**
		 * Event type is a required field.
		 */
		if (!isset($this->eventTypeId) || trim($this->eventTypeId) == "") {
			$status->addError('Select a Type', 'Type');
		}
		
		/**
		 * Event Start date is a required field.
		 * Start date should be equal to or greater than the current date.
		 */
		if (!isset($this->eventStartDate) || trim($this->eventStartDate) == "") {
			$status->addError('Enter the Start Date', 'StartDate');
		}
		elseif (!$objUtil->isValidDate($this->eventStartDate, '/')) {
			$status->addError('Invalid charcters', 'StartDate');
		} 
		else {
			$enteredStDate = strtotime(foramtDateTime($this->eventStartDate, '', 'date'));
			$curDate = strtotime(date('Y-m-d'));
			
		 	if($enteredStDate < $curDate) {
				$status->addError('Enter upcoming date', 'StartDate');
			}
		}
		
		/**
		 * Event Start time is a required field.
		 */
		if (!isset($this->eventStartTime) || trim($this->eventStartTime) == "") {
			$status->addError('Enter the Start Time', 'ClockPick');
		}
		
		/**
		 * Event Duration is a required field.
		 */
		if (!isset($this->eventDur) || trim($this->eventDur) == "") {
			$status->addError('Enter the Duration', 'ClockDuration');
		}
		
		/*if (isset($this->eventStartDate) && isset($this->eventStartTime) 
			&& ($this->eventStartDate != "") && (trim($this->eventStartTime) != "")) {
			$postDateTime = foramtDateTime($this->eventStartDate, $this->eventStartTime, '');
			
			/**
			 * check events exist or not by same time
			 */
			/*$sql = 'SELECT * FROM Events WHERE UserID=? 
				AND ? between Events.StartDate and DateAdd([minute],DurationMinutes,DateAdd([hour],DurationHours,StartDate)) 
				AND DateAdd([minute],DurationMinutes,DateAdd([hour],DurationHours,StartDate)) != ? AND Events.StartDate != ?';
			$sqlBuild = SqlBuilder()->LoadSql($sql);
			$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $_SESSION['USER']['USERID']), array(DTYPE_STRING, $postDateTime), array(DTYPE_STRING, $postDateTime), array(DTYPE_STRING, $postDateTime));
			echo $sql.'<br>';
			$events = DbConnManager::GetDb('mpower')->Exec($sql);
						
			$noEvents = count($events);
			if ($noEvents > 0) {
				$status->addError('Event present on this time', 'ClockPick');
			}*/
		//}
		return $status;
	
	}
	
	
	/**
	 * get event type information by companyId
	 */
	function getEventTypeByCompanyId($companyId = '0') {
		$sql = 'select EventTypeID, EventName from EventType where CompanyID=?';
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId));
		$eventType = DbConnManager::GetDb('mpower')->Exec($sql);
		
		$arrEvent = array();
		
		foreach ($eventType as $key => $val) {
			$keyID = $val['EventTypeID'];
			$arrEvent[$keyID] = $val['EventName'];
		}
		return $arrEvent;
	}
	
	/**
	 * get particular event details by eventId
	 */
	function getEventInfo() {
		if((!$this->eventId > 0) || !(is_numeric($this->eventId))) {
			return false;
		}
		$fieldMapObj = new fieldsMap();			
		$contactMapFName = $fieldMapObj->getContactFirstNameField();
		$contactMapLName = $fieldMapObj->getContactLastNameField();
			
		$sql = 'SELECT E.EventID, E.ContactID, E.UserID, E.EventTypeID, E.Subject, E.Description, E.DateEntered, E.DateModified,
			E.DurationHours, E.DurationMinutes, CONVERT(VARCHAR(10), E.StartDate, 101) as StartDate,CONVERT(varchar, E.StartDate, 8) as StartTime,
			ET.EventName, ET.EventColor , C.'.$contactMapFName.' AS FirstName, C.'.$contactMapLName.' AS LastName
			FROM Events AS E
			INNER JOIN EventType AS ET ON E.EventTypeID = ET.EventTypeID
			LEFT JOIN Contact AS C ON E.ContactID = C.ContactID
			WHERE E.EventID=?';
			
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $this->eventId));
		//echo $sql;
		$eventInfo = DbConnManager::GetDb('mpower')->Exec($sql);
	
		$eventSTime = explode(":", $eventInfo[0]['StartTime']);
		if (isset($eventSTime[0])) {
			if ($eventSTime[0] < 12) {
				$hType = "AM";
			} elseif ($eventSTime[0] >= 12) {
				$hType = "PM";
			}
			if ($eventSTime[0] > 12) {
				$hHour = ($eventSTime[0] - 12);
			} elseif ($eventSTime[0] == '00') {
				$hHour = 12;
			} else {
				$hHour = $eventSTime[0];
			}
		}
		if (isset($eventSTime[1])) {
			$hMinute = $eventSTime[1];
		}
		$duration = '';
		if($eventInfo[0]['DurationHours'] > 0) {
			$duration .= $eventInfo[0]['DurationHours'];
		}
		if($eventInfo[0]['DurationMinutes'] > 0) {
			$duration .= ':'.$eventInfo[0]['DurationMinutes'];
		}
		
		$startTime = $hHour.':'.$hMinute.' '.$hType;
		$eventDetails = array('EventID' => $eventInfo[0]['EventID'], 'Subject' => $eventInfo[0]['Subject'],
			'Type' => $eventInfo[0]['EventName'], 'StartDate' => $eventInfo[0]['StartDate'], 'StartTime' => $startTime, 
			'Duration' => $duration, 'DurHours' => $eventInfo[0]['DurationHours'], 'DurMinutes' => $eventInfo[0]['DurationMinutes'],
			'Notes' => $eventInfo[0]['Description'], 'ContactID' => $eventInfo[0]['ContactID'], 
			'EventType' => $eventInfo[0]['EventTypeID'], 'FirstName' => $eventInfo[0]['FirstName'], 'LastName' => $eventInfo[0]['LastName']);
		
		return $eventDetails;
	}
	
	/**
	 * get all upcoming events by accountId or contactId
	 */
	function getUpcomingEvents($companyId = 0, $contactId = 0, $accountId = 0, $today, $opt = '') {
		$fieldMapObj = new fieldsMap();			
		$contactMapFName = $fieldMapObj->getContactFirstNameField();
		$contactMapLName = $fieldMapObj->getContactLastNameField();
		
		$sql = 'SELECT E.EventID, E.ContactID, E.UserID, E.EventTypeID, E.Subject, E.Description, E.DateEntered, E.DateModified, 
			E.DurationHours, E.DurationMinutes, CONVERT(VARCHAR(10), E.StartDate, 101) as StartDate, ET.EventName, ET.EventColor 
			, C.'.$contactMapFName.' AS FirstName, C.'.$contactMapLName.' AS LastName
			FROM Events AS E
			INNER JOIN EventType AS ET ON E.EventTypeID = ET.EventTypeID 
			LEFT JOIN Contact AS C ON E.ContactID = C.ContactID';
			
		if(trim($opt) == 'current') {
			
			$sql .= ' WHERE ET.CompanyID=? AND E.UserID=? AND CONVERT(VARCHAR(10), E.StartDate, 101) = ? ORDER BY E.StartDate';			
			$sqlBuild = SqlBuilder()->LoadSql($sql);
			$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']), array(DTYPE_INT, $_SESSION['USER']['USERID']), array(DTYPE_STRING, $today));			
		}
		else if($accountId == 0) {
			$sql .= '	WHERE ET.CompanyID=? AND E.ContactID=? AND CONVERT(VARCHAR(10), E.StartDate, 120) >= ? ORDER BY E.StartDate';
			$sqlBuild = SqlBuilder()->LoadSql($sql);
			$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId), array(DTYPE_INT, $contactId), array(DTYPE_STRING, $today));
		}
		else {			
			$sql .= ' WHERE ET.CompanyID = ? AND C.AccountID = ? AND CONVERT(VARCHAR(10), E.StartDate, 120) >= ? ORDER BY E.StartDate';
			$sqlBuild = SqlBuilder()->LoadSql($sql);
			$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId), array(DTYPE_INT, $accountId), array(DTYPE_STRING, $today));
		}
		//echo $sql;
		$events = DbConnManager::GetDb('mpower')->Exec($sql);
		return $events;
	}

}

?>
