<?php
/**
 * class.Company.php - contain Company class
 * @package database
 * include DB API classes
 * include Util class for validation
 * include Status class for status return
 */
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/include/class.RecAccount.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

/**
 * Company class declaration
 * return the status OK or FALSE
 * declare createCompany() function
 * @param $newData - set new datas for insert
 * @param $sessLabels - set lables for company
 * @param $type - define the type of save insert or update
 */

class Company
{
	var $account_rec;
	var $account_map;

	public function ProcessForm() {
		if (isset($_POST['AccountID'])) {
			$this->Load($_POST['AccountID']);
		} else {
			$this->account_rec = new RecAccount();
			$rs_sql = "SELECT * FROM Account WHERE 0 = 1";
			$rs = DbConnManager::GetDb('mpower')->Execute($rs_sql);
			$rs[] = $this->account_rec;
			$this->account_rec->SetDatabase(DbConnManager::GetDb('mpower'));
			$this->account_rec['CompanyID'] = $_SESSION['company_obj']['CompanyID'];
		}
		
		$this->account_rec->Initialize();
		
		foreach ($_POST as $field => $value) {
			$this->account_rec[$field] = $value;
		}
		$this->account_rec->Save();
	}

	public function GetName() {
		return ($this->account_rec['Text01']);
	}

	public function Load($value, $type = "id") {
		$param[] = array(DTYPE_INT, $value);
		switch ($type) {
			case "id" :
				$account_sql = "SELECT * FROM Account WHERE AccountID = ?";
		}
		$account_sql = SqlBuilder()->LoadSql($account_sql)->BuildSqlParam($param);
		$this->account_rec = DbConnManager::GetDb('mpower')->GetOne($account_sql, 'RecAccount');
	
	}

	public function DrawView($form = false) {
		if (empty($this->account_map)) $this->account_map = SessionManager::GetMapArray('ACCOUNTMAP');
		
		$output = '';
		
		if ($form && isset($this->account_rec)) {
			$output .= '<input type="hidden" name="AccountID" value="' . $this->account_rec['AccountID'] . '" />';
		}
		
		$output .= '<table class="jet_datagrid">';
		foreach ($this->account_map as $position => $row) {
			if (!$form && $position == 0) continue;
			$output .= '<tr>';
			for($x = 1; $x < 3; $x++) {
				$output .= '<td class="label">' . ((isset($row[$x])) ? $row[$x]['LabelName'] . ':' : '&nbsp;') . '</td>';
				
				$output .= '<td class="' . (($x == 1) ? 'left' : 'right') . '">';
				if ($form) {
					$output .= (isset($row[$x])) ? $this->DrawFormField($row[$x]) : '&nbsp;';
				} else {
					$output .= (substr($row[$x]['FieldName'], 0, 6) == 'Select') ? $this->GetOptionVal($this->account_rec[$row[$x]['FieldName']]) : $this->account_rec[$row[$x]['FieldName']];
				}
				$output .= '</td>';
			}
			$output .= '</tr>';
		}
		$output .= '</table>';
		return $output;
	}

	private function GetOptionList($account_map_id) {
		$option_sql = SqlBuilder()->LoadSql("SELECT * FROM [Option] WHERE AccountMapID = ?")->BuildSql(array(DTYPE_INT, $account_map_id));
		$option_list = DbConnManager::GetDb('mpower')->Exec($option_sql);
		return $option_list;
	}

	private function GetOptionVal($option) {
		$option_sql = SqlBuilder()->LoadSql("SELECT * FROM [Option] WHERE OptionID = ?")->BuildSql(array(DTYPE_INT, $option));
		$option_rec = DbConnManager::GetDb('mpower')->GetOne($option_sql);
		return $option_rec->OptionName;
	}

	private function DrawFormField($field) {
		$output = '';
		if (substr($field['FieldName'], 0, 4) == 'Date') {
			$output .= '<input type="text" class="datepicker" size="30" name="' . $field['FieldName'] . '" value="' . $this->account_rec[$field['FieldName']] . '"/>';
		} elseif (substr($field['FieldName'], 0, 6) == 'Select') {
			$output .= '<select name="' . $field['FieldName'] . '"><option></option>';
			foreach ($this->GetOptionList($field['AccountMapID']) as $option) {
				$selected = ($option['OptionID'] == $this->account_rec[$field['FieldName']]) ? 'selected' : '';
				$output .= "<option value='" . $option['OptionID'] . "' " . $selected . ">" . $option['OptionName'] . "</option>";
			}
			$output .= '</select>';
		} else {
			$output .= '<input type="text" size="30" name="' . $field['FieldName'] . '" value="' . $this->account_rec[$field['FieldName']] . '"/>';
		}
		
		$output .= '<span class="clsError" id="spn_' . $field['FieldName'] . '"></span><br>';
		return $output;
	}
}

?>