<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once 'class.Status.php';

class Section extends MpRecord
{

	public function __toString()
	{
		return 'Section';
	}
	public function Initialize()
	{
		$this->db_table = 'Section';
		$this->recordset->GetFieldDef('SectionID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('CompanyID')->required = TRUE;
		parent::Initialize();
	}

	function saveSection($companyId,$accountSectionName,$contactSectionName) {
		$status = new Status();
		$sql = "SELECT * FROM Section WHERE SectionID = -1";
		$rs = DbConnManager::GetDb('mpower')->Execute($sql);
		$this->SetDatabase(DbConnManager::GetDb('mpower'));
		$this->recordset = $rs;
		$this->Initialize();
		$this->CompanyID = $companyId;
		$this->AccountSectionName = $accountSectionName;
		$this->ContactSectionName = $contactSectionName;
		$this->Save();
		return $status;
	}

	function getAccountSectionByCompanyId($companyId){
		$sql = "select SectionID,AccountSectionName from Section where CompanyID = ? and AccountSectionName is not NULL";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId));
		$sections = DbConnManager::GetDb('mpower')->Exec($sql);
		return $sections;
	}

	function getContactSectionByCompanyId($companyId){
		$sql = "select SectionID,ContactSectionName from Section where CompanyID = ? and ContactSectionName is not NULL";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId));
		$sections = DbConnManager::GetDb('mpower')->Exec($sql);
		return $sections;
	}
}



?>