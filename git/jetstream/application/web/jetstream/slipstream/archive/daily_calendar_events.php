<?php
/**
 * include the class file
 * include the DB API query file
 * include the country list
 */
include_once BASE_PATH . '/slipstream/class.Calendar.php';
include_once BASE_PATH . '/slipstream/lib.php';

/**
 * inlcude css files
 */
$smarty->append('css_include', array('jquery.tooltip.css' => 1), true);

/**
 * inlcude javascript files
 */
$smarty->append('js_include', array('jquery.js' => 1, 'daily_calendar.js' => 1, 'json.js' => 1, 'jquery.tooltip.js' => 1), true);

/**
 * include tpl file for HTML display section
 */
$smarty->append('content', array('Daily Calendar' => 'daily_calendar_events.tpl'), true);

/**
 * set the DATETITLE according to the date pass on the page
 */
if (isset($_GET['date']) && $_GET['date'] != "") {
	$date = $_GET['date'];
	$pDate = date("n/j/Y", strtotime($date));
	$smarty->assign('PAGEDATE', $date);
	$dateFormat = date("l, F j, Y", strtotime($date));
	$smarty->assign('DATETITLE', $dateFormat);
} else {
	$date = date("d-m-Y");
	$pDate = date("n/j/Y", strtotime($date));
	$smarty->assign('PAGEDATE', $date);
	$dateFormat = date("l, F j, Y", strtotime($date));
	$smarty->assign('DATETITLE', $dateFormat);
}

$startDate = date("m/d/Y", strtotime($date));
$smarty->assign('STARTDATE', $startDate);

/**
 * get the events by the start date
 * making the hours array to displaying hours in daily calendar page
 */
$events = getEventsInfoByStartDate($_SESSION['USER']['USERID'], $_SESSION['USER']['COMPANYID'], $startDate);
$arrLength = count($events);
if ($arrLength != 0) {
	$lastKey = ($arrLength - 1);
	
	for($i = 0; $i < $arrLength; $i++) {
		$splitStartTime = explode(":", $events[$i]['StartTime']);
		$hour = $splitStartTime[0];
		if ($hour == "00") {
			$newhour = 8;
		} elseif ($hour != "00") {
			$newsplitStartTime = explode(":", $events[$i]['StartTime']);
			$nhour = $newsplitStartTime[0];
			if ($nhour < 10) {
				$newhour = substr($nhour, -1);
			} else {
				$newhour = $nhour;
			}
			break;
		}
	}
	$lastsplitStartTime = explode(":", $events[$lastKey]['StartTime']);
	$lhour = $lastsplitStartTime[0];
	if ($lhour > 12) {
		$lastHour = ($lhour - 12);
		if ($lastHour < 8) {
			$lastHour = 8;
		}
	} elseif ($lhour < 12) {
		$lastHour = 8;
	} elseif ($lhour == 12) {
		$lastHour = $lhour;
	}
} else {
	$newhour = 8;
	$lastHour = 8;
}

/**
 * define an array for Hour count and Minute section
 */
$hrAM = array();
for($i = $newhour; $i <= 12; $i++) {
	if ($i < 10) {
		$hrAM[] = "0" . $i . "AM";
	} else {
		$hrAM[] = $i . "AM";
	}
}
$hrPM = array();
for($i = 1; $i <= $lastHour; $i++) {
	if ($i < 10) {
		$hrPM[] = "0" . $i . "PM";
	} else {
		$hrPM[] = $i . "PM";
	}
}
$hr = array_merge($hrAM, $hrPM);
$smarty->assign('HOURCOUNT', $hr);

$min = array();
for($j = 15; $j <= 45; $j += 15) {
	$min[] = $j;
}
$smarty->assign('GETMIN', $min);
?>