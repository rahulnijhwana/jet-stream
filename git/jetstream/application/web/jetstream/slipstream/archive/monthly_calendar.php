<?php
// This main file brings together all the modules for the page and does the final display of
// the shell - it also assigns titles.
// The order that content files and modules are appended determine their order on the page - 
// first on top and so forth.


// Title is appended to Slipstream name and is not required
$smarty->assign('page_title', 'Monthly Calendar');

// Example of how to set the content title - used it to display company or contact name, for instance
// Get the content title
// Set the month name as the title
if (isset($_GET['month']) != '' && isset($_GET['year']) != '') {
	$pMonth = $_GET['month'];
	$pYear = $_GET['year'];
	$dateTitle = date("F Y", strtotime('1-' . $pMonth . '-' . $pYear));
} else {
	$dateTitle = date("F Y");
}

$title = "<div id='tblHeaderTitle'><a href=# class=lrgBlackBold onclick='javascript:getEvent(\"prev\")'>&lt;</a>&nbsp;" . $dateTitle . "&nbsp;<a href=# class=lrgBlackBold onclick='javascript:getEvent(\"next\")'>&gt;</a></div>";
$smarty->assign('content_title', $title);

// Just perform includes of the content and modules - see search_test.php for more info on
// how to build the content/module
include BASE_PATH . '/slipstream/monthly_calendar_events.php';

// The last thing you do is include shell.php - see shell.php for further explanation and examples
include BASE_PATH . '/slipstream/shell.php';

?>