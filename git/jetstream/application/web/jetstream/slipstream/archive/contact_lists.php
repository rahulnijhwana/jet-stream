<?php
/**
 * include the class file
 * include the DB API query file
 * include the country list
 */
include_once BASE_PATH . '/include/class.fieldsMap.php';
include_once BASE_PATH . '/slipstream/class.Company.php';
include_once BASE_PATH . '/slipstream/lib.php';
// include_once BASE_PATH . '/include/country_list.php';

/**
 * include tpl file for HTML section
 */
$smarty->append('modules', array('Related Contacts' => array('template' => 'contact_lists.tpl', 'buttons' => array(array('tooltip' => 'Add', 'image' => 'add20.png', 'dest' => 'javascript:addContact()')))), true);

/**
 * create fieldMap() class object
 */
$objFieldMap = new fieldsMap();


/**
 * get fields for contacts
 */
$contactLables = 'ContactID,';
$contactMap = $objFieldMap->getContactFields();

foreach ($contactMap as $map) {
	$contactLables .= $map['FieldName'] . ",";
}
$fields = substr($contactLables, 0, -1);

/**
 * get contact id
 * assign contacts getting by contact id
 */
if (isset($_GET['contactId']) && $_GET['contactId'] != "") {

	$sql = 'SELECT ? FROM Contact WHERE AccountID=(SELECT AccountID FROM Contact WHERE ContactID=?)';
	$sqlBuild = SqlBuilder()->LoadSql($sql);
	$sql = $sqlBuild->BuildSql(array(DTYPE_INT, -1), array(DTYPE_INT, $_GET['contactId']));
	$sql = str_replace('-1', $fields, $sql);
	$contactInfo = DbConnManager::GetDb('mpower')->Exec($sql);

	$contactDetails = array();

	for($i = 0; $i < count($contactInfo); $i++) {
		$ContactID = $contactInfo[$i]['ContactID'];
		$FirstName = getContactFirstName($ContactID);
		$LastName = getContactLastName($ContactID);
		$contactDetails[] = array('ContactId' => $ContactID, 'FirstName' => $FirstName, 'LastName' => $LastName);
	}
	$smarty->assign('CONTACTS', $contactDetails);
}
?>