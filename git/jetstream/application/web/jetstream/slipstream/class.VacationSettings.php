<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once 'class.Status.php';

class VacationSettings
{
	public function GetUserVacationSettings($company_id, $person_id) {
		$sql = "SELECT FirstName, LastName, OnVacation, CONVERT(VARCHAR(10), VacationReturn, 101) AS VacationReturn, PersonID, SupervisorID FROM People WHERE CompanyID = ? AND SupervisorID not in (-3, -1) AND Level <= (SELECT Level FROM People WHERE PersonID = ?) AND Deleted = 0 ORDER BY LastName";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_id), array(DTYPE_INT, $person_id));
		echo $sql . "<br>";
		$results = DbConnManager::GetDb('mpower')->Exec($sql);
//		echo print_r($results);
		$output = array();
		foreach ($results as $result) {
			$output[$result['PersonID']] = $result;
		}
		return $output;
	}
	public function GetSingleUserVacPerms($person_id) {
//		$sql = "SELECT PersonID, OnVacation, VacationReturn from People WHERE PersonID = ?";
//		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $person_id));
		$sql = "SELECT PersonID, OnVacation, VacationReturn from People WHERE CompanyID = 508";
//echo $sql;
		$results = DbConnManager::GetDb('mpower')->Exec($sql);
		$output = array();
		foreach ($results as $result) {
			$output[$result['PersonID']] = $result;
		}
		return $output;
	}
	
	public function SaveUserVacationSettings($personID, $onVacation, $vacationReturn) {
//echo $personID."<br>";
//echo $onVacation."<br>";
//echo $vacationReturn."<br>";

		$personIDArray = explode(',', $personID);
		$onVacationArray = explode(',', $onVacation);
		$vacationReturnArray = explode(',', $vacationReturn);

		$this->updateVacationSettings($personIDArray, $onVacationArray, $vacationReturnArray);
	}

	public function updateVacationSettings($personIDArray, $onVacationArray, $vacationReturnArray) {
		for($m = 0; $m < (count($personIDArray) - 1); $m++) {
			$sql = 'UPDATE People SET OnVacation = ?, VacationReturn = ? WHERE PersonID = ?';
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $onVacationArray[$m]), ($vacationReturnArray[$m] == '' ? array(DTYPE_TIME, null) : array(DTYPE_TIME, $vacationReturnArray[$m])), array(DTYPE_INT, $personIDArray[$m]));
			$results = DbConnManager::GetDb('mpower')->Execute($sql);
//			print_r($results);
		}
	}
}

?>