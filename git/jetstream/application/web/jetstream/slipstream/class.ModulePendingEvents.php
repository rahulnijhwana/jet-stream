<?php
require_once BASE_PATH . '/slipstream/class.Event.php';
//require_once BASE_PATH . '/slipstream/class.CompanyContact.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';

class ModulePendingEvents extends JetstreamModule
{
	public $title = 'Pending Events';
	protected $javascript_files = array('overlay.js', 'jquery.js', 'jquery.bgiframe.js', 'jquery.autocomplete.js', 'event.js', 'stringexplode.js', 'ui.datepicker.js', 'jquery.bgiframe.js','jquery.cluetip.js');
	protected $css_files = array('ui.datepicker.css', 'overlay.css', 'jquery.autocomplete.css','mini_calendar.css', 'event.css', 'jquery.cluetip.css');
	protected $buttons = array();
	public $sidebar = false;
	public $template_files = array('module_pending_events.tpl');
	
	public function Init() {		
		/**
		 * Create Event() class object.
		 * Get the pending events for the user.
		 */
		$obj_event = new Event();		
		$this->pending_events = $obj_event->GetPendingEvents();				
	}
}
