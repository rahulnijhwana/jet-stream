<?php
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.CalendarPermission.php';
require_once '../include/class.ReportingTreeLookup.php';

class ModuleCalendarPermission extends JetstreamModule
{
	public $title = 'Calendar Permission';
	protected $javascript_files = array('calendar_permission.js');
	protected $css_files = array('js_admin.css');
	public $user_details;
	public $user_id;
	public $supervisor_id;

	public function Init() {
		$objCalendar = new CalendarPermission();
		
		$this->user_id = $_SESSION['USER']['USERID'];
		$company_id = $_SESSION['USER']['COMPANYID'];

		$this->user_details = ReportingTreeLookup::GetCombined($company_id);
		$this->user_details = ReportingTreeLookup::ActiveOnly($company_id, $this->user_details);
		$this->user_details = ReportingTreeLookup::SortPeople($company_id, $this->user_details, 'LastName');
		$this->user_details = ReportingTreeLookup::GetRecords($company_id, $this->user_details);

		// Remove the current user from the list
		unset($this->user_details[$this->user_id]);
		
		$this->cal_perms = $objCalendar->GetSingleUserCalPerms($this->user_id);
		$supervisor = $_SESSION['USER']['SUPERVISORID'];
		// Supervisor is always allowed read/write locked - so update the perms to reflect this
		if ($supervisor > 0) {
			$this->cal_perms[$supervisor]['ReadPermission'] = true;
			$this->cal_perms[$supervisor]['WritePermission'] = true;
			$this->cal_perms[$supervisor]['LockPermission'] = true;
			
		}
		
		// $this->supervisor_id = $_SESSION['USER']['SUPERVISORID'];
		
		// exit;
		
		// $this->user_details = $objCalendar->GetUserCalendarPermission($_SESSION['USER']['COMPANYID'], $_SESSION['USER']['USERID']);
		//print_r($this->user_details);
		//$this->AddTemplate('snippet_contact_mail_fields.tpl');
		$this->AddTemplate('module_calendar_permission.tpl');
	}
}

?>
