<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.MpRecord.php';
// require_once BASE_PATH . '/include/mpconstants.php';

/**
 * The DB fields initialization
 */
class OppInfo extends MpRecord
{

	public function Initialize() {
		$this->db_table = 'opportunities';
		$this->recordset->GetFieldDef('DealID')->auto_key = TRUE;
				
		parent::Initialize();
	}

	public function Save($simulate = false) {
		parent::Save($simulate);
	}

}

?>