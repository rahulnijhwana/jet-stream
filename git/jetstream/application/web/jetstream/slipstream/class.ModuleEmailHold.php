<?php

require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.EmailHold.php';

class ModuleEmailHold extends JetstreamModule
{
	public $title = 'Email Unassigned';
	protected $javascript_files = array('contact.js', 'ui.datepicker.js', 'jquery.cookie.js', 'overlay.js', 'jquery.autocomplete.js', 'email_hold.js');
	protected $css_files = array('jet_datagrid.css', 'ui.datepicker.css', 'overlay.css', 'jquery.autocomplete.css', 'note_print.css','email_hold.css', 'note.css');	
	
	public $email_hold;
	public $contact_hint_list;
	public $contact_email_addresses;	

	public function Init() {
		
		/**
		 * Create EmailHold() class object.
		 */
		$obj_email_hold = new EmailHold();
		$this->email_hold = $obj_email_hold->GetEmailHold($_SESSION['login_id']);
		
		/**
		 * include tpl file for HTML section.
		 */
		$this->AddTemplate('module_email_hold.tpl');
	}
}
