<?php
require_once BASE_PATH . '/slipstream/class.JetstreamPage.php';
require_once BASE_PATH . '/slipstream/class.ModuleEmailHold.php';
//require_once BASE_PATH . '/slipstream/class.ModuleRelatedContacts.php';

$page = new JetstreamPage();
$page->AddModule(new ModuleEmailHold());
//$page->AddAjaxModule(new ModuleRelatedContacts());

$page->Render();
