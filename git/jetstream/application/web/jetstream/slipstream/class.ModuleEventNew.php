<?php
if (!defined("BASE_PATH")) {
	define("BASE_PATH", realpath(dirname(__FILE__) . "/.."));
}
require_once BASE_PATH . "/include/class.ReportingTreeLookup.php";
require_once BASE_PATH . "/include/class.RecEvent.php";
require_once BASE_PATH . "/include/class.RecEventAttendee.php";
require_once BASE_PATH . "/include/class.JetDateTime.php";
require_once BASE_PATH . "/include/class.DateTimeUtil.php";
require_once BASE_PATH . "/include/class.PriorityUtil.php";
require_once BASE_PATH . "/include/class.UserUtil.php";
require_once BASE_PATH . "/include/lib.dblookup.php";
require_once BASE_PATH . "/include/lib.string.php";
require_once "class.Event.php";
require_once "class.EventTypeLookup.php";
require_once "class.JetstreamModule.php";
require_once "class.Note.php";

require_once BASE_PATH . '/include/class.GoogleConnect.php';
require_once BASE_PATH . '/include/class.GoogleCalendar.php';
require_once BASE_PATH . '/include/class.GoogleTasks.php';

define ('ADD', 0);
define ('EDIT', 1);

class ModuleEventNew extends JetstreamModule
{
	protected $javascript_files = array('ui.datepicker.js', 'jquery.cluetip.js');
	protected $css_files = array('event.css', 'ui.datepicker.css', 'mini_calendar.css', 'jquery.cluetip.css');
	
	public $repeat_types = array('Does not repeat', 'Daily', 'Weekly', 'Monthly', 5 => 'Yearly');
	public $weekdays = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
	public $pos_locs = array(1 => 'First', 'Second', 'Third', 'Fourth', 'Last');
	public $pos_types = array(7 => 'day', 8 => 'weekday', 9 => 'weekend day', 0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday');
	public $title = 'Event Information';
	public $submit_to = "ajax.Event.php";
	public $event_info;
	public $attendees;
	public $event_str;
	public $form_id;
	public $user_tz;
	public $priorities;
	private $dateTimeUtil;
	public $hourSelect;
	public $minuteSelect;
	public $periodSelect;
	
	private $company_id;
	private $user_id;

    private $isUseGoogleSync;

	function __construct($company_id = -1, $user_id = -1, $user_tz = -1) {

		$this->company_id = ($company_id == -1) ? $_SESSION['company_obj']['CompanyID'] : $company_id;
	
		$this->user_id = ($user_id == -1) ? $_SESSION['USER']['USERID'] : $user_id;
		$this->user_tz = new DateTimeZone(($user_tz == -1) ? $_SESSION['USER']['BROWSER_TIMEZONE'] : $user_tz);
		$this->priorities = PriorityUtil::all();
		
		$this->dateTimeUtil = new DateTimeUtil();
		$this->hourSelect = $this->dateTimeUtil->buildHourSelect('event_hours');
		$this->minuteSelect = $this->dateTimeUtil->buildMinuteSelect('event_minutes');
		$this->periodSelect = $this->dateTimeUtil->buildPeriodSelect('event_periods');

	}

	public function Build($rec_type, $rec_id, $action = 'display', $limit_access = false, $repeat_instance = false, $default_data = false) {

		/*
		echo '<pre>'; print_r($rec_type); echo '</pre>';
		echo '<pre>'; print_r($rec_id); echo '</pre>';
		echo '<pre>'; print_r($action); echo '</pre>';
		echo '<pre>'; print_r($limit_access); echo '</pre>';
		echo '<pre>'; print_r($repeat_instance); echo '</pre>';
		echo '<pre>'; print_r($default_data); echo '</pre>';
		*/
			
		$utc_timezone = new DateTimeZone('UTC');

		// Load data, populate new record
		if ($action != 'add') {
			$event_sql = "SELECT * FROM Event WHERE EventID = ?";
			$event_sql = SqlBuilder()->LoadSql($event_sql)->BuildSql(array(DTYPE_INT, array($rec_id)));
			
			$recordset = DbConnManager::GetDb('mpower')->Execute($event_sql, 'RecEvent');
			
			$recordset->Initialize();
			$this->event_info = $recordset[0];
	
			// Default data is used because edit opp stores form value until the opp is saved
			// This way if they re-edit an event, they can see their original edits
			if ($default_data) {
				foreach ($default_data as $field => $value) {
					$this->event_info[$field] = $value;
				}
			}
			/* Get EventType lookup
			 * Ex. Array(
			    [EventTypeID] => 1517
			    [CompanyID] => 508
			    [EventName] => (T) Phone Account Service
			    [EventColor] => E2CC76
			    [Deleted] => 0
			    [Type] => TASK
			    [UseEventInCallReport] => 1
	 		 * )
	 		 */
			$this->event_info['EventType'] = EventTypeLookup::GetRecord($this->company_id, $this->event_info['EventTypeID']);
            /*
            echo '<pre>'; print_r($this->company_id); echo '</pre>';
            echo '<pre>'; print_r($this->event_info['EventTypeID']); echo '</pre>';
            echo '<pre>'; print_r($this->event_info['EventType']); echo '</pre>'; exit;
            */

            if ( $this->event_info['EventTypeID'] == GOOGLE_EVENT_TYPE_IN_JETSTREAM ) {
                $this->event_info['EventType'] = array(
                        'EventName' => 'Google Event', 
                        'Type' => 'EVENT' 
                );

            }

            if ( $this->event_info['EventTypeID'] == GOOGLE_TASK_TYPE_IN_JETSTREAM ) {
                $this->event_info['EventType'] = array(
                        'EventName' => 'Google TASK', 
                        'Type' => 'TASK' 
                );
            }
			$this->event_info['User'] = ReportingTreeLookup::GetRecord($this->company_id, $this->event_info['UserID']);
			
			if ($repeat_instance && (!$this->event_info->InRecord('RepeatParent') || $this->event_info['EventID'] == $this->event_info['RepeatParent'])) {
				// The user has asked to load an instance of a repeating event that isn't a separate ID
				// So clone the series for the instance instance date
				$repeat_instance_dt = new JetDateTime(date('r', $repeat_instance), $this->user_tz);
				$this->event_info = CloneEvent($this->event_info, $repeat_instance_dt, $this->user_tz);
				$this->event_info['RepeatType'] = REPEAT_NONE;
				$this->event_info['RepeatParent'] = $rec_id;
				unset($this->event_info['EventID']);
			}
			
			if ($this->event_info['EventType']['Type'] == 'TASK') {
				$this->title = "Task Information";

                $date = $this->event_info['StartDateUTC'];
                $date = str_replace(':000','',$date);
                $date = str_replace('.000','',$date);
                $this->event_info['StartDateUTC'] = $date;

				$this->event_info['StartTimeStamp'] = new JetDateTime($this->event_info['StartDateUTC'], $this->user_tz);
				if($this->event_info['PriorityID']){
					$priority = PriorityUtil::one($this->event_info['PriorityID']);
					$this->event_info['PriorityLabel'] = $priority['Priority'] . ' - (' . $priority['Label'] . ')'; 
				}
			}
			else {
                $date = $this->event_info['StartDateUTC'];
                $date = str_replace(':000','',$date);
                $date = str_replace('.000','',$date);
				$this->event_info['StartTimeStamp'] = new JetDateTime($date, $utc_timezone);
				$this->event_info['StartTimeStamp']->setTimezone($this->user_tz);
			}
			
			$today_begin = new JetDateTime();
			$today_begin->setTimezone($this->user_tz);
			$today_begin->setTime(0, 0, 0);
			
			if ($this->event_info['StartTimeStamp'] < $today_begin && !$this->event_info['Closed']) {
				$this->event_info['pending'] = true;
			}
			
			// Translate data for display
            $date = $this->event_info['EndDateUTC'];
            $date = str_replace(':000','',$date);
            $date = str_replace('.000','',$date);

			$this->event_info['EndTimeStamp'] = new JetDateTime($date, $utc_timezone);
			$this->event_info['EndTimeStamp']->SetTimezone($this->user_tz);

			//$this->formatEventStr($this->event_info['StartDateUTC'], $this->event_info['EndDateUTC']);
			$this->formatEventStr($this->event_info['StartTimeStamp'], $this->event_info['EndTimeStamp']);

			if ($this->event_info['EventType']['Type'] == 'TASK') {
				$this->event_str = substr($this->event_info['StartDateUTC'],0,11);
			}

            $date = $this->event_info['RepeatEndDateUTC'];
            $date = str_replace(':000','',$date);
            $date = str_replace('.000','',$date);
			$this->event_info['RepeatEndTimeStamp'] = new JetDateTime($date, $utc_timezone);
			$this->event_info['Duration'] = $this->event_info['EndTimeStamp']->format('U') - $this->event_info['StartTimeStamp']->format('U');
			
			// Ensure that the page chooses the month or year section when they choose either sub-section
			$this->event_info['repeat_selector'] = ($this->event_info['RepeatType'] == 4 || $this->event_info['RepeatType'] == 6) ? $this->event_info['RepeatType'] - 1 : $this->event_info['RepeatType'];
			
			// Extract the repeat interval data
			$repeat_detail = unserialize($this->event_info['RepeatInterval']);
			//echo "<pre>";
			//print_r($this);exit;
			$repeat_interval = array();
			switch($this->event_info['RepeatType']) {
				case REPEAT_NONE :
					break;
				case REPEAT_DAILY :
					$repeat_interval['daily_freq'] = $repeat_detail[0];
					break;
				case REPEAT_WEEKLY :
					list($repeat_interval['weekly_freq'], $repeat_interval['weekly_days']) = $repeat_detail;
					break;
				case REPEAT_MONTHLY :
					list($repeat_interval['monthly_freq'], $repeat_interval['monthly_day']) = $repeat_detail;
					break;
				case REPEAT_MONTHLY_POS :
					list($repeat_interval['monthly_freq'], $repeat_interval['monthly_pos'], $repeat_interval['monthly_pos_type']) = $repeat_detail;
					break;
				case REPEAT_YEARLY :
					list($repeat_interval['yearly_freq'], $repeat_interval['yearly_month'], $repeat_interval['yearly_month_day']) = $repeat_detail;
					break;
				case REPEAT_YEARLY_POS :
					list($repeat_interval['yearly_freq'], $repeat_interval['yearly_pos'], $repeat_interval['yearly_pos_type'], $repeat_interval['yearly_pos_month']) = $repeat_detail;
					break;
			}
			$this->repeat_interval = $repeat_interval;
			
			$attendee_sql = 'SELECT * FROM EventAttendee WHERE EventID = ? AND (Deleted != 1 OR Deleted IS NULL)';
			
			if (is_object($this->event_info) && $this->event_info->InRecord('EventID')) {
				$attendee_source = $this->event_info['EventID'];
			}
			else {
				$attendee_source = $this->event_info['RepeatParent'];
			}
			
			$attendee_sql = SqlBuilder()->LoadSql($attendee_sql)->BuildSql(array(DTYPE_INT, $attendee_source));
			$this->attendees = DbConnManager::GetDb('mpower')->Execute($attendee_sql, 'RecEventAttendee', 'PersonID');
			$this->attendees->Initialize();
			
		}
		else { // add

			if ($rec_type == 'company') {
				$this->event_info['CompanyID'] = $rec_id;
				$this->event_info['AccountID'] = $rec_id;
	
				//$this->event_info['ContactID'] = $this->user_id;
				if ($default_data) {
					$this->event_info['Subject'] = $default_data['Subject'];
					$this->event_info['BlankTime'] = $default_data['BlankTime'];
					foreach ($default_data as $field => $value) {
						$this->event_info[$field] = $value;
						if ($field == 'StartDate') {
							$this->event_info['StartTimeStamp'] = new JetDateTime($value, $this->user_tz);
						}
						if ($field == 'EndDate') {
							$this->event_info['EndTimeStamp'] = new JetDateTime($value, $this->user_tz);
						}
					}
				}
				//$this->event_info['StartTimeStamp'] = new JetDateTime();
			}
			elseif ($rec_type == 'contact') {
				$this->event_info['ContactID'] = $rec_id;
				if ($default_data) {
					$this->event_info['Subject'] = $default_data['Subject'];
					$this->event_info['BlankTime'] = $default_data['BlankTime'];
					foreach ($default_data as $field => $value) {
						$this->event_info[$field] = $value;
						if ($field == 'StartDate') {
							$this->event_info['StartTimeStamp'] = new JetDateTime($value, $this->user_tz);
						}
						if ($field == 'EndDate') {
							$this->event_info['EndTimeStamp'] = new JetDateTime($value, $this->user_tz);
						}
					}
				}
				//$this->event_info['StartTimeStamp'] = new JetDateTime();
			}
			elseif ($rec_type == 'event') {

				if (!empty($rec_id)) {
					$this->event_info['EventID'] = $rec_id;
				}
				if ($default_data) {
					foreach ($default_data as $field => $value) {
						$this->event_info[$field] = $value;
						if ($field == 'StartDateUTC') {
							$this->event_info['StartTimeStamp'] = new JetDateTime($value);
						}
						if ($field == 'EndDateUTC') {
							$this->event_info['EndTimeStamp'] = new JetDateTime($value);
						}
						
					}
				}
			}
			
			$this->attendees[] = array('PersonID' => $this->user_id, 'Creator' => 1, 'Accepted' => 1);
		}

		foreach ($this->attendees as & $attendee) {
			if ($attendee['Creator'] == 1) {
				$this->creator = $attendee['PersonID'];
			}
			$person = ReportingTreeLookup::GetRecord($this->company_id, $attendee['PersonID']);
			$attendee['name'] = $person['FirstName'] . ' ' . $person['LastName'];
		}
		
		if (empty($this->creator)) {
			$this->creator = $this->user_id;
		}

	
		if (isset($this->event_info['AccountID']) && $this->event_info['AccountID']) {
			$this->event_info['AccountName'] = GetAccountName($this->event_info['AccountID']);
		}

		if (isset($this->event_info['CompanyID']) && $this->event_info['CompanyID']) {
			$this->event_info['CompanyName'] = GetAccountName($this->event_info['CompanyID']);
		}
		if ($this->event_info['ContactID']) {
			$this->event_info['Contact'] = GetContactName($this->company_id, $this->event_info['ContactID']);
			/* 
			 * Google response null for contact id and we update login-user id for contact id
			 * Then Jetstream admin is not in Contact table so we should lookup different way
			 */
			if ('' == $this->event_info['Contact']) {
				$person = ReportingTreeLookup::GetRecord($this->company_id, $this->event_info['ContactID']);
				$this->event_info['Contact'] = array(
    					'ContactID' => $this->event_info['ContactID'],
    					'FirstName' => $person['FirstName'],
    					'LastName' => $person['LastName'],
    					'noLink' => 1
				);
				
			}
			/*
			 * Build user tree for use with note alert
			 */
			//$this->users = UserUtil::getUsers($this->company_id, false);
			$this->users = UserUtil::getChainOfCommand($this->company_id, $this->user_id); // added 7/8/2013 by V. Gorman/BST

			$assigned = UserUtil::getAssignedUsers($this->event_info['ContactID'], 'contact');
			$users = array();
	
			foreach($this->users as $user){
				if(in_array($user['PersonID'], $assigned)){
					$user['AssignedTo'] = true;
				}
				else {
					$user['AssignedTo'] = false;
				}
				$users[] = $user;
			}
			$this->users = $users;
	
			$this->tree = UserUtil::getUserTree($this->company_id, $this->users);
		}

        /* add for alert working for new event too, besso */
        if ( empty($this->tree) ) {
			$this->users = UserUtil::getChainOfCommand($this->company_id, $this->user_id);
			$this->tree = UserUtil::getUserTree($this->company_id, $this->users);
        }

		switch ($action) {
			case 'add' :
                if ( isset($this->event_info['Contact']['Private']) ) {
				    if($this->event_info['Contact']['Private']){
				    	$this->event_info['Private'] = true;
				    }
                }
			
			case 'edit' :

				$this->permitted_people = $this->GetPermittedPeople();
				$this->form_id = 'id' . uniqid();
				$this->template_files = array('form_event_new.tpl');

				break;
		
			default :
                //echo '<pre>'; print_r($this->event_info); echo '</pre>'; exit;
				$this->AddTemplate('module_event_info.tpl');
				if ($this->event_info['AccountID']) {
					$this->event_info['AccountName'] = GetAccountName($this->event_info['AccountID']);
				}
	
				// Add the appropriate buttons to the module
				if ($this->event_info['Cancelled'] != 1 && $this->event_info['Closed'] != 1) {
					if ($this->event_info->InRecord('DealID') && ($this->event_info['DealID'] > 0)) {
						$this->AddButton('', "javascript:openOppPopup({$this->event_info['DealID']}, {$this->user_id})", 'Edit');
						$this->AddButton('', "javascript:openOppPopup({$this->event_info['DealID']}, {$this->user_id})", 'Complete');
						$this->AddButton('', "javascript:openOppPopup({$this->event_info['DealID']}, {$this->user_id})", 'Remove');
					}
					else {
						if ($this->event_info->InRecord('EventID')) {
							$event_ref = "eventid={$this->event_info['EventID']}";
							$event_encoded = '';
						}
						else {
							$button_id = '';
							$event_encoded = base64_encode(json_encode(array('i' => $this->event_info['RepeatParent'], 'd' => UnixToTruncatedDate($this->event_info['StartTimeStamp']), 'u' => $this->user_id)));
							$event_ref = "rep=" . $event_encoded;
						}
						
						$this->AddButton('', "javascript:ShowAjaxForm('EventNew&$event_ref&action=edit');", 'Edit');
						if ($this->event_info['RepeatType'] == 0 || !empty($this->event_info['RepeatParent'])) {
                            if ( $this->event_info['EventTypeID'] != GOOGLE_EVENT_TYPE_IN_JETSTREAM  && $this->event_info['EventTypeID'] != GOOGLE_TASK_TYPE_IN_JETSTREAM ) {
    							$this->AddButton('', "javascript:CompletePendingEvent('{$this->event_info['EventID']}', '$event_encoded');", 'Complete');
                            }
						}
						$this->AddButton('', "javascript:CompletePendingEvent('{$this->event_info['EventID']}', '$event_encoded', 'remove');", 'Remove');
					}
				} else {
					$this->formatting = 'content_inactive';
				}
		}
	}

	/*
	 * @param $google_sync : pull from google tasks 
	 */
	public function SaveForm($form_values, $only_return_values = false, $source_page = '', $ignore_missing = false, $google_sync = false) {

		require_once 'class.Status.php';
		
		$utc_timezone = new DateTimeZone('UTC');

		/* Debug : Event new : SaveForm */
        /*
		echo '<pre>'; print_r($form_values); echo '</pre>';
		echo '<pre>'; print_r($only_return_values); echo '</pre>';
		echo '<pre>'; print_r($source_page); echo '</pre>';
		echo '<pre>'; print_r($ignore_missing); echo '</pre>';
		echo '<pre>'; print_r($this->company_id); echo '</pre>';
        exit;
        */

		$status = new Status();
		$record = new RecEvent();

		// Determine what the ID is for this event (-1 for a new one)
		if (empty($form_values['EventID'])) {
			$form_type = ADD;
			$event_id = -1;
		}
		elseif ($form_values['EventID'] < 0) {
			$form_type = ADD;
			$event_id = $form_values['EventID'];
		} 
		else {
			$form_type = EDIT;
			$event_id = $form_values['EventID'];
		}


		// Load the record from the database
		$sql = 'SELECT * FROM Event WHERE EventID = ?';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $event_id));
		$recordset = DbConnManager::GetDb('mpower')->Execute($sql, 'RecEvent');

		if ($form_type == ADD) {
			$record = new RecEvent();
			$recordset[] = $record;
			$record->SetDatabase(DbConnManager::GetDb('mpower'));
		}
		else {
			$record = $recordset[0];
		}
		$record->Initialize();

		if (!empty($form_values['DealID'])) {
			$record->DealID = $form_values['DealID'];
		}

		$isAccount = false;
		$isContact = false;
		
		$record->CompanyID = $this->company_id;
		
/*Start JET-37 For Edit Task n Event in Google*/
	if ($form_type == EDIT) {
			$record->googleStatus = '0';
		}
/*End JET-37 */
		if (!empty($form_values['AccountID']) && ($form_values['AccountID'] > 0 || $form_values['AccountID'] == -1)) {
			$record->AccountID = $form_values['AccountID'];
			$isAccount = true;
		} else if (!empty($form_values['ContactID']) && ($form_values['ContactID'] > 0 || $form_values['ContactID'] == -1)) {
			$record->ContactID = $form_values['ContactID'];
			$isContact = true;
		} else {
			$message = ( true == $isAccount ) ? 'Company' : 'Contact' ;
			$status->addError('You must select a ' . $message, $message);
		}

		if (!empty($form_values['EventType']) && $form_values['EventType'] > 0) {

			$record->EventTypeID = $form_values['EventType'];
			$event_type = EventTypeLookup::GetRecord($this->company_id, $form_values['EventType']);
			if ($event_type['Type'] == 'TASK') {
				
				$record->RepeatType = REPEAT_NONE;
				
				if (!empty($form_values['StartDate'])) {
					$startdate = new JetDateTime($form_values['StartDate'], $utc_timezone);
					$record->StartDateUTC = $startdate->asMssql();
					$record->EndDateUTC = str_replace('12:00AM','11:59PM',$record->StartDateUTC);
				}
				else {
					$status->addError('You must enter a date for this task', 'When');
				}
	
				$record->PriorityID = $form_values['priority'];
				
			}
			elseif ($event_type['Type'] == 'EVENT') {
				if (!empty($form_values['StartDate']) && !empty($form_values['StartTime']) && !empty($form_values['EndDate']) && !empty($form_values['EndTime'])) {
					
					
					$startdate = new JetDateTime($form_values['StartDate'] . ' ' . $form_values['StartTime'], $this->user_tz);
					$enddate = new JetDateTime($form_values['EndDate'] . ' ' . $form_values['EndTime'], $this->user_tz);
					if ($startdate < $enddate) {
						$record->StartDateUTC = $startdate->asMssql();
						$record->EndDateUTC = $enddate->asMssql();
					}
					else {
						$status->addError('The end date/time must be later than the start date/time', 'When');
					}
				}
				else {
					$status->addError('All of the date/time fields must be filled', 'When');
				}
			}
		}
		else {
			$status->addError('Select an event type', 'EventType');
		}
		if (!empty($form_values['Subject']) && strlen($form_values['Subject']) > 0) {
			$record->Subject = $form_values['Subject'];
		}
		else {
			$status->addError('Enter a subject', 'Subject');
		}
		$record->Location = $form_values['Location'];
		$record->Private = ($form_values['CheckPrivate'] == 'true') ? 1 : 0;

		/*
		 * Recurring Events
		 */
		if (!empty($form_values['RepeatParent'])) {
			$record->RepeatParent = $form_values['RepeatParent'];
		}
		
		if (empty($form_values['Repeat'])) {
			$record->RepeatType = REPEAT_NONE;
		}
		else {
			$record->RepeatType = $form_values['Repeat'];
		}
		
		if ($record->RepeatType == REPEAT_MONTHLY) {
			if (!empty($form_values['repeat_detail']['monthly_type'])) {
				$record->RepeatType = $form_values['repeat_detail']['monthly_type'];
			}
			else {
				$status->addError('You must select a radio button for the type of monthly repeat', 'RepeatFreq');
			}
		}
		if ($record->RepeatType == REPEAT_YEARLY) {
			if (!empty($form_values['repeat_detail']['yearly_type'])) {
				$record->RepeatType = $form_values['repeat_detail']['yearly_type'];
			}
			else {
				$status->addError('You must select a radio button for the type of yearly repeat', 'RepeatFreq');
			}
		}
		
		$form_rpt_dtl = $form_values['repeat_detail'];
		$repeat_detail = array();
		if ($record->RepeatType > 0) {
			switch ($record->RepeatType) {
				case REPEAT_DAILY :
					$repeat_detail[] = $form_rpt_dtl['daily_freq'];
					break;
				case REPEAT_WEEKLY :
					$repeat_detail[] = $form_rpt_dtl['weekly_freq'];
					$repeat_detail[] = $form_rpt_dtl['weekly_day'];
					break;
				case REPEAT_MONTHLY :
					$repeat_detail[] = $form_rpt_dtl['monthly_freq'];
					$repeat_detail[] = $form_rpt_dtl['monthly_daynum'];
					break;
				case REPEAT_MONTHLY_POS :
					$repeat_detail[] = $form_rpt_dtl['monthly_freq'];
					$repeat_detail[] = $form_rpt_dtl['monthly_pos'];
					$repeat_detail[] = $form_rpt_dtl['monthly_pos_type'];
					break;
				case REPEAT_YEARLY :
					$repeat_detail[] = $form_rpt_dtl['yearly_freq'];
					$repeat_detail[] = $form_rpt_dtl['yearly_monthnum'];
					$repeat_detail[] = $form_rpt_dtl['yearly_daynum'];
					break;
				case REPEAT_YEARLY_POS :
					$repeat_detail[] = $form_rpt_dtl['yearly_freq'];
					$repeat_detail[] = $form_rpt_dtl['yearly_pos'];
					$repeat_detail[] = $form_rpt_dtl['yearly_pos_type'];
					$repeat_detail[] = $form_rpt_dtl['yearly_pos_month'];
					break;
			}
			foreach($repeat_detail as $detail_item) {
				if (empty($detail_item)) {
					$status->addError('All values for the selected repeat must be entered', 'RepeatFreq');
				}
			}
			if ($form_values['repeat_enddate_type'] == 1) {
				if (!empty($form_values['repeat_enddate'])) {
					$record->RepeatEndDateUTC = new JetDateTime($form_values['repeat_enddate'], $utc_timezone);
				}
				else {
					$status->addError('You must enter an end date, or choose "No end date"', 'RepeatUntil');
				}
			}
			else {
				$record->RepeatEndDateUTC = NULL;
			}
		}
		$record->RepeatInterval = serialize($repeat_detail);

		$attendees = json_decode($form_values['attendee_list']);

		// TODO Validate that the attendee list before saving the event
		/*
		 * Note Alert
		 */
        $noteAlert = false;
		if( $form_values['add_event_alert_hidden'] || !empty($form_values['alert_users']) ){
           //( isset($form_values['alert_users']) && count($form_values['alert_users']) > 0 ) ){
			
			$noteAlert = true;
		
			//if(!$form_values['email_event_alert'] && !$form_values['standard_event_alert']){
			if(!isset($form_values['email_event_alert']) && !isset($form_values['standard_event_alert'])){
				$status->addError('Select a delivery method', 'Delivery');
			}
			if(!$form_values['send_event_immediately'] && !$form_values['delay_event_until']){
				$status->addError('Select a day', 'Delay');
			}
			if(!$form_values['send_event_immediately'] && $form_values['delay_event_until']){
				
				$current_date_time = new JetDateTime();
				$current_date_time->setTimezone($this->user_tz);
				$current_date_str = $current_date_time->format('m/d/Y H:i:s A');
				$current_date_stamp = strtotime($current_date_str);
				
				$delay_day = $form_values['delay_event_until'];
				$hours = $form_values['event_hours'];
				$minutes = $form_values['event_minutes'];
				$period = $form_values['event_periods'];
				
				$delay_str =  $delay_day . ' ' . $hours . ':' . $minutes . ' ' . $period;
				$delay_stamp = strtotime($delay_str);
				
				if($current_date_stamp > $delay_stamp){
					$status->addError('Selected date is in the past', 'Delay');
				}
			}
			if(!$form_values['alert_users']){
				$status->addError('Select at least one user', 'Users');
			} 

			if(!$form_values['alert_users']){
				$status->addError('Select at least one user', 'Users');
			} 
		}

		$results['status'] = $status->GetStatus();

		if ($results['status']['STATUS'] == 'OK') {
			// For edit opp forms, we only want to return an array of values - so in this case, we
			// need to skip the save process and create an array of the dirty record vals
			
			if ($only_return_values) {
				$data = $record->GetDirtyData();
				if (!empty($form_values['Note'])) {
					$data['Note'] = $form_values['Note'];
				}
				if (isset($data['StartDateUTC'])) {
					$startdate->setTimezone($this->user_tz);
					$data['meetingDate'] = $startdate->format('m/d/y');
					$data['meetingTime'] = $startdate->format('g:i A');
				}

				$data['attendees'] = $attendees;

				$results['status']['RECID'] = $event_id;
				$results['event_data'] = $data;
				return $results;
			}

            if ( $this->company_id == -1 ) {
                $this->company_id = $_SESSION['USER']['COMPANYID'];
            }
            if ( $this->company_id == -1 ) {
                $this->isUseGoogleSync = false;
            } else {
                $this->isUseGoogleSync = GoogleConnect::isUseGoogleSync($this->company_id);
            }

            if ( $this->isUseGoogleSync ) {

                if ( isset($form_values['googleEventId']) && $form_values['googleEventId'] == '' ) {

                    $sql = 'Select googleId, googleEventId from event where eventId = ?';
			        $params= array();
                    $params[] = array(DTYPE_INT, $event_id);
                    $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
                    $aGoogle = DbConnManager::GetDb('mpower')->Exec($sql);

			        if (isset($aGoogle[0]['googleId']) && '' !== $aGoogle[0]['googleId']) {
			        	$form_values['googleId'] = $aGoogle[0]['googleId'];
			        }

			        if (isset($aGoogle[0]['googleEventId']) && '' !== $aGoogle[0]['googleEventId']) {
			        	$form_values['googleEventId'] = $aGoogle[0]['googleEventId'];
			        }

                    if ( $form_values['EventType'] == GOOGLE_TASK_TYPE_IN_JETSTREAM ) {
			        	$record->googleId = $form_values['googleId'];
			        	$record->Closed = $form_values['Closed'];
			        }
                    if ( $form_values['EventType'] == GOOGLE_EVENT_TYPE_IN_JETSTREAM ) {
			        	$record->googleEventId = $form_values['googleEventId'];
			        	$record->Closed = $form_values['Closed'];
			        }
                } else {
                    //$record->googleEventId = '';
                    // set googleEventId value for jetstream inserting
                    $hasEvent = ( $form_values['EventID'] == '' ) ? false : true;
                    if ( !$hasEvent ) {
                        $record->googleEventId = (isset($form_values['googleEventId'])) ? $form_values['googleEventId'] : '';
                        $record->googleId = (isset($form_values['googleId'])) ? $form_values['googleId'] : '';
                    }
                }
            }

            //echo '<pre>'; print_r($form_values); echo '</pre>'; 
            //exit;
            //echo '<pre>'; print_r($record); echo '</pre>'; exit;

            //error_log($form_values['googleEventId']); exit;
            /*Start JET-37 For task speed Problem */
		$_SESSION['USER']['CONTACTID'] = $record->ContactID;
		/*End JET-37 For task speed Problem */

			$record->Save();
			
			// Handle attendee_list
	
			// Load all attendees of this record, index by PersonID
			$attendee_sql = 'SELECT * FROM EventAttendee WHERE EventID = ?';
			$attendee_sql = SqlBuilder()->LoadSql($attendee_sql)->BuildSql(array(DTYPE_INT, $record->EventID));
			$attendee_set = DbConnManager::GetDb('mpower')->Execute($attendee_sql, 'RecEventAttendee', 'PersonID');
			
			// Set all their delete flags (will be cleared as we process each rec from the web page)
			$attendee_set->SetAll('Deleted', true);
			
			// First attendee is creator
			$first = true;
			foreach($attendees as $attendee) {
				if (!$attendee_set->InRecordset($attendee)) {
					$attendee_rec = new RecEventAttendee();
					$attendee_set[$attendee] = $attendee_rec;
					$attendee_rec->SetDatabase(DbConnManager::GetDb('mpower'));
					//$attendee_set->Initialize();
					$attendee_rec->EventID = $record->EventID;
					$attendee_rec->PersonID = $attendee;
				}
				if ($first) {
					$attendee_set[$attendee]->Creator = 1;
                    $creator = $attendee;
					$first = false;
				} else {
					$attendee_set[$attendee]->Creator = 0;
				}
				$attendee_set[$attendee]->Deleted = false;
			}
            //echo '<pre>'; print_r($attendee_set); echo '</pre>'; exit;
            
			$attendee_set->Initialize()->Save();
			
            //error_log(print_r($attendees,true));
            //echo '<pre>'; print_r($attendees); echo '</pre>';
            //echo '<pre>'; print_r($attendee_set); echo '</pre>'; exit;
	
			// If this is saving a clone of a repeating event, then we need to add an exception on the original date to the repeat parent
			// otherwise, we will end up with duplicate entries in the repeat series
			if (!empty($form_values['OrigStartDate'])) {
				$excep_sql = "SELECT RepeatExceptions FROM Event WHERE EventID = ?";
				$excep_sql = SqlBuilder()->LoadSql($excep_sql)->BuildSql(array(DTYPE_INT, $record->RepeatParent));
				$excep_set = DbConnManager::GetDb('mpower')->GetOne($excep_sql);
				$exception_string = $excep_set->RepeatExceptions;
				if (!empty($exception_string)) {
					$exceptions = explode(',', $exception_string);
				} else {
					$exceptions = array();
				}
				$exceptions[] = $form_values['OrigStartDate'];
				$excep_upd_sql = "UPDATE Event SET RepeatExceptions = ? WHERE EventID = ?";
				$excep_upd_sql = SqlBuilder()->LoadSql($excep_upd_sql)->BuildSql(array(DTYPE_STRING, implode(',', $exceptions)), array(DTYPE_STRING, $record->RepeatParent));
				DbConnManager::GetDb('mpower')->GetOne($excep_upd_sql);
			}
		    //echo '<pre>'; print_r($form_values); echo '</pre>'; exit;	
		    //error_log(print_r($form_values['Note'],true));
           

                    /*start:JET-2, Display of Company notes*/

			if ($form_type == ADD) {
					$form_values['Note'] = (empty($form_values['Note']))?'Event Created':$form_values['Note'];	
			}
			/*end:JET-2*/



 
			if (!empty($form_values['Note'])) {
				// Handle note
				$note = new Note();
				$note->noteCreator = $this->user_id;
				/*start:JET-36, Time Stamps on Event Completion notes are off by six hours for US Central time users*/
					$date = new DateTime("now", $utc_timezone );
					$note->noteCreationDate = $date->format("Y-m-d H:i:s");
				/*end:JET-36*/
				$note->noteSubject = "Event Created";
				$note->noteText = nl2br($form_values['Note']);
				$note->noteObjectType = NOTETYPE_EVENT;
				$note->noteObject = $record->EventID;
				$note->company_id = $this->company_id;
				
				if($noteAlert){
					$note->alertUsers = $form_values['alert_users'];
					$note->alertStandard = (isset($form_values['standard_event_alert']) && $form_values['standard_event_alert']) ? true : false;
					$note->alertEmail = (isset($form_values['email_event_alert']) && $form_values['email_event_alert']) ? true : false;
					
					if($form_values['send_event_immediately'] != 1){
						
						$delay = $form_values['delay_event_until'] . ' ' . $form_values['event_hours'] . ':' . $form_values['event_minutes'] . ' ' . $form_values['event_periods'];
						
						$now = strtotime($delay);
						$now += UTC_OFFSET;
						$timestamp = '@' . $now;
						
						$datetime = new DateTime($timestamp, new DateTimeZone('America/Chicago'));
						$note->delayUntil = $datetime->format('Y-m-d G:i:s.000');
					}
				}

                //echo '<pre>'; print_r($note); echo '</pre>'; exit;
                //error_log(print_r($note,true));
				
				$result_note = $note->createNote();
			}
		
			$results['status']['RECID'] = $record->EventID;
			if ($source_page == 'pending_events') {
				$results['dest'] = "?action=pending_events";
			} else {
				$results['dest'] = "?action=event&eventId=" . $record->EventID;
			}
			/*JET-37 Start*/
			$_SESSION['USER']['EVENT_ID'] = $results['status']['RECID'];
			/*JET-37 End*/
		}
		return $results;
//print_r($results);exit;
        //error_log('Class ModuleEventNew : call google push after insert/update on Jetstream' );
        if ( $google_sync ) {
		    if ($results['status']['STATUS'] == 'OK') {
                if ( $this->isUseGoogleSync ) {
		    		if ($event_type['Type'] == 'TASK') {
		    			if ( GoogleConnect::hasToken($creator) ) {
                            $googleConnect = new GoogleConnect($creator, false);
		    			    $googleTask = new GoogleTasks($googleConnect->tasklists, $googleConnect->tasks, $this->user_id, false);
		    			   // print_r($record);exit;
		    				$googleTask->insertGoogleTask($record);
		    			}
		    		} else if ($event_type['Type'] == 'EVENT') {
		    			if ( GoogleConnect::hasToken($creator) ) {
                            $googleConnect = new GoogleConnect($creator, false);
		    			    $googleCalendar = new GoogleCalendar($googleConnect->calendar, $this->user_id, false);
		    			   // print_r($record);exit;
		    				$googleCalendar->insertGoogleEvent($record);
		    			}
		    		}
		    	}
		    }
        }
		return $results;
	}

	public function DrawForm() {
		return;
	}

	/* called only by ajax/EventLegend.php */
	public function GetEventTypes($type=null) {
		
		$event_types = EventTypeLookup::GetAllRecords($this->company_id);
		if (!empty($this->event_info['DealID'])) {
			foreach($event_types as $id => $event_type) {
				if ($event_type['Type'] != 'EVENT') {
					unset($event_types[$id]);
				}
			}
		}
		else {
		
			if(!is_null($type)){
				if($type == 'EVENT'){
					foreach($event_types as $id => $event_type) {
						if ($event_type['Type'] != 'EVENT') {
							unset($event_types[$id]);
						}
					}
				}
				else if($type == 'TASK'){
					foreach($event_types as $id => $event_type) {
						if ($event_type['Type'] != 'TASK') {
							unset($event_types[$id]);
						}
					}
				}
			}
		}
		$labels = array();
		foreach($event_types as $id => $event_type){
			$labels[$id] = $event_type['EventName'];
		}
		array_multisort($labels, SORT_ASC, $event_types);
		/*
		 * array_multisort kills the indexes, so rebuild them
		 */
		$tmp = array();
		foreach($event_types as $id => $event_type){
			$tmp[$event_type['EventTypeID']] = $event_type;
		}
		return $tmp;
	}
	
	
	public function GetPeople() {
		$people = ReportingTreeLookup::GetAllActive($this->company_id);
		$people = ReportingTreeLookup::SortPeople($this->company_id, $people, 'LastName');
		$people_list = array();
		foreach($people as $person) {
			$person_rec = ReportingTreeLookup::GetRecord($this->company_id, $person);
			$people_list[$person] = "{$person_rec['FirstName']} {$person_rec['LastName']}";
		}
		return $people_list;
	}
	
	public function GetPermittedPeople() {
		$sql = "SELECT PersonID FROM People WHERE
			? IN (SupervisorID, PersonID)
			UNION
			SELECT OwnerPersonID as PersonID
			FROM CalendarPermission INNER JOIN People
				ON CalendarPermission.OwnerPersonID = People.PersonID
			WHERE CalendarPermission.PersonID = ?
				AND CalendarPermission.WritePermission = 1
				AND People.SupervisorID NOT IN (-1, -3)";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($this->user_id, $this->user_id)));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		$people = array();
		foreach ($result as $person) {
			$people[] = $person['PersonID'];
		}
		$people = ReportingTreeLookup::SortPeople($this->company_id, $people, 'LastName');
		$people_list = array();
		foreach($people as $person) {
			$person_rec = ReportingTreeLookup::GetRecord($this->company_id, $person);
			$people_list[$person] = "{$person_rec['FirstName']} {$person_rec['LastName']}";
		}
		return $people_list;
	}
	
	public function GetRepeatReadable() {
		$output = "Repeats ";
		$output .= " at " . FormatDate($this->event_info['StartTimeStamp'], 'time') . ' to ' . FormatDate($this->event_info['EndTimeStamp'], 'time');
		switch($this->event_info['RepeatType']) {
			case REPEAT_DAILY :
				$output .= " every {$this->repeat_interval['daily_freq']} day(s)";
				break;
			case REPEAT_WEEKLY :
				$output .= " every {$this->repeat_interval['weekly_freq']} week(s) on ";
				$output_days = array();
				foreach ($this->repeat_interval['weekly_days'] as $day) {
					$output_days[] = $this->weekdays[$day];
				}
				$output .= implode(', ', $output_days);
				break;
			case REPEAT_MONTHLY :
				$output .= " every {$this->repeat_interval['monthly_freq']} month(s) on the " . GetOrdinal($this->repeat_interval['monthly_day']);
				break;
			case REPEAT_MONTHLY_POS :
				$output .= " every {$this->repeat_interval['monthly_freq']} month(s) on the " . ($this->pos_locs[$this->repeat_interval['monthly_pos']]) . " " . $this->pos_types[$this->repeat_interval['monthly_pos_type']];
				break;
			case REPEAT_YEARLY :
				$output .= " every {$this->repeat_interval['yearly_freq']} year(s) on the " . GetOrdinal($this->repeat_interval['yearly_month_day']) . " of " . date('F', mktime(0, 0, 0, $this->repeat_interval['yearly_month'], 1));
				break;
			case REPEAT_YEARLY_POS :
				$output .= " every {$this->repeat_interval['yearly_freq']} year(s) on the " . ($this->pos_locs[$this->repeat_interval['yearly_pos']]) . " " . $this->pos_types[$this->repeat_interval['yearly_pos_type']] . " of " . date('F', mktime(0, 0, 0, $this->repeat_interval['yearly_pos_month'], 1));;
				break;
		}
		$output .= "<br>Starting " . FormatDate($this->event_info['StartTimeStamp'], 'date');

		if (!empty($this->event_info['RepeatEndDateUTC'])) {
			$output .= ", ending " . FormatDate($this->event_info['RepeatEndTimeStamp'], 'date', true);
		} else {
			$output .= " with no end date";
		}

		// $output .= "<br>Next appears on ";
		
		return $output;
	}
	
	function ConvertEventTimes() {
		$sql = 'select EventID, StartDate, EndDate, StartDateUTC, EndDateUTC, RepeatEndDateUTC, Subject from event where eventid in (
			select eventid from eventattendee where creator = 1 and personid = ?)';

		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->user_id));
		$result = DbConnManager::GetDb('mpower')->Execute($sql, 'RecEvent');
 		$result->Initialize();		
 		
		foreach ($result as &$record) {
			if ($record->InRecord('StartDate', false)) {
				$date = new JetDateTime($record->StartDate, $this->user_tz);
				$record->StartDateUTC = $date->asMsSql();
			}
			if ($record->InRecord('EndDate', false)) {
				$date = new JetDateTime($record->EndDate, $this->user_tz);
				$record->EndDateUTC = $date->asMsSql();
			}
			if ($record->InRecord('RepeatEndDateUTC', false)) {
				$date = new JetDateTime($record->RepeatEndDateUTC, $this->user_tz);
				$record->RepeatEndDateUTC = $date->asMsSql();
			}
		}
		$result->Save();
	}
	
	function formatEventStr($start, $end){
			
		if($end){

			$start = FormatDate($start);
			$end = FormatDate($end);
			
			$start_time_stamp = strtotime($start);
			$start_date = getdate($start_time_stamp);
			$start_time = date("g:i A", $start_time_stamp);
			
		        $end_time_stamp = strtotime($end);
		        $end_date = getdate($end_time_stamp);
		        $end_time = date("g:i A", $end_time_stamp);
	        
			if(($start_date['yday'] == $end_date['yday']) && ($start_date['year'] == $end_date['year'])){
	
				$display = $start_date['weekday'] . ' ' . $start_date['mon'] . '/' . $start_date['mday'] . '/' . $start_date['year'];
	
				$prefix_once = false;
				if(($start_date['hours'] < 12) && ($end_date['hours'] < 12)){
					$prefix_once = true;
				}
				else if(($start_date['hours'] >= 12) && ($end_date['hours'] >= 12)){
					$prefix_once = true;
				}
				
				if($prefix_once){
					$start_time = substr($start_time, 0,-3);
				}
				$display .=  ' - ' . $start_time . ' to ' . $end_time;
				
			}
			else {
				$display = $start_date['mon'] . '/' . $start_date['mday'] . '/' . $start_date['year'];
				$display .= $display .=  ' - ' . $start_time . ' to ';
				$display = $end_date['mon'] . '/' . $end_date['mday'] . '/' . $end_date['year'];
				$display .= ' ' . $end_time;
			}
		}
		else {
			$start_time_stamp = strtotime($start);
	        $start_date = getdate($start_time_stamp);
	        $start_time = date("g:i A", $start_time_stamp);
						
			# Tasks will have no end date specified
			$display = $start_date['weekday'] . ' ' . $start_date['mon'] . '/' . $start_date['mday'] . '/' . $start_date['year'];
		}
		$this->event_str = $display;
		
	}
}

function GetOptionsTime($selected) {
	if (empty($selected)) {
		$selected_time = -1;
	} else {
		$selected_time = mktime($selected->format('H'), $selected->format('i'), 0);
	}
	
	$day_length = 86400; // 24 hours * 60 minutes * 60 seconds
	$increment = 900; // 15 minutes * 60 seconds
	

	$options = '';
	
	$start = mktime(0, 0, 0);
	
	for($timehack = $start; $timehack < $start + $day_length; $timehack += $increment) {
		if ($timehack == $selected_time) {
			$selected_text = "selected";
		} else {
			$selected_text = '';
		}
		$date_string = date("g:i a", $timehack);
		$options .= "<option id='$date_string' $selected_text>$date_string</option>\n";
		if ($selected_time == -1 && date("g:i a", $timehack) == '12:00 pm') {
			$options .= "<option selected />";
		}
	}
	
	return $options;
}

function FormatDate($date, $show = "full", $utc=false) {

	$timezone = new DateTimeZone($utc ? 'UTC' : $_SESSION['USER']['BROWSER_TIMEZONE']);
	$date->setTimezone($timezone);

	switch ($show) {
		case "full" :
			return $date->format('n/j/Y g:i a');
			// return date('n/j/y g:i a', $date);
		case "date" :
			return $date->format('n/j/y');
			// return date('n/j/y', $date);
		case "time" :
			return $date->format('g:i a');
			// return date('g:i a', $date);
		case "trunc" :
			return $date->format('Ymd');
			// return date('Ymd', $date);
	}
}

function GetOptionsNumRng($selected, $start, $end, $ordinal = false) {
	$options = '<option />';
	for ($num = $start; $num <= $end; $num++) {
		$selected_text = ($num == $selected) ? "selected" : "";
		$disp_num = ($ordinal) ? GetOrdinal($num) : $num;
		$options .= "<option value='$num' $selected_text>$disp_num</option>";
	}
	return $options;
}

function GetOptionsMonths($selected) {
	$options = '<option />';
	for ($month = 1; $month < 13; $month++) {
		$selected_text = ($month == $selected) ? "selected" : "";
		$options .= "<option value='$month' $selected_text>" . date('F', mktime(0, 0, 0, $month, 1)) . "</option>";
	}
	return $options;
}

function GetOrdinal($number) {
	$ext = 'th';
	if (abs($number) % 100 < 4 || abs($number) % 100 > 20) {
		switch (abs($number) % 10) {
			case 1 :
				$ext = 'st';
				break;
			case 2 :
				$ext = 'nd';
				break;
			case 3 :
				$ext = 'rd';
				break;
		}
	}
	return $number . $ext;
}
