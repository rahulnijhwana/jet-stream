<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/importconstants.php';

class OptionUtil
{
	private $mapTable;	
	private $options = array();
	
	public function __construct($table, $field){
		$this->setMapTable($table);
		$this->setOptions($field);
	}
	
	private function setMapTable($table){
		switch($table)
		{
			case TABLE_COMPANY :
				$this->mapTable = "AccountMap";
				break;
			case TABLE_CONTACT :
				$this->mapTable = "ContactMap";
				break;
		}
	}
	
	private function getMapTable(){
		return $this->mapTable;
	}
	
	private function setOptions($field){
		$optionSetQuery = "SELECT OptionSetID FROM " . $this->getMapTable() . " WHERE FieldName = ? AND CompanyID = ?";
		$optionSetQuery = SqlBuilder()->LoadSql($optionSetQuery)->BuildSql(array(DTYPE_STRING, $field), array(DTYPE_INT, $_SESSION['company_id']));
		$optionSetResult = DbConnManager::GetDb('mpower')->GetOne($optionSetQuery);
		
		$this->optionSetID = $optionSetResult['OptionSetID'];
		
		$optionQuery = "SELECT OptionID, OptionName FROM [Option] WHERE OptionSetID = ?";
		$optionQuery = SqlBuilder()->LoadSql($optionQuery)->BuildSql(array(DTYPE_INT, $this->optionSetID));
		$optionResult = DbConnManager::GetDb('mpower')->Exec($optionQuery);
		
		foreach($optionResult as $result){
			$this->options[$result['OptionID']] = $result['OptionName'];
		}
	}
	
	/**
	 * Returns an OptionID from a given Option Value,
	 * (or if reverse flag is set),
	 * returns an Option Value from a given OptionID
	 * 
	 * Normal: Option Value -> OptionID
	 * Reverse: OptionID -> Option Value
	 * 
	 * @param mixed $value
	 * @param bool $reverse
	 */
	public function convert($value, $reverse=false){
		if($reverse){
			$val = $this->options[$value];			
		}
		else {
			$val = array_search(trim($value), $this->options);
		}		
		return $val ? $val : 0;
	}

}
