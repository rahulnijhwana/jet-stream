<?php
require_once 'class.JetstreamPage.php';

function JetstreamErrorPage($error, $error_detail, $error_text) {
	$page = new JetstreamPage();
	$module = new JetstreamModule();
	$module->title = 'Error: ' . $error;
	$module->error = $error;
	$module->error_detail = $error_detail;
	$module->error_text = $error_text;
	
	$module->AddTemplate('module_error.tpl');
	$page->AddModule($module);
	$page->Render();
	
	// Need to exit to stop whatever else it was going to do that caused the error
	exit;
	
}
