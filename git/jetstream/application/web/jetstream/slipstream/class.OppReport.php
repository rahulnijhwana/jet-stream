<?php
/**
 * This file contains the note class.
 * @package database
 */

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.RecAccount.php';
require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';
require_once BASE_PATH . '/include/class.MapLookup.php';
require_once BASE_PATH . '/slipstream/class.SavedSearchInfo.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

/**
 * The DB fields initialization
 */
class OppReport extends JetstreamModule
{
	public $title = 'Sales Process Report';
	public $template_files = array('snippet_opp_search_list.tpl');
	
	public $company_id;
	public $user_id;
	
	public $search;  // The search parameters
	
	public $query_type;
	
	public $show_no_of_pages = 10;
	public $total_records = 0;

	public $tot_pages;
	public $is_manager;
	
	public $search_results;
		
	function __construct($company_id = -1, $user_id = -1) {		
		$this->company_id = ($company_id == -1 && isset($_SESSION['company_obj']['CompanyID'])) ? $_SESSION['company_obj']['CompanyID'] : $company_id;
		$this->user_id = ($user_id == -1 && isset($_SESSION['USER']['USERID'])) ? $_SESSION['USER']['USERID'] : $user_id;
		//echo "in construct".$this->company_id."--".$this->user_id;
	}
		
	function ConvertLayoutToSql($layout, $table) {
		$pattern = "|\{(\w*?)\}|"; // Find words encased in curly braces
		return preg_replace($pattern, "' + $table.\\1 + '", "'$layout'");
	}
	
	public function Build($search) {
	//print_r( $search);
		$this->search = $search;
		$this->search_results = $this->SearchResults();
	}

	public function SearchResults() {

		$search_sql = "SELECT OP.OpportunityID, OP.ContactID,OP.CompanyID ,SalesPersonID,UserID,OpportunityName,DateEntered,DateModified,StartDate,ExpCloseDate,CloseDate,LastModifiedUserID,PrivateOpportunity,DealID, IsCancelled,IsClosed,SalesCycleLocID,OP.ResultID ResultID,ResultName,SCLname,SCLPercent,C.Text01,C.Text02,A.Text01 CompanyName FROM ot_opportunities OP left join ot_Results R on OP.ResultID=R.ResultID left join ot_SalesCycleLocation S on OP.SalesCycleLocID=S.SCLID, Contact C, Account A where OP.ContactID=C.ContactID and C.AccountID=A.AccountID and OP.CompanyID=?";
		$search_sql = SqlBuilder()->LoadSql($search_sql)->BuildSql(array(DTYPE_INT, $this->company_id));	
		
		$search_criteria = array();
		if($this->search['search_string'] != '') {
			$arr_search_string = $this->createMultiwordForSearch($this->search['search_string']);
			foreach($arr_search_string as $s_string) {
				$search_criteria[] = $s_string;
			}
		}
		
		
				
		if (($this->search['query_type'] ==4)) {
		
			if (count($search_criteria) > 0) {
				// Format the terms as the CONTAINS function requires
				array_walk($search_criteria, create_function('&$term', '$term = "\"$term*\"";'));
				$search_terms = implode(' AND ', $search_criteria);
			} else {
				$search_terms = FALSE;
			}
		
			if ($search_terms) {
				$where_clause .= " AND CONTAINS(OpportunityName, ?)";
				$param[] = array(DTYPE_STRING, $search_terms);
			}
			$search_sql .= $where_clause;		
			$search_sql = SqlBuilder()->LoadSql($search_sql)->BuildSqlParam($param);		
		}
		if (($this->search['query_type'] ==CONTACT)) {
	
			if (count($search_criteria) > 0) {
			$search_terms1 = ' (C.Text01 LIKE ';
			$count = 0;
			foreach($search_criteria as $term){
				$search_terms1 .= "'%".$term."%'";
				$count++;
				if($count != count($search_criteria))
				$search_terms1 .= ' OR  C.Text01 LIKE ';
			}
			$search_terms1 .= ')';
			
			$search_terms2 = ' (C.Text02 LIKE ';
			$count = 0;
			foreach($search_criteria as $term){
				$search_terms2 .= "'%".$term."%'";
				$count++;
				if($count != count($search_criteria))
				$search_terms2 .= ' OR  C.Text02 LIKE ';
			}
			$search_terms2 .= ')';
			} else {
				$search_terms1 = FALSE;
			}	
			if ($search_terms1) {				
				$where_clause = ' AND ('.$search_terms1 . ' OR '. $search_terms2.')';
				$param[] = array(DTYPE_STRING, $search_terms);
			}
			$search_sql .= $where_clause;		
		}
		//echo "here:".$this->search['query_type'];
		if (($this->search['query_type'] ==ACCOUNT)) {
			if (count($search_criteria) > 0) {
			// Format the terms as the CONTAINS function requires
			//array_walk($search_criteria, create_function('&$term', '$term = "%$term%";'));
			$search_terms = ' AND (A.Text01 LIKE ';
			$count = 0;
			foreach($search_criteria as $term){
				$search_terms .= "'%".$term."%'";
				$count++;
				if($count != count($search_criteria))
				$search_terms .= ' OR  A.Text01 LIKE ';
			}
			//echo "hrere:".$search_terms;
			//$search_terms = "A.Text01 LIKE ".implode("' OR A.Text01 LIKE '", $search_criteria);							
			}else {
				$search_terms = FALSE;
			}
		
			if ($search_terms) {
				//$where_clause .= " AND (?)";
				$where_clause = $search_terms .')';
				$param[] = array(DTYPE_STRING, $search_terms);
			}
			$search_sql .= $where_clause;		
			//$search_sql = SqlBuilder()->LoadSql($search_sql)->BuildSqlParam($param);		
		}
		
		//$search_sql = SqlBuilder()->LoadSql($search_sql)->BuildSqlParam($search_params);	
		if($this->search['sort_type']) {
			if($_SESSION['sort_by']=='DESC')
			$_SESSION['sort_by']= 'ASC';
			else $_SESSION['sort_by']='DESC';
			$search_sql .= " ORDER BY ".$this->search['sort_type'].' '.$_SESSION['sort_by'];
		}
				
		$search_result = DbConnManager::GetDb('mpower')->Exec($search_sql);
		
		return $search_result;
	}

	public function SearchResultsInfo() {
		$str_Info = '';
		if ($this->total_records > 0) {
			$end_record = $this->search['page_no'] * $this->search['records_per_page'];
			$start_record = $end_record - $this->search['records_per_page'] + 1;
			
			if ($end_record > $this->total_records) {
				$end_record = $this->total_records;
			}
			$str_Info = 'Records: ' . $start_record . ' - ' . $end_record . ' of ' . $this->total_records . '&nbsp;&nbsp;';
		}
		return $str_Info;
	}

	/**
	 * Retrieve the reords for a perticular page.
	 */
	public function returnTotPageRec() {
		/*if(!isset($this->search['page_no'])) {
			$this->search['page_no'] = 1;
		}*/
		$str_Info = '';
		
		if ($this->total_records > 0) {
			$start_record = $end_record - $this->search['records_per_page'] + 1;
			
			if ($end_record > $this->total_records) {
				$end_record = $this->total_records;
			}
			$str_Info = 'Records: ' . $start_record . ' - ' . $end_record . ' of ' . $this->total_records . '&nbsp;&nbsp;';
		}
		return $str_Info;
	}
	
	/**
	 * Parses the input field used in filter box.
	 * Here we can define The rules for filter values.
	 * Creates an array using each word entered in the input field.
	 */
	function ParseFilterSearchValue($value) {
		$parsedArr = array();
		$value = trim($value);
		if($value != '') {
			$parsedArr[] = '%'.$value.'%';
		}
		return $parsedArr;
	}
	
	
	public function GetAdvancedSearchOptions()	{
			$conditionalArray = array();

			/**
			 * Creates the conditional clause for Filteration.
			 */			
			$innerAccConWhereClause = '';
			$innerAccountWhereClause = '';
			$loadInnerSqlClause = array();
			$loadAccInnerSqlClause = array();
			$conditionalArray['Contact'] = false;

			if(count($this->search['advanced_filters']) > 0) {
				foreach($this->search['advanced_filters'] as $filterKey => $filterVal) {
					$innerWhereClause = '';
					if(trim($filterVal) == '') {
						continue;
					}
					$newFilterArr = explode('_', $filterKey);
					
					// This is to make the new method backward compatible with the original vers of this
					if ($newFilterArr[0] == 'account') {
						$newFilterArr[0] = 'CM';
					}
					if ($newFilterArr[0] == 'contact') {
						$newFilterArr[0] = 'CN';
					}
					
					/**
					 * Creates the conditional clause for Company or Contact fields Filteration.
					 */
					if(($newFilterArr[0] == 'CM') || ($newFilterArr[0] == 'CN')) {
						if($newFilterArr[0] == 'CN') {
							$conditionalArray['Contact'] = true;
						}
						
						$recType = strtolower(substr($newFilterArr[1], 0, 4));
						($newFilterArr[0] == 'CM') ? ($fieldPrefix = 'Account') : ($fieldPrefix = 'Contact');
						($newFilterArr[0] == 'CM') ? ($fieldPrefixAS = 'CM_') : ($fieldPrefixAS = 'CN_');
						//($newFilterArr[0] == 'CN') ? ($queryCompanyNote = false) : '';

						switch($recType) {
							case 'numb' :
								$filterStrings = $this->ParseFilterSearchValue($filterVal);

								if(is_array($filterStrings) && (count($filterStrings) > 0)) {
									$innerWhereClause .= " AND ( ";
									$newWhereClause = '';

									foreach($filterStrings as $filterStr) {
										if($newWhereClause != '') {
											$newWhereClause .= " OR $fieldPrefix.".$newFilterArr[1]." LIKE ? ";
										}
										else {
											$newWhereClause .= " $fieldPrefix.".$newFilterArr[1]." LIKE ? ";
										}
										$loadInnerSqlClause[] = array(DTYPE_INT, (int)$filterStr);
										($newFilterArr[0] == 'CM') ? ($loadAccInnerSqlClause[] = array(DTYPE_INT, (int)$filterStr)) : '';
									}
									$innerWhereClause .= $newWhereClause." ) ";
									
									($newFilterArr[0] == 'CM') ? ($innerAccountWhereClause .= $innerWhereClause) : '';
									$innerAccConWhereClause .= $innerWhereClause; 
								}
								break;

							case 'sele' :
							case 'bool' :
								$filterStr = $filterVal;

								if(trim($filterStr) != '') {
									$innerWhereClause .= " AND ( ";
									$innerWhereClause .= " $fieldPrefix.".$newFilterArr[1]." = ? ";
									(($recType == 'bool') && ($filterStr == '0')) ? ($innerWhereClause .= " OR $fieldPrefix.".$newFilterArr[1]." IS NULL ") : ('');
									$innerWhereClause .= " ) ";

									$loadInnerSqlClause[] = array(DTYPE_INT, (int)$filterStr);
									($newFilterArr[0] == 'CM') ? ($loadAccInnerSqlClause[] = array(DTYPE_INT, (int)$filterStr)) : '';
									($newFilterArr[0] == 'CM') ? ($innerAccountWhereClause .= $innerWhereClause) : '';
									$innerAccConWhereClause .= $innerWhereClause; 
								}
								break;

							case 'date' :
								/*
								 * If the entered date is not in correct format then it takes the current date.
								 */
								if(!strtotime($filterVal['StartDate'])) {
									$filterVal['StartDate'] = date('m/d/Y');
								}
								if(!strtotime($filterVal['EndDate'])) {
									$filterVal['EndDate'] = date('m/d/Y');
								}

								$filterStr['StartDate'] = date('m/d/Y', strtotime($filterVal['StartDate']));
								$filterStr['EndDate'] = date('m/d/Y', strtotime($filterVal['EndDate']));
								$innerWhereClause .= " AND CONVERT(varchar(20), $fieldPrefix.".$newFilterArr[1].", 101) >= ? ";
								$innerWhereClause .= " AND CONVERT(varchar(20), $fieldPrefix.".$newFilterArr[1].", 101) <= ? ";

								$loadInnerSqlClause[] = array(DTYPE_TIME, $filterStr['StartDate']);
								$loadInnerSqlClause[] = array(DTYPE_TIME, $filterStr['EndDate']);

								if($newFilterArr[0] == 'CM') {
									$loadAccInnerSqlClause[] = array(DTYPE_TIME, $filterStr['StartDate']);
									$loadAccInnerSqlClause[] = array(DTYPE_TIME, $filterStr['EndDate']);
								}
								($newFilterArr[0] == 'CM') ? ($innerAccountWhereClause .= $innerWhereClause) : '';
								$innerAccConWhereClause .= $innerWhereClause; 
								break;

							default:
								$filterStrings = $this->ParseFilterSearchValue($filterVal);

								if(is_array($filterStrings) && (count($filterStrings) > 0)) {
									$innerWhereClause .= " AND ( ";
									$newWhereClause = '';

									foreach($filterStrings as $filterStr) {
										if($newWhereClause != '') {
											$newWhereClause .= " OR $fieldPrefix.".$newFilterArr[1]." LIKE ? ";
										}
										else {
											$newWhereClause .= " $fieldPrefix.".$newFilterArr[1]." LIKE ? ";
										}
										$loadInnerSqlClause[] = array(DTYPE_STRING, $filterStr);

										($newFilterArr[0] == 'CM') ? ($loadAccInnerSqlClause[] = array(DTYPE_STRING, $filterStr)) : '';
									}
									$innerWhereClause .= $newWhereClause." ) ";
									($newFilterArr[0] == 'CM') ? ($innerAccountWhereClause .= $innerWhereClause) : '';
									$innerAccConWhereClause .= $innerWhereClause; 
								}
						}
						
					}
					
					/** 
					 * Search for active or inactive user .
					 */
					
					if ($newFilterArr[1] == 'Id'){
						 
						 $conditionalArray['assign'] = true ; 
					} 

				}
				
			} // End Of if loop
			$conditionalArray['acc_where_clause'] = $innerAccountWhereClause;
			$conditionalArray['acc_match_condition'] = $loadAccInnerSqlClause;
			
			$conditionalArray['where_clause'] = $innerAccConWhereClause;
			$conditionalArray['match_condition'] = $loadInnerSqlClause;
			//print_r($conditionalArray);
			return $conditionalArray;
		}

	/**
	 * Devides the entire word to multiple word.
	 */
	public function createMultiwordForSearch($search_string) {
		$search_string = trim($search_string);
		$arr_search_string = array();
		//$arr_search_string[] = $search_string;
		

		/*if(strpos($search_string, "'") !== False) {
			$search_string = str_replace("'", "", trim($search_string));
			$arr_search_string[] = $search_string;
		}*/
		
		$explode_dash_string = explode(' ', $search_string);
		
		if (count($explode_dash_string) > 0) {
			foreach ($explode_dash_string as $single_string) {
				//$arr_search_string[] = 	$single_string;
				$explode_comma_string = explode(',', $single_string);
				
				if (count($explode_comma_string) > 0) {
					foreach ($explode_comma_string as $single_comma_string) {
						$arr_search_string[] = $single_comma_string;
					}
				}
			}
		} else {
			$explode_comma_string = explode(',', $search_string);
			
			if (count($explode_comma_string) > 0) {
				foreach ($explode_comma_string as $single_comma_string) {
					$arr_search_string[] = $single_comma_string;
				}
			}
		}
		return $arr_search_string;
	}

	/**
	 * Creates the pages.
	 */
	public function getDashboardPages() {
		$pagination_link = '';
		if ($this->tot_pages > 0) {
			$left_page_no = 1;
			$right_pag_no = 0;
			$div_pages = floor($this->show_no_of_pages / 2);
			
			if ($this->tot_pages == 1) {
				return '';
			}
			
			$left_page_no = $this->search['page_no'] - $div_pages;
			if ($left_page_no < 0) {
				$right_pag_no = -($left_page_no);
				$left_page_no = 1;
			} else if ($left_page_no == 0) {
				$left_page_no = 1;
			}
			
			if (($left_page_no + $div_pages) == $this->search['page_no']) {
				$right_pag_no += $this->search['page_no'] + ($div_pages - 1);
			} else {
				$right_pag_no += $this->search['page_no'] + $div_pages;
			}
			
			if ($right_pag_no > $this->tot_pages) {
				$left_page_no = $left_page_no - ($right_pag_no - $this->tot_pages);
				$right_pag_no = $this->tot_pages;
			}
			if ($left_page_no <= 0) {
				$left_page_no = 1;
			}
			
			if ($this->search['page_no'] == 1) {
				$pagination_link .= '<img class="pageImage" border="0" alt="First" src="./images/off-first.gif" disabled />';
				$pagination_link .= '<img class="pageImage" border="0" alt="Prev" src="./images/off-prev.gif" disabled />';
			} else if ($this->search['page_no'] > 1) {
				$prev_page = $this->search['page_no'] - 1;
				
				$btn_first_event = "AjaxLoadPage(1);";
				$btn_prev_event = "AjaxLoadPage('$prev_page');";
				
				$pagination_link .= '<a href="#"><img class="pageImage" border="0" src="./images/first.gif" alt="First" title="First" onclick="javascript:' . $btn_first_event . '" /></a>';
				$pagination_link .= '<a href="#"><img class="pageImage" border="0" src="./images/prev.gif" alt="Prev" title="Previous" onclick="javascript:' . $btn_prev_event . '" /></a>';
			
			}
			
			if ($this->tot_pages >= $this->search['page_no']) {
				for($i = $left_page_no; $i <= $right_pag_no; $i++) {
					$btn_event = "AjaxLoadPage('$i');";
					
					if ($i == $this->search['page_no']) {
						$pagination_link .= '<span class="page">' . $i . '</span>';
					} else {
						$pagination_link .= '<a href="#" onclick="javascript:' . $btn_event . '"><span class="page">' . $i . '</span></a>';
					}
				}
			}
			
			if ((!$this->search['page_no']) || $this->search['page_no'] < $this->tot_pages) {
				$next_page = $this->search['page_no'] + 1;
				
				$btn_next_event = "AjaxLoadPage('$next_page');";
				$btn_last_event = "AjaxLoadPage('$this->tot_pages');";
				
				$pagination_link .= '<a href="#"><img class="pageImage" border="0" src="./images/next.gif" alt="Next" title="Next" onclick="javascript:' . $btn_next_event . '" /></a>';
				$pagination_link .= '<a href="#"><img class="pageImage" border="0" src="./images/last.gif" alt="Last" title="Last" onclick="javascript:' . $btn_last_event . '" /></a>';
			} else {
				$pagination_link .= '<img class="pageImage" alt="Next" border="0" src="./images/off-next.gif" disabled />';
				$pagination_link .= '<img class="pageImage" alt="Last" border="0" src="./images/off-last.gif" disabled />';
			}
		}
		return $pagination_link;
	}
	
	public function SaveSearch()
	{
		$sql = 'SELECT * FROM SavedSearch WHERE 0 = -1';
		$result = DbConnManager::GetDb('mpower')->Execute($sql, 'SavedSearch');

		$search_info = new SavedSearchInfo();
		$search_info->SetDatabase(DbConnManager::GetDb('mpower'));
		$result[] = $search_info;				
		$result->Initialize();

		$search_info->SavedSearchName = $this->saved_search_name;
		$search_info->SavedSearchText = $this->saved_search_text;
		$search_info->SavedSearchType = $this->saved_search_type;
		$search_info->SavedSearchTime = date('Y-m-d h:i:s');
		$search_info->SavedSearchUserID = $this->user_id;
		$search_info->SavedSearchTimeZone = $this->browser_time;
		
		$result->Save();
		
		$saved_search_id = $search_info->SavedSearchID;
		
		if($saved_search_id > 0)
		{
			return true;
		}
		return false;
	}
	
	public function GetSavedSearches($type)
	{
		$save_search_sql = "SELECT SavedSearchName, SavedSearchText AS SavedSearchLink	
			FROM SavedSearch WHERE SavedSearchType = ? AND SavedSearchUserID = ? ORDER BY SavedSearchTime DESC";		
		$save_search_sql = SqlBuilder()->LoadSql($save_search_sql)->BuildSql(array(DTYPE_INT, $type), array(DTYPE_INT, $this->user_id));		
		$save_search_arr = DbConnManager::GetDb('mpower')->Exec($save_search_sql);
		return $save_search_arr;
	}
}
