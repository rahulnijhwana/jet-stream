<?php
require_once BASE_PATH . '/slipstream/class.JetstreamPage.php';
require_once BASE_PATH . '/slipstream/class.ModuleEmailIncoming.php';
//require_once BASE_PATH . '/slipstream/class.ModuleRelatedContacts.php';

$page = new JetstreamPage();
$page->AddModule(new ModuleEmailIncoming());
//$page->AddAjaxModule(new ModuleRelatedContacts());

$page->Render();
