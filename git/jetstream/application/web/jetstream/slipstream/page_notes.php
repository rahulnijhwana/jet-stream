<?php
require_once BASE_PATH . '/slipstream/class.JetstreamPage.php';
require_once BASE_PATH . '/slipstream/class.ModuleNotesSearch.php';
require_once BASE_PATH . '/slipstream/class.ModuleNotesSort.php';
require_once BASE_PATH . '/slipstream/class.ModuleNotesFilter.php';
require_once BASE_PATH . '/slipstream/class.ModuleNotes.php';

$page = new JetstreamPage();

if(isset($_GET['Export'])) {
	//include BASE_PATH . '/slipstream/export_notes.php';
	include BASE_PATH . '/slipstream/export_xlsnotes.php';
	exit;
}
else if(isset($_GET['Print'])) {
	$page->AddModule(new ModuleNotes());
}
else {

	// Use a generic module because the dashboard search is such a simple page
	
	$page->AddModule(new ModuleNotesSearch());
	$page->AddModule(new ModuleNotesSort());
	$page->AddModule(new ModuleNotesFilter());
}

$page->Render();

if(!isset($_GET['Print'])) {
echo '<script language="javascript">
	$(document).ready(function() { 	
		performNoteSearch(1, 1);
	});
</script>';
}


