<?php
/**
 * @package slipstream
 */
 
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';

define('EMAIL_ATT_PATH', 'https://www.mpasa.com/attachments/');

/**
 * The EmailOutgoing class
 */
class EmailOutgoing
{

	function GetEmailOutgoing($personId){
		$email_outgoing = array();
		$field_map_obj = new fieldsMap();
		$email_list = $field_map_obj->getContactEmailFields();		
		$email_addresses = '';
		$email_addr = array();
		
		if(is_array($email_list) && !empty($email_list)) {
			foreach ($email_list as $key=>$email) {
				$email_addr[] = "EA.EmailAddress = C.$email";
				if ($key == 0) $email_addresses = " AND EA.EmailAddress = C.$email";
				else $email_addresses .= " OR EA.EmailAddress = C.$email";
			}
		}

		$sql = "SELECT CompanyID FROM People WHERE PersonID = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $personId));
		$result = DbConnManager::GetDb('mpower')->GetOne($sql);
		$company_id = $result['CompanyID'];
		
		$user_tz = new DateTimeZone($_SESSION['USER']['BROWSER_TIMEZONE']);
		$pending_date = new JetDateTime("now", $user_tz);
		
		$sql = "SELECT EmailOffset FROM Company WHERE CompanyID = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_id));
		$result = DbConnManager::GetDb('mpower')->GetOne($sql);
		$offset = $result['EmailOffset'];
		if ($offset > 0) $offset = $offset - 1;
//		$offset = 1;
		$pending_date->modify('-'.$offset.' days');
		
		// All
/*		$sql = "SELECT Notes.NoteID, Notes.EmailContentID, Notes.HasAlert, EmailContent.ReceivedDate, EmailContent.FromName, EmailContent.FromEmail, EmailContent.RecipientNames, EmailContent.RecipientEmails, EmailContent.Subject, EmailContent.Body
		FROM Notes INNER JOIN EmailContent ON Notes.EmailContentID = EmailContent.EmailContentID
		WHERE (Notes.CreatedBy = ?) AND (Notes.Subject = 'Email') AND (Notes.Incoming = 1) AND (Notes.ObjectType = 2)
		ORDER BY Notes.CreationDate";
*/
		// Incoming
/*		$sql = "SELECT Notes.NoteID, Notes.EmailContentID, Notes.HasAlert, EmailContent.ReceivedDate, EmailContent.FromName, EmailContent.FromEmail, EmailContent.RecipientNames, EmailContent.RecipientEmails, EmailContent.CcNames, EmailContent.Subject, EmailContent.Body, EmailAttachments.OriginalName AS Attachment
		FROM Notes INNER JOIN EmailContent ON Notes.EmailContentID = EmailContent.EmailContentID LEFT OUTER JOIN
			EmailAttachments ON EmailContent.EmailContentID = EmailAttachments.EmailContentID
		WHERE (Notes.CreatedBy = ?) AND (Notes.Subject = 'Email') AND (Notes.ObjectType = 2) AND (Notes.Incoming = 1) AND
			(Notes.EmailContentID IN (SELECT EmailContentID FROM EmailAddresses WHERE (PersonID = ?) AND (EmailAddressType = 'To' OR EmailAddressType = 'Cc')))		
		ORDER BY Notes.CreationDate";
*/
		// Outgoing
/*		$sql = "SELECT Notes.NoteID, Notes.EmailContentID, Notes.HasAlert, EmailContent.ReceivedDate, EmailContent.FromName, EmailContent.FromEmail, EmailContent.RecipientNames, EmailContent.RecipientEmails, EmailContent.CcNames, EmailContent.Subject, EmailContent.Body
		FROM Notes INNER JOIN EmailContent ON Notes.EmailContentID = EmailContent.EmailContentID
		WHERE (Notes.CreatedBy = ?) AND (Notes.Subject = 'Email') AND (Notes.ObjectType = 2) AND (Notes.Incoming = 1) AND
			(Notes.EmailContentID IN (SELECT EmailContentID FROM EmailAddresses WHERE (PersonID = ?) AND (EmailAddressType = 'From')))
		ORDER BY Notes.CreationDate";
*/
		$sql = "SELECT DISTINCT Notes.EmailContentID, Notes.HasAlert, EmailContent.ReceivedDate, EmailContent.FromName, EmailContent.FromEmail, EmailContent.RecipientNames, EmailContent.RecipientEmails, EmailContent.CcNames, EmailContent.Subject, CAST(EmailContent.Body AS VARCHAR(MAX)) AS Body
		FROM Notes INNER JOIN EmailContent ON Notes.EmailContentID = EmailContent.EmailContentID
		WHERE (Notes.CreatedBy = ?) AND (Notes.Subject = 'Email') AND (Notes.ObjectType = 2) AND (Notes.Incoming = 1) AND
		(Notes.EmailContentID IN (SELECT EmailContentID FROM EmailAddresses WHERE (PersonID = ?) AND (EmailAddressType = 'From')))
		ORDER BY EmailContent.ReceivedDate";
		
		//CAST(EmailContent.Body AS VARCHAR(MAX)) AS Body
		
		//$sqlBuild = SqlBuilder()->LoadSql($sql);	
//		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $personId), array(DTYPE_INT, $personId));		
	
/*		$params = array(
			array(DTYPE_INT, $personId),
			array(DTYPE_INT, $personId),
			array(DTYPE_INT, $personId)
		);
*/
		$params = array(
			array(DTYPE_INT, $personId),
			array(DTYPE_INT, $personId)
		);
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		$result = DbConnManager::GetDb('mpower')->Exec($sql);		

//		echo $sql;
				
		if (isset($result) && (count($result) > 0)) {
			foreach ($result as $key=>$row) {
				
//				$email_outgoing[$key]['NoteID'] = trim($row['NoteID']);
				$email_outgoing[$key]['EmailContentID'] = trim($row['EmailContentID']);
				$date = new DateTime(trim($row['ReceivedDate']), $user_tz);
				$email_outgoing[$key]['Date'] = $date->format('D n/j/Y g:i A');
				$email_outgoing[$key]['FromName'] = trim($row['FromName']);
				$email_outgoing[$key]['FromEmail'] = trim($row['FromEmail']);
				$email_outgoing[$key]['RecipientNames'] = trim($row['RecipientNames']);
				$email_outgoing[$key]['RecipientEmails'] = trim($row['RecipientEmails']);
				$email_outgoing[$key]['CcNames'] = trim($row['CcNames']);
				$email_outgoing[$key]['Subject'] = trim($row['Subject']);
				$email_outgoing[$key]['HasAlert'] = trim($row['HasAlert']);

				$date1 = strtotime(trim($row['ReceivedDate']));
				$date2 = strtotime($pending_date->format('Y-m-d H:i:s'));
//				echo date('Y-m-d H:i:s', $date1)." ".date('Y-m-d H:i:s', $date2)."<br>";
				
				if ($date1 <= $date2)
					$email_outgoing[$key]['Pending'] = 1;
				else
					$email_outgoing[$key]['Pending'] = 0;
				
				$sql = "SELECT OriginalName, StoredName
					FROM EmailAttachments
					WHERE EmailContentID = ?";

				$params = array(
						array(DTYPE_INT, $row['EmailContentID'])
				);
				$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
				$result2 = DbConnManager::GetDb('mpower')->Exec($sql);

				$attachments = "";
				if (isset($result2) && (count($result2) > 0)) {
					foreach ($result2 as $key2=>$row2) {
						$attachments = $attachments . "<a href=\"" . EMAIL_ATT_PATH . trim($row2['StoredName']) . "\" target=\"_blank\">" . trim($row2['OriginalName']) . "</a>";
					}
				}
				$email_outgoing[$key]['Attachment'] = $attachments;
				
			}
		}
		
//		echo $sql;
//		echo 'after: ' . count($email_incoming);

		return $email_outgoing;
	}
}
