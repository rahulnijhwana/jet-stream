<?php
require_once BASE_PATH . '/slipstream/class.JetstreamPage.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.DataImport.php';
require_once BASE_PATH . '/slipstream/class.DataImportUtil.php';
require_once BASE_PATH . '/slipstream/class.ModuleDataImport.php';
require_once BASE_PATH . '/slipstream/class.ModuleDataImportErrors.php';
require_once BASE_PATH . '/slipstream/class.ModuleDataMap.php';
require_once BASE_PATH . '/slipstream/class.ModuleDataCheck.php';
require_once BASE_PATH . '/include/importconstants.php';
require_once BASE_PATH . '/include/class.Csv.php';
require_once BASE_PATH . '/include/class.DebugUtil.php';
require_once BASE_PATH . '/libs/DataSource.php';

$page = new JetstreamPage();
$step = $_POST['step'];

switch($step)
{
	case 'map':
		$dataImport = new DataImport();
		$dataImport->setSelectedValues();
		$errors = $dataImport->getErrors();
		
		if(!empty($errors)){
			$page->smarty->assign('errors', $errors);
			$page->AddModule(new ModuleDataImport());	
		}
		else {
			$unique = $dataImport->getUniqueFileName();
			if(isset($unique)){
				unset($_SESSION['import_options']);
				$dataSource = new File_CSV_DataSource(FILE_UPLOAD_DIR . '/' . $dataImport->getUniqueFilename());
				$page->smarty->assign('rowheaders', $dataSource->getHeaders());
				$page->smarty->assign('myOptions', $dataImport->buildOptions());
				$page->smarty->assign('dataImport', $dataImport);
				$page->smarty->assign('csv', $dataImport->getUniqueFilename());
								
				$page->AddModule(new ModuleDataMap());
			}
		}
		break;
		
	case 'check' :
		
		$dataImport = new DataImport();
		
		$file = $_POST['csv'];		
		$dataSource = new Csv(FILE_UPLOAD_DIR . '/' . $file);


		$options = array();
		foreach($_POST['arr'] as $k => $v){
			
			$options[$k] = $v;

			if($v == 'PeopleAccount:PersonID:Name'){
				$_SESSION['assignCol'] = $k;
			}
		}
		$_SESSION['raw_options'] = $options;
		
		$headers = $dataSource->getHeaders();
		$count = $dataSource->countHeaders();
		
		$keys = array_keys($options);
		$values = array_values($options);

		$newOptions = array();
		
		for($i=0;$i<$count;$i++){
			
			if($values[$i] == '0'){
				$dataSource->removeColumn($headers[$i]);
			}
			else {
				$newOptions[$keys[$i]] = $values[$i]; 
			}
		}

		/*
		 * Set session value for subsequent steps
		 */
		$_SESSION['import_options'] = $newOptions;

		
		$newFile = $file . '.x';	
		$dataSourceNew = $dataSource->saveAs(FILE_UPLOAD_DIR . '/' . $newFile);

		/*
		 * Split up the post data and create an array of objects
		 */
		$dataImportUtilArray = DataImportUtil::buildArray($newOptions);
		
		//check for errors
		/*
		 * if errors
		 * 	show error template 
		 * 	break
		 */
		if(!$dataImport->checkForErrors($dataSourceNew, $dataImportUtilArray)){
			//DebugUtil::show($dataImport->getErrors());
			$module = new ModuleDataImportErrors();
			$module->errors = $dataImport->getErrors();
			$page->smarty->assign_by_ref('module', $module);
			$output = $page->smarty->fetch('module_data_import_errors.tpl');
			
			echo $output;
			exit;
		}
		
		
		if($dataImport->hasColumns(TYPE_COMPANY, $dataSource, $dataImportUtilArray)){
			$dataSourceCompany = $dataImport->buildCsv(TYPE_COMPANY, $newFile, $dataImportUtilArray);
			$existingCompanies = $dataImport->checkForExistingRecords(TYPE_COMPANY, $dataSourceCompany);
			$unique = $dataImport->getUniqueValues($existingCompanies);
		}
		/*
		 * Setup module for page display
	     * and assign the values to the template
		 */
		$module = new ModuleDataCheck();
		$module->title .= ' - Existing Companies';
		$module->existing = $existingCompanies;
		$module->unique = $unique;
		$module->type = TYPE_COMPANY;
		$module->step = 'import';
		$module->matchCount = count($unique);
		$module->titleClass = 'company_title';
		$module->idLabel = 'account_';
		$module->isContact = false;
		$module->csv = $newFile;
		
		$page->smarty->assign_by_ref('module', $module);
		
		if(!$module->complete){
			if($module->existing){
				$output = $page->smarty->fetch('module_data_check.tpl');
			}
			else {
				$output = $page->smarty->fetch('module_data_check_empty.tpl');
			}
		}
		else {
			$output = $page->smarty->fetch('module_data_import_complete.tpl');
		}

		echo $output;
		//weird output unless we exit here. page->render is showing an empty page
		exit;
		break;
		
	case 'import' :
		
		$file = $_POST['csv'];
		$type = $_POST['type'];
		
		$dataSource = new Csv(FILE_UPLOAD_DIR . '/' . $file);
		$dataImport = new DataImport();
		
		switch($type)
		{
			case TYPE_COMPANY :
				
				$unique = $_POST['unique'];
				$all = $_POST['all'];
				
				$existingCompanies = $dataImport->parseExisting($all, $unique);
				$dataImport->existingCompanies = $existingCompanies;
				
				$options = $_SESSION['import_options'];
				$dataImportUtilArray = DataImportUtil::buildArray($options);

				$dataSourceCompany = $dataImport->buildCsv(TYPE_COMPANY, $file, $dataImportUtilArray);
				
				/*
				 * Insert account (company) records and return id[] values
				 */
				$ids = $dataImport->buildInserts(TYPE_COMPANY, $dataSourceCompany, $existingCompanies);
				
				/*
				 * Set up assignments
				 */
				if($dataImport->hasColumns(TYPE_ASSIGN_COMPANY, $dataSource, $dataImportUtilArray)){
					
					$dataSourceAssign = $dataImport->buildCsv(TYPE_ASSIGN_COMPANY, $file, $dataImportUtilArray);
					$dataSourceAssign->appendColumn('AccountID', $ids);
					
					$dataSourceAssignNew = $dataImport->buildAssignments($dataSourceAssign);
					$dataSourceAssign->saveAs(FILE_UPLOAD_DIR . '/' . $file . '.assign_company.new');
				}
				
				$module = new ModuleDataCheck();
				
				if($dataImport->hasColumns(TYPE_CONTACT, $dataSource, $dataImportUtilArray)){
					
					$dataSourceContact = $dataImport->buildCsv(TYPE_CONTACT, $file, $dataImportUtilArray);
					$dataSourceContact->appendColumn('AccountID', $ids);
					
					$dataSourceContact->saveAs(FILE_UPLOAD_DIR . '/' . $file . '.contact.new');
				}
					
				$existingContacts = $dataImport->checkForExistingRecords(TYPE_CONTACT, $dataSourceContact);
				
				$module->existing = $existingContacts;
				$module->unique = $dataImport->getUniqueValues($existingContacts);
				$module->type = TYPE_CONTACT;
				$module->isContact = true;
				$module->title .= ' - Existing Contacts';
				$module->step = 'import';
				$module->matchCount = count($unique);				
				$module->csv = $file;
				$module->titleClass = 'contact_title';
				$module->idLabel = 'contact_';

				$page->AddModule($module);
				
				break;

				
			case TYPE_CONTACT :

				$unique = $_POST['unique'];
				$all = $_POST['all'];
				
				$existingContacts = $dataImport->parseExisting($all, $unique);
				
				$dataSourceContact = new Csv(FILE_UPLOAD_DIR . '/' . $file . '.contact.new');
				
				/*
				 * Insert contact records
				 */
				$ids = $dataImport->buildInserts(TYPE_CONTACT, $dataSourceContact, $existingContacts);

				/*
				 * Populate Contact Records with predefined autofill fields from associated Account record
				 */
				$dataImport->populate($ids);
				
				/*
				 * Setup assignment file
				 */
				//if($dataImport->hasColumns(TYPE_ASSIGN_CONTACT, $dataSource, $dataImportUtilArray)){

				if(isset($_SESSION['assignCol'])){
					
					$options = $_SESSION['import_options'];
					$options[$_SESSION['assignCol']] = TABLE_ASSIGN_CONTACT . ':PersonID:Name';
					
					$dataImportUtilArray = DataImportUtil::buildArray($options);
					
					$dataSourceAssign = $dataImport->buildCsv(TYPE_ASSIGN_CONTACT, $file, $dataImportUtilArray);
					$dataSourceAssign->appendColumn('ContactID', $ids);
					$dataSourceAssignNew = $dataImport->buildAssignments($dataSourceAssign);
					$dataSourceAssign->saveAs(FILE_UPLOAD_DIR . '/' . $file . '.assign_contact.new');

					$dataSourceAssignCompany = new Csv(FILE_UPLOAD_DIR . '/' . $file . '.assign_company.new');
					$dataSourceAssignContact = new Csv(FILE_UPLOAD_DIR . '/' . $file . '.assign_contact.new');
	
					$dataImport->buildInserts(TYPE_ASSIGN_COMPANY, $dataSourceAssignCompany, null, false);
					$dataImport->buildInserts(TYPE_ASSIGN_CONTACT, $dataSourceAssignContact, null, false);
					
				}
				unset($_SESSION['assignCol']);
				
				$module = new ModuleDataCheck();
				
				$module->title .= ' Complete';
				$module->complete = true;
				
				$page->AddModule($module);
		
				break;
		}
		
		break;
		
	default :
		
		$dataImport = new DataImport();
		$page->AddModule(new ModuleDataImport());
		
		break;
}
$page->Render();