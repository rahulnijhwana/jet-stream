<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once 'class.Status.php';

class AccountMap extends MpRecord
{

	public function __toString() {
		return 'Account Map Record';
	}

	public function Initialize() {
		$this->db_table = 'AccountMap';
		$this->recordset->GetFieldDef('AccountMapID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('CompanyID')->required = TRUE;
		$this->recordset->GetFieldDef('LabelName')->required = TRUE;
		$this->recordset->GetFieldDef('FieldName')->required = TRUE;
		//$this->recordset->GetFieldDef('KeywordId')->required = TRUE;
		parent::Initialize();
	}

	function insertAccountFieldsToMap($companyId, $fieldName, $labelName, $isCompany, $isZipCode, $isMandatory, $optionType, $validationType, $align, $optionSetId, $keywordId, $basicSearch, $parameters, $isPrivateField,$googleField) {
		$status = new Status();
		$sql = "SELECT * FROM AccountMap WHERE AccountMapID = -1";
		$rs = DbConnManager::GetDb('mpower')->Execute($sql);
		$this->SetDatabase(DbConnManager::GetDb('mpower'));
		$this->recordset = $rs;
		$this->Initialize();
		$this->CompanyID = $companyId;
		$this->FieldName = $fieldName;
		$this->LabelName = $labelName;
		$this->IsCompanyName = $isCompany;
		$this->IsZipCode = $isCompany;
		$this->IsRequired = $isMandatory;
		$this->OptionType = $optionType;
		$this->ValidationType = $validationType;
		$this->Align = $align;
		$this->OptionSetID = $optionSetId;
		$this->Enable = 0;
		$this->BasicSearch = $basicSearch;
		$this->Parameters = $parameters;
		$this->IsPrivate = $isPrivateField;
		$this->KeywordId = $keywordId;
		$this->googleField = $googleField;

		$this->Save();
		return $status;
	}

	function getAccountMapIdByCompanyAndField($companyId, $fieldName) {
		$sql = "select AccountMapID from AccountMap where CompanyID = ? and FieldName = ?";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId), array(DTYPE_STRING, $fieldName));
		$mapId = DbConnManager::GetDb('mpower')->Exec($sql);
		return $mapId;
	}

	function updateAccountMapById($mapId, $labelName, $isRequired, $isCompany, $isZipCode, $basicSearch, $tabOrder, $isPrivateField , $KeywordId) {
		$status = new Status();
		$sql = "SELECT * FROM AccountMap WHERE AccountMapID = ?";
		$accountMapSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $mapId));
		$result = DbConnManager::GetDb('mpower')->Execute($accountMapSql, 'AccountMap');
		if (count($result) == 1) {
			$objAccount = $result[0];
			$objAccount->Initialize();
			$objAccount->LabelName = $labelName;
			$objAccount->IsCompanyName = $isCompany;
			$objAccount->IsZipCode = $isZipCode;
			$objAccount->IsRequired = $isRequired;
			$objAccount->BasicSearch = $basicSearch;
			$objAccount->TabOrder = $tabOrder;
			$objAccount->IsPrivate = $isPrivateField;
			$objAccount->KeywordId = $KeywordId;
			$result->Save();
		}
		return $status;
	
	}

	function getAccountMapById($companyId) {
		$sql = 'SELECT AccountMapID,FieldName,LabelName,IsCompanyName,BasicSearch,IsRequired,ValidationType,Position,Align,IsPrivate FROM AccountMap WHERE CompanyID= ?';
		$accountSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $companyId));
		$accountMap = DbConnManager::GetDb('mpower')->Exec($accountSql);
		
		$_SESSION['USER']['ACCOUNTMAP'] = $accountMap;
		return $accountMap;
	}

	//Fetch different field values and validationName from AccountMap and Validation table for company
	function getFieldsFromAccountMapByCompId($companyId, $criteria) {
		$result = array();
		$searchCriteria = $criteria . '%';
		$sql = "SELECT AccountMapID,FieldName,LabelName,IsRequired,IsCompanyName,OptionType,ValidationType,Align,Enable,OptionSetID,SectionID,Position,TabOrder,IsPrivate FROM AccountMap where CompanyID= ? and FieldName Like ? Order By Position DESC";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId), array(DTYPE_STRING, $searchCriteria));
		$fieldNames = DbConnManager::GetDb('mpower')->Exec($sql);
		for($i = 0; $i < count($fieldNames); $i++) {
			$validationType = $fieldNames[$i]['ValidationType'];
			
			if ($validationType != '' || $validationType != NULL) {
				
				$sql = "SELECT ValidationName FROM Validation where ValidationID= ? ";
				$sqlBuild = SqlBuilder()->LoadSql($sql);
				$sql = $sqlBuild->BuildSql(array(DTYPE_STRING, $validationType));
				$validationName = DbConnManager::GetDb('mpower')->Exec($sql);
				if (isset($validationName[0]['ValidationName'])) {
					$fieldNames[$i]['ValidationName'] = $validationName[0]['ValidationName'];
				} else {
					$fieldNames[$i]['ValidationName'] = '-1';
				}
			} else {
				$fieldNames[$i]['ValidationName'] = '-1';
			}
		}
		if (count($fieldNames) > 0) $result = $fieldNames;
		
		return $result;
	}

	function updateFieldPositionByAccountMapId($align, $position, $sectionId, $accountMapId, $enable, $tabOrder) {
		$status = new Status();
		$sql = "SELECT * FROM AccountMap WHERE AccountMapID = ?";
		$accountMapSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $accountMapId));
		$result = DbConnManager::GetDb('mpower')->Execute($accountMapSql, 'AccountMap');
		if (count($result) == 1) {
			$objAccount = $result[0];
			$objAccount->Initialize();
			$objAccount->Align = $align;
			$objAccount->Position = $position;
			$objAccount->SectionID = $sectionId;
			$objAccount->Enable = $enable;
			if ($tabOrder) $objAccount->TabOrder = NULL;
			
			$result->Save();
		}
		return $status;
	}

	function getAccountMapDetailsById($accountMapId) {
		$sql = 'SELECT AccountMapID,FieldName,LabelName,ValidationType,OptionSetID,BasicSearch,IsCompanyName,IsZipCode,OptionType,IsRequired,TabOrder,Parameters,IsPrivate, KeywordId FROM AccountMap WHERE AccountMapID= ?';
		$accountSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $accountMapId));
		$accountMap = DbConnManager::GetDb('mpower')->Exec($accountSql);
		return $accountMap;
	}

	function updateOptionSetIdOfAccountMapFields($accountMapId) {
		$status = new Status();
		$sql = "SELECT * FROM AccountMap WHERE AccountMapID = ?";
		$accountMapSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $accountMapId));
		$result = DbConnManager::GetDb('mpower')->Execute($accountMapSql, 'AccountMap');
		if (count($result) == 1) {
			$objAccount = $result[0];
			$objAccount->Initialize();
			$objAccount->OptionSetID = NULL;
			$result->Save();
		}
		return $status;
	}

	function getMapIdBySectionId($sectionId) {
		$sql = 'SELECT AccountMapID,LabelName,FieldName FROM AccountMap WHERE SectionID = ? ';
		$accountSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $sectionId));
		$accountMap = DbConnManager::GetDb('mpower')->Exec($accountSql);
		return $accountMap;
	}

	function updateAccountMapSectionId($mapId) {
		$status = new Status();
		$sql = "SELECT * FROM AccountMap WHERE AccountMapID = ?";
		$accountMapSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $mapId));
		$result = DbConnManager::GetDb('mpower')->Execute($accountMapSql, 'AccountMap');
		if (count($result) == 1) {
			$objAccount = $result[0];
			$objAccount->Initialize();
			$objAccount->SectionID = NULL;
			$result->Save();
		}
		return $status;
	
	}

	function updateAccountFieldsTabOrderById($mapId, $tabOrder) {
		$status = new Status();
		$sql = "SELECT * FROM AccountMap WHERE AccountMapID = ?";
		$accountMapSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $mapId));
		$result = DbConnManager::GetDb('mpower')->Execute($accountMapSql, 'AccountMap');
		if (count($result) == 1) {
			$objAccount = $result[0];
			$objAccount->Initialize();
			$objAccount->TabOrder = $tabOrder;
			$result->Save();
		}
		return $tabOrder;
	}

	function checkIsCompanySet($companyId, $mapId) {
		$sql = 'SELECT IsCompanyName,AccountMapID FROM AccountMap WHERE CompanyID = ? ';
		$accountSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyId));
		$accountMap = DbConnManager::GetDb('mpower')->Exec($accountSql);
		foreach ($accountMap as $account) {
			if ($account['IsCompanyName'] == 1 && $account['AccountMapID'] != $mapId) return true;
		}
		return false;
	}
	
	function checkIsZipCodeSet($companyId, $mapId) {
		$sql = 'SELECT IsZipCode,AccountMapID FROM AccountMap WHERE CompanyID = ? ';
		$accountSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyId));
		$accountMap = DbConnManager::GetDb('mpower')->Exec($accountSql);
		foreach ($accountMap as $account) {
			if ($account['IsZipCode'] == 1 && $account['AccountMapID'] != $mapId) return true;
		}
		return false;
	}
	
	function getMaxAccountFieldValues($companyId) {
		$sql = 'SELECT LabelName,AccountMapID FROM AccountMap WHERE AccountMapID = (Select Max(AccountMapID) from AccountMap where CompanyID = ?)';
		$accountSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyId));
		$accountMap = DbConnManager::GetDb('mpower')->Exec($accountSql);
		return $accountMap;
	}

	function getMapIdAndFieldNameByCompanyId($companyId) {
		$sql = 'SELECT LabelName,AccountMapID FROM AccountMap WHERE CompanyID = ?';
		$accountSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyId));
		$accountMap = DbConnManager::GetDb('mpower')->Exec($accountSql);
		return $accountMap;
	}

	function getFieldsFromVcardKeywordMap( $companyId, $criteria )
	{
		$result = array( );
		
		$sql = "SELECT KeywordId, Keyword FROM VcardKeywordMapping";
		if( $criteria == 1 )
		{
			$sql.= " WHERE KeywordId NOT IN ( SELECT KeywordId FROM AccountMap WHERE CompanyID = ? ) ORDER BY KeywordId ASC";
		
			$keywordSql = SqlBuilder( )->LoadSql( $sql )->BuildSql( array( DTYPE_INT, $companyId ) );
			$sql = $keywordSql;
		}
		else
		{
			$sql .= " ORDER BY KeywordId ASC";
		}
		$keywords = DbConnManager::GetDb('mpower')->Exec( $sql );
		
		//print_r( $keywords ); exit( );
		return $keywords;
	}
	
	function getDeleteFields($companyId) {
		$sql = 'SELECT LabelName,AccountMapID FROM AccountMap WHERE CompanyID = ? AND SectionID IS NULL';
		$accountSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyId));
		$accountMap = DbConnManager::GetDb('mpower')->Exec($accountSql);
		return $accountMap;
	}

	function deleteAccountFieldById($accountMapId) {
		$sql = 'DELETE FROM AccountMap WHERE AccountMapID = ?';
		$accountSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $accountMapId));
		$accountMap = DbConnManager::GetDb('mpower')->Exec($accountSql);
		return $accountMap;
	}

	function getFieldTypeAndParametersByMapId($mapId) {
		$sql = 'SELECT FieldName,Parameters,AccountMapID as MapId FROM AccountMap WHERE AccountMapID = ?';
		$accountSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $mapId));
		$accountMap = DbConnManager::GetDb('mpower')->Exec($accountSql);
		return $accountMap;
	}

	function updateFieldParametersByMapId($mapId, $parameters) {
		$status = new Status();
		$sql = "update AccountMap set Parameters = ? WHERE AccountMapID = ?";
		$accountSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $parameters), array(DTYPE_INT, $mapId));
		$accountMap = DbConnManager::GetDb('mpower')->Execute($accountSql, 'AccountMap');
		return $status;
	}

	function updateValidationIdByMapId($mapId, $validationId) {
		$status = new Status();
		$sql = "update AccountMap set ValidationType = ? WHERE AccountMapID = ?";
		$accountSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $validationId), array(DTYPE_INT, $mapId));
		$accountMap = DbConnManager::GetDb('mpower')->Execute($accountSql, 'AccountMap');
		return $status;
	}

	function updateOptionTypeByMapId($mapId, $optionType) {
		$status = new Status();
		$sql = "update AccountMap set OptionType = ? WHERE AccountMapID = ?";
		$accountSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $optionType), array(DTYPE_INT, $mapId));
		$accountMap = DbConnManager::GetDb('mpower')->Execute($accountSql, 'AccountMap');
		return $status;
	}

	function countAccountFieldsBySectionAlign($compId, $secId, $align, $position) {
		$sql = "Select count(*) as count from AccountMap where CompanyId = ? AND SectionID = ? AND Align = ? AND Position = ?";
		$accountSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $compId), array(DTYPE_INT, $secId), array(DTYPE_INT, $align), array(DTYPE_INT, $position));
		$fieldCount = DbConnManager::GetDb('mpower')->Exec($accountSql);
		return $fieldCount[0];
	}

	function getMaxPositionBySecId($secId, $compId) {
		$sql = "select MAX(Position) as MaxPosition from AccountMap where CompanyID = ? AND SectionID = ?";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $compId), array(DTYPE_INT, $secId));
		$position = DbConnManager::GetDb('mpower')->Exec($sql);
		return $position[0]['MaxPosition'];
	}
}

?>
