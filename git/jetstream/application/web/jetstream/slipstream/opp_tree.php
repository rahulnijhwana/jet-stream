<?php

function opp_tree($cid, $uid, $extensions = array()) {
	// Generates a valid XHTML list of all directories, sub-directories, and files in $directory
	// Remove trailing slash
	//ReportingTreeLookup::LoadFromDb($cid);
	$person_rec = ReportingTreeLookup::GetRecord($cid, $uid);
	$user = "{$person_rec['FirstName']} {$person_rec['LastName']}";
	$this_checked = 'checked';
	if(!in_array($uid,$_SESSION['salesperson_list'])) $this_checked = '';		
	if(!isset($code)) $code = '';
	if(!isset($first_call)) $first_call = true;
	$code = "<ul";
	$code .= " class=\"php-file-tree\" "; 
	$code .= ">";	
	//$code .= "<li class=\"pft-directory\"><a href=\"#\">" . htmlspecialchars($user) . "</a><input type='checkbox' name='salesperonid' id='salesperonid' value='".$uid."' onclick=\"javascript:AjaxLoadPage(1);\" ".$this_checked." >";
	$code .= "<li class=\"pft-directory\"><a href=\"#\"><img src='images/plus.gif'> " . htmlspecialchars($user) . "</a><input type='checkbox' name='salesperonid' id='salesperonid' value='".$uid."' onclick=\"javascript:AjaxLoadPage(1);\" ".$this_checked." >";
	$code .= opp_tree_dir($cid, $uid, $extensions);
	$code .= "</li></ul>";
	return $code;
}

function opp_tree_dir($cid, $uid, $extensions = array(), $first_call = true) {
	// Recursive function called by opp_tree() to list directories/files
	
	if(!isset($opp_tree)) $opp_tree= '';
	// Get and sort directories/files
	//if( function_exists("scandir") ) $file = scandir($directory); 
	$file = ReportingTreeLookup::GetDirectChildren($cid, $uid);
	//print_r($file);
	if(count($file) > 0) {
	natcasesort($file);
	// Make directories first
	$files = $dirs = array();
	foreach($file as $this_file) {
	//echo "look: ".$this_file;
		if( count(ReportingTreeLookup::GetDirectChildren($cid, $this_file))>=1 ) $dirs[] = $this_file; else $files[] = $this_file;
	}
	$file = array_merge($dirs, $files);
	}
	if( count($file) > 0 ) { // Use 2 instead of 0 to account for . and .. "directories"
		//$opp_tree = "<ul";
		//if( $first_call ) { $opp_tree .= " class=\"php-file-tree\""; $first_call = false; }
		$opp_tree .= "<ul>";
		foreach( $file as $this_file ) {	
				$person_rec = ReportingTreeLookup::GetRecord($cid, $this_file);
				$user = "{$person_rec['FirstName']} {$person_rec['LastName']}";
				if( count(ReportingTreeLookup::GetDirectChildren($cid, $this_file))>=1 ) {
					// Directory
					//echo "<br/>";
					//print_r(ReportingTreeLookup::GetDirectChildren($cid, $this_file));
					$this_checked = 'checked';
					if(!in_array($this_file,$_SESSION['salesperson_list'])) $this_checked = '';	
	
					$opp_tree .= "<li class=\"pft-directory\"><a href=\"#\"><img src='images/plus.gif'> " . htmlspecialchars($user) . "</a><input type='checkbox' name='salesperonid' id='salesperonid' value='".$this_file."' onclick=\"javascript:AjaxLoadPage(1);\" ".$this_checked." >";
					//if($this_file != $uid)
					$opp_tree .= opp_tree_dir($cid, $this_file, $extensions, false);
					$opp_tree .= "</li>";
				} else {					
					// File
					// Get extension (prepend 'ext-' to prevent invalid classes from extensions that begin with numbers)
					$this_checked = 'checked';
					if(!in_array($this_file,$_SESSION['salesperson_list'])) $this_checked = '';
					$ext = "ext-" . substr($this_file, strrpos($this_file, ".") + 1); 
					$link = urlencode($this_file);
					$opp_tree .= "<li class=\"pft-file " . strtolower($ext) . "\">" . htmlspecialchars($user) . "<input type='checkbox' name='salesperonid' id='salesperonid' value='".$this_file."' onclick=\"javascript:AjaxLoadPage(1);\" ".$this_checked." >";
				}
		}
		$opp_tree .= "</ul>";
	}
	return $opp_tree;
}

