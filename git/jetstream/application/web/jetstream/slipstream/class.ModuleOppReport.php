<?php
/**
 * This file contains the note class.
 * @package database
 */

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';
require_once BASE_PATH . '/include/class.MapLookup.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

/**
 * The DB fields initialization
 */
class ModuleOppReport extends JetstreamModule
{
	public $company_id;
	public $user_id;
	public $search;  // The search parameters
	public $query_type;
	public $is_manager;
	public $search_results;
		
	function __construct($company_id = -1, $user_id = -1) {
		$this->company_id = ($company_id == -1 && isset($_SESSION['company_obj']['CompanyID'])) ? $_SESSION['company_obj']['CompanyID'] : $company_id;
		$this->user_id = ($user_id == -1 && isset($_SESSION['USER']['USERID'])) ? $_SESSION['USER']['USERID'] : $user_id;
	}	
}
