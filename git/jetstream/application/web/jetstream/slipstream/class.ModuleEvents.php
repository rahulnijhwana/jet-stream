<?php
require_once BASE_PATH . '/slipstream/class.Event.php';
require_once BASE_PATH . '/slipstream/class.CompanyContact.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.Contact.php';
require_once BASE_PATH . '/include/class.ReportingTree.php';

class ModuleEvents extends JetstreamModule
{
	public $title = 'Event Information';
	protected $javascript_files = array('overlay.js', 'jquery.js', 'jquery.bgiframe.js', 'jquery.autocomplete.js', 'event.js', 'stringexplode.js', 'ui.datepicker.js');
	protected $css_files = array('ui.datepicker.css', 'overlay.css', 'jquery.autocomplete.css');
	protected $buttons = array();
	public $sidebar = FALSE;
	public $disable_private = '';
	public $sub_action;
	public $event_contact_id;
	public $eventOppId;
	public $msg = '';
	public $is_restricted;
	public $eventOppType = 0;
	public $permitted_users = array();
	public $form_title = "Edit Event";
	public $curDate;
	public $event_recurring_info = FALSE;
	
	public $form_id = "event";
	public $submit_to = "ajax.Event.php";

	public function Build($rec_type, $rec_id, $action = 'display', $limit_access = FALSE, $prepop = FALSE) {
		$obj_event = new Event();
		switch ($action) {
			case 'edit' :
				$event_array = $obj_event->GetEventById($rec_id);
				$this->event_info = $event_array[0];
				break;
			case 'add' :
				if ($rec_type == 'contact') {
					$event_array = $obj_event->PopulateNewEvent($rec_id);
					$this->event_info = $event_array[0];
				}
				break;
		}
		$this->template_files = array('form_event_new.tpl');
		
		$this->time_array = array();
		for($hour = 0; $hour < 24; $hour++) {
			for($minute = 0; $minute < 60; $minute += 15) {
				$disp_hour = $hour;
				$ampm = 'AM';
				if ($disp_hour > 11) {
					$disp_hour -= 12;
					$ampm = 'PM';
				}
				if ($disp_hour == 0) {
					$disp_hour = 12;
				}
				$this->time_array[] = sprintf('%d:%02d %s', $disp_hour, $minute, $ampm);
			}
		}
		$this->permitted_users[$_SESSION['login_id']] = $_SESSION['USER']['FIRSTNAME'] . ' ' . $_SESSION['USER']['LASTNAME'];
		
		$permitted_user = $obj_event->GetPermittedUserList($_SESSION['login_id']);
		foreach ($permitted_user as $p) {
			$this->permitted_users[$p['OwnerPersonID']] = "{$p['FirstName']} {$p['LastName']}";
		}
	}

	public function InitAjax() {
		$this->sidebar = TRUE;
	}

	/*
	 * Loads the event and initializes variables for view and form
	 */
	public function Init() {
		$event_id = $this->eventId = (isset($_GET['eventId']) && is_numeric($_GET['eventId']) && $_GET['eventId'] > 0) ? $_GET['eventId'] : FALSE;
		$this->event_contact_id = $this->contactId = (isset($_GET['contactId']) && is_numeric($_GET['contactId']) && $_GET['contactId'] > 0) ? $_GET['contactId'] : FALSE;
		$this->curDate = "var cur_date = '" . json_encode(date("m/d/Y")) . "';";
		
		if (!$event_id) {
			$this->SetupAdd();
		}
		
		// $action = (isset($_GET['action'])) ? trim($_GET['action']) : FALSE;
		
		if ($event_id) {
			$obj_event = new Event();
			$this->is_restricted = $obj_event->IsRestricted($event_id, $_SESSION['login_id'], $_SESSION['company_obj']['CompanyID']);
		} else {
			$this->is_restricted = FALSE;
		}
		
		if ($this->is_restricted) {
			$this->msg = 'You are not allowed to view event information';
			return;
		}
		
		if ($event_id > 0) {
			// Load event from database
			$event_array = $obj_event->GetEventById($_GET['eventId']);
			$event_details = $event_array[0];
			
			$date_form = '';
//			if (isset($_GET['date'])) {
//				$event_details['IsCancelled'] = 0;
//				$event_details['IsClosed'] = 0;
//				$date_form = $_GET['date'];
//				$event_details['date_form'] = $date_form;
//				$event_details['date_string'] = $date_form;
//			}
			
			$this->contactId = $event_details['ContactID'];
			$this->ContactName = $event_details['Contact'];
			if (trim($event_details['CompanyName']) != '') {
				$this->ContactName .= ' (' . $event_details['CompanyName'] . ')';
			}
			
			if ($event_details['IsCancelled'] == 1) {
				$this->formatting = 'content_inactive';
			} elseif ($event_details['PrivateEvent'] == 1) {
				$this->formatting = 'content_private';
			}
			
			// Add the appropriate buttons to the module
			if ($event_details['IsCancelled'] != 1 && $event_details['IsClosed'] != 1) {
				if (isset($event_details['DealID']) && ($event_details['DealID'] > 0)) {
					$this->AddButton('', "javascript:openOppPopup({$event_details['DealID']}, {$event_details['UserID']})", 'Edit');
					$this->AddButton('', "javascript:openOppPopup({$event_details['DealID']}, {$event_details['UserID']})", 'Complete');
					$this->AddButton('', "javascript:openOppPopup({$event_details['DealID']}, {$event_details['UserID']})", 'Remove');
				} else {
					$this->AddButton('', "javascript:ShowAjaxForm('Events&eventid=$event_id&action=edit');", 'Edit');
					$this->AddButton('', "javascript:editEvent($event_id, 'EVENT', 0, 0, '$date_form');", 'Edit');
					$this->AddButton('', "javascript:closePendingEvent($event_id, '', '', 'EVENT', true, '$date_form');", 'Complete');
					$this->AddButton('', "javascript:cancelPendingEvent($event_id, 'EVENT', '$date_form');", 'Remove');
				}
			}
			
			/* If the subaction is add, then the new event form is created from the old completed event.
			Else the event form is an edit form. */
			if (isset($_GET['sub_action']) && ($_GET['sub_action'] == 'add')) {
				$this->eventId = '';
			}
			
			if (isset($_GET['date'])) {
				$this->eventId = '';
			}
			
			$this->event_info = $event_details;
			if (!empty($event_details['recurring'])) {
				$this->event_recurring_info = json_encode($event_details['recurring']);
			}
			$this->AddTemplate('module_event_info.tpl');
		}
	}

	/*
	 * Loads the current contact info to pre-populate the add event form
	 */
	protected function SetupAdd() {
		if ($this->event_contact_id) {
			$obj_contact = new Contact();
			$this->is_restricted = $obj_contact->IsRestricted($this->event_contact_id, $_SESSION['login_id'], $_SESSION['company_obj']['CompanyID']);
		} else {
			return;
		}
		
		if ($this->is_restricted) {
			$this->msg = 'You are not allowed to view event information';
			return;
		}
		
		$this->form_title = 'Add Event';
		
		// Default date is today
		$this->event_info = array('date_form' => NormalDateFormat(time()));
		
		$obj_con = new CompanyContact();
		$contact_info = $obj_con->getAccountContact($this->event_contact_id);
		$contact_name = $contact_info['Contact'];
		$this->ContactName = $contact_name;
		
		if ($contact_info['PrivateContact'] == 1) {
			$this->disable_private = '<img src="images/checkbox_checked.png"><input name="CheckPrivate" id="CheckPrivate" type="hidden" value="TRUE" />';
		} else {
			$this->show_private = TRUE;
		}
	}

	public function DrawForm() {
		// This is a dummy function to keep the form template happy
		return;
	}

	/*
	 * Module wrapper to retreive the event type selections
	 */
	function GetEventTypes() {
		return EventTypeLookup::GetSelection($_SESSION['company_obj']['CompanyID'], $this->eventOppType);
	}
}

?>
