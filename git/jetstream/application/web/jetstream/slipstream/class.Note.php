<?php
require_once BASE_PATH . '/include/lib.timezone.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.RecAccount.php';
require_once BASE_PATH . '/include/class.fieldsMap.php';
require_once BASE_PATH . '/include/class.DebugUtil.php';
require_once BASE_PATH . '/include/class.FormUtil.php';
require_once BASE_PATH . '/slipstream/class.Alert.php';
require_once BASE_PATH . '/slipstream/class.Status.php';
require_once BASE_PATH . '/slipstream/class.Email.php';
require_once BASE_PATH . '/slipstream/class.NoteInfo.php';
require_once BASE_PATH . '/slipstream/class.User.php';
require_once BASE_PATH . '/slipstream/class.CompanyContact.php';

define('MAX_PRINT_NOTES_LIMIT', 50);

/* Set the mssql textlimit to the max size.
ini_set('mssql.textlimit', 20971520);
ini_set('mssql.textsize', 20971520);

NOTE: This is now done in php.ini
*/

class Note {
    
    public $noteCreator;
    public $noteCreationDate;
    public $noteSubject;
    public $noteText;
    public $notePrivate;
    public $noteObjectType;
    public $noteObject;
    public $noteEmailContentID; 
    public $noteSplType;
    public $pageNo;
    public $filterOptions;
    public $filterSelected;
    public $searchString;
    public $paginationFor;
    public $sortType;
    private $totPages;
    private $pageRecords;
    public $noteFilterArray;
    public $sortAs;
    public $noteExport;
    public $totalRecords;
    public $cutTextColumn;
    public $notePrint;
    public $notesInactivated;
    public $loadFirst = false;
    public $people;
    public $option_set;
    public $localTimezone;
    
    public $alertUsers;
    public $alertStandard;
    public $alertEmail;
    public $delayUntil;
    
    public $company_id;
    public $user_id;
    public $user_tz;
    
    private $isShowAll;

    
    function __construct($company_id = -1, $user_id = -1) { 
    
        $this->company_id = ($company_id == -1) ? (int) $_SESSION['USER']['COMPANYID'] : (int) $company_id;
        $this->user_id = ($user_id == -1) ? (int) $_SESSION['USER']['USERID'] : (int) $user_id;

        //$this->user_tz = new DateTimeZone(($user_id == -1) ? $_SESSION['USER']['BROWSER_TIMEZONE'] : $user_tz);
        // for Google Sync
        $user_tz = isset($_SESSION['USER']['BROWSER_TIMEZONE']) ? $_SESSION['USER']['BROWSER_TIMEZONE'] : 'America/Chicago';
        $this->user_tz = new DateTimeZone($user_tz);
        
        /* Set the number of Records shown per page. */
        if(isset($_SESSION['NotesPerPage']) && is_numeric($_SESSION['NotesPerPage'])){
            $this->pageRecords = $_SESSION['NotesPerPage'];
        }
        else {
            $this->pageRecords = 5;
        }   

        /* Set the number of pages shown. */
        $this->showNoOfpages = 10;

        $this->noteExport = false;
        $this->cutTextColumn = 120;
        $this->notePrivate = 0; 

        $this->isShowAll = $this->getShowAllNotes($this->company_id);
        //$this->isShowAll = false;

    }

    /**
     * function to create the pagination links for notes.
     * @param int $type - pass the pagination type ~ 1. To display the searched notes or 2.To display the notes for company/contact/event notes.
     * return true, if it is pagination link.
     */
    public function getPages($type='') {
        
        $pageinationLink = '';
        
        if($this->totPages > 0) {

            $leftPageNo = 1;
            $rightPageNo = 0;
            $divPages = floor($this->showNoOfpages / 2);
            
            if($this->totPages == 1) {
                return '';
            }

            $leftPageNo = $this->pageNo - $divPages;
            if($leftPageNo < 0) {
                $rightPageNo = -($leftPageNo);
                $leftPageNo = 1;
            }
            else if($leftPageNo == 0) {
                $leftPageNo = 1;
            }

            if(($leftPageNo + $divPages) == $this->pageNo) {
                $rightPageNo += $this->pageNo + ($divPages - 1);
            }
            else {
                $rightPageNo += $this->pageNo + $divPages;
            }

            if($rightPageNo > $this->totPages) {
                $leftPageNo = $leftPageNo - ($rightPageNo - $this->totPages);
                $rightPageNo = $this->totPages;
            }
            if($leftPageNo <= 0) {
                $leftPageNo = 1;
            }

            if($this->pageNo == 1) {
                $pageinationLink .= '<img class="pageImage" border="0" alt="First" src="./images/off-first.gif" disabled />';
                $pageinationLink .= '<img class="pageImage" border="0" alt="Prev" src="./images/off-prev.gif" disabled />';
            }
            else if($this->pageNo > 1) {
                $prevPage = $this->pageNo - 1;

                if($this->paginationFor == 'search') {
                    $btnFirstEvent= "performNoteSearch(1);";
                    $btnPrevEvent= "performNoteSearch('$prevPage');";
                }
                else if($this->paginationFor == 'opp') {
                    $btnFirstEvent= "load_page(1);";
                    $btnPrevEvent= "load_page('$prevPage');";
                }
                else {
                    $btnFirstEvent= "loadData('$this->noteObjectType','$this->noteObject',1);";
                    $btnPrevEvent= "loadData('$this->noteObjectType','$this->noteObject','$prevPage');";
                }
                $pageinationLink .= '<a href="javascript:void(0);"><img class="pageImage" border="0" src="./images/first.gif" alt="First" title="First" onclick="javascript:'.$btnFirstEvent.'" /></a>';
                $pageinationLink .= '<a href="javascript:void(0);"><img class="pageImage" border="0" src="./images/prev.gif" alt="Prev" title="Previous" onclick="javascript:'.$btnPrevEvent.'" /></a>';

            }

            if($this->totPages >= $this->pageNo) {
                for($i = $leftPageNo; $i <= $rightPageNo; $i++) {
                    if($this->paginationFor == 'search') {
                        $btnEvent= "performNoteSearch('$i');";
                    }
                    else if($this->paginationFor == 'opp') {
                        $btnEvent= "load_page('$i');";
                    }
                    else {
                        $btnEvent= "loadData('$this->noteObjectType','$this->noteObject','$i');";
                    }
                    if($i == $this->pageNo) {
                        $pageinationLink .= '<span class="page">'.$i.'</span>';
                    }
                    else {
                        $pageinationLink .= '<a href="javascript:void(0);" onclick="javascript:'.$btnEvent.'"><span class="page">'.$i.'</span></a>';
                    }
                }
            }

            if((!$this->pageNo) || $this->pageNo < $this->totPages) {
                    $nextPage = $this->pageNo + 1;

                    if($type == 'search') {
                        $btnNextEvent= "performNoteSearch('$nextPage');";
                        $btnLastEvent= "performNoteSearch('$this->totPages');";
                    }
                    else if($this->paginationFor == 'opp') {
                        $btnNextEvent= "load_page('$nextPage');";
                        $btnLastEvent= "load_page('$this->totPages');";
                    }
                    else {
                        $btnNextEvent= "loadData('$this->noteObjectType','$this->noteObject','$nextPage');";
                        $btnLastEvent= "loadData('$this->noteObjectType','$this->noteObject','$this->totPages');";
                    }

                    $pageinationLink .= '<a href="javascript:void(0);"><img class="pageImage" border="0" src="./images/next.gif" alt="Next" title="Next" onclick="javascript:'.$btnNextEvent.'" /></a>';
                    $pageinationLink .= '<a href="javascript:void(0);"><img class="pageImage" border="0" src="./images/last.gif" alt="Last" title="Last" onclick="javascript:'.$btnLastEvent.'" /></a>';
            }
            else {
                $pageinationLink .= '<img class="pageImage" alt="Next" border="0" src="./images/off-next.gif" disabled />';
                $pageinationLink .= '<img class="pageImage" alt="Last" border="0" src="./images/off-last.gif" disabled />';
            }
        }
        return $pageinationLink;
    }


    /**
     * function to show the number of records found in this page.
     * return the pageinfo ~ The number of records showing in the page out of Total number of records.
     */
    public function getPageInfo() {
        
        $pageInfo = '';

        if($this->totalRecords > 0) {
            if(!$this->pageNo) {
                $this->pageNo = 1;
            }
            $firstRec = (($this->pageNo - 1) * $this->pageRecords) + 1;
            $lastRec = ($firstRec + $this->pageRecords) - 1;
            if($lastRec > $this->totalRecords) {
                $lastRec = $this->totalRecords;
            }
            $pageInfo = '<span style="color: #899eb3;"><b>Records </b></span>'. $firstRec . '-' . $lastRec . ' of ' . $this->totalRecords.'&nbsp;&nbsp;&nbsp;';
        }
        return $pageInfo;
    }

    /**
     * function to create a new note.
     * return the status of the saved note.
     */
    function createNote() {
        $status = new Status();
        $status = $this->doValidateNewNote();

        if ($status->status == 'OK') {
            try {
                $sql = 'SELECT * FROM Notes WHERE 0 = -1';
                $result = DbConnManager::GetDb('mpower')->Execute($sql, 'NoteInfo');

                $note_info = new NoteInfo();
                $note_info->SetDatabase(DbConnManager::GetDb('mpower'));
                $result[] = $note_info;             
                $result->Initialize();

                $note_info->CreatedBy = $this->noteCreator;
                $note_info->CreationDate = $this->noteCreationDate;
                $note_info->Subject = $this->noteSubject;
                $note_info->NoteText = $this->noteText;
                $note_info->ObjectType = $this->noteObjectType;
                $note_info->ObjectReferer = $this->noteObject;
                $note_info->EmailContentID = $this->noteEmailContentID;
                $note_info->CompanyID = $this->company_id;
                $note_info->CreatedTimezone = $_SESSION['USER']['BROWSER_TIMEZONE'];
                
                
                if(isset($this->noteSplType) && ($this->noteSplType > 0)) {
                    /*
                     * NoteSpecialType is set to 1 if the note is a call.
                     * This is set to 2 if the note is an email.
                     */
                    $note_info->NoteSpecialType = $this->noteSplType;
                }

                switch($this->noteObjectType) {
                    case NOTETYPE_COMPANY:
                        $main_table = 'Account';
                        $table_pkid = 'AccountID';
                        $table_private_field = 'PrivateAccount';
                        break;
                    case NOTETYPE_CONTACT:
                        $main_table = 'Contact';
                        $table_pkid = 'ContactID';
                        $table_private_field = 'PrivateContact';
                        break;
                    case NOTETYPE_EVENT:
                    case NOTETYPE_TASK:
                        $main_table = 'Event';
                        $table_pkid = 'EventID';
                        $table_private_field = 'PrivateEvent';
                        break;
                    default:
                        $main_table = '';
                        $table_pkid = '';
                        $table_private_field = '';
                }

                if($this->CheckPrivateType($this->noteObjectType, $this->noteObject)) {
                    $this->notePrivate = 1;
                }

                $note_info->PrivateNote = $this->notePrivate;
                
                if($this->alertUsers){
                    $note_info->HasAlert = true;
                }
                
                $result->Save();
                
                $NoteID = $note_info->NoteID;
                $status->setFormattedVar($NoteID);

                /*  
                 * Create Alert if users have been selected
                 */
                if($this->alertUsers){

                    $alert = new Alert(ALERT_TYPE_BASIC,
                                        $this->alertStandard,
                                        $this->alertEmail,
                                        $this->delayUntil,
                                        RECORD_TYPE_NOTE,
                                        $NoteID,
                                        $note_info->CreatedBy,
                                        $note_info->CreationDate,
                                        $this->alertUsers);
                    $alert->save();
                    
                    if($this->alertEmail && (!$this->delayUntil)){
                        system('/usr/bin/python /usr/local/scripts/alert.py >> /var/log/alert.log');
                    }
                }
                
            }
            catch (Exception $e) {
                $status->addError($e->getMessage());
            }
        }
        

        return $status->getStatus();
    }

    /**
     * function to validates the data entered for new Note before inserting to the db.
     * return the error if any error occurs, else returns OK.
     */
    function doValidateNewNote() {
        $status = new Status();
        //$objUtil = new Util();

        // The Note Subject field validation.
        if (!isset($this->noteSubject) || trim($this->noteSubject) == "") {
            $status->addError('The Subject of Note can\'t be left blank.', 'NoteSubject');
        }

        // The Note Text field validation.
        if (!isset($this->noteText) || trim($this->noteText) == "") {
            $status->addError('The Text of Note can\'t be left blank.', 'NoteText');
        }       
        
        return $status;
    }

    /**
     * Return the list of Contact Notes or Account Notes or Event Notes.
     */
    function getNoteList() {
        $status = new Status();
        $noteArr = array();
        try {
            
            if($this->noteObjectType == NOTETYPE_OPPORTUNITY) {
                $sqlNoteCnt = "
SELECT (
    SELECT 
        COUNT(*) 
    FROM Notes                  
    WHERE CompanyId= ? AND ObjectType=? AND ObjectReferer=? AND ((PrivateNote = 1 AND CreatedBY = ?) OR (PrivateNote = 0))) + ' ' + (
        SELECT 
            Count(*) 
        FROM Notes                      
        INNER JOIN Event ON Notes.ObjectReferer = Event.EventID AND Notes.ObjectType = ?
        INNER JOIN opportunities ON Event.DealID = opportunities.DealID                 
        WHERE opportunities.DealID = ? AND Notes.CompanyId= ? AND ((Notes.PrivateNote = 1 AND Notes.CreatedBY = ?) OR (Notes.PrivateNote = 0))) AS TOTREC ";
                $sqlNoteCnt = SqlBuilder()->LoadSql($sqlNoteCnt)->BuildSql(array(DTYPE_INT, $this->company_id), 
                    array(DTYPE_INT, $this->noteObjectType), array(DTYPE_INT, $this->noteObject), array(DTYPE_INT, $this->user_id),
                    array(DTYPE_INT, NOTETYPE_EVENT), array(DTYPE_INT, $this->noteObject), array(DTYPE_INT, $this->company_id),
                    array(DTYPE_INT, $this->user_id));
            }
            else if($this->noteObjectType == NOTETYPE_CONTACT) {
                $sqlNoteCnt = "
                    SELECT (
                        SELECT 
                            COUNT(*) 
                        FROM Notes                  
                        WHERE CompanyId= ? 
                        AND ( ObjectType = ? AND ObjectReferer = ? 
                                AND (Incoming IS NULL OR Incoming = 0)
                                AND ((PrivateNote = 1 AND CreatedBY = ?) OR (PrivateNote = 0)))
                            )
                                    + ' ' + (
                        SELECT 
                            Count(*) 
                        FROM Notes                      
                        INNER JOIN Event ON Notes.ObjectReferer = Event.EventID AND Notes.ObjectType = ?
                        WHERE Event.ContactID = ? AND Notes.CompanyId= ? AND ((Notes.PrivateNote = 1 AND Notes.CreatedBY = ?) OR (Notes.PrivateNote = 0)))
                                    + ' ' + ( 
                        SELECT 
                            Count(*) 
                        FROM Notes                      
                        INNER JOIN opportunities ON Notes.ObjectReferer = opportunities.DealID AND Notes.ObjectType = ?                         
                        WHERE opportunities.ContactID = ? AND Notes.CompanyId= ? AND ((Notes.PrivateNote = 1 AND Notes.CreatedBY = ?) OR (Notes.PrivateNote = 0))) AS TOTREC ";
                $sqlNoteCnt = SqlBuilder()->LoadSql($sqlNoteCnt)->BuildSql(
                    array(DTYPE_INT, $this->company_id), 
                    array(DTYPE_INT, $this->noteObjectType),
                    array(DTYPE_INT, $this->noteObject),
                    array(DTYPE_INT, $this->user_id),
                    array(DTYPE_INT, NOTETYPE_EVENT),
                    array(DTYPE_INT, $this->noteObject),
                    array(DTYPE_INT, $this->company_id),
                    array(DTYPE_INT, $this->user_id),
                    array(DTYPE_INT, NOTETYPE_OPPORTUNITY),
                    array(DTYPE_INT, $this->noteObject),
                    array(DTYPE_INT, $this->company_id),
                    array(DTYPE_INT, $this->user_id)
                );
            } else if ($this->noteObjectType == NOTETYPE_COMPANY && $this->isShowAll ) {
                $accountId = $this->noteObject;
                $aContact = $this->getContactsByCompanyId($accountId);
                $contacts = implode(',',$aContact);
            
                /*start:JET-1, Display of Company notes*/
                if (trim($contacts) == '') {
                   $contacts = -1;
                 }
                 /*end:JET-1*/





        
                $sqlNoteCnt = "
                SELECT (
                    (
                    SELECT COUNT(*) 
                    FROM Notes                  
                    WHERE CompanyId = ? 
                    AND ObjectType = ? AND ObjectReferer in (" . $contacts . ")" . "
                    AND (Incoming IS NULL OR Incoming = 0)
                    AND PrivateNote = 0
                    )
                    + ' ' + 
                    (
                    SELECT Count(*) 
                    FROM Notes
                    INNER JOIN Event ON Notes.ObjectReferer = Event.EventID AND Notes.ObjectType = ?
                    WHERE Event.ContactID in (" . $contacts . ") AND Notes.CompanyId= ? AND Notes.PrivateNote = 0
                    )
                    + ' ' + 
                    ( 
                    SELECT Count(*) 
                    FROM Notes                      
                    WHERE ObjectType = ? AND ObjectReferer = ? ) 
                    + ' ' + 
                    (
                    SELECT Count(*) 
                    FROM Notes
                    INNER JOIN opportunities ON Notes.ObjectReferer = opportunities.DealID AND Notes.ObjectType = ?                         
                    WHERE ( opportunities.ContactID in (" . $contacts . ") OR opportunities.CompanyID = ? )
                    AND Notes.PrivateNote = 0
                    )
                ) AS TOTREC ";
                $sqlNoteCnt = SqlBuilder()->LoadSql($sqlNoteCnt)->BuildSql(
                    array(DTYPE_INT, $this->company_id),
                    array(DTYPE_INT, NOTETYPE_CONTACT),
                    
                    array(DTYPE_INT, NOTETYPE_EVENT),
                    //array(DTYPE_INT, $this->user_id),
                    array(DTYPE_INT, $this->company_id),
                    
                    array(DTYPE_INT, NOTETYPE_COMPANY),
                    array(DTYPE_INT, $accountId),

                    array(DTYPE_INT, NOTETYPE_OPPORTUNITY),
                    array(DTYPE_INT, $this->company_id)
                );
            } else {
                $sqlNoteCnt = "
SELECT 
    COUNT(*) AS TOTREC 
FROM Notes                  
WHERE CompanyId= ? AND ObjectType=? AND ObjectReferer=? AND ((PrivateNote = 1 AND CreatedBY = ?) OR (PrivateNote = 0))";
                $sqlNoteCnt = SqlBuilder()->LoadSql($sqlNoteCnt)->BuildSql(array(DTYPE_INT, $this->company_id), 
                    array(DTYPE_INT, $this->noteObjectType), array(DTYPE_INT, $this->noteObject), array(DTYPE_INT, $this->user_id));
            }
            $resultNoteCnt = DbConnManager::GetDb('mpower')->Exec($sqlNoteCnt);
    
            $totRecords = $resultNoteCnt[0]['TOTREC'];
            $this->totalRecords = $totRecords;
            $this->totPages = ceil($totRecords / $this->pageRecords);
            
            if ($this->pageNo > 1) {
                $startRecord = (($this->pageNo - 1) * ($this->pageRecords)) + 1;
                $endRecord = $startRecord + ($this->pageRecords - 1);
            } else {
                $this->pageNo = 1;
                $startRecord = 1;
                $endRecord = $startRecord + ($this->pageRecords - 1);
            }
            
            if($this->noteObjectType == NOTETYPE_CONTACT) {
                $sqlNote = "
SELECT 
    N.PrivateNote, N.EmailAddressType AS AddressType, N.NoteID, N.CreatedBy, N.Subject, N.NoteText, N.ObjectType, N.NoteSpecialType, N.CreatedTimezone, N.HasAlert,
    CONVERT(VARCHAR(20), N.CreationDate, 20) as CreationDate, N.ObjectReferer, EC.Subject AS EmailSub, EC.Body AS EmailText,
    EC.EmailContentID , EC.FromName, EC.FromEmail, EC.RecipientNames, EC.RecipientEmails, EC.CcNames, EC.CcEmails, CONVERT(varchar, EC.ReceivedDate, 20) AS ReceivedDate,
    U.FirstName, U.LastName, N.EventID, N.EventName, N.EventStartDate, N.EventStartTime, N.IsClosedEvent, N.IsCancelledEvent, N.OppSalesPerson, N.Category,
    N.OppID, N.PersonID, A.AlertID, A.DelayUntil, TZ.zname 
FROM (
    SELECT 
        ROW_NUMBER() OVER (ORDER BY CreationDate DESC) AS RowNumber, M.* 
    FROM (
        SELECT 
            Notes.*, EventID=NULL, EventStartDate=NULL, EventStartTime=NULL, EventName = NULL,
            IsClosedEvent = NULL, IsCancelledEvent = NULL, Category=NULL, OppSalesPerson=NULL, OppID=NULL, PersonID=NULL
        FROM Notes 
        WHERE ObjectType=? AND ObjectReferer=? AND  (Incoming IS NULL OR Incoming = 0) AND ((PrivateNote = 1 AND CreatedBy = ?) OR (PrivateNote = 0))

        UNION ALL
         
        SELECT 
            Notes.*, Event.EventID, CONVERT(varchar(10), Event.StartDate, 20) AS EventStartDate, 
            CONVERT(varchar, Event.StartDate, 8) AS EventStartTime, ET.EventName,
            Event.Closed AS IsClosedEvent, Event.Cancelled AS IsCancelledEvent, Category=NULL, OppSalesPerson=NULL, OppID=NULL, PersonID=NULL 
        FROM Notes
        INNER JOIN Event ON Notes.ObjectReferer = Event.EventID AND Notes.ObjectType = ?    
        LEFT JOIN EventType ET ON Event.EventTypeID = ET.EventTypeID
        WHERE Event.ContactID = ?
        
        UNION ALL 
                            
        SELECT 
            Notes.*, EventID=NULL, EventStartDate=NULL, EventStartTime=NULL, EventName = NULL,
            IsClosedEvent = NULL, IsCancelledEvent = NULL, opportunities.Category,people.FirstName+' '+people.LastName AS OppSalesPerson, opportunities.DealID AS OppID, 
            opportunities.PersonID AS PersonID 
        FROM Notes
        INNER JOIN opportunities ON Notes.ObjectReferer = opportunities.DealID AND Notes.ObjectType = ? 
        LEFT JOIN people ON opportunities.PersonID = people.PersonID                    
        WHERE opportunities.ContactID = ? 
        ) M
    ) N
    LEFT JOIN EmailContent AS EC ON N.EmailContentID = EC.EmailContentID AND EC.EmailContentID IS NOT NULL
    LEFT JOIN Alert AS A ON N.NoteID = A.RecordID AND RecordTypeID = 1
    LEFT JOIN people AS U ON N.CreatedBy = U.PersonID
    LEFT JOIN timezones AS TZ ON U.timezone = TZ.zid
WHERE N.CompanyId= ? AND ((PrivateNote = 1 AND N.CreatedBy = ?) OR (PrivateNote = 0)) AND RowNumber BETWEEN ? AND ?";

                $sqlNote = SqlBuilder()->LoadSql($sqlNote)->BuildSql(
                    array(DTYPE_INT, $this->noteObjectType),
                    array(DTYPE_INT, $this->noteObject), 
                    array(DTYPE_INT, $this->user_id), 
                    array(DTYPE_INT, NOTETYPE_EVENT),
                    array(DTYPE_INT, $this->noteObject), 
                    array(DTYPE_INT, NOTETYPE_OPPORTUNITY),
                    array(DTYPE_INT, $this->noteObject),
                    array(DTYPE_INT, $this->company_id),
                    array(DTYPE_INT, $this->user_id), 
                    array(DTYPE_INT, (int)$startRecord),
                    array(DTYPE_INT, (int)$endRecord)
                );
            }
            else if($this->noteObjectType == NOTETYPE_OPPORTUNITY) {
                $sqlNote = "
SELECT 
    PrivateNote, CreatedTimezone, NoteID, CreatedBy, CONVERT(VARCHAR(20), CreationDate, 20) as CreationDate,
    Subject, NoteText, ObjectType, ObjectReferer, NoteSpecialType, HasAlert, U.FirstName, U.LastName, TZ.zname, 
    EventID, EventName, EventStartDate, EventStartTime, IsClosedEvent, IsCancelledEvent, RowNumber 
FROM (
    SELECT 
        ROW_NUMBER() OVER (ORDER BY NoteID DESC) AS RowNumber, M.* 
    FROM (
        SELECT 
            Notes.*, EventID=NULL, EventStartDate=NULL, EventStartTime=NULL, EventName=NULL,IsClosedEvent = NULL, IsCancelledEvent = NULL 
        FROM Notes 
        WHERE ObjectType=? AND ObjectReferer=? 
        
        UNION ALL
         
        SELECT 
            Notes.*, Event.EventID, CONVERT(varchar(10), Event.StartDate, 20) AS EventStartDate, 
            CONVERT(varchar, Event.StartDate, 8) AS EventStartTime, ET.EventName, Event.Closed AS IsClosedEvent, 
            Event.Cancelled AS IsCancelledEvent 
        FROM Notes
        INNER JOIN Event ON Notes.ObjectReferer = Event.EventID AND Notes.ObjectType = ?
        LEFT JOIN EventType ET on Event.EventTypeID = ET.EventTypeID                        
        INNER JOIN opportunities ON Event.DealID = opportunities.DealID
        WHERE opportunities.DealID = ?
        ) M
    ) _myResults
    LEFT JOIN people AS U ON CreatedBy = U.PersonID
    LEFT JOIN timezones AS TZ ON U.timezone = TZ.zid
    WHERE _myResults.CompanyId= ?   AND ((PrivateNote = 1 AND CreatedBY = ?) OR (PrivateNote = 0)) AND RowNumber BETWEEN ? AND ?";
                
                $sqlNote = SqlBuilder()->LoadSql($sqlNote)->BuildSql(array(DTYPE_INT, $this->noteObjectType), array(DTYPE_INT, $this->noteObject),
                    array(DTYPE_INT, NOTETYPE_EVENT), array(DTYPE_INT, $this->noteObject), array(DTYPE_INT, $this->company_id), 
                    array(DTYPE_INT, $this->user_id), array(DTYPE_INT, (int)$startRecord), array(DTYPE_INT, (int)$endRecord));
                
            } else if ($this->noteObjectType == NOTETYPE_COMPANY && $this->isShowAll ) {
                $accountId = $this->noteObject;
                $aContact = $this->getContactsByCompanyId($accountId);
                $contacts = implode(',',$aContact);
        

      /*start:JET-1, Display of Company notes*/
                if (trim($contacts) == '') {
                   $contacts = -1;
                 }
                 /*end:JET-1*/



              $sqlNote = "
                    SELECT 
                        N.PrivateNote, N.EmailAddressType AS AddressType, N.NoteID, N.CreatedBy, N.Subject, N.NoteText, N.ObjectType, N.NoteSpecialType, N.CreatedTimezone, N.HasAlert,
                        CONVERT(VARCHAR(20), N.CreationDate, 20) as CreationDate, N.ObjectReferer, EC.Subject AS EmailSub, EC.Body AS EmailText,
                        EC.EmailContentID , EC.FromName, EC.FromEmail, EC.RecipientNames, EC.RecipientEmails, EC.CcNames, EC.CcEmails, CONVERT(varchar, EC.ReceivedDate, 20) AS ReceivedDate,
                        U.FirstName, U.LastName, N.EventID, N.EventName, N.EventStartDate, N.EventStartTime, N.IsClosedEvent, N.IsCancelledEvent, N.OppSalesPerson, N.Category,
                        N.OppID, N.PersonID, A.AlertID, A.DelayUntil, TZ.zname 
                    FROM (
                        SELECT 
                            ROW_NUMBER() OVER (ORDER BY CreationDate DESC) AS RowNumber, M.* 
                        FROM (

                            SELECT 
                                Notes.*, EventID=NULL, EventStartDate=NULL, EventStartTime=NULL, EventName = NULL,
                                IsClosedEvent = NULL, IsCancelledEvent = NULL, Category=NULL, OppSalesPerson=NULL, OppID=NULL, PersonID=NULL
                            FROM Notes 
                            WHERE ObjectType= ? AND ObjectReferer in (" . $contacts . ")" . "
                            AND (Incoming IS NULL OR Incoming = 0) 
                            AND PrivateNote = 0

                            UNION ALL
                             
                            SELECT 
                                Notes.*, Event.EventID, CONVERT(varchar(10), Event.StartDate, 20) AS EventStartDate, 
                                CONVERT(varchar, Event.StartDate, 8) AS EventStartTime, ET.EventName,
                                Event.Closed AS IsClosedEvent, Event.Cancelled AS IsCancelledEvent, Category=NULL, OppSalesPerson=NULL, OppID=NULL, PersonID=NULL 
                            FROM Notes
                            INNER JOIN Event ON Notes.ObjectReferer = Event.EventID AND Notes.ObjectType = ?    
                            LEFT JOIN EventType ET ON Event.EventTypeID = ET.EventTypeID
                            WHERE Event.ContactID in (" . $contacts . ")" . "

                            UNION ALL 
                                                
                            SELECT 
                                Notes.*, EventID=NULL, EventStartDate=NULL, EventStartTime=NULL, EventName = NULL,
                                IsClosedEvent = NULL, IsCancelledEvent = NULL, opportunities.Category,people.FirstName+' '+people.LastName AS OppSalesPerson, opportunities.DealID AS OppID, 
                                opportunities.PersonID AS PersonID 
                            FROM Notes
                            INNER JOIN opportunities ON Notes.ObjectReferer = opportunities.DealID AND Notes.ObjectType = ? 
                            LEFT JOIN people ON opportunities.PersonID = people.PersonID                    
                            WHERE opportunities.ContactID in (" . $contacts . ")" . "

                            UNION ALL

                            SELECT 
                                Notes.*, EventID=NULL, EventStartDate=NULL, EventStartTime=NULL, EventName = NULL,
                                IsClosedEvent = NULL, IsCancelledEvent = NULL, Category=NULL, OppSalesPerson=NULL, OppID=NULL, PersonID=NULL
                            FROM Notes 
                            WHERE ObjectType= ? AND ObjectReferer= ?

                            ) M
                        ) N
                        LEFT JOIN EmailContent AS EC ON N.EmailContentID = EC.EmailContentID AND EC.EmailContentID IS NOT NULL
                        LEFT JOIN Alert AS A ON N.NoteID = A.RecordID AND RecordTypeID = 1
                        LEFT JOIN people AS U ON N.CreatedBy = U.PersonID
                        LEFT JOIN timezones AS TZ ON U.timezone = TZ.zid
                    WHERE N.CompanyId= ? AND ((PrivateNote = 1 AND N.CreatedBy = ?) OR (PrivateNote = 0)) AND RowNumber BETWEEN ? AND ?";

                    $sqlNote = SqlBuilder()->LoadSql($sqlNote)->BuildSql(
                        array(DTYPE_INT, NOTETYPE_CONTACT),

                        array(DTYPE_INT, NOTETYPE_EVENT),
                        
                        array(DTYPE_INT, NOTETYPE_OPPORTUNITY),
                        
                        
                        
                        array(DTYPE_INT, $this->noteObjectType),
                        array(DTYPE_INT, $this->noteObject),
                        
                        array(DTYPE_INT, $this->company_id),
                        array(DTYPE_INT, $this->user_id), 
                        array(DTYPE_INT, (int)$startRecord),
                        array(DTYPE_INT, (int)$endRecord)
                    );

            } else {
                $sqlNote = "
SELECT 
    PrivateNote, CreatedTimezone, NoteID, CreatedBy, CONVERT(VARCHAR(20), CreationDate, 20) as CreationDate,
    Subject, NoteText, ObjectType, ObjectReferer, NoteSpecialType, HasAlert, U.FirstName, U.LastName, TZ.zname 
FROM (
    SELECT 
        ROW_NUMBER() OVER (ORDER BY NoteID DESC) AS RowNumber, Notes.*
    FROM Notes WHERE ObjectType=? AND ObjectReferer=? ) _myResults
    LEFT JOIN people AS U ON CreatedBy = U.PersonID
    LEFT JOIN timezones AS TZ ON U.timezone = TZ.zid
    WHERE _myResults.CompanyId= ? AND ((PrivateNote = 1 AND CreatedBY = ?) OR (PrivateNote = 0)) AND RowNumber BETWEEN ? AND ?";

                // NY 12-19-2008: Modified WHERE from CompanyId= to _myResults.CompanyId= because it was
                // getting an ambiguous row error
                
                $sqlNote = SqlBuilder()->LoadSql($sqlNote)->BuildSql(
                    array(DTYPE_INT, $this->noteObjectType), 
                    array(DTYPE_INT, $this->noteObject), 
                    array(DTYPE_INT, $this->company_id), 
                    array(DTYPE_INT, $this->user_id), 
                    array(DTYPE_INT, (int)$startRecord), 
                    array(DTYPE_INT, (int)$endRecord));
            }
    
            $resultNote = DbConnManager::GetDb('mpower')->Exec($sqlNote);

            if (count($resultNote) > 0) {
                foreach ($resultNote as $note) {
                    $temp = array();
                    $temp['Type'] = $this->getNoteType($note['ObjectType'], $note['NoteSpecialType']);
                    
                    $temp['NoteSpecialType'] = $note['NoteSpecialType'];
                    $temp['HasAlert'] = $note['HasAlert'];
                                        
                    if((strtolower($temp['Type']) == 'event')   
                    && (($this->noteObjectType == NOTETYPE_CONTACT) || ($this->noteObjectType == NOTETYPE_OPPORTUNITY))) {
                        $event_type = (($note['IsClosedEvent']) ? ('Closed ') : (($note['IsCancelledEvent']) ? ('Cancelled ') : ('')));                     
                        $temp['Type'] = $event_type.$temp['Type'];
                        
                        $temp['Type'] .= ' (<a href="slipstream.php?action=event&eventId='.$note['EventID'].'">';
                        if(trim($note['EventName']) != '') {
                            $temp['Type'] .= $note['EventName'].', ';
                        }
                        
                        $temp['Type'] .= ReformatDate($note['EventStartDate']).', '.DrawTime(strtotime($note['EventStartTime'])).'</a>)';
                    }
                    
                    if((strtolower($temp['Type']) == 'opportunity') && ($this->noteObjectType == NOTETYPE_CONTACT)) {
                        $temp['Type'] .= ' (<a style="cursor:pointer;" onclick="javascript:openOppPopup('.$note['OppID'].', '.$note['PersonID'].')" >';
                        if(trim($note['OppSalesPerson']) != '') {
                            $temp['Type'] .= $note['OppSalesPerson'].', ';
                        }
                        $temp['Type'] .= getCategory($note['Category']).'</a>)';
                    }

                    if($this->noteObjectType == NOTETYPE_CONTACT) {
                        if($note['EmailContentID'] > 0) {
                            $temp['Subject'] = $note['EmailSub'];
                        
                            $email_body = 'From: ';
                            if(trim($note['FromName']) != '') {
                                $email_body .= $note['FromName'].' ';
                            }
                            $email_body .= '&lt; '.$note['FromEmail'].' &gt; <br />';
                            $email_body .= 'Date: ';
                            if(trim($note['ReceivedDate']) != '') {
                                $email_body .= ReformatDate($note['ReceivedDate']).' '.DrawTime(strtotime($note['ReceivedDate']));
                            } 
                            $email_body .= '<br />';
                            $email_body .= 'To: ';
                            
                            $email_recipients = explode(';', $note['RecipientNames']);
                            $email_recipients_emails = explode(';', $note['RecipientEmails']);
                            
                            if(count($email_recipients) > 0) {
                                $str_email_recipients = '';
                                
                                foreach($email_recipients as $key => $email_recipient) {
                                    if($str_email_recipients != '') {
                                        $str_email_recipients .=  ', '.$email_recipient.' &lt; '.$email_recipients_emails[$key].' &gt;';
                                    } else {
                                        $str_email_recipients .=  $email_recipient.' &lt; '.$email_recipients_emails[$key].' &gt;';
                                    }
                                }
                                $email_body .= $str_email_recipients;
                            }
                            
                            if(trim($note['CcNames']) != '') {
                                $email_cc_recipients = explode(';', $note['CcNames']);
                                $email_cc_recipients_emails = explode(';', $note['CcEmails']);
                                
                                if(count($email_cc_recipients) > 0) {
                                    $email_body .= '<br />CC: ';
                                    $str_email_cc_recipients = '';
                                    
                                    foreach($email_cc_recipients as $key => $email_cc_recipient) {
                                        if($str_email_cc_recipients != '') {
                                            $str_email_cc_recipients .=  ', '.$email_cc_recipient.' &lt; '.$email_cc_recipients_emails[$key].' &gt;';
                                        } else {
                                            $str_email_cc_recipients .=  $email_cc_recipient.' &lt; '.$email_cc_recipients_emails[$key].' &gt;';
                                        }
                                    }
                                    $email_body .= $str_email_cc_recipients;
                                }
                            }
                            $temp['Body'] = $email_body.'<br /><br />'.$note['EmailText'];
                            $temp['AddressType'] = $note['AddressType'];
                            
                            $obj_email = new Email();
                            $attachments = $obj_email->getEmailAttachments($note['EmailContentID']);

                            if(trim($attachments) != '') {
                                $temp['Attachment'] = $attachments;
                            }
                            
                            $temp['EmailContentID'] = $note['EmailContentID'];
                            $temp['From'] = $note['FromName'];
                            $temp['FromEmail'] = $note['FromEmail'];
                            $temp['To'] = str_replace(';', ',', $note['RecipientNames']);
                            $temp['Cc'] = $note['CcNames'];
                            $temp['Subject'] = Email::format($temp['Subject']);
                            
                            $temp['Body'] = Email::format($temp['Body'], true);
                        }
                        else {
                            $temp['Subject'] = $note['Subject'];
                            $temp['Body'] = $note['NoteText'];
                            $temp['AddressType'] = '';
                        }
                    } else {
                        $temp['Subject'] = $note['Subject'];
                        $temp['Body'] = $note['NoteText'];
                        $temp['AddressType'] = '';
                    }
                    
                    //$temp['EmailContentID'] = $note['EmailContentID'];
                    //$temp['From'] = $note['FromName'];
                    //$temp['To'] = str_replace(';', ',', $note['RecipientNames']);
                    //$temp['Cc'] = $note['CcNames'];
                    $temp['Private'] = $note['PrivateNote'];
                    //$temp['Subject'] = Email::format($temp['Subject']);
                    
                    $temp['Body'] = Email::format($temp['Body'], true);
                    $temp['TimeStamp'] = $this->getFormatedNoteTime(((!empty($note['EmailContentID'])) ? $note['EmailContentID'] : ''), $note['CreationDate'], $note['CreatedTimezone'], $note['zname']);   
                    $temp['Creator'] = $note['FirstName'].' '.$note['LastName'];
                    $noteArr[] = $temp;
                }
            }
            
            return $noteArr;
        } catch (Exception $e) {
            $status->addError($e->getMessage());
        }
        return $status->getStatus();
    }

    /**
     * Creates the array for the Filter dropdown.
     */
    function createFilterArray() {
        $filterArr = array();
        $this->people = $this->getPeople();     
        $this->option_set = $this->getFilterOptionSet($this->company_id);
        
        /* These are the default fields of the filter selectbox. */
        $filterArr['FieldName']['Default Fields'] = array('D0' => 'By Active User', 'D3'=>'By Inactive User','D1' => 'By Date Range', 'D2' => 'By Note Type');
         $this->createFilterFields('default', 'D0', 0, $filterArr['FieldVal']);
         $this->createFilterFields('default', 'D1', 0, $filterArr['FieldVal']);
         $this->createFilterFields('default', 'D2', 0, $filterArr['FieldVal']);
         $this->createFilterFields('default', 'D3', 0, $filterArr['FieldVal']);

        /* These are the mapped company and contact fields of the filter selectbox. */
        $fieldMapObj = new fieldsMap();
        $allAccountFields = $fieldMapObj->parseAccountFields();
        $allContactFields = $fieldMapObj->parseContactFields();

        if(is_array($allAccountFields) && (count($allAccountFields) > 0)) {
            foreach($allAccountFields as $compField => $compFieldVal) {             
                $filterArr['FieldName']['Company Fields'][$compField] = 'By '.$compFieldVal['Text'];
                $this->createFilterFields('CM', $compField, $compFieldVal['ID'],  $filterArr['FieldVal']);
            }
        }
        if(is_array($allContactFields) && (count($allContactFields) > 0)) {
            foreach($allContactFields as $conField => $conFieldVal) {
                $filterArr['FieldName']['Contact Fields'][$conField] = 'By '.$conFieldVal['Text'];
                $this->createFilterFields('CN', $conField, $conFieldVal['ID'], $filterArr['FieldVal']);
            }
        }
        return $filterArr;
    }

    private function createFilterFields($filterType, $fieldType, $filterMapID, &$filterFieldArray) {
            
            if($filterType == 'default') {              
                switch($fieldType) {
                    case 'D0':
                        $filterFieldArray['D0'] = '<select onChange="javascript:callNoteSearch();" class="clsTextBox" name="NText\'+indx+\'" id="NText\'+indx+\'"><option value="">Select an Active User</option>';
                        if(count($this->people) > 0)
                        {                   
                            foreach($this->people as $salesp)
                            {
                                if($salesp['Active'] == 0) {
                                    $filterFieldArray['D0'] .= '<option value="'.$salesp['PersonID'].'">'.htmlentities($salesp['PeopleName'], ENT_QUOTES).'</option>';
                                }
                            }
                        }
                        $filterFieldArray['D0'] .= '</select>';
                        break;
                    case 'D1':
                        $filterFieldArray['D1'] = '<input onKeyUp="javascript:callNoteSearch();" onChange="javascript:callNoteSearch();" class="clsTextBox" size="10" name="StartDate\'+indx+\'" id="StartDate\'+indx+\'" type="text" value="" />&nbsp;<font class="smlBlack">to</font>&nbsp;<input onKeyUp="javascript:callNoteSearch();" onChange="javascript:callNoteSearch();" class="clsTextBox" size="10" name="EndDate\'+indx+\'" id="EndDate\'+indx+\'" type="text" value="" />';
                        break;

                    case 'D2':
                        $noteSplType = $this->createNoteSplArray();
                        $filterFieldArray['D2'] = '<select onChange="javascript:setNoteType(this.value);callNoteSearch();" class="clsTextBox" name="NText\'+indx+\'" id="NText\'+indx+\'"><option value="">Select Note Type</option>';

                        foreach($noteSplType as $noteSplTypeID => $noteSplTypeName) {
                            $filterFieldArray['D2'] .= '<option value="'.$noteSplTypeID.'">'.$noteSplTypeName.'</option>';
                        }
                        $filterFieldArray['D2'] .= '</select>';
                        break;
                    case 'D3':
                        $filterFieldArray['D3'] = '<select onChange="javascript:callNoteSearch();" class="clsTextBox" name="NText\'+indx+\'" id="NText\'+indx+\'"><option value="">Select an Inactive User</option>';
                        if(count($this->people)>0)
                        {                       
                            foreach($this->people as $salesp)
                            {   
                                if($salesp['Active'] == 1) {                            
                                    $filterFieldArray['D3'] .= '<option value="'.$salesp['PersonID'].'">'.htmlentities($salesp['PeopleName'], ENT_QUOTES).'</option>';
                                }
                            }
                        }
                        $filterFieldArray['D0'] .= '</select>';
                        break;
                    default:
                        $filterFieldArray[$fieldType] = '<input onKeyUp="javascript:callNoteSearch();" type="text" name="NText\'+indx+\'" id="NText\'+indx+\'" value="" class="clsTextBox" />';
                }
            }
            else {
            $type = strtolower(substr($fieldType, 0, 4));

            switch($type) {
                case 'date':
                    $filterFieldArray[$filterType.'_'.$fieldType] = '<input onKeyUp="javascript:callNoteSearch();" onChange="javascript:callNoteSearch();" class="clsTextBox" size="10" name="StartDate\'+indx+\'" id="StartDate\'+indx+\'" type="text" value="" />&nbsp;<font class="smlBlack">to</font>&nbsp;<input onKeyUp="javascript:callNoteSearch();" onChange="javascript:callNoteSearch();" class="clsTextBox" size="10" name="EndDate\'+indx+\'" id="EndDate\'+indx+\'" type="text" value="" />';
                    break;

                case 'sele':
                    $optionList = $this->getFilterOptionList($filterMapID, $filterType);
                    $filterFieldArray[$filterType.'_'.$fieldType] = '<select onChange="javascript:callNoteSearch();" class="clsTextBox" name="NText\'+indx+\'" id="NText\'+indx+\'"><option value=""></option>';

                    foreach($optionList as $optionListVal) {
                        $filterFieldArray[$filterType.'_'.$fieldType] .= '<option value="'.$optionListVal['OptionID'].'">'.str_replace("'", "\'", $optionListVal['OptionName']).'</option>';
                    }
                    $filterFieldArray[$filterType.'_'.$fieldType] .= '</select>';
                    break;

                case 'bool' :
                    $filterFieldArray[$filterType.'_'.$fieldType] = '<select onChange="javascript:callNoteSearch();" class="clsTextBox" name="NText\'+indx+\'" id="NText\'+indx+\'"><option value=""></option>';
                    $filterFieldArray[$filterType.'_'.$fieldType] .= '<option value="1">True</option>';
                    $filterFieldArray[$filterType.'_'.$fieldType] .= '<option value="0">False</option>';
                    $filterFieldArray[$filterType.'_'.$fieldType] .= '</select>';
                    break;

                default:
                    $filterFieldArray[$filterType.'_'.$fieldType] = '<input onKeyUp="javascript:callNoteSearch();" type="text" name="NText\'+indx+\'" id="NText\'+indx+\'" value="" class="clsTextBox" />';
            }
        }
    }

    private function getFilterOptionList($mapId, $recType) {
        
        $searchField = ($recType == 'CN') ? 'ContactMapID' : 'AccountMapID';
        $option_list = array();
        
        foreach($this->option_set as $option) {
            if($option[$searchField] == $mapId) {
                $option_list[] = $option;               
            }
        }
        return $option_list;
    }

    function getFilterOptionSet($company_id) {

        $params = array(DTYPE_INT, $company_id);

        $account_option_sql = SqlBuilder()->LoadSql("SELECT O.OptionID, O.OptionName, A.AccountMapID FROM [Option] O
                            LEFT JOIN AccountMap A ON O.OptionSetID = A.OptionSetID
                            WHERE A.CompanyID = ? AND A.OptionType IS NOT NULL")
                            ->BuildSql($params);
        $account_option_list = DbConnManager::GetDb('mpower')->Exec($account_option_sql);

        
        $contact_option_sql = SqlBuilder()->LoadSql("SELECT O.OptionID, O.OptionName, C.ContactMapID FROM [Option] O
                            LEFT JOIN ContactMap C ON O.OptionSetID = C.OptionSetID
                            WHERE C.CompanyID = ? and C.OptionType IS NOT NULL")
                            ->BuildSql($params);
        $contact_option_list = DbConnManager::GetDb('mpower')->Exec($contact_option_sql);
        
        $list = array();
        foreach($account_option_list as $key=>$option_set) {
            $arr = array(
                'OptionID' => $option_set['OptionID'],
                'OptionName' => $option_set['OptionName'],
                'AccountMapID' => $option_set['AccountMapID'],
                'ContactMapID' => null
            );
            $list[] = $arr;
        }
        foreach($contact_option_list as $key=>$option_set) {
            $arr = array(
                'OptionID' => $option_set['OptionID'],
                'OptionName' => $option_set['OptionName'],
                'AccountMapID' => null,
                'ContactMapID' => $option_set['ContactMapID']
            );
            $list[] = $arr;
        }
        
        $sorted = $this->subval_sort($list, 'OptionName');
        return $sorted;
    }

    public function subval_sort($a, $subkey) {
        foreach($a as $k=>$v) {
            $b[$k] = strtolower($v[$subkey]);
        }
        asort($b);
        foreach($b as $key=>$val) {
            if(strlen($val) > 2){
                $d[] = $a[$key];
            }
            else {
                $c[] = $a[$key];
            }
        }
        $sorted = array_merge($c, $d);
        return $sorted;
    }

    /**
     * Creates the array for the Note special type dropdown present in the Filter list.
     */
    function createNoteSplArray() {
        $noteSplTypeArray = array();
        $noteSplTypeArray[NOTE_TYPE.'_'.NOTETYPE_COMPANY] = 'Company';
        $noteSplTypeArray[NOTE_TYPE.'_'.NOTETYPE_CONTACT] = 'Contact';
        $noteSplTypeArray[NOTE_TYPE.'_'.NOTETYPE_EVENT] = 'Event';
        $noteSplTypeArray[NOTE_TYPE.'_'.NOTETYPE_OPPORTUNITY] = 'Opportunity';
        $noteSplTypeArray[NOTE_SPECIAL_TYPE.'_'.NOTETYPE_INCOMING_CALL] = 'Incoming Calls';
        $noteSplTypeArray[NOTE_SPECIAL_TYPE.'_'.NOTETYPE_OUTGOING_CALL] = 'Outgoing Calls';
        $noteSplTypeArray[NOTE_SPECIAL_TYPE.'_'.NOTETYPE_EMAIL] = 'Emails';
        //$noteSplTypeArray[NOTE_SPECIAL_TYPE.'_'.NOTETYPE_ALERT] = 'Note Alerts';
        return $noteSplTypeArray;
    }

    /**
     * Parses the input field and creates the array.
     * Here we can define The rules for search.
     * Creates an array using each word entered in the input field.
     */
    function parseSearchValue($value) {
        $parsedArr = array();
        if($value != '') {
            $keywords = explode(' ', $value);

            foreach($keywords as $key) {
                if(trim($key) != '') {
                    //$parsedArr[] = '%'.$key.'%';
                    $parsedArr[] = $key;
                }
            }
        }
        return $parsedArr;
    }
    
    /**
     * Parses the input field used in filter box.
     * Here we can define The rules for filter values.
     * Creates an array using each word entered in the input field.
     */
    function parseFilterSearchValue($value) {
        $parsedArr = array();
        $value = trim($value);
        if($value != '') {
            $parsedArr[] = '%'.$value.'%';
        }
        return $parsedArr;
    }

    /**
     * Performs the Note search Query.
     */
    function searchNoteList() {
        $queryCompanyNote = true;

        $status = new Status();
        try {
            $whereClause = '';
            $whereContainsEmailContent = '';
            $whereContainsNotes = '';
            $loadSqlClause = '';
            $loadSqlClause = array();

            /*
             * Creates the conditional clause for Keyword search.
             */
            $this->searchString = $this->parseSearchValue($this->searchString);

            /*
             * If there is more than one keyword, assume user is searching for a phrase
             */
            
            $searchAnd = '';
            if(!empty($this->searchString)){
                $queryStr = implode(' AND ', $this->searchString);

                $searchAnd = ' AND (';
                
                // only applies to email objects
                $whereContainsEmailContent .= "CONTAINS(EmailContent.Subject, '".$queryStr."') OR
                        CONTAINS(EmailContent.Body, '".$queryStr."') OR ";
                
                // applies to all notes
                $whereContainsNotes .= "CONTAINS(Notes.Subject, '".$queryStr."') OR
                        CONTAINS(Notes.NoteText, '".$queryStr."')) ";
                
            }
            
            /**
             * Creates the conditional clause for Filteration.
             */
            $innerWhereClause = '';
            $innerFilterFields = array();
            $innerAccFilterFields = array();
            $filterFields = array();
            $selectFilterFields = '';
            $selectInnerFilterFields = '';
            $selectAccInnerFilterFields = '';

            $loadInnerSqlClause = array();
            $loadAccInnerSqlClause = array();

            if(is_array($this->noteFilterArray) && (count($this->noteFilterArray) > 0)) {
                foreach($this->noteFilterArray as $filterKey => $filterVal) {
                    $buildWhereClause = '';

                    if(trim($filterVal) == '') {
                        continue;
                    }
                    $newFilterArr = explode('_', $filterKey);

                    /**
                     * Creates the conditional clause for Company or Contact fields Filteration.
                     */
                    if(($newFilterArr[0] == 'CM') || ($newFilterArr[0] == 'CN')){
                        $recType = strtolower(substr($newFilterArr[1], 0, 4));
                        ($newFilterArr[0] == 'CM') ? ($fieldPrefix = 'Account') : ($fieldPrefix = 'Contact');
                        ($newFilterArr[0] == 'CM') ? ($fieldPrefixAS = 'CM_') : ($fieldPrefixAS = 'CN_');
                        ($newFilterArr[0] == 'CN') ? ($queryCompanyNote = false) : '';

                        switch($recType) {
                            case 'numb' :
                                $filterStrings = $this->parseFilterSearchValue($filterVal);

                                if(is_array($filterStrings) && (count($filterStrings) > 0)) {
                                    $innerFilterFields[] = $fieldPrefix.'.'.$newFilterArr[1] . ' AS '.$fieldPrefixAS.$newFilterArr[1];
                                    if($newFilterArr[0] == 'CM') {
                                        $innerAccFilterFields[] = $fieldPrefix.'.'.$newFilterArr[1] . ' AS '.$fieldPrefixAS.$newFilterArr[1];
                                    }
                                    else {
                                        $innerAccFilterFields[] = $fieldPrefixAS.$newFilterArr[1].' = NULL';
                                    }
                                    $filterFields[] = $fieldPrefixAS.$newFilterArr[1];
                                    $innerWhereClause .= " AND ( ";
                                    $newWhereClause = '';

                                    foreach($filterStrings as $filterStr) {
                                        if($newWhereClause != '') {
                                            $newWhereClause .= " OR $fieldPrefix.".$newFilterArr[1]." LIKE ? ";
                                        }
                                        else {
                                            $newWhereClause .= " $fieldPrefix.".$newFilterArr[1]." LIKE ? ";
                                        }
                                        $loadInnerSqlClause[] = array(DTYPE_INT, (int)$filterStr);
                                        ($newFilterArr[0] == 'CM') ? ($loadAccInnerSqlClause[] = array(DTYPE_INT, (int)$filterStr)) : '';
                                    }
                                    $innerWhereClause .= $newWhereClause." ) ";
                                }
                                break;

                            case 'sele' :
                            case 'bool' :
                                $filterStr = $filterVal;

                                if(trim($filterStr) != '') {
                                    $innerFilterFields[] = $fieldPrefix.'.'.$newFilterArr[1] . ' AS '.$fieldPrefixAS.$newFilterArr[1];
                                    if($newFilterArr[0] == 'CM') {
                                        $innerAccFilterFields[] = $fieldPrefix.'.'.$newFilterArr[1] . ' AS '.$fieldPrefixAS.$newFilterArr[1];
                                    }
                                    else {
                                        $innerAccFilterFields[] = $fieldPrefixAS.$newFilterArr[1].' = NULL';
                                    }
                                    $filterFields[] = $fieldPrefixAS.$newFilterArr[1];
                                    $innerWhereClause .= " AND ( ";
                                    $innerWhereClause .= " $fieldPrefix.".$newFilterArr[1]." = ? ";
                                    (($recType == 'bool') && ($filterStr == '0')) ? ($innerWhereClause .= " OR $fieldPrefix.".$newFilterArr[1]." IS NULL ") : ('');
                                    $innerWhereClause .= " ) ";

                                    $loadInnerSqlClause[] = array(DTYPE_INT, (int)$filterStr);
                                    ($newFilterArr[0] == 'CM') ? ($loadAccInnerSqlClause[] = array(DTYPE_INT, (int)$filterStr)) : '';
                                }
                                break;

                            case 'date' :
                                $innerFilterFields[] = $fieldPrefix.'.'.$newFilterArr[1] . ' AS '.$fieldPrefixAS.$newFilterArr[1];
                                if($newFilterArr[0] == 'CM') {
                                    $innerAccFilterFields[] = $fieldPrefix.'.'.$newFilterArr[1] . ' AS '.$fieldPrefixAS.$newFilterArr[1];
                                }
                                else {
                                    $innerAccFilterFields[] = $fieldPrefixAS.$newFilterArr[1].' = NULL';
                                }
                                $filterFields[] = $fieldPrefixAS.$newFilterArr[1];

                                /*
                                 * If the entered date is not in correct format then it takes the current date.
                                 */
                                if(!strtotime($filterVal['StartDate'])) {
                                    $filterVal['StartDate'] = date('m/d/Y');
                                }
                                if(!strtotime($filterVal['EndDate'])) {
                                    $filterVal['EndDate'] = date('m/d/Y');
                                }

                                if($newFilterArr[1] == 'StartDate') {
                                    if(trim($whereClause) != '') {
                                        $whereClause .= " AND NP.CreationDate >= ? ";
                                    }
                                    else {
                                        $whereClause .= " NP.CreationDate >= ? ";
                                    }
                                    $filterVal = date('Y-m-d 00:00:00', strtotime($filterVal));
                                }
                                else if($newFilterArr[1] == 'EndDate') {
                                    if(trim($whereClause) != '') {
                                        $whereClause .= " AND NP.CreationDate <= ? ";
                                    }
                                    else {
                                        $whereClause .= " NP.CreationDate <= ? ";
                                    }

                                    $filterVal = date('Y-m-d 23:59:59', strtotime($filterVal));
                                }

                                $filterStr['StartDate'] = date('m/d/Y', strtotime($filterVal['StartDate']));
                                $filterStr['EndDate'] = date('m/d/Y', strtotime($filterVal['EndDate']));
                                $innerWhereClause .= " AND CONVERT(varchar(20), $fieldPrefix.".$newFilterArr[1].", 101) >= ? ";
                                $innerWhereClause .= " AND CONVERT(varchar(20), $fieldPrefix.".$newFilterArr[1].", 101) <= ? ";

                                $loadInnerSqlClause[] = array(DTYPE_TIME, $filterStr['StartDate']);
                                $loadInnerSqlClause[] = array(DTYPE_TIME, $filterStr['EndDate']);

                                if($newFilterArr[0] == 'CM') {
                                    $loadAccInnerSqlClause[] = array(DTYPE_TIME, $filterStr['StartDate']);
                                    $loadAccInnerSqlClause[] = array(DTYPE_TIME, $filterStr['EndDate']);
                                }
                                break;

                            default:
                                $filterStrings = $this->parseFilterSearchValue($filterVal);

                                if(is_array($filterStrings) && (count($filterStrings) > 0)) {
                                    $innerFilterFields[] = $fieldPrefix.'.'.$newFilterArr[1] . ' AS '.$fieldPrefixAS.$newFilterArr[1];

                                    if($newFilterArr[0] == 'CM') {
                                        $innerAccFilterFields[] = $fieldPrefix.'.'.$newFilterArr[1] . ' AS '.$fieldPrefixAS.$newFilterArr[1];
                                    }
                                    else {
                                        $innerAccFilterFields[] = $fieldPrefixAS.$newFilterArr[1].' = NULL';
                                    }
                                    $filterFields[] = $fieldPrefixAS.$newFilterArr[1];
                                    $innerWhereClause .= " AND ( ";
                                    $newWhereClause = '';

                                    foreach($filterStrings as $filterStr) {
                                        if($newWhereClause != '') {
                                            $newWhereClause .= " OR $fieldPrefix.".$newFilterArr[1]." LIKE ? ";
                                        }
                                        else {
                                            $newWhereClause .= " $fieldPrefix.".$newFilterArr[1]." LIKE ? ";
                                        }
                                        $loadInnerSqlClause[] = array(DTYPE_STRING, $filterStr);

                                        ($newFilterArr[0] == 'CM') ? ($loadAccInnerSqlClause[] = array(DTYPE_STRING, $filterStr)) : '';
                                    }
                                    $innerWhereClause .= $newWhereClause." ) ";
                                }
                        }
                    }
                    else {
                    /**
                     * Creates the conditional clause for Default fields Filteration.
                     */

                        if($newFilterArr[0] == "D0" || $newFilterArr[0] == "D3") {
                            /**
                             * Creates the conditional clause for Salesperson Filteration.
                             */
                            
                            $filterStrings = $this->parseFilterSearchValue($filterVal);

                            if(is_array($filterStrings) && (count($filterStrings) >0)) {
                                if(trim($whereClause) != '') {
                                    $whereClause .= " AND ( ";
                                }
                                else {
                                    $whereClause .= " ( ";
                                }
                                $newWhereClause = '';

                                foreach($filterStrings as $filterStr) {
                                    if($newWhereClause != '') {
                                        //$newWhereClause .= " OR ( U.FIRSTNAME LIKE ?  OR U.LASTNAME LIKE ? ) ";
                                        $newWhereClause .= " OR ( U.PersonID=?) ";
                                    }
                                    else {
                                        //$newWhereClause .= " ( U.FIRSTNAME LIKE ?  OR U.LASTNAME LIKE ? ) ";
                                        $newWhereClause .= " ( U.PersonID=?) ";
                                    }
                                    //$loadSqlClause[] = array(DTYPE_STRING, $filterStr);
                                    $loadSqlClause[] = array(DTYPE_STRING, $filterVal);
                                }
                                $whereClause .= $newWhereClause." ) ";
                            }
                        }
                        else if($newFilterArr[0] == "D1") {
                            /**
                             * Creates the conditional clause for Date Range Filteration.
                             */

                            /*
                             * If the entered date is not in correct format then it takes the current date.
                             */
                            if(!strtotime($filterVal)) {
                                $filterVal = date('m/d/Y');
                            }

                            if($newFilterArr[1] == 'StartDate') {
                                if(trim($whereClause) != '') {
                                    $whereClause .= " AND NP.CreationDate >= ? ";
                                }
                                else {
                                    $whereClause .= " NP.CreationDate >= ? ";
                                }
                                $filterVal = date('Y-m-d 00:00:00', strtotime($filterVal));
                            }
                            else if($newFilterArr[1] == 'EndDate') {
                                if(trim($whereClause) != '') {
                                    $whereClause .= " AND NP.CreationDate <= ? ";
                                }
                                else {
                                    $whereClause .= " NP.CreationDate <= ? ";
                                }

                                $filterVal = date('Y-m-d 23:59:59', strtotime($filterVal));
                            }

                            $loadSqlClause[] = array(DTYPE_TIME, $filterVal);
                        }
                        else if($newFilterArr[0] == "D2") {
                            /**
                             * Creates the conditional clause for Note Type Filteration.
                             */
                            if(trim($filterVal) != '') {
                                $arr_note_type = explode('_', $filterVal);
                                if(trim($arr_note_type[0]) == NOTE_TYPE) {
                                    $note_type = $arr_note_type[1];
                                    if($note_type == NOTETYPE_CONTACT) {                                        
                                        if(trim($whereClause) != '') {
                                            $whereClause .= " AND (NP.NoteSpecialType IS NULL OR  NP.NoteSpecialType = 0 OR NP.NoteSpecialType = '')";
                                        }
                                        else {
                                            $whereClause .= " (NP.NoteSpecialType IS NULL OR  NP.NoteSpecialType = 0 OR NP.NoteSpecialType = '') ";
                                        }                                               
                                    }
                                }
                                else if(trim($arr_note_type[0]) == NOTE_SPECIAL_TYPE) {
                                    $filterVal = $arr_note_type[1];
                                    if($filterVal > 0) {
                                        if(trim($whereClause) != '') {
                                            $whereClause .= " AND NP.NoteSpecialType = ? ";
                                        }
                                        else {
                                            $whereClause .= " NP.NoteSpecialType = ? ";
                                        }       
                                        $loadSqlClause[] = array(DTYPE_INT, (int)$filterVal);
                                    }
                                }
                            }                           
                        }
                    }
                    $innerWhereClause .= $buildWhereClause;
                }
            }

            /**
             * Selects the user entered filter fields for only company.
             */
            if(count($innerAccFilterFields) > 0) {
                $selectAccInnerFilterFields = implode(', ',$innerAccFilterFields);
                if($selectAccInnerFilterFields != '') {
                    $selectAccInnerFilterFields = ', '.$selectAccInnerFilterFields;
                }
            }

            /**
             * Selects the user entered filter fields for both company and contact.
             */
            if(count($innerFilterFields) > 0) {
                $selectInnerFilterFields = implode(', ',$innerFilterFields);
                if($selectInnerFilterFields != '') {
                    $selectInnerFilterFields = ', '.$selectInnerFilterFields;
                }
            }

            /**
             * Selects the user entered filter fields for all (company, contact and default fields).
             */
            if(count($filterFields) > 0) {
                $selectFilterFields = implode(', N.',$filterFields);
                if($selectFilterFields != '') {
                    $selectFilterFields = ', '.$selectFilterFields;
                }
            }

            /*if(is_array($loadInnerSqlClause) && count($loadInnerSqlClause) > 0) {

                /**
                 * If the $queryCompanyNote is true then include the accountInnerclause to pass them in the company note query.
                 /
                if($queryCompanyNote) {
                    $loadSqlClause = array_merge($loadAccInnerSqlClause, $loadInnerSqlClause, $loadInnerSqlClause, $loadInnerSqlClause, $loadSqlClause);
                }
                else {
                    $loadSqlClause = array_merge($loadInnerSqlClause, $loadInnerSqlClause, $loadInnerSqlClause, $loadSqlClause);
                }
            }*/


            $fieldMapObj = new fieldsMap();

            $contactMapFName = $fieldMapObj->getContactFirstNameField();
            $contactMapLName = $fieldMapObj->getContactLastNameField();
            $companyMapName = $fieldMapObj->getAccountNameField();

            $inactiveAccountNote = '';
            $inactiveContactNote = '';
            if($this->notesInactivated == 'false'){
                $inactiveAccountNote = 'AND Account.Inactive = 0';
                $inactiveContactNote = 'AND Contact.Inactive = 0';
            }
            
            $sql_acc_assigned_user = '';
            $sql_con_assigned_user = '';
            if(isset($_SESSION['company_obj']['LimitAccessToUnassignedUsers']) && ($_SESSION['company_obj']['LimitAccessToUnassignedUsers'] == 1)) {

                # Allow access to notes created by anyone below the user in the chain of command
                $cc = new CompanyContact();
                $visible = $cc->getChildRecords($this->user_id);
                
                $children = $cc->getChildren($visible);
                $chain = array();
                foreach($children as $child){
                    $chain[] = $child['PersonID'];
                }
                $chain[] = $this->user_id;
                $list = implode(',', $chain);
                
                $sql_acc_assigned_user .= '
                INNER JOIN PeopleAccount AS PA ON Account.AccountID = PA.AccountID AND PA.AssignedTo = 1 
                    AND PA.PersonID IN (' . $list . ')';
                    
                $sql_con_assigned_user .= ' 
                INNER JOIN PeopleContact AS PC ON Contact.ContactID = PC.ContactID AND PC.AssignedTo = 1 
                    AND PC.PersonID IN (' . $list . ')';
            }
            
            /**
             * Query to get different types of notes.
             */
            if(!isset($note_type) || (isset($note_type) && $note_type == NOTETYPE_COMPANY)) {
                $sql4AccountNote = "
                (
                SELECT DISTINCT 
                    Notes.*, EmailSub = NULL, EmailText = NULL , EmailID = NULL, FromName = NULL, FromEmail = NULL, RecipientNames = NULL, 
                    RecipientEmails = NULL, CcNames = NULL, CcEmails = NULL, ReceivedDate = NULL, ContactFName = NULL, ContactLName = NULL, 
                    ContactID = NULL, Account.$companyMapName AS CompanyName, Account.AccountID, OppId = NULL , OppFirstName = NULL, OppLastName = NULL, 
                    OppPersonID = NULL, EventName=NULL, EventID=NULL, IsClosedEvent=NULL, IsCancelledEvent=NULL $selectAccInnerFilterFields 
                FROM Notes
                LEFT JOIN Account ON Notes.ObjectReferer = Account.AccountID ".$sql_acc_assigned_user;
                $sql4AccountNote .= " 
                WHERE Notes.CompanyID = ".$this->company_id." AND Notes.ObjectType = ".NOTETYPE_COMPANY." ".$inactiveAccountNote." ".$innerWhereClause.$searchAnd.$whereContainsNotes.")";              
            }
            
            if(!isset($note_type) || (isset($note_type) && $note_type == NOTETYPE_CONTACT)) {
                $sql4ContactNote = "
                (
                SELECT DISTINCT 
                    Notes.*, EmailContent.Subject AS EmailSub, EmailContent.Body AS EmailText, EmailContent.EmailContentID AS EmailID,                  
                    EmailContent.FromName, EmailContent.FromEmail, EmailContent.RecipientNames, EmailContent.RecipientEmails, EmailContent.CcNames, 
                    EmailContent.CcEmails, CONVERT(varchar, EmailContent.ReceivedDate, 20) AS ReceivedDate, Contact.$contactMapFName AS ContactFName, Contact.$contactMapLName AS ContactLName, Contact.ContactID,
                    Account.$companyMapName AS CompanyName, Account.AccountID, OppId = NULL , OppFirstName = NULL, OppLastName = NULL, 
                    OppPersonID = NULL, EventName=NULL, EventID=NULL, IsClosedEvent=NULL, IsCancelledEvent=NULL $selectInnerFilterFields 
                FROM Notes
                LEFT JOIN EmailContent ON Notes.EmailContentID = EmailContent.EmailContentID
                LEFT JOIN Contact ON Notes.ObjectReferer = Contact.ContactID
                $sql_con_assigned_user
                LEFT JOIN Account ON Contact.AccountID = Account.AccountID
                WHERE Notes.CompanyID = ".$this->company_id." AND Notes.ObjectType = ".NOTETYPE_CONTACT." AND  (Notes.Incoming IS NULL OR Notes.Incoming = 0) ".$inactiveContactNote." ".$innerWhereClause.$searchAnd.$whereContainsEmailContent.$whereContainsNotes.")";
            }
            
            if(!isset($note_type) || (isset($note_type) && $note_type == NOTETYPE_EVENT)) {
                $sql4EventNote = "
                (
                SELECT DISTINCT 
                    Notes.*, EmailSub = NULL, EmailText = NULL , EmailID = NULL,
                    FromName = NULL, FromEmail = NULL, RecipientNames = NULL, RecipientEmails = NULL, CcNames = NULL, CcEmails = NULL, ReceivedDate = NULL,
                    Contact.$contactMapFName AS ContactFName, Contact.$contactMapLName AS ContactLName, Contact.ContactID,
                    Account.$companyMapName AS CompanyName, Account.AccountID, Event.DealID AS OppId , 
                    people.FirstName OppFirstName, people.LastName OppLastName, people.PersonID AS OppPersonID,
                    EventType.EventName, Event.EventID, Event.Closed AS IsClosedEvent, Event.Cancelled AS IsCancelledEvent $selectInnerFilterFields 
                FROM Notes
                LEFT JOIN Event ON Notes.ObjectReferer = Event.EventID
                LEFT JOIN EventType ON EventType.EventTypeID = Event.EventTypeID
                LEFT JOIN Contact ON Event.ContactID = Contact.ContactID
                $sql_con_assigned_user 
                LEFT JOIN Account ON Contact.AccountID = Account.AccountID
                LEFT JOIN opportunities ON Event.DealID = opportunities.DealID
                LEFT JOIN people ON opportunities.PersonID = people.PersonID ";
                $sql4EventNote .= "WHERE Notes.CompanyID = ".$this->company_id." AND Notes.ObjectType = ".NOTETYPE_EVENT." AND ((Contact.ContactID IS NOT NULL ".$inactiveContactNote.") OR Contact.ContactID IS NULL) ".$innerWhereClause.$searchAnd.$whereContainsNotes.")";
            }
            if(!isset($note_type) || (isset($note_type) && $note_type == NOTETYPE_OPPORTUNITY)) {
                $sql4OppNote = "
                (
                SELECT DISTINCT 
                    Notes.*, EmailSub = NULL, EmailText = NULL , EmailID = NULL,
                    FromName = NULL, FromEmail = NULL, RecipientNames = NULL, RecipientEmails = NULL, CcNames = NULL, CcEmails = NULL, ReceivedDate = NULL,
                    Contact.$contactMapFName AS ContactFName, Contact.$contactMapLName AS ContactLName, Contact.ContactID,
                    Account.$companyMapName AS CompanyName, Account.AccountID, opportunities.DealID AS OppId,
                    people.FirstName OppFirstName, people.LastName OppLastName, people.PersonID AS OppPersonID, EventName=NULL, EventID=NULL, IsClosedEvent=NULL, IsCancelledEvent=NULL
                    $selectInnerFilterFields 
                FROM Notes
                LEFT JOIN opportunities ON Notes.ObjectReferer = opportunities.DealID
                LEFT JOIN people ON opportunities.PersonID = people.PersonID
                LEFT JOIN Contact ON opportunities.ContactID = Contact.ContactID
                $sql_con_assigned_user
                LEFT JOIN Account ON opportunities.AccountID = Account.AccountID ";
                $sql4OppNote .= "WHERE Notes.CompanyID = ".$this->company_id." AND Notes.ObjectType = ".NOTETYPE_OPPORTUNITY." AND ((Contact.ContactID IS NOT NULL ".$inactiveContactNote.") OR Contact.ContactID IS NULL) ".$innerWhereClause.$searchAnd.$whereContainsNotes.")";
            }

            /**
             * Query to get the total number of records.
             */
            
            $sqlCountNote = "SELECT COUNT(NP.NoteID) AS TOTREC FROM
                (";

            /**
             * If the $queryCompanyNote is true then include the company note query.
             */
            $add_union_all = false;
            if($queryCompanyNote) {
                if(isset($sql4AccountNote)) {
                    $sqlCountNote .= $sql4AccountNote;
                    $add_union_all = true;
                        
                    if(is_array($loadAccInnerSqlClause) && count($loadAccInnerSqlClause) > 0) {
                        $loadSqlClause = array_merge($loadAccInnerSqlClause, $loadSqlClause);                   
                    }
                }
            }
            if(isset($sql4ContactNote)) {
                if($add_union_all) {
                    $sqlCountNote .= ' UNION ALL ';                     
                }
                $sqlCountNote .= $sql4ContactNote;                  
                $add_union_all = true;
                
                if(is_array($loadInnerSqlClause) && count($loadInnerSqlClause) > 0) {
                    $loadSqlClause = array_merge($loadInnerSqlClause, $loadSqlClause);                  
                }
            }
            if(isset($sql4EventNote)) {
                if($add_union_all) {
                    $sqlCountNote .= ' UNION ALL ';                     
                }
                $sqlCountNote .= $sql4EventNote;
                $add_union_all = true;
                
                if(is_array($loadInnerSqlClause) && count($loadInnerSqlClause) > 0) {
                    $loadSqlClause = array_merge($loadInnerSqlClause, $loadSqlClause);                  
                }
            }
            if(isset($sql4OppNote)) {
                if($add_union_all) {
                    $sqlCountNote .= ' UNION ALL ';                     
                }
                $sqlCountNote .= $sql4OppNote;
                
                if(is_array($loadInnerSqlClause) && count($loadInnerSqlClause) > 0) {
                    $loadSqlClause = array_merge($loadInnerSqlClause, $loadSqlClause);                  
                }
            }
            $sqlCountNote .=    " ) NP 
                LEFT JOIN people AS U ON NP.CreatedBy = U.PersonID ";

            if(trim($whereClause != '')) {
                $whereClause .= ' AND ((NP.PrivateNote = 1 AND NP.CreatedBy = ?) OR (NP.PrivateNote = 0)) ';
            }
            else {
                $whereClause .= ' ((NP.PrivateNote = 1 AND NP.CreatedBy = ?) OR (NP.PrivateNote = 0)) ';
            }
            $loadSqlClause[] = array(DTYPE_INT, $this->user_id);
            

            /**
             * Execute Query to get the total number of records.
             */
            if(!$this->noteExport && !$this->loadFirst) {
                if(trim($whereClause) != '') {
                    $sqlCountNote .= ' WHERE '.$whereClause;
                }

                $sqlCountNote = SqlBuilder()->LoadSql($sqlCountNote)->BuildSqlParam($loadSqlClause);
                //echo $sqlCountNote;
                $resultCountNote = DbConnManager::GetDb('mpower')->Exec($sqlCountNote);
                $totRecords = $resultCountNote[0]['TOTREC'];
                $this->totalRecords = $totRecords;
                $this->totPages = ($this->pageRecords == 0) ? 1 : ceil($totRecords / $this->pageRecords);
            }

            if(($this->notePrint) && ($this->totalRecords > MAX_PRINT_NOTES_LIMIT)) {
                $error = 'Print note exceeds the maximum limit '.MAX_PRINT_NOTES_LIMIT.'.';
                $errArray = array('result'=>'false', 'error' => $error);
                return $errArray;
            }

            /**
             * Creates the Order By Clause.
             */
            $sortAsClause = ' ASC ';

            if($this->sortAs == 1) {
                $sortAsClause = ' DESC ';
            }

            if($this->sortType != '') {
                switch($this->sortType) {
                    case 'Date' :
                        $sortField = 'NP.CreationDate AS SortName';
                        $sortClause = 'ORDER BY SortName' . $sortAsClause;
                        break;
                    case 'Company' :
                        $sortField = "NP.CompanyName AS SortName";
                        $sortClause = 'ORDER BY SortName ' . $sortAsClause;
                        break;
                    case 'Contact' :
                        $sortField = "NP.ContactFName AS SortName1, NP.ContactLName AS SortName2";
                        $sortClause = 'ORDER BY SortName1 ' . $sortAsClause . ', SortName2 '.$sortAsClause;
                        break;
                    case 'Salesperson' :
                        $sortField = 'U.FirstName AS SortName1, U.LastName AS SortName2';
                        $sortClause = 'ORDER BY SortName1 ' . $sortAsClause . ', SortName2 '.$sortAsClause;
                        break;
                    case 'Opportunity' :
                        $sortField = 'NP.OppId AS SortName';
                        $sortClause = 'ORDER BY SortName' . $sortAsClause;
                        
                    default:
                        $sortField = 'NP.NoteID AS SortName';
                        $sortClause = 'ORDER BY SortName' . $sortAsClause;
                }
            } else {
                $sortField = 'NP.NoteID AS SortName';
                $sortClause = 'ORDER BY SortName' . $sortAsClause;
            }

            /**
             * Search Query.
             */
            $selectFields = "
    N.PrivateNote, N.NoteID, N.EmailAddressType AS AddressType, 
    N.FromName, N.FromEmail, N.RecipientNames, N.RecipientEmails, N.CcNames, N.CcEmails, CONVERT(varchar, N.ReceivedDate, 20) AS ReceivedDate,
    N.ObjectType, N.CreatedBy, N.Subject, N.NoteText, N.ObjectType, N.CreatedTimezone,
    N.ObjectReferer, N.NoteSpecialType, CONVERT(VARCHAR(20), N.CreationDate, 20) as NoteCreationDate, N.EmailSub, N.EmailText, N.EmailID, N.FirstName,
    N.LastName, N.CompanyName, N.ContactFName, N.ContactLName, N.ContactID, N.AccountID, N.OppId, N.OppFirstName,
    N.OppLastName, N.OppPersonID, N.EventName, N.EventID, N.IsClosedEvent, N.IsCancelledEvent, N.HasAlert, N.zname $selectFilterFields";

            $sqlWhereNote = '';

            if((!$this->noteExport) && (!$this->notePrint)) {
                $sqlRowNumber = "
    (
    SELECT 
        ROW_NUMBER() OVER ($sortClause) AS RowNumber, MyResult.* 
    FROM ";

                if(trim($whereClause) != '') {
                    $sqlWhereNote .= ' 
            WHERE '.$whereClause;
                }
                $sqlWhereNote .= '
        ) MyResult
    ) N 
    WHERE RowNumber BETWEEN ? AND ?';
            }
            else {
                $sqlRowNumber = '';

                if(trim($whereClause) != '') {
                    $sqlWhereNote .= ' 
    WHERE '.$whereClause;
                }
                $sqlWhereNote .= '
    ) N ';
                $sqlWhereNote .= ' 
    '.$sortClause.' ';
            }

            $sqlNote = "
SELECT 
    $selectFields 
FROM
    $sqlRowNumber
    (
        SELECT 
            NP.*, U.FirstName, U.LastName, TZ.zname, $sortField 
        FROM
        (";

            /**
             * If the $queryCompanyNote is true then include the company note query.
             */
            $add_union_all = false;
            if($queryCompanyNote) {
                if(isset($sql4AccountNote)) {                   
                    $sqlNote .= $sql4AccountNote;
                    $add_union_all = true;
                }
            }
            if(isset($sql4ContactNote)) {
                if($add_union_all) {
                    $sqlNote .= ' 
                UNION ALL ';                    
                }
                $sqlNote .= $sql4ContactNote;
                $add_union_all = true;
            }
            if(isset($sql4EventNote)) {
                if($add_union_all) {
                    $sqlNote .= ' 
                UNION ALL ';                    
                }
                $sqlNote .= $sql4EventNote;
                $add_union_all = true;
            }
            if(isset($sql4OppNote)) {
                if($add_union_all) {
                    $sqlNote .= ' 
                UNION ALL ';                    
                }
                $sqlNote .= $sql4OppNote;
            }
            $sqlNote .= "
            ) NP
            LEFT JOIN people AS U ON NP.CreatedBy = U.PersonID 
            LEFT JOIN timezones AS TZ ON U.timezone = TZ.zid ";
            $sqlNote .= $sqlWhereNote;

            if((!$this->noteExport) && (!$this->notePrint)) {
                if ($this->pageNo > 1) {
                    $startRecord = (($this->pageNo - 1) * ($this->pageRecords)) + 1;
                    $endRecord = $startRecord + ($this->pageRecords - 1);
                } else {
                    $this->pageNo = 1;
                    $startRecord = 1;
                    $endRecord = $startRecord + ($this->pageRecords - 1);
                }

                $loadSqlClause[] = array(DTYPE_INT, $startRecord);
                $loadSqlClause[] = array(DTYPE_INT, $endRecord);
            }

            $sqlNote = SqlBuilder()->LoadSql($sqlNote)->BuildSqlParam($loadSqlClause);
            
            $resultNote = DbConnManager::GetDb('mpower')->Exec($sqlNote);

            //echo "<pre>";print_r($resultNote);"</pre>";
            
            /**
             * Get the array Of mapping fields.
             */
            $fieldMapObj = new fieldsMap();
            $allAccountFields = $fieldMapObj->parseAccountFields();
            $allContactFields = $fieldMapObj->parseContactFields();

            /**
             * Creates the array from the result of the Search Query.
             */
            $noteArr = array();
            //$objUser = new User();
            if(is_array($resultNote) && count($resultNote) > 0) {
                foreach ($resultNote as $note) {
                    $temp = array();
                    
                    $temp['NoteID'] = $note['NoteID'];
                    $temp['HasAlert'] = $note['HasAlert'];
                    $temp['TimeStamp'] = $this->getFormatedNoteTime($note['EmailID'], $note['NoteCreationDate'], $note['CreatedTimezone'], $note['zname']); 
                    $temp['NoteSpecialType'] = $note['NoteSpecialType'];
                    //$temp['Type'] = $note['ObjectType'];
                    //$temp['Creator'] = $note['NoteCreationDate'].'-->'.$note['NoteID'].'--->'.$note['CreatedTimezone'].'-->'.$_SESSION['USER']['TIMEZONE'].'--->'.$_SESSION['USER']['BROWSER_TIMEZONE'].'-->'.$note['FirstName'].' '.$note['LastName'];
                    $temp['Creator'] = $note['FirstName'].' '.$note['LastName'];
                    $temp['AccountID'] = $note['AccountID'];
                    $temp['Company'] = $note['CompanyName'];
                    $temp['Contact'] = $note['ContactFName'].' '.$note['ContactLName'];

                    if(trim($temp['Contact']) != '') {
                        $temp['ContactID'] = $note['ContactID'];
                    }
                    
                    if(trim($note['OppId']) != '') {
                        if($_SESSION['mpower']){
                            $temp['Opp'] = '<a class="blueLinksBold" href="#" onclick="javascript:var win = Windowing.openSizedWindow(\'legacy/shared/edit_opp2.php?SN='.$_COOKIE['SN'].'&detailview=1&reqOpId='.$note['OppId'].'&reqPersonId='.$note['OppPersonID'].'#'.$note['OppId'].'_'.$note['OppPersonID'].'\', 630, 1000);">'.$note['OppId'].'</a> : '.$note['OppFirstName'].' '.$note['OppLastName'];
                        }
                        else {
                            $temp['Opp'] = '<a href="slipstream.php?action=opportunity&opportunityId='. $note['OppId'] .'">' . $this->getOppTrackerOppName($note['OppId']) . '</a>';
                        }
                    }
                    else {
                        $temp['Opp'] = '';
                    }
                    
                    $temp['Type'] = $this->getNoteType($note['ObjectType'], $note['NoteSpecialType']);
                    $event_type = (($note['IsClosedEvent']) ? ('Closed ') : (($note['IsCancelledEvent']) ? ('Cancelled ') : ('')));
                    
                    if(strtolower($temp['Type']) == 'event') {  
                        $temp['Type'] = '<a href="slipstream.php?action=event&eventId='.$note['EventID'].'">'.$event_type. $temp['Type'];                               
                        if(trim($note['EventName']) != '') {
                            $temp['Type'] .= ' ('.$note['EventName'].') ';
                        }   
                        $temp['Type'] .= '</a>';                
                    }

                    if($note['ObjectType'] == NOTETYPE_CONTACT) {
                        if($note['EmailID'] > 0) {
                            $temp['Subject'] = $note['EmailSub'];
                            
                            $email_body = 'From: ';
                            if(trim($note['FromName']) != '') {
                                $email_body .= $note['FromName'].' ';
                            }
                            $email_body .= '&lt; '.$note['FromEmail'].' &gt; <br />';
                            $email_body .= 'Date: ';
                            if(trim($note['ReceivedDate']) != '') {
                                $email_body .= ReformatDate($note['ReceivedDate']).' '.DrawTime(strtotime($note['ReceivedDate']));
                            } 
                            $email_body .= '<br />';
                            $email_body .= 'To: ';
                            
                            $email_recipients = explode(';', $note['RecipientNames']);
                            $email_recipients_emails = explode(';', $note['RecipientEmails']);
                            
                            if(count($email_recipients) > 0) {
                                $str_email_recipients = '';
                                
                                foreach($email_recipients as $key => $email_recipient) {
                                    if($str_email_recipients != '') {
                                        $str_email_recipients .=  ', '.$email_recipient.' &lt; '.$email_recipients_emails[$key].' &gt;';
                                    } else {
                                        $str_email_recipients .=  $email_recipient.' &lt; '.$email_recipients_emails[$key].' &gt;';
                                    }
                                }
                                $email_body .= $str_email_recipients;
                            }
                            
                            if(trim($note['CcNames']) != '') {
                                $email_cc_recipients = explode(';', $note['CcNames']);
                                $email_cc_recipients_emails = explode(';', $note['CcEmails']);
                                
                                if(count($email_cc_recipients) > 0) {
                                    $email_body .= '<br />CC: ';
                                    $str_email_cc_recipients = '';
                                    
                                    foreach($email_cc_recipients as $key => $email_cc_recipient) {
                                        if($str_email_cc_recipients != '') {
                                            $str_email_cc_recipients .=  ', '.$email_cc_recipient.' &lt; '.$email_cc_recipients_emails[$key].' &gt;';
                                        } else {
                                            $str_email_cc_recipients .=  $email_cc_recipient.' &lt; '.$email_cc_recipients_emails[$key].' &gt;';
                                        }
                                    }
                                    $email_body .= $str_email_cc_recipients;
                                }
                            }
                            
                            $temp['Body'] = $email_body.'<br /><br />'.$note['EmailText'];
                            $temp['AddressType'] = $note['AddressType'];
                            
                            $obj_email = new Email();
                            $attachments = $obj_email->getEmailAttachments($note['EmailID']);

                            if(trim($attachments) != '') {
                                $temp['Attachment'] = $attachments;
                            }
                            else {
                                $temp['Attachment'] = '';
                            }
                        }
                        else {
                            $temp['Subject'] = $note['Subject'];
                            $temp['Body'] = $note['NoteText'];
                            $temp['AddressType'] = '';
                            $temp['Attachment'] = '';
                        }
                    } else {
                        $temp['Subject'] = str_replace('&##124#;', '|', $note['Subject']);
                        $temp['Body'] = str_replace('&##124#;', '|', $note['NoteText']);
                        $temp['AddressType'] = '';
                        $temp['Attachment'] = '';
                    }
                    $temp['EmailContentID'] = $note['EmailID'];
                    $temp['Private'] = $note['PrivateNote'];
                    $temp['Subject'] = Email::format($temp['Subject']);
                    $temp['Body'] = Email::format($temp['Body'], true);
                    
                    $temp['From'] = $note['FromName'];
                    $temp['FromEmail'] = $note['FromEmail'];                
                    
                    $temp['To'] = str_replace(';', ',', $note['RecipientNames']);
                    $temp['Cc'] = $note['CcNames'];
                    
                    /* Always Put this code at the end of the $temp array creation. */
                    if(($this->noteExport) || ($this->notePrint))   {
                        if(count($filterFields) > 0) {
                            foreach($filterFields as $newfields) {
                                $newfieldsArr = explode('_', $newfields);

                                if(count($newfieldsArr) == 2) {
                                    if($newfieldsArr[0] == 'CM') {                                      
                                        $temp['NewFields'][$allAccountFields[$newfieldsArr[1]]['Text']] = $note[$newfields];
                                    } else if($newfieldsArr[0] == 'CN') {
                                        $temp['NewFields'][$allContactFields[$newfieldsArr[1]]['Text']] = $note[$newfields];
                                    }
                                }
                            }
                        }
                    }                                                           
                    $noteArr[] = $temp;
                }
            }           
            return $noteArr;
        } catch (Exception $e) {
            
            $status->addError($e->getMessage());
        }
        return $status->getStatus();
    }

    /**
     * function to escape the special characters for creating a Xls document.
     * @param string $val - pass a string
     * return the escaped string.
     */
    function escapeXlsChar($val) {
        $val = str_replace("\r\n", '<br />', $val);
        $val = str_replace("\n", '<br />', $val);
        $val = str_replace('"', "''", $val);
        //$val = utf8_decode($val);
        return $val;
    }

    /**
     * function to get the note type.
     * @param int $type1 - pass the object type
     * @param int $type2 - pass the note special type
     * return the column number required to wrap the Note subject/text.
     */
    function getNoteType($type1, $type2) {
        $noteType = '';

        switch($type1)
        {
            case NOTETYPE_COMPANY:
                switch($type2) {
                    case NOTETYPE_INCOMING_CALL:
                        $noteType = "Incoming Call";
                        break;

                    case NOTETYPE_OUTGOING_CALL:
                        $noteType = "Outgoing Call";
                        break;

                    case NOTETYPE_EMAIL:
                        $noteType = "Email";
                        break;

                    default:
                        $noteType = "Company";
                }
                break;

            case NOTETYPE_CONTACT:
                switch($type2) {
                    case NOTETYPE_INCOMING_CALL:
                        $noteType = "Incoming Call";
                        break;

                    case NOTETYPE_OUTGOING_CALL:
                        $noteType = "Outgoing Call";
                        break;

                    case NOTETYPE_EMAIL:
                        $noteType = "Email";
                        break;

                    default:
                        $noteType = "Contact";
                }
                break;

            case NOTETYPE_EVENT:
                $noteType = "Event";
                break;
                
            case NOTETYPE_TASK:
                $noteType = "Task";
                break;

            case NOTETYPE_OPPORTUNITY:
                $noteType = "Opportunity";
                break;
        }
        return $noteType;
    }

    /**
     * function to check if the note's object(account/contact/event) is private.
     * @param int $type - pass the object type
     * @param int $obj_id - pass the object id.
     * return true, if it is private, else returns false.
     */
    function CheckPrivateType($type, $obj_id) {
        switch($type) {
            case NOTETYPE_COMPANY:
                $main_table = 'Account';
                $table_pkid = 'AccountID';
                $table_private_field = 'PrivateAccount';
                break;
            case NOTETYPE_CONTACT:
                $main_table = 'Contact';
                $table_pkid = 'ContactID';
                $table_private_field = 'PrivateContact';
                break;
            case NOTETYPE_EVENT:
            case NOTETYPE_TASK:
                $main_table = 'Event';
                $table_pkid = 'EventID';
                $table_private_field = 'Private';
                break;
            default:
                $main_table = '';
                $table_pkid = '';
                $table_private_field = '';
        }
        if(trim($main_table) != '') {
            $sql_private = 'SELECT '.$table_private_field.' FROM '.$main_table.' WHERE '.$table_pkid.' = ? ';
            $sql_private = SqlBuilder()->LoadSql($sql_private)->BuildSql(array(DTYPE_INT, $obj_id));

            $result_private = DbConnManager::GetDb('mpower')->Exec($sql_private);

            if($result_private[0][$table_private_field] == 1) {
                return true;
            }
        }
        return false;
    }
    function getPeople()
    {
        /*$sql='select PersonID,FirstName+" "+LastName as PeopleName from People where People.Deleted = 1 and SupervisorID!=-3 and CompanyID=? order by FirstName';
        $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['company_id']));        
        $resultInactive = DbConnManager::GetDb('mpower')->Exec($sql);*/
        
        $sql = 'select PersonID,FirstName+" "+LastName as PeopleName, People.Deleted Active 
                from People where CompanyID = ? and SupervisorID not in (-1, -3) 
                order by FirstName';
        $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['company_id']));        
        $result = DbConnManager::GetDb('mpower')->Exec($sql);
        return $result;
    }
    
    function getNoteFieldsByCompanyIdAndFormType($noteFormType){
        $sql='select * from NoteMap where CompanyID = ? AND Deleted = 0 AND FormType = ?';
        $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['company_id']),array(DTYPE_INT, $noteFormType));        
        $noteField = DbConnManager::GetDb('mpower')->Exec($sql);
        return $noteField;
    }
    
    function getNoteFieldsByCompanyIdAndNoteTemplateID($noteTempId){
        $sql='select * from NoteMap where CompanyID = ? AND Deleted = 0 AND NoteTemplateID = ?';
        $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['company_id']),array(DTYPE_INT, $noteTempId));      
        $noteField = DbConnManager::GetDb('mpower')->Exec($sql);
        return $noteField;
    }
    
    //This function returns the note date in correct format to display it in browser
    function getFormatedNoteTime($noteEmailID, $noteCreatedTime, $noteTimeZone, $createdUserZone) {     
        //If the note is an email note, it converts the time from the user timezone to browser timezone.
        // because the time saved for email note, is the email received time(local time), not the GMT time.

        if(trim($noteCreatedTime) == '') return '&nbsp;';

        $utc_tz = new DateTimeZone('UTC');
        $user_tz = new DateTimeZone(trim($_SESSION['USER']['BROWSER_TIMEZONE']));
        
        if (empty($createdUserZone)) {
            $note_tz = $user_tz;
        } else {
            $note_tz = new DateTimeZone($createdUserZone);
        }
        
        if(!is_null($noteEmailID) && $noteEmailID > 0) {
            $note_date = new DateTime($noteCreatedTime, $note_tz);
        } else {
            $note_date = new DateTime($noteCreatedTime, $utc_tz);
        }           

        $user_date = clone $note_date;

        $note_date->setTimezone($note_tz);
        $user_date->setTimezone($user_tz);
            
        $currentTimezone = trim($createdUserZone);
        $timezoneRequired = trim($_SESSION['USER']['BROWSER_TIMEZONE']);
        $noteTimeStamp = $user_date->format('D n/j/Y g:i A');

        /*
        if ($user_tz->getOffset($note_date) != $note_tz->getOffset($note_date)) {
            $noteTimeStamp .= ' (originally entered ' . $note_date->format('n/j/y g:ia') . ' ' . $note_tz->getName() . ') ';
        }
        */
            
        return $noteTimeStamp;
    }
    
    /**
     * A user who is Jetstream only (no mpower)
     * will need to see the Jetstream OppTracker
     * instead of linking to the mpower opp
     * @param int $id
     */
    function getOppTrackerOppName($id){
        $sql = "SELECT OpportunityName FROM ot_Opportunities WHERE OpportunityID = ?";
        $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id));        
        $result = DbConnManager::GetDb('mpower')->GetOne($sql);
        
        return $result['OpportunityName'];
    }
    
    /**
     * Get Pending notes.
     */
    function GetEmailPrivacyEnabled() {
        $sql = "SELECT EmailPrivacyEnabled FROM Company WHERE CompanyID = ?";
        $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->company_id));
        $results = DbConnManager::GetDb('mpower')->GetOne($sql);

        return (count($results) == 0 || !$results['EmailPrivacyEnabled']) ? false : true;
        
        
//      $result = $results['EmailPrivacyEnabled'];
//      echo $results;
//      return isset($results) && $results ? true : false;
    }
    
    function GetPendingNotes() {
        $utc_timezone = new DateTimeZone('UTC');
        $pending_date = new JetDateTime("now", $this->user_tz);
//      echo $pending_date->asMsSql('b', false);
        
        $sql = "SELECT EmailOffset FROM Company WHERE CompanyID = ?";
        $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->company_id));
        $results = DbConnManager::GetDb('mpower')->GetOne($sql);
        $offset = $results['EmailOffset'];
        if ($offset > 0) $offset = $offset - 1;
        //      $offset = 1;
        $pending_date->modify('-'.$offset.' days');
    
        $sql = "SELECT NoteID FROM Notes WHERE CompanyID = ? AND CreatedBy = ? AND CreationDate <= ? AND Subject = 'Email' AND ObjectType = 2 AND Incoming = 1";
        $params[] = array(DTYPE_INT, array($this->company_id, $this->user_id));
        $params[] = array(DTYPE_STRING, $pending_date->asMsSql('b', false));
    
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $notes = DbConnManager::GetDb('mpower')->Exec($sql);
    
        //      echo '<pre>';
        //      echo $sql;
        //      echo '</pre>';
    
        return $notes;
    }
    
    function CheckPastNotes($user_id) {
        // To save on having to make this request every time the page is loaded, I am
        // caching the result with a timeout to check again
        // Saves ~300ms  NY 4/6/2009
        $now = time();
    
        if (isset($_SESSION['past_notes_check'])) {
            $past_check = $_SESSION['past_notes_check'];
            if (isset($past_check['timeout']) && $now < $past_check['timeout']) {
                //return $past_check['response'];
            }
        }
    
        $this->user_id = $user_id;
    
        // $response = $this->GetPendingNotes() > 0;
        $response = count($this->GetPendingNotes()) > 0;
        // $response = false;
        $timeout_length = 120; // 2 minutes
    
        $_SESSION['past_notes_check'] = array('timeout' => $now + $timeout_length, 'response' => $response);
        return $response;
    
    }

    function getAccountIdByContactId($contactId) {
        $sql = 'select AccountId from contact Where ContactId = ?';
        $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $contactId));     
        $result = DbConnManager::GetDb('mpower')->Exec($sql);
        $accountId = $result[0]['AccountId'];
        return $accountId;
    }

    function getContactsByCompanyId($accountId) {
        $sql = 'select ContactId from contact Where Deleted != 1 and AccountId = ?';
        $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $accountId));     
        $result = DbConnManager::GetDb('mpower')->Exec($sql);
        $aContact = array();
        foreach($result as $contact) {
            $aContact[] = $contact['ContactId'];
        }
        return $aContact;
    }

    function getShowAllNotes($companyid) {
    
        $sql = "SELECT ShowAllNotesOfContact FROM Company WHERE CompanyID = ? ";
        $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyid));
        $company = DbConnManager::GetDb('mpower')->Exec($sql);
    
        $ShowAllNotesOfContact = (!empty($company) && count($company) > 0) ? $company[0]['ShowAllNotesOfContact'] : 0;
        ($ShowAllNotesOfContact != null) ? $ShowAllNotesOfContact : 0;

        return $ShowAllNotesOfContact;
    }
}
