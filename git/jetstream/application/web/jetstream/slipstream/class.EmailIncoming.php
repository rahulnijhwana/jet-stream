<?php
/**
 * @package slipstream
 */
 
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';

define('EMAIL_ATT_PATH', 'https://www.mpasa.com/attachments/');

/**
 * The EmailIncoming class
 */
class EmailIncoming
{

	function GetEmailIncoming($personId){

		$email_incoming = array();
		$field_map_obj = new fieldsMap();
		
		/* Contact table has 4 columns for Email
		 * Text16, Text20, Text59, Text60
		 * It's not readable so we need to map with fieldMap
		 * It's specific design pattern, besso
		 */
		$email_list = $field_map_obj->getContactEmailFields();

		$email_addresses = '';
		$email_addr = array();
		
		if(is_array($email_list) && !empty($email_list)) {
			foreach ($email_list as $key=>$email) {
				$email_addr[] = "EA.EmailAddress = C.$email";
				if ($key == 0) $email_addresses = " AND EA.EmailAddress = C.$email";
				else $email_addresses .= " OR EA.EmailAddress = C.$email";
			}
		}

//AND EA.EmailAddress = C.Text16 OR EA.EmailAddress = C.Text60 OR EA.EmailAddress = C.Text20 OR EA.EmailAddress = C.Text59
//echo '<pre>'; print_r($email_addresses); echo '</pre>'; exit;

		/* 
		 * Get company id with personID 
		 * People table is base for Company users (sales)
		 */
		$sql = "SELECT CompanyID FROM People WHERE PersonID = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $personId));
		$result = DbConnManager::GetDb('mpower')->GetOne($sql);
		$company_id = $result['CompanyID'];
		
		$user_tz = new DateTimeZone($_SESSION['USER']['BROWSER_TIMEZONE']);
		$pending_date = new JetDateTime("now", $user_tz);
		
		/*
		 * 508 (asa02) return 2 as EmailOffset
		 */
		$sql = "SELECT EmailOffset FROM Company WHERE CompanyID = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_id));
		$result = DbConnManager::GetDb('mpower')->GetOne($sql);
		$offset = $result['EmailOffset'];
		if ($offset > 0) $offset = $offset - 1;
		// $offset = 1;
	
		$pending_date->modify('-'.$offset.' days');
		
		// All
/*		$sql = "SELECT Notes.NoteID, Notes.EmailContentID, Notes.HasAlert, EmailContent.ReceivedDate, EmailContent.FromName, EmailContent.FromEmail, EmailContent.RecipientNames, EmailContent.RecipientEmails, EmailContent.CcNames, EmailContent.Subject, EmailContent.Body
		FROM Notes INNER JOIN EmailContent ON Notes.EmailContentID = EmailContent.EmailContentID
		WHERE (Notes.CreatedBy = ?) AND (Notes.Subject = 'Email') AND (Notes.Incoming = 1) AND (Notes.ObjectType = 2)
		ORDER BY Notes.CreationDate";
*/
		// Test
		// Incoming
		$sql = "SELECT Notes.NoteID, Notes.EmailContentID, Notes.HasAlert, EmailContent.ReceivedDate, EmailContent.FromName, EmailContent.FromEmail, EmailContent.RecipientNames, EmailContent.RecipientEmails, EmailContent.CcNames, EmailContent.Subject, EmailContent.Body
		FROM Notes INNER JOIN EmailContent ON Notes.EmailContentID = EmailContent.EmailContentID
		WHERE (Notes.CreatedBy = ?) AND (Notes.Subject = 'Email') AND (Notes.ObjectType = 2) AND (Notes.Incoming = 1) AND
			(Notes.EmailContentID IN (SELECT EmailContentID FROM EmailAddresses WHERE (PersonID = ?) AND (EmailAddressType = 'To' OR EmailAddressType = 'Cc')))		
		ORDER BY Notes.CreationDate";

//		$sql = "SELECT Notes.NoteID, Notes.EmailContentID, Notes.HasAlert, EmailContent.ReceivedDate, EmailContent.FromName, EmailContent.FromEmail, EmailContent.RecipientNames, EmailContent.RecipientEmails, EmailContent.CcNames, EmailContent.Subject, EmailContent.Body
//		FROM Notes INNER JOIN EmailContent ON Notes.EmailContentID = EmailContent.EmailContentID";
		
		// Outgoing
/*		$sql = "SELECT Notes.NoteID, Notes.EmailContentID, Notes.HasAlert, EmailContent.ReceivedDate, EmailContent.FromName, EmailContent.FromEmail, EmailContent.RecipientNames, EmailContent.RecipientEmails, EmailContent.CcNames, EmailContent.Subject, EmailContent.Body, EmailAttachments.OriginalName AS Attachment
		FROM Notes INNER JOIN EmailContent ON Notes.EmailContentID = EmailContent.EmailContentID LEFT OUTER JOIN
			EmailAttachments ON EmailContent.EmailContentID = EmailAttachments.EmailContentID
		WHERE (Notes.CreatedBy = ?) AND (Notes.Subject = 'Email') AND (Notes.ObjectType = 2) AND (Notes.Incoming = 1) AND
			(Notes.EmailContentID IN (SELECT EmailContentID FROM EmailAddresses WHERE (PersonID = ?) AND (EmailAddressType = 'From')))
		ORDER BY Notes.CreationDate";
*/		

		//$sqlBuild = SqlBuilder()->LoadSql($sql);	
//		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $personId), array(DTYPE_INT, $personId));		
	
/*		$params = array(
			array(DTYPE_INT, $personId),
			array(DTYPE_INT, $personId),
			array(DTYPE_INT, $personId)
		);
*/
		$params = array(
			array(DTYPE_INT, $personId),
			array(DTYPE_INT, $personId)
		);
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
//		$sqlBuild = SqlBuilder()->LoadSql($sql);	
//		$sql = $sqlBuild->BuildSql();		
//		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $personId), array(DTYPE_INT, $personId));		
		$result = DbConnManager::GetDb('mpower')->Exec($sql);		
		
//		echo $sql;
//		echo '<br>count: ' . count($result);
//		echo '<br >' . print_r ($result);
		
		if (isset($result) && (count($result) > 0)) {
//			echo 'before: ' . count($result);
			foreach ($result as $key=>$row) {
//				echo 'after: ' . trim($row['FromName']);

//				$email_incoming[$key]['NoteID'] = trim($row['NoteID']);
				$email_incoming[$key]['EmailContentID'] = trim($row['EmailContentID']);
				$date = new DateTime(trim($row['ReceivedDate']), $user_tz);
				$email_incoming[$key]['Date'] = $date->format('D n/j/Y g:i A');
				$email_incoming[$key]['FromName'] = trim($row['FromName']);
				$email_incoming[$key]['FromEmail'] = trim($row['FromEmail']);
				$email_incoming[$key]['RecipientNames'] = trim($row['RecipientNames']);
				$email_incoming[$key]['RecipientEmails'] = trim($row['RecipientEmails']);
				$email_incoming[$key]['CcNames'] = trim($row['CcNames']);
				$email_incoming[$key]['Subject'] = trim($row['Subject']);
				$email_incoming[$key]['HasAlert'] = trim($row['HasAlert']);

				$date1 = strtotime(trim($row['ReceivedDate']));
				$date2 = strtotime($pending_date->format('Y-m-d H:i:s'));
				
//				echo date('Y-m-d H:i:s', $date1)." ".date('Y-m-d H:i:s', $date2)."<br>";
				
				if ($date1 <= $date2)
					$email_incoming[$key]['Pending'] = 1;
				else
					$email_incoming[$key]['Pending'] = 0;
				
				$sql = "SELECT OriginalName, StoredName
					FROM EmailAttachments
					WHERE EmailContentID = ?";

				$params = array(
						array(DTYPE_INT, $row['EmailContentID'])
				);
				$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
				$result2 = DbConnManager::GetDb('mpower')->Exec($sql);

				$attachments = "";
				if (isset($result2) && (count($result2) > 0)) {
					foreach ($result2 as $key2=>$row2) {
						$attachments = $attachments . "<a href=\"" . EMAIL_ATT_PATH . trim($row2['StoredName']) . "\" target=\"_blank\">" . trim($row2['OriginalName']) . "</a>";
					}
				}
				$email_incoming[$key]['Attachment'] = $attachments;

//				echo 'after: ' . $email_incoming;
			
			}
		}
		
//		echo $sql;
//		echo 'after: ' . count($email_incoming);

		return $email_incoming;
	}
}
