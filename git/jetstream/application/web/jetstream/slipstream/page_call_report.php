<?php
require_once BASE_PATH . '/slipstream/class.JetstreamPage.php';
require_once BASE_PATH . '/slipstream/class.ModuleCallReport.php';

$page = new JetstreamPage();

$page->AddModule(new ModuleCallReport());

$page->Render();