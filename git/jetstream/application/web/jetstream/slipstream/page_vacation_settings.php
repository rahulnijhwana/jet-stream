<?php
require_once BASE_PATH . '/slipstream/class.JetstreamPage.php';
require_once BASE_PATH . '/slipstream/class.ModuleVacationSettings.php';
$objVacationSet = new ModuleVacationSettings();
$page = new JetstreamPage();
$page->AddModule($objVacationSet);
$page->Render();

?>