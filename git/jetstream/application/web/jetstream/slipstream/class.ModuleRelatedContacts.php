<?php
require_once BASE_PATH . '/slipstream/lib.php';
require_once BASE_PATH . '/slipstream/class.CompanyContact.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.Account.php';
require_once BASE_PATH . '/slipstream/class.Contact.php';
require_once BASE_PATH . '/slipstream/class.OptionUtil.php';


class ModuleRelatedContacts extends JetstreamModule
{
	public $title = 'Related Contacts';
	
	protected $javascript_files = array();
	protected $css_files = array();
	protected $template_files = array('module_related_contacts_new.tpl');
	protected $buttons = array();
	
	public $sidebar = TRUE;
	public $is_restricted = FALSE;
	public $msg = 'None Found';
	public $id = 'related_contacts';
	public $rec_type;
	public $rec_id;
	public $empty_module;

	public function __construct($company_id, $user_id) {
		if (empty($company_id) || empty($user_id)) {
			throw new Exception("Construct requires company_id & user_id");
		}
		$this->company_id = $company_id;
		$this->user_id = $user_id;
	}

	public function Init() {
		return;
	}

	public function Draw() {
		return;
	}
	
	public function Build($rec_type, $rec_id, $limit_access = FALSE, $empty_module = FALSE) {
		
		$this->AddButton('add20.png', "javascript:ShowAjaxForm('CompanyContactNew&action=add&type=contact');", 'Add');
		
		switch ($rec_type) {
			case 'contact' :
				$account_id_sql = SqlBuilder()->LoadSql("(SELECT AccountID FROM Contact WHERE ContactID = ?) AND C.ContactID <> ?")->BuildSql(array(DTYPE_INT, array($rec_id, $rec_id)));
				break;
			case 'account' :
				$account_id_sql = SqlBuilder()->LoadSql("?")->BuildSql(array(DTYPE_INT, $rec_id));
				break;
			default :
				return;
		}
		
		if (!$empty_module) {
			$contact_map_fname = MapLookup::GetContactFirstNameField($this->company_id, true);
			$contact_map_lname = MapLookup::GetContactLastNameField($this->company_id, true);
			$contact_map_title = MapLookup::GetContactTitleField($this->company_id, true);
			
			if(!$contact_map_title){
				$contact_map_title = 'Text99';
			}
			/*
			 * Some companies use a list of titles.
			 * We'll need to get the corresponding value from Options
			 * Step 1: Init OptionUtil()
			 */
			if(substr($contact_map_title, 0, 6) == 'Select'){
				$option_util = new OptionUtil(TABLE_CONTACT, $contact_map_title);
			}
			
			$is_manager = ReportingTreeLookup::IsManager($this->company_id, $this->user_id);
	
			if ($limit_access && !$is_manager) {

				$sql = "SELECT C.ContactID, AccountID, C.Inactive, C.$contact_map_lname + ', ' + C.$contact_map_fname as Name, C.$contact_map_title as Title
					FROM Contact C
					LEFT JOIN PeopleContact PC on C.ContactID = PC.ContactID
					WHERE AccountID = $account_id_sql
					AND PC.PersonID = ?
					AND Deleted = 0 
					AND (PrivateContact = 0 OR
						? IN (SELECT PersonID FROM PeopleContact WHERE ContactID = C.ContactID)
					) ORDER BY $contact_map_lname, $contact_map_fname";
				
				$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->user_id), array(DTYPE_INT, $this->user_id));
			
			}
			else {
				
				$sql = "SELECT ContactID, AccountID, Inactive, $contact_map_lname + ', ' + $contact_map_fname as Name, C.$contact_map_title as Title
					FROM Contact C
					WHERE AccountID = $account_id_sql
					AND Deleted = 0 
					AND (PrivateContact = 0 OR
						? IN (SELECT PersonID FROM PeopleContact WHERE ContactID = C.ContactID)
					) ORDER BY $contact_map_lname, $contact_map_fname";
				$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->user_id));
			
			}
			$contacts = DbConnManager::GetDb('mpower')->Exec($sql);
			
			/*
			 * Step 2: Get Option and update return values
			 */
			if(isset($option_util)){
				$count = count($contacts);
				for($i=0;$i<$count;$i++){
					if(strlen($contacts[$i]['Title']) > 0){
						$tmp = $option_util->convert($contacts[$i]['Title'], true);
						$contacts[$i]['Title'] = $tmp;
					}
				}
			}
			$this->contacts = $contacts;
		}
		else {
			$this->contacts = array();
		}
	}

}

