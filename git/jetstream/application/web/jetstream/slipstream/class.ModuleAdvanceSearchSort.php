<?php
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
/**
 * Class for Notes Sort module
 */
class ModuleAdvanceSearchSort extends JetstreamModule
{
	public $title = 'Sort Search Results';
	public $sidebar = true;	
	public $sort = array(array('company', 'contactfname', 'contactlname'), array('Company Name', 'Contact First Name', 'Contact Last Name'));
		
	public function __construct($sort_type, $sort_dir) {
		$this->sort_type = $sort_type;
		$this->sort_dir = $sort_dir;
	}
	
	public function Init() {	
		/**
		 * include tpl file for HTML section.
		 */
		$this->AddTemplate('module_advance_search_sort.tpl');
	}	
}

?>