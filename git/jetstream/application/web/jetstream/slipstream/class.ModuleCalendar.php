<?php
if ( !defined('BASE_PATH') ) {
    define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));
}
require_once BASE_PATH . '/include/class.GoogleCalendar.php';
require_once BASE_PATH . '/slipstream/class.Event.php';
require_once BASE_PATH . '/slipstream/class.Calendar.php';
// require_once BASE_PATH . '/include/class.ReportingTree.php';
require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';

class ModuleCalendar extends JetstreamModule
{
	public $title = 'Calendar';
	protected $css_files = array('calendar_new.css', 'event.css', 'note.css', 'calendar_button.css', 'task.css', 'jquery.cluetip.css');
	protected $javascript_files = array('calendar.js', 'jquery.cluetip.js', 'jquery.hoverIntent.js');
	protected $print_css_files = array('jet_cal_print.css');
	public $type;
	public $hours = array(0 => '12am', '1am', '2am', '3am', '4am', '5am', '6am', '7am', '8am', '9am', '10am', '11am', '12pm', '1pm', '2pm', '3pm', '4pm', '5pm', '6pm', '7pm', '8pm', '9pm', '10pm', '11pm');
	public $days = array();
	public $target;  // The users checked on the dashboard
	public $sales_people = array();  // The information necessary for this page to display
	protected $id = "calendar"; // Sets the ID for the resulting module
	public $selected_sales_person;
	public $company_id;
	public $user_id;
	
	public function Init($type = '', $day = '') {
		// if (isset($_COOKIE['LastVisitedContact']) && ($_COOKIE['LastVisitedContact'] > 0)) $this->subnav = array('Back To Contact' => 'javascript:window.location = \'?action=contact&contactId=' . $_COOKIE['LastVisitedContact'] . '\';');
		$this->company_id = $_SESSION['company_id'];
		$this->user_id = $_SESSION['login_id'];
		if (empty($this->type)) {
			if (!empty($type) && in_array($type, array('daily', 'weekly', 'monthly', 'fiveweek'))) {
				$this->type = $type;
			} elseif (isset($_COOKIE['Cal_Type']) && in_array($_COOKIE['Cal_Type'], array('daily', 'weekly', 'monthly', 'fiveweek'))) {
				$this->type = $_COOKIE['Cal_Type'];
			} else {
				$this->type = 'weekly';
			}
		}
		
		if (empty($this->day)) {
			if (!empty($day)) {
				$this->day = $day;
			} elseif (!empty($_COOKIE['Cal_Day'])) {
				$this->day = $_COOKIE['Cal_Day'];
			} else {
				$this->day = date('m/d/y');
			}
		}
	}

	public function Draw() {

		// Storing the permitted users in the session saves a DB call and the user sorting
		if (isset($_SESSION['cal_permitted_users'])) {
			$permitted_users = $_SESSION['cal_permitted_users'];
		} else {
			// Get and sort the IDs for the people this user can view on the cal
			// This may be too much - includes all descendants (GetLimb) for the current user
			// May need to be updated to only children later
			$permitted_users = array_unique(array_merge(ReportingTreeLookup::CleanGetLimb($this->company_id, $this->user_id), $this->GetCalPerms($this->user_id)));
			$permitted_users = ReportingTreeLookup::ActiveOnly($this->company_id, $permitted_users);
			$permitted_users = ReportingTreeLookup::SortPeople($this->company_id, $permitted_users, 'LastName');
			$_SESSION['cal_permitted_users'] = $permitted_users;
        }

		// Get the full person records from the reporting tree
		$this->sales_people = ReportingTreeLookup::GetRecords($this->company_id, $permitted_users);
		
		// Populate the targets
		if (isset($_GET['target'])) {
			if ($_GET['target'] != '') $this->target = explode(',', $_GET['target']);
		} elseif (isset($_SESSION['cal_targets'])) {
			$this->target = $_SESSION['cal_targets'];
		}
			
		// If no targets are set, check the box for the current user
		if (!isset($this->target) || (is_array($this->target) && count($this->target) == 0)) {
			$this->target = array($_SESSION['login_id']);
		}

		// Store them in the session for now - could move to COOKIE or DB for persistence
		$_SESSION['cal_targets'] = $this->target;
		
		$obj_events = new Event();
		$obj_calendar = new Calendar();
		
		$this->AddTemplate('module_calendar_new.tpl');
		
		$timestamp = strtotime($this->day);
		
		$toggle_str = '';
		
		if (isset($_REQUEST['view']) && (trim($_REQUEST['view']) != '')) {
			$_COOKIE['TOGGLE_VIEW'] = $_REQUEST['view'];
			$toggle_view = $_REQUEST['view'];
			$toggle_str = '&view=' . $_REQUEST['view'];
		} else if (isset($_COOKIE['TOGGLE_VIEW'])) {
			$toggle_view = $_COOKIE['TOGGLE_VIEW'];
		} else {
			$toggle_view = 'box';
		}
		$this->toggle_view = $toggle_view;
		
		if ($toggle_view == 'box') {
			$show_toggle_view = 'list';
		} else {
			$show_toggle_view = 'box';
		}
		
		$query_string = str_replace($toggle_str, '', $_SERVER['QUERY_STRING']);
		
		if (isset($_REQUEST['action']) && in_array($_REQUEST['action'], array('daily', 'weekly'))) {
			$cal_action = $_REQUEST['action'];
		} else {
			$cal_action = 'weekly';
		}
		
		if (!isset($_REQUEST['action']) || (isset($_REQUEST['action']) && ($_REQUEST['action'] != 'monthly'))) {
			$view_term = ($toggle_view == 'box') ? 'List' : 'Calendar';
			$this->AddButton($toggle_view . '.png', "javascript:GetCalendar('$cal_action', '" . date("m/d/Y", $timestamp) . "', '$this->selected_sales_person',1, '$show_toggle_view')", $view_term . ' View');
		}
		$this->AddButton('print24.png', "javascript:window.print();", 'Print');
		
		if ($this->type == 'daily') {
			$this->AddMenuItem('Day', "javascript:GetCalendar('daily', '" . date("m/d/Y", $timestamp) . "', '$this->selected_sales_person',1)", TRUE);
			$this->title = 'Calendar: Daily';
			$this->days = $this->getWeekDayList($format = 'D m/d', $num_days = 1);
			$date = getdate($timestamp);
			$this->previous = date('m/d/Y', mktime(0, 0, 0, $date['mon'], $date['mday'] - 1, $date['year']));
			$this->next = date('m/d/Y', mktime(0, 0, 0, $date['mon'], $date['mday'] + 1, $date['year']));
		} else {
			$this->AddMenuItem('Day', "javascript:GetCalendar('daily', '" . date("m/d/Y", $timestamp) . "', '$this->selected_sales_person',1)");
		}
		
		if ($this->type == 'weekly') {
			$this->AddMenuItem('Week', "javascript:GetCalendar('weekly', '" . date("m/d/Y", $timestamp) . "', '$this->selected_sales_person',1)", TRUE);
			$this->title = 'Calendar: Weekly';
			$this->days = $this->getWeekDayList($format = 'D M&\n\b\s\p;j', $num_days = 0);
			$date = getdate($timestamp);
			$this->previous = date('m/d/Y', mktime(0, 0, 0, $date['mon'], $date['mday'] - 7, $date['year']));
			$this->next = date('m/d/Y', mktime(0, 0, 0, $date['mon'], $date['mday'] + 7, $date['year']));
		} else {
			$this->AddMenuItem('Week', "javascript:GetCalendar('weekly', '" . date("m/d/Y", $timestamp) . "', '$this->selected_sales_person',1)");
		}
		
		if ($this->type == 'monthly') {
			$this->AddMenuItem('Month', "javascript:GetCalendar('monthly', '" . date("m/d/Y", $timestamp) . "', '$this->selected_sales_person',1)", TRUE);
			$this->title = 'Calendar: Monthly';
			
			$month = date('m', $timestamp);
			$year = date('Y', $timestamp);
			$this->days = $obj_calendar->showCalendar($month, $year);
			$this->display_date = date('F Y', $timestamp);
			
			$this->previous = date('m/d/Y', mktime(0, 0, 0, $month - 1, 1, $year));
			$this->next = date('m/d/Y', mktime(0, 0, 0, $month + 1, 1, $year));
		} else {
			$this->AddMenuItem('Month', "javascript:GetCalendar('monthly', '" . date("m/d/Y", $timestamp) . "', '$this->selected_sales_person',1)");
		}
		
		if ($this->type == 'fiveweek') {
			$this->AddMenuItem('Next 5 Weeks', "javascript:GetCalendar('fiveweek', '" . date("m/d/Y", $timestamp) . "', '$this->selected_sales_person',1)", TRUE);
			$this->title = 'Calendar: Next 5 Weeks';
			
			// The 5 week calendar always starts from today
			$this->day = date('m/d/y');
			$this->days = $this->getWeekDayList($format = 'M j', $num_days = '5week');
			$this->display_date = "Next 5 Weeks";

			// Prev on the 5 week calendar is the month of one day before the first day shown
			$first_day = array_slice($this->days, 0, 1);
			$first_day = date_parse($first_day[0]['date']);
			$this->previous = date('m/d/Y', mktime(0, 0, 0, $first_day['month'], $first_day['day'] - 1, $first_day['year']));

			// Prev on the 5 week calendar is the month of one day after the last day shown
			$last_day = array_slice($this->days, -1, 1);
			$last_day = date_parse($last_day[0]['date']);
			$this->next = date('m/d/Y', mktime(0, 0, 0, $last_day['month'], $last_day['day'] + 1, $last_day['year']));
		} else {
			$this->AddMenuItem('Next 5 Weeks', "javascript:GetCalendar('fiveweek', '" . date("m/d/Y", $timestamp) . "', '$this->selected_sales_person',1)");
		}
		
		$this->day_of_week = date('N', $timestamp);
		
		$start_date = reset($this->days);
		$start_date = $start_date['date'];
		
		$end_date = end($this->days);
		$end_date = $end_date['date'];
		
		if (!isset($this->display_date)) {
			if ($start_date == $end_date) {
				$this->display_date = date('l, F j, Y', $timestamp);
			} else {
				if (date('m', strtotime($start_date)) != date('m', strtotime($end_date))) {
					$end_format = 'F j, Y';
					
					if (date('Y', strtotime($start_date)) != date('Y', strtotime($end_date)))
						$start_format = 'F j, Y';
					else
						$start_format = 'F j';
				} else {
					$start_format = 'F j';
					$end_format = 'j, Y';
				}
				
				$this->display_date = date($start_format, strtotime($start_date)) . ' - ' . date($end_format, strtotime($end_date));
			}
		}

		$events = $obj_events->GetEventsByDateRange($start_date, $end_date, $this->target, 'BOTH');

		foreach ($events as $event_id => $event) {
			if ($event['Type'] == 'TASK') {
				if (isset($this->days[$event['start']])) {
					$this->days[$event['start']]['tasks'][$event_id] = $event;
					if (!isset($this->days[$event['start']]['closed_task_count'])) {
						$this->days[$event['start']]['closed_task_count'] = 0;
					}
					if ($event['Closed']) {
						$this->days[$event['start']]['closed_task_count']++;
					}
				}
			} else {
				// Iterate through all the days spanned by this event
				for($event_day = $event['start']; $event_day <= $event['end']; $event_day++) {
					// Skip this one if this is the end day and the end time falls on midnight
					if ($event_day == $event['end'] && $event['end_date']->format('Hi') == '0000') {
						continue;
					}
					// Only process the day if it is within the timespan for this calendar
					if (isset($this->days[$event_day])) {
						if ($this->type != 'monthly') {
							// If today is the events start day, offset it, otherwise start it at midnight
							if ($event['start'] == $event_day) {
								$event['VerticalOffset'] = $event['start_date']->format('H') * 4 + $event['start_date']->format('i') / 15;
							} else {
								$event['VerticalOffset'] = 0;
							}
							
							if ($event['end'] == $event_day) {
								$event['Height'] = $event['end_date']->format('H') * 4 + $event['end_date']->format('i') / 15 - $event['VerticalOffset'];
							} else {
								$event['Height'] = 96 - $event['VerticalOffset'];
							}
							
							$collision = $this->GetCollisions($events, $event_id);
							$event['LeftPosition'] = ($collision['count'] == 0) ? 0 : (100 / $collision['count']) * $collision['position'];
							$event['Width'] = ($collision['count'] == 0) ? 100 : 100 / $collision['count'];
						}
						$this->days[$event_day]['events'][$event_id] = $event;
					}
				}

					
			}
		}
	}

	function GetCollisions($events, $event_id) {
		$colision_list = array();
		
		$collision['count'] = 0;
		$collision['position'] = 0;
		
		if (isset($events)) {
			foreach ($events as $sibling_event_id => $sibling_event) {
				if ($sibling_event['Type'] == 'TASK') continue; 
				$event_start = $events[$event_id]['start_date'];
				$event_end = $events[$event_id]['end_date'];
				$current_event_start = $sibling_event['start_date'];
				$current_event_end = $sibling_event['end_date'];
				
				if ($event_start < $current_event_end && $event_end > $current_event_start) {
					if ($sibling_event_id == $event_id)
						$collision['position'] = $collision['count'];
					else
						$colision_list[$sibling_event_id] = $sibling_event;
					$collision['count']++;
				}
			}
		}
		
		$list = array();
		if (isset($colision_list) && count($colision_list) > 0) {
			$event_id = array_shift(array_keys($colision_list));
			$list[] = $event_id;
			
			foreach ($colision_list as $sibling_event_id => $sibling_event) {
				$event_start = $colision_list[$event_id]['start_date'];
				$event_end = $colision_list[$event_id]['end_date'];
				$current_event_start = $sibling_event['start_date'];
				$current_event_end = $sibling_event['end_date'];
				
				if (($event_start < $current_event_end && $event_end > $current_event_start) || ($event_start == $current_event_start && $event_end == $current_event_end)) {
					if ($event_id != $sibling_event_id) $list[] = $sibling_event_id;
				}
			}
		}
		
		$collision['count'] = 1 + count($list);
		
		return $collision;
	}

	function getWeekDayList($format = 'D m/d', $num_days = 0) {
		if ($num_days == 0 || $num_days == '5week') {
			if ($num_days == 0) {
				$num_days = 7;
			} else {
				$num_days = 35;
			}
			$week_day = date("w", strtotime($this->day));
			$tw_start_tstamp = strtotime($this->day) - ($week_day * 24 * 3600) + 3600;
		} else {
			$tw_start_tstamp = strtotime($this->day);
		}
		
		$cur_month = date("n", time());
		
		$week_date_list = array();
		for($i = 0; $i < $num_days; ++$i) {
			$this_date = mktime(0, 0, 0, date('m', $tw_start_tstamp), date('d', $tw_start_tstamp) + $i, date('Y', $tw_start_tstamp));
			$stamp = date('Ymd', $this_date);
			$week_date_list[$stamp]['Day'] = $week_date_list[$stamp]['day'] = date($format, $this_date);
			// $dt[$date]['day'] = date($format, $this_date);
			$week_date_list[$stamp]['LongDay'] = date('l, F j, Y', $this_date);
			$week_date_list[$stamp]['date'] = date('m/d/Y', $this_date);
			$week_date_list[$stamp]['date_string'] = NormalDateFormat($this_date);
			//$week_date_list[$stamp]['date_string'] = "10/1/2012 1:45 pm";

			// TODO:  Configure it for week offsets for companies with different years
			$week_date_list[$stamp]['week'] = date('W', $this_date);

			$this_month = date('n', $this_date);
			if ($this_month == $cur_month) {
				$week_date_list[$stamp]['type'] = 'current';
			} elseif ($this_month < $cur_month) {
				$week_date_list[$stamp]['type'] = 'prev';
			} else {
				$week_date_list[$stamp]['type'] = 'next';
			}
			// $week_date_list[$stamp]['DayName'] = date('l', $this_date);
			
			
			//$dt[$date]['type'] = 'prev';
				//$day = date('j', $getTimeStamp);
				//$dt[$date]['date']
				
			if ($this_date == mktime(0, 0, 0)) {
				$week_date_list[$stamp]['today'] = TRUE;
			}
		}
		
		return $week_date_list;
	}

	function GetCalPerms($personId) {
		$sql = "SELECT OwnerPersonID FROM CalendarPermission WHERE PersonID = ? AND (ReadPermission = 1 OR WritePermission = 1)";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $personId));
		$results = DbConnManager::GetDb('mpower')->Exec($sql);
		$people = array();
		foreach($results as $result) {
			$people[] = $result['OwnerPersonID'];
		}
		return $people;
	}
	
	function checkHasCalPermit_remove($personId) {
		$sql = "SELECT OwnerPersonID FROM CalendarPermission WHERE PersonID = ? AND (ReadPermission = 1 OR WritePermission = 1)";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $personId));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		if (count($result) > 0) {
			$this->has_cal_permit = TRUE;
		}
		return $result;
	}
}

?>