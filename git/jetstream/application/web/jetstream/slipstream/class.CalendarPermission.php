<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once 'class.Status.php';

class CalendarPermission
{

	public function GetUserCalendarPermission($company_id, $personid) {
		$sql = "SELECT FirstName, LastName, PersonID, SupervisorID FROM People WHERE CompanyID = ? AND SupervisorID not in (-3, -1) AND PersonID != ? AND Deleted = 0 ORDER BY LastName";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_id), array(DTYPE_INT, $personid));
		// echo $sql . '<br>';
		$rs = DbConnManager::GetDb('mpower')->Exec($sql);
		//print_r($rs);
		if (count($rs) > 0) {
			foreach ($rs as $key => $result) {
				$personId = $result['PersonID'];
				$sql = "SELECT * FROM CalendarPermission WHERE PersonID = ? ";
				$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $personId));
				// echo $sql . '<br>';
				$rset = DbConnManager::GetDb('mpower')->Exec($sql);
				$rs[$key]['CalendarPermission'] = $rset;
			}
			// echo '<pre>' . print_r($rs, TRUE) . '</pre>';
			return $rs;
		} else {
			return '';
		}
	}

	public function GetSingleUserCalPerms($person_id) {
		$sql = "SELECT * from CalendarPermission WHERE OwnerPersonID = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $person_id));
		$results = DbConnManager::GetDb('mpower')->Exec($sql);
		$output = array();
		foreach ($results as $result) {
			$output[$result['PersonID']] = $result;
		}
		return $output;
	}
	
	public function SaveUserCalendarPermission($writeUser, $unWriteUser, $userId, $lockedUser = '', $unlockedUser = '', $readUser, $unReadUser) {
		$writeUserArray = explode(',', $writeUser);
		$unWriteUserArray = explode(',', $unWriteUser);
		$readUserArray = explode(',', $readUser);
		$unReadUserArray = explode(',', $unReadUser);
		$lockedUserArray = explode(',', $lockedUser);
		$unlockedUserArray = explode(',', $unlockedUser);
		$this->updateCalendarPermission($writeUserArray, $userId, 1, 'WritePermission');
		$this->updateCalendarPermission($unWriteUserArray, $userId, 0, 'WritePermission');
		$this->updateCalendarPermission($readUserArray, $userId, 1, 'ReadPermission');
		$this->updateCalendarPermission($unReadUserArray, $userId, 0, 'ReadPermission');
		$this->updateCalendarPermission($lockedUserArray, $userId, 1, 'LockPermission');
		$this->updateCalendarPermission($unlockedUserArray, $userId, 0, 'LockPermission');
	}

	public function updateCalendarPermission($userPermisionArray, $userId, $value, $col) {
		for($m = 0; $m < (count($userPermisionArray) - 1); $m++) {
			if (!empty($userPermisionArray[$m])) {
				$sql = "SELECT * FROM CalendarPermission WHERE PersonID = ? AND OwnerPersonID = ?";
				$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $userPermisionArray[$m]), array(DTYPE_INT, $userId));
				$rs = DbConnManager::GetDb('mpower')->Exec($sql);
				if (count($rs) > 0) {
					$sql = 'update CalendarPermission set ' . $col . ' = ' . $value . ' where PersonID=? AND OwnerPersonID = ?';
					$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $userPermisionArray[$m]), array(DTYPE_INT, $userId));
					$rs = DbConnManager::GetDb('mpower')->Execute($sql);
				} else {
					$sql = 'Insert into CalendarPermission (' . $col . ',PersonID,OwnerPersonID) values(' . $value . ',' . $userPermisionArray[$m] . ',' . $userId . ') ';
					$sql = SqlBuilder()->LoadSql($sql)->BuildSql();
					$rs = DbConnManager::GetDb('mpower')->Exec($sql);
				}
			}
		}
	}
}

?>