<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once 'class.Status.php';

class CompanyAccountContactTemp extends MpRecord
{

	public function __toString()
	{
		return 'CompanyAccountContactTemp Record';
	}

	public function Initialize()
	{
		$this->db_table = 'CompanyAccountContactTemp';
		$this->recordset->GetFieldDef('CompanyAccountContactTempID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('CompanyID')->required = TRUE;
		$this->recordset->GetFieldDef('RecType')->required = TRUE;
		parent::Initialize();
	}

	function insertTemplateDataByCompanyId($companyId, $recType, $tempData)
	{
		$status = new Status();
		$sql = "SELECT * FROM CompanyAccountContactTemp WHERE CompanyAccountContactTempID = -1";
		$rs = DbConnManager::GetDb('mpower')->Execute($sql);
		$this->SetDatabase(DbConnManager::GetDb('mpower'));
		$this->recordset = $rs;
		$this->Initialize();
		$this->CompanyID = $companyId;
		$this->RecType = $recType;
		$this->TemplateData = $tempData;
		$this->Save();
		return $status;
	}

	function getTemplateDataByCompanyId($companyId, $recType)
	{
		$sql = "select TemplateData from CompanyAccountContactTemp where CompanyID = ? and RecType = ?";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId), array(DTYPE_INT, $recType));
		$tempData = DbConnManager::GetDb('mpower')->Exec($sql);
		return $tempData;
	}

	function updateTemplateDataByCompanyId($companyId, $recType, $tempData)
	{
		$status = new Status();
		$sql = "SELECT * FROM CompanyAccountContactTemp WHERE CompanyID = ? AND RecType = ?";
		$tempSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyId),array(DTYPE_INT, $recType));
		$result = DbConnManager::GetDb('mpower')->Execute($tempSql, 'CompanyAccountContactTemp');
		if (count($result) == 1) {
			$objTemp = $result[0];
			$objTemp ->Initialize();
			$objTemp ->TemplateData = $tempData;
			$result->Save();
		}
		return $status;
	}
}

?>