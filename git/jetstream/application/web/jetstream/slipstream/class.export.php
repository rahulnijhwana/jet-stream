<?php
/*
 *	Will export .xls, .xlsx, and .csv 
 *  
 *  User needs to set some of the values 
 *  $properties ( Creator, LastModifiedBy, Title, Subject, Description, Keywords, Category)  []
 *  $header (header, footer)[**]
 *  $values () [**]
 *	$page 
 *	$headerFooter
 *  $filename
 *
 *	[*] - mandatory , [] - optional 
 */

require_once BASE_PATH .'/libs/PHPExcel.php';
require_once BASE_PATH .'/libs/PHPExcel/IOFactory.php';


class export {
	public $properties = array () ; 
	public $headers = array () ;
	public $datas = array () ; 
	public $headerFooter = array () ; 
	public $page = array () ; 
	public $filename = array () ; 

	function xlscolumn($i){
		$i = $i - 1 ; 
		$remainder = $i % 26 ; 
		$dividend = ( $i - $remainder )/ 26  ; 	
		return ($dividend > 0 ) ? chr($dividend-1  +65 ) . chr($remainder +65 ) : chr($remainder +65);
	}

	public function xlsx() {

		$x = 0 ;
		$y = 0 ;
		$objPHPExcel = new PHPExcel();
		
		// Setting properties
		// Example 
		//$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
		//					 ->setLastModifiedBy("Maarten Balliauw")
		//					 ->setTitle("Office 2007 XLSX Test Document")
		//					 ->setSubject("Office 2007 XLSX Test Document")
		//					 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
		//					 ->setKeywords("office 2007 openxml php")
		//					 ->setCategory("Test result file");		
		//
		if ( count($this->properties) > 0 ) {
			if ( !empty($this->properties['Creator']) ) $objPHPExcel->getProperties()->setCreator($this->properties['Creator']);
			if ( !empty($this->properties['LastModifiedBy']) ) $objPHPExcel->getProperties()->setLastModifiedBy($this->properties['LastModifiedBy']);
			if ( !empty($this->properties['Title']) ) $objPHPExcel->getProperties()->setTitle($this->properties['Title']);
			if ( !empty($this->properties['Subject']) ) $objPHPExcel->getProperties()->setSubject($this->properties['Subject']);
			if ( !empty($this->properties['Description']) ) $objPHPExcel->getProperties()->setDescription($this->properties['Description']);
			if ( !empty($this->properties['Keywords']) ) $objPHPExcel->getProperties()->setKeywords($this->properties['Keywords']);
			if ( !empty($this->properties['Category']) ) $objPHPExcel->getProperties()->setCategory($this->properties['Category']);
		}

		// Not implemented Yet
		// Set page orientation and size
		//	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		//	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		// default paper A4 orientation POTRAIT or LANDSCAPE
		if ( count($this->page) > 0 ){
			$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		}
		
		//	Set header and footer. When no different headers for odd/even are used, odd header is assumed.
		//	$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&G&C&HPlease treat this document as confidential!');
		//	$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');
	
		if ( count($this->headerFooter) > 0 ) {
			if ( !empty($this->headerFooter['Header']) ) $objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddHeader($this->headerFooter['Header']);
			if ( !empty($this->headerFooter['Footer']) ) $objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter($this->headerFooter['Footer']);
		}

		//	Not Implemented Yet 
		// Add a drawing to the header
/*		$objDrawing = new PHPExcel_Worksheet_HeaderFooterDrawing();
		$objDrawing->setName('PHPExcel logo');
		$objDrawing->setPath('./images/phpexcel_logo.gif');
		$objDrawing->setHeight(36);
		$objPHPExcel->getActiveSheet()->getHeaderFooter()->addImage($objDrawing, PHPExcel_Worksheet_HeaderFooter::IMAGE_HEADER_LEFT);
		//
		*/
		
		// Add Headers 
		if ( !empty ($this->headers)){
			$x++;
			
			for ($y =0 ; $y < count($this->headers) ; $y++){
				$objPHPExcel->getActiveSheet()->setCellValue( $this->xlscolumn($y+1).$x, $this->headers[$y]) ; 
			}
		}
		else {
			echo "ERROR: header is empty";
			exit; 
		}
		// Add data
		if ( !empty($this->headers) ){
			foreach ($this->datas as $value){
				$x++;
				for ($y =0 ; $y < count($value) ; $y++){
					$objPHPExcel->getActiveSheet()->setCellValue( $this->xlscolumn($y+1).$x, $value[$y]) ; 
				}
			}
		}
		else {
			echo "ERROR: Value is empty";
			exit; 
		}
		
		// Rename sheet
		$objPHPExcel->getActiveSheet()->setTitle('Printing');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		
		if (!empty($this->filename)) {
			header('Content-Disposition: attachment;filename="'.$this->filename[0].'.xls"');
		}
		else {
			header('Content-Disposition: attachment;filename="jetstream.xls"');
		}
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		
	}
}
?>