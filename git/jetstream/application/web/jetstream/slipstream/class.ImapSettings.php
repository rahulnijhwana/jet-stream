<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';

class ImapSettings {
	
	/**
	 * Determine if IMAP is enabled for a user
	 * User may have multiple accounts
	 * Only returns true if one is active
	 */
	public static function isEnabled(){
		
		/*
		$sql = "SELECT COUNT(*) AS enabled
				FROM ImapUserSettings
				WHERE PersonID = ? AND Enabled = 1";
		*/
		
		$sql = "SELECT Enabled
				FROM ImapCompanySettings
				WHERE CompanyID = ?";
		
		$params = array(DTYPE_INT, $_SESSION['USER']['COMPANYID']);
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql($params);
		$result = DbConnManager::GetDb('mpower')->GetOne($sql);
		
		return $result->Enabled;
	}
	
	
	/**
	 * Save the updated IMAP user credentials
	 * @param int $id
	 * @param String $username
	 * @param String $password
	 */
	public static function save($id, $username, $password){
		
		/*
		 * User clicked "Save" button without changing 
		 * credentials. Retain existing.
		 */
		if($password === 'fakepass' || !$id){
			return true;
		}
		
		$sql = "
			UPDATE ImapUserSettings
			SET
				ImapUsername = ?,
				ImapPassword = ?,
				Enabled = 1
			WHERE
				ImapUserSettingsID = ?";

		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(
			array(DTYPE_STRING, $username),
			array(DTYPE_STRING, $password),
			array(DTYPE_INT, $id)
		);
		DbConnManager::GetDb('mpower')->Exec($sql);
		return true;
	}
}