<?php
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.VacationSettings.php';
require_once '../include/class.ReportingTreeLookup.php';

class ModuleVacationSettings extends JetstreamModule
{
	public $title = 'Vacation Settings';
	protected $javascript_files = array('vacation_settings.js', 'ui.datepicker.js');
	protected $css_files = array('js_admin.css', 'ui.datepicker.css');
	public $user_details;
	public $vac_perms;
	public $user_id;
	public $supervisor_id;

	public function Init() {
		$objVacation = new VacationSettings();
		
		$this->user_id = $_SESSION['USER']['USERID'];
		$company_id = $_SESSION['USER']['COMPANYID'];

		ReportingTreeLookup::LoadFromDb($company_id);
		$this->user_details = ReportingTreeLookup::GetDirectChildren($company_id, $this->user_id);
		$this->user_details[] = $this->user_id;
//		$this->user_details = ReportingTreeLookup::GetCombined($company_id);
		$this->user_details = ReportingTreeLookup::ActiveOnly($company_id, $this->user_details);
		$this->user_details = ReportingTreeLookup::SortPeople($company_id, $this->user_details, 'LastName');
		$this->user_details = ReportingTreeLookup::GetRecords($company_id, $this->user_details);

		// Remove the current user from the list
		// unset($this->user_details[$this->user_id]);
		
//		$this->user_details = $objVacation->GetUserVacationSettings($company_id, $this->user_id);

//		echo print_r($this->user_details)."<br>";
//		echo print_r($this->vac_perms);
		
//		$supervisor = $_SESSION['USER']['SUPERVISORID'];
		// Supervisor is always allowed read/write locked - so update the perms to reflect this
//		if ($supervisor > 0) {
//			$this->vac_perms[$supervisor]['ReadSettings'] = true;
//			$this->vac_perms[$supervisor]['WriteSettings'] = true;
//			$this->vac_perms[$supervisor]['LockSettings'] = true;
//		}
		
		// $this->supervisor_id = $_SESSION['USER']['SUPERVISORID'];
		
		// exit;
		
		// $this->user_details = $objCalendar->GetUserCalendarPermission($_SESSION['USER']['COMPANYID'], $_SESSION['USER']['USERID']);
		//print_r($this->user_details);
		//$this->AddTemplate('snippet_contact_mail_fields.tpl');
		$this->AddTemplate('module_vacation_settings.tpl');
	}
}

?>
