<?php

class JetstreamModule
{
	protected $javascript_files = array();
	protected $css_files = array();
	protected $print_css_files = array();
	protected $buttons = array();
	protected $menu = array();
	protected $template_files = array();
	protected $subnav = array();
	protected $formatting = '';
	public $sidebar = FALSE;
	public $content_main_title = FALSE;
	public $content_sub_title = FALSE;

	public function AddJavascript() {
		foreach (func_get_args() as $file) {
			$this->javascript_files[] = $file;
		}
		return $this;
	}

	public function AddCss() {
		foreach (func_get_args() as $file) {
			$this->css_files[] = $file;
		}
		return $this;
	}
	
	public function AddPrintCss() {
		foreach (func_get_args() as $file) {
			$this->print_css_files[] = $file;
		}
		return $this;
	}

	public function AddTemplate() {
		foreach (func_get_args() as $file) {
			$this->template_files[] = $file;
		}
		return $this;
	}

	public function AddButton($icon, $action, $tooltip, $id = '', $target = '') {
		$this->buttons[] = array('icon' => $icon, 'action' => $action, 'tooltip' => $tooltip, 'id' => $id, 'target' => $target);
		return $this;
	}

	public function AddMenuItem($label, $action, $selected = false) {
		$this->menu[] = array('label' => $label, 'action' => $action, 'selected' => $selected);
	}
	
	public function __get($name) {
		if (!isset($this->$name)) return false;
		return $this->$name;
	}
	
	public function GetImploded($name) {
		return implode(',', $this->$name);
	}

	public function InitAjax() {
		return;
	}
	
	public function Init() {
		return;
	}

	public function Draw() {
		return;
	}
}
