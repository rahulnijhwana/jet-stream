<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once 'class.Status.php';

class NoteOption extends MpRecord
{

	public function __toString()
	{
		return 'Note Option Record';
	}

	public function Initialize()
	{
		$this->db_table = 'NoteOption';
		$this->recordset->GetFieldDef('NoteOptionID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('NoteMapID')->required = TRUE;
		parent::Initialize();
	}
	
	public function saveNoteOptions($noteMapId,$option){
		$status = new Status();
		$sql = "SELECT * FROM NoteOption WHERE NoteOptionID = -1";
		$rs = DbConnManager::GetDb('mpower')->Execute($sql);
		$this->SetDatabase(DbConnManager::GetDb('mpower'));
		$this->recordset = $rs;
		$this->Initialize();
		$this->NoteMapID = $noteMapId;
		$this->OptionName = $option;
		$this->Save();
		return $status;
	}
	
	public function deleteOptionsByMapId($noteMapId){
		$status = new Status();
		$sql = 'Delete FROM NoteOption WHERE NoteMapID = ?';
		$noteSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $noteMapId));
		$result = DbConnManager::GetDb('mpower')->Exec($noteSql);
		return $status;
	}
	
	public function getNoteOptionsByMapId($noteMapId){
		$status = new Status();
		$sql = 'Select NoteOptionID,OptionName FROM NoteOption WHERE NoteMapID = ?';
		$noteSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $noteMapId));
		$result = DbConnManager::GetDb('mpower')->Exec($noteSql);
		return $result;
	}
	
	function updateNoteOptions($optionId,$optionName){
		$status=new Status();
		$sql = "SELECT * FROM NoteOption WHERE NoteOptionID = ?";
		$optionSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $optionId));
		$result = DbConnManager::GetDb('mpower')->Execute($optionSql,'NoteOption');
		if (count($result) == 1) {
			$objOption = $result[0];
			$objOption->Initialize();
			$objOption->OptionName = trim($optionName);
			$result->Save();
		}
		return $status;
	}
	
	function deleteNoteOptionsByOptionId($optionId){
		$sql="delete from NoteOption where NoteOptionID = ?";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $optionId));
		$options = DbConnManager::GetDb('mpower')->Exec($sql);
	}
}

?>