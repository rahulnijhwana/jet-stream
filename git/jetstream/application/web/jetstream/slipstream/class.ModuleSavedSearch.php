<?php
require_once BASE_PATH . '/slipstream/class.Dashboard.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';

class ModuleSavedSearch extends JetstreamModule
{
	public $title = 'Saved Searches';
	protected $template_files = array('module_saved_search.tpl');
	public $sidebar = true;
	public $type;
	public $msg = 'None found';
	public $saved_searches = array();
	
	function __construct($search_type) {
		$this->type = (int)$search_type;
	}

	public function Init() {
	}
	
	public function DrawSavedSearches() {		
		$obj_dashboard = new Dashboard();
		$this->saved_searches = $obj_dashboard->GetSavedSearches($this->type);
		$output = '';
		
		if(count($this->saved_searches > 0))	{
			foreach($this->saved_searches as $key=>$val) {
				$output .= '<a href="#" onclick="LoadSavedSearch(\''.$val['SavedSearchLink'].'\')" >'.$val['SavedSearchName'].'</a><br/>';
			}
		}	
		return $output;
	}
}