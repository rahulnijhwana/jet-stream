<?php
/**
 * This file contains the note class.
 * @package database
 */

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.RecAccount.php';
require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';
require_once BASE_PATH . '/include/class.MapLookup.php';
require_once BASE_PATH . '/slipstream/class.SavedSearchInfo.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/include/class.SessionManager.php';


class Dashboard extends JetstreamModule
{
	public $company_id;
	public $user_id;
	public $search;
	public $query_type;
	public $show_no_of_pages = 10;
	public $total_records = 0;
	public $tot_pages;
	public $is_manager;
	public $last_search;
	public $search_results;
	public $first = false;
	
	function __construct($company_id = -1, $user_id = -1) {
		$this->company_id = ($company_id == -1 && isset($_SESSION['company_obj']['CompanyID'])) ? $_SESSION['company_obj']['CompanyID'] : $company_id;
		$this->user_id = ($user_id == -1 && isset($_SESSION['USER']['USERID'])) ? $_SESSION['USER']['USERID'] : $user_id;
	}

	function setLastSearch(){
		$this->last_search = (isset($_SESSION['LastSearch'])) ? true : false;
	}
	
	function ConvertLayoutToSql($layout, $table) {
		$pattern = "|\{(\w*?)\}|"; // Find words encased in curly braces
		return preg_replace($pattern, "' + $table.\\1 + '", "'$layout'");
	}
	
	public function Build($search) {
		$this->search = $search;
		$this->search_results = $this->SearchResults();
	}

	public function Build2($search) {
		$this->search = $search;
		$this->search_results = $this->SearchResults();
		return $this->search_results; 
	}
	
	public function SearchResults() {
		/*
		 * This function builds the query that returns a list of companies and/or contacts.
		 * You have a basic or advanced search option
		 */
		
		// The database uses generic property names.  
		// These functions return the property name for the desired field as defined in the mapping tables.
		$lbl_con_fname  = MapLookup::GetContactFirstNameField($this->company_id, TRUE); // ie:Text01
		$lbl_con_lname  = MapLookup::GetContactLastNameField($this->company_id, TRUE); // ie: Text02
		$lbl_con_city   = MapLookup::GetContactCityField($this->company_id, TRUE); // ie:Text01
		$lbl_con_state  = MapLookup::GetContactStateField($this->company_id, TRUE); // ie:Text01
		
		$lbl_comp_name  = MapLookup::GetAccountNameField($this->company_id, TRUE); // ie: Text01
		$lbl_comp_city  = MapLookup::GetAccountCityField($this->company_id, TRUE); // ie:Text01
		$lbl_comp_state = MapLookup::GetAccountStateField($this->company_id, TRUE); // ie:Text01
				
		// Set up variable names from the mappings.
		$contact_show = $this->ConvertLayoutToSql("{" . $lbl_con_fname . "} {" . $lbl_con_lname . "}", 'Contact');
		$account_show = $this->ConvertLayoutToSql("{" . $lbl_comp_name . "}", 'Account');
		$account_sort = $this->ConvertLayoutToSql("{" . $lbl_comp_name . "}", 'Account');

		$this->setLastSearch(); // set public boolean based on session variable

		$this->search['sort_dir'] = (empty($this->search['sort_dir'] )) ? 'ASC' : $this->search['sort_dir'];

		if(isset($this->search['sort_type']) && ($this->search['sort_type'] != ''))	{
			if(strtolower($this->search['sort_type']) == 'company') {
				$con_account_sort = $this->ConvertLayoutToSql("{" . $lbl_comp_name . "}", 'Account');
				$contact_sort = '';
			}
			else if(strtolower($this->search['sort_type']) == 'contactfname') {
				$contact_sort = $this->ConvertLayoutToSql("{" . $lbl_con_fname . "} {" . $lbl_con_lname . "}", 'Contact');
				$con_account_sort = '';				
			}
			else if(strtolower($this->search['sort_type']) == 'contactlname') {
				$contact_sort = $this->ConvertLayoutToSql("{" . $lbl_con_lname . "} {" . $lbl_con_fname . "}", 'Contact');
				$con_account_sort = '';
			} else {
				$contact_sort = $this->ConvertLayoutToSql("{" . $lbl_con_lname . "} {" . $lbl_con_fname . "}", 'Contact');
				$con_account_sort = $this->ConvertLayoutToSql("{" . $lbl_comp_name . "}", 'Account');
			}
		} else {	
			$contact_sort = $this->ConvertLayoutToSql("{" . $lbl_con_lname . "} {" . $lbl_con_fname . "}", 'Contact');
			$con_account_sort = $this->ConvertLayoutToSql("{" . $lbl_comp_name . "}", 'Account');
		}
		
		if(isset($this->search['sort_type']) && ($this->search['sort_type'] == '') && $this->search['Advance'] == 1  && $_GET['action'] == 'superadvancedashboard') {
			
			if(strtolower($this->search['sort_type']) != 'contact')	{
				$contact_sort = $this->ConvertLayoutToSql("{" . $lbl_comp_name . "}", 'Account');
			}			
			else {
				$account_sort = $this->ConvertLayoutToSql("{" . $lbl_con_lname . "} {" . $lbl_con_fname . "}", 'Contact');
			}
		}
				
		/* 
 		 * Over thinking the search with this criteria business.  Just use the search string with a LIKE.
 		 * Vern Gorman - Blue Star Technology (7/3/2013)
 		 
		$search_criteria = array();
		
		if($this->search['search_string'] != '') {
			$arr_search_string = $this->createMultiwordForSearch($this->search['search_string']);
			foreach($arr_search_string as $s_string) {
				$search_criteria[] = $s_string;
			}
		}

		if (count($search_criteria) > 0) {			
			
			//
			// SQL Server fulltext search doesn't work well with special characters
			// We can't match on & remove it first. If we only have an "&" we can
			// still use fulltext search this way.
			// 
			$search_criteria = $this->cleanArray($search_criteria);			
			
			//
			// If we have a hyphen in the criteria, we need to use LIKE instead of
			// CONTAINS.
			//
			if($this->containsHyphen($search_criteria)){
				
				// Use standard non-fulltext search
				$fulltext = false;
				
				$terms = array();
				foreach($search_criteria as $criteria){
					$terms[] = $criteria . '%';
				}
				$search_terms = implode(' ', $terms);
			}
			else {
				// Use fulltext search 
				$fulltext = true;
				
				// Format the terms as the CONTAINS function requires
				array_walk($search_criteria, create_function('&$term', '$term = "\"$term*\"";'));
				$search_terms = implode(' AND ', $search_criteria);

			}
		}
		else {
			$search_terms = FALSE;
		}

		*/
		
		// Just use the search_string as entered in a LIKE condition.
		$search_terms = trim($this->search['search_string']);

		$this->is_manager = ReportingTreeLookup::IsManager($this->company_id, $this->user_id);
		
		// Create the where clause from  the advanced filter options
		$advance_filter_arr = ( $this->search['Advance'] == 1  ) ? $this->GetAdvancedSearchOptions() : array();

		// building query for Account Table 
		if (($this->search['query_type'] == ACCOUNT || $this->search['query_type'] == BOTH) ) { 
			
			$param = array();
			$where_clause = 'Account.CompanyID = ? AND (Account.Deleted != 1) ';
			$param[] = array(DTYPE_INT, $this->company_id);
			
			if ($this->search['inactive_type'] == 0) {
				$where_clause .= ' AND (Account.Inactive != 1) ';
			}
			
			// Create an addition to the where clause for the assign users 
			if($this->search['assign']) { 

				if ($advance_filter_arr['assign'] == 'Unassign'){
					$advance_filter_arr['acc_where_clause']  .= " AND ( (SELECT COUNT(*) from PeopleAccount WHERE AccountId = Account.AccountID  ) = 0 ) ";
				}
				else if(trim($advance_filter_arr['acc_where_clause']) != ''){
					$advance_filter_arr['acc_where_clause']  .= " AND ( ? in (SELECT PersonID from PeopleAccount WHERE AccountId = Account.AccountID AND AssignedTo = 1 )) ";
				}
				else {
					$advance_filter_arr['acc_where_clause'] = " AND ( ? in (SELECT PersonID from PeopleAccount WHERE AccountId = Account.AccountID AND AssignedTo = 1 )) ";
				}
				if ($advance_filter_arr['assign'] != 'Unassign' ){
					$advance_filter_arr['acc_match_condition'][] = array(DTYPE_INT, (int)$this->search['assign'] );
				}
			}
			
			// Create the where clause from  the advanced filter options
			if(isset($advance_filter_arr['acc_where_clause']) && trim($advance_filter_arr['acc_where_clause']) != '') {
				$where_clause .= ' '.$advance_filter_arr['acc_where_clause'];
				
				if(is_array($advance_filter_arr['acc_match_condition'])) {
					$param = array_merge($param, $advance_filter_arr['acc_match_condition']);				
				}
				else {
					$advance_filter_arr['acc_match_condition'] = array($advance_filter_arr['acc_match_condition']);
					$param = array_merge($param, $advance_filter_arr['acc_match_condition']);
				}
			}
			
/*			if ($search_terms) {
				if($fulltext){
					$where_clause .= " AND AccountID in (
						SELECT RecID 
						FROM DashboardSearch 
						WHERE RecType = 1 
							AND CompanyID = ?
							AND CONTAINS(SearchText, ?))";
					$param[] = array(DTYPE_INT, $this->company_id);
					$param[] = array(DTYPE_STRING, $search_terms);
				}
			else {
*/					
					$where_clause .= " AND AccountID in (
				SELECT RecID 
				FROM DashboardSearch 
				WHERE RecType = 1
					AND CompanyID = ?"; 
					$param[] = array(DTYPE_INT, $this->company_id);
							
					if ($search_terms) {
//						$where_clause .= " AND (Account.$lbl_comp_name LIKE ? OR Account.$lbl_comp_name LIKE ?)";
						$where_clause .= " AND (SearchText LIKE ? OR SearchText LIKE ?)";
						$param[] = array(DTYPE_STRING, $search_terms."%");
						$param[] = array(DTYPE_STRING, "% ".$search_terms."%");
					}
					$where_clause .= ")";

//				}
//			}

			if($this->search['Advance'] != 1){
				if ($this->search['assigned_search'] == 1){
							
					$where_clause .= "
					
				AND ? in (
					SELECT PersonID 
					FROM PeopleAccount 
					WHERE AccountID = Account.AccountID AND AssignedTo = 1)"; 
					
					$param[] = array(DTYPE_INT, $_SESSION['USER']['USERID']);
				}
				else {
					// Make sure that private accounts only appear for the assigned users
					$where_clause .= " AND (PrivateAccount = 0 OR (PrivateAccount = 1
						
				AND ? in (
					SELECT PersonID 
					FROM PeopleAccount 
					WHERE AccountID = Account.AccountID AND AssignedTo = 1) )) ";
						
					$param[] = array(DTYPE_INT, $_SESSION['USER']['USERID']);
				}
			}

			/* PHP Notice:  Undefined index: advanced_filters , besso */
			if (isset($this->search['advanced_filters'])) {
				$created_date_range = $this->search['advanced_filters']['created']['Date'];
			} else {
				$created_date_range = array();
			}
			
			if(is_array($created_date_range)){
				if(count($created_date_range) == 1){
					$created_day = isset($created_date_range['start']) ? $created_date_range['start']: $created_date_range['end'];
					$where_clause .= " AND Account.CreatedDate = ?";
					$param[] = array(DTYPE_STRING, $created_day);
				}
				else if(count($created_date_range) == 2){
					$where_clause .= " AND Account.CreatedDate BETWEEN ? AND ?";
					$param[] = array(DTYPE_STRING, $created_date_range['start']);
					$param[] = array(DTYPE_STRING, $created_date_range['end']);
				}
			}
			
			if ($lbl_comp_city) {
				$qcitycomp = ", Account.$lbl_comp_city AS City";
			}
			else {
				$qcitycomp=", '' AS City";
			}
			if ($lbl_comp_state) {
				$qstatecompd = ", opt.OptionName AS State";
				$qstatecompj = "LEFT JOIN [Option] AS opt on Account.$lbl_comp_state = opt.OptionID";
			}
			else {
				$qstatecompd = ", '' AS State";
				$qstatecompj = "";
			}
			
			$query[ACCOUNT] = array('count_sql' => "SELECT COUNT(*) AS TotalRecords FROM
				(SELECT DISTINCT(Account.AccountID) FROM Account
				WHERE $where_clause ) accResult"
				, 'full_sql' => "
		SELECT 'account_' + CAST(Account.AccountID AS varchar) AS UniqueID
			, NULL as ContactID
			, Account.AccountID
			, NULL as ContactName
			, $account_show AS AccountName
			, $account_sort AS SortName
			, Account.PrivateAccount as Private
			, Account.Inactive
			, Account.$lbl_comp_name AS Comp$lbl_comp_name
			, NULL as Con$lbl_con_fname
			, NULL as Con$lbl_con_lname
			$qcitycomp
			$qstatecompd
					
		FROM Account
				$qstatecompj
		WHERE $where_clause", 'params' => $param);
		}
//					, $assigned_to_me
		
		// building query for Contact Table 
		if ($this->search['query_type'] == CONTACT || $this->search['query_type'] == BOTH){ 

			$where_clause = 'Contact.CompanyID = ? AND (Contact.Deleted != 1) ';
			$param = array(array(DTYPE_INT, $this->company_id));

			if(isset($advance_filter_arr['acc_where_clause']) && trim( $advance_filter_arr['acc_where_clause']) != '' ){ 
				if(isset($advance_filter_arr['acc_where_clause']) && trim( $advance_filter_arr['acc_where_clause']) != '' ){
					//$where_clause .= $advance_filter_arr['acc_where_clause'];
				}
				if (!empty($advance_filter_arr['acc_match_condition'])){
					//$param = array_merge($param, $advance_filter_arr['acc_match_condition']); 				
				}
				// hack for unassign only
				$where_clause = str_replace( 'AND( (SELECT COUNT(*) from PeopleAccount WHERE AccountId = Account.AccountID  ) = 0 )', ' ', $where_clause) ; 
			}
			
			if(isset($advance_filter_arr['assign']) && !empty($advance_filter_arr['assign'])) { 

				if($advance_filter_arr['assign'] == 'Unassign'){
					$advance_filter_arr['where_clause']  .= " AND( (SELECT COUNT(*) from PeopleContact WHERE ContactID = Contact.ContactID) = 0)";
				}
				
				else if(trim($advance_filter_arr['where_clause']) != ''){
					$advance_filter_arr['where_clause']  .= " AND( ? IN (SELECT PersonID FROM PeopleContact WHERE ContactID = Contact.ContactID AND AssignedTo = 1))";
				}
				else {
					$advance_filter_arr['where_clause'] = " AND ( ? IN (SELECT PersonID FROM PeopleContact WHERE ContactID = Contact.ContactID AND AssignedTo = 1))";
				}
				
				if ($advance_filter_arr['assign'] != 'Unassign'){
					$advance_filter_arr['match_condition'][] = array(DTYPE_INT, (int)$this->search['assign'] );
				}
			}
			if ($this->search['inactive_type'] == 0) {
				$where_clause .= ' AND (Contact.Inactive != 1) ';
			}
				
			
			// Create an addition to the where clause for the assign users 
			// Create the where clause from  the advanced filter options
			if(isset($advance_filter_arr['where_clause']) && trim($advance_filter_arr['where_clause']) != ''){
				$where_clause .= ( $this->search['query_type'] == BOTH && !empty($advance_filter_arr['acc_where_clause']) )  ? ' '.$advance_filter_arr['where_clause']: $advance_filter_arr['where_clause'];

				if(is_array($advance_filter_arr['match_condition'])) {
					$param = array_merge($param, $advance_filter_arr['match_condition']);				
				}
				else {
					$advance_filter_arr['match_condition'] = array($advance_filter_arr['match_condition']);
					$param = (is_array($param)) ? array_merge($param, $advance_filter_arr['match_condition']): $advance_filter_arr['match_condition'];
				}
			}
/*			if ($search_terms) {
				if($fulltext){
					$where_clause .= " AND ContactID in (
						SELECT RecID 
						FROM DashboardSearch 
						WHERE RecType = 2 
							AND CompanyID = ?
							AND CONTAINS(SearchText, ?))";
					$param[] = array(DTYPE_INT, $this->company_id);
					$param[] = array(DTYPE_STRING, $search_terms);
				}
			else {
*/					
					$where_clause .= " AND ContactID in (
				SELECT RecID 
				FROM DashboardSearch 
				WHERE RecType = 2
					AND CompanyID = ?"; 
					$param[] = array(DTYPE_INT, $this->company_id);
					if ($search_terms) {
//						$where_clause .= " AND (Contact.$lbl_con_fname LIKE ? OR Contact.$lbl_con_lname LIKE ?)";
						$where_clause .= " AND (SearchText LIKE ? OR SearchText LIKE ?)";
						$param[] = array(DTYPE_STRING, $search_terms."%");
						$param[] = array(DTYPE_STRING, "% ".$search_terms."%");
						
						//$param[] = array(DTYPE_STRING, $search_terms."%");				
						//$param[] = array(DTYPE_STRING, $search_terms."%");				
					}
					$where_clause .= ")";
//				}
//			}

			if($this->search['Advance'] != 1){
				if ($this->search['assigned_search'] == 1){
						$where_clause .= "
							
				AND ? in (
					SELECT PersonID 
					FROM PeopleContact 
					WHERE ContactID = Contact.ContactID AND AssignedTo = 1)"; 
						
						$param[] = array(DTYPE_INT, $this->user_id);
				}
				else {
					// Make sure that private accounts only appear for the assigned users
					$where_clause .= " AND (PrivateContact = 0 OR (PrivateContact = 1 
				AND ? in (
					SELECT PersonID 
					FROM PeopleContact 
					WHERE ContactID = Contact.ContactID AND AssignedTo = 1) )) ";
					$param[] = array(DTYPE_INT, $this->user_id);
				}
			}
			
			if((trim($con_account_sort) != '') && (trim($contact_sort) != '')) {
				$sort_str = $contact_sort . " + ' ' + " . $con_account_sort;
			}
			else if(trim($contact_sort) != '') {
				$sort_str = $contact_sort;
			}
			else if(trim($con_account_sort) != '') {
				$sort_str = $con_account_sort;
			}
			else {
				$sort_str = $contact_sort . " + ' ' + " . $con_account_sort;
			}
			
			if(is_array($created_date_range)){
				if(count($created_date_range) == 1){
					$created_day = isset($created_date_range['start']) ? $created_date_range['start']: $created_date_range['end'];
					$where_clause .= " AND Contact.CreatedDate = ?";
					$param[] = array(DTYPE_STRING, $created_day);
				}
				else if(count($created_date_range) == 2){
					$where_clause .= " AND Contact.CreatedDate BETWEEN ? AND ?";
					$param[] = array(DTYPE_STRING, $created_date_range['start']);
					$param[] = array(DTYPE_STRING, $created_date_range['end']);
				}
			}
			
			if ($lbl_con_city) {
				$qcitycon = ", Contact.$lbl_con_city AS City";
			}
			else {
				$qcitycon=", '' AS City";
			}
			if ($lbl_con_state) {
				$qstatecond = ", opt.OptionName AS State";
				$qstateconj = "LEFT JOIN [Option] AS opt on Contact.$lbl_con_state = opt.OptionID";
			}
			else {
				$qstatecond = ", '' AS State";
				$qstateconj = "";
			}
			
			$query[CONTACT] = array('count_sql' => "SELECT count(*) AS TotalRecords FROM
					(SELECT ContactID FROM Contact
					LEFT JOIN Account ON Contact.AccountID = Account.AccountID
					WHERE $where_clause ) conResult"
				, 'full_sql' => "
		SELECT 'contact_' + CAST(Contact.ContactID AS varchar) AS UniqueID
			, Contact.ContactID
			, Account.AccountID
			, $contact_show as ContactName
			, $account_show as AccountName
			, $sort_str AS SortName
			, Contact.PrivateContact as Private
			, Contact.Inactive
			, Account.$lbl_comp_name AS Comp$lbl_comp_name
			, Contact.$lbl_con_fname AS Con$lbl_con_fname
			, Contact.$lbl_con_lname AS Con$lbl_con_lname
			$qcitycon
			$qstatecond
					
			FROM Contact
				LEFT JOIN Account on Contact.AccountID = Account.AccountID
				$qstateconj
			WHERE $where_clause"
			, 'params' => $param
			);

		}
//					, NULL as AssignedToMe
		
		if(($this->search['query_type'] == BOTH)){
			if (!empty($advance_filter_arr['acc_match_condition']) && empty($advance_filter_arr['match_condition'])){
				$query[BOTH] = array('count_sql' => "SELECT ({$query[ACCOUNT]['count_sql']}) + ({$query[CONTACT]['count_sql']}) AS TotalRecords", 'full_sql' => "{$query[ACCOUNT]['full_sql']} UNION {$query[CONTACT]['full_sql']}", 'params' => array_merge($query[ACCOUNT]['params'], $query[CONTACT]['params']));
			}
			else if (empty($advance_filter_arr['acc_match_condition']) && !empty($advance_filter_arr['match_condition'])){
				$query[BOTH] = array('count_sql' => "SELECT ({$query[CONTACT]['count_sql']}) AS TotalRecords", 'full_sql' => "{$query[CONTACT]['full_sql']}", 'params' => array_merge(array(),  $query[CONTACT]['params']));
			}
			else {
				$query[BOTH] = array('count_sql' => "SELECT ({$query[ACCOUNT]['count_sql']}) + ({$query[CONTACT]['count_sql']}) AS TotalRecords", 'full_sql' => "{$query[ACCOUNT]['full_sql']} UNION {$query[CONTACT]['full_sql']}", 'params' => array_merge($query[ACCOUNT]['params'], $query[CONTACT]['params']));
			}
		}
		else if ( $this->search['query_type'] == CONTACT ){
			$query[BOTH] = array('count_sql' => "SELECT ({$query[CONTACT]['count_sql']}) AS TotalRecords", 'full_sql' => "{$query[CONTACT]['full_sql']}", 'params' => array_merge(array(),  $query[CONTACT]['params']));
		}
		else {
			$query[BOTH] = array('count_sql' => "SELECT ({$query[ACCOUNT]['count_sql']}) AS TotalRecords", 'full_sql' => "{$query[ACCOUNT]['full_sql']}", 'params' => array_merge(array(),  $query[ACCOUNT]['params']));
		}
	
		if (!isset($this->search['total_pages'])) {
			
			$count_sql = SqlBuilder()->LoadSql($query[$this->search['query_type']]['count_sql'])->BuildSqlParam($query[$this->search['query_type']]['params']);
			
			$count_result = DbConnManager::GetDb('mpower')->GetOne($count_sql);
			
			$this->total_records = $count_result['TotalRecords'];
			$total_pages = ceil($count_result['TotalRecords'] / $this->search['records_per_page']);
			$this->tot_pages = $total_pages;
			
			if (empty($advance_filter_arr['acc_match_condition']) && !empty($advance_filter_arr['match_condition']) && $this->search['query_type'] == ACCOUNT) {
				$this->total_records = 0;
				$total_pages = 0;
				$this->tot_pages = 0;
				return ; 
			}
		}
				
		$end_record = $this->search['page_no'] * $this->search['records_per_page'];
		$start_record = $end_record - $this->search['records_per_page'] + 1;
		
		$search_sql = "
SELECT *
FROM (
	SELECT ROW_NUMBER() OVER (ORDER BY SortName {$this->search['sort_dir']}) AS RowNumber, SearchQuery.*
	FROM ({$query[$this->search['query_type']]['full_sql']}) AS SearchQuery
	) _myResults
WHERE 
	RowNumber BETWEEN ? AND ?";
		
		$search_params = array_merge($query[$this->search['query_type']]['params'],
									array(
										array(DTYPE_INT, $start_record),
										array(DTYPE_INT, $end_record)
									));

		$search_sql = SqlBuilder()->LoadSql($search_sql)->BuildSqlParam($search_params);
	
		// @@@@@@@ Debug display line here @@@@@@@@
//echo '<pre>'; print_r($search_sql); echo '</pre>'; exit;

		// Clean out the old list
		$_SESSION['search_results'] = '';
		
		$search_result = DbConnManager::GetDb('mpower')->Exec($search_sql);
							
		// Save the following for previous/next navigation (VJG - 10/1/13)
		$_SESSION['search_results'] = $search_result;
		$_SESSION['search_total']   = $count_result['TotalRecords'];
		$_SESSION['search_curpage'] = $this->search['page_no'];
		$_SESSION['search_maxpage'] = ceil($count_result['TotalRecords'] / $this->search['records_per_page']);
//print_r($search_result);exit;
		return $search_result;
	}

	public function SearchResultsInfo() {
		$str_Info = '';
		
		if ($this->total_records > 0) {
			$end_record = $this->search['page_no'] * $this->search['records_per_page'];
			$start_record = $end_record - $this->search['records_per_page'] + 1;
			
			if ($end_record > $this->total_records) {
				$end_record = $this->total_records;
			}
			$str_Info = 'Records: ' . $start_record . ' - ' . $end_record . ' of ' . $this->total_records . '&nbsp;&nbsp;';
		}
		return $str_Info;
	}
	
	/**
	 * Retrieve the records for a perticular page.
	 */
	public function returnTotPageRec() {
		/*if(!isset($this->search['page_no'])) {
			$this->search['page_no'] = 1;
		}*/
		$str_Info = '';
		
		if ($this->total_records > 0) {
			$start_record = $end_record - $this->search['records_per_page'] + 1;
			
			if ($end_record > $this->total_records) {
				$end_record = $this->total_records;
			}
			$str_Info = 'Records: ' . $start_record . ' - ' . $end_record . ' of ' . $this->total_records . '&nbsp;&nbsp;';
		}
		return $str_Info;
	}
	
	/**
	 * Parses the input field used in filter box.
	 * Here we can define The rules for filter values.
	 * Creates an array using each word entered in the input field.
	 */
	function ParseFilterSearchValue($value) {
		$parsedArr = array();
		$value = trim($value);
		if($value != '') {
			$parsedArr[] = '%'.$value.'%';
		}
		return $parsedArr;
	}
	
	
	public function GetAdvancedSearchOptions()	{

			$conditionalArray = array();

			/**
			 * Creates the conditional clause for Filteration.
			 */			
			$innerAccConWhereClause = '';
			$innerAccountWhereClause = '';
			$loadInnerSqlClause = array();
			$loadAccInnerSqlClause = array();
			$conditionalArray['Contact'] = false;
			
			if(count($this->search['advanced_filters']) > 0) {

			// check if contact is set $innerAccountWhereClause  $loadAccInnerSqlClause
				if ( isset($this->search['advanced_filters']['contact']) ){

					$fieldPrefix = 'Contact';
					$builtContact = $this->CreateWhereClause($this->search['advanced_filters']['contact'], $fieldPrefix); 
					/* 
					 * Search for active or inactive user .
					if ($newFilterArr[1] == 'Id'){						 
						 $conditionalArray['assign'] = true ; 
					} 
						*/			
				}
			// check if account is set $innerAccConWhereClause $loadInnerSqlClause
				if (!empty($this->search['advanced_filters']['company'])){			
					$fieldPrefix = 'Account';
					$builtAccount = $this->CreateWhereClause($this->search['advanced_filters']['company'], $fieldPrefix); 
				}
			} // End Of if loop
			$conditionalArray['acc_where_clause'] = (!empty ($builtAccount['innerWhereClause'])) ? $builtAccount['innerWhereClause'] : '';
			$conditionalArray['acc_match_condition'] = (!empty($builtAccount['loadInnerSqlClause'])) ?$builtAccount['loadInnerSqlClause'] : '';
			
			$conditionalArray['where_clause'] = (!empty( $builtContact['innerWhereClause']) ) ? $builtContact['innerWhereClause']: '';
			$conditionalArray['match_condition'] = (!empty( $builtContact['loadInnerSqlClause']) ) ? $builtContact['loadInnerSqlClause'] : '';
			$conditionalArray['assign'] = (!empty( $this->search['assign']) ) ? $this->search['assign'] : '';
			
			return $conditionalArray;
		}

	/**
	 * Divides the entire word to multiple word.
	 */
	// Why did they do this?  The owners were expecting a simple phrase search. (7/2/13)
	public function createMultiwordForSearch($search_string) {
		$search_string = trim($search_string);
		$arr_search_string = array();
		$explode_dash_string = explode(' ', $search_string);
		
		if (count($explode_dash_string) > 0) {
			foreach ($explode_dash_string as $single_string) {
				$explode_comma_string = explode(',', $single_string);
				if (count($explode_comma_string) > 0) {
					foreach ($explode_comma_string as $single_comma_string) {
						$arr_search_string[] = $single_comma_string;
					}
				}
			}
		} else {
			$explode_comma_string = explode(',', $search_string);
			
			if (count($explode_comma_string) > 0) {
				foreach ($explode_comma_string as $single_comma_string) {
					$arr_search_string[] = $single_comma_string;
				}
			}
		}
		return $arr_search_string;
	}

	/**
	 * Creates the pages.
	 */
	public function getDashboardPages() {
		$pagination_link = '';
		if ($this->tot_pages > 0) {
			$left_page_no = 1;
			$right_pag_no = 0;
			$div_pages = floor($this->show_no_of_pages / 2);
			
			if ($this->tot_pages == 1) {
				return '';
			}
			
			$left_page_no = $this->search['page_no'] - $div_pages;
			if ($left_page_no < 0) {
				$right_pag_no = -($left_page_no);
				$left_page_no = 1;
			} else if ($left_page_no == 0) {
				$left_page_no = 1;
			}
			
			if (($left_page_no + $div_pages) == $this->search['page_no']) {
				$right_pag_no += $this->search['page_no'] + ($div_pages - 1);
			} else {
				$right_pag_no += $this->search['page_no'] + $div_pages;
			}
			
			if ($right_pag_no > $this->tot_pages) {
				$left_page_no = $left_page_no - ($right_pag_no - $this->tot_pages);
				$right_pag_no = $this->tot_pages;
			}
			if ($left_page_no <= 0) {
				$left_page_no = 1;
			}
			
			if ($this->search['page_no'] == 1) {
				$pagination_link .= '<img class="pageImage" border="0" alt="First" src="./images/off-first.gif" disabled />';
				$pagination_link .= '<img class="pageImage" border="0" alt="Prev" src="./images/off-prev.gif" disabled />';
			} else if ($this->search['page_no'] > 1) {
				$prev_page = $this->search['page_no'] - 1;
				
				$btn_first_event = "AjaxLoadPage(1);";
				$btn_prev_event = "AjaxLoadPage('$prev_page');";
				
				$pagination_link .= '<a href="#"><img class="pageImage" border="0" src="./images/first.gif" alt="First" title="First" onclick="javascript:' . $btn_first_event . '" /></a>';
				$pagination_link .= '<a href="#"><img class="pageImage" border="0" src="./images/prev.gif" alt="Prev" title="Previous" onclick="javascript:' . $btn_prev_event . '" /></a>';
			
			}
			
			if ($this->tot_pages >= $this->search['page_no']) {
				for($i = $left_page_no; $i <= $right_pag_no; $i++) {
					$btn_event = "AjaxLoadPage('$i');";
					
					if ($i == $this->search['page_no']) {
						$pagination_link .= '<span class="page">' . $i . '</span>';
					} else {
						$pagination_link .= '<a href="#" onclick="javascript:' . $btn_event . '"><span class="page">' . $i . '</span></a>';
					}
				}
			}
			
			if ((!$this->search['page_no']) || $this->search['page_no'] < $this->tot_pages) {
				$next_page = $this->search['page_no'] + 1;
				
				$btn_next_event = "AjaxLoadPage('$next_page');";
				$btn_last_event = "AjaxLoadPage('$this->tot_pages');";
				
				$pagination_link .= '<a href="#"><img class="pageImage" border="0" src="./images/next.gif" alt="Next" title="Next" onclick="javascript:' . $btn_next_event . '" /></a>';
				$pagination_link .= '<a href="#"><img class="pageImage" border="0" src="./images/last.gif" alt="Last" title="Last" onclick="javascript:' . $btn_last_event . '" /></a>';
			} else {
				$pagination_link .= '<img class="pageImage" alt="Next" border="0" src="./images/off-next.gif" disabled />';
				$pagination_link .= '<img class="pageImage" alt="Last" border="0" src="./images/off-last.gif" disabled />';
			}
		}
		return $pagination_link;
	}
	
	
	public function SaveSearch(){
		$sql = 'SELECT * FROM SavedSearch WHERE 0 = -1';
		$result = DbConnManager::GetDb('mpower')->Execute($sql, 'SavedSearch');

		$search_info = new SavedSearchInfo();
		$search_info->SetDatabase(DbConnManager::GetDb('mpower'));
		$result[] = $search_info;				
		$result->Initialize();

		$search_info->SavedSearchName = $this->saved_search_name;
		$search_info->SavedSearchText = $this->saved_search_text;
		$search_info->SavedSearchType = $this->saved_search_type;
		$search_info->SavedSearchTime = date('Y-m-d h:i:s');
		$search_info->SavedSearchUserID = $this->user_id;
		$search_info->SavedSearchTimeZone = $this->browser_time;
		
		$result->Save();
		
		$saved_search_id = $search_info->SavedSearchID;
		
		if($saved_search_id > 0)
		{
			return true;
		}
		return false;
	}
	
	public function GetSavedSearches($type){
		$save_search_sql = "SELECT SavedSearchName, SavedSearchText AS SavedSearchLink	
			FROM SavedSearch WHERE SavedSearchType = ? AND SavedSearchUserID = ? ORDER BY SavedSearchTime DESC";		
		$save_search_sql = SqlBuilder()->LoadSql($save_search_sql)->BuildSql(array(DTYPE_INT, $type), array(DTYPE_INT, $this->user_id));		
		$save_search_arr = DbConnManager::GetDb('mpower')->Exec($save_search_sql);

		return $save_search_arr;
	}

	public function CreateWhereClause($search, $fieldPrefix){
	
		$innerWhereClause = "";
		$loadInnerSqlClause = array();

		foreach( $search as $filterKey => $filterVal ){
			
			$recType = strtolower(substr($filterKey, 0, 4));
			
			switch($recType) {
				case 'numb' :
					if (!$this->first) {
						$filterKey = substr($filterKey,0,-6);
						$innerWhereClause .= " AND (  $fieldPrefix.".$filterKey." BETWEEN ?";
						$loadInnerSqlClause[] = array(DTYPE_INT, (int)(trim($filterVal)));
						$this->first = true;
					}
					else {
						$innerWhereClause .= " AND ? )";
						$loadInnerSqlClause[] = array(DTYPE_INT, (int)(trim($filterVal)));
						$this->first = false;
						 
					}
					break;

				case 'sele' :
					$innerWhereClause .= " AND ( "; 
					if ( in_array('EMPTY', $filterVal)) {
						unset($filterVal[array_search('EMPTY', $filterVal)]);
						$innerWhereClause .= "$fieldPrefix.".$filterKey." is NULL";
						if (count($filterVal) == 0 ){
							$innerWhereClause .= " ) ";
							break;
						}
						else $innerWhereClause .= " OR ";
					}
					$innerWhereClause .= "($fieldPrefix.".$filterKey." in (". ArrayQm($filterVal) ."))";
					foreach( $filterVal as $filterValue ) {
						$loadInnerSqlClause[] = array(DTYPE_INT, (int)(trim($filterValue)));
					}
					$innerWhereClause .= " ) ";
					break ; 

				case 'bool' :
					if (trim($filterVal) == '' || trim($filterVal) == 0){
						$innerWhereClause .= " AND (  $fieldPrefix.".$filterKey." = 0 OR $fieldPrefix. ". $filterKey ." is NULL  ) ";
					} 
					else {
						$innerWhereClause .= " AND (  $fieldPrefix.".$filterKey." = ? ) ";
						$loadInnerSqlClause[] = array(DTYPE_INT, (int)(trim($filterVal)));
					}					
					break ; 

				case 'date' :
					/*
					 * If the entered date is not in correct format then it takes the current date.
					 */
					 $keyList = array( 'start:month' => '' , 'start:day' => '' , 'end:month' => '' , 'end:day' => '' );

					// date range month and year only (Test all cases of possible years)
					if ( count($filterVal) == 4 && count( array_intersect_key($filterVal, $keyList)) == 4  ) {
						
						$years = array(
							1900,
							2008,
							2009,
							2010,
							2011,
							2012,
							2013,
							2014,
							2015,
							2016,
							2017
						);
						foreach ($years as $year) {
							$start = $filterVal['start:month'] . '/' . $filterVal['start:day'] .'/' . $year; 
							$end = $filterVal['end:month'] . '/' . $filterVal['end:day'] .'/' . $year;
							
							$filterStr['StartDate'] = ($start) ? date('m/d/Y', strtotime($start)) : date('m/d/Y', mktime(0, 0, 0, 1, 1, 1900)) ;
							$filterStr['EndDate'] = ($end) ? date('m/d/Y', strtotime($end)): date('m/d/Y') ;

							$loadInnerSqlClause[] = array(DTYPE_TIME, $filterStr['StartDate']);
							$loadInnerSqlClause[] = array(DTYPE_TIME, $filterStr['EndDate']);
							
							$innerInnerWhereClause .= " ($fieldPrefix.$filterKey BETWEEN ? AND ?) OR ";					
							
						}
						$innerInnerWhereClause = substr($innerInnerWhereClause, 0, -3);
						$innerWhereClause .= " AND ( $innerInnerWhereClause )";			
						
					} 
					else if ( count($filterVal) == 2 && count( array_intersect_key($filterVal, $keyList)) == 0  ) {
						$filterStr['StartDate'] = ($filterVal['start']) ? date('m/d/Y', strtotime($filterVal['start'])) : date('m/d/Y', mktime(0, 0, 0, 1, 1, 1900)) ;
						$filterStr['EndDate'] = ($filterVal['end']) ? date('m/d/Y', strtotime($filterVal['end'])): date('m/d/Y') ;
						$loadInnerSqlClause[] = array(DTYPE_TIME, $filterStr['StartDate']);
						$loadInnerSqlClause[] = array(DTYPE_TIME, $filterStr['EndDate']);
						$innerWhereClause .= " AND ( $fieldPrefix.$filterKey BETWEEN ? AND ? )";					
					}
					else {
						foreach( $filterVal as $key => $val ){
							$keyType = explode(':',$key);
							$type = array() ; 
							$type[$keyType[0]][$keyType[1]] = $val ; 
						}
						if ( $type['start']['month'] > $type['end']['month'] ) $boolean = "XOR"; 
						else $boolean = "AND";
						
						$innerWhereClause .= " AND ( (month($fieldPrefix.$filterKey) * 100 + day($fieldPrefix.$filterKey)) > ( ? * 100 + ?) AND (month($fieldPrefix.$filterKey) * 100 + day($fieldPrefix.$filterKey)) < ( ? * 100 + ?))" ; 
						$loadInnerSqlClause[] = ( $type['start']['month'] ) ? array(DTYPE_INT, (int)$type['start']['month'] ): array(DTYPE_INT, 1 ) ; 
						$loadInnerSqlClause[] = ( $type['start']['day'] ) ? array(DTYPE_INT, (int)$type['start']['day'] ): array(DTYPE_INT, 1 ); 
						$loadInnerSqlClause[] = ( $type['end']['month'] ) ? array(DTYPE_INT, (int)$type['end']['month'] ) :  array(DTYPE_INT, 12 ) ; 
						$loadInnerSqlClause[] = ( $type['end']['day'] ) ? array(DTYPE_INT, (int)$type['end']['day'] ) :  array(DTYPE_INT, 31 ) ; 
					}
					break ; 

				default: // assumption Text + boolean ' OR ' 
					$boolean_separator = ' OR ';
					$innerWhereClause .= " AND ("; 
					if(stristr($filterVal, $boolean_separator ) === FALSE) { // not found	
						
						// The validation type in the mapping table will tell us if we have a phone number field.
						$valType_sql = "SELECT ValidationType FROM ".$fieldPrefix."Map WHERE CompanyID=? AND FieldName='".$filterKey."'";
						$valType_sql = SqlBuilder()->LoadSql($valType_sql)->BuildSql(array(DTYPE_INT, $this->company_id));		
						$valType_Result = DbConnManager::GetDb('mpower')->GetOne($valType_sql);
						
						if ($valType_Result['ValidationType'] == 1) { // phone numbers search by digits only
							$innerWhereClause .= "( REPLACE(REPLACE(REPLACE(REPLACE(".$fieldPrefix.".".$filterKey.", '(', ''), ')', ''), '-', ''), ' ','') LIKE ? ) ";
							$filterValNum = preg_replace('/[^0-9.]+/', '', $filterVal);
							$loadInnerSqlClause[] = array(DTYPE_STRING, (trim($filterValNum).'%'));
						} else {
							$innerWhereClause .= "( $fieldPrefix.".$filterKey." LIKE ? ) ";
							$loadInnerSqlClause[] = array(DTYPE_STRING, (trim($filterVal).'%'));
						}
					} else {	// boolean is activated 
						$boolean = explode($boolean_separator, $filterVal );
						foreach ( $boolean as $count => $booleanVal){
							$innerWhereClause .= ( $count < 1 ) ? '' : ' OR '; 
							$innerWhereClause .= "( $fieldPrefix.".$filterKey." LIKE ? )";
							$loadInnerSqlClause[] = array(DTYPE_STRING, ('%'.trim($booleanVal).'%'));
						}
					}
					$innerWhereClause .= ')'; 
					break;
				}
				
			}	
		return array('innerWhereClause' => $innerWhereClause , 'loadInnerSqlClause' => $loadInnerSqlClause );
	}

/*	function cleanArray($arr){
	
		$clean = array();
	
		foreach($arr as $a){
			if($a != '&'){
				$clean[] = $a;
			}
		}
	
		return $clean;
	}
*/
	
	function containsHyphen($arr){
		foreach($arr as $a){
			if(strstr($a, '-')){
				return true;
			}
		}
		return false;
	}

}
