<?php
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';

/**
 * Class for Notes search module
 */
class ModuleNotesSearch extends JetstreamModule
{
	public $title = 'Search Notes';
	protected $javascript_files = array('jquery.js', 'note.js', 'ui.datepicker.js', 'jquery.scrollTo.js');
	protected $css_files = array('note.css', 'ui.datepicker.css');
	protected $buttons = array();
	public $sidebar = false;
	
	public function Init() {		
		/**
		 * include tpl file for HTML section.
		 */		
		$this->AddTemplate('module_notes_search.tpl');
		$this->AddButton('print24.png', 'javascript:printNotes();', 'Print');
	}
}
