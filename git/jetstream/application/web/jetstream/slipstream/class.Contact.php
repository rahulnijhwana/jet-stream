<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/include/class.DashboardSearchCreatorNew.php';
require_once BASE_PATH . '/include/class.MapLookup.php';

require_once 'class.Status.php';

class Contact extends MpRecord
{

	public function __toString() {
		return 'Contact';
	}

	public function Initialize() {
		$this->db_table = 'Contact';
		$this->recordset->GetFieldDef('ContactID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('CompanyID')->required = TRUE;
		parent::Initialize();
	}

	public function Save($simulate = false) {
		parent::Save($simulate);
		if (!$simulate) {
			
			$sql = "DELETE FROM DashboardSearch WHERE RecType = 2 and RecID = $this->ContactID";
			DbConnManager::GetDb('mpower')->Exec($sql);
			
			
			if (empty($this->Deleted)) {
				$active = ($this->Inactive == 0) ? 1 : 0;
				$fn_field = MapLookup::GetContactFirstNameField($this->CompanyID, true);
				$ln_field = MapLookup::GetContactLastNameField($this->CompanyID, true);
				
				$search_text = $this->$fn_field . ' ' . $this->$ln_field;
				
				$sql = "INSERT INTO DashboardSearch (CompanyID, RecType, RecID, SearchText, IsActive) 
					VALUES ($this->CompanyID, 2, $this->ContactID, ?, $active)";
				$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $search_text));
				DbConnManager::GetDb('mpower')->Exec($sql);
			}
//			$obj_search = new DashboardSearchCreator();
//			$obj_search->company_id = $this->CompanyID;
//			$obj_search->record_id = $this->ContactID;
//			$obj_search->StoreContactRecords();
		}
	}

	function getFieldsFromContact($type) {
		$searchCriteria = $type . '%';
		$sql = "select COLUMN_NAME as FieldName from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='Contact' and COLUMN_NAME like ?";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_STRING, $searchCriteria));
		$fieldNames = DbConnManager::GetDb('mpower')->Exec($sql);
		return $fieldNames;
	}
	/*Start JET-37 GoogleMapping*/
	function getGoogleFields($companyId) {
		$sql = "SELECT googleField,googleFieldName FROM googleFields where googleFields.googleField not in (SELECT ContactMap.googleField from ContactMap where ContactMap.CompanyID = {$companyId} and ContactMap.googleField IS NOT NULL)";
		$fieldNames = DbConnManager::GetDb('mpower')->Exec($sql);
		return $fieldNames;
	}
	/*End  JET-37 GoogleMapping*/

	function IsInactiveContact($contactId) {
		if (isset($contactId) && ($contactId > 0)) {
			$sql = "select Inactive from Contact where ContactID = ? ";
			$sqlBuild = SqlBuilder()->LoadSql($sql);
			$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $contactId));
			$result = DbConnManager::GetDb('mpower')->Exec($sql);
			return (isset($result[0]) && isset($result[0]['Inactive']) && $result[0]['Inactive'] == 1) ? TRUE : FALSE;
		}
		return FALSE;
	}

	function activateContact($contactId) {
		$status = new Status();
		$sql = "SELECT * FROM Contact WHERE ContactID = ?";
		$contactSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $contactId));
		$result = DbConnManager::GetDb('mpower')->Execute($contactSql, 'Contact');
		if (count($result) == 1) {
			$objContact = $result[0];
			$objContact->Initialize();
			$objContact->Inactive = 0;
			$result->Save();
		}
		return $status;
	}

	function IsRestricted($contact_id, $user_id, $company_id) {
		
		/*$sql="SELECT CompanyID from Contact where ContactID=?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $contact_id));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		if(count($result)>0)
		{
			if($result[0]['CompanyID']!=$company_id)
				return true;
		}*/
		
		$sql = "SELECT Contact.PrivateContact, PeopleContact.PersonID FROM Contact 
				LEFT JOIN PeopleContact ON Contact.ContactID = PeopleContact.ContactID WHERE (Contact.CompanyID = ?) AND (Contact.ContactID = ?)";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_id), array(DTYPE_INT, $contact_id));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		//echo $contact_id;
		//print_r($result);
		/*if(is_array($result) && count($result) > 0)
		{
			if($result[0]['PrivateContact'] == 1 && $result[0]['PersonID'] != $user_id)
				return true;
			else
				return false;
		}*/
		
		return ((is_array($result) && count($result) > 0) && ($result[0]['PrivateContact'] == 1 && $result[0]['PersonID'] != $user_id)) ? true : false;
	
	}
}

?>
