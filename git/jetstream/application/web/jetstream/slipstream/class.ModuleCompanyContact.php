<?php
require_once BASE_PATH . '/slipstream/class.CompanyContact.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.Contact.php';
require_once BASE_PATH . '/slipstream/class.Account.php';

class ModuleCompanyContact extends JetstreamModule
{
	protected $javascript_files = array('jquery.js', 'contact.js', 'ui.datepicker.js', 'jquery.cookie.js', 'overlay.js', 'jquery.cluetip.js');
	protected $css_files = array('jet_datagrid.css', 'ui.datepicker.css', 'overlay.css','mini_calendar.css','event.css','jquery.cluetip.css');
	//protected $print_css_files = array('company_contact_print.css');
	protected $buttons = array();
	protected $template_files = array('module_company_contact.tpl', 'shell_form.tpl', 'form_manage_company_contact.tpl');
	protected $subnav = array();

	public $sidebar = false;
	private $record;
	public $is_restricted = false;
	public $is_inactive = false;

	public function DrawView() {
		return $this->record->DrawView();
	}

	public function DrawForm() {
		return $this->record->DrawView(true);
	}

	public function Init() {
		/**
			Creates the objects for contact.
			Creates the objects for account.
			Creates the objects for companycontact.
		*/
		$objContact = new Contact();
		$objAccount = new Account();
		$this->record = new CompanyContact();

		/**
			Get the action of the page, from the query string.
			Get the accountId or ContactId , from the query string.
		*/
		$action = $_GET['action'];

		if(isset($_GET['contactId'])) {
			$contact_id = $_GET['contactId'];
		}

		if(isset($_GET['accountId'])) {
			$account_id = $_GET['accountId'];
		}

		if ($action == 'contact') {
			if(isset($contact_id)) {
				$this->is_restricted = $objContact->IsRestricted($contact_id,$_SESSION['login_id'],$_SESSION['company_obj']['CompanyID']);
				$this->is_inactive = $objContact->IsInactiveContact($contact_id);
			}

			$record_id = (isset($contact_id)) ? $contact_id : 0;
			$this->title = 'Contact Information';
			$manage_sub_nav = MANAGE_CON;

			if(!$this->is_restricted)
			{
				if (isset($_GET['referer']) && ($_GET['referer'] == 'email_hold') && isset($_GET['email']) && isset($_GET['first_name'])) {
					$this->record->first_name = $_GET['first_name'];
					$this->record->last_name = $_GET['last_name'];
					$this->record->email = $_GET['email'];
				}
				//if(isset($contact_id) && !$objContact->IsRestricted($contact_id,$_SESSION['login_id'],$_SESSION['company_obj']['CompanyID'])) {
				// 01/30/2009 : Supriti ~ In the above line again IsRestricted is called, which is not requires, as we have this value already.
				if(isset($contact_id) && !$this->is_restricted) {
					if($this->record->checkRestriction($contact_id, $action)) {
						$this->subnav = array(LOG_INCOMING_CALL => 'javascript:addNewNoteForm('.NOTETYPE_CONTACT.', \'' . $contact_id . '\', ' . NOTETYPE_INCOMING_CALL . ');', LOG_OUTGOING_CALL => 'javascript:addNewNoteForm('.NOTETYPE_CONTACT.', \'' . $contact_id . '\', ' . NOTETYPE_OUTGOING_CALL . ');');
					}
				}
				else {
					$this->subnav = array();
				}
				setcookie('LastVisitedContact',$record_id);
			}

		} else if($action == 'company') {
			if(isset($account_id)) {
				$this->is_restricted = $objAccount->IsRestricted($account_id,$_SESSION['login_id'],$_SESSION['company_obj']['CompanyID']);
				$this->is_inactive = $objAccount->IsInactiveAccount($account_id);
			}

			$record_id = (isset($account_id)) ? $account_id : 0;
			$this->title = 'Company Information';
			$manage_sub_nav = MANAGE_ACC;
		} else {
			throw new Exception('Unknown action for record display.');
		}
		$private_field = (isset($field_type) && $field_type == 'contact') ? ('PrivateContact') : ('PrivateAccount');

		if(isset($record_id) && ($record_id > 0)) {
			$is_assigned_to_cur_user = $this->record->checkRestriction($record_id, $action);

			/* Checks the permission and accordingly show the Manage Account/Manage Contact. */
			if($this->record->ShowManageRec($record_id, $action) && (!$this->is_inactive) && !$this->is_restricted) {
				//$this->subnav = array_merge($this->subnav, array(MANAGE_CON => 'javascript:showManageForm(\'contact\', '.$record_id.');'));
				$this->subnav = array_merge($this->subnav, array($manage_sub_nav => 'javascript:showManageForm(\''.$action.'\', '.$record_id.');'));
			}

			$this->record->Load($action, $record_id);
			if ($this->record->IsPrivate()) $this->formatting = 'content_private';
			if ($this->record->IsInactive()) $this->formatting = 'content_inactive';
		}
		else {
			$this->record->Create($action);
		}

		if(!$this->is_restricted)
			$this->contact_title = $this->record->GetName();
		else
			$this->contact_title = '';
		$this->FormId = 'id' . uniqid();

		// Validate and show the Edit, Activate and Print links.
		if(isset($action) && isset($record_id) && ($record_id > 0)) {
			if(!$this->is_restricted) {
				if($is_assigned_to_cur_user) {
					if((isset($contact_id) || isset($account_id)) && !($this->is_inactive)) {
						$this->AddButton('edit24.png', "#top\" onclick=\"javascript:ShowForm('$this->FormId','edit');", 'Edit');
					}
					else {
						$this->AddButton('activeImg.gif', "javascript:ActivateForm('".$action."',$record_id);", 'Activate');
					}
				}
				$this->AddButton('print24.png', 'javascript:window.print();', 'Print');
			}
		}
	}
}
?>
