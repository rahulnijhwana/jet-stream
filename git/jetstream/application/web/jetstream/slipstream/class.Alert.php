<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';

/**
 * Add an Alert to an important record to display a message on screen
 * for all recipients
 * 
 * @author eric
 */
class Alert {
	
	private $alertTypeId; //for future implementation of criticality of alert
	private $alertStandard;
	private $alertEmail;
	private $recordTypeId;
	private $recordId;
	private $createdBy;
	private $createdDate;
	private $delayUntil;
	private $recipients;
	
	public function __construct($alertTypeId, $alertStandard, $alertEmail, $delayUntil,
								$recordTypeId, $recordId, $createdBy, $createdDate, $recipients){
		
		$this->alertTypeId = $alertTypeId;
		$this->alertStandard = $alertStandard;
		$this->alertEmail = $alertEmail;
		$this->delayUntil = $delayUntil;
		$this->recordTypeId = $recordTypeId;
		$this->recordId = $recordId;
		$this->createdBy = $createdBy;
		$this->createdDate = $createdDate;
		$this->recipients = $recipients;
		
	}
	
	public function save(){
		
		$sql = "
			INSERT INTO Alert (AlertTypeID, RecordID, RecordTypeID, CreatedBy, CreatedDate,
								AlertStandard, AlertEmail, DelayUntil)
			VALUES
				(?, ?, ?, ?, ?, ?, ?, ?)";
		
		$params = array(
			array(DTYPE_INT, $this->alertTypeId),
			array(DTYPE_INT, $this->recordId),
			array(DTYPE_INT, $this->recordTypeId),
			array(DTYPE_INT, $this->createdBy),
			array(DTYPE_STRING, $this->createdDate),
			array(DTYPE_BOOL, $this->alertStandard),
			array(DTYPE_BOOL, $this->alertEmail),
			array(DTYPE_STRING, $this->delayUntil)
		);
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		$alertId = DbConnManager::GetDb('mpower')->DoInsert($sql);
		
		foreach($this->recipients as $recipient){
			$recipientSql = "INSERT INTO AlertRecipient(AlertID, PersonID, Active) VALUES (?, ?, 1)";
			$recipientParams = array(
				array(DTYPE_INT, $alertId),
				array(DTYPE_INT, $recipient)
			);
			$recipientSql = SqlBuilder()->LoadSql($recipientSql)->BuildSqlParam($recipientParams);
			DbConnManager::GetDb('mpower')->Exec($recipientSql);
		}
				
	}
}

