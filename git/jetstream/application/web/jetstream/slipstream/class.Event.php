<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/lib.date.php';
require_once BASE_PATH . '/include/lib.dblookup.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.fieldsMap.php';
require_once BASE_PATH . '/include/class.ReportingTree.php';
require_once BASE_PATH . '/include/class.JetDateTime.php';
require_once BASE_PATH . '/include/class.DebugUtil.php';
require_once BASE_PATH . "/include/class.PriorityUtil.php";
require_once BASE_PATH . '/slipstream/lib.php';
require_once BASE_PATH . '/slipstream/class.Status.php';
require_once BASE_PATH . '/slipstream/class.Util.php';
require_once BASE_PATH . '/slipstream/class.EventInfo.php';
require_once BASE_PATH . '/slipstream/class.OppInfo.php';
require_once BASE_PATH . '/slipstream/class.Contact.php';
require_once BASE_PATH . '/slipstream/class.Opps.php';
require_once BASE_PATH . '/slipstream/class.EventTypeLookup.php';

//require_once BASE_PATH . '/slipstream/class.RecurringEvent.php';


ini_set('mssql.textlimit', 20971520);
ini_set('mssql.textsize', 20971520);

class Event {
	public $eventId = 0;
	public $contactId;
	public $contact;
	public $userId;
	public $eventSubject;
	public $eventTypeId;
	public $eventStartDate;
	public $eventStartTime;
	public $eventDur;
	public $eventAction;
	public $eventPrivate = 0;
	public $saveType;
	public $dealID = 0;
	public $colseEventVal = 0;
	public $cancelEventVal = 0;
	public $allowCreatePastEvent = FALSE;
	public $company_id;
	public $user_id;
	public $recurId = 0;
	public $user_tz;

	function __construct($company_id = -1, $user_id = -1, $user_tz = -1) {
		$this->company_id = ($company_id == -1) ? $_SESSION['company_obj']['CompanyID'] : $company_id;
		$this->user_id = ($user_id == -1) ? $_SESSION['USER']['USERID'] : $user_id;
		$this->user_tz = new DateTimeZone(($user_id == -1) ? $_SESSION['USER']['BROWSER_TIMEZONE'] : $user_tz);
	}
	/**
	 * Check if the user is the owner of this event.
	 * @arg int EventID
	 * @return bool
	 */
	function CheckEventOwner($id) {
		
		$sql = "SELECT UserID FROM Events WHERE EventID = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id));
		$result = DbConnManager::GetDb('mpower')->GetOne($sql);
		$eventOwnerId = $result->UserID;
		
		if($this->user_id != $eventOwnerId && $this->HasWriteCalendarPermission($eventOwnerId, $this->user_id)){
			return true;
		}
		
		$obj_opp = new Opps();
		$obj_opp->GetPerson($this->user_id);
		
		$person_list = array();
		if (count($obj_opp->salesperson_list) > 0) {
			$person_list = array_merge($person_list, $obj_opp->salesperson_list);
		}
		$person_list = implode(',', $person_list);
		
		$sql = "SELECT COUNT(*) AS count FROM Events WHERE EventID = ? AND UserID IN ($person_list)";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id));
		$result = DbConnManager::GetDb('mpower')->GetOne($sql);
		
		return ($result->count > 0) ? true : false;
	}

	/**
	 * Check if the event can be marked as Private. An Event can only be private
	 * if the Event doesn't have any public Notes
	 * @arg int EventID
	 * @return Array containing the status and a message if Event cannot be made private
	 */
	public function CheckEventPrivatePermission($id){
		$str_permission = array();
		
		$sql = "SELECT COUNT(*) AS count FROM Notes WHERE ObjectType = ? AND ObjectReferer = ? AND PrivateNote = 0 ";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, NOTETYPE_EVENT), array(DTYPE_INT, $id));
		$result = DbConnManager::GetDb('mpower')->GetOne($sql);
		
		if($result->count > 0) {
			$str_permission['STATUS'] = false;
			$str_permission['MESSAGE'] = 'Can not make Event private. Event has public notes.';
		}
		else { 
			$str_permission['STATUS'] = true;
		}
		return $str_permission;
	}

    /* TODO
     * Event is orderd not by date.
     */
	function GetEventOrderSql(){
		return " ORDER BY EventTypeID, (SELECT LastName + ' ' + FirstName FROM People WHERE PersonID = EventAttendee.PersonID), Subject";
	}
	
	function GetEventBaseSql(){
		$sql = "
			SELECT E.EventID, E.AccountID, A.Text01 as AccountName,
				E.ContactID, E.CompanyID, E.EventTypeID, E.DealID, E.PriorityID,
				Subject, Location, StartDateUTC, EndDateUTC, ModifyDate, Cancelled, Closed, Private,
				RepeatType, RepeatInterval, RepeatEndDateUTC, RepeatExceptions, RepeatParent,
				EventAttendee.PersonID as UserID, EventType.Type
			FROM Event E
				LEFT JOIN EventAttendee ON E.EventID = EventAttendee.EventID
				LEFT JOIN EventType ON E.EventTypeID = EventType.EventTypeID
				LEFT JOIN Priority ON Priority.PriorityID = E.PriorityID
				LEFT JOIN Account A ON A.AccountID = E.AccountID
			WHERE E.CompanyID = ? AND (EventAttendee.Deleted = 0 OR EventAttendee.Deleted IS NULL)";
		
		return SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->company_id));
	}

	/**
	 * Interprets event data into a standard data set for each module to use
	 */
	function ProcessEventData($events, $this_event_date = -1) {
		$utc_timezone = new DateTimeZone('UTC');
		
		$contact_ids = array();
		if(count($events) > 0) {
			foreach ($events as $event) {
				if (!in_array($event['ContactID'], $contact_ids)) {
					$contact_ids[] = $event['ContactID'];
				}
			}
			
			$contact_lookup = GetContactInfo($this->company_id, $contact_ids);
		}

		$i = 0;
		// Populate event array based on recurring rules
		foreach ($events as $key => $event) {

            $date = $event['StartDateUTC'];
            $date = str_replace(':000','',$date);
            $date = str_replace('.000','',$date);

			if ($event['Type'] == 'TASK') {
				// In order to ensure tasks remain on the correct day, do not shift for UTC
				$start_date = new JetDateTime($date, $this->user_tz);
				$priority = PriorityUtil::one($event['PriorityID']);
				$event['Priority'] = $priority['Priority'];
            } else {
				$start_date = new JetDateTime($date,$utc_timezone);
			}
		
            $date = $event['EndDateUTC'];
            $date = str_replace(':000','',$date);
            $date = str_replace('.000','',$date);

			$end_date = new JetDateTime($date, $utc_timezone);
			$duration = GetDuration($start_date, $end_date, false);
			
			if ($this_event_date instanceof DateTime) {
				$start_date->setDate($this_event_date->format('Y'), $this_event_date->format('n'), $this_event_date->format('j'));
				$end_date = clone $start_date;
				$end_date->modify("+$duration seconds");
			}
			
			$start_date->setTimezone($this->user_tz);
			$end_date->setTimezone($this->user_tz);
			
			$event['start_date'] = $start_date;
			$event['end_date'] = $end_date;
			
			$today_begin = new JetDateTime();
			$today_begin->setTimezone($this->user_tz);
			$today_begin->setTime(0, 0, 0);

			$event['original_date'] = $event['start_date'];
				
			// Pending = the scheduled time has past and it isn't closed
			if ($event['start_date'] < $today_begin && !$event['Closed']) {

				$event['pending'] = true;
				if ($event['Type'] == 'TASK') {
					$event['start_date'] = $today_begin;
				}
			} else {
				$event['pending'] = FALSE;
				if ($event['Type'] == 'TASK' && $event['Closed']) {
					// For tasks, move the event up to the day it actually was closed
					$event['original_date'] = $event['start_date'] = new JetDateTime($event['ModifyDate'], $this->user_tz);
				}
			}
			
			$event['is_edit_functional'] = TRUE;
			
			// $event['is_viewable'] should be the test to display the event info or
			// show it as 'Personal'
			if ($event['UserID'] != $this->user_id && $event['Private'] == 1) {
				$event['is_viewable'] = FALSE;
				$event['EventID'] = 0;
			} else {
				$event['is_viewable'] = TRUE;
				$event['Contact'] = $contact_lookup[$event['ContactID']];
			}
			
			// To use as array keys and comparison in ModuleCalendar
			$event['start'] = $event['start_date']->format('Ymd');
			$event['end'] = $event['end_date']->format('Ymd');

			$event['orig_date_string'] = $event['original_date']->NormalDateFormat();
			
			// For display in tpl - these are the proper display formats
			// If we ever need to customize the way the calendar displays dates
			// and times, it can be done here in one fell swoop
			$event['start_date_string'] = $event['date_string'] = $event['date_form'] = $event['start_date']->NormalDateFormat();
			$event['end_date_string'] = $event['end_date']->NormalDateFormat();
			
			$event['start_time_string'] = $event['start_date']->DrawTime();
			$event['end_time_string'] = $event['end_date']->DrawTime();
			
			// $event['date_form'] = NormalDateFormat($event['start_time']);
			$event['start_time_form'] = $event['start_date']->format('g:i A');
			$event['end_time_form'] = $event['end_date']->format('g:i A');
			
			$event['duration_string'] = FormatDuration(GetDuration($event['start_date'], $event['end_date'], false));
			
			// Attach the user record from the tree object for easy access
			if (isset($_SESSION['tree_obj'])) {
				$event['user'] = $_SESSION['tree_obj']->GetRecord($event['UserID']);
			} else {
				// For mobile, use ReportingTree directly, besso
				$tree = new ReportingTree();
				$event['user'] = $tree->GetRecord($event['UserID']);
			}
			
			$event['id_enc'] = base64_encode(json_encode(array('i' => $event['EventID'], 'd' => UnixToTruncatedDate($event['start_date']), 'u' => $event['UserID'])));
			
			// Attach the eventtype record from the cache for easy access
			$event['eventtype'] = EventTypeLookup::GetRecord($_SESSION['USER']['COMPANYID'], $event['EventTypeID']);
			$event['eventtype']['FormEventID'] = $event['eventtype']['Type'] . '_' . $event['eventtype']['EventTypeID'];
			
			$events[$key] = $event;
			$i++;
		}

		return $events;
		
	}

	function HasWriteCalendarPermission($eventPersonId, $userId) {
		if ($eventPersonId == $userId) return TRUE;
		
		$sql = "SELECT * FROM CalendarPermission WHERE PersonID = ? AND OwnerPersonID = ? AND WritePermission = 1";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $userId), array(DTYPE_INT, $eventPersonId));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		if (count($result) > 0) {
			return TRUE;
		}
		return FALSE;
	}

	function GetPermittedUserList($eventPersonId) {
		$sql = "SELECT PersonID as OwnerPersonID, FirstName, LastName FROM People WHERE
			? IN (SupervisorID, PersonID)
			UNION
			SELECT OwnerPersonID, FirstName, LastName
			FROM CalendarPermission INNER JOIN People
				ON CalendarPermission.OwnerPersonID = People.PersonID
			WHERE CalendarPermission.PersonID = ?
			AND CalendarPermission.WritePermission = 1
			AND People.SupervisorID NOT IN (-1, -3)
			ORDER BY LastName, FirstName";
		
		//$sql = "SELECT OwnerPersonID FROM CalendarPermission WHERE PersonID = ? AND WritePermission = 1";
		

		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($eventPersonId, $eventPersonId)));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		return $result;
	}

	function GetUserCalendarPermission($eventPersonId, $userId) {
		if ($eventPersonId == $userId) return TRUE;
		
		$sql = "SELECT * FROM CalendarPermission WHERE PersonID = ? AND OwnerPersonID = ? AND (ReadPermission = 1 OR WritePermission = 1)";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $userId), array(DTYPE_INT, $eventPersonId));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		if (count($result) > 0) {
			return TRUE;
		}
		return FALSE;
	}

	function GetEventsByDateRange($start_date, $end_date, $attendee = '', $type = 'BOTH', $filter = false, $limit = false) {
		//var_dump($start_date, $end_date);
		if(is_array($attendee)){
			//FIX for weird bug where 'undefined' is being added to $attendee (temporary patch util cause is found)
			$attendee = $this->removeUndefined($attendee);
		}		
		// Convert start_date and end_date strings into UTC
		$utc_timezone = new DateTimeZone('UTC');
		
		$start_date = new JetDateTime($start_date, $utc_timezone);
		
		$end_date = new JetDateTime($end_date, $utc_timezone);
		// Advance $end_date to the last second of the day to ensure inclusion
		// of all events any time of that day in between statements
		$end_date->modify("+1 day -1 second");
		
		$sql = $this->GetEventBaseSql(FALSE) . " AND (Cancelled IS NULL or Cancelled <> 1)";
		
		$where_clause = array();
		if ($type == 'BOTH' || $type == 'EVENT') {
			
			$where_clause[] = "(
				(EventType.Type = 'EVENT' OR EventType.EventTypeID IS NULL) AND
				StartDateUTC <= ? AND (
					(
						RepeatType > 0 AND 
						(RepeatEndDateUTC >= ? OR RepeatEndDateUTC IS NULL)
					) OR (
						RepeatType = 0 AND EndDateUTC >= ?
					)
				))";

			$params[] = array(DTYPE_STRING, $end_date->asMsSql());
			$params[] = array(DTYPE_STRING, $start_date->asMsSql());
			$params[] = array(DTYPE_STRING, $start_date->asMsSql());
		}
		if ($type == 'BOTH' || $type == 'TASK') {
			$today = new JetDateTime(date('Y-m-d'), $this->user_tz);
			// $today_begin = mktime(0, 0, 0, $today['mon'], $today['mday'], $today['year']);
			// $today = array(DTYPE_STRING, date("m/d/Y", $today_begin));
			/*Start JET-37  */
			$task_sql = "EventType.Type='TASK' AND (
	            (E.Closed = 1 AND E.StartDateUTC between ? AND ?)
				OR
	            ((E.Closed = 0 OR E.Closed IS NULL) AND E.StartDateUTC >= ? AND E.StartDateUTC between ? AND ?)";
	            /*End JET-37  */
			// Task params should be in local time (i.e. time = 00:00:00) rather than UTC offset in order to ensure that
			// tasks always remain on the correct day
			$params[] = array(DTYPE_STRING, $start_date->asMsSql('b', false));
			$params[] = array(DTYPE_STRING, $end_date->asMsSql('b', false));
			$params[] = array(DTYPE_STRING, $today->asMsSql('b', false));
			$params[] = array(DTYPE_STRING, $start_date->asMsSql('b', false));
			$params[] = array(DTYPE_STRING, $end_date->asMsSql('b', false));

		
			// When today is in the result set, include all unclosed tasks from the past
			/*Start JET-37  */
			if ($type != 'TASK' && $today >= $start_date && $today <= $end_date) {
				$task_sql .= "OR ((E.Closed = 0 OR E.Closed IS NULL) AND E.StartDateUTC <= ?)";
				$params[] = array(DTYPE_STRING, $today->asMsSql());
			}
			/*End JET-37  */
			$where_clause[] = "($task_sql))";
		}

		
		$sql .= " AND (" . implode(' OR ', $where_clause) . ")";
		
		// $attendee = -1 indicates that this is to return events for all users
		if ($attendee != -1) {
			if (is_array($attendee) && count($attendee) > 0) {
				$sql .= " AND EventAttendee.PersonID in (" . ArrayQm($attendee) . ")";
				$params[] = array(DTYPE_INT, $attendee);
			} else {
				$sql .= " AND EventAttendee.PersonID = ?";
				if (!is_array($attendee) && $attendee != '') {
					$params[] = array(DTYPE_INT, $attendee);
				} else {
					$params[] = array(DTYPE_INT, $this->user_id);
				}
			}
		}
	
		if (is_array($filter) && isset($filter['type']) && isset($filter['rec_id'])) {
			if ($filter['type'] == CONTACT) {
				$sql .= " AND (E.Closed = 0 OR E.Closed IS NULL) AND E.ContactID = ?";
			}
			if ($filter['type'] == ACCOUNT) {
				//$sql .= " AND (E.Closed = 0 OR E.Closed IS NULL) AND ? = (SELECT AccountID FROM CONTACT WHERE ContactID = E.ContactID)";
				/* 
				 * For just compnay task without contact, we need not this condition, besso
				 */
				$sql .= " AND (E.Closed = 0 OR E.Closed IS NULL) AND ? = E.AccountID";
			}			
			$params[] = array(DTYPE_INT, $filter['rec_id']);
		}

		if ($type == 'TASK') {
			$sql .= ' ORDER BY Priority.Weight DESC, StartDateUTC';
		}
		else {
			$sql .= $this->GetEventOrderSql();
		}
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);

       // echo '<pre>'; print_r($sql); echo '</pre>'; exit;

		$aEvent = DbConnManager::GetDb('mpower')->Exec($sql);
        $events = array();
        if (count($aEvent) > 0 ) {
            foreach ($aEvent as $e) {
                $events[$e['EventID']] = $e;
            }
        }
        /*
		echo 'BEFORE PROCESSING:';
        echo '<pre>'; print_r($events); echo '</pre>';
        exit;
        */
		$events = $this->ProcessEventData($this->BreakoutRepeating($events, $start_date, $end_date, $limit));
		mergesort($events, 'CompareEvent');
        /*
		echo 'AFTER PROCESSING: ';
        echo '<pre>'; print_r($events); echo '</pre>';
        exit;
        */
		return $events;
	}

	function removeUndefined($arr){
		$clean = array();
		foreach($arr as $a){
			if($a != 'undefined'){
				$clean[] = $a;
			}
		}
		return $clean;
	}
	function BreakoutRepeating($events, $start_date, $end_date, $count_limit = false) {
		//echo "<pre>\n" . print_r($events, TRUE) . "\n start date: ". print_r($start_date,true) ." \n end date: " . print_r($end_date,true) ."\n</pre>";

		$utc_timezone = new DateTimeZone('UTC');
		$start_date->setTimezone($this->user_tz);
		$end_date->setTimezone($this->user_tz);
		
		$show_events = array();

		foreach ($events as $event) {
			if ($event['Type'] == 'TASK' ) {
				$show_events[] = $event;
			}
		}

		$test_day = clone $start_date;
		
		while($test_day <= $end_date) {

			foreach ($events as $key => &$event) {
				if ($event['Type'] == 'TASK') {
					continue;
				}
				if (empty($event['start_day'])) {

                    $date = $event['StartDateUTC'];
                    $date = str_replace(':000','',$date);
                    $date = str_replace('.000','',$date);

					$event['start_day'] = new DateTime($date, $utc_timezone);
					$event['start_day']->setTimeZone($this->user_tz);
					$event['start_day']->setTime(0, 0, 0);

                    if (!empty($event['RepeatEndDateUTC'])) {
                        $date = $event['RepeatEndDateUTC'];
                        $date = str_replace(':000','',$date);
                        $date = str_replace('.000','',$date);
						$event['end_day'] = new DateTime($date,$this->user_tz);
						$event['end_day']->setTimeZone($this->user_tz);
						$event['end_day']->setTime(0, 0, 0);
					}
					if ($event['RepeatType'] > 0 && !empty($event['RepeatExceptions'])) {
						$event['exception_days'] = explode(',', $event['RepeatExceptions']);
						/*
						if($event['EventID'] == 301492){
							echo 'len: ' . strlen($event['RepeatExceptions']);
							DebugUtil::show($event['exception_days'], 'EXCEPTION DAYS');
							echo 'FOUND? ';
							echo in_array('20110914', $event['exception_days']);
						}
						*/
						
					} else {
						$event['exception_days'] = array();
					}
					$event['repeat_detail'] = unserialize($event['RepeatInterval']);
				}
				/*
				
				if($event['EventID'] == 301492){
					if(!empty($event['repeat_detail'])){
						echo '<pre>';
						echo '---------------START (' . $event['StartDateUTC'] . ':' . $event['EventID'] . ':' . $event['Subject'] . ')------------------------<br>';
						print_r($event);
						echo '------------------END----------------------------------------------------------------------------<br>';
						echo '</pre>';
					}
				}
				*/
				// $repeat_detail = unserialize($event['RepeatInterval']);
				// echo "<br>Start Date: " . date("m/d/y", $event['start_day']) . " ";
				// echo "Test Date: " . date("m/d/y", $test_day) . "</br>";
				// Ensure the day we are testing is after the start of this event
				if ($test_day >= $event['start_day'] && (empty($event['end_day']) || $test_day <= $event['end_day'])) {
					// Move on if this day is in the repeating exceptions
					if (in_array($test_day->format('Ymd'), $event['exception_days'])) {
						continue;
					}
					switch ($event['RepeatType']) {
						case REPEAT_NONE :
							// If the event is for this day
							if ($test_day == $event['start_day']) {
								$show_events[] = $event;
							}
							break;
						case REPEAT_DAILY :
							$freq = $event['repeat_detail'][0];
							if (CompareRepeatFreq(unixtojd($test_day->format('U')), unixtojd($event['start_day']->format('U')), $freq )) {
								$show_events[] = CloneEvent($event, $test_day, $this->user_tz);
							}
							break;
						case REPEAT_WEEKLY :
							list ($freq, $days) = $event['repeat_detail'];
							if (CompareRepeatFreq(UnixToWeekNum($test_day->format('U')), UnixToWeekNum($event['start_day']->format('U')), $freq)) {
								// If this weekday is in $days...
								if (in_array($test_day->format('w'), $days)) {
									$show_events[] = CloneEvent($event, $test_day, $this->user_tz);
								}
							}
							break;
						case REPEAT_MONTHLY :
							// echo '<pre>' . print_r($repeat_detail, true) . '</pre>';
							list ($freq, $month_day) = $event['repeat_detail'];
							if (CompareRepeatFreq(UnixToMonthNum($test_day->format('U')), UnixToMonthNum($event['start_day']->format('U')), $freq)) {
								// and this day num matches $day...
								// or if this is the last day of the month and $day > today
								if ($test_day->format('j') == $month_day || ($test_day->format('j') == $test_day->format('t') && $month_day > $test_day->format('t'))) {
									$show_events[] = CloneEvent($event, $test_day, $this->user_tz);
								}
							}
							break;
						case REPEAT_MONTHLY_POS :
							list ($freq, $pos, $pos_type) = $event['repeat_detail'];
							if (CompareRepeatFreq(UnixToMonthNum($test_day->format('U')), UnixToMonthNum($event['start_day']->format('U')), $freq)) {
								// and this day num matches $day...
								if (GetDayPosition($pos, $pos_type, $test_day) == $test_day) {
									$show_events[] = CloneEvent($event, $test_day, $this->user_tz);
								}
							}
							break;
						case REPEAT_YEARLY :
							list ($freq, $month, $month_day) = $event['repeat_detail'];
							if (CompareRepeatFreq($test_day->format('Y'), $event['start_day']->format('Y'), $freq) && $test_day->format('m') == $month && $test_day->format('j') == $month_day) {
								$show_events[] = CloneEvent($event, $test_day, $this->user_tz);
							}
							break;
						case REPEAT_YEARLY_POS :
							list ($freq, $pos, $pos_type, $pos_month) = $event['repeat_detail'];
							if (CompareRepeatFreq($test_day->format('Y'), $event['start_day']->format('Y'), $freq) && $test_day->format('m') == $pos_month) {
								if (GetDayPosition($pos, $pos_type, $test_day) == $test_day) {
									$show_events[] = CloneEvent($event, $test_day, $this->user_tz);
								}
							}
							break;
					}
				}
				if ($count_limit && count($show_events) >= $count_limit) {
					break;
				}
			}
			$test_day->modify('+1 day');
		}
//		file_put_contents('c:/webtemp/event.txt', "\n " . print_r($show_events, TRUE ) . "\n" , FILE_APPEND);  

		return $show_events;
	}
	
	
	function event_merge($events, $recurring_event_list) {
		$recurring_events = array();
		foreach ($recurring_event_list as $recurring_event) {
			
			$found = false;
			
			foreach ($events as $event) {
				if ($event['date_form'] == $recurring_event['date_form'] && $event['recurring']['RecurringEventID'] == $recurring_event['recurring']['RecurringEventID']) {
					$found = true;
				}
			}
			
			if (!$found) $recurring_events[] = $recurring_event;
		}
		
		$events = array_merge($events, $recurring_events);
		
		usort($events, 'CompareEvent');
		
		return $events;
	}

	/**
	 * get particular event details by eventId
	 */
	function GetEventById($id, $process = true, $user = -1, $this_event_date = -1) {
		if ((!$id > 0) || !(is_numeric($id))) {
			return FALSE;
		}
		
		$sql = $this->GetEventBaseSql(FALSE) . ' AND E.EventID = ?';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id));

		if ($user != -1) {
			$sql .= ' AND PersonID = ?';
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $user));
		}
		
		$aEventInfo = DbConnManager::GetDb('mpower')->Exec($sql);
        $event_info = array();
        if ( count($aEventInfo) > 0 ) {
            foreach($aEventInfo as $e ) {
                $event_info[$e['EventId']] = $e;
            }
        }
		
		if ($process) {
			$event_info = $this->ProcessEventData($event_info, $this_event_date);
		}
		
		return $event_info;
	}

	function GetEventsUpcoming($type, $id) {
		$sql = $this->GetEventBaseSql() . " AND E.Cancelled != 1 AND (EventType.Type='EVENT' OR EventType.EventTypeID IS NULL)";
		$params = array();
		$today = date("m/d/Y");
		
		switch ($type) {
			case 'user' :
				// Casting a date to integer then back to datetime removes any associated time
				//$sql .= " AND E.UserID = ? AND cast(cast(E.StartDate as integer) as datetime) = ? ";
				// The above cast function converts "2009-01-28 18:00" to "2009-01-29 00:00", so i have removed the time and then convert it to datetime.
				// NY 2-17-09 : Why?  The original code accomplished the task.  You should explain why when you rewrite working code.
				$sql .= " AND EventAttendee.PersonID = ? AND cast(CONVERT(varchar(10), E.StartDate, 101) as datetime) = ? ";
				$params[] = array(DTYPE_INT, $this->user_id);
				$params[] = array(DTYPE_TIME, $today);
				break;
			case 'contact' :
				$sql .= " AND E.ContactID = ? AND E.StartDate >= ? AND (E.Private != 1 OR (EventAttendee.PersonID = ? AND E.Private = 1))";
				$params[] = array(DTYPE_INT, $id);
				$params[] = array(DTYPE_TIME, $today);
				$params[] = array(DTYPE_INT, $this->user_id);
				break;
			case 'account' :
				$sql .= " AND ? in (C.AccountID, O.AccountID) AND E.StartDate >= ? AND (E.Private != 1 OR (EventAttendee.PersonID = ? AND E.Private = 1))";
				$params[] = array(DTYPE_INT, $id);
				$params[] = array(DTYPE_TIME, $today);
				$params[] = array(DTYPE_INT, $this->user_id);
				break;
		}
		
		$sql .= "ORDER BY E.StartDate";
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		// echo $sql . '<br>';
		$events = DbConnManager::GetDb('mpower')->Exec($sql);
		return $this->ProcessEventData($events);
	}

	/**
	 * Get Uncompleted events.
	 */
	function GetUncompletedEvents( $type = 'EVENT' ) {
		$today = date("m/d/Y");
		$sql = $this->GetEventBaseSql(FALSE) . " AND (E.Cancelled != 1 OR E.Cancelled IS NULL)
				AND (E.Closed != 1 OR E.Closed IS NULL)
				AND EventType.Type = ?
				AND EventAttendee.Creator = 1
                AND E.StartDateUTC >= ?
                AND EventAttendee.PersonID = ?
			ORDER By E.StartDateUTC";
		$params[] = array(DTYPE_STRING, $type);
        $params[] = array(DTYPE_TIME, $today);
		$params[] = array(DTYPE_INT, $this->user_id);
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        //echo '<pre>'; print_r($sql); echo '</pre>'; exit;

		$events = DbConnManager::GetDb('mpower')->Exec($sql);
        //echo '<pre>'; print_r($events); echo '</pre>'; exit;

        $parsedEvent = $this->ProcessEventData($events);
        //echo '<pre>'; print_r($parsedEvent); echo '</pre>'; exit;

		return $this->ProcessEventData($events);
	}


	/**
	 * Get Pending events.
	 */
	function GetPendingEvents() {
		$utc_timezone = new DateTimeZone('UTC');
		$pending_end = new JetDateTime("now", $this->user_tz);
		$pending_end->setTime(0, 0, 0);
		$pending_start = clone $pending_end;
		$pending_end->modify('-1 second');
		$pending_start->modify('-4 months');
		
		$sql = $this->GetEventBaseSql(FALSE) . " AND (E.Cancelled != 1 OR E.Cancelled IS NULL)
				AND (E.Closed != 1 OR E.Closed IS NULL)
				AND (
					(EventType.Type = 'EVENT' OR EventType.EventTypeID IS NULL) 
					AND StartDateUTC <= ? 
					AND (
						((RepeatType > 0 AND RepeatEndDateUTC >= ?) OR RepeatEndDateUTC IS NULL)
						OR (RepeatType = 0 AND EndDateUTC >= ?)
					)
				)
				AND EventAttendee.PersonID = ?
				AND EventAttendee.Creator = 1
				AND EventType.Type = 'EVENT'
			ORDER By E.StartDate DESC";
		
		$params[] = array(DTYPE_STRING, array($pending_end->asMsSql(), $pending_start->asMsSql(), $pending_start->asMsSql()));
		$params[] = array(DTYPE_INT, $this->user_id);
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		$events = DbConnManager::GetDb('mpower')->Exec($sql);

        $processed = $this->ProcessEventData($this->BreakoutRepeating($events, $pending_start, $pending_end));

        //echo '<pre>'; print_r($processed); echo '</pre>'; exit;
        return $processed;
	}

	
	function CheckPastEvents($user_id) {
		// To save on having to make this request every time the page is loaded, I am
		// caching the result with a timeout to check again
		// Saves ~300ms  NY 4/6/2009
		$now = time();
		
		if (isset($_SESSION['past_events_check'])) {
			$past_check = $_SESSION['past_events_check'];
			if (isset($past_check['timeout']) && $now < $past_check['timeout']) {
				//return $past_check['response'];
			}
		}
		
		$this->user_id = $user_id;
		
		$response = count($this->GetPendingEvents()) > 0;

		$timeout_length = 120; // 2 minutes
		
		$_SESSION['past_events_check'] = array('timeout' => $now + $timeout_length, 'response' => $response);
		return $response;		
		
	}	
	
	// This function is used by Module Notes to retreive the ContactID for an event
	function GetEventsContactId($eventId) {
		$sql = "select ContactID from Events where EventID = ? ";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $eventId));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		return $result[0]['ContactID'];
	}

	/**
	 * Creates the duration array.
	 */
	public function GetDurationArray($tothour) {
		$start_duration = 15;
		$duration_arr = array();
		while ($start_duration <= $tothour * 60) {
			$hour = floor($start_duration / 60);
			$min = ($start_duration % 60);
			$duration_arr[] = str_pad($hour, 2, "0", STR_PAD_LEFT) . ':' . str_pad($min, 2, "0", STR_PAD_LEFT);
			$start_duration += 15;
		}
		return $duration_arr;
	}

	/**
	 * Cancel the Event.
	 * Set the Is EventCancelled field of opportunities table to 1 if the Event is the latest event Of that Opportunity.
	 */
	function CancelEvent() {
		$sql = "SELECT * FROM Events WHERE EventID = ? AND UserID = ?";
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->eventId), array(DTYPE_INT, $this->user_id));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		$result_info = DbConnManager::GetDb('mpower')->Execute($sql, 'EventInfo');
		
		if ($result[0]['EventID'] > 0) {
			$event_id = $result[0]['EventID'];
			$opp_id = $result[0]['DealID'];
			
			$event_info = $result_info[0];
			$event_info->IsCancelled = 1;
			$result_info->Initialize()->Save();
			
			if ($opp_id > 0) {
				$sql = "SELECT Top 1 * FROM opportunities O
					LEFT JOIN Events E ON O.DealID = E.DealID WHERE O.DealID = ?
					ORDER BY E.EventID DESC";
				
				$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $opp_id));
				$result = DbConnManager::GetDb('mpower')->Execute($sql, 'OppInfo');
				
				if ($result[0]['DealID'] > 0 && ($result[0]['EventID'] == $event_id)) {
					$opp_info = $result[0];
					$opp_info->IsEventCancelled = 1;
					$result->Initialize()->Save();
				}
			}
		}
	}

	/**
	 * Close the Event.
	 * Add a Note to the Event, if someone enters the reason for closing the event.
	 */
	function CloseEvent() {
		$sql = "SELECT * FROM Events WHERE EventID = ? AND UserID = ?";
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->eventId), array(DTYPE_INT, $this->user_id));
		$result = DbConnManager::GetDb('mpower')->Execute($sql, 'EventInfo');
		
		if ($result[0]['EventID'] > 0) {
			$event_id = $result[0]['EventID'];
			$this->contactId = $result[0]['ContactID'];
			
			if ($result[0]['DealID'] > 0) {
				$this->dealID = $result[0]['DealID'];
			}
			$event_info = $result[0];
			$event_info->IsClosed = 1;
			$event_info->DateModified = TimestampToMsSql(time());
			$result->Initialize()->Save();
			return TRUE;
		}
		return FALSE;
	}

	function CheckActiveEvent($event_id, $operation) {
		
		$sql = "SELECT * FROM Events WHERE IsCancelled != 1 AND IsClosed != 1 AND EventID = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $event_id));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		
		if (count($result) > 0 && $this->CheckEventOwner($event_id)) {
			$opp_id = $result[0]['DealID'];
			$event_id = $result[0]['EventID'];
			
			/* If an active opportunity event is associated with opportunity, and present in either FMEventID or NMEventID then it is an active event
			 */
			if ($opp_id > 0) {
				$sql = "SELECT * FROM opportunities WHERE DealID = ? AND (FMEventID = ? OR NMEventID = ?)";
				$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $opp_id), array(DTYPE_INT, $event_id), array(DTYPE_INT, $event_id));
				$result = DbConnManager::GetDb('mpower')->Execute($sql);
				
				if ($result[0]['DealID'] > 0) {
					return TRUE;
				}
			} else {
				return TRUE;
			}
		}
		return FALSE;
	}

	function GetMeetingType($opp_id) {
		
		$sql = "SELECT * FROM opportunities
					WHERE DealID = ?";
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $opp_id));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		
		if ($result[0]['DealID'] > 0) {
			
			if ($result[0]['Category'] == 1) {
				return 'First Meeting';
			} else if (($result[0]['Category'] == 2) || ($result[0]['Category'] == 3) || ($result[0]['Category'] == 4) || ($result[0]['Category'] == 5)) {
				return 'Next Meeting';
			}
		}
		return '';
	}

	function GetEventType($event_id) {
		
		$sql = "SELECT * FROM Events WHERE EventID = ?";
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $event_id));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		
		if ($result[0]['EventID'] > 0) {
			
			return $result[0]['EventTypeID'];
		}
		return '';
	}

	function IsRestricted($event_id, $user_id, $company_id) {
		
		$sql = 'select distinct(Events.UserID),Events.PrivateEvent,Logins.CompanyID from Events
		LEFT JOIN Logins on Events.UserID=Logins.PersonID
		where Events.EventID=?';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $event_id));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		if (count($result) > 0) {
			if ($result[0]['CompanyID'] != $company_id) return TRUE;
			
			if ($result[0]['UserID'] != $user_id && $result[0]['PrivateEvent'] == 1) return TRUE;
		}
		return FALSE;
	}

	function getContactNames($contact) {
		$fname = $lname = '';
		$contact = trim($contact);
		if (str_word_count($contact) > 1) {
			preg_match('/[^ ]*$/', $contact, $results);
			$lname = count($results > 0) ? $results[0] : '';
		}
		
		$fname = str_replace($lname, '', $contact);
		$fname = preg_replace('#\s{2,}#', ' ', $fname);
		
		$lname = trim($lname);
		$fname = trim($fname);
		
		$name['FirstName'] = $fname;
		$name['LastName'] = $lname;
		
		return $name;
	}

	function PopulateNewEvent($contact_id) {
		$field_map_obj = new fieldsMap();
		$contact_map_fname = $field_map_obj->getContactFirstNameField();
		$contact_map_lname = $field_map_obj->getContactLastNameField();
		$company_map_name = $field_map_obj->getAccountNameField();
		
		$sql = "SELECT C.ContactID,
				C.$contact_map_fname AS FirstName,
				C.$contact_map_lname AS LastName,
				A.AccountID,
				A.$company_map_name AS CompanyName
			FROM Contact C
				LEFT JOIN Account A ON A.AccountID = C.AccountID
			WHERE C.ContactID = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $contact_id));
		
		$event_info = DbConnManager::GetDb('mpower')->Exec($sql);
		
		$event = &$event_info[0];
		
		$event['Contact'] = $event['FirstName'] . ' ' . $event['LastName'];
		
		$event['start_date_string'] = NormalDateFormat(time());
		$event['start_time_form'] = date('g:i A');
		
		$event['end_date_string'] = NormalDateFormat(time());
		$event['end_time_form'] = date('g:i A');
		
		return $event_info;
	}
}


/*
 * Converts a date into an integer based on the julian week number that I made up
 */
function UnixToWeekNum($date) {
	// Offset julian date by 1 to ensure weeks change after Saturday
	return floor((unixtojd($date) + 1)/7);

}

/*
 * Convert a date into an integer based on the month number since year 1 AD: 12 * year + month_num
 */
function UnixToMonthNum($date) {
	return date("Y", $date) * 12 + date("n", $date);
}

function GetDayPosition($position, $pos_type, $date) {
	
	// echo $date->format('r') . ' ';
	$pos_types = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'day', 'Weekday', 'Weekend Day');
	$positions = array(1 => 'First', 'Second', 'Third', 'Fourth', 'Last');
	
	$pos_phrase = $positions[$position];
	
	
//	echo "$pos_phrase $pos_types[$pos_type] ";
	
	if (isset($_SESSION['USER']['BROWSER_TIMEZONE'])) {
		$tz = new DateTimeZone($_SESSION['USER']['BROWSER_TIMEZONE']);
	} else {
		$tz = new DateTimeZone('UTC');
	}
	
	if ($position == 5) {  // Last starts with the first day of next month, then work backwards
		$start = new JetDateTime('01/01/2000T00:00:00', $tz);
		$start->setTimezone($tz);
		$start->setDate($date->format('Y'), $date->format('m') + 1, 1);
	} else {  // Otherwise start with the last day of the previous month
		$start = new JetDateTime('01/01/2000T00:00:00', $tz);
		$start->setDate($date->format('Y'), $date->format('m'), 0);
	}
	
	// echo $start->format('r') . ' ';

	if ($pos_type == 8) { // Weekday
		$pos_type = 7; // Switch over to position type day
		if ($position < 5) { // Handle first through fourth
			// $first_of_month = new DateTime($date->format('Y') . '/' . $date->format('m') . '/01T00:00:00', $tz);
			$weekend_offset = 6 - $start->format('w');
			
			// echo $start->format('w') . ' ' . $weekend_offset . '<br>';
			
			if ($position < $weekend_offset) {
				$add_days = $position;
			} elseif ($weekend_offset > 0) { // This weekday is on the other side of a weekend, so adjust by 2
				$add_days = $position + 2;
			} else {
				$add_days = $position + 1; // Month starts on a Sunday, so shift the day by 1
			}
			$pos_phrase = "+$add_days";
		} else { // Handle last
			$day_of_week = $start->format('w');
			
			
			if ($day_of_week > 1) { // If week ends on a Mon - Fri, use that
				$sub_days = 1;
			} else {
				$sub_days = 2 + $day_of_week; // Otherwise offset by 1 or 2 days
			}
			$pos_phrase = "-$sub_days";
		}
	}
	
	if ($pos_type == 9) { // Weekend Day
		if ($position < 5) { // Handle last weekend day separately
			$first_of_month = new DateTime($date->format('Y') . '/' . $date->format('m') . '/01T00:00:00', $tz);
			if ($first_of_month->format('w') == 0) { // If month starts on a Sunday
				if ($position == 1 || $position == 3) { // First and third weekend days are Sundays
					$pos_type = 0;
				} else { // Second and forth weekend days are Saturdays
					$pos_type = 6;
				}
			} else {
				if ($position == 1 || $position == 3) {  // First and third weekend days are Saturdays
					$pos_type = 6;
				} else { // Second and forth weekend days are Sundays
					$pos_type = 0;
				}
			}

			if ($position < 3) {
				$position = 1;
			} else {
				$position = 2;
			}
		} else {
			$last_of_month = new DateTime($date->format('Y') . '/' . $date->format('m') . '/' . $date->format('t') . 'T00:00:00', $tz);

			if ($last_of_month->format('w') == 6) { // If last day of the month is a Saturday, return that
				return $last_of_month;
			} else { // Else return the last Sunday
				$pos_type = 0;
			}
		}
		$pos_phrase = $positions[$position];
	}

	$date_text = "$pos_phrase $pos_types[$pos_type]";
	$result_date = $start;
	
	$result_date->modify($date_text);
	
	// $result_date = strtotime($date_text, $start->format('U'));

	//echo $date_text . ' ' . date('m/d/y', $result_date) . ' ' . date('m/d/y', $start) . "\n";

	if ($position == 9 && $result_date->format("j") == 2) {
		$result_date->setTime(0, 0, 0);
		$result_date->setDate($result_date->format('Y'), $result_date->format('m'), 1);
	}
	return $result_date;
}

function CloneEvent($event, $target_day, $timezone) {
	$utc_timezone = new DateTimeZone('UTC');
	
	if ($event instanceof MpRecord) {
		$event->SetAllDirty();
	}
	$event['RepeatParent'] = $event['EventID'];

    $date = $event['StartDateUTC'];
    $date = str_replace(':000','',$date);
    $date = str_replace('.000','',$date);
	$start_date = new JetDateTime($date, $utc_timezone);

    $date = $event['EndDateUTC'];
    $date = str_replace(':000','',$date);
    $date = str_replace('.000','',$date);
	$end_date = new JetDateTime($date, $utc_timezone);
	
	list ($hours, $minutes) = GetDuration($start_date, $end_date);
	
	$start_date->setTimezone($timezone);
	$start_date->setDate($target_day->format('Y'), $target_day->format('m'), $target_day->format('d'));
	
	$event['StartDateUTC'] = $start_date->asMsSql();

	$end_date = clone $start_date;
	$end_date->modify("+$hours hours +$minutes minutes");
	
	$event['EndDateUTC'] = $end_date->asMsSql();
	return $event;
}

function GetDuration($start, $end, $min_sec = true) {
	$duration = $end->format('U') - $start->format('U');
	
	if (!$min_sec) {
		return $duration;
	}
	
	$hours = floor($duration / 3600);
	$minutes = ($duration % 3600) / 60;
	
	return array($hours, $minutes);
}

function FormatDuration($duration) {
	$hours = floor($duration / 3600);
	$minutes = ($duration % 3600) / 60;
	$output = '';
	if ($hours > 0) {
		$output .= $hours;
		$output .= ($hours == 1) ? " hr" : " hrs";
	}
	if ($minutes > 0) {
		if ($hours > 0) {
			$output .= " ";
		}
		$output .= $minutes;
		$output .= ($minutes == 1) ? " min" : " mins";
	}
	return $output;
}

function CompareRepeatFreq($test, $start, $freq) {
	if ($start == $test) {
		$output = true;
	} else {
		$output = (($test - $start) % $freq == 0);
	}

	return $output;
}


function mergesort(&$array, $cmp_function = 'strcmp') {
    // Arrays of size < 2 require no action.
    if (count($array) < 2) return;
    // Split the array in half
    $halfway = count($array) / 2;
    $array1 = array_slice($array, 0, $halfway);
    $array2 = array_slice($array, $halfway);
    // Recurse to sort the two halves
    mergesort($array1, $cmp_function);
    mergesort($array2, $cmp_function);
    // If all of $array1 is <= all of $array2, just append them.
    if (call_user_func($cmp_function, end($array1), $array2[0]) < 1) {
        $array = array_merge($array1, $array2);
        return;
    }
    // Merge the two sorted arrays into a single sorted array
    $array = array();
    $ptr1 = $ptr2 = 0;
    while ($ptr1 < count($array1) && $ptr2 < count($array2)) {
        if (call_user_func($cmp_function, $array2[$ptr2], $array1[$ptr1]) == 1) {
            $array[] = $array1[$ptr1++];
        }
        else {
            $array[] = $array2[$ptr2++];
        }
    }
    // Merge the remainder
    while ($ptr1 < count($array1)) $array[] = $array1[$ptr1++];
    while ($ptr2 < count($array2)) $array[] = $array2[$ptr2++];
    return;
} 

function CompareEvent($a, $b) {
	if ($a['start_date'] == $b['start_date']) {
		$result = ($a['end_date'] < $b['end_date']) ? -1 : 1;
		return $result;
	}
	
	$result = ($a['start_date'] < $b['start_date']) ? -1 : 1;

	return $result;
}
