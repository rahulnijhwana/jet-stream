<?php
require_once 'class.JetstreamPage.php';
$page = new JetstreamPage();

$company_id = $_SESSION['company_obj']['CompanyID'];
$user_id = $_SESSION['USER']['USERID'];

//$action = (empty($_GET['action'])) ? FALSE : $_GET['action'];
// $type = (empty($_GET['type'])) ? FALSE : $_GET['type'];
//		$contact_id = (empty($_GET['contactId'])) ? FALSE : $_GET['contactId'];
//		$account_id = (empty($_GET['accountId'])) ? FALSE : $_GET['accountId'];
//		
//		$prepop = FALSE;

switch ($action) {

	case 'pcompany' :
	case 'pcontact' :
		// using navigation links - set to previous page and last record of that page
		include 'class.Dashboard.php';
		$module = new Dashboard();
		$last_search = $_SESSION['LastSearch'];
		if ($last_search[page_no] > 1) {
			$last_search[page_no]--;  // set to previous page
			$_SESSION['LastSearch'] = $last_search;
		}
		$module->Build($_SESSION['LastSearch']); 
		$search_results = $_SESSION['search_results'];
		//echo "<pre>";print_r($search_results);"</pre>";

		// We want to place focus on the last record of the search result set
		$last_rec = count($search_results)-1;
		if (substr($search_results[$last_rec][UniqueID],0,1)=='a'){
			$rec_type = 'account';
			$rec_id = $search_results[$last_rec][AccountID];
		}
		else {
			$rec_type = 'contact';
			$rec_id = $search_results[$last_rec][ContactID];
		}
		break;

	case 'ncompany' :
	case 'ncontact' :
		// using navigation links - set to next page and first record of that page
		include 'class.Dashboard.php';
		$module = new Dashboard();
		$last_search = $_SESSION['LastSearch'];
		if ($last_search[page_no] < $_SESSION['search_maxpage']) {
			$last_search[page_no]++;  // set to next page
			$_SESSION['LastSearch'] = $last_search;
		}
		$module->Build($_SESSION['LastSearch']); 
		$search_results = $_SESSION['search_results'];
		//echo "<pre>";print_r($search_results);"</pre>";

		// We want to place focus on the first record of the search result set
		if (substr($search_results[0][UniqueID],0,1)=='a'){
			$rec_type = 'account';
			$rec_id = $search_results[0][AccountID];
		}
		else {
			$rec_type = 'contact';
			$rec_id = $search_results[0][ContactID];
		}
		break;

	default :
		// regular company/contact page load
		if (isset($_GET['contactId'])) {
			$rec_type = 'contact';
			$rec_id = intval($_GET['contactId']);
		} elseif (isset($_GET['accountId'])) {
			$rec_type = 'account';
			$rec_id = $_GET['accountId'];
		}
}

require_once 'class.ModuleCompanyContactNew.php';
$company_contact = new ModuleCompanyContactNew($company_id, $user_id);
$company_contact->Build($rec_type, $rec_id, 'display', true);
$page->AddModule($company_contact);

if (!$company_contact->IsRestricted()) {
	include 'class.ModuleNotes.php';
	$page->AddModule(new ModuleNotes($company_id, $user_id));

	include 'class.ModuleRelatedContacts.php';
	$related_contacts = new ModuleRelatedContacts($company_id, $user_id);
	$related_contacts->Build($rec_type, $rec_id);
	$page->AddModule($related_contacts);

	include 'class.ModuleUpcomingEvents.php';
	// $page->AddModule(new ModuleUpcomingEvents($company_id, $user_id));
	$page->AddModule(new ModuleUpcomingEvents($company_id, $user_id));
	
	$tasks = new ModuleUpcomingEvents($company_id, $user_id);
	$tasks->type = $rec_type . '_tasks';
	$page->AddModule($tasks);

	if ($_SESSION['company_obj']['Jetfile'] == 1){ 
	
		include 'class.ModuleJetFile.php' ; 
		$jetfile = new ModuleJetFile($company_id, $user_id);
		$jetfile->Build($rec_type, $rec_id);
		$page->AddModule($jetfile);
	}
	
	
	//Not to show the M-Power opportunity from now
	
	if ((isset($_SESSION['mpower']) && $_SESSION['mpower']) )    {
		include 'class.ModuleOpps.php';
		$open_opps = new ModuleOpps($company_id, $user_id);
		$open_opps->Build('Active', $rec_type, $rec_id, FALSE);
		$page->AddModule($open_opps);
		$closed_opps = new ModuleOpps($company_id, $user_id);
		$closed_opps->Build('Closed', $rec_type, $rec_id, FALSE);
		$page->AddModule($closed_opps);
	}



//	echo "<pre>" . print_r($_SESSION, true ). "</pre>" ;
// Adding opportunity to the side bar
	if(($_SESSION['company_obj']['UseOppTracker']) == 1){
		include 'class.ModuleOpportunitySidebar.php';
		
		if($rec_type == 'contact') $_SESSION['usage'] = "CONTACT";
		else $_SESSION['usage'] = "ACCOUNT";
		
		$_SESSION['type'] = 'Active';
		$page->AddModule(new ModuleOpportunitySidebar($company_id, $user_id));
		$_SESSION['type'] = 'Won';
		$page->AddModule(new ModuleOpportunitySidebar($company_id, $user_id));
		$_SESSION['type'] = 'Lost';
		$page->AddModule(new ModuleOpportunitySidebar($company_id, $user_id));
	}
}

$page->Render();
?>

<script>
function CompanyContactGoogle(){
	
		$.ajax({
				   type: "POST",
				   url: "ajax/ajax.CompanyContactGoogle.php",
				   success: function(msg){
console.log("Contact Inserted");
				   }
			});
	
	}
CompanyContactGoogle();	
</script>

