<?php
require_once BASE_PATH . '/slipstream/class.JetstreamPage.php';
require_once BASE_PATH . '/slipstream/class.ModuleEventNew.php';
require_once BASE_PATH . '/include/lib.date.php';
require_once BASE_PATH . '/slipstream/class.ModuleNotes.php';
require_once BASE_PATH . '/slipstream/class.ModuleMiniCalendar.php';
require_once BASE_PATH . '/slipstream/class.ModuleUpcomingEvents.php';

$page = new JetstreamPage();

require_once 'class.ModuleCompanyContactNew.php';

$company_id = $_SESSION['company_obj']['CompanyID'];
$user_id = $_SESSION['USER']['USERID'];

if (isset($_GET['eventId'])) {
	$event_id = filter_input(INPUT_GET, 'eventId');
	$repeat_date = false;
}
 
if (isset($_GET['rep'])) {
	// Ref is passed when they select opening an instance of a recurring
	$repeating_instance = json_decode(base64_decode(filter_input(INPUT_GET, 'rep')), true);
	$event_id = $repeating_instance['i'];
	$repeat_date = $repeating_instance['d'];
}

$event = new ModuleEventNew($company_id, $user_id);
$event->Build('event', $event_id, 'display', true, TrucatedDateToUnix($repeat_date));

// date_default_timezone_set('GMT');
// date_default_timezone_set($_SESSION['USER']['BROWSER_TIMEZONE']);
// echo date('D, d M Y H:i:s', $repeat_date) . '<br>';

$page->AddModule($event);

// $page->AddModule(new ModuleEvents());
$page->AddModule(new ModuleNotes());

if (!in_array($_SESSION['USER']['USERID'], array(8717, 8718, 8809, 8872, 8986))) {
	$page->AddAjaxModule(new ModuleMiniCalendar());
	$page->AddAjaxModule(new ModuleUpcomingEvents());
}

$page->Render();
?>
<script>
function checkGoogle(){
	
		$.ajax({
				   type: "POST",
				   url: "ajax/ajax.checkGoogle.php",
				   success: function(msg){

				   }
			});
	
	}
checkGoogle();	
</script>
