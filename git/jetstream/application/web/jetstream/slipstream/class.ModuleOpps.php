<?php
require_once BASE_PATH . '/slipstream/class.Opps.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.CompanyContact.php';
require_once BASE_PATH . '/include/class.fieldsMap.php';
require_once BASE_PATH . '/include/class.MapLookup.php';
require_once BASE_PATH . '/slipstream/class.Contact.php';
require_once BASE_PATH . '/slipstream/class.Account.php';

class ModuleOpps extends JetstreamModule
{
	public $title;
	protected $css_files = array('opps.css');
	protected $template_files = array('module_opps.tpl');
	public $sidebar = true;
	public $type;
	public $msg = 'None found';
	public $id;
	
	public $action;
	public $rec_id;
	public $rec_type;
	public $empty_module = FALSE;
	public $is_restricted = UNKNOWN;
	public $user_id;
	public $company_id;
	
	public $account_name = FALSE;
	public $contact_name = FALSE;

	function __construct($company_id = -1, $user_id = -1) {
		$this->company_id = ($company_id == -1) ? $_SESSION['company_obj']['CompanyID'] : $company_id;
		$this->user_id = ($user_id == -1) ? $_SESSION['USER']['USERID'] : $user_id;
	}

	function Build($opp_type, $rec_type, $rec_id, $is_restricted = UNKNOWN, $empty_module = FALSE) {

		$this->type = $opp_type;
		if ($opp_type == 'Active') {
			$this->id = 'opps_active';
			$this->title = 'Active M-Power Opps';
		} else {
			$this->id = 'opps_closed';
			$this->title = 'Closed M-Power Opps';
		}
		if (!$empty_module && $is_restricted == UNKNOWN) {
			switch ($rec_type) {
				case 'contact' :
					$objContact = new Contact();
					$is_restricted = $objContact->IsRestricted($rec_id, $this->user_id, $this->company_id);
					break;
				case 'account' :
					$objAccount = new Account();
					$is_restricted = $objAccount->IsRestricted($rec_id, $this->user_id, $this->company_id);
					break;
				default :
					$is_restricted = FALSE;
			}
		}
		
		if ($is_restricted === TRUE) {
			$this->msg = 'You are not allowed to view Opportunities';
			return;
		}
		
		if (!$empty_module) {
			$objOpps = new Opps($this->company_id, $this->user_id, $opp_type);
			switch ($rec_type) {
				case 'account' :
					$objOpps->accountId = $rec_id;
					break;
				case 'contact' :
					$objOpps->contactId = $rec_id;
					break;
			}
			$this->opps = $objOpps->getOpps();
		} else {
			$this->opps = array();
		}

		$this->buttons = array();
		// For active opps we need to provide the contact and account names for the Add Opp JS call
		if ($opp_type == 'Active') {
			$account = $contact = 0;
			switch ($rec_type) {
				case 'account' :
					if ($this->account_name != FALSE) {
						// They passed in the account name, so we will just use that
						$account = $this->account_name;
					} elseif (count($this->opps) > 0) {
						// We can get the company name off of any opps that were found
						$opp = current($this->opps);
						$account = $opp['Company'];
					} elseif (isset($rec_id)) {
						// Fallback - look up account name in the database
						$obj_acc = new CompanyContact();
						$account = $obj_acc->GetCompanyName($rec_id);
					}
					break;
				case 'contact' :
					if ($this->account_name != FALSE && $this->contact_name != FALSE) {
						// They passed in the account and contact names, so we will just use those
						$account = $this->account_name;
						$contact = $this->contact_name;
					} elseif (count($this->opps) > 0) {
						// We can get the company & contact names off of any opps that were found
						$opp = current($this->opps);
						$account = $opp['Company'];
						$contact = $opp['Contact'];
					} elseif (isset($rec_id)) {
						// Fallback - look up names in the database
						// This will be moved to account contact class
						$sql_base = "SELECT C.ContactID, (C." . MapLookup::GetContactFirstNameField($this->company_id, TRUE) . " + ' ' + C. " . MapLookup::GetContactLastNameField($this->company_id, TRUE) . ") AS Contact,
							A." . MapLookup::GetAccountNameField($this->company_id, TRUE) . " AS AccountName
							FROM Contact C
							LEFT JOIN Account A on C.AccountID = A.AccountID
							WHERE C.CompanyID = ? AND C.ContactID = ? ";
						
						$sql_base = SqlBuilder()->LoadSql($sql_base)->BuildSql(array(DTYPE_INT, array($this->company_id, $rec_id)));
						$result = DbConnManager::GetDb('mpower')->Exec($sql_base);
						
						$contact = $result[0]['Contact'];
						$account = $result[0]['AccountName'];
					}
					break;
			}
			$contact = urlencode(addslashes(trim($contact)));
			$account = urlencode(addslashes(trim($account)));
			if ( !($rec_type == 'contact' && $this->CheckCompany($rec_id) == '')) {
				$this->AddButton('add20.png', 'javascript:addOpp(-1, ' . $this->user_id . ', \'' . $account . '\', \'' . $contact . '\');', 'Add');
			}
		}
	}

	private function CheckCompany($contactid){
		$sql_base = "SELECT accountid FROM contact where contactid = ? ";
		$sql_base = SqlBuilder()->LoadSql($sql_base)->BuildSql(array(DTYPE_INT, $contactid ));
		$result = DbConnManager::GetDb('mpower')->GetOne($sql_base);
		return $result['accountid'] ; 
	
	}
	
	public function InitAjax() {
		return array('opp_type' => $this->type);
	}
	
//	public function Init() {
//		if (!isset($this->rec_type) || !isset($this->rec_id)) {
//			if (isset($_GET['contactId'])) {
//				$this->rec_type = 'contact';
//				$this->rec_id = $_GET['contactId'];
//			} elseif (isset($_GET['accountId'])) {
//				$this->rec_type = 'account';
//				$this->rec_id = $_GET['accountId'];
//			}
//		}
//		$this->user_id = $_SESSION['USER']['USERID'];
//		$this->company_id = $_SESSION['company_obj']['CompanyID'];
//	}


//	public function Draw() {
//		$this->id = ($this->type == 'Active') ? 'opps_active' : 'opps_closed';
//		$this->title = ($this->type == 'Active') ? 'Active M-Power Opportunities' : 'Closed M-Power Opportunities';
//		if (!$this->empty_module && $this->is_restricted == UNKNOWN) {
//			switch ($this->rec_type) {
//				case 'contact' :
//					$objContact = new Contact();
//					$this->is_restricted = $objContact->IsRestricted($this->rec_id, $this->user_id, $this->company_id);
//					break;
//				case 'account' :
//					$objAccount = new Account();
//					$this->is_restricted = $objAccount->IsRestricted($this->rec_id, $this->user_id, $this->company_id);
//					break;
//				default :
//					$this->is_restricted = FALSE;
//			}
//		}
//		
//		if ($this->is_restricted === TRUE) {
//			$this->msg = 'You are not allowed to view Opportunities';
//			return;
//		}
//		
//		if (!$this->empty_module) {
//			$objOpps = new Opps($this->type);
//			switch ($this->rec_type) {
//				case 'account' :
//					$objOpps->accountId = $this->rec_id;
//					break;
//				case 'contact' :
//					$objOpps->contactId = $this->rec_id;
//					break;
//			}
//			$this->opps = $objOpps->getOpps();
//		} else {
//			$this->opps = array();
//		}
//		
//		$this->buttons = array();
//		// For active opps we need to provide the contact and account names for the Add Opp JS call
//		if ($this->type == 'Active') {
//			$account = $contact = 0;
//			switch ($this->rec_type) {
//				case 'account' :
//					if ($this->account_name != FALSE) {
//						// They passed in the account name, so we will just use that
//						$account = $this->account_name;
//					} elseif (count($this->opps) > 0) {
//						// We can get the company name off of any opps that were found
//						$opp = current($this->opps);
//						$account = $opp['Company'];
//					} elseif (isset($this->rec_id)) {
//						// Fallback - look up account name in the database
//						$obj_acc = new CompanyContact();
//						$account = $obj_acc->GetCompanyName($this->rec_id);
//					}
//					break;
//				case 'contact' :
//					if ($this->account_name != FALSE && $this->contact_name != FALSE) {
//						// They passed in the account and contact names, so we will just use those
//						$account = $this->account_name;
//						$contact = $this->contact_name;
//					} elseif (count($this->opps) > 0) {
//						// We can get the company & contact names off of any opps that were found
//						$opp = current($this->opps);
//						$account = $opp['Company'];
//						$contact = $opp['Contact'];
//					} elseif (isset($this->rec_id)) {
//						// Fallback - look up names in the database
//						// This will be moved to account contact class
//						$sql_base = "SELECT C.ContactID, (C." . MapLookup::GetContactFirstNameField($this->company_id, TRUE) . " + ' ' + C. " . MapLookup::GetContactLastNameField($this->company_id, TRUE) . ") AS Contact,
//							A." . MapLookup::GetAccountNameField($this->company_id, TRUE) . " AS AccountName
//							FROM Contact C
//							LEFT JOIN Account A on C.AccountID = A.AccountID
//							WHERE C.CompanyID = ? AND C.ContactID = ? ";
//						
//						$sql_base = SqlBuilder()->LoadSql($sql_base)->BuildSql(array(DTYPE_INT, array($this->company_id, $this->rec_id)));
//						
//						$result = DbConnManager::GetDb('mpower')->Exec($sql_base);
//						
//						$contact = $result[0]['Contact'];
//						$account = $result[0]['AccountName'];
//					}
//					break;
//			}
//			$contact = urlencode(addslashes(trim($contact)));
//			$account = urlencode(addslashes(trim($account)));
//			$this->AddButton('add20.png', 'javascript:addOpp(-1, ' . $this->user_id . ', \'' . $account . '\', \'' . $contact . '\');', 'Add');
//		}
//	}
}

?>