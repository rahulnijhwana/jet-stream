<?php
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/opp_tree.php';
require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';

class ModuleOppTreeSidebar extends JetstreamModule
{
	public $title = 'Salespersons';
	protected $javascript_files = array('opp_tree_jquery.js', 'overlay.js', 'jquery.bgiframe.js', 'jquery.autocomplete.js', 'event.js', 'stringexplode.js', 'ui.datepicker.js', 'jquery.cluetip.js', 'jquery.hoverIntent.js');
	protected $css_files = array('opp_tree.css', 'event.css', 'ui.datepicker.css', 'overlay.css', 'jquery.autocomplete.css', 'mini_calendar.css', 'jquery.cluetip.css');
	protected $buttons = array();
	public $sidebar = TRUE;
	public $target;
	public $msg;
	
	public $id = 'opportunity_tree';
	
	public $type;
	public $rec_id;
	public $company_id;
	public $user_id;
	public $empty_module;
	public $opp_tree;

	public function Init() {

		if (!isset($this->rec_id)) {
			if (isset($_GET['contactId'])) {
				$this->rec_id = $_GET['contactId'];
			} elseif (isset($_GET['accountId'])) {
				$this->rec_id = $_GET['accountId'];
			}
		}
				
		$this->user_id = $_SESSION['USER']['USERID'];
		$this->company_id = $_SESSION['company_obj']['CompanyID'];
	}

	public function Draw() {
		$this->AddTemplate('module_opp_tree.tpl');
		$this->msg = 'None found';		
		
		$this->opp_tree= opp_tree($this->company_id, $this->user_id);				
	}
}