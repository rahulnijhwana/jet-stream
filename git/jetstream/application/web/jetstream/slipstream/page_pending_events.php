<?php
require_once BASE_PATH . '/slipstream/class.JetstreamPage.php';
require_once BASE_PATH . '/slipstream/class.ModulePendingEvents.php';

$page = new JetstreamPage();
$page->AddModule(new ModulePendingEvents());

$page->Render();