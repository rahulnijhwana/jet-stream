<?php
/**
 * class.Calendar.php - contain Calendar class
 * @package database
 * class files include
 */
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
include_once BASE_PATH . '/include/class.fieldsMap.php';
require_once BASE_PATH . '/slipstream/class.Util.php';
require_once BASE_PATH . '/slipstream/class.Status.php';

// if (!isset($_SESSION)) session_start();


class Calendar
{
	
	var $objFieldMap;

	/**
	 * function for creating the fieldsMap class object
	 */
	function __construct() {
		$this->objFieldMap = new fieldsMap();
	}

	/**
	 * function for the calendar display section
	 * @param int $month - pass the month value
	 * @param int $year - pass the year value
	 * return an array having each date of the month for that particular year, also with the blank dates
	 */
	public function showCalendar($month, $year, &$weekArr = array()) {
		/**
		 * Get the timestamp of the 1st date of the month.
		 */
		$date = strtotime("$year/$month/1");
		
		/**
		 * Get the numeric representation of (1st day of month) of the week.
		 * 1 for Sunday through 7 for Saturday.
		 */
		$st = date('w', $date) + 1;
		
		/**
		 * Get the full textual representation of the month.
		 */
		$m = date("F", $date);
		
		/**
		 * Get the number of days in the given month.
		 */
		$totaldays = date("t", $date);
		
		/**
		 * Get the total number of days need to show in the calendar.
		 * If the month starts from friday or saturday and the month has 31 days in total, then it shows 42 days, 6 weeks.
		 * If the month starts from saturday and the month has 30 days in total, then it shows 42 days, 6 weeks.
		 * Else show 35 days, 5 weeks.
		 */
		if ($st >= 6 && $totaldays == 31) {
			$tl = 42;
		} elseif ($st == 7 && $totaldays == 30) {
			$tl = 42;
		} else {
			$tl = 35;
		}
		
		/**
		 * Creates the date array starts from 1 and ends with $tl with or without the date.
		 */
		$d = 1;
		$dt = array();
		$startweek = false;
		$weekArr = array();
		
		for($i = 1; $i <= $tl; $i++) {
			if (($i - 1) % 7 == 0) {
				$startweek = true;
			}
			if ($i >= $st && $d <= $totaldays) {
				$day = $d;
				$date = date('Ymd', strtotime($year . '-' . $month . '-' . $day));
				$dt[$date]['type'] = 'current';
				$dt[$date]['day'] = $day;
				$dt[$date]['date'] = date('m/d/Y', strtotime($year . '-' . $month . '-' . $day));
				$d++;
			} elseif ($i < $st) {
				/*
				 * To get the previous month data.
				 */
				$getTimeStamp = mktime(0, 0, 0, $month, 1 - ($st - $i), $year);
				$date = date('Ymd', $getTimeStamp);
				$dt[$date]['type'] = 'prev';
				$day = date('j', $getTimeStamp);
				$dt[$date]['day'] = $day;
				$dt[$date]['date'] = date('m/d/Y', $getTimeStamp);
			} else {
				/*
				 * To get next month data.
				 */
				$getTimeStamp = mktime(0, 0, 0, $month, $d, $year);
				$date = date('Ymd', $getTimeStamp);
				$dt[$date]['type'] = 'next';
				$day = date('j', $getTimeStamp);
				$dt[$date]['day'] = ltrim($day, 0);
				$dt[$date]['date'] = date('m/d/Y', $getTimeStamp);
				$d++;
			}
			
			if ($startweek) {
				$weekIndx = count($weekArr);
				$weekArr[$weekIndx]['week'] = date('W', strtotime($dt[$date]['date']));
				$weekArr[$weekIndx]['weekdate'] = $dt[$date]['date'];
				$startweek = false;
				$dt[$date]['week'] = date('W', strtotime($dt[$date]['date']));
			}
		}
		
		$today = date('Ymd');
		if (isset($dt[$today])) {
			$dt[$today]['today'] = TRUE;
		}
		
		return $dt;
	}

	/**
	 * function to build an array having key as ContactID and value as Contact First Name
	 * @param array $passVal - pass the array from where it collects the ContactID
	 * @param string $ContactIDField - pass the key name for ContactID in above passing array $passVal
	 */
	public function fetchContacts($passVal, $ContactIDField) {
		$contactArr = array();
		foreach ($passVal as $key => $value) {
			$ContactID = $passVal[$key][$ContactIDField];
			
			$contactArr[$ContactID] = getContactFirstName($ContactID);
		}
		return $contactArr;
	}
}
?>
