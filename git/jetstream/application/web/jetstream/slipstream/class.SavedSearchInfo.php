<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.MpRecord.php';

class SavedSearchInfo extends MpRecord
{
	public function Initialize() {
		$this->db_table = 'SavedSearch';
		$this->recordset->GetFieldDef('SavedSearchID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('SavedSearchType')->required = TRUE;
		$this->recordset->GetFieldDef('SavedSearchName')->required = TRUE;
		parent::Initialize();
	}
	public function Save($simulate = false) {
		parent::Save($simulate);
	}
}