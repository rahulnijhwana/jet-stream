<?php
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.ModuleAjaxLoad.php';
require_once BASE_PATH . '/slipstream/class.Event.php';
require_once BASE_PATH . '/slipstream/class.Note.php';
/*Start Jet-37*/
require_once BASE_PATH . '/include/class.GoogleConnect.php';
/*End Jet-37*/

class JetstreamPage
{
	private $modules = array();
	private $content_html = array();
	private $sidebar_html = array();
	
	private $shell = 'shell_jetstream.tpl';

	private $subnav = array();
	private $core_css = array(
		'jetstream_page.css',
		'jetstream_menu.css',
		'module.css',
		'module_menu.css',
		'sidebar.css',
		'jet_datagrid.css',
		'acc_con_form.css',
		'overlay.css',
		'chromestyle.css',
		'jquery.autocomplete.css',
		'floating_div.css',
		'clockpick.1.2.5.css',
		'jquery-impromptu.css',
		'jquery-ui.css');
	
	private $core_print_css_files = array('print_css.css');
	private $page_css = array();

	private $core_javascript = array(
		'jquery.js',
		'shell.js',
		'cancel_form.js',
		'chrome.js',
		'jquery.autocomplete.js',
		'jquery.clockpick.1.2.6.min.js',
		'stringexplode.js',
		'event.js',
		'overlay.js',
		'json_old.js',
		'json2.js',
		'common_util.js',
		'mini_cal.js',
		'jquery-impromptu.2.7.min.js',
		'jquery.highlight-3.js',
		'jquery-ui.js',
		'ga.js');
		
	private $page_javascript = array();
	private $account_link = ''; // Used to store the link to the account for the nav_bar

	private $is_manager;
	private $sales_person = array();
	public $show_pending_events;
	public $show_pending_emails;
	public $email_privacy_enabled;
	/*Start Jet-37 */
	public $is_UseGoogleSync;
	/*End Jet-37 */

	private $css_files = array();
	private $print_css_files = array();
	private $javascript_files = array();

	public function __construct() {
		require 'smarty/configs/slipstream.php';
		$this->smarty = $smarty;
		
	}

	public function __get($name) {
		return $this->$name;
	}

	public function AddAjaxModule($ajax_module) {
		if (isset($_GET['debug']) && $_GET['debug'] == 'debug') {
			$this->AddModule($ajax_module);
		}
		
		$module = new ModuleAjaxLoad();
		$module->SetModule($ajax_module);

		$this->AddModule($module);
	}

	public function AddModule($module) {
		if (!$module instanceof JetstreamModule) {
			throw new Exception('AddModule: Module must be a JetstreamModule.');
		}
		
		$debug = isset($_GET['debug']) && $_GET['debug'] == 'debug';
		
		if ($debug) {
			echo '<div style="color:white">Module called: ' . get_class($module) . '</div>';
			$time_start = microtime(TRUE);
		}
		
		$module->Init();
		if (isset($module->no_display) && $module->no_display) continue;
		$this->css_files = array_merge($this->css_files, $module->css_files);
		$this->print_css_files = array_merge($this->print_css_files, $module->print_css_files);
		$this->javascript_files = array_merge($this->javascript_files, $module->javascript_files);
		
		if (isset($module->title) && !isset($this->title)) $this->title = $module->title;
		if (!empty($module->content_title)) $this->content_title = $module->content_title;
		if (!empty($module->content_sub_title)) $this->content_sub_title = $module->content_sub_title;
		if (isset($module->account_id)) $this->account_link = '&accountId=' . $module->account_id;
		
		if (is_array($module->subnav)) $this->subnav = array_merge($this->subnav, $module->subnav);
		
		$this->smarty->assign_by_ref('module', $module);
		if ($module instanceof ModuleAjaxLoad) {
			if ($module->sidebar) {
				$this->sidebar_html[] = $this->smarty->fetch('shell_ajaxload.tpl');
			} else {
				$this->content_html[] = $this->smarty->fetch('shell_ajaxload.tpl');
			}
		} elseif ($module->sidebar) {
			$module->Draw();
			$this->sidebar_html[] = $this->smarty->fetch('shell_sidebar.tpl');
		} else {
			$module->Draw();
			$this->content_html[] = $this->smarty->fetch('shell_module.tpl');
		}
		
		if ($debug) {
			$time_end = microtime(TRUE);
			$time = $time_end - $time_start;
			// $debug_total_time += $time;
			echo '<div style="color:lightblue">Execution time: ' . $time . ' seconds</div><br />';
		}
		
		return $module;
	}

	public function Render() {
		if (!$_SESSION['tree_obj']->IsManager($_SESSION['login_id'])) {
			$this->is_manager = FALSE;
		} else {
			$this->is_manager = TRUE;
			$person = $_SESSION['tree_obj']->GetRecord($_SESSION['login_id']);
			if (isset($person['children'])) {
				$child = $_SESSION['tree_obj']->SortPeople($person['children'], 'LastName');
				foreach ($child as $person_id) {
					$person = $_SESSION['tree_obj']->GetRecord($person_id);
					$this->sales_person[$person_id] = $person['FirstName'] . ' ' . $person['LastName'];
				}
			}
		}
		$obj_event = new Event();
		$this->show_pending_events = $obj_event->CheckPastEvents($_SESSION['login_id']);

		$obj_note = new Note();
	
		$this->show_pending_emails = $obj_note->CheckPastNotes($_SESSION['login_id']);
		$this->email_privacy_enabled = $obj_note->GetEmailPrivacyEnabled();
		/*Start Jet-37 */
		$this->is_UseGoogleSync = GoogleConnect::isUseGoogleSync($_SESSION['USER']['COMPANYID']);
		/*End Jet-37 */
		$this->print_css = array_merge(array_unique($this->print_css_files), $this->core_print_css_files);
		$this->page_css = array_diff(array_unique($this->css_files), $this->core_css);
		$this->page_javascript = array_diff(array_unique($this->javascript_files), $this->core_javascript);
		
		// Experimental - removing jquery from the core
		// Loading jquery from Google APIs in shell_jetstream.tpl
		$this->core_javascript = array_diff($this->core_javascript, array('jquery.js'));
		
		if($_SESSION['note_alert']){
		
			$this->core_javascript[] = 'note_alert.js';
		}
		
		$this->smarty->assign_by_ref('page', $this);
		$this->smarty->display($this->shell);
	}

	public function GetImploded($name) {
		return implode(',', $this->$name);
	}
}
