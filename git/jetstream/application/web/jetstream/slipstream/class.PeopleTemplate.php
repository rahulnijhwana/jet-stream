<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once 'class.Status.php';

class PeopleTemplate extends MpRecord
{

	public function __toString()
	{
		return 'People Template Record';
	}

	public function Initialize()
	{
		$this->db_table = 'PeopleTemplate';
		$this->recordset->GetFieldDef('PeopleTemplateID')->auto_key = TRUE;
		parent::Initialize();
	}
	
	function getTemplatesByPersonId($copmId,$personId){
		$sql = "select NoteTemplateID,PersonID,PeopleTemplateID from PeopleTemplate where CompanyID = ? AND PersonID = ?";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $copmId),array(DTYPE_INT, $personId));
		$noteTemp = DbConnManager::GetDb('mpower')->Exec($sql);
		return $noteTemp;
	}
	
	public function insertPeopleTemplate($compId,$userId,$tempId){
		$status = new Status();
		$sql = "SELECT * FROM PeopleTemplate WHERE PeopleTemplateID = -1";
		$rs = DbConnManager::GetDb('mpower')->Execute($sql);
		$this->SetDatabase(DbConnManager::GetDb('mpower'));
		$this->recordset = $rs;
		$this->Initialize();
		$this->CompanyID = $compId;
		$this->NoteTemplateID = $tempId;
		$this->PersonID = $userId;
		$this->Save();
		
		return $status;
	}
	
	function updatePeopleTemplate($compId,$peopleTempId,$tempId){
		$status = new Status();
		$sql = "SELECT * FROM PeopleTemplate WHERE PeopleTemplateID = ? AND CompanyID = ?";
		$peopleTempSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $peopleTempId),array(DTYPE_INT, $compId));
		$result = DbConnManager::GetDb('mpower')->Execute($peopleTempSql, 'PeopleTemplate');
		if (count($result) == 1) {
			$peopleTemp = $result[0];
			$peopleTemp->Initialize();
			$peopleTemp->NoteTemplateID = $tempId;
			$result->Save();
		}
		return $status;
	}
}

?>