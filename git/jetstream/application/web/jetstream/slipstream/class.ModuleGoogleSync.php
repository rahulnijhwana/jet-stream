<?php
//require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/include/class.GoogleConnect.php';
//require_once BASE_PATH . '/include/class.DebugUtil.php';

class ModuleGoogleSync extends JetstreamModule
{
	public $title = 'Email Sync Settings';
	public $sidebar = false;
	public $moduleGoogleSync = array();
	public $companyId;
	public $personId;
	public $isEnabled;
	
	//protected $css_files = array('imap.css', 'search.css');
	//protected $javascript_files = array('imap.js', 'note.js');
	protected $template_files;
	public function __construct($companyId,$personId)
	{
		$this->companyId = $companyId;
		$this->personId = $personId;
	}
	public function Init() {
		$this->setCompanyId($_SESSION['company_id']);
		$this->setPersonId($_SESSION['USER']['USERID']);
		$this->AddTemplate('module_googlesync.tpl');
		$email = $this->isGoogleSync($this->personID);
		$this->email = $email['0']['email'];
		//if (trim($this->email) == '') {
			$this->newurl = $this->authurl = $this->getAuthUrl();
		//}
	}
	public function isGoogleSync($personID){
		$sql = 'Select * from GoogleToken where personId = ?';
        $params[] = array(DTYPE_INT, $this->personId);
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		return $aProcessed = DbConnManager::GetDb('mpower')->Exec($sql);
	}
	private function getAuthUrl(){
		$googlecon = new GoogleConnect($this->personId,true);
		return $googlecon->getauthurl();
		//$authUrl = $googlecon->connect();
		//if($authUrl != 'token-in'){
			//return $authUrl;
			//}
	}
	//private function getNewAuthUrl(){
		//$googlecon = new GoogleConnect($this->personId,true);
		//return $googlecon->getauthurl();
	//}
	public function changeEmailSync(){
	}
	public function insertEmailSync(){
	}
	public function getCompanyId(){
	    return $this->companyId;
	}

	public function setCompanyId($companyId){
	    $this->companyId = $companyId;
	}

	public function getPersonId(){
	    return $this->personId;
	}

	public function setPersonId($personId){
	    $this->personId = $personId;
	}
}
