<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
include_once (BASE_PATH . '/slipstream/class.Option.php');
require_once 'class.Status.php';

class OptionSet extends MpRecord
{

	public function __toString()
	{
	        return 'Option Set';
	}
	public function Initialize()
	{
		$this->db_table = 'OptionSet';
		$this->recordset->GetFieldDef('OptionSetID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('CompanyID')->required = TRUE;
		$this->recordset->GetFieldDef('OptionSetName')->required = TRUE;
		parent::Initialize();
	}
	function insertOptionSet($companyId,$optionSetName){

		$status=new Status();
		$optionExistsID = 0;
		$sqlCheck = "SELECT OptionSetID FROM OptionSet WHERE CompanyID = ? AND OptionSetName LIKE ?";
		$sqlCheckBuild = SqlBuilder()->LoadSql( $sqlCheck );
		$sqlCheck = $sqlCheckBuild->BuildSql(array(DTYPE_INT, $companyId),array(DTYPE_STRING, $optionSetName));
		$optionExistsID = DbConnManager::GetDb('mpower')->Exec( $sqlCheck );
		if( !empty( $optionExistsID )  )
		{
			return $optionExistsID[0]['OptionSetID'];
		}
		else
		{	
			$sql = "SELECT * FROM OptionSet WHERE OptionSetID =-1";
			$rs = DbConnManager::GetDb('mpower') -> Execute($sql);
			$this->SetDatabase(DbConnManager::GetDb('mpower'));
			$this->recordset = $rs;
			$this->Initialize();
			$this->CompanyID = $companyId;
			$this->OptionSetName = trim($optionSetName);
			$this->Save();
			return 0;
		//	return $optionExistsID;
		}
	}
	
	function getLastOptionSetId($companyId,$optionSetName)
	{
		$sql="Select Max(OptionSetID) as OptionSetID from OptionSet where CompanyID = ? AND OptionSetName = ?";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId),array(DTYPE_STRING, $optionSetName));
		$optionsSetId = DbConnManager::GetDb('mpower')->Exec($sql);
		return $optionsSetId;
	}
	
	function getOptionsSetDetailsByOptionSetId($optionSetId)
	{
		$sql="select OptionSetName,CompanyID from OptionSet where OptionSetID = ?";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $optionSetId));
		$optionsSet = DbConnManager::GetDb('mpower')->Exec($sql);
		return $optionsSet;
	}

	function updateOptionSetName($optionSetId,$labelName){
		$status=new Status();
		$sql = "SELECT * FROM [OptionSet] WHERE OptionSetID = ?";
		$optionSetSql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $optionSetId));
		$result = DbConnManager::GetDb('mpower')->Execute($optionSetSql,'OptionSet');
		if (count($result) == 1) {
			$objOptionSet = $result[0];
			$objOptionSet->Initialize();
			$objOptionSet->OptionSetName = trim($labelName);
			$result->Save();
		}
		return $status;
	}
	
	
	function getOptionSetByCompanyId($companyId)
	{
		$optionObj = new Option();
		$sql="select OptionSetID,OptionSetName,CompanyID from OptionSet where CompanyID = ? OR CompanyID = -1";
		$sqlBuild = SqlBuilder()->LoadSql($sql);
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $companyId));
		$options = DbConnManager::GetDb('mpower')->Exec($sql);
		$optionSet = array();
		for($i=0;$i<count($options);$i++){
			$optionValues = $optionObj->getOptionsByOptionSetId($companyId,$options[$i]['OptionSetID']);
			$optionSet[] =  array('OptionSetName'=>$options[$i]['OptionSetName'],'OptionSetId'=>$options[$i]['OptionSetID'],'OptionSetValues'=>$optionValues);
		}
		return $optionSet;
	}
	

}



?>
