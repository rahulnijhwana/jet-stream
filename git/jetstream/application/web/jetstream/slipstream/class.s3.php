<?php


ini_set('display_errors', 0);





class JetStreamS3 {


	private $localPath = '';

	const DS = DIRECTORY_SEPARATOR;


	private $action = 'GET'; 			// ** must be set ( PUT  or GET or DELETE 

	public $acl = 'public-read-write'; 	// ** must be set (private, public-read, public-read-write)

	public $contentType = '' ;			// ** must be set ('application/vnd.ms-excel' ... )

	public $path ='' ; 

	private $error = ''; 					

	public $filename = '' ;



	public function __construct()

	{

		$ds = self::DS;

		$this->localPath = realpath('..').$ds.'Jetstream-S3'.$ds.'jetstream-files-live';

		

	}

	

	public function putFile( $path , $content, $files) {

		$this->action = 'PUT'; 


        if (is_uploaded_file($files)) {

            $filePath = $files;

        } elseif ($files != "") {

            error("Serious error");

        }

		$this->contentType = $content; 

		$fileContent = file_get_contents($files);

		$ds = self::DS;

		$filePath = $this->localPath.$ds.$path;

		$localDirPath = dirname($filePath);

		if (!is_dir($localDirPath)) {

			mkdir($localDirPath, 0777, true);

		}

		file_put_contents($filePath, $fileContent);


	}

	

	public function getFile ( $path , $content) {

		ob_start();	

	        $this->action = 'GET';

		$ds = self::DS;

		$filePath = $this->localPath.$ds.$path;

		$this->contentType = $content;

		if ( isset($this->filename) && $this->filename != '') {

			

			if (file_exists($filePath)) {

				header('Content-Type: '.$this->contentType);

		 		header('Content-disposition: attachment; filename='.$this->filename);

				print file_get_contents($filePath);

			} else {

				print $filePath.' File does not exist';

			}

			

		}



		ob_end_flush();


	} 

	

	public function deleteFile ( $path , $content) {

		$this->action = 'DELETE'; 

		$filePath = $this->localPath.$ds.$path;

		if (file_exists($filePath)) {

			return unlink($filePath);

		}



		return false;

	}

	

	

	private function checkErrors() {

		if ($this->acl == '') return false ;

		if ($this->contentType == '') return false ; 

		if ($this->path == '' ) return false ; 

		

		return true; 

	}



	private function hex2b64($str) {

		$raw = '';

		for ($i=0; $i < strlen($str); $i+=2) {

			$raw .= chr(hexdec(substr($str, $i, 2)));

		}

		return base64_encode($raw);

	}



	private function error($str) {

		print "<div style='color: red;'><h2><pre>$str</pre></h2></div>";

		die;

	}



	private function dump($var) {

		print "<pre>";

		print_r($var);

		print "</pre>";

	}

}



?>