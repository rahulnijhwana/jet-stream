<?php
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.MailTemplate.php';
require_once BASE_PATH . '/slipstream/class.Section.php';
require_once BASE_PATH . '/slipstream/class.ContactMap.php';
class ModuleEmailTemplate extends JetstreamModule
{
	public $title = 'Email Template';
	protected $javascript_files = array('email_template.js');
	protected $css_files = array();
	public $fck_editor;
	public $contact_map=array();
	public function Init() {
		$objMail = new MailTemplate();
		$objSec = new Section();
		$objContactMap = new ContactMap();
		$sections = $objSec->getContactSectionByCompanyId($_SESSION['USER']['COMPANYID']);
		foreach($sections as $sec){
			$sectionName = $sec['ContactSectionName'];
			$secId = $sec['SectionID'];
			$contactFields = $objContactMap->getMapIdBySectionId($secId);
			if($sectionName == 'DefaultSection')
				$sectionName = 'Main';
			$fields[] = array('SectionName'=>$sectionName,'Fields'=>$contactFields);	
		}
		$this->contact_map = $fields;
		if(isset($_POST['editor'])){
			$objMail->SaveTemplate($_SESSION['USER']['COMPANYID'],$_SESSION['login_id'],$this->ConvertLabels($this->contact_map,$_POST['editor']));
		}
		$this->fck_editor->Value=$this->PrepareTemplate($this->contact_map,$objMail->GetTemplate($_SESSION['login_id']));
		$this->AddTemplate('snippet_contact_mail_fields.tpl');
		$this->AddTemplate('module_email_template.tpl');
	}
	public function ConvertLabels($allData,$template_data='') 
	{
		foreach($allData as $data) {
			$sectionName = $data['SectionName'];
			if($sectionName == 'DefaultSection')
				$sectionName = 'Main';
				
			$fields = $data['Fields'];
			foreach($fields as $fld){
				$search = "{{$sectionName}.{$fld['LabelName']}}"; 
				$db_replace = "{{$fld['FieldName']}}";
				$template_data = str_replace($search,$db_replace,$template_data);
			}
		}
		return $template_data;

	}
	public function PrepareTemplate($allData,$template_data='') 
	{
		if(!is_array($fields))
			$template_data;
		
		foreach($allData as $data) {
			$sectionName = $data['SectionName'];
			if($sectionName == 'DefaultSection')
				$sectionName = 'Main';
				
			$fields = $data['Fields'];
			foreach($fields as $fld){
				$search = "{{$fld['FieldName']}}"; 
				$db_replace = "{{$sectionName}.{$fld['LabelName']}}";
				$template_data = str_replace($search,$db_replace,$template_data);
			}
		}
		return $template_data;
	}
}
