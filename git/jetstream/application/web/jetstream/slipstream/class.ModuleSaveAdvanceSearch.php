<?php
/**
 * include dashboard class to create the filter array
 */
require_once BASE_PATH . '/include/class.fieldsMap.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/include/class.OptionLookup.php';
require_once BASE_PATH . '/include/class.MapLookup.php';
require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';

class ModuleSaveAdvanceSearch extends JetstreamModule
{
	public $title = 'Saved Searches';
	public $sidebar = true;
	public $filter_list;
	public $company_id;
	public $user_id;
	public $saveSearchID = 0; 

	function __construct($company_id = -1, $user_id = -1) {
		$this->company_id = ($company_id == -1) ? (int) $_SESSION['USER']['COMPANYID'] : (int) $company_id;
		$this->user_id = ($user_id == -1) ? (int) $_SESSION['USER']['USERID'] : (int) $user_id;
	}

	public function Init() {
		$this->filter_list = $this->getSavedList();

		$this->AddTemplate('module_save_advance_search.tpl');
	
	}

	function getSavedList() {
		
		$sql = "SELECT Name, Saved_ID  FROM SaveAdvanceSearch WHERE Person_ID = ? AND Company_ID = ? AND time_deleted = '' ORDER BY Last_edited ";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->user_id), array(DTYPE_INT, $this->company_id) );
		$list = DbConnManager::GetDb('mpower')->Exec($sql);
		return $list; 
		
	}
	
	public function getSavedSearch( $id ) {
		
		$sql = "SELECT Options, Account, Contact  FROM SaveAdvanceSearch WHERE Saved_ID = ? and time_deleted = '' ";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id) );
		$list = DbConnManager::GetDb('mpower')->Exec($sql);
		if ( count($list) == 0 ) return 0; 
		else 	return $list[0] ; 
	
	}
	
	public function deleteSavedSearch( $id ) {
		$sql = "UPDATE SaveAdvanceSearch SET time_deleted = GETDATE() WHERE Saved_ID = ? ";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id) );
		$list = DbConnManager::GetDb('mpower')->Exec($sql);
		
		return 0; 
	}
	
	public function UpdateSavedSearchList($values ) {
		
		$sql = "UPDATE SaveAdvanceSearch SET Name = ? , Options = ? , Account = ? , Contact = ? ,  Last_Edited = GETDATE() FROM SaveAdvanceSearch WHERE Saved_ID = ?  ";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $values['name']), array(DTYPE_STRING, json_encode($values['options'])), array(DTYPE_STRING, json_encode($values['account'])), array(DTYPE_STRING, json_encode($values['contact'])), array(DTYPE_INT, $values['id']  )); 
		$list = DbConnManager::GetDb('mpower')->Exec($sql);
		
		return 0; 
		
	}
	
	public function InsertSearchList ($values) {
	
		$sql = "INSERT SaveAdvanceSearch (Person_ID, Company_ID, Last_Edited, time_deleted, Options, Account, Contact, Name) VALUES  ( ?, ?, GETDATE(), '', ?, ?, ?, ? )";	
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['USERID']), array(DTYPE_INT, $_SESSION['USER']['COMPANYID']),  array(DTYPE_STRING, json_encode($values['options'])), array(DTYPE_STRING, json_encode($values['account'])), array(DTYPE_STRING, json_encode($values['contact'])), array(DTYPE_STRING, $values['name'])); 
		$list = DbConnManager::GetDb('mpower')->DoInsert($sql);

		return $list; 	
	
	}
	
	public function checkDuplicate ($name) {

		$sql = "Select COUNT(Saved_ID) from SaveAdvanceSearch WHERE Person_ID = ? and Name = ? ";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $_SESSION['USER']['USERID']), array(DTYPE_STRING, $name ) ); 
		
		$list = DbConnManager::GetDb('mpower')->Exec($sql);
		
		return $list[0];
	}
}