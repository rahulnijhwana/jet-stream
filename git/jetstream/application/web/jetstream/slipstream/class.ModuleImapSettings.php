<?php
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/include/class.DebugUtil.php';


class ModuleImapSettings extends JetstreamModule {
	
	public $title = 'Email Sync Settings';
	public $sidebar = false;
	public $settings = array();
	public $companyId;
	public $personId;
	public $isEnabled;
	
	protected $css_files = array('imap.css', 'search.css');
	protected $javascript_files = array('imap.js', 'note.js');
	protected $template_files;

	
	public function Init() {
		$this->isEnabled = $_SESSION['USER']['IMAP_ENABLED'];
		$this->setCompanyId($_SESSION['company_id']);
		$this->setPersonId($_SESSION['USER']['USERID']);
		$this->AddTemplate('module_imap_settings.tpl');
		if($this->isEnabled){
			$this->loadSettings();
		}
		
	}
	
	private function loadSettings(){
		
		$sql = "
				SELECT
					ImapUserSettingsID,
					ImapServer.ImapServerID AS ServerID,
					PersonID,
					ImapUserSettings.Enabled,
					ImapUsername,
					LastSync,
					ImapUserSettings.Label AS UserLabel,
					ImapServer.Label AS ServerLabel
				FROM
					ImapUserSettings
				JOIN
					ImapServer ON ImapUserSettings.ImapServerID = ImapServer.ImapServerID
				WHERE
					ImapUserSettings.CompanyID = ? AND PersonID = ?";
		
		$params[] = array(DTYPE_INT, $this->companyId);
		$params[] = array(DTYPE_INT, $this->personId);
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
//echo '<pre>'; print_r($sql); echo '</pre>'; exit;
	
		$results = DbConnManager::GetDb('mpower')->Exec($sql);
		
		$this->settings = $results;
		
	}

	public function getCompanyId(){
	    return $this->companyId;
	}

	public function setCompanyId($companyId){
	    $this->companyId = $companyId;
	}

	public function getPersonId(){
	    return $this->personId;
	}

	public function setPersonId($personId){
	    $this->personId = $personId;
	}
}