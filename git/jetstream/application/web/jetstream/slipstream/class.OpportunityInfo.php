<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.MpRecord.php';
// require_once BASE_PATH . '/include/mpconstants.php';

/**
 * The DB fields initialization
 */
class OpportunityInfo extends MpRecord
{

	public function Initialize() {
		$this->db_table = 'ot_Opportunities';
		$this->recordset->GetFieldDef('OpportunityID')->auto_key = TRUE;
		//$this->recordset->GetFieldDef('UserID')->required = TRUE;
		//$this->recordset->GetFieldDef('ContactID')->required = TRUE;
		
		parent::Initialize();
	}

	public function Save($simulate = false) {
		parent::Save($simulate);
	}

}

?>