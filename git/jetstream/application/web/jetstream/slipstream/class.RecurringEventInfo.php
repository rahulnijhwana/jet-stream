<?php
/**
 * @package database
 */

require_once BASE_PATH . '/include/class.MpRecord.php';
// require_once BASE_PATH . '/include/mpconstants.php';

/**
 * The DB fields initialization
 */
class RecurringEventInfo extends MpRecord
{

	public function Initialize() {
		$this->db_table = 'RecurringEvent';
		$this->recordset->GetFieldDef('RecurringEventID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('Type')->required = TRUE;	
		
		parent::Initialize();
	}

	public function Save($simulate = false) {
		parent::Save($simulate);
	}

}

?>