<?php
/**
 * This file contains the email class.
 * @package database
 */
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/slipstream/class.Util.php';
require_once BASE_PATH . '/include/mpconstants.php';
define('EMAIL_ATT_PATH', 'https://www.mpasa.com/attachments/');
//define('EMAIL_ATT_PATH', 'http://localhost/email_service/attachments/');

/**
 * The DB fields initialization
 */
class Email
{
	/**
	 * Returns the list of attachments for the Contact Email.
	 */
	function getEmailAttachments($emailId) {
		$sqlAttachment = "SELECT * FROM EmailAttachments WHERE EmailContentID = ?";
		$sqlAttachment = SqlBuilder()->LoadSql($sqlAttachment)->BuildSql(array(DTYPE_INT, $emailId));
		$resultAttachment = DbConnManager::GetDb('mpower')->Exec($sqlAttachment);
		$strAttachment = '';

		if(count($resultAttachment) > 0) {
			foreach($resultAttachment as $attachment) {
				$strAttachment .= '<a href="'.EMAIL_ATT_PATH.$attachment['StoredName'].'" target="_blank">'.$attachment['OriginalName'].'</a>';
			}
		}
		return $strAttachment;
	}

	/**
	 * Returns the formatted text for email.
	 */
	 function format($text, $parseHypetrlinks = false) {
		$text = wordwrap(nl2br(utf8_encode($text)), 36, " ", true);
		return ($parseHypetrlinks) ? $text : $text;
	}

	/**
	* Provides Emails for a given email address
	* Used in email hold module
	*/
	function getEmails($email_address){

/*

		$sql = "SELECT EC.EmailContentID, EC.Subject, EC.Body, Convert(VARCHAR, EC.ReceivedDate, 106) AS ReceivedDate from EmailAddresses EA
				LEFT JOIN EmailContent EC On EA.EmailContentID = EC.EmailContentID
				WHERE EmailAddress = ? AND EC.PersonID = ? ORDER BY EC.EmailContentID DESC";

		$sql = SqlBuilder()->LoadSql($sql)
				->BuildSql(array(DTYPE_STRING, $email_address), array(DTYPE_INT, $_SESSION['login_id']));

*/
		// This seems to provide the correct set of email addresses to show for the email hold
		// There may be an simpler way to write this query
		$sql = "SELECT EC.EmailContentID, EC.FromName, EC.RecipientNames, EC.CcNames, EC.Subject, EC.Body, Convert(VARCHAR, EC.ReceivedDate, 106) AS ReceivedDate
		FROM EmailAddresses EA
		LEFT JOIN EmailContent EC On EA.EmailContentID = EC.EmailContentID
		WHERE
		EA.EmailAddress = ? AND
		(
		    (
				EA.EmailContentID IN (
					SELECT EmailContentID FROM EmailAddresses WHERE PersonID = ? AND emailaddresstype IN ('To', 'CC')
				)
				AND EmailAddressType = 'From'
		    ) OR (
				EA.EmailContentID IN (
					select EmailContentID from EmailAddresses where PersonID = ? and emailaddresstype = 'From'
				)
				AND EmailAddressType IN ('To', 'CC')
		    )
		) ORDER BY EC.ReceivedDate DESC";

		$sql = SqlBuilder()->LoadSql($sql)
				->BuildSql(array(DTYPE_STRING, $email_address), array(DTYPE_INT, $_SESSION['login_id']), array(DTYPE_INT, $_SESSION['login_id']));

				$result = DbConnManager::GetDb('mpower')->Exec($sql);

		// echo $sql;
		$my_result = array();
		foreach($result as $key=>$email) {
			$my_result[$key]['Subject'] = $this->format($email['Subject']);
			$my_result[$key]['Body'] = $this->format($email['Body'], true);
			$my_result[$key]['TimeStamp'] = $email['ReceivedDate'];
			$my_result[$key]['Attachment'] = $this->getEmailAttachments($email['EmailContentID']);
			$my_result[$key]['EmailContentID'] = $email['EmailContentID'];
			$my_result[$key]['FromEmail'] = $email_address;			
			$my_result[$key]['From'] = $email['FromName'];		
			$my_result[$key]['To'] = str_replace(';', ',', $email['RecipientNames']);
			$my_result[$key]['Cc'] = $email['CcNames'];
		}

		return $my_result;
	}


	function getContactID($companyid, $email_address, $contact_email_field_list) {
		
		$email_addresses = '';
		$contact_id = 0;

		if(!empty($contact_email_field_list) && count($contact_email_field_list) > 0) {
			foreach ($contact_email_field_list as $key=>$email) {
				if ($key == 0){
					$email_addresses = "$email = '$email_address'";
				}
				else {
					$email_addresses .= " OR $email = '$email_address'";
				}
			}

			$sql = "SELECT ContactID FROM Contact WHERE CompanyID = ? AND ($email_addresses)";
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyid));

			$contacts = DbConnManager::GetDb('mpower')->Execute($sql);
			$contact_id = (!empty($contacts) && count($contacts) > 0) ? $contacts[0]->ContactID : 0;
		}
		return $contact_id;
	}

	function getPersonID($companyid, $email) {
		
		$sql = "SELECT PersonID FROM People WHERE email = ? AND CompanyID = ? ";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $email), array(DTYPE_INT, $companyid));
		$people = DbConnManager::GetDb('mpower')->Exec($sql);
		
		$personID = 0;

		if(!(is_array($people) && count($people) > 0)) {
			// Get the email address from PeopleEmailAddr relationship table.
			$sql = "SELECT PersonID FROM PeopleEmailAddr WHERE CompanyID = ? AND EmailAddr = ?";
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyid), array(DTYPE_STRING, $email));
			$contacts_people = DbConnManager::GetDb('mpower')->Execute($sql);

			$personID = (!empty($contacts_people) && count($contacts_people) > 0) ? $contacts_people[0]->PersonID : 0;
			
		} else {
			$personID = $people[0]['PersonID'];
		}

		return $personID;
	}

	function getEmailPrivacyEnabled($companyid) {
	
		$sql = "SELECT EmailPrivacyEnabled FROM Company WHERE CompanyID = ? ";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyid));
		$company = DbConnManager::GetDb('mpower')->Exec($sql);
	
		$EmailPrivacyEnabled = (!empty($company) && count($company) > 0) ? $company[0]->EmailPrivacyEnabled : 0;
		($EmailPrivacyEnabled != null) ? $EmailPrivacyEnabled : 0;
	
		return $EmailPrivacyEnabled;
	}
	
	function getEmailAddressList($email_address, $email_names) {
		$email_address_list = array();
		$email_addresses = explode(';', $email_address);
		$email_names = explode(';', $email_names);

		if(!empty($email_names) && (count($email_names) == count($email_addresses))) {
			$email_address_list = array_combine($email_names, $email_addresses);
		}

		return $email_address_list;
	}

	function createEmailNote($note) {
		$re1='(\\()';	# Any Single Character 1
		$re2='.*?';	# Non-greedy match on filler
		$re3='(\\))';	# Any Single Character 2

		$time = preg_replace( "/".$re1.$re2.$re3."/is", '', $note['CreationDate'] ); 

		$sql = "INSERT INTO Notes (CreatedBy, CompanyID, CreationDate, Subject, NoteText, ObjectType, ObjectReferer, EmailContentID, NoteSpecialType, PrivateNote, Incoming)
							VALUES (?, ?, ?, 'Email', 'Email', ?, ?, ?, ?, ?, ?)";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $note['CreatedBy']),
									array(DTYPE_INT, $note['CompanyID']), array(DTYPE_STRING, $time),
									array(DTYPE_INT, NOTETYPE_CONTACT), array(DTYPE_INT, $note['ObjectReferer']), array(DTYPE_INT, $note['EmailContentID']),
									array(DTYPE_INT, NOTETYPE_EMAIL), array(DTYPE_INT, $note['PrivateNote']), array(DTYPE_INT, $note['Incoming']));
		$data = DbConnManager::GetDb('mpower')->Exec($sql);
	}

	/**
	 * Checks the ContactMap table and returns the field maps for email addresses
	 * @param company int, company id
	 * @return array, list of email addresses
	 */
	function getContactEmailFields($company){
		$contact_email_field_list = array();

		// Get the email fields
		$sql = "SELECT FieldName FROM ContactMap WHERE CompanyID = ? AND Enable = 1 AND ValidationType = 5";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company));
		
		$contact_map = DbConnManager::GetDb('mpower')->Execute($sql);

		if (count($contact_map) > 0) {
			foreach ($contact_map as $email_field) {
				$contact_email_field_list[] = $email_field->FieldName;
			}
		}
		
		return $contact_email_field_list;
	}
	
}
