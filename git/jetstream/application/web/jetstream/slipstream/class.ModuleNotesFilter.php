<?php
/**
 * include Notes class to create the filter array
 */
require_once BASE_PATH . '/slipstream/class.Note.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';

/**
 * Class for Notes Filter module
 */
class ModuleNotesFilter extends JetstreamModule
{
	public $title = 'Filter Notes';
	public $sidebar = true;
	public $filter_list;	

	
	public function Init() {	

		/**
		 * Create Notes() class object.
		 */				
		$objNote = new Note();
		
		if(isset($_GET['referer']) && (trim($_GET['referer']) != '')) {
			$note_referer = strtolower(trim($_GET['referer']));
			if($note_referer == 'company') {
				$note_referer_val = NOTE_TYPE.'_1';
			} else if($note_referer == 'contact') {
				$note_referer_val = NOTE_TYPE.'_2';
			} else if($note_referer == 'event') {
				$note_referer_val = NOTE_TYPE.'_3';
			} else {
				$note_referer_val = '';
			}
			if($note_referer_val != '') {
				$this->note_referer_js = 'var noteReferer = \''.$note_referer_val.'\';'."\n";
			}
		} else {
			$this->note_referer_js = '';
		}
		
		/**
		 * include tpl file for HTML section.
		 */
		$this->AddTemplate('module_notes_filter.tpl');
		
		$filterArray = $objNote->createFilterArray();
		$this->filter_list = 'var filterList = ' . json_encode($filterArray['FieldName']);
		
		$this->filter_list_val = 'var filterListVal = ' . json_encode($filterArray['FieldVal']);	
	}	
}

?>
