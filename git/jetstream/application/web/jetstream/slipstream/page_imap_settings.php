<?php
require_once BASE_PATH . '/slipstream/class.JetstreamPage.php';
require_once BASE_PATH . '/slipstream/class.ModuleImapSettings.php';

$settings = new ModuleImapSettings();

$page = new JetstreamPage();
$page->AddModule($settings);
$page->Render();