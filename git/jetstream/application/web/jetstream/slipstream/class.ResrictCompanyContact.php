<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/slipstream/class.Status.php';
require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';

class ResrictCompanyContact extends MpRecord
{
	public function checkRecordRestriction($limit_access, $rec_id, $rec_type) {
		if($limit_access == 1) {			
		 	$owner = '';
			$assigned_people = $this->GetRecordAssignedPeople($rec_id, $rec_type, $owner);
		
			$res_assigned_to_cur_user = FALSE;
				
			$my_descendants = ReportingTreeLookup::CleanGetLimb($_SESSION['USER']['USERID']);
			foreach($assigned_people as $assigned_person) {
				if (in_array($assigned_person, $my_descendants)) {
					$res_assigned_to_cur_user = TRUE;
					break;
				}
			}
			
			// if(count($assigned_people) > 0 && in_array($_SESSION['USER']['USERID'], $assigned_people)) {
			// 	$res_assigned_to_cur_user = TRUE;
			// }
		} else {
			$res_assigned_to_cur_user = TRUE;
		}
		$_SESSION['restricted_'.$rec_type][$rec_id] = $res_assigned_to_cur_user;
		
		return $res_assigned_to_cur_user;
	}
	
	
	
	public function GetRecordAssignedPeople($id, $field_type, &$owner) {
		$arr_assigned_people = array();
		$field_type = trim($field_type);
		$table_name = ($field_type == 'contact') ? ('PeopleContact') : ('PeopleAccount');
		$ref_field = ($field_type == 'contact') ? ('ContactID') : ('AccountID');

		$sql = SqlBuilder()->LoadSql("SELECT DISTINCT PersonID, CreatedBy FROM $table_name WHERE $ref_field = ? AND AssignedTo = 1 ")->BuildSql(array(DTYPE_INT, $id));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);

		if(is_array($result) && count($result) > 0) {
			foreach($result as $person) {
				$arr_assigned_people[] = $person['PersonID'];

				if($person['CreatedBy'] == 1) {
					$owner = $person['PersonID'];
				}
			}
		}	
		return $arr_assigned_people;
	}

}
?>