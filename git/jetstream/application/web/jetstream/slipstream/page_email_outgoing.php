<?php
require_once BASE_PATH . '/slipstream/class.JetstreamPage.php';
require_once BASE_PATH . '/slipstream/class.ModuleEmailOutgoing.php';
//require_once BASE_PATH . '/slipstream/class.ModuleRelatedContacts.php';

$page = new JetstreamPage();
$page->AddModule(new ModuleEmailOutgoing());
//$page->AddAjaxModule(new ModuleRelatedContacts());

$page->Render();
