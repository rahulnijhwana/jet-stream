<?php
require_once BASE_PATH . '/slipstream/class.JetstreamPage.php';
require_once BASE_PATH . '/slipstream/class.ModuleCalendar.php';
require_once BASE_PATH . '/slipstream/class.ModuleMiniCalendar.php';
require_once BASE_PATH . '/slipstream/class.ModuleUpcomingEvents.php';
require_once BASE_PATH . '/slipstream/class.FileSearch.php';
require_once BASE_PATH . '/include/lib.string.php';
require_once BASE_PATH . '/slipstream/class.ModuleJetFile.php';


$page = new JetstreamPage();

// Use a generic module because the dashboard search is such a simple page

$module = new FileSearch();

$module->title = 'Jetstream File Search ';

$module->AddTemplate('module_file_search.tpl');
$module->AddJavascript('jquery.form.js', 'ui.datepicker.js', 'ui.core.js', 'jquery.tablesorter.js');
//$module->AddJavascript('jquery.js', 'slipstream_jetmenu.js', 'jquery.form.js', 'ui.datepicker.js', 'ui.core.js', 'jquery.multiSelect.js', 'jquery.tablesorter.js');
$module->AddCss('search.css', 'ui.datepicker.css', 'jquery.multiSelect.css', 'advance_datagrid.css', 'table_sorter.css');
$module->search_types = array(3 => 'Company & Contact', 1 => 'Company', 2 => 'Contact');
$module->records_per_page = array(25, 50, 75, 100);
$module->getTags();


$jetfile = new ModuleJetFile($_SESSION['company_obj']['CompanyID'], $_SESSION['USER']['USERID']);

$jetfile->title = 'Jetstream File Library ';
$jetfile->Build('Company', $_SESSION['company_obj']['CompanyID'] );
$jetfile->sidebar = FALSE ; 

/* Set the search parameters from cookie. or $_GET*/


$page->AddModule($module);
$page->AddModule($jetfile);


$page->AddModule(new ModuleMiniCalendar());

$page->AddModule(new ModuleUpcomingEvents());

$tasks = new ModuleUpcomingEvents();
$tasks->type = 'tasks';
$page->AddModule($tasks);

$page->Render();
