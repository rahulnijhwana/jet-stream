<?php
require_once BASE_PATH . '/slipstream/lib.php';
require_once BASE_PATH . '/slipstream/class.CompanyContact.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.Account.php';
require_once BASE_PATH . '/slipstream/class.Contact.php';

class ModuleJetFile extends JetstreamModule
{
	public $title = 'Related Files';
	
	//protected $javascript_files = array('jqueryFileTree.js', 'slipstream_jetmenu.js','jquery.cluetip.js', 'jquery.hoverIntent.js', 'jquery.dimensions.js', 'jquery.uploadify.v2.1.0.js', 'swfobject.js', 'flash_detect.js', 'jquery.multiselect2side.js');
	protected $javascript_files = array('jqueryFileTree.js', 'slipstream_jetmenu.js','jquery.cluetip.js', 'jquery.hoverIntent.js', 'jquery.uploadify.v2.1.0.js', 'swfobject.js', 'flash_detect.js', 'jquery.multiselect2side.js');
	protected $css_files = array('jquery.FileTree.css', 'jquery.cluetip.css', 'uploadify.css', 'jquery.multiselect2side.css');
	protected $template_files = array('module_jetfile_contact.tpl');
	protected $buttons = array();
	public $sidebar = TRUE;
	public $is_restricted = FALSE;
	public $msg = 'None Found';
	public $id = 'related_contacts';
	public $contactid ; 
	public $accountid ; 
	
	public $company_id;
	public $user_id;
	
	public $rec_type;
	public $rec_id;
	public $empty_module;
	public $file_path;
	
	public function __construct($company_id, $user_id) {
		if (empty($company_id) || empty($user_id)) {
			throw new Exception("Construct requires company_id & user_id");
		}
		$this->company_id = $company_id;
		$this->user_id = $user_id;
	}

	public function Init() {
		return;
	}

	public function Draw() {
		if (($_GET['action'] != 'filesearch') ) {
			$this->AddButton('info20.png', 'slipstream.php?action=filesearch', 'File Search', 'search_file');
		}
		return;
	}
	
	public function Build($rec_type, $rec_id) {
		
		$this->rec_type = $rec_type; 
		$this->rec_id = $rec_id;
		
		$this->contactid = (isset($_GET['contactId'])) ? $_GET['contactId'] : ''; 
		$this->accountid = (isset($_GET['accountId'])) ? $_GET['accountId'] : '';
		
		switch($this->rec_type){
			case 'account':
				$this->file_path = 'Account';
				break;
			case 'contact':
				$this->file_path = 'Contact';
				break;
			case 'Company':
				$this->file_path = 'Company';
				break;
		}
	}
}
