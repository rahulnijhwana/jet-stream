<?php
/**
 * This file contains the note class.
 * @package database
 */

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.RecAccount.php';
require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';
require_once BASE_PATH . '/include/class.MapLookup.php';
require_once BASE_PATH . '/slipstream/class.SavedSearchInfo.php';
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

/**
 * The DB fields initialization
 */
class FileSearch extends JetstreamModule
{
	public $company_id;
	public $user_id;
	
	public $search;  // The search parameters
	
	public $query_type;
	
	public $show_no_of_pages = 10;
	public $total_records = 0;

	public $tot_pages;
	public $is_manager;
	
	public $search_results;
	public $tags ;
	
	function __construct($company_id = -1, $user_id = -1) {
		$this->company_id = ($company_id == -1 && isset($_SESSION['company_obj']['CompanyID'])) ? $_SESSION['company_obj']['CompanyID'] : $company_id;
		$this->user_id = ($user_id == -1 && isset($_SESSION['USER']['USERID'])) ? $_SESSION['USER']['USERID'] : $user_id;
	}
		
	public function Build($search) {
		$this->search = $search;
		$this->search_results = $this->SearchResults();
	}

	
	public function SearchResults() {
		//		echo "<pre>" . print_r($this->search) . '</pre>';  // DEBUG
		
		$where_clause = array() ; 
		$params = array() ; 
		
		// calculate tags value 
		if ( count($this->search['tags']) > 0 ) {
			$tagVal = $this->calc_tags_nValue( $this->search['tags'] ) ; 
			$where_clause[] = "( ? & TagValue) = ? ";
			$params[] = array(DTYPE_INT, $tagVal) ; 
			$params[] = array(DTYPE_INT, $tagVal) ; 
		}
		
		if ( !empty($this->search['searchstring']) ){
			$where_clause[] =  "LabelName LIKE ?" ; 
			$params[] = array(DTYPE_STRING, '%'.(trim($this->search['searchstring']).'%'));
		}
		
		if ( count($this->search['searchtype']) > 0  ) {
			$where_clause[] = "jetfile.Type in (". ArrayQm($this->search['searchtype']) .")";
			foreach( $this->search['searchtype'] as $filterValue ) {
				$params[] = array(DTYPE_STRING, $filterValue);
			}			
		}
		
		if ( !empty($this->search['datefrom']) OR !empty($this->search['dateto'])  ){
			// create time checks 
			$datefrom = (isset($this->search['datefrom'])) ? date('m/d/Y', strtotime($this->search['datefrom'])) : date('m/d/Y', mktime(0, 0, 0, 1, 1, 1900)) ;
			$dateto = (isset($this->search['dateto'])) ? date('m/d/Y', strtotime($this->search['dateto'])): date('m/d/Y', mktime(0, 0, 0, date("m")  , date("d"), date("Y"))) ;
			$params[] = array(DTYPE_TIME, $datefrom);
			$params[] = array(DTYPE_TIME, $dateto);
			$where_clause[] = " ( CreatedOn BETWEEN ? AND ? )";					
			
		} 
		
		// create outerJoins & Fields 
		$fields  = " (People.FirstName + ' '+ People.LastName) as LoginName, " ;
		$fields .= " (Account.Text01 ) as AccountName, " ;
		$fields .= " (Contact.Text01 + ' '+ Contact.Text02) as ContactName,  " ; 
		$fields .= " (Company.Name ) as CompanyName, " ; 
		
		$outerJoins  = " LEFT OUTER JOIN People on People.PersonID = Jetfile.CreatedBy ";
		$outerJoins .= " LEFT OUTER JOIN Account on Jetfile.Type = 'Account' and Account.AccountID = Jetfile.TypeID " ; 
		$outerJoins .= " LEFT OUTER JOIN Contact on Jetfile.Type = 'Contact' and Contact.ContactID = Jetfile.TypeID " ; 
		$outerJoins .= " LEFT OUTER JOIN Company on Jetfile.Type = 'Company' and Company.CompanyID = Jetfile.TypeID " ; 
		
		
		$where = implode( ' AND ', $where_clause ) . " AND jetfile.FileTypeID != 1 AND jetfile.DeletedBy = 0 AND  jetfile.CompanyID = ?" ; 
		$params[] = array(DTYPE_INT, $this->company_id) ; 
		$countsql = 'SELECT COUNT(*)as count FROM jetfile RIGHT JOIN FileType ON jetfile.FileTypeID = FileType.FileTypeID WHERE '. $where ;
		$sql = "SELECT * FROM ( SELECT $fields Jetfile.*, FileType.Icon, FileType.Extension, ROW_NUMBER() OVER (ORDER BY JetfileID ASC) AS RowNumber FROM jetfile $outerJoins RIGHT JOIN FileType ON jetfile.FileTypeID = FileType.FileTypeID WHERE ". $where . ' ) results  WHERE RowNumber BETWEEN 1 AND 25'; 

		$countsql = SqlBuilder()->LoadSql($countsql)->BuildSqlParam($params);
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		$search_results = DbConnManager::GetDb('mpower')->Exec($sql);
		$sqlcount = DbConnManager::GetDb('mpower')->GetOne($countsql);
		$this->total_records = $sqlcount['count'];
		
		return $search_results ; 

	}


	public function SearchResultsInfo() {
		$str_Info = '';

		if ($this->total_records > 0) {
			$end_record = $this->search['page_no'] * $this->search['records_per_page'];
			$start_record = $end_record - $this->search['records_per_page'] + 1;
			
			if ($end_record > $this->total_records) {
				$end_record = $this->total_records;
			}
			$str_Info = 'Records: ' . $start_record . ' - ' . $end_record . ' of ' . $this->total_records . '&nbsp;&nbsp;';
		}
		return $str_Info;
	}

	/**
	 * Retrieve the reords for a perticular page.
	 */
	public function returnTotPageRec() {
		/*if(!isset($this->search['page_no'])) {
			$this->search['page_no'] = 1;
		}*/
		$str_Info = '';
		
		if ($this->total_records > 0) {
			$start_record = $end_record - $this->search['records_per_page'] + 1;
			
			if ($end_record > $this->total_records) {
				$end_record = $this->total_records;
			}
			$str_Info = 'Records: ' . $start_record . ' - ' . $end_record . ' of ' . $this->total_records . '&nbsp;&nbsp;';
		}
		return $str_Info;
	}
	
	/**
	 * Parses the input field used in filter box.
	 * Here we can define The rules for filter values.
	 * Creates an array using each word entered in the input field.
	 */
	function ParseFilterSearchValue($value) {
		$parsedArr = array();
		$value = trim($value);
		if($value != '') {
			$parsedArr[] = '%'.$value.'%';
		}
		return $parsedArr;
	}
	
	

	/**
	 * Creates the pages.
	 */
	public function getDashboardPages() {
		$pagination_link = '';
		if ($this->tot_pages > 0) {
			$left_page_no = 1;
			$right_pag_no = 0;
			$div_pages = floor($this->show_no_of_pages / 2);
			
			if ($this->tot_pages == 1) {
				return '';
			}
			
			$left_page_no = $this->search['page_no'] - $div_pages;
			if ($left_page_no < 0) {
				$right_pag_no = -($left_page_no);
				$left_page_no = 1;
			} else if ($left_page_no == 0) {
				$left_page_no = 1;
			}
			
			if (($left_page_no + $div_pages) == $this->search['page_no']) {
				$right_pag_no += $this->search['page_no'] + ($div_pages - 1);
			} else {
				$right_pag_no += $this->search['page_no'] + $div_pages;
			}
			
			if ($right_pag_no > $this->tot_pages) {
				$left_page_no = $left_page_no - ($right_pag_no - $this->tot_pages);
				$right_pag_no = $this->tot_pages;
			}
			if ($left_page_no <= 0) {
				$left_page_no = 1;
			}
			
			if ($this->search['page_no'] == 1) {
				$pagination_link .= '<img class="pageImage" border="0" alt="First" src="./images/off-first.gif" disabled />';
				$pagination_link .= '<img class="pageImage" border="0" alt="Prev" src="./images/off-prev.gif" disabled />';
			} else if ($this->search['page_no'] > 1) {
				$prev_page = $this->search['page_no'] - 1;
				
				$btn_first_event = "AjaxLoadPage(1);";
				$btn_prev_event = "AjaxLoadPage('$prev_page');";
				
				$pagination_link .= '<a href="#"><img class="pageImage" border="0" src="./images/first.gif" alt="First" title="First" onclick="javascript:' . $btn_first_event . '" /></a>';
				$pagination_link .= '<a href="#"><img class="pageImage" border="0" src="./images/prev.gif" alt="Prev" title="Previous" onclick="javascript:' . $btn_prev_event . '" /></a>';
			
			}
			
			if ($this->tot_pages >= $this->search['page_no']) {
				for($i = $left_page_no; $i <= $right_pag_no; $i++) {
					$btn_event = "AjaxLoadPage('$i');";
					
					if ($i == $this->search['page_no']) {
						$pagination_link .= '<span class="page">' . $i . '</span>';
					} else {
						$pagination_link .= '<a href="#" onclick="javascript:' . $btn_event . '"><span class="page">' . $i . '</span></a>';
					}
				}
			}
			
			if ((!$this->search['page_no']) || $this->search['page_no'] < $this->tot_pages) {
				$next_page = $this->search['page_no'] + 1;
				
				$btn_next_event = "AjaxLoadPage('$next_page');";
				$btn_last_event = "AjaxLoadPage('$this->tot_pages');";
				
				$pagination_link .= '<a href="#"><img class="pageImage" border="0" src="./images/next.gif" alt="Next" title="Next" onclick="javascript:' . $btn_next_event . '" /></a>';
				$pagination_link .= '<a href="#"><img class="pageImage" border="0" src="./images/last.gif" alt="Last" title="Last" onclick="javascript:' . $btn_last_event . '" /></a>';
			} else {
				$pagination_link .= '<img class="pageImage" alt="Next" border="0" src="./images/off-next.gif" disabled />';
				$pagination_link .= '<img class="pageImage" alt="Last" border="0" src="./images/off-last.gif" disabled />';
			}
		}
		return $pagination_link;
	}
	

	
	private function calc_tags_nValue( $param ){
		$vals = is_array($param) ? $param : explode("|", $param); 
		
		$n = 0 ; 
		
		foreach ( $vals as &$val ){
			if ($val != '')	$n = $n + pow(2, $val); 
		}
		
		return $n ;	
	}

	
	public function getTags(){
		
		$sql = 'SELECT n, labelName from JetTags where CompanyID= ? and DeletedOn is NULL ' ; 
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $this->company_id));
		$results = DbConnManager::GetDb('mpower')->exec($sql);
		$this->tags = $results ; 	
		return 1;
	} 


}