<?php
require_once BASE_PATH . '/slipstream/class.JetstreamPage.php';
require_once BASE_PATH . '/slipstream/class.ModuleEmailTemplate.php';
require_once BASE_PATH . '/www/fckeditor/fckeditor_php5.php';
$objEmail = new ModuleEmailTemplate();
$objEmail->fck_editor = new FCKeditor('editor');
$objEmail->fck_editor->BasePath = './fckeditor/';
$objEmail->fck_editor->Height = "400px";
$objEmail->fck_editor->Width = "600px";
$objEmail->fck_editor->ToolbarSet = 'Selected';
$page = new JetstreamPage();
$page->AddModule($objEmail);
$page->Render();
