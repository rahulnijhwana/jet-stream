<?php
/**
 * @package database
 */
require_once BASE_PATH . '/include/class.fieldsMap.php';
require_once BASE_PATH . '/slipstream/class.Util.php';

/**
 * Contact class declaration
 */

class ContactHint
{

	/**
	* Provides Matching contact for a given name
	* Used in email hold module
	*/
	function getMatchingContact($first_name, $last_name) {

		$company_id = $_SESSION['mpower_companyid'];
		$contact = trim($first_name.' '. $last_name);

		$field_map_obj = new fieldsMap();
		$contact_map_fname = $field_map_obj->getContactFirstNameField();
		$contact_map_lname = $field_map_obj->getContactLastNameField();
		$company_map_name = $field_map_obj->getAccountNameField();

		$sql = "SELECT C.ContactID, (C.$contact_map_fname + ' ' + C.$contact_map_lname) AS Contact, A.$company_map_name AS Account
			FROM Contact C
			LEFT JOIN Account A on C.AccountID = A.AccountID
			WHERE A.CompanyID = $company_id AND A.Inactive != 1 AND A.PrivateAccount != 1
			AND (C.$contact_map_fname like '$contact%' OR C.$contact_map_lname like '$contact%') AND C.Inactive != 1 AND C.PrivateContact != 1";

		//$sqlBuild = SqlBuilder()->LoadSql($sql);
		//$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $_SESSION['mpower_companyid']));
		$result = DbConnManager::GetDb('mpower')->Exec($sql);

		$contact_list = array();

		if (isset($result) && (count($result) > 0)) {
			foreach ($result as $key=>$row) {
				$contact_list[$row['ContactID']]['ContactName'] = $row['Contact'];
				$contact_list[$row['ContactID']]['ContactID'] = $row['ContactID'];
				$contact_list[$row['ContactID']]['AccountName'] = $row['Account'];
			}
		}

		return $contact_list;
	}

	/**
	* Provides Email addresses for a given contact id 
	* Used in email hold module
	*/
	function getEmailAddresses($contact_id){

		$field_map_obj = new fieldsMap();
		$email_list = $field_map_obj->getContactEmailFields(false);
		$select = implode(',', array_keys($email_list));

		$sql = "SELECT $select FROM Contact WHERE ContactID = $contact_id";
		$result = DbConnManager::GetDb('mpower')->Exec($sql);

		$my_result = array();
		foreach($email_list as $field=>$label) {
			$my_result[$field]['Label'] = $label;
			$my_result[$field]['Value'] = $result[0][$field];
		}

		return $my_result;
	}

}
