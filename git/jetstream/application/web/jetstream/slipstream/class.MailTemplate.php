<?php
	require_once BASE_PATH . '/include/class.DbConnManager.php';
	require_once BASE_PATH . '/include/class.SqlBuilder.php';
	require_once BASE_PATH . '/include/class.MpRecord.php';
	require_once 'class.Status.php';	
	class MailTemplate extends MpRecord
	{	
		public function __toString()
		{
			return 'Mail Template';
		}
		public function Initialize()
		{
			$this->db_table = 'EmailTemplate';
			$this->recordset->GetFieldDef('ID')->auto_key = TRUE;
			$this->recordset->GetFieldDef('CompanyID')->required = TRUE;
			$this->recordset->GetFieldDef('PersonID')->required = TRUE;
			parent::Initialize();
		}
		public function SaveTemplate($company_id,$person_id,$template)
		{
				$sql = "SELECT * FROM EmailTemplate WHERE CompanyID =? and PersonID=?";
				$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT,$company_id),array(DTYPE_INT,$person_id));
				$rs = DbConnManager::GetDb('mpower')->Exec($sql);																	
				if(count($rs)>0){								
					$objTemplate = DbConnManager::GetDb('mpower')->Execute($sql,'MailTemplate');									
					$objTemplate[0]->Initialize();
					$objTemplate[0]->CompanyID=$company_id;
					$objTemplate[0]->PersonID=$person_id;
					$objTemplate[0]->TemplateData=$template;
					$objTemplate[0]->Save();																		
				}
				else{				
					$objTemplate = DbConnManager::GetDb('mpower')->Execute($sql);				
					$this->recordset = $objTemplate;								
					$this->SetDatabase(DbConnManager::GetDb('mpower'));	
					$this->Initialize();	
					$this->CompanyID=$company_id;
					$this->PersonID=$person_id;
					$this->TemplateData=$template;
					$this->Save();									
				}				
		}
		public function GetTemplate($person_id)
		{
				$sql = "SELECT * FROM EmailTemplate WHERE PersonID=?";
				$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT,$person_id));
				$rs = DbConnManager::GetDb('mpower')->Exec($sql);																	
				if(count($rs)>0)
					return $rs[0]['TemplateData'];
				else
					return '';
				
		
		}
	}

?>