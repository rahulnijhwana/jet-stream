<?php
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';
require_once BASE_PATH . '/slipstream/class.Note.php';
require_once BASE_PATH . '/slipstream/class.Contact.php';
require_once BASE_PATH . '/slipstream/class.Account.php';
require_once BASE_PATH . '/slipstream/class.Event.php';
require_once BASE_PATH . '/slipstream/class.Opportunity.php';
require_once BASE_PATH . '/include/class.UserUtil.php';
require_once BASE_PATH . '/include/class.DateTimeUtil.php';


class ModuleNotes extends JetstreamModule {
	
	public $title = 'Company Notes';
	protected $javascript_files = array('jquery.js', 'note.js', 'ui.datepicker.js', 'jquery.scrollTo.js');
	protected $css_files = array('note.css', 'ui.datepicker.css');
	protected $print_css_files = array('note_print.css');
	protected $buttons = array();
	public $template_files = array('module_notes.tpl', 'form_add_note.tpl');
	public $sidebar = FALSE;
	public $default_private;
	public $has_template = 0;
	public $note_template_id = -1;
	public $note_template_fields = array();
	public $count_note_fields = 0;
	public $id = 'notes';
	
	// These variables will get initialized from the URL through Init()
	// but can be initialized manually before calling Draw()
	public $action = FALSE;
	public $rec_id = 0;
	public $rec_inactive = UNKNOWN;
	public $rec_type;
	public $print = FALSE;
	public $user_id;
	public $company_id;
	public $empty_module = FALSE;
	public $is_restricted;
	public $users = array();
	
	private $dateTimeUtil;
	public $hourSelect;
	public $minuteSelect;
	public $periodSelect;

	public function Init($rec_type='', $rec_id='') {
			
		$canAssign = false;
		
		$this->dateTimeUtil = new DateTimeUtil();
		$this->hourSelect = $this->dateTimeUtil->buildHourSelect();
		$this->minuteSelect = $this->dateTimeUtil->buildMinuteSelect();
		$this->periodSelect = $this->dateTimeUtil->buildPeriodSelect();
		
		if (isset($_GET['action'])) {
			
			$this->action = trim($_GET['action']);

			switch ($this->action) {
				case 'company' :
					if (isset($_GET['accountId']) && ($_GET['accountId'] > 0)) {
						$this->rec_id = $_GET['accountId'];
						$this->rec_type = NOTETYPE_COMPANY;
						$canAssign = true;
					}
					break;
				case 'contact' :
					if (isset($_GET['contactId']) && ($_GET['contactId'] > 0)) {
						$this->rec_id = $_GET['contactId'];
						$this->rec_type = NOTETYPE_CONTACT;
						$canAssign = true;
					}
					break;
				case 'task' :
				case 'event' :
					if (isset($_GET['eventId'])) {
						$this->rec_id = filter_input(INPUT_GET, 'eventId');
						$this->rec_type = ($this->action == 'event') ? NOTETYPE_EVENT : NOTETYPE_TASK;
					}
					if (isset($_GET['rep'])) {
						// Ref is passed when they select opening an instance of a recurring
						$repeating_instance = json_decode(base64_decode(filter_input(INPUT_GET, 'rep')), true);
						$this->rec_id = $repeating_instance['i'];
					}
					break;
				case 'opportunity' :
					if (isset($_GET['opportunityId'])) {
						$this->rec_id = filter_input(INPUT_GET, 'opportunityId');
						$this->rec_type = NOTETYPE_OPPORTUNITY;
					}
					break;	
				default :
					
			}
		}
		elseif(($rec_type != '') && ($rec_id != '')){
			
			# These args are used when this module is built with an ajax call
			# to reload the page. The module is only partially initialized
			
			$this->rec_type = $rec_type;
			$this->rec_id = $rec_id;
		}
		

		if (isset($_GET['Print']) && $_GET['Print'] == 1) {
			$this->action = trim($_GET['action']);
			$this->print = TRUE;
		}
		$this->user_id = $_SESSION['login_id'];
		$this->company_id = $_SESSION['company_obj']['CompanyID'];
		
		//$this->users = UserUtil::getUsers($this->company_id, false);
		$this->users = UserUtil::getChainOfCommand($this->company_id, $this->user_id);

		# Accounts and Contacts can be assigned
		if($canAssign){
			
			$assigned = UserUtil::getAssignedUsers($this->rec_id, $this->action);
			$users = array();

			foreach($this->users as $user){
				if(in_array($user['PersonID'], $assigned)){
					$user['AssignedTo'] = true;
				}
				else {
					$user['AssignedTo'] = false;
				}
				$users[] = $user;
			}
			$this->users = $users;
		}
		$this->notesPerPageSelect = $this->getNotesPerPageSelect();
		
		$this->tree = UserUtil::getUserTree($this->company_id, $this->users);
		
	}

	public function Draw() {

		$objContact = new Contact();
		$objAccount = new Account();
		$objEvent = new Event();
		$objOpportunity = new Opportunity();
		
		if (!isset($this->is_restricted) && !$this->empty_module) {
			switch ($this->action) {
				case 'contact' :
					$this->is_restricted = $objContact->IsRestricted($this->rec_id, $this->user_id, $this->company_id);
					break;
				case 'company' :
					$this->is_restricted = $objAccount->IsRestricted($this->rec_id, $this->user_id, $this->company_id);
					break;
				case 'task' :
				case 'event' :					
					$this->is_restricted = $objEvent->IsRestricted($this->rec_id, $this->user_id, $this->company_id);
					break;
				case 'opportunity' :					
					$this->is_restricted = $objOpportunity->IsRestricted($this->rec_id, $this->user_id, $this->company_id);
					break;	
				default :
					$this->is_restricted = FALSE;
			}
		}
		
		if ($this->print) {
			$this->title = 'Search Notes';
			$this->PrintNotes();
			return;
		}
		
		if ($this->action != FALSE) {
			$form_type = -1;
			switch ($this->action) {
				case 'account' :
				case 'company' :
					$objAccount = new Account();
					$note_type = NOTETYPE_COMPANY;
					if ($this->rec_id > 0 && $this->rec_inactive == UNKNOWN) {
						$this->rec_inactive = $objAccount->IsInactiveAccount($this->rec_id);
					}
					$empty_error = 'No notes for this company.';
					$this->title = 'Company Notes';
					if ($this->is_restricted) $empty_error = 'You are not allowed to view notes';
					$form_type = 1;
					break;
				case 'contact' :
					$note_type = NOTETYPE_CONTACT;
					if ($this->rec_id > 0 && $this->rec_inactive == UNKNOWN) {
						$this->rec_inactive = $objContact->IsInactiveContact($this->rec_id);
					}
					$empty_error = 'No notes for this contact.';
					$this->title = 'Contact Notes';
					if ($this->is_restricted) $empty_error = 'You are not allowed to view notes';
					$formType = 2;
					break;
				case 'task' :
				case 'event' :
					$note_type = ($this->action == 'event') ? NOTETYPE_EVENT : NOTETYPE_TASK;
					if ($this->rec_id > 0) {
						$event_info = $objEvent->GetEventById($this->rec_id);
						if ($event_info[0]['Cancelled']) {
							$this->rec_inactive = TRUE;
						} else {
							$contactId = $event_info[0]['ContactID'];
							$this->rec_inactive = $objContact->IsInactiveContact($contactId);
						}
					}
					$empty_error = 'No notes for this event.';
					$this->title = ($this->action == 'event') ? 'Event Notes' : 'Task Notes';
					if ($this->is_restricted) $empty_error = 'You are not allowed to view notes';
					$formType = 3;					
					break;
				case 'opportunity' :
					$note_type = NOTETYPE_OPPORTUNITY;
					if ($this->rec_id > 0) {
						$opportunity_info = $objOpportunity->GetOpportunityById($this->rec_id);
						//if ($opportunity_info[0]['Cancelled']) {
						//	$this->rec_inactive = TRUE;
						//} else {
							$contactId = $opportunity_info[0]['ContactID'];
							$this->rec_inactive = $objContact->IsInactiveContact($contactId);
						//}
					}
					$empty_error = 'No notes for this opportunity.';
					$this->title = ($this->action == 'opportunity') ? 'Opportunity Notes' : 'Task Notes';
					if ($this->is_restricted) $empty_error = 'You are not allowed to view notes';
					$formType = 3;					
					break;	
			}
		}
		if (!isset($this->rec_id) || $this->rec_id < 0) {
			$this->no_display = TRUE;
			return;
		}
		
		
		$this->has_template = 0;		
		
		$get_date = (!empty($_GET['date'])) ? $_GET['date'] : false;
		if (($this->empty_module || !$this->is_restricted) && (!$get_date)) {
		
			if (!$this->rec_inactive) $this->AddButton('add24.png', "#top\" onclick=\"javascript:addNewNoteForm($note_type, $this->rec_id, 0);", 'Add Note');
			
			$this->AddButton('search24.png', 'slipstream.php?action=notes&referer=' . $this->action . '&load=first', 'Search Notes');
			
			// In the case of a newly added contact/company, we can assume that the notes 
			// are empty so we can skip the DB request
			if ($this->empty_module) {
				$this->no_records = $empty_error;
				return;
			}
			
			$obj_note = new Note();

			$obj_note->noteObjectType = $note_type;
			$obj_note->noteObject = $this->rec_id;
			
			if (isset($_POST['load']) && (strtolower(trim($_POST['load'])) == 'first')) {
				$obj_note->loadFirst = TRUE;
			}
			$note_list = $obj_note->getNoteList();
			
			if ($obj_note->CheckPrivateType($note_type, $this->rec_id)) {
				$this->default_private = 1;
			} else {
				$this->default_private = 0;
			}
			
			if (!isset($note_list['STATUS'])) {
				if (is_array($note_list) && count($note_list) > 0) {
					$this->note_list = $note_list;
					$this->page_links = $obj_note->getPages();
					$this->page_info = $obj_note->getPageInfo();
					$this->no_records = '';
				} else {
					$this->no_records = $empty_error;
				}
			}
		} else {
			$this->no_records = $empty_error;
		}
	}

	public function PrintNotes() {
		$obj_note = new Note();
		
		if (trim($_REQUEST['Keyword']) != '') {
			$obj_note->searchString = $_REQUEST['Keyword'];
		}
		
		if (isset($_REQUEST['filterOpt'])) {
			$filter_fields = $_REQUEST['filterOpt'];
		} else {
			$filter_fields = array();
		}
		$filter_array = array();
		
		if (count($filter_fields) > 0) {
			foreach ($filter_fields as $filter_key => $filter_val) {
				$new_filter_key = $filter_key + 1;
				
				if (($filter_val != '') && ($filter_val != 'D0') && ($filter_val != 'D1')) {
					if (isset($_REQUEST['NText' . $new_filter_key]) && (trim($_REQUEST['NText' . $new_filter_key]) != '')) {
						$filter_array[$filter_val] = $_REQUEST['NText' . $new_filter_key];
					}
				} else if ($filter_val == 'D0') {
					$filter_array['D0_SalesPerson'] = $_REQUEST['NText' . $new_filter_key];
				} else if ($filter_val == 'D1') {
					$filter_array['D1_StartDate'] = $_REQUEST['StartDate' . $new_filter_key];
					$filter_array['D1_EndDate'] = $_REQUEST['EndDate' . $new_filter_key];
				}
			}
		}
		
		$obj_note->notePrint = TRUE;
		$obj_note->sortType = $_REQUEST['SortType'];
		$obj_note->sortAs = $_REQUEST['SortAs'];
		$obj_note->noteFilterArray = $filter_array;
		$note_list = $obj_note->searchNoteList();
		
		if (isset($note_list['result']) && ($note_list['result'] == 'FALSE')) {
			$this->no_records = $note_list['error'];
		} else {
			if (is_array($note_list) && (count($note_list) > 0)) {
				$this->note_list = $note_list;
				$this->no_records = '';
			} else {
				$this->no_records = 'No notes match the current search term and filters.';
			}
		}
	}
	
	public function getNotesPerPageSelect(){
		
		
		$options = array(5, 10, 25, 50, 75, 100);
		$html = 'Show <select id="notes_per_page" onchange="updateNotesPerPage(this.value';
		
		$html .= ',\'' . $this->rec_type .'\'';
		$html .= ',\'' . $this->rec_id .'\'';
		
		$html .= ');">';
		
		foreach($options as $option){
			$selected = '';
			if($_SESSION['NotesPerPage'] == $option){
				$selected = 'selected="selected"';
			}
			$html .= '<option value="' . $option . '"' . $selected. '>' . $option . '</option>';
		}
		$html .= '</select>';
		
		return $html;
	}
}

