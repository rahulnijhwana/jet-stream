<?php
/**
 * @package slipstream
 */
 
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';


/**
 * The EmailHold class
 */
class EmailHold
{

	function GetEmailHold($personId){
		$email_hold = array();
		$field_map_obj = new fieldsMap();
		$email_list = $field_map_obj->getContactEmailFields();		
		$email_addresses = '';
		$email_addr = array();
		
		if(is_array($email_list) && !empty($email_list)) {
			foreach ($email_list as $key=>$email) {
				$email_addr[] = "EA.EmailAddress = C.$email";
				if ($key == 0) $email_addresses = " AND EA.EmailAddress = C.$email";
				else $email_addresses .= " OR EA.EmailAddress = C.$email";
			}
		}

		$sql = "SELECT DISTINCT EmailAddress, DisplayName, replace(LOWeR(DisplayName),' ','') as sortby FROM EmailAddresses WHERE (
		    (
				EmailContentID IN (
					SELECT EmailContentID FROM EmailAddresses WHERE PersonID = ? AND emailaddresstype IN ('To', 'CC')
				)
				AND EmailAddressType = 'From'
		    ) OR ( 
				EmailContentID IN (
					select EmailContentID from EmailAddresses where PersonID = ? and emailaddresstype = 'From'
				)
				AND EmailAddressType IN ('To', 'CC')
		    )
		)
		AND ContactID = 0
		AND (PersonID = 0 OR PersonID IS NULL)
		AND EmailAddress NOT IN (SELECT EmailAddress FROM IgnoreEmailList WHERE PersonID = ?)
		ORDER BY sortby ASC";
				
		
		
		
/*
		$sql = "SELECT A.EmailAddress, A.DisplayName FROM 
					(
						Select EC.EmailContentID, EA.EmailAddress, EA.DisplayName, EA.EmailAddressType, EC.PersonID from EmailContent EC, EmailAddresses EA 
						LEFT JOIN EmailAddresses EAL ON EAL.EmailContentID = EA.EmailContentID AND EAL.EmailAddressType = 'From' AND EAL.ContactID = 0
						LEFT JOIN Contact C ON C.CompanyID = ? $email_addresses
						WHERE EC.PersonID = ? AND EC.EmailContentID = EA.EmailContentID AND EA.PersonID = 0 AND EA.ContactID = 0 
						AND EAL.EmailAddressID IS NOT NULL AND C.ContactID IS NULL 
					) A 
				LEFT JOIN IgnoreEmailList I ON I.PersonID = A.PersonID AND I.EmailAddress = A.EmailAddress
				WHERE I.EmailAddress IS NULL GROUP BY A.EmailAddress, A.DisplayName";
*/				
				
		/*$sql = 'SELECT E.EmailAddress, E.DisplayName FROM (SELECT EA.EmailContentID FROM EmailAddresses EA WHERE EA.PersonID = ?) EC 
				LEFT JOIN EmailAddresses E ON EC.EmailContentID = E.EmailContentID 
				LEFT JOIN IgnoreEmailList IE ON IE.PersonID = ? AND E.EmailAddress = IE.EmailAddress
				WHERE E.PersonID = 0 AND E.ContactID = 0 AND IE.PersonID IS NULL';		*/
				
		$sqlBuild = SqlBuilder()->LoadSql($sql);	
//		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $personId), array(DTYPE_INT, $personId));		
		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $personId), array(DTYPE_INT, $personId), array(DTYPE_INT, $personId));		

		$result = DbConnManager::GetDb('mpower')->Exec($sql);		
		
//		echo 'count: ' . count($result). '<br >';
//		echo print_r ($result);
		
		if (isset($result) && (count($result) > 0)) {
			foreach ($result as $key=>$row) {
				$email_hold[$key]['EmailAddress'] = trim($row['EmailAddress']);
				$display_name = trim($row['DisplayName']);		
				$display_name = (!empty($display_name)) ? $display_name : trim($row['EmailAddress']);			
				$first_name = $last_name = '';			
				
				if (strpos($display_name, '@')) {
					$list = split('@', $display_name, 2);
					$display_name = $list[0]; 
				}
							
//				$display_name = preg_replace('/\d/', '', $display_name);				
				$display_name = str_replace(array('_', '.', '\''), ' ', $display_name);
				$display_name = trim($display_name);
				
				if (str_word_count($display_name) > 1) {
					preg_match('/[^ ]*$/', $display_name, $results);
					$last_name = count($result > 0) ? $results[0] : '';
				} 
				
				$first_name = trim(str_replace($last_name, '', $display_name));				
				$email_hold[$key]['DisplayName'] = ucwords(strtolower($first_name . ' ' . $last_name));
				$email_hold[$key]['FirstName'] = $first_name;
				$email_hold[$key]['LastName'] = $last_name;
			}
		}
		return $email_hold;
	}
}
