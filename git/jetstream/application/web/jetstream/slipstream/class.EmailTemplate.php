<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/slipstream/class.MailTemplate.php';
require_once BASE_PATH . '/slipstream/class.CompanyContact.php';

class EmailTemplate
{
	public $token;
	private $token_key=array();
	public $info=array();
	public $body;
	function __construct() {
		$this->token_key=array('Type'=>NULL,'Target'=>NULL,'Id'=>NULL,'CompanyID'=>NULL);
		$this->body='';
	}
	function DecodeToken()	{		
		if(trim($this->token)=='')
			return false;
		else
		{
			$str=base64_decode($this->token);			
			$str=explode('&',$str);

			$this->token_key['Type']=$str[0];
			$this->token_key['Target']=$str[1];
			$this->token_key['Id']=$str[2];			
			$this->token_key['CompanyID']=$str[3];
		}		
		return true;
	}
	function Init() {
		if(!$this->DecodeToken())		
			$this->info=false;		
		else {		
			$objMail=new MailTemplate();
			$this->body=$objMail->GetTemplate($this->token_key['Target']);			
			$this->GenerateTemplate();
		}
			
	}
	function GenerateTemplate() {		
		if($this->token_key['Type']=='contact')
		{			
			$contact_sql = 'SELECT * FROM Contact WHERE ContactID = ?';
			$contact_sql = SqlBuilder()->LoadSql($contact_sql)->BuildSql(array(DTYPE_INT, $this->token_key['Id']));			
			$contact = DbConnManager::GetDb('mpower')->Exec($contact_sql);
			
			$contact_map = "select * from ContactMap where CompanyID=? and Enable=1";
			$contact_map = SqlBuilder()->LoadSql($contact_map)->BuildSql(array(DTYPE_INT, $this->token_key['CompanyID']));			
			$field_map = DbConnManager::GetDb('mpower')->Exec($contact_map);			

			if(count($contact)>0 && count($field_map)>0)
			{
				foreach($contact[0] as $key=>$c)
				{
					$search="{{$key}}";					
					$formatted_val=$this->GetFieldValue($key,$c,$field_map);
					$replace="{$formatted_val}";
					$this->body=str_replace($search,$replace,$this->body);
				}
			}
		}
	}
	function GetFieldValue($field_name,$field_value,$field_map)
	{
		$objCC=new CompanyContact();
		$output='';
		
	foreach($field_map as $f) {
			if($f['FieldName']==$field_name) {
				switch (substr($field_name, 0, 4)) {
					case 'Numb' :
						setlocale(LC_MONETARY, 'en_US');
						if($f['Parameters'] != null && $f['FieldName'] != ''){
							$paramArr = explode("|",$f['Parameters']);
							if($paramArr[3] == 'Currency')
								$output = money_format('%+n', $field_value);
							else if($paramArr[3] == 'Percent')
								$output = $field_value .' % ' ;
							else
								$output = $field_value ;
						}else{
							$output = $field_value ;
						}
						break;
					case 'Sele' :
						$output = $objCC->GetOptionVal($field_value);
						break;
					case 'Bool' :
						$output = ($field_value == 1) ? '<img src="images/checkbox_checked.png">' : '<img src="images/checkbox_unchecked.png">';
						break;
					case 'Date' :
						$output = ReformatDate($field_value);
						break;
					case 'Text' :
						$val_type = (isset($f['ValidationType']) && !empty($f['ValidationType'])) ? $f['ValidationType'] : 0;
						switch ($val_type) {
							case 4 : // URL
								$output = "<a href=\"{$field_value}\" target=\"_blank\">{$field_value}</a>";
								break;
							case 5 : // Email
								$output = "<a href=\"mailto:{$field_value}\">{$field_value}</a>";
								break;
							default :
								$output = $field_value;
						}
						break;
					default:								
						$output=$field_value;

				}
			}
		}		
		return $output;	
	}	
	
}
?>