<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.ReportingTree.php';
require_once 'class.Contact.php';
require_once 'class.Account.php';


class CompanyContactNew {

	public $company_id;
	public $user_id;
	public $limit_company_access;

	function __construct($company_id = -1, $user_id = -1, $limit_company_access = -1) {
		$this->company_id = ($company_id == -1 && isset($_SESSION['company_obj']['CompanyID'])) ? $_SESSION['company_obj']['CompanyID'] : $company_id;
		$this->limit_company_access = ($limit_company_access == -1) ? (isset($_SESSION['company_obj']['LimitAccessToUnassignedUsers']) && ($_SESSION['company_obj']['LimitAccessToUnassignedUsers'])) : $limit_company_access;
		$this->user_id = ($user_id == -1 && isset($_SESSION['USER']['USERID'])) ? $_SESSION['USER']['USERID'] : $user_id;
	}

	/*
	 * @param $fields is columns list of Account table to lookup
	 */
	public function GetRecord($id, $map, $fields, $account_id_prepop = FALSE) {

		/*
		echo '<pre>'; print_r($id); echo '</pre>';
		echo '<pre>'; print_r($map); echo '</pre>';
		echo '<pre>'; print_r($fields); echo '</pre>';
		echo '<pre>'; print_r($account_id_prepop); echo '</pre>'; exit;
		*/
			

		// Load $fields with all the fields necessary to populate the form
		$contact = ($map['type'] == 'contact') ? TRUE : FALSE;
		
		if ($account_id_prepop) {
			// We need to get account fields to pre-populate a contact record
			// so we need to get all the fields that have an AccountMapID != NULL,
			// then get the related field names from the account map
			$account_map = MapLookup::GetAccountMap($this->company_id);
	
			foreach ($map['fields'] as $field) {
				if (!empty($field['AccountMapID'])) {
					if(isset($account_map['fields'][$field['AccountMapID']]['FieldName']) && trim($account_map['fields'][$field['AccountMapID']]['FieldName']) != '') {					
						$fields[] = $account_map['fields'][$field['AccountMapID']]['FieldName'] . ' as ' . $field['FieldName'];
					}
				}
			}
		} else {
			foreach ($map['fields'] as $field) {
				$fields[] = $field['FieldName'];
			}
		}

		// By including the companyid as criteria, we will cause a soft record not found error
		// if the user attempts to access records outside their company
		if ($contact && !$account_id_prepop) {
			$record_sql = "SELECT " . implode(", ", $fields) . " FROM Contact WHERE ContactID = ? AND Deleted != 1 AND CompanyID = ?";
			$class_type = 'Contact';
		} else {
			$record_sql = "SELECT " . implode(", ", $fields) . " FROM Account WHERE AccountID = ? AND Deleted != 1 AND CompanyID = ?";
			$class_type = 'Account';
		}
		$record_sql = SqlBuilder()->LoadSql($record_sql)->BuildSql(array(DTYPE_INT, array($id, $this->company_id)));

		$recordset = DbConnManager::GetDb('mpower')->Execute($record_sql, $class_type);
		$recordset->Initialize();

		return $recordset;
	}

	// Returns the map from MapLookup populated with this record's data
	// $type = account or contact
	public function PopulateMapValues($id, $map, $account_id_prepop = FALSE) {
		if ($map['type'] == 'contact' && !$account_id_prepop) {
			$contact = TRUE;
			$fields = array('ContactID', 'AccountID', 'PrivateContact', 'Inactive'
				, '(SELECT ' . MapLookup::GetAccountNameField($this->company_id, TRUE) . ' FROM Account WHERE AccountID = Contact.AccountID) as AccountName'
				, '(SELECT COUNT(*) FROM opportunities WHERE ContactID = contact.ContactID)
				+ (SELECT COUNT(*) FROM Notes WHERE ObjectType = 2 AND ObjectReferer = contact.ContactID and PrivateNote = 0)
				+ (SELECT COUNT(*) FROM Event WHERE ContactID = Contact.ContactID AND (Private = 0 OR Private IS NULL)) AS PublicItems');

			$private_field = 'PrivateContact';
		} elseif ($map['type'] == 'account' || $account_id_prepop) {
			$accountName = MapLookup::GetAccountNameField($this->company_id, TRUE);
			$contact = FALSE;
			$fields = array('AccountID', 'PrivateAccount', 'Inactive', $accountName . ' as AccountName');
			$private_field = 'PrivateAccount';

			if ($map['type'] == 'contact' && $account_id_prepop) {
				$contact = TRUE;
			} else {
				// Make sure it is false if it was accidently set to true for an account rec
				$account_id_prepop = FALSE;
			}
		} else {
			return FALSE;
		}

		$recordset = $this->GetRecord($id, $map, $fields, $account_id_prepop);
		if ($recordset->count() == 0) {
			include ('page_error.php');
			$error_detail = ucfirst($map['type']) . " $id";
			$error_text = "Jetstream cannot find the record you requested.  It either does not exist, or has been permanently deleted.";
			JetstreamErrorPage('Record not found', $error_detail, $error_text);
		}
		$record = $recordset[0];

		// $record->Debug();

		// Insert the record field as 'value' in the map
		foreach ($map['fields'] as &$field) {
			$field['value'] = $record->$field['FieldName'];
		}

		if ($account_id_prepop) {
			$map['account']['name'] = $record->AccountName;
			$map['account']['id'] = $id = $record->AccountID;
			$assigned_sql = "SELECT PersonID FROM PeopleAccount WHERE AccountID = ? AND AssignedTo = 1";
		} elseif ($contact) {
			$map['account']['name'] = $record->AccountName;
			$map['account']['id'] = $record->AccountID;
			$security['ContactID'] = $record->ContactID;
			$security['PublicItems'] = $record->PublicItems;
			$assigned_sql = "SELECT PersonID FROM PeopleContact WHERE ContactID = ? AND AssignedTo = 1";
		} else {
			$security['AccountID'] = $record->AccountID;
			$assigned_sql = "SELECT PersonID FROM PeopleAccount WHERE AccountID = ? AND AssignedTo = 1";
		}

		$security['Private'] = ($record->$private_field == 1) ? TRUE : FALSE;
		$security['Inactive'] = ($record->Inactive == 1) ? TRUE : FALSE;

		// Load all the peoplecontact recs into $security
		$assigned_sql = SqlBuilder()->LoadSql($assigned_sql)->BuildSql(array(DTYPE_INT, $id));
		$assigned = DbConnManager::GetDb('mpower')->Execute($assigned_sql);
		$security['is_mine'] = FALSE;
		foreach ($assigned as $person) {
			$security['assigned'][$person->PersonID] = ReportingTreeLookup::GetRecord($this->company_id, $person->PersonID);
			if ($person->PersonID == $this->user_id) {
				$security['is_mine'] = TRUE;
			}
		}
		$map['security'] = $security;

		// echo '<pre>' . print_r($map, TRUE) . '</pre>';
		// exit;
		return $map;
	}
}
