<?php
require_once BASE_PATH . '/slipstream/class.JetstreamModule.php';

/**
 * Class for Data Import module
 */
class ModuleDataImport extends JetstreamModule
{
	public $title = 'Import';
	public $sidebar = false;
	protected $css_files = array('import.css');
	
	
	public function Init() {		
		/**
		 * include tpl file for HTML section.
		 */

		$this->AddTemplate('module_data_upload.tpl');
	}
	
}