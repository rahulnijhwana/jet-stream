<?php
require_once BASE_PATH . '/slipstream/class.JetstreamPage.php';
require_once BASE_PATH . '/slipstream/class.ModuleCalendar.php';
require_once BASE_PATH . '/slipstream/class.ModuleMiniCalendar.php';
require_once BASE_PATH . '/slipstream/class.ModuleUpcomingEvents.php';
require_once BASE_PATH . '/slipstream/class.ModuleAdvanceSearchSort.php';
require_once BASE_PATH . '/slipstream/class.ModuleAdvanceSearch.php';
require_once BASE_PATH . '/slipstream/class.ModuleAdvanceSearchFilter.php';
require_once BASE_PATH . '/slipstream/class.ModuleSavedSearch.php';
require_once BASE_PATH . '/include/lib.string.php';

$page = new JetstreamPage();

// echo '<pre>' . print_r($_SESSION, TRUE) . '</pre>';


// Use a generic module because the dashboard search is such a simple page
$module = new Dashboard();
$module->title = 'Advace Search Jetstream Records';
$module->AddMenuItem('Basic', "javascript:showSearch('basic');",'');
$module->AddMenuItem('Advanced', "javascript:showSearch('advance');",'selected');
$module->AddMenuItem('Super Advanced', "javascript:showSearch('superadvance');");

// $module->AddButton('Save', "javascript:saveDashboardSearch();", 'Save');
$module->AddTemplate('module_advance_search.tpl');
$module->AddTemplate('snippet_merge_record.tpl');
$module->AddJavascript('jquery.js', 'slipstream_search.js', 'jquery.form.js');
$module->AddCss('search.css');
$module->search_types = array(3 => 'Company & Contact', 1 => 'Company', 2 => 'Contact');
$module->records_per_page = array(25, 50, 75, 100);
/* Set the search parameters from cookie. */

if(isset($_SESSION['LastSearch'])) {
	$module->Build($_SESSION['LastSearch']);
}


$page->AddModule($module);

$page->AddModule(new ModuleCalendar());

$page->AddModule(new ModuleAdvanceSearchSort($_SESSION['LastSearch']['sort_type'], $_SESSION['LastSearch']['sort_dir']));

$advanced_search = new ModuleAdvanceSearchFilter();
$advanced_search->presets = $_SESSION['LastSearch']['advanced_filters'];
$page->AddModule($advanced_search);

// $page->AddModule(new ModuleSavedSearch(1));

$page->AddModule(new ModuleMiniCalendar());

$page->AddModule(new ModuleUpcomingEvents());

$tasks = new ModuleUpcomingEvents();
$tasks->type = 'tasks';
$page->AddModule($tasks);

$page->Render();
