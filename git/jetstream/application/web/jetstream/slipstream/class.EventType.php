<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.MpRecord.php';
require_once BASE_PATH . '/slipstream/class.Status.php';
	
class EventType extends MpRecord
{
	public function __toString() 
	{
		return 'EventType Object';
	}	
	public function Initialize() 
	{
		$this->db_table = 'EventType';
		$this->recordset->GetFieldDef('EventTypeID')->auto_key = TRUE;
		$this->recordset->GetFieldDef('CompanyID')->required = TRUE; 
		parent::Initialize();
	}   	
	function getAllEventTypeByCompanyId($companyId)	
	{
		$sql='select * from EventType where CompanyID=? and (Deleted is NULL or Deleted=0)';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT,$companyId));		
		$rs = DbConnManager::GetDb('mpower')->Exec($sql);	
		return $rs;
	}
	function isUniqueEventType($eventType,$companyId,$excludeEventId)
	{
		if(trim($excludeEventId)=='')
		{
			$sql='select * from EventType where CompanyID=? and (Deleted is NULL or Deleted=0) and EventName=?';
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT,$companyId),array(DTYPE_STRING,$eventType));
		}
		else
		{
			$sql='select * from EventType where CompanyID=? and (Deleted is NULL or Deleted=0) and EventName=? and EventTypeID!=?';
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT,$companyId),array(DTYPE_STRING,$eventType),array(DTYPE_INT,$excludeEventId));		
		}				
		$rs = DbConnManager::GetDb('mpower')->Exec($sql);					
		if(empty($rs))
			return true;
		else
			return false;
		
	}
	function isUniqueEventColor($eventColor,$companyId,$excludeEventId)
	{
		if(trim($excludeEventId)=='')
		{
			$sql='select * from EventType where CompanyID=? and (Deleted is NULL or Deleted=0) and EventColor=?';
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT,$companyId),array(DTYPE_STRING,$eventColor));
		}
		else
		{
			$sql='select * from EventType where CompanyID=? and (Deleted is NULL or Deleted=0) and EventColor=? and EventTypeID!=?';
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT,$companyId),array(DTYPE_STRING,$eventColor),array(DTYPE_INT,$excludeEventId));		
		}		
		$rs = DbConnManager::GetDb('mpower')->Exec($sql);					
		if(empty($rs))
			return true;
		else
			return false;
		
	}
	function saveEvent($eventTypeId,$eventTypeName,$eventColor,$eventType,$eventUsedInReport,$companyId)
	{
		$status=new Status;
		$objEventType=new EventType;
		if($eventTypeId=='')
			$eventTypeId=-1;
			
		$sql = "SELECT * FROM EventType WHERE EventTypeID = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT,$eventTypeId));
		if($eventTypeId==-1)
		{
			$rs = DbConnManager::GetDb('mpower')->Execute($sql);												
			$objEventType->recordset = $rs;
			$objEventType->SetDatabase(DbConnManager::GetDb('mpower'));
		}
		else
		{
			
			$rs = DbConnManager::GetDb('mpower') -> Execute($sql, 'EventType');							
			if (count($rs) == 1) 
			{				
				$objEventType = $rs[0];
			}
			else
			{
				$status->addError('Event Type not found');
				return $status;
			}
		}
		$objEventType->Initialize();
		$objEventType->CompanyID=$companyId;
		$objEventType->EventName=$eventTypeName;
		$objEventType->EventColor=$eventColor;
		$objEventType->Type=$eventType;
		$objEventType->Deleted=0;
		$objEventType->UseEventInCallReport = $eventUsedInReport;
		$objEventType->Save();
		return $status;
	}
	function getEventTypeInfoByEventTypeId($eventTypeId,$companyId)	
	{
		$sql='select * from EventType where CompanyID=? and (Deleted is NULL or Deleted=0) and EventTypeID=?';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT,$companyId),array(DTYPE_INT,$eventTypeId));		
		$rs = DbConnManager::GetDb('mpower')->Exec($sql);					
		return $rs;
	}	
	function deleteEventType($eventTypeId,$companyId)
	{		
		$sql='update EventType set Deleted=1 where CompanyID=? and EventTypeID=?';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT,$companyId),array(DTYPE_INT,$eventTypeId));		
		$rs = DbConnManager::GetDb('mpower')->Exec($sql);								
	}
}
?>
