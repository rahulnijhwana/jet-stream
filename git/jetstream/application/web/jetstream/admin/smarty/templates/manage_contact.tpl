{literal}
<script>
var options={/literal}{$OPTIONSETS_JSON}{literal};
try
{
	options=eval(options);
}
catch(e)
{
	options=JSON.decode(options);
}
</script>
{/literal}
<div id="editOptionListDiv" style="background:#FFFFFF;display:none;height:500px;" >
<form method="post" action="./ajax/ajax.manageCompany.php" id="frmEditOptions" name="frmEditOptions">
	<table width="400px">
		<tr>
			<td height='5px'><span id="editOptionError" class='clsError'></span></td>
		</tr>
		<tr>
			<td>
				<span class="smlBlackBold">Options</span>
				<div id="addOptionElementsDiv"></div>
				<div id="addOptionElementsLeftDiv"></div>
			</td>
		</tr>
		<tr height="100px">
			<td><input type="button" class="clsButton" value="Cancel" onclick="javascript:cancelEditOptionList();"/></td>&nbsp;&nbsp;&nbsp;&nbsp;
			<td><input type="button" class="clsButton" value="Add Options" onclick="javascript:addEditOptionList();"/></td>&nbsp;&nbsp;&nbsp;&nbsp;		
			<td><input type="button" class="clsButton" value="Save" onclick="javascript: saveEditOptions();"/></td>
		</tr>		
	</table>
	</form>
</div>

<div id="optionListDiv" style="background:#FFFFFF;display:none;height:500px;" >
<form method="get" action="./ajax/ajax.user.php" id="frmAddOptions" name="frmAddOptions">
	<table width="400px">
		<tr>
			<td class="BlackBold">Label Name</td>
			<td><input type="text" name="optionSetName" id="optionSetName" value=""/></td>			
		</tr>
		<tr>
			<td class="BlackBold">Available Option set</td>
			<td><select id='optionSetSelect' class='clsTextBox'   onChange="javascript: populateOptions();">
					<option value="-1"> </option>
					{section name=options loop=$OPTIONSETS}
					<option value='{$smarty.section.options.index}'>{$OPTIONSETS[options].OptionSetName}</option>
					{/section}
				</select>
			</td>			
		</tr>
		<tr>
			<td class="BlackBold">Options</td>
		</tr>
		<tr>
			<td id='optionSetvalues'></td>
			<input type="hidden" name="optionSetId" id="optionSetId" value="">			
		</tr>
		<tr>
			<td height='5px'><span id="optionListError" class='clsError'></span></td>
		</tr>
		<tr height="100px">
			<td><input type="button" class="clsButton" value="Cancel" onclick="javascript:cancelOption();"/></td>&nbsp;&nbsp;&nbsp;&nbsp;
			<td><input type="button" class="clsButton" value="Add Options" onclick="javascript:addOptionList('Contact');"/></td>&nbsp;&nbsp;&nbsp;&nbsp;		
			<td><input type="button" class="clsButton" value="Save" onclick="javascript: saveOptionList('contactDiv');"/></td>
		</tr>		
	</table>
	</form>
</div>
<div id='divContactLayout'>
	<form method="get" action="./ajax/ajax.user.php" id="frmManageContact" name="frmManageContact">
	<input type='hidden' id='companyId' value='{$COMPANYID}' name='companyId'  />
	<input type="hidden" value="0" id="divValue" />
	<input type="hidden" value="0" id="childValue" />
	<table  cellpadding='0' cellspacing='0' id='tblContactLayout' class='formMainTable'>
		<tr>
			<td width='100%' colspan='2'>
				<table border='0' cellspacing='0' cellpadding='0' width="100%">
					<tr>
						<td width="1%" style='background-image:url(images/headerleft.gif);background-repeat: no-repeat;' ></td>	
						<td width="99%"style='background-image:url(images/headerbg.gif);'  align='left' class='tblHeader'><span id='frmHeader'>{$header_title}</span></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width='70%'>
				<div style="float:left;width:100%" id="contactDiv">
					<div style="float:left;width:15%"  id="typeDiv" class='smlBlackBold'>Type</div>
					<div style="float:left;width:15%"  id="labelDiv" class='smlBlackBold'>Label</div>
					<div style="float:left;width:15%" id="optionsDiv" class='smlBlackBold'>Options</div>
					<div style="float:left;width:10%" id="mandatoryDiv" class='smlBlackBold'>Mandatory</div>
					<div style="float:left;width:9%" id="companyDiv" class='smlBlackBold'><span id="spnCompany">IsFirstName</span></div>
					<div style="float:left; width:9%;" id="lastNameDiv" class='smlBlackBold'>IsLastName</div>
					<div style="float:left; width:8%;" id="emailDiv" class='smlBlackBold'>IsEmail</div>					
					<div style="float:left;" id="editDiv" class='smlBlackBold'>Edit</div>
				</div>
				
			</td>
			<td width='20%' align='center' valign='top' style="padding-top:10px;">
				<table>
					<tr>
						<td>
							<div style='border: 2px solid rgb(181, 226, 254);vertical-align:top; width:250px; height:100px;' >
							
							<div class='smlBlackBold' style="float:left;width:120px;" >Select Field Type</div>
							<div ><select id='fieldType' name='fieldType' class='clsTextBox' onChange="javascript: checkContactFieldType();">
									<option value="-1"> </option>
									<option value="Text">Text</option>
									<option value="Number">Number</option>
									<option value="Zip">Zip</option>
									<option value="Email">Email</option>
									<option value="Phone">Phone</option>
									<option value="DropDown">DropDown</option>
									<option value="CheckBox">CheckBox</option>
									<option value="RadioButton">RadioButton</option>
									<option value="Date">Date</option>
								</select>
							</div>
							<input type="hidden" id='fieldSelectContact' name='fieldSelectContact' value="">
							<input type="hidden" id='dataTypeSelectContact' name='dataTypeContact' value="">
							<br>
							<br>
							<div align="center"><input type='button' name='btnAddField' id='btnAddField' value='Add'  class='clsButton' onclick="javascript: checkNumberField(document.getElementById('fieldSelectContact').value,'Contact');"/><input type='hidden' name='frmContactType' id='frmContactType' value=''/></div>
						</div>
					</td>
				</tr>
				<tr>
					<td>
					<span id="showContactError" class='clsError'></span>
					<span id="showError" class='clsError'></span>
					</td>
				</tr>
			</table>
					{literal}
						<script type="text/javascript">
							var fieldValue= '';
							var validationValue ='';
							var display='YES';
							var mapId = '';
							var isRequired = '';
							var isFirstName = '';
							var isEmail = '';
							var isLastName = '';
							
					{/literal}
						{section name=fields loop=$CONTACTTEXTFIELDS}
					{literal}	
							var optionIds = new Array();
							var optionValues = new Array();
							fieldValue="{/literal}{$CONTACTTEXTFIELDS[fields].LabelName|escape:html}{literal}";			
							validationValue ='{/literal}{$CONTACTTEXTFIELDS[fields].ValidationName}{literal}';	
							mapId = '{/literal}{$CONTACTTEXTFIELDS[fields].ContactMapID}{literal}';
							isRequired ='{/literal}{$CONTACTTEXTFIELDS[fields].IsRequired}{literal}';
							isFirstName ='{/literal}{$CONTACTTEXTFIELDS[fields].IsFirstName}{literal}';
							isLastName ='{/literal}{$CONTACTTEXTFIELDS[fields].IsLastName}{literal}';
							isEmail ='{/literal}{$CONTACTTEXTFIELDS[fields].IsEmailAddress}{literal}';
							document.getElementById('fieldSelectContact').value = 'TextBox';
							document.getElementById('fieldType').value = '{/literal}{$CONTACTTEXTFIELDS[fields].FieldType}{literal}';
							document.getElementById('dataTypeSelectContact').value = 'Alphabet';
							addField('contactDiv',mapId,display,fieldValue,validationValue,isFirstName,isRequired,isLastName,isEmail,optionIds,optionValues);
					{/literal}					
						{/section}
						{section name=fields loop=$CONTACTNUMBERFIELDS}
					{literal}
							var optionIds = new Array();
							var optionValues = new Array();
							fieldValue="{/literal}{$CONTACTNUMBERFIELDS[fields].LabelName|escape:html}{literal}";
							validationValue ='{/literal}{$CONTACTNUMBERFIELDS[fields].ValidationName}{literal}';				
							mapId = '{/literal}{$CONTACTNUMBERFIELDS[fields].ContactMapID}{literal}';
							isRequired ='{/literal}{$CONTACTNUMBERFIELDS[fields].IsRequired}{literal}';
							isFirstName ='{/literal}{$CONTACTNUMBERFIELDS[fields].IsFirstName}{literal}';
							isLastName ='{/literal}{$CONTACTNUMBERFIELDS[fields].IsLastName}{literal}';
							isEmail ='{/literal}{$CONTACTNUMBERFIELDS[fields].IsEmailAddress}{literal}';
							document.getElementById('fieldSelectContact').value = 'TextBox';
							document.getElementById('fieldType').value = '{/literal}{$CONTACTNUMBERFIELDS[fields].FieldType}{literal}';
							document.getElementById('dataTypeSelectContact').value = 'Numeric';
							addField('contactDiv',mapId,display,fieldValue,validationValue,isFirstName,isRequired,isLastName,isEmail,optionIds,optionValues);
					{/literal}					
						{/section}
						{section name=fields loop=$CONTACTCHECKFIELDS}
					{literal}
							var optionIds = new Array();
							var optionValues = new Array();
							fieldValue="{/literal}{$CONTACTCHECKFIELDS[fields].LabelName|escape:html}{literal}";
							validationValue ='{/literal}{$CONTACTCHECKFIELDS[fields].ValidationName}{literal}';				
							mapId = '{/literal}{$CONTACTCHECKFIELDS[fields].ContactMapID}{literal}';
							isRequired ='{/literal}{$CONTACTCHECKFIELDS[fields].IsRequired}{literal}';
							isFirstName ='{/literal}{$CONTACTCHECKFIELDS[fields].IsFirstName}{literal}';
							isLastName ='{/literal}{$CONTACTCHECKFIELDS[fields].IsLastName}{literal}';
							isEmail ='{/literal}{$CONTACTCHECKFIELDS[fields].IsEmailAddress}{literal}';
							document.getElementById('fieldSelectContact').value = 'CheckBox';
							//checkContactDropDown();
							addField('contactDiv',mapId,display,fieldValue,validationValue,isFirstName,isRequired,isLastName,isEmail,optionIds,optionValues);
					{/literal}				
						{/section}
						{section name=fields loop=$CONTACTDROPDOWNFIELDS}
					{literal}
							var optionIds = new Array();
							var optionValues = new Array();
							fieldValue="{/literal}{$CONTACTDROPDOWNFIELDS[fields].OptionFields.LabelName|escape:html}{literal}";	
							validationValue ='{/literal}{$CONTACTDROPDOWNFIELDS[fields].OptionFields.ValidationName}{literal}';				
							mapId = '{/literal}{$CONTACTDROPDOWNFIELDS[fields].OptionFields.ContactMapID}{literal}';
							isRequired ='{/literal}{$CONTACTDROPDOWNFIELDS[fields].OptionFields.IsRequired}{literal}';
							isFirstName ='{/literal}{$CONTACTDROPDOWNFIELDS[fields].OptionFields.IsFirstName}{literal}';
							isLastName ='{/literal}{$CONTACTDROPDOWNFIELDS[fields].OptionFields.IsLastName}{literal}';
							isEmail ='{/literal}{$CONTACTDROPDOWNFIELDS[fields].OptionFields.IsEmailAddress}{literal}';
					{/literal}
						{section name=option loop=$CONTACTDROPDOWNFIELDS[fields].OptionValues}
					{literal}
							optionValues[{/literal}{$smarty.section.option.index}{literal}] ="{/literal} {$CONTACTDROPDOWNFIELDS[fields].OptionValues[option].OptionName|escape:html}{literal}"
							
							optionIds[{/literal}{$smarty.section.option.index}{literal}] ='{/literal} {$CONTACTDROPDOWNFIELDS[fields].OptionValues[option].OptionID}{literal}';
					{/literal}
						{/section}
					{literal}
							document.getElementById('fieldSelectContact').value = 'DropDown';
							//checkContactDropDown();
							addField('contactDiv',mapId,display,fieldValue,validationValue,isFirstName,isRequired,isLastName,isEmail,optionIds,optionValues);
					{/literal}				
						{/section}
					
						{section name=fields loop=$CONTACTRADIOFIELDS}
					{literal}
							var optionIds = new Array();
							var optionValues = new Array();
							fieldValue="{/literal}{$CONTACTRADIOFIELDS[fields].OptionFields.LabelName|escape:html}{literal}";
							validationValue ='{/literal}{$CONTACTRADIOFIELDS[fields].OptionFields.ValidationName}{literal}';				
							mapId = '{/literal}{$CONTACTRADIOFIELDS[fields].OptionFields.ContactMapID}{literal}';
							isRequired ='{/literal}{$CONTACTRADIOFIELDS[fields].OptionFields.IsRequired}{literal}';
							isFirstName ='{/literal}{$CONTACTRADIOFIELDS[fields].OptionFields.IsFirstName}{literal}';
							isLastName ='{/literal}{$CONTACTRADIOFIELDS[fields].OptionFields.IsLastName}{literal}';
							isEmail ='{/literal}{$CONTACTRADIOFIELDS[fields].OptionFields.IsEmailAddress}{literal}';
					{/literal}
						{section name=option loop=$CONTACTRADIOFIELDS[fields].OptionValues}
					{literal}
							optionValues[{/literal}{$smarty.section.option.index}{literal}] ="{/literal} {$CONTACTRADIOFIELDS[fields].OptionValues[option].OptionName|escape:html}{literal}"
							
							optionIds[{/literal}{$smarty.section.option.index}{literal}] ='{/literal} {$CONTACTRADIOFIELDS[fields].OptionValues[option].OptionID}{literal}';
					{/literal}
						{/section}
					{literal}
							document.getElementById('fieldSelectContact').value = 'RadioButton';
							//checkContactDropDown();
							addField('contactDiv',mapId,display,fieldValue,validationValue,isFirstName,isRequired,isLastName,isEmail,optionIds,optionValues);
					{/literal}					
						{/section}
						{section name=fields loop=$CONTACTDATEFIELDS}
					{literal}
							var optionIds = new Array();
							var optionValues = new Array();
							fieldValue="{/literal}{$CONTACTDATEFIELDS[fields].LabelName|escape:html}{literal}";
							validationValue ='{/literal}{$CONTACTDATEFIELDS[fields].ValidationName}{literal}';					
							mapId = '{/literal}{$CONTACTDATEFIELDS[fields].ContactMapID}{literal}';
							isRequired ='{/literal}{$CONTACTDATEFIELDS[fields].IsRequired}{literal}';
							isFirstName ='{/literal}{$CONTACTDATEFIELDS[fields].IsFirstName}{literal}';
							isLastName ='{/literal}{$CONTACTDATEFIELDS[fields].IsLastName}{literal}';
							isEmail ='{/literal}{$CONTACTDATEFIELDS[fields].IsEmailAddress}{literal}';
							document.getElementById('fieldSelectContact').value = 'Date';
							//checkContactDropDown();
							addField('contactDiv',mapId,display,fieldValue,validationValue,isFirstName,isRequired,isLastName,isEmail,optionIds,optionValues);
					{/literal}				
						{/section}
					{literal}
					</script>
					{/literal}
			</td>
			
		</tr>
		<tr>
			<td colspan='2' height='5px' align='center'><div style="bottom:10px;"><input type='button' name='btnSave' id='btnSave' value='Save'  class='clsButton' onclick="javascript: saveContactLayOut();"/></div></td>
		</tr>
		
	</table>
	<input type='hidden' name='frmUsedType' id='frmUsedType' value='Contact'/>
	</form>
</div>