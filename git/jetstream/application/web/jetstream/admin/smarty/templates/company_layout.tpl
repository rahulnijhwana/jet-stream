{literal}
	<script type="text/javascript">
		var sec = {/literal}{$JASONACCOUNTSECTION}{literal};		
		try
		{
			sec=eval(sec);
		}
		catch(e)
		{
			sec=JSON.decode(sec);
		}
		
		var secName = {/literal}{$ACCOUNTSECTIONNAME}{literal};		
		try
		{
			secName = eval(secName);
		}
		catch(e)
		{
			secName = JSON.decode(secName);
		}
		
		var sectionName = secName;
		var sections = sec;
		var maxFields = {/literal}{$TOTALFIELDS}{literal};
		var leftSection= {/literal}{$LEFTSECTION}{literal};		
		var rightSection= {/literal}{$RIGHTSECTION}{literal};
	</script>
	<script type="text/javascript" src="javascript/dragable.js"></script>

<script type="text/javascript">
$(document).ready(function() 
{ 
	var values = new Array();
	var ans = initDragDropScript(sections);
	values = addLayoutData(rightSection);
	ans = reInitDragDropScript(ans,values);
	values = addLayoutData(leftSection);
	reInitDragDropScript(ans,values);

	$(window).bind('scroll',AdjustPosition);
});

//To Do: Dynamically adjust the top position of right div
function AdjustPosition()
{      
        
}
function addLayoutData(section)
{
	var usedValues = new Array();
	for(i=0;i<section.length;i++)
	{
		if(section[i].Align==1){
			div_no=(section[i].Position*2)-1;
		}else{
			div_no=(section[i].Position*2);
		}			
		div_id = section[i].SectionId+'_'+div_no;
		document.getElementById(div_id).innerHTML = '<div class="dragDropSmallBox" id="'+escape(section[i].LabelName)+'_'+ section[i].AccountMapID+'">'+section[i].LabelName+'</div>'; 
		usedValues[i] = escape(section[i].LabelName) +'_'+ section[i].AccountMapID;
	}
	return usedValues;
}

function saveCompanySectionLayout()
{
	var unAssignedData = '';
	for(i=0;i<document.getElementById('totalFieldsDiv').childNodes.length;i++)
	{
		if(document.getElementById('totalFieldsDiv').childNodes[i].id){
			unAssignedData += document.getElementById('totalFieldsDiv').childNodes[i].id + "|";
		}
			//alert(document.getElementById('totalFieldsDiv').childNodes[i].id);			
	}
	var secData='';
	if(sections)
	{
		for(i=0;i<sections.length;i++)
		{
			if(i==0)
				secData=sections[i];
			else
				secData+=','+sections[i];
		}
	}
	dataLayout=getLayoutSettings();
	$.post("ajax/ajax.companylayout.php",
		{			
			dataLayout:dataLayout,
			action:'saveCompanySectionLayout',
			unAssignedData : unAssignedData,
			sections:secData
		}, 
		processSaveCompanySectionLayout
	);
}

function processSaveCompanySectionLayout(result)
{
	//alert(result);
}
</script>
{/literal}
<div id="manageSectionDiv" style="background:#FFFFFF;display:none;">
<form method="get" action="./ajax/ajax.companylayout.php" id="frmManageSection" name="frmManageSection">
	<table width="400px">		
		<tr height="40px">
			<td align='left'><input type="button" class="clsButton" value="Add Section" onclick="javascript:addSection();"/></td>
		</tr>
		<tr height="40px">	
			<td align='left'><input type="button" class="clsButton" value="Edit Sections" onclick="javascript: editSection('Account');"/></td>
		</tr>
		<tr height="40px">
			<td align='left'><input type="button" class="clsButton" value="Delete Sections" onclick="javascript: deleteSection();"/></td>
		</tr>
		<tr height="40px">
			<td align='left'><input type="button" class="clsButton" value="Cancel" onclick="javascript: cancelSection();"/></td>
		</tr>		
	</table>
</form>
</div>

<div id="sectionDiv" style="background:#FFFFFF;display:none;">
<form method="get" action="./ajax/ajax.companylayout.php" id="frmAddSection" name="frmAddSection">
	<table width="400px">
		<tr>
			<td class="BlackBold">Section Name</td>
			<td><input type="text" name="sectionName" id="sectionName"/>
			<input type="hidden" name="formSelected1" id="formSelected"/></td>			
		</tr>
		<tr>
			<td height='5px'><span id="sectionError" class='clsError'></span></td>
		</tr>
		
		<tr height="100px">
			<td><input type="button" class="clsButton" value="Cancel" onclick="javascript:cancelSection();"/></td>&nbsp;&nbsp;&nbsp;&nbsp;		
			<td><input type="button" class="clsButton" value="Save" onclick="javascript: saveSection('Account','addSection');"/></td>
			
		</tr>		
	</table>
</form>
</div>

<div id="editSectionDiv" style="background:#FFFFFF;display:none;">
<form method="get" action="./ajax/ajax.companylayout.php" id="frmEditSection" name="frmEditSection">
	<table width="400px">
		<tr>
			<td class="BlackBold">
			{section name=sections loop=$ACCOUNTSECTIONDETAILS}
				<div> <input type="checkBox" name="editSection{$ACCOUNTSECTIONDETAILS[sections].SectionId}" id="editSection{$ACCOUNTSECTIONDETAILS[sections].SectionId}" value="1"/> <input type="text" id="editSectionName{$ACCOUNTSECTIONDETAILS[sections].SectionId}" name="editSectionName{$ACCOUNTSECTIONDETAILS[sections].SectionId}" value="{$ACCOUNTSECTIONDETAILS[sections].SectionName}"/></div><br/>
			{/section}
			
			</td>			
		</tr>
		<tr>
			<td height='5px'><span id="editSectionError" class='clsError'></span></td>
		</tr>
		
		<tr height="100px">
			<td><input type="button" class="clsButton" value="Cancel" onclick="javascript:cancelSection();"/></td>&nbsp;&nbsp;	
			<td><input type="button" class="clsButton" value="Update" onclick="javascript: saveSection('Account','editSection');"/></td>
		</tr>		
	</table>
</form>
</div>

<div id="deleteSectionDiv" style="background:#FFFFFF;display:none;">
<form method="get" action="./ajax/ajax.companylayout.php" id="frmDeleteSection" name="frmDeleteSection">
	<table width="400px">
		<tr>
			<td class="BlackBold">
			{section name=sections loop=$ACCOUNTSECTIONDETAILS}
				<div> <input type="checkBox" name="deleteSection{$ACCOUNTSECTIONDETAILS[sections].SectionId}" id="deleteSection{$ACCOUNTSECTIONDETAILS[sections].SectionId}" value="1"/> <input type="text" id="deleteSectionName{$ACCOUNTSECTIONDETAILS[sections].SectionId}" name="deleteSectionName{$ACCOUNTSECTIONDETAILS[sections].SectionId}" value="{$ACCOUNTSECTIONDETAILS[sections].SectionName}"/></div><br/>
			{/section}
			<input type="hidden" name="formSelected" id="formSelected"/>
			</td>			
		</tr>
		<tr>
			<td height='5px'><span id="deleteSectionError" class='clsError'></span></td>
		</tr>
		
		<tr height="100px">
			<td><input type="button" class="clsButton" value="Cancel" onclick="javascript:cancelSection();"/></td>&nbsp;&nbsp;&nbsp;&nbsp;		
			<td><input type="button" class="clsButton" value="Delete" onclick="javascript: saveSection('Account','deleteSection');"/></td>
		</tr>		
	</table>
</form>
</div>

<div id='divManageCompanyLayout'>
<form method="get" action="./ajax/ajax.user.php" id="frmManageCompany" name="frmManageCompany">
	<table  cellpadding='0' cellspacing='0' width='100%' id='tblManageCompanyLayout' style='border: 1px solid rgb(181, 226, 254);'>
		<tr>
			<td width='100%'>
				<table border='0' cellspacing='0' cellpadding='0' width='100%'>
					<tr>
						<td width='1%' style='background-image:url(images/headerleft.gif);background-repeat: no-repeat;' ></td>				
						<td width='99%'style='background-image:url(images/headerbg.gif);'  align='left' class='tblHeader'><span id='frmHeader'>Manage Company Layout</span></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr height='100%'>
			<td  vAlign='top' height='100%'>
				<div id="addAccountSectionDiv" style="float:right;">
					<input type="button" Value="Manage Sections" name="addSectionBtn" id="addSectionBtn" class='clsButton' onclick="javascript: manageSection();"/>
				</div>		
				<div id="dragScriptContainer">
					{section name=sect loop=$SEC max=1}
						<div id="{$ACCOUNTSECTION[sect]}" style="border:1px solid black; width:550px;"></div>
					{/section}
					
					<div id="totalFieldsDiv" style="left:700px;min-height:300px;border: 1px solid black;height: expression(document.body.clientHeight < 300? "300px": "100%" );">
						{section name=fields loop=$ACCOUNTFIELDS}
							<div class="dragDropSmallBox" id="{$ACCOUNTFIELDS[fields].LabelName|escape:html}_{$ACCOUNTFIELDS[fields].AccountMapID}">{$ACCOUNTFIELDS[fields].LabelName}</div>
						{/section}
					</div>
						{section name=sect loop=$SEC start=1}
							<div id="{$SEC[sect]}" style="border:1px solid black; width:550px;"></div>
							
						{/section}
				</div>
				<div id="dragContent"></div>
			</td>
		</tr>
		<tr>
			<td>	
				<input type='button' onclick="javascript:saveCompanySectionLayout();" value='Save Layout' class="clsButton"/>	
			</td>
		</tr>
	</table>
	
</form>
</div>