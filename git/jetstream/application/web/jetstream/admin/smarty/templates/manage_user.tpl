<div id='divUserByAdmin'>	
	<table id="tblUserByAdmin" class="scroll grid" cellpadding="0" cellspacing="0"></table>
	<div id="pager" class="scroll" style="text-align:center;"></div>
</div>
<form method="post" action="./ajax/ajax.user.php" id="frmCreateUser" name="frmCreateUser">
<input type='hidden' id='txt_userid' name='txt_userid'/>
	<table border='0' cellpadding='0' cellspacing='0' width='800' id='tblAddUser' style='display:none;border: 1px solid rgb(181, 226, 254);' >	
		
		
		<tr>
			<td width='100%' colspan='3'>
				<table border='0' cellspacing='0' cellpadding='0'>
					<tr>
						<td width='14px' style='background-image:url(images/headerleft.gif);background-repeat: no-repeat;' >				
						<td width='786'style='background-image:url(images/headerbg.gif);'  align='left' class='tblHeader'><span id='frmHeader'>Add User</span></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width='20%' align='right' class='smlBlackBold' height='40'><span class="clsError">*</span>First Name:</td>
			<td width='1%'></td>
			<td width='84%'><input id='FirstName' maxlength='50' name='FirstName' type='text' class='clsTextBox' size='30'/><br/><span class='clsError' id='spn_FirstName'></span></td>
		</tr>

		<tr>
			<td width='15%' align='right' class='smlBlackBold' height='40'><span class="clsError">*</span>Last Name:</td>
			<td width='1%'></td>
			<td width='84%'><input id='LastName' maxlength='50' name='LastName' type='text' class='clsTextBox' size='30' /><br/><span class='clsError' id='spn_LastName'></span></td>
		</tr>
		<tr>
			<td width='15%' align='right' class='smlBlackBold' height='40'><span class="clsError">*</span>User ID:</td>
			<td width='1%'></td>

			<td width='84%'><input id='UserID' maxlength='50' name='UserID' type='text' class='clsTextBox' size='30' /><br/><span class='clsError' id='spn_UserID'></span></td>
		</tr>
		<tr>
			<td width='15%' align='right' class='smlBlackBold' height='40'><span class="clsError">*</span>Password:</td>
			<td width='1%'></td>
			<td width='84%'><input id='Password' maxlength='50' name='Password' type='password' class='clsTextBox' size='30' /><br/><span class='clsError' id='spn_Password'></span></td>
		</tr>
		<tr>
			<td width='15%' align='right' class='smlBlackBold' height='40'><span class="clsError">*</span>Re-Password:</td>
			<td width='1%'></td>
			<td width='84%' nowrap='nowrap'><input id='RePassword' maxlength='50' name='RePassword' type='password' class='clsTextBox' size='30'/><br/><span class='clsError' id='spn_RePassword'></span></td>
		</tr>
		<tr>

			<td width='15%' align='right' class='smlBlackBold' height='40'><span class="clsError">*</span>E-Mail:</td>
			<td width='1%'></td>
			<td width='84%'><input id='Email' maxlength='50' name='Email' type='text' class='clsTextBox' size='30'/><br/><span class='clsError' id='spn_Email'></span></td>
		</tr>
		<tr>
			<td width='15%' align='right' class='smlBlackBold' height='40'><span class="clsError">*</span>Start Date:</td>
			<td width='1%'></td>
			<td width='84%'><input  id='StartDate' name='StartDate' type='text' class='clsTextBox' size='30'  size='10' /><br/><span class='clsError' id='spn_StartDate'></span></td>
		</tr>
		<tr>
			<td width='15%' align='right' class='smlBlackBold' height='40'><span class="clsError">*</span>User Type:</td>
			<td width='1%'></td>
			<td width='84%'>
			<select id='userType' name='userType' class='clsTextBox' onchange='javascript: getParentUser(this.value)'>
				{html_options options=$USERTYPE} 
			</select>
			<br/><span class='clsError' id='spn_userType'></span>
			</td>
		</tr>
		<tr>
			<td width='15%' align='right' class='smlBlackBold' height='40'>Assign:</td>
			<td width='1%'></td>
			<td width='84%' id='cellAssign'>						
			</td>
		</tr>
		<tr>
			<td width='15%'></td>
			<td width='1%'></td>
			<td width='84%'><input type='submit' name='btnSubmit' id='btnSubmit' value='Submit'  class='clsButton' />&nbsp;<input type='reset' value='Clear' class='clsButton'/></td>
		</tr>
	</table>
</form>
{if $smarty.get.sec && $smarty.get.sec=='add'}
	{literal}
		<script>
			document.getElementById('divUserByAdmin').style.display='none';
			document.getElementById('tblAddUser').style.display='block';
		</script>
	{/literal}
{/if}