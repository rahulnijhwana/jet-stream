{config_load file=admin.conf section="setup"}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>Jetstream&trade; {$page_title}</title>
		{if $css_include}
		<link rel="stylesheet" type="text/css" href="css/{foreach from=$css_include name=cssi item=x key=filename}{if !$smarty.foreach.cssi.first},{/if}{$filename}{/foreach}" />
		{/if}
		{if $css_print_include}
		<link rel="stylesheet" type="text/css" media="print" href="css/{foreach from=$css_print_include name=cssi item=x key=filename}{if !$smarty.foreach.cssi.first},{/if}{$filename}{/foreach}" />
		{/if}
		{if $js_include}
		<script type="text/javascript" src="javascript/{foreach from=$js_include name=jsi item=x key=filename}{if !$smarty.foreach.jsi.first},{/if}{$filename}{/foreach}"></script>
		{/if}
		{if $js_inline}
		<script type="text/javascript">
		{foreach from=$js_inline item=jsi}
		{$jsi}
		{/foreach}
		</script>		
		{/if}

	</head>
<body>
<div>
	<table border='0' cellpadding='5' cellspacing='0' align='center' width='100%' bgcolor='#FFFFFF'>
		<tr height='50' bgcolor='#FFFFFF'>
			<td width='100%' colspan='2'>
				<table border='0' cellpadding='5' cellspacing='0' align='center' width='98%'>
					<tr>
						<td width='70%' valign='top'><h1>Section Logo</h1></td>
						<td width='30%' valign='top' align='right' class='smlBlackBold'>
							{*Call the Date structure*}
							{$smarty.now|date_format:"%A, %B %e, %Y"}
							<br>{if $smarty.session.USER}{$smarty.session.USER.FIRSTNAME} &nbsp; {$smarty.session.USER.LASTNAME}{/if}
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr  bgcolor='#FFFFFF'>
			<td colspan='2' height='30' width='100%'>		
				{*----------Start of Menu-----------------------*}
				{if $smarty.session.USER}		
					{*IF USER LOGGED IN DISPLAY HEADER FOR ADMIN/MANAGER/SALESPERSON*}
					{if $smarty.session.USER.TYPE=='ADMIN'}
						<div class='chromestyle' id='chromemenu'>
							<ul>
								<li><a href='?action=manageuser'>Manage User</a></li>
								{if $smarty.get.action==manageuser}
									<li><a href='#' onclick='javascript: showAddForm(1);'>Add User</a></li>			
								{else}
									<li><a href='?action=manageuser&sec=add'>Add User</a></li>			
								{/if}
								<li><a href='?action=company'>Add Company Fields</a></li>
								<li><a href='?action=contact'>Add Contact Fields</a></li>
								<li><a href='?action=companylayout'>Manage Company Layout</a></li>
								<li><a href='?action=contactlayout'>Manage Contact Layout</a></li>
								<li><a href='?action=event'>Manage Events</a></li>
								<li><a href='?action=logout'>Logout</a></li>
							</ul>
						</div>
						{/if}
						{else}
							{*IF USER NOT LOGGED IN*}
							
						{/if}
{*----------End of Menu-------------------------*}

</td>
</tr>
</table>
</div>
{*----------------------------BODY CONTENT---------------------------------------------*}
{foreach from=$content key=content_title item=content_tpl}
	<div class="content_body_wrap">
		<div class="content_body">
			{if is_array($content_tpl)}
				{include file=$content_tpl.template}
			{else}
			{include file=$content_tpl}
			{/if}
		</div>
	</div>
{/foreach}

{*---------------------------END OF BODY CONTENT---------------------------------------*}
</body>