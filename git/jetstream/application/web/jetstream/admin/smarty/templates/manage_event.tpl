<table width="600px" id="tblManageEvent" border='0' cellspacing='0' cellpadding='0' {if $smarty.get.sec==addevent} style="display:none"{/if}>
<tr>
	<td width='100%'>
		<table border='0' cellspacing='0' cellpadding='0' width="100%">
			<tr>
				<td width='14px' style='background-image:url(images/headerleft.gif);background-repeat: no-repeat;' ></td>	
				<td width="586px" style='background-image:url(images/headerbg.gif);'  align='left' class='tblHeader'><span id='frmHeader'>{$header_title}</span></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<form method="post" name="frmEvent" id="frmEvent" action="./ajax/ajax.addevent.php">
		<table border='0' cellpadding='0' cellspacing='0' width='600' id='tblAddEvent' style='border: 1px solid rgb(181, 226, 254);' >		
			<tr>
				<td nowrap="nowrap" width='20%' align='right' class='smlBlackBold' height='40'><span class="clsError">*</span>Event Name:</td>
				<td width='1%'></td>
				<td colspan="2"><input id='EventName' name='EventName' type='text' class='clsTextBox' size='30'/><br/><span class='clsError  error' id='spn_EventName'></span></td>

			</tr>
			<tr>
				<td width='20%' align='right' class='smlBlackBold' height='40'><span class="clsError">*</span>Event Color:</td>
				<td width='1%'></td>
				<td colspan="2" nowrap="nowrap" >
					<input readonly="readonly" id='EventColor' maxlength='50' name='EventColor' type='text' class='clsTextBox' size='30' onkeydown="javascript: changeDivColor(this.value);"/>
					<br/><span class='clsError error' id='spn_EventColor'></span>					

				</td>
				<td width="75%;">
					<a href="#" class="tt" style="border:0">
						<span id="bgSpan" style="width:16px; height:16px; border:1px solid black;margin:2px;" onmousemove="javascript: resetPallet();">&nbsp;&nbsp;&nbsp;&nbsp;</span>						
							<div class="tooltip">
							<div class="top"></div>
							<div class="middle" style="cursor:auto;border:1px solid black;width:{$BLOCKS_PER_ROW*23}px;height:{$ROWS*20}px;" id="colorHolder">
								{foreach key=key from=$COLOR_CODE item=item}	
									<span colorCode="{$item}" style="
									width:16px; height:15px; float:left; border:1px solid black;
									margin:2px;
									background-color: #{$item};
									cursor:crosshair;
									" onclick="javascript:setColor(this)">&nbsp;</span>	
									{if ($key+1)%$BLOCKS_PER_ROW==0}<br/>{/if}	
								{/foreach} 
							</div>
							<div class="bottom"></div>
						</div>
					</a>					
				
				</td>
			</tr>	
			<tr>
				<td width='15%'></td>
				<td width='1%'></td>
				<td>
				<input type='submit' name='btnSubmit' id='btnSubmit' value='Submit'  class='clsButton' />&nbsp;
				<input type='button' value='Cancel' class='clsButton' onclick="javascript:window.location='?action=event'"/>
				<input type="hidden" name="EventTypeID" id="EventTypeID"/>
				</td>
			</tr>
		</table>
		</form>	
	</td>
</tr>
<tr>
	<td>
		<table border="0" width="600px" class="event" cellspacing="0">
			<tr >
				<th colspan="2" style="border-left: 1px solid #C1DAD7;border-top: 1px solid #C1DAD7;border-bottom: 1px solid #C1DAD7;">Action</th>
				<th style="border-top: 1px solid #C1DAD7;border-bottom: 1px solid #C1DAD7;">Event Type</th>
				<th style="border-top: 1px solid #C1DAD7;border-bottom: 1px solid #C1DAD7;border-right: 1px solid #C1DAD7;">Color</th>
			</tr>
			{foreach item=event from=$EVENTTYPE}
				<tr id="tr_{$event.EventTypeID}">
					<td class="leftcell" onclick="javascript:editEventType({$event.EventTypeID})"><img src="images/edit.jpg" alt="edit" /></td>
					<td onclick="javascript:deleteEventType({$event.EventTypeID},'{$event.EventName|escape:quotes}')"><img src="images/del.png" alt="remove" /></td>
					<td>{$event.EventName}</td>
					<td><div style="width:16px;height:16px;background-color:#{$event.EventColor}"></div></td>
				</tr>			
			{/foreach}

		</table>
	</td>
</tr>
</table>
