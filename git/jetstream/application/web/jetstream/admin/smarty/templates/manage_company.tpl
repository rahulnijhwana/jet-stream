{literal}
<script>
var options={/literal}{$OPTIONSETS_JSON}{literal};
try
{
	options=eval(options);
}
catch(e)
{
	options=JSON.decode(options);
}
</script>
{/literal}

<div id="editOptionListDiv" style="background:#FFFFFF;display:none;height:500px;" >
<form method="post" action="./ajax/ajax.manageCompany.php" id="frmEditOptions" name="frmEditOptions">
	<table width="400px">
		<tr>
			<td height='5px'><span id="editOptionError" class='clsError'></span></td>
		</tr>
		<tr>
			<td style=" vertical-align:top;">
				<span class="smlBlackBold">Options</span>
				<div id="addOptionElementsDiv"></div>
				<div id="addOptionElementsLeftDiv"></div>
			</td>
		</tr>
		<tr height="100px">
			<td><input type="button" class="clsButton" value="Cancel" onclick="javascript:cancelEditOptionList();"/></td>&nbsp;&nbsp;&nbsp;&nbsp;
			<td><input type="button" class="clsButton" value="Add Options" onclick="javascript:addEditOptionList();"/></td>&nbsp;&nbsp;&nbsp;&nbsp;		
			<td><input type="button" class="clsButton" value="Save" onclick="javascript: saveEditOptions();"/></td>
		</tr>		
	</table>
	</form>
</div>

<div id="optionListDiv" style="background:#FFFFFF;display:none;height:500px;" >
<form method="get" action="./ajax/ajax.user.php" id="frmAddOptions" name="frmAddOptions">
	<table width="400px">
		<tr>
			<td class="BlackBold">Label Name</td>
			<td><input type="text" name="optionSetName" id="optionSetName" value=""/>
			<input type="hidden" name="formSelected" id="formSelected" value=""/></td>			
		</tr>
		<tr>
			<td class="BlackBold">Available Option set</td>
			<td><select id='optionSetSelect' class='clsTextBox'   onChange="javascript: populateOptions();">
					<option value="-1"> </option>
					{section name=options loop=$OPTIONSETS}
					<option value='{$smarty.section.options.index}'>{$OPTIONSETS[options].OptionSetName}</option>
					{/section}
				</select>
			</td>			
		</tr>
		<tr>
			<td class="BlackBold">Options</td>
		</tr>
		<tr>
			<td id='optionSetvalues'></td>
			<input type="hidden" name="optionSetId" id="optionSetId" value="">			
		</tr>
		<tr>
			<td height='5px'><span id="optionListError" class='clsError'></span></td>
		</tr>
		<tr height="100px">
			<td><input type="button" class="clsButton" value="Cancel" onclick="javascript:cancelOption(); "/></td>&nbsp;&nbsp;&nbsp;&nbsp;
			<td><input type="button" class="clsButton" value="Add Options" onclick="javascript:addOptionList('Company');"/></td>&nbsp;&nbsp;&nbsp;&nbsp;		
			<td><input type="button" class="clsButton" value="Save" onclick="javascript: saveOptionList('companyDiv');"/></td>
		</tr>		
	</table>
	</form>
</div>
<div id='divCompanyLayout'>
	<form method="get" action="./ajax/ajax.user.php" id="frmManageCompany" name="frmManageCompany">
	<input type='hidden' id='companyId' value='{$COMPANYID}' name='companyId'  />
	<input type="hidden" value="0" id="divValue" />
	<input type="hidden" value="0" id="childValue" />
	<table  cellpadding='0' cellspacing='0' id='tblCompanyLayout' class='formMainTable'>
		<tr>
			<td colspan="2" width='100%'>
				<table border='0' cellspacing='0' cellpadding='0' width="100%">
					<tr>
						<td width='1%' style='background-image:url(images/headerleft.gif);background-repeat: no-repeat;' ></td>	
						<td width='99%'style='background-image:url(images/headerbg.gif);'  align='left' class='tblHeader'><span id='frmHeader'>{$header_title}</span></td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr>
			<td width='70%'>
				<div style="float:left;width:100%;" id="companyDiv">
					<div style="float:left;width:15%"  id="typeDiv" class='smlBlackBold'>Type</div>
					<div style="float:left;width:15%"  id="labelDiv" class='smlBlackBold'>Label</div>
					<div style="float:left;width:13%" id="optionsDiv" class='smlBlackBold'>Options</div>
					<div style="float:left;width:10%" id="mandatoryDiv" class='smlBlackBold'>Mandatory</div>
					<div style="float:left;width:10%" id="companyDiv" class='smlBlackBold'>IsCompanyName</div>
					<div style="float:left;width:9%;text-align:center" id="selectDiv" class='smlBlackBold'>Edit</div>
				</div>
				
			</td>
			<td width='20%' align='center' valign='top' style="padding-top:10px;">
				<table>
					<tr>
						<td>
							<div style='border: 2px solid rgb(181, 226, 254);vertical-align:top; width:250px; height:100px;' >
							
							<div class='smlBlackBold' style="float:left;width:120px;" >Select Field Type</div>
							<div ><select id='fieldType' name='fieldType' class='clsTextBox' onChange="javascript: checkFieldType();">
									<option value="-1"> </option>
									<option value="Text">Text</option>
									<option value="Number">Number</option>
									<option value="Zip">Zip</option>
									<option value="Email">Email</option>
									<option value="Phone">Phone</option>
									<option value="DropDown">DropDown</option>
									<option value="CheckBox">CheckBox</option>
									<option value="RadioButton">RadioButton</option>
									<option value="Date">Date</option>
								</select>
							</div>
							<input type="hidden" id='fieldSelect' name='fieldSelect' value="">
							<input type="hidden" id='dataTypeSelect' name='dataTypeSelect' value="">
							<br>
							<div align="center"><input type='button' name='companyAddField' id='companyAddField' value='Add'  class='clsButton' onclick="javascript: checkNumberField(document.getElementById('fieldSelect').value,'Company');"/><input type='hidden' name='frmType' id='frmType' value=''/></div>
						</div>
					</td>
				</tr>
				<tr>
					<td><span id="showError" class='clsError'></span></td>
				</tr>
			</table>
				
					{literal}
						<script type="text/javascript">
							var fieldValue= '';
							var validationValue ='';
							var display='YES';
							var mapId = '';
							var isRequired ='';
							var isCompany ='';
							var isEmail = '';
							var isLastName = '';
							
					{/literal}
						{section name=fields loop=$ACCOUNTTEXTFIELDS}
					{literal}
							var optionIds = new Array();
							var optionValues = new Array();
							fieldValue="{/literal}{$ACCOUNTTEXTFIELDS[fields].LabelName|escape:html}{literal}";	
							validationValue ='{/literal}{$ACCOUNTTEXTFIELDS[fields].ValidationName}{literal}';	
							mapId = '{/literal}{$ACCOUNTTEXTFIELDS[fields].AccountMapID}{literal}';
							isRequired ='{/literal}{$ACCOUNTTEXTFIELDS[fields].IsRequired}{literal}';
							isCompany ='{/literal}{$ACCOUNTTEXTFIELDS[fields].IsCompanyName}{literal}';
							document.getElementById('fieldSelect').value = 'TextBox';
							document.getElementById('fieldType').value = '{/literal}{$ACCOUNTTEXTFIELDS[fields].FieldType}{literal}';
							document.getElementById('dataTypeSelect').value = 'Alphabet';
							addField('companyDiv',mapId,display,fieldValue,validationValue,isCompany,isRequired,isLastName,isEmail,optionIds,optionValues);
					{/literal}					
						{/section}
						{section name=fields loop=$ACCOUNTNUMBERFIELDS}
					{literal}
							var optionIds = new Array();
							var optionValues = new Array();
							fieldValue="{/literal}{$ACCOUNTNUMBERFIELDS[fields].LabelName|escape:html}{literal}";	
							validationValue ='{/literal}{$ACCOUNTNUMBERFIELDS[fields].ValidationName}{literal}';				
							mapId = '{/literal}{$ACCOUNTNUMBERFIELDS[fields].AccountMapID}{literal}';
							isRequired ='{/literal}{$ACCOUNTNUMBERFIELDS[fields].IsRequired}{literal}';
							isCompany ='{/literal}{$ACCOUNTNUMBERFIELDS[fields].IsCompanyName}{literal}';
							document.getElementById('fieldSelect').value = 'TextBox';
							document.getElementById('fieldType').value = '{/literal}{$ACCOUNTNUMBERFIELDS[fields].FieldType}{literal}';
							document.getElementById('dataTypeSelect').value = 'Numeric';
							addField('companyDiv',mapId,display,fieldValue,validationValue,isCompany,isRequired,isLastName,isEmail,optionIds,optionValues);
					{/literal}					
						{/section}
						{section name=fields loop=$ACCOUNTCHECKFIELDS}
					{literal}
							var optionIds = new Array();
							var optionValues = new Array();
							fieldValue="{/literal}{$ACCOUNTCHECKFIELDS[fields].LabelName|escape:html}{literal}";		
							validationValue ='{/literal}{$ACCOUNTCHECKFIELDS[fields].ValidationName}{literal}';				
							mapId = '{/literal}{$ACCOUNTCHECKFIELDS[fields].AccountMapID}{literal}';
							isRequired ='{/literal}{$ACCOUNTCHECKFIELDS[fields].IsRequired}{literal}';
							isCompany ='{/literal}{$ACCOUNTCHECKFIELDS[fields].IsCompanyName}{literal}';
							document.getElementById('fieldSelect').value = 'CheckBox';
							//checkDropDown();
							addField('companyDiv',mapId,display,fieldValue,validationValue,isCompany,isRequired,isLastName,isEmail,optionIds,optionValues);
					{/literal}				
						{/section}
						{section name=fields loop=$ACCOUNTDROPDOWNFIELDS}
					{literal}
							var optionIds = new Array();
							var optionValues = new Array();
							fieldValue="{/literal}{$ACCOUNTDROPDOWNFIELDS[fields].OptionFields.LabelName|escape:html}{literal}";		
							validationValue ='{/literal}{$ACCOUNTDROPDOWNFIELDS[fields].OptionFields.ValidationName}{literal}';				
							mapId = '{/literal}{$ACCOUNTDROPDOWNFIELDS[fields].OptionFields.AccountMapID}{literal}';
							isRequired ='{/literal}{$ACCOUNTDROPDOWNFIELDS[fields].OptionFields.IsRequired}{literal}';
							isCompany ='{/literal}{$ACCOUNTDROPDOWNFIELDS[fields].OptionFields.IsCompanyName}{literal}';
					{/literal}
						{section name=option loop=$ACCOUNTDROPDOWNFIELDS[fields].OptionValues}
					{literal}
							optionValues[{/literal}{$smarty.section.option.index}{literal}] ="{/literal} {$ACCOUNTDROPDOWNFIELDS[fields].OptionValues[option].OptionName|escape:html}{literal}"
							
							optionIds[{/literal}{$smarty.section.option.index}{literal}] ='{/literal} {$ACCOUNTDROPDOWNFIELDS[fields].OptionValues[option].OptionID}{literal}';
					{/literal}
						{/section}
					{literal}
							document.getElementById('fieldSelect').value = 'DropDown';
							//checkDropDown();
							addField('companyDiv',mapId,display,fieldValue,validationValue,isCompany,isRequired,isLastName,isEmail,optionIds,optionValues);
					{/literal}				
						{/section}
					
						{section name=fields loop=$ACCOUNTRADIOFIELDS}
					{literal}
							var optionIds = new Array();
							var optionValues = new Array();
							fieldValue="{/literal}{$ACCOUNTRADIOFIELDS[fields].OptionFields.LabelName|escape:html}{literal}";		
							validationValue ='{/literal}{$ACCOUNTRADIOFIELDS[fields].OptionFields.ValidationName}{literal}';				
							mapId = '{/literal}{$ACCOUNTRADIOFIELDS[fields].OptionFields.AccountMapID}{literal}';
							isRequired ='{/literal}{$ACCOUNTRADIOFIELDS[fields].OptionFields.IsRequired}{literal}';
							isCompany ='{/literal}{$ACCOUNTRADIOFIELDS[fields].OptionFields.IsCompanyName}{literal}';
					{/literal}
						{section name=option loop=$ACCOUNTRADIOFIELDS[fields].OptionValues}
					{literal}
							optionValues[{/literal}{$smarty.section.option.index}{literal}] ="{/literal} {$ACCOUNTRADIOFIELDS[fields].OptionValues[option].OptionName|escape:html}{literal}"
							
							optionIds[{/literal}{$smarty.section.option.index}{literal}] ='{/literal} {$ACCOUNTRADIOFIELDS[fields].OptionValues[option].OptionID}{literal}';
					{/literal}
						{/section}
					{literal}
							document.getElementById('fieldSelect').value = 'RadioButton';
							//checkDropDown();
							addField('companyDiv',mapId,display,fieldValue,validationValue,isCompany,isRequired,isLastName,isEmail,optionIds,optionValues);
					{/literal}					
						{/section}
						{section name=fields loop=$ACCOUNTDATEFIELDS}
					{literal}
							var optionIds = new Array();
							var optionValues = new Array();
							fieldValue="{/literal}{$ACCOUNTDATEFIELDS[fields].LabelName|escape:html}{literal}";	
							validationValue ='{/literal}{$ACCOUNTDATEFIELDS[fields].ValidationName}{literal}';					
							mapId = '{/literal}{$ACCOUNTDATEFIELDS[fields].AccountMapID}{literal}';
							isRequired ='{/literal}{$ACCOUNTDATEFIELDS[fields].IsRequired}{literal}';
							isCompany ='{/literal}{$ACCOUNTDATEFIELDS[fields].IsCompanyName}{literal}';
							document.getElementById('fieldSelect').value = 'Date';
							//checkDropDown();
							addField('companyDiv',mapId,display,fieldValue,validationValue,isCompany,isRequired,isLastName,isEmail,optionIds,optionValues);
					{/literal}				
						{/section}
					{literal}
					</script>
					{/literal}
			</td>
			
		</tr>
		<tr>
			<td colspan='2' height='5px' align='center'><div style="bottom:10px;"><input type='button' name='btnSave' id='btnSave' value='Save'  class='clsButton' onclick="javascript: saveCompanyLayOut();"/>
			<input type='hidden' name='frmUsedType' id='frmUsedType' value='Company'/></div></td>
		</tr>
		
	</table>
	</form>
</div>
	