{literal}
 <style type="text/css">
{/literal}

{foreach from=$SEC item=sec name=sec_name}{if !$smarty.foreach.sec_name.first}, {/if}#{$sec}{/foreach}
{literal}
{
	float:left;
	width:700px;
	padding:12px;
	visibility:hidden;	/* Initial state  - Don't change this */
	border:1px solid #FFF;
}
{/literal}

{foreach from=$SEC item=sec name=sec_name}{if !$smarty.foreach.sec_name.first}, {/if}#{$sec} div{/foreach}
{literal}
{
	width:145px;
	height:20px;
	line-height:20px;
	float:left;
	margin-right:15px;
	margin-bottom:2px;
	text-align:center;
}
{/literal}
{foreach from=$SEC item=sec name=sec_name}{if !$smarty.foreach.sec_name.first}, {/if}#{$sec} .dragDropSmallBox{/foreach}
{literal}
{
	border:1px solid #000;
	cursor:pointer;
	width:250px;
	background-color:#E2EBED;
}
{/literal}
{foreach from=$SEC item=sec name=sec_name}{if !$smarty.foreach.sec_name.first}, {/if}#{$sec} div div{/foreach}
{literal}
{
	margin:0px;
	border:0px;
	padding:0px;
	background-color:#FFF;
	cursor:pointer;
}
{/literal}
{foreach from=$SEC item=sec name=sec_name}{if !$smarty.foreach.sec_name.first}, {/if}#{$sec} .destinationBox{/foreach}
{literal}
{
	width:250px;	/* Width of #firstSection div + 2 */
	height:22px;
}
{/literal}
{foreach from=$SEC item=sec name=sec_name}{if !$smarty.foreach.sec_name.first}, {/if}#{$sec} .correctAnswer{/foreach}
{literal}
{
	background-color:green;
	color:#fff;
	width:250px;
	border:1px solid #000;
}
{/literal}
{foreach from=$SEC item=sec name=sec_name}{if !$smarty.foreach.sec_name.first}, {/if}#{$sec} .wrongAnswer{/foreach}
{literal}
{	
	background-color:green;
	color:#fff;
	width:250px;
	border:1px solid #000;
}
{/literal}
{foreach from=$SEC item=sec name=sec_name}{if !$smarty.foreach.sec_name.first}, {/if}#{$sec} .dragContentOver{/foreach}
{literal}
{
	border:1px solid #F00;
	width:250px;
}
{/literal}
{literal}
#dragScriptContainer{
	width:1000px;
	margin:0 auto;
	//border:1px solid #000;
	height:100%;
	margin-top:0px;
	padding:3px;
	-moz-user-select:no;
	overflow:hidden;
	background-color:#FFF;


}
#totalFieldsDiv{	/* Big div for all the answers */
	float:right;
	width:250px;
	border:1px solid #FFF;
	padding:2px;
	visibility:hidden; /* Initial state  - Don't change this */
}


#dragContent div{	/* Drag content div - i.e. specific answers when they are beeing dragged */
	border:1px solid #000;
}

#totalFieldsDiv .dragDropSmallBox{	/* Small answer divs */
	border:1px solid #000;
	width:250px;
	cursor:pointer;
}

#dragContent div{
	background-color:#FFF;
	width:250px;
}
#totalFieldsDiv.dragContentOver{	/* Mouse over answer box - i.e. indicating where dragged element will be appended if mouse button is relased */
	border:1px solid #F00;
}

/* NEVER CHANGE THIS */
#dragContent{
	position:absolute;
	display:none;
}
p{
	margin:2px;
	font-size:0.9em;
}
</style>
{/literal}


