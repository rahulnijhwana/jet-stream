{include file='header.tpl'}
<form method="post" action="" id="frmLogin" name="frmLogin">
<table border='0' cellpadding='0' cellspacing='0' width='90%' align='center'>
	<tr>
		<td width='100%' colspan='3' height='60' class='xlrgBlackBold'>Login</td>
	</tr>
	<tr>
		<td width='15%' align='right' class='smlBlackBold' height='40'><span class="clsError">*</span>User Id:</td>
		<td width='1%'></td>
		<td width='84%'><input id='editUserId' maxlength='50' name='editUserId' type='text' class='clsTextBox' size='30' {if $editUserId !=""} value="{$editUserId}" {/if}/>{if $ERROR.editUserId}<br/><span class="clsError">{$ERROR.editUserId}</span>{/if}</td>
	</tr>
	<tr>
		<td width='15%' align='right' class='smlBlackBold' height='40'><span class="clsError">*</span>Password:</td>
		<td width='1%'></td>
		<td width='84%'><input id='editPassword' maxlength='50' name='editPassword' type='password' class='clsTextBox' size='30'/>{if $ERROR.editPassword}<br/><span class="clsError">{$ERROR.editPassword}</span>{/if}</td>
	</tr>
	<tr>
		<td width='15%'></td>
		<td width='1%'></td>
		<td width='84%'><input type='submit' name='btnLogin' id='btnLogin' value='Login'  class='clsButton'/></td>
	</tr>
	<tr>
		<td width='15%' height='40'></td>
		<td width='1%'></td>
		<td class="clsError">{if $ERROR.showError}<span class="clsError">{$ERROR.showError}</span>{/if}</td>
	</tr>

</table>
</form>
{include file='footer.tpl'}