<?php
require_once BASE_PATH .'/slipstream/class.EventType.php';
require_once 'color_config.php';

$objEventType=new EventType();
$eventType=$objEventType->getAllEventTypeByCompanyId($_SESSION['USER']['COMPANYID']);

$rows=count($colors)/BLOCKS_PER_ROW;
if(count($colors) % BLOCKS_PER_ROW  > 0)
	$rows++;
$smarty->assign('COLOR_CODE',$colors);
$smarty->assign('BLOCKS_PER_ROW',BLOCKS_PER_ROW);
$smarty->assign('ROWS',$rows);
$smarty->assign('EVENTTYPE',$eventType);
$smarty->assign('header_title','Manage Events');
$smarty->append('css_include', array('style.css' => 1, 'menu.css' => 1,'adminStyle.css'=>1,'color_picker.css'=>1), true);
$smarty->append('js_include', array('jquery.js' => 1,'jquery.form.js'=>1,'json.js'=>1,'manage_event.js'=>1), true);
$smarty->append('content', array('Manage Event' => 'manage_event.tpl'), true);
$smarty->display('header.tpl');	
?>