<?php
include_once (BASE_PATH . '/slipstream/class.User.php');
include_once (BASE_PATH . '/slipstream/class.OptionSet.php');
include_once (BASE_PATH . '/slipstream/class.Option.php');
include_once (BASE_PATH . '/slipstream/class.AccountMap.php');
include_once (BASE_PATH . '/slipstream/class.Section.php');
include_once (BASE_PATH . '/slipstream/class.Validation.php');


$objUser = new User ();
$objOption = new Option();
$objOptionSet = new OptionSet();
$objAccountMap = new AccountMap();

$optionSet = $objOptionSet->getOptionSetByCompanyId($_SESSION['USER']['COMPANYID']);
$accountTextFields = $objAccountMap->getFieldsFromAccountMapByCompId($_SESSION['USER']['COMPANYID'],'Text');
$accountNumberFields = $objAccountMap->getFieldsFromAccountMapByCompId($_SESSION['USER']['COMPANYID'],'Number');
$accountBooleanFields = $objAccountMap->getFieldsFromAccountMapByCompId($_SESSION['USER']['COMPANYID'],'Bool');
$accountSelectFields = $objAccountMap->getFieldsFromAccountMapByCompId($_SESSION['USER']['COMPANYID'],'Select');
$accountDateFields = $objAccountMap->getFieldsFromAccountMapByCompId($_SESSION['USER']['COMPANYID'],'Date');

for($i=0;$i<count($accountTextFields);$i++){
$objValidation = new Validation();
	$fieldType = $objValidation->getFieldTypeByValidationId($accountTextFields[$i]['ValidationType']);

	$accountTextFields[$i]['FieldType'] = $fieldType;
}
for($i=0;$i<count($accountNumberFields);$i++){
$objValidation = new Validation();
	$fieldType = $objValidation->getFieldTypeByValidationId($accountNumberFields[$i]['ValidationType']);

	$accountNumberFields[$i]['FieldType'] = $fieldType;
}

$accountDropDownFields = array();
$accountRadioButtonFields = array();
foreach($accountSelectFields as $fields){
	if($fields['OptionSetID'] == null){
		$objUser = new User ();
		$options = $objOption->getOptionsByAccountMapId($_SESSION['USER']['COMPANYID'],$fields['AccountMapID']);

	}else{
		$optionSetDetails = $objOptionSet->getOptionsSetDetailsByOptionSetId($fields['OptionSetID']);

		if($optionSetDetails[0]['AccountMapID'] != NULL){
			$options = $objOption->getOptionsByAccountMapId($_SESSION['USER']['COMPANYID'],$optionSetDetails[0]['AccountMapID']);
		}else if($optionSetDetails[0]['ContactMapID'] != NULL){
			$options = $objOption->getOptionsByContactMapId($_SESSION['USER']['COMPANYID'],$optionSetDetails[0]['ContactMapID']);
		}
	}


	if($fields['OptionType'] == 1){
		$accountDropDownFields[] = array('OptionFields'=>$fields,'OptionValues'=>$options);
	}else{
		$accountRadioButtonFields[] = array('OptionFields'=>$fields,'OptionValues'=>$options);
	}
}
$smarty->assign ( 'OPTIONSETS_JSON', json_encode($optionSet));
$smarty->assign ( 'OPTIONSETS', $optionSet);
$smarty->assign ( 'COMPANYID', $_SESSION['USER']['COMPANYID']);
$smarty->assign ( 'ACCOUNTTEXTFIELDS', $accountTextFields);
$smarty->assign ( 'ACCOUNTNUMBERFIELDS', $accountNumberFields);
$smarty->assign ( 'ACCOUNTCHECKFIELDS', $accountBooleanFields);
$smarty->assign ( 'ACCOUNTDROPDOWNFIELDS', $accountDropDownFields);
$smarty->assign ( 'ACCOUNTRADIOFIELDS', $accountRadioButtonFields);
$smarty->assign ( 'ACCOUNTDATEFIELDS', $accountDateFields);

if(isset($_GET['sec']) && $_GET['sec']=='companylayout')
	$smarty->assign('LAYOUT','COMPANYLAYOUT');
else if(isset($_GET['sec']) && $_GET['sec']=='contactlayout')
	$smarty->assign('LAYOUT','CONTACTLAYOUT');
else if(isset($_GET['action']) && $_GET['action']=='manageuser')
	$smarty->assign('LAYOUT','MANAGEUSER');

//$smarty->display('manage_company.tpl');
$smarty->assign('header_title','Add Company Fields');
$smarty->append('css_include', array('style.css' => 1, 'menu.css' => 1), true);
$smarty->append('js_include', array('jquery.js' => 1,'jquery.form.js'=>1,'json.js'=>1,'admin_company.js'=>1,'generic_admin.js'=>1,'jquery.blockUI.js'=>1), true);
$smarty->append('content', array('Manage Company' => 'manage_company.tpl'), true);
$smarty->display('header.tpl');
?>
