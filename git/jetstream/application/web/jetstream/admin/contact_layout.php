<?php
	include_once (BASE_PATH . '/slipstream/class.User.php');
	include_once (BASE_PATH . '/slipstream/class.ContactMap.php');
	include_once (BASE_PATH . '/slipstream/class.Section.php');

	$objUser = new User ();
	$objContactMap = new ContactMap();
	$objSection = new Section();
	$contactSections = $objSection->getContactSectionByCompanyId($_SESSION['USER']['COMPANYID']);
	$allContactFields= $objContactMap->getFieldsFromContactMapByCompId($_SESSION['USER']['COMPANYID'],'%');

	$unUsedFields = array();
	$leftSectionFields = array();
	$rightSectionFields = array();
	$totalContactFields = count($allContactFields);

	for($i=0;$i<count($allContactFields);$i++){
		if($allContactFields[$i]['SectionID'] == NULL || $allContactFields[$i]['SectionID'] == ''){
			$unUsedFields[] = array('ContactMapID'=>$allContactFields[$i]['ContactMapID'],'LabelName'=>$allContactFields[$i]['LabelName']);
		}else{
			foreach($contactSections as $sections){
				if($allContactFields[$i]['SectionID'] == $sections['SectionID']){
					$sectionName = str_replace(' ', '',$sections['ContactSectionName']);
				}
			}
			if($allContactFields[$i]['Align'] == 1){

				$leftSectionFields[] = array('ContactMapID'=>$allContactFields[$i]['ContactMapID'],'LabelName'=>$allContactFields[$i]['LabelName'],'Position'=>$allContactFields[$i]['Position'],'Align'=>$allContactFields[$i]['Align'],'SectionId'=>$sectionName);

			}else{
				$rightSectionFields[] = array('ContactMapID'=>$allContactFields[$i]['ContactMapID'],'LabelName'=>$allContactFields[$i]['LabelName'],'Position'=>$allContactFields[$i]['Position'],'Align'=>$allContactFields[$i]['Align'],'SectionId'=>$sectionName);
			}

		}
	}

	$conSectionDetails = array();
	$conSection = array();
	$conSectionName = array();
	$sectionId = array();
	if(count($contactSections) == 0){
		$objSection ->saveSection($_SESSION['USER']['COMPANYID'],NULL,'DefaultSection');
		$conSection[] = 'DefaultSection';
		$conSectionName[] =  'DefaultSection';
	}
	else{
		foreach($contactSections as $sections){
			$conSection[] = str_replace(' ', '',$sections['ContactSectionName']);
			$sectionId[]=$sections['SectionID'];
			$conSectionName[] =  $sections['ContactSectionName'];
			$conSectionDetails[] = array('SectionName'=>$sections['ContactSectionName'],'SectionId'=>$sections['SectionID']);
		}
	}
	//print_r($leftSectionFields);
	$smarty->assign ( 'CONTACTSECTIONNAME', json_encode($conSectionName));
	$smarty->assign ( 'LEFTSECTION', json_encode($leftSectionFields));
	$smarty->assign ( 'RIGHTSECTION', json_encode($rightSectionFields));
	$smarty->assign ( 'JASONCONTACTSECTION', json_encode($conSection));
	$smarty->assign ( 'CONTACTSECTION', $conSection);
	$smarty->assign ( 'CONTACTSECTIONDETAILS', $conSectionDetails);
	$smarty->assign ( 'SEC', $conSection);
	$smarty->assign ( 'CONTACTFIELDS', $unUsedFields);
	$smarty->assign ( 'TOTALFIELDS', $totalContactFields);

	//$smarty->display('contact_layout.tpl')
	$smarty->assign('header_title','Manage Contact Layout');
	$smarty->append('css_include', array('style.css' => 1, 'menu.css' => 1), true);
	$smarty->append('js_include', array('jquery.js' => 1,'jquery.form.js'=>1,'jquery.blockUI.js'=>1,'json.js'=>1,'generic_layout.js'=>1), true);
	$smarty->append('content', array('Style Sheet' => 'style.tpl','Manage Contact' => 'contact_layout.tpl'), true);
	$smarty->display('header.tpl');
?>