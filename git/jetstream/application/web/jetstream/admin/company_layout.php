<?php
include_once (BASE_PATH . '/slipstream/class.User.php');
include_once (BASE_PATH . '/slipstream/class.AccountMap.php');
include_once (BASE_PATH . '/slipstream/class.Section.php');

$objUser = new User ( );
$objAccountMap = new AccountMap();
$objSection = new Section();
$accountSections = $objSection->getAccountSectionByCompanyId($_SESSION['USER']['COMPANYID']);

$allAccountFields = $objAccountMap->getFieldsFromAccountMapByCompId($_SESSION['USER']['COMPANYID'],'%');
$unUsedFields = array();
$leftSectionFields = array();
$rightSectionFields = array();
$totalAccountFields = count($allAccountFields);

for($i=0;$i<count($allAccountFields);$i++){
	if($allAccountFields[$i]['SectionID'] == NULL || $allAccountFields[$i]['SectionID'] == ''){
		$unUsedFields[] = array('AccountMapID'=>$allAccountFields[$i]['AccountMapID'],'LabelName'=>$allAccountFields[$i]['LabelName']);

	}else{
		foreach($accountSections as $sections){
			if($allAccountFields[$i]['SectionID'] == $sections['SectionID']){
				$sectionName = str_replace(' ', '',$sections['AccountSectionName']);
			}
		}
		if($allAccountFields[$i]['Align'] == 1){

			$leftSectionFields[] = array('AccountMapID'=>$allAccountFields[$i]['AccountMapID'],'LabelName'=>$allAccountFields[$i]['LabelName'],'Position'=>$allAccountFields[$i]['Position'],'Align'=>$allAccountFields[$i]['Align'],'SectionId'=>$sectionName);

		}else{
			$rightSectionFields[] = array('AccountMapID'=>$allAccountFields[$i]['AccountMapID'],'LabelName'=>$allAccountFields[$i]['LabelName'],'Position'=>$allAccountFields[$i]['Position'],'Align'=>$allAccountFields[$i]['Align'],'SectionId'=>$sectionName);
		}

	}
}

$accSectionDetails = array();
$accSection = array();
$accSectionName = array();
$sectionId = array();
if(count($accountSections) == 0){
	$objSection ->saveSection($_SESSION['USER']['COMPANYID'],'DefaultSection',NULL);
	$accSection[] = 'DefaultSection';
	$accSectionName[] = 'DefaultSection';
}
else{
	foreach($accountSections as $sections){
		$accSection[] = str_replace(' ', '', $sections['AccountSectionName']);
		$sectionId[] = $sections['SectionID'];
		$accSectionName[] = $sections['AccountSectionName'];
		$accSectionDetails[] = array('SectionName'=>$sections['AccountSectionName'],'SectionId'=>$sections['SectionID']);
	}
}
print_r($unUsedFields);
$smarty->assign ( 'ACCOUNTSECTIONNAME', json_encode($accSectionName));
$smarty->assign ( 'LEFTSECTION', json_encode($leftSectionFields));
$smarty->assign ( 'RIGHTSECTION', json_encode($rightSectionFields));
$smarty->assign ( 'JASONACCOUNTSECTION', json_encode($accSection));
$smarty->assign ( 'ACCOUNTSECTIONDETAILS', $accSectionDetails);
$smarty->assign ( 'ACCOUNTSECTION', $accSection);
$smarty->assign ( 'ACCOUNTFIELDS', $unUsedFields);
$smarty->assign ( 'SEC', $accSection);
$smarty->assign ( 'TOTALFIELDS', $totalAccountFields);
$smarty->assign('header_title','Manage Company Layout');
$smarty->append('css_include', array('style.css' => 1, 'menu.css' => 1), true);
$smarty->append('js_include', array('jquery.js' => 1,'json.js'=>1,'jquery.blockUI.js'=>1,'generic_layout.js'=>1,'jquery.form.js'=>1), true);
$smarty->append('content', array('Style Sheet' => 'style.tpl','Manage Company' => 'company_layout.tpl'), true);
$smarty->display('header.tpl');
?>
