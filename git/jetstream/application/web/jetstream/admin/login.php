<?php

require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
// require_once BASE_PATH . '/include/class.Language.php';
require_once BASE_PATH . '/slipstream/class.User.php';

if (isset($_POST ['btnLogin']) && $_POST ['btnLogin'] == 'Login') {
	$objUser = new User();
	$status = $objUser->dovalidateLogin($_POST);
	$objErr = array();
	if ($status->status == 'FALSE') {
		$status = $status->getStatus();
		foreach ($status ['ERROR'] as $key => $val) {
			$objErr [$key] = $val;
		}
		$smarty->assign('ERROR', $objErr);
		$smarty->assign('editUserId', $_POST ['editUserId']);
	} else {
		header('location:?action=dashboard');
	}
} else {
	$smarty->assign('editUserId', '');
}

$smarty->display('login.tpl');

?>
