<?php
include_once (BASE_PATH . '/slipstream/class.User.php');
include_once (BASE_PATH . '/slipstream/class.OptionSet.php');
include_once (BASE_PATH . '/slipstream/class.Option.php');
include_once (BASE_PATH . '/slipstream/class.AccountMap.php');
include_once (BASE_PATH . '/slipstream/class.ContactMap.php');
require_once BASE_PATH . '/slipstream/class.Section.php';
$objUser = new User ( );
$users = $objUser->getAllUserByCompanyId ($_SESSION['USER']['COMPANYID']);
$result = $objUser->getUserTypeByCompanyId ($_SESSION['USER']['COMPANYID']);

$smarty->assign ('USERS', $users);
$user_type = array ();
$user_type [] = '-';
$user_type [-3] = 'Admin';
$user_level = array ();

foreach ($result as $value) {
	//Array to store LevelNames with levelId as key.
	$user_type [$value ['LevelID']] = $value ['Name'];
	//Array to store LevelNames with levelNumber as key.
	$user_level_name [$value ['LevelNumber']] = $value ['Name'];

	$_SESSION ['USER'] ['USERLEVELNAME'] = $user_level_name;
}
$smarty->assign ( 'USERTYPE', $user_type );
$smarty->assign ( 'COMPANYID', $_SESSION['USER']['COMPANYID']);


$smarty->append('css_include', array('adminStyle.css' => 1, 'menu.css' => 1,'flora.datepicker.css'=>1,'grid.css'=>1,'jqModal.css'=>1), true);
$smarty->append('js_include', array('jquery.js' => 1, 'json.js'=>1,'jquery-ui-personalized-1.5.2.min.js'=>1,'admin_company.js'=>1,'generic_admin.js'=>1,'jquery.form.js'=>1,'grid.base.js'=>1,'jquery.jqGrid.js'=>1,'menu.js'=>1,'user.js'=>1), true);
$smarty->append('content', array('Manage User' => 'manage_user.tpl'), true);

$smarty->display ('header.tpl' );
?>