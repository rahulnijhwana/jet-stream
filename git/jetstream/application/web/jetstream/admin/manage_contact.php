<?php
include_once (BASE_PATH . '/slipstream/class.User.php');
include_once (BASE_PATH . '/slipstream/class.OptionSet.php');
include_once (BASE_PATH . '/slipstream/class.Option.php');
include_once (BASE_PATH . '/slipstream/class.ContactMap.php');
include_once (BASE_PATH . '/slipstream/class.Section.php');
include_once (BASE_PATH . '/slipstream/class.Validation.php');

$objUser = new User ();
$objContactMap = new ContactMap();
$objOption = new Option();
$objOptionSet = new OptionSet();

$contactTextFields = $objContactMap->getFieldsFromContactMapByCompId($_SESSION['USER']['COMPANYID'],'Text');
$contactNumberFields = $objContactMap->getFieldsFromContactMapByCompId($_SESSION['USER']['COMPANYID'],'Number');
$contactBooleanFields = $objContactMap->getFieldsFromContactMapByCompId($_SESSION['USER']['COMPANYID'],'Bool');
$contactSelectFields = $objContactMap->getFieldsFromContactMapByCompId($_SESSION['USER']['COMPANYID'],'Select');
$contactDateFields = $objContactMap->getFieldsFromContactMapByCompId($_SESSION['USER']['COMPANYID'],'Date');

$optionSet = $objOptionSet->getOptionSetByCompanyId($_SESSION['USER']['COMPANYID']);

for($i=0;$i<count($contactTextFields);$i++){
$objValidation = new Validation();
	$fieldType = $objValidation->getFieldTypeByValidationId($contactTextFields[$i]['ValidationType']);

	$contactTextFields[$i]['FieldType'] = $fieldType;
}
for($i=0;$i<count($contactNumberFields);$i++){
$objValidation = new Validation();
	$fieldType = $objValidation->getFieldTypeByValidationId($contactNumberFields[$i]['ValidationType']);

	$contactNumberFields[$i]['FieldType'] = $fieldType;
}

$contactDropDownFields = array();
$contactRadioButtonFields = array();
foreach($contactSelectFields as $fields){

	if($fields['OptionSetID'] == null){

		$options = $objOption->getOptionsByContactMapId($_SESSION['USER']['COMPANYID'],$fields['ContactMapID']);

	}else{
		$objUser = new User ();
		$optionSetDetails = $objOptionSet->getOptionsSetDetailsByOptionSetId($fields['OptionSetID']);

		if($optionSetDetails[0]['AccountMapID'] != NULL){
			$options = $objOption->getOptionsByAccountMapId($_SESSION['USER']['COMPANYID'],$optionSetDetails[0]['AccountMapID']);
		}else if($optionSetDetails[0]['ContactMapID'] != NULL){
			$options = $objOption->getOptionsByContactMapId($_SESSION['USER']['COMPANYID'],$optionSetDetails[0]['ContactMapID']);
		}
	}

	if($fields['OptionType'] == 1){
		$contactDropDownFields[] = array('OptionFields'=>$fields,'OptionValues'=>$options);
	}else{
		$contactRadioButtonFields[] = array('OptionFields'=>$fields,'OptionValues'=>$options);
	}
}

$smarty->assign ( 'OPTIONSETS_JSON', json_encode($optionSet));
$smarty->assign ( 'OPTIONSETS', $optionSet);
$smarty->assign ( 'CONTACTTEXTFIELDS', $contactTextFields);
$smarty->assign ( 'CONTACTNUMBERFIELDS', $contactNumberFields);
$smarty->assign ( 'CONTACTCHECKFIELDS', $contactBooleanFields);
$smarty->assign ( 'CONTACTDROPDOWNFIELDS', $contactDropDownFields);
$smarty->assign ( 'CONTACTRADIOFIELDS', $contactRadioButtonFields);
$smarty->assign ( 'CONTACTDATEFIELDS', $contactDateFields);
$smarty->assign('header_title','Add Contact Fields');
$smarty->append('css_include', array('style.css' => 1, 'menu.css' => 1), true);
$smarty->append('js_include', array('jquery.js' => 1,'jquery.form.js'=>1,'json.js'=>1,'admin_contact.js'=>1,'generic_admin.js'=>1,'jquery.blockUI.js'=>1), true);
$smarty->append('content', array('Manage Contact' => 'manage_contact.tpl'), true);
$smarty->display('header.tpl');

?>