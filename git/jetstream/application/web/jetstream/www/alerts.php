<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

require_once(BASE_PATH . '/include/mpconstants.php');
require_once(BASE_PATH . '/include/class.SessionManager.php');
require_once(BASE_PATH . '/mpower/class.Header.php');
require_once(BASE_PATH . '/mpower/class.Salespeople.php');
require_once(BASE_PATH . '/mpower/class.Alerts.php');
require_once(BASE_PATH . '/mpower/class.Footer.php');
require_once(BASE_PATH . '/include/class.ReportingTree.php');
require_once(BASE_PATH . '/include/class.Language.php');
require_once(BASE_PATH . '/mpower/lib.html.php');
SessionManager::Init();
SessionManager::Validate();

$page_title = 'M-Power&trade; Alerts';

$header = new Header();

$header->SetTitle($_SESSION['tree_obj']->GetPath($_SESSION['login_id']));
//$header->SetSubtabs($_SESSION['tree_obj']->GetLimb($_SESSION['login_id']));

$alerts = new Alerts();

$footer = new Footer();

CreatePage($page_title, array($header, $alerts, $footer));