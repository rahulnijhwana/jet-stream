<?php
	define('BASE_PATH', realpath(dirname(__FILE__) . '/../'));	
	require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';	
	require_once BASE_PATH . '/slipstream/class.EmailTemplate.php';		
	if(!isset($_GET['str']))
		echo 'Invalid Request';
	else
	{
		$objEmail=new EmailTemplate();
		$objEmail->token=$_GET['str'];
		$objEmail->Init();
		$smarty->assign_by_ref('module', $objEmail);
		$output = $smarty->fetch('email.tpl');
		echo $output;		
	}
	
?>