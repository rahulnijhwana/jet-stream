<?php
/* 
 * Jetstream main control page
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

/*
if (isset($_COOKIE['SN'])) {
	session_name($_COOKIE['SN']);
}
session_start();
*/

require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/class.ReportingTree.php';

/* set the variable to true, so that in session manager class, when it checks for the eula
it can know which application eula needs to be checked.
If this value is checked the session will check the Jetstream eula. */
$app_slipstream = true;
SessionManager::Init();

SessionManager::Validate();

if (empty($_SESSION['USER']['UTC_UPDATE'])) {
	require BASE_PATH . '/slipstream/update.php';
	exit;
}

$action = '';
if (!isset($_SESSION['USER'])) {
	require BASE_PATH . '/www/logout.php';
	exit;
}
elseif (isset($_GET['action'])) {
	$action = strtolower($_GET['action']);
}

switch ($action) {

	case 'company' :
	case 'contact' :
		// regular company/contact page load
		require BASE_PATH . '/slipstream/page_company_contact.php';
		break;

	case 'pcompany' :
	case 'pcontact' :
		// using navigation links - set to previous page and last record of that page
		require BASE_PATH . '/slipstream/page_company_contact.php';
		break;

	case 'ncompany' :
	case 'ncontact' :
		// using navigation links - set to next page and first record of that page
		require BASE_PATH . '/slipstream/page_company_contact.php';
		break;

	case 'dataimport' :
		require BASE_PATH . '/slipstream/page_import_event.php';
		break;
		
	case 'task':
	case 'event' :
		require BASE_PATH . '/slipstream/page_event.php';
		break;

	case 'monthly' :
	case 'weekly' :
	case 'days' :
	case 'daily' :
		require BASE_PATH . '/slipstream/page_calendar.php';
		break;

	case 'notes' :
		require BASE_PATH . '/slipstream/page_notes.php';
		break;
		
	case 'opptracker' :
		require BASE_PATH . '/slipstream/page_opp_search.php';
		break;	
			
	case 'opportunity' :
		require BASE_PATH . '/slipstream/page_opportunity.php';
		break;		

	case 'email_incoming' :
		require BASE_PATH . '/slipstream/page_email_incoming.php';
		break;
	case 'email_outgoing' :
		require BASE_PATH . '/slipstream/page_email_outgoing.php';
		break;
	
	case 'email_hold' :
		require BASE_PATH . '/slipstream/page_email_hold.php';
		break;

	case 'pending_events' :
		require BASE_PATH . '/slipstream/page_pending_events.php';
		break;
	case 'template':
		require BASE_PATH . '/slipstream/page_email_template.php';
		break;
	case 'permission':
		require BASE_PATH . '/slipstream/page_calendar_permission.php';
		break;
	case 'vacation':
		require BASE_PATH . '/slipstream/page_vacation_settings.php';
		break;
	case 'notes_number':
		require BASE_PATH . '/slipstream/page_note_settings.php';
		break;
	case 'imap_settings':
		require BASE_PATH . '/slipstream/page_imap_settings.php';
		break;	
	case 'call_report':
		require BASE_PATH . '/slipstream/page_call_report.php';
		break;
	case 'advancedashboard':
		require BASE_PATH . '/slipstream/page_advancedashboard.php';
		break;
	case 'superadvancedashboard':
		require BASE_PATH . '/slipstream/page_superadvancedashboard.php';
		break;
	case 'filesearch':
		require BASE_PATH . '/slipstream/page_filesearch.php';
		break;
	case 'google_connect':
		require BASE_PATH . '/slipstream/page_google_connect.php';
		break;
	/* Start : JET-37 */		
	case 'google_sync':
			require BASE_PATH . '/slipstream/page_google_sync.php';	
		break;
	/* End : JET-37 */
	default :
	
		if(isset($_COOKIE['DashboardSearchType']) && ($_COOKIE['DashboardSearchType'] == 'advance')) { 
			require BASE_PATH . '/slipstream/page_superadvancedashboard.php';
		}
		else {
			require BASE_PATH . '/slipstream/page_dashboard.php';
		}
		
}
