<?php
/* batch processing file */
define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));
require_once '../include/class.GoogleConnect.php';
require_once '../include/class.GoogleBackgroundPush.php';

$file = (isset($_GET['file'])) ? '/webtemp/' . $_GET['file'] : '' ;
if ( $file == '' ) {
    error_log('Contact batch push : no file error'); 
    exit;
}

$userid = (isset($_GET['userid'])) ? $_GET['userid'] : '' ;
if ( $userid == '' ) {
    error_log('Contact batch push : no userid'); 
    exit;
}

$groupid = (isset($_GET['groupid'])) ? $_GET['groupid'] : '' ;
if ( $groupid == '' ) {
    error_log('Contact batch push : no group id'); 
    exit;
}
$googleConnect = new GoogleConnect($userid, false);
$oBatch = new GoogleBackgroundPush( $googleConnect, $file, $groupid, $userid);

echo json_encode(array('code'=>'200'));
