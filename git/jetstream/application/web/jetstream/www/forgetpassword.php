<?php

define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.Language.php';
require_once BASE_PATH . '/include/lib.security.php';

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<title>M-Power&trade;</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
		<style type="text/css">
			.body {
				font-family:arial,sans-serif;
				margin-left:3em;
				margin-right:5em;
			}
			
			div.errormsg {
				color:red;
				font-family:arial,sans-serif;
				font-size:smaller;
			}
			div.success {
				color: black;
				font-family:arial,sans-serif;
				font-size: smaller;
			}		
			font.errormsg {
				color:red;
				font-family:arial,sans-serif;
				font-size:smaller;
			}
		</style>	
		
	</head>
	<body>	
<?php



if (isset($_POST['company']) && isset($_POST['username']) && !empty($_POST['company']) && !empty($_POST['username'])) {
	$company = $_POST['company'];
	$username = $_POST['username'];	
	
	$sql = 'SELECT L.ID LoginID, P.email EmailID
			FROM company C, Logins L 
			LEFT JOIN people P ON P.PersonID = L.PersonID
			WHERE DirectoryName = ? AND C.CompanyID = L.CompanyID AND L.UserID = ?';	

	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $company), array(DTYPE_STRING, $username));
	//echo '<br>'.$sql.'<br>';
	$person = DbConnManager::GetDb('mpower')->GetOne($sql);

	if ($person instanceof MpRecord) {		
		$login_id = $person->LoginID;
		$to = $person->EmailID;
		$from = "support@mpasa.com";
		$email_pattern = '/^[^@\s<&>]+@([-a-z0-9]+\.)+[a-z]{2,}$/i';
		if (preg_match($email_pattern, $to)) {		
			$token = getToken($login_id);			
			//save the token			
			saveToken($token, $login_id);
			
			$link = 'https://www.mpasa.com/jetstream/confirmforgetpassword.php?token=' . base64_encode($token);	
			//$link = 'http://localhost/asa/confirmforgetpassword.php?token=' . base64_encode($token);	
			$subject = 'Password Change Confirmation';
			$body = "Please follow the following link to get your new password" . "\n";
			$body .= $link . "\n\n";
			
			$host = "smtp.fusemail.net";
			$port = "25";
			$username = "nyoung";
			$password = "Popeye55";
			
			$headers = array ('From' => $from, 'To' => $to, 'Subject' => $subject);
			
			include 'Mail.php';
			$smtp = Mail::factory('smtp',
				array ('host' => $host,
					'port' => $port,
					'auth' => true,
					'username' => $username,
					'password' => $password)
				);
			
			if (@$smtp->send($to, $headers, $body)) {
				echo "To initiate the password reset process, please follow the instructions sent to your email address.";
			} else {
				echo 'We have not been able to process your access request. Please contact <a href="mailto:support@mpasa.com">Support</a>.';			
			} 
		} else {
			echo 'We have not been able to process your access request. Please contact <a href="mailto:support@mpasa.com">Support</a>.';			
		}
		exit;
	}
}

?>

	<div class="body">
		<b>Forgot your password?</b><br>
		<table border="0" cellpadding="0" cellspacing="0">
			<tbody>
			<tr valign="top">
				<td>
				<form action="forgetpassword.php" method="post" id="forgotpasswd">
				<table border="0" cellpadding="0" cellspacing="0" width="" height="1%">
				<tbody>
				<tr>
				<td style="padding-top: 10px; padding-bottom: 10px;" valign="top">
				<font size="-1">Please enter your Mpower/Jetstream username to start the password recovery process.</font>
				<br>
					<table border="0" cellpadding="2" cellspacing="5"><tbody>
					<tr>
					<td align="center" valign="top">
					<table bgcolor="#cbdced" border="0" cellpadding="2" cellspacing="0" width="100%">
					<tbody><tr><td>
						<table border="0" cellpadding="5" cellspacing="0" width="100%">
						<tbody>
						
							<tr>
							<td align="center" bgcolor="#ffffff" valign="top" nowrap="nowrap">
							<b><font size="-1"><?php echo Language::__get('Company')?>:</font>&nbsp;&nbsp;</b></td>
							<td align="left" bgcolor="#ffffff" colspan="2"><div class="errorbox-good">
							<input name="company" value="" size="15" type="text">
							</div></td>
							</tr>
						
							<tr><td align="center" bgcolor="#ffffff" valign="top" nowrap="nowrap">
							<b><font size="-1" face="Arial, sans-serif"><?php echo Language::__get('Username')?>:</font>&nbsp;&nbsp;</b></td>
							<td align="left" bgcolor="#ffffff"><div class="errorbox-good">
							<input name="username" value="" size="15" type="text">
							</div></td>
							<td align="left" bgcolor="#ffffff"><input value="Submit" type="submit"></td>
							</tr>
						</tbody>
						</table>
					</td></tr>
					</tbody>
					</table>
				</td></tr>
				</tbody>
				</table></td></tr>
				</tbody>
				</table>
				</form>
			</td>		
			</tr>
			</tbody>
		</table>
		
	</div>

	</body>
</html>

<?php
// generates token
function getToken($login_id) {
	$unique = md5(uniqid(rand(), true));	
	$token = crypt($login_id . $unique . trim($_SERVER['HTTP_USER_AGENT']));
	return $token;
}

// saves the token in database
function saveToken($token, $login_id) {
	$sql = "UPDATE logins SET ForgetPasswordToken = ? WHERE ID = ?";				
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $token), array(DTYPE_INT, $login_id));
	//echo '<br>'.$sql.'<br>';
	$data = DbConnManager::GetDb('mpower')->Exec($sql);
}


/*
function curPageURL() {
	$pageURL = 'http';
	if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}	
	$pageURL .= "://";
	
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}
*/

?>
