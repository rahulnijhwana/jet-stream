<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

require_once(BASE_PATH . '/include/mpconstants.php');
require_once(BASE_PATH . '/include/class.SessionManager.php');
require_once(BASE_PATH . '/mpower/class.Header.php');
require_once(BASE_PATH . '/mpower/class.Salespeople.php');
require_once(BASE_PATH . '/mpower/class.Trends.php');
require_once(BASE_PATH . '/mpower/class.Footer.php');
require_once(BASE_PATH . '/include/class.ReportingTree.php');
require_once(BASE_PATH . '/include/class.Language.php');
require_once(BASE_PATH . '/mpower/lib.html.php');
SessionManager::Init();
SessionManager::Validate();

$page_title = 'M-Power&trade; Trends';

$header = new Header();

$query = "SELECT PersonID, LastName, FirstName 
			FROM people 
			WHERE Deleted = 0 AND 
				  isSalesperson = 1 AND 
				  SupervisorID = (select L.PersonID from Logins L where L.ID = ?) 
			ORDER BY LastName";

$query = SqlBuilder()->LoadSql($query)->BuildSql(array(DTYPE_INT, $_SESSION['currentuser_loginid']));
$data = DbConnManager::GetDb('mpower')->Exec($query);

$submenu = array();
if(count($data) > 0) {
	$submenu['All'] = SessionManager::CreateUrl('legacy/board/select_trend.php','target=-1');
	foreach($data as $person) {
		$fullname = $person['FirstName'].' '.$person['LastName'];
		$submenu[$fullname] = SessionManager::CreateUrl('legacy/board/select_trend.php','target='.$person['PersonID']);
	}
}

$subtabs = array();
$subtabs['Trends'] = $submenu;
//$header->SetSubtabs($subtabs);


$salespeople = new Salespeople();
$salespeople->url = 'legacy/board/select_trend.php';

$trends = new Trends();
$trends->target = 3157;

$footer = new Footer();

CreatePage($page_title, array($header, $footer));
