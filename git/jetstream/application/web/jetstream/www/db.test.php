<?php
//phpinfo(); exit;
include_once $_SERVER['DOCUMENT_ROOT'] . '/./include/mpconstants.php';

//echo '<pre>'; print_r(shell_exec('whoami')); echo '</pre>'; exit;
//echo '<pre>'; print_r(MP_DATABASE_SERVER); echo '</pre>'; exit;
	
// DB connect test
$conn = mssql_pconnect(MP_DATABASE_SERVER, MP_DATABASEU, MP_DATABASEP);

if (!$conn || !mssql_select_db(MP_DATABASE,$conn)) {
    die('Unable to connect or select database!');
}

//$sql = "SELECT @@VERSION";
//$sql = "Select * From INFORMATION_SCHEMA.COLUMNS Where TABLE_NAME = 'people'";
//$sql = "SELECT TOP 10 * FROM questions";
//$sql = "Select TOP 10 * From dbo.company";

$sql = "
SELECT PersonID, UserID, Color, FirstName
			, LastName, IsSalesperson, SupervisorID, GroupName, 
			Rating, Level,
			StartDate, 
			AvgVLevel, 
			Email, AverageSalesCycle, FirstMeeting, AverageSale
			, ClosingRatio, TotalRevenues, TotalSales, UnreportedAvgSale
			, UnreportedSalesPerMonth, MasterAssignee, OnVacation
			, CONVERT(VARCHAR(10), VacationReturn, 101) AS VacationReturn
		FROM people left outer join
			(SELECT PersonID AS PID, AVG(CAST(Vlevel AS float)) AS AvgVLevel
			FROM opportunities
			WHERE companyid = 508
			AND vlevel > 0
			AND category NOT IN (9,6)
			GROUP BY personid) AS Valuations ON people.PersonID = Valuations.PID
		WHERE companyid = 508 AND deleted = 0";
//$sql = "SELECT * FROM people where personid = 8448";

$result = mssql_query($sql) or die('A error occured: ' . mysql_error());

$idx = 1;
while($row = mssql_fetch_array($result)) {
	echo '<pre>'; echo "[$idx] "; print_r($row); echo '</pre>';
	$idx++;
}

mssql_close($conn);
?>