<?php
/**
 * Settings
 *
 * Sets default values to allow M-Power to easily move from server to server.
 * 
 * @package Shared
 */

$db_server = '';
$log_file = '';

if(strpos($_SERVER['SERVER_SOFTWARE'], "Win32") !== FALSE) { 
    // Windows default settings
    $db_server = "128.121.65.35";
    $log_file = "c:\sourcecode\mpower.log";
    $file_separator = '\\';
}
else {
    // Unix Settings
    $db_server = "ASA-VERIO";
    $log_file = "/var/log/httpd/mpower_log"; 
    $file_separator = '/';
}

$webtemp = '/webtemp/' ;
$path = dirname($webtemp);

//$cache_path = $path .'webtemp/newcache/jetstream/jetcache/';
$cache_path = $path .'webtemp/newcache/';



//$cache_path = 'C:\\webtemp\\newcache\\jetcache\\';
//$ajax_cache_path = 'C:\\webtemp\\newcache\\ajax\\jetcache\\';
$ajax_cache_path = $path . 'webtemp/newcache/jetstream/ajax/';

//echo '****'.$cache_path;
?>
