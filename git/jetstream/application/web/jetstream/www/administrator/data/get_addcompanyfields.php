<?php

include_once (BASE_PATH . '/slipstream/class.OptionSet.php');
include_once (BASE_PATH . '/slipstream/class.AccountMap.php');

$objOptionSet = new OptionSet();
$objAccountMap = new AccountMap();

$selectedKeywordFields = $objAccountMap->getFieldsFromVcardKeywordMap( $mpower_companyid, 1 );
$allKeywordFields = $objAccountMap->getFieldsFromVcardKeywordMap( $mpower_companyid, 0 );

$optionSet = $objOptionSet->getOptionSetByCompanyId($mpower_companyid);

echo 'var ALLKEYWORDFIELDS = '.json_encode( $allKeywordFields ).';';
echo 'var SELECTEDKEYWORDFIELDS = '.json_encode( $selectedKeywordFields ).';';
echo 'var COMPANYID = '.$mpower_companyid .';';
echo 'var OPTIONSETS_JSON = '.json_encode($optionSet).';';
echo 'var OPTIONSETS = '.json_encode($optionSet).';';
?>

	var options = OPTIONSETS_JSON;
	try
	{
		options=eval(options);
	}
	catch(e)
	{
		options=JSON.decode(options);
	}	
