<?php
/**
 * @package Data
 */
include_once('_base_utils.php');
include_once('_insert_utils.php');

if (!check_admin_login())
{
	close_db();
	exit();
}

//$companyid = (int)$mpower_companyid;
$person_id = (int) $_POST['PersonID'];

$sql = "SELECT PeopleEmailAddrID, EmailAddr FROM PeopleEmailAddr WHERE PersonID = $person_id AND IsPrimaryEmailAddr IS NULL";
$emails_rs = mssql_query($sql);

echo '<table width="100%" class="SubGroup" id="addEmails">';
$count = 1;
while ($row = mssql_fetch_array($emails_rs)) {
	echo '<tr id="EmailRow_'.$count.'">';
	echo '<td><input type="hidden" name="email_id_' . $count . '" id="email_id_' . $count . '" value="' . $row['PeopleEmailAddrID'] . '">
			<input id="email_' . $count . '" name="email_'.$count.'" type="text" size="20" maxlength="50" value="' . $row['EmailAddr'] . '"></td>';
	echo '<td id="emailTD_' . $count . '"><input type="button" value="Remove" onClick="removeEmail(\'EmailRow_' . $count . '\')"></td>';
	echo '</tr>	';
	$count++;
}

close_db();

?>										
<tr id="EmailRow_<?=$count?>">
	<td><input type="hidden" name="email_id_<?=$count?>" id="email_id_<?=$count?>" value="0"><input id="email_<?=$count?>" name="email_<?=$count?>" type="text" size="20" maxlength="50"></td>
	<td id="emailTD_<?=$count?>"><input type="button" value="Add" onClick="addEmail()"></td>
</tr>
</table>
