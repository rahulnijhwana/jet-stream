<?php
include_once (BASE_PATH . '/slipstream/class.OptionSet.php');
include_once (BASE_PATH . '/slipstream/class.AccountMap.php');
include_once (BASE_PATH . '/slipstream/class.ContactMap.php');

$objAccountMap = new AccountMap();
$objOptionSet = new OptionSet();
$objContactMap = new ContactMap( );

$selectedKeywordFields = $objContactMap->getFieldsFromVcardKeywordMap( $mpower_companyid, 1 );
$allKeywordFields = $objContactMap->getFieldsFromVcardKeywordMap( $mpower_companyid, 0 );
$allAccountFields = $objAccountMap->getFieldsFromAccountMapByCompId($mpower_companyid,'%');
$optionSet = $objOptionSet->getOptionSetByCompanyId($mpower_companyid);
/*Start JET-37*/
//$googleoptionSet = array();
/*End JET-37*/
echo 'var ALLKEYWORDFIELDS = '.json_encode( $allKeywordFields ).';';
echo 'var SELECTEDKEYWORDFIELDS = '.json_encode( $selectedKeywordFields ).';';
echo 'var ALLACCOUNTFIELDS = '.json_encode($allAccountFields).';';
echo 'var COMPANYID = '.$mpower_companyid .';';
echo 'var CONTACTOPTIONSETS_JSON = '.json_encode($optionSet).';';
echo 'var CONTACTOPTIONSETS = '.json_encode($optionSet).';';
/*Start JET-37*/
//echo 'var GOOGLECONTACTOPTIONSETS = '.json_encode($googleoptionSet).';';
/*End JET-37*/
?>

	var contactOptions = CONTACTOPTIONSETS_JSON;
	try
	{
		contactOptions=eval(options);
	}
	catch(e)
	{
		contactOptions=JSON.decode(options);
	}	
