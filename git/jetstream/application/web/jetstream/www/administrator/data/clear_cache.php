<?php
/**
 * @package Data
 */

// this function is used to clear the jet cache.
function clear_jet_cache($cache_path) {
	global $mpower_companyid;

	if($mpower_companyid > 0)	{
		$cache_directories = directoryToArray( $cache_path, true ) ; 
		
		foreach ($cache_directories as $cache_directory){
//		file_put_contents('/webtemp/remove.txt',$cache_directory."\n", FILE_APPEND);

		   if ( stristr($cache_directory, $mpower_companyid) !== FALSE ){

			if(is_dir($cache_directory)) {
				remove_dir($cache_directory);
			}

		   }
		//	$jetcache_path = $cache_directory.$mpower_companyid;
		}
		// end remove 
	}	
}


function remove_dir($dir_path) {
	global $file_separator;
	
	if(is_dir($dir_path)) {		
		if($dir = @opendir($dir_path)) {
			while(($file = readdir($dir)) !== false) {
				if($file != ".." && $file != ".") {
					$file_path = $dir_path.$file_separator.$file;
					if(strpos($_SERVER['SERVER_SOFTWARE'], "Win32") !== FALSE) { 
						$file_path = str_replace('\\', '\\\\', $file_path);
					}
					//file_put_contents('/webtemp/remove.txt',$file_path."\n", FILE_APPEND);
					if(file_exists($file_path)) {
						unlink($file_path);
					}
				}
			}
			closedir($dir);
			//file_put_contents('C:\\test.log',$dir_path."\n", FILE_APPEND);
			rmdir($dir_path);
		}
	}
}

function directoryToArray( $directory, $recursive = false) {
	$array_items = array();
	if ($handle = opendir($directory)) {
		while (false !== ($file = readdir($handle))) {
			if ($file != "." && $file != "..") {
				if (is_dir($directory. "/" . $file)) {
					if($recursive) {
						$array_items = array_merge($array_items, directoryToArray($directory. "/" . $file, $recursive));
					}
					$file = $directory . "/" . $file;
					$array_items[] = preg_replace("/\/\//si", "/", $file);
				} else {
					$file = $directory . "/" . $file;
				//	$array_items[] = preg_replace("/\/\//si", "/", $file);
				}
			}
		}
		closedir($handle);
	}
	return $array_items;
}
/*
function resetCache(){
	$clearCache = 'rm -rf ' . $unixpath ; 
	$unixpath = "/webtemp/newcache/*" ; 

	exec($clearCache); 
}
*/
function resetCacheNew($directory)
{
    foreach(glob("{$directory}/*") as $file)
    {
        if(is_dir($file)) { 
            resetCacheNew($file);
        } else {
            unlink($file);
        }
    }
    rmdir($directory);
}

function resetCache(){
	$clearCache ='opt/lampp/htdocs/jet-stream/git/jetstream/application/web/jetstream/newcache/';
	var_dump(exec("rm -rf {$clearCache}"));
       // $clearCache = '/home/ubuntu/clear.sh';///home/jettest/public_html/git/jetstream/application/web/jetstream/newcache
        var_dump(exec($clearCache));//exit('pp');///opt/lampp/htdocs/jet-stream/git/jetstream/application/web/jetstream/newcache
}

if($_POST['clear_cache'] == 1){
	$dir = $_SERVER["DOCUMENT_ROOT"].'/git/jetstream/application/web/jetstream/newcache/jetstream';
	resetCacheNew($dir);
	//resetCache();
	echo 'Cache cleared successfully';
}

