<?php
function get_all_params(){
	$param_arr = array("NumberFormat"=>get_number_format(),"AllYear"=>get_year());
	return $param_arr;
}

function get_number_format(){
	$number_format_arr = array("None","Currency","Percent");
	return $number_format_arr;
}

function get_year(){
	$year_arr = array();
	$start_year = 1930;
	$current_year = date('Y');
	for($i=$start_year;$i<($current_year+11);$i++){
		array_push($year_arr,$i);
	}
	return $year_arr;
}

?>
