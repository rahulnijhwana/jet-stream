<?php

/**
 * @package Data
 */
include_once('_base_utils.php');
include_once('_insert_utils.php');

if (!check_admin_login())
{
	close_db();
	exit();
}

$street = (int) $_POST['mapStreet'];
$city = (int) $_POST['mapCity'];
$state = (int) $_POST['mapState'];
$companyid = (int) $_GET['companyId'];
$action = $_GET['action'];

if ($action == 'add') {
	$sql = "IF EXISTS (SELECT * FROM AddressMap WHERE CompanyID = $companyid)
				UPDATE AddressMap
				   SET Street = $street, 
					   City = $city, 
					   State = $state
				 WHERE CompanyID = $companyid
			ELSE
				INSERT INTO AddressMap
					   (CompanyID
					   ,Street
					   ,City
					   ,State)
				 VALUES ($companyid, $street, $city, $state)";	
	if(mssql_query($sql)) echo 'OK';
	else echo 'Error';
	close_db();
}
