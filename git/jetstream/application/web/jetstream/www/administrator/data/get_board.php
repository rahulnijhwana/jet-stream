<?php
/**
 * @package Data
 */

function timehack($term) {
	global $time_start;
	echo "// timehack - $term: " . round(microtime(true) - $time_start, 3) . " seconds\n";
	$time_start = microtime(true);
}

// $time_start = microtime(true);
// $tot_time = $time_start;
// OPTIMIZE list to do
// get_valuations.php it queries every time to make sure that ALL companies have 20 valuations, and adds them if not
// why is there a get_sources.php and a get_sources2.php

// globals available for processing and their origin
// $currentuser_data	_base_utils.php contains the current user's row of data from the people table
// $currentcompany		get_company.php	contains the current company's row of data from the people table
// $currentuser_loginid	the id from the login table of the current user

include_once('_base_utils.php');
// timehack('_base_utils');
include_once('_query_utils.php');
include_once('autorevive.php');

$sib_mode = (isset($_REQUEST['sib_mode'])) ? $_REQUEST['sib_mode'] : null;
$do_tree = (isset($_REQUEST['do_tree'])) ? $_REQUEST['sib_mode'] : null;

if (!check_user_login())
{
	close_db();
	exit();
}

// timehack('User Sec Check');

$message='';	// if this message is not empty, it will be displayed to the user in an alert
// Moved this here from login.php
// The reason is that is the user bookmarks the board page, login.php is
// not used and the updating of LastUsed is bypassed. This screws up the
// Use Alert
$lastused = date('Y-m-d H:i:s');

// check for special case where all affiliates are shown
if ($currentuser_loginid==7687)
{
	print("g_showaffiliates=true;\n");

	$company_lean_and_mean = false;

	include('get_company.php');

	include('get_user.php');
	include('get_login.php');

	$sql = "select DealID from MPGetDeals where ActualCloseDate is null or ActualCloseDate >'03/22/06'";
	$Deals=make_in_clause($sql);
	if(!strlen($Deals)) $Deals = '-1';
	$sql = "SELECT * FROM opportunities WHERE DealID IN ($Deals)";
	dump_sql_as_array('g_opps', $sql);

	$sql = "SELECT DISTINCT PersonID from opportunities where DealID in ($Deals)";
	$Peeps=make_in_clause($sql);

	$sql = "SELECT * FROM people WHERE PersonID IN ($Peeps)";
	$sql .= " ORDER BY LastName, FirstName";
	dump_sql_as_array('g_people', $sql);

	$sql = "SELECT DISTINCT Companyid FROM opportunities where DealID in ($Deals)";
	$companyList=make_in_clause($sql);

	$sql = "SELECT * FROM company where CompanyID in ($companyList)";
	dump_sql_as_array('g_affcompany', $sql);

	include('get_products.php');
	include('get_levels.php');
	include('get_submilestones.php');
	include('get_valuations.php');
	include('get_sources.php');
	include('get_sources2.php');
	include('get_opp_products_xrefs.php');

	dump_nothing_as_array(array('g_salescycles', 'g_snoozealerts','g_trends','g_alias_owner_choices','g_grouppath','g_analysisCurves'));

	print("window.g_accessType = 2;\n");

}
else
{
	print("g_showaffiliates=false;\n");

	CheckAutoRevive($mpower_companyid);
	// timehack('CheckAutoRevive');

	$companyList="('$mpower_companyid')";


	echo "//No Updqate Last Used: $noUpdateLastUsed\n";
	if (!$noUpdateLastUsed)
	{
		// N O R M A L   P R O C E S S I N G
		$LastUsedIP = $_SERVER['REMOTE_ADDR'];

		$sql = "update people set LastUsed = '$lastused', LastUsedIP = '$LastUsedIP'";
		$sql .= " where PersonID = (select PersonID from Logins";
		$sql .= " where PersonID=LoginID and convert(varbinary(50),UserID) = convert(varbinary(50),'$mpower_userid') and CompanyID = '$mpower_companyid');";
		$result = mssql_query($sql);

		//Since we need to distinguish alias logins from actual manager logins, we've added a LastUsed
		//field to the Logins table.  We'll update it here--and IT will trigger the generation of log records,
		//rather than the people table.  Alias logins will place the alias ID in the logint field
		//which is null otherwise

		$sql = "update logins set LastUsed = '$lastused', LastUsedIP = '$LastUsedIP' where UserID = '$mpower_userid' and CompanyID = '$mpower_companyid'";
		$result = mssql_query($sql);
		echo "// Got to login update: $sql\n";
	}

	// timehack('Update Last Used');


	if (isset($mpower_effective_userid) && $mpower_effective_userid != '')
		$currentuser_personid = $mpower_effective_userid;

	$company_lean_and_mean = false;

	include('get_company.php');
	// timehack('Company');

	// check CM interface before retrieving data, so that any changes to the DB will be reflected in the data retrieved
//	$CMInterface=$currentCompany['CMInterface'];
//
//	if ($CMInterface=='1')
//	{
//		$CMPath=$currentCompany['CMPath'];
//		include_once("../$CMPath/cm_interface.php");
//		$CMInterface=CM_Connect($currentCompany['CMLogin'],$currentCompany['CMPass']);
//		if ($CMInterface)
//		{
//			CM_SetUsers();		// try to get CMID's for any users that don't have them
//		}
//	}
//	else $CMInterface = false;

	$CMInterface = false;

	include('get_user.php');
	// timehack('User');
	include('get_login.php');
	// timehack('Login');
	include('get_grouppath.php');
	// timehack('Group Path');
	include('get_products.php');
	// timehack('Products');
	include('get_levels.php');
	// timehack('Levels');
	include('get_trends.php');
	// timehack('Trends');
	include('get_analysis_curves.php');
	// timehack('Analysis Curves');
	include('get_submilestones.php');
	// timehack('Submilestones');
	include('get_msanalysisquestions.php');
	// timehack('MS Analyis Questions');
	include('get_valuations.php');
	// timehack('CValuations');
	include('get_sources.php');
	// timehack('Sources');
	include('get_sources2.php');
	// timehack('Sources2');
	include('get_reports_company_xref.php');
	// timehack('Reports Company Xref');
	// shouldn't this be after the opps selection, and only return the xref for opps on this board?
	// include('get_opp_products_xrefs.php');
	// // timehack('Opp Products Xref');

	dump_sql_as_array('g_alias_owner_choices', "select Logins.Name, Logins.UserID, Logins.PasswordZ, (people.FirstName+' '+people.LastName) as AliasOfName from Alias2OwnerXRef, Logins, people where Alias2OwnerXRef.CompanyID = $mpower_companyid and Alias2OwnerXRef.PersonID = $currentuser_personid and Alias2OwnerXRef.LoginID = Logins.ID and Logins.PersonID = people.PersonID");
	// timehack('Alias Owner Choices');

	print("// --------------------\n\n");


	$period = 'm';
	$result = mssql_query("select ClosePeriod from company where CompanyID = '$mpower_companyid'");
	if ($result && ($row = mssql_fetch_assoc($result))) $period = $row['ClosePeriod'];

	$now = getdate();
	$first = mktime(0, 0, 0, $now['mon'], 1, $now['year']);
	if ($period == 'm') {
		$first = strtotime('-2 months', $first);
	} else {
		$dif = (((int) $now['mon'] - 1) % 3) + 6;
		$first = strtotime("-$dif months", $first);
	}

 	$first = date("Y/m/d", $first);

	if (isset($mpower_effective_userid) && $mpower_effective_userid != '')
	{
		$sql = "SELECT * FROM people WHERE PersonID=$mpower_effective_userid";
		$result = mssql_query($sql);
		$result_data = mssql_fetch_assoc($result);
		dump_assoc_as_array('g_effective_user', $result_data);
	}

	if (isset($_COOKIE['flat_view_id']) && $_COOKIE['flat_view_id'] != '')
	{
		print("\n//flat_view_id=".$_COOKIE['flat_view_id']."\n");
		$peeps = getSpPeepsFlat($_COOKIE['flat_view_id']);
	}
	else
	{
		$do_tree=(2 < $currentuser_level && (!isset($mpower_effective_userid) || $mpower_effective_userid == ''))
			|| (isset($mpower_effective_userid) && $sib_mode=='tree');
		if ($do_tree)
			$sql = "select PersonID from people where CompanyID='$mpower_companyid' AND Deleted = '0'";
		else
		{
			$sql = "select PersonID from people where ";
			$sql .= " (PersonID='$currentuser_personid' AND (IsSalesperson='1' OR Level='1'))";
			$sql .= " OR (SupervisorID = '$currentuser_personid' AND Deleted = '0' AND IsSalesperson='1' AND (Level='1' or AppearsOnManager='1'))";
		}
		$peeps = make_in_clause($sql);
	}
	if (!strlen($peeps))
	{
		dump_nothing_as_array(array('g_opps', 'g_people', 'g_salescycles', 'g_snoozealerts'));
		close_db();
		print("g_message='Nobody assigned to this dashboard.';");
		exit();
	}

	// these variables are only used for CM interface purposes
//	$CMSalesList=array();
//	$CMSalesMap=array();
//	$CMSalesCMID=-1;
//	$CMSalesPersonID=-1;

	$sql = "SELECT * FROM people WHERE CompanyID = '$mpower_companyid'";
	$sql .= " AND PersonID IN ($peeps) ORDER BY LastName, FirstName";

	// $sql = "SELECT people.*, logins.LastUsed as RealLastUsed
	//	FROM people left join logins on people.personid = logins.loginid 
	//	WHERE people.CompanyID = '$mpower_companyid' AND people.PersonID IN ($peeps) 
	//	ORDER BY people.LastName, people.FirstName";

	$result = mssql_query($sql);
	if ($result)
	{
		if ($CMInterface)
		{
			dump_resultset_as_array('g_people', $result, BuildCMSalesList);

			if (!$do_tree)
			{
				$msg = CM_SetOpps($CMSalesList,$CMSalesMap);
				if ($msg)
					$message .= $msg;
			}
		}
		else dump_resultset_as_array('g_people', $result);
	}

	// timehack('People');

	
	if ($do_tree) {
		dump_nothing_as_array(array('g_opps', 'g_salescycles', 'g_snoozealerts'));
	} else {
//		$sql = "SELECT DealID FROM opportunities WHERE CompanyID = '$mpower_companyid'
//			AND (Category != '6' OR ActualCloseDate >= cast('$first' as datetime))
//			AND (Category != '9' OR RemoveReason is NULL or AutoReviveDate is not NULL)
//			AND PersonID IN ($peeps)";
//		$Deals = make_in_clause($sql);
//		if(!strlen($Deals)) $Deals = '-1';
//		$sql = "SELECT * FROM opportunities WHERE DealID IN ($Deals) order by Company";
//		dump_sql_as_array('g_opps', $sql);

		$sql = "SELECT * FROM opportunities WHERE CompanyID = '$mpower_companyid'
			AND (Category != '6' OR ActualCloseDate >= cast('$first' AS datetime))
			AND (Category != '9' OR AutoReviveDate IS NOT NULL)
			AND PersonID IN ($peeps) order by Company";

		$result = mssql_query($sql);
		if ($result) {
			$deal_array = array();
			while($row = mssql_fetch_assoc($result)) {
				$deal_array[] = $row['DealID'];
			}
			$Deals = implode(",", $deal_array);
			dump_resultset_as_array('g_opps', $result);

			if (count($deal_array) > 0) {
				$sql = "SELECT * FROM Opp_Product_XRef where CompanyID IN ($companyList) AND DealID in (" . implode(",", $deal_array) . ") order by DealID, Seq ";
				dump_sql_as_array('g_opp_products_xref', $sql);
			}
		}
		
		// dump_sql_as_array('g_opps', $sql);
	}
	// timehack('Opps');

//	if (isset($Deals) && strlen($Deals) > 0) {
//		$sql = "SELECT * FROM Opp_Product_XRef where CompanyID IN ($companyList) AND DealID in ($Deals) order by DealID, Seq ";
//		// echo "//$sql\n";
//		dump_sql_as_array('g_opp_products_xref', $sql);
//	} else {
//		echo 'g_opp_products_xref = [];';
//	}
	// timehack('Opp Product XRef');
	
	// why do we need this query?
	if (isset($mpower_effective_userid) && $mpower_effective_userid != '') {
		$result = mssql_query("SELECT Level FROM people WHERE PersonID = $mpower_effective_userid");
		$eff_data = mssql_fetch_assoc($result);
		$currentuser_level = $eff_data['Level'];
		// timehack('Level');
	}


	if (!$do_tree) {
		$sql = "SELECT PersonID,ProductID,SalesCycleLength,StandardDeviation FROM salescycles WHERE CompanyID = '$mpower_companyid' AND PersonID IN ($peeps)";
		dump_sql_as_array('g_salescycles', $sql);
		// timehack('salescycles');
	//	dump_nothing_as_array(array('g_salescycles'));

		$sql = "SELECT * FROM snoozealerts WHERE PersonID IN ($peeps)";
		dump_sql_as_array('g_snoozealerts', $sql);
		// timehack('snoozealerts');
	}

	
	// 0 is no access type, 1 is salesperson, 2 is sales manager
	calc_access_type();
	print("window.g_accessType = $currentuser_accesstype;\n");

	// ***Special for asariam01***
	if ($currentuser_loginid==10259) {
		$sqlstr = "select * from people where PersonID=4923";
		$result = mssql_query($sqlstr);
		dump_resultset_as_array_append('g_people', $result);

		$sql = "SELECT DealID FROM opportunities WHERE";
		$sql .= " (Category != '6' OR ActualCloseDate >= cast('$first' as datetime))";
		$sql .= " AND (Category != '9' OR RemoveReason is NULL)";
		$sql .= " AND PersonID=4923";

		$Deals = make_in_clause($sql);
		if(!strlen($Deals)) $Deals = '-1';
		$sql = "SELECT * FROM opportunities WHERE DealID IN ($Deals) order by Company";
		$result = mssql_query($sql);
		dump_resultset_as_array_append('g_opps', $result);

		$sqlstr = "select * from products where CompanyID=379 and Deleted = '0' order by Name";
		$result = mssql_query($sqlstr);

		$firstrow = true;
		while (($row = mssql_fetch_assoc($result)))
		{
			print('g_products[g_products.length] = new Array("');

			$first = true;
			foreach ($row as $key=>$value)
			{
				if ($first)
					$first = false;
				else
					print('","');
				if ($key == 'Name')
					print('CRP: '.addslashes($value));
				else
					print(addslashes($value));
			}
			print('");');
			print("\n");
		}
		print("\n");

		$sql = "SELECT * FROM Opp_Product_XRef where CompanyID = 379 order by DealID, Seq";
		$result = mssql_query($sql);
		dump_resultset_as_array_append('g_opp_products_xref', $result);

		$sqlstr = "SELECT * FROM SubMilestones WHERE CompanyID = 379 ORDER BY Location, Seq";
		$result = mssql_query($sqlstr);
		dump_resultset_as_array_append('g_submilestones', $result);
	}
	// END ***Special for asariam01***

	if (isset($LoginSessionID))
	{
		print("window.g_LoginSessionID = $LoginSessionID;\n");
		$sqlstr = "update LoginSessions set EndTime=GetDate() where LoginSessionID=$LoginSessionID";
		mssql_query($sqlstr);
	}
	else
		print("window.g_LoginSessionID = null;\n");
}
close_db();

// echo '// imehack : Run Time: ' . round(microtime(true) - $tot_time, 3) . " seconds\n";

print("g_message='$message';");

function getSpPeepsFlat($PersonID)
{
	$sql = "select PersonID, IsSalesperson, Level from people where SupervisorID=$PersonID and Deleted<>1";
	$result = mssql_query($sql);
	$instr = '';
	while ($row = mssql_fetch_assoc($result))
	{
		if ($row['IsSalesperson'] == 1 || $row['Level'] == 1)
		{
			if ($instr != '')
				$instr .= ',';
			$instr .= $row['PersonID'];
		}
		if ($row['Level'] > 1)
		{
			$temp = getSpPeepsFlat($row['PersonID']);
			if ($temp != '')
			{
				if ($instr != '')
					$instr .= ',';
				$instr .= $temp;
			}
		}
	}
	return $instr;
}

/**
 * Callback function for dump_result that is processed for every row, used to build CM info
 */
function BuildCMSalesList($resultset,$row)
{
	GLOBAL $CMSalesList,$CMSalesCMID,$CMSalesMap,$CMSalesPersonID;
	if ($CMSalesCMID == -1)
	{
		$len = mssql_num_fields($resultset);
		for ($i = 0; $i < $len; ++$i)
		{
			$field = mssql_field_name($resultset, $i);
			if ($field=='CMID')
			{
				$CMSalesCMID=$i;
			}
			else if ($field=='PersonID')
			{
				$CMSalesPersonID=$i;
			}
		}
	}

	array_push($CMSalesList,$row[$CMSalesCMID]);	// build the list for the interface function
	$CMSalesMap[$row[$CMSalesCMID]]=$row[$CMSalesPersonID];
}
?>



















