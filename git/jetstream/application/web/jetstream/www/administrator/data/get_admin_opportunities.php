<?php
include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

ini_set('memory_limit', '32M');
$utf8=true;
include('get_opportunities_people.php');
include('get_unassigned_opportunities_people.php');
?>
