<?php
include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

function _GetSalespeopleTree(&$SalesIds, $SupervisorID)
{
	$sql = "select * from people where PersonID=$SupervisorID";
	$result = mssql_query($sql);
	if($result)
	{
		if($row = mssql_fetch_assoc($result))
			array_push($SalesIds, $SupervisorID);
	}


	$sql = "select * from people where SupervisorID=$SupervisorID";
	$result = mssql_query($sql);
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			array_push($SalesIds, $row['PersonID']);
			if($row['Level'] > 1)
				_GetSalespeopleTree($SalesIds, $row['PersonID']);
		}
	}
}

function GetSalespeopleTree($SupervisorID)
{
	$SalesIds = array();
	$idlist = "";

	_GetSalespeopleTree($SalesIds, $SupervisorID);

	$count = count($SalesIds);
	for($i=0; $i<$count; $i++)
	{
		if($i > 0) $idlist .= ",";
		$idlist .= $SalesIds[$i];
	}
	//print($idlist);
	return $idlist;
}

print("// ----- get_removedopps -----\n\n");
$fromdate = date('Y-m-d', time() - (60 * 60 * 24 * 365));

$inclause = GetSalespeopleTree($currentuser_personid);

/*
if ($currentuser_level > 1)
{
	$result = mssql_query("select PersonID from people where SupervisorID = '$inclause'");
	while ($row = mssql_fetch_assoc($result)) $inclause .= ',' . $row['PersonID'];
}
*/

//print($inclause);

$sql = 'select opportunities.*, products.Name from opportunities, products';
$sql .= ' where opportunities.ProductID = products.ProductID';
$sql .= " and opportunities.CompanyID='$mpower_companyid' and opportunities.Category='9'";
$sql .= " and opportunities.RemoveDate > '$fromdate' and opportunities.PersonID in ($inclause) order by Company";

dump_sql_as_array('g_removedopps', $sql);

$sql = "select opportunities.*, Name = '' from opportunities";
$sql .= ' where opportunities.ProductID = -1';
$sql .= " and opportunities.CompanyID='$mpower_companyid' and opportunities.Category='9'";
$sql .= " and opportunities.RemoveDate > '$fromdate' and opportunities.PersonID in ($inclause) order by Company";

dump_sql_as_array('g_removedopps', $sql, true);

?>
