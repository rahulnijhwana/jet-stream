<?php

print('<script language="JavaScript">');

include_once('_base_utils.php');
include_once('_insert_utils.php');

if (!check_admin_login())
{
	print('</script>');
	close_db();
	exit();
}

$result = insert_row('opportunities', $mpower_fieldlist, $mpower_data, 'DealID', 'Color,FirstName,LastName');

if ($result == true)
{
	$field_array = explode(',', $mpower_fieldlist);
	$field_count = count($field_array);
	$value_array = explode('|', $mpower_data);
	$value_count = count($value_array);
	$person_id_index = -1;
	for ($k = 0; $k < $field_count; ++$k)
	{
		if ($field_array[$k] == 'PersonID')
		{
			$person_id_index = $k;
			break;
		}
	}
	
	if ($person_id_index != -1)
	{
		$person_id = $value_array[$person_id_index];
		$where_list = 'PersonID = ' . $person_id;
		$result = mssql_query("SELECT SupervisorID, Level FROM people WHERE PersonID = $person_id");
		if ($result != false && mssql_num_rows($result) != 0)
		{
			$person_data = mssql_fetch_assoc($result);
			if ($person_data['Level'] == 1)
				$where_list .= ' OR PersonID = ' . $person_data['SupervisorID'];
		}
		mssql_query("UPDATE people SET LastChanged = GETDATE() WHERE $where_list");
	}
}

print('window.result = ' . ($result ? 'true' : 'false') . ';</script>');

close_db();

?>