<?php

print('<script language="JavaScript">');

include_once('_base_utils.php');
include_once('_update_utils.php');

if (!check_admin_login())
{
	print('</script>');
	close_db();
	exit();
}

$result = @update_rows('removereasons', $mpower_fieldlist, $mpower_data, 'ReasonID', '');

print('window.result = ' . (($result != false) ? 'true' : 'false') . ';</script>');
if ($result == false)
	print($UPDATE_ERROR_MSG);

close_db();

?>
