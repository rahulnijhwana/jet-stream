<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

print("// ----- get_analysis -----\n\n");

if (!isset($analysisid) || !strlen($analysisid)) $analysisid = -1;
if (!isset($tenure) || !strlen($tenure)) $tenure = 'New';
$textfield = 'ActionText' . $tenure;

if (strcasecmp($tenure, 'New') == 0) print("g_isNew = true;\n");
else print("g_isNew = false;\n");

$sql = "select * from analysis where AnalysisID = '$analysisid'";
dump_sql_as_array('g_analysis', $sql);

$sql = 'select * from reasons where ReasonID in ';
$sql .= "(select ReasonID from analysisreasonxref where AnalysisID = '$analysisid') order by ReasonID";
dump_sql_as_array('g_analysisReasons', $sql);

$sql = 'select * from questions where IsHeader > 1 or QuestionID in ';
$sql .= "(select QuestionID from analysisquestionxref where AnalysisID = '$analysisid') order by WithHeader, IsHeader DESC, QuestionID";
dump_sql_as_array('g_analysisQuestions', $sql);

$sql = "select ActionID, $textfield from actions where ActionID in ";
$sql .= "(select ActionID from analysisactionxref where AnalysisID = '$analysisid') order by ActionID";
dump_sql_as_array('g_analysisActions', $sql);
?>
