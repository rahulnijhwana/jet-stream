<?php
/**
 * @package Data
 */
include_once('_base_utils.php');
include_once('_insert_utils.php');

if (!check_admin_login())
{
	close_db();
	exit();
}

$fiscal_year_end_date = str_replace("'", '', $_POST['FiscalYearEndDate']);
$result = false;
$period_end_dates = isset($_POST['PeriodEndDate']) ? $_POST['PeriodEndDate'] : array();
$period_label_list = isset($_POST['PeriodLabel']) ? $_POST['PeriodLabel'] : array();

$year = (int)$_POST['year'];
$companyid = (int)$mpower_companyid;
$period = 1;

storeDate($companyid, $year, $fiscal_year_end_date, '', 'NULL', 'NULL');

foreach ($period_end_dates as $key=>$period_end_date) {
	$period_end_date = ($period_end_date == '0') ? 'NULL' : $period_end_date;
	$period_label = $period_label_list[$key];
	
	storeDate($companyid, $year, 'NULL', $period_label, $period, $period_end_date);
	$period++;	
}

function storeDate($companyid, $year, $fiscal_year_end_date, $period_label = '', $period, $period_end_date) {
	$result = false; 
	if ($period == 'NULL') {
		$insert_values = "$companyid, $year, '$fiscal_year_end_date', '$period_label', NULL, NULL";
		$update_values = "FiscalYearEndDate = '$fiscal_year_end_date'";	
	}
	else {
		$insert_values = "$companyid, $year, NULL, '$period_label', $period, '$period_end_date'";
		if ($period_end_date == 'NULL') {
			$update_values = "PeriodEndDate = NULL, PeriodLabel = '$period_label'";
			
		} else {	
			$update_values = "PeriodEndDate = '$period_end_date', PeriodLabel = '$period_label'";
		}
	}	

	$sql = "IF EXISTS (SELECT * FROM FiscalYear WHERE CompanyID = $companyid AND Year = $year AND Period = $period)
				UPDATE FiscalYear
				   SET $update_values   
				 WHERE CompanyID = $companyid AND Year = $year AND Period = $period
			ELSE
				INSERT INTO FiscalYear
					   (CompanyID
					   ,Year
					   ,FiscalYearEndDate
					   ,PeriodLabel
					   ,Period
					   ,PeriodEndDate)
				 VALUES ($insert_values)";	
	//file_put_contents('D:/test.log', $sql);	
	if(mssql_query($sql)) $result = true; 
	return $result;
}

close_db();

?>