<?php
/**
 * @package Data
 */
include_once('_base_utils.php');
include_once('_insert_utils.php');

if (!check_admin_login()) {
	close_db();
	exit();
}

//$companyid = (int)$mpower_companyid;
$emails = $_POST['Emails'];
$added_email_ids = $_POST['Added'];
$removed_email_ids = $_POST['Removed'];
$primary_email = $_POST['PrimaryEmail'];
$person_id = (int) $_POST['PersonID'];

if (!empty($primary_email)) {	
	$sql = "IF EXISTS (SELECT * FROM PeopleEmailAddr WHERE PersonID = $person_id AND IsPrimaryEmailAddr IS NOT NULL)
				UPDATE PeopleEmailAddr
				   SET EmailAddr = '$primary_email'   
				 WHERE PersonID = $person_id AND IsPrimaryEmailAddr IS NOT NULL
			ELSE
				INSERT INTO PeopleEmailAddr (PersonID, EmailAddr, IsPrimaryEmailAddr) VALUES ($person_id, '$primary_email', 1)";	
	mssql_query($sql) or die(); 
}

// remove emails
if (is_array($removed_email_ids) && count($removed_email_ids) > 0) {
	foreach ($removed_email_ids as $email_id) {
		$sql = "DELETE FROM PeopleEmailAddr WHERE PersonID = $person_id AND PeopleEmailAddrID = $email_id";		
		mssql_query($sql) or die();
	}	
}

// add or update emails
if (is_array($emails) && count($emails) > 0) {
	foreach ($emails as $key=>$email) {	
		$email_id = $added_email_ids[$key];
		
		if (!empty($email)) {
			$sql = "IF EXISTS (SELECT * FROM PeopleEmailAddr WHERE PersonID = $person_id AND PeopleEmailAddrID = $email_id)
						UPDATE PeopleEmailAddr
						   SET EmailAddr = '$email'   
						 WHERE PersonID = $person_id AND PeopleEmailAddrID = $email_id
					ELSE
						INSERT INTO PeopleEmailAddr (PersonID, EmailAddr) VALUES ($person_id, '$email')";	
			mssql_query($sql) or die(); 
		}
	}	
}

close_db();

?>