<?php

define('LEGACY_BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));
require_once(LEGACY_BASE_PATH . '/include/mpconstants.php');


/**
 * Database Connection
 *
 * Establishes a connection to the database defined in settings.php
 * 
 * @package Data
 */

$connfail = false;
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
	// Windows default settings
	$db_server = MP_DATABASE_WINDOWS;
} else {
	// Unix Settings
	//$db_server = 'ASA-VERIO';
	$db_server = MP_DATABASES;
}


// $db_server = MP_DATABASE_WINDOWS; // Uncomment for Test server only
// $db_server = 'ASA-VERIO';
//$dbname = 'test_new'; //'mpasac'; //'mpasac';
//$user = 'mpasac';
//$password = 'qraal+zv';

$dbname = MP_DATABASE ; 
$user = MP_DATABASEU;
$password = MP_DATABASEP;

$max_connect_attempts = 3;
for ($connect_attempt = 1; $connect_attempt <= $max_connect_attempts; $connect_attempt++) {
    $dbconn = @mssql_pconnect($db_server, $user, $password);
    if(@mssql_select_db($dbname, $dbconn) != FALSE) {
        return;
    }
}

$connfail = true;

// As it turns out, mssql_get_last_message only returns
// something if it actually connects to an SQL server.  Thus,
// DNS failures and connectivity failures will not cause a message
// to be fetched; wheras an incorrect password would do so.
$conn_err = mssql_get_last_message();

/**
 * Close the database connection.
 */
function close_db()
{
	global $dbconn;
	mssql_close($dbconn);
	$dbconn = null;
}
?>
