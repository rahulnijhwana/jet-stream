<?php
/**
 * @package Data
 */
include_once('_base_utils.php');
include_once('_insert_utils.php');

if (!check_admin_login())
{
	close_db();
	exit();
}


//print_r($_POST['seasonality']);
$year = $_POST['Year'];	
$is_seasonality = (int) $_POST['SeasonalityCheck'];
$companyid = (int) $mpower_companyid;

//$sql = "UPDATE Company SET Seasonality = $is_seasonality WHERE CompanyID = $companyid";	
//mssql_query($sql) or die(); 

$sql = "IF EXISTS (SELECT * FROM Seasonality WHERE CompanyID = $companyid AND Year = $year AND Period IS NULL AND PersonID IS NULL AND Seasonality IS NULL)
			UPDATE Seasonality
			   SET SeasonalityEnabled = $is_seasonality   
			 WHERE CompanyID = $companyid AND Year = $year AND Period IS NULL AND PersonID IS NULL AND Seasonality IS NULL
		ELSE
			INSERT INTO Seasonality
				   (CompanyID
				   ,Year					  				   
				   ,SeasonalityEnabled)
			 VALUES ($companyid, $year, $is_seasonality)";
//echo '<br>'.$sql.'<br>';
mssql_query($sql) or die(); 

if ($is_seasonality == 1) {	
	$seasonality = $_POST['seasonality'];	
	$period = 1;	
	
	foreach ($seasonality as $each_seasonality) {	
		$each_seasonality = (int) $each_seasonality;
		$sql = "IF EXISTS (SELECT * FROM Seasonality WHERE CompanyID = $companyid AND Year = $year AND Period = $period)
					UPDATE Seasonality
					   SET Seasonality = $each_seasonality   
					 WHERE CompanyID = $companyid AND Year = $year AND Period = $period
				ELSE
					INSERT INTO Seasonality
						   (CompanyID
						   ,Year					  
						   ,Period
						   ,Seasonality)
					 VALUES ($companyid, $year, $period, $each_seasonality)";	

		if (mssql_query($sql)) echo 'OK'; 
		else echo 'Failed';		
		$period++;	
	}	
} 

close_db();

?>