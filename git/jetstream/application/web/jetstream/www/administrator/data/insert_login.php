<?php
/**
 * @package Data
 */

print('<script language="JavaScript">');

include_once('_base_utils.php');
include_once('_insert_utils.php');

if (!check_admin_login())
{
	print('</script>');
	close_db();
	exit();
}

print('</script><div id="errmsg">');

$temp_data = '';
$dataarr = explode('|',$mpower_data);
$fieldarr = explode(',',$mpower_fieldlist);
$field_count = count($fieldarr);
for ($k = 0; $k < $field_count; $k++)
{
	if($fieldarr[$k] == 'Password') $pwdfieldno = $k;
	if($fieldarr[$k] == 'SuperId') $idfieldno = $k;
	if ($dataarr[$k] == 'NULL' || strlen($dataarr[$k]) == 0) $dataarr[$k] = 'NULL';
}

$password = $dataarr[4];
if($password == null || strlen($password) == 0){
	$password = generatePassword();
}else{
	if($dataarr[$idfieldno] != '-3')
		$dataarr[4] = 'NULL';
}
$ignorelist = 'SuperId';
$encpassword = crypt($password);
if(isset($pwdfieldno))
	$dataarr[$pwdfieldno] = $encpassword;
	
$temp_data = implode('|',$dataarr);
$result = insert_row('Logins', $mpower_fieldlist, $temp_data, 'ID', $ignorelist);
print('</div><script language="JavaScript">');

if ($result != false)
{
	$result = mssql_query("SELECT @@IDENTITY AS 'ID'");
	$temp_assoc = mssql_fetch_assoc($result);
	print('window.result = ' . $temp_assoc['ID'] . ';</script>');
}
else
	print('window.result = false;</script>');

close_db();

function generatePassword($length = 8) {
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
	$code = "";
	while (strlen($code) < $length) {
	$code .= $chars[mt_rand(0,strlen($chars))];
	}
	return $code;
}

?>