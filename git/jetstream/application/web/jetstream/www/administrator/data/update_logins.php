<?php

print('<script language="JavaScript">');

include_once('_base_utils.php');
include_once('_update_utils.php');

print('</script>');

if (!check_admin_login())
{
	close_db();
	exit();
}

print('<div id="errmsg">\n');

$temp_data = '';
$dataarr = explode('|',$mpower_data);
$fieldarr = explode(',',$mpower_fieldlist);
$field_count = count($fieldarr);
for ($k = 0; $k < $field_count; $k++)
{
	if($fieldarr[$k] == 'Password') $pwdfieldno = $k;
	if($fieldarr[$k] == 'SuperId') $idfieldno = $k;
}

$password = $dataarr[4];
if($password == null || $password == '' || $password == '*******'){
	$ignorelist = 'Password';
}
else{
	$ignorelist = '';
	$encpassword = crypt($password);
	if(isset($pwdfieldno))
		$dataarr[$pwdfieldno] = $encpassword;
	
	if($dataarr[$idfieldno] != '-3')
		$dataarr[4] = 'NULL';
}

$temp_data = implode('|',$dataarr);
$result = update_rows('Logins', $mpower_fieldlist, $temp_data, 'ID','SuperId,'.$ignorelist);
print('</div>');

print('<script language="JavaScript">window.result = ' . (($result != false) ? 'true' : 'false') . ';</script>');
if ($result == false)
	print('<div id="errmsg">' + $UPDATE_ERROR_MSG + '</div>');

close_db();

function generatePassword($length = 8) {
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
	$code = "";
	while (strlen($code) < $length) {
	$code .= $chars[mt_rand(0,strlen($chars))];
	}
	return $code;
}

?>
