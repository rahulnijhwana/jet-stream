<?php
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login_quiet()) exit();

if (isset($_COOKIE['flat_view_id']) && $_COOKIE['flat_view_id'] != '') {
	print("\n//flat_view_id = ".$_COOKIE['flat_view_id']."\n");
	$peeps = getSpPeepsFlat($_COOKIE['flat_view_id']);
} else {
	$do_tree = ( 2 < $currentuser_level 
			&& (!isset($mpower_effective_userid) || $mpower_effective_userid == '')
		) || ( isset($mpower_effective_userid) 
			&& $sib_mode == 'tree'
		);

	if ($do_tree) {
		$sql = "SELECT PersonID FROM people WHERE CompanyID = '$mpower_companyid' AND Deleted = '0'";
	} else {
		$sql = "SELECT PersonID FROM people WHERE 
			(PersonID='$currentuser_personid' AND (IsSalesperson = '1' OR Level = '1'))
			OR (SupervisorID = '$currentuser_personid' AND Deleted = '0' AND IsSalesperson='1' AND (Level='1' OR AppearsOnManager='1'))";
	}
	$peeps = make_in_clause($sql);
}

if (!strlen($peeps)) {
	dump_nothing_as_array(array('g_opps', 'g_people', 'g_salescycles', 'g_snoozealerts'));
	close_db();
	print("g_message='Nobody assigned to this dashboard.';");
	exit();
}

$sql = "SELECT * FROM people WHERE CompanyID = '$mpower_companyid'
	AND PersonID IN ($peeps) ORDER BY LastName, FirstName";

$people_result = mssql_query($sql);
// $output_array = array();
if ($people_result) {
	dump_resultset_as_array('g_people', $people_result);
	// while($row = mssql_fetch_assoc($result)) {
	//	$output_array[$row['PersonID']] = $row['LastName'] . ', ' . $row['FirstName'];
	// }
}
//echo json_encode($output_array);

?>
	