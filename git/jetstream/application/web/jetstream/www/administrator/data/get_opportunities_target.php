<?php

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

// This file is intended to get the opportunities for the targeted person

if (!isset($_COOKIE['mpower_target'])) {
	return;
}

$target = $_COOKIE['mpower_target'];

print("// ----- get_opportunities_person -----\n\n");
dump_sql_as_array('g_opportunities', "SELECT opportunities.* FROM opportunities WHERE opportunities.PersonID = $target");
?>
