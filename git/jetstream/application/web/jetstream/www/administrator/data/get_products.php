<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

print("// ----- get_products -----\n\n");
dump_sql_as_array('g_products', "select * from products where CompanyID IN ($companyList) and Deleted = '0' order by Name");
dump_sql_as_array('g_removed_products', "select * from products where CompanyID IN ($companyList) and Deleted = '1'");
?>
