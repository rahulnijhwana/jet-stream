<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

print("// ----- get_valuations -----\n\n");

$sql="select Count(ValID) as VCount, CompanyID from Valuations group by CompanyID having (CompanyID = '$mpower_companyid')";
$ok = mssql_query($sql);
if($ok)
{
	$data = mssql_fetch_assoc($ok);
	$cv = $data['VCount'];
	if($cv < 20)
	{
		$fill = 20 - $cv;
		for($i=0; $i < $fill; $i++)
		{
			$sql = "INSERT INTO valuations  (CompanyID,VLevel,Label,Activated) VALUES('$mpower_companyid', $i+1, '', 0)";
			$result = mssql_query($sql);
			if ($result == false)
				exit('Error inserting valuation data');
		}
	}
}
else exit('Bad valuations query');

dump_sql_as_array('g_valuations', "select * from Valuations where CompanyID IN ($companyList) order by VLevel");

?>
