<?php

include_once (BASE_PATH . '/slipstream/class.ContactMap.php');
include_once (BASE_PATH . '/slipstream/class.Section.php');

$objContactMap = new ContactMap();
$objSection = new Section();
$contactSections = $objSection->getContactSectionByCompanyId($mpower_companyid);
$allContactFields= $objContactMap->getFieldsFromContactMapByCompId($mpower_companyid,'%');

$unUsedFields = array();
$leftSectionFields = array();
$rightSectionFields = array();
$totalContactFields = count($allContactFields);
$validPosition = false;
for($i=0;$i<count($allContactFields);$i++){
	$hasSection = false;
	//Check if SectionID is NULL then the field is not assigned.
	if($allContactFields[$i]['SectionID'] == NULL || $allContactFields[$i]['SectionID'] == ''){
		$unUsedFields[] = array('ContactMapID'=>$allContactFields[$i]['ContactMapID'],'LabelName'=>$allContactFields[$i]['LabelName']);
	}else{
		foreach($contactSections as $sections){
			//If the section is present or not.
			if($allContactFields[$i]['SectionID'] == $sections['SectionID']){
				$sectionName = str_replace(' ', '',$sections['ContactSectionName']);
				$hasSection = true;
			}
		}
		//Check to see if position and align value is same for more than one field. 
		$validPosition = checkContactFieldPosition($allContactFields[$i],$mpower_companyid);
		if($hasSection && $validPosition){
			if($allContactFields[$i]['Align'] == 1){
				$leftSectionFields[] = array('ContactMapID'=>$allContactFields[$i]['ContactMapID'],'LabelName'=>$allContactFields[$i]['LabelName'],'Position'=>$allContactFields[$i]['Position'],'Align'=>$allContactFields[$i]['Align'],'SectionName'=>$sectionName,'TabOrder'=>$allContactFields[$i]['TabOrder']);
			}else{
				$rightSectionFields[] = array('ContactMapID'=>$allContactFields[$i]['ContactMapID'],'LabelName'=>$allContactFields[$i]['LabelName'],'Position'=>$allContactFields[$i]['Position'],'Align'=>$allContactFields[$i]['Align'],'SectionName'=>$sectionName,'TabOrder'=>$allContactFields[$i]['TabOrder']);
			}
		}else{
			//If section has been deleted or there is duplicate position or align value of the field then put the filed in unassigned array.
			$unUsedFields[] = array('ContactMapID'=>$allContactFields[$i]['ContactMapID'],'LabelName'=>$allContactFields[$i]['LabelName']);
			$allContactFields[$i]['Align'] = NULL;
			$allContactFields[$i]['Position'] = NULL;
			$allContactFields[$i]['SectionID'] = NULL;
		}
	}
}

$conSectionDetails = array();
$conSection = array();
$conSectionName = array();
$conSectionSize = array();
if(count($contactSections) == 0){
	$objSection ->saveSection($mpower_companyid,NULL,'DefaultSection');
	$conSection[] = 'DefaultSection';
	$conSectionName[] =  'DefaultSection';
}
else{
	foreach($contactSections as $sections){
		$conSection[] = str_replace(' ', '',$sections['ContactSectionName']);
		$conSectionName[] =  $sections['ContactSectionName'];
		$conSectionDetails[] = array('SectionName'=>$sections['ContactSectionName'],'SectionId'=>$sections['SectionID']);
		$maxPosition = getContactSectionMaxFields($sections['SectionID'],$mpower_companyid);
		if($maxPosition == NULL || $maxPosition == '')
			$maxPosition =1;

		if($maxPosition > 9){
			$conSectionSize[] = ($maxPosition + 1);
		}else{
			$conSectionSize[] = 10;
		}	
	}
}

function checkContactFieldPosition($contactField,$compId){
	$objContactMap = new ContactMap();
	$result = $objContactMap->countContactFieldsBySectionAlign($compId,$contactField['SectionID'],$contactField['Align'],$contactField['Position']);
	if($result['count'] == 1)
		return true;
	else
		return false;
}

function getContactSectionMaxFields($sectionId,$compId){
	$objContactMap = new ContactMap();
	return $objContactMap->getContactMaxPositionBySecId($sectionId,$compId);
}

echo 'var companyId = '.$mpower_companyid .';';
echo 'var CONTACTSECTIONNAME = '.json_encode($conSectionName).';';
echo 'var CONTACTLEFTSECTION = '.json_encode($leftSectionFields).';';
echo 'var CONTACTRIGHTSECTION = '.json_encode($rightSectionFields).';';
echo 'var JASONCONTACTSECTION = '.json_encode($conSection).';';
echo 'var CONTACTSECTIONDETAILS = '.json_encode($conSectionDetails).';';
echo 'var CONTACTFIELDS = '.json_encode($unUsedFields).';';
echo 'var CONTACTTOTALFIELDS = '.$totalContactFields.';';
echo 'var CONTACTSECTSIZE = '.json_encode($conSectionSize).';';
?>


function addContactLayoutData(section)
{
	var usedValues = new Array();
	for(i=0;i<section.length;i++)
	{
		if(section[i].Align==1){
			div_no=(section[i].Position*2)-1;
		}else{
			div_no=(section[i].Position*2);
		}
		var tabLabel = '';
		if(section[i].TabOrder != '' && section[i].TabOrder != null)
			tabLabel = '['+ section[i].TabOrder +']';
		else
			tabLabel = '[-]';

		div_id = section[i].SectionName+'_'+div_no;
		document.getElementById(div_id).innerHTML = '<div class="clsSectionSmallBox" id="'+escape(section[i].LabelName)+'_'+ section[i].ContactMapID+'" ondblclick="editFieldDetails(this)">'+section[i].LabelName+' '+tabLabel+'</div>';
		usedValues[i] = escape(section[i].LabelName) +'_'+ section[i].ContactMapID;
	}
	return usedValues;
}

function saveContactSectionLayout()
{

	var unAssignedData = '';
		for(i=0;i<document.getElementById('totalFieldsDiv').childNodes.length;i++)
		{
			if(document.getElementById('totalFieldsDiv').childNodes[i].id){
				unAssignedData += document.getElementById('totalFieldsDiv').childNodes[i].id + "|";
			}
		}
		var secData='';
		if(sections)
		{
			for(i=0;i<sections.length;i++)
			{
				if(i==0)
					secData=sections[i];
				else
					secData+=','+sections[i];
			}
	}
	dataLayout=getLayoutSettings();
	$.post("../../ajax/ajax.contactlayout.php",
		{
			dataLayout:dataLayout,
			action:'saveContactSectionLayout',
			unAssignedData : unAssignedData,
			sections:secData,
			companyId : companyId
		},
		processSaveContactSectionLayout
	);
}

function processSaveContactSectionLayout(contactSections)
{
	var lastContainerInner = '';
	 for(i=0;i<JASONCONTACTSECTION.length;i++){
		var secSize = CONTACTSECTSIZE[i];
		var lastContainerName = JASONCONTACTSECTION[i]+'_'+(secSize*2);
		if(document.getElementById(lastContainerName))
			lastContainerInner = document.getElementById(lastContainerName).innerHTML;
			
		if(lastContainerInner != ''){
			CONTACTSECTSIZE[i] = CONTACTSECTSIZE[i] + 1;
			manageContact();
		}else{
			if(secSize > 10){
				//To Do: Reduce the total no of blank divs as per the existing max field size.
			}
		}
	}
}