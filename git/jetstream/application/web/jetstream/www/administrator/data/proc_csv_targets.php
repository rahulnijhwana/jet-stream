<?php
/**
 * @package Data
 */

set_time_limit(120);
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>M-Power</title>
<link rel="stylesheet" type="text/css" href="admin.css">
<link rel="stylesheet" type="text/css" href="../shared/sharedstyles.css">
<script language="JavaScript" src="../shared/windowing.js"></script>
<script language="JavaScript" src="../shared/category_labels.js"></script>
</head>



<body style="background-color:lightgrey; font-family: arial" onunload="refreshparent();">


<script language="JavaScript">


var reallyrefresh = true;

// Refresh the board if we unload the window.
function refreshparent()
{
	if(reallyrefresh == true)
	{
		if ((navigator.appName.indexOf("Netscape") != -1))
			window.opener.location.href = '../board/main.html';
		else
			window.opener.history.go(0);
	}
}

function goback()
{
	// If we go back to the file selection window,
	// we can't refresh the board, since that will cause the
	// window to close.
	reallyrefresh = false;
	var frm = document.forms['frmImportTargets'];
	frm.action = "../shared/import_targets.php";
	frm.submit();
//	window.back();
}

function doErrorRpt(tmpFile)
{
	Windowing.openSizedWindow('../reports/print_targ_errors.php?tmpFile=' + escape(tmpFile), 500, 790, 'print_targ_errs');
}

function doImports(tmpFile)
{
	var frm = document.forms['frmImportTargets'];
	frm.action = 'proc_csv_targets.php?doImports=1&tmpFile=' + escape(tmpFile);
//alert(frm.action);
	reallyrefresh = false;
	frm.submit();
}

function doCancel(tmpFile)
{
	reallyrefresh = false;
	window.close();

}




</script>

<?php
$DEMO = false;
include_once('_base_utils.php');
include_once('_insert_utils.php');
include('../reports/category_labels.php');
include_once('read_targets_file.php');



// We need our own check_user_login to avoid the loginResult message (see also: _base_utils.php)
function proc_csv_check_user_login()
{
	global $currentuser_level, $currentuser_isadmin;
	if ($currentuser_level == -2)
	{
		check_login();
	}

	return (0 < $currentuser_level || $currentuser_level == -3);
}

if (!proc_csv_check_user_login())
{
	close_db();
	exit();
}


/*
	LastName 	[Required]
	FirstName	[Required]
	Company		[Required]
	Source
	Valuation
	UserID		[Required if duplicate names]

*/


//********************************IMPORT TARGETS************************************************


if(!isset($doImports))
	$doImports=0;

$unlinkTmpFile = $doImports;

if($doImports)
{
	print('<table width=100%>');
	print('<tr><td>');
	print('<center><input type="button" value="Finish" onclick="window.close();"></input>&nbsp;');
	print('<input type="button" value="Import Another File" onclick="goback();"></input></center>');
	print('</tr></td>');
	print('</table>');
}

//Must be able to access temporary file on second script instance.
$fh = 0;
if(isset($tmpFile))
{
	$uploaded_file = urldecode($tmpFile);
	$fh = fopen($uploaded_file, "r");
}
else
{
	$file_uploaded = (isset($_FILES['TargetFile']) && $_FILES['TargetFile']['size'] > 0);
	if(!file_uploaded)
	{
		print("<br><center>Uploaded file not found.</b>.</center><br>");
	}
	else
	{
		$uploaded_file = "/tmp/" . basename($_FILES['TargetFile']['name']);
		$ok = move_uploaded_file($_FILES['TargetFile']['tmp_name'], $uploaded_file);
		if(!$DEMO) { writeinslog("--- IMPORT TARGETS FROM CSV ---"); }
		if($ok)
		{
			$fh = fopen($uploaded_file, "r");
		}
	}
}

if(!$fh)
{
	print("<br><center>Error opening file <b>$uploaded_file</b>.</center><br>");
}
else
{
	$row=1;
	$success=0;
	print "<br><br>";
	print "<center><h3>File Import Status</h3></center><br>";

	$arrErrorMsgs = array();
	$arrImports = array();

	$row = ReadTargetFile($fh, 1);
	if($row)
	{
		$ct = $row - 1;
		print("<center><b>$ct LINES TOTAL</b></center><br>");
		print('<form name="frmImportTargets" enctype="application/x-www-form-urlencoded" method="post">');

		if(!$doImports)
		{
			$unlinkTmpFile = false;
			if(count($arrErrorMsgs) > 0)
			{

				$ect = count($arrErrorMsgs);
				if($ect > 1)
					$elbl = "<center><b>$ect LINES HAVE ERRORS</b></center><br>";
				else
					$elbl = "<center><b>$ect LINE HAS ERRORS</b></center><br>";

				print($elbl);
				print('<table border=0 width=\"100%\">');
				print('<tr><td width="10%"><b>Line</b></td><td width="90%"><b>Error Message</b></td></tr>');
				for ($i = 0; $i < count($arrErrorMsgs); $i++)
				{
					$rw = $arrErrorMsgs[$i]['row'];
					$msg = $arrErrorMsgs[$i]['msg'];
					print("<tr><td>$rw</td><td>$msg</td></tr>");
				}
				print("</table>");
				print('<center><input type="button" value="Print Error Report" onclick="doErrorRpt(\'' . $uploaded_file . '\')"></input></center>');
				if(count($arrImports))
				{
					$rct = $ct - $ect;
					$lbl = "Import $rct ";
					$lbl .= ($rct > 1) ? GetCatLbl("Target") . 's' : GetCatLbl("Target");

					print("<center><input type=\"button\" value=\"$lbl\" onclick=\"doImports('$uploaded_file');\"></input></center>");
				}
			}
			else
			{
				print("<center><b>NO RECORDS HAVE ERRORS</b></center><br>");
				print('<center><input type="button" value="Import ' . GetCatLbl("Target") . 's" onclick="doImports(\'' . $uploaded_file . '\')"></input></center>');
			}
			print('<center><input type="button" value="Cancel" onclick="doCancel(\'' . $uploaded_file . '\')"></input></center>');
			print("<input type=\"hidden\" name=\"usecont\" value=\"$usecont\" />");
			print("<input type=\"hidden\" name=\"usediv\" value=\"$usediv\" />");
			print("<input type=\"hidden\" name=\"usesrc2\" value=\"$usesrc2\" />");
			print("<input type=\"hidden\" name=\"popdd\" value=\"$popdd\" />");

			print('</form>');
		}
		if($doImports && count($arrImports))
		{
			$ok = mssql_query("BEGIN TRANSACTION");
			$import_error = false;
			for ($i = 0; $i < count($arrImports); $i++)
			{
				$import_error = (false == MakeOpportunity($mpower_companyid, $arrImports[$i]['company'], $arrImports[$i]['personid'], $arrImports[$i]['sourceid'], $arrImports[$i]['vlevel'], $arrImports[$i]['division'], $arrImports[$i]['contact'], $arrImports[$i]['sid2']));
				if($import_error) { break; }
			}


			if($import_error)
			{
				$ok = mssql_query("ROLLBACK TRANSACTION");
				print "<br><center>An error occured inserting data. All changes rolled back.</center><br>";
				writeinslog("--- CSV IMPORT ROLLED BACK ---");
			}
			else
			{
				$ok = mssql_query("COMMIT TRANSACTION");
	//			printf("<br><center>Rows read: %d / " . GetCatLbl("Target") . "s imported successfully: %d</center><br>", $row - 1, $success);
				printf("<br><center>%d " . GetCatLbl("Target") . "s successfully imported</center><br>", count($arrImports));

				writeinslog("--- CSV IMPORT COMMITTED ---");
			}
		}
	}
	fclose($fh);
	if($unlinkTmpFile)
		unlink($uploaded_file);	// clean up after ourselves
}


function MakeOpportunity($company_id, $target_company_name, $sales_id, $source_id, $val_level, $division, $contact, $sid2)
{
    global $popdd;
	$date = getdate();
	$now = sprintf("%04d-%02d-%02d", $date['year'], $date['mon'], $date['mday']);

    $remove_reason = ($popdd) ? "-1" : "NULL";


	// Double up any apostrophes so SQL won't choke
	$target_company_name = str_replace("'", "''", $target_company_name);
	$division = str_replace("'", "''", $division);
	$contact = str_replace("'", "''", $contact);


	$fields = "CompanyID, Category, Company, "
		. "FirstMeeting, ProductID, EstimatedDollarAmount, "
		. "Requirement1, Person, Need, Money, Time, "
		. "Requirement2, NextMeeting, ActualDollarAmount, "
		. "AdjustedCloseDate, ForecastCloseDate, PersonID, "
		. "ActualCloseDate, NextMeetingTime, LastMoved, "
		. "Alert1, Alert2, Alert3, Alert4, Alert5, Alert6, "
		. "RemoveReason, RemoveDate, FirstMeetingTime, ValID, "
		. "VLevel, TargetDate, SourceID, Division, Contact, Source2ID";

	$values = "$company_id|10|$target_company_name|NULL|"
		. "-1|NULL|0|0|0|0|0|0|NULL|NULL|NULL|NULL|"
		. "$sales_id|NULL|NULL|NULL|NULL|NULL|0|NULL|"
		. "NULL|NULL|$remove_reason|$now|NULL|NULL|$val_level|$now|$source_id|"
		. "$division|$contact|$sid2";

	$result = insert_row("opportunities", $fields, $values, 'DealID', '');
	if($result != false)
	{
		$id_result = mssql_query("SELECT @@IDENTITY AS DealID");
		$temp_assoc = mssql_fetch_assoc($id_result);
		$deal_id = $temp_assoc['DealID'];
		writeinslog("opportunities: Deal ID $deal_id added");
	}
	return $result;
}


?>
</body>
</html>
