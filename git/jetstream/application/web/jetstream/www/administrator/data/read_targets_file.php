<?php
/**
 * @package Data
 */

$ValuationUsed = 0;
$SourceUsed = 0;
$Source2Used = 0;
GetCompanySettings($ValuationUsed, $SourceUsed, $Source2Used, $Source2Label);

SqlToArray("select * from opportunities where companyid=$mpower_companyid and category=10 order by PersonID, Company", "Opportunities", $arrTargets);
SqlToArray("select * from people where companyid=$mpower_companyid and Deleted=0 and not (SupervisorID = -1) and not (SupervisorID = -3)", "People", $arrSales);
//Do not include aliases (login recs where LoginID != PersonID)
SqlToArray("select UserID, PersonID from Logins where PersonID=LoginID and CompanyID = $mpower_companyid", "Logins", $arrLogins);

if($SourceUsed)
	SqlToArray("select * from sources where Deleted=0 and companyid=$mpower_companyid", "Sources", $arrSources);
if ($ValuationUsed)
	SqlToArray("select * from valuations where companyid=$mpower_companyid", "valuations", $arrValuations);
if($Source2Used)
	SqlToArray("select * from sources2 where Deleted=0 and companyid=$mpower_companyid", "Sources2", $arrSources2);



function SqlToArray($strSQL, $TableName, &$arrResults)
{
	$arrResults = array();
	$ok = mssql_query($strSQL);
	if (!$ok)
	{
		print "Db error: $TableName";
		exit();
	}
	$ind=0;
	while ($row=mssql_fetch_assoc($ok))
	{
		$arrResults[$ind++] = $row;
	}
}

function GetCsvValues($string, $separator=",")
{
    $elements = explode($separator, $string);
    for ($i = 0; $i < count($elements); $i++) {
        $nquotes = substr_count($elements[$i], '"');
        if ($nquotes %2 == 1) {
            for ($j = $i+1; $j < count($elements); $j++) {
                if (substr_count($elements[$j], '"') > 0) {
                    // Put the quoted string's pieces back together again
                    array_splice($elements, $i, $j-$i+1,
                        implode($separator, array_slice($elements, $i, $j-$i+1)));
                    break;
                }
            }
        }
        if ($nquotes > 0) {
            // Remove first and last quotes, then merge pairs of quotes
            $qstr =& $elements[$i];
            $qstr = substr_replace($qstr, '', strpos($qstr, '"'), 1);
            $qstr = substr_replace($qstr, '', strrpos($qstr, '"'), 1);
            $qstr = str_replace('""', '"', $qstr);
        }
    }
    return $elements;
}


function checkFileFormat($row, &$msg)
{
    global $ValuationUsed, $SourceUsed, $usediv, $usecont, $usesrc2;
	$fieldcount = 0;
	$offset = 0;



    $field_array = GetCsvValues($row);
    $fieldcount = count($field_array);

    foreach($field_array as $field_num => $field) {
        if (!strlen($field) && $field_num < 3) {
			$msg = " required field missing (LastName, FirstName, Company).";
			return 0;
		}
            
    }

//	while(1)
//	{
//
//
//		$c = strpos($row, ',', $offset);
//
//		if($c > -1)
//			$field = substr($row, $offset, $c - $offset);
//		else
//			$field = substr($row, $offset);
//		if ($c > 0) $offset = ($c+1);
//		$fieldcount++;
//		if(!strlen($field) && $fieldcount < 4)
//			{
//				$msg = " required field missing (LastName, FirstName, Company).";
//				return 0;
//			}
//		if(strlen($field) && !preg_match('/^[[:ascii:]]+$/', $field))
//			{
//				$msg = " is not in CSV format";
//				return 0;
//			}
//
//		if($c == false) break;
//	}
	$reqFields = 3;
	$msg2 = "";
	if($ValuationUsed > 0) { $msg2 .= "(ValuationUsed)"; $reqFields++; }
	if($SourceUsed > 0) { $msg2 .= "(SourceUsed)"; $reqFields++; }

	if($usediv) { $msg2 .= "(usediv)"; $reqFields++; }
	if($usecont) { $msg2 .= "(usecont)"; $reqFields++; }
	if($usesrc2) { $msg2 .= "(usesrc2)"; $reqFields++; }
	if($fieldcount < $reqFields)
	{
		$msg = ": missing one or more required columns.";
		return 0;
	}
	if($fieldcount > $reqFields)
	{
		$msg = ": Fieldcount incorrect! Expected $reqFields columns but found $fieldcount.  Please activate the appropriate optional fields.";
		$msg .= $msg2;
		return 0;
	}
	return $fieldcount;
}

function GetCompanySettings(&$ValuationUsed, &$SourceUsed, &$Source2Used, &$Source2Label)
{
	global $mpower_companyid;
	$sql = "select SourceUsed, ValuationUsed, Source2Used, Source2Label from Company where CompanyID = $mpower_companyid";
	$ok = mssql_query($sql);
	if(($row = mssql_fetch_assoc($ok)))
	{
		$ValuationUsed = $row['ValuationUsed'];
		$SourceUsed = $row['SourceUsed'];
		$Source2Used = $row['Source2Used'];
		$Source2Label = $row['Source2Label'];
/*
		print "ValuationUsed: $ValuationUsed<br>SourceUsed: $SouceUsed<br>Source2Used: $Source2Used<br>Source2Label: $Source2Label<br>";
*/
	}
}


function GetSalesID($data, &$msg)
{
	global $arrSales, $arrLogins, $ValuationUsed, $SourceUsed, $usecont, $usediv;
//First, look for UserID in logins table.  If supplied use that to match
	$uidOffset = 3;
	$uid = '';
	$pid = -1;
	if($ValuationUsed)
		$uidOffset++;
	if($SourceUsed)
		$uidOffset++;
	if($usecont)
		$uidOffset++;
	if($usediv)
		$uidOffset++;
	if($usesrc2)
		$uidOffset++;
	if($uidOffset < count($data))
	{
		$uid = $data[$uidOffset];
		for ($i=0; $i<count($arrLogins); $i++)
		{
			if($uid == $arrLogins[$i]['UserID'])
			{
				$pid = $arrLogins[$i]['PersonID'];
				break;
			}
		}
		if ($pid > -1)
			return $pid;
	}
//Evidently, no userid supplied or no match.  Check first & last names
	$fname = trim($data[1]);
	$lname = trim($data[0]);
	$sCount = 0;
	for ($i=0; $i<count($arrSales); $i++)
	{
		if($fname == $arrSales[$i]['FirstName'] &&
			$lname == $arrSales[$i]['LastName'])
			{
				$pid = $arrSales[$i]['PersonID'];
				$sCount++;
			}
	}
	switch ($sCount)
	{
		case 0:
			$msg = "Salesman '" . $data[1] . " " . $data[0] . "' not found";
			return -1;
		case 1:
			return $pid;
		default:
			$msg = 'This company has more than one salesname: ' . $data[1] . ' ' . $data[0] . '. ';
			if(strlen($uid) > 0)
				$msg .= ' SUPPLIED USERID ' . $uid . ' NOT FOUND.';
			else
				$msg .= ' SUPPLY USERID AFTER OTHER FIELDS.';
			return -1;

	}
}

function GetSourceID($data, &$msg)
{
global $SourceUsed, $mpower_companyid;
	$msg='';
	if(!$SourceUsed)
		return "NULL";
	$slabel	= trim($data[3]);
	if(!strlen($slabel))
		return "NULL";
	$sql = "select SourceID from Sources where Deleted = 0 and Name = '$slabel' and CompanyID = $mpower_companyid";
	$ok = mssql_query($sql);
	if ($ok)
	{
		$row = mssql_fetch_assoc($ok);
		if ($row['SourceID'] > 0)
			return $row['SourceID'];
	}
	$msg = "Source '$slabel' not found!";
	return -1;

}

function GetValLevel($data, &$msg)
{
global $SourceUsed, $ValuationUsed, $mpower_companyid;
	$msg='';
	if(!$ValuationUsed)
		return "NULL";
	$valOffset = $SourceUsed ? 4 : 3;
	$vlevel = $data[$valOffset];
	if(!strlen($vlevel))
		return "NULL";
	$sql = "select * from Valuations where VLevel = $vlevel and CompanyID = $mpower_companyid";
	$ok = mssql_query($sql);
	if ($ok)
	{
		$row = mssql_fetch_assoc($ok);
		if($row['Activated'] != 0 && strlen($row['Label']) > 0)
			return $vlevel;
	}
	$msg = "VLevel '$vlevel' is not valid for this company.";
	return -1;
}

function GetDivision($data)
{
global $SourceUsed, $ValuationUsed, $usediv;
	if(!$usediv)
		return "NULL";
	$divOffset = 3;
	if($SourceUsed)
		$divOffset++;
	if($ValuationUsed)
		$divOffset++;
	$div = $data[$divOffset];
	if(!strlen($div))
		return "NULL";
	else
		return $div;
}

function GetContact($data)
{
global $SourceUsed, $ValuationUsed, $usediv, $usecont;
	if(!$usecont)
		return "NULL";
	$contOffset = 3;
	if($SourceUsed)
		$contOffset++;
	if($ValuationUsed)
		$contOffset++;
	if($usediv)
		$contOffset++;
	$contact = $data[$contOffset];
	if(!strlen($contact))
		return "NULL";
	else
		return $contact;
}

function GetSource2ID($data, &$msg)
{
global $usesrc2, $Source2Label, $mpower_companyid, $ValuationUsed, $SourceUsed, $usediv, $usecont, $arrSources2;
	$msg='';
	if(!$usesrc2)
		return "NULL";
	$s2offset = 3; /* was 4 */
    
	if($SourceUsed) {
		$s2offset++;
	}
	if($ValuationUsed) {
		$s2offset++;
	}
	if($usediv) {
		$s2offset++;
	}
	if($usecont) {
		$s2offset++;
	}

	$slabel	= trim($data[$s2offset]);
	if(!strlen($slabel))
		return "NULL";
	for($i=0; $i < count($arrSources2); $i++)
	{
		if($slabel != $arrSources2[$i]['Name'])
			continue;
		return 	$arrSources2[$i]['Source2ID'];
	}
/*
	$sql = "select Source2ID from Sources2 where Name = '$slabel' and CompanyID = $mpower_companyid";
	$ok = mssql_query($sql);
	if ($ok)
	{
		$row = mssql_fetch_assoc($ok);
		if ($row['Source2ID'] > 0)
			return $row['Source2ID'];
	}
*/
	$msg = "$Source2Label '$slabel' not found!  offset=$s2offset";
	return -1;

}


function isDuplicate($pid, $data, &$msg)
{
global $arrTargets;
	$msg='';
	$company = $data[2];
	$dup = 0;
//print("Checking for duplicates.  PID=$pid Company=$company<br>");
	for ($i=0; $i < count($arrTargets); $i++)
	{
		$idTest = $arrTargets[$i]['PersonID'];
		if($idTest < $pid) continue;
		if($idTest > $pid) break;
		if($company == $arrTargets[$i]['Company'])
		{
			$dup = 1;
			break;
		}
	}
	if($dup)
	{
		$fname = $data[1];
		$lname = $data[0];
		$msg ="A " . GetCatLbl("Target") . " with this salesperson and company ($lname, $fname / $company) already exists in the database!";
	}
	return $dup;
}

function ProcessDataLine($data, &$pid, &$sid, &$Company, &$vlevel, &$Division, &$Contact, &$sid2, &$errMsg)
{
	$errMsg = '';
	if(-1==($pid = GetSalesID($data, $msg)))
	{
		$errMsg = $msg;
	}
	if($pid > -1 && isDuplicate($pid, $data, $msg))
	{
		if(strlen($errMsg)) $errMsg .= '<br>';
		$errMsg .= $msg;
	}
	if (-1 == ($sid = GetSourceID($data, $msg))) //returns "'NULL'" if sources not used
	{
		if(strlen($errMsg)) $errMsg .= '<br>';
		$errMsg .= $msg;
	}
	if(-1 == ($vlevel = GetValLevel($data, $msg))) //returns "NULL" if valuations not used
	{
		if(strlen($errMsg)) $errMsg .= '<br>';
		$errMsg .= $msg;
	}
	$Division = GetDivision($data);
	$Contact = GetContact($data);
	if (-1 == ($sid2 = GetSource2ID($data, $msg))) //returns "'NULL'" if sources not used
	{
		if(strlen($errMsg)) $errMsg .= '<br>';
		$errMsg .= $msg;
	}
	return (strlen($errMsg) == 0);
}


function ReadTargetFile($fh)
{
global $arrErrorMsgs, $arrImports, $uploaded_file;
   $arg_list = func_get_args();
	$bPrintHTML = 0;
	if(count($arg_list) > 1)
		$bPrintHTML = $arg_list[1];
	$row=1;
	$success=0;

	$data = fgets($fh);
	if($data)
	{
//Make this call accommodate variable field counts!
//				$check = preg_match('/^[[:ascii:]]+,[[:ascii:]]+,[[:ascii:]]+$/', $data);
		$check = checkFileFormat($data, $msg);
		if(!$check)
		{
			if($bPrintHTML) print("<center>File <b>" . basename($uploaded_file) . "</b> $msg</center><br>");
			writeinslog("--- INVALID CSV FILE ---");
			return 0;
		}
		else if($bPrintHTML)
		{
			print("<table width=100%><tr>");
			print("<td width=20%></td>");
			print("<td width=30%>File Name:</td>");
			print("<td width=50%><b>" . basename($uploaded_file)  . "<b></td></tr>");
			print("<tr><td></td><td>File Upload Date:</td>");
			print("<td><b>" . date("F j, Y") . "</b></td></tr>");
			print("</table><br><br>");
		}
		fseek($fh, 0);
	}
//Main processing loop
	$arrErrorMsgs = array();
	$arrImports = array();
	while ($check && ($data = fgetcsv($fh, 1000, ",")) !== FALSE)
	{
		$ok = ProcessDataLine($data, $pid, $sid, &$Company, $vlevel, $Division, $Contact, $sid2, $msgGroup);
		if($ok)
		{
			$tmp = array();
			$tmp['companyid'] = $mpower_companyid;
			$tmp['company'] = $data[2];
			$tmp['personid'] = $pid;
			$tmp['sourceid'] = $sid;
			$tmp['division'] = $Division;
			$tmp['contact'] = $Contact;
			$tmp['vlevel'] = $vlevel;
			$tmp['sid2'] = $sid2;
			array_push($arrImports, $tmp);

		}
		else
		{
			$tmp = array();
			$tmp['row'] = $row;
			$tmp['msg'] = $msgGroup;
			array_push($arrErrorMsgs, $tmp);
			$row++;
			continue;
		}
		$row++;
		$success++;
	}
	return $row;
}

?>
