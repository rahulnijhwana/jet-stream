<?php

include_once (BASE_PATH . '/slipstream/class.EventType.php');
require_once 'color_config.php';

$objEventType=new EventType();
$eventType=$objEventType->getAllEventTypeByCompanyId($mpower_companyid);

$rows=count($colors)/BLOCKS_PER_ROW;
if(count($colors) % BLOCKS_PER_ROW  > 0)
	$rows++;

echo 'var companyId = '.$mpower_companyid .';';
echo 'var COLOR_CODE = '.json_encode($colors).';';
echo 'var BLOCKS_PER_ROW = '.BLOCKS_PER_ROW.';';
echo 'var ROWS = '.json_encode($rows).';';
echo 'var EVENTTYPE = '.json_encode($eventType).';';

?>

