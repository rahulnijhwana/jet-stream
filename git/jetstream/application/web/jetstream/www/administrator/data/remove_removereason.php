<?php
/**
 * @package Data
 */

print('<script language="JavaScript">');

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_admin_login())
{
	print('</script>');
	close_db();
	exit();
}

$result = mssql_query("UPDATE removereasons SET Deleted = 1 WHERE ReasonID = $mpower_remove_value");

print('window.result = ' . ($result ? 'true' : 'false') . ';</script>');

close_db();

?>