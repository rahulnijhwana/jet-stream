<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login())
{
	close_db();
	exit();
}
print('<div id="errmsg">');
// the data is now passed in an a '\012' delimited array
$rows = explode("\n", str_replace("\r\n","\n",$mpower_data));
$len = count($rows);
$result=true;
for ($i = 0; $i < $len; ++$i)
{
	$data_parts = explode('|', $rows[$i]);
	$DealID = $data_parts[0];
	$SubID = $data_parts[1];
	$Answer = $data_parts[2];
	$CompletedLocation = $data_parts[3];
	$checkanswer_result = mssql_query("SELECT * FROM SubAnswers WHERE DealID = $DealID AND SubID = $SubID");
	if (mssql_num_rows($checkanswer_result) > 0)
	{
		$sql="UPDATE SubAnswers SET Answer = '$Answer', CompletedLocation = $CompletedLocation WHERE DealID = $DealID AND SubID = $SubID and Answer <> '$Answer'";
		$update_result = mssql_query($sql);
		if ($update_result == false)
		{
			$result=false;
			print("<br>$sql<br>");
		}
	}
	else
	{
		$sql="INSERT INTO SubAnswers (DealID, SubID, Answer, CompletedLocation) VALUES ($DealID, $SubID, '$Answer', $CompletedLocation)";
		$insert_result = mssql_query($sql);
		if ($insert_result == false)
		{
			$result=false;
			print("<br>$sql<br>");
		}
	}
}
print('</div>');
$result=true;
print('<script language="JavaScript">');
print('window.result = ' . (($result==true) ? 'true' : 'false') . ';</script>');

close_db();

?>
