<?php

define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));
include_once (BASE_PATH . '/slipstream/class.AccountMap.php');
include_once (BASE_PATH . '/slipstream/class.Section.php');

$objAccountMap = new AccountMap();
$objSection = new Section();
$accountSections = $objSection->getAccountSectionByCompanyId($mpower_companyid);

$allAccountFields = $objAccountMap->getFieldsFromAccountMapByCompId($mpower_companyid,'%');
$unUsedFields = array();
$leftSectionFields = array();
$rightSectionFields = array();
$totalAccountFields = count($allAccountFields);
$validPosition = false;

for($i=0;$i<count($allAccountFields);$i++){
	$hasSection = false;
//Check if SectionID is NULL then the field is not assigned.
	if($allAccountFields[$i]['SectionID'] == NULL || $allAccountFields[$i]['SectionID'] == ''){
		$unUsedFields[] = array('AccountMapID'=>$allAccountFields[$i]['AccountMapID'],'LabelName'=>$allAccountFields[$i]['LabelName']);
	}else{
		//If the section is present or not.
		foreach($accountSections as $sections){
			if($allAccountFields[$i]['SectionID'] == $sections['SectionID']){
				$sectionName = str_replace(' ', '',$sections['AccountSectionName']);
				$hasSection = true;
			}
		}
		//Check to see if position and align value is same for more than one field. 
		$validPosition = checkAccountFieldPosition($allAccountFields[$i],$mpower_companyid);
		if($hasSection && $validPosition){
			if($allAccountFields[$i]['Align'] == 1){
				$leftSectionFields[] = array('AccountMapID'=>$allAccountFields[$i]['AccountMapID'],'LabelName'=>$allAccountFields[$i]['LabelName'],'Position'=>$allAccountFields[$i]['Position'],'Align'=>$allAccountFields[$i]['Align'],'SectionName'=>$sectionName,'TabOrder'=>$allAccountFields[$i]['TabOrder']);
			}else{
				$rightSectionFields[] = array('AccountMapID'=>$allAccountFields[$i]['AccountMapID'],'LabelName'=>$allAccountFields[$i]['LabelName'],'Position'=>$allAccountFields[$i]['Position'],'Align'=>$allAccountFields[$i]['Align'],'SectionName'=>$sectionName,'TabOrder'=>$allAccountFields[$i]['TabOrder']);
			}
		}else{
			//If section has been deleted or there is duplicate position or align value of the field then put the filed in unassigned array.
			$unUsedFields[] = array('AccountMapID'=>$allAccountFields[$i]['AccountMapID'],'LabelName'=>$allAccountFields[$i]['LabelName']);
			$allAccountFields[$i]['Align'] = NULL;
			$allAccountFields[$i]['Position'] = NULL;
			$allAccountFields[$i]['SectionID'] = NULL;
		}
	}
}

$accSectionDetails = array();
$accSection = array();
$accSectionName = array();
$accSectionSize = array();
if(count($accountSections) == 0){
	$objSection ->saveSection($mpower_companyid,'DefaultSection',NULL);
	$accSection[] = 'DefaultSection';
	$accSectionName[] = 'DefaultSection';
}
else{
	foreach($accountSections as $sections){
		$accSection[] = str_replace(' ', '', $sections['AccountSectionName']);
		$accSectionName[] = $sections['AccountSectionName'];
		$accSectionDetails[] = array('SectionName'=>$sections['AccountSectionName'],'SectionId'=>$sections['SectionID']);
		$maxPosition = getSectionMaxFields($sections['SectionID'],$mpower_companyid);
		if($maxPosition == NULL || $maxPosition == '')
			$maxPosition =1;

		if($maxPosition > 9){
			$accSectionSize[] = ($maxPosition + 1);
		}else{
			$accSectionSize[] = 10;
		}	
	}
}

echo 'var companyId = '.$mpower_companyid .';';
echo 'var ACCOUNTSECTIONNAME = '.json_encode($accSectionName).';';
echo 'var LEFTSECTION = '.json_encode($leftSectionFields).';';
echo 'var RIGHTSECTION = '.json_encode($rightSectionFields).';';
echo 'var JASONACCOUNTSECTION = '.json_encode($accSection).';';
echo 'var ACCOUNTSECTIONDETAILS = '.json_encode($accSectionDetails).';';
echo 'var ACCOUNTFIELDS = '.json_encode($unUsedFields).';';
echo 'var TOTALFIELDS = '.$totalAccountFields.';';
echo 'var ACCOUNTSECTSIZE = '.json_encode($accSectionSize).';';


function checkAccountFieldPosition($accountField,$compId){
	$objAccountMap = new AccountMap();
	$result = $objAccountMap->countAccountFieldsBySectionAlign($compId,$accountField['SectionID'],$accountField['Align'],$accountField['Position']);
	if($result['count'] == 1)
		return true;
	else
		return false;
}

function getSectionMaxFields($sectionId,$compId){
	$objAccountMap = new AccountMap();
	return $objAccountMap->getMaxPositionBySecId($sectionId,$compId);
}
?>
function addAccountLayoutData(section)
{
	var usedValues = new Array();
	for(m=0;m<section.length;m++)
	{
		if(section[m].Align==1){
			div_no=(section[m].Position*2)-1;
		}else{
			div_no=(section[m].Position*2);
		}
		var tabLabel = '';
		if(section[m].TabOrder != '' && section[m].TabOrder != null)
			tabLabel = '['+ section[m].TabOrder +']';
		else
			tabLabel = '[-]';

		div_id = section[m].SectionName+'_'+div_no;
		document.getElementById(div_id).innerHTML = '<div class="clsSectionSmallBox" id="'+escape(section[m].LabelName)+'_'+ section[m].AccountMapID+'" ondblclick="editFieldDetails(this)">'+section[m].LabelName+' '+tabLabel+'</div>';
		usedValues[m] = escape(section[m].LabelName) +'_'+ section[m].AccountMapID;
	}
	return usedValues;
}

function saveCompanySectionLayout()
{
	var unAssignedData = '';
	for(k=0;k<document.getElementById('totalFieldsDiv').childNodes.length;k++)
	{
		if(document.getElementById('totalFieldsDiv').childNodes[k].id){
			unAssignedData += document.getElementById('totalFieldsDiv').childNodes[k].id + "|";
		}
			//alert(document.getElementById('totalFieldsDiv').childNodes[k].id);
	}
	var secData='';
	if(sections)
	{
		for(j=0;j<sections.length;j++)
		{
			if(j == 0)
				secData = sections[j];
			else
				secData += ','+sections[j];
		}
	}
	dataLayout = getLayoutSettings();
	$.post('../../ajax/ajax.companylayout.php',
		{
			dataLayout:dataLayout,
			action:'saveCompanySectionLayout',
			unAssignedData : unAssignedData,
			sections:secData,
			companyId : companyId
		},
		processSaveCompanySectionLayout
	);
}

function processSaveCompanySectionLayout(accountSections)
{
	var lastContainerInner = '';
	for(i=0;i<JASONACCOUNTSECTION.length;i++){
		var secSize = ACCOUNTSECTSIZE[i];
		var lastContainerName = JASONACCOUNTSECTION[i]+'_'+(secSize*2);
		if(document.getElementById(lastContainerName))
			lastContainerInner = document.getElementById(lastContainerName).innerHTML;
		
		if(lastContainerInner != ''){
			ACCOUNTSECTSIZE[i] = ACCOUNTSECTSIZE[i] + 1;
			manageCompany();
		}else{
			if(secSize > 10){
				//To Do: Reduce the total no of blank divs as per the existing max field size.
			}
		}
	}
}
