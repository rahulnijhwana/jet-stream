<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>M-Power Dashboard</title>
		<link rel="stylesheet" type="text/css" href="../css/sharedstyles.css,boardstyles.css,tooltipstyle.css,calendarstyles.css">
        <script language="JavaScript">
        	window.forceAsWindower = true;
			<?php 
			require '../data/get_board.php'; 
			?>
        </script>
        <script language="JavaScript" src="../javascript/ci_confirm.js,utils.js,windowing.js,pageformat.js,loginscreen.js,saver2.js,load_dropdowns.js,category_labels.js,get_sales_data.js,main_alerts.js"></script>

		<?php
		/*
		Comment out the above JS group and uncomment this to more easily debug the JS
        <script language="JavaScript" src="../javascript/main.js,main_analysis.js,main_grid.js,main_calendar.js,main_trends.js,main_spreadsheet.js,main_tree.js,smartselect.js,offerings.js"></script>
		*/
		?>

		<script language="JavaScript" src="../javascript/main.js"></script>
        <script language="JavaScript" src="../javascript/main_analysis.js"></script>
        <script language="JavaScript" src="../javascript/main_grid.js"></script>
        <script language="JavaScript" src="../javascript/main_calendar.js"></script>
        <script language="JavaScript" src="../javascript/main_trends.js"></script>
        <script language="JavaScript" src="../javascript/main_spreadsheet.js"></script>
        <script language="JavaScript" src="../javascript/main_tree.js"></script>
        <script language="JavaScript" src="../javascript/smartselect.js"></script>
        <script language="JavaScript" src="../javascript/offerings.js"></script>

        </head>

    <body leftmargin=3 rightmargin=3 topmargin=3 bottommargin=3 bgcolor="white"
        onload="setTimeout('loaded()',1);" onunload="Windowing.closeAllWindows()" onresize="loaded()">
        <script language="JavaScript">
            subTrendNames();
            init();
        </script>

        <noscript>
            <p align="center"><font color="red"><b>This page requires JavaScript.</b></font></p>
        </noscript>
    </body>
</html>

