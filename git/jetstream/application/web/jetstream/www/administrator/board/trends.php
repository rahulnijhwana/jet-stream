<?php
/**
* @package Board
*/
$salesid = (isset($_REQUEST['salesid'])) ? $_REQUEST['salesid'] : null;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>M-Power</title>
        <link rel="stylesheet" type="text/css" href="../css/sharedstyles.css,boardstyles.css">
        <script language="JavaScript" src="../javascript/windowing.js,pageformat.js,utils.js"></script>

        <?php
        if ($salesid) {		//---------start of standalone
            ?>
            <script language="JavaScript" src="../javascript/main_trends.js,get_sales_data.js,category_labels.js,utils.js"></script>
            <script language="JavaScript">
                <?php
                include_once('../data/_base_utils.php');
                include_once('../data/_query_utils.php');

                if (!check_user_login()) {
                    close_db();
                    print('</script></head></html>');
                    exit();
                }
                include('../data/get_user.php');
                include('../data/get_company.php');
                include('../data/get_trends.php');


                $sql = "select * from logins where UserID = '$salesid' and CompanyID = '$mpower_companyid'";

                $result = mssql_query($sql);
                if (($row = mssql_fetch_assoc($result)))
                {
                    $pid = $row['PersonID'];
                }
                $sql="SELECT * FROM people WHERE PersonID=$pid";
                print("//SQL: $sql\n");
                dump_sql_as_array('g_people', $sql);

                $sql = "SELECT * FROM opportunities WHERE CompanyID = '$mpower_companyid'";
                $sql .= " AND (Category != '6' OR ActualCloseDate >= cast('$first' as datetime))";
                $sql .= " AND (Category != '9')";
                $sql .= " AND PersonID =$pid";

                dump_sql_as_array('g_opps', $sql);

                Print("set_cookie('mpower_userid','$mpower_userid');");
                Print("set_cookie('mpower_pwd','$mpower_pwd');");
                Print("set_cookie('mpower_companyid',$mpower_companyid);");
                ?>

                g_standalone=true;
                PID=g_people[0][g_people.PersonID];
                subTrendNames();

                function open_trend(trendID, personID) {
                    set_cookie('mpower_trendid', trendID);
                    Windowing.openSizedWindow('trend.html#' + trendID + '_' + personID, getWindowHeight(), 790, 'trenddetails');

                }
                
                // this should probably go in an include somewhere
                function getWindowHeight() {
                    var hgt = 120;
                    if(navigator.appName == "Netscape")
                    {
                        hgt = window.innerHeight;

                    }
                    else if(navigator.appName == "Microsoft Internet Explorer") {

                        hgt += document.body.clientHeight;
                    }
                    return hgt;
                }

            </script>
            <?php
        }
        else {			//---------start of non-standalone
            ?>
            <script language="JavaScript">
            <!--
                g_standalone=false;
                g_company = window.opener.g_company;
                g_user = window.opener.g_user;

                var header = window.location.hash.toString();
                PID=-1;
                if (header.charAt(0) == '#') PID = header.substr(1);

                get_salesperson_info=window.opener.get_salesperson_info;
                Trends=window.opener.Trends;
                open_trend=window.opener.open_trend;
                getWindowHeight=window.opener.getWindowHeight;
            // -->
            </script>
            <?php
        }
        ?>
    </head>

    <body bgcolor="white">
        <script language="JavaScript">
            <!--

            function init() {
                var headerText = 'Trends';

                if (PID == -1) headerText += ' / All Salespeople';
                else {
                    var info = get_salesperson_info(PID)
                    if (info.name) headerText += ' / ' + info.name;
                }

                Header.setText(headerText);
                Header.addButton(ButtonStore.getButton('Trends'));
                Header.addButton(ButtonStore.getButton('Print'));
                document.writeln(Header.makeHTML());
            }

            function do_trends() {
                document.body.style.cursor = 'wait';
                if (window.opener) window.opener.do_trends();
                document.body.style.cursor = 'auto';
            }

            function show_trends() {
                var trendsPerLine = Math.floor(document.body.offsetWidth / 130);
                if (PID == -1) {
                    document.writeln(Trends.listAll(trendsPerLine));
                }
                else {
                    document.writeln(Trends.list(PID, trendsPerLine));
                }
            }

            init();
            var ivoryBox = new IvoryBox('100%', null);
            document.writeln(ivoryBox.makeTop());
            show_trends();
            document.writeln(ivoryBox.makeBottom());

            // -->
        </script>

    </body>
</html>
