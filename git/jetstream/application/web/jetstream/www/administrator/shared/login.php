<?php
/*
$inc_path = getcwd();
for ($k = 0; $k < 1; ++$k)
	$inc_path = substr($inc_path, 1, strrpos($inc_path, "\\"));
ini_set('include_path', ".;$inc_path\\data;e:\\php\\includes");
ini_set('include_path', ".;" . substr(getcwd(), 0, strrpos(getcwd(), "\\")) . "\\data;e:\\php\\includes");
*/

// if (isset($_POST['noUpdateLastUsed']))
// 	setcookie('noUpdateLastUsed', 'true');

// if (isset($_POST['quickopen']))
//	setcookie('quickopen', 'true');


ob_start();
include_once("../data/_base_utils.php");

print("<!-- login: ");
check_user_login();
print(" -->\n");


// I'm using the quickopen flag as a key that the user is coming from the quickopen
// I don't want to trigger the EULA for the quickopen, so this should bypass it
// NY - 12/12/2007
if (isset($quickopen)) {
    $currentuser_eula = 1;
}
elseif (0 < $currentuser_level && isset($seeneula))
{
	// store the fact that this user has seen the eula in the db
//	mssql_query("update people set Eula='1' where UserID='$mpower_userid'");
	mssql_query("update logins set Eula='1' where UserID='$mpower_userid' AND CompanyID = $mpower_companyid");

	$currentuser_eula = 1;
}

$company_result = mssql_query("select * from company where CompanyID = $mpower_companyid");
$company_row = mssql_fetch_assoc($company_result);
if ($company_row['Disabled'] > 0)
{
	exit("Sorry, this company has been disabled");
}

if ((0 < $currentuser_level || -3 ==  $currentuser_level) && $currentuser_eula != 0)
{
	/* // Below was moved to get_board. Read note there.
	$lastused = date('Y-m-d H:i:s');
	$sql = "update people set LastUsed = '$lastused'";
	$sql .= " where UserID = '$mpower_userid' and CompanyID = '$mpower_companyid'";
	//print($sql);
	$result = mssql_query($sql);
	*/
	
	if (!isset($noUpdateLastUsed))
	{
		/*
		Here we create a LoginSession to track how long the user was in the system this time around
		The EndTime gets updated every minute by the board
		*/

		$sqlstr = "insert into LoginSessions (LoginID,StartTime,EndTime,CompanyID) values ($currentuser_loginid,GetDate(),GetDate(),$mpower_companyid)";
		mssql_query($sqlstr);

		$result = mssql_query("SELECT @@IDENTITY AS 'LoginSessionID'");
		$temp_assoc = mssql_fetch_assoc($result);
		setcookie('LoginSessionID', $temp_assoc['LoginSessionID']);
		setcookie('currentuser_loginid', $currentuser_loginid);
	}

	if ($currentuser_isadmin)
		$location = '../admin/admin.html';
	else
		$location = '../board/main.php';

	print("<!-- loc: $location -->");
	header("Location: $location");
	close_db();
	exit();
}


// header("Location: {$_SERVER['HTTP_REFERER']}?fail=true");
// exit();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- <title>M-Power</title> -->
<link rel="stylesheet" type="text/css" href="../css/sharedstyles.css">
<script language="JavaScript" src="../javascript/utils.js,windowing.js,pageformat.js,loginscreen.js"></script>
</head>

<body bgcolor="white">
<?php if ($currentuser_level <= 0) { ?>

<script language="JavaScript">
<!--
check_login();
// -->
</script>

<?php } else { ?>

<script language="JavaScript">
<!--
Header.setText('End User License Agreement');
Header.addButton(ButtonStore.getButton('Print'));
document.writeln(Header.makeHTML());
var ivoryBox = new IvoryBox('100%', null);
// ivoryBox.m_loc = '../shared/';
document.writeln(ivoryBox.makeTop());
// -->
</script>

<p><b>M-POWER<sup>&trade;</sup> TERMS OF USE</b></p>
<div style="height: 300px; border: 1px solid black; overflow: auto; padding: 4; background-color: white">
<div>

<?php
$eulatext = '';
$result = mssql_query("select EulaText from systemsettings where SystemID='1'");
if ($row = mssql_fetch_assoc($result))
{
	$eulatext = $row['EulaText'];
	$eulatext = stripslashes($eulatext);
}
print($eulatext);
?>

</div>
</div>

<p><i>By clicking "I Accept", you acknowledge that you have read and agree to the terms and conditions above.</i></p>
<center>
<form name="form1" action="login.php" method="post">
<input class="command" type="submit" name="seeneula" value="I Accept"> <input class="command" type="button" name="declines" value="I Decline" onclick="window.history.go(-1);">
</form>
</center>

<script language="JavaScript">
<!--
document.writeln(ivoryBox.makeBottom());
// -->
</script>

<?php } ?>

</body>
</html>

<?php
ob_end_flush();
close_db();
?>
