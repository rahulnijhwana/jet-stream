<?php
require_once 'class.DbConnManager.php';
require_once 'class.SqlBuilder.php';
require_once 'class.SectionUtil.php';

define(LABEL_TYPE_ACCOUNT, 'Account');
define(LABEL_TYPE_CONTACT, 'Contact');

class LabelUtil {
	
	private $type;
	private $table;
	private $label;
	private $section;
	private $sectionUtil;
	private $companyId;
	private $reminderField;
	
	public function __construct($type, $field, $reminderField, $sectionUtil, $companyId){
		
		$this->companyId = $companyId;
		$this->setSectionUtil($sectionUtil);
		$this->reminderField = $reminderField;
		
		$arr = self::getFieldInfo($type, $field, $companyId);
				
		$this->setType($type);
		$this->setLabel($arr['LabelName']);
		$this->setTable($type . 'Map');
		$this->setSection($arr['Section']);
	}
	
	public static function getFieldInfo($type, $field, $companyId){
		$table = $type . 'Map';
		$sql = "
			SELECT
				LabelName,
				" . $type . "SectionName AS Section
			FROM
				" . $table . "
			
			JOIN Section ON " . $table . ".SectionID = Section.SectionID
			WHERE
				FieldName = ? AND " . $table . ".CompanyID = ?";
		
		$params[] = array(DTYPE_STRING, $field);
		$params[] = array(DTYPE_INT, $companyId);
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		$results = DbConnManager::GetDb('mpower')->Exec($sql);
		
		return $results[0];
	}
	
	public function show(){
		$html = '<div>';
		$html .= $this->getType() . ' : ' . $this->getSection() . ' : ';
		$label = $this->getLabel();
		$html .= '<div class="selectLabel">';
		$html .= $this->buildLabelSelect($sectionId, $label);
		$html .= '</div>';
		$html .= '</div>';
		echo $html;
	}
	
	public function getType(){
		return $this->type;
	}
	public function getTable(){
		return $this->table;
	}
	public function getLabel(){
		return $this->label;
	}	
	public function getSection(){
		
		if(empty($this->section)){
			return 'DefaultSection';
		}
		else {
			return $this->section;
		}
	}
	
	private function setType($type){
		$this->type = $type;
	}
	private function setTable($table){
		$this->table = $table;
	}
	private function setLabel($label){
		$this->label = $label;
	}
	private function setSection($section){
		$this->section = $section;
	}
	
	private function setSectionUtil($sectionUtil){
		$this->sectionUtil = $sectionUtil;
	}
	
	public function buildLabelSelect($sectionId, $selected=''){
		
		/*
		$sql = "SELECT DISTINCT(Section.SectionID) FROM AccountMap 
				JOIN Section ON AccountMap.SectionID = Section.SectionID 
				WHERE Section.CompanyID = ?";
		*/
		
		$sql = "SELECT LabelName, FieldName FROM " . $this->getType() . "Map WHERE SectionID = ? ORDER BY LabelName";
		
		
		$params = array(DTYPE_INT, $this->sectionUtil->getDefaultSectionId($this->getType()));
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql($params);
		$results = DbConnManager::GetDb('mpower')->Exec($sql);
		
		$html = '';
		
		if(count($results) > 0){
			$html = '<select id="' . $this->reminderField . '" name="' . $this->reminderField . '" class="required" width="200"><option value="">Select field</option>';
			foreach($results as $field){
				$set = '';
				if($field['LabelName'] == $selected){
					$set = ' selected="selected"';
				}
				$html .= '<option value="' . $field['FieldName'] . '" ' . $set . '>'  . $field['LabelName'] . '</option>';
			}
			$html .= '</select>';
		}
		return $html;
	}
}