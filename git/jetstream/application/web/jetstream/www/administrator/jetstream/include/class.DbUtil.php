<?php
require_once 'class.DbConnManager.php';
require_once 'class.SqlBuilder.php';
require_once 'class.DebugUtil.php';

class DbUtil {
	
	/**
	 * Save a dbClass object to the database.
	 * If the pk has a value, the object will be updated,
	 * otherwise a new record will be inserted.
	 * @param Object $obj
	 */
	public static function save($obj){
		
		$action = '';
		$map = $obj->getMap();
		
		foreach($map as $k => $v){
			if($v->isPrimaryKey()){
				$id = self::get($k, $v, $obj);
				if($id){
					$action = 'update';
					$pk = $v->getColumnName(); 
				}
				else {
					$action = 'insert';
					continue;
				}
			}
			else {
				$columns[] = $v->getColumnName();
				$params[] = array($v->getDataType(), self::get($k, $v, $obj));
			}
		}
		
		switch($action)
		{
			case 'update' :
				self::update($obj->getTable(), $columns, $params, $pk, $id);
				break;
			case 'insert' :
				self::insert($obj->getTable(), $columns, $params);
				break;
			default :
				echo 'ERROR! Action is undefined';
		}
	}
	
	public static function insert($table, $columns, $params){
		
		$sql = "INSERT INTO " . $table;
		$keys = implode(',', $columns);
		$sql .= ' (' . $keys . ') VALUES (';
		
		$preparedValues = array_fill(0, count($columns), '?');
		$sql .= implode(',', $preparedValues) . ')';
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		DbConnManager::GetDb('mpower')->Exec($sql);
		//echo $sql;
		
	}
	
	public static function update($table, $columns, $params, $pk, $id){
		
		$sql = "UPDATE " . $table . " SET ";
		$keys = implode(' = ?, ', $columns);
		$sql .= $keys . " = ? WHERE ";
		$sql .= $pk . " = ?";

		$params[] = array(DTYPE_INT, $id);
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
		DbConnManager::GetDb('mpower')->Exec($sql);
		//echo $sql;
		
	}
	
	private static function get($name, $map, $obj){
		if($map->getDataType() == DTYPE_BOOL){
			$getter = 'is';
		}
		else {
			$getter = 'get';
		}
		$getter .= ucfirst($name);
		return $obj->$getter();
	}
	
	/**
	 * Get one row from the server matching a
	 * specified pk value
	 * @param Object $obj
	 */
	public function getOne($table, $pk, $id){
		
		$sql = "SELECT * FROM " . $table . " WHERE " . $pk . " = ?";
		
		$param = array(DTYPE_INT, $id);
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql($param);
		$result = DbConnManager::GetDb('mpower')->GetOne($sql);
		
		return $result;
		
	}
}


