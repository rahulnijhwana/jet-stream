<?php
$sn = strip_tags($_GET['SN']);
session_name($sn);
session_start();

if(!$_SESSION['quickpass']){
        header("Location: https://www.mpasa.com/jetstream/");
}

require_once 'class.CompanyUtil.php';
require_once 'class.UserUtil.php';
require_once 'class.DebugUtil.php';
require_once 'class.SessionManager.php';
require_once 'include/class.Display.php';
require_once 'lib/class.ImapCompanySettings.php';
require_once 'lib/class.ImapServer.php';
require_once 'include/class.ImapUtil.php';
require_once 'include/class.PersonUtil.php';

$parts = explode('/', $_SERVER['SCRIPT_NAME']);
$pg = $parts[count($parts) - 1];

switch($pg)
{
	case 'index.php' :
		$script = 'script.js';
		break;
	case 'reminder.php' :
		$script = 'reminder.js';
		break;
}

$companyId = $_SESSION['company_id'];
$company = CompanyUtil::getCompany($companyId);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
		<title>Jetstream Administration Area</title>
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/jquery.qtip.css" />
		<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
		<link href='https://fonts.googleapis.com/css?family=Istok+Web&v2' rel='stylesheet' type='text/css' />
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
		<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
		<script type="text/javascript" src="js/jquery.qtip.min.js"></script>
		<script type="text/javascript" src="js/jquery.blockUI.js"></script>
		<script type="text/javascript" src="js/<?php echo $script; ?>"></script>
	</head>
	<body>
		<div id="top">
			<div class="header_left">
				<h1>Jetstream Administration Area</h1>
			</div>
			<div class="header_right">
				<?php echo $company['Name']; ?>
			</div>
		</div>
		<div class="clearfix"></div>
		<div id="wrapper">
