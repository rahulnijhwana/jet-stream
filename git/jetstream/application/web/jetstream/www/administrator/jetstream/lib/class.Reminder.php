<?php
require_once 'interface.dbClass.php';

class Reminder implements dbClass {
	
	
	private $id;
	private $companyId;
	private $accountAddress1Field;
	private $accountAddress2Field;
	private $accountCityField;
	private $accountStateField;
	private $accountZipField;
	private $accountPhoneField;
	private $accountFaxField;
	private $accountUrlField;
	private $contactAddress1Field;
	private $contactAddress2Field;
	private $contactCityField;
	private $contactStateField;
	private $contactZipField;
	private $contactPhoneField;
	private $table = 'Reminder';
	private $map = array();
	
	public function __construct($input){

		if(is_numeric($input)){
			$row = DbUtil::getOne($this->table, 'CompanyID', $input);
			$this->setId($row->ReminderID);
			$this->setCompanyId($input);
			$this->setAccountAddress1Field($row->AccountAddress1Field);
			$this->setAccountAddress2Field($row->AccountAddress2Field);
			$this->setAccountCityField($row->AccountCityField);
			$this->setAccountStateField($row->AccountStateField);
			$this->setAccountZipField($row->AccountZipField);
			$this->setAccountPhoneField($row->AccountPhoneField);
			$this->setAccountFaxField($row->AccountFaxField);
			$this->setAccountUrlField($row->AccountUrlField);
			
			$this->setContactAddress1Field($row->ContactAddress1Field);
			$this->setContactAddress2Field($row->ContactAddress2Field);
			$this->setContactCityField($row->ContactCityField);
			$this->setContactStateField($row->ContactStateField);
			$this->setContactZipField($row->ContactZipField);
			$this->setContactPhoneField($row->ContactPhoneField);
		}
		elseif(is_array($input)){
			
			$this->setId($input['ReminderID']);
			$this->setCompanyId($input['CompanyID']);
			
			$this->setAccountAddress1Field($input['AccountAddress1Field']);
			$this->setAccountAddress2Field($input['AccountAddress2Field']);
			$this->setAccountCityField($input['AccountCityField']);
			$this->setAccountStateField($input['AccountStateField']);
			$this->setAccountZipField($input['AccountZipField']);
			$this->setAccountPhoneField($input['AccountPhoneField']);
			$this->setAccountFaxField($input['AccountFaxField']);
			$this->setAccountUrlField($input['AccountUrlField']);
			
			$this->setContactAddress1Field($input['ContactAddress1Field']);
			$this->setContactAddress2Field($input['ContactAddress2Field']);
			$this->setContactCityField($input['ContactCityField']);
			$this->setContactStateField($input['ContactStateField']);
			$this->setContactZipField($input['ContactZipField']);
			$this->setContactPhoneField($input['ContactPhoneField']);
			
		}
		
	}
	
	public function getId(){
		return $this->id > 0 ? $this->id : 0;
	} 
	public function getCompanyId(){
		return $this->companyId;
	}
	public function getAccountAddress1Field(){
		return $this->accountAddress1Field;
	}
	public function getAccountAddress2Field(){
		return $this->accountAddress2Field;
	}
	public function getAccountCityField(){
		return $this->accountCityField;
	}
	public function getAccountStateField(){
		return $this->accountStateField;
	}
	public function getAccountZipField(){
		return $this->accountZipField;
	}
	public function getAccountPhoneField(){
		return $this->accountPhoneField;
	}
	public function getAccountFaxField(){
		return $this->accountFaxField;
	}
	public function getAccountUrlField(){
		return $this->accountUrlField;
	}
	public function getContactAddress1Field(){
		return $this->contactAddress1Field;
	}
	public function getContactAddress2Field(){
		return $this->contactAddress2Field;
	}
	public function getContactCityField(){
		return $this->contactCityField;
	}
	public function getContactStateField(){
		return $this->contactStateField;
	}
	public function getContactZipField(){
		return $this->contactZipField;
	}
	public function getContactPhoneField(){
		return $this->contactPhoneField;
	}
	
	public function setId($id){
		$this->id = $id;
		$this->setMap('id', DTYPE_INT, 'ReminderID', true);
	}
	public function setCompanyId($companyId){
		$this->companyId = $companyId;
		$this->setMap('companyId', DTYPE_INT, 'CompanyID');
	}
	public function setAccountAddress1Field($accountAddress1Field){
		$this->accountAddress1Field = $accountAddress1Field;
		$this->setMap('accountAddress1Field', DTYPE_STRING, 'AccountAddress1Field');
	}
	public function setAccountAddress2Field($accountAddress2Field){
		$this->accountAddress2Field = $accountAddress2Field;
		$this->setMap('accountAddress2Field', DTYPE_STRING, 'AccountAddress2Field');
	}
	public function setAccountCityField($accountCityField){
		$this->accountCityField = $accountCityField;
		$this->setMap('accountCityField', DTYPE_STRING, 'AccountCityField');
	}
	public function setAccountStateField($accountStateField){
		$this->accountStateField = $accountStateField;
		$this->setMap('accountStateField', DTYPE_STRING, 'AccountStateField');
	}
	public function setAccountZipField($accountZipField){
		$this->accountZipField = $accountZipField;
		$this->setMap('accountZipField', DTYPE_STRING, 'AccountZipField');
	}
	public function setAccountPhoneField($accountPhoneField){
		$this->accountPhoneField = $accountPhoneField;
		$this->setMap('accountPhoneField', DTYPE_STRING, 'AccountPhoneField');
	}
	public function setAccountFaxField($accountFaxField){
		$this->accountFaxField = $accountFaxField;
		$this->setMap('accountFaxField', DTYPE_STRING, 'AccountFaxField');
	}
	public function setAccountUrlField($accountUrlField){
		$this->accountUrlField = $accountUrlField;
		$this->setMap('accountUrlField', DTYPE_STRING, 'AccountUrlField');
	}
	
	public function setContactAddress1Field($contactAddress1Field){
		$this->contactAddress1Field = $contactAddress1Field;
		$this->setMap('contactAddress1Field', DTYPE_STRING, 'ContactAddress1Field');
	}
	public function setContactAddress2Field($contactAddress2Field){
		$this->contactAddress2Field = $contactAddress2Field;
		$this->setMap('contactAddress2Field', DTYPE_STRING, 'ContactAddress2Field');
	}
	public function setContactCityField($contactCityField){
		$this->contactCityField = $contactCityField;
		$this->setMap('contactCityField', DTYPE_STRING, 'ContactCityField');
	}
	public function setContactStateField($contactStateField){
		$this->contactStateField = $contactStateField;
		$this->setMap('contactStateField', DTYPE_STRING, 'ContactStateField');
	}
	public function setContactZipField($contactZipField){
		$this->contactZipField = $contactZipField;
		$this->setMap('contactZipField', DTYPE_STRING, 'ContactZipField');
	}
	public function setContactPhoneField($contactPhoneField){
		$this->contactPhoneField = $contactPhoneField;
		$this->setMap('contactPhoneField', DTYPE_STRING, 'ContactPhoneField');
	}
	
	private function setLabel($name, $label){
		$this->labels[$name] = $label;
	}
	
	public function getLabel($name){
		return $this->labels[$name];
	}
	
	/*
	 * Implemented functions from dbClass
	 */
	public function setMap($name, $dataType, $columnName, $isPrimaryKey=false){
		$this->map[$name] = new DbMap($dataType, $columnName, $isPrimaryKey); 
	}
	
	public function getMap(){
		return $this->map;
	}
	
	public function getTable(){
		return $this->table;
	}
	
	public function save(){
		DbUtil::save($this);
	}
}