$(function(){
	
	$("#radio").buttonset();
	
	if($("#radio2").is(':checked')){
		$("#stage").hide();
		$("#disabled_msg").fadeIn('slow');
	}
	
	$("#radio2").click(function(){
		
		var data = $("#toggle_reminder_form").serialize();
		data += "&activate=0";
		
		$.ajax({
			type: "POST",
			url: "ajax/ajax.toggleReminder.php",
			data: data,
			success: function(msg){}
		});
		
		$("#stage").slideUp();
		$("#disabled_msg").fadeIn('slow');
	});
	
	
	$("#radio1").click(function(){
		
		var data = $("#toggle_reminder_form").serialize();
		data += "&activate=1";
		
		$.ajax({
			type: "POST",
			url: "ajax/ajax.toggleReminder.php",
			data: data,
			success: function(msg){}
		});
		
		$("#stage").slideDown();
		$("#disabled_msg").hide();
	});
	
	
	$.validator.addMethod("valueNotEquals", function(value, element, arg){
		return arg != value;
	}, "!");


	

	$("#save_reminder_btn").click(function(event){
		event.preventDefault();

		if($("#reminder_form").validate().form() == true){
			var data = $("#reminder_form").serialize();
			
			$(this).ajaxStart(function(){
				$("#save_reminder_btn").hide();
				$("#saving_reminder").show();
			});
			
			$.ajax({
				type: "POST",
				url: "ajax/ajax.saveReminder.php",
				data: data,
				success: function(msg){
					console.log(msg);
				}
			});
			$(this).ajaxStop(function(){
				$("#save_reminder_btn").show();
				$("#saving_reminder").hide();
			});
		}
		
	});

	$.extend($.validator.messages, {
		required: " (required) "
	});
	
	$( "input:submit, a, button", "#save_reminder" ).button();
	
});
