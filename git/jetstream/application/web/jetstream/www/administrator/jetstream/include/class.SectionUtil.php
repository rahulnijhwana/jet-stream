<?php
require_once 'class.DbConnManager.php';
require_once 'class.SqlBuilder.php';

class SectionUtil {
	
	private $companyId;
	
	private $defaultAccountSectionId;
	private $defaultContactSectionId;

	
	public function __construct($companyId){
		
		$this->companyId = $companyId;
		
		$this->setDefaultSection('Account');
		$this->setDefaultSection('Contact');
	}
	
	public function setDefaultSection($type){
		
		$table = $type . "Map";

		$sql = "SELECT DISTINCT(Section.SectionID) FROM " . $table . " JOIN Section ON " . $table . ".SectionID = Section.SectionID
				WHERE " . $type . "SectionName = 'DefaultSection' AND Section.CompanyID = ?";
		
		$params = array(DTYPE_INT, $this->companyId);
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql($params);
		$results = DbConnManager::GetDb('mpower')->Exec($sql);
		
		if($type == 'Account'){
			$this->setDefaultAccountSectionId($results[0]['SectionID']);
		}
		if($type == 'Contact'){
			$this->setDefaultContactSectionId($results[0]['SectionID']);
		}
	}
	
	private function setDefaultAccountSectionId($id){
		$this->defaultAccountSectionId = $id;
	}

	private function setDefaultContactSectionId($id){
		$this->defaultContactSectionId = $id;
	}
	
	public function getDefaultAccountSectionId(){
		return $this->defaultAccountSectionId;
	}

	public function getDefaultContactSectionId(){
		return $this->defaultContactSectionId;
	}
	
	public function getDefaultSectionId($type){
		switch($type)
		{
			case LABEL_TYPE_ACCOUNT:
				return $this->getDefaultAccountSectionId();
			break;
			case LABEL_TYPE_CONTACT:
				return $this->getDefaultContactSectionId();
			break;
		}
	}

}