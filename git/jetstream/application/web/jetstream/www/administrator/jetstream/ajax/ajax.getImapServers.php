<?php
include '../lib/class.ImapServer.php';
include '../include/class.ImapUtil.php';
include '../include/class.Display.php';

$companyId = strip_tags($_GET['companyId']);

$private_servers = ImapUtil::getImapServers($companyId, false);
$public_servers =  ImapUtil::getImapServers($companyId, true);

$html = Display::buildSelect($public_servers, $private_servers);
echo $html;