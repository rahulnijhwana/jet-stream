<?php
require_once 'class.FormatUtil.php';
require_once 'class.ImapUtil.php';
require_once 'class.PersonUtil.php';
require_once 'class.Reminder.php';
require_once 'class.LabelUtil.php';
require_once 'class.SectionUtil.php';

/**
 * Display content to the screen
 * All html belongs here
 * @author eric
 */
class Display {
	
	public $companyId;
	
	public function __construct($companyId){
		$this->companyId = $companyId;
	}
	
	public function showActiveImapAccounts(){
		?>
		<div id="imap_accounts" class="content">
			<div class="content_header">
				<div class="content_header_left">
					<h3>Active IMAP Accounts</h3>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php
			$users = ImapUtil::getImapUsers($this->companyId);
			
			if(count($users) > 0){

				?>
				<table id="users" class="data">
					<tr class="header">
						<td>Name</td>
						<td>Label</td>
						<td>Email Address</td>
						<td>Last Sync</td>
					</tr>
					<?php
					
					foreach($users as $user){
						?>
						<tr>
							<td><a class="edit_imap_account_link" label="<?php echo $user->getId() . '|' . $user->getPersonId() . '|' . $user->getServerId() . '|' . $user->isEnabled(); ?>" href="#"><?php echo PersonUtil::getFullName($user->getPersonId()); ?></a></td>
							<td><?php echo ImapUtil::getAccountLabel($user->getId()); ?></td>
							<td><span><?php echo $user->getUsername(); ?></span></td>
							<td><?php echo $user->getLastSync(); ?></td>
						</tr>
					<?php
					
					}
					
					?>
				</table>
			<?php
			}
			else {
				echo 'No IMAP accounts are defined. Click a user below to setup an account.';
				echo '<hr>';
			}
			?>
		</div>
	<?php

	}
	
	public function showActiveJetstreamUsers(){
		?>
		<div class="content">
			<div class="content_header">
				<div class="content_header_left">
					<h3>Active Jetstream Users</h3>
				</div>
				<div class="clearfix"></div>
			</div>
			<table id="users" class="data">
				<tr class="header">
					<td class="thin">Level</td>
					<td>Name</td>
					<td>User ID</td>
					<td>Color</td>
				</tr>
				<?php
				$people = UserUtil::getUsers($this->companyId);
				foreach($people as $person){
					?>
					<tr>
						<td class="thin">
							<?php echo $person['Level']; ?>
						</td>
						<td class="link">
							<a class="imap_account_link" label="<?php echo $person['PersonID']; ?>" href="#"><?php echo $person['FirstName'] . ' ' . $person['LastName']; ?></a>
						</td>
						<td>
							<?php echo $person['UserID']; ?>
						</td>
						<td style="background-color: <?php echo (is_numeric($person['Color']) ? '#' : '') . $person['Color']; ?>">
						</td>
					</tr>
				<?php 
				}
				?>
			</table>
		</div>
	<?php		
	}
	
	public function showAddImapServer($private){
		$label = ($private) ? 'private' : 'public';
		?>
		<div id="show_<?php echo $label; ?>" class="content">
			<div class="content_header">
				<div class="content_header_left">
					<h3 id="add_<?php echo $label; ?>_label"><?php echo ucfirst($label); ?> IMAP Servers</h3>
				</div>
				<div class="content_header_right">
					<a class="add_server" label="<?php echo $label; ?>" href="#">(add)</a>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php
			$servers = ImapUtil::getImapServers($this->companyId, $private);
			if(count($servers) > 0){
				?>
				<div>
					<table class="data">
						<tr class="header">
							<td>Label</td>
							<td>Address</td>
							<td class="thin">Port</td>
							<td class="thin">SSL</td>
						</tr>
						<?php
						foreach($servers as $server){
							?>
							<tr>
								<td class="link">
									<a class="edit_server" label="<?php echo $server->getId(); ?>" alt="<?php echo $server->isPrivate(); ?>" href="#"><?php echo $server->getLabel(); ?></a>
								</td>
								<td>
									<?php echo $server->getAddress(); ?>
								</td>
								<td class="thin">
									<?php echo $server->getPort(); ?>
								</td>
								<td class="thin">
									<?php echo FormatUtil::prettyPrint($server->isSsl()); ?>
								</td>
							</tr>
						<?php 
						}
						?>
					</table>
				</div>
			<?php
			}
			else {
				echo '<hr>';
			}
			?>
		</div>
	<?php
	}
	public static function buildSelect($public_servers, $private_servers){
		
		$html = '<select id="server_select" class="required"><option value="" selected="selected">Select an IMAP Server</option>';
		if(count($private_servers) > 0){
			$html .= '<optgroup label="Private Servers">';
			foreach($private_servers as $server){
				$html .= '<option value="' . $server->getId() . '">' . $server->getLabel() . '</option>';
			}
			$html .= '</optgroup>';
		}
		if(count($public_servers) > 0){
			$html .= '<optgroup label="Public Servers">';
			foreach($public_servers as $server){
				$html .= '<option value="' . $server->getId() . '">' . $server->getLabel() . '</option>';
			}
			$html .= '</optgroup>';
		}
		$html .= '</select>';
		
		return $html;
	}
	
	/*
	 * Appointment Reminder
	 */
	public function showReminderSettings($sn){
		$reminder = new Reminder($this->companyId);
		$sectionUtil = new SectionUtil($this->companyId);
		?>
		<div class="content">
			<div class="content_header">
				<div class="content_header_left">
					<h3>Appointment Reminder Mapping</h3>
				</div>
				<div class="clearfix"></div>
			</div>
			<form name="reminder_form" id="reminder_form">
			<table id="users" class="data">
				<tr class="header">
					<td class="forty">Field</td>
					<td class="sixty">Maps to</td>	
				</tr>
				<tr>
					<td class="darker">Account Address 1</td>
					<td>
						<?php
						$label = new LabelUtil(LABEL_TYPE_ACCOUNT, $reminder->getAccountAddress1Field(), 'AccountAddress1Field', $sectionUtil, $this->companyId);
						$label->show();
						?>
					</td>
				</tr>
				<tr>
					<td class="darker">Account Address 2</td>
					<td>
						<?php
						$label = new LabelUtil(LABEL_TYPE_ACCOUNT, $reminder->getAccountAddress2Field(), 'AccountAddress2Field', $sectionUtil, $this->companyId);
						$label->show();					
						?>
					</td>
				</tr>
				<tr>
					<td class="darker">Account City</td>
					<td>
						<?php
						$label = new LabelUtil(LABEL_TYPE_ACCOUNT, $reminder->getAccountCityField(), 'AccountCityField', $sectionUtil, $this->companyId);
						$label->show();
						?>
					</td>
				</tr>
				<tr>
					<td class="darker">Account State</td>
					<td>
						<?php
						$label = new LabelUtil(LABEL_TYPE_ACCOUNT, $reminder->getAccountStateField(), 'AccountStateField', $sectionUtil, $this->companyId);
						$label->show();
						?>	
					</td>
				</tr>
				<tr>
					<td class="darker">Account Zip</td>
					<td>
						<?php
						$label = new LabelUtil(LABEL_TYPE_ACCOUNT, $reminder->getAccountZipField(), 'AccountZipField', $sectionUtil, $this->companyId);
						$label->show();
						?>
					</td>
				</tr>
				<tr>
					<td class="darker">Account Phone</td>
					<td>
						<?php
						$label = new LabelUtil(LABEL_TYPE_ACCOUNT, $reminder->getAccountPhoneField(), 'AccountPhoneField', $sectionUtil, $this->companyId);
						$label->show();
						?>	
					</td>
				</tr>
				<tr>
					<td class="darker">Account Fax</td>
					<td>
						<?php
						$label = new LabelUtil(LABEL_TYPE_ACCOUNT, $reminder->getAccountFaxField(), 'AccountFaxField', $sectionUtil, $this->companyId);
						$label->show();
						?>					
					</td>
				</tr>
				<tr>
					<td class="darker">Account Website</td>
					<td>
						<?php
						$label = new LabelUtil(LABEL_TYPE_ACCOUNT, $reminder->getAccountUrlField(), 'AccountUrlField', $sectionUtil, $this->companyId);
						$label->show();
						?>
					</td>
				</tr>
				<tr>
					<td class="light">Contact Address 1</td>
					<td>
						<?php
						$label = new LabelUtil(LABEL_TYPE_CONTACT, $reminder->getContactAddress1Field(), 'ContactAddress1Field', $sectionUtil, $this->companyId);
						$label->show();
						?>
					</td>
				</tr>
				<tr>
					<td class="light">Contact Address 2</td>
					<td>
						<?php
						$label = new LabelUtil(LABEL_TYPE_CONTACT, $reminder->getContactAddress2Field(), 'ContactAddress2Field', $sectionUtil, $this->companyId);
						$label->show();
						?>
					</td>
				</tr>
				<tr>
					<td class="light">Contact City</td>
					<td>
						<?php
						$label = new LabelUtil(LABEL_TYPE_CONTACT, $reminder->getContactCityField(), 'ContactCityField', $sectionUtil, $this->companyId);
						$label->show();
						?>
					</td>
				</tr>
				<tr>
					<td class="light">Contact State</td>
					<td>
						<?php
						$label = new LabelUtil(LABEL_TYPE_CONTACT, $reminder->getContactStateField(), 'ContactStateField', $sectionUtil, $this->companyId);
						$label->show();
						?>
					</td>
				</tr>
				<tr>
					<td class="light">Contact Zip</td>
					<td>
						<?php
						$label = new LabelUtil(LABEL_TYPE_CONTACT, $reminder->getContactZipField(), 'ContactZipField', $sectionUtil, $this->companyId);
						$label->show();
						?>
					</td>
				</tr>
				<tr>
					<td class="light">Contact Phone</td>
					<td>
						<?php
						$label = new LabelUtil(LABEL_TYPE_CONTACT, $reminder->getContactPhoneField(), 'ContactPhoneField', $sectionUtil, $this->companyId);
						$label->show();
						?>
					</td>
				</tr>
			</table>
			<input type="hidden" name="CompanyID" value="<?php echo $this->companyId; ?>" />
			<input type="hidden" name="ReminderID" value="<?php echo $reminder->getId(); ?>" />
			<input type="hidden" name="SN" value="<?php echo $sn; ?>" />
			<p id="save_reminder" class="center">
				<button id="save_reminder_btn">Save</button>
			</p>
			<div id="saving_reminder">
				<img src="images/spinner.gif" alt="saving" />
			</div>
		</form>
		</div>
	<?php
	}
}
?>