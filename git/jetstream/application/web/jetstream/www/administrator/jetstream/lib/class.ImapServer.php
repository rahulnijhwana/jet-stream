<?php
require_once 'class.DbConnManager.php';
require_once 'class.SqlBuilder.php';
require_once 'class.DbUtil.php';
require_once 'class.DbMap.php';

class ImapServer {
	
	private $table = 'ImapServer';
	private $id;
	private $address;
	private $port;
	private $ssl;
	private $private;
	private $label;
	private $companyId;
	private $map = array();
	
	public function __construct($input=null){
		if(is_array($input)){
			$this->setId($input['id']);
			$this->setAddress($input['address']);
			$this->setPort($input['port']);
			$this->setSsl($input['ssl']);
			$this->setPrivate($input['private']);
			$this->setLabel($input['label']);
			$this->setCompanyId($input['companyId']);
		}
	}
	
	public function setMap($name, $dataType, $columnName, $isPrimaryKey=false){
		$this->map[$name] = new DbMap($dataType, $columnName, $isPrimaryKey); 
	}
	
	public function getMap(){
		return $this->map;
	}
	
	public function save(){
		DbUtil::save($this);
	}
	
	public function getTable(){
		return $this->table;
	}
	public function getId(){
		return $this->id;
	} 
	public function getAddress(){
		return $this->address;
	} 
	public function getPort(){
		return $this->port;
	}
	public function isSsl() {
		return $this->ssl ? 1 : 0;
	}
	public function isPrivate(){
		return $this->private ? 1 : 0;
	}
	public function getLabel(){
		return $this->label;
	}
	public function getCompanyId(){
		return $this->companyId;
	}
	
	public function setId($id){
		$this->id = $id;
		$this->setMap('id', DTYPE_INT, 'ImapServerID', true);
	}
	public function setAddress($address){
		$this->address = $address;
		$this->setMap('address', DTYPE_STRING, 'Address');
	}
	public function setPort($port){
		$this->port = $port;
		$this->setMap('port', DTYPE_INT, 'Port');
	}
	public function setSsl($ssl){
		$this->ssl = $ssl;
		$this->setMap('ssl', DTYPE_BOOL, 'SSL');
	}
	public function setPrivate($private){
		$this->private = $private;
		$this->setMap('private', DTYPE_BOOL, 'Private');
	}
	public function setLabel($label){
		$this->label = $label;
		$this->setMap('label', DTYPE_STRING, 'Label');
	}
	public function setCompanyId($companyId){
		$this->companyId = $companyId;
		$this->setMap('companyId', DTYPE_INT, 'CompanyID');
	}
	
	
}