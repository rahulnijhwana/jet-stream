<?php
session_name($_POST['SN']);
session_start();

include '../include/class.ReminderUtil.php';

$activate = strip_tags($_POST['activate']);
$activate = $activate ? true : false;
$companyId = strip_tags($_POST['reminder_company']);

ReminderUtil::toggleReminder($companyId, $activate);