<?php
require_once 'class.DbConnManager.php';
require_once 'class.SqlBuilder.php';

class ImapCompanySettings {
	
	public static function isEnabled($companyId){
		$sql = "SELECT Enabled FROM ImapCompanySettings WHERE CompanyID = ?";
		$params = (array(DTYPE_INT, $companyId));
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql($params);
		$results = DbConnManager::GetDb('mpower')->GetOne($sql);
		
		return (count($results) == 0 || !$results['Enabled']) ? false : true;
	}
	
}