<?php
require_once 'top.php';
require_once 'left.php';

$companyEnabled = ImapCompanySettings::isEnabled($companyId);
$display = new Display($companyId);
?>
			<div id="main">
				<div id="radio_wrapper">
					<div id="disabled_msg">IMAP is disabled for this company. Click the "ON" button to enable it.</div>
					<form name="toggle_imap_form" id="toggle_imap_form">
						<div id="radio">
						
							<input type="radio" id="radio2" name="radio" <?php echo (!$companyEnabled) ? 'checked="checked"' : ''; ?> /><label for="radio2">OFF</label>
							<input type="radio" id="radio1" name="radio" <?php echo ($companyEnabled) ? 'checked="checked"' : ''; ?> /><label for="radio1">ON</label>
						</div>
						<input type="hidden" name="imap_company" id="imap_company" value="<?php echo $companyId; ?>" />
					</form>
					<div class="clearfix"></div>
				</div>
				<div id="stage">
					<?php
					$server = new ImapServer();
					$display->showActiveImapAccounts();
					$display->showActiveJetstreamUsers();
					$display->showAddImapServer(false);
					$display->showAddImapServer(true);
					?>						
				</div>
			</div>
			<div id="add_server">
				<form name="add_server_form" id="add_server_form">
					<div class="form_header">
						<div class="form_header_right">
							<a href="#" class="close_form"><img class="close" src="images/img_close.png" alt="Cancel" /> </a>
						</div>
						<div class="form_header_left">
							<h3>Add <?php echo $label; ?> IMAP Server</h3>
						</div>
						<div class="clearfix"></div>
					</div>
					<table class="data">
						<tr>
							<td class="label">Label</td><td><input class="form" type="text" name="server_label" id="server_label" /></td>
						</tr>
						<tr>
							<td class="label">Address</td><td><input class="form" type="text" name="server_address" id="server_address" /></td>
						</tr>
						<tr>
							<td class="label">Port</td><td><input class="form" type="text" name="server_port" id="server_port" /></td>
						</tr>
						<tr>
							<td class="label">SSL</td>
							<td class="checkbox"><input type="checkbox" name="server_ssl" id="server_ssl" /></td>
						</tr>
						<tr>
							<td colspan="2" class="button">
								<div id="save_button_wrapper">
									<input type="submit" name="save_btn" id="save_btn" value="Add Server" />
								</div>
								<div id="saving">
									<img src="images/spinner.gif" alt="saving" />
								</div>
							</td>
						</tr>
					</table>
					<input type="hidden" name="company_id" id="company_id" value="<?php echo $companyId; ?>" />
					<input type="hidden" name="server_id" id="server_id" />
					<input type="hidden" name="is_private" id="is_private" />
				</form>
			</div>
			<!--  -->
			<?php 
			$private_servers = ImapUtil::getImapServers($companyId, true);
			$public_servers =  ImapUtil::getImapServers($companyId, false);
			$servers = Display::buildSelect($public_servers, $private_servers);
			?>
			<div id="imap_account">
				<form name="add_account_form" id="add_account_form">
					<div class="form_header">
						<div class="form_header_right">
							<a href="#" class="close_form"><img class="close" src="images/img_close.png" alt="Cancel" /> </a>
						</div>
						<div class="form_header_left">
							<h3 id="account_title"></h3>
							<h4 id="accountholder"></h4>
						</div>
						<div class="clearfix"></div>
					</div>
					<table class="data">
						<tr>
							<td class="label">Server</td>
							<td id="server_list"><?php echo $servers; ?></td>
						</tr>
						<tr>
							<td class="label">Label</td><td><input type="text" class="form required" name="account_label" id="account_label" /></td>
						</tr>
						<tr>
							<td class="label">Email Address</td><td><input type="text" class="form required" name="account_email_address" id="account_email_address" /></td>
						</tr>
						<tr>
							<td class="label">Password</td><td><input type="password" class="form required" name="account_password" id="account_password" /></td>
						</tr>
						<tr>
							<td class="label">Enabled</td>
							<td class="checkbox"><input type="checkbox" name="is_enabled" id="is_enabled" /></td>
						</tr>
						<tr>
							<td colspan="2" class="button">
								<div id="save_account_button_wrapper">
									<input type="submit" name="save_account_btn" id="save_account_btn" value="Save Account Settings" />
								</div>
								<div id="saving_account">
									<img src="images/spinner.gif" alt="saving" />
								</div>
							</td>
						</tr>
					</table>
					<input type="hidden" name="company_id" id="company_id" value="<?php echo $companyId; ?>" />
					<input type="hidden" name="person_id" id="person_id" />
					<input type="hidden" name="imap_account_id" id="imap_account_id" />
					<input type="hidden" name="SN" value="<?php echo $sn; ?>" />
				</form>
			</div>
		</div>
	</body>
</html>
