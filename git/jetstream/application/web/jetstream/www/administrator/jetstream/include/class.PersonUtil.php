<?php
require_once 'class.DbConnManager.php';
require_once 'class.SqlBuilder.php';

class PersonUtil {
	
	public static function getFullName($personId){
		$sql = "
			SELECT
				FirstName,
				LastName
			FROM
				People
			WHERE
				PersonID = ?";
		
		$params = (array(DTYPE_INT, $personId));
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql($params);
		$results = DbConnManager::GetDb('mpower')->GetOne($sql);
		
		return $results['FirstName'] . ' ' . $results['LastName'];
	}
}