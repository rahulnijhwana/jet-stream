<?php
interface dbClass {
	
	public function setMap($name, $dataType, $columnName, $isPrimaryKey=false);
	public function getMap();
	public function getTable();
	public function save();
	
}