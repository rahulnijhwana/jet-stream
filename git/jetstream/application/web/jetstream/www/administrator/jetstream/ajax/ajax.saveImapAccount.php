<?php
session_name($_POST['SN']);
session_start();

include 'class.ImapUserSettings.php';
include 'class.ImapUtil.php';
include 'class.Display.php';

$args = array(
	'companyId' => strip_tags($_POST['company_id']),
	'serverId' => strip_tags($_POST['server_id']),
	'personId' => strip_tags($_POST['person_id']),
	'label' => strip_tags($_POST['account_label']),
	'lastSync' => null,
	'username' => strip_tags($_POST['account_email_address']),
	'password' => strip_tags($_POST['account_password']),
	'enabled' => (strip_tags($_POST['is_enabled']) ==  'on' ? 1 : 0)
);
// edit will have an id
$id = strip_tags($_POST['imap_account_id']);
if($id){
	$args['id'] = $id;
}
$imapUserSettings = new ImapUserSettings($args);
$imapUserSettings->save();

$display = new Display($args['companyId']);
$display->showActiveImapAccounts();