<?php
class DbMap {
	
	private $dataType;
	private $columnName;
	private $isIdentity;
	
	public function __construct($dataType, $columnName, $isPrimaryKey=false){
		$this->setDataType($dataType);
		$this->setColumnName($columnName);
		$this->isIdentity = $isPrimaryKey;
	}
	
	public function getDataType(){
		return $this->dataType;
	}
	public function getColumnName(){
		return $this->columnName;
	}
	
	public function isPrimaryKey(){
		return $this->isIdentity;
	}
	
	public function setDataType($dataType){
		$this->dataType = $dataType;
	}	
	public function setColumnName($columnName){
		$this->columnName = $columnName;
	}
}