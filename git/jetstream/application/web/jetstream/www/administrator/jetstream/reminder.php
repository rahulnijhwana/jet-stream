<?php
require_once 'top.php';
require_once 'left.php';
require_once 'class.ReminderUtil.php';

$companyId = $_SESSION['company_id'];
$reminderEnabled = ReminderUtil::isEnabled($companyId);
?>
<div id="main">
	<div id="radio_wrapper">
		<div id="disabled_msg">Reminders are disabled for this company. Click the "ON" button to enable them.</div>
		<form name="toggle_reminder_form" id="toggle_reminder_form">
			<div id="radio">
				<input type="radio" id="radio2" name="radio" <?php echo (!$reminderEnabled) ? 'checked="checked"' : ''; ?> /><label for="radio2">OFF</label>
				<input type="radio" id="radio1" name="radio" <?php echo ($reminderEnabled) ? 'checked="checked"' : ''; ?> /><label for="radio1">ON</label>
			</div>
			<input type="hidden" name="reminder_company" id="reminder_company" value="<?php echo $companyId; ?>" />
			<input type="hidden" name="SN" value="<?php echo $sn; ?>" />
		</form>
		<div class="clearfix"></div>
	</div>
	<div id="stage">
		<?php
		$display = new Display($companyId);
		$display->showReminderSettings($sn);
		?>						
	</div>
</div>