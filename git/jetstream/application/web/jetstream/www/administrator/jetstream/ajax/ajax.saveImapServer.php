<?php
session_name($_POST['SN']);
session_start();

include '../lib/class.ImapServer.php';
include '../include/class.ImapUtil.php';
include '../include/class.Display.php';

$companyId = strip_tags($_POST['company_id']);

$args = array(
	'id' => strip_tags($_POST['server_id']),
	'label' => strip_tags($_POST['server_label']),
	'address' => strip_tags($_POST['server_address']),
	'port' => strip_tags($_POST['server_port']),
	'private' => strip_tags($_POST['is_private']),
	'ssl' => (strip_tags($_POST['server_ssl']) ==  'on' ? 1 : 0)
);
if($args['private']){
	$args['companyId'] = $companyId;
}

$server = new ImapServer($args);
$server->save();

$servers = ImapUtil::getImapServers($companyId);
$display = new Display($companyId);
$display->showAddImapServer($args['private']);
