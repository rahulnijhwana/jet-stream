$(function(){
	
	$("#radio").buttonset();
	
	if($("#radio2").is(':checked')){
		$("#stage").hide();
		$("#disabled_msg").fadeIn('slow');
	}
	
	$("#radio2").click(function(){
		
		data = $("#toggle_imap_form").serialize();
		data += "&activate=0";
		
		$.ajax({
			type: "POST",
			url: "ajax/ajax.toggleImap.php",
			data: data,
			success: function(msg){}
		});
		
		$("#stage").slideUp();
		$("#disabled_msg").fadeIn('slow');
	});
	
	
	$("#radio1").click(function(){
		
		data = $("#toggle_imap_form").serialize();
		data += "&activate=1";
		
		$.ajax({
			type: "POST",
			url: "ajax/ajax.toggleImap.php",
			data: data,
			success: function(msg){}
		});
		
		$("#stage").slideDown();
		$("#disabled_msg").hide();
	});
	
	
	$('.link').live('mouseover', function(){
		
		$(this).qtip({
			overwrite: false,
			content: 'Click to setup an IMAP account',
			position: {
				my: 'left center', 
				at: 'right center'
			},
			style: { 
				tip: true,
				classes: 'ui-tooltip-rounded'
			},
			show: {
				ready: true
			}

		});
	});

	$('.edit_imap_account_link').live('mouseover', function(){
		
		$(this).qtip({
			overwrite: false,
			content: 'Click to edit this IMAP account',
			position: {
				my: 'left center', 
				at: 'right center'
			},
			style: { 
				tip: true,
				classes: 'ui-tooltip-rounded'
			},
			show: {
				ready: true
			}

		});
	});

	$('.content_header_right').qtip({
		content: 'Click to add an IMAP server',
		position: {
			my: 'left center', 
			at: 'right center'
		},
		style: { 
			tip: true,
			classes: 'ui-tooltip-rounded'
		}
	});
	

	$('.add_server').live('click', function() {
		
		var type = $(this).attr('label');
		var label = $("#add_" + type + "_label").text();
		
		if(type == 'private'){
			$("#is_private").val(1);
		}
		else {
			$("#is_private").val(0);
		}
		
		$("#server_ssl").attr('checked', false);
		$("#server_id").val('');
		$("#add_server h3").html(label);
		
        $.blockUI({
        	css: {
        		cursor: 'default',
        		top: '25%'
        	},
            message: $("#add_server")
        }); 
    });
    
    
	$('.edit_server').live('click', function() {

		$("#add_server h3").html('Edit IMAP Server');
		$("#save_btn").attr('value', 'Save Changes');
		
		var type = $(this).attr('alt');

		$("#is_private").val(type);
		
		var id = $(this).attr('label');
		var label = $(this).text();
		var address = $.trim( $(this).parent().next().text());
		var port = $.trim( $(this).parent().next().next().text());
		var ssl = $.trim( $(this).parent().next().next().next().text());
		
		$("#server_id").val(id);
		$("#server_label").val(label);
		$("#server_address").val(address);
		$("#server_port").val(port);
		if(ssl == 'True'){
			$("#server_ssl").attr('checked', true);
		}
		
        $.blockUI({
        	css: {
        		cursor: 'default',
        		top: '25%'
        	},
            message: $("#add_server")
        });
	});
    
	
    $("#save_btn").live('click', function(){
		$(this).ajaxStart(function(){
			$("#save_button_wrapper").hide();
			$("#saving").show();
		});
		
		data = $("#add_server_form").serialize();
		console.log(data);
		$.ajax({
			type: "POST",
			url: "ajax/ajax.saveImapServer.php",
			data: data,
			success: function(msg){
				if($("#is_private").val() == 1){
					$("#show_private").html(msg).fadeIn();
				}
				else {
					$("#show_public").html(msg).fadeIn();
				}
				reloadServerList($("#company_id").val());
			}
		});
		
		$(this).ajaxStop(function(){
			$("#save_button_wrapper").show();
			$("#saving").hide();
	        $.unblockUI();
	    	$(':input','#add_server_form')
	    	.not(':button, :submit, :reset, :hidden, :checkbox')
	    	.val('')
	    	.removeAttr('checked')
	    	.removeAttr('selected');
		});
		
		return false;
    });

    $("#add_account_form").validate({
    	errorPlacement: function(error, element) {
    		//go away messages
    	},
    	highlight: function(element, errorClass, validClass) {
    		$(element).addClass(errorClass).removeClass(validClass);
    		$(element.form).find("label[for=" + element.id + "]")
    		.addClass(errorClass);
    	},
    	unhighlight: function(element, errorClass, validClass) {
    		$(element).removeClass(errorClass).addClass(validClass);
    		$(element.form).find("label[for=" + element.id + "]")
    		.removeClass(errorClass);
    	}

    });

    $("#save_account_btn").live('click', function(event){

    	if($("#add_account_form").validate().form() == true){
    	
			data = $("#add_account_form").serialize();
			data += '&server_id=' + $("#server_select option:selected").val();
			
			if($("#imap_account_id").val()){
				data += '&imap_account_id=' + $("#imap_account_id").val();
			}
			console.log(data);
			
			$.ajax({
				type: "POST",
				url: "ajax/ajax.saveImapAccount.php",
				data: data,
				success: function(msg){
					$("#imap_accounts").html(msg).fadeIn();
				}
			});
			
			$(this).ajaxStop(function(){
				$("#save_account_button_wrapper").show();
				$("#saving_account").hide();
		        $.unblockUI();
		    	$(':input','#add_account_form')
		    	.not(':button, :submit, :reset, :hidden')
		    	.val('')
		    	.removeAttr('checked')
		    	.removeAttr('selected');
			});
			event.preventDefault();
	    	return false;
    	}
    });
    
    
	$(".close_form").live('click', function(){
		
    	$.unblockUI();
    	
    	$("#save_btn").attr('value', 'Add Server');
    	$("#server_id").attr('value', '');
    	
    	$(':input','#add_server_form')
    	.not(':button, :submit, :reset, :hidden, :checkbox')
    	.val('')
    	.removeAttr('checked')
    	.removeAttr('selected');
    	$("#is_private").val(0);

    	$(':input','#add_account_form')
    	.not(':button, :submit, :reset, :hidden')
    	.val('')
    	.removeAttr('checked');
    	
    	return false;
    });

	$(".imap_account_link").live('click', function(){
		
		$("#imap_account_id").val('');
		
		$("#account_title").text('Add IMAP Account');
		
		var validator = $("#add_account_form").validate();
		validator.resetForm();
		
		var accountholder = $(this).text();
		$("#person_id").val($(this).attr('label'));
		$("#accountholder").text('User: ' + accountholder);

		$.blockUI({
        	css: {
        		cursor: 'default',
        		top: '25%'
        	},
            message: $("#imap_account")
        });
		
	});

	$(".edit_imap_account_link").live('click', function(){
		
		var label = $(this).attr('label');
		var parts = label.split('|');

		var id = parts[0];
		var person_id = parts[1];
		var server_id = parts[2];
		var enabled = parts[3];
		
		$("#account_title").text('Edit IMAP Account');
		var validator = $("#add_account_form").validate();
		validator.resetForm();

		var name = $.trim( $(this).parent().next().text());
		$("#account_label").val(name);

		var email = $.trim( $(this).parent().next().next().text());
		$("#account_email_address").val(email);
		
		$("#person_id").val(person_id);
		$("#is_enabled").attr('checked', enabled);
		
		$('#server_select option').eq(server_id).attr('selected', 'selected');
		
		var accountholder = $(this).text();
		$("#imap_account_id").val(id);
		$("#accountholder").text('User: ' + accountholder);

		$.blockUI({
        	css: {
        		cursor: 'default',
        		top: '25%'
        	},
            message: $("#imap_account")
        });
	});
	
	
	$.fn.clearForm = function() {
		return this.each(function() {
			
			var type = this.type, tag = this.tagName.toLowerCase();
			if (tag == 'form'){
				return $(':input',this).clearForm();
			}
			if (type == 'text' || type == 'password' || tag == 'textarea'){
				this.value = '';
			}
			else if (type == 'checkbox' || type == 'radio'){
				this.checked = false;
			}
			else if (tag == 'select'){
				this.selectedIndex = -1;
			}
		});
	};
	
});

function reloadServerList(companyId){
	$.ajax({
		type: "GET",
		url: "ajax/ajax.getImapServers.php?companyId=" + companyId,
		success: function(msg){
			console.log(msg);
			$("#server_select").replaceWith(msg);
		}
	});
}


