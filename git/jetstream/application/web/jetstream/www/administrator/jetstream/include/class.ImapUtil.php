<?php
require_once 'class.DbConnManager.php';
require_once 'class.ImapServer.php';
require_once 'class.ImapUserSettings.php';

class ImapUtil {
	
	/**
	 * Toggle IMAP on/off for a company
	 * @param int $companyId
	 * @param bool $activate
	 */
	public static function toggleImap($companyId, $activate){
		$sql = "SELECT COUNT(*) AS count FROM ImapCompanySettings WHERE CompanyID = ?";
		$param = array(DTYPE_INT, $companyId);
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql($param);
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		$count = $result[0]['count'];
		
		if($count > 0){
			//update
			$update = "UPDATE ImapCompanySettings SET Enabled = ? WHERE CompanyID = ?";
			$params[] = array(DTYPE_BOOL, $activate);
			$params[] = $param;
			$update = SqlBuilder()->LoadSql($update)->BuildSqlParam($params);
			DbConnManager::GetDb('mpower')->Exec($update);
		}
		else {
			//insert
			$insert = "INSERT INTO ImapCompanySettings (CompanyID, Enabled) VALUES (?, 1)";
			$insert = SqlBuilder()->LoadSql($insert)->BuildSql($param);

			DbConnManager::GetDb('mpower')->Exec($insert);
		}
	}
	
	public static function getImapServers($companyId, $private=false){
		
		$sql = "
			SELECT
				ImapServerID AS id,
				Address AS address,
				Port as port,
				SSL as ssl,
				Private as private,
				Label as label
			FROM
				ImapServer
			WHERE
				(CompanyID";
		
		$sql .= ($private) ? (' = ' . $companyId . ' AND Private = 1)') : ' IS NULL AND Private = 0)';
				
		$results = DbConnManager::GetDb('mpower')->Exec($sql);
		$servers = array();
		foreach($results as $result){
			$servers[] = new ImapServer($result);
		}
		return $servers;
	}
	
	public static function getImapUsers($companyId){
		$sql = "
				SELECT
					ImapUserSettingsID AS id,
					ImapServerID AS serverId,
					PersonID AS personId,
					Enabled AS enabled,
					ImapUsername AS username,
					ImapPassword AS password,
					LastSync AS lastSync,
					Label AS label
				FROM
					ImapUserSettings
				WHERE
					CompanyID = ?";
		
		$params = array(DTYPE_INT, $companyId);
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql($params);
		$results = DbConnManager::GetDb('mpower')->Exec($sql);
		
		$users = array();
		foreach($results as $result){
			$users[] = new ImapUserSettings($result);
		}
		return $users;
		
	}
	
	public static function getServerLabel($serverId){
		$sql = "SELECT Label FROM ImapServer WHERE ImapServerID = ?";
		$params = (array(DTYPE_INT, $serverId));
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql($params);
		$results = DbConnManager::GetDb('mpower')->GetOne($sql);

		return $results->Label;
	}
	
	public static function getAccountLabel($id){
		$sql = "SELECT Label FROM ImapUserSettings WHERE ImapUserSettingsID = ?";
		$params = (array(DTYPE_INT, $id));
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql($params);
		$results = DbConnManager::GetDb('mpower')->GetOne($sql);

		return $results->Label;
	}
	
}