<?php
require_once 'class.DbConnManager.php';
require_once 'class.SqlBuilder.php';

class ReminderUtil {
	
	public static function isEnabled($companyId){
		$sql = "SELECT Reminder FROM Company WHERE CompanyID = ?";
		$params = (array(DTYPE_INT, $companyId));
		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql($params);
		$results = DbConnManager::GetDb('mpower')->GetOne($sql);
		
		return (count($results) == 0 || !$results['Reminder']) ? false : true;
		
	}
	
	/**
	 * Toggle Reminders on/off for a company
	 * @param int $companyId
	 * @param bool $activate
	 */
	public static function toggleReminder($companyId, $activate){

		$update = "UPDATE Company SET Reminder = ? WHERE CompanyID = ?";
		
		$params[] = array(DTYPE_BOOL, $activate);
		$params[] = array(DTYPE_INT, $companyId);
		
		$update = SqlBuilder()->LoadSql($update)->BuildSqlParam($params);
		DbConnManager::GetDb('mpower')->Exec($update);

	}
	
}