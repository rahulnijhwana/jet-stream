<?php
require_once 'class.DbConnManager.php';
require_once 'class.SqlBuilder.php';
require_once 'class.DbUtil.php';
require_once 'class.DbMap.php';

class ImapUserSettings {
	
	private $table = 'ImapUserSettings';
	private $id;
	private $serverId;
	private $enabled;
	private $personId;
	private $username;
	private $password;
	private $lastSync;
	private $companyId;
	private $label;
	private $map = array();
	
	public function __construct($input){
		if(is_array($input)){
			$this->setId($input['id']);
			$this->setServerId($input['serverId']);
			$this->setEnabled($input['enabled']);
			$this->setPersonId($input['personId']);
			$this->setUsername($input['username']);
			$this->setPassword($input['password']);
			$this->setLastSync($input['lastSync'] ? $input['lastSync'] : null);
			$this->setCompanyId($input['companyId']);
			$this->setLabel($input['label']);
		}
	}
	
	public function setMap($name, $dataType, $columnName, $isPrimaryKey=false){
		$this->map[$name] = new DbMap($dataType, $columnName, $isPrimaryKey); 
	}
	
	public function getMap(){
		return $this->map;
	}
	
	public function getTable(){
		return $this->table;
	}
	
	public function save(){
		
		DbUtil::save($this);
	}
	
	public function getId(){
		return $this->id;
	} 
	public function getServerId(){
		return $this->serverId;
	} 
	public function isEnabled(){
		return $this->enabled;
	} 
	public function getPersonId(){
		return $this->personId;
	} 
	public function getUsername(){
		return $this->username;
	} 
	public function getPassword(){
		return $this->password;
	} 
	public function getLastSync(){
		return $this->lastSync;
	} 
	public function getCompanyId(){
		return $this->companyId;
	} 
	public function getLabel(){
		return $this->label;
	} 

	public function setId($id){
		$this->id = $id;
		$this->setMap('id', DTYPE_INT, 'ImapUserSettingsID', true);
	} 
	public function setServerId($serverId){
		$this->serverId = $serverId;
		$this->setMap('serverId', DTYPE_INT, 'ImapServerID');
	} 
	public function setEnabled($enabled){
		$this->enabled = $enabled;
		$this->setMap('enabled', DTYPE_BOOL, 'Enabled');
	} 
	public function setPersonId($personId){
		$this->personId = $personId;
		$this->setMap('personId', DTYPE_INT, 'PersonID');
	} 
	public function setUsername($username){
		$this->username = $username;
		$this->setMap('username', DTYPE_STRING, 'ImapUsername');
	} 
	public function setPassword($password){
		$this->password = $password;
		$this->setMap('password', DTYPE_STRING, 'ImapPassword');
	} 
	public function setLastSync($lastSync){
		$this->lastSync = $lastSync;
		$this->setMap('lastSync', DTYPE_STRING, 'LastSync');
	} 
	public function setCompanyId($companyId){
		$this->companyId = $companyId;
		$this->setMap('companyId', DTYPE_INT, 'CompanyID');
	} 
	public function setLabel($label){
		$this->label = $label;
		$this->setMap('label', DTYPE_STRING, 'Label');
	}
}