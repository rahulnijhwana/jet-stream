<?php
 /**
 * @package Reports
 */
include('_paul_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');

if (!isset($treeid)) {
	begin_report_orig('Login Report');
	print_tree_prompt(ANYBODY);
	end_report_orig();
	exit();
}

if (!isset($dateFrom) || !isset($dateTo)) {
	begin_report_orig('Login Report');
	?>
	<center style="font-family:Arial; font-size:12pt;">Please select the date span to include:</center>
	<?
	$sortcol=0;
	$ord=0;
	$tomorrow = date("m/d/Y", strtotime("+1 day"));
	print_date_params($treeid,$sortcol,$ord, "datefr:<$tomorrow", "dateto:<$tomorrow");
	if(!isset($dateFrom)) $dateFrom="N/A";
	if(!isset($dateTo)) $dateTo="N/A";
	end_report_orig();
	exit();
}

begin_report('Login Report');

$id_list = implode(",", GetListByTree($treeid));

//$name_list = array();
//while ($row = mssql_fetch_assoc($person_result)) {
//    $name_list[$row['personid']]['name'] = $row['firstname'] . ' ' . $row['lastname'];
//    $name_list[$row['personid']]['level'] = $row['level'];
//    $name_list[$row['personid']]['supervisor'] = $row['supervisorid'];
//}

$real_date_from = date('m/d/y', strtotime($dateFrom));
$real_date_to = date('m/d/y', strtotime($dateTo) + 86400);


$login_sql = "select distinct log.personid,
    convert(char(10), whenchanged, 101) as logdate
    from log
    where log.personid in ($id_list) 
    and whenchanged between '$real_date_from' and '$real_date_to'
    and transactionid = 300
    order by personid, logdate";


$login_result = mssql_query($login_sql);

$weekday_counts = array();
$weekend_counts = array();
$total_counts = array();

// while ($row = mssql_fetch_assoc($person_result)) {
//
// }


while ($row = mssql_fetch_assoc($login_result))
{   
    if (in_array($row['personid'], array_keys($total_counts))) {
        $total_counts[$row['personid']]++;
    }
    else {
        $total_counts[$row['personid']] = 1;
    }

    $day_of_week = GetWeekdayNum(strtotime($row['logdate']));

    if ($day_of_week < 6) {
        if (in_array($row['personid'], array_keys($weekday_counts))) {
            $weekday_counts[$row['personid']]++;
        }
        else {
            $weekday_counts[$row['personid']] = 1;
        }
    }
    else {
        if(in_array($row['personid'], array_keys($weekend_counts))) {
            $weekend_counts[$row['personid']]++;
        }
        else {
            $weekend_counts[$row['personid']] = 1;
        }
    }
}

/*
echo "<pre>";
print_r($total_counts);
print_r($weekday_counts);
print_r($weekend_counts);
echo "</pre>";
*/



$shortdateFrom = date('m/d/y', strtotime($dateFrom));
$shortdateTo = date('m/d/y', strtotime($dateTo));
$sqlstr = "select FirstName, LastName, Level from people where PersonID = $treeid";
$result = mssql_query($sqlstr);
$reportPerson = mssql_fetch_assoc($result);

$total_weekdays = WeekdayCount(strtotime($real_date_from), strtotime($real_date_to));
$total_days = DaysDiff($real_date_from, $real_date_to);
?>

		<link rel="stylesheet" type="text/css" href="../css/report.css">
		<script src="../javascript/sorttable.js,utils.js"></script>

		</td>
	</tr>
</table>
<br>
<center style="font-size:12pt; font-weight:bold;">
    <?=$reportPerson['FirstName']?> <?=$reportPerson['LastName']?><br>
    <?=$shortdateFrom?> - <?=$shortdateTo?> (<?php echo $total_days ?> days / <?php echo $total_weekdays ?> weekdays)
</center>
<br>
		<center>
			<table class="report sortable" id="tableMain">
				<colgroup>
					<col span=3 />
					<col span=1 style="background-color: #DDDDFF"/>
					<col span=1 />
					<col span=1 style="background-color: #DDDDFF" />
				</colgroup>
				<tr>
					<th>Salesperson</th>
					<th>Lvl</th>
					<th>Total Days</th>
					<th>Total %</th>
					<th>Total Weekdays</th>
					<th>Weekday %</th>
				</tr>

				<?
                WriteRow($treeid, 0, $reportPerson['FirstName'], $reportPerson['LastName'], $reportPerson['Level']);
                
                function WriteRow($personId, $indent, $firstName, $lastName, $level) {
                    global $total_counts;
                    global $weekday_counts;
                    global $total_days;
                    global $total_weekdays;

                    $indent_string = '';
                    for ($x = 0; $x < $indent; $x++) {
                        $indent_string .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }
            		echo '<tr>';
            		echo "<td class=\"jl\">" . $indent_string . $firstName . " " . $lastName . "</td>";
            		echo "<td>" . $level . "</td>";
            		echo '<td class="jr">' . $total_counts[$personId] . '</td>';
            		echo '<td class="jr">';
            		if ($total_weekdays > 0) {
                        printf("%01.1f", (round($total_counts[$personId] / $total_days * 1000)/10));
            		}
            		else {
            		    echo "<center>-</center>";
            		}
            		 
            		echo '</td>';
            		echo '<td class="jr">' . $weekday_counts[$personId] . '</td>';
            		echo '<td class="jr">';
            		if ($total_weekdays > 0) {
                        printf("%01.1f", (round($weekday_counts[$personId] / $total_weekdays * 1000)/10));
            		}
            		else {
            		    echo "<center>-</center>";
            		}
            		echo '</td>';
            		echo '</tr>';
            
            
                    $sql = "select PersonID, FirstName, LastName, Level from people where
                        SupervisorID = $personId and Deleted = '0'
                        order by lastname, firstname";
                    $result = mssql_query($sql);
                
                    while ($row = mssql_fetch_assoc($result)) {
                        WriteRow($row['PersonID'], $indent + 1, $row['FirstName'], $row['LastName'], $row['Level']);
                    }
                }
				?>
			</table>
	</body>
</html>

<?php

function WeekdayCount($dateBegin, $dateEnd) {
    $begin_week = date('Y', $dateBegin) * 52 + date('W', $dateBegin);
    $end_week = date('Y', $dateEnd) * 52 + date('W', $dateEnd);
    
    $begin_offset = (GetWeekdayNum($dateBegin) - 1);
    if ($begin_offset > 5) $begin_offset = 5;
    $end_offset = 5 - GetWeekdayNum($dateEnd - 86400);  // All date differences don't sum the last day.
    if ($end_offset < 0) $end_offset = 0;
    
    // echo GetWeekdayNum($dateBegin) . "<br>";
    // echo GetWeekdayNum($dateEnd) . "<br>";
    // echo (($end_week - $begin_week + 1) * 5) . " " . $begin_offset . " " . $end_offset . "<br>";
    
    $weekday_count = ($end_week - $begin_week + 1) * 5 - $begin_offset - $end_offset;
    return $weekday_count;
}

//This returns 1 for Monday & 7 for Sunday (same as the Date('N'...
function GetWeekdayNum($inDate) {
    $day_of_week = date('w', $inDate);
    if ($day_of_week == 0) $day_of_week = 7;
    return $day_of_week;
}

function DaysDiff($dateBegin, $dateEnd) {
    $days_diff = round((strtotime($dateEnd) - strtotime($dateBegin)) / 86400);
    return $days_diff;
}

function GetListByTree($person) {
	// $result = mssql_query($sql);
    $tree_array[] = $person;
    $tree_array = array_merge($tree_array, GetSubordinates($person));
    return $tree_array;
}

function GetSubordinates($supervisor) {
    $sql = "select PersonID from people where 
        SupervisorID = $supervisor and Deleted = '0' 
        order by lastname, firstname";
	$result = mssql_query($sql);
    $tree_array = array();
	while ($row = mssql_fetch_assoc($result)) {
        $tree_array[] = $indent . $row['PersonID'];
        $tree_array = array_merge($tree_array, GetSubordinates($row['PersonID']));
	}
    return $tree_array;    
}

?>