<?php
$start_time = microtime(true);  // DEBUG

include('_report_utils.php');
include('ci_pdf_utils.php');
include('category_labels.php');

define(SHADE_MED, "MED_BLUE");
define(SHADE_LIGHT, "LIGHT_BLUE");


define("TRUE",1);
define("FALSE",0);

/*
TO DO:
1. Enable meeting printout on two lines DONE
2. Include category (NB config columns!) and valuation DONE
3. Enable print from calendar header DONE
*/

$report = CreateReport();  // Creates an array with report variables

//Use defaults(portrait, rather than landscape
$report['pageheight'] = 11 * 72;
$report['pagewidth'] = 8.5 * 72;
$report['left'] = 72 / 4;
$report['bottom'] = 10.75 * 72;
$report['right'] = 8.25 * 72;

//Set smaller font sizes
$report['bigfontsize'] = 20;
$report['normalfontsize'] = 8;
$report['smallfontsize'] = 6;

//$month = 10;
//$year = 2005;
$mname = MonthName();  // Gets the name of the month (as a global)
$title = "$mname $year";

if($caltype == 'week')
{
	$w = $week + 1;
	$title .= "\rWeek $w";
}

$title .= "\rCalendar for:";

//$salesidlist = "3147,3149,3153,3157";
//print("Caltype=$caltype Month=$month Year=$year splist=$salesidlist<br>");

$salesnamelist;
$ShowSunday = false;
$ShowSaturday = false;

$total_conflicts = 0;

$sales_arr=GetSalesmen($salesidlist, $salesnamelist);
if(count($sales_arr) == 1)
	$title .= ' ' . $sales_arr[0]['Name'];
$appt_arr = GetAppointments($salesidlist);

$p = PDF_new();
PDF_set_parameter($p, "license", PDFLIB_LICENSE_KEY);
PDF_open_file($p, "");
StartPDFReport($report, $p, "AAAA", "BBBB", "CCCCC");
//SetLogo($report, "report_logo.jpg", 10, 40, 72, 0);
SetLogo($report, "report_logo.jpg", 14, 50, 110, 0);
$report['footertext']=$title.$salesnamelist;

BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, 0.1);
AddFreeFormText($report, $title, -1, "center", 1.5, 5.5);

if(count($sales_arr) > 1)
{
	WriteNamesHeader($report);
	BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0.1);
	AddFreeFormText($report, "     ", -1, "center", 1.5, 5.5);
}

$report['free_texts'] = array();

BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0.1);
//AddFreeFormText($report, "*    Scheduling conflict", -1, "left", 0.3, 7.5);
//array_push($report['free_texts'], array('text'=>'*', 'font'=>$report['boldfont'], 'fontsize'=>20, 'left'=>19, 'top'=>140));
BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0.1);
AddFreeFormText($report, " ", -1, "left", 0.3, 7.5);

$daycount = 5;  // How many days in a week to show (Sat or Sun?)
if($ShowSunday) $daycount++;
if($ShowSaturday) $daycount++;

/*
$spacewidth=0.1;
$width = (10.5 - ($spacewidth * ($daycount+1))) / $daycount;
*/

$spacewidth=0.05;
$width = (8.0 - ($spacewidth * ($daycount+1))) / $daycount;


BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0);
SetRowColorStandard($report, -1, SHADE_MED);

if($ShowSunday)
{
	AddColumn($report, "", "center", $spacewidth, 0);
	SetColColorStandard($report, -1, -1, SHADE_MED);
	AddColumn($report, "Sunday", "center", $width, 0);
}

AddColumn($report, "", "center", $spacewidth, 0);
SetColColorStandard($report, -1, -1, SHADE_MED);
AddColumn($report, "Monday", "center", $width, 0);

AddColumn($report, "", "center", $spacewidth, 0);
SetColColorStandard($report, -1, -1, SHADE_MED);
AddColumn($report, "Tuesday", "center", $width, 0);

AddColumn($report, "", "center", $spacewidth, 0);
SetColColorStandard($report, -1, -1, SHADE_MED);
AddColumn($report, "Wednesday", "center", $width, 0);

AddColumn($report, "", "center", $spacewidth, 0);
SetColColorStandard($report, -1, -1, SHADE_MED);
AddColumn($report, "Thursday", "center", $width, 0);

AddColumn($report, "", "center", $spacewidth, 0);
SetColColorStandard($report, -1, -1, SHADE_MED);
AddColumn($report, "Friday", "center", $width, 0);

AddColumn($report, "", "center", $spacewidth, 0);
SetColColorStandard($report, -1, -1, SHADE_MED);

if($ShowSaturday)
{
	AddColumn($report, "Saturday", "center", $width, 0);
	AddColumn($report, "", "center", $spacewidth, 0);
	SetColColorStandard($report, -1, -1, SHADE_MED);
}
// Find the day of the week this month starts with
$StartDay = GetStartDayOfWeek($month, $year);

// Find how many days are in the month
$LastDay = GetLastDayOfMonth($month, $year);

//Calculate how many weeks (rows) to show
$weekcount = ($LastDay + $StartDay) / 7;
if(($LastDay + $StartDay) % 7) $weekcount++;

// if the month starts on Saturday but we are nor showing saturdays, don't show a blank first line.
if($StartDay==6 && !$ShowSaturday) $weekcount--;

//$StartColumn = $StartDay;
//if(!$ShowSunday) $StartColumn--;

$test = 0;
$LastDayNew = 1;
while (GetDayOfWeek(($month).'/'.$LastDayNew.'/'.$year) != 0) {
	$LastDayNew++;
}
//print("LastDayNew = $LastDayNew");

$rowcount = intval($weekcount);

for($i=0; $i<=$rowcount; $i++)
{
	if($caltype == 'week' && $i != ($week)) continue;

	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
	SetRowColorStandard($report, -1, SHADE_MED);
	for($j=0; $j<$daycount*2; $j++) AddColumnText($report, " ", "center", 0, 0);

	if($i < $rowcount)
	{
//		BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
		BeginNewRow($report, RT_COL_TEXT_BOLD, NORMAL_FONTSIZE, 0);
		SetRowColorStandard($report, -1, SHADE_MED);

		for($j=0; $j<$daycount; $j++)
		{
			AddColumnText($report, " ", "center", 0, 0);
			PrintCalendarCellHeading($report, $i, $j, $ShowSunday, $ShowSaturday, $StartDay, $LastDay);
		}
		AddColumnText($report, " ", "center", 0, 0);

//		$mtgrows = GetMaxMtgs($i, $daycount, $StartColumn);
		$mtgrows = GetMaxMtgs($i, $daycount, $StartDay);
		if(!$mtgrows)
		{
			BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
			for($j=0; $j<$daycount*2; $j++) AddColumnText($report, " ", "center", 0, 0);
		}
		else
		{
			for($j = 0; $j < $mtgrows; $j++)
			{
//Blank line for space between meetings
				//if($j>0)
				{
					BeginNewRow($report, RT_COL_TEXT, SMALL_FONTSIZE, 0);
					for($l=0; $l<$daycount*2; $l++) AddColumnText($report, " ", "center", 0, 0);
				}
				AddColumnText($report, " ", "center", 0, 0);
				BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
				for($k=0;$k < $daycount; $k++)
				{
					AddColumnText($report, " ", "center", 0, 0);
					PrintCalendarCellMeeting($report, $i, $k, $j, $StartDay, $LastDay);

				}
				AddColumnText($report, " ", "center", 0, 0);
			}

		}
	}
}

if($caltype == 'week') //Paint the bottom border, if week view
{
	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
	SetRowColorStandard($report, -1, SHADE_MED);
	for($j=0; $j<$daycount*2; $j++) AddColumnText($report, " ", "center", 0, 0);

}

$total_conflicts = floor($total_conflicts/2);
BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0);
AddFreeFormText($report, " ", -1, "left", 0.3, 7.5);
BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0);
AddFreeFormText($report, "*    Scheduling conflict", -1, "left", 0.3, 7.5);
$conf = "$total_conflicts Conflict";
if($total_conflicts != 1) $conf .= "s";
$conf .= " found";
BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0);
AddFreeFormText($report, $conf, -1, "left", 0.3, 7.5);

//!!!array_push($report['free_texts'], array('text'=>($total_conflicts.' Conflict'.(($total_conflicts==1)?'':'s').' found'), 'fontsize'=>10, 'left'=>20, 'top'=>150));

PrintPDFReport($report, $p);
PDF_delete($p);

close_db();



function cl($cat)
{
	switch($cat)
	{
		case 1:
			return GetCatLbl("FM");
		case 2:
			return GetCatLbl("IP");
		case 3:
			return GetCatLbl("S") . GetCatLbl("IP");
		case 4:
			return GetCatLbl("DP");
		case 5:
			return GetCatLbl("S") . GetCatLbl("DP");
		case 6:
			return GetCatLbl("CL");
		default:
			return 'Err';
	}

}

function GetElapsedDays($StartTime, $EndTime)
{
	$Diff = strtotime($EndTime) - strtotime($StartTime);
	return (int)($Diff / (60 * 60 * 24));
}

function GetDayOfWeek($strdate)
{
	$dateinfo = getdate(strtotime($strdate));
	return $dateinfo['wday'];
}

function GetMonthDay($strdate)
{
	$dateinfo = getdate(strtotime($strdate));
	return $dateinfo['mday'];
}


function GetStartDayOfWeek($month, $year)
{
	$strdate = sprintf("%d/1/%d", $month, $year);
	return GetDayOfWeek($strdate);
}

function GetLastDayOfMonth($month, $year)
{
	if($month == 12)
	{
		$year++;
		$month = 1;
	}
	else $month++;

	$strdate = sprintf("%d/0/%d", $month, $year);
	$dateinfo = getdate(strtotime($strdate));
	return $dateinfo['mday'];
}

function CalcDayIndex($row, $col, $StartColumn)
{
global  $ShowSunday, $ShowSaturday;
	if($StartColumn==6 && !$ShowSaturday) $row++;
	if(!$ShowSunday) $StartColumn--;
	$dayindex = $row * 7 + $col - $StartColumn;
	return $dayindex;
}

function PrintCalendarCellHeading(&$report, $row, $col, $ShowSunday, $ShowSaturday, $StartColumn, $DaysInMonth)
{
	global $month, $year;
	$dayind = CalcDayIndex($row, $col, $StartColumn);
	$dayindex = $dayind + 1;
	if($dayindex < 1)
	{
		AddColumnText($report, " ", "center", 0, 0);
		return;
	}
	
	$localmonth = $month;
	$localyear = $year;
	if ($dayindex > $DaysInMonth)
	{
		$dayindex = $dayindex - $DaysInMonth;
		$localmonth++;
		if ($localmonth == 13)
		{
			$localmonth = 1;
			$localyear++;
		}
	}

	$tempyear = substr($localyear, 2);
	$text = sprintf("$localmonth/%d/$tempyear", $dayindex);
//print("heading text = $text!!!<br>");
	AddColumnText($report, $text, "center", 0, 0);
}

function MtgConflict($mtgs, $indMtg, $PersonID, $MtgTime)
{
	for ($i=0; $i<count($mtgs); $i++)
	{
		if ($i == $indMtg) continue;
		if ($mtgs[$i]['PersonID'] == $PersonID && $mtgs[$i]['MtgTime'] == $MtgTime)
			return 1;
	}
	return 0;
}

function PrintCalendarCellMeeting(&$report, $row, $col, $mtgrow, $StartColumn, $DaysInMonth)
{
global $sales_arr, $total_conflicts;
	$dayind = CalcDayIndex($row, $col, $StartColumn);
	$dayindex = $dayind+1;
	if($dayindex < 1)
	{
		AddColumnText($report, " ", "center", 0, 0);
		return;
	}
	$mtgs = GetDayAppts($dayind, 'array');
	if($mtgrow >= count($mtgs))
	{
		AddColumnText($report, " ", "center", 0, 0);
		return;
	}
	$mtg = 	$mtgs[$mtgrow];
//Should there be an asterisk, indicating a conflict?
	$conflict = MtgConflict($mtgs, $mtgrow, $mtg['PersonID'], $mtg['MtgTime']);
	if(count($sales_arr) > 1)
		$st = GetSalesmanTag($mtg['PersonID']) . ' ';
	else $st = '';
	if($conflict)
		$rec = '   ' . $st . $mtg['MtgTime'] . "\r" . $mtg['Company'];
	else
		$rec = $st . $mtg['MtgTime'] . "\r" . $mtg['Company'];
	$rec .= '(' . $mtg['Category'];
	if(strlen($mtg['VLevel']) && $mtg['VLevel'] > -1)
		$rec .= ',V' . $mtg['VLevel'];
	$rec .= ')';
	AddColumnText($report, $rec , "center", 0,0);
	if($conflict)
	{
		SetItemValue($report, -1, -1, 'boldtext', true);
		SetItemValue($report, -1, -1, 'free_text', array('text'=>'*', 'fontsize'=>20, 'font'=>$report['boldtext']));
		$total_conflicts++;
	}
}

function GetMaxMtgs($row, $daycount, $StartColumn)
{
	$max = 0;
	for ($i = 0; $i < $daycount; $i++)
	{
		$dayindex=CalcDayIndex($row, $i, $StartColumn);
		$mtg_count = GetDayAppts($dayindex, 'count');
//print("dayindex=$dayindex count=$mtg_count<br>");
		if ($mtg_count > $max) $max = $mtg_count;
	}
	return $max;

}


function GetSalesmen($salesidlist, &$namelist)
{

	$list = array();
	$namelist = "";

	$sql = "select FirstName, LastName, PersonID from people where PersonID in($salesidlist) order by LastName, FirstName";

	$res=mssql_query($sql);
	$ind = 1;
	while(($row=mssql_fetch_assoc($res)))
	{
		$first = $row['FirstName'];
		$last = $row['LastName'];

		if(strlen($namelist) > 0) $namelist .= ", ";
		$namelist .= $first . " " . $last;
		$tag = sprintf(" [%d]", $ind++);
		$namelist .= $tag;
		$tmp = array();
		$tmp['Name'] = $first . " " . $last;
		$tmp['PersonID'] = $row['PersonID'];
		$tmp['tag'] = $tag;
		array_push($list, $tmp);
	}

	return $list;
}

function GetSalesmanTag($ID)
{
global $sales_arr;
	for ($i=0; $i<count($sales_arr); $i++)
	{
		if($ID == $sales_arr[$i]['PersonID'])
		{
			return $sales_arr[$i]['tag'];
		}
	}
	return '-1';
}

function PeekMeetingsOnDate($meeting, $salesidlist, $strdate)
{
	$sql = "select PersonID, Company, Category, VLevel, $meeting as Mtg from opportunities where PersonID in($salesidlist) and $meeting=CONVERT(DATETIME,'$strdate 00:00:00',102)";

	$res=mssql_query($sql);
	if(($row=mssql_fetch_assoc($res)))
		return true;
	return false;
}


function LogMeetings($meeting, $salesidlist, &$list)
{
global $caltype, $ShowSaturday, $ShowSunday, $month, $year, $week;
	$firstofmonth = "$month/1/$year";
	$nextyear = $year;
	$nextmonth = $month + 1;
	if($nextmonth > 12)
	{
		$nextmonth = 1;
		$nextyear++;
	}
	$loc_week=$week;
	if($caltype == 'week')
	{
		if(6==GetDayOfWeek($firstofmonth) && false == PeekMeetingsOnDate($meeting, $salesidlist, $firstofmonth))
		{
			$loc_week++;
		}

		$wstart = $loc_week * 7 - GetStartDayOfweek($month, $year);
		$wend = $wstart + 7;
	}

	$mtgtime = $meeting . 'Time';

	$sql = "select PersonID, Company, Category, VLevel, $meeting as Mtg, $mtgtime as MtgTime from opportunities";
	$sql .=" where PersonID in($salesidlist) and $meeting>=CONVERT(DATETIME,'$month/01/$year 00:00:00',102)";
	$sql .=" and $meeting<CONVERT(DATETIME,'$nextmonth/07/$nextyear 00:00:00',102)and Category<>6 and Category<>9 order by $meeting, $mtgtime";
    $now=getdate();
    $strnow = $now['mon'] . '/' . $now['mday'] . '/' . $now['year'];
	$res=mssql_query($sql);
	while(($row=mssql_fetch_assoc($res)))
	{
		$strdate = $row['Mtg'];
		if(strtotime($strdate) < strtotime($strnow)) continue;
		$mday = GetMonthDay($strdate);
		if($caltype == 'week')
		{
			if ($mday <= $wstart) continue;
			if ($mday > $wend) continue;
		}
		if(6==GetDayOfWeek($strdate))
			$ShowSaturday = true;
		if(!GetDayOfWeek($strdate))
			$ShowSunday = true;
		$dayindex = GetElapsedDays($firstofmonth, $strdate);
		$tmp = array();
		$tmp['srt'] = strtotime($row['MtgTime']);
		$tmp['dayindex'] = $dayindex;
		$tmp['PersonID'] = $row['PersonID'];
		$tmp['Category'] = cl($row['Category']);
		$tmp['Mtg'] = $strdate;
		$tmp['MtgTime'] = $row['MtgTime'];
		$tmp['Company'] = $row['Company'];
		$tmp['VLevel'] = $row['VLevel'];
		array_push($list, $tmp);
	}

}

function GetAppointments($salesidlist)
{
	$list = array();
	LogMeetings('FirstMeeting', $salesidlist, $list);
	LogMeetings('NextMeeting', $salesidlist, $list);
	return $list;
}

function GetDayAppts($day, $lbl)
{
global $appt_arr;
	$tmp = array();
	$ct = 0;
	for($i=0; $i < count($appt_arr); $i++)
	{
		if ($day == $appt_arr[$i]['dayindex'])
		{
			for ($j=0; $j < count($tmp); $j++)
			{
				if (strtotime($appt_arr[$i]['MtgTime']) > strtotime($tmp[$j]['MtgTime']))
					break;
			}
			array_push($tmp, $appt_arr[$i]);
			$ct++;
		}
	}
	sort($tmp);
	if($lbl == 'array')
		return $tmp;
	else
		return $ct;
}

function MonthName()
{
	global $month, $year;
	$info = getdate(strtotime("$month/1/$year"));
	return $info['month'];
}

function WriteNamesHeader(&$report)
{
    global $sales_arr;
	$namesperrow=7;
	$namecount = count($sales_arr);
	if($namecount > 7 && $namecount < 14)
		$namesperrow = intval($namecount / 2);
	$cellwidth = round(8/$namesperrow, 2);
	for($i=0; $i < $namecount; $i++)
	{
		if($i%$namesperrow == 0)
			BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0);
		$tagwidth = .4;
		$namewidth = $cellwidth - $tagwidth;
		AddColumn($report, $sales_arr[$i]['tag'], "center", $tagwidth, 0);
		AddColumn($report, $sales_arr[$i]['Name'], "left", $namewidth, 0);
	}
}

?>
