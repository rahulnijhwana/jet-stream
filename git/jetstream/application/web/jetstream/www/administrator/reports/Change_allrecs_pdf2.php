<?php
/**
 * @package Reports
 */

/*

Issues
------
The change report still pulls its offerings directly from the opportunity table, 
which cannot support multiple offerings.  The data in the opportunity table may
not be accurate even when the opportunity only has a single product.

*/

include('_report_utils.php');
include('TableUtils.php');
include('jim_utils.php');
include('category_labels.php');
include('ci_pdf_utils.php');


define("TRUE",1);
define("FALSE",0);

$report = CreateReport();
$report['left'] = 72/4;
$report['right'] = 10.75 * 72;
$report['bottom'] = 8.25 * 72;

$report['pageheight'] = 8.5*72;
$report['pagewidth'] = 11*72;


$p = PDF_new();
PDF_set_parameter($p, "license", PDFLIB_LICENSE_KEY);
PDF_open_file($p, "");

$salesid = $treeid;
$MSCount=GetMilestoneList($treeid, $MSList);
$MSLabel="Information Phase ($MSList)";



$result = mssql_query("select Level from people where PersonID='$treeid'");
if($result)
{
	$row = mssql_fetch_array($result);
	$level = $row['Level'];
}

StartPDFReport($report, $p, "AAAA", "BBBB", "CCCCC");
//SetLogo($report, "report_logo.jpg", 10, 40, 72, 0);

$salesname = GetSalesmanData($salesid);

$issprow = mssql_fetch_assoc(mssql_query("select IsSalesperson from people where PersonID=$salesid"));
$manIsSP = $issprow['IsSalesperson'] == 1;

$salestree = make_tree_path($treeid);
$title = "Change Report";

$report['footertext'] = $title . " for $salesname";

BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, 0.1);
AddFreeFormText($report, $title, -1, "center", 1.5, 5.5);


BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0.25);
AddFreeFormText($report, $salestree, -1, "center", 1.5, 5.5);

$left = 1;
$width1 = 1;
//$width2 = 3;
$width2 = 5;


PrintLocAbbr($report, $left, $width1, $width2, $treeid);

ChangedOps($report, $mpower_companyid, $treeid, $level, $dateFrom, $dateTo);

PrintPDFReport($report, $p);
PDF_delete($p);


function PrintLocAbbr(&$report, $left, $width1, $width2, $treeid)
{
	$MSCount=GetMilestoneList($treeid, $MSList);
	$MSLabel="Information Phase ($MSList)";

	BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, .2);
	AddFreeFormText($report, "Location Abbreviations:", -1, "left", $left, 2.5);

	AddLocationRow($report, GetCatLbl("T"), GetCatLbl("Target"), $left, $width1, $width2);
	AddLocationRow($report, GetCatLbl("FM"), GetCatLbl("First Meeting"), $left, $width1, $width2);
	AddLocationRow($report, GetCatLbl("IP") . "(1-$MSCount)", GetCatLbl("Information Phase") . "($MSList)", $left, $width1, $width2);
	AddLocationRow($report, GetCatLbl("IPS"), GetCatLbl("Information Phase") . " " . GetCatLbl("Stalled"), $left, $width1, $width2);
	// AddLocationRow($report, GetCatLbl("S"). GetCatLbl("IP"), GetCatLbl("Stalled") . ' ' . GetCatLbl("Information Phase"), $left, $width1, $width2);
	AddLocationRow($report, GetCatLbl("DP"), GetCatLbl("Decision Point"), $left, $width1, $width2);
	AddLocationRow($report, GetCatLbl("DPS"), GetCatLbl("Decision Point") . " " . GetCatLbl("Stalled"), $left, $width1, $width2);
	// AddLocationRow($report, GetCatLbl("S") . GetCatLbl("DP"), GetCatLbl("Stalled") . " " . GetCatLbl("Decision Point"), $left, $width1, $width2);
	AddLocationRow($report, GetCatLbl("C"), GetCatLbl("Closed"), $left, $width1, $width2);
	AddLocationRow($report, GetCatLbl("RM"), GetCatLbl("Removed"), $left, $width1, $width2);
	AddLocationRow($report, "NA", "Unknown Location", $left, $width1, $width2);
}


/*
Shows the last change and last change location for each opportunity.
There are only detail reports, with higher level managers being able
to see the detail of their choice.  Most of the data for this report
comes from the log table.

There are two queries necessary, one to get the latest change record
for each opportunity, and the second to get the two latest change
records for the category field for the opportunity.  A group by select
sorted by salesperson should be able to retrieve this data.

Previous location is determined by looking at the second latest category log record.
If there is only one record, then no entry is put in the previous location,
or days in previous location.

Days in previous location is determined by subtracting the date of the second
latest category log record from the latest category log record.  Date and days
since last change of any kind is determined from looking at the latest log record.

Current location is determined from the latest category log record.

The days in sales cycle is computed by subtracting the opportunity�s first meeting date from today�s date.

THIS INITIAL SPEC IN MPOWER BIBLE WAS SUPPLEMENTED BY THE FOLLOWING EMAIL MSG 3-13-2004:
This is not really a Change Report as we envisioned it.
It is simply a listing of the last movement of each
opportunity on the board.  Our vision of a Change Report
is more like a log, showing all changes that the
salesperson has entered.  Check the specification for
this report, and let�s talk.

*/

function GetCatMilestones($idLoc, $countMileStones) {
	if($countMileStones < 1) {
		$countMileStones = 1;
	}
	switch($idLoc) 	{
		case 1:
			return "FM";
		case 2:
			return "IP$countMileStones";
		case 3:
			return "SIP";
		case 4:
			return "DP";
		case 5:
			return "SDP";
		case 6:
			return "CL";
		case 9:
			return "RM";
		case 10:
			return "T";
	}
	return "$idLoc";
}

function MakeMilestonesString($arrMilestones) {
	$strMS="";
	$count=count($arrMilestones);
	for($i=0;$i<$count;$i++) {
		if($arrMilestones[$i]>0)
		$strMS.=($i+1);
	}
	return $strMS;
}

function MakeLocationString($idLoc) {
	switch($idLoc) {
		case 1:
			return GetCatLbl("FM");
		case 2:
			return GetCatLbl("IP");
		case 3:
			return GetCatLbl("IPS");
			// return GetCatLbl("S") . GetCatLbl("IP");
		case 4:
			return GetCatLbl("DP");
		case 5:
			return GetCatLbl("DPS");
			// return GetCatLbl("S") . GetCatLbl("DP");
		case 6:
			return GetCatLbl("CL");
		case 9:
			return GetCatLbl("RM");
		case 10:
			return GetCatLbl("T");
	}
	return "$idLoc";

}

function MakeCategoryString($strLoc, $strMilestones) {
	return ($strLoc == "IP" || $strLoc == "SIP") ? $strLoc . $strMilestones : $strLoc;
}

function GetDealData($dealid) {
	$sql="SELECT Company, ProductID, FirstMeeting from opportunities where DealID = $dealid";
	$res = mssql_query($sql);
	$row = mssql_fetch_assoc($res);
	return $row;
}


function GetMSCount($arrMS) {
	$ct=0;
	for($i=0;$i<6;$i++)
	$ct+=$arrMS[$i];
	return $ct;
}

function ChangedOps(&$report, $idCompany, $idSupervisor, $level, $dateFrom, $dateTo)
{
	global $manIsSP;

	$arrSalesmen = array();
	//At present, we are set up to return the whole tree
	if($level == 1) {
		$strSalesmen = $idSupervisor;
	} else {
		$strSalesmen=GetSubordinateList($idSupervisor, $arrSalesmen, FALSE);
	}
	if(!strlen($strSalesmen)) {
		return;
	}
	if ($manIsSP) {
		$strSalesmen .= ',' . $idSupervisor;
	}
	
	//$mapDealProducts=array();
	$mapProducts = GetProducts($idCompany);
	if($dateFrom == ""||$dateTo == "") {
//		$sqlCatChanges = "SELECT PersonID, DealID, WhenChanged, TransactionID, LogInt, LogBit 
//			FROM log WHERE PersonID IN ($strSalesmen) 
//			AND (TransactionID = 1 or (TransactionID > 19 
//			AND TransactionID < 26)) 
//			ORDER BY PersonID, DealID, WhenChanged ASC";

		$sqlCatChanges = "SELECT opp.Company, Opp.ProductID, Opp.FirstMeeting, log.PersonID
				, log.DealID, log.WhenChanged, log.TransactionID, log.LogInt, log.LogBit 
			FROM log 
				JOIN opportunities opp on log.dealid = opp.dealid
			WHERE log.PersonID IN ($strSalesmen) 
				AND (log.TransactionID = 1 or (log.TransactionID > 19 and log.TransactionID < 26)) 
			GROUP BY log.DealID, opp.Company, Opp.ProductID, Opp.FirstMeeting, log.PersonID, log.WhenChanged, log.TransactionID, log.LogInt, log.LogBit
			ORDER BY log.PersonID, opp.Company, log.WhenChanged, log.TransactionID, log.LogBit ASC";

	} else {
//		$sqlCatChanges = "SELECT PersonID, DealID, WhenChanged, TransactionID, LogInt, LogBit 
//			FROM log WHERE PersonID IN ($strSalesmen) 
//			AND (TransactionID = 1 or (TransactionID > 19 and TransactionID < 26)) 
//			AND WhenChanged between '$dateFrom' AND '$dateTo' 
//			ORDER BY PersonID, DealID, WhenChanged, TransactionID, LogBit ASC";


		$sqlCatChanges = "SELECT opp.Company, Opp.ProductID, Opp.FirstMeeting, log.PersonID
				, log.DealID, log.WhenChanged, log.TransactionID, log.LogInt, log.LogBit 
			FROM log 
				JOIN opportunities opp on log.dealid = opp.dealid
			WHERE log.PersonID IN ($strSalesmen) 
				AND (log.TransactionID = 1 or (log.TransactionID > 19 and log.TransactionID < 26)) 
				AND log.dealid in 
					(select distinct dealid from log where log.transactionid = 1 and  log.WhenChanged between '$dateFrom' AND '$dateTo')
			ORDER BY log.PersonID, opp.Company, log.DealID, log.WhenChanged, log.TransactionID, log.LogBit ASC";
	}
	//		$sqlCatChanges="SELECT PersonID, DealID, WhenChanged, TransactionID, LogInt, LogBit FROM log WHERE DealID=3828 AND PersonID IN ($strSalesmen) AND (TransactionID = 1 or (TransactionID > 19 and TransactionID < 26)) AND WhenChanged between '$dateFrom' and '$dateTo' ORDER BY PersonID, DealID, WhenChanged, TransactionID, LogBit ASC";

	//print $sqlCatChanges;

	$res = mssql_query($sqlCatChanges);
	$arrOps = array();
	$ind = 0;
	$idDeal = 0;
	$idPerson = 0;
	$strLocPrev = "";
	$strLocFinal = "";
	$strMilestonesPrev = "";
	$strMilestonesFinal = "";
	$now = time();
	$dateNow = date("m/d/y", $now);
	$dateFinal = "";
	$company = "";
	$msCountFinal = 0;    //milestone count
	$msCountPrev = 0;
	$indAll=0;
	$arrMilestones = array_fill(0,6,0);
	while (($row = mssql_fetch_assoc($res))) {
		if ($idDeal != $row["DealID"]) {
			$arrMilestones = array_fill(0,6,0);
			//$catFinal=$catPrev=1;
			$strLocFinal = $strLocPrev="--";

			$dateFinal = $datePrev="";
			// $dealrow = GetDealData($row["DealID"]);
		}
		$idDeal = $row["DealID"];
		$idPerson = $row["PersonID"];

		$txid = $row['TransactionID'];
		$datePrev = $dateFinal;
		$dateFinal = $row["WhenChanged"];
		$strLocPrev = $strLocFinal;
		$strMilestonesPrev = $strMilestonesFinal;

		if ($txid == 1) {
			$strLocFinal = MakeLocationString($row['LogInt']);
		} else {
			// Milestone calc
			if ($txid == 400) continue;
			if (strcmp($strLocPrev,"IP") && strcmp($strLocPrev,"SIP")) continue;
			if ($arrMilestones[$txid - 20] == ($row['LogBit'])) continue;

			$arrMilestones[$txid - 20] = ($row['LogBit'] == 1);
			$strMilestonesFinal = MakeMilestonesString($arrMilestones);
		}
		
//		echo date("m/d/y", $dateFinal) . ':' . date("m/d/y", strtotime($dateFrom)) . ':' . date("m/d/y", strtotime($dateTo)) . '<br>';
//		$val = $now > strtotime($dateFrom);
//		echo $val . '<br>';
//		$val = $now < strtotime($dateFrom);
//		echo $val . '<br>';
//		$val = $now > strtotime($dateTo);
//		echo $val . '<br>';
//		$val = $now < strtotime($dateTo);
//		echo $val . '<br>';
		
		if (strtotime($dateFinal) < strtotime($dateFrom) || strtotime($dateFinal) > strtotime($dateTo)) {
			continue;
		}
		
		$arrOps["Salesman"][$ind] = $arrSalesmen[$idPerson];
		$arrOps["Company"][$ind] = $row['Company'];
		//$arrOps["Name"][$ind]=$mapDealProducts[$idDeal];
		

		
		$arrOps["Name"][$ind] = $mapProducts[$row['ProductID']];

		$arrOps["PrevLocDays"][$ind] = $datePrev == "" ? "0" : DateDiff("d", $datePrev, $dateFinal);
		$arrOps["PrevCat"][$ind] = MakeCategoryString($strLocPrev, $strMilestonesPrev);
		$arrOps["LastChgDays"][$ind] = DateDiff("d", $dateFinal, $dateNow);
		$arrOps["ChangeDate"][$ind] = date("m/d/y", strtotime($dateFinal));
		$arrOps["ChangeTime"][$ind] = date("H:i", strtotime($dateFinal));
		$arrOps["NewCat"][$ind] = MakeCategoryString($strLocFinal, $strMilestonesFinal);
		$cdays = (is_null($row['FirstMeeting'])) ? '-' : DateDiff("d", $row['FirstMeeting'], $dateFinal);
		$arrOps["CycleDays"][$ind] = $cdays > -1 ? $cdays : NULL;
		$ind++;
	}
//		exit;

	//print "   All=$indAll arr=$ind";
	//	if($ind > 0)
	//		array_multisort($arrOps["Salesman"],$arrOps["Name"],$arrOps["Company"],$arrOps["PrevCat"],$arrOps["PrevLocDays"],$arrOps["LastChgDays"],$arrOps["ChangeDate"], $arrOps["NewCat"], $arrOps["CycleDays"]);
	//		array_multisort($arrOps["Salesman"],$arrOps["Name"],$arrOps["Company"],$arrOps["ChangeDate"],$arrOps["ChangeTime"],$arrOps["PrevCat"],$arrOps["PrevLocDays"],$arrOps["LastChgDays"], $arrOps["NewCat"], $arrOps["CycleDays"]);


	BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0);
	SetRowColorStandard($report, -1, SHADE_MED);

	AddColumn($report, "", "left", 1.4, 0);
	SetColColorStandard($report, -1, -1, SHADE_MED);
	AddColumn($report, "Company", "left", 1.4, 0);
	AddColumn($report, "Offering", "left", 1.5, 0);


	AddColumn($report, "Prev Loc", "center", .5, 0);
	AddColumn($report, "Days in Prev Loc", "center", .6, 0);
	AddColumn($report, "Days since Last Activity", "center", .8, 0);

	AddColumn($report, "Date of Change", "center", .7, 0);
	AddColumn($report, "Time of Change", "center", .7, 0);

	AddColumn($report, "New Loc", "center", .5, 0);
	AddColumn($report, "Days in Sales Cycle", "center", .6, 0);


	$Name="";
	for($i = 0; $i < $ind; $i++)
	{
		BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
		//BegintableRowData(2);
		if ($Name!=$arrOps["Salesman"][$i])
		{
			//PrintTableCell($arrOps["Salesman"][$i]);
			AddColumnText($report, $arrOps["Salesman"][$i], "", 0, 0);
			$Name=$arrOps["Salesman"][$i];
		}
		else
		AddColumnText($report, "", "", 0, 0);
		$name = $arrOps['Name'][$i];

		AddColumnText($report, $arrOps["Company"][$i], "", 0, 0);
		AddColumnText($report, $name, "", 0, 0);
		AddColumnText($report, $arrOps["PrevCat"][$i], "", 0, 0);
		AddColumnText($report, $arrOps["PrevLocDays"][$i], "", 0, 0);

		AddColumnText($report, $arrOps["LastChgDays"][$i], "", 0, 0);
		AddColumnText($report, $arrOps["ChangeDate"][$i], "", 0, 0);
		AddColumnText($report, $arrOps["ChangeTime"][$i], "", 0, 0);

		AddColumnText($report, $arrOps["NewCat"][$i], "", 0, 0);
		AddColumnText($report, $arrOps["CycleDays"][$i], "", 0, 0);
	}
}

/*
function GetMilestoneList($personid, &$MSList)
{
$sql="select Requirement1,Requirement2 from Company where CompanyID=(select companyid from people where personid=$personid)";
$res=mssql_query($sql);
$MSList="1=Person; 2=Need; 3=Money; 4=Time";
$count=4;
if($res)
{
$row=mssql_fetch_assoc($res);
if(!is_null($row['Requirement1']))
{
$r1=$row['Requirement1'];
$MSList.="; 5=$r1";
$count++;
}
if(!is_null($row['Requirement2']))
{
$r2=$row['Requirement2'];
$MSList.="; 6=$r2";
$count++;
}
}

return $count;
}
*/

function GetProducts($idCompany) {
	$sql = "select * from Products where CompanyID = $idCompany";
	$ind = 0;
	$res = mssql_query($sql);
	// They may want to change this!
	$mapResults[-1] = "Unknown";
	while($row = mssql_fetch_assoc($res)) {
		$mapResults[$row["ProductID"]] = $row["Name"];
	}

	return $mapResults;
}



function DateDiff($interval, $date1, $date2) {
	$s = strtotime($date2)-strtotime($date1);
	$d = intval($s/86400);
	$s -= $d * 86400;
	$h = intval($s/3600);
	$s -= $h * 3600;
	$m = intval($s/60);
	$s -= $m * 60;
	$arrRes=array("d" => $d, "h" => $h, "m" => $m, "s" => $s);
	return $arrRes[$interval];
}

close_db();

?>
