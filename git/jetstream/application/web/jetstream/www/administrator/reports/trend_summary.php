<?php
 /**
 * @package Reports
 */
include('_report_utils.php');
include('trends.php');
include('GetSalespeopleTree.php');

$cellfontsize="16px";

begin_report('Trend Summary Report');

if (!isset($treeid))
{
	print_tree_prompt(MANAGEMENT);
	end_report();
	exit();
}

print_tree_path($treeid);

//PrintLocationKey($treeid);

/***********************
function IsSalesmanInTrend($TrendId, $SalesId, &$StartDate, &$NumDays)
{
	$StartDate = IsTrendCurrent($SalesId, $TrendId);
	if($StartDate)
	{
		$Today = time();
		$elapsed = $Today - $StartDate;
		$NumDays = (int)($elapsed / (60 * 60 * 24));
		//$StartDate = date("M d Y", $StartDate);
		$StartDate=date("m/d/y", $StartDate);
		return true;
	}
	return false;
}
************************/

/***********************************
function PrintSalesmanTrendCell($TrendId, $SalesId)
{
	if(IsSalesmanInTrend($TrendId, $SalesId, $StartDate, $NumDays))
	{
		PrintTableCell($StartDate . " (" . $NumDays . " days)");
	}
	else
	{
		PrintTableCell("--");
	}
}
**************************/

/*************************
function PrintTrendRepRow($TrendName, $TrendId, &$SalesArray)
{
	BeginTableRow();
	PrintTableCell($TrendName);

	$count = count($SalesArray);
	for($i=0; $i<$count; $i++)
	{
 		PrintSalesmanTrendCell($TrendId, $SalesArray[$i]);
	}

	EndTableRow();
}
*******************/

/****************
function PrintTrendConRow($TrendName, $TrendId)
{
	$StartDate = mktime (0,0,0,1,1,2000);

	BeginTableRow();
	$Count;
	$AvgDays;
	$CurrentCount;
	GetConsolidatedTrend($StartDate, $TrendId, $Count, $AvgDays, $CurrentCount);

	PrintTableCell($TrendName);
	PrintTableCell($Count);
	PrintTableCell($AvgDays);
	PrintTableCell($CurrentCount);

	EndTableRow();
}
*****************/

function PrintCurrentTrendConRow($TrendName, $TrendId, $SalesIds)
{
	$StartDate = mktime (0,0,0,1,1,2000);

	BeginTableRow();
	PrintTableCell($TrendName);

	$Count;
	$AvgDays;

	GetRegionCurrentTrendSummary($SalesIds, $TrendId, $Count, $AvgDays);

	if($Count > 0)
	{
		PrintTableCell($Count);
		PrintTableCell($AvgDays);
	}
	else
	{
		PrintTableCell("--");
		PrintTableCell("--");
	}

	EndTableRow();
}

function PrintTrendSummaryReport(&$TrendNames, &$TrendIds, $SupervisorID)
{
	$Headings = Array();
	array_push($Headings, "TRENDS");
	array_push($Headings, "People in Trend");
	array_push($Headings, "Avg Days in Trend");

	$SalesIds = GetSalespeopleTreeArray($SupervisorID);

	BeginTable();

	PrintTableRow($Headings);

	$TrendCount = count($TrendNames);
	for($ind=0; $ind<$TrendCount; $ind++)
	{
		$ID = $TrendIds[$ind] + 100;
		$TrendName = $TrendNames[$ind];
		PrintCurrentTrendConRow($TrendName, $ID, $SalesIds);
	}

	EndTable();
}


function TrendSummaryReport($salesid, $companyid)
{

	$TrendNames = Array();
	$TrendIds = Array();

	GetTrends($TrendNames, $TrendIds, $companyid);

	$sql="select * from people where PersonID=$salesid";
	$res=mssql_query($sql);

	if($row=mssql_fetch_assoc($res))
	{
		$level = $row['Level'];

		if($level == 2)
		{
			PrintTrendSummaryReport($TrendNames, $TrendIds, $salesid);
		}
		else
		{
			$sql = "select * from people where SupervisorID=$salesid and Deleted=0 and level>1";
			$res=mssql_query($sql);

			while($row=mssql_fetch_assoc($res))
			{
				$id = $row['PersonID'];
				$name = $row['FirstName'] . " " . $row['LastName'];
				$group = $row['GroupName'];
				if(strlen($group) > 0) $name .= " ($group)";

				print("<br><br><b>$name</b><br>");
				PrintTrendSummaryReport($TrendNames, $TrendIds, $id);
			}
		}
	}

}

?>



<?php

TrendSummaryReport($treeid, $mpower_companyid);
?>

<?php
end_report();
?>