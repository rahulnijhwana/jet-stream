<?php
 /**
 * @package Reports
 */
include('_report_utils.php');

begin_report('Trend History Report');
if (!isset($treeid))
{
	print_tree_prompt(SALESPEOPLE);
	end_report();
	exit();
}
print("<meta http-equiv=\"Refresh\" content=\"1; URL=trend_history1_pdf.php?treeid=$treeid\">");
print('<br><br><b>     Please wait while your report is being prepared.');
// print('<br><br><b>     Please wait while your report is being prepared.<br><br><iframe style="display:none;" src="/develop/reports/sample.pdf"></iframe>');
end_report();
?>
