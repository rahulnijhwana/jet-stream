<?php
 /**
 * @package Reports
 */

$sql = "select * from company where CompanyID = $mpower_companyid";
$ok = mssql_query($sql);
$cat_row = mssql_fetch_assoc($ok);

$g_column_labels = array(
array('First Meeting', $cat_row['FMLabel']),
array('FM', $cat_row['FMAbbr']),
array('Information Phase', $cat_row['IPLabel']),
array('IP', $cat_row['IPAbbr']),
array('Stalled Information Phase', $cat_row['IPSLabel']),
array('Information Phase Stalled', $cat_row['IPSLabel']),
array('IPS', $cat_row['IPSAbbr']),
array('SIP', $cat_row['IPSAbbr']),

array('Decision Point', $cat_row['DPLabel']),
array('DP', $cat_row['DPAbbr']),
array('Decision Point Stalled', $cat_row['DPSLabel']),
array('Stalled Decision Point', $cat_row['DPSLabel']),
array('DPS', $cat_row['DPSAbbr']),
array('SDP', $cat_row['DPSAbbr']),
array('Closed', $cat_row['CLabel']),
array('C', $cat_row['CAbbr']),
array('CL', $cat_row['CAbbr']),
array('Removed', $cat_row['RLabel']),
array('RM', $cat_row['RAbbr']),
array('Target', $cat_row['TLabel']),
array('T', $cat_row['TAbbr']),
array('Stalled', $cat_row['SLabel']),
array('S', $cat_row['SAbbr'])
);

function CatLbl($default, &$text)
{
	$subLabel = GetCatLbl($default);
	if($subLabel == $default) return;
	$text = str_replace($default, $subLabel, $text);
	return $text;
}

function GetCatLbl($default) {
	$default = strtoupper(trim($default));
	global $g_column_labels;

	foreach($g_column_labels as $label) {
		if (trim(strtoupper($default)) == trim(strtoupper($label[0])))
		if (strlen(trim($label[1])) > 0) {
			return trim($label[1]);
		} else {
			if ($label[0] == 'DPS') {
				$default = trim(GetCatLbl('DP')) . trim(GetCatLbl('S'));
			}
			if ($label[0] == 'IPS') {
				$default = trim(GetCatLbl('IP')) . trim(GetCatLbl('S'));
			}

			return $default;
		}
	}
}


function ScanForLabels($text)
{
	global $g_column_labels;
	for ($i=0; $i < count($g_column_labels); $i++)
	{
		// $subLabel = strlen($g_column_labels[$i][1]) > 0 ? $g_column_labels[$i][1] : '';
		// RJP 7-25-05: we need to have the defaults available as well.
		$subLabel = strlen($g_column_labels[$i][1]) ? $g_column_labels[$i][1] : $g_column_labels[$i][0];
		$text = str_replace('{{' . $g_column_labels[$i][0] . '}}' , $subLabel, $text);
	}
	return $text;
}

function SimpleReplaceLabels($text)
{
global $g_column_labels;
	for ($i=0; $i < count($g_column_labels); $i++)
	{
		$subLabel = strlen($g_column_labels[$i][1]) > 0 ? $g_column_labels[$i][1] : '';
		$text = str_replace($g_column_labels[$i][0] , $subLabel, $text);
	}
	return $text;
}


?>
