<?php
 /**
 * @package Reports
 */

function GetMilestoneNames($companyid)
{
	$locations = array();

	$sql = "select * from Company where CompanyID=$companyid";

	$result = mssql_query($sql);
	if($result)
	{
		$count = 0;
		if($row = mssql_fetch_assoc($result))
		{
			$used1 = $row['Requirement1used'];
			$used2 = $row['Requirement2used'];

			if($used1) $locations[0] = $row['Requirement1'];
			else $locations[0] = "";
			$locations[1] = strlen($row['MSLabel2']) > 0 ? $row['MSLabel2'] : "Person";
			$locations[2] = strlen($row['MSLabel3']) > 0 ? $row['MSLabel3'] : "Need";
			$locations[3] = strlen($row['MSLabel4']) > 0 ? $row['MSLabel4'] : "Money";
			$locations[4] = strlen($row['MSLabel5']) > 0 ? $row['MSLabel5'] : "Time";

/*
			$locations[1] = "Person";
			$locations[2] = "Need";
			$locations[3] = "Money";
			$locations[4] = "Time";
*/
			if($used2) $locations[5] = $row['Requirement2'];
			else $locations[5] = "";
		}
	}

	return $locations;
}

function GetMilestoneString(&$submilestones, $parent)
{
	$temp = "";
	$count = count($submilestones);
	$ind = 0;

	for($i=0; $i<$count; $i++)
	{
		$sub = $submilestones[$i];
		$p = $sub['parent'];
		if($p != $parent) continue;

		$done = $sub['done'];
		$ind++;
		if($done)
		{
			//if(strlen($temp) > 0) $temp .= ",";
			$temp .= $ind;
		}
	}

	++$parent;
	$str = "IP$parent ($temp)";
	return $str;
}

function SetMilestones(&$submilestones, $subid, $done)
{
	$count = count($submilestones);

	for($i=0; $i<$count; $i++)
	{
		$sub = $submilestones[$i];
		if($subid != $sub['id']) continue;
		$sub['done'] = $done;
		$submilestones[$i] = $sub;
		break;
	}
}

function GetMilestoneValue(&$submilestones, $subid)
{
	$count = count($submilestones);

	for($i=0; $i<$count; $i++)
	{
		$sub = $submilestones[$i];
		if($subid != $sub['id']) continue;
		return $sub['done'];
	}
	return 0;
}

function FindSubMilestones(&$submilestones, $subid)
{
	$count = count($submilestones);
	for($i=0; $i<$count; $i++)
	{
		$sub = $submilestones[$i];
		if($subid == $sub['id'])
			return $sub;
	}

	return 0;
}

function GetSubMilestones($companyid)
{
	$submilestones = array();

	$sql = "select * from submilestones where (IsHeading=0 or IsHeading is null) and (CompanyID=$companyid OR CompanyID=-999) order by Location,Seq";

	$result = mssql_query($sql);
	if($result)
	{
		$count = 0;
		while($row = mssql_fetch_assoc($result))
		{
			$mstone = array();
			$mstone['id'] = $row['SubID'];
			$mstone['parent'] = $row['Location'];
			$mstone['prompt'] = $row['Prompt'];

			$submilestones[$count++] = $mstone;
		}

	}

	return $submilestones;
}

function PrintMilestones(&$report, $submilestones, &$parentnames)
{

	$left = 1;
	$width1 = 1;
	$width2 = 3;
	$offset = 3.5;

	BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, .2);
	AddFreeFormText($report, "IP MILESTONE DETAIL:", -1, "left", $left, 2.5);

	$count = count($submilestones);

	$lastparent = -1;
	$ind = 1;
	for($i=0; $i<$count; $i++)
	{
		$sub = $submilestones[$i];
		$parent = $sub['parent'];
if(!$parentnames[$parent]) continue;
		if($parent != $lastparent)
		{
			$lastparent = $parent;
//Milestones must display begining with 1
			//$loc = $parent + 1;
			$loc++;
			$name = $parentnames[$parent];

			AddLocationRow($report, "Milestone $loc:", $name, $left, $width1, $width2);
			$ind=1;
		}


		$prompt = $sub['prompt'];
		AddLocationRow($report, "$ind:  ", $prompt, $left, $width1, $width2);
		++$ind;
	}
}

?>