<?php

function GetDirectReporters($SupervisorID)
{
	$SalesIds = array();

	$sql = "select * from people where Deleted=0 and SupervisorID=$SupervisorID";
	$result = mssql_query($sql);
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$id=$row['PersonID'];
			array_push($SalesIds, $id);
		}
	}
	return $SalesIds;
}

function _GetSalespeopleTree(&$SalesIds, $SupervisorID, $FullTree)
{
	$sql = "select * from people where Deleted=0 and PersonID=$SupervisorID";
	$result = mssql_query($sql);
	if($result)
	{
		if($row = mssql_fetch_assoc($result))
		{
			$issalesperson = $row['IsSalesperson'];

			if($issalesperson)
			{
				array_push($SalesIds, $SupervisorID);
			}
		}
	}


	$sql = "select * from people where Deleted=0 and SupervisorID=$SupervisorID";
	$result = mssql_query($sql);
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$id=$row['PersonID'];

			$level = $row['Level'];
			$issalesperson = $row['IsSalesperson'];

			if($issalesperson || $FullTree)
			{
				array_push($SalesIds, $id);
			}

			if($level > 1)
			{
				_GetSalespeopleTree($SalesIds, $id, $FullTree);
			}
		}
	}
}

function GetSalespeopleTree($SupervisorID)
{
	$full = 0;
	$args = func_get_args();
	if(count($args) > 1)
		$full = $args[1];

	$SalesIds = array();
	$idlist = "";

	_GetSalespeopleTree($SalesIds, $SupervisorID, $full);

	$count = count($SalesIds);
	for($i=0; $i<$count; $i++)
	{
		if($i > 0) $idlist .= ",";
		$idlist .= $SalesIds[$i];
	}

//print("idlist=$idlist<br>");
	return $idlist;
}

function GetSalespeopleTreeArray($SupervisorID)
{
	$SalesIds = array();
	$idlist = "";

	_GetSalespeopleTree($SalesIds, $SupervisorID, 0);

	return $SalesIds;
}

function GetFullName(&$row, $reverse, $breakline)
{
	$lastname = $row['LastName'];
	$firstname = $row['FirstName'];
	$groupname = $row['GroupName'];

	$fullname = "";

	if($reverse)
	{
		$fullname = $lastname . ",";
		if($breakline) $fullname .= "\r";
		else $fullname .= " ";
		$fullname .= $firstname;
	}
	else
	{
		$fullname = $firstname;
		if($breakline) $fullname .= "\r";
		else $fullname .= " ";
		$fullname .= $lastname;
	}

	if(strlen($groupname))
	{
		if($breakline) $fullname .= "\r";
		else $fullname .= " ";
		$fullname .= "($groupname)";
	}

	return $fullname;
}
?>