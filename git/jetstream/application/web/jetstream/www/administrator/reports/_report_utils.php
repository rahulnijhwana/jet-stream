<?php
 /**
 * @package Reports
 */
include('../data/_base_utils.php');

define('ANYBODY', 1);
define('SALESPEOPLE', 2);
define('SALESPEOPLE_MGR', 3);
define('MANAGEMENT', 4);
define('SALESPEOPLE_AND_MGR', 5);


function begin_report($title)
{
	print_report_top($title);

	if (!check_report_login())
	{
		print_login_prompt();
		end_report();
	}

	// this is no longer used, but I left it and the ensure_parameters() function here in
	// case similar functionality is needed some time in the future.  -jason
	/*if ($params && !ensure_parameters($params))
	{
		print_parameter_prompt($params);
		end_report();
	}*/
}

function end_report()
{
	print_report_bottom();
	close_db();
	exit();
}

function GetOppProducts($DealID)
{
	$sql = 	"SELECT Opp_Product_XRef.ProductID, products.Name AS ProductName";
	$sql .= " FROM Opp_Product_XRef INNER JOIN products ON Opp_Product_XRef.ProductID = products.ProductID";
	$sql .= " WHERE Opp_Product_XRef.DealID = $DealID";

	$Products = "";
	$ok = mssql_query($sql);
	while(($row=mssql_fetch_assoc($ok)))
	{
		if(strlen($Products) > 0) $Products .= ", ";
		$Products .= $row['ProductName'];
	}
	return $Products;
}

// print the tree and allow the user to select a spot in it to run the report with
// type can be ANYBODY, SALESPEOPLE, SALESPEOPLE_MGR
function print_tree_prompt($type)
{
	global $mpower_companyid, $currentuser_personid, $currentuser_isadmin;

	$people = array();
	$sql = "select * from people where CompanyID = '$mpower_companyid' and Deleted = '0'";
	$sql .= " order by LastName";
	$result = mssql_query($sql);
	while ($row = mssql_fetch_assoc($result)) array_push($people, $row);
	$len = count($people);

	if ($len == 0)
	{
		print('<p class="report_error">There are no people defined for this company.</p>');
		return;
	}

	print('<p>Please select a person or group to continue:</p>');
	print('<table align="center"><tr><td nowrap>');

	if ($currentuser_isadmin)
	{
		// people at the top of the tree have SupervisorID = -2
		for ($i = 0; $i < $len; ++$i)
		{
			$person = $people[$i];
			if ($person['SupervisorID'] == -2)
				print_person($person, $people, $type, 0);
		}

		// unassigned people have SupervisorID = -1
		for ($i = 0; $i < $len; ++$i)
		{
			$person = $people[$i];
			if ($person['SupervisorID'] == -1)
				print_person($person, $people, $type, 0);
		}
	}
	else
	{
		for ($i = 0; $i < $len; ++$i)
		{
			$person = $people[$i];
			if ($person['PersonID'] != $currentuser_personid)
				continue;
			print_person($person, $people, $type, 0);
			break;
		}
	}

	print('<br></td></tr></table>');
}

function print_person($person, $people, $type, $indent)
{
	for ($i = 0; $i < $indent; ++$i)
		print('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');

	$link = false;
	if ($type == ANYBODY)
		$link = true;
	else if ($type == SALESPEOPLE && $person['IsSalesperson'] == '1')
		$link = true;
	else if ($type == SALESPEOPLE_MGR)
	{
		$len = count($people);
		for ($i = 0; $i < $len; ++$i)
		{
			if ($people[$i]['SupervisorID'] == $person['PersonID'] && $people[$i]['IsSalesperson'] == '1')
			{
				$link = true;
				break;
			}
		}
	}
	else if ($type == MANAGEMENT && $person['Level'] > 1)
		$link = true;
	else if ($type == SALESPEOPLE_AND_MGR && $person['Level'] <= 2)
		$link = true;

	if ($link) $anchor = '<a href="javascript:use_treeid(\'' . $person['PersonID'] . '\')">';

	if ($link) print($anchor . '<img src="../images/sp_icon.gif" border=0></a>');
	else print('<img src="../images/sp_gray.gif">');

	print(' ');

	if ($link) print("$anchor<b>");
	print($person['FirstName'] . ' ' . $person['LastName']);
	if (strlen($person['GroupName'])) print(' (' . $person['GroupName'] . ')');
	if ($link) print('</b></a>');

	print('<br>');

	$len = count($people);
	for ($i = 0; $i < $len; ++$i)
	{
		$subperson = $people[$i];
		if ($subperson['SupervisorID'] == $person['PersonID'])
			print_person($subperson, $people, $type, $indent + 1);
	}
}

function print_tree_path($id)
{
	print('<p><b>' . make_tree_path($id) . "</b></p>\n\n");
}

function make_tree_path($id)
{
	global $mpower_companyid;

	$fields = 'FirstName, LastName, GroupName, SupervisorID';
	$sql = "select $fields from people where CompanyID = '$mpower_companyid' and PersonID = '$id'";
	$result = mssql_query($sql);
	if ($result && ($row = mssql_fetch_assoc($result)))
	{
		$name = $row['FirstName'] . ' ' . $row['LastName'];
		$gname = $row['GroupName'];
		if ($gname != null && strlen($gname)) $name .= ' (' . $gname . ')';

		$sid = (int) $row['SupervisorID'];
		if (0 < $sid) $name = make_tree_path($sid) . ': ' . $name;
		return $name;
	}
	else return "";
}

function ensure_parameters($params)
{
	$len = count($params);
	$out = '';
	for ($i = 0; $i < $len; ++$i)
	{
		$param = explode('|', $params[$i]);
		$pname = trim($param[0]);
		if (!isset($GLOBALS[$pname]) || !strlen($GLOBALS[$pname])) return false;
	}

	return true;
}

// this could be beefed up a bit, to allow for other than string input
function print_parameter_prompt($params)
{
	$len = count($params);
	$out = '';
	for ($i = 0; $i < $len; ++$i)
	{
		$pinfo = explode('|', $params[$i]);
		$pname = trim($pinfo[0]);
		$out .= '<tr><td nowrap class="plabel">' . trim($pinfo[1]);
		$out .= '</td><td><input type="text" name="' . $pname . '"';
		if (isset($GLOBALS[$pname])) $out .= ' value="' . $GLOBALS[$pname] . '"';
		$out .= '></td></tr>';
	}

	$url = $_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING'];
	print('<p>Please enter the following information to continue:</p>');
	print('<form name="paramform" method="post" action="' . $url . '">');
	print('<table border=0 align="center">' . $out);
	print('<tr><td>&nbsp;</td><td><input class="command" type="submit" value="continue"></td></tr></table>');
}

function print_login_prompt()
{
	global $mpower_companyid, $mpower_userid;

	print('<script language="JavaScript" src="../javascript/loginscreen.js"></script>');
	print('<script language="JavaScript">');
	print("<!--\n");
	if (!isset($mpower_companyid))
		print("document.writeln('<p class=\"error\">You must log in before viewing reports.</p><br><br>');\n");
	else
	{
		if (isset($mpower_userid))
			print("document.writeln('<p class=\"error\">Invalid login information. Please check your spelling and try again.</p>');\n");
		else
			print("document.writeln('<p>Please enter your user ID and password to continue:</p>')\n");
		print("document.writeln(LoginScreen.makeHTML());\n");
	}
	print("// -->\n");
	print('</script>');
}

function print_report_top($title)
{
	global $PHP_SELF, $SELF_URL_EXTRA;

	print('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">' . "\n");
	print('<html lang="en">' . "\n");
	print('<head>' . "\n");
	print('<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">' . "\n");
	// "M-Power" removed (for Systema)
	print("<title>$title</title>\n");
	print('<link rel="stylesheet" type="text/css" href="../css/sharedstyles.css">' . "\n");
	print('<script language="JavaScript" src="../javascript/pageformat.js,windowing.js"></script>' . "\n");
	?>
	
	<style>
	
	body, table
	{
		font-size:12pt;
		font-family:Arial;
	}
	
	</style>
	
	<?
	print('</head>' . "\n");
	print('<body leftmargin=3 rightmargin=3 topmargin=3 bottommargin=3 bgcolor="white">' . "\n");
	print('<script language="JavaScript">' . "\n");
	print('<!--' . "\n");
	print("function use_treeid(id)\n");
	print("{\n");
	print("var sep = '?';\n");
	print("if (window.location.href.indexOf('?') != -1) sep = '&';\n");
	print("window.location.href = window.location.href + sep + 'treeid=' + id;\n");
	print("}\n");
	print("function do_reselect()\n");
	print("{\n");
	print("window.location.href = '$PHP_SELF$SELF_URL_EXTRA';\n");
	print("}\n");
	print("Header.setText('$title');\n");
	print("Header.addButton(ButtonStore.getButton('Reselect'));\n");
	print("Header.addButton(ButtonStore.getButton('Print'));\n");
	print('document.writeln(Header.makeHTML());' . "\n");
	print("var ivoryBox = new IvoryBox('100%', null);\n");
	print('document.writeln(ivoryBox.makeTop());' . "\n");
	print('// -->' . "\n");
	print('</script>' . "\n");
	print('<!-- begin report-specific output -->' . "\n");
}

function print_report_bottom()
{
	print('<!-- end report-specific output -->' . "\n");
	print('<script language="JavaScript">' . "\n");
	print('<!--' . "\n");
	print('document.writeln(ivoryBox.makeBottom());' . "\n");
	print('// -->' . "\n");
	print('</script>' . "\n");
	print('</body>' . "\n");
	print('</html>' . "\n");
}

function apply_symbol()
{
    global $mpower_companyid;
	$sql = "select MonetaryValue from Company where CompanyID = '$mpower_companyid'";
//print $sql . "<br>";
	$result = mssql_query($sql);
	$row = mssql_fetch_assoc($result);
	$mv = $row['MonetaryValue'];
	if(is_null($mv)) return true;
	return ($mv == 1);

}

//Optional argument no_dec switches off decimal formatting.
//If no_dec  == -1, apply no formatting at all; just prepend $
function fm($money_value)
{
	$no_dec = 0;
	if(func_num_args() > 1)
		$no_dec = func_get_arg(1);
	switch($no_dec)
	{
		case 0:
			$fm = number_format($money_value, 2);
			break;
		case 1:
			$fm = number_format($money_value);
			break;
		case -1:
			$fm = $money_value;
	}

	if (apply_symbol())
		$fm = '$' . $fm;
	return $fm;
}

/*
//moved to _date_params.php
function print_date_params($personid, $sortcol, $ord)
{
$today=date("m/d/Y");
$mt_lastmonth = mktime(0, 0, 0, date("m")-1, date("d"),  date("Y"));
$lastmonth = date("m/d/Y", $mt_lastmonth);
?>
<script language="JavaScript" src="../javascript/utils.js"></script>
<form>
<center>
FROM: <input id="dateFrom" readonly="true" name="dateFrom" type="text" onchange="onEditDateFrom();" size="10" value="<?php print($lastmonth);?>" ><button type="button"  class="ButtonWithGradient"value="Change..." style="height:25px;" onclick="onClickChangeDateFrom();">Change...</button><br>
TO: <input id="dateTo" readonly="true" name="dateTo" type="text" onchange="onEditDateTo();" size="10" value="<?php print($today);?>" ><button type="button"  class="ButtonWithGradient"value="Change..." style="height:25px;" onclick="onClickChangeDateTo();">Change...</button><br><br>
<input type="submit" class="ButtonWithGradient"value="Continue" style="height:25px;" ></input><br>
<input type="hidden" name="treeid" value="<?php print($personid);?>" ></input><br>
<input type="hidden" name="sortcol" value="<?php print($sortcol);?>" ></input><br>
<input type="hidden" name="ord" value="<?php print($ord);?>" ></input><br>

</center>
</form>
<script language=Javascript>

			function check_dateFrom()
			{
				var dateFrom = document.getElementById("dateFrom");
				if (!dateFrom)
					return true;
				var val = dateFrom.value;
				if (val != '')
				{
					val = Dates.normalize(val);
					if (!val)
					{
						alert('Invalid date format for From Date.\nMust be either MM/DD/YY or MM/DD/YYYY');
						return false;
					}
				}
				return true;
			}

			function check_dateTo()
			{
				var dateTo = document.getElementById("dateTo");
				if (!dateTo)
					return true;
				var val = dateTo.value;
				if (val != '')
				{
					val = Dates.normalize(val);
					if (!val)
					{
						alert('Invalid date format for To Date.\nMust be either MM/DD/YY or MM/DD/YYYY');
						return false;
					}
				}
				return true;
			}



			function onEditDateFrom()
			{
				if (!check_dateFrom())
					document.getElementById("dateFrom").value = '';
			}

			function onEditDateTo()
			{
				if (!check_dateTo())
					document.getElementById("dateTo").value = '';

			}


			function onClickChangeDateFrom()
			{
//				var calwin = Windowing.openSizedPrompt("../shared/calendar.html", 320, 600);
				var calwin = Windowing.openSizedPrompt("../shared/calendar_rpt_param.html", 320, 600);

				Windowing.dropBox.calendarTarget = document.getElementById("dateFrom");
				Windowing.dropBox.onCalendarEdited = onEditDateFrom();
			}

			function onClickChangeDateTo()
			{
//				var calwin = Windowing.openSizedPrompt("../shared/calendar.html", 320, 600);
				var calwin = Windowing.openSizedPrompt("../shared/calendar_rpt_param.html", 320, 600);
				Windowing.dropBox.calendarTarget = document.getElementById("dateTo");
				Windowing.dropBox.onCalendarEdited = onEditDateTo();
			}

</script>
<?php
}
*/
?>
