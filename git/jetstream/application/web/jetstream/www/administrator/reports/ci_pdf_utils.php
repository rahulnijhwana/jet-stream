<?php
 /**
 * @package Reports
 */
define('PDFLIB_LICENSE_KEY', 'L700102-010000-114560-X8WR82-SZEJA2');
define('RT_COL_HEADING', 1);
define('RT_COL_TEXT', 2);
define('RT_FREEFORM', 3);
define('RT_COL_TEXT_BOLD', 4);

define('NORMAL_FONTSIZE', -1);
define('BIG_FONTSIZE', -2);
define('SMALL_FONTSIZE', -3);

function ReplaceInString(&$str, $lookfor, $insert, &$changed)
{
	While(1)
	{
		$pos = strpos( $str, $lookfor);
		if($pos === false) break;

		$tmp = substr($str, 0, $pos);
		$tmp .= $insert;
		$tmp .= substr($str, $pos + strlen($lookfor));

		$str = $tmp;
		$changed = true;
	}
}

function RemoveSlashes(&$text)
{
	$changed = false;
	ReplaceInString($text, "\\", " ", $changed);
	ReplaceInString($text, "/", " ", $changed);
	return $changed;
}

function BreakText($text)
{
	$count = strlen($text);
	$start = 0;
	$tempstart = 0;
	$templen = 0;
	$savlen = 0;

	for($i=0; $i<$count; $i++,$templen++)
	{
		if(substr($text, $i, 1) === " ")
		{
			if($templen > $savlen)
			{
				$start = $tempstart;
				$savlen = $templen;
			}
			$templen = 0;
			$tempstart = $i+1;
		}
	}

	if($savlen>0 && $savlen<3)
		return $text;

	$breakpos = 0;
	if($savlen>0)
		$breakpos = $start + $savlen/2;
	else
		$breakpos = $count/2;

	$newtext = substr($text, 0, $breakpos);
	$newtext .= " ";
	$newtext .= substr($text, $breakpos, $count+1-$breakpos);

	return $newtext;
}

function DrawWrappedText(&$pdflib, $text, $startleft, $starttop, $width, $maxheight, $leading, $justify, $bPrint, $valign = 'top')
{
	// rjp 4-4-07: also stop infinite recursion if maxheight < leading
	if($width < 0 || $maxheight < $leading) {
		return -1;	// prevent infinite recursing when width < 0
	}
	if(empty($justify)) $justify="left";
	if(empty($width)) $width=1.0;
	if(empty($text) && $text !== '0' && $text !== 0) {
		return $leading;
	}

	$testlen = strlen($text);
	if($testlen == 0) {
		return $leading;
	}
	PDF_set_value($pdflib, "leading", $leading);

	for($newheight = $leading; $newheight<=$maxheight; $newheight += $leading)
	{
		if ($valign == 'top')
			$topadjust = $newheight;
		else if ($valign == 'middle')
			$topadjust = ($newheight / 2) + ($leading / 2);
		else if ($valign == 'bottom')
			$topadjust = 0;
		$charsleft = PDF_show_boxed($pdflib, $text, $startleft, $starttop + $topadjust, $width, $newheight, $justify, "blind");
		if($charsleft == 0)
		{
			if($bPrint) PDF_show_boxed($pdflib, $text, $startleft, $starttop + $topadjust, $width, $newheight, $justify, "");
			return $newheight;
		}
	}

	if(RemoveSlashes($text)) {
		$rsret = DrawWrappedText($pdflib, $text, $startleft, $starttop, $width, $maxheight, $leading, $justify, $bPrint, $valign);
		return $rsret;
	}

	$newtext = BreakText($text);
	if(!strcmp($text, $newtext))
    {
    //print("newtext = $newtext<br>");
    		return -1;
    }

	$btret = DrawWrappedText($pdflib, $newtext, $startleft, $starttop, $width, $maxheight, $leading, $justify, $bPrint, $valign);
	return $btret;

//print("too long text = $text charsleft = $charsleft<br>");
	return -1;
}

function DrawRotatedText(&$pdflib, $text, $startleft, $starttop, $width, $maxheight, $leading, $justify, $bPrint, $textwidth)
{
	$fudge = 2;

	$newwidth = $textwidth;
	$left =	-($starttop + $newwidth);
//	$top = $startleft + ($width/3);

    $top = $startleft + ($width/$fudge);


	$justify = "left";

	if(empty($justify)) $justify="left";
	if(empty($width)) $width=1.0;
	if(empty($text) && $text !== '0' && $text !== 0) return $leading;

	$testlen = strlen($text);
	if($testlen == 0) return $leading;
	PDF_set_value($pdflib, "leading", $leading);

	for($newheight = $leading; $newheight<=$maxheight; $newheight += $leading)
	{
//		$charsleft = PDF_show_boxed($pdflib, $text, $left, $top + $newheight, $newwidth, $newheight, $justify, "blind");

        $charsleft = PDF_show_boxed($pdflib, $text, $starttop, $startleft + $newheight, $newwidth, $newheight, $justify, "blind");

		if($charsleft == 0)
		{
		$top = $startleft + ($width - $newheight) / 2;
//			if($bPrint) PDF_show_boxed($pdflib, $text, $startleft, $starttop + $newheight, $width, $newheight, $justify, "");
			if($bPrint) PDF_show_boxed($pdflib, $text, $left, $top + $newheight, $newwidth, $newheight, $justify, "");
			return $newwidth;
		}
	}

	return -1;
}

function DrawLine(&$pdflib, $x1, $y1, $x2, $y2)
{
	pdf_moveto($pdflib, $x1, $y1);
	pdf_lineto($pdflib, $x2, $y2);
	pdf_stroke($pdflib);
}

function PrintImage(&$pdflib, $imagename, $x, $y, $width, $maxheight)
{
	// sprintf(optbuf, "fitmethod meet boxsize {%d %d}", $width, $maxheight);

	$image = PDF_load_image($pdflib, "auto", $imagename, "");
	if ($image <= 0) return 0;

	$heightneeded = 0;

	$imageheight = PDF_get_value($pdflib, "imageheight", $image);
	$imagewidth = PDF_get_value($pdflib, "imagewidth", $image);
	if($maxheight > 0)
	{
		$aspect = $imageheight/$imagewidth;

		$heightneeded = $width * $aspect;
		if($heightneeded > $maxheight)
		{
			PDF_close_image($pdflib, $image);
			return -1;
		}
	}

	$printscale = $width / $imagewidth;
	PDF_fit_image($pdflib, $image, $x, $y, "scale $printscale");

	PDF_close_image($pdflib, $image);
	return $heightneeded;
}

function ColorRectRGB(&$pdflib, $r, $g, $b, $x, $y, $width, $height, $bStroke)
{
	$stroke = $bStroke ? "stroke" : "fill";
//	PDF_setcolor($pdflib, "fill", "rgb", $r, $g, $b, 0);
	PDF_setcolor($pdflib, $stroke, "rgb", $r, $g, $b, 0);

	PDF_rect($pdflib, $x, $y, $width, $height);
	PDF_closepath($pdflib);
	if($bStroke)
	{
		PDF_stroke($pdflib);
	}
	else
	{
		PDF_fill($pdflib);
	}
//	PDF_setcolor($pdflib, "fill", "rgb", 0, 0, 0, 0);
	PDF_setcolor($pdflib, $stroke, "rgb", 0, 0, 0, 0);

}

function DrawRect(&$pdflib, $x, $y, $width, $height)
{
	PDF_rect($pdflib, $x, $y, $width, $height);
	PDF_closepath($pdflib);
	PDF_stroke($pdflib);
}

function GetStandardColor($color, &$r, &$g, &$b)
{
	if($color == "LIGHT_YELLOW")
	{
		$r = 1;
		$g = 1;
		$b = 0.8;
	}
	else if($color == "WHITE")
	{
		$r = 1;
		$g = 1;
		$b = 1;
	}
	else if($color == "LIGHT_GRAY")
	{
		$r = 0.85;
		$g = 0.85;
		$b = 0.85;
	}
	else if($color == "DARK_BLUE")
	{
		$r = 0.40;
		$g = 0.43;
		$b = 0.60;
	}
	else if($color == "MED_BLUE")
	{
		$r = 0.70;
		$g = 0.75;
		$b = 0.85;
	}
	else if($color == "LIGHT_BLUE")
	{
		$r = 0.87;
		$g = 0.9;
		$b = 0.95;
	}
	else if($color == "MED_GREEN")
	{
		$r = 0.75;
		$g = 0.85;
		$b = 0.75;
	}
	else if($color == "LIGHT_GREEN")
	{
		$r = 0.9;
		$g = 0.95;
		$b = 0.9;
	}
	else if($color == "MED_RED")
	{
		$r = 0.85;
		$g = 0.75;
		$b = 0.75;
	}
	else
	{
		$r = 0;
		$g = 0;
		$b = 0;
	}
}

function SetRowColorStandard(&$report, $rowind, $color)
{
	GetStandardColor($color, $r, $g, $b);
	SetRowColor($report, $rowind, $r, $g, $b);
}

function SetRowValue(&$report, $rowind, $label, $value)
{
	$rows = $report['rows'];
	if($rowind < 0) $rowind = count($rows)-1;
	if($rowind < 0) return false;

	$row = $rows[$rowind];
	$row[$label] = $value;
	$rows[$rowind] = $row;
	$report['rows'] = $rows;

	return true;
}

function SetRowColor(&$report, $rowind, $r, $g, $b)
{
	$argcount=func_num_args();
	$bStroke = false;
	if($argcount > 5)
	{
		$args = func_get_args();
		$bStroke = $args[5];
	}

	$color = array();
	$color['red'] = $r;
	$color['green'] = $g;
	$color['blue'] = $b;

	$rows = $report['rows'];
	if($rowind < 0) $rowind = count($rows)-1;
	if($rowind < 0) return false;

	$row = $rows[$rowind];
	$row['usecolor'] = 1;
	$row['color'] = $color;
	$row['stroke'] = $bStroke;
	$rows[$rowind] = $row;
	$report['rows'] = $rows;

	return true;
}

function SetColColorStandard(&$report, $rowind, $colind, $color)
{
	$argcount=func_num_args();
	$bStroke = false;
	if($argcount > 4)
	{
		$args = func_get_args();
		$bStroke = $args[4];
	}

	GetStandardColor($color, $r, $g, $b);
	if($bStroke == true)
		SetColColor($report, $rowind, $colind, $r, $g, $b, true);
	else
		SetColColor($report, $rowind, $colind, $r, $g, $b);

}

function SetColColor(&$report, $rowind, $colind, $r, $g, $b)
{
	$argcount=func_num_args();
	$bStroke = false;
	if($argcount > 6)
	{
		$args = func_get_args();
		$bStroke = $args[6];
	}


	$color = array();
	$color['red'] = $r;
	$color['green'] = $g;
	$color['blue'] = $b;

	$rows = $report['rows'];
	if($rowind < 0) $rowind = count($rows) - 1;
	if($rowind < 0) return false;

	$row = $rows[$rowind];
	$items = $row['items'];
	if($colind < 0) $colind = count($items) - 1;
	if($colind < 0) return false;

	$item = $items[$colind];
	$item['usecolor'] = 1;
	$item['color'] = $color;
	$item['stroke'] = $bStroke;

	$items[$colind] = $item;
	$row['items'] = $items;
	$rows[$rowind] = $row;
	$report['rows'] = $rows;

	return true;
}

function SetItemValue(&$report, $rowind, $colind, $label, $value)
{
	$color = array();
	$color['red'] = $r;
	$color['green'] = $g;
	$color['blue'] = $b;

	$rows = $report['rows'];
	if($rowind < 0) $rowind = count($rows) - 1;
	if($rowind < 0) return false;

	$row = $rows[$rowind];
	$items = $row['items'];
	if($colind < 0) $colind = count($items) - 1;
	if($colind < 0) return false;

	$item = $items[$colind];
	$item[$label] = $value;

	$items[$colind] = $item;
	$row['items'] = $items;
	$rows[$rowind] = $row;
	$report['rows'] = $rows;

	return true;
}

function CreateReportItem($text, $alignment, $spacinginches, $widthinches, $leftinches)
{
	$item = array();
	$item['text'] = $text;
	$item['rotate'] = 0;
	$item['width'] = $widthinches * 72;
	$item['spacing'] = $spacinginches * 72;
	$item['alignment'] = $alignment;
	$item['left'] = $leftinches * 72;
	return $item;
}

function CreateReportRow($rowtype, $fontsize, $spacinginches)
{
	$argcount=func_num_args();
	$bForcePageBreak = false;
	if($argcount > 3)
	{
		$args = func_get_args();
		$bForcePageBreak = $args[3];
	}


	$row = array();
	$items = array();
	$row['items'] = $items;

	$row['rowtype'] = $rowtype;
	$row['fontsize'] = $fontsize;
	$row['spacing'] = $spacinginches * 72;
	$row['usecolor'] = 0;
	$row['rotate'] = 0;
	$row['rotate_y'] = 0;
	$row['force_page_break'] = $bForcePageBreak;

	return $row;
}

function PrintRowPDF(&$report, &$pdflib, $rowind, $headerrowind, $top, $bottom, $bPrint, $rowheight)
{
	$rows = $report['rows'];
	$row = $rows[$rowind];

	$fontsize = $row['fontsize'];
	$rowtype = $row['rowtype'];
	$rotate = $row['rotate'];
	$rotate_y = $row['rotate_y'];

	if (isset($row['valign'])) {
		$valign = $row['valign'];
	}
	
	if (isset($row['border_top'])) {
		$border_top = $row['border_top'];
	}
	
	if (isset($row['border_bottom'])) {
		$border_bottom = $row['border_bottom'];
	}
	
	$bold = $report['boldfont'];
	$text = $report['textfont'];

	if($rowtype == RT_COL_HEADING)
	{
		// if there is not at least 1.5 in left, don't start a new header section on this page
		if(($bottom - $top) < (1.5 * 72)) return -1;
	}

	$maxheight = 0;
	$x = $report['left'];

	if($rowtype == RT_COL_HEADING || $rowtype == RT_COL_TEXT_BOLD)
		$font = $bold;
	else
		$font = $text;
	PDF_setfont($pdflib, $font, $fontsize);

	$leading = $fontsize+2;
	PDF_set_value($pdflib, "leading", $leading);

	if($rowtype == RT_COL_HEADING || $rowtype == RT_COL_TEXT || $rowtype == RT_COL_TEXT_BOLD)
	{
		$headerrow = $rows[$headerrowind];
		$hitems = $headerrow['items'];
		$items = $row['items'];
		$count = count($items);
		$left = $report['left'];
		for($i=0; $i<$count; $i++)
		{
			$item = $items[$i];
			$hitem = $hitems[$i];
			$text = $item['text'];
			$item_rotate = $item['rotate'];
			$width = $hitem['width'];
			$spacing = $hitem['spacing'];
			//Override font setting above;
			if(isset($item['boldtext']) && $bold != $font)
			{
				PDF_setfont($pdflib, $bold, $fontsize);
			}
			else
				PDF_setfont($pdflib, $font, $fontsize);

			if($rowtype == RT_COL_HEADING)
				$alignment = $hitem['alignment'];
			else
				$alignment = $item['alignment'];


			$usecolor = (isset($item['usecolor'])) ? $item['usecolor'] : (isset($hitem['usecolor'])) ? $hitem['usecolor'] : null;

			$border_left = (isset($item['border_left'])) ? $item['border_left'] : (isset($hitem['border_left'])) ? $hitem['border_left'] : null;
			$border_right = (isset($item['border_right'])) ? $item['border_right'] : (isset($hitem['border_right'])) ? $hitem['border_right'] : null;
			if (!isset($border_top)) {
				$border_top = (isset($item['border_top'])) ? $item['border_top'] : null;
			}
			if (!isset($border_bottom)) {
				$border_bottom = (isset($item['border_bottom'])) ? $item['border_bottom'] : null;
			}
			
			// $usecolor = $item['usecolor'];
			// if(!isset($usecolor)) $usecolor = $hitem['usecolor'];

			// $border_left = $hitem['border_left'];
			//$border_right = $hitem['border_right'];

			// if (!$border_top)
			//	$border_top = $item['border_top'];
			// if (!$border_bottom)
			//	$border_bottom = $item['border_bottom'];
			// if (!$border_left)
			// 	$border_left = $item['border_left'];
			// if (!$border_right)
			// 	$border_right = $item['border_right'];

			if (!isset($valign)) {
				 $valign = (isset($item['valign'])) ? $item['valign'] : 'top';
			}
			
//			if (!$valign)
//				$valign = $item['valign'];
//			if (!$valign)
//				$valign = 'top';

			if($bPrint && $usecolor)
			{
				$color = (isset($item['color'])) ? $item['color'] : (isset($hitem['color'])) ? $hitem['color'] : null;

				// $color = $item['color'];
				// if(!isset($color)) $color = $hitem['color'];
                //Set stroke
				$stroke = $hitem['stroke'];
				ColorRectRGB($pdflib, $color['red'], $color['green'], $color['blue'], $left, ($top+$rowheight+4), $width, $rowheight, false);

			}

			if($rotate || $item_rotate)
			{
				PDF_save($pdflib);
				PDF_rotate($pdflib, 90);
				$textwidth = 1.75*72;
				$height = DrawRotatedText($pdflib, $text, $left, $top, $width, $bottom - $top, $fontsize + 2, $alignment, $bPrint, $textwidth);
				PDF_restore($pdflib);
			}
			else
			{
				$height = DrawWrappedText($pdflib, $text, $left, $top + 1.5, $width - 1, ($bottom - $top) - 2, $fontsize + 2, $alignment, $bPrint, $valign);
				if (isset($item['free_text']))
				{
					$tempfont = $report['textfont'];
					$tempfontsize = 10;
					$tempwidth = 100;
					$tempheight = 100;
					if (isset($item['free_text']['font']))
						$tempfont = $item['free_text']['font'];
					if (isset($item['free_text']['fontsize']))
						$tempfontsize = $item['free_text']['fontsize'];
					if (isset($item['free_text']['width']))
						$tempwidth = $item['free_text']['width'];
					if (isset($item['free_text']['height']))
						$tempheight = $item['free_texts']['height'];
					PDF_setfont($pdflib, $tempfont, $tempfontsize);
					PDF_show_boxed($pdflib, $item['free_text']['text'], $left, $top + 1.5 + $tempheight, $tempwidth, $tempheight, "left", "");
				}
			}

			if ($bPrint)
			{
				if ($border_top)
					DrawLine($pdflib, $left, $top + 4, $left + $width, $top + 4);

				if ($border_bottom)
					DrawLine($pdflib, $left, $top + $rowheight + 4, $left + $width, $top + $rowheight + 4);

				if ($border_left)
					DrawLine($pdflib, $left, $top + 4, $left, $top + $rowheight + 4);

				if ($border_right)
					DrawLine($pdflib, $left + $width, $top + 4, $left + $width, $top + $rowheight + 4);
			}

			if($height < 0)
				return -1;

			if($height > $maxheight)
				$maxheight = $height;

			$left += $width + $spacing;
		}
	}
	else if($rowtype == RT_FREEFORM)
	{
		$items = $row['items'];
		$count = count($items);
		for($i=0; $i<$count; $i++)
		{
			$item = $items[$i];
			$text = $item['text'];
			$width = $item['width'];
			$alignment = $item['alignment'];
			$left = $item['left'];
			$spacing = $item['spacing'];

			$height = DrawWrappedText($pdflib, $text, $left, $top, $width, $bottom - $top, $fontsize + 2, $alignment, $bPrint);
			if($height < 0) return -1;

			if($height > $maxheight) $maxheight = $height;

			$x += $width + $spacing;
		}
	}


	return $maxheight;
}


function PrintFooter(&$report, &$pdflib, $pageno)
{
	$footertext = $report['footertext'];
	$left = $report['left'];
	$right = $report['right'];
	$pageheight = $report['pageheight'];

	$numtext = sprintf("Page %d", $pageno);

	$font = $report['textfont'];
	$fontsize = $report['smallfontsize'];
	PDF_setfont($pdflib, $font, $fontsize);

	DrawWrappedText($pdflib, $footertext , $left, $pageheight - (72/4), $right - $left, 72/4, $fontsize + 2, "left", true);
	DrawWrappedText($pdflib, $numtext, $left, $pageheight - (72/4), $right - $left, 72/4, $fontsize + 2, "right", true);
}


function PrintReportPDF(&$report, &$pdflib, $calAsterisk=false)
{
	$top = $report['top'];
	$currenty = $top;
	$bottom = $report['bottom'];

        PDF_set_info($pdflib, "Creator", "Creator");
        PDF_set_info($pdflib, "Author", "Author");
        PDF_set_info($pdflib, "Title", "Title goes here");


	LoadFonts($report, $pdflib);

        PDF_set_parameter($pdflib, "topdown", "true");

	$pageno = 0;
        PDF_begin_page($pdflib, $report['pagewidth'], $report['pageheight']);
	PrintTop($report, $pdflib);
	PrintFooter($report, $pdflib, ++$pageno);

	$rows = $report['rows'];
	$count = count($rows);

	$headerrowind = -1;

	for($i=0; $i<$count; $i++)
	{
		$brepeated = 0;
		$row = $rows[$i];
		$rowtype = $row['rowtype'];

		if($rowtype == RT_COL_HEADING)
			$headerrowind = $i;
		else if($rowtype != RT_COL_TEXT && $rowtype != RT_COL_TEXT_BOLD) $headerrowind = -1;
		$usecolor = $row['usecolor'];

		$rowheight = PrintRowPDF($report, $pdflib, $i, $headerrowind, $currenty, $bottom, false, 0);
//		if($rowheight < 0)
		if($rowheight < 0 || $row['force_page_break'] == true)

		{
			$brepeated = 1;
			PrintLogo($report, $pdflib);
			PDF_end_page($pdflib);
		    PDF_begin_page($pdflib, $report['pagewidth'], $report['pageheight']);

			PrintTop($report, $pdflib);
			PrintFooter($report, $pdflib, ++$pageno);

			$currenty = $top;
			if($headerrowind > -1)
			{
				for($j=$headerrowind; $j > 0; $j--)
				{
					if($rows[$j]['rowtype'] != RT_COL_HEADING) break;
				}
				$headerrowstart = $j++;
				for ($j = $headerrowstart; $j <= $headerrowind; $j++)
				{
					$headerrow = $rows[$j];
					$husecolor = $headerrow['usecolor'];
					$rowheight=PrintRowPDF($report, $pdflib, $j, $j, $currenty, $report['bottom'], false, 0);
					if($husecolor)
					{
						$color = $headerrow['color'];
						ColorRectRGB($pdflib, $color['red'], $color['green'], $color['blue'], $report['left'], $currenty+$rowheight+4, $report['right']-$report['left'], $rowheight, false);
					}
					$rowheight=PrintRowPDF($report, $pdflib, $j, $j, $currenty, $report['bottom'], true, $rowheight);
					$currenty += $rowheight;
					$currenty += $headerrow['spacing'];
				}

/*
				$headerrow = $rows[$headerrowind];
				$husecolor = $headerrow['usecolor'];
				$rowheight=PrintRowPDF($report, $pdflib, $headerrowind, $headerrowind, $currenty, $report['bottom'], false, 0);
				if($husecolor)
				{
					$color = $headerrow['color'];
					ColorRectRGB($pdflib, $color['red'], $color['green'], $color['blue'], $report['left'], $currenty+$rowheight+4, $report['right']-$report['left'], $rowheight, false);
				}
				$rowheight=PrintRowPDF($report, $pdflib, $headerrowind, $headerrowind, $currenty, $report['bottom'], true, $rowheight);
				$currenty += $rowheight;
				$currenty += $headerrow['spacing'];
                */
                //Need to recalc row height for the line we skipped!
				$rowheight = PrintRowPDF($report, $pdflib, $i, $headerrowind, $currenty, $bottom, false, 0);

			}
		}

		if($brepeated == 0 || $headerrowind != $i)
		{
			if($usecolor)
			{
				$color = $row['color'];
				$left = $report['left'];
				ColorRectRGB($pdflib, $color['red'], $color['green'], $color['blue'], $left, $currenty+$rowheight+4, $report['right']-$left, $rowheight, false);
			}

			PrintRowPDF($report, $pdflib, $i, $headerrowind, $currenty, $report['bottom'], true, $rowheight + $row['spacing']);
			$currenty += $rowheight;
			$currenty += $row['spacing'];
		}
	}
	
	if (isset($report['free_texts']))
	{
		for ($k = 0; $k < count($report['free_texts']); ++$k)
		{
			$tempfont = $report['textfont'];
			$tempfontsize = 10;
			$tempwidth = 100;
			$tempheight = 100;
			if (isset($report['free_texts'][$k]['font']))
				$tempfont = $report['free_texts'][$k]['font'];
			if (isset($report['free_texts'][$k]['fontsize']))
				$tempfontsize = $report['free_texts'][$k]['fontsize'];
			if (isset($report['free_texts'][$k]['width']))
				$tempwidth = $report['free_texts'][$k]['width'];
			if (isset($report['free_texts'][$k]['height']))
				$tempheight = $report['free_texts'][$k]['height'];
			PDF_setfont($pdflib, $tempfont, $tempfontsize);
			PDF_show_boxed($pdflib, $report['free_texts'][$k]['text'], $report['free_texts'][$k]['left'], $report['free_texts'][$k]['top'], $width, $height, "left", "");
		}
	}

	PrintLogo($report, $pdflib);
        PDF_end_page($pdflib);
        PDF_close($pdflib);

	return true;
}

function AddReportItem(&$row, $text, $alignment, $spacinginches, $widthinches, $leftinches)
{
	$items = $row['items'];
	$item = CreateReportItem($text, $alignment, $spacinginches, $widthinches, $leftinches);
	$item['alignment'] = $alignment;
	$item['width'] = $widthinches * 72;
	$item['left'] = $leftinches * 72;
	array_push($items, $item);
	$row['items'] = $items;

}

function AddColumn(&$report, $text, $alignment, $widthinches, $spacinginches)
{
	$currentrow = $report['currentrow'];
	if($currentrow < 0) return false;
	$rows = $report['rows'];
	$row = $rows[$currentrow];
	if($row['rowtype'] != RT_COL_HEADING) return false;

	AddReportItem($row, $text, $alignment, $spacinginches, $widthinches, 0);
	$rows[$currentrow] = $row;
	$report['rows'] = $rows;

	return true;
}


function AddColumnText(&$report, $text, $alignment, $widthinches, $spacinginches)
{
	$currentrow = $report['currentrow'];
	if($currentrow < 0) return false;
	$rows = $report['rows'];
	$row = $rows[$currentrow];
	if($row['rowtype'] == RT_COL_HEADING) return false;

	AddReportItem($row, $text, $alignment, $spacinginches, $widthinches, 0);
	$rows[$currentrow] = $row;
	$report['rows'] = $rows;

	return true;
}


function AddFreeFormText(&$report, $text, $font, $alignment, $leftinches, $widthinches)
{
	$currentrow = $report['currentrow'];
	if($currentrow < 0) return false;
	$rows = $report['rows'];
	$row = $rows[$currentrow];
	$spacinginches = null;
	
	if($row['rowtype'] != RT_FREEFORM) return false;

	AddReportItem($row, $text, $alignment, $spacinginches, $widthinches, $leftinches);

	$rows[$currentrow] = $row;
	$report['rows'] = $rows;

	return true;
}



function BeginNewRow(&$report, $rowtype, $fontsize, $spacinginches)
{
	$argcount=func_num_args();
	$bForcePageBreak = false;
	if($argcount > 4)
	{
		$args = func_get_args();
		$bForcePageBreak = $args[4];
	}

	if($fontsize == -1) $fontsize = $report['normalfontsize'];
	if($fontsize == -2) $fontsize = $report['bigfontsize'];
	if($fontsize == -3) $fontsize = $report['smallfontsize'];
	if($bForcePageBreak)
		$row = CreateReportRow($rowtype, $fontsize, $spacinginches, $bForcePageBreak);
	else
		$row = CreateReportRow($rowtype, $fontsize, $spacinginches);
	$rows = $report['rows'];
	$count = count($rows);
	$report['currentrow'] = $count;
	array_push($rows, $row);
	$report['rows'] = $rows;

	return true;
}


function LoadFonts(&$report, &$pdflib)
{
	$report['boldfont'] = PDF_Load_font($pdflib, $report['boldfontname'], "host", "");
	$report['textfont'] = PDF_load_font($pdflib, $report['textfontname'], "host", "");

	return true;
}

function SetLogo(&$report, $filename, $x, $y, $width, $height)
{
	// CHANGED 01-11-05 RJP:
	// Pick up report logo from ../shared/images/branding/$domain

//	$domain = preg_replace("/^www./", "", $_SERVER["HTTP_HOST"]);
    $domain = 'mpasa.com';

	$report['logofilename'] = "../shared/images/branding/$domain/$filename";

	$report['logox'] = $x;
	$report['logoy'] = $y;
	$report['logowidth'] = $width;
	$report['logoheight'] = $height;
}

function PrintLogo(&$report, &$pdflib)
{
	$filename = $report['logofilename'];

	if($filename)
	{
		$x = $report['logox'];
		$y = $report['logoy'];
		$w = $report['logowidth'];
		$h = $report['logoheight'];
		// PrintImage($pdflib, $filename, $left + $x, $top + $y, $w, $h);
		PrintImage($pdflib, $filename, $x, $y, $w, $h);
	}
}

function PrintTop(&$report, &$pdflib)
{
	$currentdate = date("m/d/y");

	$font = $report['textfont'];
	$fontsize = $report['smallfontsize'];
	PDF_setfont($pdflib, $font, $fontsize);

	$left = $report['left'];
	$right = $report['right'];
	$top = $report['top'];
	$bottom = $report['bottom'];

	DrawWrappedText($pdflib, $currentdate, $left, 72*.2, $right - $left, 72/2, $fontsize + 2, "right", true);
/*
	$filename = $report['logofilename'];

	if($filename)
	{
		$x = $report['logox'];
		$y = $report['logoy'];
		$w = $report['logowidth'];
		$h = $report['logoheight'];
		PrintImage($pdflib, $filename, $left + $x, $top + $y, $w, $h);
	}
*/
	if($report['boxpage'])
	{
		$left -= 2;
		$right += 2;
		DrawRect($pdflib, $left, $bottom, $right-$left, $bottom-$top);
	}
}


function CreateReport()
{
	$report = array();
	$rows = array();
	$report['rows'] = $rows;

	$report['boldfontname'] = "Helvetica-Bold";
	$report['textfontname'] = "Helvetica";
	$report['footertext'] = "";
	$report['currentrow'] = -1;
	$report['headerrow'] = -1;

	$report['pagewidth'] = 8.5*72;
	$report['pageheight'] = 11*72;

	$report['top'] = 72*.5;
	$report['left'] = 72;
	$report['bottom'] = 10.5*72;
	$report['right'] = 7.5*72;

	$report['bigfontsize'] = 20;
	$report['normalfontsize'] = 10;

	$report['smallfontsize'] = 8;

	$report['boxpage'] = 1;


	return $report;
}

function SetLandscape(&$report)
{
	$report['pageheight'] = 8.5*72;
	$report['pagewidth'] = 11*72;

	$report['top'] = 72*.5;
	$report['left'] = 72/2;
	$report['bottom'] = 8*72;
	$report['right'] = 10.5*72;
}

function StartPDFReport(&$report, &$pdflib, $creator, $author, $title)
{

	PDF_set_info($pdflib, "Creator", $creator);
	PDF_set_info($pdflib, "Author", $author);
	PDF_set_info($pdflib, "Title", $title);

}

function PrintPDFReport(&$report, &$pdflib)
{
	$time = time();

	PrintReportPDF($report, $pdflib);

	$buf = PDF_get_buffer($pdflib);
	$len = strlen($buf);

	header("Content-type: application/pdf");
	header("Content-Length: $len");
	header("Content-Disposition: inline; filename=$time.pdf");
	print $buf;
}

?>
