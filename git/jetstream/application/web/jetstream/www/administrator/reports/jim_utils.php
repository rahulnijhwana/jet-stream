<?php
 /**
 * @package Reports
 */

define('SHADE_MED', 'MED_BLUE');
define('SHADE_LIGHT', 'LIGHT_BLUE');

function PrintColHeads($TableData,$Style, $start, $stop, $bRowHeads)
{
	BeginTableRowData($Style);
	$count = count($TableData);
	if($stop > $count) $stop=$count;
	$ind = $start;
	if ($bRowHeads) PrintTableCell("");
	while($ind < $stop)
	{
		PrintTableCell("$TableData[$ind]");
		$ind = $ind + 1;
	}
	EndTableRow();
}

function PrintColHeads_PDF(&$report, $arrLabels,   $begin, $end, $bRowHeads)
{
	if($end>count($arrLabels)) $end=count($arrLabels);
	BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0);

	SetRowColorStandard($report, -1, SHADE_MED);
	SetColColorStandard($report, -1, -1, SHADE_MED);

	if($bRowHeads)
		AddColumn($report, " ", "left", 1.0, 0);

	for($i=$begin; $i<$end; $i++)
	{
		AddColumn($report, "$arrLabels[$i]", "right", 1.0, 0);

	}
}


/*
function PrintTableCell($CellData)
{
	Print('<td>');
	Print("  ");
	Print($CellData);
	Print("  ");
}
*/

function PrintColHeadings($TableData, $colnames)
{
	BeginTableRowData(1);
	$count = count($colnames);
	$ind = 0;
	while($ind < $count)
	{
		PrintTableCell($TableData[$colnames[$ind]]);
		$ind = $ind + 1;
	}
	EndTableRow();
}

function AddLocationRow(&$report, $text1, $text2, $left, $width1, $width2)
{
	BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0);
	AddFreeFormText($report, $text1, -1, "center", $left, $width1);
	AddFreeFormText($report, $text2, -1, "left", $left+$width1, $width2);
}

function GetMilestoneList($personid, &$MSList)
{
	$sql="select MS2Label, MS3Label, MS4Label, MS5Label, Requirement1,Requirement2, Requirement1Used, Requirement2Used from Company where CompanyID=(select companyid from people where personid=$personid)";
	$res=mssql_query($sql);
	$arrMS = array();
	if($res)
		{
		$row=mssql_fetch_assoc($res);
		//if(!is_null($row['Requirement1'])) array_push($arrMS, $row['Requirement1']);
		if($row['Requirement1Used']) array_push($arrMS, $row['Requirement1']);
		if(!is_null($row['MS2Label'])) array_push($arrMS, $row['MS2Label']);
		else array_push($arrMS, "Person");
		if(!is_null($row['MS3Label'])) array_push($arrMS, $row['MS3Label']);
		else array_push($arrMS,'Need');
		if(!is_null($row['MS4Label'])) array_push($arrMS, $row['MS4Label']);
		else array_push($arrMS, 'Money');
		if(!is_null($row['MS5Label'])) array_push($arrMS, $row['MS5Label']);
		else array_push($arrMS, 'Time');
//		if(!is_null($row['Requirement2']))  array_push($arrMS, $row['Requirement2']);
		if($row['Requirement2Used'])  array_push($arrMS, $row['Requirement2']);

		for ($i = 0; $i < count($arrMS); $i++)
			{
			$ind = $i+1;
			if(strlen($MSList) > 0) $MSList .= ";";
			$MSList .= "$ind=$arrMS[$i]";
			}
		}
		else 	$MSList="1=Person; 2=Need; 3=Money; 4=Time";

	return count($arrMS);
}
/*
function GetMilestoneList($personid, &$MSList)
{
	$sql="select Requirement1,Requirement2 from Company where CompanyID=(select companyid from people where personid=$personid)";
	$res=mssql_query($sql);
	$MSList="1=Person; 2=Need; 3=Money; 4=Time";
	$count=4;
	if($res)
		{
		$row=mssql_fetch_assoc($res);
		if(!is_null($row['Requirement1']))
		{
			$r1=$row['Requirement1'];
			$MSList.="; 5=$r1";
			$count++;
		}
		if(!is_null($row['Requirement2']))
			{
			$r2=$row['Requirement2'];
			$MSList.="; 6=$r2";
			$count++;
			}
		}

	return $count;
}
*/

/*
function PrintLocAbbr(&$report, $left, $width1, $width2, $treeid)
{
	$MSCount=GetMilestoneList($treeid, $MSList);
	$MSLabel="Information Phase ($MSList)";

	BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, .2);
	AddFreeFormText($report, "Location Abbreviations:", -1, "left", $left, 2.5);


	AddLocationRow($report, "FM", "First Meeting", $left, $width1, $width2);
	AddLocationRow($report, "IP(1-$MSCount)", "Information Phase ($MSList)", $left, $width1, $width2);
	AddLocationRow($report, "SIP", "Stalled Information Phase", $left, $width1, $width2);
	AddLocationRow($report, "DP", "Decision Point", $left, $width1, $width2);
	AddLocationRow($report, "SDP", "Stalled Decision Point", $left, $width1, $width2);
	AddLocationRow($report, "RM", "Removed", $left, $width1, $width2);
	AddLocationRow($report, "NA", "Unknown Location", $left, $width1, $width2);

}
*/

function GetLoc($idLoc)
{
	switch($idLoc)
	{
		case 1:
			return "FM";
		case 2:
			return "IP";
		case 3:
			return "SIP";
		case 4:
			return "DP";
		case 5:
			return "SDP";
		case 6:
			return "CL";
		case 9:
			return "RM";
	}
	return "$idLoc";
}

/*
function BeginTable()
{
//	print("<table border=0 cellspacing=0 cellpadding=10 bgcolor='ivory'>");
print("<table border=0 cellspacing=2 cellpadding=5 bgcolor='ivory'>");
}


function EndTable()
{
	print("</table>");
}


function BeginTableRow()
{
	print("<tr>");
}

function BeginTableRowData($Row)
{
	if ($Row%2)
		print("<tr bgcolor='lightgrey'>");
	else
		print("<tr bgcolor='lightblue'>");
}

function EndTableRow()
{
	print("</tr>");
}
*/


function GetSubordinateList($idSuper, &$arrSalesmen, $bTopOnly)
{
	$strSubs="";
	$strSupers=$idSuper;
	$bFirst=TRUE;
	while(1)
	{
		if(!strlen($strSupers)) break;
		$sql="select * from people where SupervisorID in ($strSupers)";
		$strSupers="";
		$res=mssql_query($sql);
		if(!$res||(!$bFirst&&$bTopOnly)) break;
		while($row=mssql_fetch_assoc($res))
		{
			$strTst=trim($row['GroupName']);
			if(strlen($strTst)>0)
				$SalesName=$strTst;
			else
				$SalesName=$row["LastName"] . ", " . $row["FirstName"];
			$arrSalesmen[$row["PersonID"]]=$SalesName;
			if(strlen($strSupers)>0) $strSupers.=",";
			$strSupers.=$row["PersonID"];
		}
		$bFirst=FALSE;
		if(strlen($strSubs)>0&&strlen($strSupers)>0) $strSubs.=",";
		$strSubs.=$strSupers;
	}
	return $strSubs;
}



?>