<?php
 /**
 * @package Reports
 */
include('_report_utils.php');
include('TableUtils.php');
include('jim_utils.php');

define("TRUE",1);
define("FALSE",0);
$cellfontsize="16px";
// if summary == true then entire tree gets rolled up beneath each col
// summary is always true!!!!!

if (isset($summary) && $summary == 1)
{
	$SELF_URL_EXTRA = '?summary=1';
	$summary = true;
	begin_report('Average Sales Cycle Summary Report');
	$who = MANAGEMENT;
	//$who=SALESPEOPLE;
}
else
{
	$summary = false;
	begin_report('Average Sales Cycles Report');
	$who = MANAGEMENT;
//	$who = SALESPEOPLE;
}


// treeid will be set to the ID of a person to base data on
if (!isset($treeid))
{
	print_tree_prompt($who);
	end_report();
	exit();
}

?>

<script language="JavaScript">

var old_print = window.print;

function myPrint()
{
	alert("Please make sure your printer is in landscape mode for this report.");
	old_print();
}

window.print = myPrint;

</script>

<?php


print_tree_path($treeid);

$result = mssql_query("select Level from people where PersonID='$treeid'");
$row = mssql_fetch_array($result);
$level = $row['Level'];

//AvgCycles_OneLevel(1, 2, 74);
//AvgCycles_Summary(1, 2, 74);

if ($summary)
	AvgCycles_Summary($mpower_companyid, $treeid);
else
	AvgCycles_OneLevel($mpower_companyid, $treeid);

end_report();



function AvgCycles_OneLevel($CompanyID, $SupervisorID)
{
	AvgCycles($CompanyID, $SupervisorID, TRUE);
}

function AvgCycles_Summary($CompanyID, $SupervisorID)
{
	AvgCycles($CompanyID, $SupervisorID, FALSE);
}

function AvgCycles($CompanyID, $SupervisorID, $bTopOnly)
{
//Set of cols per print
//	$inc=8;
	$inc=10;  //7/20/2006

	$arrPeople=array(); //will contain level, groupname, PersonID
	$arrTopLevel=array();
	$arrProducts=array();
	GetProducts($CompanyID, $arrProducts);
	GetSubordinates($CompanyID, $SupervisorID, $arrPeople, $bTopOnly);
    //Highest level is at the top of arrPeople
	$Level=$arrPeople[0]["Level"];

	for($i=0;$i<count($arrPeople);$i++)
	{
		if ($arrPeople[$i]["Level"] < $Level) break;

		$id=$arrPeople[$i]["PersonID"];
		$strIn="In (";
//		if($arrPeople[$i]["Level"]==1) $strIn.=$id;

		if($bTopOnly==FALSE&&$arrPeople[$i]["Level"]>1)
		{
			$tmp=array();
			GetTree($id, $arrPeople, $tmp, bTopOnly);
			if(!count($tmp)) continue;
			for($j=0;$j<count($tmp);$j++)
			{
				if($j) $strIn.=",";
				$strIn.=$tmp[$j]["PersonID"];
			}
		}
		else $strIn.=$id;
		$strIn.=")";
		if($Level>1&&strlen(trim($arrPeople[$i]["GroupName"]))>0)
			$arrTopLevel[$arrPeople[$i]["GroupName"]]=$strIn;
		else
			$arrTopLevel[$arrPeople[$i]["Name"]]=$strIn;
	}
	if (!count($arrTopLevel))
	{
;		print "No data!";
		return;
	}
	ksort($arrTopLevel);

	$arrColHeads = array();
	$arrColHeads = array_keys($arrTopLevel);
	//array_unshift($arrColHeads,"");
	array_push($arrColHeads," <b>Averages");
	$ProductCount = 0; /*count($arrProducts)*/;


	$ProdSums = array_fill(0, count($arrTopLevel), 0);
	$totcols = count($arrColHeads);

    //Don't leave averages column hanging out there on their own new report section
	if ($inc==$totcols-1) $inc--;

	$rowtotals = array_fill(0, count($arrProducts), 0);
	$rowcounts = array_fill(0, count($arrProducts), 0);

	for ($start = 0, $stop = $inc; $start < $totcols; $start += $inc, $stop += $inc)
	{
		$arrCount = array();
		for($i=0; $i<$inc; $i++) $arrCount[$i] = 0;
		if($stop > $totcols)
			$stop = $totcols;
		BeginTable();
		if(!$bTopOnly) PrintColHeads($arrColHeads, 1, $start, $stop, TRUE);
		reset($arrProducts);
        $ind = $start;
		while(1)
		{
			$ID = current($arrProducts);
			$Name = key($arrProducts);
			$ok = PrintProductRepRow($Name, $ID, $arrTopLevel, $ProdSums, $start, $stop, $rowtotals[$ind], $arrCount, $rowcounts[$ind++], $bTopOnly);
			if($ok) $ProductCount++;
			if(!next($arrProducts))
				break;
		}
		BeginTableRowData(2);
		PrintTableCell(" <b>Averages");
		//Don't print the average of averages!
		if($stop > count($ProdSums))
			$stop = count($ProdSums);

		for($ind=$start; $ind < $stop; $ind++)
		{
			$avg = 0;
			if($arrCount[$ind]>0) $avg = $ProdSums[$ind] / $arrCount[$ind];
			PrintTableCell(sprintf("%d", $avg+.5));
			//PrintTableCell((int)($avg + .5));
		}
		EndTableRow();
		EndTable();
		print("<br><br>");
	}
}

//arrCount accumulates counts of products per salesman
function PrintProductRepRow($ProductName, $ProductID, &$SalesArray, &$ProdSums, $start, $stop, &$rowtotal, &$arrCount, &$rowcount, $bTopOnly)
{
	BeginTableRowData(2);

	PrintTableCell($ProductName);
	$count = count($SalesArray);
	if($stop > $count)
		$stop = $count;
	$keys = array_keys($SalesArray);
	for($i = $start; $i < $stop; ++$i)
	{
		$countOpps = 0;
		$ThisCell = PrintSalesmanProductCell($ProductID, $SalesArray[$keys[$i]], $countOpps);
		if($ThisCell > -1)
		{
			$arrCount[$i] = $arrCount[$i] + $countOpps;
 			$rowtotal = $rowtotal + $ThisCell;
// 			$rowcount++;
 			$rowcount+=$countOpps;

 			$ProdSums[$i] += $ThisCell;
 		}
	}
//	$denom = count($SalesArray);
	$denom = $rowcount;
	if($denom > 0)
	{
		$rowavg = $rowtotal/$denom;
	}
	//$ProdSums[$denom] += $rowavg;

	if($stop == $count && $bTopOnly==FALSE) {
		//PrintTableCell($rowavg);
		if (isset($rowavg)) {
			PrintTableCell(sprintf("%d", $rowavg+.5));  //Jim 7/30/03
		} else {
			PrintTableCell('-');
		}
	}

	EndTableRow();
}


//This function returns a raw count--for calcing an avg of the row total--but prints an average.
function PrintSalesmanProductCell($ProductID, $SalesID, &$Count)
{
	if(SalesmanSellsProduct($ProductID, $SalesID, $nDays, $AvgDays, $ct))
	{
		PrintTableCell("$AvgDays");
		$NumDays = $nDays;
		$Count = $ct;
		return ($NumDays);
	}
	else
	{
		PrintTableCell("--");
		return -1;
	}
}

function SalesmanSellsProduct($ProductID, $SalesID, &$NumDays, &$AvgDays, &$Count)
{
	$sumCycleDays=0;
	$avgCycleDays=0;
	$countCycleDays=0;
	IsCycleComplete($SalesID, $ProductID, $sumCycleDays, $countCycleDays);
	if($sumCycleDays)
	{
		$NumDays = (int)$sumCycleDays;
		$Count = $countCycleDays;
		$raw = ($sumCycleDays/$Count);
		$AvgDays = (int)($raw + .5);
		return true;
	}
	return false;
}

function GetSalesName($ID)
{
	$Sql="select * from people where PersonID=$ID";
	$res=mssql_query($Sql);
	if($row=mssql_fetch_assoc($res))
	{
		if($row["Level"]>1&&strlen(trim($row["GroupName"])))
			return $row["GroupName"];
        // return $row["LastName"] . ", " . $row["FirstName"];
		return $row["LastName"] . "<br>" . $row["FirstName"];

	}
	else
		return "Unknown";

}

function GetSubordinates($CompanyID, $SupervisorID, &$arrResults, $bTopOnly)
{
	$Sql = "select * from people where CompanyID=$CompanyID and Deleted=0 order by SupervisorID";
	$res = mssql_query($Sql);
	$arrGross = array();
	while($row = mssql_fetch_assoc($res))
		array_push($arrGross, $row);
	GetTree($SupervisorID, $arrGross, $arrResults, $bTopOnly);
}

function GetTree($SupervisorID, $arrIn, &$arrOut, $bTopOnly)
{
	$arrOut = array();

	$super_data = mssql_fetch_assoc(mssql_query("SELECT * FROM people WHERE PersonID = $SupervisorID"));

	$tmpSupers=array();

	array_push($tmpSupers, $SupervisorID);
	while(1)
	{
		$tmpPersons=array();
		for($i=0; $i<count($arrIn); $i++)
		{
			for($j=0; $j<count($tmpSupers); $j++)
			{
				if($arrIn[$i]["SupervisorID"] == $tmpSupers[$j])
				{
					array_push($tmpPersons, $arrIn[$i]["PersonID"]);
					$tmp=array();
					if (strlen(trim($arrIn[$i]["GroupName"]))>0)
						$tmp["GroupName"]=" <b>" .  $arrIn[$i]["GroupName"];
					else
						$tmp["GroupName"]="";
					$tmp["Name"] = " <b>".$arrIn[$i]["LastName"]."<br>".$arrIn[$i]["FirstName"];

					$tmp["SupervisorID"]=$arrIn[$i]["SupervisorID"];
					$tmp["PersonID"]=$arrIn[$i]["PersonID"];
					$tmp["Level"]=$arrIn[$i]["Level"];
					array_push($arrOut, $tmp);
				}
			}
		}
		if($bTopOnly==TRUE)
			break;
		if(!count($tmpPersons))
			break;
		$tmpSupers=$tmpPersons;
	}

}

function IsCycleComplete($SalesID, $ProductID, &$sumCycles, &$countCycles)
{
	$avgCycles = null;
	$sumCycles = null;
	$countCycles = null;
	$sql = "select PersonID from Opportunities where ProductID=$ProductID and PersonID $SalesID and (Category = 6 or category = 9) and ActualCloseDate is not null";
	if ($sel = mssql_query($sql)) {
		$SalesList_array = array();
		while($row = mssql_fetch_assoc($sel)) {
			$SalesList_array[] = $row["PersonID"];
			// if (strlen($SalesList)) {
			//	$SalesList.=",";
			// }
			//$SalesList.=$row["PersonID"];
		}
		$SalesList = implode(',', $SalesList_array);
	}
	if(!isset($SalesList) || !strlen($SalesList)) return;

	$sql = "select sum(SalesCycleLength) as CSum from SalesCycles where ProductID=$ProductID and PersonID ";
	$sql .= "in ($SalesList)";
	$sql .= " group by ProductID";
    //print "--$sql--";

	$selection = mssql_query($sql);
	if($selection)
	{
		if($row = mssql_fetch_assoc($selection))
		{
			$CycleSum = $row["CSum"];
			if($CycleSum)
				$sumCycles = $CycleSum;
		}
	}
	$sql = "select count(SalesCycleLength) as CCount from SalesCycles where ProductID=$ProductID and PersonID ";
	$sql .= "in ($SalesList)";
	$sql .= " group by ProductID";
    //print "--$sql--";

	$selection = mssql_query($sql);
	if($selection)
	{
		if($row = mssql_fetch_assoc($selection))
		{
			$CycleCount = $row["CCount"];
			if($CycleCount)
				$countCycles = $CycleCount;
		}
	}


//	return null;
}

function GetProducts($CompanyID, &$arrProducts)
{
	$sql= "select ProductID, Name from products where CompanyID=$CompanyID and deleted=0";
	$result=mssql_query($sql);
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
			$arrProducts[" <b>" . $row['Name']]=$row['ProductID'];
	}
	ksort($arrProducts);
}


?>
