<?php
 /**
 * @package Reports
 */

// constant definitions
define("STATUS_FIRST_MTG", 1);
define("STATUS_IP_LIVE", 2);
define("STATUS_STALLED", 3);
define("STATUS_LIVE", 4);
define("STATUS_DP_STALLED", 5);
define("STATUS_CLOSED", 6);

// which_quarter: returns the quarter (1 - 4) for a date string
// that begins with a month abbreviation, e.g. 'Apr 18 1776'
//
// WARNING: This does not take locales into account.  This will
// need some i18n work if non-English-speakers will be using
// this application.
function which_quarter($dt)
{
$quarters =  array(
	1 => array('', 'Jan', 'Feb', 'Mar'),
	2 => array('', 'Apr', 'May', 'Jun'),
	3 => array('', 'Jul', 'Aug', 'Sep'),
	4 => array('', 'Oct', 'Nov', 'Dec')
	);

$mo = substr($dt, 0, 3);

for($i=1; $i<=4; $i++)
	{
	$r = array_search($mo, $quarters[$i]);
	if($r > 0)
		{
		return $i;
		}
	}
return 0;
}

// current_quarter: find out the quarter in which today's date falls
function current_quarter()
{
$now = strftime("%b");	// we don't care about the day or year here
return which_quarter($now);
}

// extract the year from a string, e.g. 'Apr 18 1776 12:00:00'
function which_year($dt)
{
$ts = strtotime($dt);
$year = strftime("%Y", $ts);

return $year;
}

// integer division that avoids rounding errors
function div($a,$b)
{
return ($a-($a % $b))/$b;
}

// day_interval - get number of days between two timestamps
// Input: Two strings of the form "YYYY-MM-DD hh:mm:ss"
// Output is always positive; the dates can be sent to this
// function in either order.
function day_interval($date1, $date2)
{
// 86400 seconds = 24 hours
return(abs(div(strtotime($date1) - strtotime($date2), 86400)));
}

// sql_select_table - Generate HTML table from SQL query
//
// This table generator gets its column headings directly from the row
// fetched by mssql_fetch_assoc(), rather than delving into the system
// tables to get them.
//
// NULL values get shown as gray.
//
// $sql
//		The query; use column aliases if you want to specify your
//		own column headings in your output, e.g.:
//		"select LastName + ', ' + FirstName as Name ..."
// $table_opts
//		HTML options for the <TABLE> tag, e.g. "BORDER=1"
function sql_select_table($sql, $table_opts)
{
	$first = TRUE;
	$result = mssql_query($sql);
	print("<table $table_opts>\n");
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			if($first)
			{
				print("<tr>");
				$first = FALSE;
				$cols = array_keys($row);
				foreach($cols as $colname)
					{
					print("<th>$colname</th>");
					}
				print("</tr>\n");
			}
			print("<tr>");

			foreach($cols as $colname)
			{
				$out = $row[$colname];
				if(is_null($out))
					{ print "<td bgcolor=\"gray\">&nbsp;</td>"; }
				else
					{ print("<td>$out</td>"); }
			}
			print("</tr>\n");
		}
	}
	print("</table>\n");
}

// get_user_data
//
// Returns user data for a given userid/company id:
//
function get_user_data($user, $co)
{
/*
$sql	= "SELECT * FROM people WHERE UserID = '$user' "
	. "AND CompanyID = $co;";
*/

$sql =  "SELECT * FROM";
$sql .= " Logins INNER JOIN people";
$sql .= " ON Logins.PersonID = people.PersonID";
$sql .= " WHERE (Logins.UserID = '$user') AND (Logins.CompanyID = $co);";

$result = mssql_query($sql);
if($result)
	{
	$row = mssql_fetch_assoc($result);
	return($row);
	}
else
	{
	return(array(NULL));
	}
}

// get_level_name
//
// Returns the name of a level given a level number and company ID
function get_level_name($level, $co)
{
$sql	= "SELECT Name FROM levels WHERE LevelNumber = $level "
	. "AND CompanyID = $co;";
$result = mssql_query($sql);
if($result)
	{
	$row = mssql_fetch_row($result);
	$ret = $row[0];
	}
else
	{
	$ret = 0;
	}
return($ret);
}

function get_grace_period($co)
{
$sql	= "SELECT NonUseGracePeriod FROM company WHERE CompanyID = $co;";
$result = mssql_query($sql);
if($result)
	{
	$row = mssql_fetch_row($result);
	$ret = $row[0];
	}
else
	{
	$ret = 0;
	}
return($ret);
}

function table_headings($list)
{
print "<tr>";
foreach($list as $item)
	{
	print "<th>" . $item . "</th>";
	}
print "</tr>\n";
}

function table_row($list)
{
$count = 0;
$align = "align=\"left\"";
print "<tr>";
foreach($list as $item)
	{
	// note the triple =
	// it is necessary in order to distinguish 0 from a null string.


	if($item === "")
		{
		print '<td bgcolor="lightgrey">&nbsp;</td>';
		}
	else
		{
		print "<td $align bgcolor=\"lightblue\">" . $item . '</td>';
		}
	$count++;
	if($count == 2) $align = "align=\"right\"";
	}
print "</tr>\n";
}

// Get a POSIX timezone name from the "timezones"
// database table, given a zone ID.
function get_timezone_name($tz_id)
{
$sql	= "SELECT zname FROM timezones WHERE zid = $tz_id;";
$result = mssql_query($sql);
if($result)
	{
	$row = mssql_fetch_row($result);
	$ret = $row[0];
	}
else
	{
	$ret = "";	// return nothing for now, when done return GMT as a failsafe
	}
return($ret);
}

// Convert a timestamp retrieved from SQL Server in GMT,
// and convert it to the time zone currently set in the
// TZ environment variable.  It also removes the leading
// zero from the hour if it's less than 10.

// WARNING: the input timestamp MUST be GMT!
function timestamp_convert_tz($gmt)
{
$currzone = getenv("TZ");
putenv("TZ=GMT");	// strtotime depends on TZ, so we need to use GMT temporarily
$unixtime = strtotime($gmt);
putenv("TZ=$currzone");
// Using "g" instead of "h" strips leading zeroes.
//return(date("M d Y g:iA", $unixtime));
return(date("M d Y", $unixtime));
}

?>
