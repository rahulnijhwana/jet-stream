<?php

class Row {

	public $_row = array();
	
	public function __construct($arr){
		$this->_row = $arr[0];
	}
	
	public function _getRow(){
		return $this->_row;
	}
	
	public function _toString(){
		echo '<pre>';
		print_r($this->_row);
		echo '</pre>';
	}
}