<?php

class DataUtil {

	public $_table = "";
	public $_databaseField = "";
	public $_columnHeader = "";
	
	public function __construct($arr){
		$this->_table = $arr[0];
		$this->_databaseField = $arr[1];
		$this->_columnHeader = $arr[2];
	}
	
	public function toString($str){
		echo '<pre>';
		echo 'Database Table: ' . $this->_table . '<br>';
		echo 'Database Field: ' . $this->_databaseField . '<br>';
		echo 'Source Header: ' . $this->_columnHeader .'<br>';
		echo '</pre>';
	}
}