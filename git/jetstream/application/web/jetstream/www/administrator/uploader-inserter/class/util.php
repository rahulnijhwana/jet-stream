<?php

class util {
	static public function upload_file($filename, $dest, $valid_types){
		$errors = array();
		
		if(!array_key_exists($filename, $_FILES)){
			$errors[] = 'Upload failed. This is usually caused by a break in communication with the server or by a corrupt file. Please try again, or try a different file. If the file appears to be damaged, try opening it and resaving it in another application.';
		}
		
		if(empty($errors)){
			if ($_FILES[$filename]['error'] != 0){
				$errors[] = 'Upload file error. This is usually caused by not selecting a file, uploading a corrupt file or by a break in communication with the server.  Please try again, or try a different file.  If the file appears to be damaged, try opening it and resaving it in another application.';
			}
		}
		
		
		if(empty($errors)){
			if ( !is_uploaded_file($_FILES[$filename]["tmp_name"]) ){
				$errors[] = 'Invalid upload. This is usually caused by problems with the system or somebody attempting to misuse our system.  If you are using the system normally and get this error, please contact us and we will assist.';
			}
		}
		
		if(empty($errors)){
			$format_found = false;
			# oddly in_array() would not process this correctly
			foreach($valid_types as $vaild_type){
				if(trim($_FILES[$filename]["type"]) == trim($vaild_type)){
					$format_found = true;
				}
			}
			
			
			if(!$format_found){
				$an_error =  $_FILES[$filename]["type"] . ' is an invalid file format. <br /> Valid formats:';
				foreach($valid_types as $valid_format){
					$an_error .= '<br />' . $valid_format;
				}
				
				$errors[] = $an_error;
			}
		}
		
		if(empty($errors)){
			move_uploaded_file($_FILES[$filename]['tmp_name'], $dest);
			@chmod($dest, 0777);
		}
		
		return $errors;
	}

	static public function parse_csv_file_data($path){
		$output = array();
		$header = array();
		
		if(file_exists($path)){
			$buffer = file_get_contents($path);
			$buffer = str_replace('"', "", $buffer);
			
			# break out rows
			$rows_buffer = preg_split('/[\n\r]+/', $buffer);
			
			# foreach row, break up columns
			foreach($rows_buffer as $a_row){
				# ignore empty rows
				$test_row = str_replace(",", "", $a_row);
				if($test_row != ''){
					
					# load headers
					if(empty($header)){
						$buff_header = explode(",", $a_row);
						foreach($buff_header as $buff_col){
							if($buff_col != ''){
								$header[] = $buff_col;
							}
						}
					}else{
						# load data
						$buff_row = explode(",", $a_row);
						$temprow = array();
						foreach($buff_row as $key => $buff_col){
							if($buff_col != ''){
								$temprow[$header[$key]] = $buff_col;
							}
						}
						$output[] = $temprow;
					}
				}
			}
		}
		
		return $output;
	}
	
	static public function parse_csv_file_headers($path){
		$headers = array();
		
		if(file_exists($path)){
			# load file and remove quotes
			$buffer = file_get_contents($path);
			$buffer = str_replace('"', "", $buffer);
			
			# break out rows
			$rows_buffer = preg_split('/[\n\r]+/', $buffer);
			
			# foreach row, break up columns
			foreach($rows_buffer as $a_row){
				# ignore empty rows
				$test_row = str_replace(",", "", $a_row);
				if($test_row != ''){
					# break up row
					$buffer_headers = explode(",", $a_row);
					
					foreach($buffer_headers as $buff_head){
						# only accept non null cells
						if($buff_head != ''){
							$headers[] = $buff_head;
						}
					}
					break;
					
				}
			}
		}
		
		
		return $headers;
	}
}

?>
