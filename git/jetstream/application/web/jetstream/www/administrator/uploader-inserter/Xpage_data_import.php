<?php
set_time_limit(0);

require_once 'C:\mpower\slipstream\trunk\www\administrator\uploader-inserter\Xsettings.php';
require_once 'C:\mpower\slipstream\trunk\www\administrator\uploader-inserter\class\util.php';
require_once 'C:\mpower\slipstream\trunk\www\administrator\uploader-inserter\class\msdb.php';
require_once 'C:\mpower\slipstream\trunk\www\administrator\uploader-inserter\class\import_exception.php';
require_once 'C:\mpower\slipstream\trunk\www\administrator\uploader-inserter\class\table_loader.php';
require_once 'C:\mpower\slipstream\trunk\www\administrator\uploader-inserter\class\DataUtil.php';
require_once 'C:\mpower\slipstream\trunk\www\administrator\uploader-inserter\class\Row.php';
$connfail = false;

# GMS 010311 Two Destination Table Test
$contact_map_table = 'ContactMap';
$company_table = 'company';
$contact_table = 'Contact';
$my_companyid = $_SESSION['company_id'];
# require_once BASE_PATH . '/uploader-inserter/table_loader.php');';

# display variables
$display_form = false;
$headers = array();
$errors = array();

# unique file name to save upload as
if(isset($_POST['unique'])){
	$unique_nm = $_POST['unique'];
}else{
	$unique_nm = microtime(false);
	$unique_nm = str_replace(" ", "", $unique_nm);
	$unique_nm = str_replace(".", "", $unique_nm);
}

# path to save file as
$dest = dirname(__FILE__) . '\\'. FILE_UPLOAD_DIR . '\\' . $unique_nm;

## load spreadsheet into database table via form translation
if(isset($_POST['savetotable'])){

	$db = new table_loader();

		$contact_fieldnames_labels = $db->get_contact_fieldnames_labels($my_companyid);	
			
		$company_fieldnames = $db->get_company_fieldnames($company_table);	
		
	# gather data from csv file
	$data = util::parse_csv_file_data($dest);
	
	$i=0;
	$_values = array();
		
	foreach ($data as $arr){		
//		print_r($arr);		
		$row = new Row($arr);
		$row->_toString();
	}
	
	$translate1 = array();
	$translate2 = array();
	$translate3 = array();

	$_values = array();
	
	/**
	 * Remove unwanted items from POST
	 */
 	foreach($_POST as $k => $v){
		if(is_numeric($k)){
			array_push($_values, $v);
 		}
	}

	$_dataObjects = array();
	
	$_companies = array();
	$_contacts = array();
	
	foreach ($_values as $_value){
	
		$_split = explode(':', $_value);		
		$dataUtil = new DataUtil($_split);

		array_push($_dataObjects, $dataUtil);	
	}


}elseif(isset($_POST['tableupload'])){
	## setup to display form to translate spreadsheet to database table
	# validate and upload file
	$errors = util::upload_file('table', $dest, array("text/csv", 
								"text/comma-separated-values", 
								"application/csv", 
								"application/excel", 
								"application/vnd.ms-excel", 
								"application/vnd.msexcel", 
								"text/anytext"));
								
	if(!empty($errors)){
		$display_form = true;
	}else{
		# gather csv headers
		$headers = util::parse_csv_file_headers($dest);
		
//	print_r("<pre>");print_r($_POST); print_r("</pre>");
	
		global $my_companyid;

		$db = new table_loader();	
		$contact_fieldnames_labels = $db->get_contact_fieldnames_labels($my_companyid);	
		
	# company_columns are direct table column labels ("contact")
		$company_fieldnames = $db->get_company_fieldnames($company_table);					
		
######################################################
###    Second Page Form		
######################################################
?>

	<form action="http://localhost/jetstream/administrator/uploader-inserter/Xpage_data_import.php" method="post">
		<input type="hidden" id="unique" name="unique" value="<?php echo $unique_nm; ?>" />
		<input type="hidden" id="savetotable" name="savetotable" value="1" />
		<input type="hidden" id="companyid" name="companyid" value="<?php echo $companyid; ?>" />
		<?php $i=0; ?>
		<?php foreach($headers as $h_name){ ?>
			<div>
				<div style="float: left; width: 300px;"; >
					<label id="label<?php echo $i; ?>"><?php echo $h_name; ?></label>
				</div>
				<div style="float: left; width: 400px;">
					<select id="<?php echo $i; ?>" name="<?php echo $i; ?>">
						<option value=""></option>
						<?php foreach($contact_fieldnames_labels as $c_name){ ?>
							<option value="Contact:<?php echo trim($c_name['FieldName']); ?>:<?php echo $h_name; ?>">
								Contact - <?php echo trim($c_name['LabelName']); ?>
							</option>
						<?php } ?>
						<?php foreach($company_fieldnames as $f_name){ ?>
							<option value="Company:<?php echo trim($f_name); ?>:<?php echo $h_name; ?>">
								Company - <?php echo trim($f_name); ?>
							</option>
						<?php } ?>
					</select>
				</div>
				<div style="clear: both;"></div>
			</div>
		<?php
		$i++;
		}
		?>

		<input type="submit" value="Save <?php echo trim($contact_map_table); ?> and <?php echo trim($company_table); ?> to Database?" />
	</form>

	<?php
	}

}else{
	$display_form = true;
}

######################################################
###    First Page Form		
######################################################

if($display_form){
	if(!empty($errors)){
		foreach($errors as $an_error){
?>
	
	<ul>
		<li><?php echo $an_error; ?></li>
	</ul>
	<?php } 
	}
	?>
<div class="module_container" id="divContactList">
	<p>

	<form action="http://localhost/jetstream/administrator/uploader-inserter/Xpage_data_import.php" method="post" enctype="multipart/form-data">
		<input type="hidden" id="tableupload" name="tableupload" value="1" />
				<input type="hidden" id="companyid" name="companyid" value="<?php echo $my_companyid; ?>" />
		<input type="file" id="table" name="table" />
		<input type="submit" id="enter" value="Upload CSV File with ONE Row of Labels, NO Empty Columns:" />
	</form>
<?php                                                                                                     
}
?>
