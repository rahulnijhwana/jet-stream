<?php
class msdb {
	
	private $connection;
	private $response;
	
	protected $data;
	
	public function __construct(){
		$this->connect();
	}
	
	private function connect(){
		$this->connection = @mssql_connect(SQL_SERVER, SQL_USER, SQL_PWD) or throw_exception('Cannot connect to database');
		if($this->connection) {
			@mssql_select_db(SQL_DATABASE, $this->connection) or throw_exception('Cannot select database');
		}
	}
	
	protected function call($query){
		$this->data = array();
		$this->response = mssql_query($query)  or throw_exception('Cannot query database');; 
		while($row = mssql_fetch_array($this->response)){
			$this->data[] = $row;
		}
	}
	
	protected function last_insert_id() { 
		$this->call("SELECT @@identity AS id");
		if(isset($this->data[0]['id'])){
			return $this->data[0]['id'];
		}else{
			return 0;
		}
	}
	
	protected function insert($query){
		$this->call($query);
		return $this->last_insert_id();
	}
	
}
