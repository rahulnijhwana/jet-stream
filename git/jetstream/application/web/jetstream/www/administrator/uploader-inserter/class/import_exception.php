<?php

class import_exception extends Exception{
	public function __construct($message){
		parent::__construct($message);
		if(EXCEPTION_SEND_MAIL){
			mail(EXCEPTION_EMAIL, 'An Exception Has Occured', $message);
		}
		
	}
}
?>
