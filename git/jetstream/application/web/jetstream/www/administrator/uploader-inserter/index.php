<?php

set_time_limit(0);

# require_once($_SERVER['DOCUMENT_ROOT'] . '/settings.php');
require_once('settings.php');
# display variables
$display_form = false;
$headers = array();
$errors = array();

# unique file name to save upload as
if(isset($_POST['unique'])){
	$unique_nm = $_POST['unique'];
}else{
	$unique_nm = microtime(false);
	$unique_nm = str_replace(" ", "", $unique_nm);
	$unique_nm = str_replace(".", "", $unique_nm);
}

# path to save file as
$dest = FILE_UPLOAD_DIR . '/' . $unique_nm;


## load spreadsheet into database table via form translation
if(isset($_POST['savetotable'])){

	$db = new table_loader();

	$columns = $db->get_table_columns(TABLE_NAME);
#	$columns = array('id', 'name', 'quantity', 'other');

	# gather data from csv file
	$data = util::parse_csv_file_data($dest);
	$translate = array();
	
	# gather translation from form
	foreach($columns as $table_column){
		if(isset($_POST[$table_column]) && $_POST[$table_column] != ''){
			$translate[$_POST[$table_column]] = $table_column;
		}
	}

	# save data to database
	$total_inserted = $db->save_datasets(TABLE_NAME, $data, $translate);

	echo 'You have just uploaded ' . $total_inserted . ' records';
	exit;

}elseif(isset($_POST['tableupload'])){
	## setup to display form to translate spreadsheet to database table
	
	
	# validate and upload file
	$errors = util::upload_file('table', $dest, array("text/csv", 
								"text/comma-separated-values", 
								"application/csv", 
								"application/excel", 
								"application/vnd.ms-excel", 
								"application/vnd.msexcel", 
								"text/anytext"));
								
	if(!empty($errors)){
		$display_form = true;
	}else{
		# gather csv headers
		$headers = util::parse_csv_file_headers($dest);
	
		# gather table columns
		$db = new table_loader();
		$columns = $db->get_table_columns(TABLE_NAME);
#		$columns = array('id', 'name', 'quantity', 'other');


######################################################
###    Second Page Form		
######################################################
?>

	<form action="index.php" method="post">
		<input type="hidden" id="unique" name="unique" value="<?php echo $unique_nm; ?>" />
		<input type="hidden" id="savetotable" name="savetotable" value="1" />
		<?php foreach($columns as $c_name){ ?>
			<div>
				<div style="float: left; width: 300px;"; >
					<?php echo $c_name; ?>
				</div>
				<div style="float: left; width: 400px;">
					<select id="<?php echo $c_name; ?>" name="<?php echo $c_name; ?>">
						<option value=""></option>
						<?php foreach($headers as $h_name){ ?>
							<option value="<?php echo trim($h_name); ?>">
								<?php echo trim($h_name); ?>
							</option>
						<?php } ?>
					</select>
				</div>
				<div style="clear: both;"></div>
			</div>
		<?php } ?>
		<input type="submit" value="Save to Database" />
	</form>

	<?php
		
		
	}
	
}else{
	$display_form = true;
	
}

######################################################
###    First Page Form		
######################################################


if($display_form){
	if(!empty($errors)){
		foreach($errors as $an_error){
?>
	
	<ul>
		<li><?php echo $an_error; ?></li>
	</ul>
	<?php } 
	}
	?>

	<form action="index.php" method="post" enctype="multipart/form-data">
		<input type="hidden" id="tableupload" name="tableupload" value="1" />
		<input type="file" id="table" name="table" />
		<input type="submit" id="enter" value="Upload" />
	</form>
<?php 
}
?>
