<?php

class table_loader extends msdb {
	
	public function __construct(){
		parent::__construct();
	}
	
	# gather database columns from specified table
	public function get_table_columns($table_name){
		$output = array();
		$gather = 'exec sp_columns ' . $table_name;
		$this->call($gather);
		foreach($this->data as $row){
			$output[] = $row['COLUMN_NAME'];
		}
		
		return $output;
		
	}
	
	public function save_datasets($table_name, $data, $translate){
		$count = 0;
		foreach($data as $row){
			$query = "";
			$query_columns = array();
			$query_values = array();

			foreach($row as $sheet_header => $sheet_value){
				if($sheet_value != '' && $translate[$sheet_header] != ''){
					$query_columns[] = trim($translate[$sheet_header]);
					$query_values[] = "'" . trim(addslashes($sheet_value)) . "'";
				}
			}
				
			$query = "INSERT INTO " . $table_name . " (" . implode(", ", $query_columns) . ") VALUES(" . implode(", ", $query_values) . ")";
			$this->insert($query);
			$count++;
		}
	
		
        return $count;
	}

}

?>
