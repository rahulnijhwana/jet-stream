<?php
# show error at all
define('DISPLAY_ERROR', TRUE);
# show detailed error
define('DUMP_ERROR', TRUE);

# path to store uploaded file
define('FILE_UPLOAD_DIR','filestore');
# database info
define('TABLE_NAME', 'Verification');
define('SQL_SERVER', '75.101.239.183');
define('SQL_USER', 'mpasac');
define('SQL_PWD', 'qraal+zv');
define('SQL_DATABASE', 'mpasac');
# update ini settings for file uploads
ini_set('memory_limit', '200M');
ini_set('post_max_size', '200M');
ini_set('upload_max_filesize', '200M');

# email admin when exception occurs
define('EXCEPTION_EMAIL', '');
define('EXCEPTION_SEND_MAIL', FALSE);

# setup include paths
$paths = explode(':', ini_get('include_path'));
$paths[] = $_SERVER['DOCUMENT_ROOT'];
$paths[] = $_SERVER['DOCUMENT_ROOT'] . '/class';
ini_set('include_path', implode(':', $paths));
# setup error handling
set_error_handler("display_error");
function display_error($err_no, $err_mess, $err_file, $err_line, $err_context){
	if($err_no == E_USER_ERROR){
		if(DISPLAY_ERROR){
		
			echo $err_no . ' ' . $err_mess . ' ' . $err_file . ' ' . $err_line . '<br />';
		
			if(DUMP_ERROR){
				echo $err_context;
			}
		}
	}
}

# setup autoload to return an exception if class is not found
function __autoload($classname){
	if(!(@(include_once($classname . '.php')))){
		return eval('class ' . $classname . ' {public function __construct(){throw new import_exception(\'' . $classname . ' Is Missing\');}}');
	}
}

# function to allow ' or throw new' in line
function throw_exception($message){
	throw new import_exception($message);
}


?>
