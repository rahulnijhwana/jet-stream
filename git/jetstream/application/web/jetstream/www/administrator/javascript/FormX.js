function FormX_setValue(name, val)
{
	if (!this.myinputs[name])
	{
		var tempinput = document.createElement('input');
		tempinput.type = "text";
		tempinput.name = name;
		this.myinputs[name] = tempinput;
		this.appendChild(tempinput);
	}
	this.myinputs[name].value = val;
}

function FormX_getValue(name)
{
	if (!this.myinputs[name])
		return null;
	return this.myinputs[name].value;
}

function FormX()
{
	this.setValue = FormX_setValue;
	this.getValue = FormX_getValue;
	
	this.myinputs = new Object();
}