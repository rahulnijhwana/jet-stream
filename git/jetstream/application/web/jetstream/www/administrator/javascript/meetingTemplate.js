// JavaScript Document

function fetchSelectedFormFields(frmType){
	if(frmType.value == -1){
		cancelNoticeTempData();
		return false;
	}

		$.post('../../ajax/ajax.meetingNoticeTemplate.php',
			{
				frmType: frmType.value,
				action :'getFormFields',
				companyId : companyId
			},
			function(result)
			{
				try
				{
					result = eval(result);
				}
				catch(e)
				{
					result = JSON.decode(result);
				}	
				var detailArr = result['FieldList'];
				var tempData = '';
				if(result['Template'] != null)
					tempData = result['Template'];
				
				var sectionName = '';
				var selectField = '<select id="noticeTempFields" name="noticeTempFields" class="" style="width:auto;font-family:Verdana,Helvetica,Tahoma,Arial; font-size: 8.3pt;"><option value="-1"> </option>';
				for(i=0;i<detailArr.length;i++){
					if(detailArr[i]['SectionName'] == 'DefaultSection')
						sectionName = 'Main';
					else
						sectionName = detailArr[i]['SectionName'];
					
					var fieldsArr = new Array();
					fieldsArr = detailArr[i]['Fields'];
					selectField = selectField + '<optgroup label="'+sectionName+'">';
					for(j=0;j<fieldsArr.length;j++){
						selectField = selectField + '<option value="'+ sectionName+'.'+fieldsArr[j]['LabelName'] +'">'+fieldsArr[j]['LabelName']+'</option>';
					}
					selectField = selectField + '</optgroup>';
				}
					selectField = selectField + '</select>';
					
				document.getElementById('noticeTempSelectFieldTd').innerHTML = selectField;
				document.getElementById('templateData').value = tempData;
			}
		);
}

function insertAtCursor(myField, myValue)
{
	//IE support
	if (document.selection) {
		myField.focus();
		sel = document.selection.createRange();
		sel.text = myValue;
	}
	//MOZILLA/NETSCAPE support
	else if (myField.selectionStart || myField.selectionStart == '0') {
		var startPos = myField.selectionStart;
		var endPos = myField.selectionEnd;
		myField.value = myField.value.substring(0, startPos)
		+ myValue
		+ myField.value.substring(endPos, myField.value.length);
	} else {
		myField.value += myValue;
	}
}





function insertTemplateData(){
	if(document.getElementById('selectNoticeTemplateFormType').value == -1 || document.getElementById('noticeTempFields').value == -1)
		return false;
	
	var index = document.getElementById('noticeTempFields').selectedIndex;
	var textAreaData = document.getElementById('templateData').value;
	var newAreaData =  '{'+document.getElementById('noticeTempFields').options[index].value +' | No '+ document.getElementById('noticeTempFields').options[index].text+' !}';
	insertAtCursor(document.getElementById('templateData'), newAreaData);
	$('#templateData').focus();	
}

function saveNoticeTempData(){
	if(document.getElementById('selectNoticeTemplateFormType').value == -1)
		return false;
	var tempData = document.getElementById('templateData').value;
	var formType = '';
	if(document.getElementById('selectNoticeTemplateFormType').value == 'Account')
		formType = 1;
	else if(document.getElementById('selectNoticeTemplateFormType').value == 'Contact')
		formType = 2;
		
	$.post('../../ajax/ajax.meetingNoticeTemplate.php',
		{
			templateData: tempData,
			action :'saveNoticeTemplate',
			recType : formType,
			companyId : companyId
		},
		function(result)
		{
			try
			{
				result = eval(result);
			}
			catch(e)
			{
				result = JSON.decode(result);
			}
			if(result.STATUS=='OK')
			{
				alert("Template Saved.");
			}
		}
	);
}

function cancelNoticeTempData(){
	document.getElementById('selectNoticeTemplateFormType').value = -1;
	document.getElementById('noticeTempSelectFieldTd').innerHTML = '';
	document.getElementById('templateData').value = '';
}