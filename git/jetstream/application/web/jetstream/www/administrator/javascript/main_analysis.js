
Analysis = new Object();

Analysis.calcFor = _Analysis_calcFor;
Analysis.calcAll = _Analysis_calcAll;
Analysis.findAnalysis = _Analysis_findAnalysis;
Analysis.setField = _Analysis_setField;

// calc the analysis for all current people
function _Analysis_calcAll()
{
	for (var i = 0; i < g_people.length; ++i)
		Analysis.calcFor(g_people[i][g_people.PersonID]);
}

// calc the analysis for one person
function _Analysis_calcFor(personID)
{
	var person = find_person(personID);
	if (!person) return;

	// count opps in each category
	var fmTotal = 0;
	var ipTotal = 0;
	var dpTotal = 0;
	var cTotal = 0;
	var vCount = 0; //Count of opportunities with valuations
	var vTotal = 0;

	var now = new Date();
	var cutoff = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 30).getTime();
	for (var i = 0; i < g_opps.length; ++i)
	{
		var opp = g_opps[i];
		if (opp[g_opps.PersonID] != personID) continue;

		var cat = opp[g_opps.Category];
		if (cat == 1) ++fmTotal;
		else if (cat == 2) ++ipTotal;
		else if (cat == 4) ++dpTotal;
		else if (cat == 6)
		{
			var actualClose = opp[g_opps.ActualCloseDate];
			if (actualClose == '') continue;
			var actualCloseDate = new Date(Dates.mssql2us(actualClose)).getTime();
			if (isNaN(actualCloseDate)) continue;
			if (actualCloseDate >= cutoff) ++cTotal;
		}	
	}
	// get the salesperson's thresholds
	var fm2 = person[g_people.FMThreshold] * 0.66;
	var fm3 = person[g_people.FMThreshold] * 1.5;
	var ip2 = person[g_people.IPThreshold] * 0.66;
	var ip3 = person[g_people.IPThreshold] * 1.5;
	var dp2 = person[g_people.DPThreshold] * 0.66;
	var dp3 = person[g_people.DPThreshold] * 1.5;
	var c2 = person[g_people.CloseThreshold] * 0.66;
	var c3 = person[g_people.CloseThreshold] * 1.5;

	// figure out the curve
	var fmValue;
	var ipValue;
	var dpValue;
	var cValue;

	if (fmTotal == 0) fmValue = 0;
	else if (fmTotal < fm2) fmValue = 1;
	else if (fmTotal < fm3) fmValue = 2;
	else fmValue = 3;

	if (ipTotal == 0) ipValue = 0;
	else if (ipTotal < ip2) ipValue = 1;
	else if (ipTotal < ip3) ipValue = 2;
	else ipValue = 3;

	if (dpTotal == 0) dpValue = 0;
	else if (dpTotal < dp2) dpValue = 1;
	else if (dpTotal < dp3) dpValue = 2;
	else dpValue = 3;

	if (cTotal == 0) cValue = 0;
	else if (cTotal < c2) cValue = 1;
	else if (cTotal < c3) cValue = 2;
	else cValue = 3;

	// match that to an analysis id and rating
	var analysisID = -1;
	var rating = -1;
	var analysis = Analysis.findAnalysis(fmValue, ipValue, dpValue, cValue);
	if (analysis)
	{
		analysisID = analysis[g_analysisCurves.AnalysisID];
		var startDate = new Date(Dates.mssql2us(person[g_people.StartDate]));  //call to mssql2us is crucial   Jim 9/15/04
		var mNew = parseInt(g_company[0][g_company.MonthsConsideredNew], 10);
		var expDate = new Date(startDate.getFullYear(), startDate.getMonth() + mNew, startDate.getDate());

        //Unlikely curve?  Theoretically, rating in the curve is set to -1 if unlikely.  Nevertheless . . .
		if(analysis[g_analysisCurves.Reason] != '')
			rating = -1;
		else
		{
			if (expDate.getTime() > new Date().getTime()) rating = analysis[g_analysisCurves.RankNew];
			else rating = analysis[g_analysisCurves.RankExperienced];
		}
	}

	// store it in the person's record
	Analysis.setField(person, g_people.AnalysisID, analysisID);
	Analysis.setField(person, g_people.Rating, rating);
	Analysis.setField(person, g_people.FMValue, fmValue);
	Analysis.setField(person, g_people.IPValue, ipValue);
	Analysis.setField(person, g_people.DPValue, dpValue);
	Analysis.setField(person, g_people.CloseValue, cValue);
}

function _Analysis_findAnalysis(fm, ip, dp, c)
{
	for (var i = 0; i < g_analysisCurves.length; ++i)
	{
		var data = g_analysisCurves[i];
		if (data[g_analysisCurves.FMValue] != fm) continue;
		if (data[g_analysisCurves.IPValue] != ip) continue;
		if (data[g_analysisCurves.DPValue] != dp) continue;
		if (data[g_analysisCurves.CloseValue] != c) continue;
		return data;
	}

	return null;
}

function _Analysis_setField(person, field, value)
{
	var oldValue = person[field];
	if (oldValue != value) person.isDirty = true;
	person[field] = value;
}

