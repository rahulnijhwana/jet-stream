// JavaScript Document
var selectedUser = '';
function getUserCalendarPermission(companyid){
	if($('#usersSelect option:selected').val() == -1){
		document.getElementById('calendarPermissionMainInner').innerHTML = '';
		return false;
	}
	//alert(userSelected.supervisor.value)
	var personid = $('#usersSelect option:selected').val();
	var superid = $('#usersSelect option:selected').attr('supervisor');
	selectedUser = $('#usersSelect option:selected').val();
	$.post('../../ajax/ajax.calendarPermission.php',
	{
		PersonId : personid,
		CompanyId : companyid,
		action : 'getUsersList'
	}
	,function(result) {
			try
			{
				result = eval(result);
			}
			catch(e)
			{
				result = JSON.decode(result);
			}
			var divInner ='<table style="width:50%;" class="event"><tr><th style="border-left: 1px solid #C1DAD7;border-bottom: 1px solid #C1DAD7;border-top: 1px solid #C1DAD7;width:30%">UserName</th><th style="border-top: 1px solid #C1DAD7;border-bottom: 1px solid #C1DAD7;width:10%">Read</th><th style="border-top: 1px solid #C1DAD7;border-bottom: 1px solid #C1DAD7;width:10%;">Write</th><th style="border-top: 1px solid #C1DAD7;border-bottom: 1px solid #C1DAD7;width:10%;">Lock</th></tr>';
			
			for(i=0;i<result.length;i++){
				var firstName = result[i]['FirstName'];
				var lastName = result[i]['LastName'];
				var personId = result[i]['PersonID'];
				var calPermission = result[i]['CalendarPermission'];
				var writePer = 0;
				var readPer = 0;
				var lockPer = 0;
				var checked = 0;
				var disabled = '';
				for(j=0;j<calPermission.length;j++){
					if(calPermission[j]['OwnerPersonID'] == selectedUser){
						writePer = 	calPermission[j]['WritePermission'];
						readPer = 	calPermission[j]['ReadPermission'];
						lockPer = 	calPermission[j]['LockPermission'];
					}
				}
				if(personId == superid){
					disabled = 'disabled';	
					checked = 1;
				}
				divInner = divInner + '<tr><td class="leftcell" style="width:30%">'+lastName +' '+firstName+'</td>';
				if(readPer == 1 || checked == 1){
					divInner = divInner + '<td style="width:9%"><input person_id="'+ personId +'" type="checkbox" class="read" checked '+ disabled +'/></td>'
				}else{
					divInner = divInner + '<td style="width:9%"><input person_id="'+ personId +'" type="checkbox" class="read"/></td>'
				}
				if(writePer == 1 || checked == 1){
					divInner = divInner +'<td style="width:9%;"><input person_id="'+ personId +'" type="checkbox" checked class="write" '+ disabled +'/></td>';
				}else{
					divInner = divInner +'<td style="width:9%;"><input person_id="'+ personId +'" type="checkbox" class="write"/></td>';
				}
				
				if(lockPer == 1 || checked == 1){
					divInner = divInner +'<td style="width:9%;"><input person_id="'+ personId +'" type="checkbox" checked class="lock" '+ disabled +'/></td></tr>';
				}else{
					divInner = divInner +'<td style="width:9%;"><input person_id="'+ personId +'" type="checkbox" class="lock"/></td></tr>';
				}
			}
			divInner = divInner + '</table>';
			document.getElementById('calendarPermissionMainInner').innerHTML = divInner;
		}
	);		
}

function checkUserId(calArr,userId){
	for (j=0; j < calArr.length; j++) {
		if (calArr[j] === userId) {
			return true;
		}
	}
	return false;
}


function saveAdminCalendarSettings(){
	var user = selectedUser;
	lockcheck = $('.lock');
	writecheck = $('.write');
	readcheck = $('.read');
	var yes='';
	var no='';
	var locked = '';
	var unLock = '';
	var read = '';
	var unRead = '';
	$.each( writecheck, function(){	
		if($(this).attr('checked'))	yes+= $(this).attr('person_id') +',';				 
		else no+= $(this).attr('person_id') +',';				 
	});
	$.each( lockcheck, function(){	
		if($(this).attr('checked'))	locked+= $(this).attr('person_id') +',';				 
		else unLock+= $(this).attr('person_id') +',';				 
	});
	$.each( readcheck, function(){	
		if($(this).attr('checked'))	read+= $(this).attr('person_id') +',';				 
		else unRead+= $(this).attr('person_id') +',';				 
	});
	
	$.post('../../ajax/ajax.calendarPermission.php',
	{
		WritePermission : yes,
		UnWritePermission : no,
		LockedUser : locked,
		UnLockedUser : unLock,
		ReadPermission : read,
		UnReadPermission : unRead,
		UserId : user,
		action : 'SaveCalendarPermission'
	},
		processSavePermission
	);
}

function processSavePermission(result){
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}
	
	if(result.STATUS=='OK')
	{
		alert('Calendar Permission Saved.');	
	}
}