
var org_flat = new Object();
var org_roots = new Array();
var org_align = 'center';
var org_left_width = '50%';
var hightest_level = 0;
var org_height_spacing = 35;
var border_width = 2;
var nameFontSize = '12pt';
var org_manager_count = 0;
var org_alias_count = 0;
var org_sp_count = 0;
var org_DnD_cb = null;
var heighest_aliases = new Array();
var poslevels = new Array();

var elemIDtoNode = new Object();

var ORG_MANAGER = 0;
var ORG_ALIAS = 1;
var ORG_SP = 2;

function org_build_relation()
{
	org_align = 'center';
	org_left_width = '50%';
	hightest_level = 0;
	org_manager_count = 0;
	org_alias_count = 0;
	org_sp_count = 0;
	org_flat = new Object();
	for (var k = 0; k < g_logins.length; ++k)
	{
		if (g_logins[k][g_logins.LoginID] != '')
		{
			for (var n = 0; n < g_people.length; ++n)
			{
				if (g_people[n][g_people.PersonID] == g_logins[k][g_logins.PersonID]
					&& g_people[n][g_people.SupervisorID] != -3
					&& g_people[n][g_people.SupervisorID] != -1)
				{
					var temp = new Object();
					temp.isAlias = false;
					temp.persondata = g_people[n];
					temp.treenode = g_people[n].mynode;
					temp.Name = g_people[n][g_people.FirstName]+' '+g_people[n][g_people.LastName];
					temp.PersonID = g_people[n][g_people.PersonID];
					temp.SupervisorID = g_people[n][g_people.SupervisorID];
					temp.Level = parseInt(g_people[n][g_people.Level]);
					temp.children = new Array();
					temp.aliases = new Array();
					org_flat[g_people[n][g_people.PersonID]] = temp;
					
					if (hightest_level < temp.Level)
						hightest_level = temp.Level;
					
					break;
				}
			}
		}
	}
	for (var k = 0; k < g_logins.length; ++k)
	{
		if (g_logins[k][g_logins.LoginID] == '')
		{
			var temp = new Object();
			temp.isAlias = true;
			temp.treenode = null;
			temp.Name = g_logins[k][g_logins.Name];
			temp.ID = g_logins[k][g_logins.ID];
			temp.PersonID = g_logins[k][g_logins.PersonID];
			var tempaliases = org_flat[g_logins[k][g_logins.PersonID]].aliases;
			tempaliases[tempaliases.length] = temp;
			org_align = 'left';
			org_left_width = 60;
		}
	}
	
	org_roots = new Array();
	for (var k in org_flat)
	{
		if (org_flat[k].SupervisorID == -2)
		{
			org_roots[org_roots.length] = org_flat[k];
			org_flat[k].parent = null;
		}
		else
		{
			var children = org_flat[org_flat[k].SupervisorID].children;
			children[children.length] = org_flat[k];
			org_flat[k].parent = org_flat[org_flat[k].SupervisorID];
		}
		if (heighest_aliases[org_flat[k].Level] < org_flat[k].aliases.length)
			heighest_aliases[org_flat[k].Level] = org_flat[k].aliases.length;
	}
}

function org_print_node(node)
{
	var str = '<table cellpadding="0" cellspacing="0" style="font-family:Arial; font-size:'+nameFontSize+';"><tr>';
	str += '<td align="'+org_align+'" height="1" valign="top"';
	var spCount = 0;
	for (var k = 0; k < node.children.length; ++k)
	{
		if (node.children[k].Level == 1)
			spCount++;
	}
	var branchcount = (node.children.length-spCount);
	if (spCount > 0)
		branchcount++;
	str += 'colspan="'+branchcount+'"';
	elemIDtoNode['orgManager'+org_manager_count] = node;
	str += ' nowrap><nobr>';
	str += '<table cellpadding="0" cellspacing="0" height="100%"><tr><td valign="top"><nobr>&nbsp;&nbsp;';
	str += '<span id="orgManager'+(org_manager_count++)+'" onselectstart="return false;" style="background-color:white; border:'+border_width+'px solid black; padding:4px; display:inline-block; width:120px; text-align:center; height:'+org_height_spacing+'px;"><nobr>&nbsp;'+node.Name+'&nbsp;</nobr></span>';
	
	if (node.aliases.length > 0 || node.children.length > 0)
	{
		str += '<table width="100%" height="100%" cellpadding="0" cellspacing="0">';
		str += '<tr height="'+pathheight+'">';
		str += '<td style="border-right:'+border_width+'px solid black; font-size:1px;';
		str += '" width="'+org_left_width+'">&nbsp;</td>';
		str += '<td style="font-size:1px;';
		str += '">&nbsp;</td>';
		str += '</tr>';
		str += '</table>';
	}
	
	if (node.aliases.length > 0)
	{
		str += '</td><td valign="top">'
		str += '<table cellpadding="0" cellspacing="0" width="15" height="'+org_height_spacing+'" style="font-family:Arial; display:inline">';
		str += '<tr height="50%"><td style="border-bottom:'+border_width+'px solid black; font-size:1px;">&nbsp;</td></tr>';
		str += '<tr height="50%"><td style="font-size:1px;">&nbsp;</td></tr>';
		str += '</table>';
		str += '</td><td valign="top"><nobr>';
		str += '<span onselectstart="return false;" style="background-color:lightblue; text-align:center; border:'+border_width+'px solid black; padding:4px; display:inline-block; width:120px; height:'+org_height_spacing+'px;">';
		for (var k = 0; k < node.aliases.length; ++k)
		{
			elemIDtoNode['orgAlias'+org_alias_count] = node.aliases[k];
			str += '<div id="orgAlias'+(org_alias_count++)+'" onselectstart="return false;" style="text-align:left; cursor:default;">&nbsp;'+node.aliases[k].Name+'&nbsp;</div>';
		}
		str += '</span>&nbsp;&nbsp;&nbsp;&nbsp;';
	}
	
	str += '</nobr></td></tr></table></nobr></td></tr>';
	if (branchcount > 0)
	{
		var tempspacing = org_height_spacing;
		if (heighest_aliases[node.Level] > 0)
		{
			tempspacing += (heighest_aliases[node.Level]-node.aliases.length)*(org_height_spacing/2);
			tempspacing -= (heighest_aliases[node.Level]-node.aliases.length)*6;
		}
		str += '<tr><td colspan="'+branchcount+'"><table width="100%" cellpadding="0" cellspacing="0">';
		str += '<tr height="'+tempspacing+'"><td width="'+org_left_width+'" style="border-right:'+border_width+'px solid black; font-size:1px;">&nbsp;</td>';
		str += '<td style="font-size:1px;">&nbsp;</td></tr></table></td></tr>';
	}
	str += '<tr>';
	var curbranch = 0;
	for (var k = 0; k < node.children.length; ++k)
	{
		if (node.children[k].Level > 1)
		{
			str += '<td align="'+org_align+'" valign="top" height="1">';
			str += '<table width="100%" cellpadding="0" cellspacing="0">';
			var levelspan = node.children[k].parent.Level - node.children[k].Level;
			var pathheight = levelspan*org_height_spacing;
			if (branchcount == 1 || (curbranch > 0 && curbranch < (branchcount-1)))
			{
				str += '<tr height="'+pathheight+'">';
				str += '<td style="border-right:'+border_width+'px solid black; font-size:1px;';
				if (curbranch > 0)
					str += 'border-top:'+border_width+'px solid black;';
				str += '" width="'+org_left_width+'">&nbsp;</td>';
				str += '<td style="font-size:1px;';
				if (curbranch < (branchcount-1))
					str += 'border-top:'+border_width+'px solid black;';
				str += '">&nbsp;</td>';
				str += '</tr>';
			}
			else if (curbranch == 0)
			{
				str += '<tr height="'+pathheight+'"><td style="border-right:'+border_width+'px solid black; font-size:1px;" width="'+org_left_width+'">&nbsp;</td>';
				str += '<td style="border-top:'+border_width+'px solid black; font-size:1px;">&nbsp;</td></tr>';
			}
			else if (curbranch == (branchcount-1))
			{
				str += '<tr height="'+pathheight+'"><td style="border-top:'+border_width+'px solid black; border-right:'+border_width+'px solid black; font-size:1px;" width="'+org_left_width+'">&nbsp;</td>';
				str += '<td style="font-size:1px;">&nbsp;</td></tr>';
			}
			str += '</table>';
			str += org_print_node(node.children[k]);
			str += '</td>';
			curbranch++;
		}
	}
	if (spCount > 0)
	{
		var levelspan = node.Level - 1;
		if (levelspan == 1)
			levelspan = 0;
		else
			levelspan++;
		var pathheight = (levelspan*org_height_spacing);
		if (curbranch > 0)
			pathheight += 4;
		pathheight += (heighest_aliases[node.Level]-node.aliases.length)*(org_height_spacing/5);
		if (levelspan > 1 && node.aliases.length == 1)
			pathheight -= 5;
		if (pathheight < 0)
			pathheight = 0;
		pathheight = Math.floor(pathheight);
		str += '<td align="'+org_align+'" valign="top">';
		str += '<table width="100%" cellpadding="0" cellspacing="0">';
		str += '<tr height="'+pathheight+'"><td style="';
		if (curbranch > 0)
			str += 'border-top:'+border_width+'px solid black; ';
		str += 'border-right:'+border_width+'px solid black; font-size:1px;" width="'+org_left_width+'">&nbsp;</td>';
		str += '<td style="font-size:1px;">&nbsp;</td></tr>';
		str += '</tr></table>';
		str += '<nobr>&nbsp;<table style="display:inline; background-color:white; width:120px; font-family:Arial; font-size:'+nameFontSize+'; border:'+border_width+'px solid black;">';
		for (var k = 0; k < node.children.length; ++k)
		{
			if (node.children[k].Level == 1)
			{
				elemIDtoNode['orgSP'+org_sp_count] = node.children[k];
				str += '<tr><td nowrap><nobr><span id="orgSP'+(org_sp_count++)+'" onselectstart="return false;">'+node.children[k].Name+'</span></nobr></td></tr>';
			}
		}
		str += '</table>&nbsp;</nobr>';
		str += '</td>';
	}
	str += '</tr></table>';
	return str;
}

function makeOrgChart()
{
	heighest_aliases = new Array();
	poslevels = new Array();
	for (var k = 0; k < 8; ++k)
		heighest_aliases[k] = 0;
	org_build_relation();
	var str = '<table cellpadding="0" cellspacing="0" height="1" style="font-family:Arial; font-size:'+nameFontSize+';"><tr>';
	for (var k = 0; k < org_roots.length; ++k)
		str += '<td align="'+org_align+'" valign="top">'+org_print_node(org_roots[k])+'</td>';
	str += '</tr></table>'
	return str;
}

// Using this requires DnD.js to be included
function orgEnableDnD(cbFunction)
{
	for (var k = 0; k < org_manager_count; ++k)
	{
		var elem = document.getElementById('orgManager'+k);
		if (!elem)
			break;
		elem.orgtype = ORG_MANAGER;
		DnD_enableDrag(elem, true);
		DnD_enableReceive(elem, true);
	}
	/*
	for (var k = 0; k < org_alias_count; ++k)
	{
		var elem = document.getElementById('orgAlias'+k);
		if (!elem)
			break;
		elem.orgtype = ORG_ALIAS;
		DnD_enableDrag(elem, true);
	}
	*/
	for (var k = 0; k < org_sp_count; ++k)
	{
		var elem = document.getElementById('orgSP'+k);
		if (!elem)
			break;
		elem.orgtype = ORG_SP;
		DnD_enableDrag(elem, true);
		DnD_enableReceive(elem, true);
	}
	org_DnD_cb = cbFunction;
	window.onDrag = function(elem, receiver)
	{
		if (org_DnD_cb)
		{
			var pfrom = elemIDtoNode[elem.id];
			var pto = elemIDtoNode[receiver.id];
			if (receiver.orgtype == ORG_SP)
				pto = pto.parent;
			org_DnD_cb(pfrom, pto);
		}
	}
}