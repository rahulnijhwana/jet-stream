<?php

require 'jsmin.php';

// Output a minified version of example.js.

$path = dirname(__FILE__);

$js_path = realpath($path . '/..');

$sourcedir = opendir($path);
while (($file = readdir($sourcedir)) !== false) {
	if (substr($file, -3, 3) == '.js') {
		echo "Updating $js_path/$file...\n ";
		file_put_contents($js_path . '/' . $file, JSMin::minify(file_get_contents($file)));
	}
}
echo "Finished\n";

?>
