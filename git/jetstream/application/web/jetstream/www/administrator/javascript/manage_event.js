$(document).ready(function() {   
$('#frmEvent').ajaxForm({
	dataType: 'json', 
	success: processPostAddEvent
});
});
function processPostAddEvent(result)
{
	$('.error').html('');
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);
	}
	if(result.STATUS=='OK')
	{
		if(result.GETFORMATTEDVAR=='UPDATE')
		{
			alert('Event updated successfully');
			//window.location='?action=event';
		}
		else
		{
			alert('Event added successfully');
			//window.location='?action=event';
		}
		EVENTTYPE=result.EVENTDATA;
		
		var eventTypeData = '<table width="600px">';
		for(j=0;j<EVENTTYPE.length;j++){
			var eventReport = '';
			if(EVENTTYPE[j]['UseEventInCallReport'] == 1)
			{
				eventReport = '<img src="../../images/checkbox_checked.png" />';
			}
			else
			{
				eventReport = '<img src="../../images/checkbox_unchecked.png" />';
			}
			eventTypeData = eventTypeData +'<tr id="tr_'+EVENTTYPE[j]['EventTypeID']+'"><td class="leftcell" onclick="javascript:editEventType('+EVENTTYPE[j]['EventTypeID']+')"><img src="../images/edit.jpg" alt="edit" /></td><td onclick="javascript:deleteEventType('+EVENTTYPE[j]['EventTypeID']+',\''+escape(EVENTTYPE[j]['EventName'])+'\')"><img src="../images/del.png" alt="remove" /></td><td>'+EVENTTYPE[j]['EventName']+'</td><td>'+EVENTTYPE[j]['Type']+'</td><td><div style="width:16px;height:16px;background-color:#'+EVENTTYPE[j]['EventColor']+'"></div></td><td>'+eventReport+'</td></tr>'
		}
		eventTypeData = eventTypeData + '</table>';
		
		document.getElementById('innerEventType').innerHTML = eventTypeData;
		clearForm();
	}
	else
	{
		for(var key in result.ERROR)
			$('#spn_'+key).html(result.ERROR[key]);
	}
}
function editEventType(eventTypeId)
{
	var compId = document.getElementById('EventCompanyId').value; 
	$.post(
	'../../ajax/ajax.addevent.php',
	{
		eventTypeId:eventTypeId,				
		actionType:'LoadToEdit',
		EventCompanyId : compId
	}
	,processPostLoadToEdit
	);		
}
function processPostLoadToEdit(result)
{
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);
	}
	if(result.STATUS=='OK')
	{
		document.getElementById('EventName').value=result.EVENTNAME;
		document.getElementById('EventColor').value=result.EVENTCOLOR;
		document.getElementById('EventTypeID').value=result.EVENTID;		
		document.getElementById('EventType').value=result.EVENTTYPE;		
		if(result.UseEventInReport == 1)
		{
			document.getElementById('UseEventInReport').checked = true;
		}
		else
		{
			document.getElementById('UseEventInReport').checked = false;
		}
		document.getElementById('btnSubmit').value='Save';		
		document.getElementById('bgSpan').style.backgroundColor='#'+result.EVENTCOLOR;
	}
	else
		alert('Unable to find information for this event type');
}
function deleteEventType(eventTypeId,eventTypeName)
{
	var compId = document.getElementById('EventCompanyId').value; 
	if(confirm('Do you want to delete '+unescape(eventTypeName)+' ?'))
	{
		$.post(
		'../../ajax/ajax.addevent.php',
		{
			eventTypeId:eventTypeId,	
			eventTypeName:eventTypeName,
			actionType:'deleteEvent',
			EventCompanyId : compId
		}
		,processPostDeleteEvent
		);				
	}
}
function processPostDeleteEvent(result)
{
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);
	}
	if(result.STATUS=='OK')
	{
		alert("Event Type "+unescape(result.EVENTNAME)+" Deleted Successfully");
		document.getElementById('tr_'+result.EVENTID).style.display='none';
	}
	else
	{
		alert(result.MSG);
	}
}
function clearForm()
{
	document.getElementById('EventName').value='';
	document.getElementById('EventColor').value='';
	document.getElementById('EventTypeID').value='';	
	document.getElementById('EventType').value='';
	document.getElementById('UseEventInReport').checked=false;
	document.getElementById('bgSpan').style.backgroundColor=document.getElementById('div_manage_events').style.backgroundColor;
	$('.error').html('');	
	document.getElementById('btnSubmit').value='Submit';
}
function changeDivColor(col)
{
	$('#colorSelector div').css('backgroundColor', '#' + col);
}
function setColor(el)
{
	document.getElementById('EventColor').value=el.getAttribute('colorCode');
	document.getElementById('bgSpan').style.backgroundColor='#'+el.getAttribute('colorCode');
	document.getElementById('colorHolder').style.display='none';	
}
function resetPallet()
{
	document.getElementById('innerColorHolder').style.display='block';	
	document.getElementById('colorHolder').style.display='block';	
}