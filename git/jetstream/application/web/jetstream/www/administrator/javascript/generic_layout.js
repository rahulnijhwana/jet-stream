function manageSection(argType){
	if(argType == 'Account'){
		$.blockUI({ message: $('#manageAccountSectionDiv'),
		css: { 
		padding: '10px',
		backgroundColor: '#FFFFFF',
		opacity: '.9', 
		color: '#000000',
		top:  (jQuery(window).height() - jQuery('#manageAccountSectionDiv').height()) /3 + 'px', 
		left: (jQuery(window).width() - jQuery('#manageAccountSectionDiv').width()) /2 + 'px', 
		width: jQuery('#manageAccountSectionDiv').width() 
		}});
	}else if(argType == 'Contact'){
		$.blockUI({ message: $('#manageContactSectionDiv'),
		css: { 
		padding: '10px',
		backgroundColor: '#FFFFFF',
		opacity: '.9', 
		color: '#000000',
		top:  (jQuery(window).height() - jQuery('#manageContactSectionDiv').height()) /3 + 'px', 
		left: (jQuery(window).width() - jQuery('#manageContactSectionDiv').width()) /2 + 'px', 
		width: jQuery('#manageContactSectionDiv').width() 	
		}});
	}
}

// Maps the fields to google map adress
function manageMap(argType){
	if(argType == 'Contact'){
		$.blockUI({ message: $('#addContactMapDiv'),
		css: { 
		padding: '10px',
		backgroundColor: '#FFFFFF',
		opacity: '.9', 
		color: '#000000',
		top:  (jQuery(window).height() - jQuery('#addContactMapDiv').height()) /3 + 'px', 
		left: (jQuery(window).width() - jQuery('#addContactMapDiv').width()) /2 + 'px', 
		width: jQuery('#addContactMapDiv').width() 
		}});	
		
		var fields = CONTACTRIGHTSECTION.concat(CONTACTLEFTSECTION);
		//fields.sort();
		
		selectAccountField = '<option value="-1">-</option>';
		for(k=0;k<fields.length;k++){
			selectAccountField = selectAccountField + '<option value="'+fields[k]['ContactMapID']+'">'+fields[k]['LabelName']+'</option>';
		}
		selectAccountField = selectAccountField +'</select>';	
		
		// build the select box for address map
		$('#mapStreetTD').html('<select id="mapStreet" name="mapStreet" class="selectbox" style="width:120px">' + selectAccountField);
		$('#mapCityTD').html('<select id="mapCity" name="mapCity" class="selectbox" style="width:120px">' + selectAccountField);
		$('#mapStateTD').html('<select id="mapState" name="mapState" class="selectbox" style="width:120px">' + selectAccountField);
				
		// update the select box with values
		$('#mapStreet').val(g_address_map[0][g_address_map.Street]);
		$('#mapCity').val(g_address_map[0][g_address_map.City]);
		$('#mapState').val(g_address_map[0][g_address_map.State]);		
			
	}	
}


function editSection(argType){
	
	if(argType == 'Account'){
		var fieldDisabled = '';
		var accSectionData = '';
		for(i=0;i<ACCOUNTSECTIONDETAILS.length;i++){
			if(ACCOUNTSECTIONDETAILS[i]['SectionName'] == 'DefaultSection')
				fieldDisabled = "disabled";
			
				accSectionData = accSectionData + '<div> <input type="checkBox" name="editSection'+ACCOUNTSECTIONDETAILS[i]['SectionId']+'" id="editSection'+ACCOUNTSECTIONDETAILS[i]['SectionId']+'" value="1" '+fieldDisabled+'/><input type="text" id="editSectionName'+ACCOUNTSECTIONDETAILS[i]['SectionId']+'" name="editSectionName'+ACCOUNTSECTIONDETAILS[i]['SectionId']+'" value="'+ACCOUNTSECTIONDETAILS[i]['SectionName']+'" '+fieldDisabled+'/></div><br/>';
				fieldDisabled ='';
		}	
		document.getElementById('editAccountSectionInner').innerHTML = accSectionData;
		$.blockUI({ message: $('#editAccountSectionDiv'),
			css: { 
			padding: '10px',
			backgroundColor: '#FFFFFF',
			opacity: '.9', 
			color: '#000000',
			top:  (jQuery(window).height() - jQuery('#editAccountSectionDiv').height()) /3 + 'px', 
			left: (jQuery(window).width() - jQuery('#editAccountSectionDiv').width()) /2 + 'px', 
			width: jQuery('#editAccountSectionDiv').width() 
		}});
	}
	else if(argType == 'Contact'){
		var fieldDisabled = '';
		var conSectionData = '';
		for(i=0;i<CONTACTSECTIONDETAILS.length;i++){
			if(CONTACTSECTIONDETAILS[i]['SectionName'] == 'DefaultSection')
				fieldDisabled = "disabled";
				
				conSectionData = conSectionData + '<div> <input type="checkBox" name="editSection'+CONTACTSECTIONDETAILS[i]['SectionId']+'" id="editSection'+CONTACTSECTIONDETAILS[i]['SectionId']+'" value="1" '+fieldDisabled+'/><input type="text" id="editSectionName'+CONTACTSECTIONDETAILS[i]['SectionId']+'" name="editSectionName'+CONTACTSECTIONDETAILS[i]['SectionId']+'" value="'+CONTACTSECTIONDETAILS[i]['SectionName']+'" '+fieldDisabled+'/></div><br/>';
				fieldDisabled ='';
		}	
		document.getElementById('editContactSectionInner').innerHTML = conSectionData;
		$.blockUI({ message: $('#editContactSectionDiv'),
			css: { 
			padding: '10px',
			backgroundColor: '#FFFFFF',
			opacity: '.9', 
			color: '#000000',
			top:  (jQuery(window).height() - jQuery('#editContactSectionDiv').height()) /3 + 'px', 
			left: (jQuery(window).width() - jQuery('#editContactSectionDiv').width()) /2 + 'px', 
			width: jQuery('#editContactSectionDiv').width() 
		}});
	}
}

function deleteSection(argType){
	if(argType == 'Account'){
		var accSectionData = '';
		var fieldDisabled = '';
		for(i=0;i<ACCOUNTSECTIONDETAILS.length;i++){
			if(ACCOUNTSECTIONDETAILS[i]['SectionName'] == 'DefaultSection')
				fieldDisabled = "disabled";
				
				accSectionData = accSectionData + '<div> <input type="checkBox" name="deleteSection'+ACCOUNTSECTIONDETAILS[i]['SectionId']+'" id="deleteSection'+ACCOUNTSECTIONDETAILS[i]['SectionId']+'" value="1" '+fieldDisabled+'/><input type="text" id="deleteSectionName'+ACCOUNTSECTIONDETAILS[i]['SectionId']+'" name="deleteSectionName'+ACCOUNTSECTIONDETAILS[i]['SectionId']+'" value="'+ACCOUNTSECTIONDETAILS[i]['SectionName']+'" '+fieldDisabled+'/></div><br/>';
				fieldDisabled ='';
		}	
		document.getElementById('deleteAccountSectionInner').innerHTML = accSectionData;
		$.blockUI({ message: $('#deleteAccountSectionDiv'),
		css: { 
		padding: '10px',
		backgroundColor: '#FFFFFF',
		opacity: '.9', 
		color: '#000000',
		top:  (jQuery(window).height() - jQuery('#deleteAccountSectionDiv').height()) /3 + 'px', 
		left: (jQuery(window).width() - jQuery('#deleteAccountSectionDiv').width()) /2 + 'px', 
		width: jQuery('#deleteAccountSectionDiv').width() 
		}});
	}
	else if(argType == 'Contact'){
		var fieldDisabled = '';
		var conSectionData = '';
		for(i=0;i<CONTACTSECTIONDETAILS.length;i++){
			if(CONTACTSECTIONDETAILS[i]['SectionName'] == 'DefaultSection')
				fieldDisabled = "disabled";
				
				conSectionData = conSectionData + '<div> <input type="checkBox" name="deleteSection'+CONTACTSECTIONDETAILS[i]['SectionId']+'" id="deleteSection'+CONTACTSECTIONDETAILS[i]['SectionId']+'" value="1"'+fieldDisabled+'/><input type="text" id="deleteSectionName'+CONTACTSECTIONDETAILS[i]['SectionId']+'" name="deleteSectionName'+CONTACTSECTIONDETAILS[i]['SectionId']+'" value="'+CONTACTSECTIONDETAILS[i]['SectionName']+'"'+fieldDisabled+'/></div><br/>';
				fieldDisabled = '';
		}	
		document.getElementById('deleteContactSectionInner').innerHTML = conSectionData;
		$.blockUI({ message: $('#deleteContactSectionDiv'),
		css: { 
		padding: '10px',
		backgroundColor: '#FFFFFF',
		opacity: '.9', 
		color: '#000000',
		top:  (jQuery(window).height() - jQuery('#deleteContactSectionDiv').height()) /3 + 'px', 
		left: (jQuery(window).width() - jQuery('#deleteContactSectionDiv').width()) /2 + 'px', 
		width: jQuery('#deleteContactSectionDiv').width() 
		}});
	}	
}


function addSection(argType){
	if(argType == 'Account'){
		$.blockUI({ message: $('#addAccountSectionDiv'),
		css: { 
		padding: '10px',
		backgroundColor: '#FFFFFF',
		opacity: '.9', 
		color: '#000000',
		top:  (jQuery(window).height() - jQuery('#addAccountSectionDiv').height()) /3 + 'px', 
		left: (jQuery(window).width() - jQuery('#addAccountSectionDiv').width()) /2 + 'px', 
		width: jQuery('#addAccountSectionDiv').width() 
		}});
	}else if(argType == 'Contact'){
		$.blockUI({ message: $('#addContactSectionDiv'),
		css: { 
		padding: '10px',
		backgroundColor: '#FFFFFF',
		opacity: '.9', 
		color: '#000000',
		top:  (jQuery(window).height() - jQuery('#addContactSectionDiv').height()) /3 + 'px', 
		left: (jQuery(window).width() - jQuery('#addContactSectionDiv').width()) /2 + 'px', 
		width: jQuery('#addContactSectionDiv').width() 
		}});
	}	
}


function cancelSection(frm){
	document.getElementById('sectionName').value='';
	if(frm == 'Account'){
		document.getElementById('sectionError').innerHTML = '';	
		document.getElementById('editSectionError').innerHTML = '';
		document.getElementById('deleteSectionError').innerHTML = '';
	}else{
		document.getElementById('contactSectionError').innerHTML = '';	
		document.getElementById('contactEditSectionError').innerHTML = '';	
		document.getElementById('contactDeleteSectionError').innerHTML = '';	
	}
	$.unblockUI();	
}


function saveAddressMap(){		
	queryString = $('#frmAddressMap').formSerialize();	
	$.post('../data/save_address_map.php?'+'&action=add&companyId='+companyId, queryString,
	function(data){		
		// update the address map array
		g_address_map[0][g_address_map.Street] = $('#mapStreet').val();
		g_address_map[0][g_address_map.City] = $('#mapCity').val();
		g_address_map[0][g_address_map.State] = $('#mapState').val();
		// clear the lightbox
		$.unblockUI();
	}	
	);
}



var sectionAction = '';
function saveSection(divSet,secAction){
	sectionAction = secAction;
	
	if(divSet == 'Account'){
		switch(secAction){
			case 'addSection' : 
				var sectionName = document.getElementById('sectionName').value;
				sectionName = sectionName.replace("'","\'");
				if(sectionName == '')
				{
					document.getElementById('sectionError').innerHTML = 'SectionName is mandatory';
					return false;
				}
				queryString=$('#frmAddAccountSection').formSerialize()+'&action=save'+divSet+'Section&companyId='+companyId;
				$.post('../../ajax/ajax.companylayout.php?'+queryString,processSaveAccountSection);
				break;
			case 'editSection' : 
				queryString=$('#frmEditAccountSection').formSerialize()+'&action=edit'+divSet+'Section&companyId='+companyId;
				$.post('../../ajax/ajax.companylayout.php?'+queryString,processSaveAccountSection);
				break;
			case 'deleteSection'  :
				queryString=$('#frmDeleteAccountSection').formSerialize()+'&action=delete'+divSet+'Section&companyId='+companyId;
				$.post('../../ajax/ajax.companylayout.php?'+queryString,processSaveAccountSection);
				break;
		}
		
	}else if(divSet == 'Contact'){
		switch(secAction){
			case 'addSection' : 
				var sectionName = document.getElementById('contactSectionName').value;
				sectionName = sectionName.replace("'","\'");
				if(sectionName == '')
				{
					document.getElementById('contactSectionError').innerHTML = 'SectionName is mandatory';
					return false;
				}
				
				queryString=$('#frmAddContactSection').formSerialize()+'&action=save'+divSet+'Section&companyId='+companyId;
				$.post('../../ajax/ajax.contactlayout.php?'+queryString,processSaveContactSection);
				break;
			case 'editSection' : 
				queryString=$('#frmEditContactSection').formSerialize()+'&action=edit'+divSet+'Section&companyId='+companyId;
				$.post('../../ajax/ajax.contactlayout.php?'+queryString,processSaveContactSection);
				break;
			case 'deleteSection'  :
				queryString=$('#frmDeleteContactSection').formSerialize()+'&action=delete'+divSet+'Section&companyId='+companyId;
				$.post('../../ajax/ajax.contactlayout.php?'+queryString,processSaveContactSection);
				break;
		}
	}
}

function processSaveAccountSection(result)
{
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}
	if(result.STATUS.STATUS=='OK')
	{
		switch(sectionAction){
			case 'addSection' : 
				alert('Section added sucessfully.');
				document.getElementById('sectionName').value='';
				document.getElementById('sectionError').innerHTML = '';	
				reArrangeSections('Account',sectionAction,result.NEWSECTIONID,result.NEWSECTIONNAME);
				break;
			
			case 'editSection' : 
				alert('Section edited sucessfully.');
				document.getElementById('editSectionError').innerHTML = '';
				reArrangeSections('Account',sectionAction,result.EDITEDSECTIONSID,result.EDITEDSECTIONSNAME);
				break;
			
			case 'deleteSection'  :
				alert('Section deleted sucessfully.');
				document.getElementById('deleteSectionError').innerHTML = '';
				reArrangeSections('Account',sectionAction,result.DELETEDSECTIONSID,result.DELETEDSECTIONSNAME);
				break;
		}
		
		$.unblockUI();
		//window.location='?action=companylayout';
	}
	else
	{
		switch(sectionAction){
			case 'addSection' : 
				document.getElementById('sectionError').innerHTML= result.STATUS.ERROR[0];
				break;
			
			case 'editSection' : 
				document.getElementById('editSectionError').innerHTML= result.STATUS.ERROR[0];
				break;
			
			case 'deleteSection'  :
				document.getElementById('deleteSectionError').innerHTML= result.STATUS.ERROR[0];
				break;
		}
	}
}

function processSaveContactSection(result)
{
	try
	{
		result = eval(result);
	}
	catch(e)
	{
		result = JSON.decode(result);		
	}
	if(result.STATUS.STATUS == 'OK')
	{
		switch(sectionAction){
			case 'addSection' : 
				alert('Section added sucessfully.');
				document.getElementById('contactSectionName').value='';
				document.getElementById('contactSectionError').innerHTML = '';	
				reArrangeSections('Contact',sectionAction,result.NEWSECTIONID,result.NEWSECTIONNAME);
				break;
			
			case 'editSection' : 
				alert('Section edited sucessfully.');
				document.getElementById('contactEditSectionError').innerHTML = '';	
				reArrangeSections('Contact',sectionAction,result.EDITEDSECTIONSID,result.EDITEDSECTIONSNAME);
				break;
			
			case 'deleteSection'  :
				alert('Section deleted sucessfully.');
				document.getElementById('contactDeleteSectionError').innerHTML = '';
				reArrangeSections('Contact',sectionAction,result.DELETEDSECTIONSID,result.DELETEDSECTIONSNAME);
				break;
		}
		$.unblockUI();
		//window.location='?action=contactlayout';
	}
	else
	{
		
		switch(sectionAction){
			case 'addSection' : 
				document.getElementById('contactSectionError').innerHTML= result.STATUS.ERROR[0];
				break;

			case 'editSection' : 
				document.getElementById('contactEditSectionError').innerHTML= result.STATUS.ERROR[0];
				break;

			case 'deleteSection'  :
				document.getElementById('contactDeleteSectionError').innerHTML= result.STATUS.ERROR[0];
				break;
		}
	}
}

function strReplace(str,str1,str2){
	var strReplaceAll = str;
	var intIndexOfMatch = strReplaceAll.indexOf( str1 );

	while (intIndexOfMatch != -1){
		strReplaceAll = strReplaceAll.replace( str1, str2 );
		intIndexOfMatch = strReplaceAll.indexOf( str1 );
	}
	return strReplaceAll;
}

function reArrangeSections(type,action,secId,secName){
	if(type == 'Account'){
		switch(action){
			case 'addSection' : 
				var secDetail = new Array();
				secDetail['SectionName'] = secName;
				secDetail['SectionId'] = secId;
				
				//JASONACCOUNTSECTION.push(secName.replace(' ',''));
				JASONACCOUNTSECTION.push(strReplace(secName,' ', ''));
				ACCOUNTSECTIONNAME[ACCOUNTSECTIONNAME.length] = secName;
				ACCOUNTSECTIONDETAILS[ACCOUNTSECTIONDETAILS.length] = secDetail;
				ACCOUNTSECTSIZE[ACCOUNTSECTSIZE.length] = 10;
				break;

			case 'editSection' : 
				var origSecName = '';
				for(m=0;m<secId.length;m++){
					for(n=0;n<ACCOUNTSECTIONDETAILS.length;n++){
						if(secId[m] == ACCOUNTSECTIONDETAILS[n]['SectionId']){
							origSecName = ACCOUNTSECTIONDETAILS[n]['SectionName'];
							ACCOUNTSECTIONDETAILS[n]['SectionName'] = secName[m];
						}
					}
					//var editSecName = secName[m].replace(' ','');
					var editSecName = strReplace(secName[m],' ', '');
					for(j=0;j<JASONACCOUNTSECTION.length;j++){
						if(JASONACCOUNTSECTION[j] == strReplace(origSecName,' ', '')){
							JASONACCOUNTSECTION[j] = editSecName;
						}
					}
					for(k=0;k<ACCOUNTSECTIONNAME.length;k++){
						if(ACCOUNTSECTIONNAME[k] == origSecName){
							ACCOUNTSECTIONNAME[k] = secName[m];
						}
					}
					for(p=0;p<LEFTSECTION.length;p++){
						if(strReplace(origSecName,' ','') == LEFTSECTION[p]['SectionName']){
							LEFTSECTION[p]['SectionName'] = editSecName;
						}
					}
					
					for(q=0;q<RIGHTSECTION.length;q++){
						if(strReplace(origSecName,' ','') == RIGHTSECTION[q]['SectionName']){
							RIGHTSECTION[q]['SectionName'] = editSecName;
						}
					}
				}
				break;

			case 'deleteSection'  :
				var deletedSecName = '';
				for(m=0;m<secId.length;m++){
					for(n=0;n<ACCOUNTSECTIONDETAILS.length;n++){
						if(secId[m] == ACCOUNTSECTIONDETAILS[n]['SectionId']){
							deletedSecName = ACCOUNTSECTIONDETAILS[n]['SectionName'];
							ACCOUNTSECTIONDETAILS.splice(n,1);
						}
					}
					for(j=0;j<JASONACCOUNTSECTION.length;j++){
						if(JASONACCOUNTSECTION[j] == strReplace(deletedSecName,' ','')){
							JASONACCOUNTSECTION.splice(j,1);
						}
					}
					for(k=0;k<ACCOUNTSECTIONNAME.length;k++){
						if(ACCOUNTSECTIONNAME[k] == deletedSecName){
							ACCOUNTSECTIONNAME.splice(k,1);
						}
					}
					
					for(p=0;p<LEFTSECTION.length;p++){
						if(strReplace(deletedSecName,' ','') == LEFTSECTION[p]['SectionName']){
							var tempArr = new Array();
							tempArr['AccountMapID'] = LEFTSECTION[p]['AccountMapID'];
							tempArr['LabelName'] = LEFTSECTION[p]['LabelName'];
							tempArr['Align'] = '';
							tempArr['Position'] = '';
							tempArr['SectionId'] = '';
							LEFTSECTION.splice(p,1);
							p--;
							ACCOUNTFIELDS.push(tempArr);
						}
					}
					
					for(q=0;q<RIGHTSECTION.length;q++){
						if(strReplace(deletedSecName,' ','') == RIGHTSECTION[q]['SectionName']){
							var tempArr = new Array();
							tempArr['AccountMapID'] = RIGHTSECTION[q]['AccountMapID'];
							tempArr['LabelName'] = RIGHTSECTION[q]['LabelName'];
							tempArr['Align'] = '';
							tempArr['Position'] = '';
							tempArr['SectionId'] = '';
							//alert(RIGHTSECTION[q]['SectionId']);
							RIGHTSECTION.splice(q,1);
							q--;
							ACCOUNTFIELDS.push(tempArr);
						}
					}
				}
				break;
		}
		manageCompany();
	}else if(type == 'Contact'){
		switch(action){
			case 'addSection' : 
				var secDetail = new Array();
				secDetail['SectionName'] = secName;
				secDetail['SectionId'] = secId;
				JASONCONTACTSECTION[JASONCONTACTSECTION.length] = strReplace(secName,' ','');
				CONTACTSECTIONNAME[CONTACTSECTIONNAME.length] = secName;
				CONTACTSECTIONDETAILS[CONTACTSECTIONDETAILS.length] = secDetail;
				CONTACTSECTSIZE[CONTACTSECTSIZE.length] = 10;
				break;

			case 'editSection' : 
				var origSecName = '';
				for(m=0;m<secId.length;m++){
					for(n=0;n<CONTACTSECTIONDETAILS.length;n++){
						if(secId[m] == CONTACTSECTIONDETAILS[n]['SectionId']){
							origSecName = CONTACTSECTIONDETAILS[n]['SectionName'];
							CONTACTSECTIONDETAILS[n]['SectionName'] = secName[m];
						}
					}
					var editSecName = strReplace(secName[m],' ','');
					for(j=0;j<JASONCONTACTSECTION.length;j++){
						if(JASONCONTACTSECTION[j] == strReplace(origSecName,' ','')){
							JASONCONTACTSECTION[j] = editSecName;
						}
					}
					for(k=0;k<CONTACTSECTIONNAME.length;k++){
						if(CONTACTSECTIONNAME[k] == origSecName){
							CONTACTSECTIONNAME[k] = secName[m];
						}
					}
					for(p=0;p<CONTACTLEFTSECTION.length;p++){
						if(strReplace(origSecName,' ','') == CONTACTLEFTSECTION[p]['SectionName']){
							CONTACTLEFTSECTION[p]['SectionName'] = editSecName;
						}
					}
					
					for(q=0;q<CONTACTRIGHTSECTION.length;q++){
						if(strReplace(origSecName,' ','') == CONTACTRIGHTSECTION[q]['SectionName']){
							CONTACTRIGHTSECTION[q]['SectionName'] = editSecName;
						}
					}
				}
				break;

			case 'deleteSection'  :
				var deletedSecName = '';
				for(m=0;m<secId.length;m++){
					for(n=0;n<CONTACTSECTIONDETAILS.length;n++){
						if(secId[m] == CONTACTSECTIONDETAILS[n]['SectionId']){
							deletedSecName = CONTACTSECTIONDETAILS[n]['SectionName'];
							CONTACTSECTIONDETAILS.splice(n,1);
						}
					}
					for(j=0;j<JASONCONTACTSECTION.length;j++){
						if(JASONCONTACTSECTION[j] == strReplace(deletedSecName,' ','')){
							JASONCONTACTSECTION.splice(j,1);
						}
					}
					for(k=0;k<CONTACTSECTIONNAME.length;k++){
						if(CONTACTSECTIONNAME[k] == deletedSecName){
							CONTACTSECTIONNAME.splice(k,1);
						}
					}
					for(p=0;p<CONTACTLEFTSECTION.length;p++){
						if(strReplace(deletedSecName,' ','') == CONTACTLEFTSECTION[p]['SectionName']){
							var tempArr = new Array();
							tempArr['ContactMapID'] = CONTACTLEFTSECTION[p]['ContactMapID'];
							tempArr['LabelName'] = CONTACTLEFTSECTION[p]['LabelName'];
							tempArr['Align'] = '';
							tempArr['Position'] = '';
							tempArr['SectionId'] = '';
							CONTACTLEFTSECTION.splice(p,1);
							p--;
							CONTACTFIELDS.push(tempArr);
						}
					}
					for(q=0;q<CONTACTRIGHTSECTION.length;q++){
						if(strReplace(deletedSecName,' ','') == CONTACTRIGHTSECTION[q]['SectionName']){
							var tempArr = new Array();
							tempArr['ContactMapID'] = CONTACTRIGHTSECTION[q]['ContactMapID'];
							tempArr['LabelName'] = CONTACTRIGHTSECTION[q]['LabelName'];
							tempArr['Align'] = '';
							tempArr['Position'] = '';
							tempArr['SectionId'] = '';
							CONTACTRIGHTSECTION.splice(q,1);
							q--;
							CONTACTFIELDS.push(tempArr);
						}
					}
				}
				break;
		}
		manageContact();
	}
}


function getLayoutSettings()	
{
	var dataLayout='';
	el=$('.destinationBox');
		
	for(i=0;i<el.length;i++)
	{	
		if(el[i].id != 'totalFieldsDiv'){
			if(el[i].firstChild)
			{			
				if(i==0)
					dataLayout+= el[i].id  + ':'+el[i].firstChild.id;
				else
					dataLayout+= '|'+ el[i].id  + ':'+el[i].firstChild.id;
				
				
			}
			else
			{
				if(i==0)
					dataLayout+=  el[i].id + ':';
				else
					dataLayout+= '|'+ el[i].id + ':';
	
			}
				
		}
		
	}
return dataLayout;
}
