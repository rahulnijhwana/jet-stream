
/*Start script for manage company & manage contact fields...*/
	$(window).scroll(function(){
		if(navigator.appName == "Microsoft Internet Explorer") {
			var totalFieldsDiv = document.getElementById('totalFieldsDiv');
			totalFieldsDiv.style.top = parseInt(document.body.scrollTop);
			//totalFieldsDiv.style.left = '560';
		} 
	});
	var sectionName = '';
	var sections = '';
	var maxFields = '';
	var allSecSize = '';
	var leftSection= '';
	var rightSection= '';
	var secName ='';
	var sect = '';
	var loadForm = '';
	
	function manageCompany(){
		document.getElementById('company_page_loader_div').style.display = 'block';
		document.getElementById('manage_company_main_div').style.display = 'none';
		
		sect = JASONACCOUNTSECTION ;
		try
		{
			sect=eval(sect);
		}
		catch(e)
		{
			sect=JSON.decode(sect);
		}
	
		secName = ACCOUNTSECTIONNAME;
		try
		{
			secName = eval(secName);
		}
		catch(e)
		{
			secName = JSON.decode(secName);
		}
		sectionName = secName;
		sections = sect;
		maxFields = TOTALFIELDS;
		allSecSize = ACCOUNTSECTSIZE;
		leftSection = LEFTSECTION;
		rightSection = RIGHTSECTION;	
		var optionSelectField = '<select id="optionSetSelect" class="clsTextBox"   onChange="javascript: populateOptions(\'Account\');"><option value="-1"> </option>';
		for(j=0;j<OPTIONSETS.length;j++){
			optionSelectField = optionSelectField + '<option value='+j+'>'+OPTIONSETS[j]['OptionSetName']+'</option>';	
		}
		optionSelectField = optionSelectField + '</select>';
		document.getElementById('contactOptionSetInnerDiv').innerHTML = '';
		optionSetInnerDiv .innerHTML = optionSelectField;	
		createDataDivs('Account');
		setTimeout('showFields(\'Account\')',300);
		//showFields('Account');
	}
	
	
	function manageOppTracker(){
		
		//document.getElementById('opp_tracker_page_loader_div').style.display = 'block';
		document.getElementById('manage_opp_tracker_main_div').style.display = 'block';
		//document.getElementById('opp_tracker_page_loader_div').style.display = 'none';
		OppTrackerUse();
		manageResult();
		manageSalesCycleLocation();
		manageProducts();
	}
	
	function manageCache(){
		document.getElementById('manage_cache_main_div').style.display = 'block';
	}

	/*Begin Manage Opp Tracker*/
	
	var xmlHttp;
	var xmlHttp2;
	var xmlHttp3;
	var xmlHttp4;
	//manage opp tracker use or not
	function OppTrackerUse()
	{ 
	var url="../../ajax/ajax.getOppTrackeruse.php";
	//alert(url);
	xmlHttp=GetXmlHttpObject(stateChanged_1)
	xmlHttp.open("GET", url , true)
	xmlHttp.send(null)
	} 

	function stateChanged_1() 
	{ 
	//alert("here1: "+xmlHttp.readyState);
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
	document.getElementById("useOppTrackerDiv").innerHTML=xmlHttp.responseText
	} 
	} 
	//manage opp tracker result
	function manageResult()
	{ 
	var url="../../ajax/ajax.getOppTrackerResult.php";
	//alert(url);
	xmlHttp2=GetXmlHttpObject(stateChanged_2)
	xmlHttp2.open("GET", url , true)
	xmlHttp2.send(null)
	} 

	function stateChanged_2() 
	{ 
	//alert("here1: "+xmlHttp.readyState);
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{ 
	document.getElementById("manageResultDiv").innerHTML=xmlHttp2.responseText
	} 
	}
	
	//manage sale cycle location
	function manageSalesCycleLocation()
	{ 
	var url="../../ajax/ajax.getOppTrackerLocation.php";
	//alert(url);
	xmlHttp3=GetXmlHttpObject(stateChanged_3)
	xmlHttp3.open("GET", url , true)
	xmlHttp3.send(null)
	} 

	function stateChanged_3() 
	{ 
	//alert("here1: "+xmlHttp.readyState);
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
	document.getElementById("manageLocationDiv").innerHTML=xmlHttp3.responseText
	} 
	}
	
	//manage opp tracker products
	function manageProducts()
	{ 
	var url="../../ajax/ajax.getOppTrackerProducts.php";
	//alert(url);
	xmlHttp4=GetXmlHttpObject(stateChanged_4)
	xmlHttp4.open("GET", url , true)
	xmlHttp4.send(null)
	} 

	function stateChanged_4() 
	{ 
	//alert("here1: "+xmlHttp.readyState);
	if (xmlHttp4.readyState==4 || xmlHttp4.readyState=="complete")
	{ 
	document.getElementById("manageProductDiv").innerHTML=xmlHttp4.responseText
	} 
	}
	
	

	function GetXmlHttpObject(handler)
	{ 
	var objXmlHttp=null

	if (navigator.userAgent.indexOf("Opera")>=0)
	{
	alert("This example doesn't work in Opera") 
	return 
	}
	if (navigator.userAgent.indexOf("MSIE")>=0)
	{ 
	var strName="Msxml2.XMLHTTP"
	if (navigator.appVersion.indexOf("MSIE 5.5")>=0)
	{
	strName="Microsoft.XMLHTTP"
	} 
	try
	{ 
	objXmlHttp=new ActiveXObject(strName)
	objXmlHttp.onreadystatechange=handler 
	return objXmlHttp
	} 
	catch(e)
	{ 
	alert("Error. Scripting for ActiveX might be disabled") 
	return 
	} 
	} 
	if (navigator.userAgent.indexOf("Mozilla")>=0)
	{
	objXmlHttp=new XMLHttpRequest()
	objXmlHttp.onload=handler
	objXmlHttp.onerror=handler 
	return objXmlHttp
	}
	}
	//function to turn on/off Opp Tracker
	function useOppTracker(id)
	{ 
	var url="../../ajax/ajax.getOppTrackeruse.php?checkit="+id;
	//alert(url);
	xmlHttp8=GetXmlHttpObject(stateChanged_8)
	xmlHttp8.open("GET", url , true)
	xmlHttp8.send(null)
	}
	 

	function stateChanged_8() 
	{ 
	//alert("here1: "+xmlHttp.readyState);
	if (xmlHttp8.readyState==4 || xmlHttp8.readyState=="complete")
	{ 
		document.getElementById("useOppTrackerDiv").innerHTML=xmlHttp8.responseText
	} 
	}
	//functions to edit, save, delete, and add Results
	function editResult(r_id,r_name,r_status,r_order)
	{
		//document.getElementById('result_'+r_id).innerHTML = "<td align='center'>" + "<input type='text' name='result_name' value=\"" + r_name + "\" />" + "</td><td align='center'>" + "<input type='text' name='result_status' value=" + r_status + " />" + "</td><td align='center'>"+"<input type='text' name='result_order' value="+r_order+" />"+"</td><td align='center'><a href='javascript: saveResult("+r_id+")'>Save</a></td><td align='center'><a href=\"javascript: cancelEditResult("+r_id+",'"+escape(r_name)+"',"+r_status+','+r_order+")\">Cancel</a></td>";
		var won_selected = '';
		var lost_selected = '';
		if(r_status == 2) won_selected=' selected ';
		else lost_selected=' selected ';
		var status_select = "<select name='result_status'><option value='2' " + won_selected + ">Won</option><option value='1' " + lost_selected + ">Lost</option></select>";
		
		var order_selected = '';		
		var counter = 0;
	
		$("tr[id^='result_']").each(function() {
			counter++;
		});
		
		var order_selected = '';
		order_select = "<select name='result_order'>";
		for(var i=counter+1; i>0; i--){
			order_selected = '';
			if(i == r_order) order_selected = " selected='selected' ";
			order_select += "<option value=" + i + order_selected + ">" + i + "</option>";
		}
		order_select += "</select>";
		
		var innertext = "<td align='center'>" + "<input type='text' name='result_name' value=\"" + r_name + "\" />" + "</td><td align='center'>" + status_select + "</td><td align='center'>"+order_select+"</td><td align='center'><a href='javascript: saveResult("+r_id+")'>Save</a></td><td align='center'><a href=\"javascript: cancelEditResult("+r_id+",'"+escape(r_name)+"',"+r_status+','+r_order+")\">Cancel</a></td>";
		$('#result_' + r_id).html(innertext);
		//$('#result_'+r_id).html('text1');
	}
	
	function cancelEditResult(r_id,r_name,r_status,r_order)
	{
		//manageResult();
		var r_status_name = '';
		if(r_status == 2) r_status_name = 'Won';
		else r_status_name = 'Lost';
		$('#result_' + r_id).html("<td align='center'>"+r_name+"</td><td align='center'>"+r_status_name+"</td><td align='center'>"+r_order+"</td><td align='center'><a href=\"javascript: editResult("+r_id+",'"+escape(r_name)+"',"+r_status+','+r_order+")\">Edit</a></td><td align='center'><a href='javascript: deleteResult("+r_id+")'>Delete</a></td>");
	}
	
	var theId;
	function saveResult(id)
	{ 
	var theName = document.getElementsByName('result_name')[0].value;	
	theName = trim(theName);
	//alert(theName);
	if(theName == ''){
		alert("Result Name can't be empty! Please try again.");
		return
	}
	theName = escape(theName);
	
	theId=id;
	var url="../../ajax/ajax.saveOppTrackerResult.php?id="+id+"&name="+theName+"&status="+document.getElementsByName('result_status')[0].value+"&order="+document.getElementsByName('result_order')[0].value;
	//alert(url);
	xmlHttp5=GetXmlHttpObject(stateChanged_5)
	xmlHttp5.open("GET", url , true)
	xmlHttp5.send(null)
	} 

	function stateChanged_5() 
	{ 
	//alert("here1: "+xmlHttp.readyState);
	if (xmlHttp5.readyState==4 || xmlHttp5.readyState=="complete")
	{ 
	//document.getElementById('result_'+theId).innerHTML=xmlHttp5.responseText
		var innertext = xmlHttp5.responseText
		//alert(innertext);
		$('#result_' + theId).html(innertext)
		//$('#result_' + theId).html("<td>test</td>");
	} 
	}
	
	function deleteResult(id)
	{ 
	if(confirm("Are you sure you want to remove this result?")){
	theId=id;
	var url="../../ajax/ajax.deleteOppTrackerResult.php?id="+id;
	//alert(url);
	xmlHttp6=GetXmlHttpObject(stateChanged_6)
	xmlHttp6.open("GET", url , true)
	xmlHttp6.send(null)
	}
	} 

	function stateChanged_6() 
	{ 
	//alert("here1: "+xmlHttp.readyState);
	if (xmlHttp6.readyState==4 || xmlHttp6.readyState=="complete")
	{ 
	var thiselement = document.getElementById('result_'+theId);
	thiselement.parentNode.removeChild(thiselement);
	} 
	}
	
	function addResult()
	{ 	
	var theName = document.getElementsByName('result_name')[0].value;	
	theName = trim(theName);
	//alert(theName);
	if(theName == ''){
		alert("Result Name can't be empty! Please try again.");
		return
	}
	theName = escape(theName);
	
	var url="../../ajax/ajax.addOppTrackerResult.php?name="+theName+"&status="+document.getElementsByName('result_status')[0].value+"&order="+document.getElementsByName('result_order')[0].value;
	//alert(url);
	xmlHttp7=GetXmlHttpObject(stateChanged_7)
	xmlHttp7.open("GET", url , true)
	xmlHttp7.send(null)
	}
	 

	function stateChanged_7() 
	{ 
	//alert("here1: "+xmlHttp.readyState);
	if (xmlHttp7.readyState==4 || xmlHttp7.readyState=="complete")
	{ 
		//document.getElementById('new_result).innerHTML=xmlHttp7.responseText
		manageResult();
	} 
	}
	
	//functions to edit, save, delete, and add Sales Cycle Locations
	function editLocation(l_id,l_name,l_percent,l_order)
	{
		//document.getElementById('location_'+l_id).innerHTML = "<td align='center'>"+"<input type='text' name='location_name' value=\""+l_name+"\" />"+"</td><td align='center'>"+"<input type='text' name='location_percent' value="+l_percent+" />"+"</td><td align='center'>"+"<input type='text' name='location_order' value="+l_order+" />"+"</td><td align='center'><a href='javascript: saveLocation("+l_id+")'>Save</a></td><td align='center'><a href=\"javascript: cancelEditLocation("+l_id+",'"+escape(l_name)+"',"+l_percent+','+l_order+")\">Cancel</a></td>";
		var counter = 0;
		$("tr[id^='location_']").each(function() {
			counter++;
		});
		
		var order_selected = '';
		order_select = "<select name='location_order'>";
		for(var i=counter+1; i>0; i--){
			order_selected = '';
			if(i == l_order) order_selected = " selected='selected' ";
			order_select += "<option value=" + i + order_selected + ">" + i + "</option>";
		}
		order_select += "</select>";
		
		var percent_selected = '';
		percent_select = "<select name='location_percent'>";
		for(var i=1; i<=100; i++){
			percent_selected = '';
			if(i == l_percent) percent_selected = " selected='selected' ";
			percent_select += "<option value=" + i + percent_selected + ">" + i + "</option>";
		}
		percent_select += "</select>";
		
		var innertext = "<td align='center'>"+"<input type='text' name='location_name' value=\""+l_name+"\" />"+"</td><td align='center'>"+percent_select+"</td><td align='center'>"+ order_select +"</td><td align='center'><a href='javascript: saveLocation("+l_id+")'>Save</a></td><td align='center'><a href=\"javascript: cancelEditLocation("+l_id+",'"+escape(l_name)+"',"+l_percent+','+l_order+")\">Cancel</a></td>";
		$('#location_' + l_id).html(innertext);
	}
	
	function cancelEditLocation(l_id,l_name,l_percent,l_order)
	{
		//document.getElementById('location_'+l_id).innerHTML = "<td align='center'>"+l_name+"</td><td align='center'>"+l_percent+"</td><td align='center'>"+l_order+"</td><td align='center'><a href=\"javascript: editLocation("+l_id+",'"+escape(l_name)+"',"+l_percent+','+l_order+")\">Edit</a></td><td align='center'><a href='javascript: deleteLocation("+l_id+")'>Delete</a></td>";
		var innertext = "<td align='center'>"+l_name+"</td><td align='center'>"+l_percent+"</td><td align='center'>"+l_order+"</td><td align='center'><a href=\"javascript: editLocation("+l_id+",'"+escape(l_name)+"',"+l_percent+','+l_order+")\">Edit</a></td><td align='center'><a href='javascript: deleteLocation("+l_id+")'>Delete</a></td>";
		$('#location_' + l_id).html(innertext);
	}
	
	var theId;
	function saveLocation(id)
	{ 
	var theName = document.getElementsByName('location_name')[0].value;	
	theName = trim(theName);
	//alert(theName);
	if(theName == ''){
		alert("Sales Cycle Location Name can't be empty! Please try again.");
		return
	}
	theName = escape(theName);
	theId=id;
	var url="../../ajax/ajax.saveOppTrackerLocation.php?id="+id+"&name="+theName+"&percent="+document.getElementsByName('location_percent')[0].value+"&order="+document.getElementsByName('location_order')[0].value;
	xmlHttp9=GetXmlHttpObject(stateChanged_9)
	xmlHttp9.open("GET", url , true)
	xmlHttp9.send(null)
	} 

	function stateChanged_9() 
	{ 
	if (xmlHttp9.readyState==4 || xmlHttp9.readyState=="complete")
	{ 
	//document.getElementById('location_'+theId).innerHTML=xmlHttp9.responseText
		var innertext = xmlHttp9.responseText
		$('#location_' + theId).html(innertext);
	} 
	}
	
	function deleteLocation(id)
	{ 
	if(confirm("Do you want to remove this Sales Cycle Location?")){
	theId=id;
	var url="../../ajax/ajax.deleteOppTrackerLocation.php?id="+id;
	xmlHttp10=GetXmlHttpObject(stateChanged_10)
	xmlHttp10.open("GET", url , true)
	xmlHttp10.send(null)
	}
	} 

	function stateChanged_10() 
	{ 
	if (xmlHttp10.readyState==4 || xmlHttp10.readyState=="complete")
	{ 
	var thiselement = document.getElementById('location_'+theId);
	thiselement.parentNode.removeChild(thiselement);
	} 
	}
	
	function addLocation()
	{ 
	var theName = document.getElementsByName('location_name')[0].value;	
	theName = trim(theName);
	//alert(theName);
	if(theName == ''){
		alert("Sales Cycle Location Name can't be empty! Please try again.");
		return
	}
	theName = escape(theName);
	
	var url="../../ajax/ajax.addOppTrackerLocation.php?name="+theName+"&percent="+document.getElementsByName('location_percent')[0].value+"&order="+document.getElementsByName('location_order')[0].value;
	xmlHttp11=GetXmlHttpObject(stateChanged_11)
	xmlHttp11.open("GET", url , true)
	xmlHttp11.send(null)
	}
	 

	function stateChanged_11() 
	{ 
	if (xmlHttp11.readyState==4 || xmlHttp11.readyState=="complete")
	{ 
		manageSalesCycleLocation();
	} 
	}
	
		
	//functions to edit, save, delete, and add Products
	function editProduct(p_id,p_name,p_price,p_order)
	{
		//document.getElementById('product_'+p_id).innerHTML = "<td align='center'>"+"<input type='text' name='product_name' value=\""+p_name+"\" />"+"</td><td align='center'>"+"<input type='text' name='product_price' value="+p_price+" />"+"</td><td align='center'>"+"<input type='text' name='product_order' value="+p_order+" />"+"</td><td align='center'><a href='javascript: saveProduct("+p_id+")'>Save</a></td><td align='center'><a href=\"javascript: cancelEditProduct("+p_id+",'"+escape(p_name)+"',"+p_price+','+p_order+")\">Cancel</a></td>";	
		var counter = 0;
		$("tr[id^='product_']").each(function() {
			counter++;
		});
		
		var order_selected = '';
		order_select = "<select name='product_order'>";
		for(var i=counter+1; i>0; i--){
			order_selected = '';
			if(i == p_order) order_selected = " selected='selected' ";
			order_select += "<option value=" + i + order_selected + ">" + i + "</option>";
		}
		order_select += "</select>";
		
		var innertext = "<td align='center'>"+"<input type='text' name='product_name' value=\""+p_name+"\" />"+"</td><td align='center'>"+"<input type='text' name='product_price' value="+p_price+" />"+"</td><td align='center'>"+order_select+"</td><td align='center'><a href='javascript: saveProduct("+p_id+")'>Save</a></td><td align='center'><a href=\"javascript: cancelEditProduct("+p_id+",'"+escape(p_name)+"',"+p_price+','+p_order+")\">Cancel</a></td>";	
		$('#product_' + p_id).html(innertext);
	}
	
	function cancelEditProduct(p_id,p_name,p_price,p_order)
	{
		//document.getElementById('product_'+p_id).innerHTML = "<td align='center'>"+p_name+"</td><td align='center'>"+p_price+"</td><td align='center'>"+p_order+"</td><td align='center'><a href=\"javascript: editProduct("+p_id+",'"+escape(p_name)+"',"+p_price+','+p_order+")\">Edit</a></td><td align='center'><a href='javascript: deleteProduct("+p_id+")'>Delete</a></td>";
		var innertext = "<td align='center'>"+p_name+"</td><td align='center'>"+p_price+"</td><td align='center'>"+p_order+"</td><td align='center'><a href=\"javascript: editProduct("+p_id+",'"+escape(p_name)+"',"+p_price+','+p_order+")\">Edit</a></td><td align='center'><a href='javascript: deleteProduct("+p_id+")'>Delete</a></td>";
		$('#product_' + p_id).html(innertext);
	}
	
	var theId;
	function saveProduct(id)
	{ 
	var thePrice = document.getElementsByName('product_price')[0].value;
	thePrice = thePrice.replace(/$/g,'').replace(/,/g,'');
	//alert(thePrice);
	if(trim(thePrice) == ''){
		alert("Invalid Product Price Format! Please try again.");
		return
	}
	//alert (thePrice + '===' + parseFloat(thePrice));
	thePrice = parseFloat(thePrice);	
	if(isNaN(thePrice)) {
		alert("Invalid Product Price Format! Please try again.");
		return
		}
	
	var theName = document.getElementsByName('product_name')[0].value;	
	theName = trim(theName);
	//alert(theName);
	if(theName == ''){
		alert("Product Name can't be empty! Please try again.");
		return
	}
	theName = escape(theName);
		
	theId=id;
	var url="../../ajax/ajax.saveOppTrackerProduct.php?id="+id+"&name="+theName+"&price="+thePrice+"&order="+document.getElementsByName('product_order')[0].value;
	alert (url);
	xmlHttp12=GetXmlHttpObject(stateChanged_12)
	xmlHttp12.open("GET", url , true)
	xmlHttp12.send(null)
	} 

	function stateChanged_12() 
	{ 
	if (xmlHttp12.readyState==4 || xmlHttp12.readyState=="complete")
	{ 
	//document.getElementById('product_'+theId).innerHTML=xmlHttp12.responseText
		var innertext = xmlHttp12.responseText;
		$('#product_' + theId).html(innertext);
	} 
	}
	
	function deleteProduct(id)
	{ 
	if(confirm("Do you want to remove this Product?")){
	theId=id;
	var url="../../ajax/ajax.deleteOppTrackerProduct.php?id="+id;
	xmlHttp13=GetXmlHttpObject(stateChanged_13)
	xmlHttp13.open("GET", url , true)
	xmlHttp13.send(null)
	}
	} 

	function stateChanged_13() 
	{ 
	if (xmlHttp13.readyState==4 || xmlHttp13.readyState=="complete")
	{ 
	var thiselement = document.getElementById('product_'+theId);
	thiselement.parentNode.removeChild(thiselement);
	} 
	}
	
	function addProduct()
	{ 
	var thePrice = document.getElementsByName('product_price')[0].value;
	//thePrice = thePrice.replace('$','').replace(',','');
	thePrice = thePrice.replace(/$/g,'').replace(/,/g,'');
	//alert(thePrice);
	if(trim(thePrice) == ''){
		alert("Invalid Product Price Format! Please try again.");
		return
	}
	//alert (thePrice + '===' + parseFloat(thePrice));
	thePrice = parseFloat(thePrice);	
	if(isNaN(thePrice)) {
		alert("Invalid Product Price Format! Please try again.");
		return
		}
		
	var theName = document.getElementsByName('product_name')[0].value;	
	theName = trim(theName);
	//alert(theName);
	if(theName == ''){
		alert("Product Name can't be empty! Please try again.");
		return
	}
	theName = escape(theName);
	var url="../../ajax/ajax.addOppTrackerProduct.php?name="+theName+"&price="+thePrice+"&order="+document.getElementsByName('product_order')[0].value;
	xmlHttp14=GetXmlHttpObject(stateChanged_14)
	xmlHttp14.open("GET", url , true)
	xmlHttp14.send(null)
	}
	 

	function stateChanged_14() 
	{ 
	if (xmlHttp14.readyState==4 || xmlHttp14.readyState=="complete")
	{ 
		manageProducts();
	} 
	}
	function trimstr(str){
	   var tmp = ""
	   var count = 0
	   var newArray = str.split(" ")
	   for (var i=0;i<newArray.length;i++){
		  if (newArray[i]!=""){
			 if (count == 0)
				tmp = newArray[i]
			 else
				tmp = tmp + " " + newArray[i]
			 count++
		  }
	   }
	   return tmp
	}


	
	/*End Manage Opp Tracker*/
	
	
	
	
	
	
	
	function manageContact(){
		document.getElementById('contact_page_loader_div').style.display = 'block';
		document.getElementById('manage_contact_main_div').style.display = 'none';
		//setTimeout(5000);
		sec = JASONCONTACTSECTION;
		try
		{
			sec=eval(sec);
		}
		catch(e)
		{
			sec=JSON.decode(sec);
		}
		
		secName = CONTACTSECTIONNAME;		
		try
		{
			secName = eval(secName);
		}
		catch(e)
		{
			secName = JSON.decode(secName);
		}
		sectionName = secName;
		sections = sec;
		maxFields = CONTACTTOTALFIELDS;
		allSecSize = CONTACTSECTSIZE;
		leftSection= CONTACTLEFTSECTION;		
		rightSection= CONTACTRIGHTSECTION;
		var contactOptionSelectField = '<select id="optionSetSelect" class="clsTextBox"   onChange="javascript: populateOptions(\'Contact\');"><option value="-1"> </option>';
		for(j=0;j<CONTACTOPTIONSETS.length;j++){
			contactOptionSelectField = contactOptionSelectField + '<option value='+j+'>'+CONTACTOPTIONSETS[j]['OptionSetName']+'</option>';	
		}
		contactOptionSelectField = contactOptionSelectField + '</select>';
		document.getElementById('optionSetInnerDiv').innerHTML = '';
		document.getElementById('contactOptionSetInnerDiv').innerHTML = contactOptionSelectField;
		
		createDataDivs('Contact');
		//showFields('Contact');
		setTimeout('showFields(\'Contact\')',500);
	}

	function showFields(frm){
		if(frm == 'Account'){
			document.getElementById('company_page_loader_div').style.display = 'none';
			document.getElementById('manage_company_main_div').style.display = 'block';
		}else if(frm == 'Contact'){
			document.getElementById('contact_page_loader_div').style.display = 'none';
			document.getElementById('manage_contact_main_div').style.display = 'block';
		}		
	}
	
	function createDataDivs(formType){
		addScript('../javascript/dragable.js');
		loadForm = formType;
		if(formType == 'Account'){
			var allSec = JASONACCOUNTSECTION; 
			var fields = ACCOUNTFIELDS;
		}
		else if(formType == 'Contact'){
			var allSec = JASONCONTACTSECTION; 
			var fields = CONTACTFIELDS; 
		}

		var data = '';
		var secStyle = '';
		var secStyle1 = '';
		var secStyle2 = '';
		
		for(i=0;i<allSec.length;i++){
			if(allSec.length == 1){
				secStyle = secStyle + '#'+allSec[i] +' div';
				secStyle1 = secStyle1 + '#'+allSec[i] +' .clsSectionSmallBox';
				secStyle2 = secStyle2 + '#'+allSec[i] +' div div';
			}
			else{
				if((allSec.length - (i+1)) == 0){
					secStyle = secStyle + '#'+allSec[i] +' div';
					secStyle1 = secStyle1 + '#'+allSec[i] +' .clsSectionSmallBox';
					secStyle2 = secStyle2 + '#'+allSec[i] +' div div';				
				}
				else{
					secStyle = secStyle + '#'+allSec[i] +' div,';
					secStyle1 = secStyle1 + '#'+allSec[i] +' .clsSectionSmallBox,';
					secStyle2 = secStyle2 + '#'+allSec[i] +' div div,';				
				}
			}
		}
		
		var ss1 = document.getElementById('draggableCss');
		var def= secStyle +' {width:220px;height:20px;line-height:20px;float:left;margin-right:10px;margin-bottom:2px;} ';
		def = def + secStyle1 +' {border:1px solid #000;cursor:pointer;width:220px;background-color:#E2EBED;} ';
		def = def + secStyle2 +' {margin:0px;border:0px;padding:0px;background-color:#FFF;cursor:pointer;}';
		
		if(ss1.styleSheet)
		{
			ss1.styleSheet.cssText = def;
		}
		else
		{
			$('#draggableCss').html(def);
		}
		
		minHeight = (maxFields * 24) ;
		data = data + '<div id="totalFieldsDiv" style="left:560px !Important; height:240px;border: 1px solid black;overflow:auto;width:195px;">';
	
		for(k=0;k<fields.length;k++){
			if(formType == 'Account')
				data = data + '<div class="clsTotalDivSmallBox" id="'+ escape(fields[k]['LabelName']) +'_'+ fields[k]['AccountMapID'] +'" ondblclick="editFieldDetails(this)">'+ fields[k]['LabelName'] +'</div>';
			else if (formType == 'Contact')
				data = data + '<div class="clsTotalDivSmallBox" id="'+ escape(fields[k]['LabelName']) +'_'+ fields[k]['ContactMapID'] +'" ondblclick="editFieldDetails(this)">'+ fields[k]['LabelName'] +'</div>';
		}
		data = data + '</div>';
		
		
		for(i=0;i<allSec.length;i++){
			data = data + '<div id="'+allSec[i]+'" class="clsSection" style="border:1px solid black; width:485px;"></div>';
		}

		for(j=1;i<allSec.length;j++){
			data = data + '<div id="'+allSec[j]+'" class="clsSection" style="border:1px solid black; width:485px;"></div>';
		}
		
		if(formType == 'Account'){
			document.getElementById('contactDragContainer').innerHTML = '';
			document.getElementById('accountDragContainer').innerHTML = data;
		}
		else{
			document.getElementById('accountDragContainer').innerHTML = '';
			document.getElementById('contactDragContainer').innerHTML = data;
		}
		
		/*if (addScript('../javascript/dragable.js') ) {
		 
		  //initDragable(formType);
		} else {
		  alert("script include failed");
		}*/
	}

	function addScript(src) {
		  if (!document.getElementsByTagName || !document.createElement || !document.appendChild) {
			return false;
		  } else {
			var script = document.createElement("script");
			script.type = "text/javascript";
		   //	script.onload= setTimeout('initDragable()',1000) ;
		   	script.src = src;
			//document.getElementsByTagName("head")[0].appendChild(script);
			document.body.appendChild(script);
			 try{
			 	
			}catch(e){}
			
			return true;
		  }	
		}
		
	function initDragable(){
		if(loadForm != ''){
			var values = new Array();
			var ans = '';
			if(loadForm == 'Account'){
				ans = initDragDropScript(sections,'accountDragContent');
				values = addAccountLayoutData(rightSection);
				ans = reInitDragDropScript(ans,values);
				values = addAccountLayoutData(leftSection);
				reInitDragDropScript(ans,values);	
			}
			else if(loadForm == 'Contact'){
				ans = initDragDropScript(sections,'contactDragContent');
				values = addContactLayoutData(rightSection);
				ans = reInitDragDropScript(ans,values);
				values = addContactLayoutData(leftSection);
				reInitDragDropScript(ans,values);	
			}
		}else{
			alert("Error");	
		}
	}
	
function getLayoutSettings(){
	var dataLayout='';
	el=$('.destinationBox');
	for(i=0;i<el.length;i++)
	{	
		if(el[i].id != 'totalFieldsDiv'){
			if(el[i].firstChild)
			{			
				if(i==0)
					dataLayout+= el[i].id  + ':'+el[i].firstChild.id;
				else
					dataLayout+= '|'+ el[i].id  + ':'+el[i].firstChild.id;
			}
			else
			{
				if(i==0)
					dataLayout+=  el[i].id + ':';
				else
					dataLayout+= '|'+ el[i].id + ':';
	
			}
				
		}
		
	}
return dataLayout;
}
/*End script for manage company & manage contact fields...*/

/*Start script for manage event types...*/
function manageEvents(){	
	var eventsDiv = '<div class="middle" style="cursor:auto;border:1px solid black;width:'+Math.abs((BLOCKS_PER_ROW * 20))+'px;height:'+Math.abs((ROWS * 24))+'px;" id="colorHolder">';
	
	for(i=0;i<COLOR_CODE.length;i++){
		eventsDiv = eventsDiv + '<span colorCode="'+COLOR_CODE[i]+'" style="width:16px; height:15px; float:left; border:1px solid black;margin:2px;background-color:#'+COLOR_CODE[i]+';cursor:crosshair;" onclick="javascript:setColor(this)">&nbsp;</span>';
	}
	eventsDiv = eventsDiv + '</div>';
	document.getElementById('innerColorHolder').innerHTML = eventsDiv;
 	
	var eventTypeData = '<table width="600px">';
	for(j=0;j<EVENTTYPE.length;j++){
		var eventReport = '';
		if(EVENTTYPE[j]['UseEventInCallReport'] == 1)
		{
			eventReport = '<img src="../../images/checkbox_checked.png" />';
		}
		else
		{
			eventReport = '<img src="../../images/checkbox_unchecked.png" />';
		}
		eventTypeData = eventTypeData +'<tr id="tr_'+EVENTTYPE[j]['EventTypeID']+'"><td class="leftcell" onclick="javascript:editEventType('+EVENTTYPE[j]['EventTypeID']+')"><img src="../images/edit.jpg" alt="edit" /></td><td onclick="javascript:deleteEventType('+EVENTTYPE[j]['EventTypeID']+',\''+escape(EVENTTYPE[j]['EventName'])+'\')"><img src="../images/del.png" alt="remove" /></td><td>'+EVENTTYPE[j]['EventName']+'</td><td>'+EVENTTYPE[j]['Type']+'</td><td><div style="width:16px;height:16px;background-color:#'+EVENTTYPE[j]['EventColor']+'"></div></td><td>'+eventReport+'</td></tr>'
	}
	eventTypeData = eventTypeData + '</table>';
	document.getElementById('innerEventType').innerHTML = eventTypeData;
	document.getElementById('EventCompanyId').value = companyId;
}

/*End script for manage event types...*/

/*Start script for manage note fields...*/
function manageNoteFields(){
	var inner = '';
	if(noteTempExists){
		inner = '<table width="600px">';
		for(i=0;i<noteTemplates.length;i++){
			inner = inner + '<tr id="notesTemp_'+ noteTemplates[i]['NoteTemplateID'] +'"><td class="leftcell" onclick="javascript:editNoteTemplate('+ noteTemplates[i]['NoteTemplateID'] +')" style="width:9%"><img src="../images/edit.jpg" alt="edit" /></td><td onclick="javascript:deleteNoteTemplate('+ noteTemplates[i]['NoteTemplateID'] +')" style="width:9%"><img src="../images/del.png" alt="edit" /></td><td style="width:31%">'+ noteTemplates[i]['TemplateName'] +'</td><td>'+ noteTemplates[i]['FormType'] +'</td></tr>';	
		}
		inner = inner + '</table>';
	}else{
		inner = '<table width="600px"><tr><td class="leftcell"></td></tr></table>';	
	}
	
	document.getElementById('innerNoteFields').innerHTML = inner;
	document.getElementById('NoteCompanyId').value = companyId;	
}
/*End script for manage note fields...*/

function assignUserTemplate(){
	var templateFieldInner = '<select id="userTemplatesSelect" name="userTemplatesSelect"><option value="-1"> </option>';
	for(j=0;j<ALLTEMPLATES.length;j++){
		templateFieldInner = templateFieldInner + '<option value='+ALLTEMPLATES[j]['NoteTemplateID'] +'>'+ALLTEMPLATES[j]['TemplateName']+'</option>';	
	}
	templateFieldInner = templateFieldInner + '</select><br/><span class="clsError  error" id="spn_SelectTemplate"></span>';
	document.getElementById('selectTemplate_td').innerHTML = templateFieldInner;
	
	var userFieldInner = '<select id="usersSelect" name="usersSelect" onchange="javascript:changeTempValue()"><option value="-1"> </option>';
	for(j=0;j<ALLPEOPLE.length;j++){
		userFieldInner = userFieldInner + '<option value='+ALLPEOPLE[j][2] +'>'+ALLPEOPLE[j][1] +' '+ ALLPEOPLE[j][0]+'</option>';	
	}
	userFieldInner = userFieldInner + '</select><br/><span class="clsError  error" id="spn_SelectUser"></span>';
	document.getElementById('selectUser_td').innerHTML = userFieldInner;
}

function restore_search_data(btn_val) {			
	$.post('../../ajax/ajax.restore_search_data.php',
	{
		btn_val : btn_val
	}
	,function() {}
	);
}

function userCalendarPermission(){
	var userFieldInner = '<select id="usersSelect" name="usersSelect" onchange="javascript:getUserCalendarPermission(companyId);"><option value="-1"> </option>';
	for(j=0;j<ALLPEOPLE.length;j++){
		userFieldInner = userFieldInner + '<option supervisor='+ALLPEOPLE[j][3]+' value='+ALLPEOPLE[j][2] +'>'+ALLPEOPLE[j][1] +' '+ ALLPEOPLE[j][0]+'</option>';	
	}
	userFieldInner = userFieldInner + '</select><br/><span class="clsError  error" id="spn_SelectUser"></span>';
	document.getElementById('calendarPermissionUsersInner').innerHTML = userFieldInner;
}

function userJetFile(Action ){
	
	$.post('../../ajax/ajax.admin.php', {
			action : Action
		}, function(data) {$('#jetfiletags').html(data)}
		, 'html' 
	);
	
}

function JetFileAction(Action, param1, param2 ) {

	if (Action == 'AdminAddTags'){
		var var1 = document.getElementById('JetFileNewTags').value ; 
		var var2 = param2 ; 
	}
	else if ( Action == 'AdminEditTags'){
		var reply ;
		while ( 1 ){
			reply = prompt("Please type in the New Label.", param1); 
			if (reply != '' ){
				break ; 
			} 
		}
		var var1 = reply;
		var var2 = param2 ; 
	}
	else {
		confirm('Please confirm this DELETE action.')
		var var1 = param1 ; 
		var var2 = param2 ;
	}
	
	$.post('../../ajax/ajax.admin.php', {
			action : Action, 
			param1 : var1,  
			param2 : var2
		}, function(data) {
			userJetFile('AdminTags');
			document.getElementById('JetFileNewTags').value = '';
		}
		, 'text' 
	);
	
}
