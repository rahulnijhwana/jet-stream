
var additionalFieldParameters = '';
function addFieldsLightBox(frm){
	//console.log(frm);

	if(frm == 'Account'){
		$.blockUI({ message: $('#addAccountFieldsLightBox'),
			css: { 
			padding: '10px',
			backgroundColor: '#FFFFFF',
			opacity: '.9', 
			color: '#000000',
			top:  (jQuery(window).height() - jQuery('#addAccountFieldsLightBox').height()) /4 + 'px', 
			left: (jQuery(window).width() - jQuery('#addAccountFieldsLightBox').width()) /3 + 'px', 
			width: '500px',
			height:'400px',
			overflow:'scroll'
		}});	
	}else{
		$.blockUI({ message: $('#addContactFieldsLightBox'),
			css: { 
			padding: '10px',
			backgroundColor: '#FFFFFF',
			opacity: '.9', 
			color: '#000000',
			top:  (jQuery(window).height() - jQuery('#addContactFieldsLightBox').height()) /4 + 'px', 
			left: (jQuery(window).width() - jQuery('#addContactFieldsLightBox').width()) /3 + 'px', 
			width: '500px',
			height:'400px',
			overflow:'scroll'
		}});		
	}	
}

function checkSelectFieldType(frm){
	if(frm == 'Account'){
		var fieldType = document.getElementById('selectFieldType').value;
		document.getElementById('showAddAccountFieldError').innerHTML = '';
		document.getElementById('addAccountFieldsInner').innerHTML = '';
		
		switch(fieldType){
			case 'LongText' :
				document.getElementById('fieldSelect').value = 'LongText';
				document.getElementById('dataTypeSelect').value = 'LongText';
				break;
			case 'Text' :
				document.getElementById('fieldSelect').value = 'TextBox';
				document.getElementById('dataTypeSelect').value = 'Alphabet';
				break;
			case 'Number' :
				document.getElementById('fieldSelect').value = 'TextBox';
				document.getElementById('dataTypeSelect').value = 'Numeric';
				break;
			case 'Url' :
				document.getElementById('fieldSelect').value = 'TextBox';
				document.getElementById('dataTypeSelect').value = 'Alphabet';
				break;
			case 'Email' :
				document.getElementById('fieldSelect').value = 'TextBox';
				document.getElementById('dataTypeSelect').value = 'Alphabet';
				break;
			case 'Phone' :
				document.getElementById('fieldSelect').value = 'TextBox';
				document.getElementById('dataTypeSelect').value = 'Alphabet';
				break;
			case 'DropDown' :
				document.getElementById('fieldSelect').value = 'DropDown';
				document.getElementById('dataTypeSelect').value = 'Select';
				break;
			case 'CheckBox' :
				document.getElementById('fieldSelect').value = 'CheckBox';
				document.getElementById('dataTypeSelect').value = 'Bool';
				break;
			case 'RadioButton' :
				document.getElementById('fieldSelect').value = 'RadioButton';
				document.getElementById('dataTypeSelect').value = 'Select';
				break;
			case 'Date' :
				document.getElementById('fieldSelect').value = 'Date';
				document.getElementById('dataTypeSelect').value = 'Date';
				break;	
		}
	}else if(frm == 'Contact'){
		var fieldType = document.getElementById('selectContactFieldType').value;
		document.getElementById('showAddContactFieldError').innerHTML = '';
		document.getElementById('addContactFieldsInner').innerHTML = '';
		
		switch(fieldType){
			case 'LongText' :
				document.getElementById('fieldSelectContact').value = 'LongText';
				document.getElementById('dataTypeSelectContact').value = 'LongText';
				break;		
			case 'Text' :
				document.getElementById('fieldSelectContact').value = 'TextBox';
				document.getElementById('dataTypeSelectContact').value = 'Alphabet';
				break;
			case 'Number' :
				document.getElementById('fieldSelectContact').value = 'TextBox';
				document.getElementById('dataTypeSelectContact').value = 'Numeric';
				break;
			case 'Url' :
				document.getElementById('fieldSelectContact').value = 'TextBox';
				document.getElementById('dataTypeSelectContact').value = 'Alphabet';
				break;
			case 'Email' :
				document.getElementById('fieldSelectContact').value = 'TextBox';
				document.getElementById('dataTypeSelectContact').value = 'Alphabet';
				break;
			case 'Phone' :
				document.getElementById('fieldSelectContact').value = 'TextBox';
				document.getElementById('dataTypeSelectContact').value = 'Alphabet';
				break;
			case 'DropDown' :
				document.getElementById('fieldSelectContact').value = 'DropDown';
				document.getElementById('dataTypeSelectContact').value = 'Select';
				break;
			case 'CheckBox' :
				document.getElementById('fieldSelectContact').value = 'CheckBox';
				document.getElementById('dataTypeSelectContact').value = 'Bool';
				break;
			case 'RadioButton' :
				document.getElementById('fieldSelectContact').value = 'RadioButton';
				document.getElementById('dataTypeSelectContact').value = 'Select';
				break;
			case 'Date' :
				document.getElementById('fieldSelectContact').value = 'Date';
				document.getElementById('dataTypeSelectContact').value = 'Date';
				break;	
		}
	}
		if(fieldType != -1){
			if(frm == 'Account')
				
				checkFieldAvailable(document.getElementById('fieldSelect').value,frm);
			else
				checkFieldAvailable(document.getElementById('fieldSelectContact').value,frm);
		}
		else{
			if(frm == 'Account')
				document.getElementById('addAccountFieldsInner').innerHTML = '';
			else
				document.getElementById('addContactFieldsInner').innerHTML = '';
		}
		/*Start JET-37*/
		if(frm == 'Contact'){
				addContactFieldmap();
			}
		/*End JET-37*/
}
/* JET-37*/
function addContactFieldmap(){
		$.ajax({
			  url: '../../ajax/ajax.addEditFields.php?getContactmap=ContactgoogleMap&companyId='+companyId,
			  success: function(result){
				  var googleFields = JSON.decode(result);

				 // console.log(googleFields.code);
				  if(googleFields.code != '200' ){
					GoogleMap = '<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Google Contact Mapping</div><div id="selectBoxResize" style="float:left;"><select id="selectGoogleContactFieldTypeid" name="selectGoogleContactFieldTypeid" class="clsTextBox" style="width:134px;font-family: Verdana, Helvetica, Tahoma, Arial;border: 1px solid #808080;font-size: 8pt;" ><option value="-1">Select</option>';
				  	for(k=0;k<googleFields.length;k++){
							GoogleMap += '<option value="'+googleFields[k]['googleField']+'" >'+googleFields[k]['googleFieldName']+'</option>';
					}
					GoogleMap += '<select></div><br/>';
					document.getElementById('getGoogleContactField').innerHTML = GoogleMap;
					//$.html(GoogleMap);
						  }
					  }
			});
	}
	/* JET-37*/

function checkFieldAvailable(type,frm){
	console.log(type);
	var fieldSelected = '';
	if(frm=='Account'){
		fieldSelected = document.getElementById('selectFieldType').value;
	}
	else if(frm=='Contact'){		
		fieldSelected = document.getElementById('selectContactFieldType').value;
	}
	if(fieldSelected != -1){
	
		var fieldType = '';
		if(type == 'TextBox'){
			if(frm=='Account'){
			type = document.getElementById('dataTypeSelect').value;
			}
			else if(frm=='Contact'){		
			type = document.getElementById('dataTypeSelectContact').value;
			}
		}
		switch(type){
			case 'LongText' :
				fieldType = 'LongText';
				break;
			case 'Alphabet' :
				fieldType = 'Text';
				break;
			case 'Numeric' :
				fieldType = 'Number';
				break;
			case 'DropDown' :
				fieldType = 'Select';
				break;
			case 'CheckBox' :
				fieldType = 'Bool';
				break;
			case 'RadioButton' :
				fieldType = 'Select';
				break;
			case 'Date' :
				fieldType = 'Date';
				break;	
		}

		if(frm == 'Account'){
			queryString='btnAdd=AddAccountField&fieldName='+fieldType+'&companyId='+companyId;
			$.post('../../ajax/ajax.addEditFields.php?'+queryString,processAddNewAccountFields
			);  
		}
		else if(frm == 'Contact'){
			queryString='btnAdd=AddContactField&fieldName='+ fieldType+'&companyId='+companyId;
			$.post('../../ajax/ajax.addEditFields.php?'+queryString,processAddNewContactFields		
			);
		}
	}else{
		if(frm == 'Account')
			document.getElementById('showAddAccountFieldError').innerHTML = 'Please select a field type first';
		else if(frm == 'Contact')
			document.getElementById('showAddContactFieldError').innerHTML = 'Please select a field type first';
	}
}

function processAddNewAccountFields(result){
	
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}
	if(result.STATUS=='OK')
	{	
		if(document.getElementById('selectFieldType').value != 'DropDown' && document.getElementById('selectFieldType').value != 'RadioButton'){
			addSelectedField('Account',null,null);
		}else{
			$('#addAccountFieldsLightBox').block({ message: $('#optionListDiv'),
				css: { 
				padding: '10px',
				backgroundColor: '#FFFFFF',
				opacity: '.9', 
				color: '#000000',
				top:  (jQuery(window).height() - jQuery('#optionListDiv').height()) /4 + 'px', 
				left: (jQuery(window).width() - jQuery('#optionListDiv').width()) /3 + 'px', 
				width: '460px',
				height:'360px',
				overflow:'scroll'
			}});	
		}
	}
	else
	{
		var str='';
		for(i=0;i<result.ERROR.length;i++)
		{
			str=str+'<br/>'+result.ERROR[i];
		}
		document.getElementById('showAddAccountFieldError').innerHTML = str;
	}
}

function processAddNewContactFields(result){
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}
	
	if(result.STATUS=='OK')
	{
		if(document.getElementById('selectContactFieldType').value != 'DropDown' && document.getElementById('selectContactFieldType').value != 'RadioButton'){
			addSelectedField('Contact',null,null);
		}else{
			//document.getElementById('addAccountFieldsInner').innerHTML = '';
			$('#addContactFieldsLightBox').block({ message: $('#contactOptionListDiv'),
				css: { 
				padding: '10px',
				backgroundColor: '#FFFFFF',
				opacity: '.9', 
				color: '#000000',
				top:  (jQuery(window).height() - jQuery('#addContactFieldsLightBox').height()) /4 + 'px', 
				left: (jQuery(window).width() - jQuery('#addContactFieldsLightBox').width()) /3 + 'px', 
				width: '460px',
				height:'360px',
				overflow:'scroll'
			}});	
		}
	}
	else
	{
		var str='';
		for(i=0;i<result.ERROR.length;i++)
		{
			str=str+'<br/>'+result.ERROR[i];
		}
		document.getElementById('showAddContactFieldError').innerHTML = str;
	}
}

function addSelectedField(frm,labelValue,optionList){
	
		var selectAccountField = '';
		var selectKeywordField = '';
		var labelName = '';
		var block = '';
		var optionsField = '';
		var settingBtnStyl = 'Disabled';
		if(frm == 'Account')
			var type = document.getElementById('selectFieldType').value;
		else
			var type = document.getElementById('selectContactFieldType').value;
		
		if(type == 'Text' || type == 'Number' || type == 'Date'){
			settingBtnStyl = '';
		}
		if(labelValue != null){
			labelName = labelValue;
			//block = 'disabled';
		}
		if(optionList == null){
			optionsField = '<div style="float:left;"><input type="text" id="optionsSelect" class="clsTextBox" value="Options" maxlength="50" disabled name="optionsSelect"/></div><br/>';
		}else{
			optionsField = '<div><div style="float:left;"><select id="optionsSelect" name="optionsSelect" class="clsTextBox" style="width:135px;">';
			for (var i =0; i<optionList.length;i++)
			{
				optionsField = optionsField + '<option value="'+optionList[i]+'">'+optionList[i]+'</option>';
			}
			optionsField = optionsField + '</select></div>';
			for (var i =0; i<optionList.length;i++)
			{		
				optionsField = optionsField + '<input type=\'hidden\' id="options[]" value="'+optionList[i]+'" name="options[]" />';
			}
			optionsField = optionsField + '</div></br>';
		}
		
		if(frm == 'Contact'){
			selectAccountField = '<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Account Field</div><div style="float:left;" id="selectBoxResize"><select id="selectAccount" style="width:134px;font-family: Verdana, Helvetica, Tahoma, Arial;border: 1px solid #808080;font-size: 8pt;" name="selectAccount" class="selectbox"><option value="-1">-</option>';
			for(k=0;k<ALLACCOUNTFIELDS.length;k++){
				selectAccountField = selectAccountField + '<option value="'+ALLACCOUNTFIELDS[k]['AccountMapID']+'">'+ALLACCOUNTFIELDS[k]['LabelName']+'</option>';
			}
			selectAccountField = selectAccountField +'</select></div><br/>';	
			selectKeywordField = '<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Keyword</div><div style="float:left;" id="selectBoxResize"><select id="selectKeyword" style="width:134px;font-family: Verdana, Helvetica, Tahoma, Arial;border: 1px solid #808080;font-size: 8pt;" name="selectKeyword" class="selectbox"><option value="-1">-</option>';
			for(k=0;k<SELECTEDKEYWORDFIELDS.length;k++){
				selectKeywordField = selectKeywordField + '<option value="'+SELECTEDKEYWORDFIELDS[k]['KeywordId']+'">'+SELECTEDKEYWORDFIELDS[k]['Keyword']+'</option>';
			}
			selectKeywordField = selectKeywordField +'</select></div><br/>';
		}
		else {
			selectKeywordField = '<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Keyword</div><div style="float:left;" id="selectBoxResize"><select id="selectKeyword" style="width:134px;font-family: Verdana, Helvetica, Tahoma, Arial;border: 1px solid #808080;font-size: 8pt;" name="selectKeyword" class="selectbox"><option value="-1">-</option>';
			for(k=0;k<SELECTEDKEYWORDFIELDS.length;k++){
				selectKeywordField = selectKeywordField + '<option value="'+SELECTEDKEYWORDFIELDS[k]['KeywordId']+'">'+SELECTEDKEYWORDFIELDS[k]['Keyword']+'</option>';
			}
			selectKeywordField = selectKeywordField +'</select></div><br/>';
		}
		
		var fieldsInner = '<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Label Name</div><div style="float:left;"><input type="text" id="fieldLabelName" maxlength="8000" class="clsTextBox" value="'+ labelName +'" name="fieldLabelName" '+ block +'/></div><br/><div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Type</div><div style="float:left;"><input type="text" id="" class="clsTextBox" value="'+type+'" maxlength="50" disabled name=""/></div><br/><div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Options</div>'+ optionsField +' '+ selectAccountField +' '+ selectKeywordField +'<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Mandatory</div><div style="float:left;"><input type="checkbox" id="isMandatoryField" class="clsTextBox" value="" name="isMandatoryField"/></div><br/><div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Basic Search</div><div style="float:left;"><input type="checkbox" id="basicSearchField" class="clsTextBox" value="" name="basicSearchField"/></div></br><div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Private</div><div style="float:left;"><input type="checkbox" id="privateField" class="clsTextBox" value="" name="privateField" checked /></div></br>';
		
		if(frm == 'Account'){
			fieldsInner = fieldsInner + '<div style="float:left;width:250px;text-align:left;"  id="labelDiv" class="smlBlackBold">Is Company Name</div><div style="float:left;"><input type="checkbox" id="isCompanyNameField" class="clsTextBox" value="" name="isCompanyNameField" onChange="CheckPrivate(this);" /></div></br><div style="float:left;width:250px;text-align:left;"  id="labelDiv" class="smlBlackBold">Is Zip Code</div><div style="float:left;"><input type="checkbox" id="isZipCodeField" class="clsTextBox" value="" name="isZipCodeField" onChange="CheckPrivate(this);" /></div></br><div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Field Settings</div><div style="float:left;"><input type="button" id="accountFieldSettingsBtn" class="ButtonWithGradient" value="Settings" name="accountFieldSettingsBtn" '+ settingBtnStyl+' onclick="javascript: addFieldParameters(\'Account\');"/></div></br>';
			document.getElementById('addAccountFieldsInner').innerHTML = fieldsInner;
		}
		else{
			fieldsInner = fieldsInner + '<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Is First Name</div><div style="float:left;"><input type="checkbox" id="isFirstNameField" class="clsTextBox" value="" name="isFirstNameField" onchange="javascript:CheckPrivate(this);" /></div></br><div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Is Last Name</div><div style="float:left;"><input type="checkbox" id="isLastNameField" class="clsTextBox" value="" name="isLastNameField" onchange="javascript:CheckPrivate(this);" /></div></br><div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Field Settings</div><div style="float:left;"><input type="button" id="contactFieldSettingsBtn" class="ButtonWithGradient" value="Settings" name="contactFieldSettingsBtn" '+ settingBtnStyl+' onclick="javascript: addFieldParameters(\'Contact\');"/></div></br>';
			document.getElementById('addContactFieldsInner').innerHTML = fieldsInner;
			console.log(fieldsInner);
		}	
}

function saveNewField(frm){
	var optionList ='';
	
	if(document.getElementById('optionsSelect') != null && document.getElementById('optionsSelect').options != undefined){
		for(i=0;i<document.getElementById('optionsSelect').options.length;i++)
		{
			if(optionList == '')
				optionList = document.getElementById('optionsSelect').options[i].value;
			else
				optionList = optionList +','+ document.getElementById('optionsSelect').options[i].value;				
		}
	}
		
	if(frm == 'Account'){
		if(document.getElementById('selectFieldType').value == '-1'){
			document.getElementById('showAddAccountFieldError').innerHTML = 'Please Select field type.';
			return;
		}
		if(document.getElementById('addAccountFieldsInner').innerHTML == '')
			return;
		//alert(document.getElementById('accountOptionSetId').value);
		var labelName = document.getElementById('fieldLabelName').value;
		var type = document.getElementById('selectFieldType').value;
		var isMandatory = (document.getElementById('isMandatoryField').checked)?1:0;
		var isCompanyName = (document.getElementById('isCompanyNameField').checked)?1:0;
		var isZipCode = (document.getElementById('isZipCodeField').checked)?1:0;
		var isBasicSearch = (document.getElementById('basicSearchField').checked)?1:0;
		var isPrivate = (document.getElementById('privateField').checked)?1:0;
		var dataType = document.getElementById('dataTypeSelect').value;
		var optionSetId = document.getElementById('accountOptionSetId').value;
		var accountKeywordId = document.getElementById('selectKeyword').value;

		$.post('../../ajax/ajax.addEditFields.php',
			{
				frmType:'Account',
				btnSave:'Save',
				companyId : companyId,
				labelName:labelName,
				type : type,
				dataType : dataType,
				isMandatory : isMandatory,
				isBasicSearch : isBasicSearch,
				optionList : optionList,
				optionSetId : optionSetId,
				accountKeywordId : accountKeywordId, 
				isCompanyName : isCompanyName,
				parameter : additionalFieldParameters,
				isPrivate : isPrivate
			},
			processAddNewAccountFieldData
		);
		
	}else if(frm == 'Contact'){
		if(document.getElementById('selectContactFieldType').value == '-1'){
			document.getElementById('showAddContactFieldError').innerHTML = 'Please Select field type.';
			return;
		}
		//alert(document.getElementById('selectGoogleContactFieldType').value);
		if(document.getElementById('addContactFieldsInner').innerHTML == '')
			return;
			/* Start JET-37*/
		if(document.getElementById('selectGoogleContactFieldTypeid') && document.getElementById('selectGoogleContactFieldTypeid').value != '-1'){
		var googleField = document.getElementById('selectGoogleContactFieldTypeid').value;
		}else{
			var googleField = null;
			}
		/*End JET-37*/
			
		//alert(document.getElementById('accountOptionSetId').value);
		var labelName = document.getElementById('fieldLabelName').value;
		var type = document.getElementById('selectContactFieldType').value;
		var isMandatory = (document.getElementById('isMandatoryField').checked)?1:0;
		var isFirstName = (document.getElementById('isFirstNameField').checked)?1:0;
		var isLastName = (document.getElementById('isLastNameField').checked)?1:0;
		var isBasicSearch = (document.getElementById('basicSearchField').checked)?1:0;
		var isPrivate = (document.getElementById('privateField').checked)?1:0;
		var dataType = document.getElementById('dataTypeSelectContact').value;
		var optionSetId = document.getElementById('contactOptionSetId').value;
		var contactAccountMapId = document.getElementById('selectAccount').value;
		var contactKeywordId = document.getElementById('selectKeyword').value;

		$.post('../../ajax/ajax.addEditFields.php',
			{
				frmType:'Contact',
				btnSave:'Save',
				companyId : companyId,
				labelName:labelName,
				type : type,
				dataType : dataType,
				isMandatory : isMandatory,
				isBasicSearch : isBasicSearch,
				isFirstName : isFirstName,
				isLastName : isLastName,
				optionList : optionList,
				optionSetId : optionSetId,
				contactAccountMapId : contactAccountMapId,
				contactKeywordId : contactKeywordId,
				isCompanyName : isCompanyName,
				parameter : additionalFieldParameters,
				isPrivate : isPrivate,
				/* Start JET-37*/
				googleField : googleField
				/* End JET-37*/
			},
			processAddNewContactFieldData
		);
	}
}

function processAddNewAccountFieldData(result){

	additionalFieldParameters = '';
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}
	
	if(result.STATUS=='OK')
	{
		var newField = document.getElementById('fieldLabelName').value;
		document.getElementById('showAddAccountFieldError').innerHTML = '<div style="color:blue;">To add another field please select a field type else click Done.</div>';
		document.getElementById('addAccountFieldsInner').innerHTML = '';
		/*Start Jet-37*/
		document.getElementById('getGoogleContactField').innerHTML = '';
		/*Start Jet-37*/
		document.getElementById('selectFieldType').value = '-1';
		document.getElementById('accountCancelBtn').disabled = true;
		var ni = document.getElementById('showAddedAccountFields');
		var newdiv = document.createElement('div');
		newdiv.style.height = '30px';
		newdiv.style.float='none';
		newdiv.innerHTML = '<div class="clsTotalDivSmallBox">'+newField+'</div>';
		ni.appendChild(newdiv);
		$.post('../../ajax/ajax.addEditFields.php',
			{
				frmType:'Account',
				action :'getMaxFieldValue',
				companyId : companyId
			},
			function(result)
			{
				result = JSON.decode(result);

				TOTALFIELDS = TOTALFIELDS + 1;
				tempX = new Array();
				tempX['AccountMapID'] = result.AccountMapID;
				tempX['LabelName'] = result.LabelName;
				ACCOUNTFIELDS[ACCOUNTFIELDS.length] = tempX;
				
				OPTIONSETS = result.OptionSet;
				OPTIONSETS_JSON = result.OptionSet;
				CONTACTOPTIONSETS_JSON = result.OptionSet;
				CONTACTOPTIONSETS = result.OptionSet;
				
				options = OPTIONSETS_JSON;
				try
				{
					options = eval(options);
				}
				catch(e)
				{
					options = JSON.decode(options);
				}	
				
				contactOptions = CONTACTOPTIONSETS_JSON;
				try
				{
					contactOptions = eval(options);
				}
				catch(e)
				{
					contactOptions = JSON.decode(options);
				}	
				
				manageCompany();	
			}
		);
	}else{
		var str='';
		for(i=0;i<result.ERROR.length;i++)
		{
			str=str+'<br/>'+result.ERROR[i];
		}
		document.getElementById('showAddAccountFieldError').innerHTML = str; 	
	}
}

function processAddNewContactFieldData(result){
	additionalFieldParameters = '';
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}
	
	if(result.STATUS=='OK')
	{
		var newField = document.getElementById('fieldLabelName').value;
		document.getElementById('addContactFieldsInner').innerHTML = '';
		document.getElementById('showAddContactFieldError').innerHTML = '<div style="color:blue;">To add another field please select a field type else click Done.</div>';
		document.getElementById('selectContactFieldType').value = '-1';
		document.getElementById('contactCancelBtn').disabled = true;
		var ni = document.getElementById('showAddedContactFields');
		var newdiv = document.createElement('div');
		newdiv.style.height = '30px';
		newdiv.style.float='none';
		newdiv.innerHTML = '<div class="clsTotalDivSmallBox">'+newField+'</div>';
		ni.appendChild(newdiv);	
		
		$.post('../../ajax/ajax.addEditFields.php',
			{
				frmType:'Contact',
				action :'getMaxFieldValue',
				companyId : companyId
			},
			function(result)
			{
				result = JSON.decode(result);
				CONTACTTOTALFIELDS = CONTACTTOTALFIELDS + 1;
				tempX = new Array();
				tempX['ContactMapID'] = result.ContactMapID;
				tempX['LabelName'] = result.LabelName;
				CONTACTFIELDS[CONTACTFIELDS.length] = tempX;
				
				OPTIONSETS = result.OptionSet;
				OPTIONSETS_JSON = result.OptionSet;
				CONTACTOPTIONSETS_JSON = result.OptionSet;
				CONTACTOPTIONSETS = result.OptionSet;
				options = OPTIONSETS_JSON;
				try
				{
					options = eval(options);
				}
				catch(e)
				{
					options = JSON.decode(options);
				}	
				
				contactOptions = CONTACTOPTIONSETS_JSON;
				try
				{
					contactOptions = eval(options);
				}
				catch(e)
				{
					contactOptions = JSON.decode(options);
				}	
				
				manageContact();	
			}
		);
	}else{
		var str='';
		for(i=0;i<result.ERROR.length;i++)
		{
			str=str+'<br/>'+result.ERROR[i];
		}
		document.getElementById('showAddContactFieldError').innerHTML = str; 	
	}
}

function addFieldParameters(frmType){
	
	$.post('../../ajax/ajax.addEditFields.php',
	{
		action :'getFieldParameters'
	},
	function(result)
	{
		result = JSON.decode(result);
		var numberFormatArr = result.NumberFormat;
		var yearRangeArr = result.AllYear;
		
		var formatField = '<div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Field Format</div><div id="selectBoxResize"><select id="fieldFormat" style="width:100px;font-family: Verdana, Helvetica, Tahoma, Arial;border: 1px solid #808080;font-size: 8pt;" name="fieldFormat" class="selectbox"><option value="-1">-</option>';
		for(i=0;i<numberFormatArr.length;i++){
				formatField = formatField + '<option value="'+ numberFormatArr[i]+'">'+ numberFormatArr[i]+'</option>';
		}
		formatField = formatField +'</select></div><br/>';
		
		var startYearField = '<div style="float:left;width:150px;text-align:left;display:none;"  id="startYearLabel" class="smlBlackBold">Start Year</div><div id="selectBoxResize"><select id="startYear" style="width:100px;font-family: Verdana, Helvetica, Tahoma, Arial;border: 1px solid #808080;font-size: 8pt;display:none;" name="startYear" class="selectbox"><option value="CurrentYear">Current Year</option>';
		for(i=0;i<yearRangeArr.length;i++){
				startYearField = startYearField + '<option value="'+ yearRangeArr[i]+'">'+ yearRangeArr[i]+'</option>';
		}
		startYearField = startYearField +'</select></div><br/>';
		
		var endYearField = '<div style="float:left;width:150px;text-align:left;display:none;"  id="endYearLabel" class="smlBlackBold">End Year</div><div id="selectBoxResize"><select id="endYear" style="width:100px;font-family: Verdana, Helvetica, Tahoma, Arial;border: 1px solid #808080;font-size: 8pt;display:none;" name="endYear" class="selectbox"><option value="CurrentYear">Current Year</option>';
		for(i=0;i<yearRangeArr.length;i++){
				endYearField = endYearField + '<option value="'+ yearRangeArr[i]+'">'+ yearRangeArr[i]+'</option>';
		}
		endYearField = endYearField +'</select></div><br/>';
		
		var paramInner = '';
		if(frmType == 'Account'){
			var fieldType = document.getElementById('selectFieldType').value;
		}else if(frmType == 'Contact'){
			var fieldType = document.getElementById('selectContactFieldType').value;
		}
			if(fieldType == 'Text'){
				paramInner = paramInner + '<div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Field Width</div><div><input type="text" id="fieldWidth" class="clsTextBox" value="" name="fieldWidth"/></div></br><div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Field Height</div><div><input type="text" id="fieldHeight" class="clsTextBox" value="" name="fieldHeight"/></div><BR/><div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Max Length</div><div><input type="text" id="fieldLength" class="clsTextBox" value="" name="fieldLength"/></div>';	
			}else if(fieldType == 'Number'){
				paramInner = paramInner + '<div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Field Width</div><div><input type="text" id="fieldWidth" class="clsTextBox" value="" name="fieldWidth"/></div><BR/><div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Decimal Places</div><div><input type="text" id="decimalPlace" class="clsTextBox" value="" name="decimalPlace"/></div><BR/><div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Max Length</div><div><input type="text" id="fieldLength" class="clsTextBox" value="" name="fieldLength"/></div><br/>'+formatField;
				
			}else if(fieldType == 'Date'){
				paramInner = paramInner + '<div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Past Date Only</div><div><input type="checkbox" id="isPastDate" class="clsTextBox" value="" name="isPastDate"/></div></br><div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Year Range</div><div><input type="checkbox" id="hasYearRange" class="clsTextBox" value="" name="hasYearRange" onClick="javascript: checkYearField();"/></div><BR/><div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Month/Date only</div><div><input type="checkbox" id="isMD" class="clsTextBox" value="" name="isMD"/></div><br/>'+ startYearField + endYearField;	
			}
			document.getElementById('fieldParametersInner').innerHTML = paramInner;
			if(frmType == 'Account'){
				$('#addAccountFieldsLightBox').block({ message: $('#additionalFieldParamDiv'),
					css: { 
					padding: '10px',
					backgroundColor: '#FFFFFF',
					opacity: '.9', 
					color: '#000000',
					top:  (jQuery(window).height() - jQuery('#additionalFieldParamDiv').height()) /4 + 'px', 
					left: (jQuery(window).width() - jQuery('#additionalFieldParamDiv').width()) /3 + 'px', 
					width: '455px' ,
					height:'355px',
					overflow:'scroll' 
				}});		
			}else{
				$('#addContactFieldsLightBox').block({ message: $('#additionalFieldParamDiv'),
					css: { 
					padding: '10px',
					backgroundColor: '#FFFFFF',
					opacity: '.9', 
					color: '#000000',
					top:  (jQuery(window).height() - jQuery('#additionalFieldParamDiv').height()) /4 + 'px', 
					left: (jQuery(window).width() - jQuery('#additionalFieldParamDiv').width()) /3 + 'px', 
					width: '455px' ,
					height:'355px',
					overflow:'scroll' 
				}});
			}
		}
	);
}

function cancelFieldParam(){
	if(loadForm == 'Account')
		$('#addAccountFieldsLightBox').unblock();
	else
		$('#addContactFieldsLightBox').unblock();
		
	document.getElementById('fieldParametersError').innerHTML = '';
}

function checkYearField(){
	if(document.getElementById('hasYearRange').checked){
		document.getElementById('startYearLabel').style.display = 'block';
		document.getElementById('startYear').style.display = 'block';
		document.getElementById('endYearLabel').style.display = 'block';
		document.getElementById('endYear').style.display = 'block';
	}else{
		document.getElementById('startYearLabel').style.display = 'none';
		document.getElementById('startYear').style.display = 'none';
		document.getElementById('endYearLabel').style.display = 'none';
		document.getElementById('endYear').style.display = 'none';	
	}
}

function saveFieldParam(fieldType){
	var submitType = 'Update';
	document.getElementById('fieldParametersError').innerHTML = '';
	document.getElementById('editFieldParamError').innerHTML = '';
	if(fieldType == ''){
		if(loadForm == 'Account'){
			fieldType = document.getElementById('selectFieldType').value;  
		}else{
			fieldType = document.getElementById('selectContactFieldType').value;
		}
		submitType = 'Save';
	}
	
	var err = '';
	if(fieldType == 'Text'){
		var width = document.getElementById('fieldWidth').value	;
		var height = document.getElementById('fieldHeight').value;
		var length = document.getElementById('fieldLength').value;
		
		if(!is_valid_number(trim(width)) && trim(width) != ''){
			err = 'Please enter numeric value for "Field Width".';
			if(submitType == 'Save')
				document.getElementById('fieldParametersError').innerHTML = document.getElementById('fieldParametersError').innerHTML +'<br/>'+err;
			else
				document.getElementById('editFieldParamError').innerHTML = document.getElementById('editFieldParamError').innerHTML +'<br/>'+err;
		}
		if(!is_valid_number(trim(height)) && trim(height) != ''){
			err = 'Please enter numeric value for "Field Height" .';
			if(submitType == 'Save')
				document.getElementById('fieldParametersError').innerHTML = document.getElementById('fieldParametersError').innerHTML +'<br/>'+err;
			else
				document.getElementById('editFieldParamError').innerHTML = document.getElementById('editFieldParamError').innerHTML +'<br/>'+err;
		}
		if(!is_valid_number(trim(length)) && trim(length) != ''){
			err = 'Please enter numeric value for "Max Length" .';
			if(submitType == 'Save')
				document.getElementById('fieldParametersError').innerHTML = document.getElementById('fieldParametersError').innerHTML +'<br/>'+err;
			else
				document.getElementById('editFieldParamError').innerHTML = document.getElementById('editFieldParamError').innerHTML +'<br/>'+err;
		}
	}else if(fieldType == 'Number'){
		var width = document.getElementById('fieldWidth').value	;
		var decimal = document.getElementById('decimalPlace').value;
		var length = document.getElementById('fieldLength').value;
		
		if(!is_valid_number(trim(width)) && trim(width) != ''){
			err = 'Please enter numeric value for "Field Width".';
			if(submitType == 'Save')
				document.getElementById('fieldParametersError').innerHTML = document.getElementById('fieldParametersError').innerHTML +'<br/>'+err;
			else
				document.getElementById('editFieldParamError').innerHTML = document.getElementById('editFieldParamError').innerHTML +'<br/>'+err;
		}
		if(!is_valid_number(trim(decimal)) && trim(decimal) != ''){
			err = 'Please enter numeric value for "Decimal Places" .';
			if(submitType == 'Save')
				document.getElementById('fieldParametersError').innerHTML = document.getElementById('fieldParametersError').innerHTML +'<br/>'+err;
			else
				document.getElementById('editFieldParamError').innerHTML = document.getElementById('editFieldParamError').innerHTML +'<br/>'+err;
		}
		if(!is_valid_number(trim(length)) && trim(length) != ''){
			err = 'Please enter numeric value for "Max Length" .';
			if(submitType == 'Save')
				document.getElementById('fieldParametersError').innerHTML = document.getElementById('fieldParametersError').innerHTML +'<br/>'+err;
			else
				document.getElementById('editFieldParamError').innerHTML = document.getElementById('editFieldParamError').innerHTML +'<br/>'+err;
		}
	}else if(fieldType == 'Date'){
		var noOfFieldParameters = 0;
		if(document.getElementById('isPastDate').checked)
			noOfFieldParameters++;
		if(document.getElementById('hasYearRange').checked)
			noOfFieldParameters++;
		if(document.getElementById('isMD').checked)
			noOfFieldParameters++;
			
		if(noOfFieldParameters > 1){
			err = 'You can set only one parameter for a date field.';
			if(submitType == 'Save')
				document.getElementById('fieldParametersError').innerHTML = document.getElementById('fieldParametersError').innerHTML +'<br/>'+err;
			else
				document.getElementById('editFieldParamError').innerHTML = document.getElementById('editFieldParamError').innerHTML +'<br/>'+err;
		}else{
			if(document.getElementById('hasYearRange').checked){
				var startYear = '';
				var endYear = '';
				var today = new Date();

				if(document.getElementById('startYear').value == 'CurrentYear')
					startYear = today.getFullYear();
				else
					startYear = document.getElementById('startYear').value;
					
				if(document.getElementById('endYear').value == 'CurrentYear')
					endYear = today.getFullYear();
				else
					endYear = document.getElementById('endYear').value;	
					
				if(startYear > endYear){
					err = 'Start year should be smaller than End Year.';
					
					if(submitType == 'Save')
						document.getElementById('fieldParametersError').innerHTML = document.getElementById('fieldParametersError').innerHTML +'<br/>'+err;
					else
						document.getElementById('editFieldParamError').innerHTML = document.getElementById('editFieldParamError').innerHTML +'<br/>'+err;
				}
			}	
		}
	}
	if(err != ''){
		return false;
	}else{
		document.getElementById('fieldParametersError').innerHTML = '';
		
		/*Additional field parameters are set with |(Pipe) as the delimiter in the following order:
		 Width,Height,Length,Format,Decimal,IsPastDate,IsMonthDay,HasYearRange,StartYear,EndYear.*/
		if(fieldType == 'Text'){
			var width = document.getElementById('fieldWidth').value	;
			var height = document.getElementById('fieldHeight').value;
			var length = document.getElementById('fieldLength').value;	
			additionalFieldParameters = width+'|'+height+'|'+length+'|||||||';
		}else if(fieldType == 'Number'){
			var width = document.getElementById('fieldWidth').value	;
			var decimal = document.getElementById('decimalPlace').value;
			var length = document.getElementById('fieldLength').value;
			var format = document.getElementById('fieldFormat').value;
			additionalFieldParameters = width+'||'+length+'|'+ format+'|'+decimal+'|||||';
		}else if(fieldType == 'Date'){
			var startYear = '';
			var endYear = '';
			var isPastDate = (document.getElementById('isPastDate').checked)?1:0;
			var isDM = (document.getElementById('isMD').checked)?1:0;
			var hasYrRange = (document.getElementById('hasYearRange').checked)?1:0;
			if(hasYrRange == 1){
				var startYear = document.getElementById('startYear').value;
				var endYear = document.getElementById('endYear').value;
			}
			additionalFieldParameters = '|||||'+isPastDate+'|'+isDM+'|'+hasYrRange+'|'+startYear+'|'+endYear;
		}
		if(loadForm == 'Account' && submitType == 'Save'){
			$('#addAccountFieldsLightBox').unblock();
		}else if(loadForm == 'Contact' && submitType == 'Save'){
			$('#addContactFieldsLightBox').unblock();
		}else if(submitType == 'Update'){
			return true;
		}
	}
}

function cancelAddFieldsLightBox(frm){
	additionalFieldParameters = '';
	if(frm == 'Account'){
		document.getElementById('selectFieldType').value = '-1';
		document.getElementById('addAccountFieldsInner').innerHTML = '';
		document.getElementById('showAddAccountFieldError').innerHTML = '';
		$.unblockUI();
	}
	else if(frm == 'Contact'){
		document.getElementById('selectContactFieldType').value = '-1';
		document.getElementById('addContactFieldsInner').innerHTML = '';
		/*Start Jet-37*/
		document.getElementById('getGoogleContactField').innerHTML = '';
		/*Start Jet-37*/
		document.getElementById('showAddContactFieldError').innerHTML = '';
		$.unblockUI();
	}	
}

function doneAddingFields(frm){
	additionalFieldParameters = '';
	if(frm == 'Account'){
		document.getElementById('selectFieldType').value = '-1';
		document.getElementById('addAccountFieldsInner').innerHTML = '';
		document.getElementById('showAddAccountFieldError').innerHTML = '';
		document.getElementById('showAddedAccountFields').innerHTML = '';
		document.getElementById('accountCancelBtn').disabled = false;
		$.unblockUI();
	}
	else if(frm == 'Contact'){
		document.getElementById('selectContactFieldType').value = '-1';
		document.getElementById('addContactFieldsInner').innerHTML = '';
		/*Start Jet-37*/
		document.getElementById('getGoogleContactField').innerHTML = '';
		/*Start Jet-37*/
		document.getElementById('showAddContactFieldError').innerHTML = '';
		document.getElementById('showAddedContactFields').innerHTML = '';
		document.getElementById('contactCancelBtn').disabled = false;
		$.unblockUI();
	}	
}
var editFieldMapId = '';
var fieldTabEdit = '';
var newFieldName = '';
function editFieldDetails(fields){
	if(fields.className != 'clsTotalDivSmallBox'){
		
		var fieldInner = fields.innerHTML;
		var strtPos = fieldInner.lastIndexOf('[');
		var fieldId = fields.id;
		var sepPos = fieldId.lastIndexOf('_');
		var fMapId = fieldId.substring((sepPos + 1));
		fieldTabEdit = fieldId;
		editFieldMapId = fMapId;
		if(loadForm == 'Account'){
			$.post('../../ajax/ajax.addEditFields.php',
				{
					frmType:'Account',
					action :'EditFeild',
					companyId : companyId,
					fieldMapId : fMapId
				},
				processEditAccountFieldData
			);
		}else if(loadForm == 'Contact'){
			$.post('../../ajax/ajax.addEditFields.php',
				{
					frmType:'Contact',
					action :'EditFeild',
					companyId : companyId,
					fieldMapId : fMapId
				},
				processEditContactFieldData
			);
		}
	}else{
		alert('Sorry you cannot modify an unassigned field.');
	}	
}

function processEditAccountFieldData(result){
	
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}

	var optionType = result[0]['OptionType'];
	var labelName = result[0]['LabelName'];
	var basicSearch = (result[0]['BasicSearch'] == 1)?'checked':'';
	var isPrivate = (result[0]['IsPrivate'] == 1)?'checked':'';
	var isCompanyName = (result[0]['IsCompanyName'] == 1)?'checked':'';
	var isZipCode = (result[0]['IsZipCode'] == 1)?'checked':'';
	var isMandatory = (result[0]['IsRequired'] == 1)?'checked':'';
	var tabOrder = result[0]['TabOrder'];
	var optionDetails = result[0]['OptionDetails'];
	var fieldType = result[0]['FieldType'];
	var parameter = result[0]['Parameters'];
	var mapId = result[0]['AccountMapID'];
	var accKeywordId = result[0]['KeywordId'];
	createEditFormInner('Account',labelName,basicSearch,isCompanyName,isZipCode,isMandatory,tabOrder,optionType,null,null,null,accKeywordId,optionDetails,fieldType,parameter,isPrivate,mapId);
}

function processEditContactFieldData(result){
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}
	var optionType = result[0]['OptionType'];
	var labelName = result[0]['LabelName'];
	var basicSearch = (result[0]['BasicSearch'] == 1)?'checked':'';
	var isPrivate = (result[0]['IsPrivate'] == 1)?'checked':'';
	var isFirstName = (result[0]['IsFirstName'] == 1)?'checked':'';
	var isLastName = (result[0]['IsLastName'] == 1)?'checked':'';
	var isMandatory = (result[0]['IsRequired'] == 1)?'checked':'';
	var tabOrder = result[0]['TabOrder'];
	var conKeywordId = result[0]['KeywordId'];
	var conAccountMapId = result[0]['AccountMapID'];
	var optionDetails = result[0]['OptionDetails'];
	var fieldType = result[0]['FieldType'];
	var parameter = result[0]['Parameters'];
	var mapId = result[0]['ContactMapID'];
	var googleField = result[0]['googleField'];
	var googleFieldName = result[0]['googleFieldName'];
	var googleFields = result[1]['googleFields'];
	/*Jet-37 */
	var isUseGoogleSync = result[1]['isUseGoogleSync'];
	
	createEditFormInner('Contact',labelName,basicSearch,null,null,isMandatory,tabOrder,optionType,isFirstName,isLastName,conAccountMapId,conKeywordId,optionDetails,fieldType,parameter,isPrivate,mapId,googleField,googleFields,googleFieldName,isUseGoogleSync);
	/*Jet-37 */
}

var editOptionList = new Array();
/*Jet-37 */
function createEditFormInner(frmType,labelName,basicSearch,isCompanyName,isZipCode,isMandatory,taborder,optionType,isFirstName,isLastName,conAccountMapId,conKeywordId,optionDetails,fieldType,parameter,isPrivate,mapId,googleField,googleFields,googleFieldName,isUseGoogleSync){
	//alert('1 ' + frmType + ' 2 ' + labelName + ' 3 ' + basicSearch + ' 4 ' + isCompanyName + ' 5 ' + isMandatory + ' 6 ' + taborder + ' 7 ' + optionType + ' 8 ' + isFirstName + ' 9 ' + isLastName + ' 10 ' + conAccountMapId + ' 11 ' + conKeywordId + ' 12 ' + optionDetails + ' 13 ' + fieldType + ' 14 ' + parameter + ' 15 ' + isPrivate + ' 16 ' + mapId);
	/*Jet-37 */
	editOptionList = optionDetails;
	var selAccount = '';
	var selKeyword = '';
	//var isgoogleField = '';
	var GoogleMap = '';
	var firstNameField = '';
	var lastNameField = '';
	var companyNameField = '';
	var zipCodeField = '';
	var selectAccountField = '';
	var selectKeywordField = '';
	var optionsField = '';
	var disableOptions = 'disabled';
	var settingBtnStyl = 'disabled';
	if(optionType == 1 || optionType == 2){
		disableOptions = '';
	}
	if(taborder == null){
		taborder = '';	
	}
	if(fieldType == 'Text' || fieldType == 'Number' || fieldType == 'Date'){
		settingBtnStyl = '';
	}
	var paramSettingBtn = '<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Field Settings</div><div style="float:left;"><input type="button" id="contactFieldSettingsBtn" class="ButtonWithGradient" value="Settings" style="width:100px;" name="contactFieldSettingsBtn" '+ settingBtnStyl +' onclick="javascript: editFieldParam('+ mapId +');"/></div></br>' ;
	
	var editFieldTypeBtn = '<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Edit Field Type</div><div style="float:left;"><input type="button" id="editFieldTypeBtn" class="ButtonWithGradient" value="Field Type" style="width:100px;" name="editFieldTypeBtn" onclick=\'javascript: editFieldType("'+ fieldType +'");\'/></div></br>' ;
	
	var changeOptions = '<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Change Options</div><div style="float:left;"><input type="button" class="ButtonWithGradient" value="Options" '+ disableOptions +' style="width:100px;" onclick = "javascript:editOptionListFields(\''+frmType+'\');"/></div></br>';
	
	var tabOrderField = '<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Tab Order</div><div style="float:left;"><input type="text" id="tabOrder" class="clsTextBox" value="'+ taborder +'" maxlength="50" name="tabOrder"/></div><br/>';
	
	if(frmType == 'Account'){
		optionsField = '<div style="float:left;"><input type="text" id="optionsSelect" class="clsTextBox" value="Options" maxlength="50" disabled name="optionsSelect"/></div><br/>';
		companyNameField = '<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Is Company Name</div><div style="float:left;"><input type="checkbox" id="companyNameField" class="clsTextBox" value="" name="companyNameField" '+ isCompanyName +' onChange="CheckPrivate(this);" /></div></br>';
		zipCodeField = '<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Is Zip Code</div><div style="float:left;"><input type="checkbox" id="zipCodeField" class="clsTextBox" value="" name="zipCodeField" '+ isZipCode +' onChange="CheckPrivate(this);" /></div></br>';
		selectKeywordField = '<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Keyword Field</div><div style="float:left;" id="selectBoxResize"><select id="accKeyword" style="width:134px;font-family: Verdana, Helvetica, Tahoma, Arial;border: 1px solid #808080;font-size: 8pt;" name="acctKeyword" class="selectbox"><option value="-1">-</option>';
			for(k=0;k<ALLKEYWORDFIELDS.length;k++){
				if(ALLKEYWORDFIELDS[k]['KeywordId'] == conKeywordId)
					selKeyword = 'selected';	
				else
					selKeyword = '';
					
				selectKeywordField = selectKeywordField + '<option value="'+ALLKEYWORDFIELDS[k]['KeywordId']+'" '+ selKeyword+'>'+ALLKEYWORDFIELDS[k]['Keyword']+'</option>';
			}
			selectKeywordField = selectKeywordField +'</select></div><br/>';

			
	}else if(frmType == 'Contact'){
		firstNameField = '<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Is First Name</div><div style="float:left;"><input type="checkbox" id="firstNameField" class="clsTextBox" value="" name="firstNameField" '+ isFirstName +' onchange="javascript:CheckPrivate(this);" /></div></br>';
		lastNameField = '<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Is Last Name</div><div style="float:left;"><input type="checkbox" id="lastNameField" class="clsTextBox" value="" name="lastNameField" '+ isLastName +' onchange="javascript:CheckPrivate(this);" /></div></br>';
		selectAccountField = '<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Account Field</div><div style="float:left;" id="selectBoxResize"><select id="selectAccount" style="width:134px;font-family: Verdana, Helvetica, Tahoma, Arial;border: 1px solid #808080;font-size: 8pt;" name="selectAccount" class="selectbox"><option value="-1">-</option>';
			for(k=0;k<ALLACCOUNTFIELDS.length;k++){
				if(ALLACCOUNTFIELDS[k]['AccountMapID'] == conAccountMapId)
					selAccount = 'selected';	
				else
					selAccount = '';
					
				selectAccountField = selectAccountField + '<option value="'+ALLACCOUNTFIELDS[k]['AccountMapID']+'" '+ selAccount+'>'+ALLACCOUNTFIELDS[k]['LabelName']+'</option>';
			}
			selectAccountField = selectAccountField +'</select></div><br/>';
		selectKeywordField = '<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Keyword Field</div><div style="float:left;" id="selectBoxResize"><select id="selectKeyword" style="width:134px;font-family: Verdana, Helvetica, Tahoma, Arial;border: 1px solid #808080;font-size: 8pt;" name="selectKeyword" class="selectbox"><option value="-1">-</option>';
			for(k=0;k<ALLKEYWORDFIELDS.length;k++){
				if(ALLKEYWORDFIELDS[k]['KeywordId'] == conKeywordId)
					selKeyword = 'selected';	
				else
					selKeyword = '';
					
				selectKeywordField = selectKeywordField + '<option value="'+ALLKEYWORDFIELDS[k]['KeywordId']+'" '+ selKeyword+'>'+ALLKEYWORDFIELDS[k]['Keyword']+'</option>';
			}
			selectKeywordField = selectKeywordField +'</select></div><br/>';
			/*Jet-37 */
			//console.log(isUseGoogleSync);
			//console.log(googleField);
			//console.log(googleFields[0]['googleField']);
			if(isUseGoogleSync){
			GoogleMap = '<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Google Contact Mapping</div><div id="selectBoxResize" style="float:left;"><select id="selectGoogleContactFieldTypeid" name="selectGoogleContactFieldTypeid" class="clsTextBox" style="width:134px;font-family: Verdana, Helvetica, Tahoma, Arial;border: 1px solid #808080;font-size: 8pt;" ><option value="-1">Select</option>';
			/*Jet-37 Null issue*/
			if(googleField && googleField != 'null' && googleField != '' && googleField != '-1'){
				/*Jet-37 Null issue*/
				console.log(googleField);
			GoogleMap += '<option value="'+googleField+'" selected >'+googleFieldName+'</option>';
			}
	for(k=0;k<googleFields.length;k++){
				GoogleMap += '<option value="'+googleFields[k]['googleField']+'" >'+googleFields[k]['googleFieldName']+'</option>';
		}
		GoogleMap += '<select></div><br/>';
		}else{
			GoogleMap = '';
			}
		/*Jet-37 */
	}

	

	var fieldsInner = '<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Field Type</div><div style="float:left;"><input type="text" id="fieldTypeName" class="clsTextBox" value="'+ fieldType +'" maxlength="50" name="fieldTypeName" disabled/></div><br/><div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Label Name</div><div style="float:left;"><input type="text" id="fieldLabelName" class="clsTextBox" value="'+ labelName +'" maxlength="50" name="fieldLabelName"/></div><br/>'+GoogleMap+' '+ tabOrderField +''+ selectAccountField +' '+ selectKeywordField +' '+ changeOptions +' '+ paramSettingBtn +' '+ editFieldTypeBtn +'<div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Mandatory</div><div style="float:left;"><input type="checkbox" id="isMandatoryField" class="clsTextBox" value="" name="isMandatoryField" '+ isMandatory +'/></div><br/><div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Basic Search</div><div style="float:left;"><input type="checkbox" id="basicSearchField" class="clsTextBox" value="" name="basicSearchField" '+ basicSearch +'/><input type="hidden" id="optionTypeField" name="optionTypeField" value="'+ optionType +'"></div></br><div style="float:left;width:250px;text-align:left"  id="labelDiv" class="smlBlackBold">Private</div><div style="float:left;"><input type="checkbox" id="privateField" class="clsTextBox" value="" name="privateField" '+ isPrivate +'/></div><br/>'+ companyNameField +' '+ zipCodeField +' '+ firstNameField +' '+ lastNameField  ;
	
	if(frmType == 'Account'){
		document.getElementById('editAccountFieldsInner').innerHTML = fieldsInner;
		$.blockUI({ message: $('#editAccountFieldsLightBox'),
			css: { 
			padding: '10px',
			backgroundColor: '#FFFFFF',
			opacity: '.9', 
			color: '#000000',
			top:  (jQuery(window).height() - jQuery('#editAccountFieldsLightBox').height()) /4 + 'px', 
			left: (jQuery(window).width() - jQuery('#editAccountFieldsLightBox').width()) /2 + 'px', 
			width: '500px',
			height:'400px',
			overflow:'scroll'
		}});
		/*if(optionType == 1 || optionType == 2){
			editOptionListFields(optionDetails,'Account');
		}*/
	}else if(frmType == 'Contact'){
		document.getElementById('editContactFieldsInner').innerHTML = fieldsInner;
		$.blockUI({ message: $('#editContactFieldsLightBox'),
			css: { 
			padding: '10px',
			backgroundColor: '#FFFFFF',
			opacity: '.9', 
			color: '#000000',
			top:  (jQuery(window).height() - jQuery('#editContactFieldsLightBox').height()) /4 + 'px', 
			left: (jQuery(window).width() - jQuery('#editContactFieldsLightBox').width()) /2 + 'px', 
			width: '500px',
			height:'400px',
			overflow:'scroll'
		}});
		/*if(optionType == 1 || optionType == 2){
			editOptionListFields(optionDetails,'Contact');
		}*/
	}
}

function cancelEditFieldsLightBox(frm){
	if(frm == 'Account'){
		document.getElementById('editAccountFieldsInner').innerHTML = '';
		document.getElementById('showEditAccountFieldError').innerHTML = '';
	}
	else{
		document.getElementById('editContactFieldsInner').innerHTML = '';
		document.getElementById('showEditContactFieldError').innerHTML = '';
	}
		
	$.unblockUI() ;
}

function cancelEditFieldType(){
	if(loadForm == 'Account')
		$('#editAccountFieldsLightBox').unblock();
	else
		$('#editContactFieldsLightBox').unblock();
}

function saveEditFieldType(){
	var fieldMapId = editFieldMapId;
	var newFieldType = document.getElementById('selectEditFieldType').value;
	if(loadForm == 'Account'){
		$.post('../../ajax/ajax.addEditFields.php',
			{
				frmType:'Account',
				action :'EditFieldType',
				companyId : companyId,
				fieldMapId : fieldMapId,
				newFieldType : newFieldType
			},
			processEditFieldType
		);
		
	}else if(loadForm == 'Contact'){
		$.post('../../ajax/ajax.addEditFields.php',
			{
				frmType:'Contact',
				action :'EditFieldType',
				companyId : companyId,
				fieldMapId : fieldMapId,
				newFieldType : newFieldType
			},
			processEditFieldType
		);
	}		
}

function processEditFieldType(result){
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}
	if(result.STATUS == 'OK')
	{
		document.getElementById('fieldTypeName').value = document.getElementById('selectEditFieldType').value;
	}
	cancelEditFieldType();
}

function editFieldType(fieldType){
	var editTypeHtml = '';
	if(fieldType == 'Text' || fieldType == 'Url' || fieldType == 'Email' || fieldType == 'Phone'){
		editTypeHtml = '<div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Field Type</div><div style="float:left;"><input type="text" id="fldType" class="clsTextBox" style="width:94px;" value="'+ fieldType +'" maxlength="50" name="fldType" disabled/></div><BR/><BR/><div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Change Field Type</div><div style="float:left;" id="selectBoxResize"><select  name="selectEditFieldType" id="selectEditFieldType" style="width:94px;font-family: Verdana, Helvetica, Tahoma, Arial;border: 1px solid #808080;font-size: 8pt;"><option value="Text">Text</option><option value="Url">Url</option><option value="Email">Email</option><option value="Phone">Phone</option></select></div>';


	}else if(fieldType == 'Dropdown' || fieldType == 'Radio Button'){
		editTypeHtml = '<div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Field Type</div><div style="float:left;"><input type="text" id="fldType" class="clsTextBox" style="width:94px;" value="'+ fieldType +'" maxlength="50" name="fldType" disabled/></div><BR/><BR/><div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Change Field Type</div><div style="float:left;" id="selectBoxResize"><select  name="selectEditFieldType" id="selectEditFieldType" style="width:94px;font-family: Verdana, Helvetica, Tahoma, Arial;border: 1px solid #808080;font-size: 8pt;"><option value="DropDown">DropDown</option><option value="RadioButton">RadioButton</option></select></div>';
	}else{
		alert("Sorry you can not change field type for " + fieldType + ".");
		return true;	
	}
	
	document.getElementById('editFieldTypeInner').innerHTML = editTypeHtml;
	if(loadForm == 'Account'){
			$('#editAccountFieldsLightBox').block({ message: $('#editFieldTypeDiv'),
				css: { 
				padding: '10px',
				backgroundColor: '#FFFFFF',
				opacity: '.9', 
				color: '#000000',
				top:  (jQuery(window).height() - jQuery('#editFieldTypeDiv').height()) /4 + 'px', 
				left: (jQuery(window).width() - jQuery('#editFieldTypeDiv').width()) /3 + 'px', 
				width: '436px' ,
				height:'340px'
				//overflow:'scroll' 
			}});			
		}else{
			$('#editContactFieldsLightBox').block({ message: $('#editFieldTypeDiv'),
				css: { 
				padding: '10px',
				backgroundColor: '#FFFFFF',
				opacity: '.9', 
				color: '#000000',
				top:  (jQuery(window).height() - jQuery('#editFieldTypeDiv').height()) /4 + 'px', 
				left: (jQuery(window).width() - jQuery('#editFieldTypeDiv').width()) /3 + 'px', 
				width: '436px' ,
				height:'340px'
				//overflow:'scroll' 
			}});
		}
}

function editFieldParam(mapId){
	if(loadForm == 'Account'){
		$.post('../../ajax/ajax.addEditFields.php',
			{
				frmType:'Account',
				action :'GetSelectedFieldParams',
				companyId : companyId,
				fieldMapId : mapId
			},
			processEditFieldParam
		);
		
	}else if(loadForm == 'Contact'){
		$.post('../../ajax/ajax.addEditFields.php',
			{
				frmType:'Contact',
				action :'GetSelectedFieldParams',
				companyId : companyId,
				fieldMapId : mapId
			},
			processEditFieldParam
		);
	}		
}
var editParamFieldType = '';
var editParamFieldId = '';
function processEditFieldParam(result){
	
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}
	/*Additional field parameters are set with |(Pipe) as the delimiter in the following order:
		 Width,Height,Length,Format,Decimal,IsPastDate,IsMonthDay,HasYearRange,StartYear,EndYear.*/
	

	if(result.STATUS=='OK')
	{
		document.getElementById('editFieldParametersInner').innerHTML = '';
		var param = result.PARAMETERS['Parameters'];
		var field = result.PARAMETERS['FieldName'];
		var fieldType = field.substr(0,4);
		fieldType = (fieldType == 'Numb')?'Number':fieldType;
		editParamFieldId = result.PARAMETERS['MapId'];
		if((fieldType == 'Text' || fieldType == 'Date' || fieldType == 'Number') && param == null){
			param = '|||||||||'	;
		}
		
		if(param != null){
			var paramArr = param.split('|');
			var fWidth = paramArr[0];
			var fHeight = paramArr[1];
			var fLength = paramArr[2];
			var fFormat = paramArr[3];
			var fDecimal = paramArr[4];
			var isPastDate = (paramArr[5] == 1)?'checked':'';
			var isMonthDay = (paramArr[6] == 1)?'checked':'';
			var hasYearRange = (paramArr[7] == 1)?'checked':'';
			var strtYear = paramArr[8];
			var endYear = paramArr[9];
			editParamFieldType = fieldType;
			$.post('../../ajax/ajax.addEditFields.php',
			{
				action :'getFieldParameters'
			},
			function(result)
			{
				result = JSON.decode(result);
				var numberFormatArr = result.NumberFormat;
				var yearRangeArr = result.AllYear;
				
				var formatField = '<div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Field Format</div><div id="selectBoxResize"><select id="fieldFormat" style="width:100px;font-family: Verdana, Helvetica, Tahoma, Arial;border: 1px solid #808080;font-size: 8pt;" name="fieldFormat" class="selectbox"><option value="-1">-</option>';
				for(i=0;i<numberFormatArr.length;i++){
					if(numberFormatArr[i] == fFormat){
						formatField = formatField + '<option value="'+ numberFormatArr[i]+'" "Selected">'+ numberFormatArr[i]+'</option>';
					}else{
						formatField = formatField + '<option value="'+ numberFormatArr[i]+'">'+ numberFormatArr[i]+'</option>';
					}		
				}
				formatField = formatField +'</select></div><br/>';
				
				var startYearField = '<div style="float:left;width:150px;text-align:left;display:none;"  id="startYearLabel" class="smlBlackBold">Start Year</div><div id="selectBoxResize"><select id="startYear" style="width:100px;font-family: Verdana, Helvetica, Tahoma, Arial;border: 1px solid #808080;font-size: 8pt;display:none;" name="startYear" class="selectbox"><option value="CurrentYear">Current Year</option>';
				for(i=0;i<yearRangeArr.length;i++){
						if(yearRangeArr[i] == strtYear){
							startYearField = startYearField + '<option value="'+ yearRangeArr[i]+'" "selected">'+ yearRangeArr[i]+'</option>';
						}else{
							startYearField = startYearField + '<option value="'+ yearRangeArr[i]+'">'+ yearRangeArr[i]+'</option>';	
						}
				}
				startYearField = startYearField +'</select></div><br/>';
				
				var endYearField = '<div style="float:left;width:150px;text-align:left;display:none;"  id="endYearLabel" class="smlBlackBold">End Year</div><div id="selectBoxResize"><select id="endYear" style="width:100px;font-family: Verdana, Helvetica, Tahoma, Arial;border: 1px solid #808080;font-size: 8pt;display:none;" name="endYear" class="selectbox"><option value="CurrentYear">Current Year</option>';
				for(i=0;i<yearRangeArr.length;i++){
					if(yearRangeArr[i] == endYear){
						endYearField = endYearField + '<option value="'+ yearRangeArr[i]+'" "Selected">'+ yearRangeArr[i]+'</option>';	
					}else{
						endYearField = endYearField + '<option value="'+ yearRangeArr[i]+'">'+ yearRangeArr[i]+'</option>';	
					}
					
				}
				endYearField = endYearField +'</select></div><br/>';
				
				var paramInner = '';
				if(fieldType == 'Text'){
					paramInner = paramInner + '<div style="float:left;width:150px;text-align:left;z-index:99999;"  id="labelDiv" class="smlBlackBold">Field Width</div><div><input type="text" id="fieldWidth" class="clsTextBox" value="'+ fWidth +'" name="fieldWidth"/></div></br><div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Field Height</div><div><input type="text" id="fieldHeight" class="clsTextBox" value="'+ fHeight +'" name="fieldHeight"/></div><BR/><div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Max Length</div><div><input type="text" id="fieldLength" class="clsTextBox" value="'+ fLength +'" name="fieldLength"/></div>';	
				}else if(fieldType == 'Number'){
					paramInner = paramInner + '<div style="float:left;width:150px;text-align:left;z-index:99999;"  id="labelDiv" class="smlBlackBold">Field Width</div><div><input type="text" id="fieldWidth" class="clsTextBox" value="'+ fWidth +'" name="fieldWidth"/></div><BR/><div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Decimal Places</div><div><input type="text" id="decimalPlace" class="clsTextBox" value="'+ fDecimal +'" name="decimalPlace"/></div><BR/><div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Max Length</div><div><input type="text" id="fieldLength" class="clsTextBox" value="'+ fLength +'" name="fieldLength"/></div><br/>'+formatField;
					
				}else if(fieldType == 'Date'){
					paramInner = paramInner + '<div style="float:left;width:150px;text-align:left;z-index:99999;"  id="labelDiv" class="smlBlackBold">Past Date Only</div><div><input type="checkbox" id="isPastDate" class="clsTextBox" value="" name="isPastDate" '+isPastDate+'/></div></br><div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Year Range</div><div><input type="checkbox" id="hasYearRange" class="clsTextBox" value="" '+hasYearRange+' name="hasYearRange" onClick="javascript: checkYearField();"/></div><BR/><div style="float:left;width:150px;text-align:left"  id="labelDiv" class="smlBlackBold">Month/Date only</div><div><input type="checkbox" id="isMD" class="clsTextBox" value="" name="isMD" '+isMonthDay+'/></div><br/>'+ startYearField + endYearField;		
				}
				$('#editFieldParametersInner').html(paramInner);
				if(hasYearRange){
					checkYearField();
					}
				}
			);
		}else{
			document.getElementById('editParamSave').disabled = true; 
			document.getElementById('editFieldParamError').innerHTML = 'Sorry this field does not have any parameters.'; 
		}
		if(loadForm == 'Account'){
			$('#editAccountFieldsLightBox').block({ message: $('#editFieldParamDiv'),
				css: { 
				padding: '10px',
				backgroundColor: '#FFFFFF',
				opacity: '.9', 
				color: '#000000',
				top:  (jQuery(window).height() - jQuery('#editFieldParamDiv').height()) /4 + 'px', 
				left: (jQuery(window).width() - jQuery('#editFieldParamDiv').width()) /3 + 'px', 
				width: '436px' ,
				height:'340px'
				//overflow:'scroll' 
			}});			
		}else{
			$('#editContactFieldsLightBox').block({ message: $('#editFieldParamDiv'),
				css: { 
				padding: '10px',
				backgroundColor: '#FFFFFF',
				opacity: '.9', 
				color: '#000000',
				top:  (jQuery(window).height() - jQuery('#editFieldParamDiv').height()) /4 + 'px', 
				left: (jQuery(window).width() - jQuery('#editFieldParamDiv').width()) /3 + 'px', 
				width: '436px' ,
				height:'340px'
				//overflow:'scroll' 
			}});
		}
	}else{
		var str='';
		for(i=0;i<result.error.length;i++)
		{
			str=str+'<br/>'+result.error[i];
		}
		alert(str); 	
	}
	
}

function cancelEditFieldParam(){
	additionalFieldParameters = '';
	document.getElementById('editFieldParamError').innerHTML = '';
	document.getElementById('editParamSave').disabled = false; 
	document.getElementById('editFieldParametersInner').innerHTML = '';
	if(loadForm == 'Account')
		$('#editAccountFieldsLightBox').unblock();
	else
		$('#editContactFieldsLightBox').unblock();
}

function saveEditFieldParam(){
	if(editParamFieldType != '')

		if(!saveFieldParam(editParamFieldType))
			return false;
		
	if(loadForm == 'Account'){
		$.post('../../ajax/ajax.addEditFields.php',
			{
				frmType:'Account',
				action :'SaveEditedFieldParams',
				mapId : editParamFieldId,
				parameters : additionalFieldParameters
			},
			processSaveEditFieldParam
		);	
		
	}else if(loadForm == 'Contact'){
		$.post('../../ajax/ajax.addEditFields.php',
			{
				frmType:'Contact',
				action :'SaveEditedFieldParams',
				mapId : editParamFieldId,
				parameters : additionalFieldParameters
			},
			processSaveEditFieldParam
		);	
	}		
		
}

function processSaveEditFieldParam(result){
	
	additionalFieldParameters = '';
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	} 
	if(result.status=='OK')
	{
		alert('Field parameters updated sucessfully.');
	}
	if(loadForm == 'Account')
		$('#editAccountFieldsLightBox').unblock();
	else
		$('#editContactFieldsLightBox').unblock();
}

function is_valid_number(val){
    var test = parseInt(val, 10);
    if (isNaN(test))
        return false;

    var validChars = '0123456789';
    for (var i = 0; i < val.length; ++i)
    {
        var ch = val.charAt(i);
        if (validChars.indexOf(ch) == -1) return false;
    }

    return true;
}

function updateEditField(frm){
	//alert(editFieldMapId);
	var labelName = document.getElementById('fieldLabelName').value;
	var tabOrder = document.getElementById('tabOrder').value;
	if(tabOrder != '' && tabOrder != 'null' && !is_valid_number(tabOrder)){
		document.getElementById('showEditAccountFieldError').innerHTML = 'TabOrder should be Numeric';
		return;
	}
	var isMandatory = (document.getElementById('isMandatoryField').checked)?1:0;
	var isBasicSearch = (document.getElementById('basicSearchField').checked)?1:0;
	var isPrivate = (document.getElementById('privateField').checked)?1:0;
	var optionType = document.getElementById('optionTypeField').value;
	var labelTab = '-';
	if(tabOrder != 'null' && tabOrder != '' )
		labelTab = tabOrder; 
		
	newFieldName = labelName + " [" + labelTab +"]";
	if(frm == 'Account'){
		var isCompanyName = (document.getElementById('companyNameField').checked)?1:0;
		var isZipCode = (document.getElementById('zipCodeField').checked)?1:0;
		var accKeywordId = document.getElementById('accKeyword').value;

		$.post('../../ajax/ajax.addEditFields.php',
			{
				frmType:'Account',
				action :'SaveEditFields',
				companyId : companyId,
				labelName:labelName,
				tabOrder : tabOrder,
				isMandatory : isMandatory,
				isBasicSearch : isBasicSearch,
				fieldMapId : editFieldMapId,
				optionType : optionType,
				isCompanyName : isCompanyName,
				isZipCode : isZipCode,
				accKeywordId : accKeywordId,
				isPrivate : isPrivate
			},
			processUpdateEditAccountFieldData
		);
	}else if(frm == 'Contact'){
		var isFirstName = (document.getElementById('firstNameField').checked)?1:0;
		var isLastName = (document.getElementById('lastNameField').checked)?1:0;
		var conAccountMapId = document.getElementById('selectAccount').value;
		var conKeywordId = document.getElementById('selectKeyword').value;
		var googleField = document.getElementById('selectGoogleContactFieldTypeid').value;

		$.post('../../ajax/ajax.addEditFields.php',
			{
				frmType:'Contact',
				action :'SaveEditFields',
				companyId : companyId,
				labelName:labelName,
				tabOrder : tabOrder,
				isMandatory : isMandatory,
				isBasicSearch : isBasicSearch,
				googleField : googleField,
				isFirstName : isFirstName,
				isLastName : isLastName,
				fieldMapId : editFieldMapId,
				optionType : optionType,
				conAccountMapId : conAccountMapId,
				conKeywordId : conKeywordId,
				isPrivate : isPrivate
			},
			processUpdateEditContactFieldData
		);
	}
}

function processUpdateEditAccountFieldData(result){
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}
	
	if(result.STATUS=='OK')
	{
		document.getElementById('editAccountFieldsInner').innerHTML = '';
		document.getElementById(fieldTabEdit).innerHTML = newFieldName;
		$.unblockUI() ;
	}else{
		var str='';
		for(i=0;i<result.ERROR.length;i++)
		{
			str=str+'<br/>'+result.ERROR[i];
		}
		document.getElementById('showEditAccountFieldError').innerHTML = str; 	
	}
}

function processUpdateEditContactFieldData(result){
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}
	
	if(result.STATUS=='OK')
	{
		document.getElementById('editContactFieldsInner').innerHTML = '';
		document.getElementById(fieldTabEdit).innerHTML = newFieldName;
		$.unblockUI() ;
	}else{
		var str='';
		for(i=0;i<result.ERROR.length;i++)
		{
			str=str+'<br/>'+result.ERROR[i];
		}
		document.getElementById('showEditContactFieldError').innerHTML = str; 	
	}
}

function deleteFieldsLightBox(frmType){
	if(frmType == 'Account'){
		$.blockUI({ message: $('#deleteAccountFieldsDiv'),
		css: { 
		padding: '10px',
		backgroundColor: '#FFFFFF',
		opacity: '.9', 
		color: '#000000',
		top:  (jQuery(window).height() - jQuery('#deleteAccountFieldsDiv').height()) /3 + 'px', 
		left: (jQuery(window).width() - jQuery('#deleteAccountFieldsDiv').width()) /2 + 'px', 
		width: jQuery('#deleteAccountFieldsDiv').width() + 50,
		height:'350px',
		overflow:'scroll'
		}});
	}else if(frmType == 'Contact'){
		$.blockUI({ message: $('#deleteContactFieldsDiv'),
		css: { 
		padding: '10px',
		backgroundColor: '#FFFFFF',
		opacity: '.9', 
		color: '#000000',
		top:  (jQuery(window).height() - jQuery('#deleteContactFieldsDiv').height()) /3 + 'px', 
		left: (jQuery(window).width() - jQuery('#deleteContactFieldsDiv').width()) /2 + 'px', 
		width: jQuery('#deleteContactFieldsDiv').width() + 50,
		height:'350px',
		overflow:'scroll'
		}});
	}
	populateDeleteFields(frmType);
}

function cancelDeleteFields(frmType){
	if(frmType == 'Account')
		document.getElementById('accountDeleteFieldsError').innerHTML = '';
	else
		document.getElementById('contactDeleteFieldsError').innerHTML = '';
		
	$.unblockUI() ;
}

function populateDeleteFields(frmType){
	if(frmType == 'Account'){
		$.post('../../ajax/ajax.addEditFields.php',
			{
				frmType:'Account',
				action :'GetDeleteFields',
				companyId : companyId
			},
			processGetAccountFields
		);
	}else if(frmType == 'Contact'){
		$.post('../../ajax/ajax.addEditFields.php',
			{
				frmType:'Contact',
				action :'GetDeleteFields',
				companyId : companyId
			},
			processGetContactFields
		);	
	}
}

function processGetAccountFields(result){
	result = JSON.decode(result);
	var accountDeleteInner = '';
	for(i=0;i<result.length;i++){
		accountDeleteInner = accountDeleteInner + '<div> <input type="checkBox" name="'+ result[i]['AccountMapID'] +'" id="'+ result[i]['AccountMapID'] +'" value="1"/><input type="text" id="'+ result[i]['LabelName'] +'" name="'+ result[i]['LabelName'] +'" value="'+ result[i]['LabelName'] +'" Disabled/></div><br/>';
	}
	document.getElementById('deleteAccountFieldsInner').innerHTML = accountDeleteInner;
}

function processGetContactFields(result){
	result = JSON.decode(result);
	var contactDeleteInner = '';
	for(i=0;i<result.length;i++){
		contactDeleteInner = contactDeleteInner + '<div> <input type="checkBox" name="'+ result[i]['ContactMapID'] +'" id="'+ result[i]['ContactMapID'] +'" value="1"/><input type="text" id="'+ result[i]['LabelName'] +'" name="'+ result[i]['LabelName'] +'" value="'+ result[i]['LabelName'] +'" Disabled/></div><br/>';
	}
	document.getElementById('deleteContactFieldsInner').innerHTML = contactDeleteInner;
}

function deleteFields(frmType){
	if(frmType == 'Account'){
		queryString = $('#frmDeleteAccountFields').formSerialize()+'&action=DeleteFields&frmType='+frmType+'&companyId='+companyId;
		$.post('../../ajax/ajax.addEditFields.php?'+queryString,processDeleteAccountFields);
	}else if(frmType == 'Contact'){
		queryString = $('#frmDeleteContactFields').formSerialize()+'&action=DeleteFields&frmType='+frmType+'&companyId='+companyId;
		$.post('../../ajax/ajax.addEditFields.php?'+queryString,processDeleteContactFields);
	}
}

function processDeleteAccountFields(result){
	//alert(result);
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}
	var deletedFieldsArr = result.FIELDSDELETED ;
	if(deletedFieldsArr.length > 0){
		for(j=0;j<deletedFieldsArr.length;j++){
			for(i=0;i<ACCOUNTFIELDS.length;i++){
				if(deletedFieldsArr[j] == ACCOUNTFIELDS[i]['AccountMapID']){
					ACCOUNTFIELDS.splice(i,1);
				}	
			}	
		}
		TOTALFIELDS = (TOTALFIELDS - deletedFieldsArr.length);
	}
	if(result.STATUS.STATUS=='OK')
	{
		document.getElementById('accountDeleteFieldsError').innerHTML = '';
		//document.getElementById(fieldTabEdit).innerHTML = newFieldName;
		alert('Account fields deleted sucessfully');
		manageCompany();
		$.unblockUI() ;
		
	}else{
		var str='';
		for(i=0;i<result.STATUS.ERROR.length;i++)
		{
			str=str+'<br/>'+result.STATUS.ERROR[i];
		}
		document.getElementById('accountDeleteFieldsError').innerHTML = str; 	
	}
}

function processDeleteContactFields(result){
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}
	var deletedFieldsArr = result.FIELDSDELETED ;
	if(deletedFieldsArr.length > 0){
		for(j=0;j<deletedFieldsArr.length;j++){
			for(i=0;i<CONTACTFIELDS.length;i++){
				if(deletedFieldsArr[j] == CONTACTFIELDS[i]['ContactMapID']){
					CONTACTFIELDS.splice(i,1);
				}	
			}	
		}
		CONTACTTOTALFIELDS = (CONTACTTOTALFIELDS - deletedFieldsArr.length);
	}
	if(result.STATUS.STATUS=='OK')
	{
		document.getElementById('contactDeleteFieldsError').innerHTML = '';
		//document.getElementById(fieldTabEdit).innerHTML = newFieldName;
		alert('Contact fields deleted sucessfully');
		manageContact();
		$.unblockUI();
		//window.location='?';
	}else{
		var str='';
		for(i=0;i<result.STATUS.ERROR.length;i++)
		{
			str=str+'<br/>'+result.STATUS.ERROR[i];
		}
		document.getElementById('contactDeleteFieldsError').innerHTML = str; 	
	}
}

function CheckPrivate(obj) {
	if(obj.checked) {
		document.getElementById('privateField').checked = false;
	}
}
