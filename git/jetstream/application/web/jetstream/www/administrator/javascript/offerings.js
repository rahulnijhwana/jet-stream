//Offerings functions in common between admin.html and main.js

function getOfferingsList(dealID)
{
	return _getOfferingsData(dealID, 'list');
}

function getOfferingsVal(dealID)
{
	return _getOfferingsData(dealID, 'val');
}

function getOfferingsCount(dealID)
{
	return _getOfferingsData(dealID, 'count');
}

function getOfferingsActVal(dealID)
{
	return _getOfferingsData(dealID, 'actual');
}

function _getOfferingsData(dealID, returnType)
{
	var oCount=0;
	var oStr="";
	var oVal=0;
	var oActVal=0;
	var idPrimary = getMainOffering(dealID);
	for (var i=0; i< g_opp_products_xref.length; i++)
	{
		if(g_opp_products_xref[i].deleted == true) continue;
		if(dealID == g_opp_products_xref[i][g_opp_products_xref.DealID])
		{
			oCount++;
			var q = g_opp_products_xref[i][g_opp_products_xref.Qty];
			oVal += (q * unformat_money(g_opp_products_xref[i][g_opp_products_xref.Val]));
			var actq = g_opp_products_xref[i][g_opp_products_xref.ActQty];
			oActVal += (actq * unformat_money(g_opp_products_xref[i][g_opp_products_xref.ActVal]));
			var prodID = g_opp_products_xref[i][g_opp_products_xref.ProductID];
			if(oStr.length > 0) oStr += ", ";
//			if(prodID == idPrimary) oStr += '***';
			oStr += getProdAbbr(prodID);
		}
	}
	if(returnType == 'list') return oCount + " Items: " + oStr;
	if(returnType == 'count') return oCount;
	if(returnType == 'actual') return oActVal;
	else 
		return oVal >  0 ? oVal : '';

}



function getProdAbbr(id)
{
	var abbr = "Unk.";
	for(var i=0; i<g_products.length; i++)
	{
		if(id == g_products[i][g_products.ProductID])
		{
			abbr = g_products[i][g_products.Abbr];
			break;
		}	
	}
	return abbr;
}

function makeNewProdRec(dealid)
{
	var companyRec;
	if(typeof(g_the_only_company) == 'undefined')
		companyRec = g_company[0];
	else
		companyRec = g_the_only_company;

	var ret = new Array();
	ret.i_am_new = true;
	for (var k in g_opp_products_xref)
	{
		var n = parseInt(k, 10);
		if (!isNaN(n))
			continue;
		if (k.charAt(0) == '_' || k == 'toPipes')
			continue;
		ret[ret.length] = '';
	}
	ret[g_opp_products_xref.ID] = getTempID();
	ret[g_opp_products_xref.DealID] = dealid;
	ret[g_opp_products_xref.CompanyID] = companyRec[g_company.CompanyID];

//	loc_xrefs[loc_xrefs.length] = ret;
	return ret;
}

function getProductSalesCycle(id)
{
	for (var i=0; i <g_products.length; i++)
	{
		if (id == g_products[i][g_products.ProductID])
			return g_products[i][g_products.CalculatedSalesCycle];
	}
	return -1;
}

function getMainOffering(idOpp)
{
	var mainID = '-1';	// this has to be a string so that the evalStatement in admin will be happy
	var longestCycle = -1;
	for (var i = 0; i < g_opp_products_xref.length; i++)
	{
		if (g_opp_products_xref[i][g_opp_products_xref.DealID] != idOpp) continue;
		if (g_opp_products_xref[i].deleted == true) continue;
		var lenCycle = getProductSalesCycle(g_opp_products_xref[i][g_opp_products_xref.ProductID]);
		if (lenCycle > longestCycle)
		{
			mainID = g_opp_products_xref[i][g_opp_products_xref.ProductID];
			longestCycle = lenCycle;
		}
	}
//alert('dealID = ' + idOpp + ' longest Cycle = ' + longestCycle + ' ID=' + mainID);
	return mainID;

}

function zeroOfferings(dealID)
{

	for (var i = 0; i < g_opp_products_xref.length; i++)
	{
		if (g_opp_products_xref[i][g_opp_products_xref.DealID] == dealID) 
		{
			g_opp_products_xref[i].deleted = true;
		}	
	}
}


function arrCopy(arr)
{
	var cpy=new Array();
	var str = arr.join('|');
	var cpy=str.split('|');
	return cpy;
}

// this function should be here so that newly created arrays belong to main
function copyRecs(loc_xrefs)
{
	var seq = 0;
//alert('starting copy');
	for (var i=0; i<loc_xrefs.length; ++i)
	{

		if(loc_xrefs[i].i_am_new == true)
			continue;
//Aren't these in the wrong order????
/*
		if (loc_xrefs[i].edited == true || loc_xrefs[i].deleted == true)
		{
			g_opp_products_xref[loc_xrefs[i].globalInd] = loc_xrefs[i];
//alert('Marking edit or delete at ' + loc_xrefs[i].globalInd);
		}
		else if(loc_xrefs[i].added_locally == true)
		{
			loc_xrefs[i].added_locally = false;
			var newInd = g_opp_products_xref.length;
			g_opp_products_xref[newInd] = loc_xrefs[i];
			g_opp_products_xref[newInd].added = true;
			loc_xrefs[i].globalInd = newInd;
		}
*/

		if(loc_xrefs[i].added_locally == true)
		{
			loc_xrefs[i].added_locally = false;
			var newInd = g_opp_products_xref.length;
			g_opp_products_xref[newInd] = arrCopy(loc_xrefs[i]);
			g_opp_products_xref[newInd].added = true;
			loc_xrefs[i].globalInd = newInd;
		}
		else if (loc_xrefs[i].edited == true || loc_xrefs[i].deleted == true)
		{
			var flagAdded = 0;
			var deleted = loc_xrefs[i].deleted;
			var edited=0;
			if(g_opp_products_xref[loc_xrefs[i].globalInd].added == true) flagAdded = 1;
			if(g_opp_products_xref[loc_xrefs[i].globalInd].edited == true) edited = 1;
			
			g_opp_products_xref[loc_xrefs[i].globalInd] = arrCopy(loc_xrefs[i]);
			//var temp = arrCopy(loc_xrefs[i]);
			//g_opp_products_xref[loc_xrefs[i].globalInd] = temp;
			if (flagAdded)
				g_opp_products_xref[loc_xrefs[i].globalInd].added = true;
			g_opp_products_xref[loc_xrefs[i].globalInd].deleted=deleted;
			g_opp_products_xref[loc_xrefs[i].globalInd].edited=edited | loc_xrefs[i].edited;
			
		}


		g_opp_products_xref[loc_xrefs[i].globalInd][g_opp_products_xref.Seq] = seq;
//		g_opp_products_xref[loc_xrefs[i].globalInd][g_opp_products_xref.Val] = unformat_money(g_opp_products_xref[loc_xrefs[i].globalInd][g_opp_products_xref.Val]);
		seq++;
//alert('rec ' + seq + ' val = ' + 		g_opp_products_xref[loc_xrefs[i].globalInd][g_opp_products_xref.Val]);
	}
// ???	window.opener.g_opp_products_xref = g_opp_products_xref;
//alert('done with copy');
}
