
var addOptions = 0;

function populateOptions(frmType){
	
	var optionSelected = document.getElementById('optionSetSelect').value;
	
	if(optionSelected != -1){
		addOptions = 0;
		var inner = '<div>';
		if(options && options[optionSelected])
		{
			for(j=0;j<options[optionSelected].OptionSetValues.length;j++){
				inner = inner+'<input type="text" name="optionsValues'+ addOptions +'" id="optionsValues'+ addOptions +'" value="'+ options[optionSelected].OptionSetValues[j].OptionName +'"/><br/>';
				addOptions = addOptions + 1;	
			}	
		}
		inner=inner+'</div>';
		if(frmType == 'Account'){
			document.getElementById('contactOptionSetvalues').innerHTML = '';
			document.getElementById('optionSetvalues').innerHTML=inner;
		}
		else{
			document.getElementById('optionSetvalues').innerHTML = '';
			document.getElementById('contactOptionSetvalues').innerHTML=inner;
		}
			
	}else{
		if(frmType == 'Account')
			document.getElementById('optionSetvalues').innerHTML='';
		else
			document.getElementById('contactOptionSetvalues').innerHTML='';
		addOptions = 0;
	}
}

function addOptionList(frmType){
	var noOfCols = 5;
	var inner = '';
	for(i=0;i<noOfCols;i++)
	{
		newDiv=document.createElement('div');
		newDiv.innerHTML = '<input type="text" name="optionsValues'+ addOptions +'" id="optionsValues'+ addOptions +'" value=""/>';
		if(frmType == 'Account')
			document.getElementById('optionSetvalues').appendChild(newDiv);
		else
			document.getElementById('contactOptionSetvalues').appendChild(newDiv);
			
		addOptions = addOptions + 1;	
	}	
}
function cancelOption(type){

	if(type == 'Account'){
		document.getElementById('optionSetvalues').innerHTML=''; 
		document.getElementById('optionSetName').value='';
		document.getElementById('optionSetSelect').value='-1';
		document.getElementById('optionListError').innerHTML='';
		addOptions = 0;
		$('#addAccountFieldsLightBox').unblock();
	}
		
	else{
		document.getElementById('contactOptionSetvalues').innerHTML=''; 
		document.getElementById('contactOptionSetName').value='';
		document.getElementById('optionSetSelect').value='-1';
		document.getElementById('contactOptionListError').innerHTML='';
		addOptions = 0;
		$('#addContactFieldsLightBox').unblock();
	}
		
}

function saveOptionList(addDiv){	
	var fieldValue = '';
	//var optionSelected = -1;
	var optionSelected = document.getElementById('optionSetSelect').value;
	document.getElementById('optionSetSelect').value = -1;
	if(addDiv == 'Account')
		fieldValue = document.getElementById('optionSetName').value;
	else
		fieldValue = document.getElementById('contactOptionSetName').value;
		
	var countOptions = 0;
	var optionValues = new Array();
	fieldValue = fieldValue.replace("'","\'");
	fieldValue = fieldValue.replace(/^\s+|\s+$/g, '') ;
	if(fieldValue == '' ){
		if(addDiv == 'Account')
			document.getElementById('optionListError').innerHTML = 'LabelName is mandatory';
		else
			document.getElementById('contactOptionListError').innerHTML = 'LabelName is mandatory';
			
	return false;
	}else{
		var j =0;
		for(i=0;i<addOptions;i++){
			var optionsField = 'optionsValues'+i; 
			if(document.getElementById(optionsField).value != ''){
				optionValues[j] = document.getElementById('optionsValues'+i).value;
				optionValues[j] = optionValues[j].replace(/^\s+|\s+$/g, '') ;
				if(optionValues[j] != ''){
					countOptions = countOptions + 1;
				}
				j = j+1;
			}
		}
		if(countOptions < 1){
			if(addDiv == 'Account')
				document.getElementById('optionListError').innerHTML = 'Cannot create a blank Option Set.';
			else
				document.getElementById('contactOptionListError').innerHTML = 'Cannot create a blank Option Set.';
			return false;
		}
		if(options[optionSelected]){
			document.getElementById('optionSetId').value = options[optionSelected].OptionSetId;	
			if(addDiv == 'Account')
				document.getElementById('accountOptionSetId').value = options[optionSelected].OptionSetId;	
			else
				document.getElementById('contactOptionSetId').value = options[optionSelected].OptionSetId;	
		}else{
			document.getElementById('accountOptionSetId').value = '';
			document.getElementById('contactOptionSetId').value = '';
		}
		addSelectedField(addDiv,fieldValue,optionValues);
	}
	if(addDiv == 'Account'){
		document.getElementById('optionSetvalues').innerHTML=''; 
		document.getElementById('optionSetName').value='';
		document.getElementById('optionSetSelect').value='-1';
		document.getElementById('optionListError').innerHTML='';
		addOptions = 0;
		$('#addAccountFieldsLightBox').unblock();
	}
	else{
		document.getElementById('contactOptionSetvalues').innerHTML=''; 
		document.getElementById('contactOptionSetName').value='';
		document.getElementById('optionSetSelect').value='-1';
		document.getElementById('contactOptionListError').innerHTML='';
		addOptions = 0;
		$('#addContactFieldsLightBox').unblock();
	}
}

function processSaveOptionsFields(){
	//Dont do anything....
}

var divSelected = '';

function editOptionListFields(frm){
	divSelected = frm;
	//The edited field options list .It is defined in addEditFields.js
	var optionDetails = editOptionList;
	var options = optionDetails[0].Options;
	if(optionDetails[0].CompID < 0){
			alert ('Sorry you are not authorized to modify system defined options.');
			return false;
	}else{
		var div = '';
		var optionsetId = optionDetails[0].OptionSetId;
		if(divSelected == 'Account'){
			var mapId = optionDetails[0].AccountMapId;	
			document.getElementById('addContactOptionElementsDiv') .innerHTML = '';
			div = document.getElementById('addOptionElementsDiv');
		}else{
			var mapId = optionDetails[0].ContactMapId;	
			document.getElementById('addOptionElementsDiv') .innerHTML = '';
			div = document.getElementById('addContactOptionElementsDiv');
		}
		
		inner = '';
		for(i = 0;i<options.length;i++)
		{
			inner = inner + '<div style="height:25px;"><input type="text" name="'+ options[i].OptionID +'" id="'+ options[i].OptionID +'" value="'+ options[i].OptionName +'"></div>';
		}	
			inner = inner + '<input type="hidden" id="editOptionSetId" name="editOptionSetId" value="'+ optionsetId +'"><input type="hidden" id="editMapId" name="editMapId" value="'+ mapId +'">';
			div . innerHTML =inner;
			
		if(divSelected == 'Account'){
			$('#editAccountFieldsLightBox').block({ message: $('#editOptionListDiv'),
			css: { 
			padding: '10px',
			backgroundColor: '#FFFFFF',
			opacity: '.9', 
			color: '#000000',
			top:  (jQuery(window).height() - jQuery('#editOptionListDiv').height()) /4 + 'px', 
			left: (jQuery(window).width() - jQuery('#editOptionListDiv').width()) /3 + 'px', 
			width: '455px' ,
			height:'355px',
			overflow:'scroll' 
			}});	
		}else{
			$('#editContactFieldsLightBox').block({ message: $('#editContactOptionListDiv'),
			css: { 
			padding: '10px',
			backgroundColor: '#FFFFFF',
			opacity: '.9', 
			color: '#000000',
			top:  (jQuery(window).height() - jQuery('#editContactOptionListDiv').height()) /4 + 'px', 
			left: (jQuery(window).width() - jQuery('#editContactOptionListDiv').width()) /3 + 'px', 
			width: '455px' ,
			height:'355px',
			overflow:'scroll' 
			}});	
		}
	}
	
}
var editOptions = 1;
function addEditOptionList(frmType)
{
	inner='';
	for(i=0;i<5;i++){
		newDiv=document.createElement('div');		
		newDiv.innerHTML ='<input type="text" name="optionList[]" id="optionList[]" value=""/>';
		if(frmType == 'Account')
			document.getElementById('addOptionElementsLeftDiv').appendChild(newDiv);
		else
			document.getElementById('addContactOptionElementsLeftDiv').appendChild(newDiv);
	}
}

function checkOptionValues(optionValues)
{
	frmInstance=document.getElementById(frmInstance);		
	for(i=0;i<frmInstance.length;i++)
	{
		if(frmInstance[i].type=='text' && frmInstance[i].value.indexOf('&')>=0)
		{
			alert('Invalid charcter &');			
			frmInstance[i].focus();
			return false;
		}
	}
	return true;
}

function saveEditOptions(){
	var mapId = document.getElementById('editMapId').value;
	var opSetId = document.getElementById('editOptionSetId').value;
	//var optionList = document.getElementById('optionList[]').value;
	if(divSelected == 'Account'){
		queryString=$('#frmEditOptions').formSerialize()+'&action=saveEditOptionValues&companyId='+companyId;
		$.post('../../ajax/ajax.addEditFields.php?'+queryString, processSaveEditOptionsFields);		
	}
	else {
		queryString=$('#frmEditContactOptions').formSerialize()+'&action=saveEditOptionValues&companyId='+companyId;
		$.post('../../ajax/ajax.addEditFields.php?'+queryString, processSaveEditOptionsFields);		
	}
}
function processSaveEditOptionsFields(result){
	var type = divSelected;
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}

	if(result.STATUS=='OK')
	{
		$.post('../../ajax/ajax.addEditFields.php',
			{
				action :'getOptionSetValue',
				companyId : companyId
			},
			function(result)
			{
				result = JSON.decode(result);
								
				OPTIONSETS = result.OptionSet;
				OPTIONSETS_JSON = result.OptionSet;
				CONTACTOPTIONSETS_JSON = result.OptionSet;
				CONTACTOPTIONSETS = result.OptionSet;
				
				options = OPTIONSETS_JSON;
				try
				{
					options = eval(options);
				}
				catch(e)
				{
					options = JSON.decode(options);
				}	
				
				contactOptions = CONTACTOPTIONSETS_JSON;
				try
				{
					contactOptions = eval(options);
				}
				catch(e)
				{
					contactOptions = JSON.decode(options);
				}	
			}
		);		
		
		if(type == 'Account'){
			document.getElementById('addOptionElementsLeftDiv').innerHTML = '';
			$('#editAccountFieldsLightBox').unblock();
		}
			
		else{
			document.getElementById('addContactOptionElementsLeftDiv').innerHTML = '';
			$('#editContactFieldsLightBox').unblock();
		}
	}else{
		document.getElementById('editOptionError').innerHTML=result.ERROR[0];
	}
}
function cancelEditOptionList(type){	
	if(type == 'Account'){
		document.getElementById('addOptionElementsLeftDiv').innerHTML = '';
		document.getElementById('editOptionError').innerHTML = '';
		$('#editAccountFieldsLightBox').unblock();
	}	
	else{
		document.getElementById('contactEditOptionError').innerHTML = '';
		document.getElementById('addContactOptionElementsLeftDiv').innerHTML = '';
		$('#editContactFieldsLightBox').unblock();
	}
}
