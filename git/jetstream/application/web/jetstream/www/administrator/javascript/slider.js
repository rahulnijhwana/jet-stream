// Change

function makeSlider(name, min, max, width, initialValue, bgImage)
{
	if (!min)
		min = 0;
	if (!max)
		max = 100;
	if (!width)
		width = 200;
	if (!initialValue)
		initialValue = 0;
	if (!bgImage)
		bgImage = false;
	
	if (initialValue < min)
		initialValue = min;
	if (initialValue > max)
		initialValue = max;
	//status = 'width='+width+' max='+max+' min='+min+' initialValue='+initialValue;
	var tempwidth = Math.floor(((initialValue-min)*width)/(max-min));
	if (tempwidth == 0)
		tempwidth = 1;
	
	var str = '';
	str += ('<input type="hidden" name="'+name+'" id="'+name+'" value="0"></input>');
	str += ('<table cellpadding="0" cellspacing="0" style="position:relative; display:inline;"><tr><td>');
	str += ('<table id="mainSlider_'+name+'" inputname="'+name+'" width="'+width+'" height="20" cellpadding="0" cellspacing="0" minval="'+min+'" maxval="'+max+'" onmousemove="_slider_onMouseMove(this);" onmousedown="_slider_onMouseDown(this);" onmouseup="_slider_onMouseUp(this);" onmouseleave="_slider_onMouseLeave(this);">');
		str += ('<tr>');
			str += ('<td id="sliderTD_'+name+'" width="'+tempwidth+'">');
				str += ('<table cellpadding="0" cellspacing="0" width="100%" height="100%">');
					str += ('<tr>');
					if (bgImage)
						str += ('<td style="font-size:1px;" height="50%">&nbsp;</td>');
					else
						str += ('<td style="border-bottom:1 solid black; font-size:1px;" height="50%">&nbsp;</td>');
					str += ('</tr>');
					str += ('<tr>');
						str += ('<td style="font-size:1px;">&nbsp;</td>');
					str += ('</tr>');
				str += ('</table>');
			str += ('</td>');
			str += ('<td id="sliderThumb_'+name+'" style="border:1 solid black; font-size:1px; cursor:pointer;" width="7" bgcolor="white">&nbsp;</td>');
			str += ('<td>');
				str += ('<table cellpadding="0" cellspacing="0" width="100%" height="100%">');
					str += ('<tr>');
					if (bgImage)
						str += ('<td style="font-size:1px;" height="50%">&nbsp;</td>');
					else
						str += ('<td style="border-bottom:1 solid black; font-size:1px;" height="50%">&nbsp;</td>');
					str += ('</tr>');
					str += ('<tr>');
						str += ('<td style="font-size:1px;">&nbsp;</td>');
					str += ('</tr>');
				str += ('</table>');
			str += ('</td>');
		str += ('</tr>');
	str += ('</table>');
	str += ('</td></tr></table>');
	
	document.writeln(str);
	
	var tempelem = document.getElementById('mainSlider_'+name);
	tempelem.setRawValue = _slider_setRawValue;
	tempelem.oldValue = -9999;
	tempelem.style.backgroundImage = 'url('+bgImage+')';
	return tempelem;
}

function _slider_setRawValue(i, fireEvent)
{
	if (!fireEvent)
		fireEvent = false;
	if (i >= 0)
	{
		var fakewidth = i;
		if (fakewidth == 0)
			fakewidth = 1;
		var sliderTD = document.getElementById('sliderTD_'+this.inputname);	
		var sliderThumb = document.getElementById('sliderThumb_'+this.inputname);	
		sliderTD.width = fakewidth;
		var realval = parseInt(this.minval) + ((this.maxval - this.minval) * (i / (this.width-sliderThumb.width)));
		//status =  this.minval;
		if (realval > this.maxval)
			realval = this.maxval;
		realval = Math.floor(realval);
		if (this.oldValue != realval)
		{
			document.getElementById(this.inputname).value = realval;
			if (fireEvent && this.onchange)
				this.onchange(this, realval);
			this.oldValue = realval;
		}
	}
}

function _slider_onMouseMove(elem)
{
	if (!elem.bMouseDown)
		return;
	if (event.x > 3)
		elem.setRawValue(event.x-3, true);
}

function _slider_onMouseDown(elem)
{
	elem.bMouseDown = true;
	if (event.x > 3)
		elem.setRawValue(event.x-3, true);
}

function _slider_onMouseUp(elem)
{
	elem.bMouseDown = false;
}

function _slider_onMouseLeave(elem)
{
	elem.bMouseDown = false;
}
