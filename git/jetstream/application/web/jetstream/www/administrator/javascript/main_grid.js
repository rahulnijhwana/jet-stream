//08/14/07:  LF		Added column totals to the manager's board


g_ShowColTots=true;	// global switch to turn on/off manager column tots


Grid = new Object();

// ----------------------------- "public" methods

Grid.makeScroller = _Grid_makeScroller;
Grid.makeGrid = _Grid_makeGrid;
Grid.fillGrid = _Grid_fillGrid;

Grid.showPeople = _Grid_showPeople;
Grid.showPerson = _Grid_showPerson;
Grid.firstShown = _Grid_firstShown;
Grid.numPeopleShown = _Grid_numPeopleShown;

Grid.addOpportunity = _Grid_addOpportunity;
Grid.removeOpportunity = _Grid_removeOpportunity;

Grid.getCellInfo = _Grid_getCellInfo;

Grid.scrollLeft = _Grid_scrollLeft;
Grid.scrollRight = _Grid_scrollRight;

Grid.setView = _Grid_setView;
Grid.getView = _Grid_getView;

// ----------------------------- "public" data

Grid.enable = true;

// ----------------------------- "private" methods

Grid.calcRows = _Grid_calcRows;
Grid.upSize = _Grid_upSize;
Grid.makeGridHeader = _Grid_makeGridHeader;
Grid.makeGridHeaderNew = _Grid_makeGridHeaderNew;
Grid.calcCellID = _Grid_calcCellID;
Grid.makeNewPerson = _Grid_makeNewPerson;
Grid.personShownIndex = _Grid_personShownIndex;
Grid.personIndex = _Grid_personIndex;


// ----------------------------- initialization and "private" data

Grid.m_minPeopleShown = 5;
Grid.m_maxPeopleShown = 10;

Grid.m_rowInfo = null;
Grid.m_peopleTotals = new Array();
Grid.m_peopleShown = new Array();
Grid.m_currentView = new Object();
Grid.m_currentView.view = 'grouped';
Grid.m_currentView.firstShown = 0;
Grid.m_previousView = null;
Grid.m_cellHeight = 0;
Grid.TargetUsed = 0;
Grid.ValuationUsed = 0;

Grid.m_showSalesman = false;
var thisBegin;
var lastBegin;
var prevBegin;

var g_blinkers;

function setCloseDates() {
	var now = new Date();
	if (g_company[0][g_company.ClosePeriod] == 'q') {
		var qBegin = now.getMonth();
		while (qBegin % 3) {
			--qBegin;
		}
		thisBegin = new Date(now.getFullYear(), qBegin, 1).getTime();
		lastBegin = new Date(now.getFullYear(), qBegin - 3, 1).getTime();
		prevBegin = new Date(now.getFullYear(), qBegin - 6, 1).getTime();
	}
	else
	{
		thisBegin = new Date(now.getFullYear(), now.getMonth(), 1).getTime();
		lastBegin = new Date(now.getFullYear(), now.getMonth() - 1, 1).getTime();
		prevBegin = new Date(now.getFullYear(), now.getMonth() - 2, 1).getTime();
	}

}

// ----------------------------- implementation (public)

function _Grid_showPeople()
{
	if (Grid.m_currentView.view == 'sidebyside') {
		this.m_maxPeopleShown=5;
	} else {
		this.m_maxPeopleShown=10;
	}

	Grid.m_peopleShown = new Array();
	Grid.m_showSalesman = false;
	Grid.ShowColMgrTots=g_ShowColTots; 	// true if showing column totls
	Grid.ShowColSalesTots=false;
	var i, j;
	for (i = Grid.m_currentView.firstShown, j = 0; i < g_people.length && j < Grid.m_maxPeopleShown; ++i, ++j) {
		Grid.m_peopleShown[j] = g_people[i];
	}

	while (Grid.m_peopleShown.length < Grid.m_minPeopleShown) {
		Grid.m_peopleShown[Grid.m_peopleShown.length] = Grid.makeNewPerson();
	}
}

function _Grid_showPerson(salesPersonID)
{
	Grid.m_peopleShown = new Array();
	Grid.m_showSalesman = true;
	Grid.ShowColSalesTots=g_ShowColTots; 	// true if showing column totls
	Grid.ShowColMgrTots=false;
	for (var i = 0; i < g_people.length; ++i)
	{
		if (g_people[i][g_people.PersonID] != salesPersonID) continue;
		Grid.m_peopleShown[0] = g_people[i];
		Grid.m_peopleShown[0].perInd=i;		// keep track of where we came from
		return;
	}
}

function _Grid_firstShown()
{
	return Grid.m_peopleShown[0];
}

function _Grid_numPeopleShown()
{
	return Grid.m_peopleShown.length;
}

//function preloadImages() {
//  var d=document;
//  if(d.images){ if(!d.MM_p) d.MM_p=new Array();
//  var i,j=d.MM_p.length,a=preloadImages.arguments;
//  for(i=0; i<a.length; i++)
//    { d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
//}

//preloadImages(
//"../images/ivorybox_l.jpg","../images/ivorybox_r.jpg","../images/ivorybox_tl.jpg",
//"../images/ivorybox_tr.jpg","../images/ivorybox_bl.jpg","../images/ivorybox_br.jpg",
//"../images/ivorybox_t.jpg","../images/ivorybox_b.jpg","../images/stalledbox_tl2.jpg",
//"../images/spacer.gif","../images/stalledbox_t2.jpg","../images/stalledbox_tr2.jpg",
//"../images/stalledbox_b.jpg","../images/stalledbox_bl.jpg","../images/stalledbox_br.jpg",
//"../images/stalledbox_l.jpg","../images/stalledbox_r.jpg");


// creates the table, borders, and headings for the grid, but doesn't put any opportunities into the grid
// this is done by fillGrid, below
g_testwidth=1;

function _Grid_makeGrid()
{
	if (Grid.m_currentView.view == 'sidebyside') {
		this.m_maxPeopleShown=5;
	} else {
		this.m_maxPeopleShown=10;
	}

	//alert("Makegrid");
	g_blinkers= new Object();


	setCloseDates();	// calc the close dates for the close column for the grid


	var bTarg = Grid.TargetUsed = (g_company[0][g_company.TargetUsed]=='1');

	// the following setting can be used for testing without targets
	//	var bTarg = Grid.TargetUsed = false;
	//	var bTarg = g_target_used;


	Grid.ValuationUsed = (g_company[0][g_company.ValuationUsed]=='1');

	var width=800;
	var height=600;
	if ((navigator.appName.indexOf("Netscape") != -1)) {
		width=window.innerWidth;
		width-=20;	// somebody figure out how to determine this please
		height=window.innerHeight;
	} else {
		width=document.body.clientWidth;
		width-=document.body.rightMargin;
		width-=document.body.leftMargin;
		height=document.body.clientHeight-document.body.topMargin-document.body.bottomMargin;
	}
	var numPeople = Grid.m_peopleShown.length;
	var cellcnt= (bTarg ? numPeople * 5 : numPeople * 4); //from 4

	if (Grid.m_rowInfo == null) {
		Grid.calcRows();
	}

	Grid.m_cellHeight = Math.floor(100 / (Grid.m_rowInfo.rows + 1)) + '%';

	//	var minHeight=28;
	var minHeight=24;
	if (Grid.m_peopleShown.length == 1) {
		minHeight=41;
	}
	//	var ret = '<table height="100%" width="100%" cellspacing=0 cellpadding=0 border=0 onselectstart="return false;">';
	//	var ret = '<table height="100%" width=1% cellspacing=0 cellpadding=0 border=0 onselectstart="return false;">';

	var lCell = '<td width="8" ><img src="../images/ivorybox_l.jpg" height="100%" width=8 ></td>';
	var rCell = '<td width="11"><img src="../images/ivorybox_r.jpg" height="100%" width=11></td>';
	var rrCell = '<td width="12"><img src="../images/ivorybox_r.jpg" height="100%" width=11><img src="../images/spacer.gif" height="'+minHeight+'" width=1></td>';
	var mCell = rCell + lCell;

	/*
	var tlCell = '<td width="1%" height="1%"><img src="../images/ivorybox_tl.jpg" height="10" width=8></td>';
	var trCell = '<td width="1%" height="1%"><img src="../images/ivorybox_tr.jpg" height="10" width=11></td>';
	var tmCell = '<td colspan=2 width="1%" height="1%" nowrap><img src="../images/ivorybox_tr.jpg" height="10" width=11><img src="../images/ivorybox_tl.jpg" height="10" width=8></td>';
	var blCell = '<td width="1%" height="1%"><img src="../images/ivorybox_bl.jpg" height="11" width=8></td>';
	var brCell = '<td width="1%" height="1%"><img src="../images/ivorybox_br.jpg" height="11" width=11></td>';
	var bmCell = '<td colspan=2 width="1%" height="1%" nowrap><img src="../images/ivorybox_br.jpg" height="11" width=11><img src="../images/ivorybox_bl.jpg" height="11" width=8></td>';
	*/

	var tlCell = '<td width="8" height="1%"><img src="../images/ivorybox_tl.jpg" height="10" width=8></td>';
	var trCell = '<td width="8" height="1%"><img src="../images/ivorybox_tr.jpg" height="10" width=11></td>';
	var tmCell = '<td colspan=2 width="1%" height="1%" nowrap><img src="../images/ivorybox_tr.jpg" height="10" width=11><img src="../images/ivorybox_tl.jpg" height="10" width=8></td>';
	var blCell = '<td width="11" height="1%"><img src="../images/ivorybox_bl.jpg" height="11" width=8></td>';
	var brCell = '<td width="11" height="1%"><img src="../images/ivorybox_br.jpg" height="11" width=11></td>';
	var bmCell = '<td colspan=2 width="1%" height="1%" nowrap><img src="../images/ivorybox_br.jpg" height="11" width=11><img src="../images/ivorybox_bl.jpg" height="11" width=8></td>';


	if (Grid.m_currentView.view == 'sidebyside') {
		var ret = '<table cellspacing=0 cellpadding=1 border=0 onselectstart="return false;">';
		var frills=3+(numPeople)*(11+2);	// the cell is 11 wide, plus 2 pixels for padding + 3 for spacer (and padding)

		var infoExtra=numPeople*3;

		width=width-frills;
		var cellcnt = numPeople*(bTarg ? 5 : 4);
		var cellPix = Math.floor(width/(cellcnt+infoExtra));	// the numPeople*3 will give 4 times the size to info

		var infowidth=cellPix*4;
		var cellRound=width-(cellcnt+infoExtra)*cellPix;
		infowidth+=Math.floor(cellRound/numPeople);	// give an extra pixel or two to the info cells
		cellRound%=numPeople;	// the number of pixels left over


		// height calc
		height-=37+28;	// height of top banner
		height-=32;	// scroller
		height-=50+2;	// top labels
		height-=1+2;	// heigh of top border
		height-=4;	// don't know why
		if (Grid.m_rowInfo.rows*(minHeight+2)<height) { // check for extra room
			minHeight=Math.floor(height/Grid.m_rowInfo.rows);
			minHeight-=2;	// account for padding
		}

		var thisLabel = Dates.m_thisMonth;
		var lastLabel = Dates.m_lastMonth;
		var prevLabel = Dates.m_previousMonth;
		if (g_company[0][g_company.ClosePeriod] == 'q') {
			thisLabel = 'Q' + Dates.m_thisQuarterNum;
			lastLabel = 'Q' + Dates.m_lastQuarterNum;
			prevLabel = 'Q' + Dates.m_previousQuarterNum;
		}

		var rowCnt=Grid.m_rowInfo.rows+2;	// one for headers, another for spacers
		var lineCol='<td width="11"  rowspan='+rowCnt+' height="100%" style="background-image: url(../images/grid-border.gif);background-repeat: repeat-y;background-position: 50% 50%;"><img src="../images/grid-border.gif" width=11></td>';

		ret += '<tr><td colspan="'+1*cellcnt+4+'" bgcolor="black" height=1><img src="../images/spacer.gif" height=1></td></tr>';

		//		ret += '<tr>';



		ret += '<tr style="height:50">';
		for (var i = 0; i < 5; ++i) {
			if (i) {
				ret += lineCol;
			}
			if (bTarg) {
				ret += Grid.makeGridHeaderNew(catLabel('T'), 'head', 'grid_header_new', 1, 10,'');
			}
			ret += Grid.makeGridHeaderNew(catLabel('FM'), 'head','grid_header_new', 1, 1,'');
			ret += Grid.makeGridHeaderNew(catLabel('IP'), 'head','grid_header_new', 1, 2,'');
			ret += Grid.makeGridHeaderNew(catLabel('DP'), 'head','grid_header_new', 1, 4,'');
			//ret += Grid.makeGridHeader(thisLabel, 'grid_header', 1);
			ret += Grid.makeGridHeaderNew(thisLabel, 'head','grid_header_new', 1, 6,'');
		}
		ret += '<td width="1"></td></tr>';

		ret +='<tr height="1">';
		for (i=0;i<5;i++) {
			for (j=0;j<numPeople;j++)
			if (j==(bTarg ? 2 : 1)) {
				ret+='<td height="1" width='+infowidth+'><img src="../images/spacer.gif" height=1 width='+infowidth+'></td>';
			} else {
				ret+='<td height="1" width='+colwidth+'><img src="../images/spacer.gif" height=1 width='+colwidth+'></td>';
			}
		}
		ret += '<td width="1"></td></tr>';


		var stalledHeaderRow = (Grid.m_rowInfo.rows - Grid.m_rowInfo.stalled) - 1;
		var offset,i,id,temp;
		for (i = 0; i < Grid.m_rowInfo.rows; ++i) {
			var grid="grid_even";
			if (!grid.m_showSalesman && i%2) {
				grid="grid_odd";
			}

			temp = '<tr class="'+grid+'">';

			var cols = (numPeople * 5)/* - 1*/;

			for (var j = 0; j < cols; ++j) {
				offset = j % numPeople;

				if (offset == (bTarg ? 5 : 4)) {
					//					temp += mCell;
				}
				else if (i == stalledHeaderRow && offset == (bTarg ? 2 : 1)) {  //from 1
					temp += Grid.makeGridHeaderNew(catLabel('Stalled'), 'red','grid_header_red', 2, (j == (bTarg ? 2 : 1)) ? 3 :5,'', 'stalled_sbs'); //last arg was rowspan=2
				}
				else if (i == stalledHeaderRow && offset == (bTarg ? 3 : 2)) { //from 2
					continue;
				}
				else if (i == (Grid.m_rowInfo.month*1)+1 && offset == (bTarg ? 4 : 3)) { //from 3
					temp += Grid.makeGridHeaderNew(lastLabel, 'head','grid_header_new', 1, 6,'');
				}
				else if ((i - 1 == (Grid.m_rowInfo.month * 2)+2) && offset == (bTarg ? 4 : 3)) { //from 3
					temp += Grid.makeGridHeaderNew(prevLabel, 'head','grid_header_new', 1, 6,'');
				} else {
					id = 'r' + i + 'c' + j;
					temp += '<td id="' + id + '" class="'+grid+'" onclick="do_spreadsheet(this)"';
					//Red border around stalled area in sidebyside
					if (i > stalledHeaderRow) {
						if (offset == (bTarg ? 2 : 1)) {
							temp += ' style="border-left:0.05cm solid red"';
						} else if (offset == (bTarg ? 3 : 2)) {
							temp += ' style="border-right:0.05cm solid red"';
						}
					}

					temp += ' height="' + Grid.m_cellHeight + '">&nbsp;</td>';

				}
			}

			temp += '<td width="1"><img src="../images/spacer.gif" height="'+minHeight+'" width=1></td></tr>';
			ret+=temp;
		}

		ret += '</table>';
		return ret;
	}

	// GROUPED view
	var ret = '<table cellspacing=0 cellpadding=1 border=0 onselectstart="return false;">';
	// special width calculation
	//	var frills=1+4*(8+11);
	var frills=3+(bTarg ? 4 : 3)*(11+2);	// the cell is 11 wide, plus 2 pixels for padding + 3 for spacer (and padding)
	width-=frills;
	var cellcnt=numPeople * (bTarg ? 5 : 4); //from 4
	var padding=2*cellcnt;		// each cell takes 2 pixels of padding, one on each side
	width-=padding;
	var infoExtra=0;
	if (!Grid.m_showSalesman) {
		infoExtra=numPeople*3;
	}

	var cellPix = Math.floor(width/(cellcnt+infoExtra));	// the numPeople*3 will give 4 times the size to info

	var colwidth=cellPix;
	var infowidth=colwidth;
	if (!Grid.m_showSalesman) {
		infowidth*=4;
		var cellRound=width-(cellcnt+infoExtra)*cellPix;
		infowidth+=Math.floor(cellRound/numPeople);	// give an extra pixel or two to the info cells
		cellRound%=numPeople;	// the number of pixels left over
	}
	/////////////////////////////////////////////////////
	// height calc
	height-=37+28;	// height of top banner
	height-=32;	// scroller
	height-=50+2;	// top labels
	height-=1+2;	// heigh of top border
	height-=4;	// don't know why
	//alert(height);
	//alert(Grid.m_rowInfo.rows);
	if (Grid.m_rowInfo.rows*(minHeight+2)<height) { // check for extra room
		minHeight=Math.floor(height/Grid.m_rowInfo.rows);
		minHeight-=2;	// account for padding
	}
	//alert(minHeight);
	if (Grid.m_showSalesman) {
		if (minHeight < 67) {
			minHeight = 67;
		}
	}
	var thisLabel = Dates.m_thisMonthName;
	var lastLabel = Dates.m_lastMonthName;
	var prevLabel = Dates.m_previousMonthName;
	if (g_company[0][g_company.ClosePeriod] == 'q') {
		thisLabel = Dates.m_thisQuarter;
		lastLabel = Dates.m_lastQuarter;
		prevLabel = Dates.m_previousQuarter;
	}

	var tCell = '<td height="1%" colspan=' + numPeople + '><img src="../images/ivorybox_t.jpg" height="10" width="100%"></td>';
	var bCell = '<td height="1%" colspan=' + numPeople + '><img src="../images/ivorybox_b.jpg" height="11" width="100%"></td>';
	var sbCell = '<td height="1%" colspan=' + numPeople + '><img src="../images/stalledbox_b.jpg" height="11" width="100%"></td>';

	var sblCell = '<td width="1%" height="1%"><img src="../images/stalledbox_bl.jpg" height="11" width=8></td>';
	var sbrCell = '<td width="1%" height="1%"><img src="../images/stalledbox_br.jpg" height="11" width=11></td>';

	var smCells = new Array('<td width=11><img src="../images/ivorybox_r.jpg" height="100%" width=11></td><td width=8><img src="../images/stalledbox_l.jpg" height="100%" width=8></td>',
	'<td width=11 nowrap><img src="../images/stalledbox_r.jpg" height="100%" width=11></td><td width=8><img src="../images/stalledbox_l.jpg" height="100%" width=8></td>',
	'<td width=11 nowrap><img src="../images/stalledbox_r.jpg" height="100%" width=11></td><td width=8><img src="../images/ivorybox_l.jpg" height="100%" width=8></td>');

	var sbCells = new Array('<td colspan=2 width="1%" height="1%"><img src="../images/ivorybox_br.jpg" height="11" width=11><img src="../images/stalledbox_bl.jpg" height="11" width=8></td>',
	'<td colspan=2 width="1%" height="1%"><img src="../images/stalledbox_br.jpg" height="11" width=11><img src="../images/stalledbox_bl.jpg" height="11" width=8></td>',
	'<td colspan=2 width="1%" height="1%"><img src="../images/stalledbox_br.jpg" height="11" width=11><img src="../images/ivorybox_bl.jpg" height="11" width=8></td>');


	var stalledHeaderRow = (Grid.m_rowInfo.rows - Grid.m_rowInfo.stalled) - 1;

	var rowCnt=Grid.m_rowInfo.rows+2;	// one for headers, another for spacers
	//	if (Grid.m_showSalesman) rowCnt++;	// another row for the spacers
	var lineCol='<td width="11"  rowspan='+rowCnt+' height="100%" style="background-image: url(../images/grid-border.gif);background-repeat: repeat-y;background-position: 50% 50%;"><img src="../images/grid-border.gif" width=11></td>';
	//	var lineCol='<td width="11"  rowspan='+rowCnt+' height="100%"><img src="../images/grid-border.gif" height=100% width=11></td>';
	// the following line will add white space between the salespeople buttons and the grid
	//	ret += '<tr><td colspan="'+(cellcnt+1+(bTarg ? 4 : 3))+'" bgcolor="white" height=1><img src="../images/spacer.gif" height=1></td></tr>';
	ret += '<tr><td colspan="'+(cellcnt+1+(bTarg ? 4 : 3))+'" bgcolor="black" height=1><img src="../images/spacer.gif" height=1></td></tr>';

	ret += '<tr style="height:50">';

	if (bTarg) {
		ret += Grid.makeGridHeaderNew(catLabel('Target'), 'head', 'grid_header_new', numPeople, 10,'');
		ret += lineCol;
	}

	ret += Grid.makeGridHeaderNew(catLabel('First Meeting'), 'head','grid_header_new', numPeople, 1,'');
	ret += lineCol;

	ret += Grid.makeGridHeaderNew(catLabel('Information Phase'), 'head','grid_header_new', numPeople, 2,'');
	ret += lineCol;

	ret += Grid.makeGridHeaderNew(catLabel('Decision Point'), 'head','grid_header_new', numPeople, 4,'');
	ret += lineCol;

	ret += Grid.makeGridHeaderNew(catLabel('Closed') + ' in ' + thisLabel, 'head','grid_header_new', numPeople, 6,'');
	//	ret += Grid.makeGridHeaderNew(catLabel('Closed') , 'head', 'grid_header_new', numPeople, 6,'rowspan=10');
	ret += '<td width="1"></td></tr>';


	// in order not to lose borders on salesmen screen, spacers are put in the top row,
	// and then the col widths are reduced by one to avoid the problem
	if (Grid.m_showSalesman) {
		//In mozilla, this screws up opportunity heights.  Why???????

		// set table cells 1 pixel smaller than forced spacers
		ret+='<tr>';
		for (i=0;i<(bTarg ? 5 : 4);i++) {
			ret+='<td height="1" width='+colwidth+'><img src="../images/spacer.gif" height=1 width='+colwidth+'></td>';
		}
		ret += '<td width="1"></td></tr>';
		colwidth-=1;

	} else {
		ret +='<tr height="1">';
		for (i=0;i<(bTarg ? 5 : 4);i++) {
			if (i==(bTarg ? 2 : 1)) {
				for (j=0;j<numPeople;j++) {
					ret+='<td height="1" width='+infowidth+'><img src="../images/spacer.gif" height=1 width='+infowidth+'></td>';
				}
			} else {
				for (j=0;j<numPeople;j++) {
					ret+='<td height="1" width='+colwidth+'><img src="../images/spacer.gif" height=1 width='+colwidth+'></td>';
				}
			}
		}
		ret += '<td width="1"></td></tr>';
	}

	var cellSuffix='" onclick="do_spreadsheet(this)"';
	//	cellSuffix += ' width="' + cellWidth + '" height="' + Grid.m_cellHeight + '">&nbsp;</td>';
	//	cellSuffix += ' style="width: ' + cellWidth2 + ';" height="' + Grid.m_cellHeight + '">&nbsp;</td>';

	//	NB: In order to make the cell height work for 'grouped' in Mozilla under the new scheme, height has to be an
	//	absolute val just as width is now.
	//	cellSuffix += ' height="' + Grid.m_cellHeight + '">&nbsp;</td>';

	//Does this screw up Larry's borders??--not so far as I can tell!
	var cellSuffixShrink=cellSuffix+'">&nbsp;</td>';
	if(Grid.m_peopleShown.length > 1) {
		cellSuffix += ' height="' + minHeight + '">&nbsp;</td>';
	} else {
		//		cellSuffix += ' height="' + Grid.m_cellHeight + '">&nbsp;</td>';
		cellSuffix += ' height="' + minHeight + '">&nbsp;</td>';
	}


	var i,id,k,j,temp;
	temp="";
	//alert('about to do loop');
	for (i = 0; i < Grid.m_rowInfo.rows; ++i) {
		var shrinkRow=(i==0 && Grid.ShowColMgrTots);

		var grid="grid_even";
		if (Grid.m_showSalesman || i%2) {
			grid="grid_odd";
		}

		temp += '<tr class="'+grid+'">';

		for (j = 0; j < (bTarg ? 5 : 4); ++j) {  //from 4
			if (i == stalledHeaderRow) {
				if (j == (bTarg ? 2 : 1)) {  //from 1
					continue;
				}
				if (j == (bTarg ? 3 : 2)) { //from 2
					continue;
				}
			}
			if (j == (bTarg ? 4 : 3)) { //from 3
				if (i == Grid.m_rowInfo.month) {
					temp += Grid.makeGridHeaderNew(catLabel('Closed') + ' in ' + lastLabel, 'head','grid_header_new', numPeople, 7, 'rowspan=2');
					continue;
				}
				if (i==Grid.m_rowInfo.month+1) {
					continue; // second row of the label
				}
				if (i== (Grid.m_rowInfo.month * 2)+2) {
					temp += Grid.makeGridHeaderNew(catLabel('Closed') + ' in ' + prevLabel, 'head','grid_header_new', numPeople, 8, 'rowspan=2');
					continue;
				}
				if (i == (Grid.m_rowInfo.month * 2)+3) {
					continue;
				}
			}
			if (!((i == stalledHeaderRow - 1) && (j == (bTarg ? 2 : 1) || j == (bTarg ? 3 : 2)))) {  //from 1 2
				var temp2="";
				var cwidth=colwidth;
				if (j==(bTarg ? 2 : 1)) {
					cwidth=infowidth;
				}
				for (k = 0; k < numPeople; ++k) {
					id = 'r' + i + 'c' + (((numPeople + 1) * j) + k);
					temp2 += '<td width="'+cwidth+'" id="' + id;
					if (shrinkRow) {
						temp2+=cellSuffixShrink;
					}
					else temp2+=cellSuffix;
				}
				temp+=temp2;
			}


			if (i == stalledHeaderRow - 1) {
				if (j == (bTarg ? 2 : 1) || j == (bTarg ? 3 : 2)) { //from 1 2 error!!
					temp += Grid.makeGridHeaderNew(catLabel('Stalled'), 'red','grid_header_red', numPeople, (j == (bTarg ? 2 : 1)) ? 3 :5,'rowspan=2');
				}
			}
		}


		if (shrinkRow) {
			temp+="<td width=1></td>";
		} else {
			temp +=  '<td width=1 ><img src="../images/spacer.gif" height="'+minHeight+'" width=1></td>';
		}
		temp +='</tr>';
		// RUSS, here's how to adjust the string cat
		if (i%5==0) {
			ret += temp;
			temp="";
		}
	}
	ret+=temp;

	ret += '</table>';
	return ret;
}

var blinkCellOn = true;
function updateMtgBlink()
{
	for (var k in g_blinkers)
	{
		var cell = g_blinkers[k];
		//		var innerGuy = cell.children[0];
		var innerGuy = cell.childNodes[0];
		if (innerGuy)
		{
			if (blinkCellOn)
			{
				if (innerGuy.className=='raised') innerGuy.className='raisedred';
				if (innerGuy.className=='raised1') innerGuy.className='raised1red';
			}
			else
			{
				if (innerGuy.className=='raisedred') innerGuy.className='raised';
				if (innerGuy.className=='raised1red') innerGuy.className='raised1';
			}
		}
	}
	blinkCellOn = !blinkCellOn;
}

function showColTot(shownInd,category,actClose) {
	var pos = Grid.personIndex(Grid.m_peopleShown[shownInd][g_people.PersonID]);
	if (pos==-1) return;
	var cellID=Grid.calcCellID(pos,shownInd,category,actClose);
	// must call calcCellID even if showing nothing, in order to get alignment right
	if (Grid.allTotal[pos][category]) {
		var cell = document.getElementById(cellID);
		cell.innerHTML="<span class='grid_colTots'>"+Grid.allTotal[pos][category];
		cell.align="center";
		cell.style.padding="0";
		cell.style.border="0";
	}
}

function _Grid_fillGrid()
{
	//alert("Fillgrid");
	Grid.m_peopleTotals = new Array();
	for (var i = 0; i < g_people.length; ++i)
	Grid.m_peopleTotals[i] = new SalesTotal(g_people[i][g_people.PersonID]);

	if (Grid.ShowColMgrTots) {
		for (var i = 0; i<Grid.m_peopleShown.length;i++) {
			for (var j=1;j<=5;j++)
			showColTot(i,j);
			if (Grid.TargetUsed) showColTot(i,10);
			showColTot(i,6,thisBegin);
			showColTot(i,7,lastBegin);
			showColTot(i,8,prevBegin);
		}
	}

	for (var i = 0; i < g_opps.length; ++i)
	{
		if (Grid.addOpportunity(g_opps[i]))
		break;

	}

	blinkCellOn=true;
	if (window.g_accessType == 2)
	updateMtgBlink();
	else
	{
		if (!this.doneOnce)
		{
			setInterval("updateMtgBlink();", 750);
			this.doneOnce = true;
		}
	}

	//alert("End Fillgrid");
}

function setColTots()
{

}

function insert_break_old(strAcctName, maxlen)
{
	len=strAcctName.length;
	var offset=0;
	var pos=-1;
	var retstr = '';
	var strTmp = strAcctName;
	do
	{

		pos=strTmp.indexOf(' ');
		var nxt = (pos > -1) ? pos : strTmp.length;

		if((nxt + retstr.length) > maxlen)
		{
			if (retstr.length) retstr += '<br>';
			retstr += strTmp;
			break;
		}
		if (pos < 0)
		{
			retstr += strTmp;
			break;
		}
		pos++;
		retstr += strTmp.substr(0, pos);
		strTmp = strTmp.substr(pos);

	}
	while(1);
	return retstr;
}

function insert_break(strAcctName, maxlen)
{
	var offset = 0;
	var retstr = '';
	var pos = -1;
	var lastbr = 0;
	do
	{
		pos = strAcctName.indexOf(' ', offset);
		var nxt = (pos > -1) ? 	pos+1 : strAcctName.length;
		if ((nxt - lastbr) > maxlen)
		{
			if (retstr.length)
			{
				retstr += '<br>';
				lastbr = offset;
			}
		}
		retstr += strAcctName.substring(offset, nxt);
		//alert("offset=" + offset + " pos=" +pos + " nxt=" + nxt + " lastbr=" + lastbr);
		//alert("In: " + strAcctName + "\nout: " + retstr);
		if (pos < 0) break;
		offset = (pos+1);
	}
	while(1);
	return retstr;
}

/* moved to utils.js
//Construct dates built on d/m/yr; do arithmetic
function getDaysOld(strDate)
{
var now=new Date();
var strNow = (now.getMonth()+1)*1 + '/' + now.getDate() + '/' + now.getFullYear();
var then=new Date(strDate);
var strThen = (then.getMonth()+1)*1 + '/' + then.getDate() + '/' + then.getFullYear();
var dNow = new Date(strNow);
var dThen = new Date(strThen);
return 	Math.round((dNow.getTime() - dThen.getTime()) / 86400000);
}
*/

function _Grid_addOpportunity(opp, optionalCellID)
{
	var bTarg = Grid.TargetUsed;
	var bVal = Grid.ValuationUsed;

	// eliminate opps the easiest way first, to eliminate extra processing
	var cat = opp[g_opps.Category];
	if (cat == 9) return false; // removed opportunity
	if (!bTarg && cat == 10) return false; //Targets not displayed

	var shownPos = Grid.personShownIndex(opp[g_opps.PersonID]);
	if (shownPos == -1) return false;  // not on the board

	var pos = Grid.personIndex(opp[g_opps.PersonID]);
	if (pos == -1)
	{
		alert('Grid.addOpportunity: pos == ' + pos);
		return false; // should never happen
	}
	var d="";
	if (opp[g_opps.ActualCloseDate] != '')
	d = Dates.makeDateObj(Dates.mssql2us(opp[g_opps.ActualCloseDate])).getTime();
	var cellID = optionalCellID ? optionalCellID : Grid.calcCellID(pos, shownPos, opp[g_opps.Category],d);
	if (cellID == '') return false;
	// TESTING ONLY PLS REMOVE
	//if (0)
	//{

	var cell = document.getElementById(cellID);
	if (cell == null)
	{
		//alert("Null cell reference! DealID = " + opp[g_opps.DealID] + " cellid = <" + cellID +"> Company: " +  opp[g_opps.Company]);
		//return true;

		// this can happen, when editing opportunities, if a bunch of
		// deals are put into a category and it "overflows".  in that case the grid
		// will need to be redrawn with more available space in that category.
		//alert("Upsizing grid");
		Grid.upSize();
		document.getElementById('theGrid').innerHTML = Grid.makeGrid();
		Grid.fillGrid();
		return true;
	}
	var id = 'i' + opp[g_opps.DealID];

	var className="raised1";
	if (Grid.m_showSalesman || cat == 2 || cat==3) className="raised";	// 2 pixel thick border if saleman board or information phase

	var html = '<div class="'+className+'" style="font-size: 2px; height: 100%; width: 100%; background-color: ' + opp.color + '" id="' + id + '">';
	//	var html = '<div style="font-size: 2px; height: 100%; width: 100%; background-color: ' + opp.color + '" id="' + id + '">';

	//Show valuation aligned right, after company name
	if (Grid.m_peopleShown.length == 1)
	{
		var now = new Date();
		var dspVLevel = (bVal && (opp[g_opps.VLevel] > 0)) ? opp[g_opps.VLevel] : '';
		html += '<div class="grid_label">';
		//		html += '<table style="font-size: 125%;" width="100%" border="0">';
		//Reduce font size per Allan, 11/17/04
		html += '<table style="font-size: 100%;" width="100%" border="0"';
		if (cat == 10)  //to bottom-align date assigned and days old 7/24/2006
		html += ' height="100%"';
		html +='>';

		//		html += '<tr><td align="left" nowrap>' + insert_break(opp[g_opps.Company], 22) + '</td>';
		html += '<tr><td align="left" valign="top"> ' + opp[g_opps.Company];
		if (opp[g_opps.Division] != '')
		html += '<br>'+opp[g_opps.Division];
		if (opp[g_opps.Contact] != '')
		html += '<br>'+opp[g_opps.Contact];
		html += '</td>';

		html += '<td align="right" valign="top"><nobr><b>' + dspVLevel + '</b>';
		if (opp[g_opps.Category] == 10)
		{
			if (opp[g_opps.Renewed] == 1)
			html += '<br><span style="position:relative; top:8px; font-weight:bold;">'+g_company[0][g_company.RevivedLabel]+'</span>';
			else if (opp[g_opps.AutoReviveDate] != '')
			html += '<br><span style="position:relative; top:8px; font-weight:bold;">'+g_company[0][g_company.RenewedLabel]+'</span>';
		}
		else if (opp[g_opps.Transactional]=='1') html+='<br><span style="position:relative; top:8px; font-weight:bold;">'+g_company[0][g_company.Translabel]+'</span>';
		html += '</nobr></td></tr>';

		if (cat == 10) // This is a target!
		{

			var td = Dates.mssql2us(opp[g_opps.TargetDate]);

			if(td)
			{
				dateTd = Dates.makeDateObj(td);
				var days = getDaysOld(td);
				var prevdays = opp[g_opps.PrevDaysOld];

				html += '<tr height="1"><td colspan="2" valign="top"><table style="font-size: 100%;" width="100%" height="100%" cellpadding="0" cellspacing="0" ><tr><td nowrap align="left" valign="bottom">' + Dates.formatDate(dateTd) + '</td>';
				html += '<td nowrap align="right" valign="bottom">' + days;
				if(prevdays > 0)
				html += '+' + prevdays;
				html += ' Days Old</td></tr></table></td></tr>';

			}
			else
			html += '<tr><td>No Date Assigned</td><td></td></tr>';
			html += '</table></div>';
		}
	}
	else
	html += '<br>';


	if(cat != 10 && cat != 1)
	{

		var goodColor = 'black';
		var badColor = opp.color;

		// transactional opps get a "T" instead of a milestone grid
		if (opp[g_opps.Transactional]=='1' && Grid.m_peopleShown.length != 1)
		{
			html += '<table align="center" width="90%" height=9 cellspacing=0 cellpadding=0><tr><td style="font-size:8px;font-family:sans-serif" align="center" valign="top">';
			html += '<b>'+g_company[0][g_company.TransAbbr]+'</b>';
			html += '</td></tr></table>';

		}
		else if (opp[g_opps.Transactional]!='1')
		{
			html += '<table align="center" width="90%" height=9 bgcolor="gray" cellspacing=1 cellpadding=0><tr>';
			if(cat == 2 || cat == 3)
			{
				if (g_company[0][g_company.Requirement1used] == '1')
				{
					html += '<td class="grid_milestone" width="16%" bgcolor="' + ((opp[g_opps.Requirement1] == '1') ? goodColor : badColor) + '">&nbsp;</td>';
				}

				html += '<td class="grid_milestone" width="16%" bgcolor="' + ((opp[g_opps.Person] == '1') ? goodColor : badColor) + '">&nbsp;</td>';

				html += '<td class="grid_milestone" width="16%" bgcolor="' + ((opp[g_opps.Need] == '1') ? goodColor : badColor) + '">&nbsp;</td>';

				html += '<td class="grid_milestone" width="16%" bgcolor="' + ((opp[g_opps.Money] == '1') ? goodColor : badColor) + '">&nbsp;</td>';

				html += '<td class="grid_milestone" width="16%" bgcolor="' + ((opp[g_opps.Time] == '1') ? goodColor : badColor) + '">&nbsp;</td>';

				if (g_company[0][g_company.Requirement2used] == '1')
				{
					var lblReq2 = g_company[0][g_company.Requirement2];
					html += '<td class="grid_milestone" width="16%" bgcolor="' + ((opp[g_opps.Requirement2] == '1') ? goodColor : badColor) + '">&nbsp;</td>';
				}
			}
			else if (cat == 4 || cat == 5 || cat == 6)
			{
				html += '<td class="grid_milestone" width="64%" bgcolor="' + goodColor + '">&nbsp;</td>';
			}
			html += '</tr></table>';
		}

		if (opp.border && (cat == 2 || cat == 3 || cat == 4 || cat == 5))
		{
			html += '<table align="center" width="90%" height=9 bgcolor="gray" cellspacing=1 cellpadding=0><tr>';
			//			html += '<td class="grid_milestone" bgcolor="' + opp.border + '">&nbsp;</td></tr></table><br>';
			html += '<td class="grid_milestone" bgcolor="' + opp.border + '">&nbsp;</td></tr></table>';

		}
		//html+=html2;
	}


	//If stalled, show date entered & # of days since
	//If reassigned, include that count as well
	if (Grid.m_peopleShown.length == 1  && (cat == 3 || cat == 5))
	{
		var lm;
		if (opp[g_opps.LastMoved] == '')
		lm=  Dates.mssql2us(opp[g_opps.FirstMeeting]);
		else
		lm = Dates.mssql2us(opp[g_opps.LastMoved]);
		var dateLm = Dates.makeDateObj(lm);
		var days = getDaysOld(lm);
		var td = Dates.mssql2us(opp[g_opps.TargetDate]);
		var daysAssigned = getDaysOld(td);
		if (daysAssigned < days)
		days=daysAssigned;
		var prevdays = opp[g_opps.PrevDaysOld];
		html += '<table style="font-size: 100%;" width="100%" border="0">';
		html += '<tr><td nowrap align="left">' + Dates.formatDate(dateLm) + '</td>';
		html += '<td nowrap align="right">' + days;
		if(prevdays && prevdays > 0)
		html += '+' + prevdays;
		html += ((days==1)?' Day ':' Days ') + catLabel('Stalled') + '</td></tr>';
		html += '</table>';
	}


	/*
	//DBG: if sidebyside, show cellID
	if(Grid.m_currentView.view == 'sidebyside')
	{
	html += '<table><tr><td height=9 align="center"> CAT=' + opp[g_opps.Category] + '</tr></td></table>';
	}
	*/


	cell.innerHTML = html;

	document.getElementById(id).parentCell=cell;

	//?


	if (cat != 6 && cat !=10 && cat !=3 && cat !=5)
	{
		var today = new Date();
		var Days;
		var MissedMeetingGracePeriod=0;	// no grace on salesman's screen
		if (window.g_accessType == 2)
		MissedMeetingGracePeriod=g_company[0][g_company.MissedMeetingGracePeriod];

		if (cat==1) Days=DaySpan(today,opp[g_opps.FirstMeeting]);
		else Days=DaySpan(today,opp[g_opps.NextMeeting]);
		if (Days>MissedMeetingGracePeriod)
		g_blinkers[id] = cell;

	}



	// TESTING ONLY PLS REMOVE
	//}
	return false;
}

/*
function getMilestone(fldname)
{
switch(fldname)
{
case 'Req1':
return opp[g_opps.Requirement1];
case 'Person':
return opp[g_opps.Person];
case 'Need':
return opp[g_opps.Need];
case 'Time':
return opp[g_opps.Time];
case 'Money':
return opp[g_opps.Money];
case 'Req2':
return opp[g_opps.Requirement2];
}
}

function writeMouseoverAnchor(locid, dealid)
{
return 'onMouseOver="openMSWindow(' + locid + ', ' + dealid + ')" ';
}

function closeIt()
{

if (!myWin.closed)
myWin.self.close();
}

//Milestone window
function openMSWindow(locid, dealid)
{

var fldname, location;
switch(locid)
{
case 0:
fldname = 'Req1';
location = g_company[0][g_company.Requirement1];
break;
case 1:
fldname = location = 'Person';
break;
case 2:
fldname = location = 'Need';
break;
case 3:
fldname = location = 'Money';
break;
case 4:
fldname = location = 'Time';
break;
case 5:
fldname = 'Req2';
location = g_company[0][g_company.Requirement2];

}


Windowing.dropBox.mainBoardWin = window;
Windowing.dropBox.allChecked = getMilestone(fldname) == '1';
myWin = Windowing.openSizedPrompt('../shared/subms_prompt.php?fieldname=' + fldname + '&location=' + location + '&location_id=' + locid + '&oppid=' + dealid + '&display_only=1', 320, 600);


}
*/

function _Grid_removeOpportunity(opp, temporarily)
{
	var id = 'i' + opp[g_opps.DealID];
	var kid = document.getElementById(id);
	if (!kid)
	return '';

	var cellID = kid.parentCell.id;
	kid.parentCell.removeChild(kid);
	if (temporarily)
	return cellID;


	var pos = Grid.personIndex(opp[g_opps.PersonID]);
	if (pos == -1 || pos >= g_people.length)
	{
		alert('Grid.removeOpportunity: pos == ' + pos);
		return ''; // should never happen
	}

	var personTotal = Grid.m_peopleTotals[pos];
	var category = opp[g_opps.Category];
	personTotal['m_category' + category]--;

	// reflow the column where the item was removed
	var row = parseInt(cellID.substr(cellID.indexOf('r') + 1));
	var col = parseInt(cellID.substr(cellID.indexOf('c') + 1));

	var oldCell = document.getElementById(cellID);
	while (true)
	{
		var cellID = 'r' + (++row) + 'c' + col;
		var cell = document.getElementById(cellID);
		if (!cell) break;

		var div = null;
		for (var i = 0; i < cell.childNodes.length; ++i)
		{
			var kid = cell.childNodes[i];
			if (kid.tagName == null)
			continue;
			if (kid.tagName.toLowerCase() == 'div' && kid.id && kid.id.length && kid.id.charAt(0) == 'i')
			{
				div = kid;
				break;
			}
		}

		if (div == null)
		{
			cell.style.border = 'none';
			break; // empty cell in grid == end of column
		}

		cell.removeChild(div);
		oldCell.appendChild(div);
		div.parentCell = oldCell;
		oldCell = cell;
	}

	return '';
}

// this version of the function works for both grouped and side-by-side view, but does
// not allow the user to click in empty space on the board to open the spreadsheet view
// (he must click on a board cell containing an opportunity .. see the old version below)
function _Grid_getCellInfo(forCell)
{
	if (Grid.m_peopleShown.length == 1 && forCell.id.length > 3 && forCell.id.substr(0, 3) == 'cat')
	{
		var info = new Object();
		info.category = forCell.id.substr(3);
		info.person = Grid.m_peopleShown[0][g_people.PersonID];
		return info;
	}

	var oppID = 0;
	for (var i = 0; i < forCell.childNodes.length; ++i)
	{
		var kid = forCell.childNodes[i];
		if (kid.tagName == null) continue;
		//if (kid.tagName.toLowerCase() != 'div') continue;
		if (!kid.id || !kid.id.length || kid.id.charAt(0) != 'i') continue;
		var id = parseInt(kid.id.substr(1), 10);
		if (isNaN(id)) continue;
		oppID = id;
		break;
	}

	if (oppID == 0) return null;

	var opp = find_opportunity(oppID);
	if (!opp) return null;

	var info = new Object();
	info.category = opp[g_opps.Category];
	info.person = opp[g_opps.PersonID];
	return info;
}

// this version works for empty cells, but is not correct for side-by-side view
// (see the newer version of this function above)
/*function _Grid_getCellInfo(forCell)
{
if (Grid.m_peopleShown.length == 1 && forCell.id.length > 3 && forCell.id.substr(0, 3) == 'cat')
{
var info = new Object();
info.category = forCell.id.substr(3);
info.person = Grid.m_peopleShown[0][g_people.PersonID];
return info;
}

var row = parseInt(forCell.id.substr(forCell.id.indexOf('r') + 1));
var col = parseInt(forCell.id.substr(forCell.id.indexOf('c') + 1));

var category = 1;
if (col >= ((Grid.m_peopleShown.length + 1) * 3)) category = 6;
else if (col >= ((Grid.m_peopleShown.length + 1) * 2)) category = 4;
else if (col >= (Grid.m_peopleShown.length + 1)) category = 2;

if ((category == 2 || category == 4) && row >= (Grid.m_rowInfo.rows - Grid.m_rowInfo.stalled)) ++category;
else if (category == 6)
{
if (row >= (Grid.m_rowInfo.month + 1) * 2) category += 2;
else if (row >= (Grid.m_rowInfo.month + 1)) ++category;
}

var index = col % (Grid.m_peopleShown.length + 1);

var info = new Object();
info.category = category;
info.person = Grid.m_peopleShown[index][g_people.PersonID];

return info;
}
*/

function update_view_checksib(id)
{
	if (g_sib_mode)
	return;
	update_view(id);
}

function getUserLevel()
{
	var the_level = null;
	if (window.g_effective_user)
	the_level = window.g_effective_user[0][window.g_effective_user.Level];
	else
	the_level = window.loginResult;
	return the_level;
}

if (g_showaffiliates)
{
	function getAffCompany(Companyid)
	{
		for (var i=0;i<g_affcompany.length;i++)
		if (g_affcompany[i][g_affcompany.CompanyID]==Companyid) return g_affcompany[i][g_affcompany.Name];
		return '';
	}
}

//11/21/2005 Need to determine user level.  If user == salesperson, he could be returning from
//calendar view
function _Grid_makeScroller()
{
	var hideBackButton = (getUserLevel() == 1);
	// Remove after testing
	//var ret = '<table height=32 width="100%" cellspacing=1 cellpadding=0 border=1 background="../images/scroll_bg.gif">';
	var ret = '<table height=32 width="100%" cellspacing=2 cellpadding=0 border=0 bgcolor="black">';
	ret += '<tr align="center" onselectstart="return false;">';
	if (Grid.m_showSalesman)
	{
		//		if (Grid.m_previousView != null)
		if (Grid.m_previousView != null && !hideBackButton)
		{
			// if the current user is not a manager or does not have view-sibling permission,
			// this does not show the "view-all" button -- since he can never switch views,
			// Grid.m_previousView will always be null
			ret += '<td height=32 width=10><button id="scrollbutton_lb" class="command" onclick="update_view(\'previous\')"><b>Back</b></button></td>';
		}
	}
	else if (g_people.length>Grid.m_maxPeopleShown)
	{
		ret += '<td height=32 width=20><button id="scrollbutton_l5" class="command_new" onclick="Grid.scrollLeft(Grid.m_maxPeopleShown)"><img src="../images/left.gif"><img src="../images/left.gif"></button></td>';
		ret += '<td height=32 width=10><button id="scrollbutton_l1" class="command_new" onclick="Grid.scrollLeft(1)"><img src="../images/left.gif"></button></td>';
	}

	var cellWidth = Math.floor(100 / Grid.m_peopleShown.length) + '%';
	for (var i = 0; i < Grid.m_peopleShown.length; ++i)
	{
		var data = Grid.m_peopleShown[i];

		ret += '<td bgcolor="' + data[g_people.Color] + '" width="' + cellWidth + '" height=32';

		if (Grid.m_peopleShown.length > 1)
		{
			var id = data[g_people.PersonID];
			//			ret += ' class="scroller raised" onmousedown="press_fake_button(this)" onmouseup="lift_fake_button(this); update_view_checksib(' + id + ')" style="cursor: default">';
			ret += ' class="scroller" onmousedown="press_fake_button(this)" onmouseup="lift_fake_button(this); update_view_checksib(' + id + ')" style="cursor: default">';
		}
		else
		ret += ' class="scroller" style="cursor: default">';

		var tempinfo = get_salesperson_info(data[g_people.PersonID]);
		if (tempinfo.name)
		{
			var dspRating = tempinfo.rating > -1 ? tempinfo.rating : "Unk."; //per Allan Ellison 9/22/04
			ret += "<table border='0' width=100% cellspacing=0 cellpadding=0><tr><td width=4><img src='../images/left-edge-black.gif'></td><td class='scroller'";
			if(g_showRatingOnBoard == true && g_user[0][g_people.RatingViewAllowed] == '1')
			ret += "width=1 ";
			if (!Grid.m_showSalesman)
			{

				//				if (g_showaffiliates)
				//				{
				//					ret += "align=\"center\" nowrap><nobr>" + tempinfo.rawdata[g_people.FirstName]+'&nbsp;'+tempinfo.rawdata[g_people.LastName] + '<br>'+getAffCompany(tempinfo.rawdata[g_people.CompanyID])+'</nobr></td>';
				//				}
				//				else
				{
					ret += "align=\"center\" nowrap><nobr>" + tempinfo.rawdata[g_people.FirstName]+'<br>'+tempinfo.rawdata[g_people.LastName]+((tempinfo.tenure == 'New')?'&nbsp;(n)':'') + '</nobr></td>';
				}
			}
			else
			ret += "nowrap><nobr>" + tempinfo.name + '</nobr></td>';
			//			if(g_user[0][g_people.ShowRatingOnBoard] == '1' && g_user[0][g_people.RatingViewAllowed] == '1')
			if(g_showRatingOnBoard == true && g_user[0][g_people.RatingViewAllowed] == '1')
			{
				if (Grid.m_showSalesman)
				ret += '<td nowrap align="center" class="scroller">Rating: ';
				else
				ret += '<td nowrap align="right" class="scroller">';
				ret += dspRating+'</td>';
			}
			if (Grid.m_showSalesman)
			{
				var avgVal = 0;
				var avgValCount = 0;
				for (i = 0; i < g_opps.length; ++i)
				{
					if (g_opps[i][g_opps.Category] != 6 && g_opps[i][g_opps.Category] != 9 && g_opps[i][g_opps.PersonID] == data[g_people.PersonID] && g_opps[i][g_opps.VLevel] != '')
					{
						var tempval = parseInt(g_opps[i][g_opps.VLevel]);
						if (tempval > 0)
						{
							avgVal += tempval;
							avgValCount++;
						}
					}
				}
				if (avgValCount > 0)
				{
					avgVal = avgVal / avgValCount;
					avgVal = Math.floor(avgVal)+'.'+Math.round((avgVal*10)%10);
				}
				if(g_showRatingOnBoard == true && g_user[0][g_people.RatingViewAllowed] == '1')
				ret += '<td nowrap align="right" class="scroller" width="1" nowrap>';
				else
				ret += '<td nowrap align="right" class="scroller" nowrap>';
				ret += '<nobr>Avg '+g_company[0][g_company.ValLabel]+': '+avgVal+'</nobr></td>';
			}
			ret += "<td width=4><img src='../images/right-edge-black.gif'></td></tr></table></td>";
		}
		else
		ret += '</td>';
	}
	//  + '</td><td nowrap bgcolor="' + data[g_people.Color] + '">&nbsp;' + tempinfo.rating + '&nbsp;</td>';

	if (Grid.m_showSalesman)
	{
		//		if (Grid.m_previousView != null)
		if (Grid.m_previousView != null && !hideBackButton)
		{
			ret += '<td height=32 width=10><button id="scrollbutton_rb" class="command" onclick="update_view(\'previous\')"><b>Back</b></button></td>';
		}
	}
	else if (g_people.length>Grid.m_maxPeopleShown)
	{
		ret += '<td height=32 width=10><button id="scrollbutton_r1" class="command_new" onclick="Grid.scrollRight(1)"><img src="../images/right.gif"></button></td>';
		ret += '<td height=32 width=20><button id="scrollbutton_r5" class="command_new" onclick="Grid.scrollRight(Grid.m_maxPeopleShown)"><img src="../images/right.gif"><img src="../images/right.gif"></button></td>';
	}

	ret += '</tr></table>';
	return ret;
}

function _Grid_scrollLeft(howMany)
{
	if (!Grid.enable) return;
	if (Grid.m_currentView.firstShown == 0) return;

	document.body.style.cursor = 'wait';

	Grid.m_currentView.firstShown -= howMany;
	if (Grid.m_currentView.firstShown < 0) Grid.m_currentView.firstShown = 0;
	Grid.showPeople();

	document.getElementById('theGrid').innerHTML = Grid.makeGrid();
	document.getElementById('theScroller').innerHTML = Grid.makeScroller();
	Grid.fillGrid();

	document.body.style.cursor = 'auto';
}

function _Grid_scrollRight(howMany)
{
	if (!Grid.enable) return;
	if (g_people.length <= Grid.m_peopleShown.length + Grid.m_currentView.firstShown) return;
	if (g_people.length < howMany+Grid.m_peopleShown.length+Grid.m_currentView.firstShown)
	howMany-=(howMany+Grid.m_peopleShown.length+Grid.m_currentView.firstShown)-g_people.length;

	document.body.style.cursor = 'wait';

	Grid.m_currentView.firstShown += howMany;
	Grid.showPeople();

	document.getElementById('theGrid').innerHTML = Grid.makeGrid();
	document.getElementById('theScroller').innerHTML = Grid.makeScroller();
	Grid.fillGrid();

	document.body.style.cursor = 'auto';
}

function _Grid_setView(whichView)
{
	if (!Grid.enable) return;

	document.body.style.cursor = 'wait';
	if (whichView == 'previous' && Grid.m_previousView != null)
	{
		Grid.m_currentView = Grid.m_previousView;
		if(Grid.m_earlierView) Grid.m_previousView = Grid.m_earlierView;
		//If user==salesperson, he now may be returning from calendar view, in which case view = 'grouped' but single SP
		if(Grid.m_currentView.view == 'grouped' || Grid.m_currentView.view == 'sidebyside')
		{
			if(getUserLevel() == 1)
			{
				Grid.showPerson(Grid.firstShown()[g_people.PersonID]);
			}
			else
			Grid.showPeople();
		}
		else
		Grid.showPerson(Grid.m_currentView.view);
	}
	else
	{
		Grid.m_earlierView = Grid.m_previousView;
		Grid.m_previousView = Grid.m_currentView;
		Grid.m_currentView = new Object();
		Grid.m_currentView.view = whichView;
		Grid.m_currentView.firstShown = 0;
		if (whichView == 'grouped' || whichView == 'sidebyside')
		{
			if(getUserLevel() == 1)
			Grid.showPerson(Grid.firstShown()[g_people.PersonID], false);
			else
			Grid.showPeople();
		}
		else if(whichView == 'calendar')
		{
			toggleButtons(false);
			Calendar.Type = 'month';
			//			if(Grid.numPeopleShown() == 1)
			var prev = Grid.m_previousView.view;
			var showRoster = (getUserLevel() > 1);
			if(prev != 'grouped' && prev != 'sidebyside')
			{
				Calendar.makeRoster(prev, showRoster);
			}
			else
			{
				if(!showRoster) //user == salesman
				Calendar.makeRoster(Grid.firstShown()[g_people.PersonID], false);
				else
				Calendar.makeRoster();
			}
			Calendar.makeCalendar();
			set_cookie('view_mode',Grid.m_currentView.view);
			set_cookie('view_previous_mode',Grid.m_previousView.view);
			document.body.style.cursor = 'auto';
			return;
		}
		else Grid.showPerson(whichView); // individual view
	}

	set_cookie('view_mode',Grid.m_currentView.view);
	set_cookie('view_previous_mode',Grid.m_previousView.view);
	//Persist for sorting
	set_cookie('sort_in_effect', g_sortInEffect);
	set_cookie('sort_descending', (g_sortDescending == true ? '1' : '0'));
	//Persist for view ratings
	set_cookie('view_rating_on_board', (g_showRatingOnBoard == true ? '1' : '0'));
	document.getElementById('theGrid').innerHTML = Grid.makeGrid();
	document.getElementById('theScroller').innerHTML = Grid.makeScroller();
	Grid.fillGrid();
	document.body.style.cursor = 'auto';
}

function _Grid_getView()
{
	return Grid.m_currentView.view;
}


// ----------------------------- implementation (private)

/*
if this was to actually calculate the proper number of rows, it should use
the largest of the following values:
largest FM + 1
largest IP (including stalled) + 2
largest DP (including stalled) + 2
largest closed * 3
then increment until divisible by 3
*/
function _Grid_calcRows()
{
	var bTarg = Grid.TargetUsed;
	Grid.m_rowInfo = new Object();
	// count up how much is in each category
	Grid.allTotal = new Array();
	for (var i = 0; i < g_people.length; ++i)
	Grid.allTotal[i] = new Array(0,0,0,0,0,0,0,0,0,0,0);


	var ClosePeriod = g_company[0][g_company.ClosePeriod];
	var now = new Date();

	for (i = 0; i < g_opps.length; ++i)
	{
		var opp=g_opps[i];
		var category = opp[g_opps.Category];
		if (category == 9)
		continue; // removed opportunity
		if (!bTarg && category == 10)
		continue; //Targets not displayed  (why not continue??)
		var pos = Grid.personIndex(opp[g_opps.PersonID]);	// check is this ready yet?
		var personTotal = Grid.allTotal[pos];

		if (category == 6 || category == 7 || category == 8)
		{
			if (opp[g_opps.ActualCloseDate] == '') continue;
			if (ClosePeriod == 'q')
			{
				var qBegin = now.getMonth();
				while (qBegin % 3)
				--qBegin;
			}

			var d = Dates.makeDateObj(Dates.mssql2us(opp[g_opps.ActualCloseDate])).getTime();
			if (d >= thisBegin)
			{
				personTotal[6]++;
			}
			else if (d >= lastBegin)
			{
				personTotal[7]++;
			}
			else if (d >= prevBegin)
			{
				personTotal[8]++;
			}
			else continue;
		}
		else
		personTotal[category]++;

	}

	var rows=10;
	var stalled=3;
	var month=3;
	var maxTopStall=3;	// max of top of columns that  have stalled
	var extra=2;		// number of extra spaces to leave in each category
	for (var i = 0; i < g_people.length; ++i)
	{
		//		if (Grid.m_showSalesman && g_people[i][g_people.PersonID]!=Grid.m_peopleShown[0][g_people.PersonID]) continue;
		var personTotal=Grid.allTotal[i];
		if (personTotal[10]+extra>rows) rows=personTotal[10]+extra;
		if (personTotal[1]+extra>rows) rows=personTotal[1]+extra;
		if (personTotal[2]+extra>maxTopStall) maxTopStall=personTotal[2]+extra;
		if (personTotal[4]+extra>maxTopStall) maxTopStall=personTotal[4]+extra;
		if (personTotal[3]+extra>stalled) stalled=personTotal[3]+extra;
		if (personTotal[5]+extra>stalled) stalled=personTotal[5]+extra;
		if (personTotal[6]+extra>month) month=personTotal[6]+extra;
		if (personTotal[7]+extra>month) month=personTotal[7]+extra;
		if (personTotal[8]+extra>month) month=personTotal[8]+extra;
	}

	if (Grid.ShowColMgrTots) {		// add one extra in each cat for the totals
		rows++;
		month++;
		stalled++;
		maxTopStall++;
	}

	// adjust rows for stalled
	if (maxTopStall+stalled+2 > rows) rows=maxTopStall+stalled+2;
	else stalled=rows-maxTopStall-2;	// put extra room into stalled, so headings will be close to top

	if (month*3 + 4 > rows)
	{
		stalled+=(month*3 +4)-rows;	// put the extra room into stalled
		rows=month*3+4;
	}




	Grid.m_rowInfo.rows = rows;
	Grid.m_rowInfo.stalled = stalled;
	Grid.m_rowInfo.month = month;
	//	alert("Rows: "+(rows));
	//	alert("stalled: "+stalled);
	//	alert("month: "+month);
	//	alert("MaxTopStalled "+maxTopStall);

}

function _Grid_upSize()
{
	Grid.m_rowInfo.rows += 6;
	Grid.m_rowInfo.stalled += 2;
	Grid.m_rowInfo.month += 2;
}

function _Grid_makeGridHeader(label, className, colspan, category)
{
	var ret = '<td nowrap " height="' + Grid.m_cellHeight + '" colspan=' + colspan;

	if (category && Grid.m_peopleShown.length == 1)
	ret += ' id="cat' + category + '" onmousedown="press_fake_button(this)" onmouseup="lift_fake_button(this); do_spreadsheet(this)" style="cursor: default"';

	if (className && className.length)
	{
		if (Grid.m_peopleShown.length == 1) className += ' raised';
		ret += ' class="' + className + '"';
	}

	ret += '>' + label;
	ret += '</td>';
	return ret;
}

function _Grid_makeGridHeaderNew(label, pictname, className, colspan, category, extra)
{
	var realcat=category;
	if (category==7 || category==8) category=6;
	var ret = '<td nowrap valign="bottom" height="50" ';
	if (arguments.length > 6 && arguments[6] == 'stalled_sbs')
	{
		ret += 'style="border-top:0.05cm solid red;border-left:0.05cm solid red;border-right:0.05cm solid red" ';
	}

	if (colspan>1) ret += 'colspan=' + colspan;
	//	var ret = '<td nowrap valign="middle" colspan=' + colspan;

	if (category && Grid.m_peopleShown.length == 1)
	ret += ' id="cat' + category + '" onmousedown="press_fake_button(this)" onmouseup="lift_fake_button(this); do_spreadsheet(this)" style="cursor: default"';

	ret += ' '+extra+'>';
	ret += '<table width=100% cellpadding=0 cellspacing=0 border=0><tr><td><img src="../images/grid-'+pictname+'-left.jpg" ></td>';
	ret += '<td width=100% ';
	if (className && className.length)
	{
		//		if (Grid.m_peopleShown.length == 1) className += ' raised';
		ret += ' class="' + className + '"';
	}
	ret += '>'+ label;
	if (Grid.ShowColSalesTots) {
		ret+=' <span style="font-size:9px">('+Grid.allTotal[Grid.m_peopleShown[0].perInd][realcat]+')</span>';
	}
	ret += '</td><td><img src="../images/grid-'+pictname+'-right.jpg" align="middle"></td></tr></table></td>';
	return ret;
}


function _Grid_calcCellID(pos, shownPos, category, d) {
	var bTarg = Grid.TargetUsed;
	var personTotal = Grid.m_peopleTotals[pos];
	//	var category = opp[g_opps.Category];
	if (!bTarg && category == 10) {
		return false;
	}

	if (category == 7 || category == 8) {
		category = 6;
	}

	var row;
	if (category == 6) {
		//		if (opp[g_opps.ActualCloseDate] == '') return '';
		//		var d = Dates.makeDateObj(Dates.mssql2us(opp[g_opps.ActualCloseDate])).getTime();
		if (d >= thisBegin) {
			row = personTotal['m_category6t']++;
		} else if (d >= lastBegin) {
			row = personTotal['m_category6l']++;
			row += Grid.m_rowInfo.month + 2;
		} else if (d >= prevBegin) {
			row = personTotal['m_category6p']++;
			row += (Grid.m_rowInfo.month + 1) * 2;
			row+=2;
		} else {
			return '';
		}
	} else {
		row = personTotal['m_category' + category]++;
		if (category == 3 || category == 5) {
			row += (Grid.m_rowInfo.rows - Grid.m_rowInfo.stalled);
		}
	}

	var col;
	if (Grid.m_currentView.view == 'sidebyside') {
		col = shownPos * 5; // 5 == 4 basic categories + 1 spacer
		//		col = shownPos * (bTarg ? 6 : 5); // 6 == 5 basic categories + 1 spacer // Why doesn't it work this way?????

		if (bTarg && category != 10) {
			++col;
		}
		if (category > 1 && category != 10) {
			++col;
		}
		if (category > 3 && category != 10) {
			++col;
		}
		if (category > 5 && category != 10) {
			++col;
		}
	} else {
		col = shownPos;
		if (bTarg && (category == 1)) {
			col += Grid.m_peopleShown.length + 1;
		}

		if (category == 2 || category == 3) {
			col += (Grid.m_peopleShown.length + 1) * (bTarg ? 2 : 1);
		} else if (category == 4 || category == 5) {
			col += (Grid.m_peopleShown.length + 1) * (bTarg ? 3 : 2);
		} else if (category != 10 && category > 5) {
			col += (Grid.m_peopleShown.length + 1) * (bTarg ? 4 : 3);
		}
	}

	return 'r' + row + 'c' + col;
}

function _Grid_makeNewPerson() {
	var ret = new Array();
	for (var k in g_people)
	{
		var n = parseInt(k, 10);
		if (isNaN(n)) {
			ret[ret.length] = '';
		}
	}

	return ret;
}

function _Grid_personIndex(id) {
	for (var i = 0; i < g_people.length; ++i) {
		if (g_people[i][g_people.PersonID] == id) {
			return i;
		}
	}
	return -1;
}

function _Grid_personShownIndex(id) {
	for (var i = 0; i < Grid.m_peopleShown.length; ++i) {
		if (Grid.m_peopleShown[i][g_people.PersonID] == id) {
			return i;
		}
	}
	return -1;
}

function SalesTotal(id) {
	this.m_id = id;
	this.m_category1 = 0;
	this.m_category2 = 0;
	this.m_category3 = 0;
	this.m_category4 = 0;
	this.m_category5 = 0;
	this.m_category6t = 0;
	this.m_category6l = 0;
	this.m_category6p = 0;
	this.m_category10 = 0;
}
