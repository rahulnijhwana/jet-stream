g_admin_mode = true;

//Update screen labels if changed
g_reload_after_save = false;

isNav = (navigator.appName.indexOf("Netscape") != -1) ? true : false;
 
g_imgpath = "../images/";
g_sharedimgpath = "../images/";
g_dataurl = "../data/";
g_removedPeople = new Array();
g_removedOpps = new Array();
g_removedProducts = new Array();
g_levelNumMap = new Object();
g_productIDmap = new Object();
g_unassignedtree = null;
g_request_count = 0;
g_the_only_company = null;
g_company_tab_index = -1;
g_people_tab_index = -1;
g_opps_tab_index = -1;

//Lists subtabs
g_lists_tab_index = -1;
g_slip_stream_tab_index = -1;
g_valuations_tab_index = -1;
g_sources_tab_index = -1;
g_sources2_tab_index = -1;
g_removal_reasons_index = -1;
g_milestone_labels_index = -1;
g_category_labels_index = -1;
g_reports_index = -1;

g_products_tab_index = -1;
g_close_questions_tab_index = -1;
g_milestone_questions_tab_index = -1;
g_sub_questions_tab_index = -1;
g_demodata_tab_index = -1;

g_tableLevels = null;
g_hightest_level = -1;
g_opp_count = 0;
STATUS_COL_INDEX = -1;
SALESPERSON_COL_INDEX = -1;
COMPANY_COL_INDEX = -1;
OPP_COL_INDEX = -1;
FM_COL_INDEX = -1;
FORCASTCLOSE_COL_INDEX = -1;
ADJUSTEDCLOSE_COL_INDEX = -1;
CATEGORY_COL_INDEX = -1;
VALUATION_COL_INDEX = -1;
TARGET_COL_INDEX = -1;

g_old_search = '';
OPP_MODE_ALL = 1;
OPP_MODE_SEARCH = 2;
g_opp_mode = OPP_MODE_ALL;
g_search_count = 0;
g_temp_id_counter = 0 - Math.ceil(Math.random() * 100000);
g_selectedSp = null;
g_newest_sp = null;
g_opps = null;
g_doing_save = false;

//System Toggle Flags
g_target_used = false;
g_valuation_used = false;
g_source_used = false;
g_source2_used = false;

msPerDay = 24 * 60 * 60 * 1000;

var hasMilestoneQuestions = false;
var hasPersonQuestions = false;
var hasNeedQuestions = false;
var hasMoneyQuestions = false;
var hasTimeQuestions = false;
var hasReq1Questions = false;
var hasReq2Questions = false;
var hasDemoData = false;

var msqTabbedPane = null;
var smsTabbedPane = null;
var listsTabbedPane = null;

var g_person_q_index = -1;
var g_need_q_index = -1;
var g_money_q_index = -1;
var g_time_q_index = -1;
var g_req1_q_index = -1;
var g_req2_q_index = -1;

g_editedSubAnswers = new Array();

var g_subMSEnabled = false;

g_search_type = null;
g_search_value = null;
g_search_valuetext = null;
g_search_date_from = null;
g_search_date_to = null;

g_filter_list = Array();

var chartDirty = true;
var chartZoomElem = null;
var chartZoom = 100;
var spanPleaseWaitOrg = null;

var target_sms_table = null;
var target_location = null;
var edit_thing = null;
var edit_td = null;
var insert_at_ind = null;
var insert_at_seq = null;
var this_combo = null;
var source_ind = null;

var types_a = new Array("Alphanumeric", 1, "Numeric", 2, "Yes/No", 3, "Multiple Choice", 4);
var mandatory_a = new Array("Yes", 1, "No", 0, "Yes with optional N/A", 2);
var q_header_titles = new Array('Use', 'Name', 'Prompt', 'Type', '', 'Length', 'Fill Length', 'Mandatory');

v_header_titles = new Array("<center><b><nobr>Levels<br>(1=highest)</br></nobr></b></center>", "<center><b><nobr>Activated</nobr></b></center>", "<center><b><nobr>Description</nobr></b></center>");

g_people_list_dirty = false;
g_product_list_dirty = false;

g_sorted_people_array = null;
g_sorted_products_array = null;
g_source_list_dirty = false;
g_sorted_sources_array = null;

g_source2_list_dirty = false;
g_sorted_sources2_array = null;
var previousProdIndex = -1;
var edit_reason_index = -1;

g_assignee = null;
g_assign_data = null;
g_assign_data_col_count = null;

g_continueSaving = true;
g_saveQueueIndex = 0;
g_saveQueue = new Array();

var ignore_next = false;

function setSubAnswer(DealID, SubID, Answer, CompletedLocation)
{
	//	No longer adequate!
	//	g_editedSubAnswers[g_editedSubAnswers.length] = new Array(DealID, SubID, Answer);
	var bFound = 0;
	for(var i = 0; i< g_editedSubAnswers.length; ++i)
	{
		if (g_editedSubAnswers[i][0] == DealID && g_editedSubAnswers[i][1] == SubID)
		{
			bFound = 1;
			g_editedSubAnswers[i][2] = Answer;
			g_editedSubAnswers[i][3] = CompletedLocation;
		}
	}
	if(!bFound);
	g_editedSubAnswers[g_editedSubAnswers.length] = new Array(DealID, SubID, Answer, CompletedLocation);
}

function onSortOpps()
{
	document.getElementById('tdOpTableStatus').innerHTML = "<nobr>Sorting, please wait...</nobr>";
}

function find_salescycle(personID, productID)
{
	for (var i = 0; i < g_salescycles.length; ++i)
	{
		if (g_salescycles[i][g_salescycles.PersonID] == personID && g_salescycles[i][g_salescycles.ProductID] == productID)
		return g_salescycles[i];
	}
	return null;
}

function calc_close_date(first_meeting, personID, productID)
{
	if (productID < 0 || first_meeting == '')
	return '';
	var ss = find_salescycle(personID, productID);
	if (!ss)
	return '';

	var fm = new Date(Dates.mssql2us(first_meeting));
	return Dates.formatDate(new Date(fm.getTime() + (ss[g_salescycles.SalesCycleLength] * msPerDay)));
}

function getTempID()
{
	return g_temp_id_counter--;
}

function updateData()
{

}

// this should be moved to util.js
function getXMLHttpRequest()
{
	if(typeof ActiveXObject!="undefined")
	return new ActiveXObject("Microsoft.XMLHTTP");
	else if(typeof XMLHttpRequest!="undefined")
	return new XMLHttpRequest();
	else
	return null;
};

function onTabChanging(oldindex, newindex)
{
	if (g_doing_save)
	return false;
	if (checkUserFlds())
	return false;
	if (oldindex == g_people_tab_index && g_selectedSp && !verifyColors(g_selectedSp))
	{
		alert("The color of the currently selected person is already taken in the group. Please resolve this problem before editing other data.");
		show_color_picker();
		return false;
	}
	if(oldindex == g_products_tab_index)
	if(!onChangeProductSelect()) return false;

	Windowing.closeAllWindows();
	set_cookie('current_tab', newindex, false);
	if (Header.isBlinking('Save'))
	{
		if (confirm('Changes have been made. Would you like to save now?'))
		do_save();

	}
	// if going to the opp tab, need to read in opps
	if (newindex == g_opps_tab_index && typeof opTable=="undefined")
	{
		//showPopup();
		//setTimeout("getOpps();", 1);	// this timeout causes the page to be drawn, including our popup, while processing continues

	}

	return true;
}

function getOpps()
{
	var startdate = 'fromDate=' + document.getElementById('oppsStartDate').value;
	var enddate = 'toDate=' + document.getElementById('oppsEndDate').value;
	
	var qstr = '';
	if(startdate != '' && enddate != '') qstr = '?' +  startdate + '&' + enddate;

	var req = getXMLHttpRequest();
	req.open('POST', "../data/get_admin_opportunities.php" + qstr, false);
	//		req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	req.setRequestHeader('Content-Type', 'text/javascript');
	var str="";
	req.send(str);
	//		alert(req.responseText);
	eval(req.responseText);
	
	processOpps();
	createOpTable();
	document.body.removeChild(document.getElementById('popupWait'));
}

function showPopup(vis)
{
	var d=document.createElement("DIV");
	document.body.appendChild(d);
	d.id="popupWait";
	d.style.position="absolute";
	d.style.width="450px";
	d.style.height="100px";
	d.style.left=(getWindowWidth()-450)/2;
	d.style.top=(getWindowHeight()-100)/2;
	d.style.visibility="visible";
	d.style.zOrder=1;
	d.style.border="thin solid black";
	d.style.backgroundColor="white";
	d.style.textAlign="center";
	d.innerHTML="<br><br>Please wait while data is retrieved.";
}

function update_from_mass_op()
{
	Header.blinkOn('Save');
	opTable.redraw();
}

function update_from_edit_window(opp, oldCategory)
{
	Header.blinkOn('Save');
	if (opp.i_am_new)
	{
		opp.added = true;
		opp.i_am_new = false;
		g_opportunities[g_opportunities.length] = opp;
		++g_opp_count;
		++g_search_count;
		opTable.showPage(opTable.getPageCount() - 1);
	}
	else
	{
		opp.edited = true;
		opTable.redraw();
	}
}

function editOppCallback(edited_opp)
{
	Header.blinkOn('Save');
	opTable.redraw();
}

function onEditOpp()
{
	this.myData.myDataIndex = this.myDataIndex;
	var hash = '#' + this.myData[g_opps.DealID] + '_' + this.myData[g_opps.PersonID];
	if (this.myData[g_opps.VLevel])
	hash+= '_' + this.myData[g_opps.VLevel];
	var search = '?reqOpId='+this.myData[g_opps.DealID]+'&reqPersonId='+ this.myData[g_opps.PersonID];
	Windowing.dropBox.revive_opp = null;
	Windowing.openSizedPrompt("../shared/edit_opp2.php" + search + hash, 630, 820);
}

function onDeleteOpp()
{
	var the_opp = this.myData;
	if (confirm('Are you sure you want to delete this opportunity?'))
	{
		--g_opp_count;
		--g_search_count;
		the_opp.removed = true;
		Header.blinkOn('Save');
		opTable.deleteRow(this.myDataIndex);
	}
}

function onRemoveOppReasonCallback(str)
{
	Windowing.dropBox.the_opp[g_opportunities.Category] = '9';
	Windowing.dropBox.the_opp[g_opportunities.RemoveReason] = str;
	Windowing.dropBox.the_opp.edited = true;
	Header.blinkOn('Save');
	opTable.redraw();
}

function onRemoveOpp()
{
	Windowing.dropBox.the_opp = this.myData;
	Windowing.dropBox.myDataIndex = this.myDataIndex;
	Windowing.dropBox.msg = 'Please provide a removal reason:';
	Windowing.dropBox.options = new Array();
	for (var k = 0; k < g_removereasons.length; ++k)
	{
		var tempobj = new Object();
		tempobj.value = g_removereasons[k][g_removereasons.ReasonID];
		tempobj.text = g_removereasons[k][g_removereasons.Text];
		Windowing.dropBox.options[Windowing.dropBox.options.length] = tempobj;
	}
	Windowing.dropBox.promptCallback = onRemoveOppReasonCallback;
	Windowing.openSizedPrompt("../shared/dropdown_prompt.html", 130, 520);
}

function onReviveOppReasonCallback(str)
{
	Windowing.dropBox.the_opp[g_opportunities.Category] = str;
	Windowing.dropBox.the_opp.edited = true;
	Header.blinkOn('Save');
	opTable.redraw();
}

function CCategory(value, text)
{
	this.value = value;
	this.text = text;
}

function onReviveOpp()
{
	this.myData.myDataIndex = this.myDataIndex;
	var hash = '#' + this.myData[g_opps.DealID] + '_' + this.myData[g_opps.PersonID] + '_' + this.myData[g_opps.VLevel];
	var search = '?reqOpId='+this.myData[g_opps.DealID]+'&reqPersonId='+ this.myData[g_opps.PersonID];

	Windowing.dropBox.revive_opp = this.myData;
	Windowing.openSizedPrompt("../shared/edit_opp2.php" + search + hash, 630, 820);
}

function onPersonTreeChange(theitem)
{
	theitem.value.mynode = theitem;
	selectSalesPerson(theitem.value);
}

function showBoard()
{
	window.open('../board/main.html');
}

function do_Import()
{
	//	Windowing.openSizedPrompt("../shared/import_targets.php", 575, 775);
	Windowing.openSizedWindow("../shared/import_targets.php", 800, 900);

}

function addOppCallback(newopp)
{
	var thecopy = new Array();
	for (var k = 0; k < newopp.length; ++k)
	thecopy[k] = newopp[k];
	thecopy.added = true;
	Header.blinkOn('Save');
	g_opportunities[g_opportunities.length] = thecopy;
	++g_opp_count;
	opTable.showPage(opTable.getPageCount() - 1);
}

function onClickAddTarg()
{
	//	var hash = '#-1_-1';
	var hash = '#-1_-1_-1';
	Windowing.dropBox.revive_opp = null;
	Windowing.openSizedPrompt("../shared/edit_targ.html" + hash, 420, 625);
}


function onClickAddOpp()
{
	//	var hash = '#-1_-1';
	var hash = '#-1_-1_-1';
	var search = '?reqOpId='+ -1 +'&reqPersonId='+ -1;

	Windowing.dropBox.revive_opp = null;
	Windowing.openSizedPrompt("../shared/edit_opp2.php" + search + hash, 630, 820);
}

function onClickReassign()
{
	if(g_old_search == '')
	{
		alert('The current opportunity set is NOT filtered! Please filter the opportunities and try again.');
		return;
	}
	Windowing.openSizedPrompt("opp_reassign.html", 200, 400);

}

// gets a list of opps that are selected, returns them in an array, and unselects them
function getSelectedList()
{
	var massOpps=new Array();

	for (i=0;i<g_opportunities.length;i++)
	{
		if (g_opportunities[i].checked && g_opportunities[i].checked==true)
		{
			var DealID=g_opportunities[i][g_opportunities.DealID];
			massOpps[DealID]=DealID;
			g_opportunities[i].checked=false;
		}
	}
	return massOpps;
}

function callbackMassMove()
{
	for (i=0;i<g_opportunities.length;i++)
	{
		if (g_opportunities[i].checked && g_opportunities[i].checked==true)
		{
			g_opportunities[i].checked=false;
		}
	}

	opTable.redraw();
}

function onClickMassOpp()
{
	var i;
	var massOpps=new Array();

	for (i=0;i<g_opportunities.length;i++)
	{
		if (g_opportunities[i].checked && g_opportunities[i].checked==true)
		{
			var DealID=g_opportunities[i][g_opportunities.DealID];
			massOpps[massOpps.length]=DealID;

		}
	}
	if (massOpps.length)
	{
		Windowing.dropBox.massOpps=massOpps;
		document.body.style.cursor = 'wait';
		Windowing.openSizedPrompt('../shared/mass_op.html',300,400);
		document.body.style.cursor = 'auto';
	}
	else alert("No opportunities selected.");
}

function funcSelectAll(opp)
{
	opp.checked=true;
}

function funcSelectNone(opp)
{
	opp.checked=false;
}

function onClickSelectAll()
{
	opTable.processDisplay(funcSelectAll);
	opTable.redraw();
}

function onClickSelectNone()
{
	opTable.processDisplay(funcSelectNone);
	opTable.redraw();
}

function getWindowHeight()
{
	var hgt = 150;
	if(navigator.appName == "Netscape")
	{
		hgt = window.innerHeight;

	}
	else if(navigator.appName == "Microsoft Internet Explorer")
	{

		hgt = document.body.clientHeight;
	}
	return hgt;
}

function getWindowWidth()
{
	var hgt = 150;
	if(navigator.appName == "Netscape")
	{
		hgt = window.innerWidth;

	}
	else if(navigator.appName == "Microsoft Internet Explorer")
	{

		hgt = document.body.clientWidth;
	}
	return hgt;
}

function getProductID(pName)
{

	if(pName.length == 0) return 0;
	for (var i=0; i < g_products.length; i++)
	{
		if (pName != g_products[i][g_products.Name]) continue;
		return g_products[i][g_products.ProductID];
	}

	return -1;
}

function onClickPrint()
{
	if (!confirm('This command will print the current result set.  Do you want to continue?'))
	return;
	//	alert('Filter count: ' + g_filter_list.length);

	var strParams = '';
	for (var i=0; i<g_filter_list.length; i++)
	{
		if(i > 0) strParams += '&';
		else strParams += '?';
		var ind = (i + 1) * 1;
		strParams += 'filter' + ind + '=';
		strParams += g_filter_list[i];
	}
	//		var strParams='?type=' + g_search_type + '&value=' + g_search_value + '&valuetext=' + g_search_valuetext;
	//		strParams += '&date_to=' + g_search_date_to + '&date_from=' + g_search_date_from;



	Windowing.openSizedWindow('opp_print_pdf.php' + strParams, getWindowHeight(), 790, 'print_opps');
	document.body.style.cursor = 'auto';

}

function searchTimeout()
{
	var massOpps=getSelectedList();
	var temp_table = null;
	var results = new Array();
	if (g_search_type == 'FM')
	results = opTable.searchSpan(g_search_date_from, g_search_date_to, FM_COL_INDEX);
	else if (g_search_type == 'FC')
	results = opTable.searchSpan(g_search_date_from, g_search_date_to, FORCASTCLOSE_COL_INDEX);
	else if (g_search_type == 'AC')
	results = opTable.searchSpan(g_search_date_from, g_search_date_to, ADJUSTEDCLOSE_COL_INDEX);
	else if (g_search_type == 'SP')
	{
		var temparray = new Array();
		temparray[temparray.length] = SALESPERSON_COL_INDEX;
		results = opTable.search(g_search_value, temparray);
	}
	else if (g_search_type == 'COMP')
	{
		var temparray = new Array();
		temparray[temparray.length] = COMPANY_COL_INDEX;
		results = opTable.search(g_search_value, temparray);
	}
	else if (g_search_type == 'PRODUCT')
	{
		var temparray = new Array();
		temparray[temparray.length] = OPP_COL_INDEX;
		results = opTable.search(g_search_value, temparray);
	}
	else if (g_search_type == 'CATEGORY')
	{
		var temparray = new Array();
		temparray[temparray.length] = CATEGORY_COL_INDEX;
		results = opTable.search(g_search_value, temparray);
		g_search_value = g_search_valuetext;
	}
	else if (g_search_type == 'ACTIVE')
	{
		results = opTable.searchSpan(0, 8, CATEGORY_COL_INDEX);
		g_search_value = 'ACTIVE';
	}
	else if (g_search_type == 'REMOVED')
	{
		var temparray = new Array();
		temparray[temparray.length] = CATEGORY_COL_INDEX;
		results = opTable.search(9, temparray);
		g_search_value = 'REMOVED';
	}

	if (results.length == 0)
	{
		alert('That filter returned no results');

		opTable.redraw();
		return;
	}
	var new_search_str = null;
	if (g_search_type == 'FC' || g_search_type == 'AC' || g_search_type == 'FM')
	new_search_str = g_search_date_from + ' to ' + g_search_date_to;
	else
	new_search_str = g_search_value;
	if (g_old_search != '')
	g_old_search += ', ' + new_search_str;
	else
	g_old_search = new_search_str;
	g_search_count = results.length;

	if(g_search_type == 'PRODUCT')
	{
		g_search_value = getProductID(g_search_valuetext);
	}

	var strFilterParams = g_search_type + "|" + g_search_value + "|" + g_search_valuetext + "|" + g_search_date_from + "|" + g_search_date_to;
	g_filter_list[g_filter_list.length] = strFilterParams;

	g_opp_mode = OPP_MODE_SEARCH;

	for(var i=0;i<results.length;i++)
	{
		if (massOpps[results[i][g_opps.DealID]]) results[i].checked=true;
	}

	opTable.setData(results);
	opTable.setData(g_products, "PROD", "ProductID", "ProductID");
	opTable.setData(g_categories, "CAT");
	opTable.showPage(0);
}

function filterPromptCallback(type, value, valuetext, date_from, date_to)
{
	document.getElementById('tdOpTableStatus').innerHTML = "<nobr>Searching, please wait...</nobr>";

	g_search_type = null;
	g_search_value = null;
	g_search_valuetext = null;
	g_search_date_from = null;
	g_search_date_to = null;

	g_search_type = type;
	g_search_value = value;
	g_search_valuetext = valuetext;
	if (date_from)
	g_search_date_from = date_from;
	if (date_to)
	g_search_date_to = date_to;

	setTimeout("searchTimeout();", 1);
}

function onClickFilterOpps()
{
	if (g_opp_count < 1)
	return;
	Windowing.dropBox.people = g_people;
	Windowing.dropBox.opps = opTable.m_data;
	Windowing.dropBox.opp_key = g_opportunities;
	Windowing.dropBox.products = g_products;
	Windowing.dropBox.filterPromptCallback = filterPromptCallback;
	Windowing.dropBox.initialValue = "";
	Windowing.openSizedPrompt("filter_prompt.html", 160, 450);
}

function reassignFilteredOps(PersonID)
{
	//First, get the person rec. then, call optable.setdataval as in the following:
	/*
	if (window.opener.g_people[k][window.opener.g_people.PersonID] == g_opportunity[g_oppCols.PersonID])
	{
	g_opportunity[g_oppCols.FirstName] = window.opener.g_people[k][window.opener.g_people.FirstName];
	g_opportunity[g_oppCols.LastName] = window.opener.g_people[k][window.opener.g_people.LastName];
	g_opportunity[g_oppCols.Color] = window.opener.g_people[k][window.opener.g_people.Color];
	break;
	}
	*/
	var i;
	for(i=0; i<g_people.length; ++i)
	{
		if (g_people[i][g_people.PersonID] == PersonID)
		break;
	}

	if(!confirm('All displayed opportunities will be reassigned!  Continue?'))
	return;
	opTable.setDataVal('PersonID', PersonID);
	opTable.setDataVal('FirstName', g_people[i][g_people.FirstName]);
	opTable.setDataVal('LastName', g_people[i][g_people.LastName]);
	opTable.setDataVal('PrivateEmail', g_people[i][g_people.PrivateEmail]);
	opTable.setDataVal('Color', g_people[i][g_people.Color]);

	opTable.redraw();
	Header.blinkOn('Save');

}

function onClickSearchOpps()
{
	Windowing.dropBox.msg = 'Please enter your search criteria:';
	Windowing.dropBox.promptCallback = onSearchPromptReturn;
	Windowing.dropBox.initialValue = "";
	Windowing.openSizedPrompt("../shared/prompt.html", 150, 550);
}

function onSearchPromptReturn(str)
{
	var results = opTable.search(str, new Array(SALESPERSON_COL_INDEX, COMPANY_COL_INDEX, OPP_COL_INDEX));
	if (results.length == 0)
	{
		alert('That search returned no results');
		return;
	}
	if (g_old_search != '')
	g_old_search += ', ' + str;
	else
	g_old_search = str;
	g_search_count = results.length;
	g_opp_mode = OPP_MODE_SEARCH;
	opTable.setData(results);
	opTable.setData(g_products, "PROD", "ProductID", "ProductID");
	opTable.setData(g_categories, "CAT");
	opTable.showPage(0);
}

function onClickCloseSearch()
{
	g_old_search = '';
	// -- clear terms!
	g_search_type = null;
	g_search_value = null;
	g_search_valuetext = null;
	g_search_date_from = null;
	g_search_date_to = null;
	// --
	g_filter_list = Array();

	g_opp_mode = OPP_MODE_ALL;
	opTable.setData(g_opportunities);
	opTable.setData(g_products, "PROD", "ProductID", "ProductID");
	opTable.setData(g_categories, "CAT");
	opTable.showPage(0);
}

function myOnUnload()
{
	Windowing.closeAllWindows();
}

function allowPersonMove(the_moved, the_dest)
{
	if (the_moved.m_parent && the_moved.m_parent.value == -5)
	return false;
	if (g_selectedSp && g_selectedSp.mynode.getDistanceFrom(g_assignedtree) != -1 && !verifyColors(g_selectedSp))
	{
		alert("The color of the currently selected person is already taken in the group. Please choose a new color before assigning.");
		show_color_picker();
		return false;
	}
	if (the_dest.getDistanceFrom(g_unassignedtree) != -1)
	{
		alert("You cannot assign to a person who is unassigned");
		return;
	}
	if (the_dest.value == -5 || (the_dest.m_parent && the_dest.m_parent.value == -5))
	{
		alert('You cannot make a salesperson or manager an administrator. Please use the "Add Admin" button to create a new administrator.');
		return false;
	}
	if (the_dest.value == -3 || the_dest.value == -4)
	return true;
	if (parseInt(the_dest.value[g_people.Level]) <= the_moved.getDepth())
	{
		alert("Cannot carry out the move because there are not enough levels under the target person to account for the person(s) being moved.\nPlease change the target person's level and, if needed, define more levels for your company.");
		return false;
	}
	return true;
}

function verifyColors(the_person)
{
	if (the_person[g_people.IsSalesperson] != '1')
	return true;
	group_list = create_group_list(the_person, the_person);
	for (var k = 0; k < group_list.length; ++k)
	{
		if (group_list[k][g_people.Color] == the_person[g_people.Color])
		return false;
	}
	return true;
}

function show_color_picker(sp)
{
	if (!sp)
	sp = g_selectedSp;
	Windowing.dropBox.grouplist = create_group_list(sp, sp);;
	Windowing.dropBox.g_people = g_people;
	Windowing.dropBox.person = g_selectedSp;
	colorPicker = Windowing.openSizedPrompt("admin_chooseColor.html", 300, 300);
}

function allowPersonSelect(the_item)
{
	if (!g_selectedSp || the_item.value == g_selectedSp)
	return true;
	if (g_selectedSp && checkUserFlds())
	return false;

	if (g_selectedSp.mynode.getDistanceFrom(g_assignedtree) != -1 && !verifyColors(g_selectedSp))
	{
		alert("The color of the currently selected person is already taken in the group. Please choose a new color.");
		show_color_picker();
		return (the_item.value == g_selectedSp);
	}
	return true;
}

function onOpTableRedraw()
{
	if (g_opp_mode == OPP_MODE_SEARCH)
	{
		var start_index = opTable.getCurPage() * opTable.rowsAtOnce;
		var end_index = start_index + opTable.rowsAtOnce;
		if (end_index > g_search_count)
		end_index = g_search_count;
		var tdOpTableStatus = document.getElementById('tdOpTableStatus');
		if (tdOpTableStatus)
		tdOpTableStatus.innerHTML = "<nobr>" + ((start_index == end_index) ? 0 : (start_index + 1)) + " - " + end_index + " of "+ g_search_count + " from search results for \"" + g_old_search + "\"&nbsp;&nbsp;&nbsp;&nbsp;<button type=\"button\" class=\"command\"onclick=\"onClickCloseSearch();\">Clear Search</button></nobr>";
	}
	else
	{
		var start_index = opTable.getCurPage() * opTable.rowsAtOnce;
		var end_index = start_index + opTable.rowsAtOnce;
		if (end_index > g_opp_count)
		end_index = g_opp_count;
		var tdOpTableStatus = document.getElementById('tdOpTableStatus');
		if (tdOpTableStatus)
		tdOpTableStatus.innerHTML = "<nobr>" + ((start_index == end_index) ? 0 : (start_index + 1)) + " - " + end_index + " of "+ g_opp_count + "</nobr>";
	}
}

function cat_abbr(cat)
{
	switch (cat)
	{
		case '1':
		return getCatLabel('{{FM}}', '{{FM}}', 'FMAbbr', 'FM');
		case '2':
		return getCatLabel('{{IP}}', '{{IP}}', 'IPAbbr', 'IP');
		case '3':
		{
			var lblIP = getCatLabel('{{IP}}', '{{IP}}', 'IPAbbr', 'IP');
			var lblS = getCatLabel('{{S}}', '{{S}}', 'SAbbr', 'S');
			return lblIP + lblS;
		}
		case '4':
		return getCatLabel('{{DP}}', '{{DP}}', 'DPAbbr', 'DP');
		case '5':
		{
			var lblDP = getCatLabel('{{DP}}', '{{DP}}', 'DPAbbr', 'DP');
			var lblS = getCatLabel('{{S}}', '{{S}}', 'SAbbr', 'S');
			return lblDP + lblS;
		}
		case '6':
		return getCatLabel('{{C}}', '{{C}}', 'CAbbr', 'C');
		case '9':
		return getCatLabel('{{R}}', '{{R}}', 'RAbbr', 'R');
		case '10':
		return getCatLabel('{{T}}', '{{T}}', 'TAbbr', 'T');
	}
	return 'Err';
}

function get_rid(str)
{
	return str.unescape_html();
}

/*
function getVLevel(valID)
{

for(i=0;i<g_valuations.length;i++)
{
if (g_valuations[i][g_valuations.ValID]==valID)
return g_valuations[i][g_valuations.VLevel];
}
return "--";
}
*/

function onEditVal()
{
	alert("Edit val");
}

function onDeleteVal()
{
	alert("Delete val");
}

function onRemoveVal()
{
	alert("Remove val");
}

function dovlevel(val)
{
	if(val>-1) return val;
	return '';
}

function winnow_data(arrData, fieldname, val)
{

	var arrRet = arrData;
	for (var i=arrRet.length-1; i>-1; i--)
	{
		if(arrRet[i][arrRet[fieldname]] == val)
		arrRet.splice(i, 1);
	}
	return arrRet;

}

function onClickCheck()
{
	this.myData.checked=this.checked;
}

function fixSource2Labels()
{
	document.getElementById('tdSource2Name').innerHTML = '<nobr>' + g_the_only_company[g_company.Source2Label]+' Name</nobr>';
	document.getElementById('tdSource2Abbr').innerHTML = '<nobr>' + g_the_only_company[g_company.Source2Label]+' Abbrev</nobr>';
	document.getElementById('spanSource2msg').innerHTML = g_the_only_company[g_company.Source2Label];
}

function printOrgChart()
{
	if (chartZoomElem)
	{
		var iframeOrgChart = document.getElementById('iframeOrgChart');
		iframeOrgChart.contentWindow.printChart();
	}
}

function viewChartFulscreen()
{
	var oldattribs = Windowing.m_attribs;
	Windowing.m_attribs = 'resizable=yes,toolbar=yes,menubar=yes';
	var orgwin = Windowing.openSizedWindow("orgchart.html", 600, 800, "chartfullscreen");
	Windowing.m_attribs = oldattribs;
	orgwin.moveTo(0, 0);
	orgwin.resizeTo(screen.width, screen.height);
}

function onFinishOrgChart(zoomelem)
{
	spanPleaseWaitOrg.style.display = 'none';
	chartZoomElem = zoomelem;
	chartZoomElem.style.zoom = chartZoom+'%';
}

function onZoomChange(elem, val)
{
	chartZoom = val;
	document.getElementById('spanZoomPercentage').innerHTML = val+'%';
	var iframeOrgChart = document.getElementById('iframeOrgChart');
	if (chartZoomElem)
	{
		chartZoomElem.style.zoom = val+'%';
		iframeOrgChart.contentWindow.updateLevels();
	}
	set_cookie('orgZoom', val);
}

function processOpps()
{
	g_opps = g_opportunities;

	if(!g_target_used)
	{
		g_opportunities = winnow_data(g_opportunities, 'Category', '10');
		g_unassigned_opportunities = winnow_data(g_unassigned_opportunities, 'Category', '10');
	}


	for (var k = 0; k < g_unassigned_opportunities.length; ++k)
	g_opportunities[g_opportunities.length] = g_unassigned_opportunities[k];


	g_opp_count = g_opportunities.length;
}

function myOnLoad()
{
	if (window.g_overflow_alert)
	{
		alert("Buffer overflow error!");
		return;
	}

	document.body.removeChild(document.getElementById('tablePleaseWait'));
	if (!admin_check_login())
	return;
	document.getElementById('buttonDone').style.visibility='visible';
	document.getElementById('buttonLogout').style.visibility='visible';

	g_the_only_company = g_company[0];
	fixSource2Labels();

	var k = 0;
	var elem = null;
	while (elem = document.getElementById('closedLabel'+(k++)))
	elem.innerHTML = g_the_only_company[g_company.CLabel];

	document.getElementById('lblIPCloseRatio').innerHTML = g_the_only_company[g_company.IPLabel]+':';
	document.getElementById('lblDPCloseRatio').innerHTML = g_the_only_company[g_company.DPLabel]+':';

	g_target_used = g_the_only_company[g_company.TargetUsed] == '1';

	if(!g_target_used)
	{
		document.getElementById("btnTarg").style.visibility = 'hidden';
		document.getElementById("btnTarg").style.width = '0px';

	}

	g_valuation_used = g_the_only_company[g_company.ValuationUsed] == '1';
	g_source_used = g_the_only_company[g_company.SourceUsed] == '1';
	g_source2_used = g_the_only_company[g_company.Source2Used] == '1';

	if (g_the_only_company[g_company.SubMSEnabled] == '1')
	g_subMSEnabled = true;

	hasPersonQuestions = g_the_only_company[g_company.AskPersonQuestions] == '1';
	hasNeedQuestions = g_the_only_company[g_company.AskNeedQuestions] == '1';
	hasMoneyQuestions = g_the_only_company[g_company.AskMoneyQuestions] == '1';
	hasTimeQuestions = g_the_only_company[g_company.AskTimeQuestions] == '1';
	hasReq1Questions = g_the_only_company[g_company.AskReq1Questions] == '1';
	hasReq2Questions = g_the_only_company[g_company.AskReq2Questions] == '1';
	hasMilestoneQuestions = (hasPersonQuestions || hasNeedQuestions || hasMoneyQuestions || hasTimeQuestions || hasReq1Questions || hasReq2Questions);
	hasDemoData = g_the_only_company[g_company.demodata] == '1';

	processOpps();

	if (g_the_only_company[g_company.CustomSPGroupName] && g_the_only_company[g_company.CustomSPGroupName] != '')
	document.getElementById('trGroupName').style.visibility = 'visible';


	Header.setText(g_the_only_company[g_company.Name] + " - Administration");
	//Header.addButton(new HeaderButton('Board', null, 'showBoard();'));

	Header.addButton(new HeaderButton('Imp ' + catLabel('Tgt'), null, 'do_Import();'));

	Header.addButton(ButtonStore.getButton("Save"));
	//Header.addButton(new HeaderButton('Update', null, 'updateData();'));
	var tdMPowerHeader = document.getElementById("tdMPowerHeader");
	tdMPowerHeader.innerHTML = Header.makeHTML();
	Header.disableButton('Board');

	var div_company = document.getElementById("div_company");
	var div_people = document.getElementById("div_people");
	var div_people_edit = document.getElementById("div_people_edit");
	var div_people_orgchart = document.getElementById("div_people_orgchart");
	var div_organization = document.getElementById("div_organization");
	var div_opportunities = document.getElementById("div_opportunities");
	var div_products = document.getElementById("div_products");
	var div_close_questions = document.getElementById("div_close_questions");
	var div_milestone_questions = document.getElementById("div_milestone_questions");
	var divSubMilestones = document.getElementById("divSubMilestones");

	var checkMilestone1 = document.getElementById("checkMilestone1");
	var checkMilestone2 = document.getElementById("checkMilestone2");

	var checkTargetUsed = document.getElementById("checkTargetUsed");
	var checkValUsed = document.getElementById("checkValUsed");
	var checkSourceUsed = document.getElementById("checkSourceUsed");
	var checkSource2Used = document.getElementById("checkSource2Used");
	var checkBillingUsed = document.getElementById("checkBillingUsed");
	var textBillingPrompt = document.getElementById("textBillingPrompt");
	var textSource2Label = document.getElementById("textSource2Label");
	var textSource2Abbr = document.getElementById("textSource2Abbr");
	var textDeptLabel = document.getElementById("textDeptLabel");
	var textValLabel = document.getElementById("textValLabel");
	var textValAbbr = document.getElementById("textValAbbr");
	var textContactLabel = document.getElementById("textContactLabel");
	var checkViewSiblingsCalendar = document.getElementById("checkViewSiblingsCalendar");	
	var checkRestrictUnassignedUsers = document.getElementById("checkRestrictUnassignedUsers");
	var checkUseGoogleSync = document.getElementById("checkUseGoogleSync");
	/*Start Jet-27 */
	var checkEventAlert = document.getElementById("checkEventAlert");
	/*End Jet-27*/
	var checkMyGoalMine = document.getElementById("checkMyGoalMine");
	var checkEmailPrivacyEnabled = document.getElementById("checkEmailPrivacyEnabled");
	var checkAutoMarkEmailsPrivate = document.getElementById("checkAutoMarkEmailsPrivate");
	var checkNoteAlert = document.getElementById("checkNoteAlert");
	var checkShowAllNotesOfContact = document.getElementById("checkShowAllNotesOfContact");
	var textAutoMarkEmailsGracePeriod = document.getElementById("textAutoMarkEmailsGracePeriod");
	
	var	div_demodata = document.getElementById("div_demodata");

	if (g_the_only_company[g_company.DeptLabel] == '')
	textDeptLabel.value = 'Div/Dept/Loc';
	else
	textDeptLabel.value = g_the_only_company[g_company.DeptLabel];

	if (g_the_only_company[g_company.ValLabel] == '')
	textValLabel.value = 'Valuation';
	else
	textValLabel.value = g_the_only_company[g_company.ValLabel];

	if (g_the_only_company[g_company.ValAbbr] == '')
	textValAbbr.value = 'Val';
	else
	textValAbbr.value = g_the_only_company[g_company.ValAbbr];

	if (g_the_only_company[g_company.ContactLabel] == '')
	textContactLabel.value = 'Contact';
	else
	textContactLabel.value = g_the_only_company[g_company.ContactLabel];

	var tdTabbedPane = document.getElementById("tdTabbedPane");

	//	var div_valuations = document.getElementById("div_valuations");

	//	var div_sources= document.getElementById("div_sources");

	var div_lists = document.getElementById("div_lists");
	
	var div_slipStream = document.getElementById("div_slipStream");
	
	people_tabbedpane = new JSTabbedPane(g_imgpath);
	people_tabbedpane.addTabFromElem('<b id="manageCompanyTab"><a href="javascript:manageCompany();" class="PointerElem">Manage Company</a></b>', div_manage_company);
	people_tabbedpane.addTabFromElem('<b id="manageContactTab"><a href="javascript:manageContact();" class="PointerElem">Manage Contact</a></b>', div_manage_contact);
	people_tabbedpane.addTabFromElem('<b id="manageEventsTab"><a href="javascript:manageEvents();" class="PointerElem">Manage Events</a></b>', div_manage_events);
	people_tabbedpane.addTabFromElem('<b id="manageNoteFieldsTab"><a href="javascript:manageNoteFields();" class="PointerElem">Manage Note</a></b>', div_manage_note_fields);
	people_tabbedpane.addTabFromElem('<b id="assignUserTemplateTab"><a href="javascript:assignUserTemplate();" class="PointerElem">Assign User Templates</a></b>', div_assign_note_templates);
	people_tabbedpane.addTabFromElem('<b id="restoreSearchDataTemplateTab"><a href="javascript:void(0);" class="PointerElem">Restore Search Data</a></b>', div_restore_search_data);
	people_tabbedpane.addTabFromElem('<b id="meetingNoticeTemplateTab"><a href="javascript:void(0);" class="PointerElem">Meeting Notice Template</a></b>', div_meeting_notice_template);
	people_tabbedpane.addTabFromElem('<b id="calendarPermissionTab"><a href="javascript:userCalendarPermission();" class="PointerElem">Cal Permission</a></b>', div_calendar_permission);
	people_tabbedpane.addTabFromElem('<b id="jetFileTab"><a href="javascript:userJetFile(\'AdminTags\' );" class="PointerElem">JetFile</a></b>', div_JetFile);
	people_tabbedpane.addTabFromElem('<b id="manageOppTrackerTab"><a href="javascript:manageOppTracker();" class="PointerElem">Opp tracker</a></b>', div_manage_opp_tracker);
	people_tabbedpane.addTabFromElem('<b id="manageCacheTab"><a href="javascript:manageCache();" class="PointerElem">Cache</a></b>', div_manage_cache);
	people_tabbedpane.addToElement(div_slipStream);
	
	
	people_tabbedpane = new JSTabbedPane(g_imgpath);
	people_tabbedpane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">Edit</a></b>', div_people_edit);
	people_tabbedpane.addTabFromElem('<b id="orgChartTab"><a href="javascript:void(0);" class="PointerElem">Org Chart</a></b>', div_people_orgchart);
	people_tabbedpane.addToElement(div_people);
	people_tabbedpane.onTabChanging = function(iold, inew)
	{
		if (inew == 1 && chartDirty)
		{
			chartZoomElem = null;
			updateOrgChart();
			chartDirty = false;
		}
		return true;
	};

	tabbedpane = new JSTabbedPane(g_imgpath);
	g_company_tab_index = tabbedpane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">Company</a></b>', div_company);
	g_people_tab_index = tabbedpane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">People</a></b>', div_people);
	g_opps_tab_index = tabbedpane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">Opportunities</a></b>', div_opportunities);
	g_products_tab_index = tabbedpane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">Offerings</a></b>', div_products);
	if (g_the_only_company[g_company.AskCloseQuestions] == '1')
	g_close_questions_tab_index = tabbedpane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">Close Questions</a></b>', div_close_questions);
	if (hasMilestoneQuestions)
	g_milestone_questions_tab_index = tabbedpane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">Milestone Questions</a></b>', div_milestone_questions);
	if (g_subMSEnabled)
	g_sub_questions_tab_index = tabbedpane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">Milestones</a></b>', divSubMilestones);


	//	g_valuations_tab_index = tabbedpane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">Valuations</a></b>', div_valuations);

	g_lists_tab_index = tabbedpane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">Lists</a></b>', div_lists);
	
	g_slip_stream_tab_index = tabbedpane.addTabFromElem('<b><a href="javascript:manageCompany();" class="PointerElem">Jetstream</a></b>', div_slipStream);

	if(hasDemoData)
	{
		g_demodata_tab_index = tabbedpane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">Manage Demo</a></b>', div_demodata);
	}

	tabbedpane.onTabChanging = onTabChanging;
	tabbedpane.addToElement(tdTabbedPane);

	fillRemovalReasonsSelect();

	XPloreTree.setImagePath(g_imgpath);
	persontree = new XPloreTree();
	persontree.onMoveItem = onMovePerson;
	persontree.allowMoveCallback = allowPersonMove;
	persontree.allowSelectCallback = allowPersonSelect;
	persontree.addSelectionListener(onPersonTreeChange);
	persontree.appendToElement(document.getElementById("divPeopleTree"));

	fillPersonTree();


	setCategoryLabels();

	//Now for that pesky Stalled Limits caption.  Needs to be done BEFORE data
	var sg = document.getElementById('lblStalledLimits');
	//	var sg.innerHTML = getCatLabel('{{Stalled}}', sg.innerHTML, 'SLabel', 'Stalled');
	var html = sg.innerHTML;
	//alert(html);
	var newHTML = getCatLabel('{{Stalled}}', html, 'SLabel', 'Stalled');
	sg.innerHTML = newHTML;

	var JSHeaderText = document.getElementById("JSHeaderText");
	var editNumMonthsNew = document.getElementById("editNumMonthsNew");
	var editCompanyFM = document.getElementById("editCompanyFM");
	var editCompanyIP = document.getElementById("editCompanyIP");
	var editCompanyDP = document.getElementById("editCompanyDP");
	var editCompanyClose = document.getElementById("editCompanyClose");
	var selectPeriodLength = document.getElementById("selectPeriodLength");
	var textMilestone1 = document.getElementById("textMilestone1");
	var textMilestone2 = document.getElementById("textMilestone2");
	var textMissedMeetingGracePeriod = document.getElementById("textMissedMeetingGracePeriod");
	var selectLanguage = document.getElementById("selectLanguage");
	var textNonUseGracePeriod = document.getElementById("textNonUseGracePeriod");
	var textIPCloseRatio = document.getElementById("textIPCloseRatio");
	var textDPCloseRatio = document.getElementById("textDPCloseRatio");
	var textIPStalledLimit = document.getElementById("textIPStalledLimit");
	var textDPStalledLimit = document.getElementById("textDPStalledLimit");
	var textCustomAmountName = document.getElementById("textCustomAmountName");
	var textReviveLabel = document.getElementById("textReviveLabel");
	var textRevivedLabel = document.getElementById("textRevivedLabel");
	var textRenewLabel = document.getElementById("textRenewLabel");
	var textRenewedLabel = document.getElementById("textRenewedLabel");
	var checkMonetaryValue = document.getElementById('checkMonetaryValue');
	var textMaxDiff = document.getElementById('textMaxDiff');
	var PendingReport = document.getElementById("CheckPendingReport");
	var PendingReportDefault = document.getElementById("CheckPendingReportDefaultValue");
	// ShowContactTitle modification 10/1/10 GMS
	var ShowContactTitle = document.getElementById("checkShowContactTitle");
	var JetFile = document.getElementById("checkJetfile"); 

	JSHeaderText.value = g_the_only_company[g_company.HeaderText];
	
	editNumMonthsNew.value = g_the_only_company[g_company.MonthsConsideredNew];
	editCompanyFM.value = g_the_only_company[g_company.FMThreshold];
	editCompanyIP.value = g_the_only_company[g_company.IPThreshold];
	editCompanyDP.value = g_the_only_company[g_company.DPThreshold];
	editCompanyClose.value = g_the_only_company[g_company.CloseThreshold];
	selectPeriodLength.value = g_the_only_company[g_company.ClosePeriod];
	textMilestone1.value = g_the_only_company[g_company.Requirement1];
	textMilestone2.value = g_the_only_company[g_company.Requirement2];
	checkMilestone1.checked = (g_the_only_company[g_company.Requirement1used] == '1');
	checkMilestone2.checked = (g_the_only_company[g_company.Requirement2used] == '1');
	checkTargetUsed.checked = (g_the_only_company[g_company.TargetUsed] == '1');
	checkValUsed.checked = (g_the_only_company[g_company.ValuationUsed] == '1');
	checkSourceUsed.checked = (g_the_only_company[g_company.SourceUsed] == '1');
	checkSource2Used.checked = (g_the_only_company[g_company.Source2Used] == '1');
	checkTransactional.checked = (g_the_only_company[g_company.Transactional] == '1');
	textTransLabel.value = g_the_only_company[g_company.Translabel];
	textTransAbbr.value = g_the_only_company[g_company.TransAbbr];
	checkBillingUsed.checked = (g_the_only_company[g_company.BillDateUsed] == '1');
	textBillingPrompt.value = g_the_only_company[g_company.BillDatePrompt];
	textSource2Label.value = g_the_only_company[g_company.Source2Label];
	textSource2Abbr.value = g_the_only_company[g_company.Source2Abbr];
	checkRestrictUnassignedUsers.checked = (g_the_only_company[g_company.LimitAccessToUnassignedUsers] == '1');	
	
	checkUseGoogleSync.checked = (g_the_only_company[g_company.UseGoogleSync] == '1');
	/*Start Jet-27 */
	checkEventAlert.checked = (g_the_only_company[g_company.EventAlert] == '1');
	/*End Jet-27 */
		
	checkMyGoalMine.checked = (g_the_only_company[g_company.MyGoalMine] == '1');	
	checkEmailPrivacyEnabled.checked = (g_the_only_company[g_company.EmailPrivacyEnabled] == '1');
	checkAutoMarkEmailsPrivate.checked = (g_the_only_company[g_company.EmailPrivate] == '1');
	checkNoteAlert.checked = (g_the_only_company[g_company.NoteAlert] == '1');
	checkShowAllNotesOfContact.checked = (g_the_only_company[g_company.ShowAllNotesOfContact] == '1');
	textAutoMarkEmailsGracePeriod.value = g_the_only_company[g_company.EmailOffset];
	showHideBillPrompt();
	checkViewSiblingsCalendar.checked = (g_the_only_company[g_company.ViewSiblingsCalendar] == '1');

	textMissedMeetingGracePeriod.value = g_the_only_company[g_company.MissedMeetingGracePeriod];
	selectLanguage.value = g_the_only_company[g_company.Lang];
	textNonUseGracePeriod.value = g_the_only_company[g_company.NonUseGracePeriod];
	textIPCloseRatio.value = g_the_only_company[g_company.IPCloseRatioDefault];
	textDPCloseRatio.value = g_the_only_company[g_company.DPCloseRatioDefault];
	textIPStalledLimit.value = g_the_only_company[g_company.IPStalledLimit];
	textDPStalledLimit.value = g_the_only_company[g_company.DPStalledLimit];
	//Added per Allan E.  9/27/2004  Jim
	textCustomAmountName.value = g_the_only_company[g_company.CustomAmountName];
	textReviveLabel.value = g_the_only_company[g_company.ReviveLabel];
	textRevivedLabel.value = g_the_only_company[g_company.RevivedLabel];
	textRenewLabel.value = g_the_only_company[g_company.RenewLabel];
	textRenewedLabel.value = g_the_only_company[g_company.RenewedLabel];

	checkMonetaryValue.checked = (g_the_only_company[g_company.MonetaryValue] == '1');
	textMaxDiff.value = g_the_only_company[g_company.MaxDiff];
	
	if ( g_the_only_company[g_company.Jetfile] == 1 ) {
		
		JetFile.checked = true ; 
	}
	// ShowContactTitle modification 10/1/10 GMS
	if ( g_the_only_company[g_company.ShowContactTitle] == 1 ) {
		ShowContactTitle.checked = true ; 
	}	
	if ( g_the_only_company[g_company.PendingReport] == 1 ) {
		PendingReport.checked = true ; 
		document.getElementById('CheckPendingReportDefaultValue').disabled = false;
	}else {
		document.getElementById('CheckPendingReportDefaultValue').disabled = true;
	}
	if ( g_the_only_company[g_company.PendingReportDefaultValue] == 1 ) {
		PendingReportDefault.checked = true ; 
	}	
	
	//  this is now done when the tab is clicked
	//	createOpTable();

	mapLevelNums();
	initLevelsTable();

	mapProductIDs();
	fillProductSelect();

	initCloseQuestionsTab();
	initMilestoneQuestionsTab();
	if (g_subMSEnabled)
	initSubMilestonesTab();

	initListsTab();

	var current_tab = get_cookie('current_tab');
	if (current_tab !== null)
	tabbedpane.selectTab(current_tab);

	/*
	setCategoryLabels();

	//Now for that pesky Stalled Limits caption
	var sg = document.getElementById('lblStalledLimits');
	//	var sg.innerHTML = getCatLabel('{{Stalled}}', sg.innerHTML, 'SLabel', 'Stalled');
	var html = sg.innerHTML;
	//alert(html);
	var newHTML = getCatLabel('{{Stalled}}', html, 'SLabel', 'Stalled');
	//	sg.innerHTML = newHTML;
	*/
}

function createOpTable()
{
	opTable = new TableEx(g_sharedimgpath, onEditOpp, onDeleteOpp, onRemoveOpp);
	opTable.onSort = onSortOpps;
	opTable.onClickReviveButton = onReviveOpp;
	opTable.onClickCheck=onClickCheck;
	opTable.onRedraw = onOpTableRedraw;
	opTable.rowsAtOnce = 12;
	/*
	STATUS_COL_INDEX		= opTable.insertColumn("<center><b><nobr>Status</nobr></b></center>", "[JS]cell_value = cat_abbr(\"{Category}\");[/JS]");
	SALESPERSON_COL_INDEX	= opTable.insertColumn("<center><b><nobr>Salesperson</nobr></b></center>", "{LastName}, {FirstName}", null, null, null, "{Color}");
	COMPANY_COL_INDEX		= opTable.insertColumn("<center><b><nobr>Company</nobr></b></center>", "[JS]cell_value = get_rid(\"{Company}\");[/JS]");
	if(g_target_used)
	{
	TARGET_COL_INDEX		= opTable.insertColumn("<center><b><nobr>Date<br>Assigned</nobr></b></center>", "TargetDate", "DATE");
	}
	OPP_COL_INDEX			= opTable.insertColumn("<center><b><nobr>Offering</nobr></b></center>", "{(PROD)Name}|{(RPROD)Name}", null, false, null);
	FM_COL_INDEX			= opTable.insertColumn("<center><b><nobr>First<br>Meeting</nobr></b></center>", "FirstMeeting", "DATE");
	FORCASTCLOSE_COL_INDEX	= opTable.insertColumn("<center><b><nobr>Forecast<br>Close</nobr></b></center>", "[JS]cell_value = calc_close_date('{FirstMeeting}', {PersonID}, '{ProductID}');[/JS]", "DATE");
	ADJUSTEDCLOSE_COL_INDEX	= opTable.insertColumn("<center><b><nobr>Adjusted<br>Close</nobr></b></center>", "AdjustedCloseDate", "DATE");
	CATEGORY_COL_INDEX	= opTable.insertColumn("<center><b><nobr>Category</nobr></b></center>", "Category", null, -1, null, null, true);
	if (g_valuation_used)
	{
	VALUATION_COL_INDEX	= opTable.insertColumn("<center><b><nobr>Val<br>Level</nobr></b></center>", "[JS]cell_value = dovlevel(\"{VLevel}\");[/JS]");
	}
	*/
	STATUS_COL_INDEX		= opTable.insertColumn("<center><b>Status</b></center>", "[JS]cell_value = cat_abbr(\"{Category}\");[/JS]");
	SALESPERSON_COL_INDEX	= opTable.insertColumn("<center><b>Salesperson</b></center>", "{LastName}, {FirstName}", null, null, null, "{Color}");
	COMPANY_COL_INDEX		= opTable.insertColumn("<center><b>Company</b></center>", "[JS]cell_value = get_rid(\"{Company}\");[/JS]");
	if(g_target_used)
	{
		TARGET_COL_INDEX		= opTable.insertColumn("<center><b>Date<br>Assigned</b></center>", "TargetDate", "DATE");
	}

	//	OPP_COL_INDEX			= opTable.insertColumn("<center><b>Offering</b></center>", "{(PROD)Name}|{(RPROD)Name}", null, false, null);
	OPP_COL_INDEX			= opTable.insertColumn("<center><b>Offerings</b></center>", "[JS]cell_value = getOfferingsList(\"{DealID}\");[/JS]", null, false, null);


	var strFMLabel = getCatLabel("{{FM}}", "{{FM}}", 'FMLabel', 'First Meeting');
	strFMLabel = strFMLabel.replace(' ', '<br>');

	//	FM_COL_INDEX			= opTable.insertColumn("<center><b>First<br>Meeting</b></center>", "FirstMeeting", "DATE");
	FM_COL_INDEX			= opTable.insertColumn("<b>" + strFMLabel + "</b></center>", "FirstMeeting", "DATE");



	FORCASTCLOSE_COL_INDEX	= opTable.insertColumn("<center><b>Forecast<br>Close</b></center>", "[JS]cell_value = calc_close_date('{FirstMeeting}', '{PersonID}', '{ProductID}');[/JS]", "DATE");
	ADJUSTEDCLOSE_COL_INDEX	= opTable.insertColumn("<center><b>Adjusted<br>Close</b></center>", "AdjustedCloseDate", "DATE");
	CATEGORY_COL_INDEX	= opTable.insertColumn("<center><b>Category</b></center>", "Category", null, -1, null, null, true);
	if (g_valuation_used)
	{
		VALUATION_COL_INDEX	= opTable.insertColumn("<center><b>Val<br>Level</b></center>", "[JS]cell_value = dovlevel(\"{VLevel}\");[/JS]");
	}


	opTable.setData(g_opportunities);
	opTable.setDataKey(g_opportunities);
	opTable.setData(g_products, "PROD", "ProductID", "ProductID");
	opTable.setData(g_removed_products, "RPROD", "ProductID", "ProductID");
	opTable.setData(g_categories, "CAT");
	opTable.addToElement(document.getElementById('divOppTable'));
}

function updateOrgChart()
{
	spanPleaseWaitOrg = document.getElementById('spanPleaseWaitOrg');
	spanPleaseWaitOrg.style.display = 'inline';
	var iframeOrgChart = document.getElementById('iframeOrgChart');
	iframeOrgChart.src = 'orgchart.html';
}

//<td>Use</td><td>Name</td><td>Prompt</td><td>Type</td><td>Length</td><td>Fill Length</td><td>Mandatory</td>
tableQuestions = null;

function onClickUseQuestion()
{
	this.m_textName.disabled = !this.checked;
	this.m_textPrompt.disabled = !this.checked;
	this.m_selectType.disabled = !this.checked;
	this.m_textLength.disabled = !this.checked;
	this.m_checkFill.disabled = !this.checked;
	this.m_selectManadatory.disabled = !this.checked;
	this.my_question.edited = true;
	this.my_question[g_close_questions.Use] = (this.checked ? '1' : '0');

	Header.blinkOn('Save');
}

function onClickFillLength()
{
	this.my_question[g_close_questions.FillWholeLength] = (this.checked ? '1' : '0');
	this.my_question.edited = true;
	Header.blinkOn('Save');
}

function onChangeQuestionTextField()
{
	this.my_question[g_close_questions[this.my_field_name]] = this.value;
	this.my_question.edited = true;
	Header.blinkOn('Save');
}

function onChangeQuestionType()
{
	if (this.value == 2)
	this.my_question[g_close_questions.AlphaNum] = 1;
	else
	this.my_question[g_close_questions.AlphaNum] = 0;
	this.my_question[g_close_questions.YesNo] = ((this.value == 3) ? 1 : 0);
	this.my_question[g_close_questions.DropDown] = ((this.value == 4) ? 1 : 0);

	this.my_question.edited = true;
	//this.my_question[g_close_questions.Use] = '0';
	Header.blinkOn('Save');
	if (this.value == 1 || this.value == 2)
	{
		this.textLength.style.visibility = 'visible';
		this.checkFill.style.visibility = 'visible';
	}
	else
	{
		this.textLength.style.visibility = 'hidden';
		this.checkFill.style.visibility = 'hidden';
	}

	if (this.value == 4)
	this.butMult.style.visibility = 'visible';
	else
	this.butMult.style.visibility = 'hidden';
}

function onChangeMandatory()
{
	this.my_question[g_close_questions.Mandatory] = this.value;
	this.my_question.edited = true;
	Header.blinkOn('Save');
}

function onClickEditMult()
{
	Windowing.dropBox.g_questions = g_close_questions;
	Windowing.dropBox.g_q = this.my_question;
	Windowing.openSizedPrompt("multiple_answers.html", 350, 250);
}

function multEdited(q)
{
	q.edited = true;
	Header.blinkOn('Save');
}

function initSubMilestonesTab()
{
	var divSubMilestones = document.getElementById('divSubMilestones');
	if (smsTabbedPane)
	divSubMilestones.removeChild(smsTabbedPane.m_main_table);
	smsTabbedPane = new JSTabbedPane(g_imgpath);

	var tab_counter = 0;

	if (g_the_only_company[g_company.Requirement1used] == '1')
	{
		var divReq1Questions = document.createElement("DIV");
		g_req1_q_index = smsTabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">' + g_the_only_company[g_company.Requirement1] + '</a></b>', divReq1Questions);
		divReq1Questions.appendChild(createSubMilestonesTable(0));
		++tab_counter;
	}

	var lblPerson = g_the_only_company[g_company.MS2Label].length == 0 ? 'Person' : g_the_only_company[g_company.MS2Label];
	var lblNeed = g_the_only_company[g_company.MS3Label].length == 0 ? 'Need' : g_the_only_company[g_company.MS3Label];
	var lblMoney = g_the_only_company[g_company.MS4Label].length == 0 ? 'Money' : g_the_only_company[g_company.MS4Label];
	var lblTime = g_the_only_company[g_company.MS5Label].length == 0 ? 'Time' : g_the_only_company[g_company.MS5Label];

	var divPersonQuestions = document.createElement("DIV");
	g_person_q_index = smsTabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">' + lblPerson + '</a></b>', divPersonQuestions);
	divPersonQuestions.appendChild(createSubMilestonesTable(1));
	++tab_counter;

	var divNeedQuestions = document.createElement("DIV");
	g_need_q_index = smsTabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">' + lblNeed + '</a></b>', divNeedQuestions);
	divNeedQuestions.appendChild(createSubMilestonesTable(2));
	++tab_counter;

	var divMoneyQuestions = document.createElement("DIV");
	g_money_q_index = smsTabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">' + lblMoney + '</a></b>', divMoneyQuestions);
	divMoneyQuestions.appendChild(createSubMilestonesTable(3));
	++tab_counter;

	var divTimeQuestions = document.createElement("DIV");
	g_time_q_index = smsTabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">' + lblTime + '</a></b>', divTimeQuestions);
	divTimeQuestions.appendChild(createSubMilestonesTable(4));
	++tab_counter;

	if (g_the_only_company[g_company.Requirement2used] == '1')
	{
		var divReq2Questions = document.createElement("DIV");
		g_req2_q_index = smsTabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">' + g_the_only_company[g_company.Requirement2] + '</a></b>', divReq2Questions);
		divReq2Questions.appendChild(createSubMilestonesTable(5));
		++tab_counter;
	}

	smsTabbedPane.addToElement(divSubMilestones);
	//	fillInsertSelects();
}



function setSequenceMS(location, Seq)
{
	var seq = 0;
	for (var i=0; i< g_submilestones.length; ++i)
	{
		if(location == g_submilestones[i][g_submilestones.Location])
		{
			if(g_submilestones[i][g_submilestones.Seq] >= Seq)
			{
				g_submilestones[i][g_submilestones.Seq]++;
			}
		}
	}
}

function fillCbPrompts(table, cb)
{
	if (!table || !cb) return;
	cb.options.length = 0;
	for (var i=3; i < table.rows.length; ++i)
	{
		var rw = table.rows[i];
		if (rw.style.display == 'none') continue;
		var txt = rw.cells[0].innerHTML;
		var op = document.createElement("OPTION");
		op.value = i;
		op.text = txt;
		cb.options[cb.options.length] = op;
	}
}

function setSequence()
{
	var ind = 3;
	for(var i = 3; i < target_sms_table.rows.length; i++)
	{
		var tr = target_sms_table.rows[i];
		if(tr.style.display == 'none')
		continue;
		if(tr.mysms[g_submilestones.Seq] != ind - 2)
		{
			tr.mysms[g_submilestones.Seq] = (ind - 2);
			tr.mysms.edited = true;
		}
		var tdSeq = tr.cells[0];
		tdSeq.innerHTML= ind - 2;
		ind++;
	}
}

function makeNewSubMS()
{
	var ret = new Array();

	ret.i_am_new = true;

	for (var k in g_submilestones)
	{
		var n = parseInt(k, 10);
		if (!isNaN(n))
		continue;
		if (k.charAt(0) == '_' || k == 'toPipes')
		continue;
		ret[ret.length] = '';
	}

	ret.temp_id = getTempID();
	ret[g_submilestones.SubID] = ret.temp_id;
	return ret;
}

function findSubMS(ID)
{
	for(var i=0; i<g_submilestones.length; i++)
	{
		if(ID == g_submilestones[i][g_submilestones.SubID])
		{
			return g_submilestones[i];
		}
	}
	return null;
}

function copyRec(targ, src)
{
	targ[g_submilestones.Seq] = src[g_submilestones.Seq];
	targ[g_submilestones.Prompt] = src[g_submilestones.Prompt];
	targ[g_submilestones.Enable] = src[g_submilestones.Enable];
	targ[g_submilestones.IsHeading] = src[g_submilestones.IsHeading];
	targ[g_submilestones.AlphaNum] = src[g_submilestones.AlphaNum];
	targ[g_submilestones.YesNo] = src[g_submilestones.YesNo];
	targ[g_submilestones.CheckBox] = src[g_submilestones.CheckBox];
	targ[g_submilestones.DropDown] = src[g_submilestones.DropDown];

}

function update_sms_table(tempsms)
{
	if (tempsms.i_am_new)
	{
		tempsms.added = true;
		tempsms.i_am_new = false;
		tempsms[g_submilestones.CompanyID] = g_the_only_company[g_company.CompanyID];
		tempsms[g_submilestones.Location] = target_location;
		g_submilestones[g_submilestones.length] = tempsms;
		newMSTableRow(g_submilestones, target_sms_table, tempsms, target_location, onEditSubMilestone, onDeleteSubMilestone, -1);
		setSequence();
		Header.blinkOn('Save');
	}
	else
	{
		var Seq = tempsms[g_submilestones.Seq];
		var edit_tr = target_sms_table.rows[source_ind+2];
		copyRec(edit_tr.mysms, tempsms);
		edit_tr.mysms.edited = true;
		edit_tr.cells[0].innerHTML = Seq;
		edit_tr.cells[1].innerHTML = tempsms[g_submilestones.Prompt];
		if(Seq != source_ind)
		{
			target_sms_table.moveRow(1*source_ind + 2, 1*Seq + 2);
			setSequence();
		}
		Header.blinkOn('Save');
	}
}


function onAddSubMilestone()
{

	target_sms_table = this.mytable;
	target_location = this.myloc;
	insert_at_ind = -1;
	insert_at_seq = -1;
	source_ind = -1;
	Windowing.dropBox.msg = 'Please enter the prompt of the new checklist item:';
	Windowing.dropBox.title = 'New Checklist Item';
	Windowing.dropBox.Enable = '1';
	Windowing.dropBox.isHeading = '0';
	//	Windowing.dropBox.Seq = this.mytable.rows.length - 2;
	Windowing.dropBox.promptCallback = onAddSubMilestoneReturn;
	Windowing.dropBox.location = this.myloc;
	Windowing.dropBox.initialValue = "";
	Windowing.dropBox.fldType = "AL";
	Windowing.dropBox.arrChoices = new Array();
	Windowing.dropBox.arrChoicesOK = new Array();
	Windowing.dropBox.maxLen = '';
	Windowing.dropBox.mandatory = '1';
	//	Windowing.openSizedPrompt("insert_ms_prompt.html", 160, 550);
	//	Windowing.openSizedPrompt("edit_ms.html", 340, 750);
	Windowing.openSizedPrompt("edit_ms.html", 540, 900);
}


function onAddSubMilestoneReturn(val, isHeading, Enable, fldType, arrChoices, Length, Mandatory, rightAnswer, arrChoicesOK)
{

	var hd = (isHeading == true) ? 1 : 0;
	var tempsms = new Array();
	tempsms.added = true;
	tempsms.temp_id = getTempID();
	tempsms[g_submilestones.SubID] = tempsms.temp_id;
	tempsms[g_submilestones.CompanyID] = g_the_only_company[g_company.CompanyID];
	tempsms[g_submilestones.Prompt] = val;
	tempsms[g_submilestones.Location] = target_location;
	tempsms[g_submilestones.IsHeading] = hd;
	tempsms[g_submilestones.Enable] = Enable*1;
	tempsms[g_submilestones.RightAnswer] = rightAnswer*1;

	switch (fldType)
	{
		case "AL":
		tempsms[g_submilestones.AlphaNum] = 1;
		break;
		case "YN":
		tempsms[g_submilestones.YesNo] = 1;
		break;
		case "CB":
		tempsms[g_submilestones.CheckBox] = 1;
		break;
		case "DD":
		tempsms[g_submilestones.DropDown] = 1;
		break;
		case "CA":
		tempsms[g_submilestones.Calendar] = 1;
		break;
	}
	if(fldType == "DD")
	{
		var ind = g_submilestones.DropChoice1;
		for(var i=0; i<10; i++)
		{
			var strVal = arrChoices[i];
			if(strVal.length > 0)
			{
				tempsms[ind] = strVal;
			}
			ind++;
		}
		ind = g_submilestones.DChoiceOK1;
		for(var i=0; i<10; i++)
		{
			if(typeof(arrChoicesOK[i]) != 'undefined')
			tempsms[ind] = arrChoicesOK[i];
			ind++;
		}

		ind = g_submilestones.DropChoice11;
		for(var i=10; i<20; i++)
		{
			var strVal = arrChoices[i];
			if(strVal.length > 0)
			{
				tempsms[ind] = strVal;
			}
			ind++;
		}
		ind = g_submilestones.DChoiceOK11;
		for(var i=10; i<20; i++)
		{
			if(typeof(arrChoicesOK[i]) != 'undefined')
			tempsms[ind] = arrChoicesOK[i];
			ind++;
		}
	}

	tempsms[g_submilestones.Length] = Length;
	tempsms[g_submilestones.Mandatory] = Mandatory*1;

	g_submilestones[g_submilestones.length] = tempsms;
	newMSTableRow(g_submilestones, target_sms_table, tempsms, target_location, onEditSubMilestone, onDeleteSubMilestone, onMoveUpMS, onMoveDownMS, -1);
	setSequence();
	Header.blinkOn('Save');
}

function getFldType(rec)
{
	if(rec[g_submilestones.AlphaNum] == 1) return "AL";
	if(rec[g_submilestones.YesNo] == 1) return "YN";
	if(rec[g_submilestones.CheckBox] == 1) return "CB";
	if(rec[g_submilestones.DropDown] == 1) return "DD";
	if(rec[g_submilestones.Calendar] == 1) return "CA";
	return "";
}


function onEditSubMilestone()
{
	target_sms_table = this.mytable;
	target_location = this.myloc;
	Windowing.dropBox.msg = 'Please edit the checklist item:';
	Windowing.dropBox.title = 'Edit Existing Checklist Item';
	Windowing.dropBox.promptCallback = onEditSubMilestoneReturn;
	Windowing.dropBox.Enable = (this.mysms[g_submilestones.Enable]!=0) ? '1' : '0';
	Windowing.dropBox.initialValue = this.mytd.innerHTML;
	Windowing.dropBox.isHeading = (this.mysms[g_submilestones.IsHeading]!=0) ? '1' : '0';
	//	Windowing.dropBox.Seq = this.mytr.rowIndex - 2;
	Windowing.dropBox.fldType = getFldType(this.mysms);

	var arrChoices = new Array();
	var arrChoicesOK = new Array();
	var ind = g_submilestones.DropChoice1;
	for(var i=0; i<10; i++)
	{
		arrChoices[i] = this.mysms[ind];
		ind++;
	}
	ind = g_submilestones.DChoiceOK1;
	for(var i=0; i<10; i++)
	{
		arrChoicesOK[i] = this.mysms[ind];
		ind++;
	}

	ind = g_submilestones.DropChoice11;
	for(var i=10; i<20; i++)
	{
		arrChoices[i] = this.mysms[ind];
		ind++;
	}
	ind = g_submilestones.DChoiceOK11;
	for(var i=10; i<20; i++)
	{
		arrChoicesOK[i] = this.mysms[ind];
		ind++;
	}

	Windowing.dropBox.maxLen = this.mysms[g_submilestones.Length];
	Windowing.dropBox.arrChoices = arrChoices;
	Windowing.dropBox.arrChoicesOK = arrChoicesOK;
	Windowing.dropBox.mandatory = this.mysms[g_submilestones.Mandatory];
	Windowing.dropBox.rightAnswer = this.mysms[g_submilestones.RightAnswer];

	source_ind = this.mytr.rowIndex - 2;
	//	var hash = '#' + this.mysms[g_submilestones.SubID];
	//	Windowing.openSizedPrompt("insert_ms_prompt.html", 160, 550);
	//	Windowing.openSizedPrompt("edit_ms.html", 340, 750);
	Windowing.openSizedPrompt("edit_ms.html", 540, 900);

}

function GetMilestoneType(ms)
{
	var type;
	if (ms[g_submilestones.IsHeading]=='1') type='&nbsp;'
	else if (ms[g_submilestones.CheckBox]=='1') type="Checkbox";
	else if (ms[g_submilestones.YesNo]=='1') type="Yes/No";
	else if (ms[g_submilestones.Calendar]=='1') type="Calendar";
	else if (ms[g_submilestones.AlphaNum]=='1') type="AlphaNum";
	else if (ms[g_submilestones.DropDown]=='1') type="DropDown";
	else type='AlphaNum';
	return type;
}

function GetMilestoneMand(ms)
{
	var mand;
	if (ms[g_submilestones.IsHeading]=='1') mand='&nbsp;'
	else mand=ms[g_submilestones.Mandatory]=='1' ? 'Yes' : 'No';
	return mand;
}

//function onEditSubMilestoneReturn(val, isHeading, Seq, Enable, fldType, arrChoices, Length, Mandatory)
function onEditSubMilestoneReturn(val, isHeading, Enable, fldType, arrChoices, Length, Mandatory, rightAnswer, arrChoicesOK)
{
	var hd = (isHeading == true) ? 1 : 0;
	var edit_tr = target_sms_table.rows[source_ind+2];
	edit_tr.mysms[g_submilestones.Prompt] = val;
	edit_tr.mysms[g_submilestones.IsHeading] = hd;
	//	edit_tr.mysms[g_submilestones.Seq] = Seq;
	edit_tr.mysms[g_submilestones.RightAnswer] = 1*rightAnswer;

	edit_tr.mysms[g_submilestones.Enable] = Enable*1;
	edit_tr.mysms[g_submilestones.AlphaNum] = 0;
	edit_tr.mysms[g_submilestones.YesNo] = 0;
	edit_tr.mysms[g_submilestones.CheckBox] = 0;
	edit_tr.mysms[g_submilestones.DropDown] = 0;
	edit_tr.mysms[g_submilestones.Calendar] = 0;

	switch (fldType)
	{
		case "AL":
		edit_tr.mysms[g_submilestones.AlphaNum] = 1;
		break;
		case "YN":
		edit_tr.mysms[g_submilestones.YesNo] = 1;
		break;
		case "CB":
		edit_tr.mysms[g_submilestones.CheckBox] = 1;
		break;
		case "DD":
		edit_tr.mysms[g_submilestones.DropDown] = 1;
		break;
		case "CA":
		edit_tr.mysms[g_submilestones.Calendar] = 1;
		break;
	}
	if(fldType == "DD")
	{
		var ind = g_submilestones.DropChoice1;
		for(var i=0; i<10; i++)
		{
			edit_tr.mysms[ind] = arrChoices[i];
			ind++;
		}
		ind = g_submilestones.DChoiceOK1;
		for(var i=0; i<10; i++)
		{
			if(typeof(arrChoicesOK[i]) != 'undefined')
			edit_tr.mysms[ind] = arrChoicesOK[i] * 1;
			ind++;
		}

		ind = g_submilestones.DropChoice11;
		for(var i=10; i<20; i++)
		{
			edit_tr.mysms[ind] = arrChoices[i];
			ind++;
		}
		ind = g_submilestones.DChoiceOK11;
		for(var i=10; i<20; i++)
		{
			if(typeof(arrChoicesOK[i]) != 'undefined')
			edit_tr.mysms[ind] = arrChoicesOK[i] * 1;
			ind++;
		}
	}
	edit_tr.mysms[g_submilestones.Length] = Length;
	edit_tr.mysms[g_submilestones.Mandatory] = Mandatory*1;

	edit_tr.mysms.edited = true;

	edit_tr.cells[0].innerHTML = edit_tr.mysms[g_submilestones.Seq];
	edit_tr.cells[1].innerHTML = val;
	if(edit_tr.mysms[g_submilestones.IsHeading] == '1')
	{
		edit_tr.cells[1].style.fontStyle ="italic";
		edit_tr.cells[1].style.fontWeight="bold";
	}
	else
	{
		edit_tr.cells[1].style.fontStyle ="normal";
		edit_tr.cells[1].style.fontWeight="normal";
	}

	edit_tr.cells[2].innerHTML = GetMilestoneType(edit_tr.mysms);
	edit_tr.cells[3].innerHTML = GetMilestoneMand(edit_tr.mysms);
	/*
	if(Seq != source_ind)
	{
	target_sms_table.moveRow(1*source_ind + 2, 1*Seq + 2);
	setSequence();
	}
	*/
	Header.blinkOn('Save');
}



function onDeleteSubMilestone()
{
	if(!confirm('Are you sure you want to delete this item?'))
	{
		return;
	}
	this.mysms.removed = true;
	target_sms_table = this.mytable;
	var ind = this.mytr.rowIndex;
	target_sms_table.deleteRow(ind);
	setSequence();
	Header.blinkOn('Save');
}

function onAddMSAnalysisQuestion()
{
	target_sms_table = this.mytable;
	target_location = this.myloc;
	insert_at_seq = -1;
	insert_at_ind = -1;
	source_ind = -1;

	Windowing.dropBox.msg = 'Please enter the text of the new coaching question:';
	Windowing.dropBox.title = 'New Coaching Question';
	Windowing.dropBox.promptCallback = onAddMSAnalysisQuestionReturn;
	Windowing.dropBox.IsHeading = '0';
	//	Windowing.dropBox.Seq = this.mytable.rows.length - 2;
	Windowing.dropBox.initialValue = "";
	Windowing.openSizedPrompt("insert_ms_prompt.html", 160, 550);
}

//function onAddMSAnalysisQuestionReturn(val, isHeading, Seq)
function onAddMSAnalysisQuestionReturn(val, isHeading)
{
	var hd = (isHeading == true) ? 1 : 0;
	var tempsms = new Array();
	tempsms.added = true;
	tempsms.temp_id = getTempID();
	tempsms[g_msAnalysisQuestions.SubID] = tempsms.temp_id;
	tempsms[g_msAnalysisQuestions.CompanyID] = g_the_only_company[g_company.CompanyID];
	tempsms[g_msAnalysisQuestions.Prompt] = val;
	tempsms[g_msAnalysisQuestions.Location] = target_location;
	tempsms[g_msAnalysisQuestions.IsHeading] = hd;
	//	tempsms[g_msAnalysisQuestions.Seq] = Seq;
	g_msAnalysisQuestions[g_msAnalysisQuestions.length] = tempsms;
	newMSTableRow(g_msAnalysisQuestions, target_sms_table, tempsms, target_location, onEditMSAnalysisQuestion, onDeleteMSAnalysisQuestion, onMoveUpMS, onMoveDownMS, -1);
	setSequence();
	Header.blinkOn('Save');
}

function onEditMSAnalysisQuestion()
{
	target_sms_table = this.mytable;
	target_location = this.myloc;
	Windowing.dropBox.msg = 'Please edit the coaching question:';
	Windowing.dropBox.title = 'Edit Existing Coaching Question';
	Windowing.dropBox.promptCallback = onEditMSAnalysisQuestionReturn;
	Windowing.dropBox.initialValue = this.mytd.innerHTML;
	//	Windowing.dropBox.Seq = this.mytr.rowIndex - 2;
	source_ind = this.mytr.rowIndex - 2;
	Windowing.openSizedPrompt("insert_ms_prompt.html", 160, 550);
}


//function onEditMSAnalysisQuestionReturn(val, isHeading, Seq)
function onEditMSAnalysisQuestionReturn(val, isHeading)
{
	var hd = (isHeading == true) ? 1 : 0;
	var edit_tr = target_sms_table.rows[source_ind+2];
	edit_tr.mysms[g_msAnalysisQuestions.Prompt] = val;
	edit_tr.mysms[g_msAnalysisQuestions.IsHeading] = hd;
	//	edit_tr.mysms[g_msAnalysisQuestions.Seq] = Seq;
	edit_tr.mysms.edited = true;
	edit_tr.cells[0].innerHTML = edit_tr.mysms[g_msAnalysisQuestions.Seq];
	edit_tr.cells[1].innerHTML = val;
	/*
	if(Seq != source_ind)
	{
	target_sms_table.moveRow(1*source_ind + 2, 1*Seq + 2);
	setSequence();
	}
	*/
	Header.blinkOn('Save');
}

function onDeleteMSAnalysisQuestion()
{
	if(!confirm('Are you sure you want to delete this analysis question?'))
	{
		return;
	}

	this.mysms.removed = true;
	target_sms_table = this.mytable;
	var ind = this.mytr.rowIndex;
	target_sms_table.deleteRow(ind);
	setSequence();
	Header.blinkOn('Save');
}

function onMoveUpMS()
{

	var indSrc = this.mytr.rowIndex;
	if(indSrc == 3)
	{
		alert("Can't move the first item up!");
		return;
	}
	target_sms_table = this.mytable;
	target_sms_table.moveRow(indSrc, 1*indSrc - 1);
	setSequence();
	Header.blinkOn('Save');

}

function onMoveDownMS()
{

	var indSrc = this.mytr.rowIndex;
	if(indSrc == this.mytable.rows.length - 1)
	{
		alert("Can't move the last item down!");
		return;
	}
	target_sms_table = this.mytable;
	target_sms_table.moveRow(1*indSrc, 1*indSrc + 1);
	setSequence();
	Header.blinkOn('Save');

}

function newMSTableRow(global_array, temptable, tempsms, location, editFunc, deleteFunc, moveUp, moveDown, insert_at_ind)
{
	var temptr;
	if(insert_at_ind > 0)
	temptr = temptable.insertRow((1*insert_at_ind)+2);
	else
	temptr= temptable.insertRow(temptable.rows.length);
	temptr.mysms = tempsms;
	temptd = temptr.insertCell(temptr.cells.length);
	temptd.innerHTML = temptr.rowIndex-2;

	temptd = temptr.insertCell(temptr.cells.length);
	temptd.innerHTML = get_rid(tempsms[global_array.Prompt]);
	if(tempsms[global_array.IsHeading] == '1')
	{
		temptd.style.fontStyle ="italic";
		temptd.style.fontWeight="bold";
	}
	var prompttd = temptd;

	if (global_array==g_submilestones)
	{
		temptd = temptr.insertCell(temptr.cells.length);
		var type=GetMilestoneType(tempsms);
		temptd.innerHTML = type;

		temptd = temptr.insertCell(temptr.cells.length);
		temptd.align = 'center';
		temptd.innerHTML = GetMilestoneMand(tempsms);

	}

	temptd = temptr.insertCell(temptr.cells.length);
	temptd.width = '1';
	temptd.vAlign = 'top';
	temptd.align = "center";
	var tempbutton = document.createElement("BUTTON");
	tempbutton.mytd = prompttd;
	tempbutton.mytr = temptr;
	tempbutton.mytable = temptable;
	tempbutton.myloc = location;
	tempbutton.mysms = tempsms;
	tempbutton.className = "command";
	tempbutton.onclick = editFunc;
	tempbutton.innerHTML = '<img src="../images/go.gif">';
	temptd.appendChild(tempbutton);

	temptd = temptr.insertCell(temptr.cells.length);
	temptd.width = '1';
	temptd.vAlign = 'top';
	temptd.align = "center";
	var tempbutton = document.createElement("BUTTON");
	tempbutton.mytr = temptr;
	tempbutton.mytable = temptable;
	tempbutton.myloc = location;
	tempbutton.mysms = tempsms;
	tempbutton.className = "command";
	tempbutton.onclick = deleteFunc;
	tempbutton.innerHTML = '<img src="../images/delete.gif">';
	temptd.appendChild(tempbutton);

	//move up
	temptd = temptr.insertCell(temptr.cells.length);
	temptd.noWrap = true;
	temptd.width = '50';
	temptd.vAlign = 'top';
	temptd.align = "center";
	//temptd.nowrap

	//temptd.colspan=2;

	var tempbutton = document.createElement("BUTTON");
	tempbutton.mytr = temptr;
	tempbutton.mytable = temptable;
	tempbutton.myloc = location;
	tempbutton.mysms = tempsms;
	tempbutton.className = "command";
	tempbutton.onclick = moveUp;
	tempbutton.innerHTML = '&uarr;';
	temptd.appendChild(tempbutton);

	var spacespan = document.createElement("SPAN");
	spacespan.innerHTML = '&nbsp;&nbsp;';
	temptd.appendChild(spacespan);

	//move down
	//Move arrows closer together.  8/30/2006
	//		temptd = temptr.insertCell(temptr.cells.length);
	//		temptd.width = '1';
	//		temptd.vAlign = 'top';
	//		temptd.align = "center";
	var tempbutton = document.createElement("BUTTON");
	tempbutton.mytr = temptr;
	tempbutton.mytable = temptable;
	tempbutton.myloc = location;
	tempbutton.mysms = tempsms;
	tempbutton.className = "command";
	tempbutton.onclick = moveDown;
	tempbutton.innerHTML = '&darr;';
	temptd.appendChild(tempbutton);

	// Depends On
	//temptd = temptr.insertCell(temptr.cells.length);

}

function absTop(obj)
{
	var par = obj.offsetParent;
	if (par == null)
	return obj.offsetTop;
	else
	return obj.offsetTop + absTop(par);
}

function absLeft(obj)
{
	var par = obj.offsetParent;
	if (par == null)
	return obj.offsetLeft;
	else
	return obj.offsetLeft + absLeft(par);
}

function milestoneTableTemplate(title, text_name, global_array, addFunc, editFunc, deleteFunc, moveUp, moveDown, location)
{
	var temptable = document.createElement("TABLE");
	temptable.id = "tab" + title + "_" + location;
	//temptable.border = '1';
	//	temptable.width = '100%';
	temptable.cellSpacing=20;

	var titletr = temptable.insertRow(temptable.rows.length);
	var titletd = titletr.insertCell(titletr.cells.length);
	titletd.colSpan = '4';
	//	titletd.align = 'center';
	titletd.align = 'left';
	titletd.style.fontSize = '16pt';
	titletd.innerHTML = title;

	//add button
	var addtr = temptable.insertRow(temptable.rows.length);
	var addtd = addtr.insertCell(addtr.cells.length);
	addtd.align = "left";
	addtd.colSpan = '4';
	addtd.width = '1';
	var addbut = document.createElement("BUTTON");
	addbut.onclick = addFunc;
	addbut.className = "command";
	addbut.innerHTML = "Add";
	addbut.align = "center";
	addbut.width = '120';
	addtd.appendChild(addbut);

	//headings
	var headertr = temptable.insertRow(temptable.rows.length);

	var temptd = headertr.insertCell(headertr.cells.length);
	temptd.innerHTML = '<b>#</b>';

	var temptd = headertr.insertCell(headertr.cells.length);
	temptd.innerHTML = '<b>' + text_name + '</b>';

	if (global_array==g_submilestones)
	{
		var temptd = headertr.insertCell(headertr.cells.length);
		temptd.innerHTML = '<b>Type</b>';

		var temptd = headertr.insertCell(headertr.cells.length);
		temptd.align = 'center';
		temptd.innerHTML = '<b>Mand</b>';
		temptd.id = 'mand';
		temptd.title="Mandatory";
	}

	var temptd = headertr.insertCell(headertr.cells.length);
	temptd.align = 'center';
	temptd.width = '1';
	temptd.innerHTML = '<b>Edit</b>';

	var temptd = headertr.insertCell(headertr.cells.length);
	temptd.align = 'center';
	temptd.width = '1';
	temptd.innerHTML = '<b>Delete</b>';
	temptable.greatest_seq = 0;

	//Two blank cells for up and down arrows
	var temptd = headertr.insertCell(headertr.cells.length);
	//	temptd.align = 'center';
	//	temptd.width = '1';
	temptd.colSpan='2';
	temptd.innerHTML = '<b>Move</b>';

	//var temptd = headertr.insertCell(headertr.cells.length);
	//temptd.align = 'center';
	//temptd.width = '1';
	//temptd.innerHTML = '<b>Depends On</b>';

	//	headertr.insertCell(headertr.cells.length);
	//	temptd.align = 'center';
	//	temptd.width = '1';
	//	temptd.innerHTML = '<b>Down</b>';

	for (var k = 0; k < global_array.length; ++k)
	{
		var tempsms = global_array[k];
		if (tempsms[global_array.Location] != location || tempsms[global_array.CompanyID] != -999)
		continue;
		var temptr = temptable.insertRow(temptable.rows.length);
		temptd = temptr.insertCell(temptr.cells.length);
		temptd.innerHTML = tempsms[global_array.Prompt];

		temptd = temptr.insertCell(temptr.cells.length);
		temptd.width = '1';
		temptd.vAlign = 'top';
		temptd.align = "center";
		temptd.innerHTML = "<i>N/A</i>";

		temptd = temptr.insertCell(temptr.cells.length);
		temptd.width = '1';
		temptd.vAlign = 'top';
		temptd.align = "center";
		temptd.innerHTML = "<i>N/A</i>";

		if (1 * tempsms[global_array.Seq] > 1 * temptable.greatest_seq)
		temptable.greatest_seq = tempsms[global_array.Seq];
	}

	for (var k = 0; k < global_array.length; ++k)
	{
		var tempsms = global_array[k];
		if (tempsms.removed || tempsms[global_array.Location] != location || tempsms[global_array.CompanyID] == -999)
		continue;

		newMSTableRow(global_array, temptable, tempsms, location, editFunc, deleteFunc, moveUp, moveDown, -1);

		if (1 * tempsms[global_array.Seq] > 1 * temptable.greatest_seq)
		temptable.greatest_seq = tempsms[global_array.Seq];
	}
	addbut.mytable = temptable;
	addbut.myloc = location;
	return temptable;
}

function createSubMilestonesTable(location)
{
	var outterTable = document.createElement("TABLE");
	//	outterTable.width = '500';

	var outtertr = outterTable.insertRow(outterTable.rows.length);
	outtertr.height = "50%";
	var outtertd = outtertr.insertCell(outtertr.cells.length);
	outtertd.appendChild(milestoneTableTemplate('Milestone Criteria', 'Prompt', g_submilestones, onAddSubMilestone, onEditSubMilestone, onDeleteSubMilestone, onMoveUpMS, onMoveDownMS, location));
	outtertd.appendChild(document.createElement("BR"));

	var outtertr = outterTable.insertRow(outterTable.rows.length);
	outtertr.height = "50%";
	var outtertd = outtertr.insertCell(outtertr.cells.length);
	outtertd.appendChild(milestoneTableTemplate('Coaching Questions', 'Text', g_msAnalysisQuestions, onAddMSAnalysisQuestion, onEditMSAnalysisQuestion, onDeleteMSAnalysisQuestion, onMoveUpMS, onMoveDownMS, location));

	return outterTable;
}

function onChangeSelectMS(ID)
{
	alert('MS Identifier = ' + ID);
}

function initListsTab()
{
	var div_lists = document.getElementById("div_lists");
	if (listsTabbedPane)
	div_lists.removeChild(listsTabbedPane.m_main_table);
	listsTabbedPane = new JSTabbedPane(g_imgpath);

	var div_category_labels = document.getElementById("div_cat_labels");
	g_category_labels_index = listsTabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">Category Labels</a></b>', div_category_labels);

	var div_milestone_labels = document.getElementById("div_ms_labels");
	g_milestone_labels_index = listsTabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">Milestone Labels</a></b>', div_milestone_labels);

	var div_removal_reasons = document.getElementById("div_removal_reasons");
	g_removal_reasons_index = listsTabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">Removal Reasons</a></b>', div_removal_reasons);

	if(g_source_used)
	{
		var div_sources = document.getElementById("div_sources");
		div_sources.style.visibility="visible";
		g_sources_tab_index = listsTabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">Sources</a></b>', div_sources);
	}
	if(g_source2_used)
	{
		var div_sources2 = document.getElementById("div_sources2");
		div_sources2.style.visibility="visible";
		g_sources2_tab_index = listsTabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">'+g_the_only_company[g_company.Source2Label]+'</a></b>', div_sources2);
	}
	var div_reports = document.getElementById("div_reports");
	g_reports_index = listsTabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">Salesperson Reports</a></b>', div_reports);

	if(g_valuation_used)
	{
		var div_valuations = document.getElementById("div_valuations");
		g_valuations_tab_index = listsTabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">'+g_the_only_company[g_company.ValLabel]+'</a></b>', div_valuations);
	}




	listsTabbedPane.addToElement(div_lists);

	fillMSDropdowns();
	fillCatDropdowns();

	if(g_valuation_used)
	initValuationsTab(g_valuations);
	if(g_source_used)
	fillSourceSelect();
	if(g_source2_used)
	fillSource2Select();
	initReportsTab();
}

function initMilestoneQuestionsTab()
{
	var div_milestone_questions = document.getElementById("div_milestone_questions");
	if (msqTabbedPane)
	div_milestone_questions.removeChild(msqTabbedPane.m_main_table);
	msqTabbedPane = new JSTabbedPane(g_imgpath);

	var tab_counter = 0;

	if (g_the_only_company[g_company.Requirement1used] == '1')
	{
		var divReq1Questions = document.createElement("DIV");
		g_req1_q_index = msqTabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">' + g_the_only_company[g_company.Requirement1] + '</a></b>', divReq1Questions);
		divReq1Questions.appendChild(createQuestionsTable(g_req1_questions, msqTabbedPane, tab_counter, tabbedpane, g_milestone_questions_tab_index));
		++tab_counter;
	}

	var lblPerson = g_the_only_company[g_company.MS2Label].length == 0 ? 'Person' : g_the_only_company[g_company.MS2Label];
	var lblNeed = g_the_only_company[g_company.MS3Label].length == 0 ? 'Need' : g_the_only_company[g_company.MS3Label];
	var lblMoney = g_the_only_company[g_company.MS4Label].length == 0 ? 'Money' : g_the_only_company[g_company.MS4Label];
	var lblTime = g_the_only_company[g_company.MS5Label].length == 0 ? 'Time' : g_the_only_company[g_company.MS5Label];

	var divPersonQuestions = document.createElement("DIV");
	g_person_q_index = msqTabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">' + lblPerson+ '</a></b>', divPersonQuestions);
	divPersonQuestions.appendChild(createQuestionsTable(g_person_questions, msqTabbedPane, tab_counter, tabbedpane, g_milestone_questions_tab_index));
	++tab_counter;

	var divNeedQuestions = document.createElement("DIV");
	g_need_q_index = msqTabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">' + lblNeed + '</a></b>', divNeedQuestions);
	divNeedQuestions.appendChild(createQuestionsTable(g_need_questions, msqTabbedPane, tab_counter, tabbedpane, g_milestone_questions_tab_index));
	++tab_counter;

	var divMoneyQuestions = document.createElement("DIV");
	g_money_q_index = msqTabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">' + lblMoney + '</a></b>', divMoneyQuestions);
	divMoneyQuestions.appendChild(createQuestionsTable(g_money_questions, msqTabbedPane, tab_counter, tabbedpane, g_milestone_questions_tab_index));
	++tab_counter;

	var divTimeQuestions = document.createElement("DIV");
	g_time_q_index = msqTabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">' + lblTime + '</a></b>', divTimeQuestions);
	divTimeQuestions.appendChild(createQuestionsTable(g_time_questions, msqTabbedPane, tab_counter, tabbedpane, g_milestone_questions_tab_index));
	++tab_counter;

	if (g_the_only_company[g_company.Requirement2used] == '1')
	{
		var divReq2Questions = document.createElement("DIV");
		g_req2_q_index = msqTabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem">' + g_the_only_company[g_company.Requirement2] + '</a></b>', divReq2Questions);
		divReq2Questions.appendChild(createQuestionsTable(g_req2_questions, msqTabbedPane, tab_counter, tabbedpane, g_milestone_questions_tab_index));
		++tab_counter;
	}

	msqTabbedPane.addToElement(div_milestone_questions);
}

function createQuestionsTable(questions_array)
{
	var args = createQuestionsTable.arguments;
	var temptable = document.createElement("TABLE");
	var headertr = temptable.insertRow(temptable.rows.length);
	for (var k = 0; k < q_header_titles.length; ++k)
	{
		var headertd = headertr.insertCell(headertr.cells.length);
		headertd.align = 'center';
		headertd.innerHTML = q_header_titles[k];
	}

	var check_a = new Array();
	for (var k = 0; k < questions_array.length; ++k)
	{
		var temp_q = questions_array[k];
		var temptr = temptable.insertRow(temptable.rows.length);

		var temptd = temptr.insertCell(temptr.cells.length);
		var checkUse = document.createElement("INPUT");
		checkUse.type = "checkbox";
		checkUse.onclick = onClickUseQuestion;
		var use_it = checkUse.checked = (temp_q[questions_array.Use] == '1');
		for (var n = 1; n < args.length; n += 2)
		args[n].addCheckboxException(args[n + 1], checkUse);
		temptd.appendChild(checkUse);

		if (use_it)
		check_a[check_a.length] = checkUse;

		var temptd = temptr.insertCell(temptr.cells.length);
		var textName = document.createElement("INPUT");
		textName.type = "text";
		textName.my_field_name = 'Name';
		textName.my_question = temp_q;
		textName.onchange = onChangeQuestionTextField;
		if (use_it)
		textName.value = temp_q[questions_array.Name];
		else
		textName.disabled = true;
		temptd.appendChild(textName);

		var temptd = temptr.insertCell(temptr.cells.length);
		var textPrompt = document.createElement("INPUT");
		textPrompt.type = "text";
		textPrompt.my_field_name = 'Prompt';
		textPrompt.my_question = temp_q;
		textPrompt.onchange = onChangeQuestionTextField;
		if (use_it)
		textPrompt.value = temp_q[questions_array.Prompt];
		else
		textPrompt.disabled = true;
		temptd.appendChild(textPrompt);

		var the_type = 1;
		if (temp_q[questions_array.AlphaNum] == '1')
		the_type = 2;
		else if (temp_q[questions_array.YesNo] == '1')
		the_type = 3;
		else if (temp_q[questions_array.DropDown] == '1')
		the_type = 4;

		var temptd = temptr.insertCell(temptr.cells.length);
		var selectType = document.createElement("SELECT");
		var sel_i = -1;
		for (var n = 0; n < types_a.length; n += 2)
		{
			var tempoption = document.createElement("OPTION");
			tempoption.text = types_a[n];
			tempoption.value = types_a[n + 1];
			if (the_type == types_a[n + 1])
			sel_i = parseInt(n / 2, 10);
			if (isNav)
			selectType.add(tempoption, null);
			else
			selectType.add(tempoption);
		}
		if (use_it)
		{
			if (sel_i != -1)
			selectType.selectedIndex = sel_i;
		}
		else
		selectType.disabled = true;
		selectType.onchange = onChangeQuestionType;
		temptd.appendChild(selectType);

		var temptd = temptr.insertCell(temptr.cells.length);
		var butMult = document.createElement('BUTTON');
		//butMult.type = 'button';
		butMult.innerText = 'Edit Choices';
		butMult.innerHTML = 'Edit Choices';
		butMult.value = 'Edit Choices';
		butMult.className = 'command';
		butMult.onclick = onClickEditMult;
		butMult.my_question = temp_q;
		if (the_type != 4)
		butMult.style.visibility = 'hidden';
		temptd.appendChild(butMult);

		var temptd = temptr.insertCell(temptr.cells.length);
		var textLength = document.createElement("INPUT");
		textLength.type = "text";
		textLength.size = '4';
		textLength.my_field_name = 'Length';
		textLength.my_question = temp_q;
		textLength.onchange = onChangeQuestionTextField;
		if (use_it)
		textLength.value = temp_q[questions_array.Length];
		else
		textLength.disabled = true;
		if (the_type != 1 && the_type != 2)
		textLength.style.visibility = "hidden";
		temptd.appendChild(textLength);

		var temptd = temptr.insertCell(temptr.cells.length);
		temptd.align = "center";
		var checkFill = document.createElement("INPUT");
		checkFill.type = "checkbox";
		checkFill.my_question = temp_q;
		checkFill.onclick = onClickFillLength;
		if (use_it)
		{
			if (temp_q[questions_array.FillWholeLength] == '1')
			{
				checkFill.checked = true;
				check_a[check_a.length] = checkFill;
			}
		}
		else
		checkFill.disabled = true;
		if (the_type != 1 && the_type != 2)
		checkFill.style.visibility = "hidden";
		for (var n = 1; n < args.length; n += 2)
		args[n].addCheckboxException(args[n + 1], checkFill);
		temptd.appendChild(checkFill);

		var temptd = temptr.insertCell(temptr.cells.length);
		temptd.align = "center";
		var selectManadatory = document.createElement("SELECT");
		selectManadatory.onchange = onChangeMandatory;
		selectManadatory.my_question = temp_q;
		var sel_i = -1;
		for (var n = 0; n < mandatory_a.length; n += 2)
		{
			var tempoption = document.createElement("OPTION");
			tempoption.text = mandatory_a[n];
			tempoption.value = mandatory_a[n + 1];
			if (temp_q[questions_array.Mandatory] == mandatory_a[n + 1])
			sel_i = parseInt(n / 2, 10);
			if (isNav)
			selectManadatory.add(tempoption, null);
			else
			selectManadatory.add(tempoption);
		}
		if (use_it)
		{
			if (sel_i != -1)
			selectManadatory.selectedIndex = sel_i;
		}
		else
		selectManadatory.disabled = true;
		temptd.appendChild(selectManadatory);

		checkUse.my_question = temp_q;
		checkUse.m_textName = textName;
		checkUse.m_textPrompt = textPrompt;
		checkUse.m_selectType = selectType;
		checkUse.m_textLength = textLength;
		checkUse.m_checkFill = checkFill;
		checkUse.m_selectManadatory = selectManadatory;

		selectType.my_question = temp_q;
		selectType.textLength = textLength;
		selectType.checkFill = checkFill;
		selectType.butMult = butMult;
	}

	for (var k = 0; k < check_a.length; ++k)
	check_a[k].checked = true;

	return temptable;
}

function initCloseQuestionsTab()
{
	document.getElementById('div_close_questions').appendChild(createQuestionsTable(g_close_questions, tabbedpane, g_close_questions_tab_index));
}

function onClickCheckActive()
{
	this.LabelField.disabled = !this.checked;
	this.my_question.edited = true;
	this.my_question[g_valuations.Activated] = (this.checked ? '1' : '0');
	Header.blinkOn('Save');
}


function makeValTR(rowData, thistr, ind)
{
	var enabled;
	if(!rowData.length)
	{
		enabled = false;
	}
	else
	enabled = (0<rowData[g_valuations.Label].length);
	//Level
	temptd = thistr.insertCell(thistr.cells.length);
	temptd.align = 'center';
	temptd.innerHTML = ind+1;

	//Activated checkbox
	var tdActive = thistr.insertCell(thistr.cells.length);
	tdActive.align = 'center';
	var checkActive = document.createElement("INPUT");
	checkActive.type = 'checkbox';
	tdActive.appendChild(checkActive);
	if(enabled)
	{
		checkActive.checked = (rowData[g_valuations.Activated]== '1');
	}

	checkActive.my_question=rowData;
	checkActive.my_field_name = 'Activated';
	checkActive.my_field_name.edited=false;
	checkActive.dataIndex=ind;
	checkActive.onclick=onClickCheckActive;




	//Label
	var tdDesc = thistr.insertCell(thistr.cells.length);
	var textVal = document.createElement("INPUT");
	textVal.type = "text";
	textVal.size = "70";
	textVal.my_field_name = 'Label';
	textVal.maxLength="50";
	textVal.my_question = rowData;
	textVal.dataIndex=ind;
	textVal.my_question.edited = false;
	textVal.onchange = onChangeValTextField;
	if (enabled)
	textVal.value = rowData[g_valuations.Label];
	else
	textVal.disabled = true;
	tdDesc.appendChild(textVal);

	//Point the checkbox to the desc field
	checkActive.LabelField = textVal;

}


function onChangeValTextField()
{
	this.my_question[g_valuations[this.my_field_name]] = this.value;
	this.my_question[g_valuations.VLevel] = this.dataIndex+1;
	this.my_question.edited = true;
	Header.blinkOn('Save');
}

function initValuationsTab(arrValuations)
{
	var temptable = document.createElement("TABLE");
	document.getElementById('valTable').appendChild(temptable);
	temptable.cellspacing = "10";
	var headertr = temptable.insertRow(temptable.rows.length);
	for (var k = 0; k < v_header_titles.length; k++)
	{
		var headertd = headertr.insertCell(headertr.cells.length);
		headertd.align = 'center';
		headertd.innerHTML = v_header_titles[k];
	}
	val_hdr_len=temptable.rows.length;
	for (k = 0; k < 20; k++)
	{
		var datarow=Array(0);
		var valrow= temptable.insertRow(temptable.rows.length);
		if (k<g_valuations.length)
		datarow = g_valuations[k];
		makeValTR(datarow, valrow, k);
	}

}

function setRptPermission()
{
	//alert('xref count = ' + g_reports_company_xrefs.length);
	for(var i=0; i<g_reports_company_xrefs.length; i++)
	{
		if(this.ReportID == g_reports_company_xrefs[i][g_reports_company_xrefs.ReportID])
		{
			g_reports_company_xrefs[i][g_reports_company_xrefs.Salespeople] = this.checked ? 1 : 0;
			g_reports_company_xrefs[i].edited = true;
			Header.blinkOn('Save');
			return;
		}
	}
	//alert('adding new rec!');
	var newRec = Array();
	newRec.temp_id = getTempID();
	newRec[g_reports_company_xrefs.ID] = newRec.temp_id;
	newRec[g_reports_company_xrefs.ReportID] = this.ReportID;
	newRec[g_reports_company_xrefs.Salespeople] = this.checked ? 1 : 0;
	newRec[g_reports_company_xrefs.CompanyID] = g_the_only_company[g_company.CompanyID];
	newRec.added = true;
	g_reports_company_xrefs[g_reports_company_xrefs.length] = newRec;

	//alert('xref count = ' + g_reports_company_xrefs.length);

	Header.blinkOn('Save');
}

function getRptPermission(ReportID)
{
	for (var i = 0; i < g_reports_company_xrefs.length; i++)
	{
		if(ReportID == g_reports_company_xrefs[i][g_reports_company_xrefs.ReportID])
		return (g_reports_company_xrefs[i][g_reports_company_xrefs.Salespeople] == '1');
	}
	return 0;
}

function initReportsTab()
{
	//	var temptable = document.createElement("TABLE");
	//	document.getElementById("repTable").appendChild(temptable);
	//	temptable.cellspacing = "10";
	var reptable = document.getElementById("repTable");

	for (var i=0; i<g_reports.length; i++)
	{
		var trNew = reptable.insertRow(reptable.rows.length);

		var tdSales = trNew.insertCell(trNew.cells.length);
		var cbSales = document.createElement("INPUT");
		cbSales.type="checkbox";
		tdSales.appendChild(cbSales);
		cbSales.ReportID = g_reports[i][g_reports.ReportID];
		cbSales.onclick = setRptPermission;
		cbSales.checked = getRptPermission(g_reports[i][g_reports.ReportID]);
		tdSales.align="center";

		var tdName = trNew.insertCell(trNew.cells.length);
		tdName.innerHTML = substituteCategoryLabels(g_reports[i][g_reports.Name]);



	}
	// perhaps this can be changed to match the others, once current matches the other reports
	// several places need to be changed, including the db and which_reports.html
	var checkViewExpectedReport = document.getElementById('checkViewExpectedReport');
	checkViewExpectedReport.checked = (g_the_only_company[g_company.ViewExpectedBilling] == '1');
}

function updateGOpps()
{
	opTable.setData(g_opportunities);
	opTable.setDataKey(g_opportunities);
	opTable.setData(g_products, "PROD", "ProductID", "ProductID");
	opTable.setData(g_categories, "CAT");
	opTable.redraw();
}

function mapLevelNums()
{
	for (var k = 0; k < g_levels.length; ++k)
	g_levelNumMap[g_levels[k][g_levels.LevelNumber]] = g_levels[k];
}

function setPersonLevels(highest_level, target_node)
{
	if (!target_node)
	target_node = g_assignedtree;
	var item_count = target_node.getItemCount();
	for (var k = 0; k < item_count; ++k)
	{
		var tempnode = target_node.getItem(k);
		tempnode.value[g_people.Level] = highest_level;
		tempnode.value.edited = true;
		chartDirty = true;
		Header.blinkOn('Save');
		setPersonLevels(highest_level - 1, tempnode);
	}
}

function levelNameOnChange()
{
	this.mytr.mylevel[g_levels.Name] = this.value;
	this.mytr.mylevel.edited = true;
	Header.blinkOn('Save');
}

function levelsEnableOnChange()
{
	if ((this.mytable.highestShown - this.mytr.myIndex) > 1)
	{
		this.checked = true;
		return;
	}
	if (this.checked)
	{
		if (this.mytr.nextRow)
		{
			this.mytr.nextRow.style.visibility = "visible";
			this.mytable.highestShown = this.mytr.nextRow.myIndex;
			g_hightest_level = this.mytr.nextRow.myIndex;
		}
		else
		g_hightest_level = 6;
		this.mytr.mylevel[g_levels.Enabled] = '1';
	}
	else
	{
		if (this.mytr.myIndex < (g_assignedtree.getDepth() - 1))
		{
			alert('Please rearrange the people tree to reflect fewer levels first before disabling the number of company defined levels');
			this.checked = true;
			return;
		}
		if (this.mytr.nextRow)
		{
			this.mytr.nextRow.style.visibility = "hidden";
			this.mytr.nextRow.inputEnable.checked = false;
		}
		this.mytable.highestShown = this.mytr.myIndex;
		g_hightest_level = this.mytr.myIndex;
		this.mytr.mylevel[g_levels.Enabled] = '0';
	}

	this.mytr.mylevel.edited = true;
	Header.blinkOn('Save');
}

function levelsViewSibsOnChange()
{
	this.mytr.mylevel[g_levels.ViewSiblings] = this.checked ? '1' : '0';
	this.mytr.mylevel.edited = true;
	Header.blinkOn('Save');
}

function levelsImportOnChange()
{
	this.mytr.mylevel[g_levels.Import] = this.checked ? '1' : '0';
	this.mytr.mylevel.edited = true;
	Header.blinkOn('Save');
}


function initLevelsTable()
{
	g_tableLevels = document.getElementById("tableLevels");
	g_tableLevels.highestShown = 0;
	g_hightest_level = 0;
	var prevrow = null;
	for (var k = 0; k < g_levels.length; ++k)
	{
		var temptr = g_tableLevels.insertRow(g_tableLevels.rows.length);
		temptr.myIndex = k;

		var temptd = temptr.insertCell(temptr.cells.length);
		temptd.innerHTML = (k + 1) + ". ";

		temptd = temptr.insertCell(temptr.cells.length);
		temptd.align = "center";
		temptr.inputEnable = document.createElement("INPUT");
		//tabbedpane.addCheckboxException(g_company_tab_index, temptr.inputEnable);
		temptr.inputEnable.type = "checkbox";
		if (k < 2)
		{
			temptr.inputEnable.checked = true;
			temptr.inputEnable.style.visibility = "hidden";
		}
		temptr.inputEnable.onclick = levelsEnableOnChange;
		temptr.inputEnable.mytr = temptr;
		temptr.inputEnable.mytable = g_tableLevels;
		temptd.appendChild(temptr.inputEnable);

		temptd = temptr.insertCell(temptr.cells.length);
		temptd.align = "center";
		temptr.inputSibs = document.createElement("INPUT");
		temptr.inputSibs.mytr = temptr;
		temptr.inputSibs.type = "checkbox";
		temptr.inputSibs.onclick = levelsViewSibsOnChange;
		temptd.appendChild(temptr.inputSibs);
		temptr.inputSibs.checked = (g_levelNumMap[k + 1][g_levels.ViewSiblings] == '1');
		//tabbedpane.addCheckboxException(g_company_tab_index, temptr.inputSibs);
		//********************************
		temptd = temptr.insertCell(temptr.cells.length);
		temptd.align = "center";
		temptr.importOK = document.createElement("INPUT");
		temptr.importOK.mytr = temptr;
		temptr.importOK.type = "checkbox";
		temptr.importOK.onclick = levelsImportOnChange;
		temptd.appendChild(temptr.importOK);
		temptr.importOK.checked = (g_levelNumMap[k + 1][g_levels.Import] == '1');
		//********************************


		temptd = temptr.insertCell(temptr.cells.length);
		temptr.inputName = document.createElement("INPUT");
		temptr.inputName.mytr = temptr;
		temptr.inputName.onchange = levelNameOnChange;
		temptr.inputName.type = "text";
		temptr.inputName.maxLength="50";
		if (g_levelNumMap[k + 1])
		{
			temptr.mylevel = g_levelNumMap[k + 1];
			temptr.inputName.value = temptr.mylevel[g_levels.Name];
			temptr.inputEnable.checked = (temptr.mylevel[g_levels.Enabled] == 1);
			if (temptr.inputEnable.checked)
			g_hightest_level = k + 1;
		}
		temptd.appendChild(temptr.inputName);

		if (k > 2 && prevrow && !prevrow.inputEnable.checked)
		temptr.style.visibility = "hidden";
		else
		g_tableLevels.highestShown = k;

		if (prevrow != null)
		prevrow.nextRow = temptr;
		prevrow = temptr;
	}
}

function fillPersonTree(clearEdits)
{
	g_assignedtree = new XPloreTree("<b>Assigned</b>", -4, "../images/folder_open.gif", "../images/folder_closed.gif");
	g_assignedtree.m_movable = false;
	g_assignedtree.m_selectable = false;
	g_unassignedtree = new XPloreTree("<b>Unassigned</b>", -3, "../images/folder_open.gif", "../images/folder_closed.gif");
	g_unassignedtree.m_movable = false;
	g_unassignedtree.m_selectable = false;
	g_administratortree = new XPloreTree("<b>Administrators</b>", -5, "../images/folder_open.gif", "../images/folder_closed.gif");
	g_administratortree.m_movable = false;
	g_administratortree.m_selectable = false;
	g_administratortree.m_flat = true;
	var id2person = new Object();
	for (var k = 0; k < g_people.length; ++k)
	{
		var tempstr = g_people[k][g_people.LastName] + ", " + g_people[k][g_people.FirstName];
		if (g_people[k][g_people.SupervisorID] != -1 && g_people[k][g_people.SupervisorID] != -3)
		tempstr = "(" + g_people[k][g_people.Level] + ") " + tempstr;
		//g_people[k].mynode = new XPloreTree(tempstr, g_people[k], "../images/sp_icon.gif");
		g_people[k].mynode = new XPloreTree(tempstr, g_people[k]);
		id2person[g_people[k][g_people.PersonID]] = g_people[k];
	}
	for (var curlev = 10; curlev > -5; --curlev)
	{
		for (var k = 0; k < g_people.length; ++k)
		{
			var tempperson = g_people[k];
			if (tempperson[g_people.Level] != curlev)
			continue;
			if (tempperson[g_people.SupervisorID] == -3)
			g_administratortree.addTree(tempperson.mynode);
			else if (tempperson[g_people.SupervisorID] == -1)
			g_unassignedtree.addTree(tempperson.mynode);
			else if (tempperson[g_people.SupervisorID] == -2)
			g_assignedtree.addTree(tempperson.mynode);
			else
			{
				var tempsuper = id2person[tempperson[g_people.SupervisorID]];
				if (tempsuper)
				tempsuper.mynode.addTree(tempperson.mynode);
				else
				g_unassignedtree.addTree(tempperson.mynode);
			}
		}
	}
	persontree.addTree(g_assignedtree);
	persontree.addTree(g_unassignedtree);
	persontree.addTree(g_administratortree);
	g_assignedtree.colapseAll();
	g_assignedtree.expand();
	g_administratortree.colapseAll();
}

function mapProductIDs()
{
	for (var k = 0; k < g_products.length; ++k)
	g_productIDmap[g_products[k][g_products.ProductID]] = g_products[k];
}

function fillProductSelect()
{
	var selectProducts = document.getElementById("selectProducts");
	for (var k = 0; k < g_products.length; ++k)
	{
		var tempoption = document.createElement("OPTION");
		tempoption.text = g_products[k][g_products.Name];
		tempoption.myItem = g_products[k];
		if (isNav)
		selectProducts.add(tempoption, null);
		else
		selectProducts.add(tempoption);
	}
}

function fillSourceSelect()
{
	var selectSources = document.getElementById("selectSources");
	for (var k = 0; k < g_sources.length; ++k)
	{
		var tempoption = document.createElement("OPTION");
		tempoption.text = g_sources[k][g_sources.Name];
		tempoption.myItem = g_sources[k];
		if (isNav)
		selectSources.add(tempoption, null);
		else
		selectSources.add(tempoption);
	}
}

function fillSource2Select()
{
	var selectSources2 = document.getElementById("selectSources2");
	for (var k = 0; k < g_sources2.length; ++k)
	{
		var tempoption = document.createElement("OPTION");
		tempoption.text = g_sources2[k][g_sources2.Name];
		tempoption.myItem = g_sources2[k];
		if (isNav)
		selectSources2.add(tempoption, null);
		else
		selectSources2.add(tempoption);
	}
}


function onMovePerson(theperson, movedto, lockChildrenValues)
{
	theperson.value.mynode = theperson;
	if (movedto.value == -3)
	{
		theperson.value[g_people.SupervisorID] = -1;
		updateSPTreeText(theperson);
	}
	else if (movedto.value == -4)
	{
		theperson.value[g_people.SupervisorID] = -2;
		if (!lockChildrenValues)
		{
			var old_level = theperson.value[g_people.Level];
			theperson.value[g_people.Level] = g_hightest_level;
			if (theperson.value[g_people.Level] > 1 && old_level < 2)
			theperson.value[g_people.RatingViewAllowed] = '1';
			if (theperson.value[g_people.Level] < old_level)
			setPersonLevels(theperson.value[g_people.Level] - 1, theperson);
		}
		if (theperson.value[g_people.Color] == '808080')
		chooseNextAvailableColor(theperson.value);
		updateSPTreeText(theperson);
	}
	else if (movedto.value == -5)
	{
		theperson.value[g_people.SupervisorID] = -3;
		theperson.value[g_people.IsSaleperson] = 0;
	}
	else
	{
		theperson.value[g_people.SupervisorID] = movedto.value[g_people.PersonID];
		if (!lockChildrenValues)
		{
			var old_level = theperson.value[g_people.Level];
			theperson.value[g_people.Level] = movedto.value[g_people.Level] - 1;
			if (theperson.value[g_people.Level] > 1 && old_level < 2)
			theperson.value[g_people.RatingViewAllowed] = '1';
			if (theperson.value[g_people.Level] < old_level)
			setPersonLevels(theperson.value[g_people.Level] - 1, theperson);
		}
		if (theperson.value[g_people.Color] == '808080')
		chooseNextAvailableColor(theperson.value);
		updateSPTreeText(theperson);
	}
	theperson.value.edited = true;
	chartDirty = true;
	Header.blinkOn('Save');
}

function fillRemovalReasonsSelect()
{
	var selectRemovalReasons = document.getElementById("selectRemovalReasons");
	for (var k = 0; k < g_removereasons.length; ++k)
	{
		var tempoption = document.createElement("OPTION");
		tempoption.text = g_removereasons[k][g_removereasons.Text];
		tempoption.myReason = g_removereasons[k];
		if (isNav)
		selectRemovalReasons.add(tempoption, null);
		else
		selectRemovalReasons.add(tempoption);
	}
}

function _isDuplicateUserID() //need to look at logins table instead!
{
	if (!g_selectedSp)
	return false;
	var isAdmin = false;
	if (g_selectedSp.mynode.m_parent.value == -5)
	isAdmin = true;
	var editUserID = document.getElementById('editUserID');
	if (editUserID)
	{
		for (var k = 0; k < g_people.length; ++k)
		{
			if (g_people[k].removed || g_people[k] == g_selectedSp)
			continue;
			if (g_people[k][g_people.UserID] == editUserID.value &&
			isAdmin == (g_people[k].mynode.m_parent.value == -5))
			{
				editUserID.select();
				editUserID.focus();
				alert('Sorry, that UserID is already taken. Please enter a different UserID');
				return true;
			}
		}
	}
	return false;
}

function isDuplicateUserID_Alias(ID, userID)
{
	if (!g_selectedSp)
	return false;
	var thisPerson = g_selectedSp[g_people.PersonID];

	for (var i=0; i<g_logins.length; ++i)
	{
		//		if(g_logins[i][g_logins.PersonID] == thisPerson &&
		//			g_logins[i][g_logins.LoginID] == loginID)
		//			continue;
		if(g_logins[i][g_logins.ID] == ID) continue;
		if(g_logins[i][g_logins.UserID] == userID)
		return true;
	}
	return false;

}

function isDuplicateUserID()
{
	if (!g_selectedSp)
	return false;
	var isAdmin = false;
	if (g_selectedSp.mynode.m_parent.value == -5)
	isAdmin = true;
	var editUserID = document.getElementById('editUserID');
	var thisPerson = g_selectedSp[g_people.PersonID];
	if (editUserID)
	{
		for (var k = 0; k < g_logins.length; ++k)
		{
			if(g_logins[k][g_logins.PersonID] == thisPerson &&
			g_logins[k][g_logins.LoginID] == thisPerson)
			continue;
			if (g_logins[k][g_logins.UserID] == editUserID.value)
			//				&& isAdmin == (g_people[k].mynode.m_parent.value == -5))
			{
				editUserID.select();
				editUserID.focus();
				alert('Sorry, that UserID is already taken. Please enter a different UserID');
				return true;
			}
		}
	}
	return false;
}


function isDuplicateName()
{
	if (!g_selectedSp)
	return false;
	var isAdmin = false;
	if (g_selectedSp.mynode.m_parent.value == -5)
	isAdmin = true;
	var editFirstName = document.getElementById('editFirstName');
	var editLastName = document.getElementById('editLastName');
	for (var k = 0; k < g_people.length; ++k)
	{
		if (g_people[k].removed || g_people[k] == g_selectedSp)
		continue;
		if (g_people[k][g_people.FirstName] == editFirstName.value && editFirstName.value != '[USER]' &&
		g_people[k][g_people.LastName] == editLastName.value && editLastName.value != '[NEW]' &&
		isAdmin == (g_people[k].mynode.m_parent.value == -5))
		{
			editFirstName.select();
			editFirstName.focus();
			alert('Sorry, that combination of First Name and Last Name is already taken.');
			return true;
		}
	}
	return false;
}

function PendingReportCheck(elem)
{
//	var elem = document.getElementById(elemid);

	if (elem.checked == true ){
		g_the_only_company[g_company.PendingReport] = 1 ;
		document.getElementById('CheckPendingReportDefaultValue').disabled = false;
	}
	else {
		g_the_only_company[g_company.PendingReport] = 0 ; 
		document.getElementById('CheckPendingReportDefaultValue').disabled = true;
	}
	g_the_only_company.edited = true;
	Header.blinkOn('Save');
}

function PendingReportDefaultCheck(elem)
{
//	var elem = document.getElementById(elemid);

	if (elem.checked == true ){
		g_the_only_company[g_company.PendingReportDefaultValue] = 1 ;
	}
	else {
		g_the_only_company[g_company.PendingReportDefaultValue] = 0 ; 
	}
	g_the_only_company.edited = true;
	Header.blinkOn('Save');
}

function updateSPTreeText(the_item)
{
	var tempstr = the_item.value[g_people.LastName] + ", " + the_item.value[g_people.FirstName];
	var dist = the_item.getDistanceFrom(g_assignedtree);
	//if (the_item.value[g_people.SupervisorID] != -1 && the_item.value[g_people.SupervisorID] != -3)
	if (dist != -1)
	tempstr = "(" + the_item.value[g_people.Level] + ") " + tempstr;
	the_item.setText(tempstr);
	for (var k = 0; k < the_item.getItemCount(); ++k)
	updateSPTreeText(the_item.getItem(k));
}

function chooseNextAvailableColor(person)
{
	var group_list = create_group_list(person, person);
	var color_map = new Object();
	for (var k = 0; k < group_list.length; ++k)
	color_map[group_list[k][g_people.Color]] = group_list[k];
	for (var k = 0; k < g_simpleColorsArray.length; ++k)
	{
		var temp_a = g_simpleColorsArray_trans[k];
		for (var n = 0; n < temp_a.length; ++n)
		{
			if (!color_map[temp_a[n]])
			{
				if (person == g_selectedSp)
				setSelectedUserProperty("Color", temp_a[n]);
				else
				{
					person[g_people.Color] = temp_a[n];
					person.edited = true;
					chartDirty = true;
					Header.blinkOn('Save');
				}
				opTable.redraw();
				return;
			}
		}
	}
}

function setSelectedUserProperty(propname, val)
{
	//var selecteditem = persontree.getSelectedItem();
	//if (!selecteditem)
	if (!g_selectedSp)
	return false;
	var oldval = g_selectedSp[g_people[propname]];

	if (propname == 'UserID' && isDuplicateUserID())
	return;
	//else if ((propname == 'FirstName' || propname == 'LastName') && isDuplicateName())
	//	else if (propname == 'FirstName' || propname == 'LastName')
	//		return;

	g_selectedSp[g_people[propname]] = val;
	g_selectedSp.edited = true;
	chartDirty = true;
	Header.blinkOn('Save');

	var do_table_redraw = false;
	
	if (propname == "Color")
	{
		document.getElementById("spanColor").style.backgroundColor = val;
		for (var k = 0; k < g_opportunities.length; ++k)
		{
			if (g_selectedSp[g_people.PersonID] == g_opportunities[k][g_opportunities.PersonID])
			{
				g_opportunities[k][g_opportunities.Color] = val;
				do_table_redraw = true;
			}
		}
	}
	else if (propname == "FirstName" || propname == "LastName" || propname == "Level" || propname == "PrivateEmail")
	{
		for (var k = 0; k < g_opportunities.length; ++k)
		{
			if (g_selectedSp[g_people.PersonID] == g_opportunities[k][g_opportunities.PersonID])
			{
				g_opportunities[k][g_opportunities[propname]] = val;
				do_table_redraw = true;
			}
		}
		updateSPTreeText(g_selectedSp.mynode);
		if (propname == "Level")
		{
			var checkTypeSalesperson = document.getElementById('checkTypeSalesperson');
			if (val > 1 && oldval < 2)
			{
				document.getElementById('checkRanked').checked = true;
				checkTypeSalesperson.disabled = false;
			}
			else if (val == 1)
			{
				checkTypeSalesperson.checked = true;
				checkTypeSalesperson.disabled = true;
				setSelectedUserProperty('IsSalesperson', 1);
			}
		}
		else if(propname == "MailtoAction"){
			if(val != ''){
				document.getElementById('MailtoAction').value = val;
			}
		}
		if (propname == "PrivateEmail")
		{
			var checkPrivateEmail = document.getElementById('checkPrivateEmail');
			
			if (val == 1)
			{
				checkPrivateEmail.checked = true;
			}
		}
		g_people_list_dirty = true;
	}
	else if ( propname == "HideJetstream"){
		if ( val == '1'){
			document.getElementById('checkHideJetstream').checked = true;
		}else {
			document.getElementById('checkHideJetstream').checked = false;		
		}
		
	}
	else if ( propname == "MasterAssignee"){
		if ( val == '1'){
			document.getElementById('checkMasterAssignee').checked = true;
		}
		else {
			document.getElementById('checkMasterAssignee').checked = false;		
		}
		
	} 
	else if ( propname == "SpecialPermissionsID"){
		if ( val == '1'){
			document.getElementById('checkSpecialPermissions').checked = true;
		}
		else {
			document.getElementById('checkSpecialPermissions').checked = false;		
		}
		
	} 
	else if (propname == "IsSalesperson")
	{
		if (val == '0' && g_selectedSp[g_people.Level] > 1 && hasOpps(g_selectedSp[g_people.PersonID]))
		{
			alert("Please reassign this person's opportunities disabling the salesperson property");
			document.getElementById('checkTypeSalesperson').checked = true;
			return;
		}
		if (val == '1' && g_selectedSp.mynode.getDistanceFrom(g_assignedtree) != -1 && g_selectedSp.mynode.getDistanceFrom(g_assignedtree) != 4)
		chooseNextAvailableColor(g_selectedSp);
		g_people_list_dirty = true;
	}
	else if (propname == 'GroupName')
	g_people_list_dirty = true;

	if (do_table_redraw)
	opTable.redraw();

	return true;
}

function checkIPCloseRatio()
{
	var textIPCloseRatio = document.getElementById('textIPCloseRatio');
	if (!textIPCloseRatio)
	return true;
	var temp = parseInt(textIPCloseRatio.value);
	if (isNaN(temp) || temp < 0 || temp > 100)
	{
		alert("Information Phase Default "+g_the_only_company[g_company.CLabel]+" Ratio must be a value between 0 and 100");
		textIPCloseRatio.value = g_the_only_company[g_company.IPCloseRatioDefault];
		return false;
	}
	return true;
}

function checkDPCloseRatio()
{
	var textDPCloseRatio = document.getElementById('textDPCloseRatio');
	if (!textDPCloseRatio)
	return true;
	var temp = parseInt(textDPCloseRatio.value);
	if (isNaN(temp) || temp < 0 || temp > 100)
	{
		alert("Decision Point Default "+g_the_only_company[g_company.CLabel]+" Ratio must be a value between 0 and 100");
		textDPCloseRatio.value = g_the_only_company[g_company.DPCloseRatioDefault];
		return false;
	}
	return true;
}

function checkCompanyTab()
{
	if (!checkIPCloseRatio())
	return false;
	if (!checkDPCloseRatio())
	return false;
	return true;
}

function companyTextFieldEdited(elemid, propname)
{
	var elem = document.getElementById(elemid);
	var args = companyTextFieldEdited.arguments;
	if (args.length > 2 && args[2] == 'do_refresh')
	g_reload_after_save = true;

	//alert("Refresh after Save flag = " + g_reload_after_save);

	if (propname == 'IPCloseRatioDefault' && !checkIPCloseRatio())
	return false;
	if (propname == 'DPCloseRatioDefault' && !checkDPCloseRatio())
	return false;

	if (propname == 'CustomAmountName')
	{
		if(elem.value)
		g_the_only_company[g_company.HasCustomAmount] = 1;
		else
		g_the_only_company[g_company.HasCustomAmount] = 0;
	}

	g_the_only_company[g_company[propname]] = elem.value;
	g_the_only_company.edited = true;
	Header.blinkOn('Save');
}

function companyCheckboxEdited(elemid, propname)
{
	var elem = document.getElementById(elemid);
	var args = companyCheckboxEdited.arguments;
	if (args.length > 2 && args[2] == 'do_refresh')
	g_reload_after_save = true;

	g_the_only_company[g_company[propname]] = (elem.checked == true) ? '1' : '0';
	g_the_only_company.edited = true;
	Header.blinkOn('Save');
	if (propname == 'Requirement1used')
	{
		if (!elem.checked)
		{
			document.getElementById('textMilestone1').value = '';
			g_the_only_company[g_company.Requirement1] = '';
		}
	}
	if (propname == 'Requirement2used')
	{
		if (!elem.checked)
		{
			document.getElementById('textMilestone2').value = '';
			g_the_only_company[g_company.Requirement2] = '';
		}
	}
	if (propname == 'Requirement1used' || propname == 'Requirement2used')
	initMilestoneQuestionsTab();
}

function getPossibleLevels(the_node)
{
	var out_array = new Array();
	var toplevel = -1;
	if (the_node.m_parent.value != -3 && the_node.m_parent.value != -4)
	toplevel = the_node.m_parent.value[g_people.Level] - 1;
	else
	toplevel = g_hightest_level;
	out_array[out_array.length] = toplevel;
	var highest_child_level = 0;
	for (var k = 0; k < the_node.getItemCount(); ++k)
	{
		var tempitem = the_node.getItem(k);
		var templevel = parseInt(tempitem.value[g_people.Level]);
		if (isNaN(templevel) == false && templevel > highest_child_level)
		highest_child_level = templevel;
	}
	for (var k = toplevel - 1; k > highest_child_level; --k)
	out_array[out_array.length] = k;
	return out_array;
}

function selectSalesPerson(sp)
{	
//	alert('jellow' + sp);
	
	var main_spedit_td = document.getElementById("main_spedit_td");
	var editUserID = document.getElementById("editUserID");
	var editPassword = document.getElementById("editPassword");
	var editFirstName = document.getElementById("editFirstName");
	var editLastName = document.getElementById("editLastName");
	var editEmail = document.getElementById("editEmail");	
	var editStartDate = document.getElementById("editStartDate");
	var editFirstMeetingThreshold = document.getElementById("editFirstMeetingThreshold");
	var editInformationPhaseThreshold = document.getElementById("editInformationPhaseThreshold");
	var editDecisionPointThreshold = document.getElementById("editDecisionPointThreshold");
	var editCloseThreshold = document.getElementById("editCloseThreshold");
	var checkRanked = document.getElementById("checkRanked");
	var checkSibs = document.getElementById("checkSibs");
	var spanColor = document.getElementById("spanColor");
	var checkTypeSalesperson = document.getElementById("checkTypeSalesperson");
	var checkAppearsOnManager = document.getElementById("checkAppearsOnManager");
	var textGroupName = document.getElementById('textGroupName');
	var checkImport = document.getElementById("checkImport");
	var AllowUploads = document.getElementById("AllowUploads");
	var AllowExport = document.getElementById("AllowExport");
	var HideJetstream = document.getElementById("HideJetstream");
	var MasterAssignee = document.getElementById("MasterAssignee");
	var SpecialPermissionsID = document.getElementById("SpecialPermissionsID");
	var MailtoAction = document.getElementById("selectMailtoAction");
	


	if (sp != g_selectedSp)
	Windowing.closeAllWindows();

	if (!sp)
	{
		editFirstName.value = "";
		editLastName.value = "";
		editEmail.value = "";
		editPassword.value = "";
		editStartDate.value = "";
		editFirstMeetingThreshold = "";
		editInformationPhaseThreshold = "";
		editDecisionPointThreshold = "";


		document.getElementById("editInfo_span").className = "EditInfoSpan_Hidden";
		document.getElementById("polite_div").className = "EditInfoSpan_Visible";
		g_selectedSp = null;
		main_spedit_td.vAlign = "center";
		return;
	}

	g_selectedSp = sp;

	if (g_selectedSp.mynode.m_parent.value == -5) // Administrators
	{
		document.getElementById('trColor').style.visibility = 'hidden';
		document.getElementById('trIsSalesperson').style.visibility = 'hidden';
		document.getElementById('divSPPriv').style.display = 'none';
		document.getElementById('divSPThreshold').style.display = 'none';
		document.getElementById('divSPLevel').style.display = 'none';
		document.getElementById('trGroupName').style.visibility = 'hidden';
		document.getElementById('trAssignButton').style.visibility = 'hidden';
	}
	else if (g_selectedSp.mynode.getDistanceFrom(g_assignedtree) == -1) // Assigned (?)
	{
		document.getElementById('trColor').style.visibility = 'visible';
		document.getElementById('trIsSalesperson').style.visibility = 'visible';
		document.getElementById('divSPPriv').style.display = 'none';
		document.getElementById('divSPLevel').style.display = 'none';
		document.getElementById('trGroupName').style.visibility = 'hidden';
		document.getElementById('trAssignButton').style.visibility = 'visible';
		showSPFields(g_selectedSp[g_people.IsSalesperson] == 1);
		document.getElementById('checkTypeSalesperson').disabled = false;
	}
	else //Unassigned
	{
		if (g_selectedSp[g_people.PrivateEmail] == 1)
		{			
			setSelectedUserProperty('PrivateEmail', 1);
		}
		
		/*if (g_selectedSp[g_people.Level] == 1)
		{
			if (g_selectedSp[g_people.IsSalesperson] != 1)
			setSelectedUserProperty('IsSalesperson', 1);
			document.getElementById('checkTypeSalesperson').disabled = true;
		}		*/
		else
		document.getElementById('checkTypeSalesperson').disabled = false;
		document.getElementById('trColor').style.visibility = 'visible';
		document.getElementById('divSPLevel').style.display = 'block';
		document.getElementById('divSPPriv').style.display = 'block';
		document.getElementById('trIsSalesperson').style.visibility = 'visible';
		document.getElementById('trAssignButton').style.visibility = 'visible';
		showSPFields(g_selectedSp[g_people.IsSalesperson] == 1);

		var possible_levels = getPossibleLevels(g_selectedSp.mynode);
		var selectPersonLevel = document.getElementById('selectPersonLevel');
		while (selectPersonLevel.options.length)
		selectPersonLevel.remove(0);
		var sel_index = -1;
		for (var k = 0; k < possible_levels.length; ++k)
		{
			var tempoption = document.createElement("OPTION");
			tempoption.text = possible_levels[k];
			tempoption.value = possible_levels[k];
			if (possible_levels[k] == g_selectedSp[g_people.Level])
			sel_index = k;
			if (isNav)
			selectPersonLevel.add(tempoption, (selectPersonLevel.options[0]) ? selectPersonLevel.options[0] : null);
			else
			selectPersonLevel.add(tempoption, 0);
		}

		if (sel_index != -1)
		selectPersonLevel.selectedIndex = (possible_levels.length - sel_index) - 1;

		if (g_the_only_company[g_company.CustomSPGroupName] && g_the_only_company[g_company.CustomSPGroupName] != '')
		{
			document.getElementById('trGroupName').style.visibility = 'visible';
			if (parseInt(g_selectedSp[g_people.Level]) > 1)
			document.getElementById('tdGroupName').innerHTML = 'Group Name';
			else
			document.getElementById('tdGroupName').innerHTML = g_the_only_company[g_company.CustomSPGroupName];
		}
		else if (parseInt(g_selectedSp[g_people.Level]) > 1)
		document.getElementById('trGroupName').style.visibility = 'visible';
		else
		document.getElementById('trGroupName').style.visibility = 'hidden';
	}
	
	try {
		setTimeout("getPeopleEmails()", 100);
	} catch(e) {}

	//	editUserID.value = g_selectedSp[g_people.UserID];
	editUserID.value = getLoginProperty(g_logins.UserID, null);

	editFirstName.value = g_selectedSp[g_people.FirstName];
	editLastName.value = g_selectedSp[g_people.LastName];
	editEmail.value = g_selectedSp[g_people.Email];
	
	//	editPassword.value = g_selectedSp[g_people.PasswordZ];
	editPassword.value = getLoginProperty(g_logins.PasswordZ, null);
	editStartDate.value = Dates.formatDate(new Date(Dates.mssql2us(g_selectedSp[g_people.StartDate])));
	editFirstMeetingThreshold.value = g_selectedSp[g_people.FMThreshold];
	editInformationPhaseThreshold.value = g_selectedSp[g_people.IPThreshold];
	editDecisionPointThreshold.value = g_selectedSp[g_people.DPThreshold];
	editCloseThreshold.value = g_selectedSp[g_people.CloseThreshold];
	checkRanked.checked = (g_selectedSp[g_people.RatingViewAllowed] == 1);
	checkSibs.checked = (g_selectedSp[g_people.ViewSiblings] == 1);
	checkImport.checked = (getLoginProperty(g_logins.Import, null) == 1);
	spanColor.style.backgroundColor = g_selectedSp[g_people.Color];
	checkTypeSalesperson.checked = (g_selectedSp[g_people.IsSalesperson] == 1);
	checkAppearsOnManager.checked = (g_selectedSp[g_people.AppearsOnManager] == 1);
	textGroupName.value = g_selectedSp[g_people.GroupName];

	if(getLoginProperty(g_logins.AllowUploads) == 1){
		AllowUploads.checked = true;
	}
	else {
		AllowUploads.checked = false; 
	}

	if(getLoginProperty(g_logins.AllowExport) == 1){
		AllowExport.checked = true;
	}
	else {
		AllowExport.checked = false; 
	}


	//	HideJetstream.checked = g_selectedSp[g_people.HideJetstream];
	if(g_selectedSp[g_people.HideJetstream] == 1){
		HideJetstream.checked = true;
	}
	else {
		HideJetstream.checked = false; 
	}

	if(g_selectedSp[g_people.MasterAssignee] == 1){
		MasterAssignee.checked = true;
	}
	else {
		MasterAssignee.checked = false; 
	}

	if(g_selectedSp[g_people.SpecialPermissionsID] == 1){
		checkSpecialPermissions.checked = true;
	}
	else {
		checkSpecialPermissions.checked = false; 
	}
	
	if(g_selectedSp[g_people.MailtoAction] != ''){
		MailtoAction.value = g_selectedSp[g_people.MailtoAction];
	}
	else {
		MailtoAction.value = 0;
	}

	
	document.getElementById("editInfo_span").className = "EditInfoSpan_Visible";
	document.getElementById("polite_div").className = "EditInfoSpan_Hidden";
	main_spedit_td.vAlign = "top";
	if (g_selectedSp[g_people.Level] > 1)
	{
		document.getElementById("divAliases").style.display = "block";
		document.getElementById("spanAliasTo").innerHTML = g_selectedSp[g_people.FirstName]+" "+g_selectedSp[g_people.LastName];
		fillAliasList();
	}
	else
	document.getElementById("divAliases").style.display = "none";

	var aliasForList = new Array();
	for (var k = 0; k < g_alias_owner_xrefs.length; ++k)
	{
		if (!g_alias_owner_xrefs[k].deleted && g_alias_owner_xrefs[k][g_alias_owner_xrefs.PersonID] == g_selectedSp[g_people.PersonID])
		{
			for (var n = 0; n < g_logins.length; ++n)
			{
				if (g_alias_owner_xrefs[k][g_alias_owner_xrefs.LoginID] == g_logins[n][g_logins.ID])
				{
					for (var j = 0; j < g_people.length; ++j)
					{
						if (g_logins[n][g_logins.PersonID] == g_people[j][g_people.PersonID])
						{
							aliasForList[aliasForList.length] = g_people[j][g_people.LastName]+", "+g_people[j][g_people.FirstName]+' ('+g_people[j][g_people.Level]+')';
							break;
						}
					}
				}
			}
		}
	}
	if (aliasForList.length == 0)
	document.getElementById("divAliasesFor").style.display = "none";
	else
	{
		aliasForList.sort();
		document.getElementById("spanAliasFor").innerHTML = g_selectedSp[g_people.FirstName]+" "+g_selectedSp[g_people.LastName];
		var str = "";
		for (var k = 0; k < aliasForList.length; ++k)
		str += aliasForList[k]+"<br>";
		document.getElementById("spanAliasForList").innerHTML = str;
		document.getElementById("divAliasesFor").style.display = "block";
	}

	if (g_selectedSp.mynode.getDistanceFrom(g_assignedtree) != -1 && !verifyColors(g_selectedSp))
	{
		alert("The color of the currently selected person is already taken in the group. Please choose a new color before assigning.");
		show_color_picker();
		return;
	}
	
	
}

//Put error checking elsewhere!
function setLoginProperty(propind, val)
{
	if (!g_selectedSp)
	return false;
	/*
	if (propind == 3)
	if(IsDuplicateUserID(val))
	return false;
	*/

	Header.blinkOn('Save');
	var PersonID = g_selectedSp[g_people.PersonID];
	for (var i = 0; i < g_logins.length; ++i)
	{
		if (PersonID == g_logins[i][g_logins.PersonID] && (PersonID == g_logins[i][g_logins.LoginID]))
		break;
	}
	if (i == g_logins.length)
	{
		var newLog = Array();
		newLog['ID'] = getTempID();
		newLog[g_logins.LoginID] = PersonID;
		newLog[g_logins.PersonID] = PersonID;
		newLog[g_logins.CompanyID] = g_the_only_company[g_company.CompanyID];
		newLog[g_logins.SuperId] = g_selectedSp[g_people.SupervisorID];
		newLog.added = true;
		newLog[propind] = val;
		g_logins[g_logins.length] = newLog;
	}
	else
	{
		g_logins[i][g_logins.SuperId] = g_selectedSp[g_people.SupervisorID];
		g_logins[i][propind] = val;
		g_logins[i].edited = true;
		chartDirty = true;
	}
	return true;
}

function getLoginProperty(propind, AliasID)
{

	if (!g_selectedSp)
	return false;
	var PersonID = g_selectedSp[g_people.PersonID];
	if (AliasID==null) AliasID = PersonID;
	for (var i = 0; i < g_logins.length; ++i)
	{
		if (PersonID == g_logins[i][g_logins.PersonID] && (AliasID == g_logins[i][g_logins.LoginID]))
		break;
	}
	if (i == g_logins.length)
	{
		if(propind == g_logins.UserID) return '[new user]';
		return '';
	}else{
		if(propind == g_logins.PasswordZ) return '*******';
	}
	return g_logins[i][propind];
}

function getName(personID)
{
	for (i=0; i< g_people.length; i++)
	{
		if(personID == g_people[i][g_people.PersonID])
		{
			return g_people[i][g_people.LastName] + ', ' + g_people[i][g_people.FirstName];
		}
	}
	return 'Unknown';
}

function fillAliasList()
{
	var al = document.getElementById('selectAliasList');
	var sp = g_selectedSp[g_people.PersonID];
	if(al.options.length) al.options.length = 0;
	var alist = new Array();
	for (var i=0; i<g_logins.length; ++i)
	{
		if (g_logins[i][g_logins.PersonID] != sp || g_logins[i][g_logins.LoginID] == sp)
		continue;
		alist[alist.length] = {value:g_logins[i][g_logins.ID], text:g_logins[i][g_logins.Name], myData:g_logins[i]};
	}

	alist.sort(function(a, b)
	{
		if (a.text < b.text)
		return -1;
		else if (a.text > b.text)
		return 1;
		else
		return 0;
	});

	for (var k = 0; k < alist.length; ++k)
	{
		var temp = alist[k];
		var o = document.createElement("OPTION");
		al.options[al.options.length] = o;
		o.value = temp.value;
		o.myData = temp.myData;
		o.text = temp.text;
	}
}


//Obsolete
function passwordEdited()
{
	setSelectedUserProperty("PasswordZ", document.getElementById("editPassword").value);
}

function check_start_date()
{
	var editStartDate = document.getElementById("editStartDate");
	if (!editStartDate)
	return true;
	var val = editStartDate.value;
	if (val != '')
	{
		val = Dates.normalize(val);
		if (!val)
		{
			alert('Invlid date format for Start Date.\nMust be either MM/DD/YY or MM/DD/YYYY');
			return false;
		}
	}
	return true;
}

function onEditStartDate()
{
	if (check_start_date())
	setSelectedUserProperty("StartDate", document.getElementById("editStartDate").value);
}

function create_group_list(user_obj, exclude)
{
	if (!exclude)
	exclude = null;
	var grouplist = new Array();
	if (user_obj[g_people.IsSalesperson] == 1 && user_obj != exclude)
	grouplist[grouplist.length] = user_obj;
	for (var k = 0; k < g_people.length; ++k)
	{
		if (g_people[k].removed || g_people[k] == exclude || g_people[k][g_people.IsSalesperson] != 1)
		continue;
		if (g_people[k][g_people.PersonID] == user_obj[g_people.SupervisorID] && user_obj[g_people.Level] == 1)
		grouplist[grouplist.length] = g_people[k];
		else if (g_people[k][g_people.SupervisorID] == user_obj[g_people.PersonID] && g_people[k][g_people.Level] == 1)
		grouplist[grouplist.length] = g_people[k];
		else if (g_people[k][g_people.SupervisorID] == user_obj[g_people.SupervisorID] && user_obj[g_people.Level] == 1)
		grouplist[grouplist.length] = g_people[k];
	}
	return grouplist;
}

function onClickColorSpan()
{
	show_color_picker();
}

function onClickChangeStartDate()
{
	Windowing.dropBox.startYear=-30;
	Windowing.dropBox.endYear = 1;
	var calwin = Windowing.openSizedPrompt("../shared/calendar.html", 320, 600);
	Windowing.dropBox.startYear=-30;
	Windowing.dropBox.endYear = 1;
	Windowing.dropBox.calendarTarget = document.getElementById("editStartDate");
	Windowing.dropBox.onCalendarEdited = onEditStartDate;
}



function onClickChangeOppsStartDate()
{
	Windowing.dropBox.startYear=-30;
	Windowing.dropBox.endYear = 1;
	var calwin = Windowing.openSizedPrompt("../shared/calendar.html", 320, 600);
	Windowing.dropBox.startYear=-30;
	Windowing.dropBox.endYear = 1;
	Windowing.dropBox.calendarTarget = document.getElementById("oppsStartDate");
	Windowing.dropBox.onCalendarEdited = onEditStartDate;
}


function onClickChangeOppsEndDate()
{
	Windowing.dropBox.startYear=-30;
	Windowing.dropBox.endYear = 1;
	var calwin = Windowing.openSizedPrompt("../shared/calendar.html", 320, 600);
	Windowing.dropBox.startYear=-30;
	Windowing.dropBox.endYear = 1;
	Windowing.dropBox.calendarTarget = document.getElementById("oppsEndDate");
	Windowing.dropBox.onCalendarEdited = onEditStartDate;
}

// SHARED EDIT COMPATIBILITY FUNCTIONS

function find_matching_opportunity(company, productID)
{
	for (var i = 0; i < g_opps.length; ++i)
	{
		if (g_opps[i][g_opps.Company] != company) continue;
		if (g_opps[i][g_opps.ProductID] != productID) continue;
		return g_opps[i];
	}

	return null;
}

function find_matching_target(company, personID, dealID)
{
	for (var i=0; i < g_opps.length; i++)
	{
		if (g_opps[i][g_opps.DealID] == dealID) continue;
		if (g_opps[i][g_opps.Category] != '10') continue;
		if (g_opps[i][g_opps.Company].toUpperCase() != company.toUpperCase()) continue;
		if (g_opps[i][g_opps.PersonID] != personID) continue;
		return true;
	}
	return false;
}

function make_new_opportunity()
{
	var ret = new Array();
	ret.i_am_new = true;
	for (var k in g_opps)
	{
		var n = parseInt(k, 10);
		if (!isNaN(n))
		continue;
		if (k.charAt(0) == '_' || k == 'toPipes')
		continue;
		ret[ret.length] = '';
	}
	ret.temp_id = "'" + getTempID() + "'";
	//	ret[g_opps.DealID] = getTempID();
	ret[g_opps.DealID] = ret.temp_id;

	return ret;
}

function makeNewLogin()
{
	var ret = new Array();
	ret.i_am_new = true;
	for (var k in g_logins)
	{
		var n = parseInt(k, 10);
		if (!isNaN(n))
		continue;
		if (k.charAt(0) == '_' || k == 'toPipes')
		continue;
		ret[ret.length] = '';
	}
	ret[g_logins.ID] = getTempID();
	ret.temp_id = ret[g_logins.ID];
	return ret;
}

function find_person(id)
{
	var i;
	for (i=0;i<g_people.length;i++)
	{
		if (g_people[i][g_people.PersonID] == id) return g_people[i];
	}
	return null;
}

function get_salesperson_info(id)
{
	var ret = new Object();

	var monthsNew = g_company[0][g_company.MonthsConsideredNew];
	for (var i = 0; i < g_people.length; ++i)
	{
		if (g_people[i][g_people.PersonID] != id)
		continue;
		ret.rating = g_people[i][g_people.Rating];
		ret.color = g_people[i][g_people.Color];
		ret.name = g_people[i][g_people.FirstName] + ' ' + g_people[i][g_people.LastName];

		var expDate = new Date(g_people[i][g_people.StartDate]);
		for (; monthsNew>0; --monthsNew)
		{
			var m = expDate.getMonth();
			if (m == 11)
			{
				expDate.setMonth(0);
				expDate.setYear(expDate.getYear() + 1);
			}
			else expDate.setMonth(m + 1);
		}
		if (expDate.getTime() > new Date().getTime())
		{
			ret.tenure = 'New';
			// ret.name += ' (n)';
		}
		else ret.tenure = 'Experienced';

		ret.analysisID = g_people[i][g_people.AnalysisID];
		break;
	}

	return ret;
}

function get_opportunity_columns()
{
	var ret = new Array();
	for (var k in g_opps)
	{
		var n = parseInt(k, 10);
		if (!isNaN(n)) continue;
		if (k.charAt(0) == '_' || k == 'toPipes') continue;
		ret[ret.length] = k;
	}

	return ret;
}

function find_opportunity(id)
{
	for (var i = 0; i < g_opps.length; ++i)
	if (g_opps[i][g_opps.DealID] == id) return g_opps[i];

	return null;
}

function find_login(pid, uid)
{
	for (var i=0; i < g_logins.length; ++i)
	{
		if (g_logins[i][g_logins.UserID] == uid)
		return g_logins[i];

	}
	/*
	for (var i=0; i< g_logins.length; ++i)
	{
	if (g_logins[i][g_logins.PersonID] == pid && g_logins[i][g_logins.LoginID] == lid)
	return g_logins[i];
	}
	*/
	return null;
}

function list_product_options()
{
	if (g_product_list_dirty)
	{
		g_sorted_products_array = new Array();
		for (var k = 0; k < g_products.length; ++k)
		{
			var tempobj = new Object
			tempobj.str = g_products[k][g_products.Name];
			tempobj.data = g_products[k];
			g_sorted_products_array[g_sorted_products_array.length] = tempobj;
		}
		g_sorted_products_array.sort(people_sort_compare);
		g_product_list_dirty = false;
	}

	if (g_sorted_products_array)
	{
		var ret = '';
		for (var i = 0; i < g_sorted_products_array.length; ++i)
		ret += '<option value="' + g_sorted_products_array[i].data[g_products.ProductID] + '">' + g_sorted_products_array[i].str + '</option>';
		return ret;
	}
	else
	{
		var ret = '';
		for (var i = 0; i < g_products.length; ++i)
		{
			var data = g_products[i];
			ret += '<option value="' + data[g_products.ProductID] + '">' + data[g_products.Name] + '</option>';
		}
		return ret;
	}
}


/*
function list_source_options()
{
if (g_source_list_dirty)
{
g_sorted_sources_array = new Array();
for (var k = 0; k < g_sources.length; ++k)
{
var tempobj = new Object
tempobj.str = g_sources[k][g_sources.Name];
tempobj.data = g_sources[k];
g_sorted_sources_array[g_sorted_sources_array.length] = tempobj;
}
g_sorted_sources_array.sort(people_sort_compare);
g_source_list_dirty = false;
}

if (g_sorted_sources_array)
{
var ret = '';
for (var i = 0; i < g_sorted_sources_array.length; ++i)
ret += '<option value="' + g_sorted_sources_array[i].data[g_sources.SourceID] + '">' + g_sorted_sources_array[i].str + '</option>';
return ret;
}
else
{
var ret = '';
for (var i = 0; i < g_sources.length; ++i)
{
var data = g_sources[i];
ret += '<option value="' + data[g_sources.SourceID] + '">' + data[g_sources.Name] + '</option>';
}
return ret;
}
}
*/
// END

function people_sort_compare(val1, val2)
{
	if (val1.str.toLowerCase() > val2.str.toLowerCase())
	return 1;
	if (val1.str.toLowerCase() < val2.str.toLowerCase())
	return -1;
	return 0;
}


function list_people_options(who_is_selected)
{
	if (g_people_list_dirty)
	{
		g_sorted_people_array = new Array();
		for (var k = 0; k < g_people.length; ++k)
		{
			var data = g_people[k];
			if (data.deleted || data[g_people.IsSalesperson] != '1')
			continue;
			var tempobj = new Object();
			tempobj.data = data;
			tempobj.index = k;
			tempobj.str = data[g_people.LastName] + ', ' + data[g_people.FirstName];
			if (data[g_people.Level] > 1 && data[g_people.GroupName] && data[g_people.GroupName] != '')
			tempobj.str += ' (' + data[g_people.GroupName] + ')';
			g_sorted_people_array[g_sorted_people_array.length] = tempobj;
		}
		g_sorted_people_array.sort(people_sort_compare);
		g_people_list_dirty = false;
	}

	if (g_sorted_people_array)
	{
		var ret = '';
		for (var k = 0; k < g_sorted_people_array.length; ++k)
		{
			var data = g_sorted_people_array[k].data;
			ret += '<option value="' + data[g_people.PersonID] + '"';
			if (data[g_people.PersonID] == who_is_selected)
			ret += ' selected';
			ret += '>' + g_sorted_people_array[k].str;
			ret += '</option>';
		}
		return ret;
	}
	else
	{
		var ret = '';
		for (var k = 0; k < g_people.length; ++k)
		{
			var data = g_people[k];
			if (data[g_people.IsSalesperson] != '1')
			continue;
			ret += '<option value="' + data[g_people.PersonID] + '"';
			if (data[g_people.PersonID] == who_is_selected)
			ret += ' selected';
			ret += '>' + data[g_people.LastName] + ', ' + data[g_people.FirstName];
			if (data[g_people.Level] > 1 && data[g_people.GroupName] && data[g_people.GroupName] != '')
			ret += ' (' + data[g_people.GroupName] + ')';
			ret += '</option>';
		}
		return ret;
	}
}

function isBadUserID()
{

	var span = document.getElementById('editInfo_span');
	var visible = false;
	if(span)
	visible = (span.className != "EditInfoSpan_Hidden");
	var uid = document.getElementById('editUserID');
	if (visible && (uid.value.length == 0
	|| uid.value == '[newuser]'
	|| uid.value == '[new user]'
	|| uid.value == 'undefined'))
	{
		uid.select();
		uid.focus();
		alert('Please supply a unique User ID');
		return true;
	}
	if(isDuplicateUserID(uid.value)) return true;
	return false;
}

function isNullFirstName()
{
	var visible = false;
	var span = document.getElementById('editInfo_span');
	if(span)
	visible = (span.className != "EditInfoSpan_Hidden");
	var fn = document.getElementById('editFirstName');
	if(visible && (fn.value.length == 0 || fn.value == '[USER]'))
	{
		fn.select();
		fn.focus();
		alert('Please supply a first name');
		return true;
	}
	return false;
}

function isNullLastName()
{
	var visible = false;
	var span = document.getElementById('editInfo_span');
	if(span)
	visible = (span.className != "EditInfoSpan_Hidden");
	var ln = document.getElementById('editLastName');
	if(visible && (ln.value.length == 0 || ln.value == '[NEW]'))
	{
		ln.select();
		ln.focus();
		alert('Please supply a last name');
		return true;
	}
	return false;
}

function isNullEMail()
{
	var visible = false;
	var span = document.getElementById('editInfo_span');
	if(span)
	visible = (span.className != "EditInfoSpan_Hidden");
	var em = document.getElementById('editEmail');
	if(visible && (em.value.length == 0))
	{
		em.select();
		em.focus();
		alert('Please supply an email address');
		return true;
	}
	return false;
}



function isNullPW()
{
	var visible = false;
	var span = document.getElementById('editInfo_span');
	if(span)
	visible = (span.className != "EditInfoSpan_Hidden");
	var pw = document.getElementById('editPassword');
	if(visible && (pw.value.length == 0)
	||pw.value == 'undefined')
	{
		pw.select();
		pw.focus();
		alert('Please supply a password');
		return true;
	}
	return false;
}

function checkUserFlds()
{
	if (isNullFirstName()) return true;

	if (isNullLastName()) return true;

	if (isBadUserID()) return true;

	//if (isNullPW()) return true;

	if (isNullEMail()) return true;

	return false;
}

function dummy_func()
{
	;
}


function onAddPersonClicked(type)
{
	if(checkUserFlds())
	/*
	if (g_newest_sp &&
	(g_newest_sp[g_people.FirstName] == "[USER]" ||
	g_newest_sp[g_people.LastName] == "[NEW]"))
	*/
	{
		alert("Please finish editing the existing new user first before adding another");
		return;
	}
	g_newest_sp = new Array();
	g_newest_sp.temp_id = getTempID();
	g_newest_sp[g_people.PersonID] = g_newest_sp.temp_id;
	g_newest_sp[g_people.FirstName] = "[USER]";
	g_newest_sp[g_people.LastName] = "[NEW]";
	g_newest_sp[g_people.CompanyID] = g_the_only_company[g_company.CompanyID];
	g_newest_sp[g_people.StartDate] = Dates.formatDate(new Date());
	g_newest_sp[g_people.UserID] = "[newuser]";
	g_newest_sp[g_people.PasswordZ] = "";
	g_newest_sp[g_people.Email] = "";
	g_newest_sp[g_people.IsSalesperson] = '0';
	g_newest_sp[g_people.Color] = "808080";
	g_newest_sp[g_people.Level] = '1';
	g_newest_sp[g_people.RatingViewAllowed] = "0";
	g_newest_sp[g_people.SupervisorID] = "-1";
	g_newest_sp[g_people.AverageSalesCycle] = "0";
	g_newest_sp[g_people.LimitSalesCycle] = "0";
	g_newest_sp[g_people.FMThreshold] = g_the_only_company[g_company.FMThreshold];
	g_newest_sp[g_people.IPThreshold] = g_the_only_company[g_company.IPThreshold];
	g_newest_sp[g_people.DPThreshold] = g_the_only_company[g_company.DPThreshold];
	g_newest_sp[g_people.CloseThreshold] = g_the_only_company[g_company.CloseThreshold];
	g_newest_sp[g_people.AnalysisID] = "-2";
	g_newest_sp[g_people.Rating] = "-1";
	g_newest_sp[g_people.ViewSiblings] = "0";
	g_newest_sp[g_people.Deleted] = "0";
	g_newest_sp[g_people.GroupName] = "";
	g_newest_sp[g_people.TrendState] = "0";
	g_newest_sp[g_people.Eula] = "0";
	g_newest_sp[g_people.FMValue] = "0";
	g_newest_sp[g_people.IPValue] = "0";
	g_newest_sp[g_people.DPValue] = "0";
	g_newest_sp[g_people.CloseValue] = "0";
	g_newest_sp[g_people.timezone] = g_the_only_company[g_company.timezone];
	g_newest_sp[g_people.DateCreated] = Dates.formatDate(new Date());
	g_newest_sp[g_people.DateDeleted] = "";
	g_newest_sp[g_people.ShowRatingOnBoard] = "0";
	g_newest_sp[g_people.HideJetstream] = false ;
	g_newest_sp[g_people.MasterAssignee] = false ;
	g_newest_sp[g_people.SpecialPermissionsID] = false ;

	g_newest_sp.added = true;
	chartDirty = true;
	Header.blinkOn('Save');
	g_people[g_people.length] = g_newest_sp;
	var tempnode = null;
	if (type == "ADMIN")
	{
		g_newest_sp[g_people.SupervisorID] = "-3";
		tempnode = g_administratortree.add("[NEW], [USER]", g_people.length - 1);
	}
	else
	{
		g_newest_sp[g_people.SupervisorID] = "-1";
		tempnode = g_unassignedtree.add("[NEW], [USER]", g_people.length - 1);
	}
	tempnode.value = g_newest_sp;
	tempnode.select();
	g_newest_sp.mynode = tempnode;
	var editUserID = document.getElementById("editUserID");
	//	editUserID.select();
	//	editUserID.focus();
	var fn = document.getElementById('editFirstName');
	fn.select();
	fn.focus();
	g_people_list_dirty = true;
}

function hasOpps(personid)
{
	for (var k = 0; k < g_opps.length; ++k)
	{
		if (g_opps[k][g_opps.PersonID] == personid && g_opps[k][g_opps.Category] != '9' && !g_opps[k].TE_deleted)
		{
			return true;
			break;
		}
	}
	return false;
}

function onRemovePersonClicked()
{
	var selecteditem = persontree.getSelectedItem();
	if (!selecteditem)
	{
		alert("Please choose a sales person first");
		return;
	}

	if (hasOpps(selecteditem.value[g_people.PersonID]))
	{
		alert("You must first reassign all of this person's active opportunities");
		return;
	}

	if (confirm("Are you sure you want to remove " + selecteditem.value[g_people.FirstName] + " " + selecteditem.value[g_people.LastName] + "?"))
	{
		for (var k = 0; k < g_people.length; ++k)
		{
			if (g_people[k] == selecteditem.value)
			{
				g_people_list_dirty = true;
				g_people[k].removed = true;
				g_people[k].deleted = true;
				chartDirty = true;
				Header.blinkOn('Save');
				if (g_newest_sp == g_people[k])
				g_newest_sp = null;
				break;
			}
		}
		//Delete login records for removed person
		if (k < g_people.length)
		{
			var PersonID = g_people[k][g_people.PersonID];
			//alert('PersonID = ' + PersonID);
			var c = 0;
			for (var k = 0; k < g_logins.length; ++k)
			{
				if (g_logins[k][g_logins.PersonID] == PersonID)
				{
					g_logins[k].removed = true;
					g_logins[k].deleted = true;
					chartDirty = true;
					++c;
				}
			}
			//Alert('marked ' + c + ' records');
		}
		//g_removedPeople[g_removedPeople.length] = selecteditem.value;
		selecteditem.moveAllItems(g_unassignedtree, true);
		selecteditem.removeMe();
		//g_unassignedtree.flatten();
		document.getElementById("editInfo_span").className = "EditInfoSpan_Hidden";
		document.getElementById("polite_div").className = "EditInfoSpan_Visible";
		g_selectedSp = null;
		document.getElementById("main_spedit_td").vAlign = "center";
	}
}

function addNewAliasOwner(LoginID, PersonID)
{
	var temp = new Array();
	temp.temp_id = getTempID();
	temp[g_alias_owner_xrefs.ID] = temp.temp_id;
	temp[g_alias_owner_xrefs.LoginID] = LoginID;
	temp[g_alias_owner_xrefs.PersonID] = PersonID;
	temp[g_alias_owner_xrefs.CompanyID] = g_the_only_company[g_company.CompanyID];
	temp.added = true;
	g_alias_owner_xrefs[g_alias_owner_xrefs.length] = temp;
}

function onAddProductClicked()
{
	var newproduct = new Array();
	newproduct.temp_id = getTempID();
	newproduct[g_products.ProductID] = newproduct.temp_id;
	newproduct[g_products.Name] = '[New Offering]';
	newproduct[g_products.Deleted] = '0';
	newproduct[g_products.DefaultSalesCycle] = '30';
	newproduct[g_products.CalculatedSalesCycle] = '30';
	newproduct[g_products.ForecastSalesCycle] = '30';
	newproduct[g_products.CompanyID] = g_the_only_company[g_company.CompanyID];
	newproduct[g_products.StandardDeviation] = -1;
	newproduct[g_products.YellowAlertPercentage] = "60";
	newproduct[g_products.RedAlertPercentage] = "80";

	//new fields
	newproduct[g_products.Abbr] = '';
	newproduct[g_products.DefaultValue] = 0;
	newproduct[g_products.DefaultQty] = 1;
	newproduct[g_products.UseDefault] = 1;
	newproduct[g_products.AllowEdit] = 1;

	newproduct.added = true;
	Header.blinkOn('Save');
	g_products[g_products.length] = newproduct;
	var selectProducts = document.getElementById("selectProducts");
	var tempoption = document.createElement("OPTION");
	tempoption.text = newproduct[g_products.Name];
	tempoption.myItem = newproduct;
	if (isNav)
	selectProducts.add(tempoption, (selectProducts.options[0]) ? selectProducts.options[0] : null);
	else
	selectProducts.add(tempoption, 0);
	selectProducts.selectedIndex = 0;
	onChangeProductSelect();
	var editProductName = document.getElementById("editProductName");
	editProductName.select();
	editProductName.focus();
	g_product_list_dirty = true;
}

function onRemoveProductClicked()
{
	var selectProducts = document.getElementById("selectProducts");
	if (selectProducts.selectedIndex < 0)
	return;
	var theproduct = selectProducts.options[selectProducts.selectedIndex].myItem;
	selectProducts.remove(selectProducts.selectedIndex);
	theproduct.removed = true;
	Header.blinkOn('Save');
	document.getElementById("product_edit_td").vAlign = "middle";
	document.getElementById("product_polite_div").className = "PoliteDiv_Visible";
	document.getElementById("editProduct_span").className = "EditInfoSpan_Hidden";
}

function onRemoveSourceClicked()
{
	var selectSources = document.getElementById("selectSources");
	if (selectSources.selectedIndex < 0)
	return;
	var thesource = selectSources.options[selectSources.selectedIndex].myItem;
	selectSources.remove(selectSources.selectedIndex);
	thesource.removed = true;
	Header.blinkOn('Save');
	document.getElementById("source_edit_td").vAlign = "middle";
	document.getElementById("source_polite_div").className = "PoliteDiv_Visible";
	document.getElementById("editSource_span").className = "EditInfoSpan_Hidden";
}


function onAddSourceClicked()
{

	var newsource = new Array();
	newsource.temp_id = getTempID();
	newsource[g_sources.SourceID] = newsource.temp_id;
	newsource[g_sources.CompanyID] = g_the_only_company[g_company.CompanyID];
	newsource[g_sources.Name] = '[New Source]';
	newsource[g_sources.Abbr] = '[Abbrev]';
	newsource[g_sources.Deleted] = '0';
	newsource.added = true;
	Header.blinkOn('Save');
	g_sources[g_sources.length] = newsource;

	var selectSources = document.getElementById("selectSources");
	var tempoption = document.createElement("OPTION");
	tempoption.text = newsource[g_sources.Name];
	tempoption.myItem = newsource;
	if (isNav)
	selectSources.add(tempoption, (selectSources.options[0]) ? selectSources.options[0] : null);
	else
	selectSources.add(tempoption, 0);
	selectSources.selectedIndex = 0;
	onChangeSourceSelect();
	var editSourceName = document.getElementById("editSourceName");
	editSourceName.select();
	editSourceName.focus();
	g_source_list_dirty = true;
}

function onRemoveSource2Clicked()
{
	var selectSources2 = document.getElementById("selectSources2");
	if (selectSources2.selectedIndex < 0)
	return;
	var thesource = selectSources2.options[selectSources2.selectedIndex].myItem;
	selectSources2.remove(selectSources2.selectedIndex);
	thesource.removed = true;
	Header.blinkOn('Save');
	document.getElementById("source2_edit_td").vAlign = "middle";
	document.getElementById("source2_polite_div").className = "PoliteDiv_Visible";
	document.getElementById("editSource2_span").className = "EditInfoSpan_Hidden";
}


function onAddSource2Clicked()
{

	var newsource = new Array();
	newsource.temp_id = getTempID();
	newsource[g_sources2.SourceID] = newsource.temp_id;
	newsource[g_sources2.CompanyID] = g_the_only_company[g_company.CompanyID];
	newsource[g_sources2.Name] = '[Name]';
	newsource[g_sources2.Abbr] = '[Abbrev]';
	newsource[g_sources2.Deleted] = '0';
	newsource.added = true;
	Header.blinkOn('Save');
	g_sources2[g_sources2.length] = newsource;

	var selectSources2 = document.getElementById("selectSources2");
	var tempoption = document.createElement("OPTION");
	tempoption.text = newsource[g_sources2.Name];
	tempoption.myItem = newsource;
	if (isNav)
	selectSources2.add(tempoption, (selectSources2.options[0]) ? selectSources2.options[0] : null);
	else
	selectSources2.add(tempoption, 0);
	selectSources2.selectedIndex = 0;
	onChangeSource2Select();
	var editSource2Name = document.getElementById("editSource2Name");
	editSource2Name.select();
	editSource2Name.focus();
	g_source2_list_dirty = true;
}

function dbg_list_sources()
{
	for (var i = 0; i < g_sources.length; i++)
	{
		var tmp=g_sources[i];
		alert("name=" + tmp[g_sources.Name] + " added: " + tmp.added + " edited: " + tmp.edited);
	}
}

function onChangeProductSelect()
{
	//Before we allow change of select, check that either useDefault or allowEdit is turned on!
	var editProduct_span = document.getElementById("editProduct_span");
	var editUseDefault = document.getElementById("editUseDefault");
	var editAllowEdit = document.getElementById("editAllowEdit");
	var selectProducts = document.getElementById("selectProducts");
	if(editProduct_span.className=='EditInfoSpan_Visible' && !editUseDefault.checked && !editAllowEdit.checked)
	{
		alert('"Use Default" or "Allow Edit" must be checked!');
		if(previousProdIndex > -1) selectProducts.selectedIndex = previousProdIndex;
		return false;
	}
	previousProdIndex = selectProducts.selectedIndex;
	var product_edit_td = document.getElementById("product_edit_td");
	var product_polite_div = document.getElementById("product_polite_div");
	if (selectProducts.selectedIndex < 0)
	{
		product_edit_td.vAlign = "middle";
		product_polite_div.className = "PoliteDiv_Visible";
		editProduct_span.className = "EditInfoSpan_Hidden";
		return true;
	}
	var editProductName = document.getElementById("editProductName");
	var editProductDefaultSalesCycle = document.getElementById("editProductDefaultSalesCycle");
	var editProductYellowAlert = document.getElementById("editProductYellowAlert");
	var editProductRedAlert = document.getElementById("editProductRedAlert");
	var editProductDefaultValue = document.getElementById("editProductDefaultValue");
	var editProductAbbr = document.getElementById("editProductAbbr");
	if(editProduct_span.className=='EditInfoSpan_Visible' && editProductAbbr.value == '')
	{
		alert('You must enter a product abbreviation');
		if(previousProdIndex > -1) selectProducts.selectedIndex = previousProdIndex;
		return false;
	}
	if(editProduct_span.className=='EditInfoSpan_Visible' &&
	editUseDefault.checked && editProductDefaultValue.value == "")
	{
		alert('Since you have checked "Use Default," you must enter a default value.');
		if(previousProdIndex > -1) selectProducts.selectedIndex = previousProdIndex;
		return false;

	}


	var theproduct = selectProducts.options[selectProducts.selectedIndex].myItem;
	editProductName.value = theproduct[g_products.Name];
	editProductDefaultSalesCycle.value = theproduct[g_products.DefaultSalesCycle];
	editProductYellowAlert.value = theproduct[g_products.YellowAlertPercentage];
	editProductRedAlert.value = theproduct[g_products.RedAlertPercentage];
	editProductDefaultValue.value = theproduct[g_products.DefaultValue];
	editProductAbbr.value = theproduct[g_products.Abbr];
	editUseDefault.checked = (theproduct[g_products.UseDefault] == '1');
	editAllowEdit.checked = (theproduct[g_products.AllowEdit] == '1');

	product_edit_td.vAlign = "top";
	product_polite_div.className = "PoliteDiv_Hidden";
	editProduct_span.className = "EditInfoSpan_Visible";
	return true;
}



function sortSelect(elem)
{
	var selected_str = null;
	if (elem.selectedIndex != -1)
	selected_str = elem.options[elem.selectedIndex].text;
	var strarray = new Array();
	var productmap = new Object();
	for (var k = 0; k < elem.options.length; ++k)
	{
		strarray[k] = elem.options[k].text;
		productmap[elem.options[k].text] = elem.options[k].myItem;
	}
	while (elem.options.length > 0)
	elem.remove(0);
	strarray.sort();
	for (var k = 0; k < strarray.length; ++k)
	{
		var tempoption = document.createElement("OPTION");
		tempoption.text = strarray[k];
		tempoption.myItem = productmap[strarray[k]];
		if (isNav)
		elem.add(tempoption, null);
		else
		elem.add(tempoption);
		if (selected_str != null && selected_str == strarray[k])
		elem.selectedIndex = k;
	}
}

function onChangeProductField(elemid, fieldname)
{
	var selectProducts = document.getElementById("selectProducts");
	if (selectProducts.selectedIndex < 0)
	return;
	var editProductName = document.getElementById(elemid);
	var theoption = selectProducts.options[selectProducts.selectedIndex];
	if (fieldname == "Name")
	{
		for (var k = 0; k < g_products.length; ++k)
		{
			if (g_products[k][g_products.Name] == editProductName.value && !g_products[k].removed)
			{
				alert(editProductName.value + " already exists. Please rename this product.");
				editProductName.value = theoption.myItem[g_products.Name];
				editProductName.focus();
				return;
			}
		}
	}
	//	theoption.myItem[g_products[fieldname]] = editProductName.value;
	if(editProductName.type == 'checkbox')
	{
		theoption.myItem[g_products[fieldname]] = (editProductName.checked == true ? '1' : '0');
	}
	else
	theoption.myItem[g_products[fieldname]] = editProductName.value.trim();

	if (fieldname == "Name")
	{
		//		theoption.text = editProductName.value;
		theoption.text = editProductName.value.trim();
		sortSelect(selectProducts);
		g_product_list_dirty = true;
	}
	theoption.myItem.edited = true;
	Header.blinkOn('Save');
}

function onChangeSourceSelect()
{
	var source_edit_td = document.getElementById("source_edit_td");
	var source_polite_div = document.getElementById("source_polite_div");
	var editSource_span = document.getElementById("editSource_span");
	var selectSources = document.getElementById("selectSources");
	if (selectSources.selectedIndex < 0)
	{
		source_edit_td.vAlign = "middle";
		source_polite_div.className = "PoliteDiv_Visible";
		editSource_span.className = "EditInfoSpan_Hidden";
		return;
	}
	var editSourceName = document.getElementById("editSourceName");
	var editSourceAbbr = document.getElementById("editSourceAbbr");


	var thesource = selectSources.options[selectSources.selectedIndex].myItem;
	editSourceName.value = thesource[g_sources.Name];
	editSourceAbbr.value = thesource[g_sources.Abbr];

	source_edit_td.vAlign = "top";
	source_polite_div.className = "PoliteDiv_Hidden";
	editSource_span.className = "EditInfoSpan_Visible";
}

function onChangeSource2Select()
{
	var source2_edit_td = document.getElementById("source2_edit_td");
	var source2_polite_div = document.getElementById("source2_polite_div");
	var editSource2_span = document.getElementById("editSource2_span");
	var selectSources2 = document.getElementById("selectSources2");
	if (selectSources2.selectedIndex < 0)
	{
		source2_edit_td.vAlign = "middle";
		source2_polite_div.className = "PoliteDiv_Visible";
		editSource2_span.className = "EditInfoSpan_Hidden";
		return;
	}
	var editSource2Name = document.getElementById("editSource2Name");
	var editSource2Abbr = document.getElementById("editSource2Abbr");


	var thesource = selectSources2.options[selectSources2.selectedIndex].myItem;
	editSource2Name.value = thesource[g_sources2.Name];
	editSource2Abbr.value = thesource[g_sources2.Abbr];

	source2_edit_td.vAlign = "top";
	source2_polite_div.className = "PoliteDiv_Hidden";
	editSource2_span.className = "EditInfoSpan_Visible";
}

function onChangeSourceField(elemid, fieldname)
{
	var selectSources = document.getElementById("selectSources");
	if (selectSources.selectedIndex < 0)
	return;
	var editSourceName = document.getElementById(elemid);
	var theoption = selectSources.options[selectSources.selectedIndex];
	if (fieldname == "Name")
	{
		for (var k = 0; k < g_sources.length; ++k)
		{
			if (g_sources[k][g_sources.Name] == editSourceName.value && !g_sources[k].removed)
			{
				alert(editSourceName.value + " already exists. Please rename this opportunity source.");
				editSourceName.value = theoption.myItem[g_sources.Name];
				editSourceName.focus();
				return;
			}
		}
	}
	theoption.myItem[g_sources[fieldname]] = editSourceName.value;
	if (fieldname == "Name")
	{
		theoption.text = editSourceName.value;
		sortSelect(selectSources);
		g_source_list_dirty = true;
	}
	theoption.myItem.edited = true;
	Header.blinkOn('Save');
}

function onChangeSource2Field(elemid, fieldname)
{
	var selectSources2 = document.getElementById("selectSources2");
	if (selectSources2.selectedIndex < 0)
	return;
	var editSource2Name = document.getElementById(elemid);
	var theoption = selectSources2.options[selectSources2.selectedIndex];
	if (fieldname == "Name")
	{
		for (var k = 0; k < g_sources2.length; ++k)
		{
			if (g_sources2[k][g_sources2.Name] == editSource2Name.value && !g_sources2[k].removed)
			{
				alert(editSource2Name.value + " already exists. Please rename this opportunity source.");
				editSource2Name.value = theoption.myItem[g_sources2.Name];
				editSource2Name.focus();
				return;
			}
		}
	}
	theoption.myItem[g_sources2[fieldname]] = editSource2Name.value;
	if (fieldname == "Name")
	{
		theoption.text = editSource2Name.value;
		sortSelect(selectSources2);
		g_source2_list_dirty = true;
	}
	theoption.myItem.edited = true;
	Header.blinkOn('Save');
}


function onEditTrend1Return(val)
{
	g_the_only_company[g_company.Requirement1trend] = val;
	g_the_only_company.edited = true;
	Header.blinkOn('Save');
}

function onEditTrend2Return(val)
{
	g_the_only_company[g_company.Requirement2trend] = val;
	g_the_only_company.edited = true;
	Header.blinkOn('Save');
}

function onEditMilestone1TrendText()
{
	Windowing.dropBox.msg = '<b>Edit Trend Text</b>';
	Windowing.dropBox.title = 'Edit Trend Text';
	Windowing.dropBox.initialValue = g_the_only_company[g_company.Requirement1trend];
	Windowing.dropBox.promptCallback = onEditTrend1Return;
	Windowing.dropBox.icon = "../images/edit.gif";
	Windowing.dropBox.escape_text = true;
	Windowing.openSizedPrompt("../shared/prompt_large.html", 580, 580);
}

function onEditMilestone2TrendText()
{
	Windowing.dropBox.msg = '<b>Edit Trend Text</b>';
	Windowing.dropBox.title = 'Edit Trend Text';
	Windowing.dropBox.initialValue = g_the_only_company[g_company.Requirement2trend];
	Windowing.dropBox.promptCallback = onEditTrend2Return;
	Windowing.dropBox.escape_text = true;
	Windowing.openSizedPrompt("../shared/prompt_large.html", 580, 580);
}

function showSPFields(b)
{
	var divSPThreshold = document.getElementById("divSPThreshold");
	var trShowUpperLevelBoard = document.getElementById('trShowUpperLevelBoard');
	var trColor = document.getElementById('trColor');
	var show_color = true;
	if (persontree.getSelectedItem().getDistanceFrom(g_assignedtree) == -1)
	show_color = false;
	if (b)
	{
		divSPThreshold.style.display = "block";
		if (show_color)
		trColor.style.visibility = "visible";
		else
		trColor.style.visibility = "hidden";
	}
	else
	{
		divSPThreshold.style.display = "none";
		trShowUpperLevelBoard.style.display = "none";
		trColor.style.visibility = "hidden";
	}

	if (b && g_selectedSp && g_selectedSp[g_people.Level] > 1)
	trShowUpperLevelBoard.style.display = "block";
	else
	trShowUpperLevelBoard.style.display = "none";
}

function onAddReasonReturn(val)
{
	var selectRemovalReasons = document.getElementById('selectRemovalReasons');
	var newreason = new Array();
	newreason.temp_id = getTempID();
	newreason[g_removereasons.ReasonID] = newreason.temp_id;
	newreason[g_removereasons.Text] = val;
	newreason[g_removereasons.CompanyID] = g_the_only_company[g_company.CompanyID];
	newreason[g_removereasons.Deleted] = 0;
	newreason.added = true;
	Header.blinkOn('Save');
	g_removereasons[g_removereasons.length] = newreason;
	var tempoption = document.createElement("OPTION");
	tempoption.text = val;
	tempoption.myReason = newreason;
	if (isNav)
	selectRemovalReasons.add(tempoption, null);
	else
	selectRemovalReasons.add(tempoption);
}

function onClickAddReason()
{
	Windowing.dropBox.msg = 'Please enter the new removal reason:';
	Windowing.dropBox.title = 'New Removal Reason';
	Windowing.dropBox.promptCallback = onAddReasonReturn;
	Windowing.dropBox.initialValue = "";
	Windowing.openSizedPrompt("../shared/prompt.html", 130, 550);
}

function onEditReasonReturn(val)
{
	var selectRemovalReasons = document.getElementById('selectRemovalReasons');
	var theoption = selectRemovalReasons.options[edit_reason_index];
	theoption.text = val;
	theoption.myReason[g_removereasons.Text] = val;
	theoption.myReason.edited = true;
	Header.blinkOn('Save');
}

function onClickEditReason()
{
	var selectRemovalReasons = document.getElementById('selectRemovalReasons');
	if (selectRemovalReasons.selectedIndex < 0)
	return;
	var thereason = selectRemovalReasons.options[selectRemovalReasons.selectedIndex].myReason;
	edit_reason_index = selectRemovalReasons.selectedIndex;
	Windowing.dropBox.msg = 'Please make changes to the removal reason:';
	Windowing.dropBox.title = 'Edit Removal Reason';
	Windowing.dropBox.promptCallback = onEditReasonReturn;
	Windowing.dropBox.initialValue = thereason[g_removereasons.Text];
	Windowing.openSizedPrompt("../shared/prompt.html", 130, 550);
}

function onClickRemoveReason()
{
	var selectRemovalReasons = document.getElementById('selectRemovalReasons');
	if (selectRemovalReasons.selectedIndex < 0)
	return;
	var thereason = selectRemovalReasons.options[selectRemovalReasons.selectedIndex].myReason;
	selectRemovalReasons.remove(selectRemovalReasons.selectedIndex);
	thereason.removed = true;
	Header.blinkOn('Save');
}

function onChangeSelectPeriodLength()
{
	var selectPeriodLength = document.getElementById("selectPeriodLength");
	if (selectPeriodLength.selectedIndex < 0)
	return;
	g_the_only_company[g_company.ClosePeriod] = selectPeriodLength.value;
	g_the_only_company.edited = true;
	Header.blinkOn('Save');
}

//--------------------- Configurable Milestones & Categories----------------------------------
function getLabelRec(ID)
{
	for (var i = 0; i < g_category_labels.length; i++)
	{
		if(ID == g_category_labels[i][g_category_labels.ID]) return g_category_labels[i];
	}
	return null;
}

function setCompanyCategoryLabel(id, indLabel, indAbbr)
{
	//First option is the default!
	var drop = document.getElementById(id);
	if (drop.selectedIndex < 1)
	{
		g_the_only_company[indLabel] = '';
		g_the_only_company[indAbbr] = '';
		g_the_only_company.edited = true;
		Header.blinkOn('Save');
		return;
	}
	var idRec = drop.options[drop.selectedIndex].value;
	var catRec = getLabelRec(idRec);
	g_the_only_company[indLabel] = catRec[g_category_labels.Label];
	g_the_only_company[indAbbr] = catRec[g_category_labels.Abbr];
	g_the_only_company.edited = true;
	Header.blinkOn('Save');

}



function fillCatDropdowns()
{
	var cbT = document.getElementById('cat_t_select');

	var cbFM = document.getElementById('cat_fm_select');
	var cbIP = document.getElementById('cat_ip_select');
	var cbDP = document.getElementById('cat_dp_select');
	var cbC = document.getElementById('cat_c_select');
	var cbR = document.getElementById('cat_r_select');
	var cbS = document.getElementById('cat_s_select');

	var d = document.createElement("OPTION");
	cbT.options[cbT.options.length] = d;
	d.value = -1;
	d.text = "Target";
	var d = document.createElement("OPTION");
	cbFM.options[cbFM.options.length] = d;
	d.value = -1;
	d.text = "First Meeting";
	var d = document.createElement("OPTION");
	cbIP.options[cbIP.options.length] = d;
	d.value = -1;
	d.text = "Information Phase";
	var d = document.createElement("OPTION");
	cbDP.options[cbDP.options.length] = d;
	d.value = -1;
	d.text = "Decision Point";
	var d = document.createElement("OPTION");
	cbC.options[cbC.options.length] = d;
	d.value = -1;
	d.text = "Closed";
	var d = document.createElement("OPTION");
	cbR.options[cbR.options.length] = d;
	d.value = -1;
	d.text = "Removed";
	var d = document.createElement("OPTION");
	cbS.options[cbS.options.length] = d;
	d.value = -1;
	d.text = "Stalled";

	for (var i=0; i<g_category_labels.length; i++)
	{
		var o = document.createElement("OPTION");
		var bSel = false;
		var Label = g_category_labels[i][g_category_labels.Label];
		switch(g_category_labels[i][g_category_labels.CategoryID])
		{
			case '1':
			cbFM.options[cbFM.options.length] = o;
			if (Label == g_the_only_company[g_company.FMLabel]) bSel = true;
			break;
			case '2':
			cbIP.options[cbIP.options.length] = o;
			if (Label == g_the_only_company[g_company.IPLabel]) bSel = true;
			break;
			case '4':
			cbDP.options[cbDP.options.length] = o;
			if (Label == g_the_only_company[g_company.DPLabel]) bSel = true;
			break;
			case '6':
			cbC.options[cbC.options.length] = o;
			if (Label == g_the_only_company[g_company.CLabel]) bSel = true;
			break;
			case '7':
			cbS.options[cbS.options.length] = o;
			if (Label == g_the_only_company[g_company.SLabel]) bSel = true;
			break;
			case '9':
			cbR.options[cbR.options.length] = o;
			if (Label == g_the_only_company[g_company.RLabel]) bSel = true;
			break;
			case '10':
			cbT.options[cbT.options.length] = o;
			if (Label == g_the_only_company[g_company.TLabel]) bSel = true;
			break;
		}
		//		o.value = Label;
		o.value = g_category_labels[i][g_category_labels.ID];
		o.text = Label;
		o.selected = bSel;
	}
}

function fillMSDropdowns()
{

	var cbPerson = document.getElementById('ms_person_select');
	var cbNeed = document.getElementById('ms_need_select');
	var cbMoney = document.getElementById('ms_money_select');
	var cbTime = document.getElementById('ms_time_select');

	var d = document.createElement("OPTION");
	cbPerson.options[cbPerson.options.length] = d;
	d.value = "Person";
	d.text = "Person";
	var d = document.createElement("OPTION");
	cbNeed.options[cbNeed.options.length] = d;
	d.value = "Need";
	d.text = "Need";
	var d = document.createElement("OPTION");
	cbMoney.options[cbMoney.options.length] = d;
	d.value = "Money";
	d.text = "Money";
	var d = document.createElement("OPTION");
	cbTime.options[cbTime.options.length] = d;
	d.value = "Time";
	d.text = "Time";
	for (var i=0; i<g_milestone_labels.length; i++)
	{
		var o = document.createElement("OPTION");
		var bSel = false;
		var Label = g_milestone_labels[i][g_milestone_labels.Label];
		switch(g_milestone_labels[i][g_milestone_labels.MilestoneID])
		{
			case '2':
			cbPerson.options[cbPerson.options.length] = o;
			if (Label == g_the_only_company[g_company.MS2Label]) bSel = true;
			break;
			case '3':
			cbNeed.options[cbNeed.options.length] = o;
			if (Label == g_the_only_company[g_company.MS3Label]) bSel = true;
			break;
			case '4':
			cbMoney.options[cbMoney.options.length] = o;
			if (Label == g_the_only_company[g_company.MS4Label]) bSel = true;
			break;
			case '5':
			cbTime.options[cbTime.options.length] = o;
			if (Label == g_the_only_company[g_company.MS5Label]) bSel = true;
			break;
		}
		//		o.value = g_milestone_labels[i][g_milestone_labels.ID];
		o.value = Label;

		o.text = Label;
		o.selected = bSel;
	}

}

//---------------------------------------------------------------------------------

function onAssignClicked()
{
	if(checkUserFlds()) return;

	var selecteditem = persontree.getSelectedItem();
	if (selecteditem.m_parent.value == -5)
	{
		alert("Administrators cannot be assigned");
		return;
	}
	if (!selecteditem)
	{
		alert("Please choose a person first");
		return;
	}
	if (g_selectedSp && g_selectedSp.mynode.getDistanceFrom(g_assignedtree) != -1 && !verifyColors(g_selectedSp))
	{
		alert("The color of the currently selected person is already taken in the group. Please choose a new color before assigning.");
		show_color_picker();
		return;
	}
	g_assignee = selecteditem.value;
	Windowing.openSizedPrompt("people_tree_prompt.html", 480, 360);
}

function onUnassignClicked()
{
	var selecteditem = persontree.getSelectedItem();
	if (!selecteditem)
	{
		alert("Please choose a person first");
		return;
	}
	if (selecteditem.getDistanceFrom(g_assignedtree) != -1)
	selecteditem.move(g_unassignedtree);
}

function assign_callback(supervisor)
{
	g_assignee.edited = true;
	Header.blinkOn('Save');
	if (supervisor == -4)
	g_assignee.mynode.move(g_assignedtree);
	else
	g_assignee.mynode.move(supervisor.mynode);
	g_assignee.mynode.select();
}

function updateOppsPerson(oldid, newid)
{
	for (var k = 0; k < g_opportunities.length; ++k)
	{
		if (g_opportunities[k][g_opportunities.PersonID] == oldid)
		g_opportunities[k][g_opportunities.PersonID] = newid;
	}
}

function updateOppsProduct(oldid, newid)
{
	for (var k = 0; k < g_opportunities.length; ++k)
	{
		if (g_opportunities[k][g_opportunities.ProductID] == oldid)
		g_opportunities[k][g_opportunities.ProductID] = newid;
	}
}

function updateOppIDs(oldid, newid)
{
	for (var k = 0; k < g_opportunities.length; ++k)
	{
		if (g_opportunities[k][g_opportunities.DealID] == oldid)
		g_opportunities[k][g_opportunities.DealID] = newid;
	}
	for (var k = 0; k < g_editedSubAnswers.length; ++k)
	{
		if (g_editedSubAnswers[k][0] == oldid)
		g_editedSubAnswers[k][0] = newid;
	}
	for (var k = 0; k < g_opp_products_xref.length; ++k)
	{
		var thisRec = g_opp_products_xref[k][g_opp_products_xref.DealID];
		if (g_opp_products_xref[k][g_opp_products_xref.DealID] == oldid)
		{
			g_opp_products_xref[k][g_opp_products_xref.DealID] = newid;
		}

	}
}

function updateLoginIDs(oldid, newid)
{
	for (var i=0; i<g_logins.length; ++i)
	{
		if (g_logins[i][g_logins.PersonID] == oldid)
		g_logins[i][g_logins.PersonID] = newid;
		if (g_logins[i][g_logins.LoginID] == oldid)
		g_logins[i][g_logins.LoginID] = newid;
	}
}

function updatePersonIDs(oldid, newid)
{
	for (var k = 0; k < g_people.length; ++k)
	{
		if (g_people[k][g_people.PersonID] == oldid)
		g_people[k][g_people.PersonID] = newid;
		if (g_people[k][g_people.SupervisorID] == oldid)
		g_people[k][g_people.SupervisorID] = newid;
	}
}

function updateSubIDs(oldid, newid)
{
	for (var k = 0; k < g_submilestones.length; ++k)
	{
		if (g_submilestones[k][g_submilestones.SubID] == oldid)
		g_submilestones[k][g_submilestones.SubID] = newid;
	}
	for (var k = 0; k < g_editedSubAnswers.length; ++k)
	{
		if (g_editedSubAnswers[k][1] == oldid)
		g_editedSubAnswers[k][1] = newid;
	}
}

function updateAliasOwnerLoginIDs(oldid, newid)
{
	for (var k = 0; k < g_alias_owner_xrefs.length; ++k)
	{
		if (g_alias_owner_xrefs[k][g_alias_owner_xrefs.LoginID] == oldid)
		g_alias_owner_xrefs[k][g_alias_owner_xrefs.LoginID] = newid;
	}
}

function updateAliasOwnerPersonIDs(oldid, newid)
{
	for (var k = 0; k < g_alias_owner_xrefs.length; ++k)
	{
		if (g_alias_owner_xrefs[k][g_alias_owner_xrefs.PersonID] == oldid)
		g_alias_owner_xrefs[k][g_alias_owner_xrefs.PersonID] = newid;
	}
}

function doneSaving(result)
{
	if (result.has_error)
	alert('The save request returned the following:\n' + result);
	this.result = result;
	g_continueSaving = true;
	if (this.get_id)
	{
		this.m_data_array[this.id_index] = result;
		if (this.globalArray == g_people)
		{
			updateOppsPerson(this.m_data_array.temp_id, result);
			updatePersonIDs(this.m_data_array.temp_id, result);
			updateLoginIDs(this.m_data_array.temp_id, result);
			updateAliasOwnerPersonIDs(this.m_data_array.temp_id, result);
		}
		else if (this.globalArray == g_products)
		updateOppsProduct(this.m_data_array.temp_id, result);
		else if (this.globalArray == g_opportunities)
		updateOppIDs(this.m_data_array.temp_id, result);
		else if (this.globalArray == g_submilestones)
		updateSubIDs(this.m_data_array.temp_id, result);
		else if (this.globalArray == g_logins)
		updateAliasOwnerLoginIDs(this.m_data_array.temp_id, result);
	}
	if (this.fake_saver == g_saveQueue[g_saveQueue.length - 1])
	{
		Header.enableButton("Save");
		Header.blinkOff('Save');
		g_saver.close();
		g_doing_save = false;

		//alert("Save done; about to refresh");
		if(1/*g_reload_after_save*/)
		{
			window.location.reload();
		}
		g_reload_after_save = false;

	}
}

function processSaveQueue()
{
	if (g_continueSaving)
	{
		g_continueSaving = false;
		var tempobj = g_saveQueue[g_saveQueueIndex];
		if (tempobj.m_data)
		g_saver.m_data = tempobj.m_data.toPipes();
		else if (tempobj.m_data_multi)
		{
			g_saver.m_data = '';
			for (var k = 0; k < tempobj.m_data_multi.length; ++k)
			{
				if (k != 0)
				g_saver.m_data += '|';
				g_saver.m_data += tempobj.m_data_multi[k].toPipes();
			}
		}

		g_saver.m_url = tempobj.m_url;


		g_saver.m_params = tempobj.m_params;
		g_saver.m_callback = doneSaving;
		

		if (tempobj.get_id)
		g_saver.get_id = tempobj.get_id;
		else
		g_saver.get_id = null;

		if (tempobj.id_index)
		g_saver.id_index = tempobj.id_index;
		else
		g_saver.id_index = null;

		if (tempobj.m_data_array)
		g_saver.m_data_array = tempobj.m_data_array;
		else
		g_saver.m_data_array = null;

		if (tempobj.globalArray)
		g_saver.globalArray = tempobj.globalArray;
		else
		g_saver.globalArray = null;

		g_saver.fake_saver = tempobj;
		g_saver.save();
		++g_saveQueueIndex;
		if (g_saveQueueIndex == g_saveQueue.length)
		return;
	}
	setTimeout("processSaveQueue();", 1);
}

function FakeSaver()
{
	this.m_params = new Object();
}

function addPeopleChangesToSaveQueue(globalArray, update_filename, insert_filename, remove_filename, remove_table, remove_field, id_index, cur_node)
{
	var do_deletes = false;
	if (!cur_node)
	{
		cur_node = persontree;
		do_deletes = true;
	}
	var update_array = new Array();
	for (var k = 0; k < cur_node.getItemCount(); ++k)
	{
		var tempitem = cur_node.getItem(k);
		var temparray = tempitem.value;
		if (temparray != -3 && temparray != -4 && temparray != -5)
		{
			if (temparray && temparray.added && insert_filename)
			{
				var oSaver = new FakeSaver();
				oSaver.m_data = temparray;
				oSaver.m_callback = doneSaving;
				oSaver.m_url = g_dataurl + insert_filename;
				oSaver.m_params.mpower_companyid = g_the_only_company[g_company.CompanyID];
				oSaver.m_params.mpower_fieldlist = globalArray._fieldlist;
				if (id_index || id_index === 0)
				{
					oSaver.get_id = true;
					oSaver.id_index = id_index;
					oSaver.m_data_array = temparray;
					oSaver.globalArray = globalArray;
				}
				temparray.added = false;
				temparray.edited = false;
				temparray.removed = false;
				g_saveQueue[g_saveQueue.length] = oSaver;
			}
			else if (temparray && temparray.edited && update_filename)
			{
				update_array[update_array.length] = temparray;
				temparray.added = false;
				temparray.edited = false;
				temparray.removed = false;
			}
		}
	}

	for (var n = 0; n < cur_node.getItemCount(); ++n)
	addPeopleChangesToSaveQueue(globalArray, update_filename, insert_filename, remove_filename, remove_table, remove_field, id_index, cur_node.getItem(n))

	if (update_array.length > 0)
	{
		var oSaver = new FakeSaver();
		oSaver.m_data = null;
		oSaver.m_data_multi = update_array;
		oSaver.m_callback = doneSaving;
		oSaver.m_url = g_dataurl + update_filename;
		oSaver.m_params.mpower_companyid = g_the_only_company[g_company.CompanyID];
		oSaver.m_params.mpower_fieldlist = globalArray._fieldlist;
		g_saveQueue[g_saveQueue.length] = oSaver;
	}

	if (do_deletes)
	{
		for (var k = 0; k < globalArray.length; ++k)
		{
			var temparray = globalArray[k];
			if (temparray && temparray.removed && remove_filename)
			{
				var oSaver = new FakeSaver();
				oSaver.m_callback = doneSaving;
				oSaver.m_data = null;
				oSaver.m_url = g_dataurl + remove_filename;
				oSaver.m_params.mpower_companyid = g_the_only_company[g_company.CompanyID];
				oSaver.m_params.mpower_remove_value = temparray[globalArray[remove_field]];
				oSaver.m_params.mpower_remove_field = remove_field;
				oSaver.m_params.mpower_remove_table = remove_table;
				temparray.added = false;
				temparray.edited = false;
				temparray.removed = false;
				g_saveQueue[g_saveQueue.length] = oSaver;
			}
		}
	}
}

function addChangesToSaveQueue(globalArray, update_filename, insert_filename, remove_filename, remove_table, remove_field, id_index)
{
	var update_array = new Array();
	for (var k = 0; k < globalArray.length; ++k)
	{
		var temparray = globalArray[k];
		if (temparray && temparray.removed && remove_filename)
		{
			var oSaver = new FakeSaver();
			oSaver.m_callback = doneSaving;
			oSaver.m_data = null;
			oSaver.m_url = g_dataurl + remove_filename;
			oSaver.m_params.mpower_companyid = g_the_only_company[g_company.CompanyID];
			oSaver.m_params.mpower_remove_value = temparray[globalArray[remove_field]];
			oSaver.m_params.mpower_remove_field = remove_field;
			oSaver.m_params.mpower_remove_table = remove_table;
			temparray.added = false;
			temparray.edited = false;
			temparray.removed = false;
			g_saveQueue[g_saveQueue.length] = oSaver;
		}
		else if (temparray && temparray.added && insert_filename)
		{
			var oSaver = new FakeSaver();
			oSaver.m_data = temparray;
			oSaver.m_callback = doneSaving;
			oSaver.m_url = g_dataurl + insert_filename;
			oSaver.m_params.mpower_companyid = g_the_only_company[g_company.CompanyID];
			oSaver.m_params.mpower_fieldlist = globalArray._fieldlist;
			if (id_index || id_index === 0)
			{
				oSaver.get_id = true;
				oSaver.id_index = id_index;
				oSaver.m_data_array = temparray;
				oSaver.globalArray = globalArray;
			}
			temparray.added = false;
			temparray.edited = false;
			temparray.removed = false;
			g_saveQueue[g_saveQueue.length] = oSaver;
		}
		else if (temparray && temparray.edited && update_filename)
		{
			update_array[update_array.length] = temparray;
			temparray.added = false;
			temparray.edited = false;
			temparray.removed = false;
		}
	}

	if (update_array.length > 0)
	{
		var oSaver = new FakeSaver();
		oSaver.m_data = null;
		oSaver.m_data_multi = update_array;
		oSaver.m_callback = doneSaving;
		oSaver.m_url = g_dataurl + update_filename;
		oSaver.m_params.mpower_companyid = g_the_only_company[g_company.CompanyID];
		oSaver.m_params.mpower_fieldlist = globalArray._fieldlist;
		g_saveQueue[g_saveQueue.length] = oSaver;
	}
}

function addSubAnswersToSaveQueue()
{
	for (var k = 0; k < g_editedSubAnswers.length; ++k)
	{
		if (g_editedSubAnswers[k].i_am_happy)
		continue;
		var oSaver = new FakeSaver();
		oSaver.m_data = g_editedSubAnswers[k];
		oSaver.m_callback = doneSaving;
		oSaver.m_url = g_dataurl + "set_subanswer_admin.php";
		oSaver.m_params.mpower_companyid = g_the_only_company[g_company.CompanyID];
		g_saveQueue[g_saveQueue.length] = oSaver;
		g_editedSubAnswers[k].i_am_happy = true;
	}
}

function check_levels(the_node, the_results)
{
	if (!the_node)
	the_node = g_assignedtree;
	var item_count = the_node.getItemCount();
	if (!the_results)
	{
		the_results = new Object()
		the_results.too_deep_warning = false;
		the_results.too_shallow_warning = false;
	}
	for (var k = 0; k < item_count; ++k)
	{
		var tempitem = the_node.getItem(k);
		var tempperson = tempitem.value;
		if (tempperson[g_people.Level] < 1)
		the_results.too_deep_warning = true;
		else if (tempperson[g_people.Level] > 1 && tempitem.getItemCount() == 0)
		the_results.too_shallow_warning = true;
		check_levels(tempitem, the_results);
	}
	return the_results;
}

function do_save(bDebug)
{
	if (!Header.isBlinking('Save'))
	return;
	if (g_selectedSp)
	{
		//if (isDuplicateUserID() || isDuplicateName())
		//		if (isDuplicateUserID() || isNullPW() || isBadUserID())
		if (checkUserFlds())
		return;
		if (g_selectedSp && g_selectedSp.mynode.getDistanceFrom(g_assignedtree) != -1 && !verifyColors(g_selectedSp))
		{
			alert("The color of the currently selected person is already taken in the group. Please choose a new color before assigning.");
			show_color_picker();
			return;
		}
	}
	if (!check_start_date())
	return;
	if (!checkCompanyTab())
	return;
	g_saver = new Saver();

	g_saver.open();
	g_doing_save = true;
	g_saveQueueIndex = 0;
	g_saveQueue = new Array();
	g_continueSaving = true;
	if (bDebug)
	Saver.DEBUG = true;
	else
	Saver.DEBUG = false;
	var temparray = null;
	addChangesToSaveQueue(g_company, "update_company.php");
	addPeopleChangesToSaveQueue(g_people, "update_people.php", "insert_person.php", "remove_person.php", "people", "PersonID", g_people.PersonID);
	addChangesToSaveQueue(g_products, "update_products.php", "insert_products.php", "remove_product.php", "products", "ProductID", g_products.ProductID);
	addChangesToSaveQueue(g_removereasons, "update_removereasons.php", "insert_removereason.php", "remove_removereason.php", "removereasons", "ReasonID", g_removereasons.ReasonID);
	addChangesToSaveQueue(g_opportunities, "update_opportunities.php", "insert_opportunity.php", "remove_row.php", "opportunities", "DealID", g_opportunities.DealID);
	addChangesToSaveQueue(g_levels, "update_levels.php");
	addChangesToSaveQueue(g_close_questions, "update_close_questions.php");
	addChangesToSaveQueue(g_person_questions, "update_close_questions.php");
	addChangesToSaveQueue(g_need_questions, "update_close_questions.php");
	addChangesToSaveQueue(g_money_questions, "update_close_questions.php");
	addChangesToSaveQueue(g_time_questions, "update_close_questions.php");
	addChangesToSaveQueue(g_req1_questions, "update_close_questions.php");
	addChangesToSaveQueue(g_req2_questions, "update_close_questions.php");
	addChangesToSaveQueue(g_submilestones, "update_submilestones.php", "insert_submilestone.php", "remove_row.php", "SubMilestones", "SubID", g_submilestones.SubID);
	addChangesToSaveQueue(g_msAnalysisQuestions, "update_msanalysisquestions.php", "insert_msanalysisquestion.php", "remove_row.php", "MSAnalysisQuestions", "SubID");
	addChangesToSaveQueue(g_valuations, "update_valuations.php","insert_valuation.php", "remove_row.php", "Valuations","VLevel",g_valuations.VLevel);
	addChangesToSaveQueue(g_sources, "update_sources.php", "insert_sources.php", "remove_source.php", "sources", "SourceID", g_sources.SourceID);
	addChangesToSaveQueue(g_sources2, "update_sources2.php", "insert_sources2.php", "remove_source2.php", "sources2", "Source2ID", g_sources2.Source2ID);
	addChangesToSaveQueue(g_logins, "update_logins.php", "insert_login.php", "remove_login.php", "logins", "ID", g_logins.ID);
	//	addChangesToSaveQueue(g_reports_company_xrefs, "update_comp_rpt_xrefs.php", "insert_comp_rpt_xref.php", "remove_comp_rpt_xrefs.php", "CompanyReportsXRef", "ID", g_reports_company_xrefs.ID);
	addChangesToSaveQueue(g_reports_company_xrefs, "update_comp_rpt_xrefs.php", "insert_comp_rpt_xref.php", "remove_comp_rpt_xrefs.php", "CompanyReportsXRef", "ID");

	if (g_subMSEnabled)
	addSubAnswersToSaveQueue();
	addChangesToSaveQueue(g_opp_products_xref, "update_opp_prod_xrefs.php", "insert_opp_prod_xref.php", "remove_opp_prod_xrefs.php", "Opp_Product_XRef", "ID", g_opp_products_xref.ID);
	addChangesToSaveQueue(g_alias_owner_xrefs, "update_alias_owner_xrefs.php", "insert_alias_owner_xref.php", "remove_alias_owner_xrefs.php", "Alias2OwnerXRef", "ID", g_alias_owner_xrefs.ID);

	if (g_saveQueue.length > 0)
	{
		Header.disableButton("Save");
		processSaveQueue();
	}

}

function poll_callback(need_refresh)
{
	if (need_refresh)
	{
		if (ignore_next)
		{
			ignore_next = false;
			return;
		}
		var do_refresh = confirm("Newer data is available. Would you like to refresh?");
		if (do_refresh)
		window.location.reload();
		else
		ignore_next = true;
	}
}

function do_poll()
{
	poll_changes('../data/poll_changes.php', window.g_data_birthdate, g_the_only_company[g_company.CompanyID], null, 'admin', poll_callback);
}

function showFirstPage()
{
	opTable.showPage(0);
}

function showPrevPage()
{
	opTable.showPage(opTable.getCurPage() - 1);
}

function showNextPage()
{
	opTable.showPage(opTable.getCurPage() + 1);
}

function showLastPage()
{
	opTable.showPage(opTable.getPageCount() - 1);
}

function export_answers()
{
	//window.open('../data/export_answers.php');
	window.location.href = '../data/export_answers.php';
}


function onAddAliasClicked()
{
	var hash = '#_-1';
	Windowing.openSizedPrompt("edit_login.html" + hash, 300, 550);
}

function onEditAliasClicked()
{
	var sel = document.getElementById('selectAliasList');
	if(sel.selectedIndex < 0)
	{
		alert('Please select a name to edit!');
		return;
	}
	//	var hash = '#_' + sel.options[sel.selectedIndex].myData[g_logins.LoginID];
	var hash = '#_' + sel.options[sel.selectedIndex].myData[g_logins.UserID];

	Windowing.openSizedPrompt("edit_login.html" + hash, 300, 550);

}

function onRemoveAliasClicked()
{
	var sel = document.getElementById('selectAliasList');
	if(sel.selectedIndex < 0)
	{
		alert('Please select a name to delete!');
		return;
	}
	if (confirm('Are you sure you want to delete this alias?'))
	{
		var aliasdata = sel.options[sel.selectedIndex].myData;
		aliasdata.removed = true;
		for (var k = 0; k < g_alias_owner_xrefs.length; ++k)
		{
			if (g_alias_owner_xrefs[k][g_alias_owner_xrefs.LoginID] == aliasdata[g_logins.ID])
			g_alias_owner_xrefs[k].removed = true;
		}
		sel.options.remove(sel.selectedIndex);
		Header.blinkOn('Save');
	}
}

function updateLoginArray(aliasRec)
{
	Header.blinkOn('Save');
	if (aliasRec.i_am_new)
	{
		aliasRec.added = true;
		aliasRec.i_am_new = false;
		g_logins[g_logins.length] = aliasRec;
		fillAliasList();
	}
	else
	{
		aliasRec.edited = true;
	}
}

function getProdAbbr(id)
{
	var abbr = "Unk.";
	for(var i=0; i<g_products.length; i++)
	{
		if(id == g_products[i][g_products.ProductID])
		{
			abbr = g_products[i][g_products.Abbr];
			break;
		}
	}
	return abbr;
}


/*
function setCompanyCategoryLabel(id, indLabel, indAbbr)
{
//First option is the default!
var ind = document.getElementById(id).selectedIndex;
if (ind < 1)
{
g_the_only_company[indLabel] = '';
g_the_only_company[indAbbr] = '';
g_the_only_company.edited = true;
Header.blinkOn('Save');
return;
}
ind--;
g_the_only_company[indLabel] = g_category_labels[ind][g_category_labels.Label];;
g_the_only_company[indAbbr] = g_category_labels[ind][g_category_labels.Abbr];;
g_the_only_company.edited = true;
Header.blinkOn('Save');

}
*/

function showHideBillPrompt()
{
	var show = 'none';
	if(document.getElementById('checkBillingUsed').checked == true)
	show = 'inline';
	document.getElementById('trBillPrompt').style.display = show;
	document.getElementById('trViewExpected').style.display = show;
	if(show == 'inline')
	{
		var txtPrompt = document.getElementById('textBillingPrompt');
		if(txtPrompt.value == '')
		txtPrompt.value = 'Billing Date:';
	}

}

function onbeforeunload()
{
	if (Header.isBlinking('Save'))
	{
		event.returnValue = "Do you really want to leave without saving?";
	}
}

function doAliasReassign()
{
	var selectAliasList = document.getElementById('selectAliasList');
	if (selectAliasList.selectedIndex < 0)
	{
		alert("No is alias selected.");
		return;
	}
	var logindata = selectAliasList.options[selectAliasList.selectedIndex].myData;
	//alert(logindata[g_logins.Name]);
	g_assignee = logindata;
	Windowing.openSizedPrompt("alias_tree_prompt.html", 480, 360);
}

function alias_assign_callback(val)
{
	g_assignee[g_logins.PersonID] = val[g_people.PersonID];
	g_assignee.edited = true;
	Header.blinkOn('Save');
	fillAliasList();
}

function onCreateDemo()
{
	answer = window.confirm("This operation may take a long time. Are you sure you want to do this?");
	if(answer)
	{
		Windowing.openSizedWindow('createdemo.php', 400, 400);
	}
}

function onUpdateDemo()
{
	answer = window.confirm("This operation may take a long time. Are you sure you want to do this?");
	if(answer)
	{
		Windowing.openSizedWindow('updatedemo.php?weeks=' + editNumWeeksUpdate.value, 400, 400);
	}
}


function logoutAdmin()
{	
	window.location='../../logout.php';	
}


// forecast assistant js code


function setMonthEndDate() {
	
	try {
		var year = 	document.getElementById('year').value;
	} catch(e) {
		var d = new Date();
		var year = d.getFullYear();
	}
	
	document.getElementById('SeasonalityCheck').checked = false;
	var seasonality_checked = false;
	if (g_seasonality.length > 0) {		
		for (var k = 0; k < g_seasonality.length; ++k) {			
			var current_year = g_seasonality[k][g_seasonality.Year];
			var seasonality_enabled  = g_seasonality[k][g_seasonality.SeasonalityEnabled ];			
			if (year == current_year && seasonality_enabled == 1) {
				document.getElementById('SeasonalityCheck').checked = true;
				seasonality_checked = true;
			}
		}
	}

	// Clear all the fields
	document.getElementById('FiscalYearEndDate').value = '';	
	var id = 1;
	
	if(!seasonality_checked) document.getElementById('seasonality_total').style.display = 'none';
	else document.getElementById('seasonality_total').style.display = '';
	
	
	while(document.getElementById('PeriodEndDate_' + id)) {
		document.getElementById('PeriodEndDate_' + id).value = '';
		document.getElementById('PeriodLabel_' + id).value = '';
		document.getElementById('seasonality_' + id).value = '';
		if(!seasonality_checked) document.getElementById('seasonality_' + id).style.display = 'none';
		else document.getElementById('seasonality_' + id).style.display = '';
		id++
	}
	
	var months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December', '');
	if (g_fiscal_year.length > 0) {
		
		for (var k = 0; k < g_fiscal_year.length; ++k)
		{
			var date = g_fiscal_year[k][g_fiscal_year.PeriodEndDate];
			var label = g_fiscal_year[k][g_fiscal_year.PeriodLabel];
			var period = g_fiscal_year[k][g_fiscal_year.Period];
			var seasonality = g_fiscal_year[k][g_fiscal_year.Seasonality];
			var current_year = g_fiscal_year[k][g_fiscal_year.Year];			
			var fiscal_year_end_date = g_fiscal_year[k][g_fiscal_year.FiscalYearEndDate]
			
			if (year == current_year) {
				if (period == '') {					
					document.getElementById('FiscalYearEndDate').value = fiscal_year_end_date;
				} else {				
					document.getElementById('PeriodEndDate_' + period).value = date;
					
					if (label == '') {
						var monthid = parseInt(period, 10) - 1;						
						document.getElementById('PeriodLabel_' + period).value = months[monthid];
					}
					else {
						document.getElementById('PeriodLabel_' + period).value = label;
					}
					
					document.getElementById('seasonality_' + period).value = seasonality;
					
					if (period == 13) {
						if (date != '') document.getElementById('isExtraPeriodEnabled').checked = true;
						else document.getElementById('isExtraPeriodEnabled').checked = false;
						checkPeriod();
					} 
					
				}
			}
		}
		
	}
	calculateSeasonality();
}

function addFiscalYear() {
	
	var len = g_fiscal_year.length;
	var year = document.getElementById('year').value;
	var fiscal_year_end = document.getElementById('FiscalYearEndDate').value;
	g_fiscal_year.push(new Array(year, fiscal_year_end, '', '', '', ''));	

	var id = 1;
	while(document.getElementById('PeriodEndDate_' + id)) {
		var date = document.getElementById('PeriodEndDate_' + id).value;
		var label = document.getElementById('PeriodLabel_' + id).value;
		var seasonality = document.getElementById('seasonality_' + id).value ;
		g_fiscal_year.push(new Array(year, '', label, id, date, seasonality));
		id++;
	}	 
}


function setFMForecast() {
	document.getElementById('firstMeetingForecast').value = g_company[0][g_company.FMForecast];
}


function setPeriodEndDate() {	
	
	var min_year = g_year[0][g_year.MinYear];
	
	var d = new Date();
	var year = d.getFullYear();
	
	if (min_year < 1970) {
		min_year = year
	}	
	
	document.getElementById('year_div').innerHTML = '';
	var yesr_div = '<select name="year" id="year" onChange="setMonthEndDate()">';
		yesr_div += '<option value="-1"></option>';
		
	for (var i = min_year; i <= year + 1; ++i) {
		if(i == year) 
			yesr_div += '<option value="' + i + '" selected>' + i + '</option>';			
		else
			yesr_div += '<option value="' + i + '">' + i + '</option>';			
	}

	yesr_div += '</select>';
	document.getElementById('year_div').innerHTML = yesr_div;

	setMonthEndDate();	
}

/**
 * calculate seasonality
 *
 */
function calculateSeasonality() {
	var id = 1;
	var total = 0;
	while (document.getElementById('seasonality_' + id)) {
		var amount = parseInt(document.getElementById('seasonality_' + id).value, 10);
		if (isNaN(amount)) amount = 0;
		
		if (id < 13 || (id == 13 && document.getElementById('isExtraPeriodEnabled').checked)) {
			total += amount;
		}
		document.getElementById('seasonality_' + id).value = amount;
		id++;
	}
	
	document.getElementById('seasonality_total').innerHTML = total + '%';
	
	if (total != 100) document.getElementById('seasonality_total').style.color = 'red';
	else document.getElementById('seasonality_total').style.color = 'black';
}


/**
 * save seasonality
 *
 */
function save_seasonality() {

	var id = 1;
	var total = 0;
	while (document.getElementById('seasonality_' + id)) {
		var amount = parseInt(document.getElementById('seasonality_' + id).value, 10);
		if (isNaN(amount)) amount = 0;
		if (id < 13 || (id == 13 && document.getElementById('isExtraPeriodEnabled').checked)) {
			total += amount;
		}
		
		//total += amount; 
		document.getElementById('seasonality_' + id).value = amount;		

		id++;
	}
	document.getElementById('seasonality_total').innerHTML = total + '%';
	
	if (total != 100 && document.getElementById('SeasonalityCheck').checked) {
		alert('Seasonality must total to 100%');
		document.getElementById('seasonality_total').style.color = 'red';
		return;
	} else {
		document.getElementById('seasonality_total').style.color = 'black';
	}
	

	var SeasonalityCheck = (document.getElementById('SeasonalityCheck').checked) ? 1 : 0;
	
	try {
		var year = 	document.getElementById('year').value;
	} catch(e) {
		var d = new Date();
		var year = d.getFullYear();
	}	
		
	var data = 'Year=' + year + '&SeasonalityCheck=' + SeasonalityCheck + '&';	
	var id = 1;
	while (document.getElementById('seasonality_' + id)) {		
		data += 'seasonality[]=' + document.getElementById('seasonality_' + id).value + '&';
		id++;
	}
	
	g_seasonality.push(new Array(year, SeasonalityCheck));
	
	postData('../data/save_seasonality.php', data, 'storeSeasonalityCallBack');	
}


function checkPeriod() {
	if (document.getElementById('isExtraPeriodEnabled').checked) {
		document.getElementById('extra_period_label').style.display = '';
		document.getElementById('extra_period_range').style.display = '';
		document.getElementById('extra_period_value').style.display = '';
	} else {
		document.getElementById('extra_period_label').style.display = 'none';
		document.getElementById('extra_period_range').style.display = 'none';
		document.getElementById('extra_period_value').style.display = 'none';
		document.getElementById('PeriodEndDate_13').value = '';		
	}
	calculateSeasonality();
}



function getDateObj(date){
	var temp = date.split('/');	
	var myDate = new Date();
	myDate.setFullYear('20' + temp[2], temp[0] - 1, temp[1]);
	return myDate;
}


function checkSeasoanality() {
	var SeasonalityCheck = 0;

	if (document.getElementById('isExtraPeriodEnabled').checked) {
		var seasonality_values = new Array(7, 8, 8, 8, 7, 8, 8, 8, 7, 8, 8, 8, 7);
	} else {
		var seasonality_values = new Array(8, 8, 9, 8, 8, 9, 8, 8, 9, 8, 8, 9);
	}
	
	if (document.getElementById('SeasonalityCheck').checked) {
		var id = 1;
		while(document.getElementById('seasonality_' + id)) {
			document.getElementById('seasonality_' + id).style.display = '';
			document.getElementById('seasonality_' + id).value = seasonality_values[id - 1];
			id++;
		}
				
		document.getElementById('seasonality_total').style.display = '';
		
		SeasonalityCheck = 1;
	} else {
		var id = 1;
		while(document.getElementById('seasonality_' + id)) {
			document.getElementById('seasonality_' + id).style.display = 'none';
			id++;
		}		
		
		document.getElementById('seasonality_total').style.display = 'none';
	}
	calculateSeasonality();	
}


var http_req = false;

function createRequest(){
	if (window.XMLHttpRequest){		
		try{ http_req = new XMLHttpRequest(); }catch(e){}
		/*if (http_req.overrideMimeType) {
			http_req.overrideMimeType('text/html');
		}*/
	} 
	else if (window.ActiveXObject) {
		try {
			http_req = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e)	{
			try {
				http_req = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				http_req = false;
			}
		}
	}
	if (!http_req) alert("Error initializing XMLHttpRequest!");
}


function storeFiscalYear() {

	if (document.getElementById('year').value == '-1') {
		alert('Please select a year.');
		document.getElementById('year').focus();
		return;
	}

	var data = 'FiscalYearEndDate=' + document.getElementById('FiscalYearEndDate').value;
		data += '&' + 'year=' + document.getElementById('year').value;

		var id = 1;
		while (document.getElementById('PeriodEndDate_' + id)) {
			var date = document.getElementById('PeriodEndDate_' + id).value;
			
			if (id == 13) {
				if (document.getElementById('isExtraPeriodEnabled').checked) {
					
					if (date == '') {
						alert('Please enter the date.');
						document.getElementById('PeriodEndDate_' + id).focus();
						return;
					}

					prev_date = document.getElementById('PeriodEndDate_' + (id - 1)).value;
					
					if (getDateObj(date) <= getDateObj(prev_date)) {
						alert('Please enter a greater date than the previous period.');
						document.getElementById('PeriodEndDate_' + id).focus();
						return;
					}
					
					data += '&' + 'PeriodLabel[]=' + document.getElementById('PeriodLabel_' + id).value + '&PeriodEndDate[]=' + date;
				} else {
					data += '&' + 'PeriodLabel[]=' + document.getElementById('PeriodLabel_' + id).value + '&PeriodEndDate[]=0';
				}
			} else {
				if (date == '') {
					alert('Please enter the date.');
					document.getElementById('PeriodEndDate_' + id).focus();
					return;
				}
				
				
				if (id > 1) {					
					prev_date = document.getElementById('PeriodEndDate_' + (id - 1)).value;					
					
					if (getDateObj(date) <= getDateObj(prev_date)) {
						alert('Please enter a greater date than the previous period.');
						document.getElementById('PeriodEndDate_' + id).focus();
						return;
					}
				}
				
				data += '&' + 'PeriodLabel[]=' + document.getElementById('PeriodLabel_' + id).value + '&PeriodEndDate[]=' + date;
			}
			id++;	
		}
	
	
	
	
	addFiscalYear();	
	
	postData('../data/save_fiscal_year.php', data, 'storeFiscalYearCallBack');	
}


function storeFirstMeeting(firstMeetingForecast) {
	postData('../data/save_first_meeting.php', 'firstMeetingForecast=' + firstMeetingForecast, 'storeFirstMeetingCallBack');
}

function returnCallback(callback){
	var f_result = '';	
	if (http_req) http_req.onreadystatechange = function(){
		if (http_req.readyState == 4) {
			if (http_req.status == 200){								
				//alert(http_req.responseText)
				
				if (callback == 'storeFiscalYearCallBack') storeFiscalYearCallBack();				
				if (callback == 'storeSeasonalityCallBack')	storeSeasonalityCallBack();				
				if (callback == 'storeEmailCallBack') storeEmailCallBack();				
				if (callback == 'getPeopleEmailsCallBack') getPeopleEmailsCallBack(http_req.responseText);				
				
				//eval(callback + '("' + http_req.responseText + '");');
			}
			else if (http_req.status == 404){
				alert("Request URL does not exist");
			}
			else {
				alert("Error: status code is " + http_req.status);
			}
		}
	}
}

function storeFiscalYearCallBack() {
	setTimeout("save_seasonality()", 100);	
}


function storeSeasonalityCallBack() {	
	alert('Successfully saved.');
	checkPeriod();
}



function postData(url, args, callback){
	if (!http_req) createRequest();
	if (http_req){
		http_req.open('POST', url, true);
		http_req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		http_req.send(args);
	}
	returnCallback(callback);
}


// people add email
function addEmail() {
	var tblObj = document.getElementById('addEmails');
	var rows = tblObj.getElementsByTagName("tr");
	var row = document.createElement("tr");
	var length = rows.length;
	var id = length + 1;
	row.id = 'EmailRow_' + id;
	
	tdObj = document.createElement("td");
	tdObj.innerHTML = '<input type="hidden" name="email_id_' + id + '" id="email_id_' + id + '" value="0"><input id="email_' + id + '" name="email_' + id + '" type="text" maxlength="50" size="20">';
	row.appendChild(tdObj);
	
	tdObj = document.createElement("td");
	tdObj.id = 'emailTD_' + id;
	tdObj.innerHTML = '<input type="button" value="Add" onClick="addEmail()">';
	row.appendChild(tdObj);	
	
	document.getElementById('emailTD_' + length).innerHTML = '<input type="button" value="Remove" onClick="removeEmail(\'' + 'EmailRow_' + length + '\')">';
	
	tblObj.appendChild(row);
}


function removeEmail(rowID) {
	document.getElementById(rowID).style.display = 'none';
}

function saveEmails() {
	var id = 1;
	var data = '';
	var removed = '';
	var added = '';
	while (document.getElementById('email_' + id)) {
		
		var emailID = document.getElementById('email_id_' + id).value;
		
		if (document.getElementById('EmailRow_' + id).style.display != 'none') {
			var email = document.getElementById('email_' + id).value;
			
			if (email != '' && !is_valid_email(email)) {
				alert('Please enter the email in correct format.');
				document.getElementById('email_' + id).focus();
				return false;
			}

			if (email != '') {
				data += '&Emails[]=' + email;
				added += '&Added[]=' + emailID;
			}
			
		} else {
			if (emailID != 0) removed += '&Removed[]=' + emailID;
		}
		id++;
	}	

	var selectedPersonID = 'PersonID=' + g_selectedSp[g_people.PersonID];
	var primaryEmail = document.getElementById('editEmail').value;
	
	if (primaryEmail != '' && is_valid_email(primaryEmail)) {
		var primaryEmailStr = '&PrimaryEmail=' + document.getElementById('editEmail').value;
	}
	
	// save email
	postData('../data/save_people_emails.php', selectedPersonID + data + removed + added + primaryEmailStr, 'storeEmailCallBack');

}



function is_valid_email (email) {
	return /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(email);
}

function getPeopleEmails() {
	var selectedPersonID = 'PersonID=' + g_selectedSp[g_people.PersonID];
	postData('../data/get_people_emails.php', selectedPersonID, 'getPeopleEmailsCallBack');
}

function getPeopleEmailsCallBack(data) {	
	data = data.replace('window.loginResult = 1;', '');	
	document.getElementById('EmailContainer').innerHTML = data;
	document.getElementById('divSPEmails').style.display = '';
	
}

function storeEmailCallBack() {		
	setTimeout("getPeopleEmails()", 100);
	alert('Saved successfully.');
}

function show_calendar(targetName)
{
	document.body.style.cursor = 'wait';
	//alert(document.forms['form1'].elements[targetName]);
	Windowing.dropBox.calendarTarget = document.getElementById(targetName);
	//Windowing.dropBox.onCalendarEdited = onEditReviveDate;
	var win = Windowing.openSizedWindow('../shared/calendar.html', 320, 600, targetName);
	win.focus();
	document.body.style.cursor = 'auto';
}

function show_jetstream() {	
	if(g_defaultjetstream[0] == 1) {
		document.getElementById("jet_setting").style.display = 'block';
	} else {
		document.getElementById("jet_setting").style.display = 'none';
	}
}


function oppsDraw() {
	
	document.getElementById('divOppTable').innerHTML = '';
	document.getElementById('tdOpTableStatus').innerHTML = '';
	
	
	showPopup();
	setTimeout("getOpps();", 1);	// this timeout causes the page to be drawn, including our popup, while processing continues
	document.getElementById('oppsContent').style.display = '';
}
