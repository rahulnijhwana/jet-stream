addEvent(window, "load", sortables_init);
addEvent(window, "scroll", event_popdown_center);
addEvent(window, "resize", event_popdown_center);
var advanceddlg = null;
var currentPopDown = null;

function event_popdown_center()
{
	if (currentPopDown)
		centerAbsElem(currentPopDown);
};

var SORT_COLUMN_INDEX;
var SORT_COLUMN_DIR;

function sortables_init()
{
	// Find all tables with class sortable and make them sortable
	if (!document.getElementsByTagName)
		return;
	tbls = document.getElementsByTagName("table");
	for (ti=0;ti<tbls.length;ti++)
	{
		thisTbl = tbls[ti];
		if (((' '+thisTbl.className+' ').indexOf("sortable") != -1) && (thisTbl.id))
		{
			//initTable(thisTbl.id);
			ts_makeSortable(thisTbl);
		}
	}
}

function centerAbsElem(elem)
{
	var temptop = (document.body.scrollTop+(document.body.offsetHeight/2));
	temptop -= elem.offsetHeight/2;
	elem.style.top = temptop+"px";
	
	var templeft = (document.body.scrollLeft+(document.body.offsetWidth/2));
	templeft -= elem.offsetWidth/2;
	elem.style.left = templeft+"px";
}

function showPopDown(elemid)
{
	status = document.body.offsetHeight;
	var elem = document.getElementById(elemid);
	centerAbsElem(elem);
	elem.style.visibility = "visible";
	currentPopDown = elem;
	if (window["onShow_"+elemid])
		window["onShow_"+elemid](elem);
}

function hidePopDown()
{
	if (currentPopDown)
	{
		currentPopDown.style.visibility = "hidden"
		currentPopDown = null;
	}
}

function ts_onAdvancedDone(elem)
{
	var table = advanceddlg.table;
	var firstsel = document.getElementById('tsselect0');
	var firstchk = document.getElementById('tsdir0a');
	if (firstsel.value != -1)
	{
		for (var k = 1; k < table._sortstack.length; ++k)
		{
			var sel = document.getElementById('tsselect'+k);
			if (sel.value != -1)
			{
				var chk = document.getElementById('tsdir'+k+'a');
				table._sortstack[k-1] = {index:sel.value, dir:chk.checked, myspan:table._spans[sel.value]};
			}
			else
			{
				if (table._sortstack[k-1])
				{
					table._sortstack[k-1].myspan.innerHTML = '';
				}
				table._sortstack[k-1] = null;
			}
		}
		table._spans[firstsel.value].setAttribute("sortdir", firstchk.checked ? "up" : "down");
		ts_resortTable(table._links[firstsel.value], firstsel.value);
	}
	hidePopDown();
}

function ts_controlSelects(index)
{
	var tsselect = document.getElementById('tsselect'+index);
	if (tsselect.value == -1)
	{
		var k = index+1
		while (true)
		{
			var other = document.getElementById('tsselect'+k);
			if (!other)
				break;
			other.value = -1;
			other.disabled = true;
			k++;
		}
	}
	else
	{
		var other = document.getElementById('tsselect'+(index+1))
		if (other)
			other.disabled = false;
	}
}

function ts_showAdvancedSort(elem)
{
	var table = getParent(getParent(elem, 'TABLE').parentNode, 'TABLE');
	//alert(table.innerHTML);
	if (!advanceddlg)
	{
		advanceddlg = document.createElement("DIV");
		advanceddlg.id = "advanceddlg";
		advanceddlg.style.position = "absolute";
		advanceddlg.style.backgroundColor = "#DDDDDD";
		advanceddlg.style.border = "0px solid black";
		advanceddlg.style.visibility = "hidden";
		advanceddlg.style.width = "350px";
		advanceddlg.style.height = "350px";
		advanceddlg.style.padding = "3px";
		var str = '<center><table height="100%" class="km" style="font-family:Arial; font-size:10pt;">';
		str += '<tr>';
		str += '<td align="center" valign="top" style="font-size:12pt; font-weight:bold;" colspan="3">Advanced Sort</td>';
		str += '</tr>';
		for (var k = 0; k < table._sortstack.length; ++k)
		{
			str += '<tr>';
			if (k == 0)
				str += '<td>Sort by</td>';
			else
				str += '<td>Then by</td>';
			str += '<td id="tsfield'+k+'"></td>';
			str += '<td><input type="radio" name="tssort'+k+'" value="1" id="tsdir'+k+'a"></input>Ascending<br>';
			str += '<input type="radio" name="tssort'+k+'" value="0" id="tsdir'+k+'d"></input>Descending</td>';
			str += '</tr>';
		}
		str += '<tr><td valign="bottom" align="center" colspan="3"><input type="button" value="OK" onclick="ts_onAdvancedDone(this)" style="width:80px;"></input> <input type="button" value="Cancel" onclick="hidePopDown()" style="width:80px;"></input></td></tr>';
		str += '</table></center>';
		advanceddlg.innerHTML = str;
		document.body.appendChild(advanceddlg);
	}
	
	var firstRow = table.rows[1];
	if (firstRow)
	{
		//alert(optstr);
		var usedindexes = new Array();
		var firstNullFound = false;
		var prevobj = null;
		for (var k = 0; k < table._sortstack.length; ++k)
		{
			var obj = firstNullFound ? null : table._sortstack[k];
			var optstr = '';
			if (obj)
			{
				for (var n = 0; n < usedindexes.length; ++n)
				{
					if (usedindexes[n] == obj.index)
					{
						obj = null;
						break;
					}
				}
			}
			if (!obj)
			{
				optstr = '<option value="-1" selected></option>';
				firstNullFound = true;
			}
			else
			{
				optstr = '<option value="-1"></option>';
				usedindexes[usedindexes.length] = obj.index;
			}
			for (var n = 0; n < firstRow.cells.length; ++n)
				optstr += '<option value="'+n+'" '+((obj && obj.index == n)?'selected':'')+'>'+table._origtexts[n]+'</option>';
			document.getElementById('tsfield'+k).innerHTML = '<select id="tsselect'+k+'" '+((!obj && k != 0 && !prevobj)?'disabled="true"':'')+'" onchange="ts_controlSelects('+k+');">'+optstr+'</select>';
			document.getElementById('tsdir'+k+((!obj || obj.dir)?'a':'d')).checked = true;
			prevobj = obj;
		}
	}
	advanceddlg.table = table;
	
	showPopDown("advanceddlg");
}

function ts_onclickArrow(e, elem, index)
{
	table = getParent(elem, 'TABLE');
	if (table._sortstack[0].index != index)
	{
		var obj = null;
		var stackindex = 0;
		for (; stackindex < table._sortstack.length; ++stackindex)
		{
			if (table._sortstack[stackindex].index == index)
			{
				obj = table._sortstack[stackindex];
				break;
			}
		}
		obj.dir = !obj.dir;
		var topobj = table._sortstack[0];
		table._spans[obj.index].innerHTML = (obj.dir ? '&uarr;' : '&darr;') + '<span style="font-size:8pt; font-weight:normal;">' + (stackindex+1) + '</span>';
		table._spans[obj.index].setAttribute("sortdir", (table._spans[obj.index].getAttribute("sortdir") == 'down') ? 'up' : 'down');
		table._spans[topobj.index].setAttribute("sortdir", (table._spans[topobj.index].getAttribute("sortdir") == 'down') ? 'up' : 'down');
		ts_resortTable(table._links[topobj.index], topobj.index);
		try
		{
			e.cancelBubble = true;
		}
		catch (e) {}
	}
}

function ts_makeSortable(table)
{
	if (table.rows && table.rows.length > 0)
		var firstRow = table.rows[0];
	if (!firstRow)
		return;

	table._sortstack = new Array(null, null, null, null);
	table._origtexts = new Array();
	table._spans = new Array();
	table._links = new Array();

	// We have a first row: assume it's the header, and make its contents clickable links
	for (var i=0;i<firstRow.cells.length;i++)
	{
		var cell = firstRow.cells[i];
		var txt = ts_getInnerText(cell);
		table._origtexts[i] = txt;
		cell.innerHTML = '<a href="#" style="width:100%; height:100%;" class="sortheader" '+ 
		'onclick="ts_resortTable(this, '+i+');return false;">' + 
		txt+'<span class="sortarrow" style="margin-left:3px; color:blue;" onclick="ts_onclickArrow(event, this, '+i+'); return false;"></span></a>';
		table._links[i] = cell.childNodes[0];
		table._spans[i] = table._links[i].childNodes[1];
	}
	
	table._control_row = table.insertRow(table.rows[0]);
	table._control_row.className = "OnlyScreenBlock";
	table._control_cell = table._control_row.insertCell(table._control_row.cells[0]);
	table._control_cell.colSpan = firstRow.cells.length;
	table._control_cell.innerHTML = '<table class="km" style="font-family:Arial; font-size:10pt;"><tr><td valign="top">Click the column headers to sort&nbsp;</td><td align="right"><input type="button" value="Advanced Sort" onclick="ts_showAdvancedSort(this);"></input></td></tr></table>';
	
	table._sortfuncs = new Array();
	for (var j = 0; j < firstRow.cells.length; ++j)
	{
		var sortfn = ts_sort_caseinsensitive;
		for (var k = 1; k < table.rows.length; ++k)
		{
			var itm = ts_getInnerText(table.rows[k].cells[j]);
			if (itm.match(/^\d\d[\/-]\d\d[\/-]\d\d\d\d$/))
			{
				sortfn = ts_sort_date;
				break;
			}
			if (itm.match(/^\d\d[\/-]\d\d[\/-]\d\d$/))
			{
				sortfn = ts_sort_date;
				break;
			}
			if (itm.match(/^[�$]/))
			{
				sortfn = ts_sort_currency;
				break;
			}
			if (itm.match(/^[\d\.]+$/))
			{
				sortfn = ts_sort_numeric;
				break;
			}
		}
		table._sortfuncs[j] = sortfn;
	}
}

function ts_getInnerText(el)
{
	if (typeof el == "string")
		return el;
	if (typeof el == "undefined")
		return el;
	if (el.innerText)
		return el.innerText;	//Not needed but it is faster
	var str = "";

	var cs = el.childNodes;
	var l = cs.length;
	for (var i = 0; i < l; i++)
	{
		switch (cs[i].nodeType)
		{
			case 1: //ELEMENT_NODE
				str += ts_getInnerText(cs[i]);
				break;
			case 3:	//TEXT_NODE
				str += cs[i].nodeValue;
				break;
		}
	}
	return str;
}

var _cur_sorttable = null;

function ts_compound(a, b)
{
	var compoundval = 0;
	for (var k = 0; k < _cur_sorttable._sortstack.length; ++k)
	{
		var obj = _cur_sorttable._sortstack[k];
		if (!obj)
			break;
		SORT_COLUMN_INDEX = obj.index;
		SORT_COLUMN_DIR = obj.dir;
		var tempcmp = _cur_sorttable._sortfuncs[SORT_COLUMN_INDEX];
		var temp = tempcmp(a, b);
		if (temp != 0)
			return temp;
	}
	return 0;
}

function ts_resortTable(lnk,clid)
{
	// get the span
	var span;
	for (var ci=0;ci<lnk.childNodes.length;ci++)
	{
		if (lnk.childNodes[ci].tagName && lnk.childNodes[ci].tagName.toLowerCase() == 'span')
			span = lnk.childNodes[ci];
	}
	var td = lnk.parentNode;
	var column = clid || td.cellIndex;
	var table = getParent(td,'TABLE');

	if (table.rows.length <= 2)
		return;
	
	//SORT_COLUMN_INDEX = column;
	var firstRow = new Array();
	var newRows = new Array();
	for (i=0;i<table.rows[0].length;i++)
		firstRow[i] = table.rows[0][i];
	for (j=2;j<table.rows.length;j++)
		newRows[j-2] = table.rows[j];

	if (table._sortstack[0] && table._sortstack[0].index != column)
	{
		if (table._sortstack[table._sortstack.length-1])
			table._sortstack[table._sortstack.length-1].myspan.innerHTML = '';
		for (var k = table._sortstack.length-1; k > 0; --k)
		{
			table._sortstack[k] = table._sortstack[k-1];
			if (table._sortstack[k])
				table._sortstack[k].myspan.innerHTML = (table._sortstack[k].dir ? '&uarr;' : '&darr;') + '<span style="font-size:8pt; font-weight:normal;">' + (k+1) + '</span>';
		}
	}
	table._sortstack[0] = {index:column, dir:(span.getAttribute("sortdir") != 'down'), myspan:span};

	_cur_sorttable = table;
	newRows.sort(ts_compound);

	if (span.getAttribute("sortdir") == 'down')
	{
		ARROW = '&darr;';
		//newRows.reverse();
		span.setAttribute('sortdir','up');
	}
	else
	{
		ARROW = '&uarr;';
		span.setAttribute('sortdir','down');
	}

	// We appendChild rows that already exist to the tbody, so it moves them rather than creating new ones
	// don't do sortbottom rows
	for (i=0;i<newRows.length;i++)
	{
		if (!newRows[i].className || (newRows[i].className && (newRows[i].className.indexOf('sortbottom') == -1)))
			table.tBodies[0].appendChild(newRows[i]);
	}
	// do sortbottom rows only
	for (i=0;i<newRows.length;i++)
	{
		if (newRows[i].className && (newRows[i].className.indexOf('sortbottom') != -1))
			table.tBodies[0].appendChild(newRows[i]);
	}

	if (table._sortstack[1])
		span.innerHTML = ARROW+'<span style="font-size:8pt; font-weight:normal;">1</span>';
	else
		span.innerHTML = ARROW;
}

function getParent(el, pTagName)
{
	if (el == null)
		return null;
	else if (el.nodeType == 1 && el.tagName.toLowerCase() == pTagName.toLowerCase())	// Gecko bug, supposed to be uppercase
		return el;
	else
		return getParent(el.parentNode, pTagName);
}
function ts_sort_date(a,b)
{
	aa = ts_getInnerText(a.cells[SORT_COLUMN_INDEX]);
	bb = ts_getInnerText(b.cells[SORT_COLUMN_INDEX]);
	if (SORT_COLUMN_DIR)
		return Dates.compare(Dates.normalize(aa), Dates.normalize(bb));
	else
		return Dates.compare(Dates.normalize(bb), Dates.normalize(aa));
}

function ts_sort_currency(a,b)
{ 
	aa = ts_getInnerText(a.cells[SORT_COLUMN_INDEX]).replace(/[^0-9.]/g,'');
	bb = ts_getInnerText(b.cells[SORT_COLUMN_INDEX]).replace(/[^0-9.]/g,'');
	aa = unformat_money(aa);
	bb = unformat_money(bb);
	if (SORT_COLUMN_DIR)
		return aa - bb;
	else
		return bb - aa;
}

function ts_sort_numeric(a,b)
{ 
	aa = parseFloat(ts_getInnerText(a.cells[SORT_COLUMN_INDEX]));
	if (isNaN(aa))
		aa = 0;
	bb = parseFloat(ts_getInnerText(b.cells[SORT_COLUMN_INDEX])); 
	if (isNaN(bb))
		bb = 0;
	if (SORT_COLUMN_DIR)
		return aa-bb;
	else
		return bb-aa;
}

function ts_sort_caseinsensitive(a,b)
{
	aa = ts_getInnerText(a.cells[SORT_COLUMN_INDEX]).toLowerCase();
	bb = ts_getInnerText(b.cells[SORT_COLUMN_INDEX]).toLowerCase();
	if (aa==bb)
		return 0;
	if (SORT_COLUMN_DIR)
	{
		if (aa<bb)
			return -1;
		return 1;
	}
	else
	{
		if (aa>bb)
			return -1;
		return 1;
	}
}

function ts_sort_default(a,b)
{
	aa = ts_getInnerText(a.cells[SORT_COLUMN_INDEX]);
	bb = ts_getInnerText(b.cells[SORT_COLUMN_INDEX]);
	if (aa==bb)
		return 0;
	if (SORT_COLUMN_DIR)
	{
		if (aa<bb)
			return -1;
		return 1;
	}
	else
	{
		if (aa>bb)
			return -1;
		return 1;
	}
}

// addEvent and removeEvent
// cross-browser event handling for IE5+,  NS6 and Mozilla
// By Scott Andrew
function addEvent(elm, evType, fn, useCapture)
{
	if (elm.addEventListener)
	{
		elm.addEventListener(evType, fn, useCapture);
		return true;
	}
	else if (elm.attachEvent)
	{
		var r = elm.attachEvent("on"+evType, fn);
		return r;
	}
	else
		alert("Handler could not be removed");
} 
