function disableselect(e)
{
	return false
}

function reEnable()
{
	return true
}

function disableTextSelection(elem)
{
	elem.onselectstart = new Function ("return false");
	if (window.sidebar) // Netscape 6+
	{
		elem.onmousedown = disableselect;
		elem.onclick = reEnable;
	}
}

function deleteRowFromTable(oTable, oRow)
{
	for (var k = 0; k < oTable.rows.length; ++k)
	{
		if (oTable.rows[k] == oRow)
		{
			oTable.deleteRow(k);
			if (oTable.rows.length == 1)
				oTable.myxpt.m_title_td.colSpan = "1";
			return oRow;
		}
	}
	return null;
}

isNav = (navigator.appName.indexOf("Netscape") != -1) ? true : false;

function XPloreTree(text, value, iconimg_open, iconimg_closed, root_tree)
{
	this.appendToElement = XPloreTree_appendToElement;
	this.add = XPloreTree_add;
	this.remove = XPloreTree_remove;
	this.removeAll = XPloreTree_removeAll;
	this.removeMe = XPloreTree_removeMe;
	this.select = XPloreTree_select;
	this.clearSelection = XPloreTree_clearSelection
	this.getSelectedItem = XPloreTree_getSelectedItem;
	this.addSelectionListener = XPloreTree_addSelectionListener;
	this.removeSelectionListener = XPloreTree_removeSelectionListener;
	this.fireSelectionListeners = XPloreTree_fireSelectionListeners;
	this.expand = XPloreTree_expand;
	this.colapse = XPloreTree_colapse;
	this.addTree = XPloreTree_addTree;
	this.setRoot = XPloreTree_setRoot;
	this.colapseAll = XPloreTree_colapseAll;
	this.setText = XPloreTree_setText;
	this.clone = XPloreTree_clone;
	this.getItemCount = XPloreTree_getItemCount;
	this.getItem = XPloreTree_getItem;
	this.flatten = XPloreTree_flatten;
	this.move = XPloreTree_move;
	this.moveAllItems = XPloreTree_moveAllItems;
	this.getDistanceFrom = XPloreTree_getDistanceFrom;
	this.getDepth = XPloreTree_getDepth;
	this.showMe = XPloreTree_showMe;
	
	if (iconimg_open)
		this.m_open_icon_filename = iconimg_open;
	else
		this.m_open_icon_filename = null;
	
	if (iconimg_closed)
		this.m_closed_icon_filename = iconimg_closed;
	else
		this.m_closed_icon_filename = null;
	this.m_table = document.createElement("TABLE");
	disableTextSelection(this.m_table);
	this.m_table.className = "XPTTable";
	this.m_table.cellPadding = "0";
	this.m_table.cellSpacing = "0";
	this.m_table.myxpt = this;
	
	if (text)
	{
		if (isNav)
		{
			this.m_title_tr = document.createElement("TR");
			this.m_table.appendChild(this.m_title_tr);
		}
		else
			this.m_title_tr = this.m_table.insertRow(this.m_table.rows.length);
		if (this.m_open_icon_filename)
		{
			this.m_icon_td = this.m_title_tr.insertCell(this.m_title_tr.cells.length);
			this.m_icon_img = document.createElement("IMG");
			this.m_icon_img.className = "XPTIconImg";
			this.m_icon_img.src = this.m_open_icon_filename;
			this.m_icon_img.myXPT = this;
			this.m_icon_img.onclick = iconOnClick;
			//this.m_icon_td.innerHTML = '&nbsp;';
			this.m_icon_td.appendChild(this.m_icon_img);
			this.m_icon_td.innerHTML += '&nbsp;';
		}
		this.m_title_td = this.m_title_tr.insertCell(this.m_title_tr.cells.length);
		//this.m_title_td.colSpan = "2";
		this.m_title_span = document.createElement("NOBR");
		this.m_title_td.appendChild(this.m_title_span);
		this.m_title_span.className = "XPTTitleNOBR";
		this.m_title_span.innerHTML = text;
		this.m_title_span.onclick = titleSpanOnClick;
		this.m_title_span.onmousedown = titleSpanOnMouseDown;
		this.m_title_span.onmouseup = titleSpanOnMouseUp;
		this.m_title_span.onmouseover = titleSpanOnMouseOver;
		this.m_title_span.onmouseout = titleSpanOnMouseOut;
		this.m_title_span.myXPT = this;
		this.text = text;
	}
	else
		this.text = "";
	
	if (value || value === 0)
		this.value = value;
	else
		this.value = null;
	
	this.m_items = new Array();
	
	if (root_tree)
		this.m_root = root_tree;
	else
		this.m_root = this;
	
	this.m_movable = true;
	this.m_selectable = true;
	
	this.m_selectedItem = null;
	this.m_mouseDownItem = null;
	this.m_mouseInItem = null;
	this.expanded = true;
	this.m_selectionListeners = new Array();
	this.m_parent = null;
	this.m_item_count = 0;
	this.m_flat = false;
}

function XPloreTree_setImagePath(path)
{
	XPloreTree.s_imgpath = path;
	XPloreTree.s_img_plus = new Image();
	XPloreTree.s_img_plus.src = path + "XPT_plus.gif";
	XPloreTree.s_img_minus = new Image();
	XPloreTree.s_img_minus.src = path + "XPT_minus.gif";
	XPloreTree.s_img_blank = new Image();
	XPloreTree.s_img_blank.src = path + "XPT_blank.gif";
}

XPloreTree.setImagePath = XPloreTree_setImagePath;

function XPloreTree_getDistanceFrom(from_node)
{
	var depth_count = 1;
	var current_parent = this.m_parent;
	var target_node = null;
	if (from_node)
		target_node = from_node;
	while (current_parent != target_node)
	{
		if (!current_parent)
			return -1;
		current_parent = current_parent.m_parent;
		++depth_count;
	}
	return depth_count;
}

function XPloreTree_getDepth(depth_obj, current_depth)
{
	if (!depth_obj)
	{
		depth_obj = new Object();
		depth_obj.depth_count = 1;
		current_depth = 1;
	}
	
	if (depth_obj.depth_count < current_depth)
		depth_obj.depth_count = current_depth;
	
	++current_depth;
	for (var k = 0; k < this.m_item_count; ++k)
		this.getItem(k).getDepth(depth_obj, current_depth);
	return depth_obj.depth_count;
}

function XPloreTree_setText(text)
{
	this.text = text;
	this.m_title_span.innerHTML = text;
}

function XPloreTree_showMe()
{
	if (this.m_parent && this.m_parent != this)
	{
		this.m_parent.expand();
		this.m_parent.showMe();
	}
}

function XPloreTree_select()
{
	if (!this.m_root || !this.m_selectable || (this.m_root.allowSelectCallback && !this.m_root.allowSelectCallback(this)))
		return;
	if (this.m_root.m_selectedItem)
		this.m_root.m_selectedItem.m_title_span.className = "XPTTitleNOBR";
	this.m_root.m_selectedItem = this;
	this.m_root.fireSelectionListeners();
	this.m_title_span.className = "XPTTitleNOBR_Selected";
	this.showMe();
}

function XPloreTree_getSelectedItem()
{
	return this.m_selectedItem;
}

function XPloreTree_clearSelection()
{
	if (this.m_selectedItem)
	{
		this.m_root.m_selectedItem.m_title_span.className = "XPTTitleNOBR";
		this.m_selectedItem = null;
	}
}

function XPloreTree_addSelectionListener(listener)
{
	this.m_selectionListeners[this.m_selectionListeners.length] = listener;
}

function XPloreTree_removeSelectionListener(listener)
{
	for (var k = 0; k < this.m_selectionListeners.length; ++k)
	{
		if (this.m_selectionListeners[k] == listener)
		{
			this.m_selectionListeners[k] = null;
			return;
		}
	}
}

function XPloreTree_fireSelectionListeners()
{
	for (var k = 0; k < this.m_selectionListeners.length; ++k)
	{
		if (this.m_selectionListeners[k])
			this.m_selectionListeners[k](this.m_selectedItem);
	}
}

function XPloreTree_add(text, value, open_icon, closed_icon)
{
	if (!value && value !== false && value !== 0)
		value = null;
	if (!open_icon)
		open_icon = null;
	if (!closed_icon)
		closed_icon = null;
	if (this.m_parent && this.m_parent.m_flat)
		return this.m_parent.add(text, value, open_icon, closed_icon);
	if (this.m_title_td)
		this.m_title_td.colSpan = "2";
	var temptr = null;
	if (isNav)
	{
		temptr = document.createElement("TR");
		this.m_table.appendChild(temptr);
		temptr.in_doc = true;
	}
	else
		temptr = this.m_table.insertRow(this.m_table.rows.length);
	temptr.mynode = new XPloreTree(text, value, open_icon, closed_icon, this.m_root);
	temptr.mynode.my_tr = temptr;
	temptr.mynode.m_parent = this;
	temptr.control_td = temptr.insertCell(temptr.cells.length);
	temptr.control_td.align = "center";
	temptr.control_td.vAlign = "top";
	temptr.control_td.myXPT = temptr.mynode;
	temptr.mynode.controlimg = document.createElement("IMG");
	temptr.mynode.controlimg.className = "XPTControlImg";
	temptr.mynode.controlimg.myXPT = temptr.mynode;
	temptr.mynode.controlimg.src = XPloreTree.s_img_blank.src;
	temptr.control_td.appendChild(temptr.mynode.controlimg);
	temptr.node_td = temptr.insertCell(temptr.cells.length);
	temptr.mynode.appendToElement(temptr.node_td);
	this.m_items[this.m_items.length] = temptr;
	++this.m_item_count;
	if (this.controlimg && this.m_item_count == 1)
	{
		this.controlimg.src = XPloreTree.s_img_minus.src;
		this.controlimg.onclick = controlImgOnClick;
	}
	return temptr.mynode;
}

function XPloreTree_addTree(othertree)
{
	if (this.m_parent && this.m_parent.m_flat)
		return this.m_parent.addTree(othertree);
	if (this.m_title_td)
		this.m_title_td.colSpan = "2";
	othertree.removeMe();
	othertree.setRoot(this.m_root);
	var temptr = null;
	if (isNav)
	{
		temptr = document.createElement("TR");
		this.m_table.appendChild(temptr);
	}
	else
		temptr = this.m_table.insertRow(this.m_table.rows.length);
	temptr.mynode = othertree;
	temptr.mynode.my_tr = temptr;
	temptr.mynode.m_parent = this;
	temptr.control_td = temptr.insertCell(temptr.cells.length);
	temptr.control_td.align = "center";
	temptr.control_td.vAlign = "top";
	temptr.control_td.myXPT = temptr.mynode;
	temptr.mynode.controlimg = document.createElement("IMG");
	temptr.mynode.controlimg.className = "XPTControlImg";
	temptr.mynode.controlimg.myXPT = temptr.mynode;
	if (temptr.mynode.m_items.length > 0)
	{
		temptr.mynode.controlimg.src = XPloreTree.s_img_minus.src;
		temptr.mynode.controlimg.onclick = controlImgOnClick;
	}
	else
		temptr.mynode.controlimg.src = XPloreTree.s_img_blank.src;
	temptr.control_td.appendChild(temptr.mynode.controlimg);
	temptr.node_td = temptr.insertCell(temptr.cells.length);
	temptr.mynode.appendToElement(temptr.node_td);
	this.m_items[this.m_items.length] = temptr;
	++this.m_item_count;
	if (this.controlimg && this.m_item_count == 1)
	{
		this.controlimg.src = XPloreTree.s_img_minus.src;
		this.controlimg.onclick = controlImgOnClick;
	}
	if (this.m_flat)
		this.flatten();
	return temptr.mynode;
}

function XPloreTree_remove(item)
{
	if (this.m_root != item.m_root)
		return;
	if (isNav)
		this.m_table.removeChild(item.my_tr);
	else
		deleteRowFromTable(this.m_table, item.my_tr);
	
	for (var k = 0; k < this.m_items.length; ++k)
	{
		if (this.m_items[k] == item.my_tr)
		{
			this.m_items[k] = null;
			break;
		}
	}
	--this.m_item_count;
	if (this.m_item_count == 0)
	{
		this.controlimg.src = XPloreTree.s_img_blank.src;
		this.controlimg.onclick = null;
	}
	item.m_parent = null;
	item.m_root = null;
	if (this.m_root.m_selectedItem == item)
		this.m_root.clearSelection();
}

function XPloreTree_removeAll()
{
	var outarray = new Array();
	if (isNav)
	{
		for (var k = 0; k < this.m_items.length; ++k)
		{
			if (this.m_items[k])
			{
				if (this.m_items[k].in_doc == true)
					this.m_table.removeChild(this.m_items[k]);
				outarray[outarray.length] = this.m_items[k].mynode;
				this.m_items[k].mynode.m_parent = null;
				this.m_items[k].mynode.m_root = null;
			}
		}
	}
	else
	{
		for (var k = 0; k < this.m_items.length; ++k)
		{
			if (this.m_items[k])
			{
				deleteRowFromTable(this.m_items[k].mynode.m_table, this.m_items[k]);
				outarray[outarray.length] = this.m_items[k].mynode;
				this.m_items[k].mynode.m_parent = null;
				this.m_items[k].mynode.m_root = null;
			}
		}
	}
	this.m_items = new Array();
	this.m_item_count = 0;
	this.controlimg.src = XPloreTree.s_img_blank.src;
	this.controlimg.onclick = null;
	return outarray;
}

function XPloreTree_removeMe()
{
	if (!this.m_parent)
		return;
	this.m_parent.remove(this);
}

function XPloreTree_move(dest)
{
	var selected = (this.m_root.getSelectedItem() == this);
	this.removeMe();
	var the_clone = this.clone();
	var actual_dest = dest.addTree(the_clone).m_parent;
	if (the_clone.m_root.onMoveItem)
		the_clone.m_root.onMoveItem(the_clone, actual_dest);
	if (selected)
		the_clone.select();
}

function XPloreTree_moveAllItems(dest, lockChildrenValues)
{
	if (!lockChildrenValues)
		lockChildrenValues = false;
	var childarray = this.removeAll();
	for (var k = 0; k < childarray.length; ++k)
	{
		dest.addTree(childarray[k]);
		if (this.m_root.onMoveItem)
			this.m_root.onMoveItem(childarray[k], dest, lockChildrenValues);
	}
}

function XPloreTree_clone()
{
	var theclone = new XPloreTree(this.text, this.value, this.m_open_icon_filename, this.m_closed_icon_filename);
	for (var k = 0; k < this.m_items.length; ++k)
	{
		if (this.m_items[k])
			theclone.addTree(this.m_items[k].mynode.clone());
	}
	return theclone;
}

function XPloreTree_setRoot(newroot)
{
	this.m_root = newroot;
	for (var k = 0; k < this.m_items.length; ++k)
	{
		if (this.m_items[k])
			this.m_items[k].mynode.setRoot(newroot);
	}
}

function XPloreTree_expand()
{
	if (this.expanded)
		return;
	var temp_item = null;
	for (var k = 0; k < this.m_items.length; ++k)
	{
		temp_item = this.m_items[k];
		if (!temp_item)
			continue;
		if (isNav)
		{
			temp_item.in_doc = true;
			this.m_table.appendChild(temp_item);
		}
		else
		{
			for (var n = 0; n < temp_item.cells.length; ++n)
			{
				temp_item.cells[n].style.visibility = "visible";
				temp_item.cells[n].style.position = "static";
			}
			temp_item.style.visibility = "visible";
			temp_item.style.position = "static";
		}
	}
	this.controlimg.src = XPloreTree.s_img_minus.src;
	if (this.m_closed_icon_filename)
		this.m_icon_img.src = this.m_open_icon_filename;
	this.expanded = true;
}

function XPloreTree_colapse()
{
	if (!this.expanded || this.m_items.length == 0)
		return;
	var temp_item = null;
	for (var k = 0; k < this.m_items.length; ++k)
	{
		temp_item = this.m_items[k];
		if (!temp_item)
			continue;
		if (isNav)
		{
			temp_item.in_doc = false;
			this.m_table.removeChild(temp_item);
		}
		else
		{
			
			for (var n = 0; n < temp_item.cells.length; ++n)
			{
				temp_item.cells[n].style.visibility = "hidden";
				temp_item.cells[n].style.position = "absolute";
			}
			temp_item.style.visibility = "hidden";
			temp_item.style.position = "absolute";
		}
	}
	this.controlimg.src = XPloreTree.s_img_plus.src;
	if (this.m_closed_icon_filename)
		this.m_icon_img.src = this.m_closed_icon_filename;
	this.expanded = false;
}

function XPloreTree_colapseAll()
{
	if (this.controlimg)
		this.colapse();
	for (var k = 0; k < this.m_items.length; ++k)
	{
		if (this.m_items[k])
			this.m_items[k].mynode.colapseAll();
	}
}

function XPloreTree_getItemCount()
{
	return this.m_item_count;
}

function XPloreTree_getItem(index)
{
	var curcount = 0;
	for (var k = 0; k < this.m_items.length; ++k)
	{
		if (!this.m_items[k])
			continue;
		if (curcount == index)
			return this.m_items[k].mynode;
		++curcount
	}
	return null;
}

function XPloreTree_flatten()
{
	for (var k = 0; k < this.m_items.length; ++k)
	{
		if (this.m_items[k])
		{
			var childarray = this.m_items[k].mynode.removeAll();
			for (var n = 0; n < childarray.length; ++n)
			{
				this.addTree(childarray[n]);
				childarray[n].flatten();
			}
		}
	}
}

function XPloreTree_appendToElement(elem)
{
	elem.appendChild(this.m_table);
}

function iconOnClick()
{
	this.myXPT.select();
}

function titleSpanOnClick()
{
	this.myXPT.select();
}

function titleSpanOnMouseDown()
{
	this.myXPT.m_root.m_mouseDownItem = this.myXPT;
}

function titleSpanOnMouseUp()
{
	if (!this.myXPT.m_root.m_mouseDownItem)
		return;
	if (this.myXPT.m_root.m_mouseInItem != this.myXPT.m_root.m_mouseDownItem && this.myXPT.m_root.m_mouseDownItem.m_movable)
	{
		if (!this.myXPT.m_root.allowMoveCallback || this.myXPT.m_root.allowMoveCallback(this.myXPT.m_root.m_mouseDownItem, this.myXPT.m_root.m_mouseInItem) == true)
		{
			var curnode = this.myXPT.m_root.m_mouseInItem;
			while (true)
			{
				if (curnode.m_parent == null)
					break;
				curnode = curnode.m_parent;
				if (curnode == this.myXPT.m_root.m_mouseDownItem)
					return;
			}
			this.myXPT.m_root.m_mouseDownItem.m_parent.remove(this.myXPT.m_root.m_mouseDownItem);
			var theclone = this.myXPT.m_root.m_mouseDownItem.clone();
			var actual_dest = this.myXPT.m_root.m_mouseInItem.addTree(theclone).m_parent;
			if (theclone.m_root.onMoveItem)
				theclone.m_root.onMoveItem(theclone, actual_dest);
			theclone.select();
		}
	}
	this.myXPT.m_root.m_mouseDownItem = null;
}

function titleSpanOnMouseOver()
{
	this.myXPT.m_root.m_mouseInItem = this.myXPT;
}

function titleSpanOnMouseOut()
{
	this.myXPT.m_root.m_mouseInItem = null;
}

function controlImgOnClick()
{
	if (this.myXPT.expanded)
		this.myXPT.colapseAll();
	else
		this.myXPT.expand();
}