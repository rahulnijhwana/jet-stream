function disableselect(e)
{
	return false
}

function reEnable()
{
	return true
}

function disableTextSelection(elem)
{
	elem.onselectstart = new Function ("return false");
	if (window.sidebar) // Netscape 6+
	{
		elem.onmousedown = disableselect;
		elem.onclick = reEnable;
	}
}

function TableEx_addToElement(elem)
{
	elem.appendChild(this.m_maintable);
	this.redraw();
}

function onHeaderTDMouseOver()
{
	//this.className = "TableEx_HeaderTDMouseOver";
	this.bMouseIn = true;
}

function onHeaderTDMouseOut()
{
	this.className = "TableEx_HeaderTD";
	this.bMouseIn = false;
}

function onHeaderTDMouseDown()
{
	this.className = "TableEx_HeaderTDMouseDown";
}

function onHeaderTDMouseUp()
{
	//this.className = "TableEx_HeaderTDMouseOver";
	this.className = "TableEx_HeaderTD";
	if (this.bMouseIn)
	{
		//! Popup Menu
	}
}


function onHeaderTDClick()
{
	this.myTableEx.sort(this.myColIndex, this.myHeaderData.bSortAsc);
	this.myHeaderData.bSortAsc = !this.myHeaderData.bSortAsc;
}

function textEditOnBlur()
{
	this.myspan.valnobr.removeChild(this);
	this.myspan.valnobr.innerHTML = this.value;
	this.myspan.title = this.value;
	this.myspan.inEdit = false;
	if (this.value != this.myspan.oldval)
	{
		this.myspan.myarray[this.myspan.editindex] = this.value;
		this.myspan.myarray.edited = true;
	}
}

function editableCellOnClick()
{
	if (this.inEdit)
		return;
	this.inEdit = true;
	var temptext = document.createElement("INPUT");
	temptext.className = "TableEx_EditBox";
	temptext.type = "text";
	temptext.myspan = this;
	temptext.onblur = textEditOnBlur;
	temptext.value = this.valnobr.innerHTML;
	this.oldval = this.valnobr.innerHTML;
	this.valnobr.innerHTML = "";
	this.valnobr.appendChild(temptext);
	temptext.focus();
}

function dateCellChanged(newdate)
{
	this.myspan.myarray[this.myspan.editindex] = newdate;
	this.myspan.myarray.edited = true;
}

function dateCellOnClick()
{
	/*
	if (!document.g_TableExDateInput)
		document.g_TableExDateInput = new Object();
	document.g_TableExDateInput.mytd = this;
	document.g_TableExDateInput.value = this.innerHTML;
	*/
	var calwin = Windowing.openSizedPrompt("../shared/calendar.html", 320, 600);
	Windowing.dropBox.calendarTarget = this.myspan;
	Windowing.dropBox.myspan = this.myspan;
	Windowing.dropBox.onCalendarEdited = dateCellChanged;
	Windowing.dropBox.myarray = this.myarray;
	Windowing.dropBox.editindex = this.editindex;
}

function checkOnChange()
{
	if (!this.checkedClass)
		return;
	if (this.checked)
	{
		this.mytd.className = "TableEx_DataTD " + this.checkedClass;
		this.myspan.myarray[this.myspan.editindex] = "true";
	}
	else
	{
		this.mytd.className = "TableEx_DataTD";
		this.myspan.myarray[this.myspan.editindex] = "false";
	}
}

isNav = (navigator.appName.indexOf("Netscape") != -1) ? true : false;


function TableEx_drawRow(data_index, row_index)
{
	var k = data_index;
	if (row_index === null)
		row_index = this.m_contenttable.rows.length;
	else
		this.m_contenttable.deleteRow(row_index);
	var temptr = this.m_contenttable.insertRow(row_index);
	temptr.className = "TableEx_DataTR";

	if (this.m_row_color_secondary_field_name)
		temptr.style.backgroundColor = this.lookupSecondaryValue(k, this.m_row_color_secondary_field_name);

	var temparray = this.m_data[k];

	if (this.m_has_actions)
	{
		var temptd = temptr.insertCell(temptr.cells.length);
		temptd.align = "center";
		temptd.className = "TableEx_EditButtonTD";
		var tempcheck;

		if (this.onClickDeleteButton)
		{
			var tempimg = document.createElement("IMG");
			tempimg.src = this.imgpath + "delete.gif";
			tempimg.className = "TableEx_EditButton";
			tempimg.title = 'Delete';
			tempimg.myData = temparray;
			tempimg.myDataIndex = data_index;
			tempimg.myRowIndex = row_index;
			tempimg.myTableEx = this;
			tempimg.onclick = this.onClickDeleteButton;
			temptd.appendChild(tempimg);
		}

		if (this.onClickRemoveButton)
		{
			var tempimg = document.createElement("IMG");
			if (temparray[this.m_data_key.Category] == '9')
			{
				tempimg.src = this.imgpath + "blank_cabinet.jpg";
				//tempimg.title = 'Revive';
				//tempimg.onclick = this.onClickReviveButton;
			}
			else
			{
				tempimg.src = this.imgpath + "remove_to_cabinet.jpg";
				tempimg.title = 'Remove';
				tempimg.onclick = this.onClickRemoveButton;
			}
			tempimg.className = "TableEx_EditButton";
			tempimg.myData = temparray;
			tempimg.myDataIndex = data_index;
			tempimg.myRowIndex = row_index;
			tempimg.myTableEx = this;
			temptd.appendChild(tempimg);
		}

		if (this.onClickEditButton)
		{
			var tempimg = document.createElement("IMG");
			tempimg.src = this.imgpath + "go.gif";
			tempimg.className = "TableEx_EditButton";
			tempimg.title = 'Edit';
			tempimg.myData = temparray;
			tempimg.myDataIndex = data_index;
			tempimg.myRowIndex = row_index;
			tempimg.myTableEx = this;
			tempimg.onclick = this.onClickEditButton;
			temptd.appendChild(tempimg);
		}
		if (this.onClickCheck)	// figure out a test for this later
		{
			if (isNav)
			{
				tempcheck=document.createElement("INPUT");
				tempcheck.type = "checkbox";
				tempcheck.checked=temparray.checked;
			}
			else
				tempcheck = document.createElement('<INPUT type="checkbox"' + ((temparray.checked && temparray.checked==true) ? " checked" : "") + '>');
			tempcheck.myData=temparray;
			tempcheck.onclick=this.onClickCheck;
			temptd.appendChild(tempcheck);
		}

	}

	for (var n = 0; n < this.m_headerdata.length; ++n)
	{
		if (this.m_headerdata[n].hidden)
			continue;
		var temptd = temptr.insertCell(temptr.cells.length);
		temptd.align = 'left';
		temptd.className = "TableEx_DataTD";
		var tempspan = document.createElement("DIV");
		tempspan.className = "TableEx_DataSpan";
		temptd.appendChild(tempspan);
		var tempval = null;
		tempval = this.evalStatement(k, this.m_headerdata[n].evalStatement);
		if (tempval)
			tempval = tempval.escape_html()

		if (this.m_headerdata[n].editindex > -1)
		{
			tempspan.myTableEx = this;
			tempspan.myRowIndex = k;
			tempspan.myColIndex = n;
			tempspan.editindex = this.m_headerdata[n].editindex;
			tempspan.myarray = temparray;
			if (this.m_headerdata[n].editFunc)
				tempspan.onclick = this.m_headerdata[n].editFunc;
			else if (this.m_headerdata[n].datatype == "STRING")
				tempspan.onclick = editableCellOnClick;
			else if (this.m_headerdata[n].datatype == "DATE")
			{
				temptd.myspan = tempspan;
				temptd.onclick = dateCellOnClick;
				temptd.className = temptd.className + " TableEx_DateCell";
			}
			else if (this.m_headerdata[n].datatype.indexOf("CHECKBOX") != -1)
			{
				temptd.align = "center";
				tempspan.className = "TableEx_DataSpanForCheckbox";
				var tempcenter = document.createElement("CENTER");
				tempval = tempval.toLowerCase();
				if (isNav)
				{
					tempspan.mycheckbox = document.createElement("INPUT");
					tempspan.mycheckbox.type = "checkbox";
					if (tempval == "true" || tempval == "yes")
						tempspan.mycheckbox.checked = true;
					else
						tempspan.mycheckbox.checked = false;
				}
				else
					tempspan.mycheckbox = document.createElement('<INPUT type="checkbox"' + ((tempval == "true" || tempval == "yes") ? " checked" : "") + '>');
				tempspan.mycheckbox.className = "TableEx_CheckBox";
				tempspan.mycheckbox.mytd = temptd;
				tempspan.mycheckbox.myspan = tempspan;
				tempcenter.appendChild(tempspan.mycheckbox);
				tempspan.appendChild(tempcenter);
				var pos = this.m_headerdata[n].datatype.indexOf("(");
				var pos2 = this.m_headerdata[n].datatype.indexOf(")");
				if (pos > -1 && pos2 > -1)
				{
					tempspan.mycheckbox.checkedClass = this.m_headerdata[n].datatype.substring(pos + 1, pos2);
					if (tempspan.mycheckbox.checked)
						temptd.className = "TableEx_DataTD " + tempspan.mycheckbox.checkedClass;
					tempspan.mycheckbox.onclick = checkOnChange;
				}
				else
					tempspan.mycheckbox.checkedClass = null;
				continue;
			}
		}

		if (this.m_headerdata[n].datatype == "DATE")
			tempval = Dates.formatDate(new Date(Dates.mssql2us(tempval)));
		if (tempval == "" || tempval == null || tempval == "null" || tempval == "1/1/1900")
			tempval = "&nbsp;";

		tempspan.valnobr = document.createElement("NOBR");
		tempspan.valnobr.innerHTML = tempval;

		if (this.m_headerdata[n].secondaryFieldColor)
		{
			var littletable = document.createElement("TABLE");
			littletable.className = 'LittleTable';
			littletable.width = "100%";
			littletable.height = "100%";
			littletable.cellSpacing = "0";
			littletable.cellPadding = "0";
			var littletr = littletable.insertRow(littletable.rows.length);
			var littletd = littletr.insertCell(littletr.cells.length);
			var tempcolorspan = document.createElement("DIV");
			littletd.align = 'right';
			tempcolorspan.style.backgroundColor = this.evalStatement(k, this.m_headerdata[n].secondaryFieldColor);
			tempcolorspan.className = "TableEx_ColorBox";
			littletd.appendChild(tempcolorspan);
			littletd = littletr.insertCell(littletr.cells.length);
			littletd.align = 'left';
			littletd.className = "TableEx_ColorBoxText";
			littletd.appendChild(tempspan.valnobr);
			tempspan.appendChild(littletable);
		}
		else
			tempspan.appendChild(tempspan.valnobr);
		tempspan.title = tempval;
	}
tempcheck.checked=("1"=="1");
}

function TableEx_evalStatement(index, str)
{
	if (str.indexOf('|') > -1)
	{
		var multi_statements = str.split('|');
		for (var k = 0; k < multi_statements.length; ++k)
		{
			var temp_val = this.evalStatement(index, multi_statements[k]);
			if (temp_val && temp_val != "null" && temp_val != "")
				return temp_val;
		}
		return "";
	}
	else
	{
		var pos = -1;
		var pos2 = -1;
		var paren = -1;
		var paren2 = -1;
		var curdata = null;
		var fieldname = null;
		var timesthrough = 0;
		while (true)
		{
			pos = str.indexOf('{');
			if (pos == -1)
				break;
			pos2 = str.indexOf('}');
			if (pos2 == -1)
				break;
			paren = str.indexOf('(', pos);
			paren2 = str.indexOf(')', pos);
			if (paren != -1 && paren2 != -1)
			{
				curdata = this.m_data_with_id[str.substring(paren + 1, paren2)];
				fieldname = str.substring(paren2 + 1, pos2);
			}
			else
			{
				curdata = this.m_data;
				fieldname = str.substring(pos + 1, pos2);
			}
			var tempval = curdata.lookupValue(index, fieldname);
			if (tempval)
			{
				var re = /"/g;
				tempval = tempval.replace(re, '\\"');
			}
			str = str.replace(str.substring(pos, pos2 + 1), tempval);
			++timesthrough;
		}

		while (true)
		{
			pos = str.indexOf('[JS]');
			if (pos == -1)
				break;
			pos2 = str.indexOf('[/JS]');
			if (pos2 == -1)
				break;
			var cell_value = '';
//alert('About to call eval with: "' + str.substring(pos + 4, pos2) + '"');

			eval(str.substring(pos + 4, pos2));

			str = str.replace(str.substring(pos, pos2 + 5), cell_value);
//alert('back from eval. str=' + str);
			++timesthrough;
		}

		var re = /\\"/g;
		str = str.replace(re, '"');

		if (timesthrough == 0)
			return this.lookupValue(index, str);
		else
			return str;
	}
}

function TableEx_redraw()
{
	if (this.m_contenttable)
		this.m_contentdiv.removeChild(this.m_contenttable);

	this.m_contenttable = document.createElement("TABLE");
	this.m_contenttable.className = "TableEx_ContentTable";
	this.m_contenttable.cellPadding = "0";
	this.m_contenttable.cellSpacing = "0";

	this.m_headertr = this.m_contenttable.insertRow(this.m_contenttable.rows.length);
	this.m_headertr.className = "TableEx_HeaderTR";
	var start_index = this.m_toprow;

	

	for (var k = 0; k < this.m_toprow; ++k)
	{
		if (this.m_data[k].TE_deleted || !this.m_data[k])
			++start_index;
	}

	var end_index = start_index + this.rowsAtOnce;
	if (end_index > this.m_data.length)
		end_index = this.m_data.length;

	if (this.onClickDeleteButton || this.onClickEditButton || this.onClickRemoveButton)
	{
		var temptd = this.m_headertr.insertCell(this.m_headertr.cells.length);
		temptd.className = "TableEx_ButtonHeaderTD";
		temptd.vAlign = "middle";
		temptd.align = "center";
		var tempspan = document.createElement("DIV");
		tempspan.className = "TableEx_ButtonHeaderSpan";
		tempspan.innerHTML = "<b>Actions</b>";
		disableTextSelection(tempspan);
		temptd.appendChild(tempspan);
		this.m_has_actions = true;
	}
	else
		this.m_has_actions = false;

	for (var k = 0; k < this.m_headerdata.length; ++k)
	{
		if (this.m_headerdata[k].hidden)
			continue;
		var temptd = this.m_headertr.insertCell(this.m_headertr.cells.length);
		temptd.className = "TableEx_HeaderTD";
		temptd.vAlign = "middle";
		temptd.align = "center";
		temptd.onmouseover = onHeaderTDMouseOver;
		temptd.onmouseout = onHeaderTDMouseOut;
		temptd.onmousedown = onHeaderTDMouseDown;
		temptd.onmouseup = onHeaderTDMouseUp;
		temptd.onclick = onHeaderTDClick;
		temptd.myTableEx = this;
		temptd.myColIndex = k;
		temptd.myHeaderData = this.m_headerdata[k];
		var tempspan = document.createElement("DIV");
		if (this.m_headerdata[k].datatype && this.m_headerdata[k].datatype.indexOf("CHECKBOX") != -1)
			tempspan.className = "TableEx_HeaderSpanForCheckbox	";
		else
			tempspan.className = "TableEx_HeaderSpan";
		tempspan.innerHTML = this.m_headerdata[k].name;
		disableTextSelection(tempspan);
		temptd.appendChild(tempspan);
	}

	this.m_visible_count = 0;
	for (var k = start_index; k < end_index && k < this.m_data.length; ++k)
	{
		if (!this.m_data[k])
		{
			++end_index;
			continue;
		}
		if (this.m_data[k].TE_deleted)
		{
			++end_index;
			continue;
		}
		this.drawRow(k, null);
		++this.m_visible_count;
	}

	this.m_contentdiv.appendChild(this.m_contenttable);
	if (this.onRedraw)
		this.onRedraw();
msg=true;
}

// runs func against each row of displayed data
function TableEx_processDisplay(func)
{
	for (var k = 0; k < this.m_data.length; ++k)
	{
		if (!this.m_data[k])
		{
			continue;
		}
		if (this.m_data[k].TE_deleted)
		{
			continue;
		}
		func(this.m_data[k]);
	}

}

function TableEx_showPage(pagenum)
{
	if (pagenum < 0 || pagenum >= this.getPageCount())
		return;
	this.m_toprow = this.rowsAtOnce * pagenum;
	this.redraw();
}

function TableEx_getPageCount()
{
	return parseInt((this.m_data.length - this.m_deletedCount) / this.rowsAtOnce) + ((((this.m_data.length - this.m_deletedCount) % this.rowsAtOnce) != 0) ? 1 : 0);
}

function TableEx_getCurPage()
{
	return parseInt(this.m_toprow / this.rowsAtOnce);
}

function TableEx_insertColumn(colname, evalStatement, datatype, editindex, editFunc, secondaryFieldColor, hidden)
{
	var colobj = new Object();

	colobj.name = colname;
	colobj.evalStatement = evalStatement;
	colobj.bSortAsc = true;

	if (editindex || editindex === 0)
		colobj.editindex = editindex;
	else
		colobj.editindex = -1;

	if (editFunc)
		colobj.editFunc = editFunc;
	else
		colobj.editFunc = null;
	
	if (hidden)
		colobj.hidden = true;
	else
		colobj.hidden = false;

	if (secondaryFieldColor)
		colobj.secondaryFieldColor = secondaryFieldColor;
	else
		colobj.secondaryFieldColor = null;

	if (datatype)
	{
		datatype = datatype.toUpperCase();
		colobj.datatype = datatype;
	}
	else
		colobj.datatype = null;

	this.m_headerdata[this.m_headerdata.length] = colobj;
	if (this.addedToElem)
		this.redraw();
	return this.m_headerdata.length - 1;
}

function TEDataArray_lookupValue(index, fieldName)
{
	if (this.mainJoiningField)
	{
		var linkval = this.mytableex.m_data[index][this.mytableex.m_data_key[this.mainJoiningField]];
		if (this.joinmap[linkval])
			return this.joinmap[linkval][this[fieldName]];
		else
			return null;
	}
	else if (this.oneDim)
	{
		var linkval = this.mytableex.m_data[index][this.mytableex.m_data_key[fieldName]];
		return this[linkval];
	}
	else if (this == this.mytableex.m_data)
		return this[index][this.mytableex.m_data_key[fieldName]]
	else
		return this[index][this[fieldName]];
}

function TableEx_setData(dataarray, dataid, mainJoiningField, secJoiningField)
{
	dataarray.mytableex = this;
	dataarray.lookupValue = TEDataArray_lookupValue;
	if (!dataid)
	{
		this.m_data = dataarray;
		this.idData();
	}
	else
	{
		this.m_data_with_id[dataid] = dataarray;
		if (mainJoiningField && secJoiningField)
		{
			dataarray.mainJoiningField = mainJoiningField;
			dataarray.secJoiningField = secJoiningField;
			dataarray.joinmap = new Object();
			for (var k = 0; k < dataarray.length; ++k)
				dataarray.joinmap[dataarray[k][dataarray[dataarray.secJoiningField]]] = dataarray[k];
			this.m_data_with_id[dataid] = dataarray;
		}
		else
			dataarray.oneDim = true;
	}
}

function TableEx_setDataVal(fieldName, val)
{
	for(var i=0; i<this.m_data.length; ++i)
	{
		this.m_data[i][this.m_data_key[fieldName]] = val;
		this.m_data[i].edited = true;
	}
}

function TableEx_setDataKey(obj, dataid)
{
	this.m_data_key = obj;
}



function TableEx_lookupValue(index, fieldName)
{
	var val = this.m_data[index][this.m_data_key[fieldName]];
	return val;
}


function TableEx_deleteRow(index)
{
	this.m_data[index].TE_deleted = true;
	++this.m_deletedCount;
	--this.m_visible_count;
	var page_count = this.getPageCount();
	if (this.m_visible_count == 0 && page_count != 0)
		this.showPage(page_count - 1);
	else
		this.redraw();
}

function te_sort_asc(array1, array2)
{
	if (array1 > array2)
		return 1;
	if (array1 < array2)
		return -1;
	return 0;
}

function te_sort_desc(array1, array2)
{
	if (array1 < array2)
		return 1;
	if (array1 > array2)
		return -1;
	return 0;
}

var temp_sort_table_ex = null;

function TableEx_sort(sort_field_index, sort_asc, do_it)
{
	//var start_time = new Date().getTime();
	if (!do_it && this.onSort)
	{
		this.onSort();
		temp_sort_table_ex = this;
		setTimeout("temp_sort_table_ex.sort(" + sort_field_index + ", " + sort_asc + ", true);", 1);
		return;
	}
	if (!sort_asc && sort_asc !== false)
		sort_asc = true;
	
	var val_array = new Array();
	var temp_val = null;
	if (this.m_headerdata[sort_field_index].datatype == "DATE")
	{
		for (var k = 0; k < this.m_data.length; ++k)
		{
			temp_val = Dates.us2ordered(Dates.mssql2us(this.evalStatement(k, this.m_headerdata[sort_field_index].evalStatement)));
			val_array[k] = temp_val + '_' + k;
			//this.m_data[k].sort_val = new Date(Dates.mssql2us(this.evalStatement(k, this.m_headerdata[sort_field_index].evalStatement)));
		}
	}
	else
	{
		for (var k = 0; k < this.m_data.length; ++k)
		{
			temp_val = this.evalStatement(k, this.m_headerdata[sort_field_index].evalStatement);
			val_array[k] = temp_val + '_' + k;
			//this.m_data[k].sort_val = this.evalStatement(k, this.m_headerdata[sort_field_index].evalStatement);
		}
	}
	
	var data_copy = new Array();
	for (var k = 0; k < this.m_data.length; ++k)
		data_copy[k] = this.m_data[k];
	
	if (sort_asc)
		val_array.sort(te_sort_asc);
	else
		val_array.sort(te_sort_desc);
	
	//val_array.sort();
	for (var k = 0; k < this.m_data.length; ++k)
	{
		var _pos = val_array[k].lastIndexOf('_');
		++_pos;
		this.m_data[k] = data_copy[parseInt(val_array[k].substr(_pos))];
	}	
	
	this.idData();
	this.showPage(0);
	//alert(((new Date().getTime()) - start_time) + " miliseconds");
}

function TableEx_idData()
{
	for (var k = 0; k < this.m_data.length; ++k)
			this.m_data[k].my_index = k;
}

function TableEx_search(str, column_indices)
{
	var result_array = new Array();

	for (var k = 0; k < this.m_data.length; ++k)
	{
		if (this.m_data[k].TE_deleted)
			continue;
		for (var n = 0; n < column_indices.length; ++n)
		{
			var tempval = this.evalStatement(k, this.m_headerdata[column_indices[n]].evalStatement);
			if (tempval.indexOf(str) != -1)
			{
				result_array[result_array.length] = this.m_data[k];
				break;
			}
		}
	}

	return result_array;
}

function TableEx_searchSpan(greater_than, less_than, column_index)
{
	var result_array = new Array();

	var isDate = (this.m_headerdata[column_index].datatype == "DATE");

	if (isDate)
	{
		if (greater_than.length)
		{
			greater_than = new Date(Dates.mssql2us(greater_than))
			greater_than = greater_than.getTime();
		}

		if (less_than.length)
		{
			less_than = new Date(Dates.mssql2us(less_than))
			less_than = less_than.getTime();
		}
	}

	for (var k = 0; k < this.m_data.length; ++k)
	{
		var tempval = this.evalStatement(k, this.m_headerdata[column_index].evalStatement);
		if (isDate)
		{
			tempval = new Date(Dates.mssql2us(tempval))
			tempval = tempval.getTime();
		}
//Date ranges must be inclusive of both terms!!  Allan E, 5/13/2005
//		if (tempval > greater_than && tempval <= less_than)  
		if (tempval >= greater_than && tempval <= less_than)
			result_array[result_array.length] = this.m_data[k];
	}

	return result_array;
}

function TableEx(imgpath, onClickEditButton, onClickDeleteButton, onClickRemoveButton)
{
	this.addToElement = TableEx_addToElement;
	this.insertColumn = TableEx_insertColumn;
	this.redraw = TableEx_redraw;
	this.setData = TableEx_setData;
	this.setDataKey = TableEx_setDataKey;
	this.lookupValue = TableEx_lookupValue;
	this.evalStatement = TableEx_evalStatement;
	this.showPage = TableEx_showPage;
	this.processDisplay=TableEx_processDisplay;
	this.getPageCount = TableEx_getPageCount;
	this.getCurPage = TableEx_getCurPage;
	this.drawRow = TableEx_drawRow;
	this.deleteRow = TableEx_deleteRow;
	this.sort = TableEx_sort;
	this.idData = TableEx_idData;
	this.search = TableEx_search;
	this.searchSpan = TableEx_searchSpan;
	this.setDataVal = TableEx_setDataVal;

	this.imgpath = imgpath;

	if (onClickEditButton)
		this.onClickEditButton = onClickEditButton;
	else
		this.onClickEditButton = null;

	if (onClickDeleteButton)
		this.onClickDeleteButton = onClickDeleteButton;
	else
		this.onClickDeleteButton = null;

	if (onClickRemoveButton)
		this.onClickRemoveButton = onClickRemoveButton;
	else
		this.onClickRemoveButton = null;

	this.m_maintable = document.createElement("TABLE");
	this.m_maintable.className = "TableEx_MainTable";
	this.m_maintable.cellPadding = "0";
	this.m_maintable.cellSpacing = "0";

	this.m_contenttr = this.m_maintable.insertRow(this.m_maintable.rows.length);
	this.m_contenttd = this.m_contenttr.insertCell(this.m_contenttr.cells.length);
	this.m_contenttd.className = "TableEx_ContentTD";
	this.m_contenttd.vAlign = "top";

	this.m_contentdiv = document.createElement("DIV");
	this.m_contentdiv.mytableex = this;
	this.m_contentdiv.className = "TableEx_ContentDIV";
	this.m_contenttd.appendChild(this.m_contentdiv);

	this.m_headerdata = new Array();
	this.m_toprow = 0;
	this.rowsAtOnce = 10;
	this.addedToElem = false;
	this.m_data = new Array();
	this.m_secondary_data = null;
	this.m_secondary_map = new Object();
	this.m_linking_field_name = null;
	this.m_row_color_secondary_field_name = null;
	this.m_data_with_id = new Object();
	this.onedit = null;
	this.m_currentRows = new Object();
	this.m_deletedCount = 0;
	this.m_data_key = null;
	this.m_visible_count = 0;
}