var isIE = (navigator.appName.indexOf("Netscape") != -1) ? false : true;

var returnValue;

function ci_confirm(msg)
{
	var ok;
	if(isIE)
	{
		var args=new Object();
		var i;
		for (i=0;i<arguments.length;i++)
		{
			args[i]=arguments[i];
		}
		args.msg = msg;
		ok = showModalDialog('../shared/ci_confirm.html', args, 'dialogHeight:150px;dialogWidth:250px;');
	}
	else
	{
//		var win = window.open('chrome://ci_confirm.html','ci_confirm','chrome;alwaysRaised=yes;dependent=yes;modal=yes');
//		win.focus();
		ok = confirm(msg);		
	}
	return ok;
}

