
function processAddContactFields(result){

	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}
	if(result.STATUS=='OK')
	{
		if((document.getElementById('fieldSelectContact').value != 'DropDown') && (document.getElementById('fieldSelectContact').value != 'RadioButton')){
			addField('contactDiv',-1,'NO','','-1','0','0','','','','');
		}else{
			document.getElementById('contactOptionSetName').value = '';
			document.getElementById('optionSetSelect').value = '-1';
			document.getElementById('contactOptionSetvalues').innerHTML = '';
			addOptions = 0;
			$.blockUI({ message: $('#contactOptionListDiv'),
					css: { 
					padding: '10px',
					backgroundColor: '#FFFFFF',
					opacity: '.9', 
					color: '#000000',
					top:  (jQuery(window).height() - jQuery('#contactOptionListDiv').height()) /3 + 'px', 
					left: (jQuery(window).width() - jQuery('#contactOptionListDiv').width()) /2 + 'px', 
					width: '500px' ,
					height:'600px',
					overflow:'scroll'
					}});
		}
		//window.location='?sec=companylayout';
	}
	else
	{
		var str='';
		for(i=0;i<result.ERROR.length;i++)
		{
			str=str+'<br/>'+result.ERROR[i];
		}
		document.getElementById('showContactError').innerHTML=str;
	}
}

function checkContactFieldType(){
document.getElementById('showContactError').innerHTML = '';
var fieldType = document.getElementById('contactFieldType').value;	
switch(fieldType){
		case 'Text' :
			document.getElementById('fieldSelectContact').value = 'TextBox';
			document.getElementById('dataTypeSelectContact').value = 'Alphabet';
			break;
		case 'Number' :
			document.getElementById('fieldSelectContact').value = 'TextBox';
			document.getElementById('dataTypeSelectContact').value = 'Numeric';
			break;
		case 'Url' :
			document.getElementById('fieldSelectContact').value = 'TextBox';
			document.getElementById('dataTypeSelectContact').value = 'Alphabet';
			break;
		case 'Email' :
			document.getElementById('fieldSelectContact').value = 'TextBox';
			document.getElementById('dataTypeSelectContact').value = 'Alphabet';
			break;
		case 'Phone' :
			document.getElementById('fieldSelectContact').value = 'TextBox';
			document.getElementById('dataTypeSelectContact').value = 'Alphabet';
			break;
		case 'DropDown' :
			document.getElementById('fieldSelectContact').value = 'DropDown';
			break;
		case 'CheckBox' :
			document.getElementById('fieldSelectContact').value = 'CheckBox';
			break;
		case 'RadioButton' :
			document.getElementById('fieldSelectContact').value = 'RadioButton';
			break;
		case 'Date' :
			document.getElementById('fieldSelectContact').value = 'Date';
			break;	
	}	
}

function saveContactLayOut()
{
	if(!checkFormData('frmManageContact'))
		return;
	queryString='btnSave=Save&action=redo&companyId='+companyId;
	formValue=$('#frmManageContact').formSerialize();
	$.post('../../ajax/ajax.user.php?'+queryString,
	{
		queryString:formValue
	}
	,processSaveContactLayOut);
}

function checkFormData(frmInstance)
{
	frmInstance=document.getElementById(frmInstance);		
	for(i=0;i<frmInstance.length;i++)
	{
		if(frmInstance[i].type=='text' && frmInstance[i].value.indexOf('&')>=0)
		{

			alert('Invalid charcter &');			
			frmInstance[i].focus();
			return false;
		}

	}
	return true;

}

function processSaveContactLayOut(result)
{
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}
	if(result.STATUS=='OK')
	{
		//alert('Layout Saved');
		window.location='?action=contact';
	}
	else
	{
		var str='';
		for(i=0;i<result.ERROR.length;i++)
		{
			str=str+'<br/>'+result.ERROR[i];
		}
		document.getElementById('showContactError').innerHTML=str;
	}
}


