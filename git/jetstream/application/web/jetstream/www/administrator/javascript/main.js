function onbeforeunload()
{
	if (Header.isBlinking('Save'))
	{
		var args = new Object();
		args[0] = window;
		var ok = showModalDialog('unsaved_prompt.html', args, 'dialogWidth:300px; dialogHeight:200px;');
	}
}

function xb_refresh()
{
	if ((navigator.appName.indexOf("Netscape") != -1))
	window.location.href = 'main.html';
	else
	window.history.go(0);
}

function getCurrentHTML()
{
	var temp = document.createElement("TEXTAREA");
	temp.value = document.body.innerHTML;
	document.body.appendChild(temp);
}

//Is user an alias?  if so, does he have full rights?
g_alias_fullrights = 1;
if (g_login[0][g_login.LoginID] != g_login[0][g_login.PersonID])
g_alias_fullrights = (g_login[0][g_login.FullRights] == '1');
g_alias_importtarget = 0;
if (g_login[0][g_login.LoginID] != g_login[0][g_login.PersonID])
g_alias_importtarget = (g_login[0][g_login.Import] == '1');

g_newDealID = -1;

g_autoButtonLabel = 'Auto-'+g_company[0][g_company.ReviveLabel]+'/'+g_company[0][g_company.RenewLabel];

g_sortInEffect = 'FirstName';
g_sortDescending = false;

//Default for view ratings on board; supplement this with cookie
g_showRatingOnBoard = g_user[0][g_people.ShowRatingOnBoard];

g_sortTypes = new Object();
g_sortTypes['FirstName'] = compare_arrays.STRING;
g_sortTypes['LastName'] = compare_arrays.STRING;
g_sortTypes['Rating'] = compare_arrays.INT;
g_sortTypes['StartDate'] = compare_arrays.DATE;

g_CMInterface=g_company[0][g_company.CMInterface]=='1';

g_sib_mode = false;
g_sib_mode_tree = false;
g_child_mode = false;	// true if a manager is viewing a child board


//debug target toggle
//g_target_used = true;

// ---------------------------------------------------------------------------------------

r_timer='';

function remove_end()
{
	clearInterval(r_timer);
	checkRemove();
}

function remove(reasonID)
{

	r_opportunity[g_opps.RemoveReason] = reasonID;
	if(Windowing.dropBox.isFM)
	r_opportunity[g_opps.FM_took_place] = (Windowing.dropBox.FM_took_place.toUpperCase() == 'YES') ? 1 : 0;
	r_opportunity.isDirty=true;
	r_timer=setInterval("remove_end();", 1);
	return;
}

r_removeInd=0;

function checkRemove()
{
	//for testing, so there's only 1
	//if (r_removeInd>0) return;
	for(;r_removeInd<g_opps.length;r_removeInd++)
	{
		r_opportunity=g_opps[r_removeInd];
		if (r_opportunity[g_opps.Category]==9 && r_opportunity[g_opps.RemoveReason]=='')
		{
			//alert("Need a reason 4");
			Windowing.dropBox.isFM = r_opportunity[g_opps.Category] == 1;
			Windowing.dropBox.FM_took_place = 0;
			Windowing.dropBox.reasonCallback = remove;
			Windowing.dropBox.oppWin = this;
			var newwidth = 0;
			var newheight = 0;
			if (isNav)
			{
				newwidth = window.innerWidth;
				newheight = window.innerHeight;
			}
			else
			{
				newwidth = document.body.offsetWidth;
				newheight = document.body.offsetHeight;
			}
			//alert("about to open remove");
			//remove(0);
			//return;
			g_RemoveWindow = Windowing.openSizedWindow('../shared/remove.html', newheight, newwidth);

			return;
		}
	}
}

//-------------------------
function init()
{
	//alert('top of init.');

	if (!check_login()) return;

	if (g_message) alert(g_message);
	//alert("starting init");
	var bTarg = (g_company[0][g_company.TargetUsed] == '1');

	g_sib_mode = (get_cookie('sib_mode') == 'yes');

	g_sib_mode_tree = (get_cookie('sib_mode') == 'tree');

	document.writeln('<table border=0 cellspacing=0 cellpadding=0 height="100%" width="100%">');
	document.writeln('<tr height=55 valign="top"><td>');

	var eff_usr = get_cookie('mpower_effective_userid');

	// siblings for greater than level 2 are handle via the tree, not here
	// level 2 with viewsiblings get it for all boards

	var the_level = null;
	if (window.g_effective_user)
	the_level = window.g_effective_user[0][window.g_effective_user.Level];
	else
	the_level = window.loginResult;

	if (the_level>1)
	{
		// if the view mode is a number, then it's a child view
		var view_mode=get_cookie('view_mode');
		if (parseInt('0'+view_mode)) g_child_mode=true;

	}

	//	Header.addButton(ButtonStore.getButton('Print'));

	//	Header.addButton(new HeaderButton('Print Cal'), null, 'printCalendar()');
	if (g_alias_owner_choices.length > 0 && !window.g_effective_user && !get_cookie('owner_userid'))
	{
		window.onSelectAlias = function(index)
		{
			set_cookie('owner_userid', get_cookie('mpower_userid'));
			set_cookie('owner_pwd', get_cookie('mpower_pwd'));
			if (index != -1)
			{
				set_cookie('mpower_userid', g_alias_owner_choices[index][g_alias_owner_choices.UserID]);
				set_cookie('mpower_pwd', g_alias_owner_choices[index][g_alias_owner_choices.PasswordZ]);
			}
			window.history.go(0);
		};
		Header.setText('&nbsp;Choose A User&nbsp;');
		document.writeln(Header.makeHTML());
		var ivoryBox = new IvoryBox('100%', null);
		document.writeln(ivoryBox.makeTop());
		document.writeln('<center style="font-family:Arial;">');
		document.writeln('<span style="font-size:14pt; font-weight:bold;">Please select a login</span><br><br>');
		document.writeln('<div style="width:350px; text-align:left;"><a href="javascript:onSelectAlias(-1);">My Dashboard</a><br>');
		for (var k = 0; k < g_alias_owner_choices.length; ++k)
		{
			document.writeln('<a href="javascript:onSelectAlias('+k+');">'+g_alias_owner_choices[k][g_alias_owner_choices.Name]+' (alias of '+g_alias_owner_choices[k][g_alias_owner_choices.AliasOfName]+')</a><br>');
			//document.writeln('<a href="javascript:onSelectAlias('+k+');">'+g_alias_owner_choices[k][g_alias_owner_choices.Name]+'</a><br>');
		}
		document.writeln('</div></center><br>');
		document.writeln(ivoryBox.makeBottom());
	}
	else if ((the_level > 2 && !window.g_effective_user) || g_sib_mode_tree)
	{
		// overlord
		var treestr = '';
		if (g_sib_mode_tree)
		{
			// alert("mode tree");
			treestr = Tree.makeHTML(window.g_effective_user[0], "view");

		}
		else
		{
			// alert("not mode tree");
			if (g_user[0][g_user.ViewSiblings]>0 && g_user[0][g_user.SupervisorID]>=0)
			{
				// alert("top guy");
				//				treestr = Tree.makeHTML(Tree.topguy,"view");
				treestr = Tree.makeHTML(find_person(g_user[0][g_user.SupervisorID]),"view");
			} else {
				treestr = Tree.makeHTML(g_user[0],"mine");
			}
		}
		if (Tree.choiceCount == 1)
		{
			Tree.navToBoard(Tree.lastChoice.PersonID, Tree.lastChoice.sib);
			return;
		}
		if (get_cookie('owner_userid'))
		Header.addButton(ButtonStore.getButton('Dashboards...'));
		if(!g_showaffiliates && g_company[0][g_company.CMPath] != 'sf')
		Header.addButton(ButtonStore.getButton('Reports'));
		Header.setText('Choose A Dashboard');
		document.writeln(Header.makeHTML());
		var ivoryBox = new IvoryBox('100%', null);
		document.writeln(ivoryBox.makeTop());
		document.writeln(treestr);
		document.writeln(ivoryBox.makeBottom());
	}
	else
	{
		//Alerts button needs to be leftmost in all cases
		/*
		//Calendar function
		Header.addButton(ButtonStore.getButton('Print'));
		Header.hideButton('Print');
		Header.addButton(ButtonStore.getButton('Dashboard'));
		Header.hideButton('Dashboard');
		var showCalButton = (!g_sib_mode || (g_company[0][g_company.ViewSiblingsCalendar] == '1'));
		Header.addButton(ButtonStore.getButton('Reassign'));
		Header.hideButton('Reassign');
		*/
		//		var showCalButton = (!g_sib_mode || (g_company[0][g_company.ViewSiblingsCalendar] == '1'));
		//REmove cal button from sib mode to prevent sps from seeing peers opps
		var showCalButton = (!g_sib_mode);
		if (window.g_accessType == 2)	// manager's access
		{
			if (g_sib_mode)
			Header.addButton(ButtonStore.getButton('My Dashboard'));
			if (!g_sib_mode && !g_showaffiliates)
			{
				Header.addButton(ButtonStore.getButton('Alerts'));
				Header.addButton(ButtonStore.getButton('Reports'));
			}
			if (showCalButton)
			Header.addButton(ButtonStore.getButton('Calendar'));


			if (!g_sib_mode && !g_showaffiliates)
			{
				Header.addButton(ButtonStore.getButton('Trends'));
				Header.addButton(ButtonStore.getButton('Coaching'));
			}

			Header.addButton(ButtonStore.getButton('View'));
			if ('1' == importTargets(the_level) && !g_sib_mode)
			{
				Header.addButton(new HeaderButton('Imp ' + catLabel('Tgts'), null, 'do_Import();'));
				Header.setVarButton('Imp ' + catLabel('Tgts'),true);
			}
			if (bTarg && !g_CMInterface && (g_alias_fullrights || g_alias_importtarget))
			{
				Header.addButton(new HeaderButton('New ' + catLabel('Target'), null, 'do_new_target()'));
				Header.setVarButton('New ' + catLabel('Target'),true);
				Header.hideButton('New ' + catLabel('Target'));
			}
			if (g_alias_fullrights)
			{
				Header.addButton(ButtonStore.getButton('New Opp'));
				Header.hideButton('New Opp');
			}
			if (g_user[0][g_user.ViewSiblings] != '0' && g_user[0][g_user.Level]<3 &&
			(!g_sib_mode || g_user[0][g_user.Level]==2))
			Header.addButton(ButtonStore.getButton('Siblings'));
			if (g_user[0][g_user.Level]>2)
			Header.addButton(ButtonStore.getButton('Dashboards...'));

		}
		else	// salesperson access
		{
			if (g_sib_mode)
			Header.addButton(ButtonStore.getButton('My Dashboard'));
			if (bTarg)
			{
				Header.addButton(new HeaderButton('New ' + catLabel('Target'), null, 'do_new_target()'));
				Header.setVarButton('New ' + catLabel('Target'),true);
			}
			Header.addButton(ButtonStore.getButton('New Opp'));
			if(showCalButton)
			Header.addButton(ButtonStore.getButton('Calendar'));
			if (the_level == '1' && salespeopleReports() && !g_showaffiliates)
			Header.addButton(ButtonStore.getButton('Reports'));
			if (g_user[0][g_user.ViewSiblings] != '0' && g_user[0][g_user.Level]<3 &&
			(!g_sib_mode || g_user[0][g_user.Level]==2))
			Header.addButton(ButtonStore.getButton('Siblings'));
			if ('1' == importTargets(the_level) && !g_CMInterface && !g_sib_mode)
			{
				Header.addButton(new HeaderButton('Imp ' + catLabel('Tgts'), null, 'do_Import();'));
				Header.setVarButton('Imp ' + catLabel('Tgts'),true);
			}
			if (get_cookie('owner_userid'))
			Header.addButton(ButtonStore.getButton('Dashboards...'));
		}
		//moved from above
		Header.addButton(ButtonStore.getButton('Print'));
		Header.hideButton('Print');
		Header.addButton(ButtonStore.getButton('Dashboard'));
		Header.hideButton('Dashboard');
		Header.addButton(ButtonStore.getButton('Reassign'));
		Header.hideButton('Reassign');

		Header.addButton(new HeaderButton(g_autoButtonLabel, null, 'do_AutoRevive();'));
		Header.setVarButton(g_autoButtonLabel, true);
		if (window.g_accessType == 2)
		Header.hideButton(g_autoButtonLabel);

		if (!g_sib_mode)
		{
			Header.addButton(ButtonStore.getButton('Save'));
		}




		//alert('g_opps.length = ' + g_opps.length);
		if (0) // let's try these after makegrid
		{
			calc_opp_colors();

			//alert("Before analysis calc");
			Analysis.calcAll();
			//alert("Before trends calc");
			Trends.calc();
			//alert("Before border calc");
			calcOppBorders();
			//alert("After calcOpp Borders");
		}
		if (g_sib_mode) Header.setText(g_grouppath+" [View Only]");
		else Header.setText(g_grouppath);

		sort_2d_array(g_people, g_people[g_sortInEffect], g_sortTypes[g_sortInEffect], g_sortDescending);
		if (window.g_accessType == 2)
		{
			Grid.showPeople();
		}
		else
		{
			if (eff_usr && eff_usr != '')
			Grid.showPerson(eff_usr);
			else
			Grid.showPerson(g_user[0][g_user.PersonID]);
		}




		//****************************************
		//Header.addButton(ButtonStore.getButton('Toggle T'));
		//****************************************

		document.writeln(Header.makeHTML());
		Header.disableButton('Save');

		// for single-person with ability to view siblings, should have own view as default,
		// and have button "view siblings", causes read-only mgr view

		document.writeln('</td></tr><tr height=35 valign="bottom"><td><div id="theScroller">');
		//		document.writeln(Grid.makeScroller());
		document.writeln('</div></td></tr><tr valign="top"><td height="100%"><div id="theGrid" style="width: 100%; height: 100%">');
		//		document.writeln(Grid.makeGrid());
		document.writeln('</div></td></tr></table>');

		window.doGrid = true;

		calc_opp_colors();

		//alert("Before analysis calc");
		Analysis.calcAll();
		//alert("Before trends calc");
		Trends.calc();
		//alert("Before border calc");
		calcOppBorders();
		//alert("After calcOpp Borders");
		//alert("Before calcalerts");
		calcAllAlerts(); // needs to be after trends and analysis

		//alert("After calcalerts");
		// theory behind view_mode:
		// if there is a mode specified, then we have to first let the rest
		// of setup set the normal mode, so that there will be something to go back to
		// then use update_view to set up the new mode
		//
		if (window.g_accessType == 2)
		{
			var view_mode=get_cookie('view_mode');
			g_sortInEffect = get_cookie('sort_in_effect');
			g_sortDescending = (get_cookie('sort_descending') == '1' ? true : false);

			g_showRatingOnBoard = (get_cookie('view_rating_on_board') == '1' ? true : false);

			if (view_mode && view_mode !='' && view_mode !='previous' && view_mode != Grid.m_currentView)
			{
				var view_previous_mode=get_cookie('view_previous_mode');
				if (view_previous_mode && view_previous_mode !='')
				Grid.m_currentView.view = view_previous_mode;
				update_view(view_mode);
				run_loaded=false;
			}
		}
	}
}

function do_AutoRevive()
{
	document.body.style.cursor = 'wait';
	var hash = '#' + '-2' + '_' + Grid.firstShown()[g_people.PersonID];
	hash += '_' + getOppList(Grid.firstShown()[g_people.PersonID], -1);
	Windowing.dropBox.hash=hash;
	Windowing.openSizedWindow('spreadsheet.html?time='+(new Date()).getTime(), getWindowHeight(), 790, 'spreadsheet');
	document.body.style.cursor = 'auto';
}

function importTargets(level)
{
	var ret = false;
	var levelok = 0;
	for (var i = 0; i < g_levels.length; i++)
	{
		if (g_levels[i][g_levels.LevelNumber] == level)
		{
			levelok = g_levels[i][g_levels.Import];
			break;
		}

	}
	ret = (levelok == '1' || (g_login[0][g_login.Import] == '1'));
	return ret;
}

function salespeopleReports()
{
	for (var i=0; i < g_reports_company_xrefs.length; i++)
	{
		if (g_reports_company_xrefs[i][g_reports_company_xrefs.Salespeople] == '1')
		return true;
	}
	return false;
}

g_editedSubAnswers = new Array();

function setSubAnswer(DealID, SubID, Answer, CompletedLocation)
{
	//	No longer adequate!
	//	g_editedSubAnswers[g_editedSubAnswers.length] = new Array(DealID, SubID, Answer);
	var bFound = 0;
	for(var i = 0; i< g_editedSubAnswers.length; ++i)
	{
		if (g_editedSubAnswers[i][0] == DealID && g_editedSubAnswers[i][1] == SubID)
		{
			bFound = 1;
			g_editedSubAnswers[i][2] = Answer;
			g_editedSubAnswers[i][3] = CompletedLocation;
		}
	}
	if(!bFound);
	g_editedSubAnswers[g_editedSubAnswers.length] = new Array(DealID, SubID, Answer, CompletedLocation);
}


function q(v)
{
	return "'" + v + "'";
}

var idleSessionUpdateCount = 0;
var newLoginSession = false;
var sessionUpdaterStarted = false;

function loginSessionNotIdle()
{
	newLoginSession = idleSessionUpdateCount > 19;
	var updateSessionNow = idleSessionUpdateCount > 3;
	idleSessionUpdateCount = 0;
	if (updateSessionNow)
	updateLoginSession();
}

function updateLoginSession()
{
	if (!window.g_LoginSessionID)
	return;
	if (idleSessionUpdateCount < 3)
	{
		var http = getHTTPObject();
		var urlstr = "../data/update_loginsession.php?";
		if (newLoginSession)
		urlstr += "newLoginSession=yes&mpower_companyid"+get_cookie("mpower_companyid")+"&currentuser_loginid="+get_cookie("currentuser_loginid");
		else
		urlstr += "LoginSessionID="+g_LoginSessionID;
		http.open("GET", urlstr, false);
		http.send("");
		if (newLoginSession)
		{
			//alert("new g_LoginSessionID: "+http.responseText);
			g_LoginSessionID = http.responseText;
			set_cookie('LoginSessionID', g_LoginSessionID);
			newLoginSession = false;
		}
	}
	idleSessionUpdateCount++;
	//window.status = "idleSessionUpdateCount: "+idleSessionUpdateCount;
}

var run_loaded=true;

function loaded()
{
	//	if (window.doGrid) window.setTimeout('Grid.fillGrid()', 1);
	if ((navigator.appName.indexOf("Netscape") == -1))
	document.body.onbeforeunload=onbeforeunload;
	if (run_loaded)
	{
		var theGrid = document.getElementById('theGrid');
		if (theGrid)
		theGrid.innerHTML = Grid.makeGrid();
		else
		return;
		var theScroller = document.getElementById('theScroller');
		if (theScroller)
		theScroller.innerHTML = Grid.makeScroller();
		else
		return;
		Grid.fillGrid();
	}

	if (window.g_LoginSessionID && !sessionUpdaterStarted)
	{
		sessionUpdaterStarted = true;
		setInterval("updateLoginSession()", 1000*60);
	}
}

function calc_opp_colors()
{
	var colors = new Object();
	for (var i = 0; i < g_people.length; ++i)
	{
		var personID = g_people[i][g_people.PersonID];
		var color = g_people[i][g_people.Color];
		colors[personID] = color;
	}

	for (var i = 0; i < g_opps.length; ++i)
	{
		var personID = g_opps[i][g_opps.PersonID];
		g_opps[i].color = colors[personID];
	}
}

// ---------------------------------------------------------------------------------------

function do_MassMove()
{
	document.body.style.cursor = 'wait';
	var hash = '#' + '-1' + '_' + Grid.firstShown()[g_people.PersonID];
	hash += '_' + getOppList(Grid.firstShown()[g_people.PersonID], -1);
	Windowing.dropBox.hash=hash;

	Windowing.openSizedWindow('spreadsheet.html?time='+(new Date()).getTime(), getWindowHeight(), 790, 'spreadsheet');
	//		Windowing.openSizedWindow('spreadsheet.html' + hash, window.dialogHeight, 790, 'spreadsheet');


	document.body.style.cursor = 'auto';

}

function do_siblings()
{
	if (g_user[0][g_user.Level]>1)
	set_cookie('sib_mode', 'tree');
	else set_cookie('sib_mode', 'yes');
	set_cookie('mpower_effective_userid', g_user[0][g_user.SupervisorID]);
	xb_refresh();
}

function do_my_board()
{
	set_cookie('sib_mode', 'no');
	set_cookie('mpower_effective_userid', '');
	xb_refresh();
}

function do_new()
{
	if (g_CMInterface)
	{
		window.parent.parent.location.href = 'https://na1.salesforce.com/006/e?retURL=%2Fservlet%2Fservlet.Integration%3Flid%3D01r300000000hUf';
	}
	else
	{
		document.body.style.cursor = 'wait';
		var hash = '#-1_' + Grid.firstShown()[g_people.PersonID];
		var search = '?reqOpId=-1&reqPersonId='+ Grid.firstShown()[g_people.PersonID];
		Windowing.openSizedPrompt('../shared/edit_opp2.php' +search + hash, 630, 820);
		document.body.style.cursor = 'auto';
	}
}

function toggle_target()
{
	g_target_used = !g_company[0][g_company.TargetUsed];
	g_company[0][g_company.TargetUsed]=g_target_used;

}

function toggleButtons(showBoard)  //true=board; false=calendar
{
	var bTarg = (g_company[0][g_company.TargetUsed] == '1');
	//	var showCalButton = (!g_sib_mode || (g_company[0][g_company.ViewSiblingsCalendar] == '1'));
	//Remove calendar button from sibling mode to prevent salespeople from seeing peers' opportunity companies
	var showCalButton = (!g_sib_mode);

	if(showBoard)
	{
		if (g_child_mode && g_alias_fullrights)
		Header.showButton('Reassign');
		if (showCalButton)
		Header.showButton('Calendar');
		Header.hideButton('Dashboard');
		Header.hideButton('Print');
		if(bTarg && !g_sib_mode) Header.showButton('Imp ' + catLabel('Tgts'));
		Header.showButton('View');
		if (window.g_accessType == 2 && !g_sib_mode && !g_showaffiliates)
		{
			Header.showButton('Reports');
			Header.showButton('Coaching');
			Header.showButton('Trends');
			Header.showButton('Alerts');
		}
		if (Grid.numPeopleShown() == 1 && (g_alias_fullrights || g_alias_importtarget))
		{
			Header.showButton('New Opp');
			Header.showButton('New ' + catLabel('Target'));
		}

		Header.showButton(g_autoButtonLabel);
		Header.showButton('Save');
	}
	else
	{
		Header.showButton('Dashboard');
		Header.showButton('Print');
		if (showCalButton)
		Header.hideButton('Calendar');
		if (g_alias_fullrights || g_alias_importtarget)
		{
			Header.hideButton('New Opp');
			Header.hideButton('New ' + catLabel('Target'));
			Header.hideButton('Reassign');
		}
		if(bTarg && !g_sib_mode) Header.hideButton('Imp ' + catLabel('Tgts'));
		Header.hideButton('View');
		if (window.g_accessType == 2 && !g_sib_mode && !g_showaffiliates)
		{
			Header.hideButton('Reports');
			Header.hideButton('Coaching');
			Header.hideButton('Trends');
			Header.hideButton('Alerts');
		}
		if (Grid.numPeopleShown() == 1 && (g_alias_fullrights || g_alias_importtarget))
		{
			Header.hideButton('New Opp');
			Header.hideButton('New ' + catLabel('Target'));
		}
		Header.hideButton(g_autoButtonLabel);
		Header.hideButton('Save');

	}
}


function do_Calendar()
{
	update_view('calendar');
}

function do_Back()
{
	toggleButtons(true);
	update_view('previous');
}

function do_new_target()
{
	document.body.style.cursor = 'wait';
	var hash = '#-1_' + Grid.firstShown()[g_people.PersonID];
	//	Windowing.openSizedPrompt('../shared/edit_targ.html' + hash, 390, 400);
	Windowing.openSizedPrompt("../shared/edit_targ.html" + hash, 450, 825);

	document.body.style.cursor = 'auto';

}

function do_edit(oppID)
{
	var id = oppID.substr(1);
	var opp = find_opportunity(id);
	var hash = '#' + id + '_' + opp[g_opps.PersonID];
	hash += '_' + opp[g_opps.VLevel];
	var search = '?reqOpId='+id+'&reqPersonId='+ Grid.firstShown()[g_people.PersonID];
	Windowing.openSizedPrompt('../shared/edit_opp2.php' + search + hash, 630, 820);
	document.body.style.cursor = 'auto';
}

function getOppList(personID, category)
{
	var strRet = '';
	for (var i = 0; i < g_opps.length; i++)
	{
		var data = g_opps[i];
		if (data[g_opps.PersonID] != personID) continue;
		if (data[g_opps.Category] != category && category != -1) continue;
		if(strRet.length > 0) strRet += ",";
		strRet += data[g_opps.DealID];
	}
	return strRet;
}

function getWindowHeight()
{
	var hgt = 120;
	if(navigator.appName == "Netscape")
	{
		hgt = window.innerHeight;

	}
	else if(navigator.appName == "Microsoft Internet Explorer")
	{

		hgt += document.body.clientHeight;
	}
	return hgt;
}


function do_spreadsheet(whichCell)
{
	if (g_sib_mode)
	return;
	document.body.style.cursor = 'wait';
	var info = Grid.getCellInfo(whichCell);
	if (Grid.enable && info)
	{

		var hash = '#' + info.category + '_' + info.person;
		hash += '_' + getOppList(info.person, info.category);
		Windowing.dropBox.hash=hash;
		Windowing.openSizedWindow('spreadsheet.html?time='+(new Date()).getTime(), getWindowHeight(), 790, 'spreadsheet');
		//		Windowing.openSizedWindow('spreadsheet.html' + hash, window.dialogHeight, 790, 'spreadsheet');

	}
	document.body.style.cursor = 'auto';
}

function do_view()
{
	document.body.style.cursor = 'wait';
	var hash = '#' + Grid.getView();
	Windowing.dropBox.sib_mode = g_sib_mode;
	Windowing.dropBox.disallowRating = (g_user[0][g_user.RatingViewAllowed] == 0);
	Windowing.openSizedPrompt('which_view.html' + hash, 325, 400);
	document.body.style.cursor = 'auto';
}

function do_reports()
{
	document.body.style.cursor = 'wait';
	Windowing.openPrompt('which_report.html');
	document.body.style.cursor = 'auto';
}

function do_analysis()
{
	document.body.style.cursor = 'wait';
	var hash = '';
	if (Grid.numPeopleShown() == 1) hash = '#' + Grid.firstShown()[g_people.PersonID];
	Windowing.openPrompt('which_analysis.html' + hash);
	document.body.style.cursor = 'auto';
}

function do_trends()
{
	document.body.style.cursor = 'wait';
	var hash = '';
	if (Grid.numPeopleShown() == 1) hash = '#' + Grid.firstShown()[g_people.PersonID];
	Windowing.openPrompt('which_trends.html' + hash);
	document.body.style.cursor = 'auto';
}

function do_alerts()
{
	document.body.style.cursor = 'wait';
	//	Windowing.openSizedWindow('alerts.html', window.height, 790, 'alerts');
	Windowing.openSizedWindow('alerts.html', getWindowHeight(), 790, 'alerts');
	document.body.style.cursor = 'auto';
}

function do_ChangeBoard()
{
	if (get_cookie('owner_userid'))
	{
		set_cookie('mpower_userid', get_cookie('owner_userid'));
		set_cookie('mpower_pwd', get_cookie('owner_pwd'));
		delete_cookie('owner_userid');
		delete_cookie('owner_pwd');
	}
	delete_cookie('flat_view_id');
	set_cookie('mpower_effective_userid', '');
	xb_refresh();
}

function open_trend(trendID, personID)
{
	set_cookie('mpower_trendid', trendID);
	//	Windowing.openWindow('trend.html#' + trendID + '_' + personID, 'trenddetails');
	Windowing.openSizedWindow('trend.html#' + trendID + '_' + personID, getWindowHeight(), 790, 'trenddetails');

}

// ---------------------------------------------------------------------------------------

function save_calced_updates()
{
}

function do_save()
{
	Header.disableAllButtons();
	Grid.enable = false;
	g_saver = new Saver();
	g_saver.open();
	save_table(g_opps, g_opps.DealID, true, '\012', saved_new_opps, '../data/insert_opps.php');
}

function done_saving(errmsg)
{
	Header.enableAllButtons();
	Grid.enable = true;
	g_saver.close();
	if (errmsg)
	alert('Error Saving: ' + errmsg);
	//	else history.go(0);
	var hr=window.location.href;
	var pos = hr.indexOf('?');
	if (pos>=0) hr=hr.substr(hr,0,pos);
	if ((navigator.appName.indexOf("Netscape") == -1))
	document.body.onbeforeunload=null;
	window.location.href=hr+"?time="+(new Date()).getTime();
}

function save_table(table, idColumn, negativeIDs, delim, callback, url)
{
	var dirties = '';
	for (var i = 0; i < table.length; ++i)
	{
		if (!table[i].isDirty) continue;
		if (table[i].deleted) continue;

		if ((negativeIDs && table[i][idColumn] < 0) || (!negativeIDs && table[i][idColumn] > 0))
		{
			if (dirties.length) dirties += delim;
			dirties += table[i].toPipes();
		}
	}

	if (dirties.length)
	{
		g_saver.m_data = dirties;
		g_saver.m_callback = callback;
		g_saver.m_params['mpower_fieldlist'] = table['_fieldlist'];
		g_saver.m_url = url;
		g_saver.save();
	}
	else callback('ok');
}

function delete_from_table(table, idColumn, tableName, removeColumn, callback, url)
{
	var deleteds = '';
	for (var i = 0; i < table.length; ++i)
	{
		if (!table[i].deleted) continue;

		if (deleteds.length) deleteds += ',';
		deleteds += "'" + table[i][idColumn] + "'";
	}

	if (deleteds.length)
	{
		g_saver.reset();
		g_saver.m_params['mpower_remove_table'] = tableName;
		g_saver.m_params['mpower_remove_field'] = removeColumn;
		g_saver.m_params['mpower_remove_value'] = deleteds;
		g_saver.m_callback = callback;
		g_saver.m_url = url;
		g_saver.save();
	}
	else callback('ok');
}

function saved_new_opps(result)
{
	if (result.has_error)
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	if (result == 'Insert opportunities failed.')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}

	for (var k = 0; k < g_opp_products_xref.length; ++k)
	{
		for (var n = 0; n < result.length; n += 2)
		{
			if (g_opp_products_xref[k][g_opp_products_xref.DealID] == result[n])
			g_opp_products_xref[k][g_opp_products_xref.DealID] = result[n + 1];
			break;
		}
	}


	for (var k = 0; k < g_editedSubAnswers.length; ++k)
	{
		for (var n = 0; n < result.length; n += 2)
		{
			if (g_editedSubAnswers[k][0] == result[n])
			{
				g_editedSubAnswers[k][0] = result[n + 1];
				break;
			}
		}
	}

	save_table(g_opps, g_opps.DealID, false, '|', saved_existing_opps, '../data/update_opps.php');
}

function saved_existing_opps(result)
{
	if (result != 'ok')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	var dirties = '';
	for (var i = 0; i < g_editedSubAnswers.length; ++i)
	{
		if (dirties.length) dirties += '\012';
		dirties += g_editedSubAnswers[i].toPipes();
	}

	if (dirties.length > 0)
	{
		g_saver.reset();
		g_saver.m_data = dirties;
		g_saver.m_callback = saved_subanswers;
		g_saver.m_url = "../data/set_subanswer.php";
		g_saver.save();
	}
	else
	saved_subanswers('true');
}

function saved_subanswers(result)
{
	if (result != 'true')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	save_table(g_people, g_people.PersonID, false, '|', saved_existing_people,
	'../data/update_people_board.php');
}

function saved_existing_people(result)
{
	if (result != 'ok')
	{
		alert('saved_existing_people');
		done_saving(Saver.stripHTML(result));
		return;
	}
	save_table(g_snoozealerts, g_snoozealerts.SnoozeAlertsID, true, '\n', saved_new_snoozealerts,
	'../data/insert_snoozealerts.php');
}

function saved_new_snoozealerts(result)
{
	if (result != 'ok')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	save_table(g_snoozealerts, g_snoozealerts.SnoozeAlertsID, false, '|',
	saved_existing_snoozealerts, '../data/update_snoozealerts.php');
}

function saved_existing_snoozealerts(result)
{
	if (result != 'ok')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	delete_from_table(g_snoozealerts, g_snoozealerts.SnoozeAlertsID, 'snoozealerts', 'SnoozeAlertsID',
	deleted_snoozealerts, '../data/remove_rows.php');
}

function deleted_snoozealerts(result)
{
	if (result != 'ok')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}

	save_table(g_user, g_user.PersonID, false, '|', saved_user_settings,
	'../data/update_people_board.php');
}

//Added to save user view settings  10/19/2004
function saved_user_settings(result)
{
	if(result!='ok')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	save_table(g_login, g_login.PersonID, false, '|', saved_alias_settings,
	'../data/update_logins.php');

}



function saved_alias_settings(result)
{
	if (result != 'ok')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	for(var i=0; i<g_opp_products_xref.length; i++)
	{
		if(g_opp_products_xref[i].added==true)
		{
			g_opp_products_xref[i].added=false;
			g_opp_products_xref[i].isDirty=true;
		}
	}

	save_table(g_opp_products_xref, g_opp_products_xref.ID, true, '\012', saved_new_prod_xrefs, '../data/insert_arr_opp_prod_xref.php');
}


function saved_new_prod_xrefs(result)
{
	//alert('About to save existing xrefs.');
	if (result != 'ok')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	for(var i=0; i<g_opp_products_xref.length; i++)
	{
		if(g_opp_products_xref[i].isDirty == true) g_opp_products_xref[i].isDirty=false;
		if(g_opp_products_xref[i].edited==true)
		{
			g_opp_products_xref[i].edited=false;
			g_opp_products_xref[i].isDirty=true;
		}
	}

	save_table(g_opp_products_xref, g_opp_products_xref.ID, false, '|', saved_existing_prod_xrefs, '../data/update_arr_opp_prod_xrefs.php');
}

function saved_existing_prod_xrefs(result)
{
	//alert('Saved existing xrefs. result=' + result);
	if (result != 'ok')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	delete_from_table(g_opp_products_xref, g_opp_products_xref.ID, 'Opp_Product_XRef', 'ID', deleted_opp_prod_xrefs, '../data/remove_rows.php');
}

function deleted_opp_prod_xrefs(result)
{
	//alert('deleted xrefs. result=' + result);
	if (result != 'ok')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	done_saving();
}


// ---------------------------------------------------------------------------------------

function get_company_id()
{
	return g_company[0][g_company.CompanyID];
}


function find_opportunity(id)
{
	for (var i = 0; i < g_opps.length; ++i)
	if (g_opps[i][g_opps.DealID] == id) return g_opps[i];

	return null;
}

function find_opportunity_index(id)
{
	for (var i = 0; i < g_opps.length; ++i)
	if (g_opps[i][g_opps.DealID] == id) return i;

	return -1;
}

function find_matching_opportunity(company, productID)
{
	for (var i = 0; i < g_opps.length; ++i)
	{
		if (g_opps[i][g_opps.Company] != company) continue;
		if (g_opps[i][g_opps.ProductID] != productID) continue;
		return g_opps[i];
	}

	return null;
}

function find_matching_target(company, personID, dealID)
{
	for (var i=0; i < g_opps.length; i++)
	{
		if (g_opps[i][g_opps.DealID] == dealID) continue;
		if (g_opps[i][g_opps.Category] != '10') continue;
		if (g_opps[i][g_opps.Company].toUpperCase() != company.toUpperCase()) continue;
		if (g_opps[i][g_opps.PersonID] != personID) continue;
		return true;
	}
	return false;
}

function find_product(id)
{
	for (var i = 0; i < g_products.length; ++i)
	if (g_products[i][g_products.ProductID] == id) return g_products[i];

	return null;
}


function find_salescycle(personID, productID)
{
	for (var i = 0; i < g_salescycles.length; ++i)
	{
		if (g_salescycles[i][g_salescycles.PersonID] != personID) continue;
		if (g_salescycles[i][g_salescycles.ProductID] != productID) continue;
		return g_salescycles[i];
	}

	return null;
}

function list_salespeople_options(selectFirst)
{
	var ret = '';
	for (var i = 0; i < g_people.length; ++i)
	{
		var data = g_people[i];
		if (data[g_people.IsSalesperson] == '0') continue;
		ret += '<option';
		if (selectFirst && i == 0) ret += ' selected';
		ret += ' value="' + data[g_people.PersonID] + '">';
		ret += data[g_people.FirstName] + ' ' + data[g_people.LastName] + '</option>';
	}

	return ret;
}

function list_product_options()
{
	var ret = '';
	for (var i = 0; i < g_products.length; ++i)
	{
		var data = g_products[i];
		ret += '<option value="' + data[g_products.ProductID] + '">' + data[g_products.Name] + '</option>';
	}

	return ret;
}


g_temp_id_counter = 0 - Math.ceil(Math.random() * 100000);

function getTempID()
{
	return g_temp_id_counter--;
}

function make_new_opportunity()
{
	var ret = new Array();
	for (var k in g_opps)
	{
		var n = parseInt(k, 10);
		if (!isNaN(n))
		continue;
		if (k.charAt(0) == '_' || k == 'toPipes' || k == 'quicksort' || k == 'radixsort')
		continue;
		ret[ret.length] = '';
	}
	ret[g_opps.DealID] = getTempID();
	return ret;
}

function get_opportunity_columns()
{
	var ret = new Array();
	for (var k in g_opps)
	{
		var n = parseInt(k, 10);
		if (!isNaN(n)) continue;
		if (k.charAt(0) == '_' || k == 'toPipes') continue;
		ret[ret.length] = k;
	}

	return ret;
}

// ---------------------------------------------------------------------------------------

function alerts_changed()
{
	if (!Header.isBlinking('Save')) Header.blinkOn('Save');
	Header.enableButton('Save');
}

function update_from_edit_window(opp, oldCategory, oldPersonID)
{
	var skip_calcs=false;
	if(arguments.length > 3)
	skip_calcs=('skip_calcs' == arguments[3]);
	document.body.style.cursor = 'wait';

	opp.isDirty = true;

	var dealID = opp[g_opps.DealID];
	if ((dealID < 0 && !opp.added) || oldCategory == 9)
	{
		// add a new row & get a new deal ID
		//if (dealID == -1)
		//	dealID = --g_newDealID;
		opp[g_opps.DealID] = dealID;
		g_opps[g_opps.length] = opp;
		var info = get_salesperson_info(opp[g_opps.PersonID]);
		opp.color = info.color;
		Grid.addOpportunity(opp);
		opp.added = true;
	}
	else if (oldCategory == opp[g_opps.Category] && oldPersonID == opp[g_opps.PersonID])
	{
		// keep the cell in the same place, and avoid re-flowing
		var cellID = Grid.removeOpportunity(opp, true);
		Grid.addOpportunity(opp, cellID);
	}
	else
	{
		if (oldPersonID != opp[g_opps.PersonID])
		opp.color = get_salesperson_info(opp[g_opps.PersonID]).color;
		var newCategory = opp[g_opps.Category];
		var newPersonID = opp[g_opps.PersonID];
		opp[g_opps.Category] = oldCategory;
		opp[g_opps.PersonID] = oldPersonID;
		Grid.removeOpportunity(opp);
		opp[g_opps.Category] = newCategory;
		opp[g_opps.PersonID] = newPersonID;
		Grid.addOpportunity(opp);
	}
	if(!skip_calcs)
	{

		Analysis.calcFor(opp[g_opps.PersonID]); // recalculate analysis for that user
		Trends.calc();
		calcOppBorders();
		calcAllAlerts(); // needs to be after trends and analysis
	}

	var win = Windowing.getWindow(g_user[0][g_user.PersonID]+'spreadsheet');
	if (win)
	win.update_from_edit_window(opp, oldCategory, oldPersonID);

	win = Windowing.getWindow(g_user[0][g_user.PersonID]+'alerts');
	if (win)
	win.update_from_edit_window();

	if (!Header.isBlinking('Save'))
	Header.blinkOn('Save');
	Header.enableButton('Save');

	document.body.style.cursor = 'auto';
}

function do_person_calcs(personID)
{
	Analysis.calcFor(personID); // recalculate analysis for that user
	Trends.calc();
	calcOppBorders();
	calcAllAlerts(); // needs to be after trends and analysis
}

function update_view(whichView)
{
	if (!Grid.enable || (g_sib_mode && g_user[0][g_user.Level]>1) || g_sib_mode_tree)
	return;
	if (!whichView || whichView == '') return;

	if (whichView == 'grouped' || whichView == 'sidebyside')
	{
		sort_2d_array(g_people, g_people[g_sortInEffect], g_sortTypes[g_sortInEffect], g_sortDescending);
	}
	if (parseInt('0'+whichView)) g_child_mode=true;
	else g_child_mode=false;
	Grid.setView(whichView);

	if (g_alias_fullrights)
	{
		if (Grid.numPeopleShown() == 1 && whichView != 'calendar')
		{
			Header.showButton('New Opp');
			Header.showButton('New ' + catLabel('Target'));
		}
		else
		{
			Header.hideButton('New Opp');
			Header.hideButton('New ' + catLabel('Target'));
		}
		if (g_child_mode)
		{
			Header.showButton('Reassign');
			Header.showButton(g_autoButtonLabel);
		}
		else
		{
			Header.hideButton('Reassign');
			Header.hideButton(g_autoButtonLabel);
		}
	}
	else if (g_alias_importtarget)
	{
		if (Grid.numPeopleShown() == 1 && whichView != 'calendar')
		Header.showButton('New ' + catLabel('Target'));
		else
		Header.hideButton('New ' + catLabel('Target'));
	}
}



function findValuation(id)
{
	var ct = g_valuations.length;
	var i;
	for(i=0; i<ct; i++)
	{
		if(id == g_valuations[i][g_valuations.ValID]) return g_valuations[i];
	}
	return null;
}

function getVLevel(valID)
{
	for(var i=0;i<g_valuations.length;i++)
	{
		if (g_valuations[i][g_valuations.ValID]==valID)
		return g_valuations[i][g_valuations.VLevel];
	}

	return "--";
}

function do_Import()
{
	// was openSizedPrompt, but window needs to be scrollable for output (rjp)
	Windowing.openSizedWindow("../shared/import_targets.php", 575, 775, 'import');
}



