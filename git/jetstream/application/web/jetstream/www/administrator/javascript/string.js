#include_once(slider.js)

String.prototype.trim = function() { 
	var	str = this.replace(/^\s\s*/, ''), ws = /\s/, i = str.length;
	while (ws.test(str.charAt(--i)));
	return str.slice(0, i + 1);
};

String.prototype.ltrim = function() { 
	return this.replace(/^\s\s*/, '');
};

String.prototype.rtrim = function() { 
	var	str = this, ws = /\s/, i = str.length;
	while (ws.test(str.charAt(--i)));
	return str.slice(0, i + 1);
};

String.prototype.addslashes = function() { 
	return this.replace('/(["\'\])/g', "\\$1").replace('/\0/g', "\\0");
};

String.prototype.stripslashes = function() { 
	return this.replace('/\0/g', '0').replace('/\(.)/g', '$1');
};

String.prototype.strip_tags = function() {
	return this.replace(/<\/?[^>]+>/gi, '');
};

String.prototype.escape_html = function() {
	var div = document.createElement('div');
	var text = document.createTextNode(this);
	div.appendChild(text);
	return div.innerHTML;
};

String.prototype.unescape_html = function() {
	var div = document.createElement('div');
	div.innerHTML = this.strip_tags();
	return div.childNodes[0].nodeValue;
};