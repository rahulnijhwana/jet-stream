var shuffleQuestions = true;	/* Shuffle questions ? */
	//var shuffleAnswers = true;	/* Shuffle answers ? */
	var lockedAfterDrag = false;	/* Lock items after they have been dragged, i.e. the user get's only one shot for the correct answer */
	/* Don't change anything below here */
	var dragContentDiv = false;
	var dragContent = false;
	//var sections = new Array('DefaultSection');
	for(var no=0;no<sections.length;no++){
		//alert(sections[no]);
		var a = sections[no];
	}
	var dragSource = false;
	var dragDropTimer = -1;
	var destinationObjArray = new Array();
	var destination = false;
	var dragSourceParent = false;
	var dragSourceNextSibling = false;
	var secondSection;
	var sourceObjectArray = new Array();
	var arrayOfEmptyBoxes = new Array();
	var arrayOfAnswers = new Array();

	function getTopPos(inputObj)
	{
	  if(!inputObj || !inputObj.offsetTop)return 0;
	  var returnValue = inputObj.offsetTop;
	  while((inputObj = inputObj.offsetParent) != null)returnValue += inputObj.offsetTop;
	  return returnValue;
	}

	function getLeftPos(inputObj)
	{
	  if(!inputObj || !inputObj.offsetLeft)return 0;
	  var returnValue = inputObj.offsetLeft;
	  while((inputObj = inputObj.offsetParent) != null)returnValue += inputObj.offsetLeft;
	  return returnValue;
	}

	function cancelEvent()
	{
		return false;
	}

	function initDragDrop(e)
	{
		if(document.all)e = event;
		for(var no=0;no<sections.length;no++){
			var divId = sections[no];
			if(lockedAfterDrag && this.parentNode.parentNode.id==divId)return;
			dragContentDiv.style.left = e.clientX  + Math.max(document.documentElement.scrollLeft,document.body.scrollLeft) + 'px';
			dragContentDiv.style.top = e.clientY  + Math.max(document.documentElement.scrollTop,document.body.scrollTop) + 'px';
			dragSource = this;
			dragSourceParent = this.parentNode;
			dragSourceNextSibling = false;
			if(this.nextSibling)dragSourceNextSibling = this.nextSibling;
			if(!dragSourceNextSibling.tagName)dragSourceNextSibling = dragSourceNextSibling.nextSibling;

			dragDropTimer=0;
			timeoutBeforeDrag();

			return false;
		}
	}

	function timeoutBeforeDrag(){
		if(dragDropTimer>=0 && dragDropTimer<10){
			dragDropTimer = dragDropTimer +1;
			setTimeout('timeoutBeforeDrag()',10);
			return;
		}
		if(dragDropTimer>=10){
			dragContentDiv.style.display='block';
			dragContentDiv.innerHTML = '';
			dragContentDiv.appendChild(dragSource);
		}
	}

	function dragDropMove(e)
	{
		if(dragDropTimer<10){
			return;
		}

		if(document.all)e = event;

		var scrollTop = Math.max(document.documentElement.scrollTop,document.body.scrollTop);
		var scrollLeft = Math.max(document.documentElement.scrollLeft,document.body.scrollLeft);

		dragContentDiv.style.left = e.clientX + scrollLeft + 'px';
		dragContentDiv.style.top = e.clientY + scrollTop + 'px';

		var dragWidth = dragSource.offsetWidth;
		var dragHeight = dragSource.offsetHeight;


		var objFound = false;

		var mouseX = e.clientX + scrollLeft;
		var mouseY = e.clientY + scrollTop;

		destination = false;
		for(var no=0;no<destinationObjArray.length;no++){
			var left = destinationObjArray[no]['left'];
			var top = destinationObjArray[no]['top'];
			var width = destinationObjArray[no]['width'];
			var height = destinationObjArray[no]['height'];

			destinationObjArray[no]['obj'].className = 'destinationBox';
			var subs = destinationObjArray[no]['obj'].getElementsByTagName('DIV');
			if(!objFound && subs.length==0){
				if(mouseX < (left/1 + width/1) && (mouseX + dragWidth/1) >left && mouseY < (top/1 + height/1) && (mouseY + dragHeight/1) >top){
					destinationObjArray[no]['obj'].className='dragContentOver';
					destination = destinationObjArray[no]['obj'];
					objFound = true;
				}
			}
		}

		sourceObjectArray['obj'].className='';

		if(!objFound){
			var left = sourceObjectArray['left'];
			var top = sourceObjectArray['top'];
			var width = sourceObjectArray['width'];
			var height = sourceObjectArray['height'];

			if(mouseX < (left/1 + width/1) && (mouseX + dragWidth/1) >left && mouseY < (top/1 + height/1) && (mouseY + dragHeight/1) >top){
				destination = sourceObjectArray['obj'];
				sourceObjectArray['obj'].className='dragContentOver';
			}
		}
		return false;
	}


	function dragDropEnd()
	{
		if(dragDropTimer<10){
			dragDropTimer = -1;
			return;
		}
		dragContentDiv.style.display='none';
		//sourceObjectArray['obj'].style.backgroundColor = '#FFF';
		if(destination){
			dragSource.className='clsSectionSmallBox';
			destination.appendChild(dragSource);
			destination.className='destinationBox';

			if(destination.id && destination.id=='totalFieldsDiv'){
				dragSource.className='clsTotalDivSmallBox';
			}

		}else{
			if(dragSourceNextSibling)
				dragSourceNextSibling.parentNode.insertBefore(dragSource,dragSourceNextSibling);
			else
				dragSourceParent.appendChild(dragSource);
		}
		if(dragSource.className == 'clsTotalDivSmallBox'){
			//alert(dragSource.id);
			if(checkFields(dragSource))
				addUnassignedField(dragSource);
				
			id = dragSource.id;
			splitid = id.split("_");
			dragSource.innerHTML = unescape(splitid[0]);	
		}else if(dragSource.className == 'clsSectionSmallBox'){
			//alert(dragSource.parentNode.id);
			addAssignedField(dragSource);
			inner = dragSource.innerHTML;
			if(inner.substring(inner.length-1) != ']'){
				inner = inner + ' [-]';	
				dragSource.innerHTML = inner;
			}
		}
		dragDropTimer = -1;
		dragSourceNextSibling = false;
		dragSourceParent = false;
		destination = false;
		if(loadForm == 'Account'){
			saveCompanySectionLayout();	
		}else if(loadForm == 'Contact'){
			saveContactSectionLayout();	
		}
	}
	
	function addUnassignedField(source){
		var strtPos = source.id.lastIndexOf('_');
		var labelName = unescape(source.id.substr(0,strtPos));
		//alert(loadForm);
		var mapId = source.id.substr((strtPos + 1),source.id.length);
		var tempArr = new Array();
		tempArr['LabelName'] = labelName;
		tempArr['Align'] = '';
		tempArr['Position'] = '';
		tempArr['SectionId'] = '';
			
		if(loadForm == 'Account'){
			tempArr['AccountMapID'] = mapId;
			ACCOUNTFIELDS.push(tempArr);
			for(i=0;i<LEFTSECTION.length;i++){
				if(LEFTSECTION[i].AccountMapID == mapId){
					LEFTSECTION.splice(i,1); 
				}
			}
			for(i=0;i<RIGHTSECTION.length;i++){
				if(RIGHTSECTION[i].AccountMapID == mapId){
					RIGHTSECTION.splice(i,1); 
				}
			}
		}else if(loadForm == 'Contact'){
			tempArr['ContactMapID'] = mapId;
			CONTACTFIELDS.push(tempArr);
			for(i=0;i<CONTACTLEFTSECTION.length;i++){
				if(CONTACTLEFTSECTION[i].ContactMapID == mapId){
					CONTACTLEFTSECTION.splice(i,1); 
				}
			}
			for(i=0;i<CONTACTRIGHTSECTION.length;i++){
				if(CONTACTRIGHTSECTION[i].ContactMapID == mapId){
					CONTACTRIGHTSECTION.splice(i,1); 
				}
			}
		}
		
	}
	
	function addAssignedField(source){
		var parentId = source.parentNode.id;
		var strtPos = source.id.lastIndexOf('_');
		var labelName = unescape(source.id.substr(0,strtPos));
		var mapId = source.id.substr((strtPos + 1),source.id.length);
		var secPos = parentId.lastIndexOf('_');
		var secName = parentId.substr(0,secPos);
		var divNo = parentId.substr((secPos+1),parentId.length);
		var align = divNo % 2;
		if(align == 1){
			var pos = (parseInt(divNo) + 1)/2;	
		}else{
			var pos = (divNo)/2;	
		}
		
		if(checkFields(source)){
			//Means Field was assigned earlier
			if(loadForm == 'Account'){
				for(i=0;i<LEFTSECTION.length;i++){
					if(LEFTSECTION[i].AccountMapID == mapId){
						LEFTSECTION[i]['Align'] = align; 
						LEFTSECTION[i]['Position'] = pos;
						LEFTSECTION[i]['SectionName'] = strReplace(secName,' ', ''); 
					}
				}
				for(i=0;i<RIGHTSECTION.length;i++){
					if(RIGHTSECTION[i].AccountMapID == mapId){
						RIGHTSECTION[i]['Align'] = align; 
						RIGHTSECTION[i]['Position'] = pos;
						RIGHTSECTION[i]['SectionName'] = strReplace(secName,' ', '');
					}
				}
			}else if(loadForm == 'Contact'){
				for(i=0;i<CONTACTLEFTSECTION.length;i++){
					if(CONTACTLEFTSECTION[i].ContactMapID == mapId){
						CONTACTLEFTSECTION[i]['Align'] = align; 
						CONTACTLEFTSECTION[i]['Position'] = pos;
						CONTACTLEFTSECTION[i]['SectionName'] = strReplace(secName,' ', ''); 
					}
				}
				for(i=0;i<CONTACTRIGHTSECTION.length;i++){
					if(CONTACTRIGHTSECTION[i].ContactMapID == mapId){
						CONTACTRIGHTSECTION[i]['Align'] = align; 
						CONTACTRIGHTSECTION[i]['Position'] = pos;
						CONTACTRIGHTSECTION[i]['SectionName'] = strReplace(secName,' ', '');
					}
				}
			}
			
			
		}else{
			var tempArr = new Array();
			tempArr['LabelName'] = labelName;
			tempArr['Align'] = align;
			tempArr['Position'] = pos;
			tempArr['SectionName'] = strReplace(secName,' ', '');
			tempArr['TabOrder'] = null;
			if(loadForm == 'Account'){
				tempArr['AccountMapID'] = mapId;
				if(align == 1){
					LEFTSECTION.push(tempArr);	
				}else{
					RIGHTSECTION.push(tempArr);
				}
				for(i=0;i<ACCOUNTFIELDS.length;i++){
					if(ACCOUNTFIELDS[i].AccountMapID == mapId){
						ACCOUNTFIELDS.splice(i,1);
					}
				}
			}else if(loadForm == 'Contact'){
				tempArr['ContactMapID'] = mapId;
				if(align == 1){
					CONTACTLEFTSECTION.push(tempArr);	
				}else{
					CONTACTRIGHTSECTION.push(tempArr);
				}
				for(i=0;i<CONTACTFIELDS.length;i++){
					if(CONTACTFIELDS[i].ContactMapID == mapId){
						CONTACTFIELDS.splice(i,1);
					}
				}
			}
			
		}
		
	}
	//Check if the field was unassigned earlier then return false 
	function checkFields(source){
		var strtPos = source.id.lastIndexOf('_');
		var mapId = source.id.substr((strtPos + 1),source.id.length);
		if(loadForm == 'Account'){
			for(i=0;i<LEFTSECTION.length;i++){
				if(LEFTSECTION[i].AccountMapID == mapId){
					return true;
				}
			}
			for(i=0;i<RIGHTSECTION.length;i++){
				if(RIGHTSECTION[i].AccountMapID == mapId){
					return true; 
				}
			}	
		}else if(loadForm == 'Contact'){
			for(i=0;i<CONTACTLEFTSECTION.length;i++){
				if(CONTACTLEFTSECTION[i].ContactMapID == mapId){
					return true;
				}
			}
			for(i=0;i<CONTACTRIGHTSECTION.length;i++){
				if(CONTACTRIGHTSECTION[i].ContactMapID == mapId){
					return true; 
				}
			}	
		}
		return false;
	}
	
	function initDragDropScript(sections,drag_content)
	{
		dragContentDiv = document.getElementById(drag_content);
		if(navigator.appName == "Microsoft Internet Explorer") {
			var totalFieldsDiv = document.getElementById('totalFieldsDiv');
			document.getElementById('totalFieldsDiv').style.position = 'absolute';
			//document.getElementById('totalFieldsDiv').style.left = '560';
			document.getElementById('totalFieldsDiv').style.top = '0';
			
		} else {
			totalFieldsDiv = document.getElementById('totalFieldsDiv');
			document.getElementById('totalFieldsDiv').style.position='fixed';
		}
		
		totalFieldsDiv.onselectstart = cancelEvent;
		var divs = totalFieldsDiv.getElementsByTagName('DIV');
		var answers = new Array();

		for(var no=0;no<divs.length;no++){
			if(divs[no].className=='clsTotalDivSmallBox'){
				divs[no].onmousedown = initDragDrop;
				answers[answers.length] = divs[no];
				arrayOfAnswers[arrayOfAnswers.length] = divs[no];
			}

		}
		var data = '';
		var count = 1;		
		for(var nos=0;nos<sections.length;nos++)
		{
			//alert(sections[nos]);
			if(secName[nos] != 'DefaultSection'){
				document.getElementById(sections[nos]).innerHTML += "<div style='border:1px solid #000; text-align:center; width:450px;display:block;'>"+secName[nos]+"</div><br>";				
			}
			for(var no=1;no<=(2*allSecSize[nos]);no++)
			{				
				data+= "<div class='destinationBox'  id='"+sections[nos]+"_"+no+"'></div>";
			}
			document.getElementById(sections[nos]).innerHTML += data;			
			count++;
			data='';
		}

		sourceObjectArray['obj'] = totalFieldsDiv;
		sourceObjectArray['left'] = getLeftPos(totalFieldsDiv);
		sourceObjectArray['top'] = getTopPos(totalFieldsDiv);
		sourceObjectArray['width'] = totalFieldsDiv.offsetWidth;
		sourceObjectArray['height'] = totalFieldsDiv.offsetHeight + parseInt(document.body.scrollHeight);

		var questions = new Array();
		var questionsOpenBoxes = new Array();
		var divs = new Array();
		for(var no=0;no<sections.length;no++){
			document.getElementById(sections[no]).onselectstart = cancelEvent;
			divs[no] = document.getElementById(sections[no]).getElementsByTagName('DIV');
		}
		for(var nos=0;nos<divs.length;nos++){
			for(var no=0;no<divs[nos].length;no++){
				if(divs[nos][no].className=='destinationBox'){
					var index = destinationObjArray.length;
					destinationObjArray[index] = new Array();
					destinationObjArray[index]['obj'] = divs[nos][no];
					destinationObjArray[index]['left'] = getLeftPos(divs[nos][no])
					destinationObjArray[index]['top'] = getTopPos(divs[nos][no])
					destinationObjArray[index]['width'] = divs[nos][no].offsetWidth;
					destinationObjArray[index]['height'] = divs[nos][no].offsetHeight;
					questionsOpenBoxes[questionsOpenBoxes.length] = divs[nos][no];
					arrayOfEmptyBoxes[arrayOfEmptyBoxes.length] = divs[nos][no];
				}
				if(divs[nos][no].className=='clsSectionSmallBox'){
					questions[questions.length] = divs[nos][no];
				}
			}
		}

		for(var no=0;no<sections.length;no++){
			document.getElementById(sections[no]).style.visibility = 'visible';
		}
		totalFieldsDiv.style.visibility = 'visible';

		document.documentElement.onmouseup = dragDropEnd;
		document.documentElement.onmousemove = dragDropMove;
		return answers;
	}
	
	function reInitDragDropScript(ans,values)
	{
		var usedValues = values;
		var divs = new Array();
		for(var i=0;i<usedValues.length;i++){
			divs[i] = document.getElementById(usedValues[i]);
		}
		var answers = ans;

		for(var no=0;no<divs.length;no++){
			if(divs[no].className=='clsSectionSmallBox'){
				divs[no].onmousedown = initDragDrop;
				answers[answers.length] = divs[no];
				arrayOfAnswers[arrayOfAnswers.length] = divs[no];
			}
		}
		document.documentElement.onmouseup = dragDropEnd;
		document.documentElement.onmousemove = dragDropMove;
		return answers;
	}
	
setTimeout('initDragable()',1000) ;