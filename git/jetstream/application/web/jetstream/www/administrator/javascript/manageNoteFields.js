// JavaScript Document
$(document).ready(function() {   
$('#frmNoteFields').ajaxForm({
	dataType: 'json', 
	success: processPostAddNoteField
});
});
function processPostAddNoteField(result)
{
	$('.error').html('');
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);
	}
	if(result.STATUS=='OK')
	{
		if(result.GETFORMATTEDVAR=='UPDATE')
		{
			document.getElementById('note_fields_td').innerHTML = '';
			alert('Template updated successfully.');
			for(i=0;i<noteTemplates.length;i++){
				if(noteTemplates[i]['NoteTemplateID'] == result.TempId){
					noteTemplates[i]['TemplateName'] = result.TemplateName;
					var formType = '';
					if(result.FormType == -1)
						formType = 'Default';
					else if(result.FormType == 1)
						formType = 'Account';
					else if(result.FormType == 2)
						formType = 'Contact';
					else if(result.FormType == 3)
						formType = 'Event';
					
					noteTemplates[i]['FormType'] = formType;
				}
			}
			manageNoteFields();
			clerTemp();
			//window.location='?';
		}
		else
		{
			//alert(noteTemplates.length);
			var arrLength = noteTemplates.length
			if(!noteTempExists){
				noteTempExists = true;
				arrLength =0;
			}
			var type = '';
			tempX = new Array();
			tempX['NoteTemplateID'] = result.TempId;
			tempX['TemplateName'] = result.TemplateName;
			if(result.FormType == -1)
				type = 'Default';
			else if(result.FormType == 1)
				type = 'Account';
			else if(result.FormType == 2)
				type = 'Contact';
			else if(result.FormType == 3)
				type = 'Event';
			
			tempX['FormType'] = type;			
			noteTemplates[arrLength] = tempX;			
			manageNoteFields();
			document.getElementById('note_fields_td').innerHTML = '';
			$.blockUI({ message: $('#div_note_template_fields'),
				css: { 
				padding: '10px',
				backgroundColor: '#FFFFFF',
				opacity: '.9', 
				color: '#000000',
				top:  (jQuery(window).height() - jQuery('#div_note_template_fields').height()) /4 + 'px', 
				left: (jQuery(window).width() - jQuery('#div_note_template_fields').width()) /3 + 'px', 
				width: '500px',
				height:'400px',
				overflow:'scroll'
			}});
			clerTemp();
			populateTemplateFieldInner(result.TempId);
		}
	}
	else
	{
		for(var key in result.ERROR)
			$('#spn_'+key).html(result.ERROR[key]);
	}
}

function changeNoteFormType(formType){
	var compId = document.getElementById('NoteCompanyId').value;
	var frmType = formType.value;
	
	$.post(
	'../../ajax/ajax.manageNoteFields.php',
	{
		FormType : frmType,
		action : 'getNoteTemplatesByFormType',
		CompanyId : compId
	}
	,processPopulateNoteInner
	);		
}

function processPopulateNoteInner(result){
	try
	{
		result = eval(result);
	}
	catch(e)
	{
		result = JSON.decode(result);
	}

	var noteTemp = result;
	var inner = '';
	if(noteTemp.length > 0){
		inner = '<table width="600px">';
		for(i=0;i<noteTemp.length;i++){
			inner = inner + '<tr id="notesTemp_'+ noteTemp[i]['NoteTemplateID'] +'"><td class="leftcell" onclick="javascript:editNoteTemplate('+ noteTemp[i]['NoteTemplateID'] +')" style="width:9%"><img src="../images/edit.jpg" alt="edit" /></td><td onclick="javascript:deleteNoteTemplate('+ noteTemp[i]['NoteTemplateID'] +')" style="width:9%"><img src="../images/del.png" alt="edit" /></td><td style="width:31%">'+ noteTemp[i]['TemplateName'] +'</td><td>'+ noteTemp[i]['FormType'] +'</td></tr>';	
		}
		inner = inner + '</table>';
	}else{
		inner = '<table width="600px"><tr><td class="leftcell"></td></tr></table>';	
	}
	document.getElementById('innerNoteFields').innerHTML = inner;
	//document.getElementById('NoteTemplateName').value = '';
	document.getElementById('noteSubmit').value = 'Save'
}

function clearNoteField(){
	addNoteOptions = 0;
	clearNoteTempFields();
	document.getElementById('temp_fields_td').innerHTML = '';
	$.unblockUI();
	//window.location='?';
}


function populateTemplateFieldInner(tempId){
	$.post('../../ajax/ajax.manageNoteFields.php',
		{
			action :'getTemplateFields',
			tempId : tempId,
			companyId : companyId
		},
		function(result)
		{	
			try
			{
				result = eval(result);
			}
			catch(e)
			{
				result = JSON.decode(result);
			}
			var tempFields = result;
			var inner = '';
			if(tempFields.length > 0){
				inner = '<table width="400px">';
				for(i=0;i<tempFields.length;i++){
					inner = inner + '<tr id="notes_'+ tempFields[i]['NoteMapID'] +'"><td class="leftcell" onclick="javascript:editNoteField('+ tempFields[i]['NoteMapID'] +')" style="width:9%"><img src="../images/edit.jpg" alt="edit" /></td><td onclick="javascript:deleteNoteField('+ tempFields[i]['NoteMapID'] +')" style="width:9%"><img src="../images/del.png" alt="edit" /></td><td style="width:41%">'+ tempFields[i]['LabelName'] +'</td><td>'+ tempFields[i]['FieldType'] +'</td></tr>';	
				}
				inner = inner + '</table>';
			}else{
				inner = '<table width="400px"><tr><td class="leftcell"></td></tr></table>';	
			}
			document.getElementById('tempFieldInner').innerHTML = inner;
			document.getElementById('NoteTemplateId').value = tempId;
			document.getElementById('TemplateId').value = tempId;
		}
	);
}

function saveNoteTempField(){

	var act = 'saveTemplateField';
	if(document.getElementById('tempFieldSubmit').value == 'Update')
		act = 'updateTemplateField';
	
	var tempId = document.getElementById('TemplateId').value;
	var queryString = '';
	
	queryString=$('#frmNoteTempFields').formSerialize()+'&action='+act+'&companyId='+companyId;
	$.post('../../ajax/ajax.manageNoteFields.php?'+queryString,
		function(result)
		{	
			try
			{
				result = eval(result);
			}
			catch(e)
			{
				result = JSON.decode(result);
			}
			if(result.STATUS == 'OK'){
				if(document.getElementById('temp_fields_td'))
					document.getElementById('temp_fields_td').innerHTML = '';
				document.getElementById('spn_NoteTempFieldName').innerHTML = '';
				document.getElementById('spn_NoteFieldType').innerHTML = '';
				populateTemplateFieldInner(tempId);
				clearNoteTempFields();
				addNoteOptions =0;
			}else
			{
				for(var key in result.ERROR)
					$('#spn_'+key).html(result.ERROR[key]);
			}
		}
	);	
}

function clearNoteTempFields(){
	document.getElementById('NoteTempFieldName').value = '';
	document.getElementById('NoteFieldType').value = -1;
	document.getElementById('tempFieldSubmit').value = 'Save'
}

function clerTemp(){
	document.getElementById('NoteTemplateName').value = '';
	document.getElementById('NoteFormType').value = 0;	
	document.getElementById('noteSubmit').value = 'Save';
}

function editNoteTemplate(tempId){
	$.post('../../ajax/ajax.manageNoteFields.php',
		{
			action :'getTemplate',
			tempId : tempId,
			companyId : companyId
		},
		function(result)
		{	
			try
			{
				result = eval(result);
			}
			catch(e)
			{
				result = JSON.decode(result);
			}
			if(result.STATUS == 'OK'){
				document.getElementById('NoteTemplateName').value = result.TemplateName;
				document.getElementById('NoteFormType').value = result.FormType;
				document.getElementById('NoteTemplateId').value = tempId;
				document.getElementById('noteSubmit').value = 'Update';
			}
		}
	);		
	
	$.blockUI({ message: $('#div_note_template_fields'),
		css: { 
		padding: '10px',
		backgroundColor: '#FFFFFF',
		opacity: '.9', 
		color: '#000000',
		top:  (jQuery(window).height() - jQuery('#div_note_template_fields').height()) /4 + 'px', 
		left: (jQuery(window).width() - jQuery('#div_note_template_fields').width()) /3 + 'px', 
		width: '500px',
		height:'400px',
		overflow:'scroll'
	}});
	populateTemplateFieldInner(tempId);
}

function deleteNoteTemplate(tempId){
	document.getElementById('note_fields_td').innerHTML = '';
	$.post(
	'../../ajax/ajax.manageNoteFields.php',
	{
		TempId : tempId,
		action : 'DeleteTemplate'
	}
	,processDeleteTemplate
	);		
}

function processDeleteTemplate(result){
	try
	{
		result = eval(result);
	}
	catch(e)
	{
		result = JSON.decode(result);
	}
	if(result.STATUS == 'OK')
	{
		alert('Note Template deleted successfully');
		document.getElementById('notesTemp_'+result.NoteTempId).style.display='none';	
	}
	else
		alert('Unable to find information for this template.');
}

function editNoteField(noteFieldId){
	document.getElementById('note_fields_td').innerHTML = '';
	$.post(
	'../../ajax/ajax.manageNoteFields.php',
	{
		NoteFieldId : noteFieldId,
		action : 'EditField',
		companyId : companyId
	}
	,processEditNote
	);		
}

function processEditNote(result){
	try
	{
		result = eval(result);
	}
	catch(e)
	{
		result = JSON.decode(result);
	}
	if(result.STATUS == 'OK')
	{
		document.getElementById('NoteTempFieldName').value = result.FieldName;
		document.getElementById('NoteFieldType').value = result.FieldType;
		document.getElementById('NoteFieldMapId').value = result.NoteMapId;
		document.getElementById('tempFieldSubmit').value = 'Update';	
		if(result.FieldType == 'DropDown' || result.FieldType == 'RadioButton'){
			editNoteOptionList(result);
		}
	}
	else
		alert('Unable to find information for this field.');
}

function deleteNoteField(noteFieldId){

	document.getElementById('note_fields_td').innerHTML = '';
	$.post(
	'../../ajax/ajax.manageNoteFields.php',
	{
		NoteFieldId : noteFieldId,
		action : 'DeleteField',
		CompanyId : companyId
	}
	,processDeleteNote
	);		
}

function processDeleteNote(result){
	try
	{
		result = eval(result);
	}
	catch(e)
	{
		result = JSON.decode(result);
	}
	if(result.STATUS == 'OK')
	{
		alert('Note Field deleted successfully.');
		document.getElementById('notes_'+result.NoteMapId).style.display='none';	
	}
	else
		alert('Unable to find information for this field.');
}

function checkNoteFieldType(){
		document.getElementById('spn_NoteFieldType').innerHTML = '';
		var fieldType = document.getElementById('NoteFieldType').value;
		if(fieldType == 'DropDown' || fieldType == 'RadioButton'){
			$('#div_note_template_fields').block({ message: $('#noteOptionListDiv'),
				css: { 
				padding: '10px',
				backgroundColor: '#FFFFFF',
				opacity: '.9', 
				color: '#000000',
				top:  (jQuery(window).height() - jQuery('#div_note_template_fields').height()) /4 + 'px', 
				left: (jQuery(window).width() - jQuery('#div_note_template_fields').width()) /3 + 'px', 
				width: '460px',
				height:'360px',
				overflow:'scroll'
			}});
		}else{
			return;	
		}
}
var addNoteOptions = 0;

function cancelNoteOption(){
	document.getElementById('noteOptionSetvalues').innerHTML=''; 
	document.getElementById('noteOptionSetName').value='';
	document.getElementById('NoteTempFieldName').value = '';
	document.getElementById('NoteFieldType').value = '-1';
	document.getElementById('noteOptionListError').innerHTML='';
	document.getElementById('note_fields_td').innerHTML = '';
	if(document.getElementById('temp_fields_td'))
		document.getElementById('temp_fields_td').innerHTML = '';
	addNoteOptions = 0;
	$('#div_note_template_fields').unblock();
}

function editNoteOptionList(result){
	//alert(result.NoteOptions);
	document.getElementById('noteMapId').value = result.NoteMapId;
	div = document.getElementById('addNoteOptionElementsDiv');
	opt = result.NoteOptions;
	inner = '';
	tdInner = '';
	for(i=0;i<opt.length;i++){
		//alert(opt[i]['NoteOptionID'])	;
		//inner = inner + '<div style="height:25px;"><input type="text" name="optionList[]" id="optionList[]" value="'+ opt[i]['OptionName'] +'"></div>';
		//tdInner = tdInner + '<input type="hidden" id="noteOptions[]" value="'+ opt[i]['OptionName'] +'" name="noteOptions[]" />';
		
		inner = inner + '<div style="height:25px;"><input type="text" name="option_'+opt[i]['NoteOptionID']+'" id="option_'+opt[i]['NoteOptionID']+'" value="'+ opt[i]['OptionName'] +'"></div>';
	}
	div . innerHTML =inner;
	document.getElementById('note_fields_td').innerHTML = tdInner;
	
	$('#div_note_template_fields').block({ message: $('#editNoteOptionListDiv'),
		css: { 
		padding: '10px',
		backgroundColor: '#FFFFFF',
		opacity: '.9', 
		color: '#000000',
		top:  (jQuery(window).height() - jQuery('#editNoteOptionListDiv').height()) /4 + 'px', 
		left: (jQuery(window).width() - jQuery('#editNoteOptionListDiv').width()) /3 + 'px', 
		width: '500px',
		height:'400px',
		overflow:'scroll'
	}});
	
}

var editOptions = 1;
function addEditNoteOptionList()
{
	inner='';
	for(i=0;i<5;i++){
		newDiv=document.createElement('div');		
		newDiv.innerHTML ='<input type="text" name="optionList[]" id="optionList[]" value=""/>';
		document.getElementById('addNoteOptionElementsLeftDiv').appendChild(newDiv);
	}
}

function saveEditNoteOptions(){
	//var optionList = document.getElementById('optionList[]').value;
	//alert(optionList);
	queryString=$('#frmEditNoteOptions').formSerialize()+'&action=saveEditNoteOption';
		$.post('../../ajax/ajax.manageNoteFields.php?'+queryString,processSaveEditNoteOptionsFields);
	$('#div_note_template_fields').unblock();
}

function processSaveEditNoteOptionsFields(){
	//document.getElementById('noteOptions[]').value = '';
	document.getElementById('addNoteOptionElementsLeftDiv').innerHTML = '';
	document.getElementById('editNoteOptionError').innerHTML = '';
}
function cancelEditNoteOptionList(){
	//document.getElementById('noteOptions[]').value = '';
	document.getElementById('addNoteOptionElementsLeftDiv').innerHTML = '';
	document.getElementById('editNoteOptionError').innerHTML = '';
	$('#div_note_template_fields').unblock();
}

function addNoteOptionList(){
	var noOfCols = 5;
	var inner = '';
	for(i=0;i<noOfCols;i++)
	{
		newDiv=document.createElement('div');
		newDiv.innerHTML = '<input type="text" name="noteOptionsValues'+ addNoteOptions +'" id="noteOptionsValues'+ addNoteOptions +'" value=""/>';
		document.getElementById('noteOptionSetvalues').appendChild(newDiv);
		addNoteOptions = addNoteOptions + 1;	
	}		
}

function saveNoteOptionList(){
	var fieldValue = document.getElementById('noteOptionSetName').value;
	var countOptions = 0;
	var optionValues = new Array();
	fieldValue = fieldValue.replace("'","\'");
	fieldValue = fieldValue.replace(/^\s+|\s+$/g, '') ;
	if(fieldValue == '' ){
		document.getElementById('noteOptionListError').innerHTML = 'LabelName is mandatory';
	return false;
	}else{
		var j =0;
		for(i=0;i<addNoteOptions;i++){
			var optionsField = 'noteOptionsValues'+i; 
			if((document.getElementById(optionsField).value).replace(/^\s+|\s+$/g, '') != ''){
				optionValues[j] = document.getElementById(optionsField).value;
				countOptions = countOptions + 1;
				j = j+1;
			}
		}
		if(countOptions < 1){
			document.getElementById('noteOptionListError').innerHTML = 'Cannot create a blank Option Set.';
			return false;
		}
		var tdInner = '';
		for(i=0;i<optionValues.length;i++){			
			tdInner = tdInner + '<input type="hidden" id="noteOptions[]" value="'+ optionValues[i] +'" name="noteOptions[]" />';
		}
		document.getElementById('temp_fields_td').innerHTML = tdInner;
	}
	document.getElementById('NoteTempFieldName').value = fieldValue;
	document.getElementById('noteOptionSetvalues').innerHTML=''; 
	document.getElementById('noteOptionSetName').value='';
	document.getElementById('noteOptionListError').innerHTML='';
	addOptions = 0;
	$('#div_note_template_fields').unblock();
}
	
function saveUserTemplate(){
	var hasError = checkUserTemplateFields();
	if(hasError)
		return false;
	
	var formSel = document.getElementById('SelectFormType').value;
	var userId = document.getElementById('usersSelect').value;
	var tempSel = document.getElementById('userTemplatesSelect').value;
	$.post('../../ajax/ajax.manageUserTemplates.php',
		{
			action :'saveUserTemplate',
			userId : userId,
			formType : formSel,
			template : tempSel,
			companyId : companyId
		},
		function(result)
		{	
			try
			{
				result = eval(result);
			}
			catch(e)
			{
				result = JSON.decode(result);
			}
			if(result.STATUS=='OK'){
				alert('User template set sucessfully.');	
			}
		}
	);		
	
}

function onFormChange(){
	var formSel = document.getElementById('SelectFormType').value;
	$.post('../../ajax/ajax.manageUserTemplates.php',
		{
			action :'getFormTemplate',
			formType : formSel,
			companyId : companyId
		},
		function(result)
		{	
			try
			{
				result = eval(result);
			}
			catch(e)
			{
				result = JSON.decode(result);
			}
			var templateFieldInner = '<select id="userTemplatesSelect" name="userTemplatesSelect" ><option value="-1"> </option>';
			for(j=0;j<result.length;j++){
				templateFieldInner = templateFieldInner + '<option value='+result[j]['NoteTemplateID'] +'>'+result[j]['TemplateName']+'</option>';	
			}
			templateFieldInner = templateFieldInner + '</select><br/><span class="clsError  error" id="spn_SelectTemplate"></span>';
			document.getElementById('selectTemplate_td').innerHTML = templateFieldInner;
			changeTempValue();
		}
	);		
}
	
function changeTempValue(){
	document.getElementById('userTemplatesSelect').value = -1;
	var formSel = document.getElementById('SelectFormType').value;
	var userId = document.getElementById('usersSelect').value;
	$.post('../../ajax/ajax.manageUserTemplates.php',
		{
			action :'getUserNoteTemplate',
			userId : userId,
			formType : formSel,
			companyId : companyId
		},
		function(result)
		{	
			try
			{
				result = eval(result);
			}
			catch(e)
			{
				result = JSON.decode(result);
			}
			document.getElementById('userTemplatesSelect').value = result['NoteTemplateID'];
		}
	);		
}

function cancelUserTemplate(){
	document.getElementById('SelectFormType').value = -2;
	document.getElementById('usersSelect').value = -1;
	document.getElementById('userTemplatesSelect').value = -1;
	document.getElementById('spn_SelectFormType').innerHTML = '';
	document.getElementById('spn_SelectUser').innerHTML = '';
	document.getElementById('spn_SelectTemplate').innerHTML = '';
}

function checkUserTemplateFields(){
	var hasError = false;
	var formSel = document.getElementById('SelectFormType').value;
	var userId = document.getElementById('usersSelect').value;
	var tempSel = document.getElementById('userTemplatesSelect').value; 
	if(formSel == '-2'){
		document.getElementById('spn_SelectFormType').innerHTML = 'Please select a form type.';
		hasError = true;
	}
	if(userId == '-1'){
		document.getElementById('spn_SelectUser').innerHTML = 'Please select a user.';	
		hasError = true;
	}
	if(tempSel == '-1'){
		document.getElementById('spn_SelectTemplate').innerHTML = 'Please select a template.';	
		hasError = true;
	}
	if(hasError)
		return true;
	else
		return false;
}