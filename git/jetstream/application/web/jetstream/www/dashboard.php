<?php

define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));
	
require_once(BASE_PATH . '/include/mpconstants.php');
require_once(BASE_PATH . '/include/class.SessionManager.php');
require_once(BASE_PATH . '/mpower/class.Header.php');
require_once(BASE_PATH . '/mpower/class.Salespeople.php');
require_once(BASE_PATH . '/mpower/class.DashSwimlanes.php');
require_once(BASE_PATH . '/mpower/class.Footer.php');
require_once BASE_PATH . '/include/class.ReportingTree.php';
require_once(BASE_PATH . '/include/class.Language.php');
require_once(BASE_PATH . '/mpower/lib.html.php');
SessionManager::Init();
SessionManager::Validate();

$page_title = 'M-Power&trade; Dashboard';

$header = new Header();

$salespeople = new Salespeople();
$salespeople->url = 'dashboard.php';

$swimlanes = new DashSwimlanes();
$swimlanes->target = $_SESSION['tree_obj']->GetTarget();

$footer = new Footer();

CreatePage($page_title, array($header, $salespeople, $swimlanes, $footer));
