<?php

define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.Language.php';

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<title>M-Power&trade;</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
		<style type="text/css">
			.body {
				font-family:arial,sans-serif;
				margin-left:3em;
				margin-right:5em;
			}
			
			div.errormsg {
				color:red;
				font-family:arial,sans-serif;
				font-size:smaller;
			}
			div.success {
				color: black;
				font-family:arial,sans-serif;
				font-size: smaller;
			}		
			font.errormsg {
				color:red;
				font-family:arial,sans-serif;
				font-size:smaller;
			}
			
		</style>	
		
	</head>
	<body>	
	
<?php

$valid = false;
$error_msg = '';

if (isset($_POST['submit'])) {


	$company = isset($_POST['company']) ? strtolower(trim($_POST['company'])) : '';
	$username = isset($_POST['username']) ? strtolower(trim($_POST['username'])) : '';
	$password = isset($_POST['password']) ? $_POST['password'] : '';
	$new_password = isset($_POST['newPassword']) ? trim($_POST['newPassword']) : '';
	$new_confirm_password = isset($_POST['newConfirmPassword']) ? $_POST['newConfirmPassword'] : '';

	if (!empty($company) && !empty($username) && !empty($password)) {
		$sql = "SELECT Logins.ID, Logins.Password FROM Company INNER JOIN Logins on Company.CompanyID = Logins.CompanyID
			WHERE Company.DirectoryName = ? and Logins.UserID = ?";
		
//		$sql = 'SELECT L.ID FROM company C, Logins L 
//				WHERE DirectoryName = ? AND C.CompanyID = L.CompanyID AND 
//				L.UserID = ? AND Passwordz = ?';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $company), array(DTYPE_STRING, $username));
		$login = DbConnManager::GetDb('mpower')->Execute($sql);	
		
		if (count($login) == 0) {		
			$error_msg = 'Please provide you company, username and password correctly.';
		} else {
			$password_strength = 6;
			$loginid = $login[0]['ID'];
			$db_password = $login[0]['Password'];
			
			if ($db_password != crypt($password, $db_password)) {
				$error_msg = 'Please provide you company, username and password correctly.';
			} else {
				if (strlen($new_password) >= $password_strength) {
					if ($new_password == $new_confirm_password) {
						$valid =  true;
					} else {
						$error_msg = 'Password does not match the confirm password.';
					}
				} else {
					$error_msg = "Minimum of $password_strength characters in length.";
				}
				
				if ($valid) {		
					$newPassword = $_POST['newPassword'];
					$sql = 'UPDATE Logins SET Password = ? WHERE ID = ?';
					$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, crypt($newPassword)), array(DTYPE_INT, $loginid));		
					
					if (DbConnManager::GetDb('mpower')->Exec($sql)) {
						echo 'Your password has been successfully changed.';
					} else {
						echo 'Error in new password update. Please try again.';				
					}
				}
			}
		}  
	}	
}

if (!$valid) {
?>

	<div class="body">
		<b>Change your password?</b><br>
		<table border="0" cellpadding="0" cellspacing="0">
			<tbody>
			<tr valign="top">
				<td>
				<form action="changepassword.php" method="post" id="forgotpasswd">
				<table border="0" cellpadding="0" cellspacing="0" width="" height="1%">
				<tbody>
				<tr>
				<td style="padding-top: 10px; padding-bottom: 10px;" valign="top">
				<font size="-1">Please enter your new Mpower/Jetstream password. Password must be minimum of 6 characters in length.</font>
				<br>
					<table border="0" cellpadding="2" cellspacing="5"><tbody>
					<tr>
					<td align="center" valign="top">
					<table bgcolor="#cbdced" border="0" cellpadding="2" cellspacing="0" width="100%">
					<tbody><tr><td>
						<table border="0" cellpadding="5" cellspacing="0" width="100%">
						<tbody>
						
							<tr>
							<td align="right" bgcolor="#ffffff" valign="top" nowrap="nowrap">
							<b><font size="-1"><?php echo Language::__get('Company')?>:</font>&nbsp;&nbsp;</b></td>
							<td align="left" bgcolor="#ffffff" colspan="2"><input name="company" value="<?php if (isset($company)) echo $company;?>" size="15" type="text"></td>
							</tr>
						
							<tr><td align="right" bgcolor="#ffffff" valign="top" nowrap="nowrap">
							<b><font size="-1" face="Arial, sans-serif"><?php echo Language::__get('Username')?>:</font>&nbsp;&nbsp;</b></td>
							<td align="left" bgcolor="#ffffff" colspan="2"><input name="username" value="<?php if (isset($username)) echo $username;?>" size="15" type="text"></td>
							
							
							<tr><td align="right" bgcolor="#ffffff" valign="top" nowrap="nowrap">
							<b><font size="-1" face="Arial, sans-serif"><?php echo Language::__get('Password')?>:</font>&nbsp;&nbsp;</b></td>
							<td align="left" bgcolor="#ffffff" colspan="2"><input name="password" value="" size="15" type="password"></td>
							
	
							<tr><td align="right" bgcolor="#ffffff" valign="top" nowrap="nowrap">
							<b><font size="-1" face="Arial, sans-serif">New Password:</font>&nbsp;&nbsp;</b></td>
							<td align="left" bgcolor="#ffffff" colspan="2"><input name="newPassword" value="" size="15" type="password"></td>
	
							<tr>
							<td align="right" bgcolor="#ffffff" valign="top" nowrap="nowrap">
							<b><font size="-1" face="Arial, sans-serif">Confirm New Password:</font>&nbsp;&nbsp;</b></td>
							<td align="left" bgcolor="#ffffff"><input name="newConfirmPassword" value="" size="15" type="password"></td>																		
							<td align="left" bgcolor="#ffffff"><input value="Submit" name="submit" type="submit"></td>
							</tr>
						</tbody>
						</table>
						<div class="errormsg"><?php echo $error_msg;?></div>
					</td></tr>
					</tbody>
					</table>
				</td></tr>
				</tbody>
				</table></td></tr>
				</tbody>
				</table>
				</form>
			</td>		
			</tr>
			</tbody>
		</table>
		
	</div>
<?php
}
?>

	</body>
</html>