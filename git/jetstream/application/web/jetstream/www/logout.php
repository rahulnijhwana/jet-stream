<?php
define ( 'BASE_PATH', realpath ( dirname ( __FILE__ ) . '/..' ) );

require_once BASE_PATH . '/include/class.SessionManager.php';

SessionManager::Init();

$path = (isset($_SESSION['USER']['COMPANY'])) ? './?company=' . $_SESSION['USER']['COMPANY'] : '.';

SessionManager::Destroy();
if(isset($_COOKIE) && isset($_COOKIE["mpower_companyid"]))
{
	setcookie ('mpower_companyid', '', time() - 36000);
	setcookie('currentuser_loginid','',time()-36000);
	setcookie('mpower_pwd','',time()-36000);
	setcookie('mpower_userid','',time()-36000);
	setcookie('mpower_effective_userid','',time()-36000);
	setcookie('currentuser_isadmin','',time()-36000);

	setcookie('currentuser_loginid','',time()-36000);
	setcookie('account_name','',time()-36000);
	setcookie('contact_first_name','',time()-36000);
	setcookie('contact_last_name','',time()-36000);	
}

if(isset($_COOKIE['MiniCalMonth'])) {
	setcookie('MiniCalMonth','',time()-36000);
	setcookie('MiniCalYear','',time()-36000);
}
if(isset($_COOKIE['DashboardSearch'])) {
	setcookie('DashboardSearch','',time()-36000);
	setcookie('SearchString','',time()-36000);
	setcookie('SearchType','',time()-36000);
	setcookie('RecordsPerPage','',time()-36000);
	setcookie('PageNo','',time()-36000);
	setcookie('AssignedSearch','',time()-36000);
	setcookie('InactiveType','',time()-36000);
}
if(isset($_COOKIE['LastVisitedContact'])) {
	setcookie('LastVisitedContact','',time()-36000);
}


header("Location: $path");

exit;

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';


$company_id = isset ( $_GET ['CID'] ) && ! empty ( $_GET ['CID'] ) ? urldecode ( $_GET ['CID'] ) : '';

$sql = 'SELECT DirectoryName, LogoName FROM company where CompanyID = ?';
$sql = SqlBuilder ()->LoadSql ( $sql )->BuildSql ( array (DTYPE_INT, $company_id ) );
$company_record = DbConnManager::GetDb ( 'mpower' )->GetOne ( $sql );

$logo = '';
$dir = '';

// This translates the company logo info from the DB into the v3 name and
if (! is_null ( $company_record->LogoName )) {
	$logo_file = 'images/companylogos/' . str_replace ( 'logo', $company_record->DirectoryName, $company_record->LogoName );
	$logo = "<img src=\"$logo_file\">";
	$dir = 'Please click <a href="index.php?company=' . $company_record->DirectoryName . '">here</a> to access the M-Power system.';
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<title>M-Power&trade;</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
		<style type="text/css">
		div.aligncenter {
			margin-left: auto;
			margin-right: auto;
			text-align: center;
			width: 500px;
			border: 0px solid black;
		}
		</style>
	</head>
	<body>
		<div class="aligncenter">
		<p>Thank you for using M-Power&trade;</p>
		<p><?=$logo?></p>
		<p><?=$dir?></p>
		</div>
	</body>
</html>
