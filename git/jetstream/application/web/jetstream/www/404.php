<?

/*
In order to avoid creating a directory for each company that signs up for M-Power, we
use this custom 404 script to parse the path sent to the server and route the user to 
the login script if it matches an active company in M-Power
*/

define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

//require_once BASE_PATH . '/include/lib.array.php';


$uri = str_replace('/slip/', '', $_SERVER['REQUEST_URI']);
$uri = str_replace('/', '', $uri);

// So, if what is left is numbers and letters only, lets check to see if it is a valid M-Power company
if (ereg('^[A-Za-z0-9]*$', $uri)) {
	require_once BASE_PATH . '/include/class.DbConnManager.php';
	require_once BASE_PATH . '/include/class.SqlBuilder.php';
	
	$company_sql = 'SELECT CompanyID FROM company WHERE Disabled != 1 AND DirectoryName = ?';
	$company_sql = SqlBuilder()->LoadSql($company_sql)->BuildSql(array(DTYPE_STRING, $uri));
	
	if ($company_record = DbConnManager::GetDb('mpower')->GetOne($company_sql)) {
		// It matches - redirect the browser to the login page for that company
		header("HTTP/1.0 200 OK");
		$real_loc = "http://{$_SERVER['SERVER_NAME']}" . dirname($_SERVER['SCRIPT_NAME']) . "/?company=$uri";
		header("Location: $real_loc");
		exit();
	}

}

// The real 404 output should go here
//echo '404 Error';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>404: Not found : M-Power</title>
<!--link href="css/404.css" rel="stylesheet" type="text/css" media="all" /-->
<style type="text/css">
body {
	font-size: 62.5%;
	font-family: Helvetica, Arial, Tahoma, Geneva, Verdana, sans-serif;
	margin: 0;
	padding: 0;
	text-align: center;
	background-color: #FFF;
	color: #666;
}

#content-fullcol h1 {
	font-size: 3.2em;
	color: #367E29;
	margin: 0;
	padding: 0 0 8px 0;
	font-weight: normal;
}

#content-fullcol p {
	font-size: 1.2em;
	line-height: 1.25em;
	color: #666;
	margin: 0;
	padding: 0 0 10px 0;
}

#container-mid {
	margin: 0 auto;
	width: 879px;
	padding: 0 15px 0 0;
	text-align: left;
}

#content-wrapper {
	padding: 100px 15px 30px 15px;
}
</style>

</head>
<body>
<div id="container-mid">
<div id="content-wrapper">
<div id="content-fullcol">
<h1>404: Not found</h1>
<p><span>The page you are looking for may have been removed, had its
name changed, or is temporarily unavailable.</span></p>
<p>--------------------------------------------------------------------------------</p>
<p class="style1">Please try the following:</p>

<ul>
	<li>If you typed the page address in the Address bar, make sure that it
	is spelled correctly.</li>
	<li>Open the <a href="https://www.mpasa.com/">www.mpasa.com</a> home
	page, and then look for links to the information you want.</li>
	<li>Click the Back button to try another link. <br>
	</li>
</ul>
</div>
</div>
</div>


</body>
</html>