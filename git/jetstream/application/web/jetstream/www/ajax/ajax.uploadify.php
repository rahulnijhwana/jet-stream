<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.s3.php';

SessionManager::Init();
SessionManager::Validate();


//	file_put_contents('D:\\webtemp\\test.txt',"HE::PW");

if (!empty($_FILES)) {
	$tempFile = $_FILES['Filedata']['tmp_name'];
	
	if (!empty($_GET['action'])){
		$params = explode ('|', $_GET['action']); 
		// type, id , accountid|contactid , id 
		$path['type'] = $params[2] ;
		$path['id'] =  $params[3]  ;
		$path['parentID'] = $params[1]  ;
		$path['companyID'] = $params[4]; 
		$path['userID'] = $params[5]; 		
		$path['size'] = $_FILES['Filedata']['size']; 

	}
	$fileNew = CreateNewFile( $tempFile, $_FILES['Filedata']['name'], $path) ; 

}

function CreateNewFile( $tempFile, $name, $path ){
	$CompanyID = $path['companyID'] ;
	$UserID =  $path['userID'] ; 
	
	// check file or folder 
	// check enough parameter 
	// if file, upload file 
	// insert & return created ID 
	// 0 for failure 

	$fileParts  = pathinfo($name);		
	
	// check if it is an acceptable file extnsions 		
	$sql = "SELECT FileTypeID, ContentType FROM FileType WHERE Extension = ? "; 
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $fileParts['extension'])); 
	$fileInfo = DbConnManager::GetDb('mpower')->Exec($sql);


	if (empty ($fileInfo[0]['FileTypeID']) ){
		print "ERROR - File Type is unrecognized.";
		return "ERROR";
	}

	$contentType = $fileInfo[0]['ContentType']; 
	$filetypeid = $fileInfo[0]['FileTypeID'] ;
	
	// update database first 
	// returns JetFileID 
	$labelName = $fileParts['basename'] ;
	$type = $path['type'] ; // 1 - contact ; 2- account ; 3 - share 
	$id  = $path['id'];
	$parentID = ($path['parentID'] == 0 ) ? ($path['parentID'] + 1) : $path['parentID'];
	
	$insertsql ="INSERT INTO JetFile VALUES (?, ?, ?, ?, ?, ?, GETDATE(), '', NULL, '', NULL, ?, ? , 0, ? )"; 
	$insertsql = SqlBuilder()->LoadSql($insertsql)->BuildSql(array(DTYPE_STRING, $labelName), array(DTYPE_INT, $CompanyID), array(DTYPE_INT, $id), 
	array(DTYPE_INT, $parentID), array(DTYPE_INT, '1'), array(DTYPE_INT, $UserID), array(DTYPE_STRING, $type), array(DTYPE_INT, $fileInfo[0]['FileTypeID']), array(DTYPE_INT, $path['size']));

	$newjetfileid = DbConnManager::GetDb('mpower')->DoInsert($insertsql);
	if ( empty($newjetfileid) ){
		print "ERROR - Database Connection Failed. ";
		return "ERROR";
	}
	$filepath = $CompanyID . '/' . $path['type'] . '/' . $path['id'] . '/'. $newjetfileid ."." . strtolower($fileParts['extension']) ;

	// the upload file 
	$jetfile = new JetStreamS3 ; 
	$jetfile->putFile( $filepath  , $contentType ,$tempFile);	
	print $newjetfileid  ; 
	return 1 ; 

	// FILE Upload done before this call 
	
}
