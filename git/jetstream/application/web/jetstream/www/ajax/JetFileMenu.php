<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.s3.php';

SessionManager::Init();
SessionManager::Validate();

$menu['folder'][] = array ( 'Label'=> 'Rename Folder', 'javascript' => 'jetfileRename');
$menu['folder'][] = array ( 'Label'=> 'Delete Folder', 'javascript' => 'jetfolderDelete');
$menu['folder'][] = array ( 'Label'=> 'Move Folder', 'javascript' => 'jetfileMove');
$menu['folder'][] = array ( 'Label'=> 'New Folder', 'javascript' => 'jetfolderCreatenew');
$menu['folder'][] = array ( 'Label'=> 'Attach File', 'javascript' => 'jetfileUpload');

$menu['file'][] = array ( 'Label'=> 'Rename File', 'javascript' => 'jetfileRename');
$menu['file'][] = array ( 'Label'=> 'Delete File', 'javascript' => 'jetfileDelete');
$menu['file'][] = array ( 'Label'=> 'Move File', 'javascript' => 'jetfileMove');
$menu['file'][] = array ( 'Label'=> 'Download File', 'javascript' => 'DownloadFile');
$menu['file'][] = array ( 'Label'=> 'Property', 'javascript' => 'Property');


$type = $_GET['type'] ;
$id = $_GET['id'];
$category = $_GET['category'];
$cid = $_GET['cid'];

$accountid = (!empty($_GET['accountid'])) ? $_GET['accountid'] : '' ; 
$contactid = (!empty($_GET['contactid'])) ? $_GET['contactid'] : '' ; 

$params = $_SESSION['company_obj']['CompanyID'] . '|' . $_SESSION['USER']['USERID'];

$smarty->assign_by_ref('menu', $menu[$type]);
$smarty-> assign_by_ref('type', $type);
$smarty-> assign_by_ref('id', $id ); 
$smarty-> assign_by_ref('category', $category ); 
$smarty-> assign_by_ref('cid', $cid ); 
$smarty-> assign_by_ref('accountid', $accountid ); 
$smarty-> assign_by_ref('contactid', $contactid ); 
$smarty-> assign_by_ref('params', $params ); 


$smarty->display('snippet_jetfile_menu.tpl');


