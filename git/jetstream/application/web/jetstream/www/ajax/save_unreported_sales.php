<?php
/**
* @package Ajax
*/
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');
require_once(BASE_PATH . '/include/class.ReportingTree.php');

SessionManager::Init();
SessionManager::Validate();

$personid = (int) $_POST['PersonID'];
$period = (int) $_POST['Period'];
$sales = (int) $_POST['Sales'];
$revenue = (int) $_POST['Revenue'];
$year = (int) $_POST['Year'];

if (isset($sales) || isset($revenue)) {
	$sql = "IF EXISTS (SELECT * FROM UnreportedSales WHERE PersonId = $personid AND Year = $year AND Period = $period )
				UPDATE UnreportedSales
				   SET Sales = $sales
					  ,Revenue = $revenue
				 WHERE PersonId = $personid AND Year = $year AND Period = $period
			ELSE
				INSERT INTO UnreportedSales
					   (PersonId
					   ,Year
					   ,Period
					   ,Sales
					   ,Revenue)
				 VALUES (?, ?, ?, ?, ?)";
				 
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $personid), array(DTYPE_INT, $year), array(DTYPE_INT, $period), array(DTYPE_INT, $sales), 
						array(DTYPE_FLOAT, $revenue));
	
	
	if(DbConnManager::GetDb('mpower')->Exec($sql)) {
		$checksum = $personid + $period + $sales + $revenue;
		echo $checksum;
	} else {
		echo 'Error in saving';
	}
}
