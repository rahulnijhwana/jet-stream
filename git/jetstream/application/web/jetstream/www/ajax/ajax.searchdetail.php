<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
chdir (BASE_PATH . '/slipstream');
require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.ModuleCompanyContactNew.php';

SessionManager::Init();
SessionManager::Validate();

list ($type, $id) = split('_', $_POST['request_id']);

$module = new ModuleCompanyContactNew($_SESSION['company_obj']['CompanyID'], $_SESSION['USER']['USERID']);
$module->Build($type, $id, 'default_only', true);
$smarty->assign_by_ref('module', $module);
foreach ($module->template_files as $file) {
	$smarty->display($file);
}