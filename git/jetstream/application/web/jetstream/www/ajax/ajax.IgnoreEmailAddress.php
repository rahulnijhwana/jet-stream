<?php
/**
* @package Ajax
*/
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');
require_once(BASE_PATH . '/include/class.ReportingTree.php');

SessionManager::Init();
SessionManager::Validate();

$email_address = $_POST['email'];


$sql = "INSERT INTO IgnoreEmailList (PersonID, EmailAddress) VALUES (?, ?)";
$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['mpower_effective_userid']), array(DTYPE_STRING, $email_address));
DbConnManager::GetDb('mpower')->Exec($sql);