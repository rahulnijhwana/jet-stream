<?php

ini_set("memory_limit", "256M");

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';
require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.ExcelReport.php';
require_once BASE_PATH . '/include/class.ReportUtil.php';

SessionManager::Init();
//SessionManager::Validate();
$dateRange='';
$start = '';
$end = '';
$salespersonIds = $_REQUEST['salespersonIds'];


$salesperson_str = 'Salesperson(s): ';
$salesPerson_array = array();
$salesPerson_array = explode(',',$salespersonIds);

$counter = 0;
foreach($salesPerson_array as $salesperson){

	$person_rec = ReportingTreeLookup::GetRecord($_SESSION['company_obj']['CompanyID'], $salesperson);
	$user = "{$person_rec['FirstName']} {$person_rec['LastName']}";
	
	if($counter !== 0){
		$salesperson_str .= ', '.$user;
	}
	else {
		$salesperson_str .= $user;
	}
	$counter++;
}

$weighted = $_REQUEST['weighted'];

if(isset($_REQUEST['start'])){
	$start = $_REQUEST['start'];
}
if(isset($_REQUEST['end'])){
	$end = $_REQUEST['end'];
}

if ( isset($_REQUEST['action']) && ($_REQUEST['action'] == 'export')){

	BuildSalesProcessReport($weighted,$salespersonIds,$salesperson_str) ;
	exit;
}

else if ( isset($_REQUEST['action']) && ($_REQUEST['action'] == 'OppCalendarRPT')){

	BuildCalendarReport($weighted,$salespersonIds,$salesperson_str) ;
	exit;
}

else if ( isset($_REQUEST['action']) && ($_REQUEST['action'] == 'OppCalendarRPTOpps')){
	//$weighted = $_REQUEST['weighted'];

	BuildCalendarReport_opps($weighted,$salespersonIds,$salesperson_str) ;
	exit;
}

else if ( isset($_REQUEST['action']) && ($_REQUEST['action'] == 'OppCalendarRPTProds')){
	//$weighted = $_REQUEST['weighted'];
	BuildCalendarReport_prods($weighted,$salespersonIds,$salesperson_str) ;
	exit;
}
else if ( isset($_REQUEST['action']) && ($_REQUEST['action'] == 'ClosedCustRPT')){
	//if(isset($_REQUEST['months']))
	//$dateRange = $_REQUEST['months']. ' MONTHS ';
	//else $dateRange = '12 MONTHS ';
	//if(isset($_REQUEST['months']))
	//$dateRange = $_REQUEST['months'];
	//else $dateRange = 12;
	BuildClosedReport($start,$end,$salespersonIds,$salesperson_str) ;
	exit;
}

else if ( isset($_REQUEST['action']) && ($_REQUEST['action'] == 'ClosedOppRPT')){
	//if(isset($_REQUEST['months']))
	//$dateRange = $_REQUEST['months'];
	//else $dateRange = 12;
	BuildClosedReport_opps($start,$end,$salespersonIds,$salesperson_str) ;
	exit;
}

else if ( isset($_REQUEST['action']) && ($_REQUEST['action'] == 'ClosedProdRPT')){
	//if(isset($_REQUEST['months']))
	//$dateRange = $_REQUEST['months'];
	//else $dateRange = 12;
	BuildClosedReport_prods($start,$end,$salespersonIds,$salesperson_str) ;
	exit;
}

function BuildSalesProcessReport($weighted,$salespersonIds,$salesperson_str) {
	
	$dateRange = '';
	$header = array () ;

	//$header[] = "SALES PROCESS REPORT";
	//$header[] = 'WEIGHTED FORECAST REPORT'.' WITH WON AND LOST DATE WITHIN '.$dateRange;
	//$header[] = 'ACTIVE OPPORTUNITY VALUES BY CUSTOMER (BY SALES CYCLE LOCATION)';

	if($weighted == 'true') $header[] = 'FORECASTED REVENUES BY CUSTOMER (BY SALES CYCLE STAGE)';
	else $header[] = 'ACTIVE OPPORTUNITY VALUES BY CUSTOMER (BY SALES CYCLE STAGE)';


	$header2 = array();
	$header2[] = "Run Date:  ".date("n/d/Y")."          User:  ".$_SESSION['USER']['FIRSTNAME'].' '.$_SESSION['USER']['LASTNAME'];
	$outputArray[] = $header2;

	$header3 = array () ;
	$header3[] = $salesperson_str;

	$outputArray[] = $header3;

	$SCLIDs = array() ;

	$order = array() ;
	$order[] = 'Sales Cycle Step';
	
	$SCLName = array() ;
	$SCLName[] = 'Sales Cycle Stage';
	
	$line = array() ;
	$line[] = 'Client-Defined Closed %';
	
	$No_of_opp = array() ;
	$No_of_opp[] = '# Reached';

	$SCL_counter = 0;

	$start_date = date("m/d/Y", strtotime("-".$dateRange, time()));
	$sql = "SELECT SCLID, SCLPercent, SCLName FROM ot_SalesCycleLocation WHERE CompanyID = ? AND InUse = 1 ORDER BY SortingOrder";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']));

	$result_list = DbConnManager::GetDb('mpower')->GetSqlResult($sql);
	while ($result = mssql_fetch_assoc($result_list)) {
		
		$SCL_counter++;
		
		$order[] = $SCL_counter;
		$line[] = $result['SCLPercent'] / 100;//.'%';
		$SCLName[] = $result['SCLName'];
		
		$opp_sql = "SELECT count(*) as Count FROM ot_Opportunities WHERE CompanyID = ? AND SalesCycleLocID=".$result['SCLID']." AND (CloseDate is null OR CloseDate >= '".$start_date."')";
		$opp_sql = SqlBuilder()->LoadSql($opp_sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']));
		
		$opp_result_list = DbConnManager::GetDb('mpower')->GetSqlResult($opp_sql) ;
		
		while (	$opp_result = mssql_fetch_array($opp_result_list)) {
			$No_of_opp[] = $opp_result[0];
		}
		$SCLIDs[$SCL_counter]=$result['SCLID'];
		$SCLPercents[$SCL_counter]=$result['SCLPercent'];
	}

	/*
	print_r($order);
	print_r($line);
	print_r($SCLName);
	*/
	//array_pop($order);
	//array_pop($line);
	//array_pop($SCLName);


	$No_of_opp[] = 0;
	for($i=count($No_of_opp)-2;$i>0;$i--){
		$No_of_opp[$i] = $No_of_opp[$i] + $No_of_opp[$i+1];
		//echo $No_of_opp[$i].'<br />';
	}
	unset($No_of_opp[count($No_of_opp)-1]);

	//$outputArray[] = $No_of_opp;

	$No_of_opp_closed = array() ;
	$No_of_opp_closed[] = '# Reached Closed';
	
	for($i=1;$i<count($No_of_opp);$i++){
		$No_of_opp_closed[$i] = $No_of_opp[count($No_of_opp)-1];
		//echo 'closed: ' . $No_of_opp_closed[$i].'<br />';
	}
	//$outputArray[] = $No_of_opp_closed;


	$outputArray[] = $order;
	if($weighted != 'true'){
		$SCLName[] = "Totals";
	}
	$outputArray[] = $SCLName;

	if($weighted == 'true'){
		$line[] = "Totals";
		$outputArray[] = $line;
	}


	$account_money = array();
	/*
	 $sales_sql = "SELECT OP.OpportunityID, OP.ContactID,OP.CompanyID ,SalesPersonID,UserID,OpportunityName,StartDate,ExpCloseDate,CloseDate,SalesCycleLocID,OP.ResultID ResultID,ResultName,SCLname,SCLPercent,C.Text01,C.Text02,A.Text01 CompanyName, A.AccountID AccountID
	 FROM ot_opportunities OP left join ot_Results R on OP.ResultID=R.ResultID left join ot_SalesCycleLocation S on OP.SalesCycleLocID=S.SCLID, Contact C, Account A where OP.ContactID=C.ContactID and C.AccountID=A.AccountID and OP.CompanyID=?";
	 */
	$sales_sql = "SELECT OP.OpportunityID, OP.ContactID,OP.CompanyID ,SalesPersonID,UserID,OpportunityName,StartDate,ExpCloseDate,CloseDate,SalesCycleLocID,OP.ResultID ResultID,ResultName,SCLname,SCLPercent,C.Text01,C.Text02,A.Text01 CompanyName, A.AccountID AccountID
FROM ot_opportunities OP left join ot_Results R on OP.ResultID=R.ResultID left join ot_SalesCycleLocation S on OP.SalesCycleLocID=S.SCLID, Contact C, Account A where OP.ContactID=C.ContactID and C.AccountID=A.AccountID and OP.CompanyID=?"." AND OP.ResultID IS NULL AND (CloseDate is null OR CloseDate >= '".$start_date."')";

	$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']));

	/*
	 $start = date("m/d/Y", strtotime("-6 months", time()));
	 $sales_sql .= " AND StartDate >= ? ";
	 $sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_STRING, $start));
	 */
	$sales_sql .= " AND SalesPersonID in (".$salespersonIds.") ";
	//echo $sales_sql;
	//$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_STRING, $salespersonIds));

	$result_list = DbConnManager::GetDb('mpower')->GetSqlResult($sales_sql) ;
	while (	$result = mssql_fetch_assoc($result_list)) {

		if(!isset($_SESSION[$result['AccountID']])){
			$account_money[$result['AccountID']][] = $result['CompanyName'];
			for($i=1; $i<=$SCL_counter; $i++){
				$account_money[$result['AccountID']][$i] = '';
			}
			$_SESSION[$result['AccountID']] = 'in';
		}
		$op_sql = "SELECT OpportunityID, OP.ProductID, Est_Quantity, Est_Price, Act_Quantity, Act_Price, ProductName FROM ot_Opp_Prod OP, ot_Products P WHERE
			OpportunityID = ?  and OP.ProductID=P.ProductID and (OP.Deleted != 1 OR OP.Deleted IS NULL)";

		$op_sql = SqlBuilder()->LoadSql($op_sql)->BuildSql(array(DTYPE_INT, $result['OpportunityID']));

		
		$op_result = DbConnManager::GetDb('mpower')->Exec($op_sql);
		

		$opp_prod_list = array();
		$est_total = 0;
		$act_total = 0;
		foreach ($op_result as $product) {
			$opp_prod_list[$product['ProductID']]['name'] = $product['ProductName'];
			$opp_prod_list[$product['ProductID']]['est_qty'] = $product['Est_Quantity'];
			$opp_prod_list[$product['ProductID']]['est_price'] = $product['Est_Price'];
			$opp_prod_list[$product['ProductID']]['est_ext'] = $product['Est_Quantity'] * $product['Est_Price'];
			$est_total += $opp_prod_list[$product['ProductID']]['est_ext'];
			$opp_prod_list[$product['ProductID']]['act_qty'] = $product['Act_Quantity'];
			$opp_prod_list[$product['ProductID']]['act_price'] = $product['Act_Price'];
			$opp_prod_list[$product['ProductID']]['act_ext'] = $product['Act_Quantity'] * $product['Act_Price'];
			$act_total += $opp_prod_list[$product['ProductID']]['act_ext'];
		}
		
		for($i=1; $i<=$SCL_counter; $i++){
			if($SCLIDs[$i]==$result['SalesCycleLocID']) {
				//if($i != $SCL_counter-1)
			
				//echo 'id: ' . $SCLIDs[$i] . ' result: ' . $result['SalesCycleLocID'] . ' percent: ' . $result['SCLPercent'] . '<br>';
				
				if($result['SCLPercent'] != 100){
					$account_money[$result['AccountID']][$i] += $est_total;
					/*
					 if($weighted != 'true'){
					 $account_money[$result['AccountID']][$i] += $est_total;
					 }else{
					 $account_money[$result['AccountID']][$i] += ($est_total * $result['SCLPercent'] / 100);
					 }
					 */
					//echo "<br/>".$result['SCLPercent'];
				}
				else {
					$account_money[$result['AccountID']][$i] += $act_total;
				}
			}//else $account_money[$result['AccountID']][$i] += 0;
		}
	}

	$vertical_total = array();
	$vertical_total[] = 'TOTALS';
	$client_weighted = array();
	
	if($weighted == 'true') $client_weighted[] = 'TOTALS';
	else $client_weighted[] = 'Client-Determined Weighted Dollars';
	
	for($i=0; $i<$SCL_counter; $i++){
		$vertical_total[] = '';
		$client_weighted[] = '';
	}
	
	if($weighted != 'true'){

		foreach($account_money as $amount){
			$theAmount = array();
			$company_total = 0;
			for($i=1; $i<$SCL_counter+1; $i++){
				$company_total += $amount[$i];
				$vertical_total[$i] += $amount[$i];
			}
			for($i=0; $i<$SCL_counter+1; $i++){
				$theAmount[$i] = $amount[$i];
			}
			$theAmount[] = $company_total;
			$outputArray[] = $theAmount;
		}
	}
	else{
		foreach($account_money as $amount){
			$theAmount = array();
			$company_total = 0;
			for($i=1; $i<$SCL_counter+1; $i++){
				$company_total += $amount[$i] * $SCLPercents[$i]/100;
				$vertical_total[$i] += $amount[$i];
			}
			for($i=0; $i<$SCL_counter+1; $i++){
				if($i != 0 && $amount[$i] != '')
				$theAmount[$i] = $amount[$i] * $SCLPercents[$i]/100;
				else $theAmount[$i] = $amount[$i] ;

			}
			$theAmount[] = $company_total;
			$outputArray[] = $theAmount;
		}
	}

	$grand_total = 0;
	$client_weighted_total = 0;
	
	for($i=1; $i<$SCL_counter+1; $i++){
		$grand_total += $vertical_total[$i];
		$client_weighted[$i] = ($vertical_total[$i] * $SCLPercents[$i] / 100);
		$client_weighted_total += $client_weighted[$i];
		$client_weighted[$i] = $client_weighted[$i];
	}
	
	$client_weighted[$i] = $client_weighted_total;
	$vertical_total[] = $grand_total;
	
	if($weighted != 'true'){
		$outputArray[] = $vertical_total;
	}
	else{
		$outputArray[] = $client_weighted;
	}
	
	$forecastTitle = array();
	
	if($weighted == 'true'){
		$forecastTitle[] = 'ACTIVE OPPORTUNITY VALUES';
	}
	else{
		$forecastTitle[] = 'FORECAST';
	}
	
	$outputArray[] = $forecastTitle;

	if($weighted != 'true'){
		$outputArray[] = $order;
		$outputArray[] = $SCLName;
		$outputArray[] = $line;
	}
	if($weighted != 'true'){
		$outputArray[] = $client_weighted;
	}
	else{
		$outputArray[] = $vertical_total;
	}

	$f = array();

	if($weighted == 'true'){
		$f[] = 'FORECASTED REVENUES BY CUSTOMER (BY SALES CYCLE STAGE)';
	}
	else{
		$f[] = 'ACTIVE OPPORTUNITY VALUES BY CUSTOMER (BY SALES CYCLE STAGE)';
	}
	
	$export = new export();
	
	if($weighted == 'true'){
		$export->weighted = true;
	}
	
	$export->report_type = 'SalesProcess';
	$export->filename = $f;
	$export->headers = $header ;
	$export->datas = $outputArray;
	$export->sp_row_height = ReportUtil::calcSalesPersonRowHeight($salesperson_str);
	
	$export->xlsx();

}

function BuildCalendarReport($weighted,$salespersonIds,$salesperson_str) {

	$header1 = array();
	//$header1[] = "SALES CALENDAR REPORT - CLIENT TOTALS";
	if($weighted == 'true') $header1[] = 'FORECASTED REVENUES BY CUSTOMER (BY MONTH)';
	else $header1[] = 'ACTIVE OPPORTUNITY VALUES BY CUSTOMER (BY MONTH)';
	$header2 = array();
	$header2[] = "Run Date:  ".date("n/d/Y")."          User:  ".$_SESSION['USER']['FIRSTNAME'].' '.$_SESSION['USER']['LASTNAME'];
	$outputArray[] = $header2;


	$header3 = array () ;
	$header3[] = $salesperson_str;
	$outputArray[] = $header3;


	$header = array () ;
	//$header[] = "SALES CALENDAR REPORT";
	$header[] = "Company";

	$start_date = array () ;
	$this_month = date("n");
	$this_year = date("Y");
	$future_1_year = date("Y",strtotime("1 years"));
	$future_11_month = date("n",strtotime("11 months"));

	$company_names = array();
	$account_money = array();

	$max_sql = "SELECT MAX(ExpCloseDate) as max FROM ot_opportunities OP left join ot_Results R on OP.ResultID=R.ResultID left join ot_SalesCycleLocation S on OP.SalesCycleLocID=S.SCLID where SCLPercent!=100 and OP.ResultId is null and OP.CompanyID=?";
	$max_sql = SqlBuilder()->LoadSql($max_sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']));
	$max_result = DbConnManager::GetDb('mpower')->GetOne($max_sql);
	$max_expCloseDate_time = strtotime($max_result['max']);
	//echo "Max date: ".$max_expCloseDate_time;
	$now = getdate();
	$today_begin = mktime(0, 0, 0, $now['mon'], $now['mday'], $now['year']);
	$time_diff =  $max_expCloseDate_time - $today_begin;
	$month_diff = ceil($time_diff / (24*60*60*30)) +1;
	//echo "month diff: ".$month_diff;
	$max_month_number = $month_diff > 12? $month_diff : 12;

	$month_counter = 0;
	for ($i=$this_month; $i<=($this_month+$max_month_number); $i++){
		$month_counter++;
		if($i>12 && $i <=24) {
			$j = $i % 12==0? 12 : $i % 12;
			$k = $this_year +1;
		}else if ($i>24 && $i <=36) {
			$j = $i % 12==0? 12 : $i % 12;
			$k = $this_year +2;
		}
		else {
			$j=$i;
			$k=$this_year;
		}
		$start = mktime(0, 0, 0, $j, 1, $k);
		$start_date[] = date("m/d/Y H:i:s", $start);
		if($month_counter < 13){
			$header[] = date('M \'y', $start);
			//echo "<br/>......month: ".date('F', $start);
		}
		//echo "<br/>......month: ".date('F,Y', $start);
	}
	$company_amount = array();

	for ($i=0; $i<$max_month_number; $i++){
		$sales_sql = "SELECT OP.OpportunityID, OP.ContactID,OP.CompanyID ,SalesPersonID,UserID,OpportunityName,StartDate,ExpCloseDate,CloseDate,SalesCycleLocID,OP.ResultID ResultID,ResultName, R.status, SCLname,SCLPercent,C.Text01,C.Text02,A.Text01 CompanyName, A.AccountID AccountID
FROM ot_opportunities OP left join ot_Results R on OP.ResultID=R.ResultID left join ot_SalesCycleLocation S on OP.SalesCycleLocID=S.SCLID, Contact C, Account A where OP.ContactID=C.ContactID and C.AccountID=A.AccountID and SCLPercent!=100 and OP.ResultId is null and OP.CompanyID=?";
		$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']));

		$start = $start_date[$i];
		//echo "<br/>......start: ".$start;
		$sales_sql .= " AND ExpCloseDate >= ? ";
		$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_STRING, $start));

		$end = $start_date[$i+1];
		//echo "<br/>......end: ".$end;
		$sales_sql .= " AND ExpCloseDate < ? ";
		$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_STRING, $end));

		$sales_sql .= " AND SalesPersonID in (".$salespersonIds.") ";

		$result_list = DbConnManager::GetDb('mpower')->GetSqlResult($sales_sql) ;

		while (	$result = mssql_fetch_assoc($result_list)) {

			if(!in_array($result['CompanyName'],$company_names)){
				$company_names[] = $result['CompanyName'];
				$account_money[$result['AccountID']][] = $result['CompanyName'];
				$company_amount[$result['AccountID']][] = $result['CompanyName'];
				for($j=1; $j<$max_month_number+1; $j++){
					$account_money[$result['AccountID']][$j] = '';
					//echo $account_money[$result['AccountID']][$j];
				}
			}

			$op_sql = "SELECT OpportunityID, OP.ProductID, Est_Quantity, Est_Price, Act_Quantity, Act_Price, ProductName FROM ot_Opp_Prod OP, ot_Products P WHERE
			OpportunityID = ?  and OP.ProductID=P.ProductID and (OP.Deleted != 1 OR OP.Deleted IS NULL)";

			$op_sql = SqlBuilder()->LoadSql($op_sql)->BuildSql(array(DTYPE_INT, $result['OpportunityID']));
			$op_result = DbConnManager::GetDb('mpower')->Exec($op_sql);

			$opp_prod_list = array();
			$est_total = 0;
			$act_total = 0;

			foreach ($op_result as $product) {
				$opp_prod_list[$product['ProductID']]['name'] = $product['ProductName'];
				$opp_prod_list[$product['ProductID']]['est_qty'] = $product['Est_Quantity'];
				$opp_prod_list[$product['ProductID']]['est_price'] = $product['Est_Price'];
				$opp_prod_list[$product['ProductID']]['est_ext'] = $product['Est_Quantity'] * $product['Est_Price'];
				$est_total += $opp_prod_list[$product['ProductID']]['est_ext'];
				$opp_prod_list[$product['ProductID']]['act_qty'] = $product['Act_Quantity'];
				$opp_prod_list[$product['ProductID']]['act_price'] = $product['Act_Price'];
				$opp_prod_list[$product['ProductID']]['act_ext'] = $product['Act_Quantity'] * $product['Act_Price'];
				$act_total += $opp_prod_list[$product['ProductID']]['act_ext'];
			}
			if($weighted == 'true') $account_money[$result['AccountID']][$i+1] += $est_total * $result['SCLPercent'] / 100;
			else $account_money[$result['AccountID']][$i+1] += $est_total;
		}
	}//end for ($i=1; $i<13; $i++)


	$vertical_total = array();
	$vertical_total[] = 'TOTALS';
	$company_total = array();
	$company_total[] = 'Company TOTALS';

	for($i=1; $i<=$max_month_number; $i++){
		$vertical_total[] = '';
		$company_total[] = '';
		//$company_amount[] = 0;
	}

	$company_counter = 0;
	foreach($account_money as $amount){
		//$company_total = 0;
		for($i=1; $i<=$max_month_number; $i++){

			$company_total[$company_counter] += $amount[$i];
			$vertical_total[$i] += $amount[$i];
		}
		$company_counter++;
	}
	setlocale(LC_MONETARY, 'en_US');
	$company_counter = 0;
	$some_array =  array();

	foreach($account_money as $amount){
		$k = 0;
		for($i=0; $i<=$max_month_number; $i++){
			if($i<=12 || $vertical_total[$i] > 0){
				if($i == 0)
				$some_array[$k] = $amount[$i];
				else $some_array[$k] = $amount[$i];

				if($i>12 && !isset($header[$k])){
					$header[$k] = date('M \'y',strtotime($start_date[$i-1]));//.', '.date('y',strtotime($start_date[$i-1]));
				}
				$k++;
			}
		}

		if(!isset($header[$k])) {$header[$k] = "Totals";	$outputArray[] = $header; }
		$some_array[$k] = $company_total[$company_counter];
		$outputArray[] = $some_array;
		$company_counter++;
	}

	$grand_total = 0;
	for($i=1; $i<=$max_month_number; $i++){
		$grand_total += $vertical_total[$i];
	}
	$k=0;
	for($i=0; $i<=$max_month_number; $i++){
		if($i<=12 || $vertical_total[$i] > 0){
			if($i == 0)
			$some_array[$k] = $vertical_total[$i];
			else $some_array[$k] = $vertical_total[$i];
			$k++;
		}
		//$vertical_total[$i] = '$'.$vertical_total[$i];
	}
	$some_array[$k] = $grand_total;
	$outputArray[] = $some_array;
	$f = array();
	//$f[] = 'SalesCalendarReport';
	$export = new export() ;
	$export->report_type = 'SalesCalendar';
	if($weighted == 'true') {
		//$export->report_type = 'FORECASTED REVENUES BY CUSTOMER (BY MONTH)';
		$f[] = 'FORECASTED REVENUES BY CUSTOMER (BY MONTH)';
	}
	else {
		//$export->report_type = 'ACTIVE OPPORTUNITY VALUES BY CUSTOMER (BY MONTH)';
		$f[] = 'ACTIVE OPPORTUNITY VALUES BY CUSTOMER (BY MONTH)';
	}
	$export->filename = $f;
	$export->headers = $header1;
	$export->datas = $outputArray;
	$export->sp_row_height = ReportUtil::calcSalesPersonRowHeight($salesperson_str);

	$export->xlsx();

}


function BuildCalendarReport_opps($weighted,$salespersonIds,$salesperson_str) {

	$header1 = array();
	//$header1[] = "SALES CALENDAR REPORT - CLIENT OPPORTUNITY DETAIL";
	//echo '$weighted: '.$weighted;
	if($weighted == 'true') $header1[] = 'FORECASTED REVENUES BY CUSTOMER / DETAIL (BY MONTH)';
	else $header1[] = 'ACTIVE OPPORTUNITY VALUES BY CUSTOMER / DETAIL (BY MONTH)';
	$header2 = array();
	$header2[] = "Run Date:  ".date("n/d/Y")."          User:  ".$_SESSION['USER']['FIRSTNAME'].' '.$_SESSION['USER']['LASTNAME'];
	$outputArray[] = $header2;


	$header3 = array () ;
	$header3[] = $salesperson_str;
	$outputArray[] = $header3;


	$header = array () ;
	//$header[] = "SALES CALENDAR REPORT";
	$header[] = "Company";
	$header[] = "Opportunity/Salesperson(s)";

	$start_date = array () ;
	$this_month = date("n");
	$this_year = date("Y");
	$future_1_year = date("Y",strtotime("1 years"));
	$future_11_month = date("n",strtotime("11 months"));

	$company_names = array();
	$opp_names = array();
	$account_money = array();
	$opp_money = array();

	$total_money = array();


	$max_sql = "SELECT MAX(ExpCloseDate) as max FROM ot_opportunities OP left join ot_Results R on OP.ResultID=R.ResultID left join ot_SalesCycleLocation S on OP.SalesCycleLocID=S.SCLID where SCLPercent!=100 and OP.ResultId is null and OP.CompanyID=?";
	$max_sql = SqlBuilder()->LoadSql($max_sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']));
	$max_result = DbConnManager::GetDb('mpower')->GetOne($max_sql);
	$max_expCloseDate_time = strtotime($max_result['max']);
	$now = getdate();
	$today_begin = mktime(0, 0, 0, $now['mon'], $now['mday'], $now['year']);
	$time_diff =  $max_expCloseDate_time - $today_begin;
	$month_diff = ceil($time_diff / (24*60*60*30)) +1;
	$max_month_number = $month_diff > 12? $month_diff : 12;

	$month_counter = 0;
	for ($i=$this_month; $i<=($this_month+$max_month_number); $i++){
		$month_counter++;
		if($i>12 && $i <=24) {
			$j = $i % 12==0? 12 : $i % 12;
			$k = $this_year +1;
		}else if ($i>24 && $i <=36) {
			$j = $i % 12==0? 12 : $i % 12;
			$k = $this_year +2;
		}
		else {
			$j=$i;
			$k=$this_year;
		}
		$start = mktime(0, 0, 0, $j, 1, $k);
		$start_date[] = date("m/d/Y H:i:s", $start);
		if($month_counter < 13){
			$header[] = date('M \'y', $start);
		}
	}
	//$company_amount = array();
	//$opp_amount = array();
	$has_opp = array();
	for ($i=0; $i<$max_month_number+2; $i++){
		$has_opp[$i] = false;
		$total_money[$i] = 0;
	}
	$total_money[0] = '';
	$total_money[1] = 'TOTALS';

	for ($i=0; $i<$max_month_number; $i++){
		//$sales_sql = "SELECT OP.OpportunityID, OP.ContactID,OP.CompanyID ,SalesPersonID,UserID,OpportunityName,StartDate,ExpCloseDate,CloseDate,SalesCycleLocID,OP.ResultID ResultID,ResultName, R.status, SCLname,SCLPercent,C.Text01,C.Text02,A.Text01 CompanyName, A.AccountID AccountID FROM ot_opportunities OP left join ot_Results R on OP.ResultID=R.ResultID left join ot_SalesCycleLocation S on OP.SalesCycleLocID=S.SCLID, Contact C, Account A where OP.ContactID=C.ContactID and C.AccountID=A.AccountID and SCLPercent!=100 and OP.ResultId is null and OP.CompanyID=?";
		$sales_sql = "SELECT OP.OpportunityID oppId, OP.ContactID,OP.CompanyID ,SalesPersonID,OP.UserID,OpportunityName,OP.StartDate,ExpCloseDate,CloseDate,SalesCycleLocID,OP.ResultID ResultID,ResultName, R.status, SCLname,SCLPercent,C.Text01,C.Text02,A.Text01 CompanyName, A.AccountID AccountID, EstTotal, FirstName, LastName FROM ot_opportunities OP left join ot_Results R on OP.ResultID=R.ResultID left join ot_SalesCycleLocation S on OP.SalesCycleLocID=S.SCLID, Contact C, Account A, People P where OP.ContactID=C.ContactID and C.AccountID=A.AccountID and SCLPercent!=100 and OP.ResultId is null  and P.PersonId=SalesPersonID and OP.CompanyID=?";
		$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']));

		$start = $start_date[$i];
		$sales_sql .= " AND ExpCloseDate >= ? ";
		$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_STRING, $start));

		$end = $start_date[$i+1];
		$sales_sql .= " AND ExpCloseDate < ? ";
		$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_STRING, $end));

		$sales_sql .= " AND SalesPersonID in (".$salespersonIds.") ";

		$result_list = DbConnManager::GetDb('mpower')->GetSqlResult($sales_sql) ;

		while (	$result = mssql_fetch_assoc($result_list)) {


			for($j=0; $j<=$max_month_number+1; $j++){
				$opp_names[$result['AccountID']][$result['oppId']][$j] = '';
				//$account_money[$result['AccountID']][$j] = 0;
			}
			//$opp_names[$result['AccountID']][$result['oppId']]['CompanyName']= $result['CompanyName'];
			//$opp_names[$result['AccountID']][$result['oppId']]['OpportunityName']= $result['OpportunityName'];
			if(!in_array($result['CompanyName'],$company_names)){
				$company_names[$result['AccountID']] = $result['CompanyName'];
				$opp_names[$result['AccountID']][$result['oppId']][0]= $result['CompanyName'];
					
				for($j=0; $j<=$max_month_number+1; $j++){
					$account_money[$result['AccountID']][$j] = '';
				}
				$account_money[$result['AccountID']][0] = '';
				$account_money[$result['AccountID']][1] = 'Sub-Total';
			}else
			$opp_names[$result['AccountID']][$result['oppId']][0]= '';

			$opp_names[$result['AccountID']][$result['oppId']][1]= html_entity_decode($result['OpportunityName'],ENT_QUOTES).'/'.html_entity_decode($result['FirstName'],ENT_QUOTES).' '.html_entity_decode($result['LastName'],ENT_QUOTES);

			if($weighted == 'true'){
				$opp_names[$result['AccountID']][$result['oppId']][$i+2]= $result['EstTotal'] * $result['SCLPercent'] / 100;
				$opp_names[$result['AccountID']][$result['oppId']][$max_month_number+1]= $result['EstTotal'] * $result['SCLPercent'] / 100;
					
				$account_money[$result['AccountID']][$i+2] += $result['EstTotal'] * $result['SCLPercent'] / 100;
				$account_money[$result['AccountID']][$max_month_number+1] += $result['EstTotal'] * $result['SCLPercent'] / 100;
					
				$total_money[$i+2] += $result['EstTotal'] * $result['SCLPercent'] / 100;
				$total_money[$max_month_number+1] += $result['EstTotal'] * $result['SCLPercent'] / 100;
			} else {
				$opp_names[$result['AccountID']][$result['oppId']][$i+2]= $result['EstTotal'];
				$opp_names[$result['AccountID']][$result['oppId']][$max_month_number+1]= $result['EstTotal'];
					
				$account_money[$result['AccountID']][$i+2] += $result['EstTotal'];
				$account_money[$result['AccountID']][$max_month_number+1] += $result['EstTotal'];
					
				$total_money[$i+2] += $result['EstTotal'];
				$total_money[$max_month_number+1] += $result['EstTotal'];
			}
			//print_r($opp_names);
			$has_opp[$i+2] = true;
		}
		if($i>=12 && !isset($header[$i+2]) && $has_opp[$i+2]){
			$header[] = date('M \'y',strtotime($start_date[$i]));
		}
	}//for ($i=0; $i<$max_month_number; $i++){


	$header[] = "Totals";
	$outputArray[] = $header;


	foreach($opp_names as $key => $companies){
		//$opp_row[] = $company_names[$key];

		foreach ($companies as $opps){
			$i = 0;
			$temp_array = array();
			foreach($opps as $opp_row){
				if($i<14 || $has_opp[$i] || $i == $max_month_number+1)
				$temp_array[] = $opp_row;
				$i++;
			}
			$outputArray[] =  $temp_array;
		}
		$j = 0;
		$temp_array1 = array();
		foreach($account_money[$key] as $total_row){
			if($j<14 || $has_opp[$j] || $j == $max_month_number+1)
			$temp_array1[] = $total_row;
			$j++;
		}
		$outputArray[] =  $temp_array1;
		//$outputArray[] = $account_money[$key];
		$outputArray[] =  array();


	}
	$k = 0;
	$temp_array2 = array();
	foreach($total_money as $total){
		if($k<14 || $has_opp[$k] || $k == $max_month_number+1)
		$temp_array2[] = $total;
		$k++;
	}
	$outputArray[] =  $temp_array2;


	$f = array();
	//$f[] = 'Anticipated Revenue By Customer and Opportunity';
	$export = new export() ;
	$export->report_type = 'SalesCalendarOpp';
	if($weighted == 'true') {
		//$export->report_type = 'FORECASTED REVENUES BY CUSTOMER / DETAILS';
		$f[] = 'FORECASTED REVENUES BY CUSTOMER / DETAILS';
	}
	else {
		//$export->report_type = 'ACTIVE OPPORTUNITY VALUES BY CUSTOMER / DETAILS';
		$f[] = 'ACTIVE OPPORTUNITY VALUES BY CUSTOMER / DETAILS';
	}
	$export->filename = $f;
	$export->headers = $header1;
	$export->datas = $outputArray ;
	$export->sp_row_height = ReportUtil::calcSalesPersonRowHeight($salesperson_str);
	
	$export->xlsx() ;

}



function BuildCalendarReport_prods($weighted,$salespersonIds,$salesperson_str) {

	$header1 = array();
	//$header1[] = "SALES CALENDAR REPORT - CLIENT OPPORTUNITY DETAIL";
	//echo '$weighted: '.$weighted;
	if($weighted == 'true') $header1[] = 'FORECASTED REVENUES BY PRODUCT (BY MONTH)';
	else $header1[] = 'ACTIVE OPPORTUNITY VALUES BY PRODUCT (BY MONTH)';
	$header2 = array();
	$header2[] = "Run Date:  ".date("n/d/Y")."          User:  ".$_SESSION['USER']['FIRSTNAME'].' '.$_SESSION['USER']['LASTNAME'];
	$outputArray[] = $header2;


	$header3 = array () ;
	$header3[] = $salesperson_str;
	$outputArray[] = $header3;


	$header = array () ;
	//$header[] = "SALES CALENDAR REPORT";

	$header[] = "Product Name";
	$header[] = "Company";
	$header[] = "Opportunity/Salesperson(s)";

	$start_date = array () ;
	$this_month = date("n");
	$this_year = date("Y");
	$future_1_year = date("Y",strtotime("1 years"));
	$future_11_month = date("n",strtotime("11 months"));

	$company_names = array();
	$prod_names = array();
	$opp_names = array();
	$account_money = array();
	$opp_money = array();

	$total_money = array();


	$max_sql = "SELECT MAX(ExpCloseDate) as max FROM ot_opportunities OP left join ot_Results R on OP.ResultID=R.ResultID left join ot_SalesCycleLocation S on OP.SalesCycleLocID=S.SCLID where SCLPercent!=100 and OP.ResultId is null and OP.CompanyID=?";
	$max_sql = SqlBuilder()->LoadSql($max_sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']));
	$max_result = DbConnManager::GetDb('mpower')->GetOne($max_sql);
	$max_expCloseDate_time = strtotime($max_result['max']);
	$now = getdate();
	$today_begin = mktime(0, 0, 0, $now['mon'], $now['mday'], $now['year']);
	$time_diff =  $max_expCloseDate_time - $today_begin;
	$month_diff = ceil($time_diff / (24*60*60*30)) +1;
	$max_month_number = $month_diff > 12? $month_diff : 12;

	$month_counter = 0;
	for ($i=$this_month; $i<=($this_month+$max_month_number); $i++){
		$month_counter++;
		if($i>12 && $i <=24) {
			$j = $i % 12==0? 12 : $i % 12;
			$k = $this_year +1;
		}else if ($i>24 && $i <=36) {
			$j = $i % 12==0? 12 : $i % 12;
			$k = $this_year +2;
		}
		else {
			$j=$i;
			$k=$this_year;
		}
		$start = mktime(0, 0, 0, $j, 1, $k);
		$start_date[] = date("m/d/Y H:i:s", $start);
		if($month_counter < 13){
			$header[] = date('M \'y', $start);
		}
	}
	//$company_amount = array();
	//$opp_amount = array();
	$has_opp = array();
	for ($i=0; $i<$max_month_number+3; $i++){
		$has_opp[$i] = false;
		$total_money[$i] = 0;
	}

	$total_money[0] = 'TOTALS';
	$total_money[1] = '';
	$total_money[2] = '';

	for ($i=0; $i<$max_month_number; $i++){
		//$sales_sql = "SELECT OP.OpportunityID, OP.ContactID,OP.CompanyID ,SalesPersonID,UserID,OpportunityName,StartDate,ExpCloseDate,CloseDate,SalesCycleLocID,OP.ResultID ResultID,ResultName, R.status, SCLname,SCLPercent,C.Text01,C.Text02,A.Text01 CompanyName, A.AccountID AccountID FROM ot_opportunities OP left join ot_Results R on OP.ResultID=R.ResultID left join ot_SalesCycleLocation S on OP.SalesCycleLocID=S.SCLID, Contact C, Account A where OP.ContactID=C.ContactID and C.AccountID=A.AccountID and SCLPercent!=100 and OP.ResultId is null and OP.CompanyID=?";
		$sales_sql = "SELECT OP.OpportunityID oppId, OP.ContactID,OP.CompanyID ,SalesPersonID,OP.UserID,OpportunityName,OP.StartDate,ExpCloseDate,CloseDate,SalesCycleLocID,OP.ResultID ResultID,ResultName, R.status, SCLname,SCLPercent,C.Text01,C.Text02,A.Text01 CompanyName, A.AccountID AccountID, EstTotal, FirstName, LastName FROM ot_opportunities OP left join ot_Results R on OP.ResultID=R.ResultID left join ot_SalesCycleLocation S on OP.SalesCycleLocID=S.SCLID, Contact C, Account A, People P  where OP.ContactID=C.ContactID and C.AccountID=A.AccountID and SCLPercent!=100 and OP.ResultId is null and P.PersonId=SalesPersonID and OP.CompanyID=?";
		$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']));

		$start = $start_date[$i];
		$sales_sql .= " AND ExpCloseDate >= ? ";
		$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_STRING, $start));

		$end = $start_date[$i+1];
		$sales_sql .= " AND ExpCloseDate < ? ";
		$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_STRING, $end));

		$sales_sql .= " AND SalesPersonID in (".$salespersonIds.") ";

		$result_list = DbConnManager::GetDb('mpower')->GetSqlResult($sales_sql) ;

		while (	$result = mssql_fetch_assoc($result_list)) {

			/*
			 for($j=0; $j<=$max_month_number+1; $j++){
				$opp_names[$result['AccountID']][$result['oppId']][$j] = '';
				//$account_money[$result['AccountID']][$j] = 0;
				}
				*/
			//$opp_names[$result['AccountID']][$result['oppId']]['CompanyName']= $result['CompanyName'];
			//$opp_names[$result['AccountID']][$result['oppId']]['OpportunityName']= $result['OpportunityName'];


			$op_sql = "SELECT OpportunityID, OP.ProductID, Est_Quantity, Est_Price, Act_Quantity, Act_Price, ProductName FROM ot_Opp_Prod OP, ot_Products P WHERE
			OpportunityID = ?  and OP.ProductID=P.ProductID and (OP.Deleted != 1 OR OP.Deleted IS NULL)";

			$op_sql = SqlBuilder()->LoadSql($op_sql)->BuildSql(array(DTYPE_INT, $result['oppId']));
			$op_result = DbConnManager::GetDb('mpower')->Exec($op_sql);
			/*
			 $opp_prod_list = array();
			 $est_total = 0;
			 $act_total = 0;
			 */
			foreach ($op_result as $product) {

				/*
				 $opp_prod_list[$product['ProductID']]['name'] = $product['ProductName'];
				 $opp_prod_list[$product['ProductID']]['est_qty'] = $product['Est_Quantity'];
				 $opp_prod_list[$product['ProductID']]['est_price'] = $product['Est_Price'];
				 $opp_prod_list[$product['ProductID']]['est_ext'] = $product['Est_Quantity'] * $product['Est_Price'];
				 $est_total += $opp_prod_list[$product['ProductID']]['est_ext'];

				 $opp_prod_list[$product['ProductID']]['act_qty'] = $product['Act_Quantity'];
				 $opp_prod_list[$product['ProductID']]['act_price'] = $product['Act_Price'];
				 $opp_prod_list[$product['ProductID']]['act_ext'] = $product['Act_Quantity'] * $product['Act_Price'];
				 $act_total += $opp_prod_list[$product['ProductID']]['act_ext'];
				 */

				//print_r($company_names);
				//echo "<br/>".$result['CompanyName'];

				for($j=0; $j<=$max_month_number+2; $j++){
					if (!isset($opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][$j])) $opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][$j] = '';
					//$account_money[$result['AccountID']][$j] = 0;
				}
				/*
				 if(!in_array($result['CompanyName'],$company_names)){
				 //echo "<br/>".$result['CompanyName'];

				 $company_names[$result['AccountID']] = $result['CompanyName'];
				 $opp_names[$result['AccountID']][$product['ProductID']][0]= $result['CompanyName'];


				 for($j=0; $j<=$max_month_number+2; $j++){
				 $account_money[$result['AccountID']][$j] = '';
				 }
				 $account_money[$result['AccountID']][0] = '';
				 $account_money[$result['AccountID']][1] = 'Total';

				 } else if (!isset($opp_names[$result['AccountID']][$product['ProductID']][0])) $opp_names[$result['AccountID']][$product['ProductID']][0]= '';
				 */
				if(!in_array($product['ProductName'],$prod_names)){
					//echo "<br/>".$result['CompanyName'];

					$prod_names[$product['ProductID']] = $product['ProductName'];
					$opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][0]= $product['ProductName'];

					for($j=0; $j<=$max_month_number+2; $j++){
						$account_money[$product['ProductID']][$j] = '';
					}
					$account_money[$product['ProductID']][0] = 'Sub-Total';
					$account_money[$product['ProductID']][1] = '';
					$account_money[$product['ProductID']][2] = '';

				} else if (!isset($opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][0])) $opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][0]= '';

				$opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][1]= html_entity_decode($result['CompanyName'],ENT_QUOTES);
				$opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][2]= html_entity_decode($result['OpportunityName'],ENT_QUOTES) .'/'.html_entity_decode($result['FirstName'],ENT_QUOTES).' '.html_entity_decode($result['LastName'],ENT_QUOTES);

				if($weighted == 'true'){
					$opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][$i+3] = $product['Est_Quantity'] * $product['Est_Price'] * $result['SCLPercent'] / 100;
					$opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][$max_month_number+2] += $product['Est_Quantity'] * $product['Est_Price'] * $result['SCLPercent'] / 100;

					$account_money[$product['ProductID']][$i+3] += $product['Est_Quantity'] * $product['Est_Price'] * $result['SCLPercent'] / 100;
					$account_money[$product['ProductID']][$max_month_number+2] += $product['Est_Quantity'] * $product['Est_Price'] * $result['SCLPercent'] / 100;

					$total_money[$i+3] += $product['Est_Quantity'] * $product['Est_Price'] * $result['SCLPercent'] / 100;
					$total_money[$max_month_number+2] += $product['Est_Quantity'] * $product['Est_Price'] * $result['SCLPercent'] / 100;
				} else {
					$opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][$i+3] = $product['Est_Quantity'] * $product['Est_Price'];
					$opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][$max_month_number+2] += $product['Est_Quantity'] * $product['Est_Price'];

					$account_money[$product['ProductID']][$i+3] += $product['Est_Quantity'] * $product['Est_Price'];
					$account_money[$product['ProductID']][$max_month_number+2] += $product['Est_Quantity'] * $product['Est_Price'];

					$total_money[$i+3] += $product['Est_Quantity'] * $product['Est_Price'];
					$total_money[$max_month_number+2] += $product['Est_Quantity'] * $product['Est_Price'];
				}
				//print_r($opp_names);
				$has_opp[$i+3] = true;
			}
		}
		if($i>=12 && !isset($header[$i+3]) && $has_opp[$i+3]){
			$header[] = date('M \'y',strtotime($start_date[$i]));
		}
	}//for ($i=0; $i<$max_month_number; $i++){


	$header[] = "Totals";
	$outputArray[] = $header;
	//print_r($opp_names);

	foreach($opp_names as $key => $products){
		//$opp_row[] = $company_names[$key];

		foreach ($products as $opps){
			foreach($opps as $opp_row){
				$i = 0;
				$temp_array = array();
				//print_r($opp_row);
				foreach($opp_row as $theprod){
					if($i<15 || $has_opp[$i] || $i == $max_month_number+2)
					$temp_array[] = $theprod;
					$i++;
				}
				$outputArray[] =  $temp_array;
			}
		}
		$j = 0;
		$temp_array1 = array();
		foreach($account_money[$key] as $total_row){
			if($j<15 || $has_opp[$j] || $j == $max_month_number+2)
			$temp_array1[] = $total_row;
			$j++;
		}
		$outputArray[] =  $temp_array1;
		//$outputArray[] = $account_money[$key];
		$outputArray[] =  array();


	}
	$k = 0;
	$temp_array2 = array();
	foreach($total_money as $total){
		if($k<15 || $has_opp[$k] || $k == $max_month_number+2)
		$temp_array2[] = $total;
		$k++;
	}
	$outputArray[] =  $temp_array2;


	$f = array();
	//$f[] = 'Anticipated Revenue by Customer and Product';
	$export = new export() ;
	$export->report_type = 'SalesCalendarProd';
	if($weighted == 'true')
	{
		//$export->report_type = 'FORECASTED REVENUES BY PRODUCT';
		$f[] = 'FORECASTED REVENUES BY PRODUCT';
	}
	else {
		//$export->report_type = 'ACTIVE OPPORTUNITY VALUES BY PRODUCT';
		$f[] = 'ACTIVE OPPORTUNITY VALUES BY PRODUCT';
	}
	$export->filename = $f;
	$export->headers = $header1;
	$export->datas = $outputArray;
	$export->sp_row_height = ReportUtil::calcSalesPersonRowHeight($salesperson_str);
	
	$export->xlsx() ;

}

function BuildClosedReport($start_in, $end, $salespersonIds,$salesperson_str) {

	$header1 = array();
	$header1[] = 'CLOSED OPPORTUNITY VALUES BY CUSTOMER (BY MONTH)'.' WITH CLOSE DATE FROM '.$start_in.' TO '.$end;
	$header2 = array();
	$header2[] = "Run Date:  ".date("n/d/Y")."          User:  ".$_SESSION['USER']['FIRSTNAME'].' '.$_SESSION['USER']['LASTNAME'];
	$outputArray[] = $header2;

	$header3 = array () ;
	$header3[] = $salesperson_str;
	$outputArray[] = $header3;

	$header = array () ;
	$header[] = "Company";

	$start_date = array () ;

	$from_date = $start_in;
	$from_time = strtotime($from_date);
	$from_date_num = date('d',$from_time);
	$from_month = date('n',$from_time);
	$from_year = date('Y',$from_time);

	$to_time = strtotime($end);
	$to_date_num = date('d',$to_time);
	$to_month = date('n',$to_time);
	$to_year = date('Y',$to_time);

	$time_diff = $to_time - $from_time;
	$month_diff = ceil($time_diff / (24*60*60*30)) + 2;

	$company_names = array();
	$account_money = array();

	$month_counter = 0;
	$k=0;
	$j=0;
	for ($i=$from_month; $i<=($from_month+$month_diff); $i++){
		$month_counter++;
		$j = $i % 12==0? 12 : $i % 12;
		if($k == 0) $k=$from_year;
		if($month_counter == 1) {
			$start = mktime(0, 0, 0, $j, $from_date_num, $k);
			$start_date[] = date("m/d/Y H:i:s", $start);
		} else if($j == $to_month && $k == $to_year){
			$start = mktime(0, 0, 0, $j, 1, $k);
			$start_date[] = date("m/d/Y H:i:s", $start);
			$start = mktime(0, 0, 0, $j, $to_date_num, $k);
			$start_date[] = date("m/d/Y H:i:s", $start);
			break;
		}else {
			$start = mktime(0, 0, 0, $j, 1, $k);
			$start_date[] = date("m/d/Y H:i:s", $start);
		}
		if($i % 12==0) ($k++);
	}
	//echo $month_diff." and ".$month_counter." and ".count($start_date);
	if($month_counter >= count($start_date)) $month_counter = count($start_date) -1;
	$company_amount = array();
	$has_opp = array();
	for ($i=0; $i<$month_counter; $i++){
		$has_opp[$i] = false;
	}
	$column = 0;
	for ($i=0; $i<$month_counter; $i++){
		$sales_sql = "
			SELECT
				OP.OpportunityID, OP.ContactID,OP.CompanyID ,SalesPersonID, UserID,
				OpportunityName, StartDate, ExpCloseDate, CloseDate, SalesCycleLocID,
				OP.ResultID ResultID, ResultName, R.status, SCLname, SCLPercent,
				C.Text01, C.Text02, A.Text01 CompanyName, A.AccountID AccountID
			FROM
				ot_opportunities OP
			left join
				ot_Results R on OP.ResultID = R.ResultID
			left join
				ot_SalesCycleLocation S on OP.SalesCycleLocID = S.SCLID,
				Contact C, Account A
			where
				OP.ContactID = C.ContactID and C.AccountID = A.AccountID and OP.ResultId is not null AND (R.ResultName LIKE 'Won' OR R.ResultName LIKE 'Sold') and OP.CompanyID=?";

		$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']));

		$start = $start_date[$i];
		//echo "<br/>......start: ".$start;
		$sales_sql .= " AND CloseDate >= ? ";
		$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_STRING, $start));

		$end = $start_date[$i+1];
		//echo "<br/>......end: ".$end;
		$sales_sql .= " AND CloseDate < ? ";
		$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_STRING, $end));

		$sales_sql .= " AND SalesPersonID in (".$salespersonIds.") ";

		$result_list = DbConnManager::GetDb('mpower')->GetSqlResult($sales_sql) ;

		while (	$result = mssql_fetch_assoc($result_list)) {

			if(!in_array($result['CompanyName'],$company_names)){
				$company_names[] = $result['CompanyName'];
				$account_money[$result['AccountID']][] = $result['CompanyName'];
				$company_amount[$result['AccountID']][] = $result['CompanyName'];
				for($j=1; $j<$month_counter+1; $j++){
					$account_money[$result['AccountID']][$j] = '';
					//echo $account_money[$result['AccountID']][$j];
				}
			}

			$op_sql = "SELECT OpportunityID, OP.ProductID, Est_Quantity, Est_Price, Act_Quantity, Act_Price, ProductName FROM ot_Opp_Prod OP, ot_Products P WHERE
			OpportunityID = ?  and OP.ProductID=P.ProductID and (OP.Deleted != 1 OR OP.Deleted IS NULL)";

			$op_sql = SqlBuilder()->LoadSql($op_sql)->BuildSql(array(DTYPE_INT, $result['OpportunityID']));
			$op_result = DbConnManager::GetDb('mpower')->Exec($op_sql);

			$opp_prod_list = array();
			$est_total = 0;
			$act_total = 0;

			foreach ($op_result as $product) {
				$opp_prod_list[$product['ProductID']]['name'] = $product['ProductName'];
				$opp_prod_list[$product['ProductID']]['est_qty'] = $product['Est_Quantity'];
				$opp_prod_list[$product['ProductID']]['est_price'] = $product['Est_Price'];
				$opp_prod_list[$product['ProductID']]['est_ext'] = $product['Est_Quantity'] * $product['Est_Price'];
				$est_total += $opp_prod_list[$product['ProductID']]['est_ext'];
				$opp_prod_list[$product['ProductID']]['act_qty'] = $product['Act_Quantity'];
				$opp_prod_list[$product['ProductID']]['act_price'] = $product['Act_Price'];
				$opp_prod_list[$product['ProductID']]['act_ext'] = $product['Act_Quantity'] * $product['Act_Price'];
				$act_total += $opp_prod_list[$product['ProductID']]['act_ext'];
			}
			$account_money[$result['AccountID']][$i+1] += $act_total;
			//$account_money[$result['AccountID']][] += $act_total;
			$has_opp[$i+1] = true;
		}
		/*
		 if($month_diff > 12){
			if( ($i>=$month_diff-13) || (!isset($header[$i+1]) && $has_opp[$i+1])){
			$column++;
			if($i != $month_diff-1) $header[] = date('M \'y',strtotime($start));
			}
			}else{
			//if($i>0){
			$column++;
			if($i != $month_diff-1) $header[] = date('M \'y',strtotime($start));
			//}
			}
			*/

		if($month_counter > 12){
			if( ($i>=$month_counter-13) || (!isset($header[$i+1]) && $has_opp[$i+1])){
				$column++;
				$has_opp[$i+1] = true;
				if($i != $month_counter) $header[] = date('M \'y',strtotime($start));
			}
		}else{
			$column++;
			$has_opp[$i+1] = true;
			if($i != $month_counter) $header[] = date('M \'y',strtotime($start));
		}

	}//end for ($i=1; $i<13; $i++)


	$vertical_total = array();
	$vertical_total[] = 'TOTALS';
	$company_total = array();
	$company_total[] = 'Company TOTALS';

	for($i=1; $i<=$month_counter; $i++){
		$vertical_total[] = '';
		$company_total[] = '';
		//$company_amount[] = 0;
	}

	$company_counter = 0;
	foreach($account_money as $amount){
		//$company_total = 0;
		$counter = 0;
		for($i=1; $i<=$month_counter; $i++){
			if( $has_opp[$i]){
				$counter++;
				$company_total[$company_counter] += $amount[$i];
				$vertical_total[$counter] += $amount[$i];
			}
		}
		$company_counter++;
	}
	//setlocale(LC_MONETARY, 'en_US');
	$company_counter = 0;
	$some_array =  array();
	$headershowed = 0;
	foreach($account_money as $amount){
		$some_array =  array();
		$k = 0;
		$some_array[$k] = $amount[0];
		for($i=1; $i<=$month_counter; $i++){
			if($has_opp[$i]){
				$k++;
				$some_array[] = $amount[$i];
			}
		}

		//if(!isset($header[$k+1])) {$header[$k+1] = "Totals";}
		//echo "here!";
		//print_r($header);
		if($headershowed===0){
			if(!isset($header[$k+1])) {$header[$k+1] = "Totals";}
			$outputArray[] = $header;
			$headershowed=1;
		}

		$some_array[$k+1] = $company_total[$company_counter];
		$outputArray[] = $some_array;
		$company_counter++;
	}

	if($headershowed===0){
		$header[] = "Totals";
		$outputArray[] = $header;
		$headershowed=1;
	}

	$some_array =  array();
	$grand_total = 0;
	for($i=1; $i<=$month_counter; $i++){
		$grand_total += $vertical_total[$i];
	}
	$k=0;
	$some_array[$k] = $vertical_total[0];
	for($i=1; $i<=$month_counter; $i++){
		if($has_opp[$i]){
			$k++;
			$some_array[] = $vertical_total[$i];
		}
	}
	$some_array[$k+1] = $grand_total;
	$outputArray[] = $some_array;
	$f = array();
	$f[] = 'CLOSED OPPORTUNITY VALUES BY CUSTOMER (BY MONTH)';
	$export = new export() ;
	$export->report_type = 'SalesCalendar';
	$export->filename = $f;
	$export->headers = $header1;
	$export->datas = $outputArray;
	$export->sp_row_height = ReportUtil::calcSalesPersonRowHeight($salesperson_str);	

	$export->xlsx() ;

}


function BuildClosedReport_opps($start_in, $end, $salespersonIds, $salesperson_str) {

	$header1 = array();
	//$header1[] = "SALES CALENDAR REPORT - CLIENT TOTALS";
	//if($weighted == 'true') $header1[] = 'ANTICIPATED REVENUE BY CUSTOMER / WEIGHTED';
	//else
	$header1[] = 'CLOSED OPPORTUNITY VALUES BY CUSTOMER / DETAIL (BY MONTH)'.' WITH CLOSE DATE FROM '.$start_in.' TO '.$end;
	$header2 = array();
	$header2[] = "Run Date:  ".date("n/d/Y")."          User:  ".$_SESSION['USER']['FIRSTNAME'].' '.$_SESSION['USER']['LASTNAME'];
	$outputArray[] = $header2;


	$header3 = array () ;
	$header3[] = $salesperson_str;
	$outputArray[] = $header3;


	$header = array () ;
	//$header[] = "SALES CALENDAR REPORT";
	$header[] = "Company";
	$header[] = "Opportunity/Salesperson(s)";

	$start_date = array () ;

	$from_date = $start_in;

	$from_time = strtotime($from_date);
	$from_date_num = date('d',$from_time);
	$from_month = date('n',$from_time);
	$from_year = date('Y',$from_time);

	$to_time = strtotime($end);
	$to_date_num = date('d',$to_time);
	$to_month = date('n',$to_time);
	$to_year = date('Y',$to_time);

	$time_diff = $to_time - $from_time;
	/* month diff between start and end and plus 2 */
	$month_diff = ceil($time_diff / (24*60*60*30)) + 2;

	$company_names = array();
	$account_money = array();

	$month_counter = 0;
	$k=0;
	$j=0;
	for ($i=$from_month; $i<=($from_month+$month_diff); $i++){
		$month_counter++;
		$j = $i % 12==0? 12 : $i % 12;
		if($k == 0) $k=$from_year;
		if($month_counter == 1) {
			$start = mktime(0, 0, 0, $j, $from_date_num, $k);
			$start_date[] = date("m/d/Y H:i:s", $start);
		} else if($j == $to_month && $k == $to_year){
			$start = mktime(0, 0, 0, $j, 1, $k);
			$start_date[] = date("m/d/Y H:i:s", $start);
			$start = mktime(0, 0, 0, $j, $to_date_num, $k);
			$start_date[] = date("m/d/Y H:i:s", $start);
			break;
		}else {
			$start = mktime(0, 0, 0, $j, 1, $k);
			$start_date[] = date("m/d/Y H:i:s", $start);
		}
		if($i % 12==0) ($k++);
	}
	if($month_counter >= count($start_date)) $month_counter = count($start_date) -1;
	$company_amount = array();
	$has_opp = array();
	$opp_names = array();
	for ($i=0; $i<=$month_counter+3; $i++){
		$has_opp[$i] = false;
		$total_money[$i] = 0;
	}
	$total_money[0] = '';
	$total_money[1] = 'TOTALS';
	$column = 0;
	for ($i=0; $i<$month_counter; $i++){

		$sales_sql = "
			SELECT
				OP.OpportunityID oppId, OP.ContactID,OP.CompanyID ,SalesPersonID,OP.UserID,OpportunityName,
				OP.StartDate,ExpCloseDate,CloseDate,SalesCycleLocID,OP.ResultID ResultID,ResultName,
				R.status, SCLname,SCLPercent,C.Text01,C.Text02,A.Text01 CompanyName, A.AccountID AccountID,
				EstTotal, ActTotal, FirstName, LastName
			FROM
				People P,
				ot_opportunities OP
			left join
				ot_Results R on OP.ResultID = R.ResultID
			left join
				ot_SalesCycleLocation S on OP.SalesCycleLocID = S.SCLID,
				Contact C, Account A
			where
				OP.ContactID = C.ContactID and C.AccountID=A.AccountID and OP.ResultId is not null 
				AND R.status = '2'
				and P.PersonId=SalesPersonID and OP.CompanyID=?";
		/*
		 * lookup ajax.getOppTrackerResult.php
		 * status column is actural code for ResultName here, besso
		 * AND (R.ResultName LIKE 'Won' OR R.ResultName LIKE 'Sold')
		 */
		
		$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']));

		$start = $start_date[$i];
		$sales_sql .= " AND CloseDate >= ? ";
		$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_STRING, $start));

		$end = $start_date[$i+1];
		$sales_sql .= " AND CloseDate <= ? ";
		$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_STRING, $end));

		$sales_sql .= " AND SalesPersonID in (".$salespersonIds.") ";
		$result_list = DbConnManager::GetDb('mpower')->GetSqlResult($sales_sql) ;

		while (	$result = mssql_fetch_assoc($result_list)) {


			for($j=0; $j<=$month_counter+2; $j++){
				$opp_names[$result['AccountID']][$result['oppId']][$j] = '';
			}

			if(!in_array($result['CompanyName'],$company_names)){
				$company_names[$result['AccountID']] = $result['CompanyName'];
				$opp_names[$result['AccountID']][$result['oppId']][0]= $result['CompanyName'];
					
				for($j=0; $j<=$month_counter+2; $j++){
					$account_money[$result['AccountID']][$j] = '';
				}
				$account_money[$result['AccountID']][0] = '';
				$account_money[$result['AccountID']][1] = 'Sub-Total';
			}else
			$opp_names[$result['AccountID']][$result['oppId']][0]= '';

			$opp_names[$result['AccountID']][$result['oppId']][1]= html_entity_decode($result['OpportunityName'],ENT_QUOTES).'/'.html_entity_decode($result['FirstName'],ENT_QUOTES).' '.html_entity_decode($result['LastName'],ENT_QUOTES);

			$opp_names[$result['AccountID']][$result['oppId']][$i+2]= $result['ActTotal'];
			$opp_names[$result['AccountID']][$result['oppId']][$month_counter+2]= $result['ActTotal'];

			$account_money[$result['AccountID']][$i+2] += $result['ActTotal'];
			$account_money[$result['AccountID']][$month_counter+2] += $result['ActTotal'];

			$total_money[$i+2] += $result['ActTotal'];
			$total_money[$month_counter+2] += $result['ActTotal'];
			$has_opp[$i+2] = true;
		}
		if($month_counter > 12){
			if( ($i>=$month_counter-13) || (!isset($header[$i+2]) && $has_opp[$i+2])){
				$column++;
				$has_opp[$i+2] = true;
				if($i != $month_counter) $header[] = date('M \'y',strtotime($start));
			}
		}else{
			$column++;
			$has_opp[$i+2] = true;
			if($i != $month_counter) $header[] = date('M \'y',strtotime($start));
		}

	}

	//print_r($has_opp);

	$header[] = "Totals";
	$outputArray[] = $header;

	foreach($opp_names as $key => $companies){
		foreach ($companies as $opps){
			$i = 0;
			$temp_array = array();
			foreach($opps as $opp_row){
				//echo $i."=".$has_opp[$i]."<br/>";
				if($has_opp[$i] || $i == $month_counter+2 || $i<2){
					$temp_array[] = $opp_row;
				}
				$i++;
			}
			//print_r($temp_array);
			$outputArray[] =  $temp_array;
		}
		$j = 0;
		$temp_array1 = array();
		foreach($account_money[$key] as $total_row){

			if($has_opp[$j] || $j == $month_counter+2 || $j<2){
				$temp_array1[] = $total_row;

			}
			$j++;
		}
			
		$outputArray[] =  $temp_array1;
		$outputArray[] =  array();
	}
	$k = 0;
	$temp_array2 = array();
	//print_r($total_money);
	foreach($total_money as $total){
		if($has_opp[$k] || $k == $month_counter+2 || $k<2){
			$temp_array2[] = $total;

		}
		$k++;
		//$temp_array2[] = $total;
	}
	//print_r($has_opp);

	$outputArray[] =  $temp_array2;

	$f = array();
	$f[] = 'CLOSED OPPORTUNITY VALUES BY CUSTOMER / DETAIL (BY MONTH)';
	$export = new export() ;
	$export->report_type = 'SalesCalendarOpp';
	$export->filename = $f;
	$export->headers = $header1;
	$export->datas = $outputArray;
	$export->sp_row_height = ReportUtil::calcSalesPersonRowHeight($salesperson_str);
	
	$export->xlsx() ;
}

function BuildClosedReport_prods($start_in, $end,$salespersonIds,$salesperson_str) {

	$header1 = array();
	$header1[] = 'CLOSED OPPORTUNITY VALUES BY PRODUCT (BY MONTH)'.' WITH CLOSE DATE FROM '.$start_in.' TO '.$end;
	$header2 = array();
	$header2[] = "Run Date:  ".date("n/d/Y")."          User:  ".$_SESSION['USER']['FIRSTNAME'].' '.$_SESSION['USER']['LASTNAME'];
	$outputArray[] = $header2;


	$header3 = array () ;
	$header3[] = $salesperson_str;
	$outputArray[] = $header3;


	$header = array () ;
	$header[] = "Product Name";
	$header[] = "Company";
	$header[] = "Opportunity/Salesperson(s)";

	$company_names = array();
	$prod_names = array();
	$opp_names = array();
	$account_money = array();
	$opp_money = array();
	$total_money = array();


	$start_date = array () ;
	$this_month = date("n");
	$this_year = date("Y");
	/*
	 //$from_date = date("m/d/Y", strtotime("-".$dateRange, time()));
	 $from_date = date("m/d/Y", strtotime("-".$dateRange.' MONTHS ', time()));
	 //echo $from_date;
	 $from_time = strtotime($from_date);
	 $from_month = date('n',$from_time)+1;
	 $from_year = date('Y',$from_time);
	 //echo $from_month;
	 $company_names = array();
	 $account_money = array();


	 $now = getdate();
	 $today_begin = mktime(0, 0, 0, $now['mon'], $now['mday'], $now['year']);
	 //$time_diff = $today_begin - $from_time;
	 //$month_diff = ceil($time_diff / (24*60*60*30));
	 $month_diff = $dateRange+1;
	 */
	$company_names = array();
	$account_money = array();

	$from_date = $start_in;
	//echo $from_date;
	$from_time = strtotime($from_date);
	$from_date_num = date('d',$from_time);
	$from_month = date('n',$from_time);
	$from_year = date('Y',$from_time);

	$to_time = strtotime($end);
	$to_date_num = date('d',$to_time);
	$to_month = date('n',$to_time);
	$to_year = date('Y',$to_time);

	$time_diff = $to_time - $from_time;
	$month_diff = ceil($time_diff / (24*60*60*30)) + 1;
	$month_counter = 0;
	$k=0;
	$j=0;
	for ($i=$from_month; $i<=($from_month+$month_diff); $i++){
		$month_counter++;
		$j = $i % 12==0? 12 : $i % 12;
		if($k == 0) $k=$from_year;
		if($month_counter == 1) {
			$start = mktime(0, 0, 0, $j, $from_date_num, $k);
			$start_date[] = date("m/d/Y H:i:s", $start);
		} else if($j == $to_month && $k == $to_year){
			$start = mktime(0, 0, 0, $j, 1, $k);
			$start_date[] = date("m/d/Y H:i:s", $start);
			$start = mktime(0, 0, 0, $j, $to_date_num, $k);
			$start_date[] = date("m/d/Y H:i:s", $start);
			break;
		}else {
			$start = mktime(0, 0, 0, $j, 1, $k);
			$start_date[] = date("m/d/Y H:i:s", $start);
		}
		if($i % 12==0) ($k++);
	}
	if($month_counter >= count($start_date)) $month_counter = count($start_date) -1;

	$has_opp = array();
	for ($i=0; $i<=$month_counter+3; $i++){
		$has_opp[$i] = false;
		$total_money[$i] = 0;
	}

	$total_money[0] = 'TOTALS';
	$total_money[1] = '';
	$total_money[2] = '';
	$column = 0;
	for ($i=0; $i<$month_counter; $i++){

		$sales_sql = "
			SELECT
				OP.OpportunityID oppId, OP.ContactID,OP.CompanyID ,SalesPersonID,OP.UserID,
				OpportunityName,OP.StartDate,ExpCloseDate,CloseDate,SalesCycleLocID,OP.ResultID ResultID,
				ResultName, R.status, SCLname,SCLPercent,C.Text01,C.Text02,A.Text01 CompanyName,
				A.AccountID AccountID, EstTotal, FirstName, LastName
			FROM
				ot_opportunities OP
			left join
				ot_Results R on OP.ResultID = R.ResultID
			left join
				ot_SalesCycleLocation S on OP.SalesCycleLocID = S.SCLID,
				Contact C, Account A, People P 
			where
				OP.ContactID = C.ContactID and C.AccountID = A.AccountID and OP.ResultId is not null 
				AND R.status = '2'
				and P.PersonId=SalesPersonID and OP.CompanyID=?";
				// 				AND (R.ResultName LIKE 'Won' OR R.ResultName LIKE 'Sold') 
		$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']));

		$start = $start_date[$i];
		$sales_sql .= " AND CloseDate >= ? ";
		$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_STRING, $start));

		$end = $start_date[$i+1];
		$sales_sql .= " AND CloseDate <= ? ";
		$sales_sql = SqlBuilder()->LoadSql($sales_sql)->BuildSql(array(DTYPE_STRING, $end));

		$sales_sql .= " AND SalesPersonID in (".$salespersonIds.") ";

		$result_list = DbConnManager::GetDb('mpower')->GetSqlResult($sales_sql) ;

		while (	$result = mssql_fetch_assoc($result_list)) {
			$op_sql = "SELECT OpportunityID, OP.ProductID, Est_Quantity, Est_Price, Act_Quantity, Act_Price, ProductName FROM ot_Opp_Prod OP, ot_Products P WHERE
			OpportunityID = ?  and OP.ProductID=P.ProductID and (OP.Deleted != 1 OR OP.Deleted IS NULL)";

			$op_sql = SqlBuilder()->LoadSql($op_sql)->BuildSql(array(DTYPE_INT, $result['oppId']));
			$op_result = DbConnManager::GetDb('mpower')->Exec($op_sql);

			foreach ($op_result as $product) {

				for($j=0; $j<=$month_counter+3; $j++){
					if (!isset($opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][$j])) $opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][$j] = '';
					//$account_money[$result['AccountID']][$j] = 0;
				}
				if(!in_array($product['ProductName'],$prod_names)){

					$prod_names[$product['ProductID']] = $product['ProductName'];
					$opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][0]= $product['ProductName'];

					for($j=0; $j<=$month_counter+3; $j++){
						$account_money[$product['ProductID']][$j] = '';
					}
					$account_money[$product['ProductID']][0] = 'Sub-Total';
					$account_money[$product['ProductID']][1] = '';
					$account_money[$product['ProductID']][2] = '';

				} else if (!isset($opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][0])) $opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][0]= '';

				$opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][1]= html_entity_decode($result['CompanyName'],ENT_QUOTES);
				$opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][2]= html_entity_decode($result['OpportunityName'],ENT_QUOTES) .'/'.html_entity_decode($result['FirstName'],ENT_QUOTES).' '.html_entity_decode($result['LastName'],ENT_QUOTES);

				$opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][$i+3] = $product['Act_Quantity'] * $product['Act_Price'];
				$opp_names[$product['ProductID']][$result['AccountID']][$result['oppId']][$month_counter+3] += $product['Act_Quantity'] * $product['Act_Price'];
					
				$account_money[$product['ProductID']][$i+3] += $product['Act_Quantity'] * $product['Act_Price'];
				$account_money[$product['ProductID']][$month_counter+3] += $product['Act_Quantity'] * $product['Act_Price'];
					
				$total_money[$i+3] += $product['Act_Quantity'] * $product['Act_Price'];
				$total_money[$month_counter+3] += $product['Act_Quantity'] * $product['Act_Price'];

				//print_r($opp_names);
				$has_opp[$i+3] = true;
			}
		}
		/*
		 if($month_diff > 12){
			if( ($i>=$month_diff-13) || (!isset($header[$i+3]) && $has_opp[$i+3])){
			$column++;
			if($i != $month_diff-1) $header[] = date('M \'y',strtotime($start));
			}
			}else{
			//if($i>0){
			$column++;
			if($i != $month_diff-1) $header[] = date('M \'y',strtotime($start));
			//}
			}
			*/
		if($month_counter > 12){
			if( ($i>=$month_counter-13) || (!isset($header[$i+3]) && $has_opp[$i+3])){
				$column++;
				$has_opp[$i+3] = true;
				if($i != $month_counter) $header[] = date('M \'y',strtotime($start));
			}
		}else{
			$column++;
			$has_opp[$i+3] = true;
			if($i != $month_counter) $header[] = date('M \'y',strtotime($start));
		}
	}//for ($i=0; $i<$max_month_number; $i++){


	$header[] = "Totals";
	$outputArray[] = $header;
	//print_r($opp_names);

	foreach($opp_names as $key => $products){
		//$opp_row[] = $company_names[$key];

		foreach ($products as $opps){
			foreach($opps as $opp_row){
				$i = 0;
				$temp_array = array();
				//print_r($opp_row);
				foreach($opp_row as $theprod){
					if($has_opp[$i] || $i == $month_counter+3 || $i<3)
					$temp_array[] = $theprod;
					$i++;
				}

				$outputArray[] =  $temp_array;
			}
		}
		$j = 0;
		$temp_array1 = array();
		foreach($account_money[$key] as $total_row){
			if($has_opp[$j] || $j == $month_counter+3 || $j < 3)
			$temp_array1[] = $total_row;
			$j++;
		}

		$outputArray[] =  $temp_array1;
		$outputArray[] =  array();
	}
	$k = 0;
	$temp_array2 = array();
	foreach($total_money as $total){
		if($has_opp[$k] || $k == $month_counter+3 || $k<3)
		$temp_array2[] = $total;
		$k++;
	}

	$outputArray[] =  $temp_array2;


	$f = array();
	$f[] = 'CLOSED OPPORTUNITY VALUES BY PRODUCT (BY MONTH)';
	$export = new export() ;
	$export->report_type = 'SalesCalendarProd';
	$export->filename = $f;
	$export->headers = $header1;
	$export->datas = $outputArray ;
	$export->sp_row_height = ReportUtil::calcSalesPersonRowHeight($salesperson_str);
	
	$export->xlsx() ;

}
