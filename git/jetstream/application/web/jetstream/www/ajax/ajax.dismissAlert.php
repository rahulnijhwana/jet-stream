<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

SessionManager::Init();
SessionManager::Validate();

$alertRecipientId = strip_tags($_POST['id']);

if($alertRecipientId){
	$sql = "UPDATE AlertRecipient SET Active = 0 WHERE AlertRecipientID = ?";
	$params = array(DTYPE_INT, $alertRecipientId);
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql($params);
	DbConnManager::GetDb('mpower')->Exec($sql);
}
