<?php
/**
 * define BASE PATH
 * @package database
 * class file include
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../'));

require_once BASE_PATH . '/slipstream/class.ModuleMiniCalendar.php';
require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

SessionManager::Init();
SessionManager::Validate();


/**
 * dashboard page next month calendar section
 */
if (isset($_POST['callType'])) {
	$curMonth = $_POST['monthVal'];
	$curYear = $_POST['yearVal'];
	
	if($_POST['callType'] == 'prev') {		
		$curDateTime  = mktime(0, 0, 0, $curMonth-1, 1, $curYear);
	}
	else if($_POST['callType'] == 'next') {
		$curDateTime  = mktime(0, 0, 0, $curMonth+1, 1, $curYear);
	}
	else if($_POST['callType'] == 'today') {
		$curDateTime  = mktime(0, 0, 0, date('m'), 1, date('Y'));
	}
	$moduleMonthInt = date('m', $curDateTime);
	$moduleYear = date('Y', $curDateTime);
	$moduleMonth = date('F', $curDateTime);
	$moduleToday = date('m/d/Y');
	
	$target = (isset($_GET['target']) && $_GET['target'] > 0) ? $_GET['target'] : 0;
		
	/**
	 * Create an object of module mini calendar.	 
	 */	
	$module = new ModuleMiniCalendar($target);
	/*
	 * Numeric representation of the month.
	 * Numeric representation of the year.
	 * Full textual representation of the month.
	 * Day of the month without leading zeros.
	 */	
	$module->month_int = $moduleMonthInt;
	$module->year = $moduleYear;
	$module->month = $moduleMonth;
	$module->today = $moduleToday;

    if(isset($_GET['EventMiniCalendar'])) $module->eventMiniCalendar = true;
		
	if(($module->month_int == date('m')) && ($module->year == date('Y'))) {
		$module->show_today = false;
	}
	else {
		$module->show_today = true;
	}
	
	
	$module->Init();	
	$module->Draw();

	$smarty->assign_by_ref('module', $module);
	$output = $smarty->fetch('snippet_mini_calendar.tpl');
		
	//echo json_encode(array('STATUS' => 'MONTHEVENT', 'WEEKS' => count($weekArr), 'YEAR' => $NextYear, 'MONTH' => $strMonth, 'MONTHINT' => $tmonth, 'DATA' => $fdata));
	echo json_encode(array('month' => $moduleMonthInt, 'year' =>  $moduleYear,'result' => $output));
}
?>