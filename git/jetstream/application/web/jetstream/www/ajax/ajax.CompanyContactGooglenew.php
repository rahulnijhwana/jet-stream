<?php

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

chdir (BASE_PATH . '/slipstream');
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/slipstream/class.ModuleCompanyContactNew.php';
require_once BASE_PATH . '/include/class.SessionManager.php';


SessionManager::Init();
SessionManager::Validate();

$company_id = $_SESSION['company_obj']['CompanyID'];
$user_id = $_SESSION['USER']['USERID'];

$count = count($results);
require_once BASE_PATH . '/include/class.GoogleConnect.php';

$isUseGoogleSync = GoogleConnect::isUseGoogleSync($company_id);

if ( $isUseGoogleSync ) {
	if ( GoogleConnect::hasToken($user_id) ) {
		require_once BASE_PATH . '/include/class.GoogleContacts.php';
		$googleConnect = new GoogleConnect($user_id, false);
		$googleContacts = new GoogleContacts($googleConnect->contacts, $user_id, false);
			if (isset($_POST['action']) && $_POST['action'] == 'inactive'){
				$cid = intval($_POST['id']);
				$googleContacts->deleteGoogleContact($cid);
				}
		}
}else{
	echo "Google Sync Disabled";
	}


