<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.Dashboard.php';

SessionManager::Init();
SessionManager::Validate();

$obj_dashboard = new Dashboard($_SESSION['USER']['COMPANYID'], $_SESSION['USER']['USERID']);
	
$filter_args = array(
	'Page' => FILTER_SANITIZE_NUMBER_INT,
	'RecordsPerPage' => FILTER_SANITIZE_NUMBER_INT,
	'SearchType' => FILTER_SANITIZE_NUMBER_INT,
	'SearchString' => FILTER_SANITIZE_STRING,
	'selectInactive' => FILTER_SANITIZE_NUMBER_INT,
	'AssignedSearch' => FILTER_SANITIZE_NUMBER_INT,
	'SortType' => FILTER_SANITIZE_STRING,
	'SortAs' => FILTER_SANITIZE_NUMBER_INT
);

$inputs = filter_input_array(INPUT_POST, $filter_args);

$search = array();

if ($inputs['Page'] > 0) {
	$search['page_no'] = $inputs['Page'];
} else {
	$search['page_no'] = 1;
}

if($inputs['RecordsPerPage'] > 0) {
	$search['records_per_page'] = $inputs['RecordsPerPage'];
} else {
	$search['records_per_page'] = 50;
}

if($inputs['AssignedSearch']) {
	$search['assigned_search'] = 1;
} else {
	$search['assigned_search'] = 0;
}




//$search['query_type'] = $inputs['SearchType'];
$search['query_type'] = ( $inputs['SearchType']  ==  '' ) ? CONTACT : $inputs['SearchType'] ; 
//echo $search['query_type'];

$search['search_string'] = $inputs['SearchString'];

if($inputs['selectInactive'] == 1) {
	$search['inactive_type'] = 1;
} else {
	$search['inactive_type'] = 0;
}

$search['assign'] = ( isset($_POST['assignto']) ) ? $_POST['assignto'] : ''; 
$search['sort_type'] = $inputs['SearchType'];
$search['Advance'] = ( isset($_POST['Advance'] ) ) ?  $_POST['Advance'] : '' ; 
$search['inclContacts'] = ( isset($_POST['inclContacts'] ) ) ?  $_POST['inclContacts'] : '' ;

if ( isset($_POST['Advance']) ) {
	$search['advanced_filters'] = array();
	
	foreach($_POST as $posted_field => $posted_value) {
		
		if ((substr($posted_field, 0, 8) == 'contact_') || (substr($posted_field, 0, 8) == 'company_')) {
			if ( $posted_value != '' ) {
				$fields = explode("_", $posted_field ); 
				if ( is_array($posted_value)) {
					$search['advanced_filters'][$fields[0]][$fields[1]] = $posted_value;
				}
				else if ( count($fields) == 2 ){
					$search['advanced_filters'][$fields[0]][$fields[1]] = filter_input(INPUT_POST, $posted_field);
				}
				elseif ( count($fields) == 3) {
					$search['advanced_filters'][$fields[0]][$fields[2]][$fields[1]] = filter_input(INPUT_POST, $posted_field);
				}
			}
		}
		else if(substr($posted_field, 0, 8) == 'created_'){
			if ( $posted_value != '' ) {
				$fields = explode("_", $posted_field ); 
				$search['advanced_filters'][$fields[0]][$fields[2]][$fields[1]] = filter_input(INPUT_POST, $posted_field);
			}
		}
		
	}

}
else {
	$search['query_type'] = ( $inputs['SearchType']  ==  '' ) ? BOTH : $inputs['SearchType'] ; 
}


if($inputs['SortAs'] == 1) {
	$search['sort_dir'] = 'DESC';
}
else {
	$search['sort_dir'] = 'ASC';
}

 
$_SESSION['LastSearch'] = $search;

$obj_dashboard->Build($search);
$smarty->assign_by_ref('module', $obj_dashboard);

//$smarty->assign('search_results', $obj_dashboard->SearchResults($search));
//$smarty->assign('record_info', $obj_dashboard->SearchResultsInfo());
//$smarty->assign('page_links', $obj_dashboard->getDashboardPages());
//$smarty->assign('is_manager', $obj_dashboard->is_manager);
//$smarty->assign('limit_account_access', $_SESSION['limit_account_access']);

$output = $smarty->fetch('snippet_dashboard_search_list.tpl');
echo $output;