<?php
set_time_limit(-1); 
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
require_once '../../include/class.GoogleConnect.php';
require_once '../../include/class.GoogleContactBatchPush.php';

$userid = $companyid = '';
if (!empty($_GET['userid'])) {
    $userid = $_GET['userid'];
}
if (!empty($_GET['companyid'])) {
    $companyid = $_GET['companyid'];
}

if ( '' == $userid && '' == $companyid ) {
    require_once '../../include/class.SessionManager.php';
    SessionManager::Init();
    if (!empty($_SESSION['USER']['USERID'])) {
        $userid = $_SESSION['USER']['USERID'];
    }
    if (!empty($_SESSION['USER']['COMPANYID'])) {
        $companyid = $_SESSION['USER']['COMPANYID'];
    }
}

if ( '' == $userid || '' == $companyid ) {
    echo json_encode(array('code'=>'500','message'=>'no user id or company id'));
    exit;
}

//echo '<pre>'; print_r($userid); echo '</pre>';
//echo '<pre>'; print_r($companyid); echo '</pre>'; exit;

$isUseGoogleSync = GoogleConnect::isUseGoogleSync($companyid);
//echo '<pre>'; print_r($isUseGoogleSync); echo '</pre>'; exit;

if ($isUseGoogleSync) {
    if ( GoogleConnect::hasToken($userid) ) {
        $googleConnect = new GoogleConnect($userid, false);
        $oBatch = new GoogleContactBatchPush( $googleConnect->client, $companyid, $userid );
    } else {
        error_log('User has no token in googleToken table : ' . $userid); 
    }
}

echo json_encode(array('code'=>'200'));
