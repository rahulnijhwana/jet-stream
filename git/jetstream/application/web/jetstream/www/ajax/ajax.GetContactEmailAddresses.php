<?php
/**
 * Start the session.
 * define BASE PATH
 * DB API functions file include
 * @package database
 * class file include
 */

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.ContactHint.php';
require_once BASE_PATH . '/slipstream/class.ModuleEmailHold.php';

SessionManager::Init();
SessionManager::Validate();

$contact_id = (isset($_POST['ContactID'])) ? $_POST['ContactID'] : 0;

/**
 * create ContactHint() class object
 */
$obj_contact_hint = new ContactHint();
$contact_email_addresses = $obj_contact_hint->getEmailAddresses($contact_id);

$output = '';
$module = new ModuleEmailHold();
$module->contact_email_addresses = $contact_email_addresses;
$smarty->assign_by_ref('module', $module);
$output = $smarty->fetch('snippet_contact_email_addresses.tpl');

echo $output;


