<?php
//phpinfo();
//var_dump(curl_version());
//exit;

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
require_once '../../include/class.GoogleConnect.php';

/* if param exist, it's CLI call */
$userid = $companyid = '';
if (!empty($_GET['userid'])) {
    $userid = $_GET['userid'];
}
if (!empty($_GET['companyid'])) {
    $companyid = $_GET['companyid'];
}

if ( '' == $userid && '' == $companyid ) {
    require_once '../../include/class.SessionManager.php';
    SessionManager::Init();
    if (!empty($_SESSION['USER']['USERID'])) {
        $userid = $_SESSION['USER']['USERID'];
    }
    if (!empty($_SESSION['USER']['COMPANYID'])) {
        $companyid = $_SESSION['USER']['COMPANYID'];
    }
}

if ( '' == $userid || '' == $companyid ) {
    echo json_encode(array('code'=>'500','message'=>'no user id or company id'));
    exit;
}

$isUseGoogleSync = GoogleConnect::isUseGoogleSync($companyid);

if ($isUseGoogleSync) {

    /******** Deprecated as calendar permission
    // Get all reporting tree
    require_once JETSTREAM_ROOT . '/../slipstream/class.ModuleCalendar.php';
    require_once JETSTREAM_ROOT . '/../include/class.ReportingTreeLookup.php';
    // Get all valid users of the company
    $reportingTreeLookup = new ReportingTreeLookup();
    $moduleCalendar = new ModuleCalendar();
    //echo '<pre>'; print_r($moduleCalendar); echo '</pre>'; exit;

    $permitted_users = array_unique(array_merge(ReportingTreeLookup::CleanGetLimb($companyid, $userid), $moduleCalendar->GetCalPerms($userid)));
    $permitted_users = ReportingTreeLookup::ActiveOnly($companyid, $permitted_users);
    $permitted_users = ReportingTreeLookup::SortPeople($companyid, $permitted_users, 'LastName');
    //if ( count($permitted_users) > 0 ) {
        //$permitted_users = array_reverse($permitted_users);
    //}
    **********/

    $permitted_users = GoogleConnect::getPermittedUsers($userid);
    //echo '<pre>'; print_r($permitted_users); echo '</pre>'; exit;

    foreach ($permitted_users as $user) {
        if ( GoogleConnect::hasToken($user) ) {
            //error_log('Google sync with this user : ' . $user);
            // For sync, we should loop all sales of reporting tree
            $googleConnect = new GoogleConnect($user, true);
        }
    }
}

echo json_encode(array('code'=>'200'));
