<?php
/**
 * Start the session.
 * define BASE PATH
 * DB API functions file include
 * @package Ajax
 * class file include
 */

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.ModuleMiniCalendar.php';

SessionManager::Init();
SessionManager::Validate();

$target = (isset($_GET['target']) && $_GET['target'] > 0) ? $_GET['target'] : 0;

$output = '';
$module = new ModuleMiniCalendar($target);
$module->Init();
$module->Draw();
$module->eventMiniCalendar = true;

$smarty->assign_by_ref('module', $module);
$output = $smarty->fetch('module_mini_calendar.tpl');

echo $output;