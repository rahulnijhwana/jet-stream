<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

SessionManager::Init();
SessionManager::$check_eula = FALSE;
SessionManager::Validate();

$emailId = (int)$_GET['email'];

$sql = "SELECT Body FROM EmailContent WHERE EmailContentID = ? and CompanyID = ?";
$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $emailId), array(DTYPE_INT, $_SESSION['company_obj']['CompanyID']));
$result = DbConnManager::GetDb('mpower')->Exec($sql);

// $text = $result[0]['Body'];

//for ($pos = 0; $pos < strlen($text); $pos ++) {
//	if (ord($text[$pos]) < 20 || ord($text[$pos]) > 126) {
//		echo "[" . ord($text[$pos]) . "]";
//	} else {
//		echo $text[$pos];
//	}
//}

//
// chr(160) (HEX A0) appears in the emails before we started uploading HTML encoding, but not after
if (strstr($result[0]['Body'], chr(160))) {
	echo str_replace(chr(160), '', nl2br($result[0]['Body']));
} else {
	echo $result[0]['Body'];
}
