<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/class.User.php';
require_once BASE_PATH . '/slipstream/class.AccountMap.php';
require_once BASE_PATH . '/slipstream/class.Account.php';
require_once BASE_PATH . '/slipstream/class.Contact.php';
require_once BASE_PATH . '/slipstream/class.Option.php';
require_once BASE_PATH . '/slipstream/class.Status.php';
require_once BASE_PATH . '/slipstream/class.ContactMap.php';
require_once BASE_PATH . '/slipstream/class.OptionSet.php';
require_once BASE_PATH . '/slipstream/class.Validation.php';
require_once BASE_PATH . '/slipstream/class.Section.php';

$objAccountMap = new AccountMap();
$objContactMap = new ContactMap();
$objContact = new Contact();
$objAccount = new Account();
$objOption = new Option();
$objValidation = new Validation();

if (!isset($_SESSION)) session_start();
//print_r($_POST['action']);

if(isset($_POST['btnSubmit']) && $_POST['btnSubmit']=='Submit')
{
	$objUser=new User;
	$userValues = $_POST;
	$result=$objUser->createUser($userValues,$_SESSION['USER']['COMPANYID']);
	echo json_encode($result);
}

if(isset($_POST['btnSubmit']) && $_POST['btnSubmit']=='Update' && isset($_POST['txt_userid']))
{
	$objUser=new User;
	$userValues = $_POST;
	echo json_encode($objUser->updateUser($userValues,$_SESSION['USER']['COMPANYID']));
}elseif (isset($_POST['btnSubmit']) && $_POST['btnSubmit'] == 'GETINFO') {
	$objUser = new User();
	echo json_encode($objUser->getUserByPrimaryKey($_POST['txt_userid']));
} elseif (isset($_POST['btnSubmit']) && $_POST['btnSubmit'] == 'DELETE') {
	$objUser = new User();
	echo json_encode($objUser->deleteUser($_POST['txt_userid']));
}
if (isset($_POST['levelId'])) {
	$objUser = new User();
	echo json_encode($objUser->getAllParentUserByLevelId($_SESSION['USER']['COMPANYID'], $_POST['levelId']));
}
// Check if selected field can be added or not(i.e there should be unused column for the selected type in the db)
if(isset($_GET['btnAdd'])){
	//print_r($_GET);exit;
	$dataType=$_POST['dataType'];
	$dataType=explode(',',$dataType);
	$status=new Status();
	$objUser=new User;
	$field = $_GET['fieldName'];
	if($_GET['btnAdd'] == 'AddAccountField'){
		$totalNumberOfField = $objAccount->getFieldsFromAccount($field);
		$usedFields = $objAccountMap->getFieldsFromAccountMapByCompId($_GET['companyId'],$field);
	}else if($_GET['btnAdd'] == 'AddContactField'){
		$totalNumberOfField = $objContact->getFieldsFromContact($field);
		$usedFields = $objContactMap->getFieldsFromContactMapByCompId($_GET['companyId'],$field);
		//print_r($usedFields);
	}

	$fieldType = '';
	switch ($field){
		case "Text" :
			$fieldType = 'Alphabet';
			break;
		case "Number" :
			$fieldType = 'Numeric';
			break;
		case "Select" :
			$fieldType = 'DropDown';
			break;
		case "Bool" :
			$fieldType = 'CheckBox';
		break;
		case "Date" :
			$fieldType = 'Date';
		break;
	}
	$countData = 0;
	if(isset($dataType) && count($dataType)>0){

		if($field == 'Select'){
			foreach($dataType as $data){

				if($data == 'DropDown' || $data == 'RadioButton'){
					$countData = $countData+1;
				}
			}

		}else{
			foreach($dataType as $data){

				if($data == $fieldType){
					$countData = $countData+1;
				}
			}
		}
	}
	if(count($totalNumberOfField) <=  count($usedFields) + $countData){
		$status->addError('Cannot have more than '.count($totalNumberOfField).' '.$field.' fields');
	}
	echo json_encode($status->getStatus());
}

if(isset($_GET['btnSave']))
	{
		//print_r($_POST);exit;
		$queryString = urldecode($_POST['queryString']);
		$queryString = explode('&',$queryString);
		$data=array();
		foreach($queryString as $q)
		{
			$expStr = explode('=',$q);
			$prefix=substr($expStr[0],strlen($expStr[0])-2,2);
			if($prefix=='[]')
			{
				if(isset($data[$expStr[0]]) && is_array($data[$expStr[0]]))
					$data[$expStr[0]][]=$expStr[1];
				else
				{
					if(isset($data[$expStr[0]]))
					{
						$tmpValue=$data[$expStr[0]];
						$data[$expStr[0]]=array();
						$data[$expStr[0]][]=$tmpValue;
					}
					else
					{
						$data[$expStr[0]][] = $expStr[1];
					}
				}
			}
			else
			{
				//if(count($expStr)>1)
					$data[$expStr[0]] = $expStr[1];
			}
		}
		//print_r($data);exit;
		//print_r($_GET);
		$objUser=new User;
		$status=new Status();
		$companyData = False;
		if(isset($_GET['frmType']) && $_GET['frmType'] == 'Company')
		{
			$companyData = True;
		}
		if(isset($data['label[]']) && isset($data['dataType[]'])){
			$textErrorCount=0;
			$numberErrorCount=0;
			$checkErrorCount=0;
			$dropErrorCount=0;
			$radioErrorCount=0;
			$dateErrorCount=0;
			$companySet = 0;
			$firstNameSet = 0;
			$lastNameSet = 0;
// GMS 10/1/2010
			$contactTitleSet = 0;
			//Fetch different field names from Contact table
			if(isset($companyData) && !$companyData){
				$totalTextFields = $objContact->getFieldsFromContact('Text');
				$totalNumberFields = $objContact->getFieldsFromContact('Number');
				$totalBoolFields = $objContact->getFieldsFromContact('Bool');
				$totalSelectFields = $objContact->getFieldsFromContact('Select');
				$totalDateFields = $objContact->getFieldsFromContact('Date');
			}else{
				//Fetch different field names from Account table
				$totalTextFields = $objAccount->getFieldsFromAccount('Text');
				$totalNumberFields = $objAccount->getFieldsFromAccount('Number');
				$totalBoolFields = $objAccount->getFieldsFromAccount('Bool');
				$totalSelectFields = $objAccount->getFieldsFromAccount('Select');
				$totalDateFields = $objAccount->getFieldsFromAccount('Date');
			}
			for($i=0;$i<count($data['dataType[]']);$i++){

				$objAccountMap = new AccountMap;
				$objContactMap = new ContactMap;
				$objOption = new Option;
				$objUser=new User;
				$isFirstName = NULL;
				$isLastName = NULL;
				$isEmail = NULL;
				$isMandatory = 0;
				$isCompany = 0;
				$contactFieldAccountMapId = NULL;
				$optionType = NULL;
				$rowNo = $data['rowNo[]'][$i];
				$validationId = NULL;
				$accountSelectField = 'selectAccount'.$rowNo;
				$typeSelectedField = 'fieldTypeSelected'.$rowNo;
				$companyNameField = 'isCompanyName'.$rowNo;
				$mandatoryField = 'isMandatory'.$rowNo;
				$firstNameField = 'isFirstName'.$rowNo;
				$lastNameField = 'isLastName'.$rowNo;
// GMS 10/1/2010			
				$contactTitleField = 'isContactTitle'.$rowNo;
				$emailField = 'isEmail'.$rowNo;
				$optionField = 'options'.$rowNo.'[]';
				$optionSetId = 'optionSetId'.$rowNo;
				$optionSet = NULL;
				$validationType = NULL;
				$align = 3;

				if(isset($data[$typeSelectedField]) && $data[$typeSelectedField] != -1){
					$validationId = $objValidation->getValidationIdByName($data[$typeSelectedField]);
				}

				if($data['dataType[]'][$i] == 'RadioButton'){
					$type = 'SelectOptions';
					$optionType = 2;
				}elseif($data['dataType[]'][$i] == 'DropDown'){
					$type = 'SelectOptions';
					$optionType = 1;
				}else{
					$type = $data['dataType[]'][$i];
				}

				switch($type){
					case "Alphabet":
						//Fetch all the used Text fields for contact from ContactMap Table
						if(isset($companyData) && !$companyData){
							$usedTextFields = $objContactMap->getFieldsFromContactMapByCompId($_GET['companyId'],'Text');
						}else{
						//Fetch all the used Text fields for account from AccountMap Table
							$usedTextFields = $objAccountMap->getFieldsFromAccountMapByCompId($_GET['companyId'],'Text');
						}
						$unusedFields = array();
						foreach($totalTextFields as $totalFields){
							$used = False;
							foreach($usedTextFields as $usedFields){
								if($totalFields['FieldName'] == $usedFields['FieldName']){
									$used =True;
								}
							}
							if(!$used){
								$unusedFields[] = $totalFields['FieldName'];
							}
						}

						if(isset($data['label[]'][$i]) && trim($data['label[]'][$i])!='')
						{
							$labelName = $data['label[]'][$i];

							if(isset($data[$companyNameField]) && $data[$companyNameField] == 1)
							{
								foreach($data['usedRow[]'] as $rowData){
									if(isset($data['isCompanyName'.$rowData]) && $data['isCompanyName'.$rowData] == 1){
										$companySet = 1;
									}
								}

								foreach($data['rowNo[]'] as $newRow){
									if($newRow != $rowNo){
										if(isset($data['isCompanyName'.$newRow]) && $data['isCompanyName'.$newRow] == 1){
											$companySet = 1;
										}
									}

								}

								if($companySet != 1){
									$mapId = NULL;
									$mapCount = 0;
									foreach($usedTextFields as $textFields){
										if($textFields['IsCompanyName'] == 1){
											if(isset($companyData) && !$companyData){
												$mapId = $textFields['ContactMapID'];
											}else{
												$mapId = $textFields['AccountMapID'];
											}
										}
									}
									if($mapId != NULL){
										foreach($data['usedRow[]'] as $rowData){
											if(isset($data['mapId'.$rowData]) && $data['mapId'.$rowData] == $mapId){
												$mapCount = 1;
											}
										}
										if($mapCount == 0){
											$companySet = 1;
										}
									}
								}

								if($companySet != 1)
								{
									$isCompany = 1;
								}else{
									if(count($status->error) == 0)
									$status->addError('Cannot have more than one CompanyName field');
								}
							}
							if(isset($data[$firstNameField]) && $data[$firstNameField] == 1)
							{
								foreach($data['usedRow[]'] as $rowData){
									if(isset($data['isFirstName'.$rowData]) && $data['isFirstName'.$rowData] == 1){
										$firstNameSet = 1;
									}
								}

								foreach($data['rowNo[]'] as $newRow){
									if($newRow != $rowNo){
										if(isset($data['isFirstName'.$newRow]) && $data['isFirstName'.$newRow] == 1){
											$firstNameSet = 1;
										}
									}

								}

								if($firstNameSet != 1){
									$mapId = NULL;
									$mapCount = 0;
									foreach($usedTextFields as $textFields){
										if($textFields['IsFirstName'] == 1){
											if(isset($companyData) && !$companyData){
												$mapId = $textFields['ContactMapID'];
											}else{
												$mapId = $textFields['AccountMapID'];
											}
										}
									}
									if($mapId != NULL){
										foreach($data['usedRow[]'] as $rowData){
											if(isset($data['mapId'.$rowData]) && $data['mapId'.$rowData] == $mapId){
												$mapCount = 1;
											}
										}
										if($mapCount == 0){
											$firstNameSet = 1;
										}
									}
								}
								if($firstNameSet != 1)
								{
									$isFirstName = 1;
								}else{
									if(count($status->error) == 0)
									$status->addError('Cannot have more than one FirstName field');
								}
							}
							if(isset($data[$lastNameField]) && $data[$lastNameField] == 1)
							{
								foreach($data['usedRow[]'] as $rowData){
									if(isset($data['isLastName'.$rowData]) && $data['isLastName'.$rowData] == 1){
										$lastNameSet = 1;
									}
								}

								foreach($data['rowNo[]'] as $newRow){
									if($newRow != $rowNo){
										if(isset($data['isLastName'.$newRow]) && $data['isLastName'.$newRow] == 1){
											$lastNameSet = 1;
										}
									}

								}

								if($lastNameSet != 1){
									$mapId = NULL;
									$mapCount = 0;
									foreach($usedTextFields as $textFields){
										if($textFields['IsLastName'] == 1){
											if(isset($companyData) && !$companyData){
												$mapId = $textFields['ContactMapID'];
											}else{
												$mapId = $textFields['AccountMapID'];
											}
										}
									}
									if($mapId != NULL){
										foreach($data['usedRow[]'] as $rowData){
											if(isset($data['mapId'.$rowData]) && $data['mapId'.$rowData] == $mapId){
												$mapCount = 1;
											}
										}
										if($mapCount == 0){
											$lastNameSet = 1;
										}
									}
								}

								if($lastNameSet != 1)
								{
									$isLastName = 1;
								}else{
									if(count($status->error) == 0)
									$status->addError('Cannot have more than one LastName field');
								}
							}
							if(isset($data[$mandatoryField]) && $data[$mandatoryField] == 1)
							{
								$isMandatory = 1;
							}
							if(isset($data[$accountSelectField]) && $data[$accountSelectField] != -1)
							{
								$contactFieldAccountMapId = $data[$accountSelectField];
							}
							if(isset($data[$emailField]) && $data[$emailField] == 1)
							{
								$isEmail = 1;
							}

							if(count($totalTextFields) > count($usedTextFields)){
								$fieldName = $unusedFields[0];
							}else{
								if($textErrorCount != 1){
									$status->addError('Cannot add more than '.count($totalTextFields).' text fields');
									$textErrorCount = 1;
								}
							}
							if(count($status->error) == 0){
								if(isset($companyData) && !$companyData){
									$result = $objContactMap->insertContactFieldsToMap($_GET['companyId'],$fieldName,$labelName,$isMandatory,$optionType,$validationId,$isFirstName,$isLastName,$isEmail,$align,$optionSet,$contactFieldAccountMapId);
								}else{
									$result = $objAccountMap->insertAccountFieldsToMap($_GET['companyId'],$fieldName,$labelName,$isCompany,$isMandatory,$optionType,$validationId,$align,$optionSet);
								}
							}
						}else{
							$status->addError('All label names are mandatory.');
						}
						break;

					case "Numeric":

							//Fetch all the used Text fields for contact from ContactMap Table
							if(isset($companyData) && !$companyData){
								$usedNumberFields = $objContactMap->getFieldsFromContactMapByCompId($_GET['companyId'],'Number');
							}else{
							//Fetch all the used Number fields for account from AccountMap Table
								$usedNumberFields = $objAccountMap->getFieldsFromAccountMapByCompId($_GET['companyId'],'Number');
							}
							$unusedFields = array();
							foreach($totalNumberFields as $totalFields){
								$used = False;
								foreach($usedNumberFields as $usedFields){
									if($totalFields['FieldName'] == $usedFields['FieldName']){
										$used =True;
									}
								}
								if(!$used){
									$unusedFields[] = $totalFields['FieldName'];
								}
							}

							if(isset($data['label[]'][$i]) && trim($data['label[]'][$i])!='')
							{
								$labelName = $data['label[]'][$i];

								if(isset($data[$mandatoryField]) && $data[$mandatoryField] == 1)
								{
									$isMandatory = 1;
								}
								if(isset($data[$accountSelectField]) && $data[$accountSelectField] != -1)
								{
									$contactFieldAccountMapId = $data[$accountSelectField];
								}
								if(count($totalNumberFields) != count($usedNumberFields)){
									$fieldName = $unusedFields[0];

								}else{
									if($numberErrorCount != 1){
										$status->addError('Cannot add more than '.count($totalNumberFields).' number fields');
										$numberErrorCount = 1;
									}
								}

								if(count($status->error) == 0){
									if(isset($companyData) && !$companyData){
										$result = $objContactMap->insertContactFieldsToMap($_GET['companyId'],$fieldName,$labelName,$isMandatory,$optionType,$validationId,$isFirstName,$isLastName,$isEmail,$align,$optionSet,$contactFieldAccountMapId);
									}else{
										$result = $objAccountMap->insertAccountFieldsToMap($_GET['companyId'],$fieldName,$labelName,$isCompany,$isMandatory,$optionType,$validationId,$align,$optionSet);
									}
								}
							}else{
								$status->addError('All label names are mandatory.');

							}
						break;

					case "CheckBox":

							//Fetch all the used Boolean fields for contact from ContactMap Table
							if(isset($companyData) && !$companyData){
								$usedBoolFields = $objContactMap->getFieldsFromContactMapByCompId($_GET['companyId'],'Bool');
							}else{
							//Fetch all the used Boolean fields for account from AccountMap Table
								$usedBoolFields = $objAccountMap->getFieldsFromAccountMapByCompId($_GET['companyId'],'Bool');

							}
							$unusedFields = array();
							foreach($totalBoolFields as $totalFields){
								$used = False;
								foreach($usedBoolFields as $usedFields){
									if($totalFields['FieldName'] == $usedFields['FieldName']){
										$used =True;
									}
								}
								if(!$used){
									$unusedFields[] = $totalFields['FieldName'];
								}
							}

							if(isset($data['label[]'][$i]) && trim($data['label[]'][$i])!='')
							{
								$labelName = $data['label[]'][$i];

								if(isset($data[$mandatoryField]) && $data[$mandatoryField] == 1)
								{
									$isMandatory = 1;
								}
								if(isset($data[$accountSelectField]) && $data[$accountSelectField] != -1)
								{
									$contactFieldAccountMapId = $data[$accountSelectField];
								}
								if(count($totalBoolFields) != count($usedBoolFields)){
									$fieldName = $unusedFields[0];
								}else{
									if($checkErrorCount!=1){
										$status->addError('Cannot add more than '.count($totalBoolFields).' check box fields');
										$checkErrorCount=1;
									}
								}
								if(count($status->error) == 0){
									if(isset($companyData) && !$companyData){
										$result = $objContactMap->insertContactFieldsToMap($_GET['companyId'],$fieldName,$labelName,$isMandatory,$optionType,$validationId,$isFirstName,$isLastName,$isEmail,$align,$optionSet,$contactFieldAccountMapId);
									}else{
										$result = $objAccountMap->insertAccountFieldsToMap($_GET['companyId'],$fieldName,$labelName,$isCompany,$isMandatory,$optionType,$validationId,$align,$optionSet);
									}
								}
							}else{
								$status->addError('All label names are mandatory.');
							}
						break;

					case "SelectOptions":

							//Fetch all the used Select fields for contact from ContactMap Table
							if(isset($companyData) && !$companyData){
								$usedSelectFields = $objContactMap->getFieldsFromContactMapByCompId($_GET['companyId'],'Select');
							}else{
							//Fetch all the used Select fields for account from AccountMap Table
								$usedSelectFields = $objAccountMap->getFieldsFromAccountMapByCompId($_GET['companyId'],'Select');
							}
							$unusedFields = array();
							foreach($totalSelectFields as $totalFields){
								$used = False;
								foreach($usedSelectFields as $usedFields){
									if($totalFields['FieldName'] == $usedFields['FieldName']){
										$used =True;
									}
								}
								if(!$used){
									$unusedFields[] = $totalFields['FieldName'];
								}
							}

							if(isset($data['label[]'][$i]) && trim($data['label[]'][$i])!='')
							{
								$labelName = $data['label[]'][$i];

								if(isset($data[$mandatoryField]) && $data[$mandatoryField] == 1)
								{
									$isMandatory = 1;
								}
								if(isset($data[$accountSelectField]) && $data[$accountSelectField] != -1)
								{
									$contactFieldAccountMapId = $data[$accountSelectField];
								}
								if(count($totalSelectFields) != count($usedSelectFields)){
									$fieldName = $unusedFields[0];
								}else{
									if($dropErrorCount!=1){
										$status->addError('Cannot add more than '.count($totalSelectFields).' dropdown fields');
										$dropErrorCount=1;
									}
								}
								if(isset($data[$optionSetId]) && $data[$optionSetId] != ''){
									$objOptionSet = new OptionSet;
									$objOption = new Option;
									$optionSetDetails = $objOptionSet->getOptionsSetDetailsByOptionSetId($data[$optionSetId]);

									if($optionSetDetails[0]['AccountMapID'] != NULL){
										$options = $objOption->getOptionsByAccountMapId($_GET['companyId'],$optionSetDetails[0]['AccountMapID']);
									}else if($optionSetDetails[0]['ContactMapID'] != NULL){
										$options = $objOption->getOptionsByContactMapId($_GET['companyId'],$optionSetDetails[0]['ContactMapID']);
									}

									if(count($options) == count($data[$optionField])){
										$optionSet = $data[$optionSetId];

										for($m=0;$m<count($data[$optionField]);$m++){

											if(stripslashes($data[$optionField][$m]) != $options[$m]['OptionName'])
											{
												$optionSet = NULL;
												break;
											}
										}
									}else{
											$optionSet = NULL;
									}

								}


								if(count($status->error) == 0){

									if(isset($companyData) && !$companyData){
										$result = $objContactMap->insertContactFieldsToMap($_GET['companyId'],$fieldName,$labelName,$isMandatory,$optionType,$validationId,$isFirstName,$isLastName,$isEmail,$align,$optionSet,$contactFieldAccountMapId);
										$mapId = $objContactMap->getContactMapIdByCompanyAndField($_GET['companyId'],$fieldName);
									}else{

										$result = $objAccountMap->insertAccountFieldsToMap($_GET['companyId'],$fieldName,$labelName,$isCompany,$isMandatory,$optionType,$validationId,$align,$optionSet);
										$mapId = $objAccountMap->getAccountMapIdByCompanyAndField($_GET['companyId'],$fieldName);
									}

									if($optionSet == NULL){
										if(isset($data[$optionField]) && count($data[$optionField]) >= 1){
											foreach($data[$optionField] as $option){
												$objOption = new Option;
												if(isset($companyData) && !$companyData){
													$objOption->insertOptions($_GET['companyId'],trim($option),NULL,$mapId[0]['ContactMapID']);
												}else{
													$objOption->insertOptions($_GET['companyId'],trim($option),$mapId[0]['AccountMapID'],NULL);
												}
											}
											if(isset($companyData) && !$companyData){

												$optionSetObj = new OptionSet;
												$optionSetObj ->insertOptionSet($_GET['companyId'],NULL,$mapId[0]['ContactMapID'],$labelName);
											}else{

												$optionSetObj = new OptionSet;
												$optionSetObj ->insertOptionSet($_GET['companyId'],$mapId[0]['AccountMapID'],NULL,$labelName);
											}
										}
									}
								}
							}else{
								$status->addError('All label names are mandatory.');
							}
						break;

					case "Date":

							//Fetch all the used Date fields for contact from ContactMap Table
							if(isset($companyData) && !$companyData){
								$usedDateFields = $objContactMap->getFieldsFromContactMapByCompId($_GET['companyId'],'Date');
							}else{
							//Fetch all the used Date fields for account from AccountMap Table
								$usedDateFields = $objAccountMap->getFieldsFromAccountMapByCompId($_GET['companyId'],'Date');
							}
							$unusedFields = array();
							foreach($totalDateFields as $totalFields){
								$used = False;
								foreach($usedDateFields as $usedFields){
									if($totalFields['FieldName'] == $usedFields['FieldName']){
										$used =True;
									}
								}
								if(!$used){
									$unusedFields[] = $totalFields['FieldName'];
								}
							}
							if(isset($data['label[]'][$i]) && trim($data['label[]'][$i])!='')
							{
								$labelName = $data['label[]'][$i];

								if(isset($data[$mandatoryField]) && $data[$mandatoryField] == 1)
								{
									$isMandatory = 1;
								}
								if(isset($data[$accountSelectField]) && $data[$accountSelectField] != -1)
								{
									$contactFieldAccountMapId = $data[$accountSelectField];
								}
								if(count($totalDateFields) != count($usedDateFields)){
									$fieldName = $unusedFields[0] ;
								}else{
								if($dateErrorCount!=1){
										$status->addError('Cannot add more than '.count($totalBoolFields).' date fields');
										$dateErrorCount=1;
									}
								}
								if(count($status->error) == 0){
									if(isset($companyData) && !$companyData){
										$result = $objContactMap->insertContactFieldsToMap($_GET['companyId'],$fieldName,$labelName,$isMandatory,$optionType,$validationId,$isFirstName,$isLastName,$isEmail,$align,$optionSet,$contactFieldAccountMapId);
									}else{
										$result = $objAccountMap->insertAccountFieldsToMap($_GET['companyId'],$fieldName,$labelName,$isCompany,$isMandatory,$optionType,$validationId,$align,$optionSet);
									}
								}
							}else{
								$status->addError('All label names are mandatory.');
							}
						break;
				}
			}

		}
		//For update of the edited fields..
		if(isset($data['usedRow[]'])){
			$rowNo = -1;
			$companySet = 0;
			$firstNameSet = 0;
			$lastNameSet = 0;
// GMS 10/1/2010				
			$contactTitleSet = 0;	
			for($i=0;$i<count($data['usedRow[]']);$i++){
				$rowNo = $data['usedRow[]'][$i];
				$label= 'editLabel'.$rowNo;
				$mapIdName = 'mapId'.$rowNo;
				$companyField = 'isCompanyName'.$rowNo;
				$firstNameField = 'isFirstName'.$rowNo;
				$lastNameField = 'isLastName'.$rowNo;
				$emailField = 'isEmail'.$rowNo;
				$mandatoryField = 'isMandatory'.$rowNo;
				$accountSelectField = 'selectAccount'.$rowNo;
				$labelName='';
				$contactFieldAccountMapId = NULL;
				$isCompany = 0;
				$isFirstName = NULL;
				$isLastName = NULL;
				$isEmail = NULL;
// GMS 10/1/2010
				$isContactTitle = NULL;
				$contactTitleField = 'isContactTitle'.$rowNo;	
				$isMandatory = 0;
				$mapIdValue = -1;
				if(isset($data[$label]) ){
					if(trim($data[$label]) != ''){
						if($data['usedType[]'][$i] == 'Alphabet'){
							if(isset($companyData) && !$companyData){
								$usedTextFields = $objContactMap->getFieldsFromContactMapByCompId($_GET['companyId'],'Text');
							}else{
								$usedTextFields = $objAccountMap->getFieldsFromAccountMapByCompId($_GET['companyId'],'Text');
							}

							if(isset($data[$companyField]) && $data[$companyField] == 1)
							{
								foreach($data['usedRow[]'] as $rowData){
									if($rowNo != $rowData){
										if(isset($data['isCompanyName'.$rowData]) && $data['isCompanyName'.$rowData] == 1){
											$companySet = 1;
										}
									}
								}

								if($companySet != 1){
									$mapId = NULL;
									$mapCount = 0;
									foreach($usedTextFields as $textFields){
										if($textFields['IsCompanyName'] == 1){
											if(isset($companyData) && !$companyData){
												$mapId = $textFields['ContactMapID'];
											}else{
												$mapId = $textFields['AccountMapID'];
											}
										}
									}
									if($mapId != NULL){
										foreach($data['usedRow[]'] as $rowData){

											if(isset($data['mapId'.$rowData]) && $data['mapId'.$rowData] == $mapId){
												$mapCount = 1;

											}
										}
										if($mapCount == 0){
											$companySet = 1;
											//exit;
										}
									}
								}
								if($companySet != 1)
								{
									$isCompany = 1;
								}else{
									if(count($status->error) == 0)
									$status->addError('Cannot have more than one CompanyName field');
								}
							}
							if(isset($data[$firstNameField]) && $data[$firstNameField] == 1)
							{
								foreach($data['usedRow[]'] as $rowData){
									if($rowNo != $rowData){
										if(isset($data['isFirstName'.$rowData]) && $data['isFirstName'.$rowData] == 1){
											$firstNameSet = 1;
										}
									}
								}

								if($firstNameSet != 1){
									$mapId = NULL;
									$mapCount = 0;
									foreach($usedTextFields as $textFields){
										if($textFields['IsFirstName'] == 1){
											if(isset($companyData) && !$companyData){
												$mapId = $textFields['ContactMapID'];
											}else{
												$mapId = $textFields['AccountMapID'];
											}
										}
									}
									if($mapId != NULL){
										foreach($data['usedRow[]'] as $rowData){

											if(isset($data['mapId'.$rowData]) && $data['mapId'.$rowData] == $mapId){
												$mapCount = 1;

											}
										}
										if($mapCount == 0){
											$firstNameSet = 1;
										}
									}
								}
								if($firstNameSet != 1)
								{
									$isFirstName = 1;
								}else{
									if(count($status->error) == 0)
									$status->addError('Cannot have more than one FirstName field');
								}
							}

							if(isset($data[$lastNameField]) && $data[$lastNameField] == 1)
							{
								foreach($data['usedRow[]'] as $rowData){
									if($rowNo != $rowData){
										if(isset($data['isLastName'.$rowData]) && $data['isLastName'.$rowData] == 1){
											$lastNameSet = 1;
										}
									}
								}

								if($lastNameSet != 1){
									$mapId = NULL;
									$mapCount = 0;
									foreach($usedTextFields as $textFields){
										if($textFields['IsLastName'] == 1){
											if(isset($companyData) && !$companyData){
												$mapId = $textFields['ContactMapID'];
											}else{
												$mapId = $textFields['AccountMapID'];
											}
										}
									}
									if($mapId != NULL){
										foreach($data['usedRow[]'] as $rowData){

											if(isset($data['mapId'.$rowData]) && $data['mapId'.$rowData] == $mapId){
												$mapCount = 1;

											}
										}
										if($mapCount == 0){
											$lastNameSet = 1;
										}
									}
								}
								if($lastNameSet != 1)
								{
									$isLastName = 1;
								}else{
									if(count($status->error) == 0)
									$status->addError('Cannot have more than one LastName field');
								}
							}
// GMS 10/1/2010							
							if(isset($data[$contactTitleField]) && $data[$contactTitleField] == 1)
							{
								foreach($data['usedRow[]'] as $rowData){
									if($rowNo != $rowData){
										if(isset($data['isContactTitle'.$rowData]) && $data['isC'.$rowData] == 1){
											$contactTitleSet = 1;
										}
									}
								}

								if($contactTitleSet != 1){
									$mapId = NULL;
									$mapCount = 0;
									foreach($usedTextFields as $textFields){
										if($textFields['IsContactTitle'] == 1){
											if(isset($companyData) && !$companyData){
												$mapId = $textFields['ContactMapID'];
											}else{
												$mapId = $textFields['AccountMapID'];
											}
										}
									}
									if($mapId != NULL){
										foreach($data['usedRow[]'] as $rowData){

											if(isset($data['mapId'.$rowData]) && $data['mapId'.$rowData] == $mapId){
												$mapCount = 1;

											}
										}
										if($mapCount == 0){
											$contactTitleSet = 1;
										}
									}
								}
								if($contactTitleSet != 1)
								{
									$isContactTitle = 1;
								}else{
									if(count($status->error) == 0)
									$status->addError('Cannot have more than one ContactTitle field');
								}
							}						
									
							if(isset($data[$emailField]) && $data[$emailField] == 1)
							{
								$isEmail = 1;
							}
						}
						
						$labelName = $data[$label];
						$mapIdValue = $data[$mapIdName];

						if(isset($data[$mandatoryField]) && $data[$mandatoryField] == 1)
						{
							$isMandatory = 1;
						}
						if(isset($data[$accountSelectField]) && $data[$accountSelectField] != -1)
						{
							$contactFieldAccountMapId = $data[$accountSelectField];
						}
						if(count($status->error) == 0){
							if(isset($companyData) && !$companyData){
								$objContactMap = new ContactMap;
								$result = $objContactMap ->updateContactMapById($mapIdValue,$labelName,$isMandatory,$isFirstName,$isLastName,$isEmail,$contactFieldAccountMapId);
								if($data['usedType[]'][$i] == 'DropDown' || $data['usedType[]'][$i] == 'RadioButton'){
									$objOptionSet = new OptionSet;
									$objOptionSet -> updateContactOptionSet($mapIdValue,$labelName);
								}
							}else{
								$objAccountMap = new AccountMap;
								$result = $objAccountMap ->updateAccountMapById($mapIdValue,$labelName,$isMandatory,$isCompany);
								if($data['usedType[]'][$i] == 'DropDown' || $data['usedType[]'][$i] == 'RadioButton'){
									$objOptionSet = new OptionSet;
									$objOptionSet -> updateAccountOptionSet($mapIdValue,$labelName);
								}
							}
						}
					}
					else{
						$status->addError('All label names are mandatory.');
					}
				}

			}
		}
		echo json_encode($status->getStatus());
	}

?>
