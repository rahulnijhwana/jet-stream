<?php
/**
 * Start the session.
 * define BASE PATH
 * DB API functions file include
 * @package database
 * class file include
 */

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.Email.php';
require_once BASE_PATH . '/slipstream/class.ModuleEmailHold.php';

SessionManager::Init();
SessionManager::Validate();

$email_address = (isset($_POST['EmailAddress'])) ? $_POST['EmailAddress'] : '';

/**
 * create ContactHint() class object
 */
$obj_email = new Email();
$emails = $obj_email->getEmails($email_address);

$output = '';
$module = new ModuleEmailHold();
$module->note_list = $emails;
$smarty->assign_by_ref('module', $module);
$output = $smarty->fetch('snippet_email_hold_emails.tpl');

echo $output;