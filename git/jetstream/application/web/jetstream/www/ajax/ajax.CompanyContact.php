<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

chdir (BASE_PATH . '/slipstream');

require_once BASE_PATH . '/slipstream/class.ModuleCompanyContactNew.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/constants.php';
//require_once BASE_PATH . '/include/class.MapLookup.php';

// require_once BASE_PATH . '/slipstream/class.Account.php';
// require_once BASE_PATH . '/slipstream/class.Contact.php';
// require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';

SessionManager::Init();
SessionManager::Validate();

$company_id = $_SESSION['company_obj']['CompanyID'];
$user_id = $_SESSION['USER']['USERID'];

$rec = new ModuleCompanyContactNew($company_id, $user_id);

/*Start Jet-37 for Add Current company in contact */
if(isset($_POST['account'])){
	$_SESSION['USER']['ACCOUNTNAME'] = $_POST['account'];
	}
/*Start Jet-37 for Add Current company in contact */

//$isPrivate = isset($_POST['private']) ? $_POST['private'] : false;

if (isset($_POST['action']) && ($_POST['action'] == 'active' || $_POST['action'] == 'inactive')) {

    echo json_encode($rec->Activate($_POST['type'], $_POST['id'], $_POST['action']));
    exit;
}

require_once BASE_PATH . '/slipstream/Inspekt.php';
$input = Inspekt::makePostCage();
$result = $rec->ValidateInput($input);

//die('ajaxcompanycontact');
/*
if ( $result['status']['STATUS'] == 'OK' ) {
    
    require_once BASE_PATH . '/include/class.GoogleConnect.php';
    $isUseGoogleSync = GoogleConnect::isUseGoogleSync($company_id);

    if ( $isUseGoogleSync ) {
        if ( GoogleConnect::hasToken($user_id) ) {
            require_once BASE_PATH . '/include/class.GoogleContacts.php';
            $googleConnect = new GoogleConnect($user_id, false);
            $googleContacts = new GoogleContacts($googleConnect->contacts, $user_id, false);
            //$googleContacts->getContactsList();
            $contactId = isset($_REQUEST['ID']) ? $_REQUEST['ID'] : '';
            // Update
            if ( '' != $contactId ) {
                $googleContacts->deleteGoogleContact($contactId);
            }
            $contactId = $result['status']['RECID'];
            $addr = $rec->GetMapAddress();
            $googleContacts->insertGoogleContacts($_REQUEST, $contactId);
        }
    }
}
*/
//var_dump($result);exit;
// $result['add'] = TRUE; // For testing, uncommenting this will cause it to treat an edit like an add
if ($result['status']['STATUS'] == 'OK' && !isset($result['dest'])) {
	$rec->CreateModuleFromExisting();
	require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
	$smarty->assign_by_ref('module', $rec);
	$result['main_title'] = $rec->content_title;
	$result['sub_title'] = $rec->content_sub_title;
	$result['modules'][$rec->id]['location'] = 'content';
	$result['modules'][$rec->id]['html'] = $smarty->fetch('shell_module.tpl');

	// I'm not real fond of this method of populating the subnav, but it works
	$result['subnav_items'] = '';
	$first = TRUE;
	foreach ($rec->subnav as $label => $link) {
		$item = "<li";
		if ($first) $item .= ' class="first"';
		$item .= '><a href="' . $link . '">' . $label .'</a></li>';
		$result['subnav_items'] .= $item;
		$first = FALSE;
	}
	
	// We can really speed up reloading the resulting pages by quickly
	// delivering empty modules back when we do an add
	// TODO: Create standard functions for building modules in ajax
	if (isset($result['add']) && $result['add']) {
		include BASE_PATH . '/slipstream/class.ModuleNotes.php';
		$module = new ModuleNotes();
		$module->action = $rec->map['type'];
		$module->rec_id = $rec->rec_id;
		$module->empty_module = TRUE;
		$module->Draw();
		$smarty->assign_by_ref('module', $module);
		$result['modules'][$module->id]['location'] = 'content';
		$result['modules'][$module->id]['html'] = $smarty->fetch('shell_module.tpl');

		include BASE_PATH . '/slipstream/class.ModuleRelatedContacts.php';
		$module = new ModuleRelatedContacts($company_id, $user_id);
		// A new account cannot have any related contacts
		$empty_rec_module = false;
		if ($rec->map['type'] == 'account') {
			$empty_rec_module = TRUE;
		}
		$module->Build($rec->map['type'], $rec->rec_id, FALSE, $empty_rec_module);
		$smarty->assign_by_ref('module', $module);
		$result['modules'][$module->id]['location'] = 'sidebar';
		$result['modules'][$module->id]['html'] = $smarty->fetch('shell_sidebar.tpl');

		include BASE_PATH . '/slipstream/class.ModuleUpcomingEvents.php';
		$module = new ModuleUpcomingEvents();
		$module->type = $rec->map['type'];
		$module->rec_id = $rec->rec_id;
		$module->empty_module = TRUE;
		$module->Draw();
		$smarty->assign_by_ref('module', $module);
		$result['modules'][$module->id]['location'] = 'sidebar';
		$result['modules'][$module->id]['html'] = $smarty->fetch('shell_sidebar.tpl');

		if (isset($_SESSION['mpower']) && $_SESSION['mpower'])  {
			include BASE_PATH . '/slipstream/class.ModuleOpps.php';
			$module = new ModuleOpps();
			$module->user_id = $_SESSION['login_id'];
			$module->Build('Active', $rec->map['type'], $rec->rec_id, FALSE, TRUE);
			$smarty->assign_by_ref('module', $module);
			$result['modules'][$module->id]['location'] = 'sidebar';
			$result['modules'][$module->id]['html'] = $smarty->fetch('shell_sidebar.tpl');
	
			$module->Build('Closed', $rec->map['type'], $rec->rec_id, FALSE, TRUE);
			$result['modules'][$module->id]['location'] = 'sidebar';
			$result['modules'][$module->id]['html'] = $smarty->fetch('shell_sidebar.tpl');
		}
	}
}
$_SESSION['USER']['INSCONTACT']= $result['status']['RECID'];
//~ 
//~ $url = 'https://'.JETSTREAM_HOST.'/ajax/ajax.CompanyContactGoogle.php';
//~ $ch = curl_init($url);
//~ 
//~ curl_setopt($ch, CURLOPT_TIMEOUT_MS, 1000);
//~ curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
//~ curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, FALSE);
//~ curl_setopt($ch, CURLOPT_POST, 1);
//~ echo $sessionId = session_id();
//~ 
//~ curl_setopt($ch, CURLOPT_POSTFIELDS,     "sessionId={$sessionId}");
//~ curl_exec($ch);
//~ curl_close($ch);

echo json_encode($result);
