<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
chdir (BASE_PATH . '/slipstream');
require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.ModuleCallReport.php';

SessionManager::Init();
SessionManager::Validate();

$dt_frm = $_POST['dt_frm'];
$dt_to = $_POST['dt_to'];
$req_id = $_POST['request_id'];
$search_report = $_POST['search_text'];

$uncompleted_tasks = $_POST['uncompleted_tasks'] == 'true' ? 1 : 0;
$uncompleted_events = $_POST['uncompleted_events'] == 'true' ? 1 : 0;

if(isset($_POST['report_user_id']) && ($_POST['report_user_id'] > 0)) {
	$report_user = $_POST['report_user_id'];
}
else {
	$report_user = '';
}

function GetCompletedEvent($event_arr) {
	if($event_arr['IsClosed'] == 1)	{
		return true;
	}
	else {
		return false;
	}
}

if((trim($dt_frm) != '') || (trim($dt_to) != '')) {
	$module = new ModuleCallReport();	
	$module->sel_report_user = $report_user;
	$module->event_list = $module->CreateReport($dt_frm, $dt_to, $uncompleted_tasks, $uncompleted_events);	
	$module->total_event_counts = count($module->event_list);
	$module->completed_event_counts = count(array_filter($module->event_list, "GetCompletedEvent"));
	$module->table_id = "table_".$req_id;
	$module->search_report = $search_report;
	$smarty->assign_by_ref('module', $module);
	$output = $smarty->fetch('snippet_report_details.tpl');
	echo $output;
}

