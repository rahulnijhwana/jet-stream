<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.Oppboard.php';

SessionManager::Init();
SessionManager::Validate();

$obj_dashboard = new Oppboard($_SESSION['USER']['COMPANYID'], $_SESSION['USER']['USERID']);

$filter_args = array(
	'Page' => FILTER_SANITIZE_NUMBER_INT,
	'daysRange' => FILTER_SANITIZE_STRING,
	'SearchType' => FILTER_SANITIZE_NUMBER_INT,
	'SearchString' => FILTER_SANITIZE_STRING,
	'includeActive' => FILTER_SANITIZE_NUMBER_INT,
	'includeWon' => FILTER_SANITIZE_NUMBER_INT,
	'includeLost' => FILTER_SANITIZE_NUMBER_INT,
	'AssignedSearch' => FILTER_SANITIZE_NUMBER_INT,
	'SortType' => FILTER_SANITIZE_STRING,
	'SortAs' => FILTER_SANITIZE_STRING,
	'salesperson_list' => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES)
	//'SortAs' => FILTER_SANITIZE_NUMBER_INT
);

$inputs = filter_input_array(INPUT_POST, $filter_args);
//echo "here";
//echo $inputs['salesperson_list'];

$search = array();

if($inputs['salesperson_list']) {
	$search['salesperson_list'] = json_decode($inputs['salesperson_list']);
} else {
	$search['salesperson_list'] = ReportingTreeLookup::GetLimb($_SESSION['USER']['COMPANYID'], $_SESSION['USER']['USERID']);
}

//print_r($search['salesperson_list']);

if ($inputs['Page'] > 0) {
	$search['page_no'] = $inputs['Page'];
} else {
	$search['page_no'] = 1;
}

if($inputs['daysRange']) {
	$search['age_range'] = $inputs['daysRange'];
} else {
	$search['age_range'] = '90 days';
}

if($inputs['AssignedSearch']) {
	$search['assigned_search'] = 1;
} else {
	$search['assigned_search'] = 0;
}

$search['query_type'] = $inputs['SearchType'];


$search['search_string'] = $inputs['SearchString'];

if($inputs['includeActive'] == 1) {
	$search['active_opp'] = 1;
} else {
	$search['active_opp'] = 0;
}


if($inputs['includeWon'] == 1) {
	$search['won_opp'] = 1;
} else {
	$search['won_opp'] = 0;
}

if($inputs['includeLost'] == 1) {
	$search['lost_opp'] = 1;
} else {
	$search['lost_opp'] = 0;
}

$search['advanced_filters'] = array();
foreach($_POST as $posted_field => $posted_value) {
	if (substr($posted_field, 0, 15) == 'advfilterinput_') {
		$search['advanced_filters'][substr($posted_field, 15)] = filter_input(INPUT_POST, $posted_field);
	}
}
/*
if($inputs['SortType']) {
	$search['sort_type'] = $inputs['SortType'];
} else {
	$search['sort_type'] = 'OpportunityName';
}
*/
$search['sort_type'] = $inputs['SortType'];

//echo "hrere...".$search['sort_type'];
/*
if($inputs['SortAs'] == 1) {
	$search['sort_dir'] = 'DESC';
}
else {
	$search['sort_dir'] = 'ASC';
}
*/

$search['sort_dir'] = $inputs['SortAs'];

$_SESSION['LastOppSearch'] = $search;

$obj_dashboard->Build($search);
$smarty->assign_by_ref('module', $obj_dashboard);

$output = $smarty->fetch('snippet_opp_search_list.tpl');

echo $output;

// echo json_encode(array('output' => $output));
