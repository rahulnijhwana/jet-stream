<?php
/**
 * Start the session.
 * define BASE PATH
 * DB API functions file include
 * @package database
 * class file include
 */

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
require_once BASE_PATH . '/include/mpconstants.php';
require_once BASE_PATH . '/slipstream/class.ModuleOpportunityNew.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

SessionManager::Init();
SessionManager::Validate();

$company_id = $_SESSION['USER']['COMPANYID'];
$user_id = $_SESSION['USER']['USERID'];

$form_filters = array(
	'OpportunityID' => FILTER_SANITIZE_NUMBER_INT,
	'ContactID' => FILTER_SANITIZE_NUMBER_INT,
	'UserID' => FILTER_SANITIZE_NUMBER_INT,
	'DealID' => FILTER_SANITIZE_NUMBER_INT,
	'SalesPersonID' => FILTER_SANITIZE_NUMBER_INT,
	'Contact' => FILTER_SANITIZE_STRING,
	'OpportunityName' => FILTER_SANITIZE_STRING,
	'salesloc' => FILTER_SANITIZE_NUMBER_INT,
	'result' => FILTER_SANITIZE_NUMBER_INT,
	'StartDate' => FILTER_SANITIZE_STRING,
	'ExpCloseDate' => FILTER_SANITIZE_STRING,
	'StartTime' => FILTER_SANITIZE_STRING,
	'EndTime' => FILTER_SANITIZE_STRING,
	'CloseDate' => FILTER_SANITIZE_STRING,
	'Note' => FILTER_SANITIZE_STRING,
	'product_list' => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
	'est_qty_list' => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
	'est_val_list' => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
	'act_qty_list' => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
	'act_val_list' => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
	'est_total' => FILTER_SANITIZE_STRING,
	'act_total' => FILTER_SANITIZE_STRING
);

$form_values = filter_input_array(INPUT_POST, $form_filters);

// Get the repeat detail based on what type of repeat was chosen


$opportunity = new ModuleOpportunityNew($company_id, $user_id);

$no_save = ($form_values['DealID'] != 0) ? true : false;

$results = $opportunity->SaveForm($form_values, $no_save, '');

echo json_encode($results);


function ParseQuery($query) {
	$query = html_entity_decode($query);
	$query = explode('&', $query);
	$output = array();
	
	foreach($query as $param) {
		list($key, $value) = explode('=', $param);
		$output[$key] = $value;
	}
	return $output;
} 
