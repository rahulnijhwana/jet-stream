<?php
/**
 * define BASE PATH
 * Start the session.
 * Run the restore search data script in background.
 */

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/mpconstants.php';

SessionManager::Init();
SessionManager::Validate();
$file_name = RESTORE_SEARCH_FILE;
$php_command = PHP_PATH;
$log_file = PHP_LOG_PATH;

if(isset($_POST['btn_val'])) {
	$btn_val = $_POST['btn_val'];
} else {
	$btn_val = Null;
}

if(isset($_SESSION['company_id']) && ($_SESSION['company_id'] > 0)) {
	$cid = $_SESSION['company_id'];
} else {
	$cid = Null;
}

/*if the user is an admin user then execute this command. */
//if(isset($_SESSION['USER']['SUPERVISORID']) && $_SESSION['USER']['SUPERVISORID'] == -3) {
	//system("/usr/local/bin/php $file_name $btn_val $cid > $log_file &");
	system("/usr/local/bin/php $file_name $btn_val $cid > /dev/null &");
//}
?>