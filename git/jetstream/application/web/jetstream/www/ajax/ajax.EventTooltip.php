<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/class.Event.php';
require_once BASE_PATH . '/slipstream/class.ModuleEventNew.php';

require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/lib.date.php';

SessionManager::Init();
SessionManager::Validate();

$objEvent = new Event();

//$input_filter = array(
//	'EventID' => FILTER_VALIDATE_INT,
//	'd' => FILTER_VALIDATE_INT,
//	'u' => FILTER_VALIDATE_INT
//);
// $ajax_input = filter_input_array(INPUT_GET, $input_filter);

$enc_file = filter_input(INPUT_GET, 'ref');

$ajax_input = json_decode(base64_decode($enc_file), true);

if (!empty($ajax_input['u'])) {
	$user = $ajax_input['u'];
} else {
	$user = -1;
}

$date = TrucatedDateToUnix($ajax_input['d']);

$date = new DateTime(date('r', $date));
//if (strlen($date) == 8) {
//	$year = substr($date, 0, 4);
//	$month = substr($date, 4, 2);
//	$day = substr($date, -2);
//	$date = mktime(0, 0, 0, $month, $day, $year);
//} else {
//	$date = -1;
//}

$events = $objEvent->GetEventById($ajax_input['i'], true, $user, $date);

$output = '';
$module = new ModuleEventNew();
$module->msg = 'Private event - Content not available';
// $module->Init();		


// list($year, $moda) = str_split($ajax_input['d'], 4);
// list($month, $day) = str_split($moda, 2);

//if(isset($_GET['date'])) {
//	$events[0]['IsCancelled'] = 0;
//	$events[0]['IsClosed'] = 0;	
//	$events[0]['date_form'] = $_GET['date'];
//	$events[0]['date_string'] = $_GET['date'];
//}
$module->events = $events;

//echo '<pre>'; print_r($module->events); echo '</pre>'; exit;

$smarty->assign_by_ref('module', $module);
$output = $smarty->fetch('snippet_event_list.tpl');				

echo $output;
//echo "$month $day $year<br>";
//echo date('m/d/Y', $date) . '<br>';

