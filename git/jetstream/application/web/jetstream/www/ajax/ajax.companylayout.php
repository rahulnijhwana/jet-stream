<?php
session_start();
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

include_once (BASE_PATH . '/slipstream/class.Section.php');
require_once BASE_PATH . '/slipstream/class.AccountMap.php';
require_once BASE_PATH . '/slipstream/class.Status.php';
require_once BASE_PATH . '/slipstream/class.Util.php';
include_once('../administrator/include/settings.php');
include_once('../administrator/data/clear_cache.php');


if (isset($_GET['action']) && $_GET['action'] == 'saveAccountSection') {
	
	$status = new Status();
	$objSection = new Section();
	$accountSectionName = $_GET['sectionName'];
	$objUtil = new Util();
	if (!$objUtil->isAlphaNumeric($accountSectionName, array(' ', '-', '.', '/', '(', ')', '[', ']', '{', '}', ',', '?', '!'))) $status->addError('Invalid charcter in Section Name');
	
	$existingSec = $objSection->getAccountSectionByCompanyId($_GET['companyId']);
	foreach ($existingSec as $sec) {
		if ($sec['AccountSectionName'] == $accountSectionName) $status->addError('Sorry cannot have more than one section with same name.');
	}
	if ($status->status == 'OK') {
		$status = $objSection->saveSection($_GET['companyId'], $accountSectionName, NULL);
		$maxSecId = $objSection->getMaxSecIdBySecName($_GET['companyId'], $accountSectionName, NULL);
	}
	
	/* Clear the jetcache directory. */
	clear_jet_cache($ajax_cache_path);
	
	echo json_encode(array('STATUS' => $status->getStatus(), 'NEWSECTIONID' => $maxSecId[0]['SectionID'], 'NEWSECTIONNAME' => $accountSectionName));
} elseif (isset($_GET['action']) && $_GET['action'] == 'editAccountSection') {
	$status = new Status();
	$editedSectionIds = array();
	$editedSectionNames = array();
	$sectionSet = false;
	$editedSectionId = 'editSection';
	$objSection = new Section();
	$objUtil = new Util();
	$sections = $objSection->getAccountSectionByCompanyId($_GET['companyId']);
	foreach ($sections as $sec) {
		if ($sec['AccountSectionName'] != 'DefaultSection') {
			if (isset($_GET[$editedSectionId . $sec['SectionID']])) {
				$sectionSet = true;
				if (!$objUtil->isAlphaNumeric($_GET['editSectionName' . $sec['SectionID']], array(' ', '-', '.', '/', '(', ')', '[', ']', '{', '}', ',', '?', '!'))) {
					if ($status->status == 'OK') {
						$status->addError('Invalid charcter in Section Name');
					}
				}
				if ($status->status == 'OK') {
					$editedSectionIds[] = $sec['SectionID'];
					$editedSectionNames[] = $_GET['editSectionName' . $sec['SectionID']];
					$status = $objSection->updateAccountSectionById($_GET['editSectionName' . $sec['SectionID']], $sec['SectionID']);
				}
			}
		}
	}
	if (!$sectionSet) $status->addError("Please select a section to edit.");
	
	/* Clear the jetcache directory. */
	clear_jet_cache($ajax_cache_path);
	
	echo json_encode(array('STATUS' => $status->getStatus(), 'EDITEDSECTIONSID' => $editedSectionIds, 'EDITEDSECTIONSNAME' => $editedSectionNames));
} elseif (isset($_GET['action']) && $_GET['action'] == 'deleteAccountSection') {
	//print_r($_GET);exit;
	$status = new Status();
	$deletedSectionIds = array();
	$deletedSectionNames = array();
	$sectionSet = false;
	$deletedSectionId = 'deleteSection';
	$deletedSectionName = 'deleteSectionName';
	$objSection = new Section();
	$sections = $objSection->getAccountSectionByCompanyId($_GET['companyId']);
	foreach ($sections as $sec) {
		if ($sec['AccountSectionName'] != 'DefaultSection') {
			if (isset($_GET[$deletedSectionId . $sec['SectionID']])) {
				$sectionSet = true;
				$deletedSectionIds[] = $sec['SectionID'];
				$deletedSectionNames[] = $_GET[$deletedSectionName . $sec['SectionID']];
				$accountMap = new AccountMap();
				$mapIds = $accountMap->getMapIdBySectionId($sec['SectionID']);
				foreach ($mapIds as $map) {
					$accountMap = new AccountMap();
					$accountMap->updateAccountMapSectionId($map['AccountMapID']);
				}
				$status = $objSection->deleteSectionById($sec['SectionID']);
			}
		}
	}
	if (!$sectionSet) $status->addError("Please select a section to delete.");
	
	/* Clear the jetcache directory. */
	clear_jet_cache($ajax_cache_path);
	
	echo json_encode(array('STATUS' => $status->getStatus(), 'DELETEDSECTIONSID' => $deletedSectionIds, 'DELETEDSECTIONSNAME' => $deletedSectionNames));
}

if (isset($_POST['action']) && $_POST['action'] == 'saveCompanySectionLayout') {
	
	$unAssignedData = $_POST['unAssignedData'];
	$unAssignedData = explode('|', $unAssignedData);
	$secArr = array();
	
	$layout = $_POST['dataLayout'];
	$layout = explode('|', $layout);
	for($i = 0; $i < count($layout); $i++)
		$layout[$i] = explode(':', $layout[$i]);
	
	$objSection = new Section();
	$accountSections = $objSection->getAccountSectionByCompanyId($_POST['companyId']);
	$sectionData = array();
	
	for($i = 0; $i < count($layout); $i++) {
		if ($layout[$i][1] != '') {
			$fieldData = explode('_', $layout[$i][1]);
			
			$mapId = $fieldData[count($fieldData) - 1];
			
			$sectionData = explode('_', $layout[$i][0]);
			$sectionName = $sectionData[0];
			$fieldNo = $sectionData[1];
			$position = (round($fieldNo / 2));
			$align = $fieldNo % 2;
			
			for($k = 0; $k < count($accountSections); $k++) {
				$secArr[$k] = $accountSections[$k]['AccountSectionName'];
			}
			
			foreach ($accountSections as $section) {
				if ($sectionName == str_replace(' ', '', $section['AccountSectionName'])) {
					$sectionId = $section['SectionID'];
				}
			}
			
			$objAccountMap = new AccountMap();
			$result = $objAccountMap->updateFieldPositionByAccountMapId($align, $position, $sectionId, $mapId, 1, false);
		}
	}
	for($i = 0; $i < count($unAssignedData); $i++) {
		$objAccountMap = new AccountMap();
		$unAssignedData[$i] = explode('_', $unAssignedData[$i]);
		if ($unAssignedData[$i][0] != '') {
			$objAccountMap = new AccountMap();
			$accountMapId = $unAssignedData[$i][count($unAssignedData[$i]) - 1];
			$result = $objAccountMap->updateFieldPositionByAccountMapId(NULL, NULL, NULL, $accountMapId, 0, true);
		}
	}
	
	/* Clear the jetcache directory. */
	clear_jet_cache($ajax_cache_path);
	
	echo json_encode($accountSections);
}

if (isset($_POST['action']) && $_POST['action'] == 'saveCompanyTabOrder') {
	
	$splitOrder = $_POST['tabOrder'];
	$splitOrder = split('=', $splitOrder);
	$mapId = trim($splitOrder[0]);
	$tabOrder = trim($splitOrder[1]);
	$objAccountMap = new AccountMap();
	$order = $objAccountMap->updateAccountFieldsTabOrderById($mapId, $tabOrder);
	
	/* Clear the jetcache directory. */
	clear_jet_cache($ajax_cache_path);
	
	echo json_encode($order);
}

?>