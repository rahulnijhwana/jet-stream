<?php

if (!defined('BASE_PATH')) {
	define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
}

require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.s3.php';

SessionManager::Init();
SessionManager::Validate();


if (empty($_POST['jetfileid']) ){
	echo "ERROR no Jetfileid " . $_POST['jetfileid'] ; 
	exit ; 
}

$userid = $_SESSION['USER']['USERID'];
$companyid = $_SESSION['company_obj']['CompanyID']; 
$jetfileid = $_POST['jetfileid'] ; 

if (!empty($_POST['action']) && $_POST['action'] == 'rename'){
 
	if (empty($_POST['param1']) || empty($_POST['jetfileid']) ){
		// ERROR // 
		exit ; 
	}
	$labelname = $_POST['param1']; 
	$jetfileid = $_POST['jetfileid'] ; 

	$sql = 'UPDATE JetFile SET LabelName = ? , LastModifiedOn = GETDATE() , LastModifiedBy = ? WHERE JetFileID = ? ' ; 
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $labelname), array(DTYPE_INT, $userid ), array(DTYPE_INT, $jetfileid ));

	$results = DbConnManager::GetDb('mpower')->GetOne($sql);
	echo 'done';
}

else if (!empty($_POST['action']) && $_POST['action'] == 'createFolder'){
	if (empty($_POST['param1']) || empty($_POST['jetfileid']) ){
		// ERROR // 
		echo "ERROR";
		exit ; 
	}
/*	
	if (preg_match('/\d+/', $_POST['param2'])) {
		$type = 'Account';
		$id = $_POST['param2']; 
		$labelname = $_POST['param1']; 

	} else {
    	$type = 'Contact';
		$id = $_POST['param1']; 
		$labelname = $_POST['param2']; 

	}*/
	$type = $_POST['param1'];
	$id = $_POST['param2'];
	$labelname = $_POST['param3']; 
	

	$sql ="INSERT INTO JetFile VALUES (?, ?, ?, ?, 1, ?, GETDATE(), '', NULL, '', NULL, ?, 1 , 0 , 0 )"; 
 	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $labelname), array(DTYPE_INT, $companyid), array(DTYPE_INT, $id), array(DTYPE_INT, $jetfileid),  array(DTYPE_INT, $userid), array(DTYPE_STRING, $type));

	$results = DbConnManager::GetDb('mpower')->DoInsert($sql);
	echo 'Success';
}

else if (!empty($_POST['action']) && $_POST['action'] == 'delete'){

	$sql = 'UPDATE JetFile SET DeletedBy = ? , DeletedOn = GETDATE() WHERE JetFileID = ? ' ; 
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $userid), array(DTYPE_INT, $jetfileid ));
//	echo $sql ; 
	$results = DbConnManager::GetDb('mpower')->execute($sql);
	echo 'Success';
}

else if (!empty($_POST['action']) && $_POST['action'] == 'move'){

	if (empty($_POST['param1'])  ){
		echo 'error';
		exit ; 
	}
	
	$sql = 'UPDATE JetFile SET ParentID = ? ,  LastModifiedOn = GETDATE() , LastModifiedBy = ? WHERE JetFileID = ? ' ; 
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $parentID), array(DTYPE_INT, $userid ), array(DTYPE_INT, $jetfileid ));
	$results = DbConnManager::GetDb('mpower')->GetOne($sql);
	echo 'Success';
}

else if (!empty($_POST['action']) && ( $_POST['action'] == 'open' || $_POST['action'] == 'saveAs' ) ){
	if (empty($_POST['param1']) || empty($_POST['param2'])) return 'ERROR' ;
	
	$type = $_POST['param1'] ; // Account or Contact
	$id = $_POST['param2'] ;

	$sql = "SELECT j.LabelName, f.ContentType, f.Extension FROM JetFile j, FileType f WHERE J.JetFileID = ? AND j.FileTypeID = f.FileTypeID;";

	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $jetfileid)); 
	$extension = DbConnManager::GetDb('mpower')->Exec($sql);
	$contentType = $extension[0]['ContentType'];
	$ext = $extension[0]['Extension'];
	$label = $extension[0]['LabelName'];
	$s3path = $companyid . '/' . $type . '/'. $id . '/'.$jetfileid .'.'. $ext ;
	
	$jetfile = new JetStreamS3(); 
	//if ( $_POST['action'] == 'saveAs')
	$jetfile->filename = '"' . $label . '"';  
	
	$jetfile->getFile($s3path, $contentType) ; 
	
}

else if (!empty($_POST['action']) && $_POST['action'] == 'checkFolderEmpty' ){
	
	echo CheckEmptyFolder($jetfileid);
}

else if (!empty($_POST['action']) && $_POST['action'] == 'deleteAll'){

	$sql = 'UPDATE JetFile SET DeletedBy = ? , DeletedOn = GETDATE() WHERE JetFileID in ( SELECT JetFileID where JetFileID = ? or ParentID = ? ) ' ; 
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $userid), array(DTYPE_INT, $jetfileid ), array(DTYPE_INT, $jetfileid ));
	$results = DbConnManager::GetDb('mpower')->execute($sql);
	echo 'Success';
}

else if (!empty($_POST['action']) && $_POST['action'] == 'Moveto') {

	$destination = $_POST['param1']; 

	$sql = 'UPDATE JetFile SET ParentID = ? WHERE JetFileID = ?' ; 
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $destination), array(DTYPE_INT, $jetfileid ));

	$results = DbConnManager::GetDb('mpower')->execute($sql);
	echo 'Success';
}

else if (!empty($_POST['action']) && $_POST['action'] == 'getTags') {  

	$results = getTags ($companyid); 
	
	echo '<select name="TagSelect[]" id=multiselect2 multiple=multiple >';
	foreach ($results as $result){
		echo "<option value=\"{$result['n']}\">{$result['labelName']}</option>";
	}
	echo '</select>';

}

else if (!empty($_POST['action']) && $_POST['action'] == 'updateTags') {  
	if (empty($_POST['param1'])) return 'ERROR' ;

	$param1 = $_POST['param1'];
	$nValue = calc_tags_nValue($param1); 
	
	
	$sql = 'UPDATE JetFile SET TagValue = ? WHERE JetFileID = ? ' ; 	
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $nValue), array(DTYPE_STRING, $jetfileid));
	$results = DbConnManager::GetDb('mpower')->exec($sql);
	
	echo "Success";
}

else if (!empty($_POST['action']) && $_POST['action'] == 'getProperty') {  
	
	$results = getOneProperties($jetfileid ); 
	
	$allTags = getTags($companyid); 
	$checkbox = '<div name=tagdetails id=tagdetails style=\'display:none\'><table cellpadding=0 cellspacing=0 border=0><tbody>' ; 
	$checkbox .= '<tr>'; 
	$x = 0 ;
	$summary = array () ; 
	$check = '' ; 
	
	foreach ($allTags as $tags ){
		if ( ($x % 3) == 0 ) $checkbox .= '<tr>'; 

		if ( ($results['TagValue'] & pow(2, $tags['n']))  == pow(2, $tags['n'])){
			$check = 'checked';
			$summary[] = $tags['labelName']; 
		}   
		else $check = ''; 
		
		$checkbox .= "<td><input name=\"propertytags\"  id=\"propertytags\" value=\"{$tags['n']}\" type=\"checkbox\" {$check}> {$tags['labelName']} </td>"; 

		if ( $x %3 == 2  )  $checkbox .= '</tr>';
		$x++ ; 
	}
	if ( ($x % 3) != 0 ) $checkbox .= '</tr>'; 
	
	$checkbox .= '</tbody></table></div>';

	$tagbox = '<div name=tagsummary id=tagsummary> '. implode( ', ', $summary) .' </div>' . $checkbox;

	$htmltext = '' ; 
	$htmltext .= "<div align=center>";
	$htmltext .= "<input type=hidden value=\"$jetfileid\" name=\"jetid\" id=\"jetid\">";
	$htmltext .= "<b>{$results['LabelName']} Properties</b>";
	$htmltext .= "<table border=0 cellspacing=0 cellpadding=0 width=100%>";
	$htmltext .= "<tr><td colspan=2 align=center> <hr size=1 width=90% color=\"#dddddd\" ></td></tr>";
	$htmltext .= "<tr><td align=right width=100px><img src=\"images/{$results['Icon']}.png\"> &nbsp; &nbsp; </td><td align=left><input type=text size=30 value=\"{$results['LabelName']}\" name=\"propertyLabel\" id=\"propertyLabel\"></td></tr>";
	$htmltext .= "<tr><td colspan=2 align=center><hr size=1 width=90% color=\"#dddddd\"></td></tr>";
	$htmltext .= "<tr><td align=right>Category: </td><td align=left> {$results['Type']}</td></tr>";
	$htmltext .= "<tr><td align=right>Size: </td><td align=left> {$results['Size']} bytes</td></tr>";
	$htmltext .= "<tr><td colspan=2 align=center><hr size=1 width=90% color=\"#dddddd\"></td></tr>";
	$htmltext .= "<tr><td align=right>Created On: </td><td align=left> {$results['CreatedOn']}</td></tr>";
	$htmltext .= "<tr><td align=right>Created By: </td><td align=left> {$results['LoginName']}</td></tr>";
	$htmltext .= "<tr><td colspan=2 align=center><hr size=1 width=90% color=\"#dddddd\"></td></tr>";
	$htmltext .= "<tr><td align=right valign=top ><div onclick=\"javascript:ToogleTags()\"><img src=\"images/plus.gif\" id=tagimg name=tagimg>{$results['tags']} Tags: </div></td><td align=left>" . $tagbox . "</td></tr>";
	$htmltext .= "</table></div>" ; 
	
	echo $htmltext ; 

}

else if (!empty($_POST['action']) && $_POST['action'] == 'updateProperty') {

	$label = $_POST['param1']; 
	$tags =  $_POST['param2'] ; 
	$n = calc_tags_nValue( $tags );
	

	$sql = 'UPDATE JetFile SET TagValue = ? , LabelName = ? , LastModifiedOn = GETDATE() , LastModifiedBy = ? WHERE JetFileID = ? ' ; 	
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $n), array (DTYPE_STRING, $label) , array(DTYPE_INT, $userid), array(DTYPE_INT, $jetfileid));
//	file_put_contents('D:\\webtemp\\test.txt', $sql);
	$results = DbConnManager::GetDb('mpower')->exec($sql);
	
	echo "Success";

}

/*************** SUPPORTING FUNCTIONS **************************/

function CheckEmptyFolder ($ParentID){
	
	$sql = 'SELECT COUNT(JetFileID) as \'computed\' FROM JetFile WHERE  ParentID = ?  ';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $ParentID));

	$results = DbConnManager::GetDb('mpower')->exec($sql);
	 
	return $results[0]['computed'];

} 

function calc_tags_nValue( $param ){
	$vals = is_array($param) ? $param : explode("|", $param); 
	
	$n = 0 ; 
	
	foreach ( $vals as &$val ){
		if ($val != '')	$n = $n + pow(2, $val); 
	}
	
	return $n ;
}

function getTags ($companyid){
	$sql = 'SELECT n, labelName from JetTags where CompanyID= ? and DeletedOn is NULL ' ; 
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $companyid));
	$results = DbConnManager::GetDb('mpower')->exec($sql);

	return $results ; 
}

function getOneProperties( $jetfileid){
	$sql = "SELECT Jetfile.* , FileType.* , (People.FirstName + ' '+ People.LastName) as LoginName FROM JetFile "; 
	$sql .= 'RIGHT JOIN filetype on Jetfile.filetypeid = filetype.filetypeid ' ; 	
	$sql .= 'LEFT OUTER JOIN People on People.PersonID = Jetfile.CreatedBy ' ; 	
	$sql .= 'WHERE JetFileID = ? ' ; 	
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql( array(DTYPE_STRING, $jetfileid));
	$results = DbConnManager::GetDb('mpower')->getOne($sql);
	
	return $results ; 
} 


?>