<div style="float: right; text-align: left;">
	<select id="snoozer" name="snoozer" style="margin: 12px 10px 0px 0px">
		<option value="5">5 &nbsp;Minutes</option>
		<option value="10">10 Minutes</option>
		<option value="15">15 Minutes</option>
		<option value="30">30 Minutes</option>
		<option value="60">1 Hour</option>
		<option value="120">2 Hours</option>
		<option value="240">4 Hours</option>
		<option value="480">8 Hours</option>
		<option value="720">12 Hours</option>
		<option value="1440">24 Hours</option>
	</select>
</div>