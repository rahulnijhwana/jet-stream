<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/class.CompanyContact.php';
require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/class.ReportingTree.php';

SessionManager::Init();
SessionManager::Validate();

$obj_comp_con = new CompanyContact();

if(isset($_POST['show']) && ($_POST['show'] == 1)) {
	
	$output = $obj_comp_con->DrawManageForm($_POST['ID'], $_POST['action']);
	echo $output;
}
else {
	if(isset($_POST['people'])) {
		$obj_comp_con->children = $_POST['children'];
		$obj_comp_con->assigned_people_arr = $_POST['people'];	
	}
	
	$obj_comp_con->AssignPeople($_POST['ID'], $_POST['action']);
}