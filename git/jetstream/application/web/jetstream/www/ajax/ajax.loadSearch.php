<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/class.Dashboard.php';
require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/class.ReportingTree.php';
require_once BASE_PATH . '/slipstream/class.ModuleSavedSearch.php';


SessionManager::Init();
SessionManager::Validate();

$result = array();

if(isset($_POST) & (count($_POST) > 0))
{
	foreach($_POST as $key=>$val)
	{
		$result[] = array('name' => $key, 'nameval' => $val);
	}
}

echo json_encode(array('result' => $result));
?>