<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.Dashboard.php';
require_once BASE_PATH . '/include/class.MapLookup.php';
require_once BASE_PATH . '/include/class.OptionLookup.php';
require_once BASE_PATH . '/slipstream/class.export.php';
require_once BASE_PATH . '/slipstream/class.CompanyContactNew.php';

SessionManager::Init();
SessionManager::Validate();

/***********Save Group List*********/
if ( isset($_POST['action']) && ( $_POST['action'] == 'save')  ) {
	
	$savedList['account'] = json_decode(str_replace('\\', '', $_POST['account'])); 
	$savedList['contact'] = json_decode(str_replace('\\', '', $_POST['contact'])); 
	$savedList['label'] = $_POST['groupname'];
	$savedList['id']= $_POST['groupid'];

	$account = ( count($savedList['account']) > 0 ) ?  implode (";", $savedList['account']) : ''; 
	$contact = ( count($savedList['contact']) > 0 ) ?  implode (";", $savedList['contact']) : ''; 

	if ( $savedList['id'] < 1  ){ // save New
		$sql = "INSERT INTO ExportField (LabelName, CompanyID, ContactID, Account, Contact ) VALUES (?, ?, ?, ?, ?)";		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $savedList['label']), array(DTYPE_INT, $_SESSION['USER']['COMPANYID']), array(DTYPE_INT, $_SESSION['USER']['USERID']), array(DTYPE_STRING, $account), array(DTYPE_STRING, $contact));
		$results = DbConnManager::GetDb('mpower')->DoInsert($sql);
	}
	else { // edit 
		$sql = "UPDATE ExportField
				SET LabelName = ? ,
					CompanyID = ? ,
					ContactID = ? ,
					Account = ? ,
					Contact = ? 
				WHERE ExportFieldID = ? ";		
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $savedList['label']), array(DTYPE_INT, $_SESSION['USER']['COMPANYID']), array(DTYPE_INT, $_SESSION['USER']['USERID']), array(DTYPE_STRING, $account), array(DTYPE_STRING, $contact), array(DTYPE_INT, $savedList['id']));
		$results = DbConnManager::GetDb('mpower')->Exec($sql);
	}
}

/***********Delete Group List*********/
if ( isset($_POST['action']) || ($_POST['action'] == 'delete')){
//	echo "im in";
	$sql = "UPDATE ExportField SET Deleted= GETDATE() WHERE ExportFieldID = ?";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_POST['groupid']));
	$results = DbConnManager::GetDb('mpower')->Exec($sql);
	echo "success";
	
}
/***********Get Group List*********/
if ( (isset($_GET['group'])) && ($_GET['group'] != 0 )){
	// get information from table
	$sql = "SELECT ExportFieldID, LabelName, Account, Contact from ExportField WHERE ExportFieldID = ?";		
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_GET['group']));
	
	$results = DbConnManager::GetDb('mpower')->Execute($sql);
	
	$savedList['label'] = $results[0]['LabelName'];
	$savedList['id']= $results[0]['ExportFieldID'];
	$savedList['account']= explode(";", $results[0]['Account']); 
	$savedList['contact']= explode(";", $results[0]['Contact']); 
}
else {
	$savedList['label'] = 'untitled group';
	$savedList['id']= 0;
	$savedList['account']= array(); 
	$savedList['contact']= array(); 
} 

/***********Get the Custom Field*********/
if ( (isset($_GET['action']))  && ($_GET['action'] == 'edit') ){
	$ContactLayouts =  MapLookup::GetContactLayout($_SESSION['USER']['COMPANYID'] );
	$AccountLayouts =  MapLookup::GetAccountLayout($_SESSION['USER']['COMPANYID'] );
	$ContactMaps =  MapLookup::GetContactMap($_SESSION['USER']['COMPANYID'] );
	$AccountMaps =  MapLookup::GetAccountMap($_SESSION['USER']['COMPANYID'] );
	
	$output = '' ; 
	$output .= '<div class="advance_section_head" style="font-weight:100" ><div> Check all the fields that are to be included in the export file.</div>';
	$output .='<div style="background-color:#F0F5FA;">  ';
	$output .='<ul><li> To save an export group enter a name and click <b>Save</b></li>';
	$output .='<li> To save the excel file, click <b>Export</b></li></ul> </div>';	
	$output .= '<table border=0 cellpadding=0 cellspacing=0 width=100%>' ;
	$output .= '<tr><td>' ;
	$output .= '<div class="advance_section_head"  >'.'Company'.'</div>';
	$output .= '</td></tr>' ;

	$i = 0 ; 
	
	foreach ($AccountLayouts as &$AccountLayout ) {
	
		// print header 
		$output .= '<tr><td>' ;
		$output .= '<div class="advance_section_head_b"  onclick="javascript:ToggleHidden(\'section'.$i.'\');"> <img id="toggle_section'.$i.'" src="images/plus.gif"/>'.$AccountLayout['SectionName'].'</div>';
		$output .= '<div id="section'. $i .'" class="advance_datagrid hidden" style="display:hidden">';
		$output .= '<table cellpadding=0 cellspacing=0  >' ; 
		foreach ( $AccountLayout['rows'] as &$row ) {
			$output .= '<tr>' ; 
			foreach ( array(1,0) as $position){
				if (isset($row[$position] )){ 
					if ( $AccountMaps['fields'][$row[$position]]['FieldName'] ) {
						$output .= '<td width=2% ><input type=checkbox name=account value='.$AccountMaps['fields'][$row[$position]]['FieldName'];
						if (in_array($AccountMaps['fields'][$row[$position]]['FieldName'], $savedList['account'])) $output .= ' checked=yes></td>';
						else $output .= ' ></td>';
						$output .= "<td width=48% class=label1>".$AccountMaps['fields'][$row[$position]]['LabelName']."</td>";
					}
					else {
						$output .= '<td width=2%> </td><td width=48%> </td>';
					}
				}
			}
			$output .= '</tr>' ; 
		}	
		$output .= '</table>' ; 
		$output .= '</div></div></td></tr>' ;
		$i++; 
	}
	
	$output .= '<tr><td>' ;
	$output .= '<div class="advance_section_head" >'.'Contact'.'</div>';
	$output .= '</div></td></tr>' ;
	
	foreach ($ContactLayouts as &$ContactLayout ) {
	
		// print header 
		$output .= '<tr><td>' ;
		$output .= '<div class="advance_section_head_b"  onclick="javascript:ToggleHidden(\'section'.$i.'\');"> <img id="toggle_section'.$i.'" src="images/plus.gif"/>'.$ContactLayout['SectionName'].'</div>';
		$output .= '<div id="section'. $i .'" class="advance_datagrid hidden" style="display:hidden">';
		$output .= '<table cellpadding=0 cellspacing=0  >' ; 
		foreach ( $ContactLayout['rows'] as &$row ) {
			$output .= '<tr>' ; 
			foreach ( array(1,0) as $position){
				if (isset ($row[$position])){
					$output .= '<td width=2% ><input type=checkbox name=contact value='.$ContactMaps['fields'][$row[$position]]['FieldName'];
					if (in_array($ContactMaps['fields'][$row[$position]]['FieldName'], $savedList['contact'])) $output .= ' checked=yes></td>';
					else $output .= ' ></td>';
					$output .= "<td width=48% class=label1>".$ContactMaps['fields'][$row[$position]]['LabelName']."</td>";
				}
			}
			$output .= '</tr>' ; 
		}	
		$output .= '</table>' ; 
		$output .= '</div></div></td></tr>' ;
		$i++; 
	}
//background:#909CB2;
	$output .='</table>' ; 
	$output .='<div class="advance_section_head" style="font-weight:100" >Save As: <input type=text name=groupname size=20 maxlength=20 value="'.$savedList['label'];
	$output .= '"> <input type=hidden name=groupid value='.$savedList['id'] .'></div></div>';
	
	echo $output; 

}



?>
