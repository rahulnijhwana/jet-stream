<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.ModuleAdvanceSearchFilter.php';

SessionManager::Init();
SessionManager::Validate();

$module = new ModuleAdvanceSearchFilter();

echo $module->CreateFilterEntry(filter_input(INPUT_POST, 'field'), filter_input(INPUT_POST, 'form_id'));
