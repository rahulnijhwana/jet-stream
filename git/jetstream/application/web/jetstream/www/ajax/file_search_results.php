<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.FileSearch.php';

SessionManager::Init();
SessionManager::Validate();


$obj_dashboard = new FileSearch($_SESSION['USER']['COMPANYID'], $_SESSION['USER']['USERID']);

$filter_args = array(
	'Page' => FILTER_SANITIZE_NUMBER_INT,
	'RecordsPerPage' => FILTER_SANITIZE_NUMBER_INT,
	'SearchType' => array( 'filter' =>FILTER_SANITIZE_STRING, 'flags' => FILTER_REQUIRE_ARRAY) , 
	'filename' => FILTER_SANITIZE_STRING,
	'tags' => array( 'filter' =>FILTER_SANITIZE_NUMBER_INT, 'flags' => FILTER_REQUIRE_ARRAY) , 
	'datefrom' => FILTER_SANITIZE_STRING,
	'dateto' => FILTER_SANITIZE_STRING
);

$inputs = filter_input_array(INPUT_POST, $filter_args);

$search = array();

if ($inputs['Page'] > 0) {
	$search['page_no'] = $inputs['Page'];
} else {
	$search['page_no'] = 1;
}

if($inputs['RecordsPerPage'] > 0) {
	$search['records_per_page'] = $inputs['RecordsPerPage'];
} else {
	$search['records_per_page'] = 50;
}

if($inputs['SearchType'] != '' ) {
	$search['searchtype'] = $inputs['SearchType'];
} else {
	$search['searchtype'] = array( 'Account', 'Contact', 'Library', 'Event', 'Notes' );
}

$search['searchstring'] = $inputs['filename'] ; 
$search['dateto'] = $inputs['dateto'] ; 
$search['datefrom'] = $inputs['datefrom'] ; 
$search['tags'] = $inputs['tags']; 


$_SESSION['FileSearch'] = $search;

$obj_dashboard->Build($search);
$smarty->assign_by_ref('module', $obj_dashboard);

$output = $smarty->fetch('snippet_file_search_list.tpl');

echo $output;

