<?php
//ob_clean();
// session_start();
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once '../../include/mpconstants.php';
require_once (BASE_PATH . '/slipstream/class.Note.php');
require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.fieldsMap.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.ModuleNotes.php';

SessionManager::Init();
SessionManager::Validate();

$objNote = new Note();


if(trim($_POST['Keyword']) != '') {
	$objNote->searchString = $_POST['Keyword'];
}

$filterFields = array();
if(isset($_REQUEST['filterOpt'])) {
	$filterFields = $_REQUEST['filterOpt'];
}
$filterArray = array();

if(is_array($filterFields) && (count($filterFields) > 0)) {
	foreach($filterFields as $filterKey => $filterVal) {
		$newFilterKey = $filterKey + 1;

		if(($filterVal != '') && ($filterVal != 'D0') && ($filterVal != 'D3') && ($filterVal != 'D1')) {
			if(isset($_POST['NText'.$newFilterKey]) && (trim($_POST['NText'.$newFilterKey]) != '')) {
				$filterArray[$filterVal] = $_POST['NText'.$newFilterKey];
			}
			else if(isset($_POST['StartDate'.$newFilterKey])) {
				$filterArray[$filterVal]['StartDate'] = $_POST['StartDate'.$newFilterKey];
				$filterArray[$filterVal]['EndDate'] = $_POST['EndDate'.$newFilterKey];
			}
		}
		else if($filterVal == 'D0') {
			$filterArray['D0_SalesPerson'] = $_POST['NText'.$newFilterKey];
		}
		else if($filterVal == 'D1') {
			$filterArray['D1_StartDate'] = $_POST['StartDate'.$newFilterKey];
			$filterArray['D1_EndDate'] = $_POST['EndDate'.$newFilterKey];
		}
		else if($filterVal == 'D3') { 
			$filterArray['D3_SalesPerson'] = $_POST['NText'.$newFilterKey];
		}
		
	}
}

$objNote->pageNo = $_POST['PageNo'];
$objNote->sortType = $_POST['SortType'];
$objNote->sortAs = $_POST['SortAs'];
$objNote->noteFilterArray = $filterArray;
$objNote->paginationFor = 'search';
$objNote->notesInactivated = $_POST['InactiveNote'];

$noteList = $objNote->searchNoteList();

//if(isset($_POST['load']) && (strtolower(trim($_POST['load'])) == 'first')) {
//	$pageInfo = '';
//}
//else {
	$pageLinks = $objNote->getPages('search');
	$pageInfo = $objNote->getPageInfo();
//}
	
if(count($noteList) > 0) {
	$module = new ModuleNotes();
	$module->Init();

	if(!isset($noteList['STATUS'])) {
		$module->note_list = $noteList;
		$module->page_links = $pageLinks;
		$module->page_info = $pageInfo;
		$module->tot_rec = $objNote->totalRecords;
	}

	$smarty->assign_by_ref('module', $module);
	$output = $smarty->fetch('snippet_note_list.tpl');
}
else {
	$output = 'No notes match the current search term and filters.';
}
echo $output;