<?php

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/include/mpconstants.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/class.GoogleTasks.php';

SessionManager::Init();
SessionManager::Validate();
$company_id = $_SESSION['USER']['COMPANYID'];
$user_id = $_SESSION['USER']['USERID'];
$event_id = $_POST['eventid'];

/*start:JET-36, Time Stamps on Event Completion notes are off by six hours for US Central time users*/
$utc_timezone = new DateTimeZone('UTC');
/*end:JET-36*/

$isUseGoogleSync = GoogleConnect::isUseGoogleSync($company_id);
$sql = 'SELECT * FROM Event WHERE EventID = ?';
$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $event_id));
$recordset = DbConnManager::GetDb('mpower')->Execute($sql, 'RecEvent');
$record = $recordset[0];

if ( $isUseGoogleSync ) {
    if ( GoogleConnect::hasToken($user_id) ) {
        $sql = 'SELECT ET.Type FROM Event E
        	  JOIN EventType ET ON E.EventTypeID = ET.EventTypeID
        	 WHERE E.EventID = ?';
        $params[] = array(DTYPE_INT, $event_id);
        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $aType = DbConnManager::GetDb('mpower')->Exec($sql);
        $eventType = isset($aType[0]['Type']) ? $aType[0]['Type'] : false;

        $params = array();
        $sql2 = 'SELECT PersonID FROM EventAttendee WHERE EventID = ? and Creator = 1';
        $params[] = array(DTYPE_INT, $event_id);
        $sql = SqlBuilder()->LoadSql($sql2)->BuildSqlParam($params);
        $aRow = DbConnManager::GetDb('mpower')->Exec($sql);
        $creator = isset($aRow[0]['PersonID']) ? $aRow[0]['PersonID'] : $user_id;

        if (false != $eventType) {
        	if ('EVENT' == $eventType) {
                if ($record['Closed'] == 1 || $record['Cancelled'] == 1 ) {
                    $googleConnect = new GoogleConnect($creator, false);
                    $googleEvent = new GoogleCalendar($googleConnect->calendar, $creator, false);
        			$googleEvent->setDeletedGoogleEvent($record['googleEventId']);

        		}
        	} else {
                $googleConnect = new GoogleConnect($creator, false);
        		if ($record['Closed'] == 1) {
                    $googleTask = new GoogleTasks($googleConnect->tasklists, $googleConnect->tasks, $creator, false);
        			$googleTask->setCompletedGoogleTask($record['googleId']);
        		} else if ($record['Cancelled'] == 1) {
                    $googleTask = new GoogleTasks($googleConnect->tasklists, $googleConnect->tasks, $creator, false);
        			$googleTask->setClosedGoogleTask($record['googleId']);
        		}		
        	}
        }

    }
}
/* End Jet-37 for Record Delete,cancel and Closed */

?>
