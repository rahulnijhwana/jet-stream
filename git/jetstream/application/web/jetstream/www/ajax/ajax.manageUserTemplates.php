<?
	session_start();
	define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

	require_once BASE_PATH . '/slipstream/class.Status.php';
	require_once BASE_PATH . '/slipstream/class.NoteMap.php';
	require_once BASE_PATH . '/slipstream/class.NoteTemplate.php';
	require_once BASE_PATH . '/slipstream/class.PeopleTemplate.php';
	include_once('../administrator/include/settings.php');
	include_once('../administrator/data/clear_cache.php');
	
	if(isset($_POST['action']) && $_POST['action'] == 'getUserNoteTemplate')
	{
		$defaultTemp = array();
		$objNoteTemp = new NoteTemplate;
		$noteTemp = $objNoteTemp->getNoteTemplatesByFormTypeAndCompId($_POST['companyId'],$_POST['formType']);
		
		$objPeopleTemplate = new PeopleTemplate;
		$userTemplates = $objPeopleTemplate->getTemplatesByPersonId($_POST['companyId'],$_POST['userId']);

		if(count($userTemplates) > 0){
			for($i=0;$i<count($userTemplates);$i++){
				foreach($noteTemp as $temp){
					if($userTemplates[$i]['NoteTemplateID'] == $temp['NoteTemplateID']){
						$defaultTemp = $userTemplates[$i];
					}
				}
			}
		}
		echo json_encode($defaultTemp);
	}
	
	if(isset($_POST['action']) && $_POST['action'] == 'getFormTemplate')
	{
		$defaultTemp = array();
		$objNoteTemp = new NoteTemplate;
		$noteTemp = $objNoteTemp->getNoteTemplatesByFormTypeAndCompId($_POST['companyId'],$_POST['formType']);
		
		echo json_encode($noteTemp);
	}
	
	
	if(isset($_POST['action']) && $_POST['action'] == 'saveUserTemplate')
	{
		$status = new Status();
		$tempPeopleId = '';
		$hasTemp = false;
		$objNoteTemp = new NoteTemplate;
		$noteTemp = $objNoteTemp->getNoteTemplatesByFormTypeAndCompId($_POST['companyId'],$_POST['formType']);
		
		$objPeopleTemplate = new PeopleTemplate;
		$userTemplates = $objPeopleTemplate->getTemplatesByPersonId($_POST['companyId'],$_POST['userId']);

		if(count($userTemplates) > 0){
			for($i=0;$i<count($userTemplates);$i++){
				foreach($noteTemp as $temp){
					if($userTemplates[$i]['NoteTemplateID'] == $temp['NoteTemplateID']){
						$hasTemp = true;
						$tempPeopleId = $userTemplates[$i]['PeopleTemplateID'];
					}
				}
			}
		}
		if($hasTemp){
			$objPeopleTemplate->updatePeopleTemplate($_POST['companyId'],$tempPeopleId,$_POST['template']);
		}else{
			$objPeopleTemplate->insertPeopleTemplate($_POST['companyId'],$_POST['userId'],$_POST['template']);
		}
		
		/* Clear the jetcache directory. */
		clear_jet_cache($ajax_cache_path);
	
		echo json_encode($status->getStatus());
	}
?>