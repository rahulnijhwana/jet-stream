<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.RecAccount.php';
require_once BASE_PATH . '/include/class.RecContact.php';

SessionManager::Init();
SessionManager::Validate();

$company_id = $_SESSION['USER']['COMPANYID'];

$form_filters = array(
	'mergeType' => FILTER_SANITIZE_STRING,
	'primaryContact' => FILTER_SANITIZE_NUMBER_INT,
	'secondaryContact' => FILTER_SANITIZE_NUMBER_INT,
	'primaryAccount' => FILTER_SANITIZE_NUMBER_INT,
	'secondaryAccount' => FILTER_SANITIZE_NUMBER_INT
);
$form_values = filter_input_array(INPUT_POST, $form_filters);

if ($form_values['mergeType'] == 'Contact') {
	
	$sql = 'SELECT * from Contact WHERE ContactID = ? AND CompanyID = ?';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($form_values['primaryContact'], $company_id)));
	$contact_rec = DbConnManager::GetDb('mpower')->GetOne($sql, 'RecContact');

	$primary_contact_id = $contact_rec['ContactID'];
	
	$sql = 'SELECT * from Contact WHERE ContactID = ? AND CompanyID = ?';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($form_values['secondaryContact'], $company_id)));
	$secondary_rec = DbConnManager::GetDb('mpower')->Exec($sql);

	$secondary_contact_id = $secondary_rec[0]['ContactID'];
	
	foreach ($secondary_rec[0] as $key => $v) {
		if ($contact_rec->$key == NULL && $v != NULL) {
			$contact_rec->$key = $v;
		}
	}

	$contact_rec->Initialize();
	$contact_rec->SetDatabase(DbConnManager::GetDb('mpower'));
	$contact_rec->Save();
	
	$standard_params = array(
		array(DTYPE_INT, $primary_contact_id),
		array(DTYPE_INT, $secondary_contact_id)
	);
	
	$sql = 'UPDATE Opportunities SET ContactID = ? WHERE ContactID = ?';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($standard_params);
	DbConnManager::GetDb('mpower')->Execute($sql);
	
	$sql = 'UPDATE ot_Opportunities SET ContactID = ? WHERE ContactID = ? ';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($standard_params);
	DbConnManager::GetDb('mpower')->Execute($sql);
	
	$sql = 'UPDATE Event SET ContactID = ? WHERE ContactID = ?';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($standard_params);
	DbConnManager::GetDb('mpower')->Execute($sql);

	$sql = 'UPDATE EmailAddresses SET ContactID = ? WHERE ContactID = ?';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($standard_params);
	DbConnManager::GetDb('mpower')->Execute($sql);
	
	$sql = 'UPDATE PeopleContact SET ContactID = ? WHERE ContactID = ? ';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($standard_params);
	DbConnManager::GetDb('mpower')->Execute($sql);
	
	$note_params = array(
		array(DTYPE_INT, $primary_contact_id),
		array(DTYPE_INT, NOTETYPE_CONTACT),
		array(DTYPE_INT, $secondary_contact_id)
	);
	$sql = 'UPDATE Notes SET ObjectReferer = ? WHERE ObjectType = ? and ObjectReferer = ?';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($note_params);
	DbConnManager::GetDb('mpower')->Execute($sql);
	
	$sql = 'UPDATE Contact SET Deleted = 1 WHERE ContactID = ?';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array (DTYPE_INT, $secondary_contact_id));
	DbConnManager::GetDb('mpower')->Execute($sql);
	
	echo json_encode(array ('STATUS' => 'OK', 'PRIMARY' => $primary_contact_id, 'CONTACTID' => $secondary_contact_id, 'MERGE_TYPE' => 'CONTACT'));
}
elseif ($form_values['mergeType'] == 'Account') {

	$sql = 'SELECT * from Account WHERE AccountID = ? AND CompanyID = ?';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($form_values['primaryAccount'], $company_id)));
	$account_rec = DbConnManager::GetDb('mpower')->GetOne($sql, 'RecAccount');

	$primary_account_id = $account_rec['AccountID'];
	
	$sql = 'SELECT * from Account WHERE AccountID = ? AND CompanyID = ?';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, array($form_values['secondaryAccount'], $company_id)));
	
	$secondary_rec = DbConnManager::GetDb('mpower')->Exec($sql);
	
	$secondary_account_id = $secondary_rec[0]['AccountID'];
	
	foreach ($secondary_rec[0] as $key => $v) {
		if ($account_rec->$key == NULL && $v != NULL) {
			$account_rec->$key = $v;
		}
	}
	
	$account_rec->Initialize();
	$account_rec->SetDatabase(DbConnManager::GetDb('mpower'));
	$account_rec->Save();

	$standard_params = array(
		array(DTYPE_INT, $primary_account_id),
		array(DTYPE_INT, $secondary_account_id)
	);
	
	$sql = 'UPDATE Opportunities SET AccountID = ? WHERE AccountID = ?';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($standard_params);
	DbConnManager::GetDb('mpower')->Execute($sql);											
	
	$sql = 'UPDATE Contact SET AccountID = ? WHERE AccountID = ?';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($standard_params);
	DbConnManager::GetDb('mpower')->Execute($sql);
	
	$sql = 'UPDATE PeopleAccount SET AccountID = ? WHERE AccountID = ? ';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($standard_params);
	DbConnManager::GetDb('mpower')->Execute($sql);	
	
	$note_params = array(
		array(DTYPE_INT, $primary_account_id),
		array(DTYPE_INT, NOTETYPE_COMPANY),
		array(DTYPE_INT, $secondary_account_id)
	);

	$sql = 'UPDATE Notes SET ObjectReferer = ? WHERE ObjectType = ? and ObjectReferer = ?';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($note_params);
	DbConnManager::GetDb('mpower')->Execute($sql);
	
	$sql = 'UPDATE Account SET Deleted = 1 WHERE AccountID = ?';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $secondary_account_id));
	DbConnManager::GetDb('mpower')->Execute($sql);
	
	
	echo json_encode(array ('STATUS' => 'OK', 'PRIMARY' => $primary_account_id, 'ACCOUNTID' => $secondary_account_id, 'MERGE_TYPE' => 'COMPANY'));
}
