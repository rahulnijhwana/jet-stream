<?php
ini_set("memory_limit", "256M");

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.Dashboard.php';
require_once BASE_PATH . '/include/class.MapLookup.php';
require_once BASE_PATH . '/include/class.OptionLookup.php';
require_once BASE_PATH . '/slipstream/class.export.php';

SessionManager::Init();
SessionManager::Validate();

/***********  get the list of fields for both contact and company   *********/
if ( isset($_GET['type']) && $_GET['type'] == 'new' ) {
	$getsql = "SELECT ExportFieldID, LabelName from ExportField WHERE COMPANYID = ? AND CONTACTID = ? AND Deleted is NULL ";

	$getsql = SqlBuilder()->LoadSql($getsql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']), array(DTYPE_INT, $_SESSION['USER']['USERID']));
	$results = DbConnManager::GetDb('mpower')->Exec($getsql);

	$groupname = array() ; 
	foreach ($results as &$result){
		$groupname[$result['ExportFieldID']] = $result['LabelName'] ;
	}
	echo json_encode($groupname) ; 
	exit() ; 
}

/*
 * $_POST
 * action : export
 * account = all or id
 * contact = all or id
 * groupname = all or id
 * groupid = all or id
 */
/***********  get the list of contact and account   *********/
if ( isset($_POST['account']) || isset ($_POST['contact'])){

	if ( $_POST['groupid'] != 0 && $_POST['groupid'] != 'all'){ // get List from Database 
		$sql = "SELECT Account, Contact FROM ExportField WHERE ExportFieldID = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_POST['groupid']));
		$group = DbConnManager::GetDb('mpower')->Exec($sql);
		$accounts = ( $_POST['account'] ==  'undefined' ) ?  explode(';', $group[0]['Account']) : json_decode( str_replace("\\", '', $_POST['account']) );
		$contacts = ( $_POST['contact'] ==  'undefined' ) ?  explode(';', $group[0]['Contact']) : json_decode( str_replace("\\", '', $_POST['contact']) );

		if ( !is_array($accounts)) $accounts = array ($accounts) ;
		if ( !is_array($contacts)) $contacts = array ($contacts) ;
		
		// reminder to replace the  "\" from JSON javascript output 
		
	}
	else {
		if (!empty($_POST['account'])) {
			$accounts =  json_decode( str_replace("\\", '', $_POST['account'])) ; 
		}
		else $accounts = array () ; 
		if (!empty($_POST['contact'])) {
			$contacts = json_decode( str_replace("\\", '', $_POST['contact']) );
		}
		else {
			$contacts = array () ; 
		}
		
	}
}
/* for debug
if ( $_GET['groupid'] != 0 ){ // get List from Database 
	$sql = "SELECT Account, Contact FROM ExportField WHERE ExportFieldID = ?";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_GET['groupid']));
	$group = DbConnManager::GetDb('mpower')->Exec($sql);	
}
$accounts = explode(';', $group[0]['Account']);
$contacts = explode(';', $group[0]['Contact']);
*/


/***********  Export Data  *********/
// for debug
//if ( isset($_GET['type']) && $_GET['type'] == 'test' ) {

if ( isset($_POST['action']) || ($_POST['action'] == 'export')){

	$dashboard = new Dashboard($_SESSION['USER']['COMPANYID'], $_SESSION['USER']['USERID']);
	$search = $_SESSION['LastSearch'] ;
	
	if ( !(isset ($search['query_type'] )) )  $search[query_type]= 3 ; 

    /* export limit is hard coded as 5000 */
    /* besso adhoc */
	$search['records_per_page'] = 5000; // controls the number of records printed 
	$dashboard->Build($search);
	
	if ( (count($dashboard->search_results) < 1 ) || $dashboard->search_results[0] == NULL ) {
		echo "ERROR : NULL Search Result."; 
		exit(); 
	}
	/*Start JET-44*/
	if ( !is_array($accounts)) $accounts = array ($accounts) ;
	if ( !is_array($contacts)) $contacts = array ($contacts) ;
	/*Start JET-44*/

	if (!empty($accounts) && !empty($contacts)) $columnList = array('account' => $accounts, 'contact' => $contacts);
	else if (empty($accounts) && !empty($contacts)) $columnList = array('contact' => $contacts);
	else $columnList =  array('account' => $accounts );

//	$columnList = array('account' => $accounts, 'contact' => $contacts);
//	$resultHTML = BuildSearchArray($dashboard->search_results, $columnList) ; 
//echo "<pre>";var_dump($columnList);exit;
	BuildSearchArray($dashboard->search_results, $columnList) ; 

//	Export::simpleHTMLTable($resultHTML, 'excel') ; 
	exit;
}


function GetLabelName($map) {
	if ( !(is_array($map)) ) {
		return "Error" ; 
	} 
	$list = array() ; 

	foreach ( $map['fields'] as &$field ){
		if ( preg_match ("/^Select/", $field['FieldName'])){
			$list[$field['FieldName']] = $field['LabelName'] . "__" . $field['OptionSetID']; 
		}
		else $list[$field['FieldName']] = $field['LabelName']; 
	}
	return $list ; 
}

function GetMap($map, $columnlist) {
	if ( !(is_array($map)) ) {
		return "Error 1" ; 
	} 
	if ( !(is_array($columnlist)) ){
		return "Error 2" ;
	}
	$list = array() ; 
	foreach ( $map['fields'] as &$field ){
		if (in_array($field['LabelName'], $columnlist)) {
			if ( ($field['FieldName'] != 'Text01') && ($field['FieldName'] != 'Text02') && ($field['FieldName'] != 'Text03') ){
				$list[$field['LabelName']] = $field['FieldName']; 
				echo $field['LabelName'] ." = " . $field['FieldName'] . "<br>" ; 
			}
		}
	}
	return $list ; 
}

function GetInfofromSearchResult($search, $id){
	$result = array () ; 
	foreach ($search as &$arr){
		$result[] = $arr[$id]; 
	}
	return $result ; 
}

function GetValues($list, $searchResult) {

	$account = GetInfofromSearchResult($searchResult, 'AccountID');
	$contact = GetInfofromSearchResult($searchResult, 'ContactID');
	
	//select COntactID, (select optionName from [dbo].[Option] where OptionId = Select02) as Select02, (select optionName from [dbo].[Option] where OptionId = Select03) as Select03 from Contact where Text01 = 'Allan'
	// search for Select the append '(select optionName from [dbo].[Option] where OptionId = {ColumnName}) as' ColumnName
	
	if ( !empty($list['contact']) && (trim($list['contact'][0]) != '' ) ){
		
		$temparr = array () ; 
		foreach( $list['contact'] as $key => $value ){
			if ( preg_match ("/^Select/", $value)){
				$list['contact'][$key] = "(select optionName from [dbo].[Option] where OptionId = $value) as $value " ; 
			} 
		}
		$list['contact'][] =  'ContactID' ;
		$fieldlist = (implode(", ", $list['contact'])) ; 
		$sql = "SELECT $fieldlist FROM Contact WHERE ContactID in (". ArrayQm($contact) .") ";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $contact));
//		$results = DbConnManager::GetDb('mpower')->Exec($sql);
//		foreach ($results as &$result){
//			$temparr[$result['ContactID']] = $result ; 
//		} 
//		$output['contact'] = $temparr;
//		echo "after contact" .  memory_get_usage() . "<br>";

		$result_list = DbConnManager::GetDb('mpower')->GetSqlResult($sql) ;
		while (	$result = mssql_fetch_assoc($result_list)) {
			$temparr[$result['ContactID']] = $result ; 
		}
		$output['contact'] = $temparr;
		unset($temparr);
		unset($result_list);
		unset($result);
		
	}

	if ( !empty($list['account']) && (trim($list['account'][0]) != '' )){

		$temparr = array () ; 
		foreach( $list['account'] as $key => $value ){
			if ( preg_match ("/^Select/", $value)){
				$list['account'][$key] = "(select optionName from [dbo].[Option] where OptionId = $value) as $value " ; 
			} 
		}
		array_push($list['account'], 'AccountID'); 
		$fieldlist = (implode(", ", $list['account'])) ; 
		$sql = "SELECT $fieldlist FROM Account WHERE AccountID in (". ArrayQm($account) .") ";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $account));
//		$results = DbConnManager::GetDb('mpower')->Exec($sql);
//	
//		foreach ($results as &$result){
//			$temparr[$result['AccountID']] = $result ; 
//		} 
		$result_list = DbConnManager::GetDb('mpower')->GetSqlResult($sql) ;
		while (	$result = mssql_fetch_assoc($result_list)) {
			$temparr[$result['AccountID']] = $result ; 
		}
		
		$output['account'] = $temparr;
		unset($temparr);
		unset($result_list);
		unset($result);
	}
	
	return $output ; 
}

function GetHeader($column, $map){
	
	if(!is_array($column)){
		$tmp = array();
		array_push($tmp, $column);
		$column = $tmp;
	}
	
	$header = array();
	
	foreach($column as &$fieldname){
		if(preg_match( "/.__./", $map[$fieldname])) {
			$splitOptionSet = explode( "__" , $map[$fieldname]); 
			$header[] = $splitOptionSet[0];
		}
		else {
			$header[] = $map[$fieldname];
		}
	}

	return $header; 
}

/* 
 * Generate Lists for contact or company and export excel
 * @params $columnlist columnlist to show ex) 'all' or else
 */
function BuildSearchArray($search_list, $columnlist) {

    $count = count($search_list);

    /* TODO mapping column spending cost, besso */	
	$map['contact'] = GetLabelName(MapLookup::GetContactMap($_SESSION['USER']['COMPANYID'])) ;
	$map['account'] = GetLabelName(MapLookup::GetAccountMap($_SESSION['USER']['COMPANYID'])) ;
	
	if ( $_POST['groupid'] == 'all' && $_POST['groupname'] == 'all') {
		$columnlist['account'] = array_keys($map['account']) ;
		$columnlist['contact'] = array_keys($map['contact']) ;
	//	echo "<pre>";var_dump($columnlist['account'],$columnlist['contact']);exit;
	}
	$values = GetValues($columnlist, $search_list) ;
	//echo "<pre>";var_dump($values);exit;	
	$header = array () ;
	
	if ( isset($columnlist['contact'])  & isset($columnlist['account'])){
		$header = array_merge(GetHeader($columnlist['contact'], $map['contact']), GetHeader($columnlist['account'], $map['account']));
	}
	else if ( isset($columnlist['contact'])){
		$header = GetHeader($columnlist['contact'], $map['contact']);
	}
	else { 
		$header = GetHeader($columnlist['account'], $map['account']);
	}
    
	foreach( $search_list as &$search){
		$line = array() ;
		if ( isset($columnlist['contact'])){
			foreach($columnlist['contact'] as &$column){
				$line[] = ( isset($search['ContactID'])) ? $values['contact'][$search['ContactID']][$column] :'';	
			}
		}
		if ( isset($columnlist['account'])){
			foreach($columnlist['account'] as &$column){
				$line[] = ( isset($search['AccountID'])) ? $values['account'][$search['AccountID']][$column] : '';
			}					
		}
		if (implode('',$line) != '' ) $outputArray[] = $line ; 
	}
	//echo "<pre>";var_dump($header);exit;
	$export = new export() ; 

	$export->headers = $header ; 

	$export->datas = $outputArray ; 
	
	$export->xlsx() ; 
	
}

function AllFieldname (){
	$ContactLayouts =  MapLookup::GetContactLayout($_SESSION['USER']['COMPANYID'] );
	$AccountLayouts =  MapLookup::GetAccountLayout($_SESSION['USER']['COMPANYID'] );
	$ContactMaps =  MapLookup::GetContactMap($_SESSION['USER']['COMPANYID'] );
	$AccountMaps =  MapLookup::GetAccountMap($_SESSION['USER']['COMPANYID'] );

	foreach ($AccountLayouts as &$AccountLayout ) {
		foreach ( $AccountLayout['rows'] as &$row ) {
			foreach ( array(1,0) as $position){
				if (!empty($row[$position] )){ 
					if ( $AccountMaps['fields'][$row[$position]]['FieldName'] ) {
						$account[] = $AccountMaps['fields'][$row[$position]]['FieldName'] ; 
					}
				}
			}
		}	
	}

	foreach ($ContactLayouts as &$ContactLayout ) {
		foreach ( $ContactLayout['rows'] as &$row ) {
			foreach ( array(1,0) as $position){
				if (!empty ($row[$position])){
					$contact[] = $ContactMaps['fields'][$row[$position]]['FieldName'];
				}
			}
		}	
	}

	
	unset($ContactLayouts);
	unset($AccountLayouts);
	unset($ContactMaps);
	unset($AccountMaps);
	
	return array ( 'accounts' => $account , 'contacts' => $contact ); 
	
}


?>
