<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/class.Event.php';
require_once BASE_PATH . '/slipstream/class.ModuleEvents.php';

require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

SessionManager::Init();
SessionManager::Validate();

$objEvent = new Event();

$task_date = $_GET['date'];

$events = $objEvent->GetEventsByDateRange($task_date, $task_date, $_SESSION['cal_targets'], 'TASK');

$output = '';
$module = new ModuleEvents();
$module->Init();		
$module->events = $events;
$smarty->assign_by_ref('module', $module);
$smarty->display('snippet_event_list.tpl');				

