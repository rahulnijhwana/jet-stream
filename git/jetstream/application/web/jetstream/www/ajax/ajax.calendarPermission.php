<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
require_once BASE_PATH . '/slipstream/class.CalendarPermission.php';
require_once BASE_PATH . '/slipstream/class.Status.php';
if (!isset($_SESSION)) session_start();
//print_r($_POST['action']);

$objCalendar = new CalendarPermission();

if(isset($_POST['action']) && $_POST['action'] == 'SaveCalendarPermission'){
	$status=new Status();
	$objCalendar->SaveUserCalendarPermission($_POST['WritePermission'],$_POST['UnWritePermission'],$_POST['UserId'],$_POST['LockedUser'],$_POST['UnLockedUser'],$_POST['ReadPermission'],$_POST['UnReadPermission']);
	echo json_encode($status->getStatus());
}

if(isset($_POST['action']) && $_POST['action'] == 'getUsersList'){
	$userDetails = $objCalendar->GetUserCalendarPermission($_POST['CompanyId'],$_POST['PersonId']);
	//print_r($userDetails);exit;
	echo json_encode($userDetails);
}


?>
