<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/slipstream/class.User.php';

session_start();
header('Content-type: application/xml; charset="utf-8"', true);
echo '<?xml version ="1.0" encoding="utf-8"?>';

$objUser = new User();

$rec_count = $objUser->getAllUserByCompanyIdRecordCount($_SESSION['USER']['COMPANYID']);

if (!isset($_GET['page']))
	$page = 1;
else
	$page = $_GET['page'];

$records = $_GET['rows'];
$result = $objUser->getAllUserByCompanyId($_SESSION['USER']['COMPANYID'], $page, $records);
$total = ceil($rec_count[0]['TotalRecord'] / $records);
$user_level_name = array();
$user_level_name = $_SESSION['USER']['USERLEVELNAME'];

if ($rec_count[0]['TotalRecord'] <= $records) $page = 1;
echo '<rows>' . '<page>' . $page . '</page>' . '<total>' . $total . '</total>' . '<records>' . $records . '</records>';
foreach ($result as $objResult) {
	echo "<row id='" . $objResult['UserID'] . "'>" . "<cell><![CDATA[<img src='images/edit.jpg' onclick='javascript:editUser(" . $objResult['UserID'] . ");' />]]></cell>" . "<cell><![CDATA[<img src='images/del.png'  onclick='javascript:deleteUser(" . $objResult['UserID'] . ");' />]]></cell>" . "<cell><![CDATA[" . $objResult['UserName'] . "]]></cell>" . "<cell><![CDATA[" . $objResult['FirstName'] . "]]></cell>" . "<cell><![CDATA[" . $objResult['LastName'] . "]]></cell>" . "<cell><![CDATA[" . $objResult['Email'] . "]]></cell>";
	if ($objResult['SupervisorID'] == -3)
		echo "<cell><![CDATA[Admin]]></cell>";
	else
		echo "<cell><![CDATA[" . $user_level_name[$objResult['Level']] . "]]></cell>";
	
	echo '</row>';
}
echo '</rows>';

?>