<?php
require_once '../../include/constants.php';
require_once JETSTREAM_ROOT . '/../include/class.SessionManager.php';
require_once JETSTREAM_ROOT . '/../include/class.DbConnManager.php';
require_once JETSTREAM_ROOT . '/../include/class.SqlBuilder.php';

SessionManager::Init();
SessionManager::Validate();
unset($_SESSION['USER']['google']['token']);

if (!empty($_SESSION['USER']['USERID'])) {
	$personID = $_SESSION['USER']['USERID'];
} else {
	$msg = 'No USERID in SESSION';
	echo json_encode(array('code'=>500,'msg'=>$msg)); exit;
}

$sql = 'Delete From GoogleToken Where personID = ?';
$params[] = array(DTYPE_INT, $personID);
$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
$status = DbConnManager::GetDb('mpower')->Exec($sql);
if (!$status) {
	$msg = 'No USERID in SESSION';
	echo json_encode(array('code'=>500,'msg'=>$msg)); exit;
}

$sql = 'Delete From GoogleContact Where personID = ?';
$params = array();
$params[] = array(DTYPE_INT, $personID);
$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
$status = DbConnManager::GetDb('mpower')->Exec($sql);
if (!$status) {
	$msg = 'Fail to truncate GoogleContact table';
	echo json_encode(array('code'=>500,'msg'=>$msg)); exit;
}


echo json_encode(array('code'=>200,'msg'=>'success'));
