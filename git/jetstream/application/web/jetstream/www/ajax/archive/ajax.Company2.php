<?php
/**
 * define BASE PATH
 * include DB API class
 * include fieldMap class file
 * include company class file
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/class.CompanyContact.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';

SessionManager::Init();
SessionManager::Validate();

$company_contact = new CompanyContact();

$result['status'] = $company_contact->ProcessForm();

if ($result['status']['STATUS'] == 'OK') {
	$result['title'] = $company_contact->GetName();
	$result['view'] = $company_contact->DrawView();
}
//$smarty->assign('EDITDATA', $company->DrawView(true));
//$result['edit_form'] = $smarty->fetch('company_edit.tpl');

echo json_encode($result);







//
///**
// * create Company() class object
// * create fieldMap() class object
// * create Status() class object
// */
//$objCompany = new Company();
//$objFieldMap = new fieldsMap();
//$status = new Status();
//
///**
// * get layout structure for account from fieldMap() class
// */
//$sessLables = $objFieldMap->getAccountFields();
//$AccountLables = $objFieldMap->getAccountLayout();
//$leftLables = $AccountLables['leftPanel'];
//$rightLables = $AccountLables['rightPanel'];
//
///**
// * Set the option values for the Select fields.
// */
//$options = array();
//
//foreach ($sessLables as $key => $value) {
//	if (substr($sessLables[$key]['FieldName'], 0, 6) == 'Select') {
//		
//		$accMapID = $sessLables[$key]['AccountMapID'];
//		$options[$accMapID] = getOptionByAccountMapId($accMapID, $_SESSION['USER']['COMPANYID']);
//	}
//}
//
///**
// * validation functionality for account add and update form
// * account add and update functionality
// */
//if (isset($_POST)) {
//	$newData = $_POST;
//	
//	/**
//	 * validation section
//	 */
//	if (isset($_POST['updAccountID']) && $_POST['updAccountID'] != '') {
//		$result = doValidateUpdateRecord($newData, $sessLables);
//	} else {
//		$result = doValidateNewRecord($newData, $sessLables);
//	}
//	
//	$resStatus = $result->getStatus();
//	
//	if ($resStatus['STATUS'] == 'FALSE') {
//		echo json_encode(array('result' => $result->getStatus()));
//	}
//	
//	/**
//	 * insert or update account section
//	 */
//	if ($resStatus['STATUS'] == 'OK') {
//		if (isset($_POST['updAccountID']) && $_POST['updAccountID'] != '') {
//			$result = $objCompany->createCompany($newData, $sessLables, 'update');
//			
//			/**
//			 * set view HTML data
//			 */
//			//**********************
//			$viewData = "<table class='jet_datagrid'>";
//			//	$viewData .= "<colgroup span='4'><col class='label'><col class='left'><col class='label'><col class='right'></colgroup>";
//			$left = current($leftLables);
//			$right = current($rightLables);
//			while ($right || $left) {
//				$lblLeft = str_replace(" ", "_", $left['LabelName']);
//				$viewData .= "<tr><td class='label'>" . ((isset($left['LabelName'])) ? $left['LabelName'] . ':' : '&nbsp;') . "</td>";
//				$viewData .= "<td class='left'><span class='view' id='spn".$lblLeft."'>";
//				if (substr($left['FieldName'], 0, 6) == 'Select') {
//					$optID = $_POST['LB' . $lblLeft];
//					$optName = getOptionNameById($optID, $_SESSION['USER']['COMPANYID']);
//					$viewData .= $optName['OptionName'];
//				} else {
//					$viewData .= $_POST['LB' . $lblLeft];
//				}
//				$viewData .= "</span></td>";
//				
//				$lblRight = str_replace(" ", "_", $right['LabelName']);
//				$viewData .= "<td class='label'>" . ((isset($right['LabelName'])) ? $right['LabelName'] . ':' : '&nbsp;') . "</td>";
//				$viewData .= "<td class='right'><span class='view' id='spn".$lblRight."'>";
//				if (substr($right['FieldName'], 0, 6) == 'Select') {
//					$optID = $_POST['LB' . $lblRight];
//					$optName = getOptionNameById($optID, $_SESSION['USER']['COMPANYID']);
//					$viewData .= $optName['OptionName'];
//				} else {
//					$viewData .= $_POST['LB' . $lblRight];
//				}
//				$viewData .= "</span></td>";
//				$viewData .= "</tr>";
//				$right = next($rightLables);
//				$left = next($leftLables);
//			} 
//				
//			$viewData .= "</table>";
//			
//			/**
//			 * set lightbox view HTML data
//			 */
//			$viewLBData = "<table class='jet_datagrid'>";
//			$right = current($rightLables);
//			$left = current($leftLables);
//			while ($right || $left) {
//				$chkLeftField = ($left['IsRequired'] == 1) ? '<span class="smlRedBold">*</span>':'';
//				$lblLeft = str_replace(" ", "_", $left['LabelName']);
//
//				$viewLBData .= "<tr><td class='label' valign='top'>".$chkLeftField . ((isset($left['LabelName'])) ? $left['LabelName'] . ':' : '&nbsp;') . "</td>";
//				$viewLBData .= "<td class='left'>";
//
//				if (substr($left['FieldName'], 0, 4) == 'Date') {
//					$viewLBData .= "<input type='text' class='datepicker' size='30' name='LB" . $lLblName . "' id='LB" . $lLblName . "' value='" . $_POST['LB' . $lblLeft] . "'/>";
//				} elseif (substr($left['FieldName'], 0, 6) == 'Select') {
//					$accMapID = $left['AccountMapID'];
//					$viewLBData .= "<select name='LB" . $lLblName . "' id='LB" . $lLblName . "' class='clsTextBox' style='width:210px;'>";
//					$option = current($selectOptions[$accMapID]);
//					while ($option) {
//						$selected = ($option['OptionID'] == $_POST['LB' . $lblRight]) ? 'selected' : '';
//						$viewLBData .= "<option value='" . $option['OptionID'] . "' " . $selected . ">" . $option['OptionName'] . "</option>";
//						$option = next($selectOptions[$accMapID]);
//					}
//					$viewLBData .= "</select>";
//				} else {
//					$viewLBData .= "<input type='text' class='clsTextBox' size='30' name='LB" . $lLblName . "' id='LB" . $lLblName . "' value='" . $_POST['LB' . $lblLeft] . "'/>";
//				}
//				$viewLBData .= "<br><span class='clsError' id='spn_LB" . $lLblName . "'></span></div><br>";
//
//
//				$viewLBData .= "</td>";
//
//				$chkRightField = ($right['IsRequired'] == 1) ? '<span class="smlRedBold">*</span>':'';
//				$lblRight = str_replace(" ", "_", $right['LabelName']);
//
//				$viewLBData .= "<td class='label' valign='top'>".$chkRightField . ((isset($right['LabelName'])) ? $right['LabelName'] . ':' : '&nbsp;') . "</td>";
//				$viewLBData .= "<td class='left'>";
//
//				if (substr($right['FieldName'], 0, 4) == 'Date') {
//					$viewLBData .= "<input type='text' class='datepicker' size='30' name='LB" . $rLblName . "' id='LB" . $rLblName . "' value='" . $_POST['LB' . $lblRight] . "'/>";
//				} elseif (substr($right['FieldName'], 0, 6) == 'Select') {
//					$accMapID = $right['AccountMapID'];
//					$viewLBData .= "<select name='LB" . $rLblName . "' id='LB" . $rLblName . "' class='clsTextBox' style='width:210px;'>";
//					$option = current($selectOptions[$accMapID]);
//					while ($option) {
//						$selected = ($option['OptionID'] == $_POST['LB' . $lblRight]) ? 'selected' : '';
//						$viewLBData .= "<option value='" . $option['OptionID'] . "' " . $selected . ">" . $option['OptionName'] . "</option>";
//						$option = next($selectOptions[$accMapID]);
//					}
//					$viewLBData .= "</select>";
//				} else {
//					$viewLBData .= "<input type='text' class='clsTextBox' size='30' name='LB" . $rLblName . "' id='LB" . $rLblName . "' value='" . $_POST['LB' . $lblRight] . "'/>";
//				}
//				$viewLBData .= "<br><span class='clsError' id='spn_LB" . $rLblName . "'></span></div><br>";
//				$viewLBData .= "</td>";
//				$viewLBData .= "</tr>";
//				$right = next($rightLables);
//				$left = next($leftLables);
//			} 
//			
//			$viewLBData .= "</table>";
//		} else {
//			
//			$result = $objCompany->createCompany($newData, $sessLables, 'insert');
//		}
//		echo json_encode(array('result' => $result));
//	}
//}
?>