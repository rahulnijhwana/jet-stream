<?php

/**
 * define BASE PATH
 * include DB API class
 * include fieldMap class file
 * include company class file
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/class.Contact.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.conf';

SessionManager::Init();
SessionManager::Validate();

$contact = new Contact();

$contact->ProcessForm();

$result['title'] = $contact->GetName();
$result['view'] = $contact->DrawView();

echo json_encode($result);

///**
// * session start
// * define BASE PATH
// * class file include
// * DB API functions file include
// */
//
//session_start();
//
//define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
//include_once BASE_PATH . '/include/class.fieldsMap.php';
//require_once BASE_PATH . '/slipstream/class.Contact.php';
//require_once BASE_PATH . '/slipstream/lib.php';
//require_once BASE_PATH . '/slipstream/class.Status.php';
//
///**
// * create Contact() class object
// * create fieldMap() class object
// * create Status() class object
// */
//$objContact = new Contact();
//$objFieldMap = new fieldsMap();
//$status = new Status();
//
///**
// * get layout structure for contact from fieldMap() class
// */
//$sessLables = $objFieldMap->getContactFields();
//$ContactLables = $objFieldMap->getContactLayout();
//$leftLables = $ContactLables['leftPanel'];
//$rightLables = $ContactLables['rightPanel'];
//
///**
// * Set the option values for the Select fields.
// */
//$options = array();
//
//foreach ($sessLables as $key => $value) {
//	if (substr($sessLables[$key]['FieldName'], 0, 6) == 'Select') {
//		
//		$accMapID = $sessLables[$key]['AccountMapID'];
//		$options[$accMapID] = getOptionByAccountMapId($accMapID, $_SESSION['USER']['COMPANYID']);
//	}
//}
//
///**
// * validation functionality for contact add and update form
// * contact add and update functionality
// */
//if (isset($_POST['btnSaveContact'])) {
//	
//	$newData = $_POST;
//	
//	/**
//	 * validation section
//	 */
//	if (isset($_POST['editContactId']) && $_POST['editContactId'] != '') {
//		$result = doValidateUpdateRecord($newData, $sessLables);
//	} else {
//		$result = doValidateNewRecord($newData, $sessLables);
//	}
//
//	$resStatus = $result->getStatus();
//
//	if ($resStatus['STATUS'] == 'FALSE') {
//		echo json_encode(array('result' => $result->getStatus()));
//	}
//	if ($resStatus['STATUS'] == 'OK') {
//		if (isset($_POST['editContactId']) && $_POST['editContactId'] != '') {
//			$result =$objContact->InsertContactInfo($newData, $sessLables, 'update');
//			
//			/**
//			 * set view HTML data
//			 */	
//			$viewData = "<table class='jet_datagrid'>";
//			//	$viewData .= "<colgroup span='4'><col class='label'><col class='left'><col class='label'><col class='right'></colgroup>";
//			$left = current($leftLables);
//			$right = current($rightLables);
//			while ($right || $left) {
//				$lblLeft = str_replace(" ", "_", $left['LabelName']);
//				$viewData .= "<tr><td class='label'>" . ((isset($left['LabelName'])) ? $left['LabelName'] . ':' : '&nbsp;') . "</td>";
//				$viewData .= "<td class='left'><span class='view' id='spn".$lblLeft."'>";
//				if (substr($left['FieldName'], 0, 6) == 'Select') {
//					$optID = $_POST['LB' . $lblLeft];
//					$optName = getOptionNameById($optID, $_SESSION['USER']['COMPANYID']);
//					$viewData .= $optName['OptionName'];
//				} else {
//					if(isset($_POST['LB' . $lblLeft])) 
//						$viewData .= $_POST['LB' . $lblLeft];
//				}
//				$viewData .= "</span></td>";
//
//				$lblRight = str_replace(" ", "_", $right['LabelName']);
//				$viewData .= "<td class='label'>" . ((isset($right['LabelName'])) ? $right['LabelName'] . ':' : '&nbsp;') . "</td>";
//				$viewData .= "<td class='right'><span class='view' id='spn".$lblRight."'>";
//				if (substr($right['FieldName'], 0, 6) == 'Select') {
//					$optID = $_POST['LB' . $lblRight];
//					$optName = getOptionNameById($optID, $_SESSION['USER']['COMPANYID']);
//					$viewData .= $optName['OptionName'];
//				} else {
//					if(isset($_POST['LB' . $lblRight])) 
//						$viewData .= $_POST['LB' . $lblRight];
//				}
//				$viewData .= "</span></td>";
//				$viewData .= "</tr>";
//				$right = next($rightLables);
//				$left = next($leftLables);
//			} 
//							
//			$viewData .= "</table>";
//			
//			/**
//			 * set lightbox view HTML data
//			 */
//			$viewLBData = "<div id='leftPanel' style='float:left; width:350px;'>";
//			foreach ($leftLables as $key => $value) {
//				$lblLeft = str_replace(" ", "_", $leftLables[$key]['LabelName']);
//				$viewLBData .= "<div style='height:10px;'><div class='BlackBold' style='float:left; width:100px;'>";
//				if ($leftLables[$key]['IsRequired'] == 1) {
//					$viewLBData .= "<span class='smlRedBold'>*</span>";
//				}
//				$viewLBData .= $leftLables[$key]['LabelName'];
//				$viewLBData .= "&nbsp:&nbsp;</div>";
//				if (substr($leftLables[$key]['FieldName'], 0, 4) == 'Date') {
//					$viewLBData .= "<span><input type='text' class='datepicker' size='30' name='LB" . $lblLeft . "' id='LB" . $lblLeft . "' value='" . $_POST['LB' . $lblLeft] . "'/></span>";
//				} elseif (substr($leftLables[$key]['FieldName'], 0, 6) == 'Select') {
//					$accMapID = $leftLables[$key]['AccountMapID'];
//
//					$viewLBData .= "<span><select name='LB" . $lblLeft . "' id='LB" . $lblLeft . "' class='clsTextBox' style='width:210px;'>";
//					foreach ($options[$accMapID] as $key => $value) {
//						$selected =  ($options[$accMapID][$key]['OptionID'] == $_POST['LB' . $lblRight])?'selected':'';
//						$viewLBData .= "<option value='" . $options[$accMapID][$key]['OptionID'] . "' ".$selected.">" . $options[$accMapID][$key]['OptionName'] . "</option>";
//					}
//					$viewLBData .= "</select></span>";
//
//				} else {
//					$viewLBData .= "<span><input type='text' class='clsTextBox' size='30' name='LB" . $lblLeft . "' id='LB" . $lblLeft . "' value='" . $_POST['LB' . $lblLeft] . "'/></span>";
//				}
//				$viewLBData .= "<br><span style='padding-left:100px;' class='clsError' id='spn_LB" . $lblLeft . "'></span></div><br>";
//			}
//			$viewLBData .= "</div><div id='rightPanel'>";
//			foreach ($rightLables as $key => $value) {
//				$lblRight = str_replace(" ", "_", $rightLables[$key]['LabelName']);
//				$viewLBData .= "<div style='height:10px;'><div class='BlackBold' style='float:left; width:100px;'>";
//				if ($rightLables[$key]['IsRequired'] == 1) {
//					$viewLBData .= "<span class='smlRedBold'>*</span>";
//				}
//				$viewLBData .= $rightLables[$key]['LabelName'];
//				$viewLBData .= "&nbsp:&nbsp;</div>";
//				if (substr($rightLables[$key]['FieldName'], 0, 4) == 'Date') {
//					$viewLBData .= "<span><input type='text' class='datepicker' size='30' name='LB" . $lblRight . "' id='LB" . $lblRight . "' value='" . $_POST['LB' . $lblRight] . "'/></span>";
//				} elseif (substr($rightLables[$key]['FieldName'], 0, 6) == 'Select') {
//					$accMapID = $rightLables[$key]['AccountMapID'];
//
//					$viewLBData .= "<span><select name='LB" . $lblRight . "' id='LB" . $lblRight . "' class='clsTextBox' style='width:210px;'>";
//					foreach ($options[$accMapID] as $key => $value) {
//						$selected =  ($options[$accMapID][$key]['OptionID'] == $_POST['LB' . $lblRight])?'selected':'';
//						$viewLBData .= "<option value='" .$options[$accMapID][$key]['OptionID']. "' ".$selected.">" .$options[$accMapID][$key]['OptionName']. "</option>";
//					}
//					$viewLBData .= "</select></span>";
//
//				} else {
//					$viewLBData .= "<span><input type='text' class='clsTextBox' size='30' name='LB" . $lblRight . "' id='LB" . $lblRight . "' value='" . $_POST['LB' . $lblRight] . "'/></span>";
//				}
//				$viewLBData .= "<br><span class='clsError' id='spn_LB" . $lblRight . "'></span></div><br>";
//			}
//			$viewLBData .= "</div>";
//		} else {
//			$result =$objContact->InsertContactInfo($newData, $sessLables, 'insert');
//			
//			/**
//			 * set view HTML data
//			 */	
//			$viewData = "<div id='leftPanel' style='float:left; width:230px;'>";
//			foreach ($leftLables as $key => $value) {
//				$lblLeft = str_replace(" ", "_", $leftLables[$key]['LabelName']);
//
//				$viewData .= "<div style='height:10px;'><span class='BlackBold'>". $leftLables[$key]['LabelName'] ."&nbsp:&nbsp;</span><span class='view' id='spn".$lblLeft."'><span class='smlBlack'>";
//
//
//
//				if (isset($leftLables[$key]['OptionValue']) != '') {
//					$viewData .= $_POST[$lblLeft];
//				} else {
//					$viewData .= $_POST[$lblLeft];
//				}
//				$viewData .= "</span></span></div><br>";
//			}
//			$viewData .= "</div><div id='rightPanel'>";
//			foreach ($rightLables as $key => $value) {
//				$lblRight = str_replace(" ", "_", $rightLables[$key]['LabelName']);
//
//				$viewData .= "<div style='height:10px;'><span class='BlackBold'>". $rightLables[$key]['LabelName'] ."&nbsp:&nbsp;</span><span class='view' id='spn".$lblRight."'><span class='smlBlack'>";
//
//
//
//				if (substr($rightLables[$key]['FieldName'], 0, 6) == 'Select') {
//					$optID = $_POST[$lblRight];
//					$optName = getOptionNameById($optID, $_SESSION['USER']['COMPANYID']);
//					$viewData .= $optName['OptionName'];
//				} else {
//					$viewData .= $_POST[$lblRight];
//				}
//				$viewData .= "</span></span></div><br>";
//			}
//			$viewData .= "</div>";
//
//			/**
//			 * set lightbox view HTML data
//			 */
//			$viewLBData = "<div id='leftPanel' style='float:left; width:350px;'>";
//			foreach ($leftLables as $key => $value) {
//				$lblLeft = str_replace(" ", "_", $leftLables[$key]['LabelName']);
//				$viewLBData .= "<div style='height:10px;'><div class='BlackBold' style='float:left; width:100px;'>";
//				if ($leftLables[$key]['IsRequired'] == 1) {
//					$viewLBData .= "<span class='smlRedBold'>*</span>";
//				}
//				$viewLBData .= $leftLables[$key]['LabelName'];
//				$viewLBData .= "&nbsp:&nbsp;</div>";
//				if (substr($leftLables[$key]['FieldName'], 0, 4) == 'Date') {
//					$viewLBData .= "<span><input type='text' class='datepicker' size='30' name='LB" . $lblLeft . "' id='LB" . $lblLeft . "' value='" . $_POST[$lblLeft] . "'/></span>";
//				} elseif (substr($leftLables[$key]['FieldName'], 0, 6) == 'Select') {
//					$accMapID = $leftLables[$key]['AccountMapID'];
//
//					$viewLBData .= "<span><select name='LB" . $lblLeft . "' id='LB" . $lblLeft . "' class='clsTextBox' style='width:210px;'>";
//					foreach ($options[$accMapID] as $key => $value) {
//						$selected =  ($options[$accMapID][$key]['OptionID'] == $_POST[$lblRight])?'selected':'';
//						$viewLBData .= "<option value='" . $options[$accMapID][$key]['OptionID'] . "' ".$selected.">" . $options[$accMapID][$key]['OptionName'] . "</option>";
//					}
//					$viewLBData .= "</select></span>";
//
//				} else {
//					$viewLBData .= "<span><input type='text' class='clsTextBox' size='30' name='LB" . $lblLeft . "' id='LB" . $lblLeft . "' value='" . $_POST[$lblLeft] . "'/></span>";
//				}
//				$viewLBData .= "<br><span style='padding-left:100px;' class='clsError' id='spn_LB" . $lblLeft . "'></span></div><br>";
//			}
//			$viewLBData .= "</div><div id='rightPanel'>";
//			foreach ($rightLables as $key => $value) {
//				$lblRight = str_replace(" ", "_", $rightLables[$key]['LabelName']);
//				$viewLBData .= "<div style='height:10px;'><div class='BlackBold' style='float:left; width:100px;'>";
//				if ($rightLables[$key]['IsRequired'] == 1) {
//					$viewLBData .= "<span class='smlRedBold'>*</span>";
//				}
//				$viewLBData .= $rightLables[$key]['LabelName'];
//				$viewLBData .= "&nbsp:&nbsp;</div>";
//				if (substr($rightLables[$key]['FieldName'], 0, 4) == 'Date') {
//					$viewLBData .= "<span><input type='text' class='datepicker' size='30' name='LB" . $lblRight . "' id='LB" . $lblRight . "' value='" . $_POST[$lblRight] . "'/></span>";
//				} elseif (substr($rightLables[$key]['FieldName'], 0, 6) == 'Select') {
//					$accMapID = $rightLables[$key]['AccountMapID'];
//
//					$viewLBData .= "<span><select name='LB" . $lblRight . "' id='LB" . $lblRight . "' class='clsTextBox' style='width:210px;'>";
//					foreach ($options[$accMapID] as $key => $value) {
//						$selected =  ($options[$accMapID][$key]['OptionID'] == $_POST[$lblRight])?'selected':'';
//						$viewLBData .= "<option value='" .$options[$accMapID][$key]['OptionID']. "' ".$selected.">" .$options[$accMapID][$key]['OptionName']. "</option>";
//					}
//					$viewLBData .= "</select></span>";
//
//				} else {
//					$viewLBData .= "<span><input type='text' class='clsTextBox' size='30' name='LB" . $lblRight . "' id='LB" . $lblRight . "' value='" . $_POST[$lblRight] . "'/></span>";
//				}
//				$viewLBData .= "<br><span class='clsError' id='spn_LB" . $lblRight . "'></span></div><br>";
//			}
//			$viewLBData .= "</div>";			
//		}
//		
//		/**
//		 * get all contacts after add or update to show in Related Contacts box
//		 */
//		$contactLables = 'ContactID,';
//		foreach ($sessLables as $map) {
//			$contactLables .= $map['FieldName'] . ",";
//		}
//		$fields = substr($contactLables, 0, -1);
//		$sql = 'SELECT ? FROM Contact WHERE AccountID=?';
//		$sqlBuild = SqlBuilder()->LoadSql($sql);
//		$sql = $sqlBuild->BuildSql(array(DTYPE_INT, -1), array(DTYPE_INT, $_POST['AccountID']));
//		$sql = str_replace('-1', $fields, $sql);
//		$contactInfo = DbConnManager::GetDb('mpower')->Exec($sql);
//
//		$contactDetails = array();
//
//		for($i = 0; $i < count($contactInfo); $i++) {
//			$contactDetails[] = array('ContactId' => $contactInfo[$i]['ContactID'], 'FirstName' => $contactInfo[$i]['Text01'], 'LastName' => $contactInfo[$i]['Text02']);
//		}
//		
//		//Build HTML for contact box
//		if(count($contactDetails) != 0) {
//			$contactData = "<div style='margin: 10px 10px 10px 10px;'>";
//			foreach($contactDetails as $key=>$value) {
//				$contactData.= "<div style='height:20px;'><a href='slipstream.php?action=contact&contactId=".$contactDetails[$key]['ContactId']."' class='blueLinksBold'>".$contactDetails[$key]['FirstName']."&nbsp;".$contactDetails[$key]['LastName']."</a></div>";
//			}
//			$contactData.= "</div>";
//		} else {
//			$contactData.= "<div style='margin: 10px 10px 10px 10px;' class='BlackBold'>No Contacts found for this company.</div>";
//		}
//		
//		echo json_encode(array('result' => $result->getStatus(), 'viewdata' => $viewData, 'viewlbdata' => $viewLBData, 'contactdata' => $contactData));
//	}
//}
//
//
///**
// * validation functionality for contact add from company page
// * add contact functionality
// */
//if(isset($_POST['postType']) && $_POST['postType'] == 'AddContact') {
//	$newData = $_POST;
//	$result = doValidateNewRecord($newData, $sessLables);
//	
//	$resStatus = $result->getStatus();
//	
//	if ($resStatus['STATUS'] == 'FALSE') {
//		echo json_encode(array('result' => $result->getStatus()));
//	}
//	if ($resStatus['STATUS'] == 'OK') {
//		$result =$objContact->InsertContactInfo($newData, $sessLables, 'insert');
//		echo json_encode(array('result' => $result->getStatus(), 'addType'=>'contact'));
//	}
//}
//
?>
