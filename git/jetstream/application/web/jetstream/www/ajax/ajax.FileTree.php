<?php

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

SessionManager::Init();
SessionManager::Validate();

/***********   Modified Version of jQuery File Tree 
			   root = 0 
 ***********/
/*
<ul class="jqueryFileTree" style="display: none;">
	<li class="directory collapsed"><a href="#" rel="1">folder a</a></li>
	<li class="directory collapsed"><a href="#" rel="1">folder b</a></li>
	<li class="directory collapsed"><a href="#" rel="1"> folder c </a></li>
	<li class="file ext_xls"><a href="#" rel="0" >file 1</a></li>
	<li class="file ext_pdf"><a href="#" rel="0" >file 2</a></li>
	<li class="file ext_txt"><a href="#" rel="0" >file 3</a></li>	
</ul>
*/


if ( empty($_POST['action']) && empty($_POST['dir']) && empty($_POST['id']) ){
	echo "ERROR"; 
	exit ; 
} 
if ($_POST['action'] == 'contact') {
	$category = 'Contact';
	$cid = $_POST['id'];
	$contactid = $_POST['id']; 
	$accountid = '';
	$label = getLabelName($category, $cid);
}
else if ($_POST['action'] == 'Company'){
	$category = 'Company' ; 
	$cid = $_POST['id'] ;
	$label = getLabelName($category, $cid);
}

else if ($_POST['action'] == 'account' ){
	$category = 'Account' ; 
	$cid = $_POST['id'] ;
	$label = getLabelName($category, $cid);
}

else {
	$split = explode('_', $_POST['action']) ; 
	$category = $split[0] ; 
	$cid = $_POST['id'] ;
	$label = getLabelName($category, $cid);
 }
 
 
 if ( isset($_POST['action']) && isset($_POST['dir']) && isset($_POST['id']) && $_POST['action'] == 'contact'){
	echo "<script>$('.cluetip').cluetip({ cluetipClass: 'jetfile', attribute: 'href', activation: 'click', width: 150, leftOffset: 0, sticky: true, closePosition: 'top', closeText:'X', arrows: false,  showTitle: false});</script>";

	$lists = getListing($_POST['action'], $_POST['id'], $_POST['dir'] ); 
	echo ' <ul class="jqueryFileTree" style="display: none;"> ';

	// print out all the folder first 
	$files = array () ; 
	if ($_POST['dir'] == 0){
//		$display = (strlen($label) > 14) ? substr($label,0, 14 )  : $label ; 
		echo ' <li class="directory collapsed"><a href="#" rel="1" >'.substr($label,0, 14 ).' Documents </a> '.CompileMenu('folder','1' ).'</li>';
	}
	foreach ($lists as $list){
		if ( $list['Extension'] == 'Folder' ){	
			echo '<li class="directory collapsed"><a href="#" rel="'.$list['JetFileID'].'">'. substr($list['LabelName'],0,14).'</a>'.CompileMenu('folder',$list['JetFileID'] ).'</li>';
		}
		else{
			array_push($files, $list); 
		}		
	} 
	foreach ($files as $file){
		echo '<li class="file ext_'.$file['Extension'].'"><a href="#" rel="'.$file['JetFileID'].'" >'. substr($file['LabelName'],0,14) .'</a>' . CompileMenu('file',$file['JetFileID'] );
		echo '</li>';
	}
	echo '</ul>';
} 

else if ( isset($_POST['action']) && isset($_POST['dir']) && isset($_POST['id']) && $_POST['action'] == 'account'){
	echo "<script>$('.cluetip').cluetip({ cluetipClass: 'jetfile', attribute: 'href', activation: 'click', width: 150, leftOffset: 0, sticky: true, closePosition: 'top', closeText:'X', arrows: false,  showTitle: false});</script>";
//	echo "<script>$('.cluetip').cluetip({ dropShadow: true, cluetipClass: 'rounded', showTitle:false, attribute: 'href', activation: 'click', width: 150,  closePosition: 'Bottom',  });</script>";closeText: 'X',

	$lists = getListing($_POST['action'], $_POST['id'], $_POST['dir'] ); 
	echo ' <ul class="jqueryFileTree" style="display: none;"> ';
	// print out all the folder first 
	$files = array () ; 
	if ($_POST['dir'] == 0){
		echo ' <li class="directory collapsed"><a href="#" rel="1"> '.$label.' Documents </a> '.CompileMenu('folder','1' ).'</li>';
	}
	foreach ($lists as $list){
		if ( $list['Extension'] == 'Folder' ){	
			echo '<li class="directory collapsed"><a href="#" rel="'.$list['JetFileID'].'">'. $list['LabelName'].'</a>'.CompileMenu('folder',$list['JetFileID'] ).'</li>';
		}
		else{
			array_push($files, $list); 
		}		
	} 
	foreach ($files as $file){
		echo '<li class="file ext_'.$file['Extension'].'"><a href="#" rel="'.$file['JetFileID'].'" >'. $file['LabelName'] .'</a>' . CompileMenu('file',$file['JetFileID'] );
		echo '</li>';
	}
	echo '</ul>';
} 

else if ( isset($_POST['action']) && isset($_POST['dir']) && isset($_POST['id']) && $_POST['action'] == 'Company'){
	echo "<script>$('.cluetip').cluetip({ cluetipClass: 'jetfile', attribute: 'href', activation: 'click', width: 150, leftOffset: 0, sticky: true, closePosition: 'top', closeText:'X', arrows: false,  showTitle: false});</script>";
//	echo "<script>$('.cluetip').cluetip({ dropShadow: true, cluetipClass: 'rounded', showTitle:false, attribute: 'href', activation: 'click', width: 150,  closePosition: 'Bottom',  });</script>";closeText: 'X',

	$lists = getListing($_POST['action'], $_POST['id'], $_POST['dir'] ); 
	echo ' <ul class="jqueryFileTree" style="display: none;"> ';
	// print out all the folder first 
	$files = array () ; 
	if ($_POST['dir'] == 0){
		echo ' <li class="directory collapsed"><a href="#" rel="1"> '.$label.' Documents </a> '.CompileMenu('folder','1' ).'</li>';
	}
	foreach ($lists as $list){
		if ( $list['Extension'] == 'Folder' ){	
			echo '<li class="directory collapsed"><a href="#" rel="'.$list['JetFileID'].'">'. $list['LabelName'].'</a>'.CompileMenu('folder',$list['JetFileID'] ).'</li>';
		}
		else{
			array_push($files, $list); 
		}		
	} 
	foreach ($files as $file){
		echo '<li class="file ext_'.$file['Extension'].'"><a href="#" rel="'.$file['JetFileID'].'" >'. $file['LabelName'] .'</a>' . CompileMenu('file',$file['JetFileID'] );
		echo '</li>';
	}
	echo '</ul>';

}

else if ( count(explode('_', $_POST['action'])) >= 1){

	$lists = getFolderListing($_POST['action'], $_POST['id'], $_POST['dir']  ); 
	echo ' <ul class="jqueryFileTree" style="display: none;"> ';
	// print out all the folder first 
	$files = array () ; 
	if ($_POST['dir'] == 0){
		echo ' <li class="directory collapsed"><a href="#" rel="1" ondblClick="javascript:chooseFolder(1, \'Related Documets\')"> '.$label.' Documents </a> </li>';
	}
	if (count($lists) > 0 ){
		foreach ($lists as $list){
			if ( $list['Extension'] == 'Folder' ){	
				echo '<li class="directory expanded"><a href="#" rel="'.$list['JetFileID'].'" ondblClick="javascript:chooseFolder('. $list['JetFileID'] .', \''.$list['LabelName'].'\' )">'. $list['LabelName'].'</a></li>';
			}
		} 
	}
	echo '</ul>';
	
}


function CompileMenu($t, $i){
	global $contactid , $accountid; 
	global $category , $cid; 
	
//	return "<a href=\"ajax/JetFileMenu.php?type=$t&id=$i&accountid=$accountid&contactid=$contactid&\"".' id="event_legend" style="" class="cluetip-clicked cluetip" ><img src="images/asc.gif" style="padding-bottom:3px"></a>';
	return "<a href=\"ajax/JetFileMenu.php?type=$t&id=$i&category=$category&cid=$cid&accountid=$accountid&contactid=$contactid&\"".' id="event_legend" style="" class="cluetip-clicked cluetip" ><img src="images/menu_icon.gif" ></a>';
}

function getListing ($type, $id , $ParentID ){

	$sql = 'SELECT JetFileID, LabelName, Extension from JetFile as j , FileType as f WHERE j.CompanyID = ? AND j.typeid  = ? AND j.type = \''. $type .'\' AND j.ParentID = ?  AND j.FileTypeID = f.FileTypeID AND ( DeletedBy =\'\' OR DeletedOn = \'\' ) ORDER By LabelName ';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']), array(DTYPE_INT, $id), array(DTYPE_INT, $ParentID));
	$results = DbConnManager::GetDb('mpower')->exec($sql);

	return $results;

}

function getLabelName ($type , $id ){
	$field = ($type == 'Company') ? 'Name' : ' Text01, Text02 '; 
	$sql = "SELECT  $field  from $type WHERE " . $type . "ID  = ? " ;
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id));

	$results = DbConnManager::GetDb('mpower')->exec($sql);

	if ($type == 'Account' || $type == 'account' ) return $results[0]['Text01'] ;
	else if ($type == 'Company') return $results[0]['Name']; 
	else return $results[0]['Text01'] . ' ' . $results[0]['Text02'] ;
	return $results;

} 

function getFolderListing ($type, $id , $ParentID ){
//	$t = ($type == 'Contact') ? 'ContactID' : 'AccountID' ;  
	$temp = explode('_', $type); 
	$sql = 'SELECT JetFileID, LabelName, Extension , j.FileTypeID from JetFile as j , FileType as f WHERE j.CompanyID = ? AND j.typeid  = ? AND j.type = \''.  $temp[0] .'\' AND j.ParentID = ?  AND j.FileTypeID = f.FileTypeID AND j.FileTypeID = 1 AND ( DeletedBy =\'\' OR DeletedOn = \'\' ) ORDER By LabelName ';	
//	$sql = 'SELECT JetFileID, LabelName from JetFile WHERE CompanyID = ? AND typeid  = ? AND type = \''. $temp[0] .'\' AND ParentID = ?  AND FileTypeID = 1 AND ( DeletedBy =\'\' OR DeletedOn = \'\' ) ORDER By LabelName ';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']), array(DTYPE_INT, $id), array(DTYPE_INT, $ParentID));
//	print $sql;
	$results = DbConnManager::GetDb('mpower')->exec($sql);

	return $results;
}

function CheckEmptyFolder ($type, $id , $ParentID){
	
	$t = ($type == 'contact') ? 'ContactID' : 'AccountID' ; 
	
	$sql = 'SELECT COUNT(JetFileID) FROM JetFile WHERE CompanyID = ? AND ' . $t . ' = ? AND ParentID = ?  ';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']), array(DTYPE_INT, $id), array(DTYPE_INT, $ParentID));

	$results = DbConnManager::GetDb('mpower')->exec($sql);
	 
	return $results[0]['computed'];

} 



?>