<?php
/* Start Jet-37 For event alert for event attendees */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
//require_once BASE_PATH . '/include/class.AccountUtil.php';
//require_once BASE_PATH . '/include/class.ContactUtil.php';
require_once BASE_PATH . '/include/class.EventUtil.php';
//require_once BASE_PATH . '/include/class.UserUtil.php';
//require_once BASE_PATH . '/include/class.JetDateTime.php';
//require_once BASE_PATH . '/include/lib.date.php';
//require_once BASE_PATH . '/slipstream/class.Note.php';

require_once BASE_PATH . '/slipstream/class.Event.php';
require_once BASE_PATH . '/slipstream/class.EventTypeLookup.php';
require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';


SessionManager::Init();
SessionManager::Validate();

function formatEventAlert($alert){
	$utc = $alert['StartDateUTC'];
	$StartDateUTC = strtotime($utc);

	$utc_date = DateTime::createFromFormat(
					'F j Y, g:i a', 
					date('F j Y, g:i a', $StartDateUTC), 
					new DateTimeZone('UTC')
	);
	if(isset($_SESSION['USER']['BROWSER_TIMEZONE'])){
	$utc_date->setTimeZone(new DateTimeZone($_SESSION['USER']['BROWSER_TIMEZONE']));
		}else{
		$utc_date->setTimeZone(new DateTimeZone('America/Chicago'));
		}
		
	$output = '<div class="show">';
	$output .= '<table class="note_alert_table" cellpadding="8">';
	$output .= '<tr>';
	$output .= '<td width="14%" class="subtitle">Subject</td>';
	$output .= '<td width="36%"> '.$alert['Subject'].' </td>';
	$output .= '<td width="14%" class="subtitle">Contact</td>';
	$output .= '<td width="36%"> '.$alert['Contact'].'</td>';
	$output .= '</tr>';
	$output .= '<tr>';
	$output .= '<td class="subtitle"> Attendees </td>';
	$output .= '<td>'.$alert['attendees'].' </td>';
	$output .= '<td class="subtitle">Location</td>';
	$output .= '<td>'.$alert['Location'].'</td>';
	$output .= '</tr>';
	$output .= '<tr>';
	$output .= '<td class="subtitle">Date</td>';
	$output .= '<td>'.$utc_date->format('F j Y, g:i a').'</td>';
	$output .= '<td class="subtitle">Event Type</td>';
	$output .= '<td>'.$alert['EventName'].'<input type="hidden" class ="alert_EventAttendee_cl" name="EventAttendee[]" value='.$alert['EventAttendeeID'].'></td>';
	$output .= '</tr>';
	$output .= '</table>';
	$output .= '</div>';

	return $output;
	
	}
  
  
	$isUseEventAlert = EventUtil::isUseEventAlert($_SESSION['USER']['COMPANYID']);
	$json = '';
	if($isUseEventAlert){
					$time = time() + 6000;
					$currenttime = time();
					$datetime = new DateTime('@' . $time, new DateTimeZone('UTC'));
					$now = new DateTime('@' . $currenttime, new DateTimeZone('UTC'));
					$eventalertTime = $datetime->format('Y-m-d H:i:s');
					$currentTime = $now->format('Y-m-d H:i:s');
					$sql = "select Event.*,EventAttendee.EventAttendeeID from Event
					INNER JOIN EventAttendee
					ON EventAttendee.EventID=Event.EventID
					AND EventAttendee.PersonID = '{$_SESSION['USER']['USERID']}'
					AND Active != 0
					where StartDateUTC BETWEEN '$currentTime' AND '$eventalertTime' AND (Cancelled != 1 OR Cancelled IS NULL) AND (Closed != 1 OR Closed IS NULL)
					";
					$results = DbConnManager::GetDb('mpower')->Exec($sql);
					$output = '';
					$count = count($results);
					if($count > 0){
					$alerts = array();
					$output = array();
					for($i=0;$i<$count;$i++){
						$EventType = EventTypeLookup::GetRecord($_SESSION['company_obj']['CompanyID'], $results[$i]['EventTypeID']);
						$result[$i]['EventName'] = addslashes($EventType['EventName']);
						$result[$i]['EventAttendeeID'] = $results[$i]['EventAttendeeID'];
						$result[$i]['Subject'] = $results[$i]['Subject'];
						$result[$i]['Location'] = $results[$i]['Location'];
						$result[$i]['StartDateUTC'] = $results[$i]['StartDateUTC'];
						$attendee_sql = "SELECT * FROM EventAttendee WHERE EventID = '{$results[$i]['EventID']}' AND (Deleted != 1 OR Deleted IS NULL)";
						$attendees = DbConnManager::GetDb('mpower')->Exec($attendee_sql);
						$attende = '';
						foreach ($attendees as $attendee) {
							$person = ReportingTreeLookup::GetRecord($_SESSION['company_obj']['CompanyID'], $attendee['PersonID']);
							$attende.= $person['FirstName'] . ' ' . $person['LastName'].' ,';
						}
						$result[$i]['attendees'] = rtrim($attende,',');
						
						$Contact = GetContactName($_SESSION['company_obj']['CompanyID'], $results[$i]['ContactID']);
						$result[$i]['Contact'] = $Contact['FirstName'].' '.$Contact['LastName'].'('.$Contact['CompanyName'].')';
						//$alerts[] = $result[$i];
							
						$output[] = formatEventAlert($result[$i]);
					}
					//$output = formatEventAlert($alerts[0]);
					$json = json_encode(array('count' => $count, 'output' => $output));
				//	print_r($json);exit;
				}
		}
	echo $json;
?>
