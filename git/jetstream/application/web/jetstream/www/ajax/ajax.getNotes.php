<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once '../../include/mpconstants.php';
require_once (BASE_PATH . '/slipstream/class.Note.php');
require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.ModuleNotes.php';

SessionManager::Init();
SessionManager::Validate();

if (isset($_POST['ObjType']) && ($_POST['ObjType'] != '') 
	&& isset($_POST['ObjVal']) && ($_POST['ObjVal'] > 0) && isset($_POST['PageNo'])) {
	
	$objNote = new Note();
	$objNote->pageNo = $_POST['PageNo'];
	$objNote->noteObjectType = $_POST['ObjType'];
	$objNote->noteObject = $_POST['ObjVal'];
	
	$noteList = $objNote->getNoteList();
	$pageLinks = $objNote->getPages();
	$pageInfo = $objNote->getPageInfo();
	$resultstr = '';
		
	if(count($noteList) > 0) {
		$module = new ModuleNotes();
		$module->Init($objNote->noteObjectType, $objNote->noteObject);		
		
		if(!isset($noteList['STATUS'])) {
			$module->note_list = $noteList;
			$module->page_links = $pageLinks;
			$module->page_info = $pageInfo;
			$module->tot_rec = $objNote->totalRecords;
		}
			
		$smarty->assign_by_ref('module', $module);
		$output = $smarty->fetch('snippet_note_list.tpl');		
	}
	echo $output;
}