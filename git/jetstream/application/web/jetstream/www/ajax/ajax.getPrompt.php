<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
//require_once BASE_PATH . '/slipstream/opp_tree.php';
require_once BASE_PATH . '/include/class.ReportingTreeLookup.php';

SessionManager::Init();

//$allsales =  array();
	//$Opp_Results = "<link href='../css/ui.datepicker.css' type='text/css' rel='stylesheet'>";
	//$Opp_Results .= "<script src='ui.datepicker.js' type='text/javascript'></script>";
	$Opp_Results = "<p>Select Salesperson(s):</p>";
	$Opp_Results .= "<hr />";
	$Opp_Results .= opp_tree($_SESSION['company_obj']['CompanyID'], $_SESSION['USER']['USERID']);
	$Opp_Results .= "<hr />";
	$action = $_GET['action'];
	
	if($action == 'ClosedCustRPT' || $action =='ClosedOppRPT' || $action == 'ClosedProdRPT'){
			$Opp_Results .= 'Report Start Date</span> <input type="text" name="StartDate" class="datepicker" size="6" value="" /> <br />';
			$Opp_Results .= 'Report  End  Date &nbsp;</span> <input type="text" name="EndDate" class="datepicker" size="6" value="" />';
			$Opp_Results .= "<hr />";	
			}
		
	/*
	if($action == 'export' || $action == 'ClosedCustRPT' || $action =='ClosedOppRPT' || $action == 'ClosedProdRPT')
			$Opp_Results .= '<br />Report Date Range:<br/><input type="textfield" name="dateRange" value="12" />';
			else 
			$Opp_Results .= '<br/><input type="checkbox" value="true" name="weighted"/> Weighted';
	*/		
	//$Opp_Results = 'Report Date Range:<br/><input type="textfield" name="dateRange" value="12" />';
	//if(!isset($allsales)) {
			//$allsales = ReportingTreeLookup::GetLimb($_SESSION['company_obj']['CompanyID'], $_SESSION['USER']['USERID']);
	//	}

	//echo $Opp_Results;
	/*
			if(count($allsales)>0){
			for($i=0; $i<count($allsales);$i++){
				$person_rec = ReportingTreeLookup::GetRecord($_SESSION['company_obj']['CompanyID'], $allsales[$i]);
				$user = "{$person_rec['FirstName']} {$person_rec['LastName']}";
				$Opp_Results .= "<li>". htmlspecialchars($user) ."<input type='checkbox' name='salesperonid' id='salesperonid' value='".$allsales[$i]."' checked ></li>";
			}
		}
	*/	
	echo $Opp_Results;
	
function opp_tree($cid, $uid, $extensions = array()) {

	$person_rec = ReportingTreeLookup::GetRecord($cid, $uid);
	$user = "{$person_rec['FirstName']} {$person_rec['LastName']}";
	
	if(!isset($code)) $code = '';
	if(!isset($first_call)) $first_call = true;
	$code = "<ul>";
	$code .= "<li>"."<input type='checkbox' name='salesperonid' id='salesperonid' value='".$uid."' checked >".htmlspecialchars($user);
	$code .= opp_tree_dir($cid, $uid);
	$code .= "</li></ul>";
	return $code;
}

function opp_tree_dir($cid, $uid, $first_call = true) {
		
	if(!isset($opp_tree)) $opp_tree= '';
	$file = ReportingTreeLookup::GetDirectChildren($cid, $uid);
	if(count($file) > 0) {
	natcasesort($file);
	// Make directories first
	$files = $dirs = array();
	foreach($file as $this_file) {
	//echo "look: ".$this_file;
		if( count(ReportingTreeLookup::GetDirectChildren($cid, $this_file))>=1 ) $dirs[] = $this_file; else $files[] = $this_file;
	}
	$file = array_merge($dirs, $files);
	}
	if( count($file) > 0 ) { 
		$opp_tree .= "<ul>";
		foreach( $file as $this_file ) {	
				$person_rec = ReportingTreeLookup::GetRecord($cid, $this_file);
				$user = "{$person_rec['FirstName']} {$person_rec['LastName']}";
				if( count(ReportingTreeLookup::GetDirectChildren($cid, $this_file))>0 ) {
					
					//$this_checked = 'checked';
					//if(!in_array($this_file,$_SESSION['salesperson_list'])) $this_checked = '';	
	
					$opp_tree .= "<li>" ."<input type='checkbox' name='salesperonid' id='salesperonid' value='".$this_file."' checked >". htmlspecialchars($user);
					$opp_tree .= opp_tree_dir($cid, $this_file, false);
					$opp_tree .= "</li>";
				} else {					
					// File
					// Get extension (prepend 'ext-' to prevent invalid classes from extensions that begin with numbers)
					//$this_checked = 'checked';
					//if(!in_array($this_file,$_SESSION['salesperson_list'])) $this_checked = '';
					//$ext = "ext-" . substr($this_file, strrpos($this_file, ".") + 1); 
					$link = urlencode($this_file);
					$opp_tree .= "<li>" ."<input type='checkbox' name='salesperonid' id='salesperonid' value='".$this_file."' checked >". htmlspecialchars($user) . "</li>";
				}
		}
		$opp_tree .= "</ul>";
	}
	return $opp_tree;
}	
?>