<?php

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/class.ReportingTree.php';
require_once BASE_PATH . '/slipstream/class.ModuleCalendar.php';

SessionManager::Init();
SessionManager::Validate();

date_default_timezone_set('America/Chicago');
$calendar = new ModuleCalendar();

$calendar->Init($_GET['action'], $_GET['day']);
$calendar->Draw();

$smarty->assign_by_ref('module', $calendar);

$output = '';
foreach ($calendar->template_files as $template_file) {
	$output .= $smarty->fetch('shell_module.tpl');
}

echo $output;

?>