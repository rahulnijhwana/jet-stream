<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/include/mpconstants.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/slipstream/class.Note.php';
require_once BASE_PATH . '/slipstream/class.User.php';
require_once BASE_PATH . '/slipstream/class.ModuleNotes.php';

SessionManager::Init();
SessionManager::Validate();

/* Set the Current timestamp. */
$curTimeStamp = date('Y-m-d H:i:s');

$objNote = new Note();
$objNote->noteCreator = $_SESSION['USER']['USERID'];
$objNote->noteCreationDate = $curTimeStamp;
$objNote->noteSubject = $_POST['NoteSubject'];
$objNote->noteObjectType = $_POST['NoteObjType'];
$objNote->noteObject = $_POST['NoteObjVal'];
$objNote->noteSplType = $_POST['NoteSplType'];

/* Note Alert */
$objNote->alertUsers = $_POST['alert_user'];
$objNote->alertStandard = ($_POST['standard_alert'] == 'on') ? true : false;
$objNote->alertEmail = ($_POST['email_alert'] == 'on') ? true : false;

if(isset($_POST['delay_until'])){
	$delay = $_POST['delay_until'] . ' ' . $_POST['hours'] . ':' . $_POST['minutes'] . ' ' . $_POST['periods'];
	$now = strtotime($delay);
	$now += UTC_OFFSET;
	$timestamp = '@' . $now;
	$datetime = new DateTime($timestamp, new DateTimeZone('America/Chicago'));
	$objNote->delayUntil = $datetime->format('Y-m-d G:i:s.000');
}

if (isset($_POST['CheckPrivate']) && ($_POST['CheckPrivate'])) {
	$objNote->notePrivate = 1;
}
if ($_POST['HasTempalte'] != 1 || $_POST['NoteTempId'] == -1) {
        
	$note_text = $_POST['NoteText'];
	$note_text = filter_var($note_text, FILTER_SANITIZE_STRING);
	$note_text = charset_decode_utf_8($note_text);
	$note_text = nl2br($note_text);
	$objNote->noteText = $note_text;
	
}
else {
	
	$noteFields = $objNote->getNoteFieldsByCompanyIdAndNoteTemplateID($_POST['NoteTempId']);
	$noteTempText = '';
	
	for($i = 0; $i < count($noteFields); $i++) {
		switch ($noteFields[$i]['FieldType']) {
			case 'CheckBox' :
				$FieldName = 'NoteField' . $i;
				if ($i != 0) {
					if (isset($_POST[$FieldName]) && $_POST[$FieldName] == 1) {
						$noteTempText = $noteTempText . '<BR/><BR/><b>' . $noteFields[$i]['LabelName'] . '</b> : True';
					} else {
						$noteTempText = $noteTempText . '<BR/><BR/><b>' . $noteFields[$i]['LabelName'] . '</b> : False';
					}
				} else {
					if (isset($_POST[$FieldName]) && $_POST[$FieldName] == 1) {
						$noteTempText = $noteTempText . '<b>' . $noteFields[$i]['LabelName'] . '</b> : True';
					} else {
						$noteTempText = $noteTempText . '<b>' . $noteFields[$i]['LabelName'] . '</b> : False';
					}
				}
				break;
			default :
				$FieldName = 'NoteField' . $i;
				if ($i != 0)
					$noteTempText = $noteTempText . '<BR/><BR/><b>' . $noteFields[$i]['LabelName'] . '</b> : ' . $_POST[$FieldName];
				else
					$noteTempText = $noteTempText . '<b>' . $noteFields[$i]['LabelName'] . '</b> : ' . $_POST[$FieldName];
		}
	}
	$objNote->noteText = nl2br($noteTempText);
}

$result = $objNote->createNote();
$noteList = $objNote->getNoteList();
$pageLinks = $objNote->getPages();
$pageInfo = $objNote->getPageInfo();
$output = '';

if (count($noteList) > 0) {
	$module = new ModuleNotes();
	$module->Init();
	
	if (!isset($noteList['STATUS'])) {
		$module->note_list = $noteList;
		$module->page_links = $pageLinks;
		$module->page_info = $pageInfo;
		$module->tot_rec = $objNote->totalRecords;
	}
	
	$smarty->assign_by_ref('module', $module);
	$output = $smarty->fetch('snippet_note_list.tpl');
}
echo json_encode(array('result' => $result, 'resultList' => $output));

// Taken from php.net utf8-decoded page
// It indicates that it was originally from Squirrelmail
// Converts utf8 encoded strings into html equivalent characters
function charset_decode_utf_8 ($string) {
      /* Only do the slow convert if there are 8-bit characters */
    /* avoid using 0xA0 (\240) in ereg ranges. RH73 does not like that */
    if (! ereg("[\200-\237]", $string) and ! ereg("[\241-\377]", $string))
        return $string;

    // decode three byte unicode characters
    $string = preg_replace("/([\340-\357])([\200-\277])([\200-\277])/e",       
    "'&#'.((ord('\\1')-224)*4096 + (ord('\\2')-128)*64 + (ord('\\3')-128)).';'",   
    $string);

    // decode two byte unicode characters
    $string = preg_replace("/([\300-\337])([\200-\277])/e",
    "'&#'.((ord('\\1')-192)*64+(ord('\\2')-128)).';'",
    $string);

    return $string;
} 
