<?
	session_start();
	define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

	require_once BASE_PATH . '/slipstream/class.Status.php';
	require_once BASE_PATH . '/slipstream/class.NoteMap.php';
	require_once BASE_PATH . '/slipstream/class.NoteTemplate.php';
	require_once BASE_PATH . '/slipstream/class.NoteOption.php';
	include_once('../administrator/include/settings.php');
	include_once('../administrator/data/clear_cache.php');
	
	if(isset($_POST['action']) && $_POST['action'] == 'getNoteTemplatesByFormType')
	{
		$objNoteTemp = new NoteTemplate;
		$noteTemp = $objNoteTemp->getNoteTemplatesByFormTypeAndCompId($_POST['CompanyId'],$_POST['FormType']);
		echo json_encode($noteTemp);
	}
	
	if(isset($_POST['noteSubmit']) && $_POST['noteSubmit'] == 'Save')
	{
		$status = new Status();
		if(trim($_POST['NoteTemplateName']) == '')
		{
			$status->addError('Please enter template name','NoteTemplateName');
			echo json_encode($status->getStatus());
		}
		if($status->status == 'OK')
		{
			$objNoteTemp = new NoteTemplate;
			$result = $objNoteTemp->saveNoteTemplate($_POST['NoteCompanyId'],trim($_POST['NoteTemplateName']),$_POST['NoteFormType']);
			
			/* Clear the jetcache directory. */
			clear_jet_cache($ajax_cache_path);
			
			echo json_encode(array('STATUS'=>'OK','TemplateName'=>$result[0]['TemplateName'],'FormType'=>$result[0]['FormType'],'TempId'=>$result[0]['NoteTemplateID']));
		}
	}
	
	if(isset($_POST['noteSubmit']) && $_POST['noteSubmit'] == 'Update')
	{
		$status = new Status();
		if(trim($_POST['NoteTemplateName']) == '')
		{
			$status->addError('Please enter template name','NoteTemplateName');
			echo json_encode($status->getStatus());
		}
		if($status->status == 'OK')
		{
			$status->setFormattedVar('UPDATE');
			$objNoteTemp = new NoteTemplate;
			$result = $objNoteTemp->updateNoteTemplate($_POST['NoteTemplateId'],trim($_POST['NoteTemplateName']));
			
			/* Clear the jetcache directory. */
			clear_jet_cache($ajax_cache_path);
			
			echo json_encode(array('STATUS'=>'OK','GETFORMATTEDVAR'=>'UPDATE','TemplateName'=>$_POST['NoteTemplateName'],'FormType'=>$_POST['NoteFormType'],'TempId'=>$_POST['NoteTemplateId']));
		}
	}
	
	if(isset($_POST['action']) && $_POST['action'] == 'getTemplate')
	{
		$objNoteTemp = new NoteTemplate;
		$result = $objNoteTemp->getNoteTemplateFields($_POST['companyId'],$_POST['tempId']);
		echo json_encode(array('STATUS'=>'OK','TemplateName'=>$result[0]['TemplateName'],'FormType'=>$result[0]['FormType'],'TempId'=>$result[0]['NoteTemplateID']));
	}
	
	
	if(isset($_POST['action']) && $_POST['action'] == 'getTemplateFields')
	{
		$status = new Status();
		$objNoteMap = new NoteMap;
		$result = $objNoteMap->getAllFieldsByTemplateId($_POST['tempId'],$_POST['companyId']);
		echo json_encode($result);
	}
	
	if(isset($_GET['action']) && ($_GET['action'] == 'saveTemplateField' || $_GET['action'] == 'updateTemplateField'))
	{
		//print_r($_GET);exit;
		$status = new Status();
		if(!isset($_GET['NoteTempFieldName']) || trim($_GET['NoteTempFieldName']) == ''){
			$status->addError('Please enter Field name','NoteTempFieldName');
		}
		if(!isset($_GET['NoteFieldType']) || trim($_GET['NoteFieldType']) == '-1'){
			$status->addError('Please select Field type','NoteFieldType');
		}
		if($status->status == 'OK')
		{
			$objNoteMap = new NoteMap;
			if($_GET['action'] == 'saveTemplateField')
				$noteMapID =  $objNoteMap->saveNoteFields($_GET['companyId'],$_GET['NoteFieldType'],$_GET['NoteTempFieldName'],$_GET['TemplateId']);
			else if($_GET['action'] == 'updateTemplateField')
				$result = $objNoteMap->updateNoteFieldDetailsByMapId($_GET['NoteFieldMapId'],$_GET['companyId'],$_GET['NoteTempFieldName'],$_GET['NoteFieldType']);
		}
		if($_GET['action'] == 'saveTemplateField'){
			$fieldMapId = $noteMapID[0]['NoteMapID'];
		}else if($_GET['action'] == 'updateTemplateField'){
			$fieldMapId = $_GET['NoteFieldMapId'];
			$objNoteMap = new NoteMap;
			$result = $objNoteMap->getNoteFieldDetailsByFieldId($fieldMapId,$_GET['CompanyId']);
			if($result[0]['FieldType'] != $_GET['NoteFieldType']){
				$objNoteOption = new NoteOption;
				$noteOpt = $objNoteOption ->getNoteOptionsByMapId($fieldMapId);
				if(count($noteOpt[0]) > 0)
					$objNoteOption->deleteOptionsByMapId($fieldMapId);
			}
		}
		if(($_GET['NoteFieldType'] == 'DropDown' || $_GET['NoteFieldType'] == 'RadioButton') && isset($_GET['noteOptions']) && $_GET['noteOptions'] != ''){
			$options = $_GET['noteOptions']; 
			foreach($options as $opt){
				$objNoteOption = new NoteOption;
				$status = $objNoteOption->saveNoteOptions($fieldMapId,$opt);	
			}
		}
		/* Clear the jetcache directory. */
		clear_jet_cache($ajax_cache_path);
			
		echo json_encode($status->getStatus());
	}
		
	if(isset($_POST['action']) && $_POST['action'] == 'DeleteTemplate')
	{
		$objNoteTemp = new NoteTemplate;
		$status = $objNoteTemp->deleteNoteTemplateByTempId($_POST['TempId']);
		if($status->status == 'OK') {
			/* Clear the jetcache directory. */
			clear_jet_cache($ajax_cache_path);
			
			echo json_encode(array('STATUS'=>'OK','NoteTempId'=>$_POST['TempId']));
		}
		else
			echo json_encode(array('STATUS'=>'FALSE'));
	}
	
	if(isset($_POST['action']) && $_POST['action'] == 'EditField')
	{
		$objNoteMap = new NoteMap;
		$result = $objNoteMap->getNoteFieldDetailsByFieldId($_POST['NoteFieldId'],$_POST['companyId']);
		if(count($result) == 0)
			echo json_encode(array('STATUS'=>'FALSE'));
		else {
			/* Clear the jetcache directory. */
			clear_jet_cache($ajax_cache_path);
			
			echo json_encode(array('STATUS'=>'OK','FieldName'=>$result[0]['LabelName'],'FieldType'=>$result[0]['FieldType'],'NoteMapId'=>$result[0]['NoteMapID'],'NoteOptions'=>$result[0]['NoteOptions']));
		}
	}
	
	if(isset($_POST['action']) && $_POST['action'] == 'DeleteField')
	{
		$objNoteMap = new NoteMap;
		$status = $objNoteMap->deleteNoteFieldByFieldId($_POST['NoteFieldId'],$_POST['CompanyId']);
		if($status->status == 'OK') {
			/* Clear the jetcache directory. */
			clear_jet_cache($ajax_cache_path);
			
			echo json_encode(array('STATUS'=>'OK','NoteMapId'=>$_POST['NoteFieldId']));
		}
		else
			echo json_encode(array('STATUS'=>'FALSE'));
	}
	
	if(isset($_GET['action']) && $_GET['action'] == 'saveEditNoteOption')
	{
		$optionList = $_GET['optionList'];
		$objNoteOption = new NoteOption;
		$result = $objNoteOption->getNoteOptionsByMapId($_GET['noteMapId']);
		//print_r($result);exit;
		foreach($result as $res){
			if(isset($_GET['option_'.$res['NoteOptionID']]) && $_GET['option_'.$res['NoteOptionID']] != ''){
				//update option by id
				$objNoteOption = new NoteOption;
				$objNoteOption->updateNoteOptions($res['NoteOptionID'],$_GET['option_'.$res['NoteOptionID']]);
			}else{
				//delete option by id
				$objNoteOption = new NoteOption;
				$objNoteOption->deleteNoteOptionsByOptionId($res['NoteOptionID']);
			}
		}
		
		foreach($optionList as $option){
			if($option != '' && $option != NULL){ 
				$objNoteOption = new NoteOption;
				$status = $objNoteOption->saveNoteOptions($_GET['noteMapId'],$option);
			}
		}
		
		/* Clear the jetcache directory. */
		clear_jet_cache($ajax_cache_path);
			
		echo json_encode($status->getStatus());
	}
	
	
	
	
	
	if(isset($_POST['action']) && $_POST['action'] == 'getNoteFieldsByFormType')
	{
		$objNoteMap = new NoteMap;
		$noteFields = $objNoteMap->getNoteFieldByFormTypeAndCompId($_POST['FormType'],$_POST['CompanyId']);
		echo json_encode($noteFields);
	}
	
	
	/*
	
	
	if(isset($_POST['noteSubmit']) && $_POST['noteSubmit'] == 'Update')
	{
		$status = validateNoteFields();
		if($status->status == 'OK')
		{
			$objNoteMap = new NoteMap;
			$status = $objNoteMap->updateNoteFieldDetailsByMapId($_POST['NoteMapId'],$_POST['NoteCompanyId'],$_POST['NoteFieldName'],$_POST['NoteFieldType']);
		}
		if($status->status=='OK' && $_POST['NoteMapId']!='')
			$status->setFormattedVar('UPDATE');
			
		echo json_encode($status->getStatus());
	}
	
	function validateTemplateFields()
	{
		$status=new Status();
		if(trim($_POST['NoteTemplateName']) == '')
		{
			$status->addError('Please enter template name','NoteTemplateName');
		}
		
		if(trim($_POST['NoteFieldType']) == '' || trim($_POST['NoteFieldType']) == -1)
		{
			$status->addError('Please enter field type','NoteFieldType');
		}	
		return $status;
	}
	
	
	*/
?>