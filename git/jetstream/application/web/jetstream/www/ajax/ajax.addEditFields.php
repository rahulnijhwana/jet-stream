<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/class.User.php';
require_once BASE_PATH . '/slipstream/class.AccountMap.php';
require_once BASE_PATH . '/slipstream/class.Account.php';
require_once BASE_PATH . '/slipstream/class.Contact.php';
require_once BASE_PATH . '/slipstream/class.Option.php';
require_once BASE_PATH . '/slipstream/class.Status.php';
require_once BASE_PATH . '/slipstream/class.ContactMap.php';
require_once BASE_PATH . '/slipstream/class.OptionSet.php';
require_once BASE_PATH . '/slipstream/class.Validation.php';
require_once BASE_PATH . '/slipstream/class.Section.php';

require_once BASE_PATH . '/include/class.GoogleConnect.php';

require_once BASE_PATH . '/www/administrator/data/field_parameters.php';

$objAccountMap = new AccountMap();
$objContactMap = new ContactMap();
$objContact = new Contact();
$objAccount = new Account();
$objOption = new Option();
$objValidation = new Validation();

if (!isset($_SESSION)) session_start();
//print_r($_POST);exit;

// Check if selected field can be added or not(i.e there should be unused column for the selected type in the db)
if(isset($_GET['btnAdd'])){
	//print_r($_GET);exit;
	$dataType=(isset($_POST['dataType'])) ? $_POST['dataType'] : '';
	$dataType=explode(',',$dataType);
	$status=new Status();
	$objUser=new User;
	$field = $_GET['fieldName'];
	if($_GET['btnAdd'] == 'AddAccountField'){
		$totalNumberOfField = $objAccount->getFieldsFromAccount($field);
		$usedFields = $objAccountMap->getFieldsFromAccountMapByCompId($_GET['companyId'],$field);
	}else if($_GET['btnAdd'] == 'AddContactField'){
		$totalNumberOfField = $objContact->getFieldsFromContact($field);
		$usedFields = $objContactMap->getFieldsFromContactMapByCompId($_GET['companyId'],$field);
		//print_r($usedFields);exit;
	}
	if(count($totalNumberOfField) <=  count($usedFields)){
		$status->addError('Cannot have more than '.count($totalNumberOfField).' '.$field.' fields');
	}
	echo json_encode($status->getStatus());
}
//print_r($_POST);exit('pp');
/*Array
(
    [frmType] => Contact
    [btnSave] => Save
    [companyId] => 508
    [labelName] => phone
    [type] => Text
    [dataType] => Alphabet
    [isMandatory] => 0
    [isBasicSearch] => 0
    [isFirstName] => 0
    [isLastName] => 0
    [optionList] => 
    [optionSetId] => 
    [contactAccountMapId] => 54
    [contactKeywordId] => -1
    [isCompanyName] => undefined
    [parameter] => 
    [isPrivate] => 1
    [googleField] => phoneNumber-work
)*/
//Save Account/Contact new field.
if(isset($_POST['btnSave']) && $_POST['btnSave'] == 'Save'){ //print_r( $_POST ); exit( );
	if(isset($_POST['frmType']) && $_POST['frmType'] == 'Account')
		$companyData = true;
	else if(isset($_POST['frmType']) && $_POST['frmType'] == 'Contact')
		$companyData = false;

	//Fetch different field names from Contact table
	if(isset($companyData) && !$companyData){
		$totalLongTextFields = $objContact->getFieldsFromContact('LongText');
		$totalTextFields = $objContact->getFieldsFromContact('Text');
		$totalNumberFields = $objContact->getFieldsFromContact('Number');
		$totalBoolFields = $objContact->getFieldsFromContact('Bool');
		$totalSelectFields = $objContact->getFieldsFromContact('Select');
		$totalDateFields = $objContact->getFieldsFromContact('Date');
	}else{
		//Fetch different field names from Account table
		$totalLongTextFields = $objAccount->getFieldsFromAccount('LongText');
		$totalTextFields = $objAccount->getFieldsFromAccount('Text');
		$totalNumberFields = $objAccount->getFieldsFromAccount('Number');
		$totalBoolFields = $objAccount->getFieldsFromAccount('Bool');
		$totalSelectFields = $objAccount->getFieldsFromAccount('Select');
		$totalDateFields = $objAccount->getFieldsFromAccount('Date');
	}
	//print_r($_POST);exit;
	$status = new Status();
	$type = $_POST['dataType'];
	$googleField = $_POST['googleField'];
	$basicSearchField = 0;
	$isCompanyNameField = NULL;
	$isZipCodeField = NULL;
	$isFirstNameField = NULL;
	$isLastNameField = NULL;
	$align = NULL;
	$labelField = '';
	$basicSearchField = NULL;	
	$validationId = NULL;
	$optionSetId = NULL;
	$optionType = NULL;
	$contactFieldAccountMapId = NULL;
	$contactFieldKeywordId = NULL;
	$accounttFieldKeywordId = NULL;
	$fieldParameters = NULL;
	$isPrivateField = 0;
// GMS 10/1/2010
	$isContactTitleField = NULL;
	
	if(isset($_POST['labelName']) && trim($_POST['labelName']) != ''){
		$labelField = trim($_POST['labelName']);
	}else{
		if(count($status->error) == 0)
			$status->addError('Cannot leave Label Name blank.');
	}

	if(isset($_POST['isCompanyName']) && trim($_POST['isCompanyName']) != 0){
		$companyNameSet = $objAccountMap->checkIsCompanySet($_POST['companyId'],NULL);
		if(!$companyNameSet){
			$isCompanyNameField = 1;
			$basicSearchField = 1;
		}else{
			if(count($status->error) == 0)
				$status->addError('Cannot have more than one Company Name field');
		}
	}
	if(isset($_POST['isZipCode']) && trim($_POST['isZipCode']) != 0){
		$zipCodeSet = $objAccountMap->checkIsZipCodeSet($_POST['companyId'],NULL);
		if(!$zipCodeSet){
			$isZipCodeField = 1;
			$basicSearchField = 1;
		}else{
			if(count($status->error) == 0)
				$status->addError('Cannot have more than one Zip Code field');
		}
	}
	
	if(isset($_POST['isFirstName']) && trim($_POST['isFirstName']) != 0){
		$firstNameSet = $objContactMap->checkIsFirstNameSet($_POST['companyId'],NULL);
		if(!$firstNameSet){
			$isFirstNameField = 1;
			$basicSearchField = 1;
		}else{
			if(count($status->error) == 0)
				$status->addError('Cannot have more than one First Name field');
		}
	}
	if(isset($_POST['isLastName']) && trim($_POST['isLastName']) != 0){
		$lastNameSet = $objContactMap->checkIsLastNameSet($_POST['companyId'],NULL);
		if(!$lastNameSet){
			$isLastNameField = 1;
			$basicSearchField = 1;
		}else{
			if(count($status->error) == 0)
				$status->addError('Cannot have more than one Last Name field');
		}
	}
		/*
// GMS 9/22/2010
		if(isset($_POST['isContactTitle']) && trim($_POST['isContactTitle']) != 0){
		$contactTitleSet = $objContactMap->checkIsContactTitleSet($_POST['companyId'],NULL);
		if(!$contactTitleSet){
			$isContactTitleField = 1;
			$basicSearchField = 1;
		}else{
			if(count($status->error) == 0)
				$status->addError('Cannot have more than one Contact Title field');
		}
		
	}
	*/
	
	
	if(isset($_POST['isBasicSearch']) && trim($_POST['isBasicSearch']) != 0){
		$basicSearchField = 1;
	}
//	if(isset($_POST['isMandatory']) && trim($_POST['isMandatory']) != 0){ $isMandatoryField = 1;}
	
	$isMandatoryField = (isset($_POST['isMandatory']) && trim($_POST['isMandatory']) != 0) ? 1 : 0;	

	if(isset($_POST['isPrivate']) && trim($_POST['isPrivate']) != 0){
		$isPrivateField = 1;
	}

	if(isset($_POST['contactAccountMapId']) && trim($_POST['contactAccountMapId']) != -1){
		$contactFieldAccountMapId = $_POST['contactAccountMapId'];
	}
	
//	if(isset($_POST['contactKeywordId']) && trim($_POST['contactKeywordId']) != -1){
//		$contactFieldKeywordId = $_POST['contactKeywordId'];
//	}
	$contactKeywordId = (isset($_POST['contactKeywordId']) && trim($_POST['contactKeywordId']) != -1) ? $_POST['contactKeywordId'] : -1;	
	
	$accountFieldKeywordId = (isset($_POST['accountKeywordId']) && trim($_POST['accountKeywordId']) != -1) ? $_POST['accountKeywordId'] : -1;	
	
//	if(isset($_POST['accountKeywordId']) && trim($_POST['accountKeywordId']) != -1){
//		$accountFieldKeywordId= $_POST['accountKeywordId'];
//	}

	
	if(isset($_POST['optionList']) && trim($_POST['optionList']) != ''){
		$optionStr = $_POST['optionList'];
		$optionArr = explode(',',$optionStr);
	}

	if(isset($_POST['type']) && $_POST['type'] != -1){
		$validationId = $objValidation->getValidationIdByName($_POST['type']);
	}
	
	if(isset($_POST['type']) && ($_POST['type'] == 'Text' || $_POST['type'] == 'Number' || $_POST['type'] == 'Date')){
		$fieldParameters = '|||||||||';
	}
	
	if(isset($_POST['parameter']) && $_POST['parameter'] != ''){
		$fieldParameters = $_POST['parameter'];
	}
	/*Jet-37*/
	if(isset($_POST['googleField']) && $_POST['googleField'] != ''){
		$googleField = $_POST['googleField'];
	}
	/*Jet-37*/
	switch($type){
		case "LongText":
			//Fetch all the used Text fields for contact from ContactMap Table
			if(isset($companyData) && !$companyData){
				$usedTextFields = $objContactMap->getFieldsFromContactMapByCompId($_POST['companyId'],'LongText');
			}else{
			//Fetch all the used Text fields for account from AccountMap Table
				$usedTextFields = $objAccountMap->getFieldsFromAccountMapByCompId($_POST['companyId'],'LongText');
			}
			$unusedFields = array();
			foreach($totalLongTextFields as $totalFields){
				$used = False;
				foreach($usedTextFields as $usedFields){
					if($totalFields['FieldName'] == $usedFields['FieldName']){
						$used =True;
					}
				}
				if(!$used){
					$unusedFields[] = $totalFields['FieldName'];
				}
			}
			break;
		case "Alphabet":
			//Fetch all the used Text fields for contact from ContactMap Table
			if(isset($companyData) && !$companyData){
				$usedTextFields = $objContactMap->getFieldsFromContactMapByCompId($_POST['companyId'],'Text');
			}else{
			//Fetch all the used Text fields for account from AccountMap Table
				$usedTextFields = $objAccountMap->getFieldsFromAccountMapByCompId($_POST['companyId'],'Text');
			}
			$unusedFields = array();
			foreach($totalTextFields as $totalFields){
				$used = False;
				foreach($usedTextFields as $usedFields){
					if($totalFields['FieldName'] == $usedFields['FieldName']){
						$used =True;
					}
				}
				if(!$used){
					$unusedFields[] = $totalFields['FieldName'];
				}
			}
			//print_r($unusedFields);exit;
			break;
		case "Numeric":
			//Fetch all the used Numeric fields for contact from ContactMap Table
			if(isset($companyData) && !$companyData){
				$usedNumberFields = $objContactMap->getFieldsFromContactMapByCompId($_POST['companyId'],'Number');
			}else{
			//Fetch all the used Numeric fields for account from AccountMap Table
				$usedNumberFields = $objAccountMap->getFieldsFromAccountMapByCompId($_POST['companyId'],'Number');
			}
			$unusedFields = array();
			foreach($totalNumberFields as $totalFields){
				$used = False;
				foreach($usedNumberFields as $usedFields){
					if($totalFields['FieldName'] == $usedFields['FieldName']){
						$used =True;
					}
				}
				if(!$used){
					$unusedFields[] = $totalFields['FieldName'];
				}
			}
		break;
		case "Select":
			//Fetch all the used Select fields for contact from ContactMap Table
			if(isset($companyData) && !$companyData){
				$usedSelectFields = $objContactMap->getFieldsFromContactMapByCompId($_POST['companyId'],'Select');
			}else{
			//Fetch all the used Select fields for account from AccountMap Table
				$usedSelectFields = $objAccountMap->getFieldsFromAccountMapByCompId($_POST['companyId'],'Select');
			}
			if(isset($_POST['type']) && $_POST['type'] == 'DropDown')
				$optionType = 1;
			else if(isset($_POST['type']) && $_POST['type'] == 'RadioButton')
				$optionType = 2;

			$unusedFields = array();
			foreach($totalSelectFields as $totalFields){
				$used = False;
				foreach($usedSelectFields as $usedFields){
					if($totalFields['FieldName'] == $usedFields['FieldName']){
						$used =True;
					}
				}
				if(!$used){
					$unusedFields[] = $totalFields['FieldName'];
				}
			}
			if(isset($_POST['optionSetId']) && $_POST['optionSetId'] != ''){
				$objOptionSet = new OptionSet;
				$objOption = new Option;
				$options = $objOption->getOptionsByOptionSetId($_POST['companyId'],$_POST['optionSetId']);
				
				if(count($options) == count($optionArr)){
					$optionSetId = $_POST['optionSetId'];

					for($m=0;$m<count($optionArr);$m++){

						if(stripslashes($optionArr[$m]) != $options[$m]['OptionName'])
						{
							$optionSetId = NULL;
							break;
						}
					}
				}else{
					$optionSetId = NULL;
				}
			}
			$fieldName = $unusedFields[0];
			
			if($optionSetId == NULL){
				if(isset($optionArr) && count($optionArr) >= 1){
					$optionSetObj = new OptionSet;
					$optionSetExists = $optionSetObj ->insertOptionSet($_POST['companyId'],$labelField);
					//echo json_encode( $optionSetExists );
//					if( $optionSetExists == 0 )
//					{
						$arrOptionSet = $optionSetObj ->getLastOptionSetId($_POST['companyId'],$labelField);
						$optionSetId = $arrOptionSet[0]['OptionSetID'];
						foreach($optionArr as $option){
							$objOption = new Option;
							$objOption->insertOptions($_POST['companyId'],trim($option),$optionSetId);
						}
//					}
//					else
//					{
//						$status->addError( 'Option Set Exists !!!' );
//					}
				}
			}
			/*
			if(count($status->error) == 0){
				if(isset($companyData) && !$companyData){
					$result = $objContactMap->insertContactFieldsToMap($_POST['companyId'],$fieldName,$labelField,$isMandatoryField,$optionType,$validationId,$isFirstNameField,$isLastNameField,$align,$optionSetId,$contactFieldAccountMapId,$contactFieldkeywordId,$basicSearchField,$fieldParameters, $isPrivateField);
				}else{
					$result = $objAccountMap->insertAccountFieldsToMap($_POST['companyId'],$fieldName,$labelField,$isCompanyNameField,$isMandatoryField,$optionType,$validationId, $align, $optionSetId, $accountFieldKeywordId , $basicSearchField,$fieldParameters, $isPrivateField);
				}
			}
			
// GMS 9/22/2010
			if(count($status->error) == 0){
				if(isset($companyData) && !$companyData){
					$result = $objContactMap->insertContactFieldsToMap($_POST['companyId'],$fieldName,$labelField,$isMandatoryField,$optionType,$validationId,$isFirstNameField,$isLastNameField,$align,$optionSetId,$contactFieldAccountMapId,$contactFieldkeywordId,$basicSearchField,$fieldParameters, $isPrivateField, $isContactTitleField);
				}else{
					$result = $objAccountMap->insertAccountFieldsToMap($_POST['companyId'],$fieldName,$labelField,$isCompanyNameField,$isMandatoryField,$optionType,$validationId, $align, $optionSetId, $accountFieldKeywordId , $basicSearchField,$fieldParameters, $isPrivateField, $isContactTitleField);
				}
			}
			*/			
		break;
		case "Bool":
			//Fetch all the used Boolean fields for contact from ContactMap Table
			if(isset($companyData) && !$companyData){
				$usedBoolFields = $objContactMap->getFieldsFromContactMapByCompId($_POST['companyId'],'Bool');
			}else{
			//Fetch all the used Boolean fields for account from AccountMap Table
				$usedBoolFields = $objAccountMap->getFieldsFromAccountMapByCompId($_POST['companyId'],'Bool');

			}
			$unusedFields = array();
			foreach($totalBoolFields as $totalFields){
				$used = False;
				foreach($usedBoolFields as $usedFields){
					if($totalFields['FieldName'] == $usedFields['FieldName']){
						$used =True;
					}
				}
				if(!$used){
					$unusedFields[] = $totalFields['FieldName'];
				}
			}
		break;
		case "Date":
			//Fetch all the used Date fields for contact from ContactMap Table
			if(isset($companyData) && !$companyData){
				$usedDateFields = $objContactMap->getFieldsFromContactMapByCompId($_POST['companyId'],'Date');
			}else{
			//Fetch all the used Date fields for account from AccountMap Table
				$usedDateFields = $objAccountMap->getFieldsFromAccountMapByCompId($_POST['companyId'],'Date');
			}
			$unusedFields = array();
			foreach($totalDateFields as $totalFields){
				$used = False;
				foreach($usedDateFields as $usedFields){
					if($totalFields['FieldName'] == $usedFields['FieldName']){
						$used =True;
					}
				}
				if(!$used){
					$unusedFields[] = $totalFields['FieldName'];
				}
			}
		break;
	}

//var_dump($_POST['companyId'],$fieldName,$labelField,$isMandatoryField,$optionType,$validationId,$isFirstNameField,$isLastNameField,$align,$optionSetId,$contactFieldAccountMapId,$contactFieldKeywordId,$basicSearchField,$fieldParameters, $isPrivateField, $isContactTitleField,$googleField);
//var_dump(count($status->error) == 0);
// GMS 10/1/2010
	$fieldName = $unusedFields[0];
//	if($type != 'Select'){
		if(count($status->error) == 0){
			if(isset($companyData) && !$companyData){
				$result = $objContactMap->insertContactFieldsToMap($_POST['companyId'],$fieldName,$labelField,$isMandatoryField,$optionType,$validationId,$isFirstNameField,$isLastNameField,$align,$optionSetId,$contactFieldAccountMapId,$contactFieldKeywordId,$basicSearchField,$fieldParameters, $isPrivateField, $isContactTitleField,$googleField);
			}else{
				$result = $objAccountMap->insertAccountFieldsToMap($_POST['companyId'],$fieldName,$labelField,$isCompanyNameField,$isZipCodeField,$isMandatoryField,$optionType,$validationId,$align,$optionSetId,$accountFieldKeywordId , $basicSearchField,$fieldParameters, $isPrivateField, $isContactTitleField);
				
			}
		}

		
		
//	}
	echo json_encode($status->getStatus());
}
/*Start JET-37*/
if(isset($_GET['getContactmap']) && $_GET['getContactmap']=='ContactgoogleMap'){
		$isUseGoogleSync = GoogleConnect::isUseGoogleSync($_GET['companyId']);
	if($isUseGoogleSync){
	$re = $objContact->getGoogleFields($_GET['companyId']);
		echo json_encode($re);
		}else{
			echo json_encode(array('code'=>'200'));
			}
	}
/*Start JET-37*/
if(isset($_POST['action']) && trim($_POST['action']) == 'EditFeild'){

	$objOption = new Option();
	$objOptionSet = new OptionSet();
	$optionDetails = array();
	$optionAccountMapId = NULL;
	$mapId = $_POST['fieldMapId'];
	$companyId = $_POST['companyId'];
	$fieldDetails = '';
	$fieldType ='';

	if(isset($_POST['frmType']) && $_POST['frmType'] == 'Account'){
		$fieldDetails = $objAccountMap->getAccountMapDetailsById($mapId);
		$fieldDetails[0]['LabelName'] = htmlentities($fieldDetails[0]['LabelName'], ENT_QUOTES);
		$fieldOptionType = $fieldDetails[0]['OptionType'];
		if($fieldOptionType == 1 || $fieldOptionType== 2){
			if($fieldOptionType == 1)
				$fieldType = 'Dropdown';
			else
				$fieldType = 'Radio Button';

			$options = $objOption->getOptionsByOptionSetId($companyId,$fieldDetails[0]['OptionSetID']);
			$optCompid = -1;
			if(count($options) > 0)
				$optCompid = $options[0]['CompanyID'];
			$optionDetails[] = array('OptionSetId'=>$fieldDetails[0]['OptionSetID'],'AccountMapId'=>$mapId,'Options'=>$options,'CompID'=>$optCompid);
		}
		if($fieldType == ''){
			$fieldName = $fieldDetails[0]['FieldName'];
			if(stristr($fieldName,"Text"))
				$fieldType = $objValidation->getFieldTypeByValidationId($fieldDetails[0]['ValidationType']);
			else if(stristr($fieldName,"Bool"))
				$fieldType = 'Check Box';
			else if(stristr($fieldName,"Date"))
				$fieldType = 'Date';
			else if(stristr($fieldName,"Number"))
				$fieldType = 'Number';
		}
		$fieldDetails[0]['OptionDetails'] = $optionDetails ;
		$fieldDetails[0]['FieldType'] = $fieldType ;

	}else if(isset($_POST['frmType']) && $_POST['frmType'] == 'Contact'){
	//	var_dump($mapId);exit;
		$fieldDetails = $objContactMap->getContactMapDetailsById($mapId);
		/*START JET-37 Contact mapping*/
		$fieldDetails[1]['googleFields'] = $objContact->getGoogleFields($_POST['companyId']);
		$fieldDetails[1]['isUseGoogleSync'] = GoogleConnect::isUseGoogleSync($_POST['companyId']);
		/*END JET-37 Contact mapping*/
		$fieldDetails[0]['LabelName'] = htmlentities($fieldDetails[0]['LabelName'], ENT_QUOTES);
		$fieldOptionType = $fieldDetails[0]['OptionType'];
		if($fieldOptionType == 1 || $fieldOptionType== 2){
			if($fieldOptionType == 1)
				$fieldType = 'Dropdown';
			else
				$fieldType = 'Radio Button';

			$options = $objOption->getOptionsByOptionSetId($companyId,$fieldDetails[0]['OptionSetID']);
			$optCompid = -1;
			if(count($options) > 0)
				$optCompid = $options[0]['CompanyID'];
			$optionDetails[] = array('OptionSetId'=>$fieldDetails[0]['OptionSetID'],'ContactMapId'=>$mapId,'Options'=>$options,'CompID'=>$optCompid);
		}
		if($fieldType == ''){
			$fieldName = $fieldDetails[0]['FieldName'];
			if(stristr($fieldName,"Text"))
				$fieldType = $objValidation->getFieldTypeByValidationId($fieldDetails[0]['ValidationType']);
			else if(stristr($fieldName,"Bool"))
				$fieldType = 'Check Box';
			else if(stristr($fieldName,"Date"))
				$fieldType = 'Date';
			else if(stristr($fieldName,"Number"))
				$fieldType = 'Number';
		}
		$fieldDetails[0]['OptionDetails'] = $optionDetails ;
		$fieldDetails[0]['FieldType'] = $fieldType ;
	}
	echo json_encode($fieldDetails);
}

if(isset($_POST['action']) && $_POST['action'] == 'SaveEditFields'){
	//print_r($_POST);exit;
	if(isset($_POST['frmType']) && $_POST['frmType'] == 'Account')
		$companyData = true;
	else if(isset($_POST['frmType']) && $_POST['frmType'] == 'Contact')
		$companyData = false;

	$status=new Status();
	$mapIdValue = $_POST['fieldMapId'];
	$isCompanyNameField = NULL;
	$isZipCodeField = NULL;
	$isFirstNameField = NULL;
	$isLastNameField = NULL;
// GMS 9/22/2010
	$isContactTitleField = NULL;
	$align = NULL;
	$labelField = '';
	$basicSearchField = NULL;
	$isPrivateField = NULL;
	$tabOrder = NULL;
	$contactFieldAccountMapId = NULL;
	$contactFieldKeywordId = NULL;
	$accountFieldKeywordId = NULL;

	if(isset($_POST['labelName']) && trim($_POST['labelName']) != ''){
		$labelField = trim($_POST['labelName']);
	}else{
		if(count($status->error) == 0)
			$status->addError('Cannot leave Label Name blank.');
	}

	if(isset($_POST['isCompanyName']) && trim($_POST['isCompanyName']) != 0){
		$companyNameSet = $objAccountMap->checkIsCompanySet($_POST['companyId'],$mapIdValue);
		if(!$companyNameSet){
			$isCompanyNameField = 1;
		}else{
			if(count($status->error) == 0)
				$status->addError('Cannot have more than one CompanyName field');
		}
	}

	if(isset($_POST['isZipCode']) && trim($_POST['isZipCode']) != 0){
		$zipCodeSet = $objAccountMap->checkIsZipCodeSet($_POST['companyId'],$mapIdValue);
		if(!$zipCodeSet){
			$isZipCodeField = 1;
		}else{
			if(count($status->error) == 0)
				$status->addError('Cannot have more than one Zip Code field');
		}
	}
	
	
	if(isset($_POST['isFirstName']) && trim($_POST['isFirstName']) != 0){
		$firstNameSet = $objContactMap->checkIsFirstNameSet($_POST['companyId'],$mapIdValue);
		if(!$firstNameSet){
			$isFirstNameField = 1;
		}else{
			if(count($status->error) == 0)
				$status->addError('Cannot have more than one FirstName field');
		}
	}
	if(isset($_POST['isLastName']) && trim($_POST['isLastName']) != 0){
		$lastNameSet = $objContactMap->checkIsLastNameSet($_POST['companyId'],$mapIdValue);
		if(!$lastNameSet){
			$isLastNameField = 1;
		}else{
			if(count($status->error) == 0)
				$status->addError('Cannot have more than one LastName field');
		}
	}
// GMS 9/10/2010
	if(isset($_POST['isContactTitle']) && trim($_POST['isContactTitle']) != 0){
		$contactTitleSet = $objContactMap->checkIsContactTitleSet($_POST['companyId'],$mapIdValue);
		if(!$contactTitleSet){
			$isContactTitleField = 1;
		}else{
			if(count($status->error) == 0)
				$status->addError('Cannot have more than one Title field');
		}
	}
	if(isset($_POST['isBasicSearch']) && trim($_POST['isBasicSearch']) != 0){
		$basicSearchField = 1;
	}

	$isMandatoryField = (isset($_POST['isMandatory']) && trim($_POST['isMandatory']) != 0) ? 1 : 0;	
	
	if(isset($_POST['isPrivate']) && trim($_POST['isPrivate']) != 0){
		$isPrivateField = 1;
	}	
	if(isset($_POST['conAccountMapId']) && trim($_POST['conAccountMapId']) != -1){
		$contactFieldAccountMapId = $_POST['conAccountMapId'];
	}
	if(isset($_POST['conKeywordId']) && trim($_POST['conKeywordId']) != -1){
		$contactFieldKeywordId = $_POST['conKeywordId'];
	}
	if(isset($_POST['accKeywordId']) && trim($_POST['accKeywordId']) != -1){
		$accountFieldKeywordId = $_POST['accKeywordId'];
	}
	if(isset($_POST['tabOrder']) && trim($_POST['tabOrder']) != 'null' && trim($_POST['tabOrder']) != ''){
		$tabOrder = $_POST['tabOrder'];
	}
	if(isset($_POST['googleField']) && trim($_POST['googleField']) != ''){
		$googleField = $_POST['googleField'];
	}
	if(count($status->error) == 0){
		if(isset($companyData) && !$companyData){
			$objContactMap = new ContactMap;
// GMS 10/1/2010	/*Start JET-37 */		
			$result = $objContactMap ->updateContactMapById($mapIdValue,$labelField,$isMandatoryField,$isFirstNameField,$isLastNameField,$contactFieldAccountMapId,$contactFieldKeywordId,$basicSearchField,$tabOrder, $isPrivateField, $isContactTitleField,$googleField);
			
			/*Start JET-37 */		
			if($_POST['optionType'] == 1 || $_POST['optionType'] == 2){
				$mapDetails = $objContactMap->getContactMapDetailsById($mapIdValue);
				$optionSetId = $mapDetails[0]['OptionSetID'];
				$objOptionSet = new OptionSet;
				$optSetDet = $objOptionSet -> getOptionsSetDetailsByOptionSetId($optionSetId);
				if($optSetDet[0]['CompanyID'] != -1)
					$objOptionSet -> updateOptionSetName($optionSetId,$labelField);
			}
		}else{
			$objAccountMap = new AccountMap;
			$result = $objAccountMap ->updateAccountMapById($mapIdValue,$labelField,$isMandatoryField,$isCompanyNameField,$isZipCodeField,$basicSearchField,$tabOrder, $isPrivateField, $accountFieldKeywordId);
			if($_POST['optionType'] == 1 || $_POST['optionType'] == 2){
				$objOptionSet = new OptionSet;
				$mapDetails = $objAccountMap->getAccountMapDetailsById($mapIdValue);
				$optionSetId = $mapDetails[0]['OptionSetID'];
				$optSetDet = $objOptionSet -> getOptionsSetDetailsByOptionSetId($optionSetId);
				if($optSetDet[0]['CompanyID'] != -1)
					$objOptionSet -> updateOptionSetName($optionSetId,$labelField);
			}
		}
	}
	echo json_encode($status->getStatus());
}

if(isset($_POST['action']) && $_POST['action'] == 'getMaxFieldValue'){
	if($_POST['frmType'] == 'Account'){
		$objAccountMap = new AccountMap;
		$result = $objAccountMap->getMaxAccountFieldValues($_POST['companyId']);
	}else{
		$objContactMap = new ContactMap;
		$result = $objContactMap->getMaxContactFieldValues($_POST['companyId']);
	}
	$objOptionSet = new OptionSet;
	$optionSet = $objOptionSet->getOptionSetByCompanyId($_POST['companyId']);
	$result[0]['OptionSet'] = $optionSet;
	echo json_encode($result[0]);
}

if(isset($_POST['action']) && $_POST['action'] == 'getOptionSetValue'){
	$objOptionSet = new OptionSet;
	$optionSet = $objOptionSet->getOptionSetByCompanyId($_POST['companyId']);
	$result[0]['OptionSet'] = $optionSet; print_r( $optionSet );
	echo json_encode($result[0]);
}

if(isset($_POST['action']) && $_POST['action'] == 'getFieldParameters'){
	$result = get_all_params();
	echo json_encode($result);
}

if(isset($_POST['action']) && $_POST['action'] == 'GetSelectedFieldParams'){
	$status = new Status();
	$selectedMapId = $_POST['fieldMapId'];

	if($_POST['frmType'] == 'Account'){
		$objAccountMap = new AccountMap;
		$result = $objAccountMap->getMapIdAndFieldNameByCompanyId($_POST['companyId']);
		foreach($result as $res){
			if($res['AccountMapID'] == $selectedMapId){
				$fieldParam = $objAccountMap->getFieldTypeAndParametersByMapId($selectedMapId);	
			}
		}
	}else{
		$objContactMap = new ContactMap;
		$result = $objContactMap->getMapIdAndFieldNameByCompanyId($_POST['companyId']);
		foreach($result as $res){
			if($res['ContactMapID'] == $selectedMapId){
				$fieldParam = $objContactMap->getFieldTypeAndParametersByMapId($selectedMapId);	
			}
		}
	}
	echo json_encode(array('STATUS'=>'OK','PARAMETERS'=>$fieldParam[0]));
}

if(isset($_POST['action']) && $_POST['action'] == 'SaveEditedFieldParams'){
	if($_POST['frmType'] == 'Account'){
		$objAccountMap = new AccountMap;
		$result = $objAccountMap->updateFieldParametersByMapId($_POST['mapId'],$_POST['parameters']);
	}else{
		$objContactMap = new ContactMap;
		$result = $objContactMap->updateFieldParametersByMapId($_POST['mapId'],$_POST['parameters']);
	}
	echo json_encode($result);
}

if(isset($_GET['action']) && $_GET['action'] == 'saveEditOptionValues'){
	//print_r($_GET);exit;
	$status = new Status();
	$optionExist = false;
	$countOptions = 0;
	$objOption = new Option();
	$newOptions = $_GET['optionList'];
	$optionSetId = $_GET['editOptionSetId'];
	
	$oldOptions = $objOption->getOptionsByOptionSetId($_GET['companyId'],$optionSetId);
	//print_r($oldOptions);exit;
	if(isset($_GET['optionList'])){
		foreach($newOptions as $options){
			if(trim($options) !='')
				$countOptions = $countOptions + 1;
	
		}
	}
	foreach($oldOptions as $old){
		if(isset($_GET[$old['OptionID']]) && trim($_GET[$old['OptionID']]) != ''){
			$optionExist = true;
		}
	}
	if($countOptions > 0 || $optionExist){
		if(isset($_GET['optionList'])){
			foreach($newOptions as $options){
				$objOption = new Option();
				$objOption->insertOptions($_GET['companyId'],$options,$optionSetId);
			}
		}
		foreach($oldOptions as $old){
			if($_GET[$old['OptionID']] != null || trim($_GET[$old['OptionID']]) != ''){
				$objOption = new Option();
				$objOption->updateOptions($old['OptionID'],$_GET[$old['OptionID']]);
			}else{
				$objOption = new Option();
				$objOption->deleteOptionsByOptionId($old['OptionID']);
			}
		}
	}else{
		$status->addError('Can not save blank Option Set.');
	}
	echo json_encode($status->getStatus());
}


if(isset($_POST['action']) && $_POST['action'] == 'GetDeleteFields'){

	if($_POST['frmType'] == 'Account'){
		$objAccountMap = new AccountMap;
		$result = $objAccountMap->getDeleteFields($_POST['companyId']);
	}else{
		$objContactMap = new ContactMap;
		$result = $objContactMap->getDeleteFields($_POST['companyId']);
	}
	echo json_encode($result);
}

if(isset($_GET['action']) && $_GET['action'] == 'DeleteFields'){
	$status = new Status();
	$isFieldSelected = false;
	$deletedFields = array();
	if($_GET['frmType'] == 'Account'){
		$objAccountMap = new AccountMap;
		$result = $objAccountMap->getMapIdAndFieldNameByCompanyId($_GET['companyId']);
		//print_r($result);exit;
		for($i=0;$i<count($result);$i++){
			if(isset($_GET[$result[$i]['AccountMapID']]) && $_GET[$result[$i]['AccountMapID']] == 1){
				$isFieldSelected = true;
				$objAccountMap = new AccountMap;
				$accountDetails = $objAccountMap->getAccountMapDetailsById($result[$i]['AccountMapID']);
				if($accountDetails[0]['IsCompanyName']){
					$status->addError('Sorry you can not delete the company name field.');
				}
				if(count($status->error) == 0){
					$deletedFields[] = $result[$i]['AccountMapID'];
					//Delete AccountField and update Account table
					$fieldName = $accountDetails[0]['FieldName'];
					$objAccountMap = new AccountMap;
					$objAccountMap->deleteAccountFieldById($result[$i]['AccountMapID']);
					$sql = 'update Account set '.$fieldName.'= NULL where CompanyID = ?';
					$sqlBuild = SqlBuilder()->LoadSql($sql);
					$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $_GET['companyId']));
					$rs = DbConnManager::GetDb('mpower')->Exec($sql);
					
					if($accountDetails[0]['OptionType'] == 1 || $accountDetails[0]['OptionType'] == 2){
						
						$sql='Select AccountMapID,LabelName from AccountMap where OptionSetID = ? AND CompanyID = ?';
						$sqlBuild = SqlBuilder()->LoadSql($sql);
						$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $accountDetails[0]['OptionSetID']),array(DTYPE_INT, $_GET['companyId']));
						$accountRes = DbConnManager::GetDb('mpower')->Exec($sql);
						if(count($accountRes) == 0){
							$sql='Select ContactMapID,LabelName from ContactMap where OptionSetID = ? AND CompanyID = ?';
							$sqlBuild = SqlBuilder()->LoadSql($sql);
							$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $accountDetails[0]['OptionSetID']),array(DTYPE_INT, $_GET['companyId']));
							$contactRes = DbConnManager::GetDb('mpower')->Exec($sql);
							if(count($contactRes) == 0){
								$sql='Select CompanyID from OptionSet where OptionSetID = ? ';
								$sqlBuild = SqlBuilder()->LoadSql($sql);
								$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $accountDetails[0]['OptionSetID']));
								$optSetRes = DbConnManager::GetDb('mpower')->Exec($sql);
								if($optSetRes[0]['CompanyID'] != -1){
									$sql = 'Delete from [Option] where OptionSetID = ?';
									$sqlBuild = SqlBuilder()->LoadSql($sql);
									$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $accountDetails[0]['OptionSetID']));
									$rs = DbConnManager::GetDb('mpower')->Exec($sql);
	
									$sql = 'Delete from [OptionSet] where OptionSetID = ?';
									$sqlBuild = SqlBuilder()->LoadSql($sql);
									$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $accountDetails[0]['OptionSetID']));
									$rs = DbConnManager::GetDb('mpower')->Exec($sql);
								}
							}
						}
					}
				}	
			}
		}//End For Loop
	}else{
		$objContactMap = new ContactMap;
		$result = $objContactMap->getMapIdAndFieldNameByCompanyId($_GET['companyId']);
		for($i=0;$i<count($result);$i++){
			if(isset($_GET[$result[$i]['ContactMapID']]) && $_GET[$result[$i]['ContactMapID']] == 1){
				$isFieldSelected = true;
				$objContactMap = new ContactMap;
				$contactDetails = $objContactMap->getContactMapDetailsById($result[$i]['ContactMapID']);
				if($contactDetails[0]['IsFirstName']){
					$status->addError('Sorry you can not delete the first name field.');
				}
				if($contactDetails[0]['IsLastName']){
					$status->addError('Sorry you can not delete the last name field.');
				}
				if(count($status->error) == 0){
					$deletedFields[] = $result[$i]['ContactMapID'];
					//Delete AccountField and update Account table
					$fieldName = $contactDetails[0]['FieldName'];
					$objContactMap = new ContactMap;
					$objContactMap->deleteContactFieldById($result[$i]['ContactMapID']);
					$sql='update Contact set '.$fieldName.'= NULL where CompanyID = ?';
					$sqlBuild = SqlBuilder()->LoadSql($sql);
					$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $_GET['companyId']));
					$rs = DbConnManager::GetDb('mpower')->Exec($sql);			
				
					if($contactDetails[0]['OptionType'] == 1 || $contactDetails[0]['OptionType'] == 2){
					//this field is a select field check if this field is optionSet
						$sql='Select AccountMapID,LabelName from AccountMap where OptionSetID = ? AND CompanyID = ?';
						$sqlBuild = SqlBuilder()->LoadSql($sql);
						$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $contactDetails[0]['OptionSetID']),array(DTYPE_INT, $_GET['companyId']));
						$accountRes = DbConnManager::GetDb('mpower')->Exec($sql);
						if(count($accountRes) == 0){
							$sql='Select ContactMapID,LabelName from ContactMap where OptionSetID = ? AND CompanyID = ?';
							$sqlBuild = SqlBuilder()->LoadSql($sql);
							$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $contactDetails[0]['OptionSetID']),array(DTYPE_INT, $_GET['companyId']));
							$contactRes = DbConnManager::GetDb('mpower')->Exec($sql);
							if(count($contactRes) == 0){
								$sql='Select CompanyID from OptionSet where OptionSetID = ? ';
								$sqlBuild = SqlBuilder()->LoadSql($sql);
								$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $contactDetails[0]['OptionSetID']));
								$optSetRes = DbConnManager::GetDb('mpower')->Exec($sql);
								if($optSetRes[0]['CompanyID'] != -1){
									$sql = 'Delete from [Option] where OptionSetID = ?';
									$sqlBuild = SqlBuilder()->LoadSql($sql);
									$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $contactDetails[0]['OptionSetID']));
									$rs = DbConnManager::GetDb('mpower')->Exec($sql);
	
									$sql = 'Delete from [OptionSet] where OptionSetID = ?';
									$sqlBuild = SqlBuilder()->LoadSql($sql);
									$sql = $sqlBuild->BuildSql(array(DTYPE_INT, $contactDetails[0]['OptionSetID']));
									$rs = DbConnManager::GetDb('mpower')->Exec($sql);
								}
							}
						}
						
					}
					
				}	
			}
		}
		
	}
	if(!$isFieldSelected)
			$status->addError('Please select a field you want to delete.');
			
	echo json_encode(array('STATUS'=>$status->getStatus(),'FIELDSDELETED'=>$deletedFields));
}

if(isset($_POST['action']) && $_POST['action'] == 'EditFieldType'){
	$status = new Status();
	if($_POST['newFieldType'] == 'Text' || $_POST['newFieldType'] == 'Phone' || $_POST['newFieldType'] == 'Url' || $_POST['newFieldType'] == 'Email'){
		$validationId = $objValidation->getValidationIdByName($_POST['newFieldType']);
		if($_POST['frmType'] == 'Account')
			$objAccountMap->updateValidationIdByMapId($_POST['fieldMapId'],$validationId);
		else if($_POST['frmType'] == 'Contact')
			$objContactMap->updateValidationIdByMapId($_POST['fieldMapId'],$validationId);
			
	}else if($_POST['newFieldType'] == 'DropDown' || $_POST['newFieldType'] =='RadioButton'){
		if($_POST['newFieldType'] == 'DropDown')
			$optionType = 1;
		else
			$optionType = 2;
		
		if($_POST['frmType'] == 'Account')
			$objAccountMap->updateOptionTypeByMapId($_POST['fieldMapId'],$optionType);
		else if($_POST['frmType'] == 'Contact')
			$objContactMap->updateOptionTypeByMapId($_POST['fieldMapId'],$optionType);	
	}
	echo json_encode($status->getStatus());
}

?>
