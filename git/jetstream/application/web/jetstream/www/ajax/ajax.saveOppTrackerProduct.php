<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';


$filter_args = array(
	'price' => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
	//'price' => FILTER_SANITIZE_NUMBER_STRING,
	'name' => FILTER_SANITIZE_STRING,
	'order' => FILTER_SANITIZE_NUMBER_INT
);

$inputs = filter_input_array(INPUT_GET, $filter_args);

$name = $inputs['name'];
$price = $inputs['price'];
$order = $inputs['order'];
	
	//$sql = "update ot_Products set ProductName='".$_REQUEST['name']."', price=".$_REQUEST['price'].", SortingOrder=".$_REQUEST['order']." WHERE ProductID= ? ";
	$sql = "update ot_Products set ProductName='".$name."', price=".$price.", SortingOrder=".$order." WHERE ProductID= ? ";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_REQUEST['id']));	
	DbConnManager::GetDb('mpower')->Exec($sql);
		
		//$Opp_Results = "<td align='center'>".$_REQUEST['name']."</td><td align='center'>".$_REQUEST['price']."</td><td align='center'>".$_REQUEST['order']."</td><td align='center'><a href=\"javascript: editProduct(".$_REQUEST['id'].",'".$_REQUEST['name']."',".$_REQUEST['price'].",".$_REQUEST['order'].")\">Edit</a></td><td align='center'><a href='javascript: deleteProduct(".$_REQUEST['id'].")'>Delete</a></td>";
		$Opp_Results = "<td align='center'>".$name."</td><td align='center'>".$price."</td><td align='center'>".$order."</td><td align='center'><a href=\"javascript: editProduct(".$_REQUEST['id'].",'".htmlentities($name)."',".$price.",".$order.")\">Edit</a></td><td align='center'><a href='javascript: deleteProduct(".$_REQUEST['id'].")'>Delete</a></td>";
	
	echo $Opp_Results;
	
?>