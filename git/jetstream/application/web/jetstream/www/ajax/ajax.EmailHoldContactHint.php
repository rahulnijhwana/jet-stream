<?php
/**
 * Start the session.
 * define BASE PATH
 * DB API functions file include
 * @package database
 * class file include
 */

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.ContactHint.php';
require_once BASE_PATH . '/slipstream/class.ModuleEmailHold.php';

SessionManager::Init();
SessionManager::Validate();

$first_name = (isset($_POST['FirstName'])) ? $_POST['FirstName'] : '';
$last_name = (isset($_POST['LastName'])) ? $_POST['LastName'] : '';

/**
 * create ContactHint() class object
 */
$obj_contact_hint = new ContactHint();
$contact_list = $obj_contact_hint->getMatchingContact($first_name, $last_name);

$output = '';
$module = new ModuleEmailHold();
$module->contact_hint_list = $contact_list;
$smarty->assign_by_ref('module', $module);
$smarty->display('snippet_contact_hint.tpl');

// echo $output;

