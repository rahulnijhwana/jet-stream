<?php
/**
* @package Ajax
*/
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');
require_once(BASE_PATH . '/include/class.ReportingTree.php');

SessionManager::Init();
SessionManager::Validate();

$emailContentID = $_POST['emailContentID'];

$sql = "UPDATE Notes SET PrivateNote = 0, Incoming = 0 WHERE EmailContentID = ? ";
$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $emailContentID));
DbConnManager::GetDb('mpower')->Exec($sql);
