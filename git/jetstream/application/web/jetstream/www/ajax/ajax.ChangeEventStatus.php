<?php
/**
 * Start the session.
 * define BASE PATH
 * DB API functions file include
 * @package database
 * class file include
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
require_once BASE_PATH . '/include/mpconstants.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/class.RecEvent.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.GoogleTasks.php';
require_once BASE_PATH . '/slipstream/class.Event.php';
require_once BASE_PATH . '/slipstream/class.Note.php';

//require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
//require_once BASE_PATH . '/include/class.SessionManager.php';
//require_once BASE_PATH . '/slipstream/class.RecurringEvent.php';

SessionManager::Init();
SessionManager::Validate();

$company_id = $_SESSION['USER']['COMPANYID'];
$user_id = $_SESSION['USER']['USERID'];
/*start:JET-36, Time Stamps on Event Completion notes are off by six hours for US Central time users*/
$utc_timezone = new DateTimeZone('UTC');
/*end:JET-36*/

$form_filters = array(
	'EventID' => FILTER_SANITIZE_NUMBER_INT,
	'rep' => FILTER_SANITIZE_STRING,
	'Note' => FILTER_SANITIZE_STRING,
	'FollowOn' => FILTER_SANITIZE_STRING,
	'Action' => FILTER_SANITIZE_STRING
);

$form_values = filter_input_array(INPUT_POST, $form_filters);

$url = parse_url(filter_input(INPUT_SERVER, 'HTTP_REFERER'));
$url_query = ParseQuery($url['query']);
if (isset($url_query['action'])) {
	$source = $url_query['action'];
} else {
	$source = '';
}

if (!empty($form_values['rep'])) {
	$repeating_instance = json_decode(base64_decode($form_values['rep']), true);
	$event_id = $repeating_instance['i'];
	$repeat_date = TrucatedDateToUnix($repeating_instance['d']);
	$repeat_instance = true;
} elseif (isset($form_values['EventID'])) {
	$event_id = $form_values['EventID'];
}

//echo $event_id . ' ' . $repeat_date . ' ' . $repeat_instance;
//exit;


// Load the record from the database
$sql = 'SELECT * FROM Event WHERE EventID = ?';
$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $event_id));
$recordset = DbConnManager::GetDb('mpower')->Execute($sql, 'RecEvent');
$record = $recordset[0];
$record->Initialize();

if ($form_values['Action'] == 'GetContact') {
	if ($record['ContactID'] == null) {
		$js_inst = array('nocontact' => $event_id);
	} else {
		$js_inst = array('nocontact' => null);
	}
	echo json_encode($js_inst);
	exit;
}


// Add an exception to the repeat series parent
if ($record['RepeatType'] > 0 && isset($repeat_date)) {
	if ($record->InRecord('RepeatExceptions', false)) {
		$exceptions = explode(',', $record['RepeatExceptions']);
	} else {
		$exceptions = array();
	}
	$exceptions[] = date('Ymd', $repeat_date);
	$record['RepeatExceptions'] = implode(',', $exceptions);
	$record->Save();
}
// To remove an instance of a repeating, we just need to add an exception, so don't clone
if (!($record['RepeatType'] > 0 && $form_values['Action'] == 'remove' && isset($repeat_date))) {
	// To close a repeating event that is not a specific instance, we need to clone the series
	if ($record['RepeatType'] > 0 && isset($repeat_date)) {

		$tz = new DateTimeZone($_SESSION['USER']['BROWSER_TIMEZONE']);
		
		$record_dt = new JetDateTime(date('r', $repeat_date), $tz);
		$record = CloneEvent($record, $record_dt, $tz);
		
//		$record = CloneEvent($record, $repeat_date);
		$record['RepeatType'] = REPEAT_NONE;
		$record['RepeatParent'] = $event_id;
		unset($record['EventID']);
		$clone = true;
	}
	if ($form_values['Action'] == 'remove') {
		$record['Cancelled'] = 1;
	} else {
		$record['Closed'] = 1;
	}
	
	$record->ModifyDate = TimestampToMsSql(time());
	
	$record->Save();
	
	if (isset($clone) && $clone) {
		// Make a cheap copy of the attendees from the series parent to the cloned copy
		$attendee_sql = "insert into eventattendee select ?, PersonID, Creator, Deleted from eventattendee where eventid = ?";
		$attendee_sql = SqlBuilder()->LoadSql($attendee_sql)->BuildSql(array(DTYPE_INT, array($record->EventID, $record->RepeatParent)));
		// echo $attendee_sql . "\n";
		DbConnManager::GetDb('mpower')->Exec($attendee_sql);
	}
		
	// Insert an event completed note, if it was entered	
	if ($record['Closed'] && !empty($form_values['Note'])) {
		$note = new Note();
		$note->noteCreator = $user_id;
		/*start:JET-36, Time Stamps on Event Completion notes are off by six hours for US Central time users*/
			$date = new DateTime("now", $utc_timezone );
			$note->noteCreationDate = $date->format("Y-m-d H:i:s");
		/*end:JET-36*/
		$note->noteSubject = "Event Completed";
		$note->noteText = nl2br($form_values['Note']);
		$note->noteObjectType = NOTETYPE_EVENT;
		$note->noteObject = $record->EventID;
		$result_note = $note->createNote();
	}
}


/* Start Jet-37 for Record Delete,cancel and Closed */

//$company_id = $_SESSION['USER']['COMPANYID'];
//$user_id = $_SESSION['USER']['USERID'];

//$url = "https://$_SERVER[HTTP_HOST]/jetstream/ajax/ajax.ChangeEventStatusGoogle.php";
//$ch = curl_init($url);
//curl_setopt($ch, CURLOPT_TIMEOUT, 5);
//curl_setopt($ch, CURLOPT_POST, 1);
//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); 
//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//curl_setopt($ch, CURLOPT_POSTFIELDS,"companyid=$company_id&userid=$user_id&eventid=$event_id");
//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//curl_exec($ch);
//curl_error($ch);
//curl_close($ch);
/*End Jet-37 for Record Delete,cancel and Closed */
//$isUseGoogleSync = GoogleConnect::isUseGoogleSync($company_id);
//if ( $isUseGoogleSync ) {
    //if ( GoogleConnect::hasToken($user_id) ) {
        //$sql = 'SELECT ET.Type FROM Event E
        	  //JOIN EventType ET ON E.EventTypeID = ET.EventTypeID
        	 //WHERE E.EventID = ?';
        //$params[] = array(DTYPE_INT, $event_id);
        //$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        //$aType = DbConnManager::GetDb('mpower')->Exec($sql);
        //$eventType = isset($aType[0]['Type']) ? $aType[0]['Type'] : false;

        //$params = array();
        //$sql2 = 'SELECT PersonID FROM EventAttendee WHERE EventID = ? and Creator = 1';
        //$params[] = array(DTYPE_INT, $event_id);
        //$sql = SqlBuilder()->LoadSql($sql2)->BuildSqlParam($params);
        //$aRow = DbConnManager::GetDb('mpower')->Exec($sql);
        //$creator = isset($aRow[0]['PersonID']) ? $aRow[0]['PersonID'] : $user_id;

        //if (false != $eventType) {
        	//if ('EVENT' == $eventType) {
                //if ($record['Closed'] == 1 || $record['Cancelled'] == 1 ) {
                    //$googleConnect = new GoogleConnect($creator, false);
                    //$googleEvent = new GoogleCalendar($googleConnect->calendar, $creator, false);
        			//$googleEvent->setDeletedGoogleEvent($record['googleEventId']);
        		//}
        	//} else {
                //$googleConnect = new GoogleConnect($creator, false);
        		//if ($record['Closed'] == 1) {
                    //$googleTask = new GoogleTasks($googleConnect->tasklists, $googleConnect->tasks, $creator, false);
        			//$googleTask->setCompletedGoogleTask($record['googleId']);
        		//} else if ($record['Cancelled'] == 1) {
                    //$googleTask = new GoogleTasks($googleConnect->tasklists, $googleConnect->tasks, $creator, false);
        			//$googleTask->setClosedGoogleTask($record['googleId']);
        		//}		
        	//}
        //}

    //}
//}
/* End Jet-37 for Record Delete,cancel and Closed */

if ($form_values['FollowOn'] == 'true') {
	$js_inst = array('followon' => $record['ContactID']);
} elseif ($source == 'pending_events') {
	$js_inst = array('dest' => '?action=pending_events');
} else {
	$js_inst = array('dest' => '?action=dashboard');
}

echo json_encode($js_inst);
exit;

/**
 * Create an object of module event.	 
 */
$obj_event = new Event();
$action = $_GET['action'];
$output = array();
$today = date('Y-m-d H:i:s');

// if date is passed assume as rercurring event
// create one event and paass th id
if(isset($_GET['date']) && !empty($_GET['date']) && isset($_GET['action']) && ($_GET['action'] == 'cancel' || $_GET['action'] == 'close')) {
	
	if(isset($_REQUEST['eventId']) && ($_REQUEST['eventId'] > 0)) {
	
		$date = $_GET['date'];
		$event_id = (int) $_REQUEST['eventId'];	
	
		// get the event details
		$obj_recurring_event = new RecurringEvent($_SESSION['USER']['COMPANYID'], $_SESSION['USER']['USERID']);
		
		$status = $obj_recurring_event->UpdateEventStatus($event_id, $date, $action);		
		$output['followOnEvent'] = 0;
		$output['EventID'] = $status['ID'];
	
		
		if($_GET['action'] == 'close') {
			
			/**
			 * If the description field is specified, then it will automatically add that description as a Note.
			 */	
			if(isset($_POST['closingNote']) && (trim($_POST['closingNote']) != '') && $status) {
				$obj_note = new Note();
				$obj_note->noteCreator = $_SESSION['USER']['USERID'];
				$obj_note->noteCreationDate = $today;
				$obj_note->noteSubject = 'Closing Event';
				$obj_note->noteText = nl2br($_POST['closingNote']);
				$obj_note->noteObjectType = (int)NOTETYPE_EVENT;
				$obj_note->noteObject = (int)$status['ID'];
				$result_note = $obj_note->createNote();				
			}			
			
			/**
			 * If Create Follow On Event checkbox is checked then need to show the add event form for the same contact.
			 */
			if(isset($_REQUEST['followOnEvent']) && ($_REQUEST['followOnEvent'])) {
				/**
				 * Send the contact id to load a new event form.	 
				 */				
				//$output['contactId'] = $obj_event->contactId;
				$output['eventId'] = $status['ID'];				
				$output['followOnEvent'] = 1;
			}		
		}
		echo json_encode(array('result' => $output));
		exit;
	}	
} 

if(isset($_REQUEST['eventId']) && ($_REQUEST['eventId'] > 0)) {
	$obj_event->eventId = $_REQUEST['eventId'];	
	
	if($action  == 'cancel') {
		$obj_event->CancelEvent();
	}
	else if($action  == 'close') {
		$status = $obj_event->CloseEvent();
		
		/**
		 * If the description field is specified, then it will automatically add that description as a Note.
		 */	
		if(isset($_POST['closingNote']) && (trim($_POST['closingNote']) != '') && $status) {
			$obj_note = new Note();
			$obj_note->noteCreator = $_SESSION['USER']['USERID'];
			$obj_note->noteCreationDate = $today;
			$obj_note->noteSubject = 'Closing Event';
			$obj_note->noteText = nl2br($_POST['closingNote']);
			$obj_note->noteObjectType = (int)NOTETYPE_EVENT;
			$obj_note->noteObject = (int)$obj_event->eventId;
			$result_note = $obj_note->createNote();				
		}
		$output['followOnEvent'] = 0;
		
		/**
		 * If Create Follow On Event checkbox is checked then need to show the add event form for the same contact.
		 */
		if(isset($_REQUEST['followOnEvent']) && ($_REQUEST['followOnEvent'])) {
			/**
			 * Send the contact id to load a new event form.	 
			 */				
			$output['contactId'] = $obj_event->contactId;
			$output['eventId'] = $obj_event->eventId;
			if(isset($obj_event->dealID))
			$output['oppId'] = $obj_event->dealID;
			$output['followOnEvent'] = 1;
		}	
	}
	echo json_encode(array('result' => $output));
}


function ParseQuery($query) {
	$query = html_entity_decode($query);
	$query = explode('&', $query);
	$output = array();
	
	foreach($query as $param) {
		list($key, $value) = explode('=', $param);
		$output[$key] = $value;
	}
	return $output;
} 

?>
