<?php
/**
 * define BASE PATH
 * include DB API functions file
 * @package database
 * class file include
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
include_once BASE_PATH . '/slipstream/lib.php';
require_once BASE_PATH . '/include/mpconstants.php';
require_once BASE_PATH . '/slipstream/class.Calendar.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

SessionManager::Init();
SessionManager::Validate();

/**
 * create Calendar() class object
 */
$objCalendar = new Calendar();

/**
 * get event details
 * calculating the date and time format from DB value
 */
if (isset($_POST['eventId'])) {
	$eventInfo = getEventsInfoByEventId($_POST['eventId']);
	$eventSTime = explode(":", $eventInfo[0]['StartTime']);
	if (isset($eventSTime[0])) {
		if ($eventSTime[0] < 12) {
			$HType = "AM";
		} elseif ($eventSTime[0] > 12) {
			$HType = "PM";
		} elseif ($eventSTime[0] == 12) {
			$HType = "PM";
		}
		if ($eventSTime[0] > 12) {
			$HHour = ($eventSTime[0] - 12);
		} elseif ($eventSTime[0] == '00') {
			$HHour = 12;
		} else {
			$HHour = $eventSTime[0];
		}
	}
	if (isset($eventSTime[1])) {
		$HMinute = $eventSTime[1];
	}
	$eventDetails = array('EventID' => $eventInfo[0]['EventID'], 'Subject' => $eventInfo[0]['Subject'], 'Type' => $eventInfo[0]['EventName'], 'StartDate' => $eventInfo[0]['StartDate'], 'StartHour' => $HHour, 'StartMinute' => $HMinute, 'HourType' => $HType, 'DurHour' => $eventInfo[0]['DurationHours'], 'DurMinute' => $eventInfo[0]['DurationMinutes'], 'Notes' => $eventInfo[0]['Description']);
	
	echo json_encode(array('STATUS' => 'DISPLAYEVENT', 'EVENTINFO' => $eventDetails, 'EVENTTYPEID' => $eventInfo[0]['EventTypeID']));
}

/**
 * getting upcoming events through ContactID
 * return status accordingly result
 */
if (isset($_POST['ContactID']) != "") {
	$today = date("m/d/Y");
	$events = getUpcomingEventsByContactId($_SESSION['USER']['COMPANYID'],$_POST['contactId'], $today);
	
	$eventTypes = getEventType();
	foreach ($eventTypes as $key => $value) {
		$arrTypes[$eventTypes[$key]['EventTypeID']] = $eventTypes[$key]['EventName'];
	}
	if ($status->status == 'OK') {
		echo json_encode(array('STATUS' => 'INSERTEVENT', 'EVENTS' => $events, 'TYPE' => $type, 'CONTACTNAME' => $contactName[0], 'ID' => $ID, 'EVENTTYPES' => $arrTypes));
	} else {
		echo json_encode($status->getStatus());
	}
}

/**
 * getting events of a particular date when daily calendar page loads
 */
if (isset($_POST['SD'])) {
	if ($_POST['SD'] != "") {
		$events = getEventsInfoByStartDate($_SESSION['USER']['USERID'], $_SESSION['USER']['COMPANYID'], $_POST['SD']);
		$contactArr = $objCalendar->fetchContacts($events, 'ContactID');
		echo json_encode(array('STATUS' => 'PAGEEVENTS', 'EVENTS' => $events, 'CONTACTS' => $contactArr));
	}

}
/**
 * getting events for next date
 */
if (isset($_POST['actionType']) && $_POST['actionType'] == 'NextDay') {
	//Process for  Day goes here
	$dateVal = $_POST['DateVal'];
	$dt = strtotime($dateVal . '+1 day');
	$nextDate = date('m/d/Y', $dt);
	$dateTitle = date("l, F j, Y", strtotime($nextDate));
	$events = getEventsInfoByStartDate($_SESSION['USER']['USERID'], $_SESSION['USER']['COMPANYID'], $nextDate);
	
	//collecting hours array for daily calendar page for next date page to show the events in the respective hours section
	$arrLength = count($events);
	$lastKey = ($arrLength - 1);
	if ($arrLength != 0) {
		
		for($i = 0; $i < $arrLength; $i++) {
			$splitStartTime = explode(":", $events[$i]['StartTime']);
			$hour = $splitStartTime[0];
			if ($hour == "00") {
				$newhour = 8;
			} elseif ($hour != "00") {
				$newsplitStartTime = explode(":", $events[$i]['StartTime']);
				$nhour = $newsplitStartTime[0];
				if ($nhour < 10) {
					$newhour = substr($nhour, -1);
				} else {
					$newhour = $nhour;
				}
				break;
			}
		}
		$lastsplitStartTime = explode(":", $events[$lastKey]['StartTime']);
		$lhour = $lastsplitStartTime[0];
		if ($lhour > 12) {
			$lastHour = ($lhour - 12);
			if ($lastHour < 8) {
				$lastHour = 8;
			}
		} elseif ($lhour < 12) {
			$lastHour = 8;
		} elseif ($lhour == 12) {
			$lastHour = $lhour;
		}
	} else {
		$newhour = 8;
		$lastHour = 8;
	}
	
	//define a array for Hour count
	$hrAM = array();
	for($i = $newhour; $i <= 12; $i++) {
		if ($i < 10) {
			$hrAM[] = "0" . $i . "AM";
		} else {
			$hrAM[] = $i . "AM";
		}
	}
	$hrPM = array();
	for($i = 1; $i <= $lastHour; $i++) {
		if ($i < 10) {
			$hrPM[] = "0" . $i . "PM";
		} else {
			$hrPM[] = $i . "PM";
		}
	}
	$hr = array_merge($hrAM, $hrPM);
	
	$min = array();
	for($j = 15; $j <= 45; $j += 15) {
		$min[] = $j;
	}
	$contactArr = $objCalendar->fetchContacts($events, 'ContactID');
	echo json_encode(array('STATUS' => 'PAGEEVENT', 'EVENTS' => $events, 'DATE' => $nextDate, 'DATETITLE' => $dateTitle, 'HOURCOUNT' => $hr, 'GETMIN' => $min, 'CONTACTS' => $contactArr));

}
/*
 * Getting events for prev date
 */
if (isset($_POST['actionType']) && $_POST['actionType'] == 'PrevDay') {
	//process for day goes here
	$dateVal = $_POST['DateVal'];
	$dt = strtotime($dateVal . '-1 day');
	$PrevDate = date('m/d/Y', $dt);
	$dateTitle = date("l, F j, Y", strtotime($PrevDate));
	$events = getEventsInfoByStartDate($_SESSION['USER']['USERID'], $_SESSION['USER']['COMPANYID'], $PrevDate);
	
	//collecting hours array for daily calendar page for prev date to show the events in the respective hours section
	$arrLength = count($events);
	$lastKey = ($arrLength - 1);
	if ($arrLength != 0) {
		$lastKey = ($arrLength - 1);
		
		for($i = 0; $i < $arrLength; $i++) {
			$splitStartTime = explode(":", $events[$i]['StartTime']);
			$hour = $splitStartTime[0];
			if ($hour == "00") {
				$newhour = 8;
			} elseif ($hour != "00") {
				$newsplitStartTime = explode(":", $events[$i]['StartTime']);
				$nhour = $newsplitStartTime[0];
				if ($nhour < 10) {
					$newhour = substr($nhour, -1);
				} else {
					$newhour = $nhour;
				}
				break;
			}
		}
		$lastsplitStartTime = explode(":", $events[$lastKey]['StartTime']);
		$lhour = $lastsplitStartTime[0];
		if ($lhour > 12) {
			$lastHour = ($lhour - 12);
			if ($lastHour < 8) {
				$lastHour = 8;
			}
		} elseif ($lhour < 12) {
			$lastHour = 8;
		} elseif ($lhour == 12) {
			$lastHour = $lhour;
		}
	} else {
		$newhour = 8;
		$lastHour = 8;
	}
	
	//define a array for Hour count
	$hrAM = array();
	for($i = $newhour; $i <= 12; $i++) {
		if ($i < 10) {
			$hrAM[] = "0" . $i . "AM";
		} else {
			$hrAM[] = $i . "AM";
		}
	}
	$hrPM = array();
	for($i = 1; $i <= $lastHour; $i++) {
		if ($i < 10) {
			$hrPM[] = "0" . $i . "PM";
		} else {
			$hrPM[] = $i . "PM";
		}
	}
	$hr = array_merge($hrAM, $hrPM);
	
	$min = array();
	for($j = 15; $j <= 45; $j += 15) {
		$min[] = $j;
	}
	$contactArr = $objCalendar->fetchContacts($events, 'ContactID');
	echo json_encode(array('STATUS' => 'PAGEEVENT', 'EVENTS' => $events, 'DATE' => $PrevDate, 'DATETITLE' => $dateTitle, 'HOURCOUNT' => $hr, 'GETMIN' => $min, 'CONTACTS' => $contactArr));

}

/**
 * getting events for a particular date range for week view
 */
if (isset($_POST['Type'])) {
	if ($_POST['Type'] == "weekly") {
		$firstDate = $_POST['FirstDate'];
		$lastDate = $_POST['LastDate'];
		
		$events = getEventsInfoByDates($_SESSION['USER']['USERID'], $_SESSION['USER']['COMPANYID'], $firstDate, $lastDate);
		$contactArr = $objCalendar->fetchContacts($events, 'ContactID');
		echo json_encode(array('STATUS' => 'PAGEEVENTS', 'EVENTS' => $events, 'CONTACTS' => $contactArr));
	}

}

/**
 * getting events for next week
 */
if (isset($_POST['actionType']) && $_POST['actionType'] == 'NextWeek') {
	//process for day goes here
	$firstDate = $_POST['FirstDateVal'];
	$lastDate = $_POST['LastDateVal'];
	
	$dtFirst = strtotime($lastDate . '+1 day');
	$nextWeekFirstDate = date('m/d/Y', $dtFirst);
	$dtLast = strtotime($lastDate . '+7 day');
	$nextWeekLastDate = date('m/d/Y', $dtLast);
	
	//create the week days to display in top of the weekly calendar page
	$weekDateType = array();
	for($i = $nextWeekFirstDate; $i <= $nextWeekLastDate; $i = date('m/d/Y', strtotime($i . '+1 day'))) {
		$weekDates = $i;
		$weekDateType[] = date("l-j-M", strtotime($weekDates));
	}
	
	$dateTitle = date("F j, Y", strtotime($nextWeekFirstDate)) . " - " . date("F j, Y", strtotime($nextWeekLastDate));
	$events = getEventsInfoByDates($_SESSION['USER']['USERID'], $_SESSION['USER']['COMPANYID'], $nextWeekFirstDate, $nextWeekLastDate);
	
	//collecting hours array for daily calendar page for next week page to show the events in the respective hours section
	$arrLength = count($events);
	$lastKey = ($arrLength - 1);
	if ($arrLength != 0) {
		
		for($i = 0; $i < $arrLength; $i++) {
			$splitStartTime = explode(":", $events[$i]['StartTime']);
			$hour = $splitStartTime[0];
			if ($hour == "00") {
				$newhour = 8;
			} elseif ($hour != "00") {
				$newsplitStartTime = explode(":", $events[$i]['StartTime']);
				$nhour = $newsplitStartTime[0];
				if ($nhour < 10) {
					$newhour = substr($nhour, -1);
				} else {
					$newhour = $nhour;
				}
				break;
			}
		}
		$lastsplitStartTime = explode(":", $events[$lastKey]['StartTime']);
		$lhour = $lastsplitStartTime[0];
		if ($lhour > 12) {
			$lastHour = ($lhour - 12);
			if ($lastHour < 8) {
				$lastHour = 8;
			}
		} elseif ($lhour < 12) {
			$lastHour = 8;
		} elseif ($lhour == 12) {
			$lastHour = $lhour;
		}
		for($i = 0; $i < $arrLength; $i++) {
			$splitStartTime = explode(":", $events[$i]['StartTime']);
			$hour = $splitStartTime[0];
			if ($hour == '12') {
				$lastHour = 12;
			}
		}
	} else {
		$newhour = 8;
		$lastHour = 8;
	}
	
	//define a array for Hour count
	$hrAM = array();
	for($i = $newhour; $i <= 12; $i++) {
		if ($i < 10) {
			$hrAM[] = "0" . $i . "AM";
		} else {
			$hrAM[] = $i . "AM";
		}
	}
	$hrPM = array();
	for($i = 1; $i <= $lastHour; $i++) {
		if ($i < 10) {
			$hrPM[] = "0" . $i . "PM";
		} else {
			$hrPM[] = $i . "PM";
		}
	}
	$hr = array_merge($hrAM, $hrPM);
	
	$min = array();
	for($j = 15; $j <= 45; $j += 15) {
		$min[] = $j;
	}
	$contactArr = $objCalendar->fetchContacts($events, 'ContactID');
	echo json_encode(array('STATUS' => 'PAGEEVENT', 'EVENTS' => $events, 'FIRSTDATE' => $nextWeekFirstDate, 'LASTDATE' => $nextWeekLastDate, 'DATETITLE' => $dateTitle, 'HOURCOUNT' => $hr, 'GETMIN' => $min, 'CONTACTS' => $contactArr, 'WEEKDAY' => $weekDateType));

}

/**
 * getting events for prev week
 */
if (isset($_POST['actionType']) && $_POST['actionType'] == 'PrevWeek') {
	/**
	 * process for day goes here
	 */
	$firstDate = $_POST['FirstDateVal'];
	$lastDate = $_POST['LastDateVal'];
	
	$dtFirst = strtotime($firstDate . '-7 day');
	$prevWeekFirstDate = date('m/d/Y', $dtFirst);
	$dtLast = strtotime($firstDate . '-1 day');
	$prevWeekLastDate = date('m/d/Y', $dtLast);
	
	/**
	 *create the week days to display in top of the weekly calendar page
	 */
	$weekDateType = array();
	for($i = $prevWeekFirstDate; $i <= $prevWeekLastDate; $i = date('m/d/Y', strtotime($i . '+1 day'))) {
		$weekDates = $i;
		$weekDateType[] = date("l-j-M", strtotime($weekDates));
	}
	
	$dateTitle = date("F j, Y", strtotime($prevWeekFirstDate)) . " - " . date("F j, Y", strtotime($prevWeekLastDate));
	$events = getEventsInfoByDates($_SESSION['USER']['USERID'], $_SESSION['USER']['COMPANYID'], $prevWeekFirstDate, $prevWeekLastDate);
	
	/**
	 * collecting hours array for daily calendar page for prev week page to show the events in the respective hours 			section
	 */
	$arrLength = count($events);
	$lastKey = ($arrLength - 1);
	if ($arrLength != 0) {
		
		for($i = 0; $i < $arrLength; $i++) {
			$splitStartTime = explode(":", $events[$i]['StartTime']);
			$hour = $splitStartTime[0];
			if ($hour == "00") {
				$newhour = 8;
			} elseif ($hour != "00") {
				$newsplitStartTime = explode(":", $events[$i]['StartTime']);
				$nhour = $newsplitStartTime[0];
				if ($nhour < 10) {
					$newhour = substr($nhour, -1);
				} else {
					$newhour = $nhour;
				}
				break;
			}
		}
		$lastsplitStartTime = explode(":", $events[$lastKey]['StartTime']);
		$lhour = $lastsplitStartTime[0];
		if ($lhour > 12) {
			$lastHour = ($lhour - 12);
			if ($lastHour < 8) {
				$lastHour = 8;
			}
		} elseif ($lhour < 12) {
			$lastHour = 8;
		} elseif ($lhour == 12) {
			$lastHour = $lhour;
		}
		for($i = 0; $i < $arrLength; $i++) {
			$splitStartTime = explode(":", $events[$i]['StartTime']);
			$hour = $splitStartTime[0];
			if ($hour == '12') {
				$lastHour = 12;
			}
		}
	} else {
		$newhour = 8;
		$lastHour = 8;
	}
	
	/**
	 * define a array for Hour count
	 */
	$hrAM = array();
	for($i = $newhour; $i <= 12; $i++) {
		if ($i < 10) {
			$hrAM[] = "0" . $i . "AM";
		} else {
			$hrAM[] = $i . "AM";
		}
	}
	$hrPM = array();
	for($i = 1; $i <= $lastHour; $i++) {
		if ($i < 10) {
			$hrPM[] = "0" . $i . "PM";
		} else {
			$hrPM[] = $i . "PM";
		}
	}
	$hr = array_merge($hrAM, $hrPM);
	
	$min = array();
	for($j = 15; $j <= 45; $j += 15) {
		$min[] = $j;
	}
	$contactArr = $objCalendar->fetchContacts($events, 'ContactID');
	echo json_encode(array('STATUS' => 'PAGEEVENT', 'EVENTS' => $events, 'FIRSTDATE' => $prevWeekFirstDate, 'LASTDATE' => $prevWeekLastDate, 'DATETITLE' => $dateTitle, 'HOURCOUNT' => $hr, 'GETMIN' => $min, 'CONTACTS' => $contactArr, 'WEEKDAY' => $weekDateType));

}

/**
 * getting events for next month
 */
if (isset($_POST['actionType']) && $_POST['actionType'] == 'NextMonth') {
	$Month = $_POST['MonthVal'];
	$Year = $_POST['YearVal'];
	if ($Month == 12) {
		$NextMonth = 1;
		$NextYear = ($Year + 1);
	} else {
		$NextMonth = ($Month + 1);
		$NextYear = $Year;
	}
	$setDate = "1-" . $NextMonth . "-" . $NextYear;
	$today = date("n_o_F_j", strtotime($setDate));
	
	$expDate = explode("_", $today);
	$tmonth = $expDate['0'];
	$year = $expDate['1'];
	$strMonth = $expDate['2'];
	$todayDate = $expDate['3'];
	$totWeek = 2 + ceil((date("t", mktime(0, 0, 0, $tmonth, 1, $year)) - (8 - date("w", mktime(0, 0, 0, $tmonth, 1, $year)))) / 7);
	
	$myCelandar = new Calendar();
	$monthlyCalendar = $myCelandar->showCalendar($tmonth, $year);
	
	/**
	 * Assign week section to the Calendar
	 */
	$count = 0;
	$arr = array();
	for($i = 0, $j = 1; $i < count($monthlyCalendar); $i++) {
		if ($monthlyCalendar[$i] != '-') {
			$arr[$j][] = $monthlyCalendar[$i];
		} else {
			$arr[$j][] = '-';
		}
		if (count($arr[$j]) == 7) {
			$j++;
		}
	}

	function isNum($variable) {
		if (is_int($variable)) {
			return $variable;
		}
	}
	$weekArr = array();
	$j = 1;
	foreach ($arr as $key => $value) {
		$weekArr[$j] = array_filter($value, "isNum");
		$j++;
	}
	
	/**
	 * Event Section
	 */
	//$events = getEvents($_SESSION['USER']['COMPANYID'], $_SESSION['USER']['USERID']);
	$events = getMonthEvents($_SESSION['USER']['COMPANYID'], $_SESSION['USER']['USERID'], $tmonth, $year);
	$eventArr = array();
	foreach ($events as $key => $val) {
		$sDT = $val['StartDate'];
		
		$mdy = explode("/", $sDT);
		if (isset($mdy[0])) {
			$month = $mdy[0];
		}
		if (isset($mdy[1])) {
			$date = $mdy[1];
		}
		if (isset($mdy[2])) {
			$cyear = $mdy[2];
		}
		if ($month == $tmonth && $cyear == $year) {
			$eventArr[$date][] = array('Date' => $date, 'EventName' => $val['EventName'], 'EventColor' => $val['EventColor'], 'EventID' => $val['EventID'], 'StartDate' => date("l, F j, Y", strtotime($val['StartDate'])));
			//$eventArr[$date][] = array('Date'=>$date);
		}
	}
	
	$eventDate = array();
	foreach ($eventArr as $key => $value) {
		$eventDate[(integer) $key] = 'true';
	}
	
	/**
	 * Monthly calendar design section
	 * Make the table structure for the next month
	 */
	$idata = "<table class='month' width='100%'><tr class='days' height='30'><td  align='center'></td><td  align='center'>Sun</td><td align='center'>Mon</td><td align='center'>Tue</td><td  align='center'>Wed</td><td align='center'>Thu</td><td align='center'>Fri</td><td align='center'>Sat</td></tr>";
	
	$weekcount = 1;
	
	$idata .= "<tr><td align='center' class='weeks' width='10px;'><a href='slipstream.php?action=weekly&fdate=1&ldate=" . max($weekArr[1]) . "&month=" . $NextMonth . "&year=" . $NextYear . "' class='BlackLinks'>W1</a></td>";
	
	$weekcount++;
	$count = 0;
	
	foreach ($monthlyCalendar as $key => $value) {
		if ($value < 10) {
			$itemVal = '0' . $value;
		} else {
			$itemVal = $value;
		}
		if ($value == '-') {
			$style = 'blankday';
		} else {
			$style = 'tdtable';
		}
		$idata .= "<td align='center' width='80px;' valign='top' class='" . $style . "'>";
		if ($value != '-') {
			
			if (array_key_exists($value, $eventDate)) {
				$idata .= "<a href='slipstream.php?action=daily&date=" . $tmonth . "/" . $value . "/" . $NextYear . "' class='dateLinks'>" . $value . "</a>";
				$idata .= "<table><tr>";
				$countTD = 0;
				foreach ($eventArr[$itemVal] as $key1 => $value1) {
					$idata .= "<td><div id='container'><a href='slipstream.php?action=event&eventId=" . $value1['EventID'] . "'  class='tt'><div style='float: relative; height:10px; width:10px; background-color:#" . $value1['EventColor'] . ";'>&nbsp;</div><span class='tooltip'><span class='top'></span><span class='middle'>" . $value1['EventName'] . "<br>" . $value1['StartDate'] . "</span><span class='bottom'></span></span></a></div><div style='height:1px;'></div></td>";
					$countTD++;
					if ($countTD % 5 == 0) {
						$idata .= "</tr><tr>";
					}
				}
				$idata .= "</tr></table>";
			} else {
				$idata .= $value;
			}
		
		}
		
		$idata .= "</td>";
		$count++;
		if ($count == 7) {
			$idata .= "</tr><tr>";
			if ($weekcount <= count($weekArr)) {
				$idata .= "<td align='center' class='weeks' width='10px;' height='100px;'>";
				if (count($weekArr[$weekcount]) != 0) {
					$idata .= "<a href='slipstream.php?action=weekly&fdate=" . min($weekArr[$weekcount]) . "&ldate=" . max($weekArr[$weekcount]) . "&month=" . $NextMonth . "&year=" . $NextYear . "' class='BlackLinks'>W" . $weekcount . "</a>";
				} else {
					$idata .= "W" . $weekcount;
				}
				$idata .= "</td>";
				$weekcount++;
			}
			$count = 0;
		}
	}
	$fdata = $idata . "</tr></table>";
	
	echo json_encode(array('STATUS' => 'MONTHEVENT', 'WEEKS' => count($weekArr), 'YEAR' => $NextYear, 'MONTH' => $strMonth, 'MONTHINT' => $tmonth, 'DATA' => $fdata));

}

/**
 * getting events for prev month
 */
if (isset($_POST['actionType']) && $_POST['actionType'] == 'PrevMonth') {
	$Month = $_POST['MonthVal'];
	$Year = $_POST['YearVal'];
	if ($Month == 1) {
		$PrevMonth = 12;
		$PrevYear = ($Year - 1);
	} else {
		$PrevMonth = ($Month - 1);
		$PrevYear = $Year;
	}
	$setDate = "1-" . $PrevMonth . "-" . $PrevYear;
	$today = date("n_o_F_j", strtotime($setDate));
	
	$expDate = explode("_", $today);
	$tmonth = $expDate['0'];
	$year = $expDate['1'];
	$strMonth = $expDate['2'];
	$todayDate = $expDate['3'];
	$totWeek = 2 + ceil((date("t", mktime(0, 0, 0, $tmonth, 1, $year)) - (8 - date("w", mktime(0, 0, 0, $tmonth, 1, $year)))) / 7);
	
	$myCelandar = new Calendar();
	$monthlyCalendar = $myCelandar->showCalendar($tmonth, $year);
	
	/**
 	 * assign week section to the Calendar
 	 */
	$count = 0;
	$arr = array();
	for($i = 0, $j = 1; $i < count($monthlyCalendar); $i++) {
		if ($monthlyCalendar[$i] != '-') {
			$arr[$j][] = $monthlyCalendar[$i];
		} else {
			$arr[$j][] = '-';
		}
		if (count($arr[$j]) == 7) {
			$j++;
		}
	}

	function isNum($variable) {
		if (is_int($variable)) {
			return $variable;
		}
	}
	$weekArr = array();
	$j = 1;
	foreach ($arr as $key => $value) {
		$weekArr[$j] = array_filter($value, "isNum");
		$j++;
	}
	
	/**
 	 * event Section
 	 */
	//$events = getEvents($_SESSION['USER']['COMPANYID'], $_SESSION['USER']['USERID']);
	$events = getMonthEvents($_SESSION['USER']['COMPANYID'], $_SESSION['USER']['USERID'], $tmonth, $year);
	$eventArr = array();
	foreach ($events as $key => $val) {
		$sDT = $val['StartDate'];
		
		$mdy = explode("/", $sDT);
		if (isset($mdy[0])) {
			$month = $mdy[0];
		}
		if (isset($mdy[1])) {
			$date = $mdy[1];
		}
		if (isset($mdy[2])) {
			$cyear = $mdy[2];
		}
		if ($month == $tmonth && $cyear == $year) {
			$eventArr[$date][] = array('Date' => $date, 'EventName' => $val['EventName'], 'EventColor' => $val['EventColor'], 'EventID' => $val['EventID'], 'StartDate' => date("l, F j, Y", strtotime($val['StartDate'])));
		}
	}
	
	$eventDate = array();
	foreach ($eventArr as $key => $value) {
		$eventDate[(integer) $key] = 'true';
	}
	
	/**
	 * monthly calendar design section
	 * make the table structure for the next month
	 */
	$idata = "<table class='month' width='100%'><tr class='days' height='30'><td  align='center'></td><td  align='center'>Sun</td><td align='center'>Mon</td><td align='center'>Tue</td><td  align='center'>Wed</td><td align='center'>Thu</td><td align='center'>Fri</td><td align='center'>Sat</td></tr>";
	
	$weekcount = 1;
	
	$idata .= "<tr><td align='center' class='weeks' width='10px;'><a href='slipstream.php?action=weekly&fdate=1&ldate=" . max($weekArr[1]) . "&month=" . $PrevMonth . "&year=" . $PrevYear . "' class='BlackLinks'>W1</a></td>";
	
	$weekcount++;
	$count = 0;
	
	foreach ($monthlyCalendar as $key => $value) {
		if ($value < 10) {
			$itemVal = '0' . $value;
		} else {
			$itemVal = $value;
		}
		if ($value == '-') {
			$style = 'blankday';
		} else {
			$style = 'tdtable';
		}
		$idata .= "<td align='center' width='80px;' valign='top' class='" . $style . "'>";
		if ($value != '-') {
			
			if (array_key_exists($value, $eventDate)) {
				$idata .= "<a href='slipstream.php?action=daily&date=" . $tmonth . "/" . $value . "/" . $PrevYear . "' class='dateLinks'>" . $value . "</a>";
				$idata .= "<table><tr>";
				$countTD = 0;
				foreach ($eventArr[$itemVal] as $key1 => $value1) {
					$idata .= "<td><div id='container'><a href='slipstream.php?action=event&eventId=" . $value1['EventID'] . "' class='tt'><div style='float: relative; height:10px; width:10px; background-color:#" . $value1['EventColor'] . ";'>&nbsp;</div><span class='tooltip'><span class='top'></span><span class='middle'>" . $value1['EventName'] . "<br>" . $value1['StartDate'] . "</span><span class='bottom'></span></span></a></div><div style='height:1px;'></div></td>";
					$countTD++;
					if ($countTD % 5 == 0) {
						$idata .= "</tr><tr>";
					}
				}
				$idata .= "</tr></table>";
			} else {
				$idata .= $value;
			}
		
		}
		
		$idata .= "</td>";
		$count++;
		if ($count == 7) {
			$idata .= "</tr><tr>";
			
			if ($weekcount <= count($weekArr)) {
				$idata .= "<td align='center' class='weeks' width='10px;' height='100px;'>";
				if (count($weekArr[$weekcount]) != 0) {
					$idata .= "<a href='slipstream.php?action=weekly&fdate=" . min($weekArr[$weekcount]) . "&ldate=" . max($weekArr[$weekcount]) . "&month=" . $PrevMonth . "&year=" . $PrevYear . "' class='BlackLinks'>W" . $weekcount . "</a>";
				} else {
					$idata .= "W" . $weekcount;
				}
				$idata .= "</td>";
				$weekcount++;
			}
			$count = 0;
		}
	}
	$fdata = $idata . "</tr></table>";
	
	echo json_encode(array('STATUS' => 'MONTHEVENT', 'WEEKS' => count($weekArr), 'YEAR' => $PrevYear, 'MONTH' => $strMonth, 'MONTHINT' => $tmonth, 'DATA' => $fdata));

}
?>