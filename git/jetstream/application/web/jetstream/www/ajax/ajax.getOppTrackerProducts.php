<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

SessionManager::Init();

	$Opp_Results = "<form method='post' id='frmLocation' name='frmLocation'>";
	$Opp_Results .= "<table width='400px' border='1px'><tr height='40px'><th colspan=5>Product Management</th></tr><tr height='40px'><th align='center'>Product Name</th><th align='center'>Price</th><th align='center'>Showing Order</th><th align='center'>Edit</th><th align='center'>Delete</th></tr>";
					
	$sql = "SELECT ProductID, ProductName, Price, SortingOrder FROM ot_Products WHERE IsEnabled=1 AND CompanyID = ? order by SortingOrder";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['company_id']));	
	$result_list = DbConnManager::GetDb('mpower')->GetSqlResult($sql) ;
	$counter = 0;
	while (	$result = mssql_fetch_assoc($result_list)) {		
		$Opp_Results .= "<tr id='product_".$result['ProductID']."' height='40px'><td align='center'>".$result['ProductName']."</td><td align='center'>".$result['Price']."</td><td align='center'>".$result['SortingOrder']."</td><td align='center'><a href=\"javascript: editProduct(".$result['ProductID'].",'".htmlentities($result['ProductName'])."',".$result['Price'].",".$result['SortingOrder'].")\">Edit</a></td><td align='center'><a href='javascript: deleteProduct(".$result['ProductID'].")'>Delete</a></td></tr>";
		$counter++;
	}
	$order_select = "<select name='product_order'>";
	for($i=$counter+1; $i>0;$i--){
	$order_select .= "<option value=$i>$i</option>";
	}
	$order_select .= "</select>";
	
	$Opp_Results .= "<tr id='new_product'><td align='center'><input type='text' name='product_name' value='' /></td><td align='center'><input type='text' name='product_price' value='' /></td><td align='center'>".$order_select."</td><td align='center'><input type='button' name='add_Product' value='Add' onclick='addProduct();'></td><td align='center'><input type='button' name='reset_form' value='Reset' onclick='this.form.reset();'></td></tr>";
	$Opp_Results .= "</table></form>";		
	echo $Opp_Results;
?>