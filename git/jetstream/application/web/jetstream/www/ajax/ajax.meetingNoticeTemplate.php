<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/class.AccountMap.php';
require_once BASE_PATH . '/slipstream/class.ContactMap.php';
require_once BASE_PATH . '/slipstream/class.Section.php';
require_once BASE_PATH . '/slipstream/class.CompanyAccountContactTemp.php';
require_once BASE_PATH . '/slipstream/class.ModuleEmailTemplate.php';
include_once('../administrator/include/settings.php');
include_once('../administrator/data/clear_cache.php');

$objCACTemplate = new CompanyAccountContactTemp();
$objAccountMap = new AccountMap();
$objContactMap = new ContactMap();
$objSection = new Section();
$objModuleTemp = new ModuleEmailTemplate();
$allFields = '';
if (!isset($_SESSION)) session_start();

if(isset($_POST['action']) && $_POST['action'] == 'getFormFields'){
	if(isset($_POST['companyId'])){
		$detailsArr = array();
		$fields = array();
		if($_POST['frmType'] == 'Account'){
			$sections = $objSection->getAccountSectionByCompanyId($_POST['companyId']);
			foreach($sections as $sec){
				$sectionName = '';
				$secId = $sec['SectionID'];
				$accountFields = $objAccountMap->getMapIdBySectionId($secId);
				if($sec['AccountSectionName'] == 'DefaultSection')
					$sectionName = 'Main';
				else
					$sectionName = $sec['AccountSectionName'];
				$fields[] = array('SectionName'=>$sectionName,'Fields'=>$accountFields);	
			}
			$_SESSION['ALLTEMPFIELDS'] = $fields;
			$tempData = $objCACTemplate->getTemplateDataByCompanyId($_POST['companyId'],1);
			$tempData = ConvertFields($fields,$tempData[0]['TemplateData']);
		}
		else if($_POST['frmType'] == 'Contact'){
			$sections = $objSection->getContactSectionByCompanyId($_POST['companyId']);
			foreach($sections as $sec){
				$sectionName = '';
				$secId = $sec['SectionID'];
				$contactFields = $objContactMap->getMapIdBySectionId($secId);
				if($sec['ContactSectionName'] == 'DefaultSection')
					$sectionName = 'Main';
				else
					$sectionName = $sec['ContactSectionName'];
				$fields[] = array('SectionName'=>$sectionName,'Fields'=>$contactFields);	
			}
			$_SESSION['ALLTEMPFIELDS'] = $fields;
			$tempData = $objCACTemplate->getTemplateDataByCompanyId($_POST['companyId'],2);
			$tempData = ConvertFields($fields,$tempData[0]['TemplateData']);
		}
		$detailsArr = array('FieldList'=>$fields,'Template'=>$tempData);
		echo json_encode($detailsArr);
	}
}

if(isset($_POST['action']) && $_POST['action'] == 'saveNoticeTemplate'){
	$status=new Status();
	if(isset($_POST['companyId'])){
		$result = $objCACTemplate->getTemplateDataByCompanyId($_POST['companyId'],$_POST['recType']);
		if(count($result) > 0){
			$objCACTemplate->updateTemplateDataByCompanyId($_POST['companyId'],$_POST['recType'],ConvertLabels($_SESSION['ALLTEMPFIELDS'],$_POST['templateData']));
		}
		else{
			$objCACTemplate->insertTemplateDataByCompanyId($_POST['companyId'],$_POST['recType'],ConvertLabels($_SESSION['ALLTEMPFIELDS'],$_POST['templateData']));
		}	
		
		/* Clear the jetcache directory. */
		clear_jet_cache($ajax_cache_path);
	}else{
		$status->addError('Error saving template data.');
	}
	echo json_encode($status->getStatus());
}

function ConvertLabels($allData,$template_data='') 
{
	foreach($allData as $data) {
		$sectionName = $data['SectionName'];
		if($sectionName == 'DefaultSection')
			$sectionName = 'Main';
			
		$fields = $data['Fields'];
		foreach($fields as $fld){
			$search = "{$sectionName}.{$fld['LabelName']}"; 
			$db_replace = "{$fld['FieldName']}";
			$template_data = str_replace($search,$db_replace,$template_data);
		}
	}
	return $template_data;

}
	
function ConvertFields($allData,$template_data='') 
{
	if(!is_array($fields))
		$template_data;
	
	foreach($allData as $data) {
		$sectionName = $data['SectionName'];
		if($sectionName == 'DefaultSection')
			$sectionName = 'Main';
			
		$fields = $data['Fields'];
		foreach($fields as $fld){
			$search = "{$fld['FieldName']}"; 
			$db_replace = "{$sectionName}.{$fld['LabelName']}";
			$template_data = str_replace($search,$db_replace,$template_data);
		}
	}
	return $template_data;
}
?>
