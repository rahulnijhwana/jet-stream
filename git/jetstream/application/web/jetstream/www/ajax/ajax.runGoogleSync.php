<?php
// run google sync as backend process

if ( '' == $userid && '' == $companyid ) {
    require_once '../../include/class.SessionManager.php';
    SessionManager::Init();
    if (!empty($_SESSION['USER']['USERID'])) {
        $userid = $_SESSION['USER']['USERID'];
    }
    if (!empty($_SESSION['USER']['COMPANYID'])) {
        $companyid = $_SESSION['USER']['COMPANYID'];
    }
}

if ( $userid == '' || $companyid == '' ) {
    error_log('Fail to run runGoogleSync : no userid or companyid');
    echo json_encode(array('code'=>'200'));
}

$googleScriptPath = realpath(dirname(__FILE__));
$url = 'https://www.mpasatest.com/jetstream/ajax/ajax.callGoogleSync.php?userid=' . $userid . '&companyid=' . $companyid;
$command = "/usr/bin/curl --insecure --request GET '$url'";

@shell_exec(sprintf('%s > /dev/null 2>&1 &', $command));

echo json_encode(array('code'=>'200'));


