<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

SessionManager::Init();

	$Opp_Results = "<form method='post' id='frmLocation' name='frmLocation'>";
	$Opp_Results .= "<table width='400px' border='1px'><tr height='40px'><th colspan=5>Sales Cycle Location Management</th></tr><tr height='40px'><th align='center'>Location Name</th><th align='center'>Percentage</th><th align='center'>Showing Order</th><th align='center'>Edit</th><th align='center'>Delete</th></tr>";
					
	$sql = "SELECT SCLID, SCLName, SCLPercent, SortingOrder FROM ot_SalesCycleLocation WHERE InUse=1 and CompanyID = ? order by SortingOrder";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['company_id']));	
	$result_list = DbConnManager::GetDb('mpower')->GetSqlResult($sql) ;
	$counter = 0;
	while (	$result = mssql_fetch_assoc($result_list)) {		
		$Opp_Results .= "<tr id='location_".$result['SCLID']."' height='40px'><td align='center'>".$result['SCLName']."</td><td align='center'>".$result['SCLPercent']."</td><td align='center'>".$result['SortingOrder']."</td><td align='center'><a href=\"javascript: editLocation(".$result['SCLID'].",'".htmlentities($result['SCLName'])."',".$result['SCLPercent'].",".$result['SortingOrder'].")\">Edit</a></td><td align='center'><a href='javascript: deleteLocation(".$result['SCLID'].")'>Delete</a></td></tr>";
		$counter++;
	}
	$order_select = "<select name='location_order'>";
	for($i=$counter+1; $i>0;$i--){
	$order_select .= "<option value=$i>$i</option>";
	}
	$order_select .= "</select>";
	
	$percent_select = "<select name='location_percent'>";
	for($i=1; $i<=100;$i++){
	$percent_select .= "<option value=$i>$i</option>";
	}
	$percent_select .= "</select>";
	
	$Opp_Results .= "<tr id='new_location'><td align='center'><input type='text' name='location_name' value='' /></td><td align='center'>".$percent_select."</td><td align='center'>".$order_select."</td><td align='center'><input type='button' name='add_Location' value='Add' onclick='addLocation();'></td><td align='center'><input type='button' name='reset_form' value='Reset' onclick='this.form.reset();'></td></tr>";
	$Opp_Results .= "</table></form>";		
	echo $Opp_Results;
?>