<?php
/**
 * Start the session.
 * define BASE PATH
 * DB API functions file include
 * @package database
 * class file include
 */

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
require_once BASE_PATH . '/include/mpconstants.php';
require_once BASE_PATH . '/slipstream/class.ModuleEventNew.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

SessionManager::Init();
SessionManager::Validate();

$company_id = $_SESSION['USER']['COMPANYID'];
$user_id = $_SESSION['USER']['USERID'];

$form_filters = array(
	'EventID' => FILTER_SANITIZE_NUMBER_INT,
	'AccountID' => FILTER_SANITIZE_NUMBER_INT,
	'ContactID' => FILTER_SANITIZE_NUMBER_INT,
	'DealID' => FILTER_SANITIZE_NUMBER_INT,
	'Contact' => FILTER_SANITIZE_STRING,
	'Subject' => FILTER_SANITIZE_STRING,
	'Location' => FILTER_SANITIZE_STRING,
	'EventType' => FILTER_SANITIZE_NUMBER_INT,
	'StartDate' => FILTER_SANITIZE_STRING,
	'StartTime' => FILTER_SANITIZE_STRING,
	'EndTime' => FILTER_SANITIZE_STRING,
	'EndDate' => FILTER_SANITIZE_STRING,
	'Repeat' => FILTER_SANITIZE_NUMBER_INT,
	'attendee_list' => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
	'CheckPrivate' => FILTER_SANITIZE_STRING,
	'RepeatParent' => FILTER_SANITIZE_NUMBER_INT,
	'OrigStartDate' => FILTER_SANITIZE_NUMBER_INT,
	'Note' => FILTER_SANITIZE_STRING,
	'priority' => FILTER_SANITIZE_NUMBER_INT,
	'add_event_alert_hidden' => FILTER_SANITIZE_NUMBER_INT,
	'email_event_alert' => FILTER_SANITIZE_STRING,
	'standard_event_alert' => FILTER_SANITIZE_STRING,
	'send_event_immediately' => FILTER_SANITIZE_STRING,
	'delay_event_until' => FILTER_SANITIZE_STRING,
	'event_hours' => FILTER_SANITIZE_STRING,
	'event_minutes' => FILTER_SANITIZE_STRING,
	'event_periods' => FILTER_SANITIZE_STRING
);

$form_values = filter_input_array(INPUT_POST, $form_filters);
	
$form_values['alert_users'] = $_POST['alert_user'];

switch ($form_values['Repeat']) {
	case REPEAT_DAILY :
		$repeat_detail_filters = array(
			'daily_freq' => FILTER_SANITIZE_NUMBER_INT
		);
		break;
		
	case REPEAT_WEEKLY :
		$repeat_detail_filters = array(
			'weekly_freq' => FILTER_SANITIZE_NUMBER_INT,
			'weekly_day' => array('filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY)
		);
		break;
		
	case REPEAT_MONTHLY :
		$repeat_detail_filters = array(
			'monthly_freq' => FILTER_SANITIZE_NUMBER_INT,
			'monthly_type' => FILTER_SANITIZE_NUMBER_INT,
			'monthly_daynum' => FILTER_SANITIZE_NUMBER_INT,
			'monthly_pos' => FILTER_SANITIZE_NUMBER_INT,
			'monthly_pos_type' => FILTER_SANITIZE_NUMBER_INT
		);
		break;
		
	case REPEAT_YEARLY :
		$repeat_detail_filters = array(
			'yearly_freq' => FILTER_SANITIZE_NUMBER_INT,
			'yearly_type' => FILTER_SANITIZE_NUMBER_INT,
			'yearly_monthnum' => FILTER_SANITIZE_NUMBER_INT,
			'yearly_daynum' => FILTER_SANITIZE_NUMBER_INT,
			'yearly_pos' => FILTER_SANITIZE_NUMBER_INT,
			'yearly_pos_type' => FILTER_SANITIZE_NUMBER_INT,
			'yearly_pos_month' => FILTER_SANITIZE_NUMBER_INT
		);
		break;
		
	default :
		$repeat_detail_filters = array();
}

if (count($repeat_detail_filters) > 0) {
	$form_values['repeat_enddate_type'] = filter_input(INPUT_POST, 'repeat_enddate_type', FILTER_SANITIZE_NUMBER_INT);
	$form_values['repeat_enddate'] = filter_input(INPUT_POST, 'repeat_enddate', FILTER_SANITIZE_STRING);
}

$form_values['repeat_detail'] = filter_input_array(INPUT_POST, $repeat_detail_filters);

$referer_url = parse_url(filter_input(INPUT_SERVER, 'HTTP_REFERER'));
$referer_url_query = ParseQuery($referer_url['query']);

if (isset($referer_url_query['action'])) {
	$source = $referer_url_query['action'];
} else {
	$source = '';
}
$event = new ModuleEventNew($company_id, $user_id);

$no_save = ($form_values['DealID'] != 0) ? true : false;

$sync = true;

$results = $event->SaveForm($form_values, $no_save, $source, false, $sync);

echo json_encode($results);

function ParseQuery($query) {
	$query = html_entity_decode($query);
	$query = explode('&', $query);
	$output = array();
	
	foreach($query as $param) {
		list($key, $value) = explode('=', $param);
		$output[$key] = $value;
	}
	return $output;
} 
