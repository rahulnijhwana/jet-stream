<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/include/class.SessionManager.php';

SessionManager::Init();
SessionManager::Validate();

$notesPerPage = strip_tags($_POST['notes_per_page']);
$_SESSION['NotesPerPage'] = $notesPerPage;