<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/class.UserUtil.php';
require_once BASE_PATH . '/include/class.JetDateTime.php';
require_once BASE_PATH . '/include/lib.date.php';

SessionManager::Init();
SessionManager::Validate();

$snooze = $_POST['snooze'];
$seconds = $snooze * 60;

$time = time();
$time += $seconds;

$datetime = new DateTime('@' . $time, new DateTimeZone('UTC'));

$snoozeUntil = $datetime->format('Y-m-d G:i:s.000');
$alertRecipientId = strip_tags($_POST['id']);

if($alertRecipientId){
	$sql = "UPDATE AlertRecipient SET SnoozeUntil = ? WHERE AlertRecipientID = ?";
	$params[] = array(DTYPE_STRING, $snoozeUntil);
	$params[] = array(DTYPE_INT, $alertRecipientId);
	$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
	DbConnManager::GetDb('mpower')->Exec($sql);
}
