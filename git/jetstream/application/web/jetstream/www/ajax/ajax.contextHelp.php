<?php
$msg = '
<div class="context_help">
	<h2 class="help_header">Note Alerts</h2>
	<p><span class="emphasize">Note Alerts</span> are used to notify other users of something important in Jetstream.</p>
	<p>By adding a <span class="emphasize">Note Alert</span> to a Note, recipients will be presented with an Alert on their screen
	immediately, or via Email if specified.</p>
	<p>Click the <span class="emphasize">Add Alert</span> button to add a <span class="emphasize">Note Alert</span></p>
</div>
';

echo $msg;