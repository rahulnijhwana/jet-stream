<?php
session_start();
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

include_once (BASE_PATH . '/slipstream/class.Section.php');
require_once BASE_PATH . '/slipstream/class.AccountMap.php';
require_once BASE_PATH . '/slipstream/class.ContactMap.php';
require_once BASE_PATH . '/slipstream/class.Util.php';
include_once('../administrator/include/settings.php');
include_once('../administrator/data/clear_cache.php');

if (isset($_GET['action']) && $_GET['action'] == 'saveContactSection') {
	
	$status = new Status();
	$objSection = new Section();
	$contactSectionName = $_GET['contactSectionName'];
	$objUtil = new Util();
	if (!$objUtil->isAlphaNumeric($contactSectionName, array(' ', '-', '.', '/', '(', ')', '[', ']', '{', '}', ',', '?', '!'))) $status->addError('Invalid charcter in Section Name');
	
	$existingSec = $objSection->getContactSectionByCompanyId($_GET['companyId']);
	foreach ($existingSec as $sec) {
		if ($sec['ContactSectionName'] == $contactSectionName) $status->addError('Sorry cannot have more than one section with same name.');
	}
	if ($status->status == 'OK') {
		$status = $objSection->saveSection($_GET['companyId'], NULL, $contactSectionName);
		$maxSecId = $objSection->getMaxSecIdBySecName($_GET['companyId'], NULL, $contactSectionName);
	}
	
	/* Clear the jetcache directory. */
	clear_jet_cache($ajax_cache_path);
	
	echo json_encode(array('STATUS' => $status->getStatus(), 'NEWSECTIONID' => $maxSecId[0]['SectionID'], 'NEWSECTIONNAME' => $contactSectionName));
} elseif (isset($_GET['action']) && $_GET['action'] == 'editContactSection') {
	$status = new Status();
	$editedSectionIds = array();
	$editedSectionNames = array();
	$sectionSet = false;
	$objUtil = new Util();
	$editedSectionId = 'editSection';
	$objSection = new Section();
	$sections = $objSection->getContactSectionByCompanyId($_GET['companyId']);
	foreach ($sections as $sec) {
		if ($sec['ContactSectionName'] != 'DefaultSection') {
			if (isset($_GET[$editedSectionId . $sec['SectionID']])) {
				$sectionSet = true;
				if (!$objUtil->isAlphaNumeric($_GET['editSectionName' . $sec['SectionID']], array(' ', '-', '.', '/', '(', ')', '[', ']', '{', '}', ',', '?', '!'))) {
					if ($status->status == 'OK') {
						$status->addError('Invalid charcter in Section Name');
					}
				}
				if ($status->status == 'OK') {
					$editedSectionIds[] = $sec['SectionID'];
					$editedSectionNames[] = $_GET['editSectionName' . $sec['SectionID']];
					$status = $objSection->updateContactSectionById($_GET['editSectionName' . $sec['SectionID']], $sec['SectionID']);
				}
			}
		}
	}
	if (!$sectionSet) $status->addError("Please select a section to edit.");
	
	/* Clear the jetcache directory. */
	clear_jet_cache($ajax_cache_path);
	
	echo json_encode(array('STATUS' => $status->getStatus(), 'EDITEDSECTIONSID' => $editedSectionIds, 'EDITEDSECTIONSNAME' => $editedSectionNames));
} elseif (isset($_GET['action']) && $_GET['action'] == 'deleteContactSection') {
	$status = new Status();
	$deletedSectionIds = array();
	$deletedSectionNames = array();
	$sectionSet = false;
	$deletedSectionId = 'deleteSection';
	$deletedSectionName = 'deleteSectionName';
	$objSection = new Section();
	$sections = $objSection->getContactSectionByCompanyId($_GET['companyId']);
	foreach ($sections as $sec) {
		if ($sec['ContactSectionName'] != 'DefaultSection') {
			if (isset($_GET[$deletedSectionId . $sec['SectionID']])) {
				$sectionSet = true;
				$deletedSectionIds[] = $sec['SectionID'];
				$deletedSectionNames[] = $_GET[$deletedSectionName . $sec['SectionID']];
				$contactMap = new ContactMap();
				$mapIds = $contactMap->getMapIdBySectionId($sec['SectionID']);
				foreach ($mapIds as $map) {
					$contactMap = new ContactMap();
					$contactMap->updateContactMapSectionId($map['ContactMapID']);
				}
				$status = $objSection->deleteSectionById($sec['SectionID']);
			}
		}
	}
	if (!$sectionSet) $status->addError("Please select a section to delete.");
	
	/* Clear the jetcache directory. */
	clear_jet_cache($ajax_cache_path);
	
	echo json_encode(array('STATUS' => $status->getStatus(), 'DELETEDSECTIONSID' => $deletedSectionIds, 'DELETEDSECTIONSNAME' => $deletedSectionNames));
}

if (isset($_POST['action']) && $_POST['action'] == 'saveContactSectionLayout') {
	echo "<pre>";
	print_r($_POST);
	echo "</pre>";exit;
	$unAssignedData = $_POST['unAssignedData'];
	$unAssignedData = explode('|', $unAssignedData);
	
	$layout = $_POST['dataLayout'];
	$layout = explode('|', $layout);
	
	for($i = 0; $i < count($layout); $i++) {
		$layout[$i] = explode(':', $layout[$i]);
	}
	$objSection = new Section();
	$contactSections = $objSection->getContactSectionByCompanyId($_POST['companyId']);
	$sectionData = array();
	
	for($i = 0; $i < count($layout); $i++) {
		if ($layout[$i][1] != '') {
			$fieldData = explode('_', $layout[$i][1]);
			
			$mapId = $fieldData[count($fieldData) - 1];
			
			$sectionData = explode('_', $layout[$i][0]);
			$sectionName = $sectionData[0];
			$fieldNo = $sectionData[1];
			$position = (round($fieldNo / 2));
			$align = $fieldNo % 2;
			
			foreach ($contactSections as $section) {
				if ($sectionName == str_replace(' ', '', $section['ContactSectionName'])) {
					$sectionId = $section['SectionID'];
				}
			}
			$objContactMap = new ContactMap();
			$result = $objContactMap->updateFieldPositionByContactMapId($align, $position, $sectionId, $mapId, 1, false);
		}
	}
	for($i = 0; $i < count($unAssignedData); $i++) {
		$objContactMap = new ContactMap();
		$unAssignedData[$i] = explode('_', $unAssignedData[$i]);
		if ($unAssignedData[$i][0] != '') {
			$objContactMap = new ContactMap();
			$contactMapId = $unAssignedData[$i][count($unAssignedData[$i]) - 1];
			$result = $objContactMap->updateFieldPositionByContactMapId(NULL, NULL, NULL, $contactMapId, 0, true);
		}
	}
	
	/* Clear the jetcache directory. */
	clear_jet_cache($ajax_cache_path);
	
	echo json_encode($contactSections);
}
if (isset($_POST['action']) && $_POST['action'] == 'saveContactTabOrder') {
	$splitOrder = $_POST['tabOrder'];
	$splitOrder = split('=', $splitOrder);
	$mapId = trim($splitOrder[0]);
	$tabOrder = trim($splitOrder[1]);
	$objContactMap = new ContactMap();
	$order = $objContactMap->updateContactFieldsTabOrderById($mapId, $tabOrder);
	
	/* Clear the jetcache directory. */
	clear_jet_cache($ajax_cache_path);
	
	echo json_encode($order);
}

?>
