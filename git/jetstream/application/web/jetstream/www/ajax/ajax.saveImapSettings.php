<?php
sleep(2);

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
require_once BASE_PATH . '/slipstream/class.ImapSettings.php';

$id = strip_tags($_POST['settings_id']);
$username = strip_tags($_POST['imap_username']);
$pass = strip_tags($_POST['imap_pass']);

if(ImapSettings::save($id, $username, $pass)){
	echo 'OK';
}