<?php
/**
* @package Ajax
*/
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');
//require_once(BASE_PATH . '/include/class.ReportingTree.php');
require_once(BASE_PATH . '/include/mpconstants.php');
require_once(BASE_PATH . '/slipstream/class.Note.php');


SessionManager::Init();
SessionManager::Validate();

$contact_id = (int) $_POST['ContactID'];
$update_set = $_POST['Update'];
$email_address = $_POST['Email'];

$breakdown = explode( '=', $update_set) ; 
$set = $breakdown[0]; 

if (isset($contact_id) && !empty($update_set) && isset($email_address)) {	
	// update contact table with the new email address 
	$sql = "UPDATE Contact SET $set= ?  WHERE ContactID = ? AND CompanyID = ?";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $email_address), array(DTYPE_INT, $contact_id), array(DTYPE_INT, $_SESSION['USER']['COMPANYID']));	
	
	DbConnManager::GetDb('mpower')->Exec($sql);	
	
	// Create Notes
	// if the contact is the sender then create a note for the contact immedately.
	// if the contact is receiver, check if the sender is a salesperson then create a note for the contact.
	$sql =  "SELECT EmailAddresses.EmailContentID, EmailAddresses.EmailAddressType, emailcontent.ReceivedDate FROM emailcontent, EmailAddresses 
	WHERE emailcontent.CompanyID = ? AND EmailAddresses.EmailcontentID = emailcontent.EmailcontentID AND EmailAddresses.EmailAddress = ?";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['USER']['COMPANYID']), array(DTYPE_STRING, $email_address));
	//echo '-->'.$sql."\n\n";
	$email_addresses = DbConnManager::GetDb('mpower')->GetSqlResult($sql);
	
	if (!empty($email_addresses)) {
		//foreach ($email_addresses as $email) {			
		 while($email = mssql_fetch_assoc($email_addresses)) {
			$email_content_id = $email['EmailContentID'];			
			if ($email['EmailAddressType'] == 'From') {
				addNote(0, 0, $contact_id, $email_content_id, $email['ReceivedDate']);
			} else {						
				addNote($_SESSION['USER']['USERID'], 0, $contact_id, $email_content_id, $email['ReceivedDate']);				
			}
		}
	}
	
	// Update contactid of the email address table	
	if (!empty($email_address)) {
		$sql = 'UPDATE EmailAddresses SET ContactID = ? FROM emailcontent 
				WHERE emailcontent.CompanyID = ? AND EmailAddresses.EmailcontentID = emailcontent.EmailcontentID AND EmailAddresses.EmailAddress = ?';				
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $contact_id), array(DTYPE_INT, $_SESSION['USER']['COMPANYID']), array(DTYPE_STRING, $email_address));
		//echo '-->'.$sql."\n\n";
		$data = DbConnManager::GetDb('mpower')->Exec($sql);
	}
}


function addNote($person_id, $private_note, $contact_id, $email_content_id, $received_date) {		
	$objNote = new Note();
	$objNote->noteCreator = $person_id;
	$objNote->noteCreationDate = $received_date;
	$objNote->noteSubject = 'Email';
	$objNote->noteText = 'Email';
	$objNote->noteObjectType = NOTETYPE_CONTACT;
	$objNote->noteObject = $contact_id;
	$objNote->noteSplType = NOTETYPE_EMAIL;					
	$objNote->noteEmailContentID = $email_content_id;	
	$objNote->notePrivate = $private_note;
										
	$result = $objNote->createNote();		
}