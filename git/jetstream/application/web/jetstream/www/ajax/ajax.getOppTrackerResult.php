<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

SessionManager::Init();

	$Opp_Results = "<form method='post' id='frmResult' name='frmResult'>";
	$Opp_Results .= "<table width='400px' border='1px' id='result_table'><tr height='40px'><th colspan=5>Result Management</th></tr><tr height='40px'><th align='center'>Result Name</th><th align='center'>Status</th><th align='center'>Showing Order</th><th align='center'>Edit</th><th align='center'>Delete</th></tr>";
					
	$sql = "SELECT ResultID, ResultName, status, SortingOrder FROM ot_Results WHERE InUse=1 and CompanyID = ? order by SortingOrder";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_SESSION['company_id']));	
	$result_list = DbConnManager::GetDb('mpower')->GetSqlResult($sql) ;
	$counter = 0;
	while (	$result = mssql_fetch_assoc($result_list)) {		
		//$Opp_Results .= "<tr id='result_".$result['ResultID']."' height='40px'><td align='center'>".$result['ResultName']."</td><td align='center'>".$result['status']."</td><td align='center'>".$result['SortingOrder']."</td><td align='center'><a href=\"javascript: editResult(".$result['ResultID'].",'".$result['ResultName']."',".$result['status'].",".$result['SortingOrder'].")\">Edit</a></td><td align='center'><a href='javascript: deleteResult(".$result['ResultID'].")'>Delete</a></td></tr>";
		if($result['status'] === 2) $status='Won';
		else $status='Lost';
		$Opp_Results .= "<tr id='result_".$result['ResultID']."' height='40px'><td align='center'>".$result['ResultName']."</td><td align='center'>".$status."</td><td align='center'>".$result['SortingOrder']."</td><td align='center'><a href=\"javascript: editResult(".$result['ResultID'].",'". htmlentities($result['ResultName'])."',".$result['status'].",".$result['SortingOrder'].")\">Edit</a></td><td align='center'><a href='javascript: deleteResult(".$result['ResultID'].")'>Delete</a></td></tr>";
		$counter++;
	}
	$status_select ="<select name='result_status'><option value='2'>Won</option><option value='1'>Lost</option></select>";
	$order_select = "<select name='result_order'>";
	for($i=$counter+1; $i>0;$i--){
		$order_select .= "<option value=$i>$i</option>";
	}
	$order_select .= "</select>";
	
	//$Opp_Results .= "<tr id='new_result'><td align='center'><input type='text' name='result_name' value='' /></td><td align='center'><input type='text' name='result_status' value='' /></td><td align='center'><input type='text' name='result_order' value='' /></td><td align='center'><input type='button' name='add_Result' value='Add' onclick='addResult();'></td><td align='center'><input type='button' name='reset_form' value='Reset' onclick='this.form.reset();'></td></tr>";
	$Opp_Results .= "<tr id='new_result'><td align='center'><input type='text' name='result_name' value='' /></td><td align='center'>".$status_select."</td><td align='center'>".$order_select."</td><td align='center'><input type='button' name='add_Result' value='Add' onclick='addResult();'></td><td align='center'><input type='button' name='reset_form' value='Reset' onclick='this.form.reset();'></td></tr>";
	$Opp_Results .= "</table></form>";		
	echo $Opp_Results;
?>