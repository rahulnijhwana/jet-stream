<?php
/**
 * Start the session.
 * define BASE PATH
 * DB API functions file include
 * @package database
 * class file include
 */

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
require_once BASE_PATH . '/include/mpconstants.php';
require_once BASE_PATH . '/slipstream/class.ModuleEvents.php';
require_once BASE_PATH . '/slipstream/class.Event.php';
require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

SessionManager::Init();
SessionManager::$check_eula = FALSE;
SessionManager::Validate();


/**
 * Create an object of module event.
 */
$obj_event = new Event();
$permitted_user = $obj_event->GetPermittedUserList($_SESSION['login_id']);

$module = new ModuleEvents();

$module->permitted_users[$_SESSION['login_id']] = $_SESSION['USER']['FIRSTNAME'].' '.$_SESSION['USER']['LASTNAME'];

foreach($permitted_user as $p){

	$module->permitted_users[$p['OwnerPersonID']] = "{$p['FirstName']} {$p['LastName']}";
}

$module->sub_action = $_GET['sub_action'];
if(isset($_GET['oppId'])) {
	if($_GET['oppId'] > 0) {
		$module->eventOppId = $_GET['oppId'];
	}
	$module->eventOppType = 1;
}

$module->Init();

if(empty($module->event_info)) $module->event_info['user']['PersonID'] = $_SESSION['login_id'];

$smarty->assign_by_ref('module', $module);
$output = $smarty->fetch('form_event_new.tpl');
echo $output;
