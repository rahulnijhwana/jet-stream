<?php



if (!defined("BASE_PATH")) {
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
}
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/class.JetDateTime.php';
require_once BASE_PATH . '/include/class.GoogleConnect.php';
require_once BASE_PATH . '/include/class.GoogleCalendar.php';
require_once BASE_PATH . '/include/class.GoogleTasks.php';

require_once BASE_PATH . '/slipstream/class.EventTypeLookup.php';

SessionManager::Init();
SessionManager::Validate();

//$sql = "SELECT * FROM Event where googleStatus = NULL";
//$sql = "SELECT TOP 1* FROM Event where googleStatus = NULL order by EventID desc";
//echo $sql = "SELECT TOP 1* FROM Event where googleStatus = NULL and EventID = '{$_SESSION['USER']['EVENT_ID']}'";

/*Start JET-37 For Edit */
$eventID = $_SESSION['USER']['EVENT_ID'];
unset($_SESSION['USER']['EVENT_ID']);
$sql = "SELECT TOP 1* FROM Event where ( googleStatus = NULL OR googleStatus = '0' ) and EventID = '$eventID'";
/*End JET-37 For Edit */
$results = DbConnManager::GetDb('mpower')->Exec($sql);

$count = count($results);

if($count > 0){
foreach($results as $result){
		
		$record = new RecEvent();
		
		
		$sql = 'SELECT * FROM Event WHERE EventID = ?';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $result['EventID']));
		$recordset = DbConnManager::GetDb('mpower')->Execute($sql, 'RecEvent');
		$record = $recordset[0];
		$record->Initialize();
		
		if('' == $company_id){
			$company_id = $_SESSION['USER']['COMPANYID'];
			}
		if('' == $user_id){
			$user_id = $_SESSION['USER']['USERID'];
			}
			
		if ( '' == $company_id ) {
			$isUseGoogleSync = false;
		} else {
			$isUseGoogleSync = GoogleConnect::isUseGoogleSync($company_id);
		}
		$event = EventTypeLookup::GetRecord($company_id, $result['EventTypeID']);
	
		$record->CompanyID = $company_id;
		$record->EventID = $result['EventID'];
		
		$record->EventTypeID = $result['EventTypeID'];
		$record->StartDateUTC = $result['StartDateUTC'];
		$record->EndDateUTC = $result['EndDateUTC'];
		$record->Subject = $result['Subject'];
		$record->Location = $result['Location'];
		//$record->Private = $result['Private'];
		$record->RepeatType = $result['RepeatType'];
		$record->RepeatInterval = $result['RepeatInterval'];
		if ($event['Type'] == 'EVENT'){
			$record->ContactID = $result['ContactID'];
		}
		if ($event['Type'] == 'TASK'){
			$record->AccountID = $result['AccountID'];
			$record->PriorityID = $result['PriorityID'];
			}
		//$record->googleEventId = $result['googleEventId'];
		//$record->googleId = $result['googleId'];
		//$record->googleStatus = 1;
		$data = $record->GetDirtyData();
		
			
		$attendee_sql = 'SELECT * FROM EventAttendee WHERE EventID = ?';
		$attendee_sql = SqlBuilder()->LoadSql($attendee_sql)->BuildSql(array(DTYPE_INT, $result['EventID']));
		$attendee_set = DbConnManager::GetDb('mpower')->Execute($attendee_sql, 'RecEventAttendee', 'PersonID');
		if('' == $creator){
		if($attendee_set[$user_id]->Creator == 1){
			$creator = $user_id;
			}
		}
		
		if ( $isUseGoogleSync ) {
				if ($event['Type'] == 'TASK') {
					if ( GoogleConnect::hasToken($creator) ) {
						$googleConnect = new GoogleConnect($creator, false);
						$googleTask = new GoogleTasks($googleConnect->tasklists, $googleConnect->tasks, $user_id, false);
						$googleTask->insertGoogleTask($record);
					}
				} else if ($event['Type'] == 'EVENT') {
					if ( GoogleConnect::hasToken($creator) ) {
						$googleConnect = new GoogleConnect($creator, false);
						$googleCalendar = new GoogleCalendar($googleConnect->calendar, $user_id, false);
						
						$googleCalendar->insertGoogleEvent($record);
					}
				}
			}

	
	
	
}	
}
?>
