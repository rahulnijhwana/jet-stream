<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.Dashboard.php';
require_once BASE_PATH . '/slipstream/class.ModuleAdvanceSearch.php';
require_once BASE_PATH . '/slipstream/class.ModuleSaveAdvanceSearch.php';

SessionManager::Init();
SessionManager::Validate();

if($_GET['action'] == 'loadSearch'){

	$advanced_search = new ModuleAdvanceSearch();
	$advanced_search->saveSearchID = ( !empty ( $_POST['Id']) ) ? $_POST['Id'] : 0 ;
	$advanced_search->saveSearchName = ( !empty ( $_POST['Name']) ) ? $_POST['Name'] : '' ;

	$advanced_search->Init();
	$smarty->assign_by_ref('module', $advanced_search);
	
	echo $smarty->fetch('module_advance_search.tpl');
}
else if($_GET['action'] == 'loadList'){

	$savedList = new ModuleSaveAdvanceSearch; 
	$savedList->Init();
	$smarty->assign_by_ref('module', $savedList);

	echo $smarty->fetch('module_save_advance_search.tpl');

}
else if($_GET['action'] == 'Save'){ // Check if it is an update or an Insert 

	if(isset($_POST['Name']) &&  empty($_POST['Saved_Id'])){
		$check = ModuleSaveAdvanceSearch::checkDuplicate ($_POST['Name']); 

		if($check['computed'] != 0){
			echo "error";
			exit;
		}
	} 
	if(isset($_POST['Advance'])){
		$saved = array();
		foreach($_POST as $posted_field => $posted_value) {			
			if ((substr($posted_field, 0, 8) == 'contact_') || (substr($posted_field, 0, 8) == 'company_')) {
				if ( $posted_value != '' ) {
					$fields = explode("_", $posted_field ); 
					if ( is_array($posted_value)) {
						$saved[$fields[0]][$fields[1]] = $posted_value;
					}
					else if ( count($fields) == 2 ){
						$saved[$fields[0]][$fields[1]] = filter_input(INPUT_POST, $posted_field);
					}
					elseif ( count($fields) == 3) {
						$saved[$fields[0]][$fields[2]][$fields[1]] = filter_input(INPUT_POST, $posted_field);
					}
				}
			}			
		}
		//save options 
		$saved['options']['RecordsPerPage'] = (!empty($_POST['RecordsPerPage'])) ? $_POST['RecordsPerPage']: '' ; 
		$saved['options']['assignto'] = (!empty($_POST['assignto'])) ? $_POST['assignto'] : '';
		$saved['options']['SelectInactive'] = (!empty($_POST['SelectInactive'])) ? $_POST['SelectInactive'] : '' ;
		$saved['options']['AssignedSearch'] = (!empty($_POST['AssignedSearch']  )) ? $_POST['AssignedSearch'] : '';
		$saved['options']['page_no'] = (!empty($_POST['page_no']  )) ? $_POST['page_no'] : '';
		$saved['options']['query_type'] = (!empty($_POST['query_type']  )) ? $_POST['query_type'] : 3;
		$saved['options']['sort_type'] = (!empty($_POST['sort_type']  )) ? $_POST['sort_type'] : '';
		$saved['options']['selecInactive'] = (!empty($_POST['selectInative']  )) ? $_POST['selectInactive'] : '';
			
		//save new or update
		if ( isset($_POST['Saved_Id']) && $_POST['Saved_Id'] != 0 ) {
			$out = ModuleSaveAdvanceSearch::UpdateSavedSearchList( array ('options' => $saved['options'] , 'account' => $saved['company'], 'contact' => $saved['contact'], 'name' => $_POST['Name'], 'id' => $_POST['Saved_Id'] )); 
		}
		else { 
			$out = ModuleSaveAdvanceSearch::InsertSearchList( array ('options' => $saved['options'] , 'account' => $saved['company'], 'contact' => $saved['contact'], 'name' => $_POST['Name']));
		}
	}
	$savedList = new ModuleSaveAdvanceSearch; 
	$savedList->Init();
	
	$smarty->assign_by_ref('module', $savedList);
	echo $smarty->fetch('module_save_advance_search.tpl');
}
else if ( $_GET['action'] == 'deleteList'  ) {
	
	if ( !empty($_POST['Id']) && $_POST['Id'] != 0 ) {
		$out = ModuleSaveAdvanceSearch:: deleteSavedSearch(   ( $_POST['Id'])  ); 
	}

	$savedList = new ModuleSaveAdvanceSearch; 
	$savedList->Init();
	
	$smarty->assign_by_ref('module', $savedList);
	echo $smarty->fetch('module_save_advance_search.tpl');
}