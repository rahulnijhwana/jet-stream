<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/class.Dashboard.php';
require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/class.ReportingTree.php';
require_once BASE_PATH . '/slipstream/class.ModuleSavedSearch.php';


SessionManager::Init();
SessionManager::Validate();

$obj_dashboard = new Dashboard();

if(isset($_POST['SaveSearch']) && ($_POST['SaveSearch'] == 1)) 
{
	$str_saved_search = '';
	
	foreach($_POST AS $key=>$val)
	{
		if(is_array($val))
		{
			foreach($val AS $innerkey=>$innerval)
			{
				if($str_saved_search == '')
				{
					$str_saved_search .= $key.'_'.$innerkey.'='.$innerval;
				}
				else
				{
					$str_saved_search .= '&'.$key.'_'.$innerkey.'='.$innerval;
				}
			}
		}
		else
		{
			if($str_saved_search == '')
			{
				$str_saved_search .= $key.'='.$val;
			}
			else
			{
				$str_saved_search .= '&'.$key.'='.$val;
			}
		}
	}
	$obj_dashboard->saved_search_name = $_POST['SavedSearchName'];
	$obj_dashboard->saved_search_text = $str_saved_search;
	$obj_dashboard->saved_search_type = $_POST['SavedSearchType'];
	$output = $obj_dashboard->SaveSearch();	
	
	if($output)
	{
		$obj_saved_searches = new ModuleSavedSearch($_POST['SavedSearchType']);
		echo $obj_saved_searches->DrawSavedSearches();
	}
	else
	{
		echo 'failure';
	}
}
?>