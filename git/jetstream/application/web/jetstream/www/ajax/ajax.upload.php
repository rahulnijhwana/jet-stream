<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../'));
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/slipstream/class.MailTemplate.php';

SessionManager::Init();
SessionManager::Validate();

$uploaddir ='../emailtemplate/';
$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
if(filesize($_FILES['userfile']['tmp_name']) > 153600) {
	echo json_encode(array('STATUS'=>'FALSE','MSG'=>'Unable to upload, maximum file size reached.'));
	exit;
}
$img_info=@getimagesize($_FILES['userfile']['tmp_name']);
if($img_info){
	
	$ext=GetFileExtension(basename($_FILES['userfile']['name']));
	$file_name=md5($_SESSION['login_id']). strtotime("now") . $ext;//strtotime("now") to bypass browser cache
	$uploadfile = $uploaddir . $file_name;
	/*$objMail=new MailTemplate();	
	$template_data=$objMail->GetTemplate($_SESSION['login_id']);	
	preg_match('/([_%\w-]+\.(?:jpe?g|JPE?G|gif|GIF|png|PNG|bmp|BMP))/m',$template_data,$files);
	if(isset($files) && is_array($files) ){
		foreach($files as $f)
			@unlink('../emailtemplate/'.$f);
	}*/
    	
	//echo json_encode(array('STATUS'=>'FALSE','MSG'=>));
	
	$full_path= GetServerPath() . 'emailtemplate/' . $file_name;	
	if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
		echo json_encode(array('STATUS'=>'OK','FILENAME'=>$full_path));
	}
	else {
		echo json_encode(array('STATUS'=>'FALSE','MSG'=>'Error occured while uploading file.'));
	}
}
else {
	echo json_encode(array('STATUS'=>'FALSE','MSG'=>'Invalid file.'));
}
function GetFileExtension($filename){
  return substr($filename, strrpos($filename, '.'));
}
function GetServerPath() {
		$protocol = 'http';
		if (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') {
			$protocol = 'https';
		}
		$host = $_SERVER['HTTP_HOST'];
		$baseUrl = $protocol . '://' . $host;
		if (substr($baseUrl, -1)=='/') {
			$baseUrl = substr($baseUrl, 0, strlen($baseUrl)-1);
		}		
		$current_path=str_replace('ajax/ajax.upload.php','',$_SERVER["SCRIPT_NAME"]);
		$current_path=$baseUrl . $current_path;
		return $current_path;		
	}	

?>
