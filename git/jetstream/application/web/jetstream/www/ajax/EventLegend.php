<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/slipstream/class.ModuleEventNew.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

SessionManager::Init();
SessionManager::Validate();

$module = new ModuleEventNew();
$event_types = $module->GetEventTypes();

$smarty->assign_by_ref('event_types', $event_types);
$smarty->display('snippet_event_legend.tpl');

