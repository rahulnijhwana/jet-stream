<?php

/**
 * define BASE PATH
 * include DB API class
 * include fieldMap class file
 * include company class file
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/class.CompanyContact.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
//require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.conf';

SessionManager::Init();
SessionManager::Validate();

$record = new CompanyContact();

$result['record_id'] = $record->ProcessForm();
$result['title'] = $record->GetName();
$result['view'] = $record->DrawView();

if ($_GET['redirect']) {
	header("Location: ../slipstream.php?action=company?companyId=" . $result['record_id']);
} else {
	echo json_encode($result);
}
