<?php

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/class.CompanyContact.php';
require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.conf';
require_once BASE_PATH . '/include/class.SessionManager.php';

SessionManager::Init();
SessionManager::Validate();

$record = new CompanyContact();

$record->Create('company');
$smarty->assign('EDITDATA', $record->DrawView(true));
$smarty->display('contact_edit.tpl');
