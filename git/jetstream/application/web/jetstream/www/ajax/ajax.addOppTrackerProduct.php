<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

SessionManager::Init();
$filter_args = array(
	'price' => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
	//'price' => FILTER_SANITIZE_NUMBER_STRING,
	'name' => FILTER_SANITIZE_STRING,
	'order' => FILTER_SANITIZE_NUMBER_INT
);

$inputs = filter_input_array(INPUT_GET, $filter_args);

$name = $inputs['name'];
$price = $inputs['price'];
$order = $inputs['order'];

	//$sql = "insert into ot_Products(CompanyId, ProductName,Price, SortingOrder, IsEnabled ) values(".$_SESSION['company_id'].",'".$_REQUEST['name']."', ".$_REQUEST['price'].", ".$_REQUEST['order'].",1)";	
	$sql = "insert into ot_Products(CompanyId, ProductName,Price, SortingOrder, IsEnabled ) values(".$_SESSION['company_id'].",'".$name."', ".$price.", ".$order.",1)";	
	DbConnManager::GetDb('mpower')->Exec($sql);
	
?>