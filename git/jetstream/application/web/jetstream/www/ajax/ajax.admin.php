<?php

if (!defined('BASE_PATH')) {
	define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
}

require_once BASE_PATH . '/slipstream/smarty/configs/slipstream.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

SessionManager::Init();

$companyID = $_SESSION['company_id'];


if (!empty($_POST['action']) && $_POST['action'] == 'AdminTags'){
	
	$sql = 'SELECT JetTagsID, n, labelName from JetTags where CompanyID= ? and DeletedOn is NULL order by labelname ASC' ; 
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $companyID));
	$results = DbConnManager::GetDb('mpower')->exec($sql);

	$i = 1 ; 
	
	echo "<table  class=\"event\">";
	echo ' <tr>
				<th> &nbsp;  </th>
				<th align=center> Tags  </th>
				<th align=center> Action  </th>
			</tr>';

	foreach ($results as $result){
		echo "<tr>";
		echo "<td> $i </td>";
		echo "<td> {$result['labelName']}  </td>";
		echo "<td> (<a href=\"javascript:JetFileAction('AdminEditTags', '{$result['labelName']}', '{$result['JetTagsID']}' )\">Edit</a>)  &nbsp;  (<a href=\"javascript:JetFileAction('AdminDeleteTags', '{$result['JetTagsID']}', '' )\">Delete</a>)  </td>";
		echo "</tr>" ; 
		$i++;
	}
	echo "</table>";

}

else if (!empty($_POST['action']) && $_POST['action'] == 'AdminAddTags'){
	if ( empty($_POST['param1'])){
		echo "ERROR - blank label";
		exit;
	}
	else $label = $_POST['param1']; 
	
	$n = getLastValue($companyID)  ;
	
	$sql = 'INSERT INTO JetTags VALUES ( ? , ? , ? , NULL ) ' ; 
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyID), array(DTYPE_STRING, $label), array(DTYPE_INT, $n));
	$results = DbConnManager::GetDb('mpower')->exec($sql);

	echo "Success";
	
}

else if (!empty($_POST['action']) && $_POST['action'] == 'AdminDeleteTags'){

	if ( empty($_POST['param1'])){
		echo "ERROR - Missing ID";
		exit;
	}
	else $id = $_POST['param1']; 
	
	$sql = 'UPDATE JetTags SET DeletedOn = GETDATE() WHERE JetTagsID = ?' ; 
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id));
	$results = DbConnManager::GetDb('mpower')->exec($sql);
	
	echo "Success";
}
 
else if (!empty($_POST['action']) && $_POST['action'] == 'AdminEditTags'){

	if ( empty($_POST['param1']) || empty($_POST['param2'])){
		echo "ERROR - not enough params ";
		exit;
	}
	else{
		$label = $_POST['param1'];
		$ID = $_POST['param2'];
	}
	
	$sql = 'UPDATE JetTags SET labelName =? WHERE JetTagsID = ?' ; 
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $label), array(DTYPE_INT, $ID));
	$results = DbConnManager::GetDb('mpower')->exec($sql);

	echo "Success";
	
}



/*************** SUPPORTING FUNCTIONS **************************/

function getMaxValue($companyID){

	$sql = 'SELECT MAX(n) as n from JetTags where CompanyID= ? and DeletedOn is NULL ' ; 
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $companyID));
	$results = DbConnManager::GetDb('mpower')->GetOne($sql);
		
	return $results['n'] + 1 ;  

}

function getLastValue($companyID){

	$sql = 'SELECT TOP 1 l.n + 1 AS start, MIN(fr.n) - 1  AS stop FROM JetTags AS l
    LEFT OUTER JOIN JetTags AS r ON l.n = r.n - 1 AND l.companyID = r.companyID
    LEFT OUTER JOIN JetTags AS fr ON l.n < fr.n  AND l.companyID = fr.companyID
	WHERE r.n IS NULL AND fr.n IS NOT NULL AND l.DeletedOn IS NULL AND r.DeletedOn IS NULL AND fr.DeletedOn IS NULL AND l.CompanyID = ?
	GROUP BY l.n, r.n' ; 
	
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $companyID));
	$results = DbConnManager::GetDb('mpower')->GetOne($sql);
		
	if ( $results['start'] == '' ){
		return getMaxValue($companyID) ;
	}  
	else return $results['start'];  

}


?>