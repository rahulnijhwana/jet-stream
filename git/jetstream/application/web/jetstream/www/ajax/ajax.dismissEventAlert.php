<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';

SessionManager::Init();
SessionManager::Validate();

$alert_EventAttendee_id = $_POST['id'];


if($alert_EventAttendee_id){
	$sql = "UPDATE EventAttendee SET Active = 0 WHERE EventAttendeeID = ? ";
	$params = array(DTYPE_INT, $alert_EventAttendee_id);
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql($params);
	DbConnManager::GetDb('mpower')->Exec($sql);
}

