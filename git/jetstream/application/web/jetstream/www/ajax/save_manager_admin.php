<?php
/**
* @package Ajax
*/
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');
require_once(BASE_PATH . '/include/class.ReportingTree.php');

SessionManager::Init();
SessionManager::Validate();

$person = $_POST['person'];
$user = isset($_POST['user']) ? $_POST['user'] : array();
$company_set_goal = isset($_POST['CompanySetGoal']) ? $_POST['CompanySetGoal'] : array();
$FM_threshold = isset($_POST['FMThreshold']) ? $_POST['FMThreshold'] : array();
$IP_threshold = isset($_POST['IPThreshold']) ? $_POST['IPThreshold'] : array();
$DP_threshold = isset($_POST['DPThreshold']) ? $_POST['DPThreshold'] : array();
$close_Threshold = isset($_POST['CloseThreshold']) ? $_POST['CloseThreshold']: array();
$color = isset($_POST['Color']) ?  $_POST['Color']: array();
$companyid = $_SESSION['mpower_companyid'];
$year = date('Y');

$seasonality = isset($_POST['Sesonality']) ? $_POST['Sesonality'] : array();
$seasonality_check = isset($_POST['SeasonalityCheck']) ? $_POST['SeasonalityCheck'] : array();

$seasonality = array_chunk($seasonality, 13);
$status = true;

$year = (int) $_POST['Year'];

if (isset($person)) {
	
	$seasonality_key = 0;
	foreach($person as $key=>$personid) {
		$deleted = 1;	
		$date_deleted = date('Y-m-d', strtotime("now"));
		if(in_array($personid, $user)) { $deleted = 0; $date_deleted = NULL;}	
		
		$company_goal = (int) str_replace(',', '', str_replace('$', '', $company_set_goal[$key]));		
		
		$goal_sql = "IF EXISTS (SELECT * FROM Goal WHERE PersonID = ? and Year = ?)
						UPDATE Goal
						   SET CompanySetGoal = ?
						 WHERE PersonID = ? and Year = ?
					ELSE
						INSERT INTO Goal
							   (PersonID, Year, CompanySetGoal)
						 VALUES (?, ?, ?)";
					 
		$goal_sql = SqlBuilder()->LoadSql($goal_sql)->BuildSql(array(DTYPE_INT, $personid), array(DTYPE_INT, $year),
																array(DTYPE_INT, $company_goal), array(DTYPE_INT, $personid), 
																array(DTYPE_INT, $year), array(DTYPE_INT, $personid),
																array(DTYPE_INT, $year), array(DTYPE_INT, $company_goal));			


		$is_seasonality = in_array($personid, $seasonality_check) ? 1 : 0;	
		if ($is_seasonality) {
		
			$person_seasonality = $seasonality[$seasonality_key];
			$period = 1;
			
			if(count($person_seasonality)) {
				
				foreach ($person_seasonality as $each_seasonality) {
					$each_seasonality = (int) $each_seasonality;
					$seasonality_sql = "IF EXISTS (SELECT * FROM Seasonality WHERE PersonID = ? AND Year = ? AND Period = ?)
								UPDATE Seasonality
								   SET Seasonality = ?   
								 WHERE PersonID = ? AND Year = ? AND Period = ?
							ELSE
								INSERT INTO Seasonality
									   (PersonID
									   ,Year					  
									   ,Period
									   ,Seasonality)
								 VALUES (?, ?, ?, ?)";	
					
					$seasonality_sql = SqlBuilder()->LoadSql($seasonality_sql)->BuildSql(array(DTYPE_INT, $personid), array(DTYPE_INT, $year), array(DTYPE_INT, $period),
																array(DTYPE_INT, $each_seasonality), array(DTYPE_INT, $personid), 
																array(DTYPE_INT, $year), array(DTYPE_INT, $period),
																array(DTYPE_INT, $personid), array(DTYPE_INT, $year),
																array(DTYPE_INT, $period), array(DTYPE_INT, $each_seasonality));
					DbConnManager::GetDb('mpower')->Exec($seasonality_sql);
					$period++;	
				}
			}
			$seasonality_key++;
		}
		
		
		$seasonality_enabled_sql = "IF EXISTS (SELECT * FROM Seasonality 
					WHERE PersonID = ? AND Year = ? AND Period IS NULL AND Seasonality IS NULL)
					UPDATE Seasonality
					   SET SeasonalityEnabled = ?   
					 WHERE PersonID = ? AND Year = ? AND Period IS NULL AND Seasonality IS NULL
				ELSE
					INSERT INTO Seasonality
						   (PersonID
						   ,Year							  
						   ,SeasonalityEnabled)
					 VALUES (?, ?, ?)";	
		
		$seasonality_enabled_sql = SqlBuilder()->LoadSql($seasonality_enabled_sql)->BuildSql(
													array(DTYPE_INT, $personid), array(DTYPE_INT, $year), 
													array(DTYPE_INT, $is_seasonality), array(DTYPE_INT, $personid), 
													array(DTYPE_INT, $year), array(DTYPE_INT, $personid), array(DTYPE_INT, $year),
													array(DTYPE_INT, $is_seasonality));
										
		DbConnManager::GetDb('mpower')->Exec($seasonality_enabled_sql);


		$user_sql = "UPDATE people
					   SET FMThreshold = ?,
						   IPThreshold = ?,
						   DPThreshold = ?,
						   CloseThreshold = ?,
						   Deleted = ?,
						   Color = ?						   
					 WHERE PersonID = ?";				 
		$user_sql = SqlBuilder()->LoadSql($user_sql)->BuildSql(array(DTYPE_INT, $FM_threshold[$key]), array(DTYPE_INT, $IP_threshold[$key]), 
																array(DTYPE_INT, $DP_threshold[$key]), array(DTYPE_INT, $close_Threshold[$key]), 
																array(DTYPE_INT, $deleted), array(DTYPE_STRING, $color[$key]), array(DTYPE_INT, $personid));
																
		if(DbConnManager::GetDb('mpower')->Exec($goal_sql) && DbConnManager::GetDb('mpower')->Exec($user_sql)) {		
			//echo 'OK';
		} else {
			//echo 'Error in saving';
			$status = false;
			break;
		}
	}
}
