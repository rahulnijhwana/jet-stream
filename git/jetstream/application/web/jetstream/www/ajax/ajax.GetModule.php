<?php

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
require_once '../../slipstream/smarty/configs/slipstream.php';
require_once '../../include/class.SessionManager.php';
require_once '../../include/class.ReportingTree.php';

chdir (BASE_PATH . '/slipstream');

SessionManager::Init();
SessionManager::Validate();

//TODO This name needs to be scrubbed on arrival - this could be dangerous
include BASE_PATH . '/slipstream/class.' . $_GET['module'] . '.php';
$form = (isset($_GET['form']) && $_GET['form'] == 'true') ? true : false;
$output = '';

if (class_exists($_GET['module'])) {
	$module_name = $_GET['module'];

	$company_id = $_SESSION['company_obj']['CompanyID'];
	$user_id = $_SESSION['USER']['USERID'];
	$limit_access = (isset($_SESSION['limit_access'])) ? $_SESSION['limit_access'] : FALSE;
	$module = new $module_name($company_id, $user_id);

	switch ($module_name) {
		case 'ModuleCompanyContactNew' :
			$action = $_GET['action'];
			$prepop = FALSE;
			if ($action == 'add') {
				$rec_type = $_GET['type'];
				if ($rec_type == 'contact' && isset($_GET['accountId'])) {
					$rec_id = $_GET['accountId'];
					$prepop = TRUE;
				} else {
					$rec_id = -1;
				}
			} else {
				if (isset($_GET['contactId'])) {
					$rec_type = 'contact';
					$rec_id = $_GET['contactId'];
				} elseif (isset($_GET['accountId'])) {
					$rec_type = 'account';
					$rec_id = $_GET['accountId'];
				}
			}
			
			if ( isset($_GET['referer']) && $_GET['referer'] == 'email_hold'){
				$def_value = array ('email' => $_GET['email'], 'first_name' => $_GET['first_name'], 'last_name' => $_GET['last_name'] ); 
			}
			else $def_value= array() ; 
			$module->Build($rec_type, $rec_id, $action, $limit_access, $prepop, $def_value);
			break;
		case 'ModuleEventNew' :
			$repeat_date = false;
			if (isset($_GET['eventid'])) {
				$rec_id = filter_input(INPUT_GET, 'eventid');
				$rec_type = 'event';
				$action = 'edit';
				if ($rec_id < 0) {
					$action = $_GET['action'];
				}
			} elseif (isset($_GET['companyid'])) {
				$rec_id = filter_input(INPUT_GET, 'companyid');
				$rec_type = 'company';
				$action = 'add';
			} elseif (isset($_GET['contactid'])) {
				$rec_id = filter_input(INPUT_GET, 'contactid');
				$rec_type = 'contact';
				$action = 'add';
			} elseif (isset($_GET['oppid'])) {
				$rec_id = filter_input(INPUT_GET, 'dealid');
				$rec_type = 'opp';
				$action = 'add';
			}

			if (isset($_GET['rep'])) {
				// Ref is passed when they select opening an instance of a recurring
				$rec_type = 'event';
				// echo base64_decode(filter_input(INPUT_GET, 'rep')) . '<br>';
				$repeating_instance = json_decode(base64_decode(filter_input(INPUT_GET, 'rep')), true);
				$rec_id = $repeating_instance['i'];
				$repeat_date = TrucatedDateToUnix($repeating_instance['d']);
				$action = 'edit';
			}
			
			$form_vals = array();
			foreach(array_keys($_POST) as $post_var) {
				$form_vals[$post_var] = filter_input(INPUT_POST, $post_var);
				if ($form_vals[$post_var] == 'null') {
					unset ($form_vals[$post_var]);
				}
			}
			if (isset($_GET['StartDate'])) {
				$form_vals['StartDate'] = filter_input(INPUT_GET, 'StartDate');
			}
			if (isset($_GET['EndDate'])) {
				$form_vals['EndDate'] = filter_input(INPUT_GET, 'EndDate');
			}
			if (isset($_GET['BlankTime'])) {
				$form_vals['BlankTime'] = filter_input(INPUT_GET, 'BlankTime');
			}
			if (isset($_GET['Subject'])) {
				$form_vals['Subject'] = filter_input(INPUT_GET, 'Subject');
			}

			$module->Build($rec_type, $rec_id, $action, true, $repeat_date, $form_vals);
			break;
			
		//adding opportunity
		case 'ModuleOpportunityNew' :
			//$repeat_date = false;
			if (isset($_GET['opportunityid'])) {
				$rec_id = filter_input(INPUT_GET, 'opportunityid');
				$rec_type = 'opportunity';
				$action = 'edit';
				if ($rec_id < 0) {
					$action = $_GET['action'];
				}
			} elseif (isset($_GET['contactid'])) {
				$rec_id = filter_input(INPUT_GET, 'contactid');
				$rec_type = 'contact';
				$action = 'add';
			} 
			/*
			if (isset($_GET['rep'])) {
				// Ref is passed when they select opening an instance of a recurring
				$rec_type = 'opportunity';
				// echo base64_decode(filter_input(INPUT_GET, 'rep')) . '<br>';
				$repeating_instance = json_decode(base64_decode(filter_input(INPUT_GET, 'rep')), true);
				$rec_id = $repeating_instance['i'];
				$repeat_date = TrucatedDateToUnix($repeating_instance['d']);
				$action = 'edit';
			}
			*/
			$form_vals = array();
			foreach(array_keys($_POST) as $post_var) {
				$form_vals[$post_var] = filter_input(INPUT_POST, $post_var);
				if ($form_vals[$post_var] == 'null') {
					unset ($form_vals[$post_var]);
				}
			}
			
			
			$module->Build($rec_type, $rec_id, $action, true, '', $form_vals);
			break;
			
			
			
			
			
		default : 
			$module = new $module_name();
			$module->Init();
			$module->Draw();
	}

	if (isset($module->no_display) && $module->no_display) continue;
			
	$smarty->assign_by_ref('module', $module);
	if ($form) {
		$output = $smarty->fetch('shell_form.tpl');
	} elseif ($module->sidebar) {
		$output = $smarty->fetch('shell_sidebar.tpl');
	} else {
		$output = $smarty->fetch('shell_module.tpl');
	}

	echo $output;
}

?>
