<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/class.AccountUtil.php';
require_once BASE_PATH . '/include/class.ContactUtil.php';
require_once BASE_PATH . '/include/class.EventUtil.php';
require_once BASE_PATH . '/include/class.UserUtil.php';
require_once BASE_PATH . '/include/class.JetDateTime.php';
require_once BASE_PATH . '/include/lib.date.php';
require_once BASE_PATH . '/slipstream/class.Note.php';


SessionManager::Init();
SessionManager::Validate();


function formatAlert($alert){
    
    $date = $alert['CreatedDate'];
    $date = str_replace(':000','',$date);
    $date = str_replace('.000','',$date);

	$timestamp = strtotime($date);
	$timestamp -= UTC_OFFSET;
	$created = '@' . $timestamp;

	$created = new DateTime($created, new DateTimeZone('America/Chicago'));
	$output = '<div class="show">';
	$output .= '<table class="note_alert_table">';
	$output .= '<tr>';
	$output .= '<td class="subtitle">Subject</td>';
	$output .= '<td>' . $alert['Subject'] . '</td>';
	$output .= '<td class="subtitle">Contact</td>';
	$output .= '<td>' . $alert['Contact'] . '</td>';
	$output .= '</tr>';
	$output .= '<tr>';
	$output .= '<td class="subtitle">Created By</td>';
	$output .= '<td>' . $alert['FullName'] . '</td>';
	$output .= '<td class="subtitle">Company</td>';
	$output .= '<td>' . $alert['Account'] . '</td>';
	$output .= '</tr>';
	$output .= '<tr>';
	$output .= '<td class="subtitle">Date</td>';
	$output .= '<td>' . $created->format('F j Y, g:i a') . '</td>';
	$output .= '<td class="subtitle">Note Type</td>';
	$output .= '<td>' . $alert['Type'] . '</td>';
	$output .= '</tr>';
	$output .= '</table>';
	$output .= '<div id="note_text">';
	$output .= nl2br($alert['NoteText']);
	$output .= '</div>';
	$output .= '<input type="hidden" name="alert_recipient_id" id="alert_recipient_id" value="' . $alert['AlertRecipientID'] . '" />';
	$output .= '<input type="hidden" name="alert_recipient_id" id="previous_alert_recipient_id" value="' . $alert['previous'] . '" />';
	$output .= '<input type="hidden" name="alert_recipient_id" id="next_alert_recipient_id" value="' . $alert['next'] . '" />';
	$output .= '</div>';

	return $output;
}

$time = time();
$datetime = new DateTime('@' . $time, new DateTimeZone('UTC'));
$now = $datetime->format('Y-m-d G:i:s.000');

$sql = "
	SELECT * FROM Alert
	JOIN AlertRecipient ON Alert.AlertID = AlertRecipient.AlertID
	WHERE
		PersonID = ? AND
		Active = 1 AND
		(DelayUntil <= ? OR DelayUntil IS NULL) AND
		(SnoozeUntil <= ? OR SnoozeUntil IS NULL)";

/* code for test */
//$sql = "SELECT top 3 * FROM Alert JOIN AlertRecipient ON Alert.AlertID = AlertRecipient.AlertID WHERE PersonID = ? AND Active = 0";

$params[] = array(DTYPE_INT, $_SESSION['USER']['USERID']);
$params[] = array(DTYPE_STRING, $now);
$params[] = array(DTYPE_STRING, $now);

$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
$results = DbConnManager::GetDb('mpower')->Exec($sql);

$output = '';
$count = count($results);

$json = '';
if($count > 0){

	$alerts = array();
	
	for($i=0;$i<$count;$i++){

		$noteSql = "SELECT Subject, NoteText, ObjectType, ObjectReferer, NoteSpecialType FROM Notes WHERE NoteID = ?";
			
		$noteParams = array(DTYPE_INT, $results[$i]['RecordID']);
		$noteSql = SqlBuilder()->LoadSql($noteSql)->BuildSql($noteParams);
		$noteResult = DbConnManager::GetDb('mpower')->GetOne($noteSql);
		$fullName = UserUtil::getFullName($results[$i]['CreatedBy']);
		
		$results[$i]['NoteText'] = $noteResult['NoteText'];
		$results[$i]['Subject'] = $noteResult['Subject'];
		$results[$i]['FullName'] = $fullName;
		
		$note = new Note();
		$results[$i]['Type'] = $note->getNoteType($noteResult['ObjectType'], $noteResult['NoteSpecialType']);
		
		$referrer = $noteResult['ObjectReferer'];
		
		switch($noteResult['ObjectType']){
			
			case NOTETYPE_COMPANY:
				$results[$i]['Contact'] = '';
				$results[$i]['Account'] = AccountUtil::getAccountName($referrer);
				break;
			case NOTETYPE_CONTACT:
				$contact = ContactUtil::getContact($referrer);
				$results[$i]['Contact'] = $contact['Text01'] . ' ' . $contact['Text02'];
				$results[$i]['Account'] = AccountUtil::getAccountName($contact['AccountID']);
				break;
			case NOTETYPE_EVENT:
				$contact = EventUtil::getEventContact($referrer);
				$results[$i]['Contact'] = $contact['Text01'] . ' ' . $contact['Text02'];
				$results[$i]['Account'] = AccountUtil::getAccountName($contact['AccountID']);
				break;
		}
		
		$results[$i]['previous'] = $results[$i-1]['AlertRecipientID'];
		$results[$i]['next'] = $results[$i+1]['AlertRecipientID'];
		
		$alerts[] = $results[$i];
	}
	
	$output = formatAlert($alerts[0]);
	$json = json_encode(array('count' => $count, 'output' => $output));
	
}

echo $json;
