<?
session_start();
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/class.Status.php';
require_once BASE_PATH . '/slipstream/class.EventType.php';
include_once ('../administrator/include/settings.php');
include_once ('../administrator/data/clear_cache.php');

if (isset($_POST['btnSubmit']) && ($_POST['btnSubmit'] == 'Submit' || $_POST['btnSubmit'] == 'Save')) {
	$status = validateEventType();
	$objEventType = new EventType();
	if ($status->status == 'OK') { //saveEvent($eventTypeId,$eventTypeName,$eventColor,$companyId)			
		if (isset($_POST['UseEventInReport'])) {
			$eventUsedInReport = 1;
		} else {
			$eventUsedInReport = 0;
		}
		$status = $objEventType->saveEvent($_POST['EventTypeID'], trim($_POST['EventName']), trim($_POST['EventColor']), $_POST['EventType'], $eventUsedInReport, $_POST['EventCompanyId']);
	}
	if ($status->status == 'OK' && $_POST['EventTypeID'] != '') $status->setFormattedVar('UPDATE');
	
	$status = $status->getStatus();
	if ($status['STATUS'] == 'OK') {
		$status['EVENTDATA'] = $objEventType->getAllEventTypeByCompanyId($_POST['EventCompanyId']);
	}
	/* Clear the jetcache directory. */
	clear_jet_cache($ajax_cache_path);
	
	echo json_encode($status);
}
if (isset($_POST['actionType']) && $_POST['actionType'] == 'LoadToEdit') {
	$objEventType = new EventType();
	$result = $objEventType->getEventTypeInfoByEventTypeId($_POST['eventTypeId'], $_POST['EventCompanyId']);
	if (count($result) == 0)
		echo json_encode(array('STATUS' => 'FALSE'));
	else
		echo json_encode(array('STATUS' => 'OK', 'EVENTNAME' => $result[0]['EventName'], 'EVENTCOLOR' => $result[0]['EventColor'], 'EVENTID' => $result[0]['EventTypeID'], 'EVENTTYPE' => $result[0]['Type'], 'UseEventInReport' => $result[0]['UseEventInCallReport']));
}
if (isset($_POST['actionType']) && $_POST['actionType'] == 'deleteEvent') {
	$sql = 'select * from Events where EventTypeID=?'; // and (Status is NULL or )';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $_POST['eventTypeId']));
	$rs = DbConnManager::GetDb('mpower')->Exec($sql);
	if (!empty($rs))
		echo json_encode(array('STATUS' => 'FALSE', 'MSG' => 'Can not delete, some events are defined with[ ' . $_POST['eventTypeName'] . ' ]'));
	else {
		$objEventType = new EventType();
		$objEventType->deleteEventType($_POST['eventTypeId'], $_POST['EventCompanyId']);
		
		/* Clear the jetcache directory. */
		clear_jet_cache($ajax_cache_path);
		
		echo json_encode(array('STATUS' => 'OK', 'EVENTID' => $_POST['eventTypeId'], 'EVENTNAME' => stripslashes($_POST['eventTypeName'])));
	}
}

function validateEventType() {
	$objEventType = new EventType();
	$status = new Status();
	if (trim($_POST['EventName']) == '') {
		$status->addError('Please enter event type', 'EventName');
	} else if (!$objEventType->isUniqueEventType(trim($_POST['EventName']), $_POST['EventCompanyId'], $_POST['EventTypeID'])) {
		$status->addError('Event type exist', 'EventName');
	}
	if (trim($_POST['EventColor']) == '') {
		$status->addError('Please enter event color', 'EventColor');
	}
	if (trim($_POST['EventType']) == '') {
		$status->addError('Please select event type', 'EventType');
	}
	return $status;
}

?>