function toggleInfo(controlId) {
	if ($('#' + controlId).css('display') == 'none' || $('#' + controlId).css('display') == '') {
		$('#' + controlId).slideDown("fast");
		// css('display', 'block');
		$('#toggle_' + controlId).attr('src', 'images/minus.gif');
		if (!$('#' + controlId).attr('loaded')) {
			$('#' + controlId).attr('loaded', true);
			AjaxLoadDetail(controlId);
		}
	} else {
		$('#' + controlId).slideUp("fast");
		// css('display', 'none');
		$('#toggle_' + controlId).attr('src', 'images/plus.gif');
	}
}

function AjaxLoadDetail(detail_id) {
	$.post("ajax/ajax.searchdetail.php", {
		request_id :detail_id
	}, function(data, success) {
		$("#" + detail_id).html(data);
	}, "html");
}

var page = 0;
var total_pages = 0;
var search_string = '';
var search_type = '';
var loading_page = false;
var merge_contact = new Array();
var merge_account = new Array();
var callTimer = true;
var Formadvancedata = '';
var dashboardSearchType = 'basic';
var sort_order = 'ASC';
var sort_by = 'OpportunityName';

function dumpObj(obj, name, indent, depth) {
    if (depth > 20) {
           return indent + name + ": <Maximum Depth Reached>\n";
    }

    if (typeof obj == "object") {
           var child = null;
           var output = indent + name + "\n";
           indent += "\t";
           for (var item in obj)
           {
                 try {
                        child = obj[item];

                 } catch (e) {
                        child = "<Unable to Evaluate>";
                 }

                 if (typeof child == "object") {
                        output += dumpObj(child, item, indent, depth + 1);
                 } else {
                        output += indent + item + ": " + child + "\n";
                 }
           }
           return output;
    } else {
           return obj;
    }
}

function AjaxLoadPage(page) {	
	if (loading_page != true) {
		loading_page = true;

		//alert(sort_by);
		var form_info = $("#SlipstreamSearch").serializeArray();
		form_info.push(({name:'Page', value:page}));

		// alert(form_info);
		// var Formdata = 'Page=' + page + '&' + $("#SlipstreamSearch").serialize();
		
		//populate the salesperson_list to be passed back to check which sales person's opp to show
		salesperson_array = new Array();
		count = 0;
	
		$("input[id^='salesperonid']").each(function() {
			if(this.checked){
			salesperson_array[count] = this.value;
			count++;
			}
		});
		$("input[name='salesperson_list']").val(JSON.stringify(salesperson_array));
		form_info.push({name:'salesperson_list', value:JSON.stringify(salesperson_array)});
		//alert(JSON.stringify(salesperson_array));
		
		callTimer = true;
		var dashboardSearchType = 'basic';
		/*
		if($("#advanced_filter")) {
			var test = '';
			var advanced_data = Array();
			$("[name^='advfilterinput_']").each(function() {
				form_info.push({name:this.name, value:this.value});
			});
			form_info.push({name:'SortType', value:$('#SortType').val()});
			form_info.push({name:'SortAs', value:$('#SortAs').val()});
		}
		*/
		// alert(dumpObj(form_info));
		// alert('--->' + form_info);
		$('#dashboardsearch').fadeTo("fast", .1);
		$.post("ajax/opportunity_search_results.php", form_info, function(data, success) {
			$('#dashboardsearch').html(data).fadeTo("fast", 1);
			loading_page = false;
			if ($('#chkMergeRecord').attr('checked')) {
				$('.mergeClass').css( {
					display :"inline"
				});
				for (i = 0; i < merge_contact.length; i++)
					$('#chk_' + merge_contact[i]['CID']).attr('checked', true);
				for (i = 0; i < merge_account.length; i++)
					$('#chk_' + merge_account[i]['ACID']).attr('checked', true);
			} else
				$('.mergeClass').css( {
					display :"none"
				});
			// resetMerge();//uncheck Merge check box and hide Merge Button
			}, "html");
	}
}

$(document).ready( function() {
	$('#chkMergeRecord').attr('checked', false);

	if (get_cookie('SearchString') && get_cookie('SearchString').length > 0) {
		curPageId = get_cookie('PageNo');
		AjaxLoadPage(curPageId);
	}

	/*
	 * $('#SlipstreamSearch').ajaxForm({ beforeSubmit: function() {
	 * $('#searchresults').html(''); page = 0; } , dataType: 'json' , success:
	 * function(data) { $('#searchresults').html(data.output);
	 * $('#searchresults').slideDown("fast"); page = 1; search_string =
	 * $("input[name='SearchString']").attr('value'); search_type =
	 * $("select[name='SearchType']").attr('value'); } });
	 */
	/*
	 * $("#searchresults").scroll(function(){ if
	 * ($("#searchresults").attr('scrollTop') +
	 * $("#searchresults").attr('clientHeight') >
	 * $("#searchresults").attr('scrollHeight') - 150) { AjaxLoadPage(); } });
	 */
});

function toggleMergeButton(val) {
	if (val) {
		$('#btnMerge').css( {
			visibility :"visible"
		});
		$('.mergeClass').css( {
			display :"inline"
		});
	} else {
		$('#btnMerge').css( {
			visibility :"hidden"
		});
		$('.mergeClass').css( {
			display :"none"
		});

	}
}

function doMerge() {

	if (merge_contact.length == 0 && merge_account.length == 0) {
		alert('Select any two Contact or Account to merge');
		return;
	} else if (merge_contact.length > 0 && merge_account.length > 0) {
		alert('Please select records from One category only(Contact/Account)');
		return false;
	}
	if (merge_contact.length > 0)
		type = 'Contact';
	else
		type = 'Account';
	if (type == 'Contact') {
		if (validateContactMerge(merge_contact)) {
			if (confirm('Do you want to merge contacts ?')) {
				op = '';
				for (i = 0; i < merge_contact.length; i++)
					op = op + '<option value="' + merge_contact[i]['CID'] + '" accid="' + merge_contact[i]['ACID']
							+ '">' + merge_contact[i]['CNAME'] + '</option>';
				$("select#mergeList").html(op);
				left = ($(document).width() - $('#divMerge').width()) / 2;
				$('#cellMergeText').html('Select Primary contact');
				$('#mergeType').attr('value', 'Contact');
				blockUI('divMerge', '', '', left, 200);
			}
		}

	} else {
		if (validateAccountMerge(merge_account)) {

			if (confirm('Do you want to merge accounts ?')) {
				op = '';
				for (i = 0; i < merge_account.length; i++)
					op = op + '<option value="' + merge_account[i]['ACID'] + '">' + merge_account[i]['ACNAME']
							+ '</option>';
				$("select#mergeList").html(op);
				left = ($(document).width() - $('#divMerge').width()) / 2;
				$('#cellMergeText').html('Select Primary Account');
				$('#mergeType').attr('value', 'Account');
				blockUI('divMerge', '', '', left, 200);
			}
		}

	}

}

function validateContactMerge(contacts) {
	prevAccNo = -1;
	if (contacts.length != 2) {
		alert('Only 2 contact can be merged at a time' + contacts.length);
		return false;
	}
	for (i = 0; i < contacts.length; i++) {
		if (prevAccNo == -1)
			prevAccNo = contacts[i]['ACID'];
		else if (prevAccNo != contacts[i]['ACID']) {
			alert('All the Contacts must belong to same account');
			return false;
		}
	}
	return true;

}

function validateAccountMerge(accounts) {

	if (accounts.length != 2) {
		alert('Only 2 account can be merged at a time');
		return false;
	}
	return true;

}

function setAttr(el, attr) {
	if ($('#' + el).attr('checked')) {
		$('#' + el).addClass(attr);
		if (attr == 'chkContact') {
			var newContact = new Array();
			newContact['CID'] = $('#' + el).attr('cid');
			newContact['ACID'] = $('#' + el).attr('accid');
			newContact['ACNAME'] = $('#' + el).attr('accname');
			newContact['CNAME'] = $('#' + el).attr('cname');
			merge_contact[merge_contact.length] = newContact;
		} else {
			var newAccount = new Array();
			newAccount['ACID'] = $('#' + el).attr('accid');
			newAccount['ACNAME'] = $('#' + el).attr('accname');
			merge_account[merge_account.length] = newAccount;
		}
	} else {
		$('#' + el).removeClass(attr);
		tmpArr = new Array();
		if (attr == 'chkContact') {
			for (i = 0; i < merge_contact.length; i++) {
				if (merge_contact[i]['CID'] != $('#' + el).attr('cid'))
					tmpArr[tmpArr.length] = merge_contact[i];
			}
			merge_contact = tmpArr;
		} else {
			for (i = 0; i < merge_account.length; i++) {
				if (merge_account[i]['ACID'] != $('#' + el).attr('accid'))
					tmpArr[tmpArr.length] = merge_account[i];
			}
			merge_account = tmpArr;

		}
	}

}

function closeMergedList() {
	$("select#mergeList").html('');
	$("#mergeType").attr('value', '');
	unblockUI('divMerge');
}

function saveMergedList(type) {
	if ($('#mergeType').attr('value') == 'Contact') {
		secondaryContact = '';
		primaryContact = $('#mergeList option:selected').val();
		if (typeof primaryContact == 'undefined') {
			alert('Please select a contact');
			return;
		}
		accountId = $('#mergeList option:selected').attr('accid');
		for (i = 0; i < document.getElementById('mergeList').options.length; i++) {
			if (!document.getElementById('mergeList').options[i].selected) {
				if (secondaryContact == '')
					secondaryContact = document.getElementById('mergeList').options[i].value;
				else
					secondaryContact = secondaryContact + ',' + document.getElementById('mergeList').options[i].value;
			}

		}
		$.post('./ajax/ajax.Merge.php', {
			mergeType :'Contact',
			primaryContact :primaryContact,
			secondaryContact :secondaryContact,
			accountId :accountId
		}, mergeCallback);

	} else {
		secondaryAccount = '';
		primaryAccount = $('#mergeList option:selected').val();
		if (typeof primaryAccount == 'undefined') {
			alert('Please select an Account');
			return;
		}
		for (i = 0; i < document.getElementById('mergeList').options.length; i++) {

			if (!document.getElementById('mergeList').options[i].selected) {
				if (secondaryAccount == '')
					secondaryAccount = document.getElementById('mergeList').options[i].value;
				else
					secondaryAccount = secondaryAccount + ',' + document.getElementById('mergeList').options[i].value;
			}

		}
		$.post('./ajax/ajax.Merge.php', {
			mergeType :'Account',
			primaryAccount :primaryAccount,
			secondaryAccount :secondaryAccount
		}, mergeCallback);

	}
}

function mergeCallback(result) {
	result = ParseStatus(result);
	if (result.STATUS == 'OK') {
		merge_contact = new Array();
		merge_account = new Array();
		closeMergedList();
		curPageId = get_cookie('PageNo');
		if (curPageId != null)
			AjaxLoadPage(curPageId);
	}
}

function resetMerge() {
	$('.chkContact').attr('checked', false);
	$('.chkAccount').attr('checked', false);
	$('#chkMergeRecord').attr('checked', false);
	$('#btnMerge').css( {
		visibility :"hidden"
	});
	$("#mergeType").attr('value', '');
	$("select#mergeList").html('');
	merge_contact = new Array();
	merge_account = new Array();
}

function get_cookie(cookieName) {
	var name = cookieName + '=';
	var dc = document.cookie;

	if (dc.length > 0) {
		var begin = dc.indexOf(name);
		if (begin != -1) {
			begin += name.length;
			var end = dc.indexOf(';', begin);
			if (end == -1)
				end = dc.length;
			return unescape(dc.substring(begin, end));
		}
	}

	return null;
}

/****************** Advance Search **************************/
function showSearch(search_type) {
	if(search_type == 'advance') {
		document.cookie = 'DashboardSearchType=advance';
		window.location.href = 'slipstream.php?action=advancedashboard';
	}
	else	{
		document.cookie = 'DashboardSearchType=basic';
		window.location.href = 'slipstream.php?action=dashboard';
	}
}

if (typeof (filterListArray) != 'undefined') {
	/*if (typeof (noteReferer) != 'undefined') {
		createFilterOpt(1, 'D2', true);
		createFilterOpt(2, '', false);

		if ($('#NText1').length > 0) {
			$('#NText1').val(noteReferer);
			setNoteType(noteReferer);
		}
	} else {
		alert('hello');*/
		createFilterOpt(1, '', false);
	//}
}

function callDashBoardSearch(sort_by_in) {
	if(sort_order == 'DESC') sort_order='ASC';
	else sort_order='DESC';
	sort_by = sort_by_in;
	//alert(sort_by);
	setTimeout("AjaxLoadPage('1')", 500);
/*
	if (callTimer) {
		callTimer = false;
		setTimeout("AjaxLoadPage('1')", 500);
	}
*/	
}

function createFilterOpt(indx, selIndex, chooseFilter) {
	filterSelArray = Array();
	var showSelectBox = false;
	
	for ( var t = 1; t <= $('#filterIndex').val(); t++) {
		filterSelArray[filterSelArray.length] = $('#filterBox' + t).val();
	}
	var selectBoxstr = '<select name="filterOpt[]" class="clsFilterBox" id="filterBox' + indx
			+ '"  onChange="chooseFilterOpt(' + indx + ')">';
	selectBoxstr += '<option value="">Choose Filter Field</option>';

	for ( var filterListID in filterListArray) {
		selectBoxstr += '<optgroup label="------' + filterListID + '------">';
		var filterIDIndex = 0;
		var filterSelectedAccSectionText = '';
		var filterSelectedConSectionText = '';
		
		for ( var filterID in filterListArray[filterListID]) {
			if (filterListID == 'Company Fields') {
				var selOpt = findObjInArray('CM_' + filterID, filterSelArray, selIndex);
				if (selOpt) {
					continue;
				}
				var filterSectionOption = filterListArray[filterListID][filterID].split('||');
				var filterSectionText = filterSectionOption[0];
				var filterOptionText = filterSectionOption[1];
				
				if((filterSectionText != '') && (filterSectionText != filterSelectedAccSectionText)) {
					filterSelectedAccSectionText = filterSectionText;
					selectBoxstr += '<optgroup label="--' + filterSectionText + '--">';
				}
				
				if (('CM' + filterIDIndex == selIndex) || ('CM_' + filterID == selIndex)) {
					selectBoxstr += '<option value="CM_' + filterID + '" selected >'
							+ filterOptionText + '</option>';
					showSelectBox = true;
				} else {
					selectBoxstr += '<option value="CM_' + filterID + '" >' + filterOptionText
							+ '</option>';
					showSelectBox = true;
				}
			} else if (filterListID == 'Contact Fields') {
					var selOpt = findObjInArray('CN_' + filterID, filterSelArray, selIndex);
					if (selOpt) {
						continue;
					}
					
					var filterSectionOption = filterListArray[filterListID][filterID].split('||');
					var filterSectionText = filterSectionOption[0];
					var filterOptionText = filterSectionOption[1];
					
					if((filterSectionText != '') && (filterSectionText != filterSelectedConSectionText)) {
						filterSelectedConSectionText = filterSectionText;
						selectBoxstr += '<optgroup label="--' + filterSectionText + '--">';
					}
				
					if (('CN' + filterIDIndex == selIndex) || ('CN_' + filterID == selIndex)) {
						selectBoxstr += '<option value="CN_' + filterID + '" selected >'
								+ filterOptionText + '</option>';
						showSelectBox = true;
					} else {
						selectBoxstr += '<option value="CN_' + filterID + '" >'
								+ filterOptionText + '</option>';
						showSelectBox = true;
					}
			} else {
				var selOpt = findObjInArray(filterID, filterSelArray, selIndex);
				if (selOpt) {
					continue;
				}

				if (filterID == selIndex) {
					selectBoxstr += '<option value="' + filterID + '" selected >'
							+ filterListArray[filterListID][filterID] + '</option>';
					showSelectBox = true;
				} else {
					selectBoxstr += '<option value="' + filterID + '" >' + filterListArray[filterListID][filterID]
							+ '</option>';
					showSelectBox = true;
				}
			}
			filterIDIndex++;
		}
		selectBoxstr += '</optgroup>';
	}
	selectBoxstr += '</select>';

	if (!showSelectBox) {
		selectBoxstr = '';
	}
	if ($('#filterSelect' + indx).length) {
		$('#filterSelect' + indx).html(selectBoxstr);
	} else {
		var nextIndx = indx;
		var result = '<div id="filter' + nextIndx + '" class="filter-list">';
		result += '<div id="filterSelect' + nextIndx + '" class="filter-list">' + selectBoxstr + '</div>';
		result += '<div id="filterText' + nextIndx + '" class="filter-list"></div><br /></div>';

		return result;
	}

	if (chooseFilter) {
		chooseFilterOpt(indx);
	}
}

function findObjInArray(val, arrayname, selIndex) {
	if ((val != selIndex)) {
		for ( var elem in arrayname) {
			if (arrayname[elem] == val) {
				return true;
			}
		}
	}
	return false;
}

function chooseFilterOpt(indx) {
	for ( var t = 1; t <= $('#filterIndex').val(); t++) {
		if ($('#filterBox' + t).length) {
			createFilterOpt(t, $('#filterBox' + t).val(), false);
		}
	}

	var curSelectBoxVal = $('#filterBox' + indx).val();
	var strDivContent = showFilterTextBox(curSelectBoxVal, indx);
	$('#filterText' + indx).html(strDivContent);

	/*
	 * if(curSelectBoxVal.lastIndexOf('Date') != -1) {
	 * if($('#NText'+indx).length) { $("#NText"+indx).datepicker({ showOn:
	 * "both", buttonImage: "./images/calendar.gif", buttonImageOnly: true }); } }
	 */

	if ($('#NText' + indx).length) {
		$('#NText' + indx).focus();
	} else if ($('#STARTDATE' + indx).length) {
		$('#STARTDATE' + indx).focus();
	}

	if ($('#StartDate' + indx).length) {
		$("#StartDate" + indx).datepicker( {
			showOn :"both",
			buttonImage :"./images/calendar.gif",
			buttonImageOnly :true
		});
	}

	if ($('#EndDate' + indx).length) {
		$("#EndDate" + indx).datepicker( {
			showOn :"both",
			buttonImage :"./images/calendar.gif",
			buttonImageOnly :true
		});
	}

	if (indx == $('#filterIndex').val()) {
		var nextIndx = indx + 1;
		$('#filterIndex').val(nextIndx);

		var result = createFilterOpt(nextIndx, '', false);

		$('#filter' + indx).append(result);
	}
}

function showFilterTextBox(selType, indx) {
	var strContent = '';
	var curSelectBoxVal = $('#filterBox' + indx).val();

	if (typeof (filterListValArray[selType]) != 'undefined') {
		strContent = eval("'" + filterListValArray[selType] + "'");
	}
	return strContent;
}

function buildSearchData(page) {
	var totIndex = $('#filterIndex').val();
	var Formdata = '';

	for ( var i = 1; i < totIndex; i++) {
		if ($('#filterBox' + i).length) {
			if (Formdata == '') {
				Formdata += 'filterOpt[]=' + $('#filterBox' + i).val();
			} else {
				Formdata += '&filterOpt[]=' + $('#filterBox' + i).val();
			}
		}

		if ($('#NText' + i).length) {
			if (Formdata == '') {
				Formdata += 'NText' + i + '=' + $('#NText' + i).val();
			} else {
				Formdata += '&NText' + i + '=' + $('#NText' + i).val();
			}
		} else if ($('#StartDate' + i).length) {
			if (Formdata == '') {
				Formdata += 'StartDate' + i + '=' + $('#StartDate' + i).val();
			} else {
				Formdata += '&StartDate' + i + '=' + $('#StartDate' + i).val();
			}
			Formdata += '&EndDate' + i + '=' + $('#EndDate' + i).val();
		}
	}
	
	Formdata += '&SortType=' + $('#SortType').val();
	Formdata += '&SortAs=' + $('#SortAs').val();

	return Formdata;
}

function resetSearchdata() {
	/*$('#SlipstreamSearch').each {
		$(this).val('');
	}
	$("#SlipstreamSearch").reset();
	if($("#filterIndex").val() > 0)	{
		//$("#advance_filter").html('');
		createFilterOpt(1, '', false);
	}*/
}

function saveDashboardSearch() {
	if($('#dashboardsearch').html() != '') {
		blockUI('saveSearchDiv', '30%', '', '35%', '10%');
	}
	else {
		blockUI('saveSearchConfrm', '30%', '', '35%', '10%');
	}	
}

function hideConfrmDiv() {
	unblockUI('saveSearchConfrm');
}

function cancelSaveDashboardSearch() {
	unblockUI('saveSearchDiv');
}

function AjaxSavePage(page) {
		if($('#SaveSearchName').val() == '') {
			$('#savedSearchError').html('Please enter the Search Name.').fadeIn('slow');
			setTimeout("$('#savedSearchError').fadeOut('slow');", 2000);
		}
		else {			
			var Formdata = 'Page=' + page + '&SaveSearch=1&' + $("#SlipstreamSearch").serialize();
			var SavedSearchName = $('#SaveSearchName').val();
			callTimer = true;
			var SavedSearchType = 0;
			
			if(document.getElementById("advanceSearch")) {
				Formadvancedata = buildSearchData(page);
				var SavedSearchType = 1;
				
				if(Formadvancedata != '')	{
					Formdata += '&' + Formadvancedata;
				}
				Formdata += '&filterIndex=' +$('#filterIndex').val();	 
			}
			Formdata += '&SavedSearchType='+SavedSearchType+'&SavedSearchName='+SavedSearchName;
			
			//$('#dashboardsearch').fadeTo("fast", .1);
			$.post("ajax/ajax.saveSearch.php", Formdata, function(data, success) {
				if(data != 'failure')	{
					$('#saved_search_list').html(data);
				}
				unblockUI('saveSearchDiv');
			});
		}
}

function LoadSavedSearch(saveddata) {
		var Formdata = saveddata;
		$('#dashboardsearch').fadeTo("fast", .1);
		createFilterOpt(1, '', false);
		
		$.post("ajax/ajax.loadSearch.php", Formdata, function(data, success) {
			
				result = ParseStatus(data);
				
				var filterOptArr = Array();
				var filterOptTextArr = Array();
				filterOptIndx = 1;
				for(var cntfilter in result.result) {
					if(result.result[cntfilter].name.search('filterOpt') != -1) {
						var splitResultArr = result.result[cntfilter].name.split('_');	
						createFilterOpt(filterOptIndx, result.result[cntfilter].nameval, true);
						filterOptIndx++;
					}
				}
				for(var resid in result.result) {
					if(result.result[resid].name.search('filterOpt') == -1) {
						if($('#'+result.result[resid].name).length > 0) {
							if($('#'+result.result[resid].name).attr("type") == 'checkbox') {
								if(result.result[resid].nameval == 1)	{
									$('#'+result.result[resid].name).attr("checked", true);
								}
								else {
									$('#'+result.result[resid].name).attr("checked", false);
								}
							}
							else	{
								$('#'+result.result[resid].name).val(result.result[resid].nameval);
							}
						}
					}
				}
		});
}


var prompt;
var xmlHttp;

function stateChanged_1() 
	{ 
		//alert("here1: "+xmlHttp.readyState);
		if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
		{ 
			prompt=xmlHttp.responseText;
		} 
	}

function CreateReport(report_type, weighted_in, contact, groupname, groupid) {
			//var prompt = '';
			//alert(report_type);
			var action;
			var weighted;
			action = 'jello'; 
			switch(report_type){
			case '1': action = 'export';
					weighted = 'false';
			break;
			case '2': action = 'OppCalendarRPT';
					weighted = 'false';
			break;
			case '3': action = 'OppCalendarRPTOpps';
					weighted = 'false';
			break;
			case '4': 		
			action = 'OppCalendarRPTProds';		
			weighted = 'false';
			break;
			case '5': action = 'export';
					weighted = 'true';
			break;
			case '6': action = 'OppCalendarRPT';
					weighted = 'true';
			break;
			case '7': action = 'OppCalendarRPTOpps';
					weighted = 'true';
			break;
			case '8': action = 'OppCalendarRPTProds';
					weighted = 'true';
			break;
			case '9': action = 'ClosedCustRPT';
					weighted = 'false';
			break;
			case '10': action = 'ClosedOppRPT';
					weighted = 'false';
			break;
			case '11': action = 'ClosedProdRPT';
					weighted = 'false';
			break;
			
			}
			 
			var dat = new Date();
			var curday = dat.getDate();
			var curmon = dat.getMonth()+1;
			var curyear = dat.getFullYear();
			var endyear = (''+curyear).substring(2);
			var startyear = (''+(curyear-1)).substring(2);

			var url="ajax/ajax.getPrompt.php";
			BusyOn();
			$.get(url, {action: action}, function(prompt) {				
			
				var buttons = {'Create Report':true, Cancel:false};
				type = 'close';
				$.prompt(prompt, {
				buttons: buttons,
				submit: 
					function(v, m, f) {
					if (v) {
				//	alert(f.salesperonid);
					SaveGroupField(action, f.StartDate, f.EndDate, weighted, groupname, f.salesperonid);				
				//	Sample(action, f.StartDate, f.EndDate, weighted, groupname, f.salesperonid);				
					
					}
				}
				});
				if(action == 'ClosedCustRPT' || action =='ClosedOppRPT' || action == 'ClosedProdRPT'){
				$("input[name='StartDate']").datepicker({
					showOn :"both",
					buttonImage : "/jetstream/images/calendar.gif",
					buttonImageOnly :true,
					dateFormat: 'm/d/y'
				});
				
				$("input[name='EndDate']").datepicker({
					showOn :"both",
					buttonImage : "/jetstream/images/calendar.gif",
					buttonImageOnly :true,
					dateFormat: 'm/d/y'
				});
				document.getElementsByName('StartDate')[0].value = curmon+'/'+curday+'/'+startyear;
				document.getElementsByName('EndDate')[0].value = curmon+'/'+curday+'/'+endyear;
				}
				BusyOff();
			});			
}

function Sample(action, start, end, weighted, groupname, salespersonIds) {
	 var width = 400;
    var height = 200;
    var left = parseInt((screen.availWidth/2) - (width/2));
    var top = parseInt((screen.availHeight/2) - (height/2));
    var windowFeatures = "width=" + width + ",height=" + height + ",status=no,resizable=no,titlebar=no,toolbar=no,menubar=no,location=no,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;
	
	var params = '?action='+action+'&salespersonIds='+salespersonIds+'&weighted='+weighted+'&start='+start+'&end='+end;
    w = window.open("ajax/sample.php"+params,'GeneratingReport',windowFeatures);
	setTimeout ( "w.close()", 25000 );

}

var w;
function SaveGroupField(action, start, end, weighted, groupname, salespersonIds) {
	//alert(action);
	//alert(document.getElementById("months").value); 
	//if(document.getElementById("weighted").checked) weighted = true;
	//else weighted = false;
	//alert(weighted);
	/*
	if((typeof months !='undefined') ) {
	//alert(months);
		if(isNaN(parseInt(months))){
		alert("Please type in a number for the number of month");
		BusyOff();
		return;
		}
		if(months > 120){
		alert("Please type in a number less than 120 to have a report that is within 10 years");
		BusyOff();
		return;
		}
	}
	*/
	//var params = '?action='+action+'&months='+months+'&salespersonIds='+salespersonIds+'&weighted='+weighted;
	 var width = 400;
    var height = 200;
    var left = parseInt((screen.availWidth/2) - (width/2));
    var top = parseInt((screen.availHeight/2) - (height/2));
    var windowFeatures = "width=" + width + ",height=" + height + ",status=yes,resizable=no,toolbar=no,menubar=no,location=no,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;
	
	var params = '?action='+action+'&salespersonIds='+salespersonIds+'&weighted='+weighted+'&start='+start+'&end='+end;
	//var w = window.open("ajax/ajax.OppSalesProcess.php"+params,null,"height=200,width=400,status=yes,toolbar=no,menubar=no,location=no");
    w = window.open("ajax/ajax.OppSalesProcess.php"+params,'GeneratingReport',windowFeatures);
	//setTimeout("writeToWindow()", 50);
/*
		BusyOn();
		$.post("ajax/ajax.OppSalesProcess.php", 
		{action: action,
		months: months,
		salespersonIds: salespersonIds,
		weighted: weighted}, 
		function(excel) {
			var w = window.open();			
			w.document.open();			
			w.document.write(excel); 
			w.document.close();		
			//w.close();	
			BusyOff();
		});
*/

	/*
		var submitForm = getNewSubmitForm();
		createNewFormElement(submitForm, "action", action);
		//createNewFormElement(submitForm, "months", document.getElementById("months").value);
		createNewFormElement(submitForm, "months", months);
		createNewFormElement(submitForm, "salespersonIds", salespersonIds);
		createNewFormElement(submitForm, "groupname", groupname);
		createNewFormElement(submitForm, "weighted", weighted);	
		submitForm.setAttribute("action", "ajax/ajax.OppSalesProcess.php");
		submitForm.submit();
	*/	
}
function writeToWindow() {
    var newContent = "<html>";
    newContent += "<body><h1>Please wait.</h1>";
    newContent += "</body></html>";
    w.document.write(newContent);
    //w.document.close();
}

function getNewSubmitForm(){
 var submitForm = document.createElement("FORM");
 document.body.appendChild(submitForm);
 submitForm.method = "POST";
 return submitForm;
}

function createNewFormElement(inputForm, elementName, elementValue){

 var newElement = document.createElement("input");
 newElement.setAttribute("name", elementName);
 newElement.setAttribute("type", "hidden");
 inputForm.appendChild(newElement);
 newElement.value = elementValue;
 return newElement;
}

	function GetXmlHttpObject(handler)
	{ 
	var objXmlHttp=null

	if (navigator.userAgent.indexOf("Opera")>=0)
	{
	alert("This example doesn't work in Opera") 
	return 
	}
	if (navigator.userAgent.indexOf("MSIE")>=0)
	{ 
	var strName="Msxml2.XMLHTTP"
	if (navigator.appVersion.indexOf("MSIE 5.5")>=0)
	{
	strName="Microsoft.XMLHTTP"
	} 
	try
	{ 
	objXmlHttp=new ActiveXObject(strName)
	objXmlHttp.onreadystatechange=handler 
	return objXmlHttp
	} 
	catch(e)
	{ 
	alert("Error. Scripting for ActiveX might be disabled") 
	return 
	} 
	} 
	if (navigator.userAgent.indexOf("Mozilla")>=0)
	{
	objXmlHttp=new XMLHttpRequest()
	objXmlHttp.onload=handler
	objXmlHttp.onerror=handler 
	return objXmlHttp
	}
	}