$(function(){
	
	$(".show").bind('click', function(){
        blockUI('loginForm', '400px', '140px', '37%', '20%');
        var parts = $(this).attr('id').split(':');
        $("#imap_username").val(parts[1]);
		$("#imap_pass").val("fakepass");
		$("#settings_id").val(parts[0]);
		$("#save_button").show();
	});
	
    $("#close_form").click(function(){
        unblockUI();
        $("#loginForm").hide();
    });
	
	$("#toggle_display").click(function(){
		$("#server_settings").toggle();
	});

	$("#save_button").click(function(){
		$("#saving").ajaxStart(function(){
			$("#save_button_wrapper").hide();
			$(this).show();
		});
		$.ajax({
			type: "POST",
			url: "ajax/ajax.saveImapSettings.php",
			data: $("#imap_settings").serialize(),
			success: function(msg){
				if(msg != "OK"){
					$("#saving").html('<p class="error">ERROR! Problem saving changes</p>');					
				}
			}
		});
		$("#saving").ajaxStop(function(){
			$("#save_button_wrapper").show();
			$(this).hide();
	        unblockUI();
	        $("#loginForm").hide();
		});
	});
});