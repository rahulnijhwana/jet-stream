var assigned_people = new Array();

function showManageForm(form_type, id) {
	var SN = get_cookie('SN');
	var Formdata = 'action=' + form_type + ' &ID= ' + id + '&show=1';
	BusyOn();
	$.post('./ajax/ajax.assignPeople.php?SN=' + SN, Formdata, function(data) {
		if (data) {
			if (!$('#assignDiv').length) {
				$('#content').append('<div id="assignDiv" />');
			}
			form = $('#assignDiv');
			form.html(data);
			$.each(form.find('input'), function() {
				id = $(this).attr('id');
				assigned_people[id] = $(this).attr('checked');
			});
			blockUI(form, '60%', '', '20%', '10%');
			BusyOff();
			
			var chkSelectAll = true;
			$.each(form.find("input[name^='people[]']"), function() {
				if(!$(this).attr("checked")) {
					chkSelectAll = false;
				}
			});
			
			if(chkSelectAll) {
				$('#sel_all').attr("checked", true);
			}
		}
	});
}

function closeManageForm() {
	cur_data = new Array();
	form = $('#assignDiv');
	$.each(form.find('input'), function() {
		id = $(this).attr('id');
		cur_data[id] = $(this).attr('checked');
	});
	if (IsModified(assigned_people, cur_data)) {
		if (!IfClosing())
			return;
	}
	unblockUI(form);
}

function assignPeople(action) {
	var SN = get_cookie('SN');
	var Formdata = 'action=' + action;
	Formdata += '&' + $("#assignForm").serialize();
	// alert(Formdata);
	BusyOn();
	$.post('./ajax/ajax.assignPeople.php?SN=' + SN, Formdata, function(data) {
		action = action.replace(/^\s+|\s+$/g, '');
		if ((action == 'account') || (action == 'company')) {
			var action_type = 'company';
			action = 'account';
		} else {
			var action_type = action;
		}
		window.location.href = '?action=' + action_type + '&' + action + 'Id=' + $("input[name='ID']").val();
	});
}

function selectAllCheckBox() {
	if ($('#sel_all').attr('checked')) {
		$("input[type='checkbox']").attr('checked', true);
	} else {
		$("input[type='checkbox']").attr('checked', false);
	}
}


function GetAjaxRec(params) {
	$('#calendar').fadeTo("fast", .1);

	$.get('ajax/ajax.GetModule.php', params, FillCalendar);
}

function UpdateModule(data) {
	$('#calendar').html(data).fadeTo("fast", 1);
}


/*
 * function clearForm() { $("input[type='checkbox']").attr('checked', false); }
 */