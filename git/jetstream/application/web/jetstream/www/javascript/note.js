var linkarray = '';
var callTimer = true;
var maxPrintNoteLimit = 50;
var noOfNoteFields = 0;
var note_type = '';

$(function() {
	
	try {
		showNoteAlertHelp();
	}
	catch(err){}
	
	$("#btnAddNote").click(function(){
		
		var valid = false;
		
		/* Note text fields */
		$(".clsTextBox").each(function(){
			var id = $(this).attr('id');
			var error_span = '#spn_' + id;
			if($(this).attr('value').length == 0){
				$(error_span).html('<img src="images/note_alert.gif" /> Cannot be blank.');
			}
			else {
				valid = true;
			}
		});

		/* If our note text fields have errors, scroll to top */
		if(!valid){
			$.scrollTo("#overlay", {duration : 2000});
		}
		
		/*
		 * Note Alert!
		 * This will only be applicable if Note Alert is enabled,
		 * and the section is expanded
		 */
		if($("#CheckPrivate").attr('disabled')){
			
			/* Delivery Method: (one must be checked) */
			if($("#standard_alert").attr('checked') == false && $("#email_alert").attr('checked') == false){
				$("#spn_delivery_error").html('<img src="images/note_alert.gif" /> Select a delivery method');
				valid = false;
			}

			/* Delay Until:
			 * If a delay is set, make sure that a day has been selected
			 */
			if(!$("#send_immediately_yes").attr('checked')){
				if($("#delay_until_day").attr('value').length == 0){
					$("#spn_delay_until_error").html('<img src="images/note_alert.gif" /> Select a day');
					valid = false;
				}
				else {
					var chosen = $("#delay_until_day").datepicker("getDate");
					var hours = parseInt($("#hours").val());
					var minutes = parseInt($("#minutes").val());
					var period = $("#periods").val();
					
					if(period == 'PM' && hours != 12){
						hours += 12;
					}
					chosen.setHours(hours);
					chosen.setMinutes(minutes);
					
					var current = new Date();

					if(chosen < current){
						$("#spn_delay_until_error").html('<img src="images/note_alert.gif" /> Chosen date is in the past');
						valid = false;
					}
				}
			}
			
			/*
			 * Select Recipient:
			 * At least one user must be selected
			 */
			var nobody_selected = true;
			$(".all_users").each(function(){
				if($(this).attr('checked')){
					nobody_selected = false;
					return false;
				}
			});
			if(nobody_selected){
				$("#spn_select_user_error").html('<img src="images/note_alert.gif" /> Select at least one user');
				valid = false;
			}
		}
		var sn = $("#sn").attr('value');
		
		if(valid){
			$("#save_button_wrapper").hide();
			$("#saving").show();
			callAddNote(sn);
		}
	});
	
	$("#buttonCancel").click(function(){
		cancelNote();
	});
	
	$(".clsTextBox").focus(function(){
		var id = $(this).attr('id');
		var error_span = '#spn_' + id;
		$(error_span).text('');
	});
	
	$("#ui-datepicker-trigger").click(function(){
		$("#spn_delay_until_error").text('');
	});
	
	$("#standard_alert").click(function(){
		$("#spn_delivery_error").text('');
	});

	$("#email_alert").click(function(){
		$("#spn_delivery_error").text('');
	});
	
	
	$(".all_users").change(function(){
		$("#spn_select_user_error").text('');
	});

	$("#select_all").click(function(){
		$("#spn_select_user_error").text('');
	});
	
	$("#select_assigned").click(function(){
		$("#spn_select_user_error").text('');
	});
	
	$(".level").click(function(){
		$("#spn_select_user_error").text('');
	});
	
	
	$(".level").click(function(){
		
		id = ($(this).attr('id'));
		
		if($("#level_checked").attr('value') != 1){
			$('.'+id).attr('checked', true);
			$("#level_checked").attr('value', 1);
		}
		else {
			$('.'+id).removeAttr('checked');
			$("#level_checked").attr('value', 0);
		}
	});
	
	
	$("#delay_until_day").datepicker({
		showOn: "button",
		buttonImage: "images/calendar.gif",
		buttonImageOnly: true,
		disabled: true
	});
	
	$(".ui-datepicker-trigger").hide();
	
	$("#send_immediately_no").click(function(){
		$(".ui-datepicker-trigger").attr('title', 'Select a Date');
		$(".ui-datepicker-trigger").show();
		$("#delay_until_day" ).datepicker('enable');
		$("#delay_until_day" ).attr('value','');
		$("#hours").removeAttr('disabled');
		$("#minutes").removeAttr('disabled');
		$("#periods").removeAttr('disabled');
	});

	$("#send_immediately_yes").click(function(){
		$(".ui-datepicker-trigger").hide();
		$("#delay_until_day" ).datepicker('disable');
		$("#hours").attr('disabled', 'disabled');
		$("#minutes").attr('disabled', 'disabled');
		$("#periods").attr('disabled', 'disabled');
		$("#spn_delay_until_error").text('');
		
	});
	
	$("#expand_alert").click(function(){
		
		$("#add_alert").slideToggle();
		
		if($("#CheckPrivate").attr('disabled')){
			$("#CheckPrivate").removeAttr('disabled');
			$("#label_private").css('color', '#333');
			$("#btnAddNote").attr('value', 'Save');
			$(this).attr('value', 'Add Alert');
			$.scrollTo("#overlay", {duration : 2000});
		}
		else {
			$("#CheckPrivate").attr('disabled', 'disabled');
			$("#label_private").css('color', '#cacaca');
			$("#btnAddNote").attr('value', 'Save and Send Alert');
			$(this).attr('value', 'Remove Alert');
			$.scrollTo("#label_private", {duration : 1000});
		}
	});
	
	$("#CheckPrivate").click(function(){
		if($("#expand_alert").attr('disabled')){
			$("#expand_alert").removeAttr('disabled');
    		$("#expand_alert").attr('value', 'Add Alert');
			$("#expand_alert").css('color', 'red');
		}
        else {
    		$("#expand_alert").attr('disabled', 'disabled');
    		$("#expand_alert").attr('value', 'Disabled');
    		$("#expand_alert").css('color', '#cacaca');
        }
	});
	

	
	$("#select_all").click(function(){
		toggleAllSelected();
	});

	$("#select_assigned").click(function(){
		toggleAssignedSelected();
	});


	if ($('#StartDate2').length) {
		$("#StartDate2").datepicker( {
			showOn : "both",
			buttonImage : "./images/calendar.gif",
			buttonImageOnly : true
		});
	}

	if ($('#EndDate2').length) {
		$("#EndDate2").datepicker( {
			showOn : "both",
			buttonImage : "./images/calendar.gif",
			buttonImageOnly : true
		});
	}

	if (typeof (filterListArray) != 'undefined') {
		if (typeof (noteReferer) != 'undefined') {
			createFilterOpt(1, 'D2', true);
			createFilterOpt(2, '', false);

			if ($('#NText1').length > 0) {
				$('#NText1').val(noteReferer);
				setNoteType(noteReferer);
			}
		} else {
			createFilterOpt(1, '', false);
		}
	}
	
	$("#Keyword").keydown(function(event){
		if(event.keyCode == 13){
			event.preventDefault();
			callNoteSearch();
		}
	});
	
	$("#btn_notes_search").click(function(event){
		event.preventDefault();
		callNoteSearch();
	});
	
	$("#select_level").click(function(){
		$("#level_msg").show();
		$(".header").fadeOut();
		$(".header").fadeIn();
		$(".header").fadeOut();
		$(".header").fadeIn();
	});

	
});

function updateNotesPerPage(per_page, rec_type, rec_id){
	$.ajax({
		   type: "POST",
		   url: "ajax/ajax.updateNotesPerPage.php",
		   data: "notes_per_page=" + per_page,
		   success: function(msg){
			   makeCall(rec_type, rec_id);
		   }
	});
}

function makeCall(rec_type, rec_id){
	if(rec_type != ''){
		loadData(rec_type, rec_id, 1);
	}
	else {
		performNoteSearch('1');
	}
}

function toggleAllSelected(){
	
	var current_state = $("#all_checked").attr('value');
	
	if(current_state != 1){
		$(".all_users").attr('checked', true);
		$("#all_checked").attr('value', 1);
	}
	else {
		resetSelected();
	}
}

function toggleAssignedSelected(){

	var current_state = $("#assigned_checked").attr('value');
	
	if(current_state != 1){
		$(".assigned_users").attr('checked', true);
		$("#assigned_checked").attr('value', 1);
	}
	else {
		resetSelected();	
	}
}



function resetSelected(){
	
	$(".all_users").removeAttr('checked');
	$("#all_checked").attr('value', 0);
	
	$(".assigned_users").removeAttr('checked');
	$("#assigned_checked").attr('value', 0);
}


function callAddNote(SNID, hasTemplate, noteTempId, noteFields) {
	
	noOfNoteFields = noteFields;
	
	var Formdata = $("#addNewNote").serialize();
	Formdata += '&Resolution=' + screen.width + 'X' + screen.height + '&HasTempalte=' + hasTemplate + '&NoteTempId=' + noteTempId;
	
	var Formurl = 'ajax/ajax.Note.php?SN=' + SNID;
	
	$.ajax({
		type: 'POST',
		url: Formurl,
		data: Formdata,
		success: function(msg) {
			processPostData(msg);
		}
    	});
	$("#btnAddNote").ajaxStop(function(){
		$("#save_button_wrapper").show();
		$("#saving").hide();
	});

}


function processPostData(result) {
	
	result = ParseStatus(result);

	if (result.result.STATUS == 'OK') {			

		unblockUI('addNote');
		
		clearNoteFields();

		if (result.resultList != '') {
			var noteTable = result.resultList;
			$('#noteDiv').html(noteTable).fadeIn('slow');
		}
		$('#noteDiv').show();
		
	}
}

function clearNoteFields(){

	$("input").removeAttr("disabled");
	$("textarea").removeAttr("disabled");
	$("select").removeAttr("disabled");

	$('#NoteSubject').val('');
	$('#NoteText').val('');
	
	$('#CheckPrivate').removeAttr('checked');
	$("#CheckPrivate").removeAttr('disabled');

	$(".all_users").removeAttr('checked');
	$("#all_checked").attr('value', false);
	$("#assigned_checked").attr('value', false);

	$("#add_alert").hide();
	
	$("#delay_until_day").attr('disabled', 'disabled');
	$("#hours").attr('disabled', 'disabled');
	$("#minutes").attr('disabled', 'disabled');
	$("#periods").attr('disabled', 'disabled');
	
	$("#label_private").css('color', '#333');
	$("#expand_alert").attr('value', 'Add Alert');
	$("#btnAddNote").attr('value', 'Save');
	
	$("#saving").hide();
	$("#btnAddNote").show();
	$("#buttonCancel").show();
	

}


function hideSucDiv() {
	$('#noteSuccMsg').fadeOut('slow');
}

function loadData(oType, oVal, page) {
	
	showLoadSymbol();
	$('#noteDiv').fadeTo("fast", .1);

	var SN = get_cookie('SN');
	var sreenResolution = screen.width + 'X' + screen.height;
	
	$.post('./ajax/ajax.getNotes.php?SN=' + SN, {
		ObjType :oType,
		ObjVal :oVal,
		PageNo :page,
		Resolution :sreenResolution
	}, showNoteData);
}

function callNoteSearch() {
	if (callTimer) {
		callTimer = false;
		setTimeout("performNoteSearch('1')", 500);
	}
}

function performNoteSearch(page, loadFirst) {
	
	callTimer = true;
	showLoadSymbol();
	var SN = get_cookie('SN');
	
	$('#noteDiv').fadeTo("fast", .1);
	if (typeof (loadFirst) != 'undefined') {
		$('#SortAs').val(1);
	}
	var Formdata = buildSearchData(page);
	Formdata += '&Resolution=' + screen.width + 'X' + screen.height;

	if (typeof (loadFirst) != 'undefined') {
		Formdata += '&load=first';
	}

	$.post('./ajax/ajax.searchNote.php?SN=' + SN, Formdata, function(data) {
		if (data) {
			showNoteData(data);
		}
	});
}

/* Need to move it to a common js file. */
function get_cookie(cookie_name) {
	var results = document.cookie.match('(^|;) ?' + cookie_name + '=([^;]*)(;|$)');

	if (results)
		return (unescape(results[2]));
	else
		return null;
}

function buildSearchData(page) {
	var totIndex = $('#filterIndex').val();
	var Formdata = '';

	for ( var i = 1; i < totIndex; i++) {
		if ($('#filterBox' + i).length) {
			if (Formdata == '') {
				Formdata += 'filterOpt[]=' + $('#filterBox' + i).val();
			} else {
				Formdata += '&filterOpt[]=' + $('#filterBox' + i).val();
			}
		}

		if ($('#NText' + i).length) {
			if (Formdata == '') {
				Formdata += 'NText' + i + '=' + $('#NText' + i).val();
			} else {
				Formdata += '&NText' + i + '=' + $('#NText' + i).val();
			}
		} else if ($('#StartDate' + i).length) {
			if (Formdata == '') {
				Formdata += 'StartDate' + i + '=' + $('#StartDate' + i).val();
			} else {
				Formdata += '&StartDate' + i + '=' + $('#StartDate' + i).val();
			}
			Formdata += '&EndDate' + i + '=' + $('#EndDate' + i).val();
		}
	}
	if (page) {
		Formdata += '&PageNo=' + page;
	}
	Formdata += '&SortType=' + $('#SortType').val();
	Formdata += '&SortAs=' + $('#SortAs').val();
	Formdata += '&Keyword=' + $('#Keyword').val();
	Formdata += '&InactiveNote=' + $('#inactiveNote').attr('checked');

	return Formdata;
}
function buildExportNoteUrl() {
	var Formdata = buildSearchData();
	var newLink = './slipstream.php?action=notes&Export=1&' + Formdata;
	return newLink;
}

function buildPrintNoteUrl() {
	var Formdata = buildSearchData();
	var newLink = './slipstream.php?action=notes&Print=1&' + Formdata;
	return newLink;
}

function exportNotes() {
	if ($('#noteDiv').html().length != 0) {
		var newExportLink = buildExportNoteUrl();
		hideLoadSymbol();
		window.location.href = newExportLink;
	}
}

function printNotes() {
	if ($('#noteDiv').html().length != 0) {
		hideLoadSymbol();

		if ($('#totRec').val() > maxPrintNoteLimit) {
			alert('Print note exceeds the maximum limit ' + maxPrintNoteLimit);
		}
		else {
			var newPrintLink = buildPrintNoteUrl();
			// window.location.href = newPrintLink;
			// window.target = "_blank";
			// window.location.href = newPrintLink;
			// window.open(newPrintLink, '_blank');
			window.print(newPrintLink);
		}
	}
}

function findObjInArray(val, arrayname, selIndex) {
	if ((val != selIndex)) {
		for ( var elem in arrayname) {
			if (arrayname[elem] == val) {
				return true;
			}
		}
	}
	return false;
}
function createFilterOpt(indx, selIndex, chooseFilter) {
	filterSelArray = Array();
	var showSelectBox = false;

	for ( var t = 1; t <= $('#filterIndex').val(); t++) {
		filterSelArray[filterSelArray.length] = $('#filterBox' + t).val();
	}
	var selectBoxstr = '<select name="filterOpt[]" class="clsFilterBox" id="filterBox' + indx
			+ '"  onChange="chooseFilterOpt(' + indx + ')">';
	selectBoxstr += '<option value="">Choose Filter Field</option>';

	for ( var filterListID in filterListArray) {

		if (!((note_type == 'NT_1') && (filterListID == 'Contact Fields'))) {
			selectBoxstr += '<optgroup label="----' + filterListID + '----">';
		}
		var filterIDIndex = 0;

		for ( var filterID in filterListArray[filterListID]) {
			if (filterListID == 'Company Fields') {
				var selOpt = findObjInArray('CM_' + filterID, filterSelArray, selIndex);
				if (selOpt) {
					continue;
				}

				if (('CM' + filterIDIndex == selIndex) || ('CM_' + filterID == selIndex)) {
					selectBoxstr += '<option value="CM_' + filterID + '" selected >'
							+ filterListArray[filterListID][filterID] + '</option>';
					showSelectBox = true;
				} else {
					selectBoxstr += '<option value="CM_' + filterID + '" >' + filterListArray[filterListID][filterID]
							+ '</option>';
					showSelectBox = true;
				}
			} else if (filterListID == 'Contact Fields') {
				if (note_type != 'NT_1') {
					var selOpt = findObjInArray('CN_' + filterID, filterSelArray, selIndex);
					if (selOpt) {
						continue;
					}

					if (('CN' + filterIDIndex == selIndex) || ('CN_' + filterID == selIndex)) {
						selectBoxstr += '<option value="CN_' + filterID + '" selected >'
								+ filterListArray[filterListID][filterID] + '</option>';
						showSelectBox = true;
					} else {
						selectBoxstr += '<option value="CN_' + filterID + '" >'
								+ filterListArray[filterListID][filterID] + '</option>';
						showSelectBox = true;
					}
				}
			} else {
				var selOpt = findObjInArray(filterID, filterSelArray, selIndex);
				if (selOpt) {
					continue;
				}

				if (filterID == selIndex) {
					selectBoxstr += '<option value="' + filterID + '" selected >'
							+ filterListArray[filterListID][filterID] + '</option>';
					showSelectBox = true;
				} else {
					selectBoxstr += '<option value="' + filterID + '" >' + filterListArray[filterListID][filterID]
							+ '</option>';
					showSelectBox = true;
				}
			}
			filterIDIndex++;
		}
		selectBoxstr += '</optgroup>';
	}
	selectBoxstr += '</select>';

	if (!showSelectBox) {
		selectBoxstr = '';
	}
	if ($('#filterSelect' + indx).length) {
		$('#filterSelect' + indx).html(selectBoxstr);
	} else {
		var nextIndx = indx;
		var result = '<div id="filter' + nextIndx + '" class="filter-list">';
		result += '<div id="filterSelect' + nextIndx + '" class="filter-list">' + selectBoxstr + '</div>';
		result += '<div id="filterText' + nextIndx + '" class="filter-list"></div><br /></div>';

		return result;
	}

	if (chooseFilter) {
		chooseFilterOpt(indx);
	}
}

function chooseFilterOpt(indx) {
	for ( var t = 1; t <= $('#filterIndex').val(); t++) {
		if ($('#filterBox' + t).length) {
			createFilterOpt(t, $('#filterBox' + t).val(), false);
		}
	}

	var curSelectBoxVal = $('#filterBox' + indx).val();
	var strDivContent = showFilterTextBox(curSelectBoxVal, indx);
	$('#filterText' + indx).html(strDivContent);

	/*
	 * if(curSelectBoxVal.lastIndexOf('Date') != -1) {
	 * if($('#NText'+indx).length) { $("#NText"+indx).datepicker({ showOn:
	 * "both", buttonImage: "./images/calendar.gif", buttonImageOnly: true }); } }
	 */

	if ($('#NText' + indx).length) {
		$('#NText' + indx).focus();
	} else if ($('#STARTDATE' + indx).length) {
		$('#STARTDATE' + indx).focus();
	}

	if ($('#StartDate' + indx).length) {
		$("#StartDate" + indx).datepicker( {
			showOn :"both",
			buttonImage :"./images/calendar.gif",
			buttonImageOnly :true
		});
	}

	if ($('#EndDate' + indx).length) {
		$("#EndDate" + indx).datepicker( {
			showOn :"both",
			buttonImage :"./images/calendar.gif",
			buttonImageOnly :true
		});
	}

	if (indx == $('#filterIndex').val()) {
		var nextIndx = indx + 1;
		$('#filterIndex').val(nextIndx);

		var result = createFilterOpt(nextIndx, '', false);

		$('#filter' + indx).append(result);
	}
}

function showFilterTextBox(selType, indx) {
	var strContent = '';
	var curSelectBoxVal = $('#filterBox' + indx).val();

	if (typeof (filterListValArray[selType]) != 'undefined') {
		strContent = eval("'" + filterListValArray[selType] + "'");
	}
	/*
	 * if(selType == 'D1') { strContent = '<input
	 * onKeyUp="javascript:callNoteSearch();"
	 * onChange="javascript:callNoteSearch();" class="clsTextBox" size="10"
	 * name="StartDate'+indx+'" id="StartDate'+indx+'" type="text" value=""
	 * />&nbsp;<font class="smlBlack">to</font>&nbsp;<input
	 * onKeyUp="javascript:callNoteSearch();"
	 * onChange="javascript:callNoteSearch();" class="clsTextBox" size="10"
	 * name="EndDate'+indx+'" id="EndDate'+indx+'" type="text" value="" />'; }
	 * else if(selType == 'D2') { strContent = '<select
	 * onChange="javascript:callNoteSearch();" class="clsTextBox"
	 * name="NText'+indx+'" id="NText'+indx+'"><option value="">Select Note
	 * Type</option>';
	 * 
	 * for (var noteSplTypeID in noteSplTypeArray) { strContent += '<option
	 * value="'+noteSplTypeID+'">'+noteSplTypeArray[noteSplTypeID]+'</option>'; }
	 * strContent += '</select>'; }else if((selType != '') &&
	 * (curSelectBoxVal.lastIndexOf('Date') != -1)) { strContent = '<input
	 * onChange="javascript:callNoteSearch();"
	 * onKeyUp="javascript:callNoteSearch();" type="text" name="NText'+indx+'"
	 * id="NText'+indx+'" value="" class="clsTextBox" />'; } else if(selType !=
	 * '') { strContent = '<input onKeyUp="javascript:callNoteSearch();"
	 * type="text" name="NText'+indx+'" id="NText'+indx+'" value=""
	 * class="clsTextBox" />'; } else { strContent = ' '; }
	 */
	return strContent;
}

function addNewNoteForm(objType, objVal, splType, subject) {
	
	blockUI('addNote', '60%', '', '20%', '10%');
	
	$('#NoteObjType').val(objType);
	$('#NoteObjVal').val(objVal);
	$('#NoteSplType').val(splType);
	$('#NoteSubject').val(subject);
	$('.clsError').html('');
	
	form_data = GetFormData($('#addNote'));
	
	/* Set an Initial check point of
	 * form data which will be checked
	 * when user will cancel the
	 * form(shell.js)
	 */
}

function showLoadSymbol() {
	if ($('#loadingDiv').length) {
		$('#loadingDiv').show();
	}
}

function hideLoadSymbol() {
	if ($('#loadingDiv').length) {
		$('#loadingDiv').hide();
	}
}

function cancelNote() {
	cur_data = GetFormData($('#addNote'));
	if (IsModified(form_data, cur_data)) {
		if (!IfClosing()){
			return;
		}
	}
	unblockUI('addNote');
	clearNoteFields();
}

function showNoteData(result) {
	try {
		$('.clsError').html('');
		$('.clsSucc').html('');
		$('#loadingDiv').hide();
		$('#noteDiv').html(result).fadeTo("fast", 1);

		var words = $("#Keyword").attr('value');
		if(words){
			$('html').highlight(words);
		}
		hideLoadSymbol();

		if($("#notes").length != 0) {
			$.scrollTo("#notes", {duration : 1000});
		}
		else {
			$.scrollTo("#top", {duration : 1000});
		}
	}
	catch (e) {
		alert('Err :' + e);
	}
}

function createNoteTemplate(values) {
	var inner = '<table width = "100%">';
	for (i = 0; i < values.length; i++) {
		var optArr = values[i]["NoteOptions"];

		switch (values[i]['FieldType']) {

		case 'TextArea':
			inner = inner + '<tr><td width="15%" valign="top">' + values[i]["LabelName"]
					+ '</td><td width="1%" valign="top">:</td><td width="84%" valign="top"><textarea id="NoteField' + i
					+ '" name="NoteField' + i + '" class="clsTextBox" cols="70" rows="6"></textarea></td></tr>';
			break;

		case 'CheckBox':
			inner = inner
					+ '<tr><td width="15%" valign="top">'
					+ values[i]["LabelName"]
					+ '</td><td width="1%" valign="top">:</td><td width="84%" valign="top"><input type="checkbox" class="clsTextBox" id="NoteField'
					+ i + '" name="NoteField' + i + '" value="1"/></td></tr>';
			break;

		case 'Date':
			inner = inner
					+ '<tr><td width="15%" valign="top">'
					+ values[i]["LabelName"]
					+ '</td><td width="1%" valign="top">:</td><td width="84%" valign="top"><input type="text" class="dtpicker" value="" id="NoteField'
					+ i + '" name="NoteField' + i + '"/></td></tr>';
			break;

		case 'DropDown':
			inner = inner
					+ '<tr><td width="15%" valign="top">'
					+ values[i]["LabelName"]
					+ '</td><td width="1%" valign="top">:</td><td width="84%" valign="top"><select id="NoteField'
					+ i
					+ '" style="width:134px;font-family: Verdana, Helvetica, Tahoma, Arial;border: 1px solid #808080;font-size: 8pt;" name="NoteField'
					+ i + '" class="selectbox"><option value=" ">-</option>';
			for (j = 0; j < optArr.length; j++) {
				inner = inner + '<option value="' + optArr[j]['OptionName'] + '">' + optArr[j]['OptionName']
						+ '</option>';
			}
			inner = inner + '</select></td></tr>';
			break;

		case 'RadioButton':
			inner = inner
					+ '<tr><td width="15%" valign="top">'
					+ values[i]["LabelName"]
					+ '</td><td width="1%" valign="top">:</td><td width="84%" valign="top"><div style="border:1px solid gray;width:83%">';
			for (j = 0; j < optArr.length; j++) {
				inner = inner + '<input type="radio" class="clsTextBox" value="' + optArr[j]['OptionName']
						+ '" id="NoteField' + i + '" name="NoteField' + i + '"/>' + optArr[j]['OptionName'] + '<br/>';
			}
			inner = inner + '</div></td></tr>';
			break;

		default:
			inner = inner
					+ '<tr><td width="15%" valign="top">'
					+ values[i]["LabelName"]
					+ '</td><td width="1%" valign="top">:</td><td width="84%" valign="top"><input type="text" class="clsTextBox" value="" id="NoteField'
					+ i + '" name="NoteField' + i + '"/></td></tr>';
		}
	}
	var inner = inner + '</table>';
	document.getElementById('noteTemplateInnerTd').innerHTML = inner;
	$(".dtpicker").datepicker( {
		showOn :"both",
		buttonImage :"./images/calendar.gif",
		buttonImageOnly :true
	});
}

function setNoteType(note_type_val) {
	note_type = note_type_val;

	for ( var t = 1; t <= $('#filterIndex').val(); t++) {
		if ($('#filterBox' + t).length) {
			createFilterOpt(t, $('#filterBox' + t).val(), false);
		}
	}
}

function MarkNotePrivate(key, emailContentID, privateNote) {
	if (privateNote) {
		if (!confirm('Mark Private?')) {
			return;
		}
		$.post('ajax/ajax.MarkNotePrivate.php', {
			emailContentID :emailContentID
		});
	} else {
		if (!confirm('Mark Public?')) {
			return;
		}
		$.post('ajax/ajax.MarkNotePublic.php', {
			emailContentID :emailContentID
		});
	}
}
