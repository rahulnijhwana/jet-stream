var recType = 'OPPORTUNITY';
var virtualOpportunityCloseDate = '';

$(document).ready( function() {
	$('#addNote').hide();
	$('#addNewEvent').hide();
});

var loadFromEditOpp = false;
if (undefined !== window.editOppPage) {
	var loadFromEditOpp = true;
	
}

function LoadRptngEvnt(event_id, event_ref) {
	// For repeating opportunities, we need to determine if they want to load the series or the instance

	var prompt = "You clicked on a repeating opportunity.  Did you want to open this opportunity or the repeating series of opportunities?";
	
	var mybuttons = {};
	mybuttons['This Opportunity'] = 'one';
	mybuttons['the Repeating Series'] = 'series';
	
	$.prompt(prompt, {
		buttons: mybuttons,
		submit: function(v, m, f) {
			if (v == 'one') {
				BusyOn();
				location.href = "?action=opportunity&rep=" + event_ref;
			}
			if (v == 'series') {
				BusyOn();
				location.href="?action=opportunity&eventId=" + event_id;
			}
		}
	});
}

function cancelPendingOpportunity(id, etype, date) {
	msg = '';
	if (etype == 'OPPORTUNITY')
		msg = "Do you want to cancel this Opportunity?";
	else
		msg = "Do you want to cancel this Task?";

	if (confirm(msg)) {
		
		if(date != '') {
			date = '&date=' + date;
		}	
		
		var postUrl = 'ajax/ajax.ChangeOpportunityStatus.php?action=cancel&eventId=' + id + date;

		$.post(postUrl, function(data) {
			if(date != '') {	
				result = ParseStatus(data);
				window.location = 'slipstream.php?action=opportunity&eventId=' + result.result.OpportunityID;
			} else {
				window.location.reload();
			}
		});
	}
}

function CompletePendingOpportunity(id, repeat_info, type, oppid) {
	if (oppid != '' && oppid > 0) {
	} else {
		// For repeating opportunities, we need to determine if they want to load the series or the instance
		if (type == 'remove') {
			var prompt = 'Are you sure you want to permanently remove this opportunity?';
			var buttons = {Remove:true, Cancel:false};
		} else {
			var prompt = 'Opportunity Completion Notes<br/><textarea name="closingNote" rows="8" cols="55" />';
			if (repeat_info == '') {
				prompt += '<br/><input type="checkbox" value="true" name="followOnOpportunity"/> Create next opportunity';
			}
			var buttons = {Complete:true, Cancel:false};
			type = 'close';
		}
		$.prompt(prompt, {
			buttons: buttons,
			submit: function(v, m, f) {
				if (v) {
					
					BusyOn();
					$.post('ajax/ajax.ChangeOpportunityStatus.php', 
							{OpportunityID: id, rep: repeat_info, Note: f.closingNote, FollowOn: f.followOnOpportunity, Action: type},
							function (data) {
								if (data.dest) {
									window.location = data.dest;
								} else if (data.followon) {
									ShowAjaxForm('OpportunityNew&contactid=' + data.followon + '&action=add');
								}
							}, "json"
					);
				}
			}
		});
	}
}

function closePendingOpportunity(id, oppid, personId, etype, next_event, date) {

	
	if (oppid != '' && oppid > 0) {
		// if(confirm("Do you want to complete this Opportunity?")) {
		openOppPopup(oppid, personId);
		// }
	} else {
		$("input").attr("disabled", "");
		$("textarea").attr("disabled", "");
		
		if (etype == 'OPPORTUNITY') {
			$('#lblType').html('Opportunity');
			// $('#chkHolder').show();
		} else {
			$('#lblType').html('Task');
			// $('#chkHolder').hide();
		}
		// if(confirm(msg)) {

		$('#eventId').val(id);
		$('#closingNote').val('');
		if (next_event) {
			$('#chkHolder').show();
			$('#followOnOpportunity').attr('checked', false);
		} else {
			$('#chkHolder').hide();
		}
		form_data = GetFormData($('#closingOpportunityNote')); // set an Initial

		blockUI('closingOpportunityNote', '550px', '', (jQuery(window).width() - 550) / 2 + 'px', 100);

	}
}

function submitCloseOpportunity() {
	var postData = $('#closeOpportunity').serialize();	
	var date = '';
	if (virtualOpportunityCloseDate != '') {
		date = '&date=' + virtualOpportunityCloseDate;
		virtualOpportunityCloseDate = '';
	}
	
	var postUrl = 'ajax/ajax.ChangeOpportunityStatus.php?action=close' + date;	
	
	$.post(postUrl, postData, function(data) {
		result = ParseStatus(data);

		unblockUI('closingOpportunityNote');
		if (data != 0) {			
			var con_id = result.result.contactId;
			var event_id = result.result.eventId;
			if (result.result.followOnOpportunity == 1) {
				if (result.result.oppId > 0) {
					var opp_id = result.result.oppId;
					addOpportunity(con_id, 'add', event_id, opp_id);
				} else {
					addOpportunity(con_id, 'add', event_id);
				}
			} else {
				//result = ParseStatus(data);				
				window.location = 'slipstream.php?action=opportunity&eventId=' + result.result.OpportunityID;
				//window.location.reload();
			}
		} else {
			window.location.reload();
		}

		$("input").attr("disabled", "");
		$("textarea").attr("disabled", "");
	});

	$("input").attr("disabled", "disabled");
	$("textarea").attr("disabled", "disabled");
}

function cancelClosingFunct() {
	cur_data = GetFormData($('#closingOpportunityNote'));
	if (IsModified(form_data, cur_data)) {
		if (IfClosing()) {
			$('#closingNote').val('');
			unblockUI('closingOpportunityNote');
		}
	} else {
		$('#closingNote').val('');
		unblockUI('closingOpportunityNote');
	}
}

function toggleOpportunityRow() {
	if ($("option:selected", "select#Type").val().substring(0, 4) == 'TASK') {
		$('.OpportunityRows').hide();
		$('#ClockPick').val('');
		$('#ClockDuration').val('');
	} else {
		// $('#ClockPick').val('');
		// $('#ClockDuration').val('');
		$('.OpportunityRows').show();
	}
}

var firstMeeting = false;
var nextMeeting = false;

function showNoCancel() {
	$('#btnNoCancell').show();
	$('#btnRescheduled').hide();
}

function isFirstMeetingClosed() {
	var fm_id = g_opportunity[g_opps.FMOpportunityID];
	var nm_id = g_opportunity[g_opps.NMOpportunityID];

	if (jet_opportunities[fm_id] != undefined) {
		if (jet_opportunities[fm_id]['Closed'] != 1 && jet_opportunities[fm_id]['Cancelled'] != 1) {
			return false;
		}
	}
	
	return true;
}

function isNextMeetingClosed() {
	var nm_id = g_opportunity[g_opps.NMOpportunityID];
	if (jet_opportunities[nm_id] != undefined) {
		if (jet_opportunities[nm_id]['Closed'] != 1 && jet_opportunities[nm_id]['Cancelled'] != 1) {
			return false;
		}
	}
	
	return true;
}

function EditOppOpportunity(params) {
	if (typeof(params) != 'object') {
		params = {};
	}
	var fm_id = g_opportunity[g_opps.FMOpportunityID];
	var nm_id = g_opportunity[g_opps.NMOpportunityID];
	var category = g_opportunity[g_opps.Category];
	
	var buttons = {Complete:'Complete', Reschedule:'Reschedule', Remove:'Remove'};
	if (params.reschedule == false) {
		delete buttons.Reschedule;
	}
	
	if ((fm_id == '' || fm_id == 0) && (nm_id == '' || nm_id == 0)) {
		EditOppOpenOpportunity(-2);
	} else if ((fm_id == -2 || fm_id > 0) && typeof(jet_opportunities[fm_id]) != 'undefined' && jet_opportunities[fm_id]['Closed'] != 1) {
		var prompt = 'The "First Meeting" on ' + $('#firstMeeting').val() + ' is currently open';
		var buttons = {Complete:'Complete', Reschedule:'Reschedule'};
		delete buttons.Remove;
		EditOppPrmptOpnMtg(prompt, fm_id, buttons, params.followup);
	} else if (nm_id > 0) {
		if (jet_opportunities[nm_id]['Closed'] != 1) {
			var prompt = 'The "Next Meeting" on ' + $('#nextMeeting').val() + ' is currently open';
			EditOppPrmptOpnMtg(prompt, nm_id, buttons, params.followup);
		} else {
			// Go directly to add opportunity, do not stop at Go, do not collect $200
			EditOppOpenOpportunity(-1);
		}
	} else {
		// Go directly to add opportunity, do not stop at Go, do not collect $200
		EditOppOpenOpportunity(-1);
	}
}

function EditOppPrmptOpnMtg(prompt, meeting_id, prompt_buttons, prmpt_nxt_mtg) {
	var states = {
		state0: {
			html:prompt,
			buttons: prompt_buttons,
			submit: function (v,m,f) {
				if (!v) {
					return true;
				}
				if (v == 'Complete') {
					$.prompt.goToState('state1');
				} else if (v == 'Remove') {
					$.prompt.goToState('state2');
				} else if (v == 'Reschedule') {
					// Open this opportunity in the opportunity editor
					EditOppOpenOpportunity(meeting_id);
					return true;
				}

				return false;
			}
		},
		state1: {
			html: 'Opportunity Completion Notes<br/><textarea name="closingNote" style="width:380px;height:160px;" />',
			buttons: {Complete:true, Cancel:false},
			submit: function (v,m,f) {
				if (v) {
					jet_opportunities[meeting_id]['Closed'] = true;
					if (f.closingNote) {
						jet_opportunities[meeting_id]['Note'] = f.closingNote;
					}
					// EditOppOpportunity();
					// return true;
					if (prmpt_nxt_mtg != false) {
						$.prompt.goToState('state3');
						return false;
					}
					return true
				} else {
					$.prompt.goToState('state0');
					return false;
				}
			}
		},
		state2: {
			html: 'Are you sure you want to permanently remove this opportunity?',
			buttons: {Remove:true, Cancel:false},
			submit: function (v,m,f) {
				if (v) {
					jet_opportunities[meeting_id]['Closed'] = true;
					// EditOppOpportunity();
					// return true;
					if (prmpt_nxt_mtg != false) {
						$.prompt.goToState('state3');
						return false;
					}
				} else {
					$.prompt.goToState('state0');
					return false;
				}
				return true;
			}
		},
		state3: {
			html: 'Add a new meeting now?',
			buttons: {Yes:true, No:false},
			submit: function (v,m,f) {
				if (v) {
					EditOppOpportunity();
				}
				return true;
			}
		}
	};
	
	$.prompt(states);
}

function EditOppOpenOpportunity(opportunity_id) {
	var send_data = '';
	var action = 'edit';
	if (opportunity_id < 0) {
		action = 'add';
		if (!jet_opportunities[opportunity_id]) {
			var contact_id = g_opportunity[g_opps.ContactID];
			if ($('#ContactID').val() != '') {
				contact_id = $('#ContactID').val();
			}
			var this_opp_id = (opp_id == 0) ? -1 : opp_id;
			
			send_data = {DealID: this_opp_id, ContactID: -1};
		}
	}
	if (jet_opportunities[opportunity_id]) {
		send_data = jet_opportunities[opportunity_id];
		// send_data['DealID'] = this_opp_id;
	}
	ShowAjaxForm('OpportunityNew&opportunityid=' + opportunity_id + '&action=' + action, '', send_data);
}

function EditOppResultProc(result, rec_id) {
	// Merge the results with the existing jet_opportunities hash
	var incoming_opportunity = {};
	incoming_opportunity[rec_id] = result;
	jQuery.extend(true, jet_opportunities, incoming_opportunity);

	// var cat = getOppsCategory();
	// Determine which meeting the result should go into
	var fm_id = g_opportunity[g_opps.FMOpportunityID];
	var nm_id = g_opportunity[g_opps.NMOpportunityID];
	
	if (rec_id == -2 || rec_id == g_opportunity[g_opps.FMOpportunityID] 
			|| (g_opportunity[g_opps.FMOpportunityID] == '' && g_opportunity[g_opps.NMOpportunityID] == '')) {
		// If nothing is set, or the opportunityid matches current FM then it is a FM
		g_opportunity[g_opps.FMOpportunityID] = rec_id;
		var field_prefix = 'first';
	} else {
		// Otherwise it is a NM
		g_opportunity[g_opps.NMOpportunityID] = rec_id;
		var field_prefix = 'next';
	}
	
	// Update the editopp form for the proper meeting
	if (result.meetingDate) {
		$('#' + field_prefix + 'Meeting').val(result.meetingDate);
	}
	if (result.meetingTime) {
		$('#' + field_prefix + 'MeetingTime').val(result.meetingTime);
	}

	unblockUI(form);
	form.remove();
	
	// Tell EditOpp that a save is needed
	something_changed();
}

function CheckDateExpire(inputDate) {
	var currdate = new Date();
	var list = inputDate.split('/');
	var inputDate = list[0] + '/' + list[1] + '/' + '20' + list[2];
	var userdate = new Date(inputDate)

	return (userdate < currdate) ? true : false;
}

function textCounter(field, maxlimit) {
	if (maxlimit != 0 && maxlimit != '') {
		if (field.value.length > maxlimit - 1) {
			field.value = field.value.substring(0, maxlimit);
			alert('Textarea value can only be ' + maxlimit + ' characters in length.');
			return false;
		}
	}
}

function ClearContact() {
	$('#ContactID').val("");
}

function CreateNewContact(form_location, params, form_action, key) {
	$('body').data('CurrentRow', 'email_hold_' + key);
	ShowAjaxForm(form_location, params, form_action);
}

/**
 * Cancel the edit opps opportunity dialog
 */
function CancellDialog() {
	$('#nextMeetingStatus').hide();
	unblockUI('addNewOpportunity');
}

/**
 * Returns the selected edit opps category.
 * 
 */
function getOppsCategory() {
	var cat = 0;
	try {
		cat = parseInt(value_of_radio());
	} catch (e) {
	}

	return cat;
}

function ResetCalTooltips() {
	$(document).ready( function() {
		$('.preview').cluetip( {
			attribute :'id',
			width :264,
			showTitle :false,
			arrows :true
		});
		$('.task_preview').cluetip( {
			attribute :'id',
			width :264,
			sticky :true,
			closePosition :'title',
			arrows :true
		});
	});
}
