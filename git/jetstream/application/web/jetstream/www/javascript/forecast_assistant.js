/** 
 * @fileoverview This file is to be used for loading and calculating Forecast Assistant.
 * Adds and edits personal goal, unreported sales and unreported revenues
 * It controls all of the client side forecast functions used for forecast page.
 * It requests the ajax php pages for storing edited data.
 */


/**
 * This runs for first time, when the document get loaded.
 */
$(document).ready(function(){
						   
	$("div.info").fadeIn('slow')
	if (personid == -1) {
		checkTeamAvg();
	} else {
		calculate();
		adjustmentSlider();
		updateSalesCycle();
	}
});


/**
 * This function increases or decreases the Average Sale slider value.
 */
function shiftAvgSales(option) {
	
	var average_sales_ori_slider = makeInt($('#average_sales_ori_slider').html());
	var average_sales_slider = makeInt($('#average_sales_slider').html());
	var increment = Math.round(average_sales_ori_slider / 100);
	
	if (option == 1) {
		if (average_sales_slider < (average_sales_ori_slider * 2)) {
			$("#faslider4").slider("moveTo", "+=" + increment);
		}
	}
	
	if (option == 0) {
		if (average_sales_slider > average_sales_ori_slider) {
			$("#faslider4").slider("moveTo", "-=" + (increment/2));
		}
	}		
}

/**
 * This function increases or decreases the Closing Ratio slider value.
 */
function shiftClosingRatio(option) {
	
	var ori_slider = makeInt($('#closing_ratio_ori_slider').html());
	var slider = makeInt($('#closing_ratio_slider').html());

	if (option == 1) {
		if (slider < 100) {
			$("#faslider3").slider("moveTo", "+=1");
		}
	}
	
	if (option == 0) {
		if (slider > ori_slider) {
			$("#faslider3").slider("moveTo", "-=1");
		}
	}		
}

/**
 * This function increases or decreases the Sales Cycle slider value.
 */
function shiftSalesCycle(option) {
	
	var ori_slider = makeInt($('#sales_cycle_original').html());
	var slider = makeInt($('#sales_cycle_slider').html());

	if (option == 1) {
		if (slider < ori_slider) {
			$("#faslider2").slider("moveTo", "+=1");
		}
	}
	
	if (option == 0) {
		if (slider > ori_slider/2) {
			$("#faslider2").slider("moveTo", "-=1");
		}
	}		
}

/**
 * This function increases or decreases the First Meetings slider value.
 */
function shiftFirstMeeting(option) {
	
	var ori_slider = makeInt($('#first_meeting_orig_slider').html());
	var slider = makeInt($('#first_meeting_slider').html());

	if (option == 1) {
		if (slider < ori_slider * 2) {
			$("#faslider1").slider("moveTo", "+=1");
		}
	}
	
	if (option == 0) {		
		if (slider > ori_slider) {			
			$("#faslider1").slider("moveTo", "-=1");
		}
	}		
}


/**
 * This function makes the Average Sale to edit.
 * 
 * @param {String} origid. This holds the div id.
 * @param {String} inputid. This holds the input box id.
 */
function editAdjustment(origid, inputid) {
	var amount = formatCurrency(makeInt($('#' + origid).html()));
	$('#' + inputid).val(amount);
	$('#' + origid).hide();	
	$('#' + inputid).show();
	$('#' + inputid).focus();
}


/**
 * This function commit the changes to Average Sale.
 * 
 * @param {String} origid. This holds the div id.
 * @param {String} inputid. This holds the input box id.
 */
function commitAdjustment(origid, inputid) {
	var amount = formatCurrency(makeInt($('#' + inputid).val()));

	$('#' + inputid).val(amount);
	$('#' + origid).html(amount);	
	$('#' + origid).show();	
	$('#' + inputid).hide();	

	var average_sales_ori_slider = makeInt($('#average_sales_ori_slider').html());
	var average_sales_slider = makeInt($('#average_sales_slider').html());

	if (average_sales_slider > (average_sales_ori_slider * 2)) average_sales_slider = average_sales_ori_slider * 2;
	if (average_sales_slider <= average_sales_ori_slider) average_sales_slider = average_sales_ori_slider;
	
	var increment = average_sales_slider - average_sales_ori_slider;	
	if(increment == 0) increment = 1;
	$("#faslider4").slider("moveTo", increment);
	$('#average_sales_slider').html(formatCurrency(average_sales_slider));
}


/**
 * This function initializes the adjustment sliders.
 */
function adjustmentSlider() {
	
	// first meeting slider
	first_meeting_slider_value = (first_meeting_slider_value == 0) ? 1 : first_meeting_slider_value;	
    $("#faslider1").slider({	  
	  max:first_meeting_slider_value,
      slide: function (ev, ui) {       
		$('#first_meeting_slider').html(ui.value + first_meeting_slider_value);		
		updateSalesCycle();
      },
	  stop: function (ev, ui) {			    
		setTimeout("updateProjection()", 100) ;
      }
    });
	
	// sales cycle slider
	var sales_cycle_slider_default = sales_cycle_slider_value + 1;
	sales_cycle_slider_value = Math.round(sales_cycle_slider_value / 2);
	
    $("#faslider2").slider({	  
	  max:sales_cycle_slider_default,
	  min:sales_cycle_slider_value,
	  startValue:sales_cycle_slider_default,
      slide: function (ev, ui) {       
		$('#sales_cycle_slider').html(ui.value - 1);
		updateSalesCycle();		
      },
	  stop: function (ev, ui) {			    
		setTimeout("updateProjection()", 100) ;
      }
    });
	
	// closing ratio slider
	max_closing_ratio_slider_value = closing_ratio_slider_value * 2;
	max_closing_ratio_slider_value = (max_closing_ratio_slider_value > 100) ? 100 : max_closing_ratio_slider_value;
	
    $("#faslider3").slider({	 
	  min:closing_ratio_slider_value,
	  max:max_closing_ratio_slider_value,
      slide: function (ev, ui) {       
		$('#closing_ratio_slider').html(ui.value);
		updateSalesCycle();
      },
	  stop: function (ev, ui) {		
		setTimeout("updateProjection()", 100) ;
      }
    });
	
	// average Sale slider
    $("#faslider4").slider({	  
	  max:average_sales_slider_value, 
      slide: function (ev, ui) {       
		$('#average_sales_slider').html(formatCurrency(ui.value + average_sales_slider_value));		
      },
	  stop: function (ev, ui) {		
		setTimeout("updateProjection()", 100) ;
      }
    });	
}

/**
 * This function calculates the sales cycle value and updates it.
 * Basic Sales Per Month = First meeting * Closing Ratio
 * Sales Cycle Original Val / Sales Cycle Slider Val * Basic Sales Per Month = Actual Sales Per Month
 */
function updateSalesCycle() {	
	var sales_cycle_original = $('#sales_cycle_original').html();
	var sales_cycle_slider = $('#sales_cycle_slider').html();
	var closing_ratio = parseFloat($('#closing_ratio_slider').html()) / 100;
	var first_meetings = parseFloat($('#first_meeting_slider').html());
	var basic_monthly_sales = first_meetings * closing_ratio;	
	var actual_sales_per_month = sales_cycle_original / sales_cycle_slider * basic_monthly_sales;
	$('#monthly_sales_slider').html((actual_sales_per_month).toFixed(1));
}

/**
 * This function calculates the prjection values and updates it.
 */
function updateProjection() {
	calculateProjectionMonth();
	calculateProjectionQuarter();
	updateFutureQuarterProjection();
	todayToFiscalEnd();
	calculateProjectedPercentage();
}

/**
 * It gives the effect to the field which has modified from calculation.
 *
 * @param {String} id It holds the id of cell.
 */
function runit(id) {
	$(id).fadeOut().fadeIn();
}

/**
 * It converts a given string to int number.
 *
 * @param {String} numb It holds the string.
 */
function makeInt(numb) {
	if (!numb) numb = '0';
	var num = numb.toString().replace(/\,/g,'');
	if (num.charAt(0) == '$') num = num.substr(1);
	if (isNaN(parseInt(num))) num = "0";
	return parseInt(num,10);
}


/**
 * It converts a given string to floating number.
 *
 * @param {String} numb It holds the string.
 */
function makeFloat(numb) {
	if (!numb) numb = '0';
	var num = numb.toString().replace(/\,/g,'');
	if (num.charAt(0) == '$') num = num.substr(1);
	if (isNaN(parseFloat(num))) num = "0";
	return parseFloat(num);
}


/**
 * This function converts a given number to money format. It converts to US mony format
 *
 * @param {String} numb. It holds the amount string.
 */
function formatCurrency(numb){
	var num = numb.toString().replace(/\,/g,'');
	if (num.charAt(0) == '$') num = num.substr(1);
	num = parseFloat(num,10);
	if (isNaN(parseInt(num))) num = 0;

	var sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	var cents = num%100;
	num = Math.floor(num/100).toString();
	if (cents < 10){
		cents = "0" + cents;
	}
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++){
		num = num.substring(0,num.length-(4*i+3))+','+	num.substring(num.length-(4*i+3));
	}
	
	//return (((sign)?'':'-') + '$' + num + '.' + cents);
	return (((sign)?'':'-') + '$' + num);
}

// Global variables
var actual_amount = new Array();
var actual_goal_amount = 0;
var total_amount = 0;

/**
 * This function calculates Actual Sales and Actual revenue in month by month table.
 * This function also calculates Unreported Sales ## and Unreported Sales $$ in Goal Analysis table
 * It stores actual revenue in an global array actual_amount
 *
 */
function calculateActualAmounts() {
	var id = 1;
	var total_unreported_revenue = 0;
	var total_unreported_sales = 0;
	
	while (document.getElementById('unreported_sales_' + id))	{
		var unreported_sales = makeInt($('#unreported_sales_' + id).val());
		var reported_sales = makeInt($('#reported_sales_' + id).html());
		var actual_sales = unreported_sales + reported_sales;
		$('#actual_sales_' + id).html(actual_sales);
		runit('#actual_sales_' + id);

		var unreported_revenue = makeFloat($('#unreported_revenue_' + id).val());
		var reported_revenue = makeFloat($('#reported_revenue_' + id).html());
		var actual_revenue = unreported_revenue + reported_revenue;
		actual_amount[id] = actual_revenue;
		$('#actual_' + id).html(formatCurrency(actual_revenue));
		runit('#actual_' + id);

		total_unreported_revenue += unreported_revenue;
		total_unreported_sales += unreported_sales;
		id++;
	}

	// Calculate 18 and 18a
	$('#unreported_sales_personal').html(total_unreported_sales);
	runit('#unreported_sales_personal');

	$('#unreported_sales_company').html(total_unreported_sales);
	runit('#unreported_sales_company');

	$('#unreported_revenue_personal').html(formatCurrency(total_unreported_revenue));
	runit('#unreported_revenue_personal');

	$('#unreported_revenue_company').html(formatCurrency(total_unreported_revenue));
	runit('#unreported_revenue_company');	
}


/**
 * This function calculates Total Sales to date in Goal Analysis table
 */
function totalSalesToDate() {
	
	var mp_reported_sales_company = makeInt($('#mp_reported_sales_company').html());
	var unreported_sales_company = makeInt($('#unreported_sales_company').html());
	var mp_reported_sales_personal = makeInt($('#mp_reported_sales_personal').html());
	var unreported_sales_personal = makeInt($('#unreported_sales_personal').html());

	$('#company_sales_todate').html(mp_reported_sales_company + unreported_sales_company);
	runit('#company_sales_todate');

	$('#personal_sales_todate').html(mp_reported_sales_personal + unreported_sales_personal);
	runit('#personal_sales_todate');
}


/**
 * This function calculates Goal Analysis, Projections and Month By Month Sales and Required Sales report
 */
function calculate() {

	actual_goal_amount = document.getElementById('goal_company').checked ? makeFloat($('#company_goal').html()) : makeFloat($('#personal_goal').val());
	$('#goal_amount').html(formatCurrency(actual_goal_amount));
	
	calculateActualAmounts();	
	calculateRequiredAmount();		
	calculateMonthlyQuota();	
	calculateForecast();
	
	calculatePastQuarterSales();
	totalRevenueSalesTodate();
	calculateActualQuarter();
	calculateRequiredQuarter()
	
	calculateActual();
	totalSalesToDate();
	calculateMonthlyGoal();	
	calculatePercentageMonthlyQuota();
	
	calculateQuarterlyQuote();
	calculateReachGoalAnalysis();
	updateProjection();
}


/**
 * This function calculates total actual amount January 1st to Today
 * It calculates M-Power reported Sales ## and M-Power reported Sales $$
 */
function calculateForecast() {

	var period = 1;	
	var total_sales_todate = 0;
	var total_revenue_todate = 0;
		total_amount = 0;
		
	while (period <= current_period) {
		
		total_sales_todate += makeInt($('#reported_sales_' + period).html());
		total_revenue_todate += makeFloat($('#reported_revenue_' + period).html());		
		period++;
	}
	
	var id = 1;
	var total_unreported_revenue = 0;
	while (document.getElementById('unreported_sales_' + id))	{		
		total_unreported_revenue += makeFloat($('#unreported_revenue_' + id).val());
		id++;
	}

	total_amount += total_revenue_todate + total_unreported_revenue;
	
	// M-Power Reported Sales $	
	$('#mp_reported_sales_company').html(total_sales_todate);
	runit('#mp_reported_sales_company');
	$('#mp_reported_sales_personal').html(total_sales_todate);
	runit('#mp_reported_sales_personal');

	// M-Power Reported revenue total revenue to date
	$('#mp_reported_revenue_company').html(formatCurrency(total_revenue_todate));
	runit('#mp_reported_revenue_company');
	$('#mp_reported_revenue_personal').html(formatCurrency(total_revenue_todate));
	runit('#mp_reported_revenue_personal');	
}

/**
 * This function calculates quarterly quote
 * 
 */
function calculateQuarterlyQuote() {
	var quarter = 1;	
	
	while (document.getElementById('quater_quota_' + quarter)) {		
		var actual = makeInt($('#actual_quater_' + quarter).html());
		var required = makeInt($('#required_quater_' + quarter).html());
		
		var quater_quota = (required == 0) ? 0 : actual / required;
		quater_quota = Math.round(quater_quota * 100)
		
		if (quater_quota >= 100) {
			$('#quater_quota_' + quarter).css('color','black');			
		} else {
			$('#quater_quota_' + quarter).css('color','red');			
		}		
		
		
		$('#quater_quota_' + quarter).html( quater_quota + '%');
		runit('#quater_quota_' + quarter);		
		quarter++;
	}
}

/**
 * This function calculates Total Sales to date and Total $$ to date
 * 
 */
function totalRevenueSalesTodate() {
	var mp_reported_revenue_company = makeFloat($('#mp_reported_revenue_company').html());
	var unreported_revenue_company = makeFloat($('#unreported_revenue_company').html());
	var mp_reported_revenue_personal = makeFloat($('#mp_reported_revenue_personal').html());
	var unreported_revenue_personal = makeFloat($('#unreported_revenue_personal').html());

	$('#company_revenue_todate').html(formatCurrency(mp_reported_revenue_company + unreported_revenue_company));
	runit('#company_revenue_todate');
	
	$('#personal_revenue_todate').html(formatCurrency(mp_reported_revenue_personal + unreported_revenue_personal));
	runit('#personal_revenue_todate');
}


/**
 * This function calculates actual quarter amount for a given quarter
 * 
 */
function calculateActualQuarter() {	
	var quarter = 1;
	
	while (document.getElementById('actual_quater_' + quarter)) {
		// update the actual quarter amount
		var actual_quarter_amount = formatCurrency(calculateQuarterAmount(actual_amount, quarter));
		$('#actual_quater_' + quarter).html(actual_quarter_amount);
		runit('#actual_quater_' + quarter);
		
		quarter++;
	}
}

/**
 * This function calculates history amount and miss/over for passed months
 * 
 */
function calculatePastQuarterSales() {
	var quarter = 1;
	var required_amount = getMonthlyQuota(actual_goal_amount);
	
	while (quarter < current_quarter) {
	
		var actual_quarter_amount = calculateQuarterAmount(actual_amount, quarter);		
		var required_quarter_amount = calculateQuarterAmount(required_amount, quarter);
		
		$('#quarter_sales_' + quarter).html(formatCurrency(actual_quarter_amount));
		runit('#quarter_sales_' + quarter);
		
		// Miss/Over
		var miss_over_amount = actual_quarter_amount - required_quarter_amount;

		if (miss_over_amount >= 0) {
			$('#miss_over_' + quarter).css('color','black');			
		} else {
			$('#miss_over_' + quarter).css('color','red');
			miss_over_amount = (-1) * miss_over_amount;
		}

		$('#miss_over_' + quarter).html(formatCurrency(miss_over_amount));
		runit('#miss_over_' + quarter);
		
		quarter++;
	}
}


/**
 * This function calculates required quarter amount for a given quarter
 * 
 */
function calculateRequiredQuarter() {	
	var quarter = 1;
	var required_amount = getMonthlyQuota(actual_goal_amount);
	
	while (document.getElementById('required_quater_' + quarter)) {
		var required_quarter_amount = calculateQuarterAmount(required_amount, quarter);
		$('#required_quater_' + quarter).html(formatCurrency(required_quarter_amount));
		runit('#required_quater_' + quarter);
		quarter++;
	}
}


/**
 * Today to current Month end should be the projection for the remaining days of the current month.  
 * Today to current Month End should = Days left this month / 31 * Sales per Month * Average Sale
 */  
function calculateProjectionMonth() {
	// Total $$ Sold Current Month
	$('#current_month_sales').html($('#actual_' + current_period).html());
	runit('#current_month_sales');	
	
	var sales_per_month = makeFloat($('#monthly_sales_slider').html());
	var average_sale = makeFloat($('#average_sales_slider').html());	
	
	var total_unreported_sales = makeInt($('#unreported_sales_company').html());
	var unreported_sales_per_month =  makeFloat(total_unreported_sales / current_period);
	var unreported_average_sale = (total_unreported_sales == 0) ? 0 : makeFloat(makeFloat($('#unreported_revenue_company').html()) / total_unreported_sales);

	// Today to current Month End
	var today_current_month_end = (days_left_this_month / 31) * (sales_per_month * average_sale + unreported_sales_per_month * unreported_average_sale + original_unreported_avg_sale * original_unreported_sales_per_month);
	$('#current_month_end').html(formatCurrency(today_current_month_end));
	runit('#current_month_end');	
	
	// Today to current Month End
	var current_total_projection = makeFloat($('#current_month_sales').html()) + today_current_month_end;
	$('#monthly_total_projection').html(formatCurrency(current_total_projection));
	runit('#monthly_total_projection');	
	
	//  Today to current Month End Miss over	
	var monthly_total_projection_missover = current_total_projection - makeFloat($('#required_quota_' + current_period).html());
	
	var temp_miss_over_amount = 0;
	if (monthly_total_projection_missover >= 0) {
		$('#monthly_total_projection_missover').css('color','black');
		temp_miss_over_amount = monthly_total_projection_missover;
	} else {
		$('#monthly_total_projection_missover').css('color','red');
		temp_miss_over_amount = (-1) * monthly_total_projection_missover;
	}
	
	$('#monthly_total_projection_missover').html(formatCurrency(temp_miss_over_amount));
	runit('#monthly_total_projection_missover');	
}

/**
 * Calculates the projection for this quarter
 *
 */
function calculateProjectionQuarter() {
	
	// Total $$ sold current quarter
	var total_sold_current_quarter = calculateQuarterAmount(actual_amount, current_quarter);
	$('#total_sold_current_quarter').html(formatCurrency(total_sold_current_quarter));
	runit('#total_sold_current_quarter');	
	
	/**
	 * Today to current quarter end
	 * Today to current quarter end should be calculated similar to the month end above:  
	 * Days left this quarter / 31 * Sales per Month * Average Sale
	 */	
	var sales_per_month = makeFloat($('#monthly_sales_slider').html());
	var average_sale = makeFloat($('#average_sales_slider').html());
	
	var total_unreported_sales = makeInt($('#unreported_sales_company').html());
	var unreported_sales_per_month =  makeFloat(total_unreported_sales / current_period);
	var unreported_average_sale = (total_unreported_sales == 0) ? 0 : makeFloat(makeFloat($('#unreported_revenue_company').html()) / total_unreported_sales);

	var today_quarter_end = (days_left_this_quarter / 31) * (sales_per_month * average_sale + unreported_sales_per_month * unreported_average_sale + original_unreported_avg_sale * original_unreported_sales_per_month);
	$('#today_quarter_end').html(formatCurrency(today_quarter_end));
	runit('#today_quarter_end');	
	
	// Total Projection Current Quarter	
	var projection_current_quarter = makeFloat(total_sold_current_quarter) + today_quarter_end;
	$('#projection_current_quarter').html(formatCurrency(projection_current_quarter));
	runit('#projection_current_quarter');

	var required_amount = getMonthlyQuota(actual_goal_amount);	
	var required_quarter_amount = calculateQuarterAmount(required_amount, current_quarter);		
	var temp_miss_over_amount = projection_current_quarter - makeFloat(required_quarter_amount);
	var miss_over_current_projection_quarter = temp_miss_over_amount;	
	
	if (temp_miss_over_amount >= 0) {
		$('#miss_over_current_projection_quarter').css('color','black');
	} else {
		$('#miss_over_current_projection_quarter').css('color','red');
		miss_over_current_projection_quarter = (-1) * miss_over_current_projection_quarter;	
	}
	
	$('#miss_over_current_projection_quarter').html(formatCurrency(miss_over_current_projection_quarter));
	runit('#miss_over_current_projection_quarter');	
}


/**
 * Future quarters should be updating with sliders:  
 * days in the quarter / 31 * sales per month * average sale
 *
 */
function updateFutureQuarterProjection() {
	
	var sales_per_month = makeFloat($('#monthly_sales_slider').html());
	var average_sale = makeFloat($('#average_sales_slider').html());
	
	var total_unreported_sales = makeInt($('#unreported_sales_company').html());
	var unreported_sales_per_month =  makeFloat(total_unreported_sales / current_period);
	var unreported_average_sale = (total_unreported_sales == 0) ? 0 : makeFloat(makeFloat($('#unreported_revenue_company').html()) / total_unreported_sales);
	
	var id = current_quarter + 1;
	while (document.getElementById('quarter_sales_' + id)) {		
		var days = 3 * 31;
		if (current_quarter == 4 && total_period == 13) days = 4 * 31;
		
		var temp_amount = (days / 31) * (sales_per_month * average_sale + unreported_sales_per_month * unreported_average_sale + original_unreported_avg_sale * original_unreported_sales_per_month);
		$('#quarter_sales_' + id).html(formatCurrency(temp_amount));	
		runit('#quarter_sales_' + id);		
		
		var required_amount = getMonthlyQuota(actual_goal_amount);	
		var required_quarter_amount = calculateQuarterAmount(required_amount, id);		
		var temp_amount_miss_over = temp_amount - makeFloat(required_quarter_amount);
		
		if (temp_amount_miss_over >= 0) {			
			$('#miss_over_' + id).css('color','black');
		} else {
			temp_amount_miss_over = (-1) * temp_amount_miss_over
			$('#miss_over_' + id).css('color','red');
		}
		
		$('#miss_over_' + id).html(formatCurrency(temp_amount_miss_over));
		runit('#miss_over_' + id);		
		
		id++;
	}
}

/**
 * This function displays January 1st to Today Actual amount
 */
function calculateActual() {		
	$('#actual_total').html(formatCurrency(total_amount));
	runit('#actual_total');
}


/**
 * This function returns a given quarter amount;
 *
 * @param {Array} . It holds the amount array.
 * @param {Int} quarter. It holds the quarter number.
 *
 * @retuns total quarter amount
 * @type {Float} 
 */
function calculateQuarterAmount(actual_array, quarter) {
	
	var quarter_end = quarter * 3;
	var quarter_start = quarter_end - 2;
	var quarter_amount = 0.0;
	
	if (total_period == 13 && quarter == 4) quarter_end = 13;
	
	for (var i = quarter_start; i <= quarter_end; ++i) {
		quarter_amount += makeFloat(actual_array[i]);
	}
	
	return quarter_amount;
}

/**
 * This function calculates Projected Percentage To Annual, Below Annual and Above Annual
 *
 */
function calculateProjectedPercentage() {
	
	// To Annual
	var fiscal_year = makeFloat($('#fiscal_year').html());	
	var projected_annual_goal = (actual_goal_amount == 0) ? '-' : Math.round(((fiscal_year / actual_goal_amount) * 100), 2) + '%';
	$('#projected_annual').html(projected_annual_goal);
	runit('#projected_annual');
	
	// above/below annual
	projected_annual_goal = makeFloat(projected_annual_goal);	
	var above_below_annual = 100 - projected_annual_goal;

	if (above_below_annual <= 0) {
		$('#above_below_annual').css('color','black');
		above_below_annual = (above_below_annual == 0) ? 0 : (-1) * above_below_annual;
	} else {
		$('#above_below_annual').css('color','red');
	}
	
	$('#above_below_annual').html(Math.round(above_below_annual, 2) + '%');
	runit('#above_below_annual');

	// % To Annual Goal
	var annual_goal = (actual_goal_amount == 0) ? '-' : Math.round(((total_amount / actual_goal_amount) * 100), 2);
	var year_completed = makeFloat($('#year_completed').html());
		
	if (annual_goal >= year_completed) {
		$('#annual_goal_completed').css('color','black');			
		annual_goal = annual_goal + '%';
	} else {
		if(annual_goal != '-') {
			$('#annual_goal_completed').css('color','red');
			annual_goal = annual_goal + '%';
		}
	}				
					
	$('#annual_goal_completed').html(annual_goal);
	runit('#annual_goal_completed');
}


/**
 * Today to end fiscal year should be updating with sliders:  
 *
 * days left to the end of the fiscal year/31 * sales per month * average sale
 */
function todayToFiscalEnd() {
	var sales_per_month = makeFloat($('#monthly_sales_slider').html());
	var average_sale = makeFloat($('#average_sales_slider').html());	
	var total_unreported_sales = makeInt($('#unreported_sales_company').html());
	var unreported_sales_per_month =  makeFloat(total_unreported_sales / current_period);
	var unreported_average_sale = (total_unreported_sales == 0) ? 0 : makeFloat(makeFloat($('#unreported_revenue_company').html()) / total_unreported_sales);
	var today_to_end_fiscal_year = (days_left_this_year / 31) * (sales_per_month * average_sale + unreported_sales_per_month * unreported_average_sale + original_unreported_avg_sale * original_unreported_sales_per_month);
	
	$('#today_to_end_fiscal_year').html(formatCurrency(today_to_end_fiscal_year));
	runit('#today_to_end_fiscal_year');

	calculateFiscalYear();
}


/**
 * Fiscal year 2008 should be updating with sliders:  
 * Should be the sum of the history so far this year + today to end fiscal year
 */
function calculateFiscalYear() {
	var fiscal_year = 0;	
		fiscal_year = makeFloat($('#today_to_end_fiscal_year').html());
		fiscal_year += makeFloat($('#total_sold_current_quarter').html());
	for (var i = 1; i <= 3; ++i ) {		
		if (document.getElementById('quarter_sales_' + i)) {
			fiscal_year += makeFloat($('#quarter_sales_' + i).html());
		}
	}
	
	$('#fiscal_year').html(formatCurrency(fiscal_year));
	runit('#fiscal_year');
	
	var fiscal_year_miss_over = fiscal_year - actual_goal_amount;
	if (fiscal_year_miss_over > 0) {
		$('#fiscal_year_miss_over').css('color','black');		
	} else {
		fiscal_year_miss_over *= (-1);
		$('#fiscal_year_miss_over').css('color','red');
	}
	
	$('#fiscal_year_miss_over').html(formatCurrency(fiscal_year_miss_over));
	runit('#fiscal_year_miss_over');
}



/**
 * This function calculates % to Monthly Quota
 */
function calculatePercentageMonthlyQuota() {
	var id = 1;
	var monthly_quota = getMonthlyQuota(actual_goal_amount);	
	
	while (document.getElementById('required_quota_' + id)) {
		var monthly_requirement = monthly_quota[id];		
		
		var monthly_goal_amount = (monthly_requirement == 0) ? 0 : Math.round((makeFloat(actual_amount[id]) / monthly_requirement) * 100,2) ;
		
		if (monthly_goal_amount >= 100) {
			$('#percentage_monthly_quota_' + id).css('color','black');			
		} else {
			$('#percentage_monthly_quota_' + id).css('color','red');			
		}				
		
		
		$('#percentage_monthly_quota_' + id).html(monthly_goal_amount + '%');
		runit('#percentage_monthly_quota_' + id);
		id++;		
	}
}


/**
 * This function calculates % to Monthly Goal
 */
function calculateMonthlyGoal() {
	var id = 1;
	var required_amount = getRequiredAmount(actual_goal_amount);
	
	while (document.getElementById('required_revenue_' + id)) {
		var monthly_requirement = required_amount[id];		
		var monthly_goal_amount = (monthly_requirement == 0) ? 0 : Math.round((makeFloat(actual_amount[id]) / monthly_requirement) * 100,2) ;
		
		if (monthly_goal_amount >= 100) {
			$('#percentage_monthly_goal_' + id).css('color','black');			
		} else {
			$('#percentage_monthly_goal_' + id).css('color','red');			
		}				
				
		
		$('#percentage_monthly_goal_' + id).html(monthly_goal_amount + '%');
		runit('#percentage_monthly_goal_' + id);
		id++;
	}
}


function calculateReachGoalAnalysis() {
	var company_goal = makeFloat($('#company_goal').html());
	var personal_goal = makeFloat($('#personal_goal').val());
	var company_required_amount = getMonthlyQuota(company_goal);
	var personal_required_amount = getMonthlyQuota(personal_goal);
	var current_period_actual_amount = makeFloat(actual_amount[current_period]);
	
	// Total $$ to reach this Month's Goal
	var toreach_month_goal_company =  makeFloat(company_required_amount[current_period]) - current_period_actual_amount;	
	var temp_month_goal_company = (toreach_month_goal_company > 0) ? toreach_month_goal_company : 0;
	var toreach_month_goal_personal =  makeFloat(personal_required_amount[current_period]) - current_period_actual_amount;
	var temp_month_goal_personal = (toreach_month_goal_personal > 0) ? toreach_month_goal_personal : 0;

	$('#toreach_month_goal_company').html(formatCurrency(temp_month_goal_company));
	runit('#toreach_month_goal_company');
	$('#toreach_month_goal_personal').html(formatCurrency(temp_month_goal_personal));
	runit('#toreach_month_goal_personal');
		
	var company_actual_amount_this_qurter = 0.0;
	var company_required_amount_this_qurter = 0.0;
	var personal_required_amount_this_qurter = 0.0;	
	var period_limit = 3 * current_quarter;
	var period_start = period_limit - 2;
	
	if (total_period == 13 && current_quarter == 4) period_limit = 13;
	
	for (var i = period_start; i <= period_limit; i++) {		
		
		var temp_actual_amount = makeFloat(actual_amount[i]);
		var temp_company_required_amount =  makeFloat(company_required_amount[i]);
		var temp_personal_required_amount = makeFloat(personal_required_amount[i]);

		company_actual_amount_this_qurter += temp_actual_amount;
		company_required_amount_this_qurter += makeFloat(company_required_amount[i]);
		personal_required_amount_this_qurter += makeFloat(personal_required_amount[i]);
	}
	
	var toreach_quater_goal_company = company_required_amount_this_qurter - company_actual_amount_this_qurter;	
		toreach_quater_goal_company = (toreach_quater_goal_company > 0) ? toreach_quater_goal_company : 0;
	
	$('#toreach_quater_goal_company').html(formatCurrency(toreach_quater_goal_company));
	runit('#toreach_quater_goal_company');	
	
	var toreach_quarter_goal_personal = personal_required_amount_this_qurter - company_actual_amount_this_qurter;
		toreach_quarter_goal_personal = (toreach_quarter_goal_personal > 0) ? toreach_quarter_goal_personal : 0;
		
	$('#toreach_quarter_goal_personal').html(formatCurrency(toreach_quarter_goal_personal));
	runit('#toreach_quarter_goal_personal');	
	
	var toreach_year_goal_company = company_goal - total_amount;
		toreach_year_goal_company = (toreach_year_goal_company > 0) ? toreach_year_goal_company : 0;
	$('#toreach_year_goal_company').html(formatCurrency(toreach_year_goal_company));
	runit('#toreach_year_goal_company');	
	
	var toreach_year_goal_personal = personal_goal - total_amount;
		toreach_year_goal_personal = (toreach_year_goal_personal > 0) ? toreach_year_goal_personal : 0;
	$('#toreach_year_goal_personal').html(formatCurrency(toreach_year_goal_personal));
	runit('#toreach_year_goal_personal');	
}


/**
 * This function calculates the carry forward for a given period
 * @param {Int} period. It holds the current period or month.
 * @param {array} actual_amount an array. It holds the actual revenue data for each year.
 *
 * @returns The carry forward amount upto the given period.
 * @type Float
 */
function calculateCarryForward(period, goal_amount) {
	var carry_forward = 0;
	var id = 1;
	
	var temp_required_amount = goal_amount * seasonality[1] / 100;
	while(document.getElementById('required_revenue_' + id) &&  (id < period)) {		
		var temp_actual_amount = makeFloat(actual_amount[id]);
		var temp_amount_to_add = temp_required_amount - temp_actual_amount;		
		carry_forward += temp_amount_to_add;
		id++;
	}
	
	var temp_period = total_period - (period - 1);
	carry_forward = (temp_period == 0) ? 0 : carry_forward / temp_period;
	
	return carry_forward;
}



/**
 * This function calculates the monthly quota amount for each month
 */
function calculateMonthlyQuota() {	
	
	var monthly_quota = getMonthlyQuota(actual_goal_amount);
	var id = 1;

	while (document.getElementById('required_quota_' + id)) {
		var tid = '#required_quota_' + id;
		$(tid).html(formatCurrency(monthly_quota[id]));
		runit(tid);		
		id++;
	}	
}


/**
 * This function calculates the required amount for each month
 */
function calculateRequiredAmount() {	
	var required_amount = getRequiredAmount(actual_goal_amount);
	var id = 1;

	while (document.getElementById('required_revenue_' + id)) {
		var tid = '#required_revenue_' + id;
		$(tid).html(formatCurrency(required_amount[id]));
		runit(tid);		
		id++;
	}
}

function getMonthlyQuota(goal_amount) {
	
	var monthly_quota = new Array();
	var id = 1;
	
	while (document.getElementById('required_quota_' + id)) {		
		monthly_quota[id] = goal_amount * seasonality[id - 1] / 100;
		id++;
	}
	return monthly_quota;
}

function getRequiredAmount(goal_amount) {
	
	var required_amount = new Array();
	var id = 1;
	var current_carry_forward = 0;
	
	while (document.getElementById('required_revenue_' + id)) {
		if (id > current_period)  {
			var carry_forward = current_carry_forward;
		} else {
			var carry_forward = calculateCarryForward(id, goal_amount);
				current_carry_forward = carry_forward;
		}	
		
		carry_forward = (carry_forward < 0)? 0 : carry_forward;		
		required_amount[id] = goal_amount * seasonality[id - 1] / 100 +  carry_forward;
		
		id++;
	}
	return required_amount;
}


/**
 * This function lets a cell to edit. when the user clicks on the cell it shows a input box to edit the cell
 * @param {String} cell It holds the cell type i.e. sales or revenue.
 * @param {Int} id It holds the current cell id.
 */
function edit(cell, id) {
	var cellid = '#unreported_' + cell + '_' + id;
	var divid = '#unreported_' + cell + '_div_' + id;

	$(divid).hide();
	$(cellid).show();	
	
	if (cell == 'sales') {
		$(cellid).val(makeInt($(divid).html()));
	} else {
		$(cellid).val(makeFloat($(divid).html()));
	}
	
	$(cellid).focus();
}


/**
 * This function commites a cell. 
 * It hides the input field and calculates the values and stores the value using ajax call.
 * @param {String} cell It holds the cell type i.e. sales or revenue.
 * @param {Int} id It holds the current cell id.
 */
function commit(cell, id) {	
	var unreported_sale = $('#unreported_' + cell + '_' + id).val();
	var amount = (cell == 'sales') ? makeInt(unreported_sale) :  formatCurrency(unreported_sale);

	$('#unreported_' + cell + '_' + id).hide();
	var tempid = '#unreported_' + cell + '_div_' + id;	
	$(tempid).html(amount.toString())
	$(tempid).show();	
	
	save_data(makeInt(id));	
	calculate();	
	
	var rowid = 1;
	var unreported_sales = 0;
	var unreported_revenue = 0;
	var reported_sales = 0;
	var reported_revenue = 0;
	while (document.getElementById('unreported_sales_' + rowid))	{
		unreported_sales += makeInt($('#unreported_sales_' + rowid).val());
		unreported_revenue += makeInt($('#unreported_revenue_' + rowid).val());
		rowid++;
	}
}

/**
 * This function stores the edited cell value.
 * It stores the value using ajax call.
 *
 * @param {Int} period It holds the current cell id.
 */
function save_data(period) {
	
	var sales = makeInt($('#unreported_sales_' + period).val());  
	var revenue = makeInt($('#unreported_revenue_' + period).val());
		personid = makeInt(personid);
	var checksum = personid + period + sales + revenue;
	var data = { 'PersonID' : personid, 'Period' : period, 'Sales' : sales, 'Revenue' : revenue, 'Year' : year };

	$.post("ajax/save_unreported_sales.php?SN=" + sn, data,
	  function(data){
		if (checksum == data) {
			//alert(data);
		} else {
			//alert(data + '---' + checksum + ' -- > failed');
		}
	  });
}


checkKeyStroke = function(keyStroked) {
	var keycode = keyStroked.keyCode;

	if (keycode == 13) return "enter";

	var tabPressed = (keycode == 9 || keycode == '\t') ? true : false;

	if (keyStroked.shiftKey == true && tabPressed) return "shift-tab";
	if (tabPressed) return "tab";

	return false;
}


function editField(keyStroked, fieldType, rowID) {
	if (checkKeyStroke(keyStroked) == 'enter') edit(fieldType, rowID);
 }


function commitField(keyStroked, fieldType, rowID) {
	if (checkKeyStroke(keyStroked) == 'enter') commit(fieldType, rowID);
}


/**
 * This function is called when the user changes the personal goal
 * It stores the data through ajax call
 */
function save_personal_goal() {
	
	var personal_goal = makeFloat($('#personal_goal').val());
	var company_goal = makeFloat($('#company_goal').html());
		personid = makeInt(personid);
	var checksum = personid + personal_goal;	

	if (company_goal > personal_goal) {
		alert('Personal goal must be greater than or equal to company set goal.');
		$('#personal_goal').val($('#company_goal').html());
		personal_goal = company_goal;
	}

	$.post("ajax/save_personal_goal.php?SN=" + sn, { 'PersonID' : personid, 'PersonalGoal' : personal_goal, 'Year' : year },
	  function(data){
		if (checksum == data) {
			//alert(data + '---' + checksum + ' -- > Success');
		} else {
			//alert(data + '---' + checksum + ' -- > failed');
		}
	  });
	
	$('#personal_goal').val(formatCurrency(personal_goal));
}

/**
 * On the team version, any salesperson or team numbers below average, in 
 * any column that has an average at the bottom, should appear in red.
 *
 */
function checkTeamAvg() {
	var id = 0;	
	var first_meeting_avg = makeFloat($('#first_meeting_avg').html());
	var sales_cycle_avg = makeFloat($('#sales_cycle_avg').html());	
	var closing_ratio_avg = makeFloat($('#closing_ratio_avg').html());	
	var average_sale_avg = makeFloat($('#average_sale_avg').html());	
	var sales_per_month_avg = makeFloat($('#sales_per_month_avg').html());	

	var avg_percentage_goal_to_date = makeFloat($('#avg_percentage_goal_to_date').html());	
	
	
	var num = $('#to_goal_avg').html();
	num = num.toString().replace(/\%/g,'');	
	var to_goal_avg = makeFloat(num);
	
	while (document.getElementById('first_meeting_avg_' + id)) {
		var first_meeting = makeFloat($('#first_meeting_avg_' + id).html());
		var sales_cycle = makeFloat($('#sales_cycle_avg_' + id).html());
		var closing_ratio = makeFloat($('#closing_ratio_avg_' + id).html());
		var average_sale = makeFloat($('#average_sale_avg_' + id).html());
		var sales_per_month = makeFloat($('#sales_per_month_avg_' + id).html());
		var percentage_goal_to_date = makeFloat($('#percentage_goal_to_date_' + id).html());		
		var sales_to_date = makeFloat($('#sales_to_date_' + id).html());
		var goal_to_date = makeFloat($('#goal_to_date_' + id).html());		
		
		var tempnum = $('#to_goal_avg_' + id).html();
			tempnum = tempnum.toString().replace(/\%/g,'');
		var to_goal = makeFloat(tempnum);
		
		if (first_meeting < first_meeting_avg) $('#first_meeting_avg_' + id).css('color', 'red');
		if (sales_cycle < sales_cycle_avg) $('#sales_cycle_avg_' + id).css('color', 'red');
		if (closing_ratio < closing_ratio_avg) $('#closing_ratio_avg_' + id).css('color', 'red');
		if (average_sale < average_sale_avg) $('#average_sale_avg_' + id).css('color', 'red');
		if (sales_per_month < sales_per_month_avg) $('#sales_per_month_avg_' + id).css('color', 'red');
		if (to_goal < to_goal_avg) $('#to_goal_avg_' + id).css('color', 'red');
		if (sales_to_date < goal_to_date) $('#sales_to_date_' + id).css('color', 'red');		
		if(percentage_goal_to_date < avg_percentage_goal_to_date) $('#percentage_goal_to_date_' + id).css('color', 'red');
		
		
		id++;
	}
}


