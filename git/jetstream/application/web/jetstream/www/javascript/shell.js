var user_modified_arr = new Array();
var clear_form = true;

function DoTest() {
	$('#content').children().fadeOut('slow', function() {
		$(this).remove();
	});
	$('#sidebar').children('.module').fadeOut('slow', function() {
		$(this).remove();
	});

	$.get('ajax/ajax.GetCalendar.php', {
		action :'daily'
	}, function(data) {
		$('#content').append(data);
	});
}

function ShowAjaxForm(form_location, params, send_data) {

	if(form_location.search('action=edit') == -1) {
		var show_form_action = 'add';
	}
	else {
		var show_form_action = 'edit';
	}
	
	$("#cluetip").hide();
	
	form_location = 'ajax/ajax.GetModule.php?form=true&module=Module' + form_location;
	
	// Need this to handle using editor in EditOpp
	if (document.URL.indexOf('legacy') > 0) {
		form_location = '../../' + form_location;
	}
		
	if (undefined !== window.this_account_id) {
		form_location += "&accountId=" + this_account_id;
	}
	BusyOn();
	$.post(form_location,
		send_data,
		function(data) {
			var ajax_form = $(data);
			$('#content').append(ajax_form);
			user_modified_arr = new Array();
			ShowForm(ajax_form.attr('id'), show_form_action);
			BusyOff();
		}
	);
}

function ShowForm(form_id, form_action) {
	
	form = $('#' + form_id).find("form").parent();
	
	// set an Initial check point of form data
	// which will be checked when user will
	// cancel the form
	form_data = GetFormData(form);
	if (form_action == 'edit') {
		setEditFormModifiedFieldArray(form);
	}
	$("input").attr("disabled", "");
	$("select").attr("disabled", "");

	blockUI(form, '800px', '', (jQuery(window).width() - 800) / 2 + 'px', 100);

	
	$.each(form.find("input[name^='Date']"), function() {
		var range = '-80:+10';
		if ($(this).attr('Range') != undefined && $(this).attr('Range') != '') {
			range = $(this).attr('Range');
		}
		$(this).datepicker( {
			showOn :"both",
			buttonImage :"./images/calendar.gif",
			yearRange :range,
			buttonImageOnly :true
		});

	});

	$("#Contact").autocomplete({
		source: function(request, response){
			$.ajax({
				url: "./legacy/data/contact_search.php",
				dataType: "json",
				data: {
					term: request.term
				},
				success: function( data ) {
					response( $.map( data, function( item ) {
						return {
							label: item.Contact,
							value: item.Contact,
							id: item.ContactID
						}
					}));
				}
			});
		},
        select: function(event, ui) {
        	form.find("input[name^='Contact']").val(ui.item.id);
        }

	});

	$("#account").autocomplete({
		source: function(request, response){
			$.ajax({
				url: "./legacy/data/company_search.php",
				dataType: "json",
				data: {
					term: request.term
				},
				success: function( data ) {
					response( $.map( data, function( item ) {
						return {
							label: item.Account,
							value: item.Account,
							id: item.AccountID
						}
					}));
				}
			});
		},
        select: function(event, ui) {
        	form.find("input[name^='AccountID']").val(ui.item.id);
        }

	});

	/* This is for contact form.
	if ($("#account").length > 0) {
		form.find("input[name^='account']").keyup( function() {
			clearForm(form, 'contact');
		});
		form.find("input[name^='account']").autocomplete("./legacy/data/company_search.php", {
			width :260,
			selectFirst :false,
			extraParams : {
				fromcontact :true
			}
		});

		function findAccountValueCallback(event, data, formatted) {
			clearForm(form, 'contact');

			// data[1] returns the id.
			form.find("input[name^='AccountID']").val(data[1]);
			ProcessAutoComplete(data, form);
			clear_form = false;
		}
		//form.find("input[name^='account']").result(findAccountValueCallback).next().click();
	}

	//This is for company/account form. 
	if ($("#company_account").length > 0) {
		form.find("input[id^='company_account']").keyup( function() {
			clearForm(form, 'account');
		});
		form.find("input[id^='company_account']").autocomplete("./legacy/data/company_search.php", {
			width :260,
			selectFirst :false,
			extraParams : {
				fromcompany :true
			}
		});
		function findCompAccountValueCallback(event, data, formatted) {
			// data[1] returns the id.
			if (form.find("input[name^='ID']").length > 0) {
				form.find("input[name^='ID']").val(data[1]);
			}
			if (form.find("input[name^='FormType']").length > 0) {
				form.find("input[name^='FormType']").val('edit');
			}

			clearForm(form, 'account');
			ProcessAutoComplete(data, form);
			clear_form = false;
		}
		form.find("input[id^='company_account']").result(findCompAccountValueCallback).next().click();
	}
//	*/

	$(form.find("input")).change( function() {
		setUserModifiedFieldArray($(this).attr('name'), form);
	});
	$(form.find("input")).keyup( function() {
		setUserModifiedFieldArray($(this).attr('name'), form);
	});
	

	/* Event Form (Note Alert) */
	try {
		$("#event_hours").attr('disabled', 'disabled');
		$("#event_minutes").attr('disabled', 'disabled');
		$("#event_periods").attr('disabled', 'disabled');
	}
	catch(err){
		
	}
}

function setEditFormModifiedFieldArray(formobj) {
	formobj.find("input[type^='text']").each( function() {
		if ($(this).val() != undefined && $(this).val().replace(/^\s+|\s+$/g, '') != '') {
			setUserModifiedFieldArray($(this).attr('name'), formobj);
		}
	});
	formobj.find("textarea").each( function() {
		if ($(this).val() != undefined && $(this).val().replace(/^\s+|\s+$/g, '') != '') {
			setUserModifiedFieldArray($(this).attr('name'), formobj);
		}
	});
	formobj.find("select").each( function() {
		if ($(this).val() != undefined && $(this).val().replace(/^\s+|\s+$/g, '') != '') {
			setUserModifiedFieldArray($(this).attr('name'), formobj);
		}
	});
	formobj.find("checkbox").each( function() {
		if ($(this).val() != undefined && $(this).val()) {
			setUserModifiedFieldArray($(this).attr('name'), formobj);
		}
	});
	formobj.find("radio").each( function() {
		if ($(this).val() != undefined && $(this).val()) {
			setUserModifiedFieldArray($(this).attr('name'), formobj);
		}
	});
}

function ProcessAutoComplete(data, form) {
	for ( var obj in data) {
		if ((obj == 0) || (obj == 1)) {
			continue;
		}
		var fobj = form.find("input[name^='" + obj + "']");

		if (checkField(fobj)) {
			var fobj_type = fobj.attr('type');
			if ((fobj_type == "checkbox") || (fobj_type == "radio")) {
				if (data[obj]) {
					fobj.attr("checked", true);
				} else {
					fobj.attr("checked", false);
				}
			} else {
				fobj.val(data[obj]);
			}
		}
	}
}

function clearForm(formobj, formtype) {
	if (clear_form) {
		formobj.find("input[type^='text']").each( function() {
			if (checkField($(this))) {
				$(this).val('');
			}
		});
		formobj.find("textarea").each( function() {
			if (checkField($(this))) {
				$(this).val('');
			}
		});
		formobj.find("select").each( function() {
			if (checkField($(this))) {
				$(this).val('');
			}
		});
		formobj.find("checkbox").each( function() {
			if (checkField($(this))) {
				$(this).val('');
			}
		});
		formobj.find("radio").each( function() {
			if (checkField($(this))) {
				$(this).val('');
			}
		});

		if ((formtype == 'account') && (formobj.find("input[name^='FormType']").val() != 'edit')) {
			formobj.find("input[name^='ID']").val('');
		}
		else if (formtype == 'contact') {
			formobj.find("input[name^='AccountID']").val('');
		}
	}
	clear_form = true;
}

function checkField(obj) {
	if (obj.attr('id') == 'company_account') {
		return false;
	}
	if (obj.attr('id') == 'account') {
		return false;
	}
	if (obj.attr('name')) {
		no_entry = false;
		for ( var temp = 0; temp <= user_modified_arr.length; temp++) {
			if (user_modified_arr[temp] == obj.attr('name')) {
				var no_entry = true;
			}
		}
		if (no_entry) {
			return false;
		}
	}

	return true;
}

function SubmitForm(form) {
	BusyOn();
	var form = $(form);

	$.post(form.attr("action"), form.serialize(), function(data) {
		if (data && data.status.STATUS == 'OK') {
			//$.ajax({
				   //type: "POST",
				   //url: "ajax/ajax.checkGoogle.php",
				   //success: function(msg){
				   //}
			//});
			$('.form_error').hide();
			if (data.dest) {
				if (Get('action') == 'email_hold') {
					var row = $("body").data('CurrentRow');
					$('#' + row).fadeOut('slow', function() {
						$(this).remove();
					});
					BusyOff();
					unblockUI(form.parent());
				}
				else {
					top.location = data.dest;
				}
			}
			else if (data.modules) {
				if (data.subnav_items != undefined) {
					$('#subnav_items').html(data.subnav_items);
					$('#subnav').show();
				}
				else {
					$('#subnav').hide();
				}

				if (data.main_title != undefined) {
					$('#main_title').html(data.main_title).show();
				}
				else {
					$('#main_title').hide();
				}

				if (data.sub_title != undefined) {
					$('#sub_title').html(data.sub_title).show();
				}
				else {
					$('#sub_title').hide();
				}
				
				if (data.add == true) {
					UpdateModules(data.modules, true);
				}
				else {
					UpdateModules(data.modules);
				}

				unblockUI(form.parent());
				form.remove();
				BusyOff();
				//window.location.reload();
			}
			else if (data.event_data) {
				EditOppResultProc(data.event_data, data.status.RECID);
				BusyOff();
			}
			
		}
		else {
			form.find("[id^='err_']").hide();
			for ( var field in data.status.ERROR) {
				form.find('#err_' + field).html(data.status.ERROR[field]);
				form.find('#err_' + field).show();
			}
			BusyOff();
		}

		}, "json");

	return false; // Stops HTML from sutmitting the form
}

function UpdateModules(modules, clear) {
	// Clear removes all modules from content and sidebar
	if (clear == true) {
		$('#content_modules').html('');
		$('#sidebar_modules').html('');
	}
	
	
	for (var module_id in modules) {
		if (module_id.length == 0) continue;
		if ($('#' + module_id).length == 0) {
			// We need to create a new div to hold the module
			if (modules[module_id]['location'] == 'sidebar') {
				$('#sidebar_modules').append('<div id="' + module_id + '" />');
			}
			else {
				$('#content_modules').append('<div id="' + module_id + '" />');
			}
		}
		$('#' + module_id).html(modules[module_id]['html']);
	}
}


function CancelForm(form) {
	cur_data = GetFormData($(form).parent());
	if (IsModified(form_data, cur_data)) {
		if (IfClosing()) {
			RestoreData($(form).parent(), form_data);
			unblockUI($(form).parent());
			$(form).remove();
		}
	} else {
		unblockUI($(form).parent());
		$(form).remove();
	}
}

/*
 * Hides a DOM element identified by the id with a jQuery fast slide. Also,
 * switches an image with the name toggle_ and the same id from a - to a + and
 * back.
 */
function ToggleHidden(id) {
	if ($('#' + id).css('display') == 'none' || $('#' + id).css('display') == '') {
		$('#' + id).slideDown("slow");
		$('#toggle_' + id).attr('src', 'images/minus.gif');
//		$.cookie(id, 1);
	} else {
		$('#' + id).slideUp("fast");
		$('#toggle_' + id).attr('src', 'images/plus.gif');
//		$.cookie(id, null);
	}
}

function MarkActive(type, id, dest) {
	if (window.confirm("Are you sure you want to mark this record " + dest + "?")) {
		$.post('ajax/ajax.CompanyContact.php', {
			type :type,
			action :dest,
			id :id
		}, function(result) {
		/*Start Jet-37 For Remove Contact */
		if(dest = 'inactive'){
		$.post('ajax/ajax.CompanyContactGooglenew.php',
			{action :dest,id :id},function(res){
				alert('Contact Mark Inactive Successfully');
				});
				setTimeout(window.location.reload(), 3000);
			}else{
				window.location.reload();
				}
			/*End Jet-37 For Remove Contact */
			
		});
	}
}

function setUserModifiedFieldArray(field_name, form) {
	var indx = user_modified_arr.length;
	var no_entry = false;

	if (user_modified_arr.length == 0) {
		user_modified_arr[indx] = field_name;
	} else {
		for ( var temp = 0; temp < user_modified_arr.length; temp++) {
			
			if (form.find("input[name^='" + field_name + "']").length > 0) {
				var fobj_val = form.find("input[name^='" + field_name + "']").val();
			
				if (user_modified_arr[temp] == field_name) {
						if ((fobj_val == undefined) || fobj_val.replace(/^\s+|\s+$/g, '') == '') {
							user_modified_arr.splice(temp, 1);
						}					
					var no_entry = true;
				}
				else if ((fobj_val == undefined) || fobj_val.replace(/^\s+|\s+$/g, '') == '') {
					var no_entry = true;
				}
			}
		}
		
		if (!no_entry) {
			user_modified_arr[indx] = field_name;
		}
	}
}

function blinkPending() {
	$('#pending_event').toggleClass('pending_event');
}

function blinkPendingEmail() {
	$('#pending_email').toggleClass('pending_email');
}

function DoQuickSearch() {

	// If the user is already on the dashboard, just do the quick search
	if ($('#dashboardsearch').length > 0 && $('#SearchString').length > 0 && $("#opp_tracker").text() == '') {
		$('#SearchString').val($('#quicksearch').val());
		AjaxLoadPage(1);
		return false;
	}
	// Otherwise, do the default form action
	return true;
}

// To use Title as hint in text boxes
// From
// http://webservices.blog.gustavus.edu/2008/06/23/text-input-example-text-with-jquery/
function switchText() {
	if ($(this).val() == $(this).attr('title'))
		$(this).val('').removeClass('exampleText');
	else if ($.trim($(this).val()) == '')
		$(this).addClass('exampleText').val($(this).attr('title'));
}

// End Title Hint section

$(document).ready( function() {

	//checkLocation();
	
	$("#change_location").live('click', function(){
		showConfirmLocationDialog();
	});
		
	$('#firstChild').css( {
		marginLeft :0
	});
	// Running this line causes IE7 to forward to a blank white page
	// cssdropdown.startchrome("chromemenu");
	// Initiates the blinking if the pending event is set
	if ($('#pending_event').length > 0)
		setInterval('blinkPending()', 1000);

	if ($('#pending_email').length > 0)
		setInterval('blinkPendingEmail()', 1000);

	$('input[type=text][title!=""]').each( function() {
		if ($.trim($(this).val()) == '')
			$(this).val($(this).attr('title'));
		if ($(this).val() == $(this).attr('title'))
			$(this).addClass('exampleText');
	}).focus(switchText).blur(switchText);

	$('form').submit( function() {
		$(this).find('input[type=text][title!=""]').each( function() {
			if ($(this).val() == $(this).attr('title'))
				$(this).val('');
		});
	});
	
	$("#toggle_dashboard_search").click(function(){
		$("#dashboardsearch").slideToggle();
		if($(this).attr('src') == 'images/minus-med.gif'){
			$(this).attr('src', 'images/plus-med.gif');
		}
		else {
			$(this).attr('src', 'images/minus-med.gif');
		}
	});
	$("#SlipstreamSearch").submit(function(){
		$("#dashboardsearch").show();
		if($("#toggle_dashboard_search").attr('src') == 'images/plus-med.gif'){
			$("#toggle_dashboard_search").attr('src', 'images/minus-med.gif');
		}

	});
	
	$("#SlipstreamSearch").submit(function(){
		$("#cluetip").hide();
	});
	
});

function checkLocation(){
	$.ajax({
		   type: "POST",
		   url: "ajax/ajax.checkLocation.php",
		   success: function(msg){
			   if(msg.length == 0){
				   showConfirmLocationDialog();
			   }
		   }
	});
}

function showConfirmLocationDialog(){

	getLocationChooser();

	$('#confirm_location').dialog({
		title: 'Set Your Timezone',
		modal: true,
        width: 300,
        height: 300,
        closeOnEscape: false,
        draggable: false,
        resizable: false,
        open: function(event, ui) {
        	$(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
        	"Continue": function() {
        		if($("#location_chooser").val() == ''){
        			$("#location_error").text('Please make a selection.');
        			$("#location_error").show();
        		}
        		else {
        			setLocation($("#location_chooser").val());
                    $(this).dialog("close"); 
        		}
            }
        }
	});
}

function setLocation(location_id){
	$.ajax({
		   type: "POST",
		   url: "ajax/ajax.setLocation.php",
		   data: "location=" + location_id,
		   success: function(msg){
			   $("#zone").html(msg);
		   }
	});
}


function getLocationChooser(){
	$.ajax({
		   type: "POST",
		   url: "ajax/ajax.getLocationChooser.php",
		   success: function(msg){
			   if(msg.length > 0){
				   $("#confirm_location").html(msg);
			   }
			   else {
				   $("#confirm_location").html('');
			   }
		   }
	});
	
}

function Get(name) {
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regexS = "[\\?&]" + name + "=([^&#]*)";
	var regex = new RegExp(regexS);
	var results = regex.exec(window.location.href);
	return (results == null) ? "" : results[1];
}

// Corrects an issue with IE7 opacity when using jQuery fade functions
jQuery.fn.fadeIn = function(speed, callback) {
	return this.animate( {
		opacity :'show'
	}, speed, function() {
		if (jQuery.browser.msie)
			this.style.removeAttribute('filter');
		if (jQuery.isFunction(callback))
			callback();
	});
};

jQuery.fn.fadeOut = function(speed, callback) {
	return this.animate( {
		opacity :'hide'
	}, speed, function() {
		if (jQuery.browser.msie)
			this.style.removeAttribute('filter');
		if (jQuery.isFunction(callback))
			callback();
	});
};

jQuery.fn.fadeTo = function(speed, to, callback) {
	return this.animate( {
		opacity :to
	}, speed, function() {
		if (to == 1 && jQuery.browser.msie)
			this.style.removeAttribute('filter');
		if (jQuery.isFunction(callback))
			callback();
	});
};

function ParseStatus(result) {
	return JSON.parse(result);
}

function NoBubble(e, link) {
	if (!e) var e = window.event;
	e.cancelBubble = true;
	if (e.stopPropagation) {
		e.stopPropagation();
	}

	if (link) {
		window.location = link;
	}
}

/*Start jet-27*/
var eventInterval = 3000;

$(function(){
	setInterval('checkEventAlert()', eventInterval);
});

function checkEventAlert(){
	if($(".event_alerts").dialog("isOpen") != true){
		$.ajax({
				   type: "POST",
				   url: "ajax/ajax.checkEventAlert.php",
				   success: function(msg){
					   var obj = $.parseJSON(msg);
					   if(obj != undefined){
						   for (i = 0; i < obj.output.length; i++) {
								$("#event_alerts"+i).html(obj.output[i]);
								showEventAlerts(i);
								var d_id = i+1;
								$( "<div id='event_alerts"+d_id+"' style='display: none;'></div>"  ).insertAfter( "#event_alerts"+i );
							}

					   }
					   else {
						   $(".event_alerts").html('');
					   }
				   }
			});
		}
	
	}
	function showEventAlerts(d_id){
	$('#event_alerts'+d_id).dialog({
		title: 'Event Alert!',
		modal: true,
		width: 700,
		height: 300,
		dialogClass: 'eventpopcss',
		show:{
			effect:"pulsate", 
			duration: 300, 
			easing:"easeOutExpo"
		},
		hide:{
			effect:"drop", 
			direction:"down", 
			distance:100, 
			duration:500, 
			easing:"easeOutExpo"
		},
		buttons: {
	        "Dismiss": function() {
				var Attid = $(this).find(".alert_EventAttendee_cl").val();
	           dismissEventAlert(d_id,Attid);
	         //  alert($(this).find(".alert_EventAttendee_cl").val());//url("../images/table_hdr_tile.jpg") repeat-x scroll 0 0 rgba(0, 0, 0, 0)
	            $(this).dialog("close"); 
	        } 

        }
	}).prev(".ui-widget-header").css({'background':'url("images/table_hdr_tile.jpg") repeat-x scroll 0 0 rgba(0, 0, 0, 0)','border':'1px solid #008ae6'});//.ui-widget-header
}

function dismissEventAlert(d_id,Attid){


		$.ajax({
			   type: "POST",
			   url: "ajax/ajax.dismissEventAlert.php",
			   data: "id=" + Attid,
			   success: function(msg){
				   
				   
				   
				   }
		});
			if(d_id != 0){
							$('#event_alerts'+d_id).remove();
						}
			
	}
/*End jet-27*/
