var cal_date = '';
// var prev_id = '';

function GetCalendar(action, date, target, fake, toggle_view) {
	cal_date = date;
	document.cookie = 'Cal_Type=' + action;
	document.cookie = 'Cal_Day=' + date;
	
	if(typeof toggle_view != 'undefined') {
		var str_toggle_view = toggle_view;
		document.cookie = 'TOGGLE_VIEW=' + str_toggle_view;
	} else {
		var str_toggle_view = '';
	}
	$("#cluetip").hide();
	
	// Intentionally leaving out 'target' causes it to use previously selected target
	GetAjaxCal({action: action, day: date, str_toggle_view: toggle_view});
}

function GetAjaxCal(params) {
	// cal_date = date;
	$('#calendar').fadeTo("fast", .1);

	$.get('ajax/ajax.GetCalendar.php', params, FillCalendar);
}

function FillCalendar(data) {
	// Added removing filter attr to solve issue with IE7 cleartype
	$('#calendar').html(data).fadeTo("fast", 1);
//	prev_id = '';	
}

function AlignSalesPerson(){
	sales_person=$('.opp');
	max_width=150;
	if(sales_person.length>0)
	{
		for(i=0;i<sales_person.length;i++)
		{
			person=$('#'+sales_person[i].id);			
			if(max_width<person.width())			
				max_width=person.width();			
		}		
	}
	sales_person.css({width:max_width});	
}

function PagePosition_remove(object) {

}

function GetTask_remove(date,key,target,cal_type) {	
	if ($('#div_' + key).attr('loaded') != 'true') {

		if(prev_id == '') {
			prev_id = key;
		} else if(prev_id != key) {
			$('#list_'+prev_id).hide();
			prev_id = key;
		} else if (prev_id == key) {
			return;
		}

		$.get('ajax/ajax.GetTask.php', {date: date},
			function(data) {
				$('#list_' + key).html(data);
				$('#div_' + key).attr('loaded','true');

				cur_div = $('#list_' + key);

				offset = $('#div_' + key).offset({ border: true, padding: true, margin:true});

				left_pos = offset.left;
				
				if (cal_type == 'daily') {
					width = $('#div_' + key).width();
					left_pos = left_pos + $('#div_' + key).width() / 2 - $('#list_' + key).width() / 2;
				}

				cur_div.css({left:left_pos, display:'block'});

				if ($.browser.msie && cal_type == 'monthly') cur_div.css({'margin-top':'-3px'});

				cur_div.show()
					.mouseover(function(){cur_div.show();})
					.mousemove(function(kmouse){cur_div.show();})
					.mouseout(function(){cur_div.hide();});
			}
		);
	} else {
		cur_div = $('#list_' + key);
		cur_div.show();
	}
}

//function HideMergedCalendarForm() {
//	$('#secMergeCal').slideUp('slow');		
//	$('#secMergeCal').attr('show',0);	
//	$('.mergeCal').attr('checked',false);
//	$('.mergeCal').removeClass('mergeCal');
//}

function GetMergedCalendar() {
	mergedStr = '';
	
	$("input[id^='chk_']").each(function() { 			
		if(this.checked) {
			mergedStr = (mergedStr == '') ? $(this).attr('person_id') : mergedStr + ',' + $(this).attr('person_id');
		}
	}); 

	$("#cluetip").hide();
	GetAjaxCal({action: $('#cal_type').val(), day: cal_date, target: mergedStr});
}

// Fires whenever one of the people checkboxes is checked
function AddMergeClass(chk) {
//	if(chk.attr('checked'))	
//		chk.addClass('mergeCal');	
//	else
//		chk.removeClass('mergeCal');
		
	GetMergedCalendar();	
}

function CheckAllCal() {
	if ($('#chkbx_all').is(':checked')) {		
		$("input[id^='chk_']").each(function() { this.checked = true; }); 
	} else {		
		$("input[id^='chk_']").each(function() { this.checked = false; }); 			
	}
	GetMergedCalendar();
}

function GetCalendarTime(event, doc, date, offset) {
	var y;
	if (event.y != undefined) {
		y = event.y + doc.getElementById('week_cal').scrollTop;
	} else {
		y = event.clientY - doc.getElementById('week_cal').offsetTop + doc.getElementById('week_cal').scrollTop + doc.documentElement.scrollTop;
	}
//	return y;
//	y = event.y + doc.getElementById('week_cal').scrollTop;
	time = (y - 2)/44;

	hour = time - time % 1 + offset;
	if (hour > 23) {
		var d = new Date(date);
		d.setDate(d.getDate() + 1);
		date = d.getMonth().toString() + '/' + d.getDate().toString() + '/' + d.getFullYear().toString();
	}
	if (hour < 12 || hour > 23) ap = ' am';
	else ap = ' pm';
	if (hour < 1) hour = 12;
	if (hour > 12) hour -= 12;

	min = ((time % 1) * 60).toFixed();
	if (min >= 0 && min < 15) strMin = '00';
	if (min >= 15 && min < 30) strMin = '15';
	if (min >= 30 && min < 45) strMin = '30';
	if (min >= 45 && min < 60) strMin = '45';

	strDate = date.toString() + ' ' + hour.toString() + ':' + strMin + ap;
	return  strDate;
}
