function f_filterResults(n_win, n_docel, n_body) {
	var n_result = n_win ? n_win : 0;
	if (n_docel && (!n_result || (n_result > n_docel))) {
		n_result = n_docel;
	}
	return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
}

function f_clientWidth() {
	return f_filterResults (window.innerWidth ? window.innerWidth : 0, document.documentElement ? document.documentElement.clientWidth : 0, document.body ? document.body.clientWidth : 0);
}

var DashColumnSizer = {
	spacer_total : 0,
	spacers : [],
	SetColumnWidths : function() {
		var my_width = f_clientWidth();
		// spacers.length * right margin + table margin? + spacers between columns
		my_width -= this.spacers.length * 1 + 7 + 20;
		if (my_width < 900) {
			my_width = 900;  // Set a minimum width for the swimlanes
		}
		var col_width = Math.floor(my_width / this.spacer_total);
		for (var spacer in this.spacers) {
			this.spacers[spacer].width = col_width * this.spacers[spacer].modifier;
		}
	},
	init : function() {
		var count = 0;
		var spacer_cols = [10, 1, 3, 6.3];
		var people_count = 0;
		for (var col in spacer_cols) {
			var row = document.getElementById("ph" + spacer_cols[col]);
			people_count = count;
			count = 0;
			for (var i = 0; i < row.childNodes.length; i++) {
				if (row.childNodes[i].nodeType != 1) {
					continue;
				}
				var spacer = new Image();
				spacer.src = 'images/spacer.gif';
				spacer.height = '10';
				spacer.alt = '';
				spacer.col = spacer_cols[col];
				if ((spacer_cols[col] == 3 && count < people_count) || dash_type == 'detail') {
					spacer.modifier = 1;
				} else if (spacer_cols[col] == 3 || spacer_cols[col] == 6.3) {
					spacer.modifier = 0.4;
				} else {
					spacer.modifier = 0.4;
				}
				this.spacer_total += spacer.modifier;
				
				row.childNodes[i].appendChild(spacer);
				this.spacers.push(spacer);
				count++;
			}
		}
		this.SetColumnWidths();
		window.onresize = function() {DashColumnSizer.SetColumnWidths();};
	}

};
