// Modified to also allow the passing in of jQuery objects
// Will also now create an overlay div if none is present
function blockUI(id, width, height, left, top) {
	if (!$('#overlay').length) {
		if ($.browser.msie) {
			$('#content').append('<iframe src="overlay.html" id="overlay" class="overlay"></iframe>');
		} else {
			$('#content').append('<div id="overlay" class="overlay" />');
		}
	}

	if (id) {
		if (id.css) { // Checks to see if id is a jquery object
			var display = id;
		} else {
			var display = $('#' + id); // Else assume it is a string
		}

		if (width != '') {
			display.css('width', width);
		}
		if (height != '') {
			display.css('height', height);
		}
		if (left != '') {
			display.css('left', left);
		}
		if (top != '') {
			display.css('top', top);
		}

		display.addClass("overlay_form");
		display.show();
		
		var overlay = $('#overlay');

		var overlay_height = $(document).height();
		var window_height = $(window).height();
		
		if (window_height > overlay_height) {
			overlay_height = window_height;
		}
		
		overlay_height += 500;
		
		overlay.css('height', overlay_height + "px");
		overlay.css('width', $(window).width() + "px");
		overlay.show();
	}
}

function unblockUI(id) {
	$('#overlay').hide();
	if (id) {
		if (id.css) { // Checks to see if id is a jquery object
			var display = id;
		} else {
			var display = $('#' + id); // Else assume it is a string
		}
		display.hide();
	}
}

function BusyOn() {
return true;
	if (!$('#busy').length) {
		if ($.browser.msie) {
			$('#content').append('<iframe src="overlay.html" id="busy" class="overlay"></iframe>');
		} else {
			$('#content').append('<div id="busy" class="overlay" />');
		}
	}
	if (!$('#busy_spinner').length) {
		$('#content').append('<div id="busy_spinner" class="busy"><div></div></div>');
	}

	var overlay = $('#busy');

	var overlay_height = $(document).height();
	var window_height = $(window).height();
	if (window_height > overlay_height) {
		overlay_height = window_height;
	}
	overlay.css('height', overlay_height + "px");
	overlay.css('z-index', 3000);
	overlay.css('width', $(window).width() + "px");
	overlay.show();

	// Spinner height = 51px x 51px /2 = (25 x 25)
	var spinner = $('#busy_spinner');
	spinner.css('top', ($(window).height() / 2 - 100 + $(window).scrollTop()));
	spinner.css('right', ($(window).width() / 2 - 25))
	spinner.css('z-index', 3001);
	spinner.show();
}

function BusyOff() {
	$('#busy').hide();
	$('#busy_spinner').hide();
}
