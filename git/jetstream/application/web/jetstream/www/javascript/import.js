function selectAllRadio() {
	if ($('#sel_all').attr('checked')) {
		$("input:radio.new").attr('checked', true);
	} else {
		$("input:radio.current").attr('checked', true);
	}
}

function allChecked(){

	if($("input:radio[name='unique[0]']:checked")){
		alert('all checked');
	}
	else {
		alert('not all checked');
	}
}

function countUnchecked() {
	
	var radio = $("input:radio:unchecked");
	
    var n = $("input:radio[name^='unique']:unchecked").length;
    $("#out").text(n + (n == 1 ? " is" : " are") + " unchecked!" + radio);
    
    $.each(radio, function(index, value){
    	$("#out").append( index + ': ' + $(value).attr("class") + '<br>');
    });
}

function BusyOnTest() {
	if (!$('#busyTest').length) {
		if ($.browser.msie) {
			$('#content').append('<iframe src="overlay.html" id="busyTest" class="overlay"></iframe>');
		} else {
			$('#content').append('<div id="busyTest" class="overlay" />');
		}
	}
	if (!$('#busy_spinner').length) {
		$('#content').append('<div id="busy_spinner" class="busyTest">Searching for existing Companies. Please wait...<div></div></div>');
	}

	var overlay = $('#busyTest');

	var overlay_height = $(document).height();
	var window_height = $(window).height();
	if (window_height > overlay_height) {
		overlay_height = window_height;
	}
	overlay.css('height', overlay_height + "px");
	overlay.css('z-index', 3000);
	overlay.css('width', $(window).width() + "px");
	overlay.show();

	// Spinner height = 51px x 51px /2 = (25 x 25)
	var spinner = $('#busy_spinner');
	spinner.css('top', ($(window).height() / 2 - 300 + $(window).scrollTop()));
	spinner.css('right', ($(window).width() / 2 - 250))
	spinner.css('z-index', 3001);
	spinner.show();
}

$(function() {
	$("select").addClass('required');
	$("#map").validate();
});