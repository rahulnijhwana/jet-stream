$(document).ready( function() {
	
	// Hide all subfolders at startup
	$(".php-file-tree").find("UL").hide();
	
	// Expand/collapse on click
	$(".pft-directory A").click( function() {
		$(this).parent().find("UL:first").slideToggle("medium");
		
		if($(this).find("img:first").attr('src') == 'images/plus.gif')
		$(this).find("img:first").attr('src', 'images/minus.gif');
		else
		$(this).find("img:first").attr('src', 'images/plus.gif');
		
		if( $(this).parent().attr('className') == "pft-directory" ) return false;
	});
});


