var interval = 30000;

/*Start jet-27*/
//var eventInterval = 3000;
/*End jet-27*/

$(function(){
	setInterval('checkAlerts()', interval);
});

/*Start jet-27*/
//$(function(){
	//setInterval('checkEventAlert()', eventInterval);
//});

//function checkEventAlert(){
	//if($(".event_alerts").dialog("isOpen") != true){
		//$.ajax({
				   //type: "POST",
				   //url: "ajax/ajax.checkEventAlert.php",
				   //success: function(msg){
					   //var obj = $.parseJSON(msg);
					   //if(obj != undefined){
						   //for (i = 0; i < obj.output.length; i++) {
								//$("#event_alerts"+i).html(obj.output[i]);
								//showEventAlerts(i);
								//var d_id = i+1;
								//$( "<div id='event_alerts"+d_id+"' style='display: none;'></div>"  ).insertAfter( "#event_alerts"+i );
							//}

					   //}
					   //else {
						   //$(".event_alerts").html('');
					   //}
				   //}
			//});
		//}
	
	//}
	//function showEventAlerts(d_id){
	//$('#event_alerts'+d_id).dialog({
		//title: 'Event Alert!',
		//modal: true,
		//width: 700,
		//height: 300,
		//dialogClass: 'eventpopcss',
		//show:{
			//effect:"pulsate", 
			//duration: 300, 
			//easing:"easeOutExpo"
		//},
		//hide:{
			//effect:"drop", 
			//direction:"down", 
			//distance:100, 
			//duration:500, 
			//easing:"easeOutExpo"
		//},
		//buttons: {
	        //"Dismiss": function() {
				//var Attid = $(this).find(".alert_EventAttendee_cl").val();
	           //dismissEventAlert(d_id,Attid);
	         ////  alert($(this).find(".alert_EventAttendee_cl").val());//url("../images/table_hdr_tile.jpg") repeat-x scroll 0 0 rgba(0, 0, 0, 0)
	            //$(this).dialog("close"); 
	        //} 

        //}
	//}).prev(".ui-widget-header").css({'background':'url("images/table_hdr_tile.jpg") repeat-x scroll 0 0 rgba(0, 0, 0, 0)','border':'1px solid #008ae6'});//.ui-widget-header
//}

//function dismissEventAlert(d_id,Attid){


		//$.ajax({
			   //type: "POST",
			   //url: "ajax/ajax.dismissEventAlert.php",
			   //data: "id=" + Attid,
			   //success: function(msg){
				   
				   
				   
				   //}
		//});
			//if(d_id != 0){
							//$('#event_alerts'+d_id).remove();
						//}
			
	//}
/*End jet-27*/

function checkAlerts(){
	if($("#note_alerts").dialog("isOpen") != true){
		$.ajax({
			   type: "POST",
			   url: "ajax/ajax.checkAlerts.php",
			   success: function(msg){
				   var obj = $.parseJSON(msg);
				   if(obj != undefined){
					   if(obj.output.length > 0){
						   $("#note_alerts").html(obj.output);
						   showAlerts();
					   }
				   }
				   else {
					   $("#note_alerts").html('');
				   }
			   }
		});
	}
}

function dismissAlert(){
	var alert_recipient_id = $("#alert_recipient_id").attr('value');
	$.ajax({
		   type: "POST",
		   url: "ajax/ajax.dismissAlert.php",
		   data: "id=" + alert_recipient_id,
		   success: function(msg){}
	});
}

function snoozeAlert(){
	var alert_recipient_id = $("#alert_recipient_id").attr('value');
	var snooze_option = $("#snoozer option:selected").val();
	$.ajax({
		   type: "POST",
		   url: "ajax/ajax.snoozeAlert.php",
		   data: "id=" + alert_recipient_id + '&snooze=' + snooze_option,
		   success: function(msg){}
	});
}

function showNext(){
	console.log('getting next note alert');
	$(".show").hide();
	var next = $(".show").next('.hide');
	next.addClass('.show');
	
}

function showPrevious(){
	console.log('getting previous note alert');
	$(".show").hide();
}


function showAlerts(){
	
	$('#note_alerts').dialog({
		title: 'Note Alert!',
		modal: true,
		width: 700,
		height: 500,
		show:{
			effect:"pulsate", 
			duration: 300, 
			easing:"easeOutExpo"
		},
		hide:{
			effect:"drop", 
			direction:"down", 
			distance:100, 
			duration:500, 
			easing:"easeOutExpo"
		},
		buttons: {
			"Snooze": function() { 
			snoozeAlert();
			$(this).dialog("close"); 
		    },
            /*
        	"Previous": function() { 
        		showPrevious();
            },
            "Next": function() { 
            	showNext();
            },
            */
	        "Dismiss": function() { 
	            dismissAlert();
	            $(this).dialog("close"); 
	        } 

        }
	});
	$.ajax({
		   type: "GET",
		   url: "ajax/ajax.snoozeOptions.php",
		   success: function(msg){
				$(".ui-dialog-buttonpane").append(msg);
		   }
	});
}

function showNoteAlertHelp(){
	try {
		$('.note_alert_help').cluetip({
			width: '300px',
			showTitle: false,
			cluetipClass: 'jtip',
			arrows: true,
			dropShadow: false,
			cursor: 'pointer',
			positionBy: 'fixed',
			leftOffset: '35px',
			topOffset: '15px'
		});
	}
	catch(err){
		//just for the pages that do not contain the #help div
	}
}








