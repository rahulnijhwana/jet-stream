var recType = 'EVENT';
var virtualEventCloseDate = '';

$(document).ready( function() {
	$('#addNote').hide();
	$('#addNewEvent').hide();
	//$(window).resize(function(){
	//	ResetCalTooltips();
	//});
});

var loadFromEditOpp = false;
if (undefined !== window.editOppPage) {
	var loadFromEditOpp = true;
	
}

function LoadRptngEvnt(event_id, event_ref) {
	// For repeating events, we need to determine if they want to load the series or the instance

	var prompt = "You clicked on a repeating event.  Did you want to open this event or the repeating series of events?";
	
	var mybuttons = {};
	mybuttons['This Event'] = 'one';
	mybuttons['the Repeating Series'] = 'series';
	
	$.prompt(prompt, {
		buttons: mybuttons,
		submit: function(v, m, f) {
			if (v == 'one') {
				BusyOn();
				location.href = "?action=event&rep=" + event_ref;
			}
			if (v == 'series') {
				BusyOn();
				location.href="?action=event&eventId=" + event_id;
			}
		}
	});
//	
//	$('#content').append("<div id='RptngEvnt'>You clicked on a repeating event.  You can:<br />" +
//			"<input type='button' value='Open this Event' style='width:300px;' onclick='location.href=\"?action=event&rep=" + event_ref + "\"' /><br />" +
//			"<input type='button' value='Open the Series of Repeating Events'  style='width:300px;' onclick='location.href=\"?action=event&eventId=" + event_id + "\"' /></div>"
//	);
//
//
//	blockUI('RptngEvnt', '40%', '', '30%', '10%');
	
	// set an Initial check point of
	// form data which will be checked
	// when user will cancel the
	// form(shell.js)
}

//
// try {
// nextMeetingEdited = false;
// if (editOppPage)
//
// } catch (err) {
// }

//function editEvent(id, type, oppid, personId, date_ts) {
//	if (oppid != '' && oppid > 0) {
//		openOppPopup(oppid, personId)
//	} else {
//
//		recType = type;
//		//alert(date_ts)
//		addEvent(0, 'edit', id, 0, date_ts);
//		
//	}
//}

//function addEvent(cid, sub_action, id, opp_id, date) {
//	if (sub_action == undefined) {
//		var sub_action = 'add';
//	}
//	if ((opp_id != undefined) && ((opp_id > 0) || (opp_id == -1))) {
//		var str_opp = '&oppId=' + opp_id;
//	} else {
//		var str_opp = '';
//	}
//	if (id > 0) {
//		var eid = id;
//	}
//
//	
//	if (loadFromEditOpp) {
//		Formurl = '../../ajax/ajax.LoadEventForm.php?action=event&sub_action=' + sub_action + '&eventId=' + eid
//				+ str_opp;
//	} else {
//		if (recType == 'EVENT')
//			Formurl = 'ajax/ajax.LoadEventForm.php?action=event&sub_action=' + sub_action + '&eventId=' + eid + str_opp;
//		else
//			Formurl = 'ajax/ajax.LoadEventForm.php?action=task&sub_action=' + sub_action + '&eventId=' + eid + str_opp;
//	}
//
//	if (cid > 0) {
//		Formurl += '&contactId=' + cid;
//	}
//	
//	if(date != '') {
//		Formurl += '&date='+date;
//	}
//
//	$
//			.post(
//					Formurl,
//					function(data) {
//						if (!$('#addNewEvent').length) {
//							var eventformdiv = '<div id="addNewEvent" style="display:none;" class="overlay_form" onclick="javascript:hideDurationDiv();"></div>';
//							$('#content').append(eventformdiv);
//						}
//						$('#addNewEvent').html(data);
//						var form = $('#addNewEvent');
//						$("input").attr("disabled", "");
//						$("textarea").attr("disabled", "");
//						$("select").attr("disabled", "");
//						toggleEventRow();// For Follow On Task
//						/*
//						 * jQuery.blockUI({message: $('#saveEvent') , css: {
//						 * width: '60%', left : '20%', top: '10%'}});
//						 */
//						/*
//						 * This function is used to show the lightbox, which
//						 * uses following parameter. 1: id ~ Id of the div in
//						 * which the form is present. 2: width, 3: height, 4:
//						 * left position, 5: top position
//						 */
//						form_data = GetFormData(form);// set an Initial check
//						// point of form data
//						// which will be checked
//						// when user will cancel
//						// the form(shell.js)
//						blockUI(form, '750px', '', (jQuery(window).width() - 750) / 2 + 'px', 100);
//
//						
//
//						if ($("#Contact").length) {
//							if (loadFromEditOpp) {
//								var url = "../data/contact_search.php";
//								$('#repeate_row').hide();
//							} else {
//								var url = "./legacy/data/contact_search.php";
//							}
//
//							$("#Contact")
//									.keyup(
//											function() {
//												if ($('#CheckPrivate').length > 0) {
//													if (!$('#CheckPrivate').attr('checked')) {
//														if ($('#PrivateContact').length > 0) {
//															$('#PrivateContact')
//																	.html(
//																			'<input name="CheckPrivate" id="CheckPrivate" type="checkbox" value="true" />');
//														}
//													}
//												} else {
//													if ($('#PrivateContact').length > 0) {
//														$('#PrivateContact')
//																.html(
//																		'<input name="CheckPrivate" id="CheckPrivate" type="checkbox" value="true" />');
//													}
//												}
//											});
//
//							$("#Contact").autocomplete(url, {
//								width :260,
//								selectFirst :false
//							});
//
//							function findContactValueCallback(event, data, formatted) {
//								if (data[1] > 0) {
//									$('#ContactID').val(data[1]);
//									if (data['PrivateContact'] != 'undefined') {
//										if ($('#PrivateContact').length > 0) {
//											if (data['PrivateContact'] == 1) {
//												$('#PrivateContact')
//														.html(
//																'<img src="images/checkbox_checked.png"><input name="CheckPrivate" id="CheckPrivate" type="hidden" value="true" />');
//											} else {
//												// $('#PrivateContact').html('<input
//												// name="CheckPrivate"
//												// id="CheckPrivate"
//												// type="checkbox" value="true"
//												// />');
//												if ($('#CheckPrivate').length > 0) {
//													if (!$('#CheckPrivate').attr('checked')) {
//														if ($('#PrivateContact').length > 0) {
//															$('#PrivateContact')
//																	.html(
//																			'<input name="CheckPrivate" id="CheckPrivate" type="checkbox" value="true" />');
//														}
//													}
//												} else {
//													if ($('#PrivateContact').length > 0) {
//														$('#PrivateContact')
//																.html(
//																		'<input name="CheckPrivate" id="CheckPrivate" type="checkbox" value="true" />');
//													}
//												}
//											}
//										}
//									}
//								}
//							}
//
//							$("#Contact").result(findContactValueCallback).next().click();
//						}
//
//						if (loadFromEditOpp) {
//							$('img.ui-datepicker-trigger').attr('src', '../../images/calendar.gif');
//							$('#contact_row').hide();
//							// $('#btnClose').hide();
//							// $('#btnBlankOut').hide();
//							$('#user_row').hide();
//							// $('#Notes').attr('cols', '30');
//
//							if (firstMeeting) {
//								// load the first meeting event info
//								if (FMEventEditor['StartDate'] != '') {
//									$('#StartDate').val(FMEventEditor['StartDate']);
//								}
//								$('#ClockPick').val(FMEventEditor['StartTime']);
//								$('#Type').val(FMEventEditor['EventType']);
//								$('#Subject').val(FMEventEditor['Subject']);
//								$('#ClockDuration').val(FMEventEditor['Duration']);
//								$('#Notes').val(FMEventEditor['Description']);
//
//								if (FMEventEditor['FMEventIsClosed'] == 1) {
//									displayClosedEvent();
//								}
//
//							} else if (nextMeeting) {
//								// load the next meeting event info
//								var cat = getOppsCategory();
//
//								if (nextMeetingEventStatus == 'RESCHEDULED' || nextMeetingEdited
//										&& (NMEventEditor['NMEventIsClosed'] != 1)) {
//									if (NMEventEditor['NMEventIsClosed'] != 1 || nextMeetingEdited) {
//										if (NMEventEditor['StartDate'] != '')
//											$('#StartDate').val(NMEventEditor['StartDate']);
//										$('#Type').val(NMEventEditor['EventType']);
//										$('#ClockPick').val(NMEventEditor['StartTime']);
//										$('#Subject').val(NMEventEditor['Subject']);
//										$('#ClockDuration').val(NMEventEditor['Duration']);
//										$('#Notes').val(NMEventEditor['Description']);
//									}
//								}
//							}
//						}			
//						
//					});
//}

//function displayClosedEvent() {
//	$('#button_tr').hide();
//	$('#description_tr').hide();
//	$('#cancel_link').html('<a href="javascript:CancellDialog();">Cancel</a>');
//	$('select#Type').attr('disabled', 'true');
//	$('input#StartDate').attr('disabled', 'true');
//	$('input#ClockPick').attr('disabled', 'true');
//	$('input#Subject').attr('disabled', 'true');
//	$('input#ClockDuration').attr('disabled', 'true');
//}

//function saveEventToDb(SNID, stalled) {
//	if (loadFromEditOpp) {
//		var startDate = $('#StartDate').val();
//		var startTime = $('#ClockPick').val();
//		var eventType = $('#Type').val();
//		var subject = $('#Subject').val();
//		var duration = $('#ClockDuration').val();
//		var description = $('#Notes').val();
//		var is_next_meeting_editted = false;
//
//		var validate = true;
//		if (subject == '') {
//			$('#spn_Subject').html('&#32;' + 'Enter Subject').hide().fadeIn();
//			validate = false
//		} else {
//			$('#spn_Subject').html('');
//		}
//
//		if (eventType == '') {
//			$('#spn_Type').html('&#32;' + 'Please select the Event Type').hide().fadeIn();
//			validate = false
//		} else {
//			$('#spn_Type').html('');
//		}
//
//		if (startDate == '') {
//			$('#spn_StartDate').html('&#32;' + 'Enter the Start Date').hide().fadeIn();
//			validate = false
//		} else {
//			$('#spn_StartDate').html('');
//		}
//
//		if ($('#Type option:selected').attr('event_type') != 'TASK') {
//
//			if (startTime == '') {
//				$('#spn_ClockPick').html('&#32;' + 'Enter the Start Time').hide().fadeIn();
//				validate = false
//			} else {
//				$('#spn_ClockPick').html('');
//			}
//			if (duration == '') {
//				$('#spn_ClockDuration').html('&#32;' + 'Enter the Duration').hide().fadeIn();
//				validate = false
//			} else {
//				$('#spn_ClockDuration').html('');
//			}
//		}
//
//		if ($('#Type option:selected').attr('event_type') == 'TASK') {
//			alert('Task can not be created from Opportunity');
//			return;
//		}
//
//		if (!validate) {
//			return;
//		}
//
//		if (firstMeeting) {
//			FMEventEditor['Subject'] = subject;
//			FMEventEditor['EventType'] = eventType;
//			FMEventEditor['StartDate'] = startDate;
//			FMEventEditor['StartTime'] = startTime;
//			FMEventEditor['Duration'] = duration;
//			FMEventEditor['Description'] = description;
//			FMEventEditor['SaveEvent'] = true;
//
//			$('#firstMeeting').val(startDate);
//			$('#firstMeetingTime').val(startTime);
//			FMSaveEvent = true;
//
//		} else if (nextMeeting) {
//			NMEventEditor['Subject'] = subject;
//			NMEventEditor['EventType'] = eventType;
//			NMEventEditor['StartDate'] = startDate;
//			NMEventEditor['StartTime'] = startTime;
//			NMEventEditor['Duration'] = duration;
//			NMEventEditor['Description'] = description;
//			NMEventEditor['SaveEvent'] = true;
//
//			$('#nextMeeting').val(startDate);
//			$('#nextMeetingTime').val(startTime);
//		}
//
//		$('#addNewEvent').html('');
//		unblockUI('addNewEvent');
//
//		nextMeetingEdited = true;
//		g_somethingWasChanged = true;
//
//	} else {
//		var Formdata = $("#saveEvent").serialize();
//
//		var Formurl = 'ajax/ajax.Event.php?SN=' + SNID;
//		Formdata = Formdata + '&' + 'saveType=' + $("option:selected", "select#Type").attr("event_type");
//		$.post(Formurl, Formdata, function(data) {
//			if (data) {
//				processEventData(data);
//			}
//		});
//
//		$("input").attr("disabled", "disabled");
//		$("textarea").attr("disabled", "disabled");
//		$("select").attr("disabled", "disabled");
//	}
//}

///**
// * The function is called from the edit opps event dialog to process the event
// * closing status
// */
//function check_event(action, dialog) {
//	nextMeetingEventStatus = action;
//	var cat = getOppsCategory();
//
//	if (action == 'RESCHEDULED' || action == 'CLOSED' || action == 'CANCELLED') {
//		if (action == 'CLOSED' || action == 'CANCELLED') {
//			if (firstMeeting) {
//				fmMeetingStatus = action;
//				fmMeetingDialogNote = $('#eventNote').val();
//				FMEventEditor['Description'] = $('#eventNote').val();
//
//			} else if (nextMeeting) {
//				if ((FMEventEditor['FMEventID'] != '0' && FMEventEditor['FMEventIsClosed'] != '1')
//						|| NMEventEditor['StartDate'] == '') {
//					fmMeetingStatus = action;
//					fmMeetingDialogNote = $('#eventNote').val();
//					FMEventEditor['Description'] = $('#eventNote').val();
//				} else {
//					nmMeetingStatus = action;
//					nmMeetingDialogNote = $('#eventNote').val();
//					NMEventEditor['Description'] = $('#eventNote').val();
//				}
//
//			} else if (FMEventEditor['FMEventID'] != '0'
//					&& !(FMEventEditor['FMEventIsClosed'] == '1' || FMEventEditor['FMEventIsCancelled'] == '1')) {
//				fmMeetingStatus = action;
//				fmMeetingDialogNote = $('#eventNote').val();
//			} else if (!(NMEventEditor['NMEventIsClosed'] == '1' || NMEventEditor['NMEventIsCancelled'] == '1')) {
//				nmMeetingStatus = action;
//				nmMeetingDialogNote = $('#eventNote').val();
//			}
//
//		} else {
//			if (firstMeeting) {
//				fmMeetingStatus = action;
//				fmMeetingDialogNote = $('#eventNote').val();
//				FMEventEditor['Description'] = $('#eventNote').val();
//			} else if (nextMeeting) {
//				nmMeetingStatus = action;
//				nmMeetingDialogNote = $('#eventNote').val();
//				NMEventEditor['Description'] = $('#eventNote').val();
//			}
//		}
//
//		// get the edit opps selected category
//		meetingDialogNote = $('#eventNote').val();
//		g_somethingWasChanged = true;
//		$('#nextMeetingStatus').html('').hide();
//
//		if (action == 'CANCELLED') {
//			// return to editopp blank out fields
//			/*
//			 * if (firstMeeting) { $('#firstMeeting').val('');
//			 * $('#firstMeetingTime').val(''); FMEventEditor['FMEventID'] = '0'; }
//			 * else if (nextMeeting) { $('#nextMeeting').val('');
//			 * $('#nextMeetingTime').val(''); }
//			 */
//			if ($('#nextMeeting').val() != '') {
//				$('#nextMeeting').val('');
//				$('#nextMeetingTime').val('');
//			} else if ($('#firstMeeting').val() != '') {
//				$('#firstMeeting').val('');
//				$('#firstMeetingTime').val('');
//				FMEventEditor['FMEventID'] = '0';
//			}
//
//			unblockUI('addNewEvent');
//		} else {
//			if ((cat == 3) || (cat == 5) || (cat == 9) || (cat == 6) || (firstMeeting && action == 'CLOSED')) {
//				unblockUI('addNewEvent');
//			} else {
//				addEvent();
//			}
//		}
//
//		// call the edit opps category changed
//		category_changed(cat);
//	}
//}

//function cancelEvent(formType) {
//	cur_data = GetFormData($('#addNewEvent'));
//	if (IsModified(form_data, cur_data)) {
//		if (IfClosing()) {
//			$('#addNewEvent').html('');
//			unblockUI('addNewEvent');
//		}
//	} else {
//		$('#addNewEvent').html('');
//		unblockUI('addNewEvent');
//	}
//}

//function processEventData(result) {
//	result = ParseStatus(result);
//
//	var resStatus = explode("_", result.result.GETFORMATTEDVAR);
//	var accountId = resStatus['1'];
//	var tblAddNote = $('#saveEvent');
//
//	if (result.result.STATUS == 'FALSE') {
//		$('.clsError').hide();
//
//		for ( var key in result.result.ERROR) {
//			$('#spn_' + key).html('&#32;' + result.result.ERROR[key]);
//			$("#spn_" + key).fadeIn('slow');
//		}
//	} else if (result.result.STATUS == 'OK') {
//		if (result.eventId) {
//			// var win_url = window.location.href;
//			// var get_indx = win_url.indexOf("pending_events");			
//			
//			window.location = 'slipstream.php?action=event&eventId=' + result.eventId;		
//			//window.location.reload();
//			return;
//		}
//		
//		// var noteTable = result.resultList;
//		$('.clsError').html('').fadeIn('slow');
//
//		if (result.action == 'add') {
//			$('#Subject').val('');
//			$('#Type').val('');
//			$('#StartDate').val(curDate);
//			$('#ClockPick').val('');
//			$('#ClockDuration').val('');
//			$('#Notes').val('');
//			$('.clsError').html('');
//		}
//		unblockUI('addNewEvent');
//	}
//	
//	$("input").attr("disabled", "");
//	$("textarea").attr("disabled", "");
//	$("select").attr("disabled", "");
//}

//function showDurationDiv() {
//	var isIE = (navigator.appVersion.indexOf("MSIE") != -1) ? true : false;
//
//	if (isIE) {
//		$('#durationDiv').css('top', '75%');
//		$('#durationDiv').css('left', '23%');
//	} else {
//		$('#durationDiv').css('top', 'auto');
//		$('#durationDiv').css('left', 'auto');
//	}
//	$('#durationDiv').show();
//}

//function hideDurationDiv() {
//	var isIE = (navigator.appVersion.indexOf("MSIE") != -1) ? true : false;
//
//	if (isIE) {
//		document.onclick = chk;
//	} else {
//		document.addEventListener("click", function(e) {
//			return chkFF(e)
//		}, true);
//	}
//
//}

//function chkFF(objEvent) {
//	var eventSource = objEvent.target.name;
//
//	if (typeof (eventSource) == 'undefined') { // Clicking Outside the div
//		$('#durationDiv').hide();
//	}
//}

//function chk() {
//	var isIE = (navigator.appVersion.indexOf("MSIE") != -1) ? true : false;
//
//	if (isIE) { // For IE browser
//		var eventSource = event.srcElement.name;
//		if (typeof (eventSource) == 'undefined') { // Clicking Outside the div
//			$('#durationDiv').hide();
//		}
//	}
//}

//function selectDuration() {
//	$('#ClockDuration').val($('#durations').val());
//	$('#durationDiv').hide();
//}

//function validateDuration() {
//	var validChars = "0123456789:";
//	var duration = $('#ClockDuration').val();
//	var returnDur = '';
//	for ( var i = 0; i <= duration.length; i++) {
//		char = duration.charAt(i);
//
//		if (!(validChars.indexOf(char) == -1)) {
//			returnDur += char
//		}
//	}
//	$('#ClockDuration').val(returnDur);
//}

function cancelPendingEvent(id, etype, date) {
	msg = '';
	if (etype == 'EVENT')
		msg = "Do you want to cancel this Event?";
	else
		msg = "Do you want to cancel this Task?";

	if (confirm(msg)) {
		
		if(date != '') {
			date = '&date=' + date;
		}	
		
		var postUrl = 'ajax/ajax.ChangeEventStatus.php?action=cancel&eventId=' + id + date;

		$.post(postUrl, function(data) {
			if(date != '') {	
				result = ParseStatus(data);
				window.location = 'slipstream.php?action=event&eventId=' + result.result.EventID;
			} else {
				window.location.reload();
			}
		});
	}
}

function CompletePendingEvent(id, repeat_info, type, oppid) {

	if (oppid != '' && oppid > 0) {
	} else {

		// For repeating events, we need to determine if they want to load the series or the instance
		if (type == 'remove') {

			var prompt = 'Are you sure you want to permanently remove this event?';
			var buttons = {Remove:true, Cancel:false};

			$.prompt(prompt, {
				buttons: buttons,
				submit: function(v, m, f) {
					//alert('EventID:'+id+', rep: '+repeat_info+', Note: '+f.closingNote+', FollowOn: '+f.followOnEvent+', Action:'+type);
					
					
					
					if (v) {
						BusyOn();
						$.post('ajax/ajax.ChangeEventStatus.php', 
								{EventID: id, rep: repeat_info, Note: f.closingNote, FollowOn: f.followOnEvent, Action: type},
								function (data) {
								/*Start Jet-37 For Remove*/	
									$.post('ajax/ajax.ChangeEventStatusGoogle.php', 
										{ eventid: id },
										function (result) {
											alert('Deleted from Google');
										});
										/*End Jet-37 For Remove*/
									if (data.dest) {
										window.location = data.dest;
									} else if (data.followon) {
										ShowAjaxForm('EventNew&contactid=' + data.followon + '&action=add');
									}
								}, "json"
						);
					}
				}
			});
		} else {

			$.post('ajax/ajax.ChangeEventStatus.php', 
					{EventID: id, rep: repeat_info, Action: "GetContact"},
					function (data) {

						/* Even if there are not contact, we can complete Event, besso */
						//if (data.nocontact) {
						//	alert('Please select Edit and assign a contact to the Event before completing.');
						//} else {
							var prompt = 'Event Completion Notes<br/><textarea name="closingNote" rows="8" cols="55" />';
							if (repeat_info == '') {
								prompt += '<br/><input type="checkbox" value="true" name="followOnEvent"/> Create next event';
							}
							var buttons = {Complete:true, Cancel:false};
							type = 'close';

							$.prompt(prompt, {
								buttons: buttons,
								submit: function(v, m, f) {

									if (v) {

										BusyOn();
										$.post('ajax/ajax.ChangeEventStatus.php', 
												{EventID: id, rep: repeat_info, Note: f.closingNote, FollowOn: f.followOnEvent, Action: type},
												function (data) {
													if (data.dest) {
														window.location = data.dest;
													} else if (data.followon) {
														ShowAjaxForm('EventNew&contactid=' + data.followon + '&action=add');
													}
												}, "json"
										);
									}
								}
							});
						//}
					}, "json"
			);
		}
	}
}

function closePendingEvent(id, oppid, personId, etype, next_event, date) {

	
	if (oppid != '' && oppid > 0) {
		// if(confirm("Do you want to complete this Event?")) {
		openOppPopup(oppid, personId);
		// }
	} else {
		$("input").attr("disabled", "");
		$("textarea").attr("disabled", "");
		
//		if (date != '') {
//			virtualEventCloseDate = date;
//		}	
		
//		msg = '';
//		if (etype == 'EVENT') {
//			msg = "Do you want to complete this Event?";
//		} else {
//			msg = "Do you want to complete this Task?";
//		}
		
		if (etype == 'EVENT') {
			$('#lblType').html('Event');
			// $('#chkHolder').show();
		} else {
			$('#lblType').html('Task');
			// $('#chkHolder').hide();
		}
		// if(confirm(msg)) {

		$('#eventId').val(id);
		$('#closingNote').val('');
		if (next_event) {
			$('#chkHolder').show();
			$('#followOnEvent').attr('checked', false);
		} else {
			$('#chkHolder').hide();
		}
		form_data = GetFormData($('#closingEventNote')); // set an Initial
		// check point of
		// form data which
		// will be checked
		// when user will
		// cancel the
		// form(shell.js)
		blockUI('closingEventNote', '550px', '', (jQuery(window).width() - 550) / 2 + 'px', 100);

		// }
	}
}

function submitCloseEvent() {
	var postData = $('#closeEvent').serialize();	
	var date = '';
	if (virtualEventCloseDate != '') {
		date = '&date=' + virtualEventCloseDate;
		virtualEventCloseDate = '';
	}
	
	var postUrl = 'ajax/ajax.ChangeEventStatus.php?action=close' + date;	
	
	$.post(postUrl, postData, function(data) {
		result = ParseStatus(data);

		unblockUI('closingEventNote');
		if (data != 0) {			
			var con_id = result.result.contactId;
			var event_id = result.result.eventId;
			if (result.result.followOnEvent == 1) {
				if (result.result.oppId > 0) {
					var opp_id = result.result.oppId;
					addEvent(con_id, 'add', event_id, opp_id);
				} else {
					addEvent(con_id, 'add', event_id);
				}
			} else {
				//result = ParseStatus(data);				
				window.location = 'slipstream.php?action=event&eventId=' + result.result.EventID;
				//window.location.reload();
			}
		} else {
			window.location.reload();
		}

		$("input").attr("disabled", "");
		$("textarea").attr("disabled", "");
	});

	$("input").attr("disabled", "disabled");
	$("textarea").attr("disabled", "disabled");
}

function cancelClosingFunct() {
	cur_data = GetFormData($('#closingEventNote'));
	if (IsModified(form_data, cur_data)) {
		if (IfClosing()) {
			$('#closingNote').val('');
			unblockUI('closingEventNote');
		}
	} else {
		$('#closingNote').val('');
		unblockUI('closingEventNote');
	}
}

function toggleEventRow() {
	if ($("option:selected", "select#Type").val().substring(0, 4) == 'TASK') {
		$('.EventRows').hide();
		$('#ClockPick').val('');
		$('#ClockDuration').val('');
	} else {
		// $('#ClockPick').val('');
		// $('#ClockDuration').val('');
		$('.EventRows').show();
	}
}

var firstMeeting = false;
var nextMeeting = false;

function showNoCancel() {
	$('#btnNoCancell').show();
	$('#btnRescheduled').hide();
}

function isFirstMeetingClosed() {
	var fm_id = g_opportunity[g_opps.FMEventID];
	var nm_id = g_opportunity[g_opps.NMEventID];

	if (jet_events[fm_id] != undefined) {
		if (jet_events[fm_id]['Closed'] != 1 && jet_events[fm_id]['Cancelled'] != 1) {
			return false;
		}
	}
	
	return true;
	//	var fm_id = g_opportunity[g_opps.FMEventID];
	//	if (jet_events[fm_id]['Closed'] == 1 || jet_events[fm_id]['Cancelled'] == 1 )
	
	// check if NM is closed
	// var status = (fmMeetingStatus == 'CLOSED' || fmMeetingStatus == 'CANCELLED' || fmMeetingDialogNote != ''
	// 		|| FMEventEditor['FMEventIsClosed'] == '1' || FMEventEditor['FMEventIsCancelled'] == '1');
	//	var status = true;
	
	//	return status;
}

function isNextMeetingClosed() {
	var nm_id = g_opportunity[g_opps.NMEventID];
	if (jet_events[nm_id] != undefined) {
		if (jet_events[nm_id]['Closed'] != 1 && jet_events[nm_id]['Cancelled'] != 1) {
			return false;
		}
	}
	
	return true;
	// check if NM is closed
	//	var status = (nmMeetingStatus == 'CLOSED' || nmMeetingStatus == 'CANCELLED' || nmMeetingDialogNote != ''
	//			|| NMEventEditor['NMEventIsClosed'] == '1' || NMEventEditor['NMEventIsCancelled'] == '1' || $(
	//			'#nextMeeting').val() == '');
	//
	//	var cat = getOppsCategory();
	//	if ((cat == 3 || cat == 5 || cat == 9 || cat == 6) && NMEventEditor['SaveEvent'] && nextMeetingEventStatus == '') {
	//		status = false;
	//	}
	//	var status = true;
	
	//	return status;
}

function EditOppEvent(params) { 
	if (typeof(params) != 'object') {
		params = {};
	}
	var fm_id = g_opportunity[g_opps.FMEventID];
	var nm_id = g_opportunity[g_opps.NMEventID];
	var category = g_opportunity[g_opps.Category];
	
	var buttons = {Complete:'Complete', Reschedule:'Reschedule', Remove:'Remove'};
	if (params.reschedule == false) {
		delete buttons.Reschedule;
	}
	if (params.remove == false) {
		delete buttons.Remove;
	}
	
	if ((fm_id == '' || fm_id == 0) && (nm_id == '' || nm_id == 0)) {
		EditOppOpenEvent(-2);
	} else if ((fm_id == -2 || fm_id > 0) && typeof(jet_events[fm_id]) != 'undefined' && jet_events[fm_id]['Closed'] != 1) {
		var prompt = 'The "First Meeting" on ' + $('#firstMeeting').val() + ' is currently open';
		// var buttons = {Complete:'Complete', Reschedule:'Reschedule'};
		if (!params.allow_fm_remove) {
			delete buttons.Remove;
		}
		EditOppPrmptOpnMtg(prompt, fm_id, buttons, params.followup, params.do_after_func);
	} else if (nm_id == -1 || nm_id > 0) {
		if (jet_events[nm_id]['Closed'] != 1) {
			var prompt = 'The "Next Meeting" on ' + $('#nextMeeting').val() + ' is currently open';
			EditOppPrmptOpnMtg(prompt, nm_id, buttons, params.followup, params.do_after_func);
		} else {
			// Go directly to add event, do not stop at Go, do not collect $200
			EditOppOpenEvent(-1);
		}
	} else {
		// Go directly to add event, do not stop at Go, do not collect $200
		EditOppOpenEvent(-1);
	}
}

function EditOppPrmptOpnMtg(prompt, meeting_id, prompt_buttons, prmpt_nxt_mtg, do_after_func) {
	var states = {
		state0: {
			html:prompt,
			buttons: prompt_buttons,
			submit: function (v,m,f) {
				if (!v) {
					return true;
				}
				if (v == 'Complete') {
					$.prompt.goToState('state1');
				} else if (v == 'Remove') {
					$.prompt.goToState('state2');
				} else if (v == 'Reschedule') {
					// Open this event in the event editor
					EditOppOpenEvent(meeting_id);
					if ($.isFunction(do_after_func)) do_after_func();
					return true;
				}

				return false;
			}
		},
		state1: {
			html: 'Event Completion Notes<br/><textarea name="closingNote" style="width:380px;height:160px;" />',
			buttons: {Complete:true, Cancel:false},
			submit: function (v,m,f) {
				if (v) {
					jet_events[meeting_id]['Closed'] = true;
					if (f.closingNote) {
						jet_events[meeting_id]['Note'] = f.closingNote;
					}
					// EditOppEvent();
					// return true;
					if (prmpt_nxt_mtg != false) {
						$.prompt.goToState('state3');
						return false;
					}
					if ($.isFunction(do_after_func)) do_after_func();
					return true;
				} else {
					$.prompt.goToState('state0');
					return false;
				}
			}
		},
		state2: {
			html: 'Are you sure you want to permanently remove this event?',
			buttons: {Remove:true, Cancel:false},
			submit: function (v,m,f) {
				if (v) {
					jet_events[meeting_id]['Cancelled'] = true;
					// EditOppEvent();
					// return true;
					if (prmpt_nxt_mtg != false) {
						$.prompt.goToState('state3');
						return false;
					}
				} else {
					$.prompt.goToState('state0');
					return false;
				}
				if ($.isFunction(do_after_func)) do_after_func();
				return true;
			}
		},
		state3: {
			html: 'Add a new meeting now?',
			buttons: {Yes:true, No:false},
			submit: function (v,m,f) {
				if (v) {
					EditOppEvent();
				}
				if ($.isFunction(do_after_func)) do_after_func();
				return true;
			}
		}
	};
	
	$.prompt(states);
}

function EditOppOpenEvent(event_id) {
	var send_data = '';
	var action = 'edit';
	if (event_id < 0) {
		action = 'add';
		if (!jet_events[event_id]) {
			var contact_id = g_opportunity[g_opps.ContactID];
			if ($('#ContactID').val() != '') {
				contact_id = $('#ContactID').val();
			}
			var this_opp_id = (opp_id == 0) ? -1 : opp_id;
			
			send_data = {DealID: this_opp_id, ContactID: -1};
		}
	}
	if (jet_events[event_id]) {
		send_data = jet_events[event_id];
		// send_data['DealID'] = this_opp_id;
	}
	ShowAjaxForm('EventNew&eventid=' + event_id + '&action=' + action, '', send_data);
}

function EditOppResultProc(result, rec_id) {
	// Merge the results with the existing jet_events hash
	var incoming_event = {};
	incoming_event[rec_id] = result;
	jQuery.extend(true, jet_events, incoming_event);

	// var cat = getOppsCategory();
	// Determine which meeting the result should go into
	var fm_id = g_opportunity[g_opps.FMEventID];
	var nm_id = g_opportunity[g_opps.NMEventID];
	
	if (rec_id == -2 || rec_id == g_opportunity[g_opps.FMEventID] 
			|| (g_opportunity[g_opps.FMEventID] == '' && g_opportunity[g_opps.NMEventID] == '')) {
		// If nothing is set, or the eventid matches current FM then it is a FM
		g_opportunity[g_opps.FMEventID] = rec_id;
		var field_prefix = 'first';
	} else {
		// Otherwise it is a NM
		g_opportunity[g_opps.NMEventID] = rec_id;
		var field_prefix = 'next';
	}
	
	// Update the editopp form for the proper meeting
	if (result.meetingDate) {
		$('#' + field_prefix + 'Meeting').val(result.meetingDate);
	}
	if (result.meetingTime) {
		$('#' + field_prefix + 'MeetingTime').val(result.meetingTime);
	}

	unblockUI(form);
	form.remove();
	
	// Tell EditOpp that a save is needed
	something_changed();
}

// EditOppEditMtg() {

///**
// * The function is called from edit opps window when user clicks on the FM and
// * NM meeting data and time button
// */
//function callEvent(meeting, opp_id) {
//	var cat = getOppsCategory();
//	var displayAddEvent = true;
//
//	
//	
//	firstMeeting = (meeting == 'FirstMeeting') ? true : false;
//	nextMeeting = (meeting == 'NextMeeting') ? true : false;
//
//	if (((FMEventEditor['FMEventID'] == '0' && FMEventEditor['StartDate'] != '') || (FMEventEditor['FMEventID'] != '0' && FMEventEditor['FMEventIsClosed'] != '1'))
//			&& !isFirstMeetingClosed()) {
//		// First meeting completion dialog
//		displayNextMeetingDialog();
//
//		if (!firstMeeting) {
//			$('#btnRescheduled').hide();
//			$('#btnCancell').hide();
//		}
//
//		displayAddEvent = false;
//	} else if (nextMeeting && NMEventEditor['NMEventID'] != 0 && NMEventEditor['NMEventIsClosed'] != 1
//			&& !isNextMeetingClosed()) {
//		// next meeting completion dialog
//		displayNextMeetingDialog();
//		displayAddEvent = false;
//	}
//
//	/*
//	 * else if (cat == 3 || cat == 5 || cat == 6 || cat == 9 || cat == 10) { if
//	 * ((FMEventEditor['FMEventID'] != '0' && FMEventEditor['FMEventIsClosed'] !=
//	 * '1') || (NMEventEditor['NMEventIsClosed'] != '1' &&
//	 * NMEventEditor['NMEventID'] != '0')) {
//	 * 
//	 * alert (2) displayNextMeetingDialog(); $('#btnRescheduled').hide();
//	 * $('#btnCancell').hide(); displayAddEvent = false; } } else if
//	 * (firstMeeting) { if (FMEventEditor['FMEventID'] != '0' &&
//	 * FMEventEditor['FMEventIsClosed'] != '1') { displayNextMeetingDialog();
//	 * displayAddEvent = false; } }
//	 */
//	/*
//	 * else if (nextMeeting) { if (NMEventEditor['NMEventID'] != 0 &&
//	 * NMEventEditor['NMEventIsClosed'] != 1) { displayNextMeetingDialog();
//	 * displayAddEvent = false; }
//	 * 
//	 * /*else if (FMEventEditor['FMEventID'] != '0' &&
//	 * FMEventEditor['FMEventIsClosed'] != '1') { displayNextMeetingDialog();
//	 * $('#btnRescheduled').hide(); $('#btnCancell').hide(); displayAddEvent =
//	 * false; } }
//	 */
//
//	if (displayAddEvent)
//		addEvent(0, 'add', 0, opp_id);
//}

function CheckDateExpire(inputDate) {
	var currdate = new Date();
	var list = inputDate.split('/');
	var inputDate = list[0] + '/' + list[1] + '/' + '20' + list[2];
	var userdate = new Date(inputDate);

	return (userdate < currdate) ? true : false;
}

function textCounter(field, maxlimit) {
	if (maxlimit != 0 && maxlimit != '') {
		if (field.value.length > maxlimit - 1) {
			field.value = field.value.substring(0, maxlimit);
			alert('Textarea value can only be ' + maxlimit + ' characters in length.');
			return false;
		}
	}
}

function ClearContact() {
	$('#ContactID').val("");
}

function ClearAccount() {
	$('#AccountID').val("");
}

///**
// * This function is called from the edit opps to disply the event dialog for
// * closing event satus.
// */
//function displayEventDialog() {
//	displayNextMeetingDialog();
//	showNoCancel();
//
//	/*
//	 * if (firstMeeting || $('#nextMeeting').val() == '') {
//	 * $('#btnRescheduled').hide(); $('#btnCancell').hide(); }
//	 */
//}
//
//function displayNextMeetingDialog() {
//
//	$('#content').html('<div id="nextMeetingStatus">' + str + '</div>');
//
//	if (firstMeeting || $('#nextMeeting').val() == '') {
//		var input_date = $('#firstMeeting').val();
//		var input_time = $('#firstMeetingTime').val();
//
//	} else {
//		var input_date = $('#nextMeeting').val();
//		var input_time = $('#nextMeetingTime').val();
//	}
//
//	var cat = getOppsCategory();
//	var str = '';
//
//	if (!(cat == 6 || cat == 9)) {
//		str += '<div id="cancelLink"><a href="javascript:CancellDialog();">Cancel</a></div>';
//	}
//
//	str += '<div style="padding:10px 0px 5px 50px; ;"><span id="displayMessage">For the event on '
//			+ input_date
//			+ ' at '
//			+ input_time
//			+ ' add a note and select one of the following choices:</span><br><textarea name="eventNote" id="eventNote" rows="10" cols="60"></textarea></div>';
//	str += '<div style="padding:0px 0px 5px 50px;">';
//	str += '<div id="btnRescheduled"><input type="button" value="Reschedule the Event" style="width:200px; height:60px" onclick="check_event(\'RESCHEDULED\')"></div>';
//	str += '<div id="btnClosed"><input type="button" value="Complete the Event" style="width:200px; height:60px" onclick="check_event(\'CLOSED\')"></div>';
//	str += '<div id="btnCancell"><input type="button" value="Remove the Event" style="width:200px; height:60px" onclick="check_event(\'CANCELLED\')"></div>';
//	str += '</div>';
//	$('#nextMeetingStatus').html(str);
//	blockUI($('#nextMeetingStatus'), '750px', '', (jQuery(window).width() - 750) / 2 + 'px', 100);
//}

function CreateNewContact(form_location, params, form_action, key) {
	$('body').data('CurrentRow', 'email_hold_' + key);
	ShowAjaxForm(form_location, params, form_action);
}

/**
 * Cancel the edit opps event dialog
 */
function CancellDialog() {
	$('#nextMeetingStatus').hide();
	unblockUI('addNewEvent');
}

/**
 * Returns the selected edit opps category.
 * 
 */
function getOppsCategory() {
	var cat = 0;
	try {
		cat = parseInt(value_of_radio());
	} catch (e) {
	}

	return cat;
}

function ResetCalTooltips() {
		
	try {
		$('.preview').cluetip( {
			positionBy: 'topRight',
			attribute :'id',
			width : 272,
			dropShadow: false,
			showTitle :false,
			sticky: true,
			closeText: '<img src="images/img_close.png" style="width: 16px; height: 16px" alt="close" />',
			arrows :true
			
		});
		$('.task_preview').cluetip( {
			attribute :'id',
			width : 272,
			dropShadow: false,
			showTitle :false,
			sticky: true,
			closePosition: 'title',
			closeText: '<img src="images/img_close.png" style="width: 16px; height: 16px" alt="close" />',
			arrows :true
		});
	}
	catch(err){
		//some pages don't have cluetip.js included
	}
}
