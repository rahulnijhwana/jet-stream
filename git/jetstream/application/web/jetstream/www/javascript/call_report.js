$(document).ready( function() {
	if ($('#ReportStartDate').length) {
		$("#ReportStartDate").datepicker( {
			showOn :"both",
			buttonImage :"./images/calendar.gif",
			buttonImageOnly :true
		});
	}
	if ($('#ReportEndDate').length) {
		$("#ReportEndDate").datepicker( {
			showOn :"both",
			buttonImage :"./images/calendar.gif",
			buttonImageOnly :true
		});
	}
	$("#call_report_filter_tasks").click(function(){
		reloadReport();
	});

	$("#call_report_filter_events").click(function(){
		reloadReport();
	});

});

function reloadReport(){
	$(".default_report").each(function(){
		var id = $(this).attr('id');
		$('#toggle_' + id).attr('src', 'images/plus.gif');
		$('.default_report').attr('loaded', false);
		$('.default_report').css('display', 'none');
	});
}

function CallReportSearch() {
	if(($('#ReportStartDate').val() != '') && $('#ReportEndDate').val() != '')	{
		$('.content_body').fadeTo('slow', 0.5);

		var date_frm = $('#ReportStartDate').val();
		var date_to = $('#ReportEndDate').val();
		var uncompleted_tasks = $("#call_report_filter_tasks").attr('checked');
		var uncompleted_events = $("#call_report_filter_events").attr('checked');
		
		if($('#report_user').length) {
			var report_user_id = $('#report_user').val();
		}
		else {
			var report_user_id = '';
		}
		AjaxLoadReport('report_content', date_frm, date_to, report_user_id, 'search', uncompleted_tasks, uncompleted_events);
		$('#report_type').val('search');
	}
	else {
		alert('Please select a date range');
	}
}

function FilterReport() {
	if($('#report_type').val() != 'search') {
		$('.default_report').html('<img class="spinner" src="images/slipstream_spinner.gif" />');
		$('.default_report').slideUp("fast");
		$('.default_report').attr('loaded', false);
		$('.toggle_default_report').attr('src', 'images/plus.gif');
	}
	else {
		CallReportSearch();
	}
}

function toggleCallReport(controlId, date_frm, date_to, uncompleted_tasks, uncompleted_events) {
	if ($('#' + controlId).css('display') == 'none' || $('#' + controlId).css('display') == '') {
		$('.default_report').html('<img class="spinner" src="images/slipstream_spinner.gif" />');
		$('#' + controlId).slideDown("fast");
		$('#toggle_' + controlId).attr('src', 'images/minus.gif');

		if($('#report_user').length) {
			var report_user_id = $('#report_user').val();
		}
		else {
			var report_user_id = '';
		}
		var uncompleted_tasks = $("#call_report_filter_tasks").attr('checked');
		var uncompleted_events = $("#call_report_filter_events").attr('checked');
		AjaxLoadReport(controlId, date_frm, date_to, report_user_id, '', uncompleted_tasks, uncompleted_events);
	}
	else {
		$('#' + controlId).slideUp("fast");
		$('#toggle_' + controlId).attr('src', 'images/plus.gif');
	}
}

function AjaxLoadReport(detail_id, date_frm, date_to, report_user_id, search_text, uncompleted_tasks, uncompleted_events) {	
	$.post("ajax/ajax.reportdetails.php", {
		request_id : detail_id,
		dt_frm : date_frm,
		dt_to : date_to,
		report_user_id : report_user_id,
		search_text : search_text,
		uncompleted_tasks : uncompleted_tasks,
		uncompleted_events : uncompleted_events
		
	},
	function(data, success) {		
		$('.content_body').fadeTo('slow', 1);
		var report_div = "table_" + detail_id;
		if(search_text != 'search') {
			$("#" + detail_id).html(data).ready(
				function() {
					$("#table_" + detail_id).tablesorter({
						sortList: [[0,0]]
					});
				}
			);
		}
		else {
			$("#div_" + detail_id).html(data).ready(
				function() {
					$("#table_" + detail_id).tablesorter({
						sortList: [[0,0]]
					});
				}
			);
		}
	}, "html");
}
