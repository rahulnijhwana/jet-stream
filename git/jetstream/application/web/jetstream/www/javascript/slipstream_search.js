function toggleInfo(controlId) {
	if ($('#' + controlId).css('display') == 'none' || $('#' + controlId).css('display') == '') {
		$('#' + controlId).slideDown("fast");
		// css('display', 'block');
		$('#toggle_' + controlId).attr('src', 'images/minus.gif');
		if (!$('#' + controlId).attr('loaded')) {
			$('#' + controlId).attr('loaded', true);
			AjaxLoadDetail(controlId);
		}
	} else {
		$('#' + controlId).slideUp("fast");
		// css('display', 'none');
		$('#toggle_' + controlId).attr('src', 'images/plus.gif');
	}
}

function AjaxLoadDetail(detail_id) {
	$.post("ajax/ajax.searchdetail.php", {
		request_id :detail_id
	}, function(data, success) {
		$("#" + detail_id).html(data);
	}, "html");
}

var page = 0;
var total_pages = 0;
var search_string = '';
var search_type = '';
var loading_page = false;
var merge_contact = new Array();
var merge_account = new Array();
var callTimer = true;
var Formadvancedata = '';
var dashboardSearchType = 'basic';

function dumpObj(obj, name, indent, depth) {
    if (depth > 20) {
           return indent + name + ": <Maximum Depth Reached>\n";
    }

    if (typeof obj == "object") {
           var child = null;
           var output = indent + name + "\n";
           indent += "\t";
           for (var item in obj)
           {
                 try {
                        child = obj[item];

                 } catch (e) {
                        child = "<Unable to Evaluate>";
                 }

                 if (typeof child == "object") {
                        output += dumpObj(child, item, indent, depth + 1);
                 } else {
                        output += indent + item + ": " + child + "\n";
                 }
           }
           return output;
    } else {
           return obj;
    }
}

function AjaxLoadPage(page) {	
	if (loading_page != true) {
		loading_page = true;

		var form_info = $("#SlipstreamSearch").serializeArray();
		form_info.push(({name:'Page', value:page}));

		// alert(form_info);
		// var Formdata = 'Page=' + page + '&' + $("#SlipstreamSearch").serialize();
		
		callTimer = true;
		var dashboardSearchType = 'basic';
		if($("#advanced_filter")) {
			var test = '';
			var advanced_data = Array();
			$("[name^='advfilterinput_']").each(function() {
				form_info.push({name:this.name, value:this.value});
			});
			form_info.push({name:'SortType', value:$('#SortType').val()});
			form_info.push({name:'SortAs', value:$('#SortAs').val()});
		}
		// alert(dumpObj(form_info));
		// alert('--->' + form_info);
		$('#dashboardsearch').fadeTo("fast", .1);
		$.post("ajax/search_results.php", form_info, function(data, success) {

			/*
			 * Set the search parameters in the cookie.
			 */
			//document.cookie = 'DashboardSearchType=' + dashboardSearchType;
			//document.cookie = 'DashboardSearch=true';
			//document.cookie = 'PageNo=' + page;
			//document.cookie = 'RecordsPerPage=' + $("select[name='RecordsPerPage']").attr('value');
			//document.cookie = 'SearchType=' + $("select[name='SearchType']").attr('value');
			//document.cookie = 'SearchString=' + $("input[name='SearchString']").attr('value');
			//document.cookie = 'AssignedSearch=' + $('#AssignedSearch').attr('checked');
			//document.cookie = 'InactiveType=' + $("#selectInactive").attr('checked');
			
			$('#dashboardsearch').html(data).fadeTo("fast", 1);
			loading_page = false;
			if ($('#chkMergeRecord').attr('checked')) {
				$('.mergeClass').css( {
					display :"inline"
				});
				for (i = 0; i < merge_contact.length; i++)
					$('#chk_' + merge_contact[i]['CID']).attr('checked', true);
				for (i = 0; i < merge_account.length; i++)
					$('#chk_' + merge_account[i]['ACID']).attr('checked', true);
			} else
				$('.mergeClass').css( {
					display :"none"
				});
			// resetMerge();//uncheck Merge check box and hide Merge Button
			}, "html");
	}
}

$(document).ready( function() {
	$('#chkMergeRecord').attr('checked', false);

	if (get_cookie('SearchString') && get_cookie('SearchString').length > 0) {
		curPageId = get_cookie('PageNo');
		AjaxLoadPage(curPageId);
	}

	/*
	 * $('#SlipstreamSearch').ajaxForm({ beforeSubmit: function() {
	 * $('#searchresults').html(''); page = 0; } , dataType: 'json' , success:
	 * function(data) { $('#searchresults').html(data.output);
	 * $('#searchresults').slideDown("fast"); page = 1; search_string =
	 * $("input[name='SearchString']").attr('value'); search_type =
	 * $("select[name='SearchType']").attr('value'); } });
	 */
	/*
	 * $("#searchresults").scroll(function(){ if
	 * ($("#searchresults").attr('scrollTop') +
	 * $("#searchresults").attr('clientHeight') >
	 * $("#searchresults").attr('scrollHeight') - 150) { AjaxLoadPage(); } });
	 */
});

function toggleMergeButton(val) {
	if (val) {
		$('#btnMerge').css( {
			display :"inline"
		});
		$('.mergeClass').css( {
			display :"inline"
		});
	} else {
		$('#btnMerge').css( {
			display :"none"
		});
		$('.mergeClass').css( {
			display :"none"
		});

	}
}

function doMerge() {

	if (merge_contact.length == 0 && merge_account.length == 0) {
		alert('Select any two Contacts or Accounts to merge');
		return;
	} else if (merge_contact.length > 0 && merge_account.length > 0) {
		alert('Please select records from One category only(Contact/Account)');
		return false;
	}
	if (merge_contact.length > 0)
		type = 'Contact';
	else
		type = 'Account';
	if (type == 'Contact') {
		if (validateContactMerge(merge_contact)) {
			if (confirm('Do you want to merge contacts ?')) {
				op = '';
				for (i = 0; i < merge_contact.length; i++)
					op = op + '<option value="' + merge_contact[i]['CID'] + '" accid="' + merge_contact[i]['ACID']
							+ '">' + merge_contact[i]['CNAME'] + '</option>';

				$("select#mergeList").html(op);
				left = ($(document).width() - $('#divMerge').width()) / 2;
				$('#cellMergeText').html('Select Primary Contact');
				$('#mergeType').attr('value', 'Contact');
				blockUI('divMerge', '', '', left, 200);
			}
		}

	} else {
		if (validateAccountMerge(merge_account)) {

			if (confirm('Do you want to merge accounts ?')) {
				op = '';
				for (i = 0; i < merge_account.length; i++)
					op = op + '<option value="' + merge_account[i]['ACID'] + '">' + merge_account[i]['ACNAME']
							+ '</option>';
				$("select#mergeList").html(op);
				left = ($(document).width() - $('#divMerge').width()) / 2;
				$('#cellMergeText').html('Select Primary Account');
				$('#mergeType').attr('value', 'Account');
				blockUI('divMerge', '', '', left, 200);
			}
		}

	}

}

function validateContactMerge(contacts) {
	prevAccNo = -1;
	if (contacts.length != 2) {
		alert('Only 2 contact can be merged at a time' + contacts.length);
		return false;
	}
	for (i = 0; i < contacts.length; i++) {
		if (prevAccNo == -1)
			prevAccNo = contacts[i]['ACID'];
		else if (prevAccNo != contacts[i]['ACID']) {
			alert('All the Contacts must belong to same account');
			return false;
		}
	}
	return true;

}

function validateAccountMerge(accounts) {

	if (accounts.length != 2) {
		alert('Only 2 account can be merged at a time');
		return false;
	}
	return true;

}

function setAttr(el, attr) {
	if ($('#' + el).attr('checked')) {
		$('#' + el).addClass(attr);
		if (attr == 'chkContact') {
			var newContact = new Array();
			newContact['CID'] = $('#' + el).attr('cid');
			newContact['ACID'] = $('#' + el).attr('accid');
			newContact['ACNAME'] = $('#' + el).attr('accname');
			newContact['CNAME'] = $('#' + el).attr('cname');
			merge_contact[merge_contact.length] = newContact;
		} else {
			var newAccount = new Array();
			newAccount['ACID'] = $('#' + el).attr('accid');
			newAccount['ACNAME'] = $('#' + el).attr('accname');
			merge_account[merge_account.length] = newAccount;
		}
	} else {
		$('#' + el).removeClass(attr);
		tmpArr = new Array();
		if (attr == 'chkContact') {
			for (i = 0; i < merge_contact.length; i++) {
				if (merge_contact[i]['CID'] != $('#' + el).attr('cid'))
					tmpArr[tmpArr.length] = merge_contact[i];
			}
			merge_contact = tmpArr;
		} else {
			for (i = 0; i < merge_account.length; i++) {
				if (merge_account[i]['ACID'] != $('#' + el).attr('accid'))
					tmpArr[tmpArr.length] = merge_account[i];
			}
			merge_account = tmpArr;

		}
	}

}

function closeMergedList() {
	$("select#mergeList").html('');
	$("#mergeType").attr('value', '');
	unblockUI('divMerge');
}

function saveMergedList(type) {
	if ($('#mergeType').attr('value') == 'Contact') {
		secondaryContact = '';
		primaryContact = $('#mergeList option:selected').val();
		if (typeof primaryContact == 'undefined') {
			alert('Please select a contact');
			return;
		}
		accountId = $('#mergeList option:selected').attr('accid');
		for (i = 0; i < document.getElementById('mergeList').options.length; i++) {
			if (!document.getElementById('mergeList').options[i].selected) {
				if (secondaryContact == '')
					secondaryContact = document.getElementById('mergeList').options[i].value;
				else
					secondaryContact = secondaryContact + ',' + document.getElementById('mergeList').options[i].value;
			}

		}
		$.post('./ajax/ajax.Merge.php', {
			mergeType :'Contact',
			primaryContact :primaryContact,
			secondaryContact :secondaryContact,
			accountId :accountId
		}, mergeCallback);

	} else {
		secondaryAccount = '';
		primaryAccount = $('#mergeList option:selected').val();
		if (typeof primaryAccount == 'undefined') {
			alert('Please select an Account');
			return;
		}
		for (i = 0; i < document.getElementById('mergeList').options.length; i++) {

			if (!document.getElementById('mergeList').options[i].selected) {
				if (secondaryAccount == '')
					secondaryAccount = document.getElementById('mergeList').options[i].value;
				else
					secondaryAccount = secondaryAccount + ',' + document.getElementById('mergeList').options[i].value;
			}

		}
		$.post('./ajax/ajax.Merge.php', {
			mergeType :'Account',
			primaryAccount :primaryAccount,
			secondaryAccount :secondaryAccount
		}, mergeCallback);

	}
}

function mergeCallback(result) {
	result = ParseStatus(result);
	if (result.STATUS == 'OK') {
		merge_contact = new Array();
		merge_account = new Array();
		closeMergedList();
		curPageId = get_cookie('PageNo');
		if (curPageId != null){
			AjaxLoadPage(curPageId);
		}
		else {
			AjaxLoadPage(1);
		}
		$("#btnMerge").hide();
		$("#chkMergeRecord").removeAttr("checked");
	}
	
}

function resetMerge() {
	$('.chkContact').attr('checked', false);
	$('.chkAccount').attr('checked', false);
	$('#chkMergeRecord').attr('checked', false);
	$('#btnMerge').css( {
		visibility :"hidden"
	});
	$("#mergeType").attr('value', '');
	$("select#mergeList").html('');
	merge_contact = new Array();
	merge_account = new Array();
}

function get_cookie(cookieName) {
	var name = cookieName + '=';
	var dc = document.cookie;

	if (dc.length > 0) {
		var begin = dc.indexOf(name);
		if (begin != -1) {
			begin += name.length;
			var end = dc.indexOf(';', begin);
			if (end == -1)
				end = dc.length;
			return unescape(dc.substring(begin, end));
		}
	}

	return null;
}

/****************** Advance Search **************************/
function showSearch(search_type) {
	if(search_type == 'advance') {
		document.cookie = 'DashboardSearchType=advance';
		window.location.href = 'slipstream.php?action=advancedashboard';
	}
	else if (search_type == 'superadvance'){
		document.cookie = 'DashboardSearchType=advance';
		window.location.href = 'slipstream.php?action=superadvancedashboard';	
	} 
	else	{
		document.cookie = 'DashboardSearchType=basic';
		window.location.href = 'slipstream.php?action=dashboard';
	}
}

if (typeof (filterListArray) != 'undefined') {
	/*if (typeof (noteReferer) != 'undefined') {
		createFilterOpt(1, 'D2', true);
		createFilterOpt(2, '', false);

		if ($('#NText1').length > 0) {
			$('#NText1').val(noteReferer);
			setNoteType(noteReferer);
		}
	} else {
		alert('hello');*/
		createFilterOpt(1, '', false);
	//}
}

function callDashBoardSearch() {
	if (callTimer) {
		callTimer = false;
		setTimeout("AjaxLoadPage('1')", 500);
	}
}

function createFilterOpt(indx, selIndex, chooseFilter) {
	filterSelArray = Array();
	var showSelectBox = false;
	
	for ( var t = 1; t <= $('#filterIndex').val(); t++) {
		filterSelArray[filterSelArray.length] = $('#filterBox' + t).val();
	}
	var selectBoxstr = '<select name="filterOpt[]" class="clsFilterBox" id="filterBox' + indx
			+ '"  onChange="chooseFilterOpt(' + indx + ')">';
	selectBoxstr += '<option value="">Choose Filter Field</option>';

	for ( var filterListID in filterListArray) {
		selectBoxstr += '<optgroup label="------' + filterListID + '------">';
		var filterIDIndex = 0;
		var filterSelectedAccSectionText = '';
		var filterSelectedConSectionText = '';
		
		for ( var filterID in filterListArray[filterListID]) {
			if (filterListID == 'Company Fields') {
				var selOpt = findObjInArray('CM_' + filterID, filterSelArray, selIndex);
				if (selOpt) {
					continue;
				}
				var filterSectionOption = filterListArray[filterListID][filterID].split('||');
				var filterSectionText = filterSectionOption[0];
				var filterOptionText = filterSectionOption[1];
				
				if((filterSectionText != '') && (filterSectionText != filterSelectedAccSectionText)) {
					filterSelectedAccSectionText = filterSectionText;
					selectBoxstr += '<optgroup label="--' + filterSectionText + '--">';
				}
				
				if (('CM' + filterIDIndex == selIndex) || ('CM_' + filterID == selIndex)) {
					selectBoxstr += '<option value="CM_' + filterID + '" selected >'
							+ filterOptionText + '</option>';
					showSelectBox = true;
				} else {
					selectBoxstr += '<option value="CM_' + filterID + '" >' + filterOptionText
							+ '</option>';
					showSelectBox = true;
				}
			} else if (filterListID == 'Contact Fields') {
					var selOpt = findObjInArray('CN_' + filterID, filterSelArray, selIndex);
					if (selOpt) {
						continue;
					}
					
					var filterSectionOption = filterListArray[filterListID][filterID].split('||');
					var filterSectionText = filterSectionOption[0];
					var filterOptionText = filterSectionOption[1];
					
					if((filterSectionText != '') && (filterSectionText != filterSelectedConSectionText)) {
						filterSelectedConSectionText = filterSectionText;
						selectBoxstr += '<optgroup label="--' + filterSectionText + '--">';
					}
				
					if (('CN' + filterIDIndex == selIndex) || ('CN_' + filterID == selIndex)) {
						selectBoxstr += '<option value="CN_' + filterID + '" selected >'
								+ filterOptionText + '</option>';
						showSelectBox = true;
					} else {
						selectBoxstr += '<option value="CN_' + filterID + '" >'
								+ filterOptionText + '</option>';
						showSelectBox = true;
					}
			} else {
				var selOpt = findObjInArray(filterID, filterSelArray, selIndex);
				if (selOpt) {
					continue;
				}

				if (filterID == selIndex) {
					selectBoxstr += '<option value="' + filterID + '" selected >'
							+ filterListArray[filterListID][filterID] + '</option>';
					showSelectBox = true;
				} else {
					selectBoxstr += '<option value="' + filterID + '" >' + filterListArray[filterListID][filterID]
							+ '</option>';
					showSelectBox = true;
				}
			}
			filterIDIndex++;
		}
		selectBoxstr += '</optgroup>';
	}
	selectBoxstr += '</select>';

	if (!showSelectBox) {
		selectBoxstr = '';
	}
	if ($('#filterSelect' + indx).length) {
		$('#filterSelect' + indx).html(selectBoxstr);
	} else {
		var nextIndx = indx;
		var result = '<div id="filter' + nextIndx + '" class="filter-list">';
		result += '<div id="filterSelect' + nextIndx + '" class="filter-list">' + selectBoxstr + '</div>';
		result += '<div id="filterText' + nextIndx + '" class="filter-list"></div><br /></div>';

		return result;
	}

	if (chooseFilter) {
		chooseFilterOpt(indx);
	}
}

function findObjInArray(val, arrayname, selIndex) {
	if ((val != selIndex)) {
		for ( var elem in arrayname) {
			if (arrayname[elem] == val) {
				return true;
			}
		}
	}
	return false;
}

function chooseFilterOpt(indx) {
	for ( var t = 1; t <= $('#filterIndex').val(); t++) {
		if ($('#filterBox' + t).length) {
			createFilterOpt(t, $('#filterBox' + t).val(), false);
		}
	}

	var curSelectBoxVal = $('#filterBox' + indx).val();
	var strDivContent = showFilterTextBox(curSelectBoxVal, indx);
	$('#filterText' + indx).html(strDivContent);

	/*
	 * if(curSelectBoxVal.lastIndexOf('Date') != -1) {
	 * if($('#NText'+indx).length) { $("#NText"+indx).datepicker({ showOn:
	 * "both", buttonImage: "./images/calendar.gif", buttonImageOnly: true }); } }
	 */

	if ($('#NText' + indx).length) {
		$('#NText' + indx).focus();
	} else if ($('#STARTDATE' + indx).length) {
		$('#STARTDATE' + indx).focus();
	}

	if ($('#StartDate' + indx).length) {
		$("#StartDate" + indx).datepicker( {
			showOn :"both",
			buttonImage :"./images/calendar.gif",
			buttonImageOnly :true
		});
	}

	if ($('#EndDate' + indx).length) {
		$("#EndDate" + indx).datepicker( {
			showOn :"both",
			buttonImage :"./images/calendar.gif",
			buttonImageOnly :true
		});
	}

	if (indx == $('#filterIndex').val()) {
		var nextIndx = indx + 1;
		$('#filterIndex').val(nextIndx);

		var result = createFilterOpt(nextIndx, '', false);

		$('#filter' + indx).append(result);
	}
}

function showFilterTextBox(selType, indx) {
	var strContent = '';
	var curSelectBoxVal = $('#filterBox' + indx).val();

	if (typeof (filterListValArray[selType]) != 'undefined') {
		strContent = eval("'" + filterListValArray[selType] + "'");
	}
	return strContent;
}

function buildSearchData(page) {
	var totIndex = $('#filterIndex').val();
	var Formdata = '';

	for ( var i = 1; i < totIndex; i++) {
		if ($('#filterBox' + i).length) {
			if (Formdata == '') {
				Formdata += 'filterOpt[]=' + $('#filterBox' + i).val();
			} else {
				Formdata += '&filterOpt[]=' + $('#filterBox' + i).val();
			}
		}

		if ($('#NText' + i).length) {
			if (Formdata == '') {
				Formdata += 'NText' + i + '=' + $('#NText' + i).val();
			} else {
				Formdata += '&NText' + i + '=' + $('#NText' + i).val();
			}
		} else if ($('#StartDate' + i).length) {
			if (Formdata == '') {
				Formdata += 'StartDate' + i + '=' + $('#StartDate' + i).val();
			} else {
				Formdata += '&StartDate' + i + '=' + $('#StartDate' + i).val();
			}
			Formdata += '&EndDate' + i + '=' + $('#EndDate' + i).val();
		}
	}
	
	Formdata += '&SortType=' + $('#SortType').val();
	Formdata += '&SortAs=' + $('#SortAs').val();

	return Formdata;
}

function resetSearchdata() {
	/*$('#SlipstreamSearch').each {
		$(this).val('');
	}
	$("#SlipstreamSearch").reset();
	if($("#filterIndex").val() > 0)	{
		//$("#advance_filter").html('');
		createFilterOpt(1, '', false);
	}*/
}

function saveDashboardSearch() {
	if($('#dashboardsearch').html() != '') {
		blockUI('saveSearchDiv', '30%', '', '35%', '10%');
	}
	else {
		blockUI('saveSearchConfrm', '30%', '', '35%', '10%');
	}	
}

function hideConfrmDiv() {
	unblockUI('saveSearchConfrm');
}

function cancelSaveDashboardSearch() {
	unblockUI('saveSearchDiv');
}

function AjaxSavePage(page) {
		if($('#SaveSearchName').val() == '') {
			$('#savedSearchError').html('Please enter the Search Name.').fadeIn('slow');
			setTimeout("$('#savedSearchError').fadeOut('slow');", 2000);
		}
		else {			
			var Formdata = 'Page=' + page + '&SaveSearch=1&' + $("#SlipstreamSearch").serialize();
			var SavedSearchName = $('#SaveSearchName').val();
			callTimer = true;
			var SavedSearchType = 0;
			
			if(document.getElementById("advanceSearch")) {
				Formadvancedata = buildSearchData(page);
				var SavedSearchType = 1;
				
				if(Formadvancedata != '')	{
					Formdata += '&' + Formadvancedata;
				}
				Formdata += '&filterIndex=' +$('#filterIndex').val();	 
			}
			Formdata += '&SavedSearchType='+SavedSearchType+'&SavedSearchName='+SavedSearchName;
			
			//$('#dashboardsearch').fadeTo("fast", .1);
			$.post("ajax/ajax.saveSearch.php", Formdata, function(data, success) {
				if(data != 'failure')	{
					$('#saved_search_list').html(data);
				}
				unblockUI('saveSearchDiv');
			});
		}
}

function LoadSavedSearch(saveddata) {
		var Formdata = saveddata;
		$('#dashboardsearch').fadeTo("fast", .1);
		createFilterOpt(1, '', false);
		
		$.post("ajax/ajax.loadSearch.php", Formdata, function(data, success) {
			
				result = ParseStatus(data);
				
				var filterOptArr = Array();
				var filterOptTextArr = Array();
				filterOptIndx = 1;
				for(var cntfilter in result.result) {
					if(result.result[cntfilter].name.search('filterOpt') != -1) {
						var splitResultArr = result.result[cntfilter].name.split('_');	
						createFilterOpt(filterOptIndx, result.result[cntfilter].nameval, true);
						filterOptIndx++;
					}
				}
				for(var resid in result.result) {
					if(result.result[resid].name.search('filterOpt') == -1) {
						if($('#'+result.result[resid].name).length > 0) {
							if($('#'+result.result[resid].name).attr("type") == 'checkbox') {
								if(result.result[resid].nameval == 1)	{
									$('#'+result.result[resid].name).attr("checked", true);
								}
								else {
									$('#'+result.result[resid].name).attr("checked", false);
								}
							}
							else	{
								$('#'+result.result[resid].name).val(result.result[resid].nameval);
							}
						}
					}
				}
		});
}

/****************** Export **************************/

function ExportForm(search) {

	$.get('ajax/ajax.HTMLExport.php', {
		type : 'new'
	}, function (data) {
	if ( data == '') editGroupField('0');
	else InitialPrompt(data);
	}
	, 'json' 
	); 

}
		
function InitialPrompt(data){
		$("#jqibox").remove();
		var temp = {
			state0: {
				html:'<div class="advance_section_head"> <table border=0 cellspacing=0 cellpadding=0><tr class=advance_section_head_b><th>This function will export current search results.</th></tr><tr style="background-color:#F0F5FA;"> <td><ul><li><b>Create New</b> : will create a new set of export group fields </li><li><b>Load</b> : will open selected saved export group</li></ul></td></tr><tr style="background-color:#ADC2AD;color:white;font-weight:100"><th> Export Groups :' + outputGroupList(data) + '</th></tr></table></div>',
				buttons: { 'Delete' : 'delete', 'Create_New': 'custom', Load: 'edit', Cancel: 'exit'},
				focus: 1,
				submit:function(v,m,f){ 
					if(v == 'custom'){
						// $.prompt.close();
						editGroupField('0');
						return false;
					}
					else if(v == 'export'){
						$.prompt.close();
						SaveGroupField('export', f.account, f.contact, f.groupname, f.group);
						return true;
					}
					else if(v == 'delete'){
						
						SaveGroupField('delete', '', '', f.groupname, f.group);
						alert('Export Group field ' + f.groupname + ' have been deleted!');
						return true;						
					}
					else if (v == 'edit'){
						if (f.group == 0 ){
							alert('Please pick which Export group to export');
							return false;
						}
						$.prompt.close();
						editGroupField(f.group);
						return false ; 
					}
					else if (v == 'exit'){
						$.prompt.close();
						return true; 
					}
					else 
					return false ;
				}
			} 
		}
		$.prompt(temp);
		$("#jqi_state0_buttonDelete").hide();
		
	}

function outputGroupList(data){

	var output = '&nbsp; &nbsp;<select id=group name=group onchange=javascript:switch2delete()>' ; 
	
	if ( data.length == 0 ) {
		output += '<option value=0> -empty list- </option>';
	}
	else {
		output += '<option value=0>  </option>'; 
		for ( var i in data )	{
			output += '<option value=' + i + '> '+ data[i] + '</option>'; 
		} 
	}
	output += '</select>' ; 
	return output;
}

function switch2delete(){
	var select = document.getElementById("group"); 
	//var button = document.getElementById("jqi_state0_buttonCreate%20New");
	if (select.value != 0 ){
		$("#jqi_state0_buttonCreate_New").hide();
		$("#jqi_state0_buttonDelete").show();

//		button.value = "delete";
//		button.innerHTML = "Delete";

	}
	else {
		$("#jqi_state0_buttonCreate_New").show();
		$("#jqi_state0_buttonDelete").hide();
//		button.value = "custom";
//		button.innerHTML = "Create New";	
	}

}

function editGroupField(d) {

	$.get('ajax/ajax.CustomField.php', {
		action : 'edit',
		group : d 
	},function (data) {CustomFieldprompt(data)}
	, 'text' 
	);
}


function SaveGroupField(action, account, contact, groupname, groupid) {
	console.log(action, account, contact, groupname, groupid);
	if ( action != 'export' ) {
		$.post('ajax/ajax.CustomField.php', {
			action : action,
			account : JSON.stringify(account), 
			contact : JSON.stringify(contact),
			groupname : groupname, 
			groupid : groupid 
		}, function(data) {}
		, 'text' 
		);
	}
	else {
	
		var submitForm = getNewSubmitForm();
		createNewFormElement(submitForm, "action", action);
		createNewFormElement(submitForm, "account", JSON.stringify(account));
		createNewFormElement(submitForm, "contact", JSON.stringify(contact));
		createNewFormElement(submitForm, "groupname", groupname);
		createNewFormElement(submitForm, "groupid", groupid);	
		submitForm.setAttribute("action", "ajax/ajax.HTMLExport.php");
		submitForm.submit();

	}
}

function getNewSubmitForm(){
 var submitForm = document.createElement("FORM");
 document.body.appendChild(submitForm);
 submitForm.method = "POST";
 return submitForm;
}

function createNewFormElement(inputForm, elementName, elementValue){

 var newElement = document.createElement("input");
 newElement.setAttribute("name", elementName);
 newElement.setAttribute("type", "hidden");
 inputForm.appendChild(newElement);
 newElement.value = elementValue;
 return newElement;
}

function checkbox2string( checkbox ){
	var output = '' ;
	for (i=0; i< checkbox.length; i++){
		if (checkbox[i].checked==true){
			output += checkbox[i].name + ";" ; }
	}
	return output; 
} 

function CustomFieldprompt(data){
		
		var temp = {
			state0: {
				html:data,
				buttons: {  Export: 'export', Save: 'save', Back: 'exit'},
				focus: 1,
				submit:function(v,m,f){ 
					if(v == 'export'){ 
						if (typeof(f.account) == 'undefined' && typeof(f.contact) == 'undefined' ){
							alert('Please pick the field you want to export');
							return false;
						}
						$.prompt.close();
						SaveGroupField('export', f.account, f.contact, f.groupname, f.groupid);
						return true;
					}
					else if (v == 'save'){
						if (f.groupname == '') {
							alert('Please enter the Save As name');
							return false;
						}
						if (typeof(f.account) == 'undefined' && typeof(f.contact) == 'undefined' ){
							alert('Please pick the field you want to export');
							return false;
						}
				//		$.prompt.close();
						SaveGroupField('save', f.account, f.contact, f.groupname, f.groupid);
				//		AjaxLoadSlipstreamSearch('loadList', 0 , '');
						return false;
					}
					else if (v == 'exit'){
						$.prompt.close();
						return true; 
					}
					else 
					return false ;
				}
			} 
		}
		$.prompt(temp, {scroll:true});
}

function numbersonly(myfield, e, dec){
    var key;
    var keychar;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
       return true;
    keychar = String.fromCharCode(key);
   
	// control keys
    if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
       return true;
    // numbers
    else if ((("0123456789").indexOf(keychar) > -1))
       return true;
    else
       return false;
	}

/****************** Save Search **************************/

function SaveSlipstreamSearchPrompt(){
		//get the field Value for 
		var Save_Name = document.getElementById("Saved_Name");
		var Save_Id = document.getElementById("Saved_Id");
		
		var temp = {
			state0: {
				html:'Please enter a name <input type="text" name=Save_Name size=20 value="' + Save_Name.value + '">',
				buttons: { Cancel: 'exit', Save: 'save' },
				focus: 1,
				submit:function(v,m,f){ 
					if(v == 'save'){
						$.prompt.close();
						AjaxLoadSlipstreamSearch('Save', Save_Id.value , f.Save_Name );
					//	AjaxLoadSlipstreamSearch('loadList', 0 , '')
						return true;
					}
					else if (v == 'exit'){
						$.prompt.close();
						return true; 
					}
					else 
					return false ;
				}
			} 
		}
		$.prompt(temp);
	}

function AjaxLoadSlipstreamSearch (Action, Id  , Name  ) {
	if ( Action == 'Save' || Action == 'loadList' ||  Action == 'deleteList' ) {
		var Target = '#SavedList' ;
	} else {
		var Target = '#dashboard' ;
	}
	
	if ( Action == 'deleteList' ){
		var answer = confirm("Are you sure you want to delete " + Name +  "?")
		if (answer){
			alert("Deleted! To undo this operation please contact adminstrator.")
		}
	}
	
	var form_info = $("#SlipstreamSearch").serializeArray();
	form_info.push({ name: 'Name', value : Name }, { name: 'Id', value : Id });
//	form_info.push({ name: 'Id', value : Id });
	
	callTimer = true;
	$(Target).fadeTo("fast", .1);
	$.post("ajax/save_search_list.php?action=" + Action , form_info, function(data, success) {
		if ( data == 'error' ){
			alert("Error, Duplicate ID has been found.");
			AjaxLoadSlipstreamSearch('loadList', 0 , '');
			SaveSlipstreamSearchPrompt();
		}else {
			$(Target).html(data).fadeTo("fast", 1);	
			loading_page = false;
		} 
	});
	HideSearchType();
	
}

function AdvanceSearchReset() {
//	document.SlipstreamSearch.reset(); 
	document.SlipstreamSearch.Saved_Id.value = 0 ;
	document.SlipstreamSearch.Saved_Name.value = '' ; 
	for(i=0; i<document.SlipstreamSearch.elements.length; i++){
	//	document.SlipstreamSearch.elements[i].name  			value + ".<br />");
	//	alert(document.SlipstreamSearch.elements[i].class);
		if ( document.SlipstreamSearch.elements[i].className != "multiSelect" ){
			document.SlipstreamSearch.elements[i].value = '' ;
		}
		else {
			document.SlipstreamSearch.elements[i].value = 'Select Options' ;		
		}
		
	}
	document.SlipstreamSearch.Advance.value = 1 ;
	alert("All searched parameters have been cleared!") ; 
	
	return; 
}

function HideSearchType(){
	var searchType = document.getElementById('SearchType');
	var company = 'company' ; 
	var contact = 'contact' ; 
	var division = 'division_';
	var bar = 'bar_';
	$('#' +'btnExptAll').slideDown("fast");
	if (searchType.value == 1 ) {
		// hide company bar & div 
		$('#' + bar+contact).slideUp("fast");
		$('#' + division + contact).slideUp("fast");
		// show contact bar & div 
		$('#' + bar+company).slideDown("fast");
		$('#' + division + company).slideDown("fast");
	}
	else {
		// show both 
		$('#' + bar+contact).slideDown("fast");
		$('#' + bar+company).slideDown("fast");
		$('#' + division + contact).slideDown("fast");
		$('#' + division + company).slideDown("fast");
	}
	AjaxLoadPage(1);
}

function ShowHide(element ,action) {
	
	var button = document.getElementById(element) ;
	if (action =='show'){
		button.style.display = 'block' ; 
	}
	else {
		button.style.display = 'hidden'; 
	}
}
