
function checkNumberField(type,frm)
{
	if(document.getElementById('fieldType').value != -1){
	
		var fieldType = '';
		if(type == 'TextBox'){
			if(frm=='Company'){
			type = document.getElementById('dataTypeSelect').value;
			}
			else if(frm=='Contact'){		
			type = document.getElementById('dataTypeSelectContact').value;
			}
		}
		switch(type){
			case 'Alphabet' :
				fieldType = 'Text';
				break;
			case 'Numeric' :
				fieldType = 'Number';
				break;
			case 'DropDown' :
				fieldType = 'Select';
				break;
			case 'CheckBox' :
				fieldType = 'Bool';
				break;
			case 'RadioButton' :
				fieldType = 'Select';
				break;
			case 'Date' :
				fieldType = 'Date';
				break;	
		}

			dataTypeCollection='';
			el=$('.dataType');
			for(i=0;i<el.length;i++)
			{
				if(dataTypeCollection=='')
					dataTypeCollection=el[i].value;
				else
					dataTypeCollection+=','+el[i].value;
			}

		if(frm == 'Company'){
			queryString='btnAdd=AddAccountField&fieldName='+ fieldType;
			$.post('./ajax/ajax.user.php?'+queryString,
				{
				dataType:dataTypeCollection
				}
				,processAddCompanyFields
			);  
		}
		else if(frm == 'Contact'){
			queryString='btnAdd=AddContactField&fieldName='+ fieldType;
			$.post('./ajax/ajax.user.php?'+queryString,
			{
				dataType:dataTypeCollection
			}
			,processAddContactFields		
			);
		}
	}else{
		if(frm == 'Company')
			document.getElementById('showError').innerHTML = 'Please select a field type first';
		else if(frm == 'Contact')
			document.getElementById('showContactError').innerHTML = 'Please select a field type first';
	}
	
}

function editField(id,fieldEnable,status){
var formType = document.getElementById('frmUsedType').value;
	if(id!=-1){
		var editField = 'editLabel'+fieldEnable;
		var editOption = 'optionsSelect'+fieldEnable;
		var editMandatory = 'isMandatory'+fieldEnable;
		var editIsLastName = 'isLastName'+fieldEnable;
		var editIsEmail = 'isEmail'+fieldEnable;
		if(formType == 'Company'){
			var editIsCompany = 'isCompanyName'+fieldEnable;
		}else if(formType == 'Contact')	{
			var editIsCompany = 'isFirstName'+fieldEnable;
		}
		var isOptions = 'options'+fieldEnable+'[]';
		var editOptions = '.cls'+fieldEnable;
		var mapId = 'mapId'+fieldEnable;
		var editValidation = 'validation' +fieldEnable;
		if(status == 1){
			document.getElementById(editIsCompany).disabled=false;
			if(formType == 'Contact'){
				document.getElementById(editIsLastName).disabled=false;
				document.getElementById(editIsEmail).disabled=false;
			}
		}
		document.getElementById(editField).disabled=false;
		document.getElementById(editMandatory).disabled=false;

		document.getElementById(mapId).value = id;
		if(document.getElementById(isOptions) != null){
			$(editOptions).attr("disabled", false);
		}
	}
}
function addElement(inner,height,divId) {
  var ni = document.getElementById(divId);
  var br = document.createElement('br');
  ni.appendChild(br);
  var numi = document.getElementById('divValue');
  var num = (document.getElementById('divValue').value -1)+ 2;
  numi.value = num;
  var newdiv = document.createElement('div');
  var divIdName = 'Div' + num ;
  newdiv.setAttribute('id',divIdName);
	newdiv.style.border = '2px solid rgb(181, 226, 254)';
	newdiv.style.height = height;
	
	newdiv.style.float='none';
    newdiv.innerHTML = inner;
  ni.appendChild(newdiv);

}

function addField(divId,mapId,display,fieldValue,validationValue,checkCompany,checkMandatory,isLastName,isEmail,optionIds,optionNames){

if(document.getElementById('optionSetId')!=null){
	var optionSetId = document.getElementById('optionSetId').value;
}
if(divId == 'companyDiv'){
	var divName='company';
	var type = document.getElementById('fieldSelect').value;
	var selDataType = document.getElementById('dataTypeSelect').value;
}else if(divId == 'contactDiv'){
	var divName='contact';
	var type = document.getElementById('fieldSelectContact').value;
	var selDataType = document.getElementById('dataTypeSelectContact').value;
}

var fieldTypeSelected  =  document.getElementById('fieldType').value;

var cnum = document.getElementById('childValue');
var inum = (document.getElementById('childValue').value -1)+ 2;
cnum.value = inum;

if(checkCompany == 1){
	var	checkComp = 'Checked';
}else{
	var checkComp = '';
}

if(checkMandatory == 1){
	var	checkMand = 'Checked';
}else{
	var checkMand = '';
}

if(isEmail == 1){
	var	checkEmail = 'Checked';
}else{
	var checkEmail = '';
}

if(isLastName == 1){
	var	checkLastName = 'Checked';
}else{
	var checkLastName = '';
}

if(display != 'NO'){
	var block = 'disabled';
	var fieldName = 'editLabel'+inum;
	var dataType = 'usedType[]';
	var dataTypeCls = '';
	var row = 'usedRow[]';
}else{
	var block = '';
	var fieldName = 'label[]';
	var dataType = 'dataType[]';
	var dataTypeCls = 'dataType';
	var row = 'rowNo[]';
}

var blockLastNameEmail = '';
if(fieldTypeSelected != 'Text' || display != 'NO'){
	blockLastNameEmail = 'disabled';
}
var fieldSelectedType = '<input type="hidden" id="fieldTypeSelected'+ inum +'" value="'+ fieldTypeSelected +'" name="fieldTypeSelected'+ inum +'" />';

if(divId=='contactDiv'){
	var isLastNameCheck = '<div style="float:left;width:7%;"><input type=\'checkbox\' id="isLastName'+ inum +'" value="1" name="isLastName'+ inum +'"   '+ checkLastName +' '+blockLastNameEmail+'/></div>';
	var isEmailCheck = '<div style="float:left;width:7%;"><input type=\'checkbox\' id="isEmail'+ inum +'" value="1" name="isEmail'+ inum +'" '+ checkEmail +' '+blockLastNameEmail+'/></div>';
	var fieldId = 'isFirstName' + +inum;
}else if(divId=='companyDiv'){
	var isLastNameCheck ='';
	var isEmailCheck = '';
	var fieldId = 'isCompanyName' + +inum;
}


switch(type){
	case 'TextBox':
		var inner = '<div style="float:left;width:14%;"><input type=\'text\' maxlength="10" id=\'\' class="clsTextBox" style="width:90%;" value="'+ fieldTypeSelected +' " name=\'\' disabled="disabled"/><input class="'+dataTypeCls+'" type=\'hidden\' id="'+dataType+'" value= "'+ selDataType +'"  name="'+dataType+'"/></div><div style="float:left;width:15%;"><input type=\'text\' id="'+fieldName+'" class="clsTextBox" style="width:90%;" value="'+fieldValue+'" maxlength="50" name="'+fieldName+'" '+block+'/></div><div style="float:left;width:17%"><input type=\'text\' class="clsTextBox" style="width:75%;" id=\'\' value="Option" name=\'\' disabled="disabled"/></div><div style="float:left;width:11%;"><input type=\'checkbox\' id="isMandatory'+ inum +'" value="1" name="isMandatory'+ inum +'" '+checkMand+'  '+block+'/><input type=\'hidden\' id=\'text\' value= "'+ inum +'"  name="'+row+'"/><input type=\'hidden\' id="mapId'+ inum +'" value= ""  name="mapId'+ inum +'"/></div>';
		if(selDataType == 'Numeric')
			inner = inner + '<div style="float:left;width:10%;"><input type=\'checkbox\' id='+ fieldId +' value="1" name='+ fieldId +' disabled="disabled"/></div>'+ isLastNameCheck +' '+ isEmailCheck +' '+ fieldSelectedType +'<div style="float:left;"><img onclick="javascript:editField('+mapId+','+ inum +','+ 0 +');" src="images/edit.jpg" title=""/></div>';
		else
			inner = inner + '<div style="float:left;width:10%;"><input type=\'checkbox\' id='+ fieldId +' value="1" name='+ fieldId +' '+checkComp+'  '+blockLastNameEmail+'/></div> '+ isLastNameCheck +' '+ isEmailCheck +' '+ fieldSelectedType +'<div style="float:left;"><img  onclick="javascript:editField('+mapId+','+ inum +','+ 1 +');" src="images/edit.jpg" title=""/></div>';
		var height = '20px';
		addElement(inner,height,divId);	
		break;
	
	case 'DropDown':
		var height = '20px';
		var inner = '<div style="float:left;width:14%;"><input type=\'text\' class="clsTextBox" id=\'text\' style="width:90%;" value="Select Box" name=\'text\' disabled="disabled"/></div><div style="float:left;width:15%;"><input type=\'text\' class="clsTextBox" maxlength="50" id="'+fieldName+'" style="width:90%;" value="'+fieldValue+'" name="'+fieldName+'" '+block+'/></div><div style="float:left;width:17%;">';
		inner = inner+'<select id="optionsSelect'+ inum +'" name="optionsSelect'+ inum +'" class="clsTextBox" style="width:75%;" '+block+'><option value="-1">-</option>';
		for (var i =0; i<optionNames.length;i++)
		{
			inner=inner+'<option value="'+optionNames[i]+'">'+optionNames[i]+'</option>';
		}
		inner = inner + '</select>';
		for (var i =0; i<optionNames.length;i++)
		{		
			inner = inner + '<div><input type=\'hidden\' class ="cls'+ inum +'" id="options'+ inum +'[]" value="'+optionNames[i]+'" name="options'+ inum +'[]" '+block+'/></div>';
		}
		
			inner = inner + '</div><div style="float:left;width:11%;"><input type=\'checkbox\' id="isMandatory'+ inum +'" value="1" name="isMandatory'+ inum +'" '+checkMand+' '+block+'/></div><div style="float:left;width:10%;"><input type=\'checkbox\' id='+ fieldId +' value="1" name='+ fieldId +' disabled="disabled"/><input type=\'hidden\' id=\'text\' value= "'+ inum +'"  name="'+row+'"/><input class="'+dataTypeCls+'" type=\'hidden\' id="'+dataType+'" value= "DropDown"  name="'+dataType+'" /><input type=\'hidden\' id="mapId'+ inum +'" value= ""  name="mapId'+ inum +'"/><input type=\'hidden\' id="optionSetId'+inum+'" value= '+ optionSetId +'  name="optionSetId'+inum+'"/>';
		
		for (var i =0; i<optionIds.length;i++)
		{
			inner = inner + '<input type=\'hidden\' id="optionId'+ inum +'[]" value= "'+ optionIds[i] +'"  name="optionId'+ inum +'[]"/>';
		}
		    inner = inner +	'</div> '+ isLastNameCheck +' '+ isEmailCheck +'<div style="float:left;"><img onclick="javascript:editOptionsField('+mapId+','+ inum +','+ 0 +',\''+divName+'\');" src="images/edit.jpg" title=""/></div>';
		addElement(inner,height,divId);	
		break;
	
	case 'CheckBox':
		var height = '20px';
		var inner = '<div style="float:left;width:14%;"><input type=\'text\' class="clsTextBox" style="width:90%;" id=\'text\' value="Check Box" name=\'text\' disabled="disabled"/></div><div style="float:left;width:15%;"><input type=\'text\' class="clsTextBox" maxlength="50" id="'+fieldName+'" style="width:90%;" value="'+fieldValue+'" name="'+fieldName+'" '+block+'/></div><div style="float:left;width:17%"><input type=\'text\' class="clsTextBox" style="width:75%;" id=\'text[]\' value="" name=\'text[]\' disabled="disabled"/></div><div style="float:left;width:11%;"><input type=\'checkbox\' id="isMandatory'+ inum +'" value="1" name="isMandatory'+ inum +'" '+checkMand+' '+block+'/></div><div style="float:left;width:10%;"><input type=\'checkbox\' id='+ fieldId +' value="1" name='+ fieldId +' disabled="disabled"/><input type=\'hidden\' id=\'text\' value= "'+ inum +'"  name="'+row+'"/><input class="'+dataTypeCls+'" type=\'hidden\' id="'+dataType+'" value= "CheckBox"  name="'+dataType+'"/><input type=\'hidden\' id="mapId'+ inum +'" value= ""  name="mapId'+ inum +'"/></div> '+ isLastNameCheck +' '+ isEmailCheck +'<div style="float:left;"><img onclick="javascript:editField('+mapId+','+ inum +','+ 0 +');"  src="images/edit.jpg" title=""/></div>';
		addElement(inner,height,divId);	
		break;
	
	case 'RadioButton':
		var height = '20px';
		var inner = '<div style="float:left;width:14%;"><input type=\'text\' class="clsTextBox" id=\'text\' style="width:90%;" value="Radio Button" name=\'text\' disabled="disabled"/></div><div style="float:left;width:15%;"><input type=\'text\' class="clsTextBox" maxlength="50" id="'+fieldName+'" style="width:90%;" value="'+fieldValue+'" name="'+fieldName+'" '+block+'/></div><div style="float:left;width:17%;">';
		inner = inner+'<select id="optionsSelect'+ inum +'" name="optionsSelect'+ inum +'" class="clsTextBox" style="width:75%;" '+block+'><option value="-1">-</option>';
		for (var i =0; i<optionNames.length;i++)
		{
			inner=inner+'<option value="'+optionNames[i]+'">'+optionNames[i]+'</option>';
		}
		inner = inner + '</select>';
		for (var i =0; i<optionNames.length;i++)
		{		
			inner = inner + '<div><input type=\'hidden\' class ="cls'+ inum +'" id="options'+ inum +'[]" value="'+optionNames[i]+'" name="options'+ inum +'[]" '+block+'/></div>';
		}

			inner = inner + '</div><div style="float:left;width:11%;"><input type=\'checkbox\' id="isMandatory'+ inum +'" value="1" name="isMandatory'+ inum +'" '+checkMand+' '+block+'/></div><div style="float:left;width:10%;"><input type=\'checkbox\' id='+ fieldId +' value="1" name='+ fieldId +' disabled="disabled"/><input type=\'hidden\' id=\'text\' value= "'+ inum +'"  name="'+row+'"/><input class="'+dataTypeCls+'" type=\'hidden\' id="'+dataType+'" value= "RadioButton"  name="'+dataType+'" /><input type=\'hidden\' id="mapId'+ inum +'" value= ""  name="mapId'+ inum +'"/><input type=\'hidden\' id="optionSetId'+inum+'" value= '+ optionSetId +'  name="optionSetId'+inum+'"/>';

		for (var i =0; i<optionIds.length;i++)
		{
			inner = inner + '<input type=\'hidden\' id="optionId'+ inum +'[]" value= "'+ optionIds[i] +'"  name="optionId'+ inum +'[]"/>';
		}
		inner = inner +	'</div> '+ isLastNameCheck +' '+ isEmailCheck +'<div style="float:left;"><img onclick="javascript:editOptionsField('+mapId+','+ inum +','+ 0 +',\''+divName+'\');" src="images/edit.jpg" title=""/></div>';
		addElement(inner,height,divId);	
		break;
	
	case 'Date':
		var height = '20px';
		var inner = '<div style="float:left;width:14%;"><input type=\'text\' class="clsTextBox" style="width:90%;" id=\'text\' value="Date" name=\'text\' disabled="disabled"/></div><div style="float:left;width:15%;"><input type=\'text\' class="clsTextBox" maxlength="50" id="'+fieldName+'" style="width:90%;" value="'+fieldValue+'" name="'+fieldName+'" '+block+'/></div><div style="float:left;width:17%"><input type=\'text\' style="width:75%;" class="clsTextBox" id=\'text[]\' value="" name=\'text[]\' disabled="disabled"/></div><div style="float:left;width:11%;"><input type=\'checkbox\' id="isMandatory'+ inum +'" value="1" name="isMandatory'+ inum +'" '+checkMand+' '+block+'/></div><div style="float:left;width:10%;"><input type=\'checkbox\' id='+ fieldId +' value="1" name='+ fieldId +' disabled="disabled"/><input type=\'hidden\' id=\'text\' value= "'+ inum +'"  name="'+row+'"/><input class="'+dataTypeCls+'" type=\'hidden\' id="'+dataType+'" value= "Date"  name="'+dataType+'"/><input type=\'hidden\' id="mapId'+ inum +'" value= ""  name="mapId'+ inum +'"/></div> '+ isLastNameCheck +' '+ isEmailCheck +'<div style="float:left;"><img onclick="javascript:editField('+mapId+','+ inum +','+ 0 +');" src="images/edit.jpg" title=""/></div>';
		addElement(inner,height,divId);
		break;
	
	}

	var validationField = 'validation'+ inum;
	if(document.getElementById(validationField) != null){
		document.getElementById(validationField).value = validationValue;
	}
	document.getElementById('fieldType').value = '-1';
}


var addOptions = 0;

function populateOptions(){
	
	var optionSelected =document.getElementById('optionSetSelect').value;
	if(optionSelected != -1){
		addOptions = 0;
		var inner = '<div>';
		if(options && options[optionSelected])
		{
			for(j=0;j<options[optionSelected].OptionSetValues.length;j++){
				inner = inner+'<input type="text" name="optionsValues'+ addOptions +'" id="optionsValues'+ addOptions +'" value="'+ options[optionSelected].OptionSetValues[j].OptionName +'"/><br/>';
				addOptions = addOptions + 1;	
			}	
		}
		inner=inner+'</div>';
		document.getElementById('optionSetvalues').innerHTML=inner;
		
	}else{
		document.getElementById('optionSetvalues').innerHTML='';
		addOptions = 0;
	}
}

function addOptionList(){
	var noOfCols = 5;
	var inner = '';
	for(i=0;i<noOfCols;i++)
	{
		newDiv=document.createElement('div');
		newDiv.innerHTML = '<input type="text" name="optionsValues'+ addOptions +'" id="optionsValues'+ addOptions +'" value=""/>';
		document.getElementById('optionSetvalues').appendChild(newDiv);
		addOptions = addOptions + 1;	
	}	
}
function cancelOption(){
	document.getElementById('optionSetvalues').innerHTML=''; 
	document.getElementById('optionSetName').value='';
	document.getElementById('optionSetSelect').value='-1';
	document.getElementById('optionListError').innerHTML=''; 
	$.unblockUI();	
}

function saveOptionList(addDiv){	
	var optionSelected =document.getElementById('optionSetSelect').value;
	var fieldValue = document.getElementById('optionSetName').value;
	var countOptions = 0;
	var optionValues = new Array();
	fieldValue = fieldValue.replace("'","\'");
	fieldValue = fieldValue.replace(/^\s+|\s+$/g, '') ;
	if(fieldValue == '' ){
		document.getElementById('optionListError').innerHTML = 'LabelName is mandatory';
	return false;
	}else{
		var j =0;
		for(i=0;i<addOptions;i++){
			var optionsField = 'optionsValues'+i;  
			if(document.getElementById(optionsField).value != ''){
				optionValues[j] = document.getElementById('optionsValues'+i).value;
				optionValues[j] = optionValues[j].replace(/^\s+|\s+$/g, '') ;
				if(optionValues[j] != ''){
					countOptions = countOptions + 1;
				}
				j = j+1;
			}
		}
		if(countOptions < 1){
			document.getElementById('optionListError').innerHTML = 'Cannot create a blank Option Set.';
			return false;
		}
		if(options[optionSelected]){
			document.getElementById('optionSetId').value = options[optionSelected].OptionSetId;	
		}
		addField(addDiv,-1,'NO',fieldValue,'-1','0','0','','','',optionValues);
	}

$.unblockUI();
}
function processSaveOptionsFields(){
	
}
	
var optionFieldno = '';
var optionMapId = '';
var optionStatus = '';
var divSelected = '';
function editOptionsField(mapId,fieldNo,status,divName){
	optionFieldno = fieldNo;
	optionMapId = mapId;
	optionStatus = status;
	divSelected =divName;
	if(mapId > 0){
		if(divSelected == 'company'){
			queryString= 'action=EditOptions&mapId='+ mapId;
			$.post('./ajax/ajax.manageCompany.php?'+queryString,processEditOptionFields);	
		}else{
			queryString= 'action=EditOptions&mapId='+ mapId;
			$.post('./ajax/ajax.manageContact.php?'+queryString,processEditOptionFields);	
		}	
	}else{
		alert('Can not edit options without saving.');
		return;
	}
	
}

function processEditOptionFields(result){
	try
	{
		var optionDetails=eval(result);
	}
	catch(e)
	{
		var optionDetails=JSON.decode(result);
	}
	var options = optionDetails[0].Options;
	if(optionDetails[0].OptionsAccountMapId < 0){
			alert ('Sorry you are not authorized to modify system defined options.');
			$.unblockUI();
			document.getElementById('optionsSelect'+optionFieldno).disabled = false;
			editField(optionMapId,optionFieldno,optionStatus);
	}else{
		if(divSelected == 'company'){
			var mapId = optionDetails[0].AccountMapId;	
		}else{
			var mapId = optionDetails[0].ContactMapId;	
		}
		var optionsetId = optionDetails[0].OptionSetId;
		var div = document.getElementById('addOptionElementsDiv');
		
		inner = '';
		for(i = 0;i<options.length;i++)
		{
			inner = inner + '<div style="height:25px;"><input type="text" name="optionList[]" id="optionList[]" value="'+ options[i].OptionName +'"></div>'
		}	
			inner = inner + '<input type="hidden" id="editOptionSetId" name="editOptionSetId" value="'+ optionsetId +'"><input type="hidden" id="editMapId" name="editMapId" value="'+ mapId +'">';
			div . innerHTML =inner;
			
		$.blockUI({ message: $('#editOptionListDiv'),
		css: { 
		padding: '10px',
		backgroundColor: '#FFFFFF',
		opacity: '.9', 
		color: '#000000',
		top:  (jQuery(window).height() - jQuery('#editOptionListDiv').height()) /3 + 'px', 
		left: (jQuery(window).width() - jQuery('#editOptionListDiv').width()) /2 + 'px', 
		width: '500px' ,
		height:'600px',
		overflow:'scroll' 
		}});	
	}
	
}
var editOptions = 1;
function addEditOptionList()
{
	inner='';
	for(i=0;i<5;i++){
		newDiv=document.createElement('div');		
		newDiv.innerHTML ='<input type="text" name="optionList[]" id="optionList[]" value=""/>';
		document.getElementById('addOptionElementsLeftDiv').appendChild(newDiv);
	}
}

function checkOptionValues(optionValues)
{
	frmInstance=document.getElementById(frmInstance);		
	for(i=0;i<frmInstance.length;i++)
	{
		if(frmInstance[i].type=='text' && frmInstance[i].value.indexOf('&')>=0)
		{

			alert('Invalid charcter &');			
			frmInstance[i].focus();
			return false;
		}

	}
	return true;

}

function saveEditOptions(){
	var mapId = document.getElementById('editMapId').value;
	var opSetId = document.getElementById('editOptionSetId').value;
	var optionList = document.getElementById('optionList[]').value;
	//checkOptionValues(optionList);
	if(divSelected == 'company'){
		queryString=$('#frmEditOptions').formSerialize()+'&action=saveEditOptionValues';
		$.post('./ajax/ajax.manageCompany.php?'+queryString,processSaveEditOptionsFields);		
	}else{
		queryString=$('#frmEditOptions').formSerialize()+'&action=saveEditOptionValues';
		$.post('./ajax/ajax.manageContact.php?'+queryString,processSaveEditOptionsFields);		
	}
}
function processSaveEditOptionsFields(result){
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}

	if(result.STATUS=='OK')
	{
		document.getElementById('addOptionElementsLeftDiv').innerHTML = '';
		$.unblockUI();
		editField(optionMapId,optionFieldno,optionStatus)
	}else{
		document.getElementById('editOptionError').innerHTML=result.ERROR[0];
	}
}
function cancelEditOptionList(){
	$.unblockUI();
	document.getElementById('addOptionElementsLeftDiv').innerHTML = '';
	editField(optionMapId,optionFieldno,optionStatus)
}
