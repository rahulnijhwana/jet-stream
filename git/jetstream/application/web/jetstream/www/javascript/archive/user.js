$(document).ready( function() {
	jQuery("#tblUserByAdmin").jqGrid( {
		height :300,
		url :'ajax/user_by_admin_xml.php',
		datatype :'xml',
		colNames : [ 'Edit', 'Delete', 'User Name', 'First Name', 'Last Name', 'Email', 'Type' ],
		colModel : [ {
			name :'actionEdit',
			index :'actionEdit',
			width :50,
			align :'center'
		}, {
			name :'actionDelete',
			index :'actionDelete',
			width :70,
			align :'center'
		}, {
			name :'userName',
			index :'userName',
			width :200
		}, {
			name :'firstName',
			index :'firstName',
			width :150,
			align :'left'
		}, {
			name :'lastName',
			index :'lastName',
			width :150,
			align :'left'
		}, {
			name :'email',
			index :'email',
			width :200,
			align :'left'
		}, {
			name :'type',
			index :'type',
			width :150,
			sortable :false
		} ],
		pager :jQuery('#pager'),
		rowNum :10,
		// rowList:[10,20,30],
		sortname :'userName',
		sortorder :"asc",
		viewrecords :true,
		imgpath :'themes/basic/images',
		caption :'Manage User'
	});
	$('#StartDate').datepicker( {
		showOn :"both",
		buttonImage :"../images/calendar.gif",
		buttonImageOnly :true
	});

	$('#frmCreateUser').ajaxForm( {
		dataType :'json',
		success :processPostData
	});
});
function processPostData(result) {
	try {
		result = eval(result);
	} catch (e) {
		alert('Err : ' + e);
	}
	try {
		for (i = 0; i < document.frmCreateUser.elements.length; i++) {
			if (document.frmCreateUser.elements[i].type == 'text'
					&& (document.frmCreateUser.elements[i].id != 'txt_userid' || document.frmCreateUser.elements[i].type == 'select-one')
					|| document.frmCreateUser.elements[i].type == 'password') {
				document.getElementById('spn_' + document.frmCreateUser.elements[i].id).innerHTML = '';
			}
		}
	} catch (e) {
		alert(e);
	}
	if (result.STATUS == 'FALSE') {
		for ( var key in result.ERROR) {
			$("#spn_" + key).hide();
			$("#spn_" + key).fadeIn('fast');
			document.getElementById('spn_' + key).innerHTML = result.ERROR[key];
		}
	} else
		window.location = '?action=manageuser';
}

function showAddForm(d) {
	if (d == 1) {
		$("#divUserByAdmin").hide();
		document.getElementById('frmHeader').innerHTML = 'Add User';
		document.getElementById('frmCreateUser').reset();
		if (document.getElementById('cbAssign'))
			document.getElementById('cbAssign').style.display = 'none';
		document.getElementById('btnSubmit').value = 'Submit';
		for (i = 0; i < document.frmCreateUser.elements.length; i++) {
			if (document.frmCreateUser.elements[i].type == 'text'
					&& (document.frmCreateUser.elements[i].id != 'txt_userid' || document.frmCreateUser.elements[i].type == 'select-one')
					|| document.frmCreateUser.elements[i].type == 'password') {
				document.getElementById('spn_' + document.frmCreateUser.elements[i].id).innerHTML = '';
			}
		}

		$("#tblAddUser").fadeIn('slow');
	} else if (d == 2) {

		$("#divUserByAdmin").hide();
		$("#tblAddUser").fadeIn('slow');
	} else {
		$("#tblAddUser").hide();
		$("#divUserByAdmin").fadeIn('slow');
	}

}

function processFormUpdate(result) {
	result = JSON.decode(result);
	document.getElementById('FirstName').value = result.USERDATA.FirstName;
	document.getElementById('LastName').value = result.USERDATA.LastName;
	document.getElementById('UserID').value = result.USERDATA.UserName;
	document.getElementById('Email').value = result.USERDATA.Email;
	document.getElementById('StartDate').value = result.USERDATA.StartDate;
	document.getElementById('btnSubmit').value = 'Update';
	document.getElementById('frmHeader').innerHTML = 'Edit User';
	document.getElementById('spn_Password').innerHTML = 'Leave the password field blank if you don\'t want to change it';
	showAddForm(2);
	if (result.LEVEL)
		processParentUser(result.LEVEL);
	if (result.USERDATA.SupervisorID != -3) {
		document.getElementById('cbAssign').value = result.USERDATA.SupervisorID;
		document.getElementById('userType').value = result.USERDATA.LevelID;
	} else {
		document.getElementById('userType').value = -3;
	}
}

function processFormUpdate(result) {
	try {
		result = eval(result);
	} catch (e) {
		result = JSON.decode(result);
	}

	/*
	 * document.getElementById('FirstName').value=result.USERDATA.FirstName;
	 * document.getElementById('LastName').value=result.USERDATA.LastName;
	 * document.getElementById('UserID').value=result.USERDATA.UserName;
	 * document.getElementById('Email').value=result.USERDATA.Email;
	 * document.getElementById('StartDate').value=result.USERDATA.StartDate;
	 * document.getElementById('btnSubmit').value='Update';
	 */

	document.frmCreateUser.FirstName.value = result.USERDATA.FirstName;
	document.frmCreateUser.LastName.value = result.USERDATA.LastName;
	document.frmCreateUser.UserID.value = result.USERDATA.UserName;
	document.frmCreateUser.Email.value = result.USERDATA.Email;
	document.frmCreateUser.StartDate.value = result.USERDATA.StartDate;
	document.frmCreateUser.btnSubmit.value = 'Update';

	document.getElementById('frmHeader').innerHTML = 'Edit User';
	document.getElementById('spn_Password').innerHTML = 'Leave the password field blank if you don\'t want to change it';
	showAddForm(2);
	if (result.LEVEL)
		processParentUser(result.LEVEL);
	if (result.USERDATA.SupervisorID != -3) {
		document.frmCreateUser.cbAssign.value = result.USERDATA.SupervisorID;
		document.frmCreateUser.userType.value = result.USERDATA.LevelID;
	} else {
		document.frmCreateUser.userType.value = -3;
	}
}

function processFormDelete(result) {
	window.location = '?action=manageuser';
}

function editUser(txt_userid) {
	if (confirm('Do you want to modify this record?')) {
		document.getElementById('txt_userid').value = txt_userid;
		$.post("ajax/ajax.user.php", {
			txt_userid :txt_userid,
			btnSubmit :'GETINFO'
		}, processFormUpdate);
	}
}

function deleteUser(txt_userid) {
	if (confirm('Do you want to delete this record?')) {
		document.getElementById('txt_userid').value = txt_userid;
		$.post("ajax/ajax.user.php", {
			txt_userid :txt_userid,
			btnSubmit :'DELETE'
		}, processFormDelete);
	}
}
function getParentUser(levelId) {
	$.post('ajax/ajax.user.php', {
		levelId :levelId
	}, processParentUser);
}

function processParentUser(result) {
	try {
		try {
			result = eval(result);
		} catch (e) {
			result = JSON.decode(result);
		}
		if (result.STATUS == 'OK') {

			if (result.PARENT != '') {
				var data = '<select id="cbAssign" name="cbAssign" class="clsTextBox"><option value="-1">-</option>';
				for ( var key in result.PARENT) {
					data += '<OPTGROUP LABEL="' + result.PARENT[key] + '">';
					for ( var dataKey in result.DATA[key]) {
						if (result.DATA[key][dataKey].USERID && result.DATA[key][dataKey].NAME)
							data += '<option value="' + result.DATA[key][dataKey].USERID + '">'
									+ result.DATA[key][dataKey].NAME + '</option>';
					}
					data += '</OPTGROUP>';
				}
				data += '</select><br/><span id="spn_cbAssign"></span>';
				document.getElementById('cellAssign').innerHTML = data;
			} else {
				var data = '<select id="cbAssign" name="cbAssign" class="clsTextBox"><option value="-1">-</option>';
				data += '</select><br/><span id="spn_cbAssign"></span>';
				document.getElementById('cellAssign').innerHTML = data;
			}
		}
	} catch (e) {
		alert(e);
	}
}