$(document).ready(function() {       
    $("#tblCompanyLayout").fadeIn('slow');  
    
});

function checkFieldType(){
document.getElementById('showError').innerHTML = '';
var fieldType = document.getElementById('fieldType').value;	
switch(fieldType){
		case 'Text' :
			document.getElementById('fieldSelect').value = 'TextBox';
			document.getElementById('dataTypeSelect').value = 'Alphabet';
			break;
		case 'Number' :
			document.getElementById('fieldSelect').value = 'TextBox';
			document.getElementById('dataTypeSelect').value = 'Numeric';
			break;
		case 'Zip' :
			document.getElementById('fieldSelect').value = 'TextBox';
			document.getElementById('dataTypeSelect').value = 'Alphabet';
			break;
		case 'Email' :
			document.getElementById('fieldSelect').value = 'TextBox';
			document.getElementById('dataTypeSelect').value = 'Alphabet';
			break;
		case 'Phone' :
			document.getElementById('fieldSelect').value = 'TextBox';
			document.getElementById('dataTypeSelect').value = 'Numeric';
			break;
		case 'DropDown' :
			document.getElementById('fieldSelect').value = 'DropDown';
			break;
		case 'CheckBox' :
			document.getElementById('fieldSelect').value = 'CheckBox';
			break;
		case 'RadioButton' :
			document.getElementById('fieldSelect').value = 'RadioButton';
			break;
		case 'Date' :
			document.getElementById('fieldSelect').value = 'Date';
			break;	
	}	
}

function processAddCompanyFields(result){
	
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}
	
	if(result.STATUS=='OK')
	{
		if(document.getElementById('fieldSelect').value != 'DropDown' && document.getElementById('fieldSelect').value != 'RadioButton'){
			addField('companyDiv',-1,'NO','','-1','0','0','','','','');
		}else{
			document.getElementById('optionSetName').value = '';
			document.getElementById('optionSetSelect').value = '-1';
			document.getElementById('optionSetvalues').innerHTML = '';
			document.getElementById('optionSetId').value = '';
			addOptions = 0;
			$.blockUI({ message: $('#optionListDiv'),
					css: { 
					padding: '10px',
					backgroundColor: '#FFFFFF',
					opacity: '.9', 
					color: '#000000',
					top:  (jQuery(window).height() - jQuery('#optionListDiv').height()) /3 + 'px', 
					left: (jQuery(window).width() - jQuery('#optionListDiv').width()) /2 + 'px', 
					width: '500px',
					height:'600px',
					overflow:'scroll'
					}});
		}

		//window.location='?sec=companylayout';
	}
	else
	{
		var str='';
		for(i=0;i<result.ERROR.length;i++)
		{
			str=str+'<br/>'+result.ERROR[i];
		}
		document.getElementById('showError').innerHTML=str;
	}
}

function saveCompanyLayOut()
{
	if(!checkFormData('frmManageCompany'))
		return;
	queryString='btnSave=Save&action=redo&frmType=Company';	
	formValue=$('#frmManageCompany').formSerialize();		

	$.post('./ajax/ajax.user.php?'+queryString,
	{
		queryString:formValue
	}	
	,processSaveLayOut);
}
function checkFormData(frmInstance)
{
	frmInstance=document.getElementById(frmInstance);		
	for(i=0;i<frmInstance.length;i++)
	{
		if(frmInstance[i].type=='text' && frmInstance[i].value.indexOf('&')>=0)
		{
			
			alert('Invalid charcter &');			
			frmInstance[i].focus();
			return false;
		}
		
	}
	return true;
	
}
function processSaveLayOut(result)
{
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}
	if(result.STATUS=='OK')
	{
		//alert('Layout Saved');
		window.location='?action=company';
	}
	else
	{
		var str='';
		for(i=0;i<result.ERROR.length;i++)
		{
			str=str+'<br/>'+result.ERROR[i];
		}
		document.getElementById('showError').innerHTML=str;
	}
}



