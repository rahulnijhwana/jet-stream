$(document).ready( function() {
	/*
	 * $('#addEvent').ajaxForm({ dataType: 'json', success: processPostData });
	 * $(function() { $('.tool_tip *').tooltip();
	 * 
	 * }); $.post("ajax/ajax.DailyCalendar.php",{
	 * Type:'weekly',FirstDate:document.getElementById('FirstDate').value,
	 * LastDate:document.getElementById('LastDate').value}, processPostData);
	 */
});

function stripslashes(str) {
	str = str.replace(/\\'/g, '\'');
	str = str.replace(/\\"/g, '"');
	str = str.replace(/\\\\/g, '\\');
	str = str.replace(/\\0/g, '\0');
	return str;
}

function getEventInfo(type, subject, description, time, contact) {
	var info = "<div>" + type + "</div><div>" + time + "</div><div>Subject: " + subject + "</div><div>Description: "
			+ description + "</div><div>Contact: " + contact + "</div>";
	info = info.replace("'", "&#39;");
	return info;
}

function getEvent(type) {
	if (type == "next") {
		var Month = document.getElementById('Month').value;
		var Year = document.getElementById('Year').value;

		$.post('./ajax/ajax.DailyCalendar.php', {
			MonthVal :Month,
			YearVal :Year,
			actionType :'NextMonth'
		}, processMonthData);
	}
	if (type == "prev") {
		var Month = document.getElementById('Month').value;
		var Year = document.getElementById('Year').value;

		$.post('./ajax/ajax.DailyCalendar.php', {
			MonthVal :Month,
			YearVal :Year,
			actionType :'PrevMonth'
		}, processMonthData);
	}
}
function processMonthData(result) {
	try {
		try {
			result = eval(result);
		} catch (e) {
			result = JSON.decode(result);
		}
		if (result.STATUS == 'MONTHEVENT') {
			document.getElementById('Month').value = result.MONTHINT;
			document.getElementById('Year').value = result.YEAR;

			document.getElementById('tblHeaderTitle').innerHTML = "<div id='tblHeaderTitle'><a href='#' class=lrgBlackBold onclick='javascript:getEvent(\"prev\")'>&lt;</a>&nbsp;"
					+ result.MONTH
					+ "&nbsp;"
					+ result.YEAR
					+ "&nbsp;<a href=# class=lrgBlackBold onclick='javascript:getEvent(\"next\")'>&gt;</a></div>";

			document.getElementById('monthlyCalendar').innerHTML = result.DATA;
		}
	} catch (e) {
		alert('Err :' + e);
	}
}