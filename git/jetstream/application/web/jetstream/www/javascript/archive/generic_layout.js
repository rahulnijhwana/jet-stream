function manageSection(){
$.blockUI({ message: $('#manageSectionDiv'),
	css: { 
	padding: '10px',
	backgroundColor: '#FFFFFF',
	opacity: '.9', 
	color: '#000000',
	top:  (jQuery(window).height() - jQuery('#manageSectionDiv').height()) /3 + 'px', 
	left: (jQuery(window).width() - jQuery('#manageSectionDiv').width()) /2 + 'px', 
	width: jQuery('#manageSectionDiv').width() 
	}});
	
}

function addSection(){
$.blockUI({ message: $('#sectionDiv'),
	css: { 
	padding: '10px',
	backgroundColor: '#FFFFFF',
	opacity: '.9', 
	color: '#000000',
	top:  (jQuery(window).height() - jQuery('#sectionDiv').height()) /3 + 'px', 
	left: (jQuery(window).width() - jQuery('#sectionDiv').width()) /2 + 'px', 
	width: jQuery('#sectionDiv').width() 
	}});
	
}
function editSection(){
$.blockUI({ message: $('#editSectionDiv'),
	css: { 
	padding: '10px',
	backgroundColor: '#FFFFFF',
	opacity: '.9', 
	color: '#000000',
	top:  (jQuery(window).height() - jQuery('#editSectionDiv').height()) /3 + 'px', 
	left: (jQuery(window).width() - jQuery('#editSectionDiv').width()) /2 + 'px', 
	width: jQuery('#editSectionDiv').width() 
	}});
	
}
function deleteSection(){
$.blockUI({ message: $('#deleteSectionDiv'),
	css: { 
	padding: '10px',
	backgroundColor: '#FFFFFF',
	opacity: '.9', 
	color: '#000000',
	top:  (jQuery(window).height() - jQuery('#deleteSectionDiv').height()) /3 + 'px', 
	left: (jQuery(window).width() - jQuery('#deleteSectionDiv').width()) /2 + 'px', 
	width: jQuery('#deleteSectionDiv').width() 
	}});
	
}
function cancelSection(){
	document.getElementById('sectionName').value='';	
	$.unblockUI();	
}
var sectionAction = '';
function saveSection(divSet,secAction){
	
	sectionAction = secAction;
	
	if(divSet == 'Account'){
		switch(secAction){
			case 'addSection' : 
				var sectionName = document.getElementById('sectionName').value;
				sectionName = sectionName.replace("'","\'");
				if(sectionName == '')
				{
					document.getElementById('sectionError').innerHTML = 'SectionName is mandatory';
					return false;
				}
				queryString=$('#frmAddSection').formSerialize()+'&action=save'+divSet+'Section';
				$.post('./ajax/ajax.companylayout.php?'+queryString,processSaveAccountSection);
				break;
			case 'editSection' : 
				queryString=$('#frmEditSection').formSerialize()+'&action=edit'+divSet+'Section';
				$.post('./ajax/ajax.companylayout.php?'+queryString,processSaveAccountSection);
				break;
			case 'deleteSection'  :
				queryString=$('#frmDeleteSection').formSerialize()+'&action=delete'+divSet+'Section';
				$.post('./ajax/ajax.companylayout.php?'+queryString,processSaveAccountSection);
				break;
		}
		
	}else{
		switch(secAction){
			case 'addSection' : 
				var sectionName = document.getElementById('sectionName').value;
				sectionName = sectionName.replace("'","\'");
				if(sectionName == '')
				{
					document.getElementById('sectionError').innerHTML = 'SectionName is mandatory';
					return false;
				}
				
				queryString=$('#frmAddSection').formSerialize()+'&action=save'+divSet+'Section';
				$.post('./ajax/ajax.contactlayout.php?'+queryString,processSaveContactSection);
				break;
			case 'editSection' : 
				queryString=$('#frmEditSection').formSerialize()+'&action=edit'+divSet+'Section';
				$.post('./ajax/ajax.contactlayout.php?'+queryString,processSaveContactSection);
				break;
			case 'deleteSection'  :
				queryString=$('#frmDeleteSection').formSerialize()+'&action=delete'+divSet+'Section';
				$.post('./ajax/ajax.contactlayout.php?'+queryString,processSaveContactSection);
				break;
		}
	}
}

function processSaveAccountSection(result)
{
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}
	if(result.STATUS=='OK')
	{
		$.unblockUI();
		window.location='?action=companylayout';
	}
	else
	{
		switch(sectionAction){
			case 'addSection' : 
				document.getElementById('sectionError').innerHTML= result.ERROR[0];
				break;
			
			case 'editSection' : 
				document.getElementById('editSectionError').innerHTML= result.ERROR[0];
				break;
			
			case 'deleteSection'  :
				document.getElementById('deleteSectionError').innerHTML= result.ERROR[0];
				break;
		}
	}
}

function processSaveContactSection(result)
{
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);		
	}
	if(result.STATUS=='OK')
	{
		$.unblockUI();
		window.location='?action=contactlayout';
	}
	else
	{
		
		switch(sectionAction){
			case 'addSection' : 
				document.getElementById('sectionError').innerHTML= result.ERROR[0];
				break;

			case 'editSection' : 
				document.getElementById('editSectionError').innerHTML= result.ERROR[0];
				break;

			case 'deleteSection'  :
				document.getElementById('deleteSectionError').innerHTML= result.ERROR[0];
				break;
		}
	}
}

function getLayoutSettings()	
{
	var dataLayout='';
	el=$('.destinationBox');
		
	for(i=0;i<el.length;i++)
	{	
		if(el[i].id != 'totalFieldsDiv'){
			if(el[i].firstChild)
			{			
				if(i==0)
					dataLayout+= el[i].id  + ':'+el[i].firstChild.id;
				else
					dataLayout+= '|'+ el[i].id  + ':'+el[i].firstChild.id;
				
				
			}
			else
			{
				if(i==0)
					dataLayout+=  el[i].id + ':';
				else
					dataLayout+= '|'+ el[i].id + ':';
	
			}
				
		}
		
	}
return dataLayout;
}
