function InsertContactField() {
	
	   var oEditor = FCKeditorAPI.GetInstance('editor');	
	   if ( oEditor.EditMode == FCK_EDITMODE_WYSIWYG ) {	      	      	      
	      cur_value=$('#cbContactFields option:selected').attr('text');  
	      cur_sec=$('#cbContactFields option:selected').attr('sec_name');  
	      oEditor.InsertHtml( '{'+cur_sec+'.'+cur_value+'}' );
	   }
	   else
      		alert( 'You must be in Edit mode!' ) ;
}
