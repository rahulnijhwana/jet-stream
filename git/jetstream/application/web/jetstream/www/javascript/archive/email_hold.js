function IgnoreEmailAddress(key, emailAddress) {
	if (!confirm('This email address will be added to your email address ignore list. Do you want to continue?')) {
		return;
	}

	$.post('ajax/ajax.IgnoreEmailAddress.php', {
		email :emailAddress
	}, function(data) {
		$('#' + key).fadeOut('slow', function() {
			$(this).remove()
		});
	});
}

var currentRow = -1;

function ContactHintForm(firstName, lastName, emailAddress, rowIndex) {
	currentRow = rowIndex;
	$.post('ajax/ajax.EmailHoldContactHint.php', {
		FirstName :firstName,
		LastName :lastName
	}, function(data) {
		$('#contact_hint').html(data);
		blockUI('contact_hint', '750px', '', (jQuery(window).width() - 750) / 2 + 'px', 100);
		contact_search();
		$('#emailaddress').html(emailAddress);
		form_data = GetFormData($('#contact_hint'));// set an Initial check
													// point of form data which
													// will be checked when user
													// will cancel the
													// form(shell.js)
		});
}
function CancelContactHintForm() {
	cur_data = GetFormData($('#contact_hint'));
	if (IsModified(form_data, cur_data)) {
		if (IfClosing()) {
			unblockUI('contact_hint');
		}
	} else {
		unblockUI('contact_hint');
	}
}

function contact_search() {
	$("#Contact").autocomplete("./legacy/data/contact_search.php", {
		width :260,
		selectFirst :false
	});

	function findContactValueCallback(event, data, formatted) {
		getContactEmail(data[1], data[0]);
		$('#contact_search_radio').val(data[1]);
	}

	$("#Contact").result(findContactValueCallback).next().click();
	// console.debug($('div'));
}

function getContactEmail(contactID, contact) {
	if (contactID > 0 && contact != '') {
		$.post('ajax/ajax.GetContactEmailAddresses.php', {
			ContactID :contactID
		}, function(data) {
			$('#contact_email_address').html(contact + '<br>' + data);
		});
	} else {
		$('#contact_email_address').html('');
	}
}

function assignEmail(thisObj, id) {
	if ($(thisObj).is(':checked')) {
		// console.debug('checked');
		$('#contact_hint').data(id, $('#' + id).val());
		$('#' + id).val($('#emailaddress').html());
	} else {
		$('#' + id).val($('#contact_hint').data(id));
		// console.debug('unchecked');
	}
}

function updateEmailAddress() {
	// console.debug('save');
	var contactID = $("input[@type=radio][@checked]").val();

	var data = 'Update=';
	var flag = true;
	$("input[@type=checkbox][@checked]").each( function() {
		var attribute = $(this).attr('relative');
		var value = $('#' + attribute).val();
		var qstr = attribute + '=' + value;

		if (flag) {
			data += attribute + '=\'' + value + '\'';
			flag = false;
		} else {
			data += ',' + attribute + '=\'' + value + '\'';
		}
	});

	if (flag) {
		alert('Please select at least one email address to update the email hold.');
	}

	data += '&' + 'ContactID' + '=' + contactID;
	data += '&' + 'Email' + '=' + $('#emailaddress').html();

	$.post('ajax/ajax.SaveContactEmailAddresses.php', data, function(data) {
		// $('#contact_email_address').html(data);
			$('#contact_email_address').html('Record updated sucessfully.');
			// window.location = '?action=contact&contactId=' + contactID;
			// alert(currentRow);
			if (currentRow >= 0)
				$('#email_hold_' + currentRow).fadeOut('slow', function() {
					$(this).remove()
				});
			unblockUI('contact_hint');

		});
}

function closeLightBox() {
	unblockUI('contact_hint');
}

function DisplayEmails(firstName, lastName, emailAddress, rowIndex) {
	currentRow = rowIndex;
	$.post('ajax/ajax.EmailHoldDisplayEmails.php', {
		EmailAddress :emailAddress
	}, function(data) {
		$('#contact_hint').html(data);
		blockUI('contact_hint', '750px', '', (jQuery(window).width() - 750) / 2 + 'px', 100);
		// contact_search();
			$('#emailaddress').html(emailAddress);
		});
}
