var advancedJavaScriptSupport = document.createElement && document.getElementsByTagName && createXMLHTTPObject();
/* XMLHTTP */

function sendRequest(url,callback,postData) {
	var req = createXMLHTTPObject();
	if (!req) return;
	var method = (postData) ? "POST" : "GET";
	req.open(method,url,true);
	req.setRequestHeader('User-Agent','XMLHTTP/1.0');
	if (postData)
		req.setRequestHeader('Content-type','application/x-www-form-urlencoded');
	req.onreadystatechange = function () {
		if (req.readyState != 4) return;
		if (req.status != 200 && req.status != 304) {
//			alert('HTTP error ' + req.status);
			return;
		}
		callback(req);
	}
	if (req.readyState == 4) return;
	req.send(postData);
}

function XMLHttpFactories() {
	return [
		function () {return new XMLHttpRequest()},
		function () {return new ActiveXObject("Msxml2.XMLHTTP")},
		function () {return new ActiveXObject("Msxml3.XMLHTTP")},
		function () {return new ActiveXObject("Microsoft.XMLHTTP")}
	];
}

function createXMLHTTPObject() {
	var xmlhttp = false;
	var factories = XMLHttpFactories();
	for (var i=0;i<factories.length;i++) {
		try {
			xmlhttp = factories[i]();
		}
		catch (e) {
			continue;
		}
		break;
	}
	return xmlhttp;
}

/* Popin Script */
function showPopIn(popinName){
	popInBoxId=popinName;
	document.getElementById("divMakeInactive").style.display='block';
	
	document.getElementById("divMakeInactive").style.left = document.body.scrollLeft;
	document.getElementById("divMakeInactive").style.top = document.body.scrollTop;
	document.getElementById("divMakeInactive").style.width = document.body.clientWidth;
	document.getElementById("divMakeInactive").style.height = document.body.clientHeight;

	
	document.getElementById(popinName).style.display='';	
	document.getElementById(popinName).style.left = document.body.scrollLeft;
	document.getElementById(popinName).style.top = document.body.scrollTop;
	document.getElementById(popinName).style.width = document.body.clientWidth;
	document.getElementById(popinName).style.height = document.body.clientHeight;
}
window.onresize=function()
{
try{
	document.getElementById(popInBoxId).style.left = document.body.scrollLeft;
	document.getElementById(popInBoxId).style.top = document.body.scrollTop;
	document.getElementById(popInBoxId).style.width = document.body.clientWidth;
	document.getElementById(popInBoxId).style.height = document.body.clientHeight;

	document.getElementById("divMakeInactive").style.left = document.body.scrollLeft;
	document.getElementById("divMakeInactive").style.top = document.body.scrollTop;
	document.getElementById("divMakeInactive").style.width = document.body.clientWidth;
	document.getElementById("divMakeInactive").style.height = document.body.clientHeight;
} catch(err) {
	window.status = err;
}
};

window.onscroll=function()
{
try{
	document.getElementById(popInBoxId).style.left = document.body.scrollLeft;
	document.getElementById(popInBoxId).style.top = document.body.scrollTop;
	document.getElementById(popInBoxId).style.width = document.body.clientWidth;
	document.getElementById(popInBoxId).style.height = document.body.clientHeight;

	document.getElementById("divMakeInactive").style.left = document.body.scrollLeft;
	document.getElementById("divMakeInactive").style.top = document.body.scrollTop;
	document.getElementById("divMakeInactive").style.width = document.body.clientWidth;
	document.getElementById("divMakeInactive").style.height = document.body.clientHeight;
} catch(err){
	window.status = err;
}
};
function hideConfirm(boxId)
{
	document.getElementById("divMakeInactive").style.display = 'none';
	document.getElementById(boxId).style.display = 'none';
}

/* Popin Script */

function trimAll(sString)
{
	while (sString.substring(0,1) == ' ') {
		sString = sString.substring(1, sString.length);
	}
	while (sString.substring(sString.length-1, sString.length) == ' ') {
		sString = sString.substring(0,sString.length-1);
	}
	return sString;
}

   function isInteger (s)
   {
      var i;

      if (isEmpty(s))
      if (isInteger.arguments.length == 1) return 0;
      else return (isInteger.arguments[1] == true);

      for (i = 0; i < s.length; i++)
      {
         var c = s.charAt(i);

         if (!isDigit(c)) return false;
      }

      return true;
   }
	function isEmpty(s)
	{
		return ((s == null) || (s.length == 0))
	}
	
	function isDigit (c)
	{
		return ((c >= "0") && (c <= "9"))
	}
