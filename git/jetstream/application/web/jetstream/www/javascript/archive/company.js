var linkarray='';
$(document).ready(function() { 
	$('#CompanyForm').ajaxForm({
		dataType: 'json'
        , success: function(data) { 
        	$('#accountView').html(data.view);
        	$('#main_title').html(data.title);
			$.unblockUI();
		} 
	});

	$('#addNote').hide();
});

function processData(result) {
	try
	{
		try
		{		
			result=eval(result);
		}
		catch(e)
		{
			result=JSON.decode(result);
		}
		if(result.result.GETFORMATTEDVAR != null)
		{
			var resStatus = explode( "_",result.result.GETFORMATTEDVAR);
			var accountId = resStatus['1'];
		}
		var tblAddCompany=document.getElementById('addNewCompany');
		$('.view').html('');
		
		if(result.result.STATUS=='FALSE')
		{
			for(var key in result.result.ERROR)
			{
				$("#spn_"+key).hide();	
				$("#spn_"+key).fadeIn('slow');
				document.getElementById('spn_'+key).innerHTML='&#32;'+result.result.ERROR[key];
			}
		}
		else if(result.result.STATUS=='OK')
		{
			if(resStatus['0']=='UPDATE')
			{
				$('#accountView').html(result.viewdata);
				$('#accountLBView').html(result.viewlbdata);
				$('#AccountID').attr('value', accountId);
				$('#updAccountID').attr('value', accountId);
				// document.getElementById('accountView').innerHTML=result.viewdata;

				// document.getElementById('accountLBView').innerHTML='';				
				// document.getElementById('accountLBView').innerHTML=result.viewlbdata;

				// document.getElementById('AccountID').value=accountId;
				// document.getElementById('updAccountID').value=accountId;

				$(".datepicker").datepicker({ 
						showOn: "both", 
						buttonImage: "../images/calendar.gif", 
						buttonImageOnly: true 
				});



				$('.view').show();
			
				// $('.edit').hide();
				// $('.view').fadeIn('slow');
				// $('.clsSucc').fadeIn('slow');
				// $('.view').show();
				// $('.clsSucc').show();
				$.unblockUI();
				
				$('#succMsg').html('Account updated successfully.');
			}
			if(resStatus['0']=='INSERT')
			{
				if(result.addType) {
					location.href = 'slipstream.php?action=contact&contactId='+accountId;
					$.unblockUI();
				}
				else
				{
					/*document.getElementById('accountView').innerHTML='';				
					document.getElementById('accountView').innerHTML=result.viewdata;

					document.getElementById('accountLBView').innerHTML='';				
					document.getElementById('accountLBView').innerHTML=result.viewlbdata;

					document.getElementById('AccountID').value=accountId;
					document.getElementById('updAccountID').value=accountId;

					$(".datepicker").datepicker({ 
							showOn: "both", 
							buttonImage: "../images/calendar.gif", 
							buttonImageOnly: true 
					});

					document.getElementById('succMsg').innerHTML='New Account added successfully.';

					document.getElementById('addContactImage').innerHTML="<a title='Add' href='slipstream.php?action=contact&accountId="+accountId+"'><img src='images/add20.png'></a>";	


					$('.edit').hide();
					$('.view').fadeIn('slow');
					$('.clsSucc').fadeIn('slow');
					$('.view').show();
					$('.clsSucc').show();
					$.unblockUI();*/
					location.href = 'slipstream.php?action=company&accountId='+accountId;
					$.unblockUI();
				}
			}
		}
	} 
	catch(e) 
	{
		alert('Err :'+e);
	}
	
}
function editAccountInfo() {
	//$.blockUI({ message: $('#editCompany'),
//});
	$.blockUI({ message: $('#editCompany'),
		css: { 
		top:  (jQuery(window).height() - 300) /2 + 'px', 
		left: (jQuery(window).width() - 750) /2 + 'px',
		width: '750px' 
	}});
	$(".datepicker").datepicker({ 
		showOn: "both", 
		buttonImage: "../images/calendar.gif", 
		buttonImageOnly: true 
	});	
}

function addContact(id) {
	$.blockUI({ message: $('#addNewContact'),
		css: { 
		backgroundColor: '#FFFFFF',
		color: '#000000',
		top:  (jQuery(window).height() - 300) /2 + 'px', 
		left: (jQuery(window).width() - 750) /2 + 'px', 
		width: '750px' 
		}});
	$('.editDIV').show();
}

function saveContact() {
	$("#frmAddContact").ajaxSubmit(processData); 
}
function addContactCancel() {
	$.unblockUI();
	$('.editDIV').hide();
}