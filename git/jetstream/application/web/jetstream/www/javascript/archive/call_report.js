function toggleCallReport(controlId, date_frm, date_to) {
	if ($('#' + controlId).css('display') == 'none' || $('#' + controlId).css('display') == '') {
		$('#' + controlId).slideDown("fast");
		// css('display', 'block');
		$('#toggle_' + controlId).attr('src', 'images/minus.gif');
		if (!$('#' + controlId).attr('loaded')) {
			$('#' + controlId).attr('loaded', true);
			AjaxLoadReport(controlId, date_frm, date_to);
		}
	} else {
		$('#' + controlId).slideUp("fast");
		// css('display', 'none');
		$('#toggle_' + controlId).attr('src', 'images/plus.gif');
	}
}

function AjaxLoadReport(detail_id, date_frm, date_to) {
	$.post("ajax/ajax.reportdetails.php", {
		request_id :detail_id,
		dt_frm :date_frm,
		dt_to :date_to
	}, function(data, success) {
		$("#" + detail_id).html(data);
	}, "html");
}