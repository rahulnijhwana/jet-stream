
this.EventPreview = function(calendar){	
	/* CONFIG */
	xOffset = 10;
	yOffset = 10;		
	cached_eventid = 0;
	
	$("body").append("<p id='preview'>Loading</p>");
		
	$(".preview").hover(function(e){		
		var eventid = 0;
			eventid = this.id;		
		
		//$("body").append("<p id='preview'>Loading</p>");
		
		$("#preview")
			.html('Loading')
			.css("top",(e.pageY - xOffset) + "px")
			.css("left",((e.pageX - (yOffset + $("#preview").width())) > 0 ? (e.pageX - (yOffset + $("#preview").width())) : (e.pageX + yOffset)) + "px")
		
		
		
		if (!$("#preview").data(eventid)) {	
			$("#preview").data(eventid, $(this).html())	;
			$("#preview").load('ajax/ajax.EventTooltip.php?EventID='+eventid, '', function() {
			$(this)
				.css("top",(e.pageY - xOffset) + "px")
				.css("left",((e.pageX - (yOffset + $("#preview").width())) > 0 ? (e.pageX - (yOffset + $("#preview").width())) : (e.pageX + yOffset)) + "px");
				$("#preview").data(eventid, $(this).html())	;	
			});
		} else {						
			$("#preview").html($("#preview").data(eventid));			
			$("#preview")
			.css("top",(e.pageY - xOffset) + "px")
			.css("left",((e.pageX - (yOffset + $("#preview").width())) > 0 ? (e.pageX - (yOffset + $("#preview").width())) : (e.pageX + yOffset)) + "px")
		}
		
		if($('#preview').is(':hidden')) $("#preview").show();

    },
	function(){
		$("#preview").hide();
    });	
	
	
	//$(".preview").blur(function(e){
		//$("#preview").hide('fast');
	//});
	
};
