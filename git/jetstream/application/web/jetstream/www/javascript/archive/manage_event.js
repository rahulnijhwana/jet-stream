$(document).ready(function() {   
$('#frmEvent').ajaxForm({
	dataType: 'json', 
	success: processPostAddEvent
});
});
function processPostAddEvent(result)
{
	$('.error').html('');
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);
	}
	if(result.STATUS=='OK')
	{
		if(result.GETFORMATTEDVAR=='UPDATE')
		{
			alert('Event updated successfully');
			window.location='?action=event';
		}
		else
		{
			alert('Event added successfully');
			window.location='?action=event';
		}
			
		clearForm();
	}
	else
	{
		for(var key in result.ERROR)
			$('#spn_'+key).html(result.ERROR[key]);
	}
}
function editEventType(eventTypeId)
{
	$.post(
	'./ajax/ajax.addevent.php',
	{
		eventTypeId:eventTypeId,				
		actionType:'LoadToEdit'
	}
	,processPostLoadToEdit
	);		
}
function processPostLoadToEdit(result)
{
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);
	}
	if(result.STATUS=='OK')
	{
		document.getElementById('EventName').value=result.EVENTNAME;
		document.getElementById('EventColor').value=result.EVENTCOLOR;
		document.getElementById('EventTypeID').value=result.EVENTID;		
		document.getElementById('btnSubmit').value='Save';		
		document.getElementById('bgSpan').style.backgroundColor='#'+result.EVENTCOLOR;
	}
	else
		alert('Unable to find information for this event type');
}
function deleteEventType(eventTypeId,eventTypeName)
{
	if(confirm('Do you want to delete '+eventTypeName+' ?'))
	{
		$.post(
		'./ajax/ajax.addevent.php',
		{
			eventTypeId:eventTypeId,	
			eventTypeName:eventTypeName,
			actionType:'deleteEvent'
		}
		,processPostDeleteEvent
		);				
	}
}
function processPostDeleteEvent(result)
{
	try
	{
		result=eval(result);
	}
	catch(e)
	{
		result=JSON.decode(result);
	}
	if(result.STATUS=='OK')
	{
		alert("Event Type "+result.EVENTNAME+" Deleted Successfully");
		document.getElementById('tr_'+result.EVENTID).style.display='none';
	}
	else
	{
		alert(result.MSG);
	}
}
function clearForm()
{
	document.getElementById('EventName').value='';
	document.getElementById('EventColor').value='';
	document.getElementById('EventTypeID').value='';	
	$('.error').html('');	
	document.getElementById('btnSubmit').value='Submit';
}
function changeDivColor(col)
{
	$('#colorSelector div').css('backgroundColor', '#' + col);
}
function setColor(el)
{
	document.getElementById('EventColor').value=el.getAttribute('colorCode');
	document.getElementById('bgSpan').style.backgroundColor='#'+el.getAttribute('colorCode');
	document.getElementById('colorHolder').style.display='none';	
}
function resetPallet()
{
	document.getElementById('colorHolder').style.display='block';	
}