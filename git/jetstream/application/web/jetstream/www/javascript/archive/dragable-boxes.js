	
	var container = 5;	
	var innerDivNo = 2;
	var sectionArr =  new Array();
	sectionArr[0]="Saab";
	sectionArr[1]="Volvo";
	sectionArr[2]="BMW";
	var columnParentBoxId = 'floatingBoxParentContainer';
	var transparencyWhenDragging = true;
	var dragObjectBorderWidth = 1;	
	var columnParentBox;	
	
	var boxIndex = 0;	
	var dragableBoxesArray = new Array();
	
	var dragDropCounter = -1;
	var dragObject = false;
	var dragObjectNextSibling = false;
	var dragObjectParent = false;
	var destinationObj = false;
	
	var mouse_x;
	var mouse_y;	
	var el_x;
	var el_y;	
	
	var rectangleDiv;
	var okToMove = true;
	var documentHeight = false;
	var documentScrollHeight = false;
	var opera = navigator.userAgent.toLowerCase().indexOf('opera')>=0?true:false;
		
	function initDragDropBox(e)
	{		
		dragDropCounter = 1;
		if(document.all)e = event;
		
		if (e.target) source = e.target;
			else if (e.srcElement) source = e.srcElement;
			if (source.nodeType == 3) // defeat Safari bug
				source = source.parentNode;
		
		if(source.tagName.toLowerCase()=='img' || source.tagName.toLowerCase()=='a' || source.tagName.toLowerCase()=='input' || source.tagName.toLowerCase()=='td' || source.tagName.toLowerCase()=='tr' || source.tagName.toLowerCase()=='table')return;
		
	
		mouse_x = e.clientX;
		mouse_y = e.clientY;	
		var numericId = this.id.replace(/[^0-9]/g,'');
		el_x = getLeftPos(this.parentNode.parentNode)/1;
		el_y = getTopPos(this.parentNode.parentNode)/1 - document.documentElement.scrollTop;
			
		dragObject = this.parentNode.parentNode;
		
		documentScrollHeight = document.documentElement.scrollHeight + 100 + dragObject.offsetHeight;
		
		
		if(dragObject.nextSibling){
			dragObjectNextSibling = dragObject.nextSibling;
			if(dragObjectNextSibling.tagName!='DIV')dragObjectNextSibling = dragObjectNextSibling.nextSibling;
		}
		dragObjectParent = dragableBoxesArray[numericId]['parentObj'];
			
		dragDropCounter = 0;
		initDragDropBoxTimer();	
		
		return false;
	}
	
	
	function initDragDropBoxTimer()
	{
		if(dragDropCounter>=0 && dragDropCounter<10){
			dragDropCounter++;
			setTimeout('initDragDropBoxTimer()',10);
			return;
		}

		
	}

	function moveDragableElement(e){
		if(document.all)e = event;
		if(dragDropCounter<10)return;
		
		if(document.all && e.button!=1 && !opera){
			stop_dragDropElement();
			return;
		}
		
		if(document.body!=dragObject.parentNode){
			dragObject.style.width = (dragObject.offsetWidth - (dragObjectBorderWidth*2)) + 'px';
			dragObject.style.position = 'absolute';	
			dragObject.style.textAlign = 'left';
			if(transparencyWhenDragging){	
				dragObject.style.filter = 'alpha(opacity=70)';
				dragObject.style.opacity = '0.7';
			}	
			dragObject.parentNode.insertBefore(rectangleDiv,dragObject);
			rectangleDiv.style.display='block';
			document.body.appendChild(dragObject);

			rectangleDiv.style.width = dragObject.style.width;
			rectangleDiv.style.height = (dragObject.offsetHeight - (dragObjectBorderWidth*2)) + 'px'; 
			
		}
		
		var leftPos = e.clientX;
		var topPos = e.clientY + document.documentElement.scrollTop;
		
		dragObject.style.left = (e.clientX - mouse_x + el_x) + 'px';
		dragObject.style.top = (el_y - mouse_y + e.clientY + document.documentElement.scrollTop) + 'px';
								
		if(!okToMove)return;
		okToMove = false;

		destinationObj = false;	
		rectangleDiv.style.display = 'none'; 
		
		var objFound = false;
		var tmpParentArray = new Array();
		
		if(!objFound){
			for(var no=1;no<dragableBoxesArray.length;no++){
				if(dragableBoxesArray[no]['obj']==dragObject)continue;
				tmpParentArray[dragableBoxesArray[no]['obj'].parentNode.id] = true;
				if(!objFound){
					var tmpX = getLeftPos(dragableBoxesArray[no]['obj']);
					var tmpY = getTopPos(dragableBoxesArray[no]['obj']);

					if(leftPos>tmpX && leftPos<(tmpX + dragableBoxesArray[no]['obj'].offsetWidth) && topPos>(tmpY-20) && topPos<(tmpY + (dragableBoxesArray[no]['obj'].offsetHeight/2))){
						destinationObj = dragableBoxesArray[no]['obj'];
						destinationObj.parentNode.insertBefore(rectangleDiv,dragableBoxesArray[no]['obj']);
						rectangleDiv.style.display = 'block';
						objFound = true;
						break;
						
					}
					
					if(leftPos>tmpX && leftPos<(tmpX + dragableBoxesArray[no]['obj'].offsetWidth) && topPos>=(tmpY + (dragableBoxesArray[no]['obj'].offsetHeight/2)) && topPos<(tmpY + dragableBoxesArray[no]['obj'].offsetHeight)){
						objFound = true;
						if(dragableBoxesArray[no]['obj'].nextSibling){
							
							destinationObj = dragableBoxesArray[no]['obj'].nextSibling;
							if(!destinationObj.tagName)destinationObj = destinationObj.nextSibling;
							if(destinationObj!=rectangleDiv)destinationObj.parentNode.insertBefore(rectangleDiv,destinationObj);
						}else{
							destinationObj = dragableBoxesArray[no]['obj'].parentNode;
							dragableBoxesArray[no]['obj'].parentNode.appendChild(rectangleDiv);
						}
						rectangleDiv.style.display = 'block';
						break;					
					}
					
					
					if(!dragableBoxesArray[no]['obj'].nextSibling && leftPos>tmpX && leftPos<(tmpX + dragableBoxesArray[no]['obj'].offsetWidth)
					&& topPos>topPos>(tmpY + (dragableBoxesArray[no]['obj'].offsetHeight))){
						destinationObj = dragableBoxesArray[no]['obj'].parentNode;
						dragableBoxesArray[no]['obj'].parentNode.appendChild(rectangleDiv);	
						rectangleDiv.style.display = 'block';	
						objFound = true;				
						
					}
				}
				
			}
		
		}
		
		if(!objFound){
			
			for(var no=4;no<((sectionArr.length*2)+4);no++){
				if(!objFound){
					var obj = document.getElementById('dragableBoxesColumn' + no);			
					
						var left = getLeftPos(obj)/1;						
					
						var width = obj.offsetWidth;
						if(leftPos>left && leftPos<(left+width)){
							destinationObj = obj;
							obj.appendChild(rectangleDiv);
							rectangleDiv.style.display='block';
							objFound=true;		
						}				
				}
			}		
		}

		setTimeout('okToMove=true',5);		
	}
	
	function stop_dragDropElement()
	{
		if(dragDropCounter<10){
			dragDropCounter = -1
			return;
		}
		dragDropCounter = -1;
		if(transparencyWhenDragging){
			dragObject.style.filter = null;
			dragObject.style.opacity = null;
		}		
		dragObject.style.position = 'static';
		dragObject.style.width = null;
		var numericId = dragObject.id.replace(/[^0-9]/g,'');
		if(destinationObj && destinationObj.id!=dragObject.id){
			
			if(destinationObj.id.indexOf('dragableBoxesColumn')>=0){
				destinationObj.appendChild(dragObject);
				dragableBoxesArray[numericId]['parentObj'] = destinationObj;
			}else{
				destinationObj.parentNode.insertBefore(dragObject,destinationObj);
				dragableBoxesArray[numericId]['parentObj'] = destinationObj.parentNode;
			}
		}else{
			if(dragObjectNextSibling){
				dragObjectParent.insertBefore(dragObject,dragObjectNextSibling);	
			}else{
				dragObjectParent.appendChild(dragObject);
			}				
		}
		
		rectangleDiv.style.display = 'none'; 
		dragObject = false;
		dragObjectNextSibling = false;
		destinationObj = false;

		documentHeight = document.documentElement.clientHeight;	
		
		
		//var testArr = getPosition('left', 'dragableBoxesColumn1');
		//updatePosition(1,testArr,1);
		//var testArr = getPosition('middle','dragableBoxesColumn2');
		//updatePosition(2,testArr,1);
		var testArr = getPosition('right','dragableBoxesColumn3');
		//updatePosition(3,testArr,0);
	}

	
	function getPosition(pos, divid) {
		//var childNodeArray = document.getElementById(divid).childNodes;
		var childNodeArray = document.getElementById(divid).getElementsByTagName('DIV');
		var fieldsArr = new Array();
		var j =0;
		for (var i in childNodeArray) {			
			try {
				if(childNodeArray[i].className == 'dragableBoxHeader' && childNodeArray[i].id != '' && typeof(childNodeArray[i].id) != 'undefined') {
				
				fieldsArr[j] = childNodeArray[i].id;
					
				j = j+1;	

				}
			} catch(e){}
		}
		return fieldsArr;
	}
	function updatePosition(align,fields,enable){
		queryString='action=updatePosition&align='+align+'&fieldValues='+ fields+'&formType='+formType+'&enable='+enable;
	$.post('./ajax/ajax.user.php?'+queryString);
	
	}
	
	function getTopPos(inputObj)
	{		
	  var returnValue = inputObj.offsetTop;
	  while((inputObj = inputObj.offsetParent) != null){
	  	if(inputObj.tagName!='HTML')returnValue += inputObj.offsetTop;
	  }
	  return returnValue;
	}
	
	function getLeftPos(inputObj)
	{
	  var returnValue = inputObj.offsetLeft;
	  while((inputObj = inputObj.offsetParent) != null){
	  	if(inputObj.tagName!='HTML')returnValue += inputObj.offsetLeft;
	  }
	  return returnValue;
	}
		
	
	
	function boxHeader(parentObj,notDrabable, divid)
	{
		var div = document.createElement('DIV');
		div.className = 'dragableBoxHeader';
		div.id = divid + '_' + boxIndex;
		if(!notDrabable){
			div.onmousedown = initDragDropBox;
			div.style.cursor = 'move';
		}
		
		var textSpan = document.createElement('SPAN');
		textSpan.id = 'dragableBoxHeader_txt' + boxIndex;
		div.appendChild(textSpan);
				
		parentObj.appendChild(div);	
	}
	
	
	function addBoxContentContainer(parentObj, heightOfBox, data)
	{
		var div = document.createElement('DIV');
		div.className = 'dragableBoxContent';
		if(opera)div.style.clear='none';
		//div.id = 'dragableBoxContent' + boxIndex;
		div.innerHTML = data;
		parentObj.appendChild(div);			
		if(heightOfBox && heightOfBox/1>40){
			div.style.height = heightOfBox + 'px';
			div.setAttribute('heightOfBox',heightOfBox);
			div.heightOfBox = heightOfBox;	
			if(document.all)div.style.overflowY = 'auto';else div.style.overflow='-moz-scrollbars-vertical;';
			if(opera)div.style.overflow='auto';
		}		
	}
	

	function createBox(columnIndex,heightOfBox, data, divid, uniqueIdentifier,notDragable)
	{
		boxIndex++;
		
		var maindiv = document.createElement('DIV');
		maindiv.className = 'dragableBox';
		maindiv.id = 'dragableBox' + boxIndex;
		
		var div = document.createElement('DIV');
		div.className='dragableBoxInner';
		maindiv.appendChild(div);		
		
		boxHeader(div, notDragable, divid);
		addBoxContentContainer(div, heightOfBox, data);
		
		
		var obj = document.getElementById('dragableBoxesColumn' + columnIndex);	
	
		var subs = obj.getElementsByTagName('DIV');
		if(subs.length>0){
			obj.insertBefore(maindiv,subs[0]);
		}else{
			obj.appendChild(maindiv);
		}
		
		dragableBoxesArray[boxIndex] = new Array();
		dragableBoxesArray[boxIndex]['obj'] = maindiv;
		dragableBoxesArray[boxIndex]['parentObj'] = maindiv.parentNode;
		dragableBoxesArray[boxIndex]['uniqueIdentifier'] = uniqueIdentifier;
		dragableBoxesArray[boxIndex]['heightOfBox'] = heightOfBox;
		dragableBoxesArray[boxIndex]['boxState'] = 1;	// Expanded		
	}
	
	function initBoxes()
	{

		if(!columnParentBoxId){
			alert('No parent box defined for your columns');
			return;
		}
		columnParentMainBox = document.getElementById(columnParentBoxId);
		columnParentMainBox.style.cssText = 'vertical-align:top;';
		//var columnWidth = Math.floor(100/container);
		var columnWidth = Math.floor(100/3);
		var sumWidth = 0;
		var br = document.createElement('br');
		for(var i=0;i<sectionArr.length;i++){
			var innerDiv = document.createElement('DIV');
			innerDiv.id = 'innerDiv' + (i+1);
			innerDiv.style.padding = '10px';
			innerDiv.style.margin = '10px';
			//innerDiv.style.display='table-cell';
			innerDiv.style.cssText='float:left;';
			innerDiv.style.border ='2px solid rgb(181, 226, 254)';
			//innerDiv.style.width = '350px';
			innerDiv.style.width = columnWidth + '%';
			//innerDiv.style.height = '100px';
			columnParentMainBox.appendChild(br);
			columnParentMainBox.appendChild(innerDiv);
			columnParentMainBox.appendChild(br);
			
		}
		container = 2;
		var colNo = 3;
		for(var i=0;i<sectionArr.length;i++){
			//var 'columnParentBox'+(i+1) = document.getElementById('innerDiv' + (i+1));
			for(var no=0;no<container;no++){
				colNo = colNo +1;
				var div = document.createElement('DIV');
				//if(no==(container-1))columnWidth = 99 - sumWidth;
				if(no==(7-1))columnWidth = 99 - sumWidth;
				sumWidth = sumWidth + columnWidth;
				div.style.cssText = 'float:left;width:'+columnWidth+'%;padding:0px;margin:0px;';
				div.style.height='100%';
				div.style.styleFloat='left';
				div.style.width = columnWidth + '%';
				//div.style.width ='150px';
				div.style.padding = '0px';
				div.style.margin = '0px';
				div.id = 'dragableBoxesColumn' + (colNo);
				document.getElementById('innerDiv' + (i+1)).appendChild(div);
				var clearObj = document.createElement('HR');	
				clearObj.style.clear = 'both';
				clearObj.style.visibility = 'hidden';
				div.appendChild(clearObj);
			}
		}
		var div = document.createElement('DIV');
		//if(no==(container-1))columnWidth = 99 - sumWidth;
		if(no==(7-1))columnWidth = 99 - sumWidth;
		sumWidth = sumWidth + columnWidth;
		div.style.cssText = 'float:left;width:'+columnWidth+'%;padding:0px;margin:0px;';
		div.style.height='100%';
		div.style.styleFloat='left';
		div.style.width = columnWidth + '%';
		//div.style.width ='180px';
		div.style.padding = '0px';
		div.style.margin = '0px';
		div.id = 'dragableBoxesColumn3';
		columnParentMainBox.appendChild(div);
		var clearObj = document.createElement('HR');	
		clearObj.style.clear = 'both';
		clearObj.style.visibility = 'hidden';
		div.appendChild(clearObj);
		
		var clearingDiv = document.createElement('DIV');
		columnParentMainBox.appendChild(clearingDiv);
		for(var i =0; i<sectionArr.length;i++){
			document.getElementById('innerDiv' + (i+1)).appendChild(clearingDiv);
		}
		clearingDiv.style.clear='both';


		rectangleDiv = document.createElement('DIV');
		//rectangleDiv.id = 'rectangleDiv';
		rectangleDiv.style.display = 'none';
		document.body.appendChild(rectangleDiv);


		document.body.onmousemove = moveDragableElement;
		document.body.onmouseup = stop_dragDropElement;
		documentHeight = document.documentElement.clientHeight;	
		dataLayout();
	}

	

	
	window.onload = initBoxes;