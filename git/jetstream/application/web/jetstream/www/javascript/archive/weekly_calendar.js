$(document).ready( function() {
	/*
	 * $('#addEvent').ajaxForm({ dataType: 'json', success: processPostData });
	 * $(function() { $('.tool_tip *').tooltip();
	 * 
	 * });
	 */
	$.post("ajax/ajax.DailyCalendar.php", {
		Type :'weekly',
		FirstDate :document.getElementById('FirstDate').value,
		LastDate :document.getElementById('LastDate').value
	}, processPostData);
});

function stripslashes(str) {
	str = str.replace(/\\'/g, '\'');
	str = str.replace(/\\"/g, '"');
	str = str.replace(/\\\\/g, '\\');
	str = str.replace(/\\0/g, '\0');
	return str;
}

function getEventInfo(type, subject, description, time, contact) {
	var info = "<div>" + type + "</div><div>" + time + "</div><div>Subject: " + subject + "</div><div>Description: "
			+ description + "</div><div>Contact: " + contact + "</div>";
	info = info.replace("'", "&#39;");
	return info;
}

function getEvent(type) {
	if (type == "next") {
		var FirstDate = document.getElementById('FirstDate').value;
		var LastDate = document.getElementById('LastDate').value;

		$.post('./ajax/ajax.DailyCalendar.php', {
			FirstDateVal :FirstDate,
			LastDateVal :LastDate,
			actionType :'NextWeek'
		}, processDayData);
	}
	if (type == "prev") {
		var FirstDate = document.getElementById('FirstDate').value;
		var LastDate = document.getElementById('LastDate').value;

		$.post('./ajax/ajax.DailyCalendar.php', {
			FirstDateVal :FirstDate,
			LastDateVal :LastDate,
			actionType :'PrevWeek'
		}, processDayData);
	}
}
function processDayData(result) {
	try {
		try {
			result = eval(result);
		} catch (e) {
			result = JSON.decode(result);
		}
		if (result.STATUS == 'PAGEEVENT') {
			document.getElementById('dailyEvent').innerHTML = document.getElementById('dailyEventBlank').innerHTML;
			document.getElementById('FirstDate').value = result.FIRSTDATE;
			document.getElementById('LastDate').value = result.LASTDATE;

			var initialdata = "<table border='0' cellpadding='0' cellspacing='0' width='90%' align='center'><tr>";
			var data = '';
			for (i = 0; i < result.WEEKDAY.length; i++) {
				var res = result.WEEKDAY[i];
				var dayVal = res.split("-");
				var data = data + "<td align='center'><a href='#' class='blueLinks'>" + dayVal[0] + "<br>" + dayVal[1]
						+ "&nbsp;" + dayVal[2] + "</a></td>";
			}
			document.getElementById('weekday').innerHTML = initialdata + '' + data + "</tr></table>";

			document.getElementById('tblHeaderTitle').innerHTML = "<div id='tblHeaderTitle'><a href='#' class=lrgBlackBold onclick='javascript:getEvent(\"prev\")'>&lt;</a>&nbsp;"
					+ result.DATETITLE
					+ "&nbsp;<a href=# class=lrgBlackBold onclick='javascript:getEvent(\"next\")'>&gt;</a></div>";

			var data = "<table border='0' cellpadding='0' cellspacing='0' align='center' width='100%'><tr><td width='100%' valign='top' style='padding-top:20px;'><table border='0' cellpadding='0' cellspacing='1' align='center' height='200' width='96%' bgcolor='#C0E6FE'>";

			for ( var i = 0; i < result.HOURCOUNT.length; i++) {
				data = data
						+ "<tr bgcolor='#FFFFFF'><td width='7%' class='hour' align='center' valign='top'  rowspan='4'><a href='#' class='whiteLinks'>"
						+ result.HOURCOUNT[i] + "</a></td><td valign='top' id='" + result.HOURCOUNT[i]
						+ "' style='height:10px;' class='blankCellHeight'>&nbsp;</td></tr>";

				for ( var j = 0; j < result.GETMIN.length; j++) {
					data = data + "<tr bgcolor='#FFFFFF'><td valign='top' id='" + result.HOURCOUNT[i] + "_"
							+ result.GETMIN[j] + "' height='10'></td></tr>";
				}
			}
			data = data + "</table></td></tr></table>";
			document.getElementById('dailyEvent').innerHTML = data;

			if (result.EVENTS.length != 0) {
				for (i = 0; i < result.EVENTS.length; i++) {
					var now = new Date(result.EVENTS[i]['StartDate']);
					var eventDay = now.getDay('dddd');

					var durHours = result.EVENTS[i]['DurationHours'];
					var durMins = result.EVENTS[i]['DurationMinutes'];
					var approxDurMinutes = (60 * durHours) + durMins;
					var cellNoHighlights = (approxDurMinutes / 15);
					var cellHeight = (cellNoHighlights * 11);
					var cellColor = result.EVENTS[i]['EventColor'];

					var dataEndTime = result.EVENTS[i]['EndDate'];
					var timeEnd = dataEndTime.replace("  ", " ");
					var expEndDate = timeEnd.split(" ");
					var endTime = expEndDate[1];

					var startTime = result.EVENTS[i]['StartTime'];
					var splitStartTime = startTime.split(":");
					var hour = splitStartTime[0];
					var min = splitStartTime[1];
					if (hour == "00") {
						var hr = "12";
						var type = "AM";
					}
					if (hour < "12" && hour != "00") {
						var hr = hour;
						var type = "AM";
					}
					if (hour > "12") {
						var hr = (hour - 12);
						if (hr < 10) {
							hr = "0" + hr;
						}
						var type = "PM";
					}
					if (hour == "12") {
						var hr = hour;
						var type = "PM";
					}
					if (eventDay == 6)
						;
					{
						var lPad = '522px';
					}
					if (eventDay == 0) {
						var lPad = '125px';
					}
					if (eventDay == 1) {
						var lPad = '175px';
					}
					if (eventDay == 2) {
						var lPad = '245px';
					}
					if (eventDay == 3) {
						var lPad = '315px';
					}
					if (eventDay == 4) {
						var lPad = '400px';
					}
					if (eventDay == 5) {
						var lPad = '475px';
					}
					if (min == "00") {
						var id = hr + "" + type;
						document.getElementById(id).innerHTML = "<a href='#' class='Links' onclick='javascript:location.href=\"slipstream.php?action=event&eventId="
								+ result.EVENTS[i]['EventID']
								+ "\"'><div class='tool_tip' title=\""
								+ getEventInfo(result.EVENTS[i]['EventName'], result.EVENTS[i]['Subject'],
										result.EVENTS[i]['Description'], hr + ':00' + type + ' to 11:30AM',
										result.CONTACTS[result.EVENTS[i]['ContactID']])
								+ "\"><div  style='position:absolute; float:left; border: black 0px solid; background-color:#"
								+ cellColor
								+ "; width:110px; height:"
								+ cellHeight
								+ "px; left:"
								+ lPad
								+ "' class='smlBlack'>"
								+ result.EVENTS[i]['EventName']
								+ "<br/>"
								+ hr
								+ ":00"
								+ type
								+ " to " + endTime + "</div></div></a>";

					} else {
						var id = hr + "" + type + "_" + min;
						document.getElementById(id).innerHTML = "<a href='#' class='Links' onclick='javascript:location.href=\"slipstream.php?action=event&eventId="
								+ result.EVENTS[i]['EventID']
								+ "\"'><div class='tool_tip' title='"
								+ getEventInfo(result.EVENTS[i]['EventName'], result.EVENTS[i]['Subject'],
										result.EVENTS[i]['Description'], hr + ':' + min + '' + type + ' to 11:30AM',
										result.CONTACTS[result.EVENTS[i]['ContactID']])
								+ "'><div style='position:absolute; float:left;; border: black 0px solid; background-color:#"
								+ cellColor
								+ "; width:110px; height:"
								+ cellHeight
								+ "px; left:"
								+ lPad
								+ "' class='smlBlack'>"
								+ result.EVENTS[i]['EventName']
								+ "<br/>"
								+ hr
								+ ":"
								+ min
								+ ""
								+ type + " to " + endTime + "</div></div></a>";
					}

				}
			}
		}
	} catch (e) {
		alert('Err :' + e);
	}
	$('.tool_tip').tooltip();
}
function processPostData(result) {
	try {
		try {
			result = eval(result);
		} catch (e) {
			result = JSON.decode(result);
		}
		if (result.STATUS == 'PAGEEVENTS') {
			for (i = 0; i < result.EVENTS.length; i++) {
				var now = new Date(result.EVENTS[i]['StartDate']);
				var eventDay = now.getDay('dddd');

				var durHours = result.EVENTS[i]['DurationHours'];
				var durMins = result.EVENTS[i]['DurationMinutes'];
				var approxDurMinutes = (60 * durHours) + durMins;
				var cellNoHighlights = (approxDurMinutes / 15);
				var cellHeight = (cellNoHighlights * 11);
				var cellColor = result.EVENTS[i]['EventColor'];

				var dataEndTime = result.EVENTS[i]['EndDate'];
				var timeEnd = dataEndTime.replace("  ", " ");
				var expEndDate = timeEnd.split(" ");
				var endTime = expEndDate[1];

				var startTime = result.EVENTS[i]['StartTime'];
				var splitStartTime = startTime.split(":");
				var hour = splitStartTime[0];
				var min = splitStartTime[1];
				if (hour == "00") {
					var hr = "12";
					var type = "AM";
				}
				if (hour < "12" && hour != "00") {
					var hr = hour;
					var type = "AM";
				}
				if (hour > "12") {
					var hr = (hour - 12);
					if (hr < 10) {
						hr = "0" + hr;
					}
					var type = "PM";
				}
				if (hour == "12") {
					var hr = hour;
					var type = "PM";
				}
				if (eventDay == 6)
					;
				{
					var lPad = '522px';
				}
				if (eventDay == 0) {
					var lPad = '125px';
				}
				if (eventDay == 1) {
					var lPad = '175px';
				}
				if (eventDay == 2) {
					var lPad = '245px';
				}
				if (eventDay == 3) {
					var lPad = '315px';
				}
				if (eventDay == 4) {
					var lPad = '400px';
				}
				if (eventDay == 5) {
					var lPad = '475px';
				}
				if (min == "00") {
					var id = hr + "" + type;
					// alert(id);
					document.getElementById(id).innerHTML = "<a href='#' class='Links' onclick='javascript:location.href=\"slipstream.php?action=event&eventId="
							+ result.EVENTS[i]['EventID']
							+ "\"'><div class='tool_tip' title='"
							+ getEventInfo(result.EVENTS[i]['EventName'], result.EVENTS[i]['Subject'],
									result.EVENTS[i]['Description'], hr + ':00' + type + ' to 11:30AM',
									result.CONTACTS[result.EVENTS[i]['ContactID']])
							+ "'><div style='position:absolute; float:200; border: black 0px solid; background-color:#"
							+ cellColor
							+ "; width:110px; height:"
							+ cellHeight
							+ "px; left:"
							+ lPad
							+ "' class='miniBlack' >"
							+ result.EVENTS[i]['EventName']
							+ "<br/>"
							+ hr
							+ ":00"
							+ type
							+ " to " + endTime + "</div></div></a>";

				} else {
					var id = hr + "" + type + "_" + min;
					// alert(id);
					document.getElementById(id).innerHTML = document.getElementById(id).innerHTML
							+ "<a href='#' class='Links' onclick='javascript:location.href=\"slipstream.php?action=event&eventId="
							+ result.EVENTS[i]['EventID']
							+ "\"'><div class='tool_tip' title='"
							+ getEventInfo(result.EVENTS[i]['EventName'], result.EVENTS[i]['Subject'],
									result.EVENTS[i]['Description'], hr + ':' + min + '' + type + ' to 11:30AM',
									result.CONTACTS[result.EVENTS[i]['ContactID']])
							+ "'><div style='position:absolute; float:200; border: black 0px solid; background-color:#"
							+ cellColor + "; width:110px; height:" + cellHeight + "px; left:" + lPad
							+ "' class='miniBlack'>" + result.EVENTS[i]['EventName'] + "<br/>" + hr + ":" + min + ""
							+ type + " to " + endTime + "</div></div></a>";
				}

			}
		}
	} catch (e) {
		alert('Err :' + e);
	}
	$('.tool_tip').tooltip();
}