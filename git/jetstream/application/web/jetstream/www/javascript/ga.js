/* google API */
function popupCenter(url, title, w, h) {
        var left = (window.screen.width/2)-(w/2);
        var top = (window.screen.height/2)-(h/2);
        window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

$(document).ready( function() {
    // run Google Contact Sync behind (time consuming for 1st processing)
	//$.ajax({
		//url: '/jetstream/ajax/ajax.runGoogleSync.php',
		//dataType: 'json',
		//method: 'get',
		//success: function(res){
		//}
	//});

    //REVIEW : how to check popup page result	
	$('#google-connect').live('click', function(e) {
        window.open('googleConnect.php');
        /***
        var w = 600,
            h = 500,
            url = 'googleConnect.php',
            title = 'Google Oauth',
            left = (window.screen.width/2)-(w/2),
            top = (window.screen.height/2)-(h/2),
            targetWin = window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
        ***/

        //popupCenter('googleConnect.php','Google Oauth','600','500');

        /*
        if ( targetWin.closed ) {
            alert('closed');
            targetWin.opener.location.reload(true);
        }
        */
		var that = $(e.target);
		that.attr('id','google-disconnect').text('Google Disconnect');
	});

	$('#google-disconnect').live('click', function(e) {
		var that = $(e.target);
		$.ajax({
			url: '/jetstream/ajax/ajax.GoogleDisconnect.php',
			dataType: 'json',
			method: 'post',
			success: function(res){
				if (res.code == '200') {
					alert('Google Disconnected');
					that.attr('id','google-connect').text('Google Connect');
				}
			}
		});
	});
});
