
function getMonth(type,target) {
        var month = $('#dashMonth').val();
        var year = $('#dashYear').val();
		var target = 0;
		try {
			// Get the selected user from edit-opportunity 
			target = ssSalesPerson.getSelectedVal();			
		} catch(err) {
		}
		
//		if (typeof webpath == 'undefined') {
//			var webpath = '';
//		}
		
        var ajax_url = webpath + '/ajax/ajax.dashboard.php?target=' + target;
		
        try {
			if(EventMiniCalendar) ajax_url = BASE_URL + '/ajax/ajax.dashboard.php?EventMiniCalendar=true&target=' + target;
        } catch(err) {}


        if(typeof target == 'undefined') {
			$.post(ajax_url,
				{
					monthVal : month,
					yearVal : year,
					callType : type
				},
				processMonthData
			);
        } else {
			$.post(ajax_url,
				{
					monthVal : month,
					yearVal : year,
					callType : type,
					target:target
				},
				processMonthData
			);
        }
}

function processMonthData(result) {
	result = ParseStatus(result);

	/*
	* Set the mini calendar parameters in the cookie.
	*/
	document.cookie = 'MiniCalMonth=' + result.month;
	document.cookie = 'MiniCalYear=' + result.year;

	$('#miniCalendar').html(result.result);
	$('#dashMonth').val(result.month);
	$('#dashYear').val(result.year);
}

function processMonthData(result) {
	result = ParseStatus(result);
	/*
	* Set the mini calendar parameters in the cookie.
	*/
	document.cookie = 'MiniCalMonth=' + result.month;
	document.cookie = 'MiniCalYear=' + result.year;

	$('#miniCalendar').html(result.result);
	$('#dashMonth').val(result.month);
	$('#dashYear').val(result.year);
}

function showToolTip() {
	$('#EventTypeLegend').fadeIn('slow');
	$('#EventTypeLegend').show();
}

function hideToopTip() {
	$('#EventTypeLegend').fadeOut('slow');
	$('#EventTypeLegend').hide();
}
                 
