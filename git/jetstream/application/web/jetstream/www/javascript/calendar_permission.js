// JavaScript Document
function saveCalendarPermission(user){
		writePer = $('.write');
		var yes='';
		var no='';
		$.each( writePer, function(){	
			if($(this).attr('checked'))	yes+= $(this).attr('person_id') +',';				 
			else no+= $(this).attr('person_id') +',';				 
		});
		readPer = $('.read');
		var read='';
		var unRead='';
		$.each( readPer, function(){	
			if($(this).attr('checked'))	read += $(this).attr('person_id') +',';				 
			else unRead += $(this).attr('person_id') +',';				 
		});
		$.post('./ajax/ajax.calendarPermission.php',
			{
				WritePermission : yes,
				UnWritePermission : no,
				ReadPermission : read,
				UnReadPermission : unRead,
				UserId : user,
				action : 'SaveCalendarPermission'
			},
				processSavePermission
			);
	
}

function processSavePermission(result){
	result = ParseStatus(result);
	
	if(result.STATUS=='OK')
	{
		alert('Calendar Permission Saved.');	
	}
}