/****************** Jet Menu **************************/
// jetfile_ * is for Files Operation 
// jetfolder_ * is for Folder Operation 
var loading_page = false;

function AjaxFileLoadPage(page) {	
	if (loading_page != true) {
		loading_page = true;

		var form_info = $("#SlipstreamSearch").serializeArray();
		form_info.push(({name:'Page', value:page}))

		// alert(form_info);
		// var Formdata = 'Page=' + page + '&' + $("#SlipstreamSearch").serialize();
		
		callTimer = true;
		var dashboardSearchType = 'basic';
		if($("#advanced_filter")) {
			var test = '';
			var advanced_data = Array();
			$("[name^='advfilterinput_']").each(function() {
				form_info.push({name:this.name, value:this.value});
			});
			form_info.push({name:'SortType', value:$('#SortType').val()});
			form_info.push({name:'SortAs', value:$('#SortAs').val()});
		}
		$('#dashboardsearch').fadeTo("fast", .1);
		$.post("ajax/file_search_results.php", form_info, function(data, success) {
			

			$('#dashboardsearch').html(data).fadeTo("fast", 1);
			loading_page = false;
			if ($('#chkMergeRecord').attr('checked')) {
				$('.mergeClass').css( {
					display :"inline"
				});
				for (i = 0; i < merge_contact.length; i++)
					$('#chk_' + merge_contact[i]['CID']).attr('checked', true);
				for (i = 0; i < merge_account.length; i++)
					$('#chk_' + merge_account[i]['ACID']).attr('checked', true);
			} else
				$('.mergeClass').css( {
					display :"none"
				});
			}, "html");
	}
}

function FileSearchReset() {
	document.SlipstreamSearch.reset(); 
}

function jetfileUpload( type, id, category ,cid, params){
/*		if ( category == 'Account' || category == 'account'){
			var sendItem = 'Account|' + cid;			
		} else if ( category == 'Contact' || category == 'contact') {
			var sendItem = 'Contact|' + cid;			
		} else if ( category == 'Company') {
			var sendItem = 'Company|' + cid;			
		} else {
			var sendItem = 'Contact|' + cid;			
		}*/
		var sendItem = category +'|' + cid;			
		
		var html = '';
		html += '<div id="fileQueue"></div>' ; 
		html += '<script type="text/javascript">' ; 
		html += '$(document).ready(function() {';

		html += "$('#fileInput').uploadify({" ; 
		html += "uploader : 'flash/uploadify.swf'," ;
		html += "script : 'ajax/ajax.uploadify.php?action="+ type +'|'+ id +'|'+  sendItem  + '|'+ params+ "&',"; 
		html += "cancelImg : 'images/cancel.png',";
		html += "auto : true";
		html += ', onComplete : function (a, b, c, d, e) {';
		html += "var mySplit = d.split('/script>');";
		html += "var myValue = mySplit[1].split('||');";
		html += 'AddTag(myValue[1]);';
		html += '}';
		html += "});});";
		html += '</script>';
		html += '<div id=multiselect222></div>';
		html += '<input id="fileInput" name="fileInput" type="file" /> ' ; 
		var nonFlash = '' ; 
		nonFlash += 'Your browser does not have Flash Player installed. <br>	'; 
		nonFlash += 'Please install Flash Player to continue. <br>	'; 
		nonFlash += '<br>To install Flash Player click <a href="http://get.adobe.com/flashplayer/" target=new >here</a>.<br>'; 
//		if (navigator.mimeTypes && navigator.mimeTypes["application/x-shockwave-flash"]) $.prompt(html,{ buttons: { Done: false } });
		if (FlashDetect.installed) {
			$.prompt(html,{ buttons: { Done: false } });
			GetTags('multiselect222', id  ); 
		} else $.prompt(nonFlash,{ buttons: { Done: false } });
}

function AddTag( id ){
	
	var tags = document.getElementById('TagSelectms2side__dx');
	var allTags = ''; 
	var x ; 
	for ( x = 0 ; x < tags.length ; x++ ){
		
		allTags += tags[x].value + '|' ;
	}
	jetfileAction('updateTags', id, allTags, '', '') ; 
}

function GetTags( name, id ){
	var html = '' ; 

	var html = $.post('ajax/ajax.JetFileAction.php', {
		action : 'getTags',
		jetfileid : id, 
		param1 : '',
		param2 : '' 
		}, function (data) {
			 $('#'+ name).html(data);
	 		$('#multiselect2').multiselect2side({ moveOptions: false });
		}
		, 'html' 
	);				
} 

function uploadFile(file) {

	var form_info = $("#JetFile").serializeArray();
	form_info.push({ name: 'action', value : 'upload' },{ name: 'file', value : file }, { name: 'folder', value : '1' }, { name: 'type', value : 'contact' });
	
	callTimer = true;
//	$('FileTree').fadeTo("fast", .1);
	$.post("ajax/ajax.UploadFile.php?" , form_info, function(data, success) {
		if ( data == 'error' ){
			alert("Error, Duplicate ID has been found.");
			AjaxLoadSlipstreamSearch('loadList', 0 , '');
			SaveSlipstreamSearchPrompt();
		}else {
			$(Target).html(data).fadeTo("fast", 1);	
			loading_page = false;
		} 
	});
	HideSearchType();
	
}

function jetfolderDelete( type, id, category , cid ) {
	alert(type + ' ' + id + ' ' + category + ' ' + cid);			
	
	$.post('ajax/ajax.JetFileAction.php', {
		action : 'checkFolderEmpty',
		jetfileid : id, 
		param1 : '',
		param2 : '' 
	}, function (data) {
		if ( data == '0') {
			if (confirm('Are you sure, you want to delete this folder? ')){
				jetfileAction( 'delete', id, '', '');
				alert("Folder Deleted");			
			}
		}
		else {
			if (confirm('This folder is not empty, do you want to delete all the files? ')){
				jetfileAction( 'deleteAll', id, '', '');
				alert("Folder Deleted");				
			}
		}
	}
	, 'text' 
	); 	
	
}

function jetfolderCreatenew( type, id, category ,cid ) {
	var html = '';
	html += '<p>Please enter the name of the folder: <input name="newname" type="text"></p>' ; 
	$.prompt(html,{
		  submit: function(v,m,f){ 
			if ( v == 'continue' ){
				if (f.newname == '') {
					alert('Please fill in the name.');
					return false; 
				}
				else{
					jetfileAction( 'createFolder', id, category, cid, f.newname);
					return true;
				}
			}
			else{
				return true; 
			}
		  },
		  buttons: { Continue:'continue', Cancel:'cancel' }
	});	
}

function jetfileRename( type, id, category ,cid ) {
	var html = '';
	html += '<p>Please enter the new name: <input name="newname" type="text"></p>' ; 
//	$.prompt( html,{callback: rename()}, { buttons: { Continue:true , Cancel: false } });
	$.prompt(html,{
		  submit: function(v,m,f){ 
			if ( v == 'continue' ){
				if (f.newname == '') {
					alert('Please fill in the name.');
					return false; 
				}
				else{
					jetfileAction( 'rename', id, f.newname, '');
					return true;
				}
			}
			else{
				return true; 
			}
		  },
		  buttons: { Continue:'continue', Cancel:'cancel' }
	});	
}

function jetfileAction(action, jetfileID, param1, param2, param3){
	$.post('ajax/ajax.JetFileAction.php', {
		action : action,
		jetfileid : jetfileID, 
		param1 : param1,
		param2 : param2, 
		param3 : param3 
	}, function(data) {}
	, 'text' 
	);
}

function jetfileDelete( type, id, category ,cid ) {
	confirm('Are you sure, you want to delete this file? '); 
	jetfileAction( 'delete', id, '', '');
	alert("File Deleted"); 
}

function jetfileMove( type, id, category , cid ) {

	var html = '';
	var temp = category + '_f';  
	var id2 = cid 
	html += '<script type="text/javascript">';
	html += '$(document).ready( function() {';
	html += " $('#fileTreeTree').fileTree({ root: 0, script: 'ajax/ajax.FileTree.php', folderEvent: 'click', expandSpeed: 750, collapseSpeed: 750, action:'"+ temp +"f' , id: "+ id2 +"  }, function(file) { ";
	html += '	alert("Done! ");';
	html += '});';
    html += '});';
	html +="</script>";
	html += '<div>Double Click on the destination folder: <input type="text" readonly max="30" size="10" id=movetolabel name=movetolabel> ' ;
	html += ' <input type="hidden" id=movetoid name=movetoid></div>' ; 
	html += '<div id="fileTreeTree" class="filetree" style="margin-left:20px"></div>' ; 
	
	$.prompt(html,{
		  submit: function(v,m,f){ 
			if ( v == 'continue' ){
				jetfileAction('Moveto', id, f.movetoid, '' , '');
				return true ; 
			}
			else{
				return true; 
			}
		  },
		  buttons: { Continue:'continue', Cancel:'cancel' }
	});	
}

function DownloadFile(action, jetfileID, param1, param2){
	var submitForm = getNewSubmitForm();
	if (action == 'file'){
		createNewFormElement(submitForm, "action", 'saveAs');
		if ( param1 != ''){
			createNewFormElement(submitForm, "param1", 'Account');
			createNewFormElement(submitForm, "param2", param1);		
		}
		else {
			createNewFormElement(submitForm, "param1", 'Contact');
			createNewFormElement(submitForm, "param2", param2);				
		}
	}  	
	else {
		createNewFormElement(submitForm, "action", action);
		createNewFormElement(submitForm, "param1", param1);
		createNewFormElement(submitForm, "param2", param2);	
	}
	
	createNewFormElement(submitForm, "jetfileid", jetfileID);
	submitForm.setAttribute("action", "ajax/ajax.JetFileAction.php");
	submitForm.submit();

}

function getNewSubmitForm(){
 var submitForm = document.createElement("FORM");
 document.body.appendChild(submitForm);
 submitForm.method = "POST";
 return submitForm;
}

function createNewFormElement(inputForm, elementName, elementValue){

 var newElement = document.createElement("input");
 newElement.setAttribute("name", elementName);
 newElement.setAttribute("type", "hidden");
 inputForm.appendChild(newElement);
 newElement.value = elementValue;
 return newElement;
}

function chooseFolder( id, labelname ){
	var formid = document.getElementById("movetoid") ; 
	formid.value = id ;
	var formlabel = document.getElementById("movetolabel") ; 
	formlabel.value = labelname ;
}

function Property( type, id, category , cid ) {

	var html = $.post('ajax/ajax.JetFileAction.php', {
		action : 'getProperty',
		jetfileid : id, 
		param1 : '',
		param2 : '' 
		}, function (data) {
		 	$.prompt(data,{
				callback: updateProperty,
				buttons: { Update: 'Update', Cancel: 'Cancel' }
			});
		}
		, 'html' 
	);	
}	

function updateProperty(v,m,f){
	var allTags = '' ; 
	for ( x = 0 ; x < f.propertytags.length ; x++ ){
		allTags += f.propertytags[x] + '|' ;
	}
	if(v == 'Update'){
		jetfileAction('updateProperty', f.jetid, f.propertyLabel, allTags, '');
	}  
}

function ToogleTags(){
	if ($("#tagdetails").css('display') == "none"	){
		$("#tagimg").attr("src", "images/minus.gif");
//		$("#tagsummary").hide("slow");
//		$("#tagdetails").show("slow");	
		$("#tagsummary").slideUp();
		$("#tagdetails").slideDown();	

	}
	else {
		$("#tagimg").attr("src", "images/plus.gif");
		$("#tagsummary").slideDown();
		$("#tagdetails").slideUp();
	}

} 



