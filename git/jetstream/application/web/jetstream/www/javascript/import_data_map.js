
function selectAllRadio() {
	if ($('#sel_all').attr('checked')) {
		$("input:radio.new").attr('checked', true);
	} else {
		$("input:radio.current").attr('checked', true);
	}
}


function removeItem(selected){
	
    $(".required").each(function(){
    	if( $(this).val() != selected){
    		$('option[value='+selected+']', this).remove();
    	}
	});

}

function resetSelect(selected, previous){

	$(".required").each(function(){
    	if( $(this).val() != selected && previous.val() != ''){
    		
    		previous.clone().appendTo(this);
    		
    		var isSelected = $(this).val();
        	$(this).html( $(this).sort_select_box() );
        	
        	var selectOne = $('option[value=""]', this);
        	var doNotMap = $('option[value="0"]', this);

        	$('option[value=""]', this).remove();
        	$('option[value="0"]', this).remove();
        	
        	$(this).prepend(doNotMap);
        	$(this).prepend(selectOne);
        	
        	$('option[value='+isSelected+']', this).attr('selected', 'selected');
    	}
	});
	
}

$.fn.sort_select_box = function(){
	
	var my_options = $("option", this);
	my_options.sort(function(a,b) {
		if (a.text > b.text) return 1;
		else if (a.text < b.text) return -1;
		else return 0
	})
	return my_options;
	
}

$(function() {

	$("select").addClass('required');
	$("select").find('option:contains("Company Name")').addClass('highlightOption');
	
	$("#submitMap").click(function(event){
		
		event.preventDefault();
		
		if($("#map").validate().form()){
			
			$.blockUI({
				message: '<h2 class="dark">Searching for existing company records...<br>Please wait</h2><br><img src="images/103.gif" />',
					css: { 
						border: 'none', 
						padding: '15px', 
						backgroundColor: '#fff', 
						color: '#fff',
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px'
						
					}
			}); 
	        
			var $form = $("#map"),
				term = $form.find( 'input[name="step"]' ).val(),
				file = $form.find( 'input[name="csv"]' ).val();
			
		    var $inputs = $('#map :input[type=select-one]');
	
		    var values = {};
		    $inputs.each(function() {
		        values[this.name] = $(this).val();
		    });
			
			$.post("slipstream.php?action=dataimport", { step: term, csv: file, arr: values }, function(data) {
			   $("#wrap").html(data);
			   $("html").scrollTop();
			   $.unblockUI();
			});
		}
	});
	
	


	

	
});