var form_data = new Array();//to store all form values
function GetFormData(form) {
	data = new Array();
	element = ['input','select','textarea'];
	for(i=0; i<element.length; i++) {
		$.each( form.find(element[i]), function(){			
			id = $(this).attr('name');
			switch($(this).attr('type')) {
				case 'text':
				case 'select-one':
				case 'textarea':
					data[id] = $(this).val();
					break;
				case 'checkbox':
				case 'radio':
					data[id] = $(this).attr('checked');
					break;
			}
		});
	}
	return data;	
}
function IfClosing() {
	return (confirm('Are you sure you want to cancel and lose this entry?'));
}
function RestoreData(form, form_data) {
		for(i=0; i<element.length; i++) {
			$.each( form.find(element[i]), function(){			
				id = $(this).attr('name');
				switch($(this).attr('type')) {
					case 'text':
					case 'select-one':
					case 'textarea':
						$(this).val(form_data[id]);
						break;
					case 'checkbox':
						$(this).attr('checked',form_data[id]);
						break;
				}
			});
		}
}
function IsModified(prev_data,cur_data) {
	try {
		for(var key in prev_data) {
			if(prev_data[key]!=cur_data[key])
				return true;
		}
	}
	catch(e){
		return true;
	}
	return false;
}
