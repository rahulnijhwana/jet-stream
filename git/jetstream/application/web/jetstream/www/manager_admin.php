<?php

define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

require_once(BASE_PATH . '/include/mpconstants.php');
require_once(BASE_PATH . '/include/class.SessionManager.php');
require_once(BASE_PATH . '/mpower/class.Header.php');
require_once(BASE_PATH . '/mpower/class.ManagerAdmin.php');
require_once(BASE_PATH . '/mpower/class.Footer.php');
require_once BASE_PATH . '/include/class.ReportingTree.php';
require_once(BASE_PATH . '/include/class.Language.php');
require_once(BASE_PATH . '/mpower/lib.html.php');
SessionManager::Init();
SessionManager::Validate();

$page_title = 'M-Power&trade; Manager Admin';

$header = new Header();

$manager_admin = new ManagerAdmin();

$footer = new Footer();

CreatePage($page_title, array($header, $manager_admin, $footer));
