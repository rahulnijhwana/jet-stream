<?php

	ob_start();
	session_start();
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
	
	require_once realpath(dirname(__FILE__) . '/autoload.php');
	require_once 'constants.php';
	$client = new Google_Client();
	
	$client->setApplicationName(GOOGLE_CLIENT_NAME);
	$client->setClientId(GOOGLE_CLIENT_ID);
	$client->setClientSecret(GOOGLE_CLIENT_SECRET);
	$client->setRedirectUri(GOOGLE_CALLBACK_URL);
    $client->setScopes(array(
        'https://www.googleapis.com/auth/plus.me', 
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/calendar',
        'https://www.googleapis.com/auth/calendar.readonly'
    ));
    $client->setAccessType('offline');
    $client->setApprovalPrompt('force');
    $cal = new Google_Service_Calendar($client);
    $task = new Google_Service_Tasks($client);
    
    
    $conn1 = mssql_pconnect('192.168.55.131','sa', 'tech');
	mssql_select_db('mpasac_test',$conn1);
	$row = mssql_query('select * from GoogleToken where PersonID = 10104', $conn1);
	$res = mssql_fetch_object($row);
	
	
	//$client->refreshToken($res->refresh_token);
	//print_r($client->getAccessToken());
    //exit;
    if (isset($_GET['logout'])) {
			unset($_SESSION['token']);
		}
	if (isset($_GET['code'])) {
		
			$client->authenticate($_GET['code']);
			$_SESSION['token'] = $client->getAccessToken();
			header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
			
		}
	if (isset($_SESSION['token'])) {
			$client->setAccessToken($_SESSION['token']);
		}
	if ($client->isAccessTokenExpired()){

			$client->refreshToken($res->refresh_token);
			$_SESSION['token'] = $client->getAccessToken();

		}

	if ($client->getAccessToken()) {
			$row1 = mssql_query('select * from GoogleToken', $conn1);
	while($res = mssql_fetch_object($row1)){
		$result[] = $res;
		}

		foreach($result as $res1){
//:::::::::::::::::::::::::::::::::::::::Event Start::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::		
			if(isset($res1->calendar_id)){
			$syncToken = $res1->google_sync_token;
			$optParams = array('syncToken' => $syncToken);
			$events = $cal->events->listEvents($res1->calendar_id,$optParams);
			$eventlis = $events->getItems();
						if($eventlis){
							$synctoken = $events->getNextSyncToken();
							$qry1 = "update GoogleToken set google_sync_token = '$synctoken' where PersonID =".$res1->PersonID;
							mssql_query($qry1, $conn1);
							$qry2 = 'select CompanyID from people where PersonID = '.$res1->PersonID;
							$row2 = mssql_query($qry2, $conn1);
							$ress = mssql_fetch_object($row2);
							//print_r($ress->CompanyID);exit('yy');
							foreach($eventlis as $event){
										$data['googleEventId'] = addslashes($event['id']);
										//$data['description'] = addslashes($event['description']);
										$data['Subject'] = addslashes($event['summary']);
										$data['Location'] = addslashes($event['location']);
										$data['googleCalendarId'] = addslashes($res1->calendar_id);
										//print_r($event->getStart());exit;
										$data['StartDateUTC'] = addslashes($event->getStart()->dateTime);
										$data['EndDateUTC'] = addslashes($event->getEnd()->dateTime);
										$event_update = "update Event set ";
										$event_update1 = '';
										foreach($data as $da=>$d){
											$event_update1 .= " $da = '$d' ,";
											}
										$event_update .=  rtrim($event_update1, ",");
										$event_update .= " where googleEventId = '$event[id]' ";
										$data['CompanyID'] = $ress->CompanyID;
										$data['RepeatType'] = 0;
										$data['ContactID'] = 743180;
										$data['googleEventId'] = $event['id'];
										//echo $qry3 = 'select ContactID,CompanyID from Contact where CompanyID = '.$data['CompanyID'];
										//$row3 = mssql_query($qry3, $conn1);
										//while($res3 = mssql_fetch_object($row3)){
											//$res4[] = $res3;
											//}
										//echo "<pre>";
										//print_r($res4);exit;
										$procedure = " BEGIN
										if exists (select * from Event where googleEventId = '$event[id]')
										begin
										   ".$event_update."
										end
										else
										begin
										   INSERT INTO Event (" . implode(", ", array_keys($data)) . ")
										   VALUES ('" . implode("', '", $data) . "');
										   end		  
										END";
/*DECLARE @loop INT
SET @loop = 0
WHILE @loop <= 10
BEGIN   
SET @loop = @loop + 1   
PRINT @loop
END*/
										//echo $procedure;exit;
										$re = mssql_query($procedure, $conn1);
										//var_dump($re);
										mssql_query($event_update, $conn1);
								}
								echo "<pre>";
								print_r($events);exit;
							
						}
				}
//::::::::::::::::::::::::::::::::Event End::::::::::::::Task Start:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			$predate = strtotime('-1 day');
			$updatedMin = date(DATE_RFC3339,$predate);

			$optParams = array('updatedMin' => $updatedMin);
			$tasks = $task->tasks->listTasks($res1->task_list_id,$optParams);
			$task4update = $tasks->getItems();
			//echo "<pre>";
			//print_r($task4update);exit;

			if($task4update){
			//$qrytask = "update GoogleToken set google_updated_datetime = '$updatedMin' where PersonID = ".$res1->PersonID;
			//mssql_query($qrytask, $conn1);
			
			foreach($task4update as $taskk){
							$qrytask2 = 'select CompanyID from people where PersonID = '.$res1->PersonID;
							$rowtask2 = mssql_query($qrytask2, $conn1);
							$resstask = mssql_fetch_object($rowtask2);
							
							$pattern = "/Location : (.*?)\nEvent Type :/s";
							preg_match($pattern,$taskk->notes,$match);
							$pattern1 = "/([^\(.*?\)]+)/s";
							preg_match($pattern1,$taskk->title,$match1);
							//$data['googleEventId'] = addslashes($taskk->id);
							$data['Location'] = addslashes($match['1']);
							//$data['ModifyDate'] = addslashes($taskk['updated']);
							$data['Subject'] = addslashes($match1['1']);
							//$data['ModifyTime'] = json_encode($taskk['title']);
							//$data['StartDateUTC'] = json_encode($taskk['title']);
							//$data['EndDateUTC'] = json_encode($taskk['title']);
							$task_update = "update Event set ";
							$task_update1 = '';
							foreach($data as $da=>$d){
								$task_update1 .= " $da = '$d' ,";
								}
							$task_update .=  rtrim($task_update1, ",");
							$task_update .= " where googleId = '$taskk[id]' ";
							$data['googleId'] = addslashes($taskk['id']);
							$data['CompanyID'] = addslashes($resstask->CompanyID);
							$data['RepeatType'] = 0;
							
							
							
							$procedure = " BEGIN
										if exists (select * from Event where googleId = '$taskk[id]')
										begin
										   ".$task_update."
										end
										else
										begin
										   INSERT INTO Event (" . implode(", ", array_keys($data)) . ")
										   VALUES ('" . implode("', '", $data) . "');
										   end		  
										END";
							$taskre = mssql_query($procedure, $conn1);
										var_dump($taskre);
							
							
							
							
							mssql_query($task_update, $conn1);
							print_r($taskk);exit;
					}
				}
//::::::::::::::::::::::::::::::::::::::::::::::Task end:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::







			}
		
		
//function JetEvent(){

			//$syncToken = $res->google_sync_token;
			//$optParams = array('syncToken' => $syncToken);
			//$events = $cal->events->listEvents($res->calendar_id,$optParams);
			//$eventlis = $events->getItems();
			
			//if($eventlis){
				//$synctoken = $events->getNextSyncToken();
				//$qry = "update GoogleToken set google_sync_token = '$synctoken' where PersonID = 10104";
				//mssql_query($qry, $conn1);

				//foreach($eventlis as $event){
							//$data['googleEventId'] = addslashes($event['id']);
							////$data['description'] = addslashes($event['description']);
							//$data['Subject'] = addslashes($event['summary']);
							//$data['Location'] = json_encode($event['location']);
							////print_r($event->getStart());exit;
							//$data['StartDateUTC'] = addslashes($event->getStart()->dateTime);
							//$data['EndDateUTC'] = addslashes($event->getEnd()->dateTime);
							
							//$event_update = "update Event set ";
							//$event_update1 = '';
							//foreach($data as $da=>$d){
								//$event_update1 .= " $da = '$d' ,";
								//}
							//$event_update .=  rtrim($event_update1, ",");
							//$event_update .= " where googleEventId = '$event[id]' ";

							//mssql_query($event_update, $conn1);
					//}
					//print_r($events);exit;
				
			//}

	//}
	
	}else {
        $authUrl = $client->createAuthUrl();
        print "<a class='login' href='$authUrl'>Connect me!</a>";
    }
    
?>
