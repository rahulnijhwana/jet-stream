<?php
	ob_start();
	session_start();
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
	require_once realpath(dirname(__FILE__) . '/autoload.php');
	require_once 'constants.php';
	require_once 'mpconstants.php';
	$client = new Google_Client();
	$client->setApplicationName(GOOGLE_CLIENT_NAME);
	$client->setClientId(GOOGLE_CLIENT_ID);
	$client->setClientSecret(GOOGLE_CLIENT_SECRET);
	$client->setRedirectUri(GOOGLE_CALLBACK_URL);
    $client->setScopes(array(
        'https://www.googleapis.com/auth/plus.me', 
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/calendar',
        'https://www.googleapis.com/auth/calendar.readonly'
    ));
    $client->setAccessType('offline');
    $client->setApprovalPrompt('force');
    $cal = new Google_Service_Calendar($client);
    $task = new Google_Service_Tasks($client);
    		// M-Power Database informations

    $conn1 = mssql_pconnect(MP_DATABASE_SERVER,MP_DATABASEU, MP_DATABASEP);
	mssql_select_db(MP_DATABASE,$conn1);
	
	$row = mssql_query('select GoogleToken.*,( Select companyID from people where PersonID = GoogleToken.PersonID ) as companyID from GoogleToken', $conn1);
	//$res = mssql_fetch_object($row);
	//var_dump($res);exit;
	while($res = mssql_fetch_object($row)){
		$result[] = $res;
		}

		foreach($result as $res1){

			$rr = 'user'.$res1->PersonID;
			$isUseGoogleSync = "SELECT UseGoogleSync FROM Company WHERE CompanyID = ".$res1->companyID;
			$isGoogleSync = mssql_query($isUseGoogleSync);
			$UseGoogleSync = mssql_fetch_object($isGoogleSync);
		if($UseGoogleSync->UseGoogleSync != null && $UseGoogleSync->UseGoogleSync != 0){

			$client->refreshToken($res1->refresh_token);
				$$rr = $client->getAccessToken();
				$client->setAccessToken($$rr);
				
				if($client->isAccessTokenExpired()){
					$client->refreshToken($res1->refresh_token);
					$client->getAccessToken();
				}

	if(isset($res1->task_list_id)){

			
			if(isset($res1->google_task_datetime)){
			$updatedMin = $res1->google_task_datetime;
			}
			$predate = time();
			$updatedMin1 = date(DATE_RFC3339,$predate);
			

			$optParams = array('updatedMin' => $updatedMin,'showDeleted' => true);
			$tasks = $task->tasks->listTasks($res1->task_list_id,$optParams);
			$task4update = $tasks->getItems();


			if($task4update){

			
			foreach($task4update as $taskk){
				
				if(isset($taskk->title)){
					if(!isset($taskk->deleted) && $taskk->deleted == NULL){
							$pattern1 = "/([^\(.*?\)]+)/s";
							preg_match($pattern1,$taskk->title,$match1);
							$data['Subject'] = addslashes(trim(@$match1['1']));
							
							$data['ModifyTime'] = $taskk->updated;
							$data['StartDateUTC'] = $taskk->due;
							$EndDateUTC = new DateTime($taskk->due);
							$EndDateUTC->modify("+23 hours");
							$data['EndDateUTC'] =$EndDateUTC->format('Y-m-d\TH:i:s\Z');
							$data['Private'] = 0;
							$data['PriorityID'] = 1;
							$data['RepeatInterval'] = 'a:0:{}';
							$data['googleStatus'] = 1;

							$task_update = "update Event set ";
							$task_update1 = '';
							foreach($data as $da=>$d){
								$task_update1 .= " $da = '$d' ,";
								}
							$task_update .=  rtrim($task_update1, ",");
							$task_update .= " where googleId = '$taskk[id]' ";
							
							$sql = "Select EventTypeID from EventType where EventName = 'Google Task' and CompanyID = ".$res1->companyID;
							$res = mssql_query($sql,$conn1);
							$data['EventTypeID'] =mssql_fetch_object($res)->EventTypeID;
							
							$data['googleId'] = addslashes($taskk['id']);
							$data['CompanyID'] = addslashes($res1->companyID);

							$data['RepeatType'] = 0;
							
							$peoplemapjoin = "Select m.FieldName,m.CompanyID from ContactMap m,people p where ( m.LabelName = 'Email - Primary' OR m.IsEmailAddress = 1 ) and p.CompanyID = m.CompanyID and p.PersonId=".$res1->PersonID;
									$result = mssql_query($peoplemapjoin, $conn1);
									$resc = '';
									while($CompanyIDres = mssql_fetch_object($result)){
										$resc[] = $CompanyIDres;
										}
									$task_insert ='';
									if(!empty($resc)){
										$sql ="Select ContactID from Contact where ";
										foreach($resc as $re=>$r){
											$sql .= "(($r->FieldName in ('$res1->email')) OR ";
											}
											$sql = rtrim($sql,'OR ');
											$sql .= ') AND CompanyID = '.$resc[0]->CompanyID;
											$ContactID = mssql_query($sql, $conn1);
										while($ContactIDres = mssql_fetch_object($ContactID)){
												$data['ContactID'] = $ContactIDres->ContactID;
												$task_insert .= " begin
													   INSERT INTO Event (" . implode(", ", array_keys($data)) . ")
													   VALUES ('" . implode("', '", $data) . "')
													   select @NewID=IDENT_CURRENT('Event')
													   INSERT INTO EventAttendee (EventID,PersonID,Creator,Deleted) values (@NewID,'{$res1->PersonID}','1','0')
													   end";
												   }
												$procedure = " BEGIN
												DECLARE @NewID int
													if exists (select * from Event where googleId = '$taskk[id]')
													begin
													   ".$task_update."
													end
													else
													".$task_insert."	  
													END";
										}else{
											$procedure = " BEGIN
													if exists (select * from Event where googleId = '$taskk[id]')
													begin
													   ".$task_update."
													end		  
													END";
											}
											mssql_query($procedure, $conn1);


						}else{
							$sql = "UPDATE Event set Cancelled = 1 where googleId = '{$taskk['id']}'";
							mssql_query($sql, $conn1);
							}
					}
					}
					$qrytask = "update GoogleToken set google_task_datetime = '$updatedMin1' where PersonID = ".$res1->PersonID;
					mssql_query($qrytask, $conn1);
				}
			}

		}			
}
?>
