<?php

	ob_start();
	session_start();
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
	
	require_once realpath(dirname(__FILE__) . '/autoload.php');
	
	require_once 'constants.php';
	require_once 'mpconstants.php';
	
	$client = new Google_Client();
	
	$client->setApplicationName(GOOGLE_CLIENT_NAME);
	$client->setClientId(GOOGLE_CLIENT_ID);
	$client->setClientSecret(GOOGLE_CLIENT_SECRET);
	$client->setRedirectUri(GOOGLE_CALLBACK_URL);
    $client->setScopes(array(
        'https://www.googleapis.com/auth/plus.me', 
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/calendar',
        'https://www.googleapis.com/auth/calendar.readonly'
    ));
    $client->setAccessType('offline');
    $client->setApprovalPrompt('force');
    $cal = new Google_Service_Calendar($client);
    $task = new Google_Service_Tasks($client);
    
    $conn1 = mssql_pconnect(MP_DATABASE_SERVER,MP_DATABASEU, MP_DATABASEP);
	mssql_select_db(MP_DATABASE,$conn1);
	
	//$row = mssql_query('select * from GoogleToken', $conn1);
	$row = mssql_query('select GoogleToken.*,( Select companyID from people where PersonID = GoogleToken.PersonID ) as companyID from GoogleToken', $conn1);
	//$res = mssql_fetch_object($row);
	//var_dump($res);exit;
	while($res = mssql_fetch_object($row)){
		$result[] = $res;
		}
		
		foreach($result as $res1){
			
	$isUseGoogleSync = "SELECT UseGoogleSync FROM Company WHERE CompanyID = ".$res1->companyID;
			$isGoogleSync = mssql_query($isUseGoogleSync);
			$UseGoogleSync = mssql_fetch_object($isGoogleSync);
		if($UseGoogleSync->UseGoogleSync != null && $UseGoogleSync->UseGoogleSync != 0){
			$rr = 'user'.$res1->PersonID;

			$client->refreshToken($res1->refresh_token);
				$$rr = $client->getAccessToken();
				$client->setAccessToken($$rr);
				
				if($client->isAccessTokenExpired()){
					$client->refreshToken($res1->refresh_token);
					$client->getAccessToken();
				}
			
			if(isset($res1->calendar_id)){
			$syncToken = $res1->google_sync_token;
			
			$optParams = array('syncToken' => $syncToken);

			$events = $cal->events->listEvents($res1->calendar_id,$optParams);
			
			$eventlis = $events->getItems();

						if($eventlis){
							


							foreach($eventlis as $event){
							if($event['status'] != 'cancelled'){
									$data['googleEventId'] = addslashes($event['id']);
									$data['Subject'] = addslashes($event['summary']);
									$data['Location'] = addslashes($event['location']);
									$data['googleCalendarId'] = addslashes($res1->calendar_id);
									
									$StartDateUTC = new DateTime($event->getStart()->dateTime);
									$StartDateUTC->setTimezone(new DateTimeZone('UTC'));
									$data['StartDateUTC'] = $StartDateUTC->format('Y-m-d H:i:s');
									
									$EndDateUTC = new DateTime($event->getEnd()->dateTime);
									$EndDateUTC->setTimezone(new DateTimeZone('UTC'));
									$data['EndDateUTC'] = $EndDateUTC->format('Y-m-d H:i:s');
									var_dump($data);
									//$data['StartDateUTC'] = addslashes(date('Y-m-d h:i:s', strtotime($event->getStart()->dateTime)));//Y/m/d H:i:s
									//$data['EndDateUTC'] = addslashes(date('Y-m-d h:i:s', strtotime($event->getEnd()->dateTime)));//Y/m/d H:i:s
									$data['googleStatus'] = 1;
									$event_update = " update Event set ";
									$event_update1 = '';
									foreach($data as $da=>$d){
										$event_update1 .= " $da = '$d' ,";
										}
									$event_update .=  rtrim($event_update1, ",");
									$event_update .= " where googleEventId = '$event[id]' ";
									$data['CompanyID'] = $res1->companyID;
									$sql = "Select EventTypeID from EventType where EventName = 'Google Event' and CompanyID = ".$res1->companyID;
									$res = mssql_query($sql,$conn1);
									
									$data['EventTypeID'] = '';
									if ($eventTypeResObj = mssql_fetch_object($res)) {
										$data['EventTypeID'] = $eventTypeResObj->EventTypeID;
									}
									
									$data['RepeatType'] = 0;
									$data['googleEventId'] = $event['id'];
									$atten = $event->getAttendees();
									$attendee = "'";
									foreach($atten as $att){
										$attendee .= $att->email."','";
										}
									
										$attendee .= $event->getCreator()->email."'";
										
										
										//print_r($attendeelist);exit;
									//$peoplemapjoin = "Select m.FieldName,m.CompanyID from ContactMap m,people p where m.IsEmailAddress = 1 and p.CompanyID = m.CompanyID and p.PersonId=".$res1->PersonID;
									$peoplemapjoin = "Select m.FieldName,m.CompanyID from ContactMap m,people p where ( m.LabelName = 'Email - Primary' OR m.IsEmailAddress = 1 ) and p.CompanyID = m.CompanyID and p.PersonId=".$res1->PersonID;
									$result = mssql_query($peoplemapjoin, $conn1);
									$resc = '';
									while($CompanyIDres = mssql_fetch_object($result)){
										$resc[] = $CompanyIDres;
										}
										//print_r($resc);exit;
										$event_insert = '';
										if(!empty($resc)){
											
											$sql ="Select ContactID from Contact where (";
										foreach($resc as $re=>$r){
											$sql .= '('.$r->FieldName.' in ('.$attendee.')) OR ';
											}
											$sql = rtrim($sql,'OR ');
											$sql .= ') AND CompanyID = '.$resc[0]->CompanyID;
											$ContactID = mssql_query($sql, $conn1);
											//print_r(mssql_fetch_object($ContactID));exit;
											while($ContactIDres = mssql_fetch_object($ContactID)){
												$data['ContactID'] = $ContactIDres->ContactID;
												$event_insert .= " begin
											   INSERT INTO Event (" . implode(", ", array_keys($data)) . ")
											   VALUES ('" . implode("', '", $data) . "')
											   select @NewID=IDENT_CURRENT('Event')
											    INSERT INTO EventAttendee (EventID,PersonID,Creator,Deleted) values (@NewID,'{$res1->PersonID}','1','0')
											   end ";	
											}
											$procedure = " BEGIN
											DECLARE @NewID int
											if exists (select * from Event where googleEventId = '$event[id]')
											begin
											   ".$event_update."
											end
											else
											
											".$event_insert."		  
											END";
										}else{
											$procedure = " BEGIN
									if exists (select * from Event where googleEventId = '$event[id]')
									begin
									   ".$event_update."
									end		  
									END";
											
											}
									echo $procedure;

									$re = mssql_query($procedure, $conn1);

								}else{
									$sql = "UPDATE Event set Cancelled = 1 where googleEventId = '{$event['id']}'";
									mssql_query($sql, $conn1);
									
									}
							}

						}

				}
				$synctoken = $events->getNextSyncToken();
							$qry1 = "update GoogleToken set google_sync_token = '$synctoken' where PersonID =".$res1->PersonID;
							mssql_query($qry1, $conn1);



				
		}
	}



?>
