<?php
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
	require_once dirname(__FILE__) . '/autoload.php';
	require_once 'constants.php';
	
	require_once JETSTREAM_ROOT . '/../include/class.DbConnManager.php';
	require_once JETSTREAM_ROOT . '/../include/class.SqlBuilder.php';
	$client = new Google_Client();
	$client->setApplicationName(GOOGLE_CLIENT_NAME);
	$client->setClientId(GOOGLE_CLIENT_ID);
	$client->setClientSecret(GOOGLE_CLIENT_SECRET);
    $client->setRedirectUri(GOOGLE_CALLBACK_URL);
    $client->setScopes(array(
        'https://www.googleapis.com/auth/plus.me', 
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/calendar',
        'https://www.googleapis.com/auth/calendar.readonly',

    ));
    $client->setAccessType('offline');
    $client->setApprovalPrompt('force');
    $cal = new Google_Service_Calendar($client);
    
    if (isset($_GET['logout'])) {
			unset($_SESSION['token']);
		}
	if (isset($_GET['code'])) {
		
			$client->authenticate($_GET['code']);
			$_SESSION['token'] = $client->getAccessToken();
			header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
			
		}
	if (isset($_SESSION['token'])) {
			$client->setAccessToken($_SESSION['token']);
		}
	if ($client->isAccessTokenExpired()){
			if (! $info->getEventInfo('token')[0]->refresh_token) {
					throw new Google_AuthException("The OAuth 2.0 access token has expired, "
						. "and a refresh token is not available. Refresh tokens are not "
						. "returned for responses that were auto-approved.");
				}else{
			$client->refreshToken($info->getEventInfo('token')[0]->refresh_token);
			$_SESSION['token'] = $client->getAccessToken();
			}
		}
		var_dump($client->getAccessToken());exit;
	if ($client->getAccessToken()) {
			//echo $syncToken = $events->getNextSyncToken();
			//$syncToken = 'CIC7o5SdjsMCEIC7o5SdjsMCGAU=';
			//$optParams = array('syncToken' => $syncToken);
			//$events = $cal->events->listEvents('primary', $optParams);
			$events = $cal->events->listEvents('primary');
			var_dump($events);exit;
	}else {
        $authUrl = $client->createAuthUrl();
        print "<a class='login' href='$authUrl'>Connect me!</a>";
    }
    
?>
