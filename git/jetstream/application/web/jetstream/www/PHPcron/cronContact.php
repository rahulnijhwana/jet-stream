<?php

	ob_start();
	session_start();
	//error_reporting(E_ALL);
	//ini_set("display_errors", 1);
	
	require_once realpath(dirname(__FILE__) . '/autoload.php');
	
	require_once 'constants.php';
	require_once 'mpconstants.php';
	
	$client = new Google_Client();
	
	$client->setApplicationName(GOOGLE_CLIENT_NAME);
	$client->setClientId(GOOGLE_CLIENT_ID);
	$client->setClientSecret(GOOGLE_CLIENT_SECRET);
	$client->setRedirectUri(GOOGLE_CALLBACK_URL);
    $client->setScopes(array(
        'https://www.googleapis.com/auth/contacts.readonly',
        'https://www.google.com/m8/feeds'
    ));
    $client->setAccessType('offline');
    $client->setApprovalPrompt('force');
    //$conn1 = mssql_pconnect('192.168.55.131','sa', 'tech');
    
    $conn1 = mssql_pconnect(MP_DATABASE_SERVER,MP_DATABASEU, MP_DATABASEP);
	mssql_select_db(MP_DATABASE,$conn1);
	
	$row = mssql_query('select GoogleToken.*,( Select companyID from people where PersonID = GoogleToken.PersonID ) as companyID from GoogleToken', $conn1);
	while($res = mssql_fetch_object($row)){
		$result[] = $res;
		}
		
		
		$t = '$t';
		$name = 'gd$name';
		$namePrefix = 'gd$namePrefix';
		$fullName = 'gd$fullName';
		$givenName = 'gd$givenName';
		$additionalName = 'gd$additionalName';
		$familyName = 'gd$familyName';
		$email = 'gd$email';
		$phoneNumber = 'gd$phoneNumber';
		$structuredPostalAddress = 'gd$structuredPostalAddress';
		$extendedProperty = 'gd$extendedProperty';
		$formattedAddress = 'gd$formattedAddress';
		$neighborhood = 'gd$neighborhood';
		$street = 'gd$street';
		$city = 'gd$city';
		$region = 'gd$region';
		$postcode = 'gd$postcode';
		$country = 'gd$country';
		$pobox = 'gd$pobox';
		$groupMembershipInfo = 'gContact$groupMembershipInfo';
		$website = 'gContact$website';
		

		$rr ='';
		foreach($result as $res1){
			$rr = 'user'.$res1->PersonID;
			
			$isUseGoogleSync = "SELECT UseGoogleSync FROM Company WHERE CompanyID = ".$res1->companyID;
			$isGoogleSync = mssql_query($isUseGoogleSync);
			$UseGoogleSync = mssql_fetch_object($isGoogleSync);
		if($UseGoogleSync->UseGoogleSync != null && $UseGoogleSync->UseGoogleSync != 0){

				$client->refreshToken($res1->refresh_token);
				$$rr = $client->getAccessToken();
				$client->setAccessToken($$rr);
				
				if($client->isAccessTokenExpired()){
					$client->refreshToken($res1->refresh_token);
					$client->getAccessToken();
				}

			
			
			
		if(isset($res1->google_updated_datetime)){
			$updatedMin = $res1->google_updated_datetime;
			}
		$qry = "Select FieldName,googleField from ContactMap where companyID = ( Select companyID from people where PersonID={$res1->PersonID}) and googleField != null";
		$maplist = mssql_query($qry, $conn1);
		echo "<pre>";
		while($mapli = mssql_fetch_object($maplist)){
			$map[] = $mapli;
			}
			
		if(isset($res1->google_updated_datetime)){

		$res1->google_updated_datetime;
		$updatedMin = new DateTime($res1->google_updated_datetime);
		$updatedMin->setTimezone(new DateTimeZone('UTC'));
		$updatedMinn = $updatedMin->format('Y-m-d\TH:i:s');
		//exit;
		}
		$predate = time();
		$updatedMin1 = date(DATE_RFC3339,$predate);

		$max_results = 25;
		$accesstoken = json_decode($$rr)->access_token;
		
	


		$url = "https://www.google.com/m8/feeds/contacts/{$res1->email}/full?max-results={$max_results}&oauth_token={$accesstoken}&alt=json&showdeleted=true&updated-min=".$updatedMinn;
		$contactlist  = json_decode(curl_file_get_contents($url));
		
		$contact = @$contactlist->feed->entry;
		if(is_array($contact)){
			
		foreach($contact as $cont => $c){
			echo "<br>";
			print_r($c);
			$groupMember = @$c->$groupMembershipInfo;
			if(is_array($groupMember)){
				foreach($groupMember as $group){
				$GoogleGroupId = $group->href;
				$deleted = $group->deleted;
					}
				}
			
		    $url = $c->id->$t;
			$keys = parse_url($url); // parse the url
			$path = explode("/", $keys['path']); // splitting the path
			$GoogleContactId = end($path);
			
			

			var_dump($deleted);
			echo "</pre>";
		if($deleted == 'false'){	
			var_dump('Working for new Contacts');
			$data['GoogleContactId'] = $GoogleContactId;
			$title = @$c->title;
			$title->$t;//title
			$gdname = @$c->$name;
			
			$data['name-namePrefix'] = property_exists($gdname,$namePrefix)?$gdname->$namePrefix->$t:'';//Prefix
			$data['name-fullName'] = property_exists($gdname,$fullName)?$gdname->$fullName->$t:'';//full name
			$data['name-givenName'] = property_exists($gdname,$givenName)?$gdname->$givenName->$t:'';//first name
			$data['name-additionalName'] = property_exists($gdname,$additionalName)?$gdname->$additionalName->$t:'';//Middle name
			$data['name-familyName'] = property_exists($gdname,$familyName)?$gdname->$familyName->$t:'';//last name
			
			$gdemail = @$c->$email;
			if(is_array($gdemail)){
			foreach($gdemail as $gemail){
				$eemail = parse_url($gdemail[0]->rel,PHP_URL_FRAGMENT);
				if($eemail == 'home'){
					$data['email-home'] =  isset($gemail)?$gemail->address:'';
					}
				if($eemail == 'work'){
					$data['email-work'] =  isset($gemail)?$gemail->address:'';
					}
				}
			}
			
			$gdphoneNumber = @$c->$phoneNumber;
			if(is_array($gdphoneNumber)){
			foreach($gdphoneNumber as $phno){
				$pphno = parse_url($phno->rel,PHP_URL_FRAGMENT);
				if($pphno == 'work'){
					$data['phoneNumber-work'] =  isset($phno)?$phno->$t:'';
					}
				if($pphno == 'main'){
					$data['phoneNumber-main'] =  isset($phno)?$phno->$t:'';
					}
				if($pphno == 'work_fax'){
					$data['phoneNumber-work_fax'] =  isset($phno)?$phno->$t:'';
					}
				if($pphno == 'mobile'){
					$data['phoneNumber-mobile'] =  isset($phno)?$phno->$t:'';
					}
				if($pphno == 'home'){
					$data['phoneNumber-home'] =  isset($phno)?$phno->$t:'';
					}
				if($pphno == 'home_fax'){
					$data['phoneNumber-home_fax'] = isset($phno)?$phno->$t:'';
					}
				}
			}
		//	print_r($data);
			$gdstructuredPostalAddress = isset($structuredPostalAddress)?$c->$structuredPostalAddress:'';
			if(is_array($gdstructuredPostalAddress)){
			foreach($gdstructuredPostalAddress as $struc){
					$data['structuredPostalAddress-formattedAddress'] =  property_exists($struc,$formattedAddress)?$struc->$formattedAddress->$t:'';
					$data['structuredPostalAddress-street'] =  property_exists($struc,$street)?$struc->$street->$t:'';
					$data['structuredPostalAddress-city'] =  property_exists($struc,$city)?$struc->$city->$t:'';//City code needed
					$structuredPostalAddress_region =  property_exists($struc,$region)?$struc->$region->$t:'';
					$data['structuredPostalAddress-region'] = getOption($structuredPostalAddress_region,$conn1);
					$data['structuredPostalAddress-postcode'] =  property_exists($struc,$postcode)?$struc->$postcode->$t:'';
					$structuredPostalAddress_country =  property_exists($struc,$country)?$struc->$country->$t:'';//Country code needed
					$data['structuredPostalAddress-country'] = getOption($structuredPostalAddress_country,$conn1);
					$data['structuredPostalAddress-pobox'] =  property_exists($struc,$pobox)?$struc->$pobox->$t:'';
					$data['structuredPostalAddress-neighborhood'] = property_exists($struc,$neighborhood)?$struc->$neighborhood->$t:'';
				}
			}
			
		//	$website = property_exists($c,$website)?$c->$website:'';
			$web= property_exists($c,$website)?$c->$website:'';
			if(is_array($web)){
			foreach($web as $w){
				$data['website-website'] =  $w->href;
				}
			}

			$gdextendedProperty = property_exists($c,$extendedProperty)?$c->$extendedProperty:'';
			if(is_array($gdextendedProperty)){
			$data['ContactID'] = $jetContactID = $gdextendedProperty[0]->value;
					}
					foreach($data as $key=>$value)
					{
						if(is_null($value) || $value == '')
						unset($data[$key]);
					}
				$insertdata = insertarr($map,$data);
				

				$insertdata['companyID'] = $res1->companyID;
				
				//var_dump($qrryres);
				//if($qrryres){
					//echo "data inserted from jetstream";
					//}else{
						$insertdata['googleStatus'] = 1;
					
					$contact_insert = " BEGIN
						DECLARE @NewID int
						IF NOT EXISTS (select * from GoogleContact where GoogleContactId = '{$data['GoogleContactId']}')
						BEGIN
						insert into Contact (" . implode(", ", array_keys($insertdata)) . ") values ('" . implode("', '", $insertdata) . "') 
						select @NewID=IDENT_CURRENT('Contact')
						INSERT INTO GoogleContact (PersonID,ContactID,GoogleContactId) values ('{$res1->PersonID}',@NewID,'{$data['GoogleContactId']}')
						INSERT INTO DashboardSearch (RecID, RecType,CompanyID,SearchText) values (@NewID,'2','{$res1->companyID}','{$data['name-fullName']}')
						END
						END";
					var_dump($contact_insert);echo "<br>";
					
					
						//$contact_insert = "
						//DECLARE @NewID int
						//insert into Contact (" . implode(", ", array_keys($insertdata)) . ") values ('" . implode("', '", $insertdata) . "') 
						//select @NewID=IDENT_CURRENT('Contact')
						//INSERT INTO DashboardSearch (RecID, RecType,CompanyID,SearchText) values (@NewID,'2','{$res1->companyID}','{$data['name-fullName']}')
						//";
					//var_dump($contact_insert);echo "<br>";
						var_dump(mssql_query($contact_insert, $conn1));
				//	}
				

			}else{
				echo $sql = "UPDATE Contact SET Inactive = '1' WHERE ContactID = ( Select top 1 ContactID from GoogleContact where GoogleContactId = '{$GoogleContactId}' )";
				var_dump(mssql_query($sql, $conn1),'Working for deleted Contacts');
				}
			}


		}
		
	}
	echo $qrycontact = "update GoogleToken set google_updated_datetime = '$updatedMin1' where PersonID = ".$res1->PersonID;

	mssql_query($qrycontact, $conn1);
}
 function curl_file_get_contents($url)
		 {
		 $curl = curl_init();
		 curl_setopt($curl,CURLOPT_URL,$url);
		 curl_setopt($curl, CURLOPT_HTTPHEADER,array('GData-Version: 3.0'));
		 curl_setopt($curl,CURLOPT_RETURNTRANSFER,TRUE);
		 curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,5);
		 curl_setopt($curl,CURLOPT_HTTPGET,true);
		curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($curl, CURLOPT_SSLVERSION, 1);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		
		$contents = curl_exec($curl);
		curl_close($curl);
		return $contents;
		}


function insertarr($map,$data){
	$insarr = array();
	foreach($map as $m){
		$insarr[$m->FieldName]= isset($data[$m->googleField])?$data[$m->googleField]:'';
		}
		return $insarr;
	}
function getOption($OptionName,$conn1) {
        $sql = "Select OptionId from [Option] Where OptionName = '{$OptionName}'";
        $r2 = mssql_query($sql, $conn1);
		return mssql_fetch_object($r2)->OptionId;
    }



?>
