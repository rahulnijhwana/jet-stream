<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.Language.php';
require_once BASE_PATH . '/include/lib.security.php';

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<title>M-Power&trade;</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
		<style type="text/css">
			.body {
				font-family:arial,sans-serif;
				margin-left:3em;
				margin-right:5em;
			}
			
			div.errormsg {
				color:red;
				font-family:arial,sans-serif;
				font-size:smaller;
			}
			div.success {
				color: black;
				font-family:arial,sans-serif;
				font-size: smaller;
			}		
			font.errormsg {
				color:red;
				font-family:arial,sans-serif;
				font-size:smaller;
			}
		</style>	
		
	</head>
	<body>	
<?php

$token = base64_decode($_GET['token']);

if (!empty($token) && isvalidToken($token)) {
	$update =  false;
	$error_msg = '';
	$password_strength = 6;
	
	if (isset($_POST['password']) && isset($_POST['confirmPassword']) && !empty($_POST['password'])) {		
		if (strlen(trim($_POST['password'])) >= $password_strength) {
			if ($_POST['password'] == $_POST['confirmPassword']) $update =  true;
			else $error_msg = 'Password does not match the confirm password.';
		} else {
			$error_msg = 'Minimum of 6 characters in length.';
		}
	} 
	
	
	if ($update) {		
		$password = $_POST['password'];
		$sql = 'UPDATE Logins SET Password = ?, Passwordz = ?, ForgetPasswordToken = NULL WHERE ForgetPasswordToken = ? AND ForgetPasswordToken IS NOT NULL';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, crypt($password)), array(DTYPE_STRING, $password),	array(DTYPE_STRING, $token));		
		//echo '<br>'.$sql.'<br>';
														
		if (DbConnManager::GetDb('mpower')->Exec($sql)) {
			echo 'Your password has been successfully updated.';
		} else {
			echo 'Error in new password update. Please try again.';
		}
	
	} else {
	
	?>	

	
	<div class="body"><b>Forgot password</b><br>
		<table border="0" cellpadding="0" cellspacing="0">
			<tbody>
			<tr valign="top"><td>
				
				<form action="confirmforgetpassword.php?token=<?php echo $_GET['token'];?>" method="post">
				<table border="0" cellpadding="0" cellspacing="0" width="" height="1%">
				<tbody>
				<tr>
				<td style="padding-top: 10px; padding-bottom: 10px;" valign="top">
				<font size="-1">Please enter your new password for Mpower/Jetstream. Password must be minimum of 6 characters in length.</font>
				<br>
					<table border="0" cellpadding="2" cellspacing="5"><tbody>
					<tr>
					<td align="center" valign="top">
					<table bgcolor="#cbdced" border="0" cellpadding="2" cellspacing="0" width="100%">
					<tbody><tr><td>
						<table border="0" cellpadding="5" cellspacing="0" width="100%">
						<tbody>
						
							<tr>
							<td align="center" bgcolor="#ffffff" valign="top" nowrap="nowrap">
							<b><font size="-1"><?php echo Language::__get('Password')?>:</font>&nbsp;&nbsp;</b></td>
							<td align="left" bgcolor="#ffffff" colspan="2"><div class="errorbox-good">
							 <input type="password" name="password" value="" size="15">
	
							</div></td>
							</tr>
						
							<tr><td align="center" bgcolor="#ffffff" valign="top" nowrap="nowrap">
							<b><font size="-1" face="Arial, sans-serif"><?php echo Language::__get('ConfirmPassword')?>:</font>&nbsp;&nbsp;</b></td>
							<td align="left" bgcolor="#ffffff"><div class="errorbox-good">
							<input type="password" name="confirmPassword" value="" size="15">
							</div></td>
							<td align="left" bgcolor="#ffffff"><input value="Submit" type="submit"></td>
							</tr>
						</tbody>
						</table>
						<div class="errormsg"><?php echo $error_msg;?></div>
					</td></tr>
					</tbody>
					</table>
				</td></tr>
				</tbody>
				</table></td></tr>
				</tbody>
				</table>
				</form>
				
			</td>		
			</tr>
			</tbody>
		</table>
	</div>
	<?php
	}
	
} else {
	echo 'We have not been able to process your access request. Please contact <a href="mailto:support@mpasa.com">Support</a>.';
}
?>

	</body>
</html>


<?php


function isvalidToken($token) {
	$sql = 'SELECT count(ID) AS CNT FROM Logins WHERE ForgetPasswordToken = ? AND ForgetPasswordToken IS NOT NULL';
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $token));
	//echo '<br>'.$sql.'<br>';
	$login = DbConnManager::GetDb('mpower')->GetOne($sql);
	return ($login instanceof MpRecord && $login->CNT > 0) ? true : false;
}

?>

