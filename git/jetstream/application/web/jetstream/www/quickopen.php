<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/class.CompanyUtil.php';
require_once BASE_PATH . '/include/class.UserUtil.php';

SessionManager::Init();

if(!$_SESSION['quickpass']){
        header("Location: index.php");
}

$companyId = strip_tags($_GET['company_id']);

$people = UserUtil::getUsers($companyId);
$company = CompanyUtil::getCompany($companyId);
?>
<!DOCTYPE table PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Jetstream/M-Power :: Quick Open</title>
		<style>
			body {
				font-family: Arial;
			}
			table td {
				padding: 5px;
			}
			table td.centered {
				text-align: center;
			}
			div#links {
				font-size: 1.2em;
				padding: 20px;
			}
		</style>
	</head>
	<body>
		<h1><?php echo $company['Name']; ?></h1>
		<table>
			<tr>
				<th>Level</th>
				<th>Name</th>
				<th>Username</th>
				<th>Last Login</th>
			</tr>
			<?php foreach($people as $person) : ?>
				<tr bgcolor="<?php echo $person['Color']; ?>">
					<td class="centered"><?php echo $person['Level']; ?></td>
					<td><?php echo '<a href="index.php?username='.$person['UserID'].'&SN='.$_GET['SN'].'">'.$person['FirstName'].' '.$person['LastName'].'</a>'; ?></td>
					<td><?php echo $person['UserID']; ?></td>
					<td>
						<?php
						if($person['LastUsed']){
							$stamp = strtotime($person['LastUsed']);
							$stamp -= UTC_OFFSET;
							echo date('m/d/Y H:m:s', $stamp);
						}
						?>
					</td>
				</tr>
			<?php endforeach; ?>
		</table>
		<div id="links">
			<a href="administrator/jetstream/?SN=<?php echo $_GET['SN']; ?>">New Jetstream Administration Area</a>
			
		</div>
	</body>
</html>
