<?php
 /**
 * Routines for exporting files
 * Vern Gorman - Blue Star Technology
 * June 13, 2013
 */

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/slipstream/class.export.php';

function export_csv($filename) {
	// Call this to create a CSV file
	
	header("Content-type: text/csv");
	header("Content-Disposition: attachment; filename=".$filename.".csv");
	header("Pragma: no-cache");
	header("Expires: 0");

	$array = $_SESSION['ExportArray'];
	outputCSV($array);
}
	
function outputCSV($data) {
	$outstream = fopen("php://output", "w");
	function __outputCSV(&$vals, $key, $filehandler) {
		fputcsv($filehandler, $vals);
	}
	array_walk($data, "__outputCSV", $outstream);
	fclose($outstream);
}

function export_xls($filename) {
	// Call this to create an Excel file
		
	// Throw the file name into an array since that is what the class is looking for
	$f_name = array();
	$f_name[0] = $filename;
	
	// Grab the session array carrying the report headers and data.
	$inarray = array();
	$inarray = $_SESSION['ExportArray'];	
	
	// Load data from the incoming array
	$header = array();
	$dataArray = array();
	$i = 0;
	foreach ($inarray as $row){
		if ($i == 0) {
			// Load headers from first line of incoming array
			$header = $row;
		}
		else {
			// Everything after the first row is data
			array_push($dataArray, $row);
		}
		$i++;
	}

	// create an instance of the export class
	$export = new export();
	
	// Load up the required parameters
//	print_r($header);
//	echo "<br />";
	$export->headers = $header;
	
//	print_r($_SESSION['ExportArray']);
//	echo "<br />";
	$export->datas = $dataArray;
//	$export->datas = $_SESSION['ExportArray'];

//	print_r($filename);
//	echo "<br />";
	$export->filename = $f_name;
		
	// Execute the method to create the export file.
	$export->xlsx();
}

?>