<?php
 /**
 * @package Reports
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_report_utils.php');
include('TableUtils.php');
include('path.php');
include('ci_pdf_utils.php');

define(SHADE_MED, "MED_BLUE");
define(SHADE_LIGHT, "LIGHT_BLUE");
include('submilestones.php');
include('category_labels.php');


function GetCategoryChanges($dealid, $fulldetail)
{
	$categories=array();

	if($fulldetail)
		$sql = "select * from log where DealID=$dealid and (TransactionID = 1 OR TransactionID = 400 OR (TransactionID >= 20 AND TransactionID <= 25)) order by WhenChanged";
	else
		$sql = "select * from log where DealID=$dealid and (TransactionID = 1 OR (TransactionID >= 20 AND TransactionID <= 25)) order by WhenChanged";

	$result = mssql_query($sql);
	$count=0;
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$temp=array();
//Must use First Meeting Date, not WhenChanged, for init of series!
			if ($row['TransactionID']==1&&$row['LogInt']==1)  //First Meeting
			{
				if(!($date=GetFMDate($dealid)))
					$date=strtotime($row['WhenChanged']);
			}
			else
				$date = strtotime($row['WhenChanged']);
			$temp['date']=$date;
			$temp['logint'] = $row['LogInt'];
			$temp['logbit'] = $row['LogBit'];
			$temp['transactionid'] = $row['TransactionID'];
			$temp['dealid'] = $dealid;


			$count++;
			array_push($categories, $temp);
		}
	}

	if($fulldetail)
	{
		for($i=0; $i<$count; $i++)
		{
			$temp = $categories[$i];
			$transid = $temp['transactionid'];
			$date = $temp['date'];
			if($transid > 19 && $transid < 26)
			{
				for($j=$i+1; $j<$count; $j++)
				{
					$temp2 = $categories[$j];
					$transid2 = $temp2['transactionid'];

					$date2 = $temp2['date'];
					if($transid2 != 400) break;
					else if(GetElapsedSeconds($date, $date2) > 10) break;
					else
					{
						$categories[$j] = $temp;
						$categories[$j-1] = $temp2;
					}
				}
				$i = $j - 1;
			}
		}
	}

	return $categories;
}

function GetFMDate($dealid)
{
	$strSQL="select FirstMeeting from Opportunities where DealID=$dealid";
	$fmdate=NULL;
	$res=mssql_query($strSQL);
	if($res)
		{
			if(($row=mssql_fetch_assoc($res)))
				$fmdate=$row['FirstMeeting'];
		}
	return strtotime($fmdate);
}


function CountDays(&$daycells, $category, $days)
{
	if(0 === strpos($category, "IP", 0)) $category = "IP";
	$val = $daycells[$category];
	$val += $days;

	$daycells[$category] = $val;
	if($category == "SIP") CountDays($daycells, "TSIP", 1);
	if($category == "SDP") CountDays($daycells, "TSDP", 1);
}

function MakeMilestonesString($arrMilestones)
{
	$strMS="";
	$count=count($arrMilestones);
	for($i=0;$i<$count;$i++)
	{
		if($arrMilestones[$i]>0)
			$strMS.=($i+1);
	}
	return $strMS;
}

function GetMilestoneCount($companyid)
{
	$count=4;
	$sql="select Requirement1used, Requirement2used from Company where CompanyID=$companyid";
	$res=mssql_query($sql);
	if($res)
	{
		if($row = mssql_fetch_assoc($res))
		{
			if($row['Requirement1used'] == 1)
				$count++;
			if($row['Requirement2used'] == 1)
				$count++;
		}
	}
	return $count;
}

function MakeCategoryCells($op, &$daycells, $fulldetail, $submilestones, $companyid)
{
	$categories = $op['categories'];
	$firstmeeting = $op['firstmeeting'];
	$prevmeeting = $firstmeeting;

	$count = count($categories);
	$cells = array();
	$celldata = array();
	$infocount=0;
	$top = "";
	$lastcatid = -1;
	$first = 1;
	$lasttrans = -1;
	$lastparent = -1;

	$ok;
	$milestones = array();
	$mc=GetMilestoneCount($companyid);
//	for($i=0; $i<6; $i++) $milestones[$i]=0;
	for($i=0; $i<$mc; $i++) $milestones[$i]=0;

	for($i=0; $i<$count; $i++)
	{
		$cat = $categories[$i];
		$date = $cat['date'];
		$logint = $cat['logint'];
		$catid = $logint;
		$transactionid = $cat['transactionid'];
		$logbit = $cat['logbit'];
		$dealid = $cat['dealid'];

		if($transactionid == 400)
		{
			$subid = $logint;
			$oldval = GetMilestoneValue($submilestones, $subid);
			if($oldval == $logbit) continue;

			SetMilestones($submilestones, $subid, $logbit);
		}

		if($transactionid > 1 && $transactionid < 400 && $logbit == 0 && $infocount == 0)
			continue;

		$days=GetElapsedDays($prevmeeting, $date);
		$seconds=GetElapsedSeconds($prevmeeting, $date);

		if($i>0)
		{
			if($transactionid == 400)
			{
				$sub = FindSubMilestones($submilestones, $subid);
				$parent = $sub['parent'];
				if($parent == $lastparent && $seconds < 60) $ok = false;
				$lastparent = $parent;
			}
			else
			{
				if($transactionid > 1 && $lastcatid != 2) $ok = false;
				if($transactionid > 1 && $lasttrans > 1 && $seconds < 120) $ok = false;
			}

			if($ok && $top)
			{
				$celldata['top'] = $top;
				$celldata['bottom'] = $days;
				array_push($cells, $celldata);

				CountDays($daycells, $top, $days);
			}
		}

		$lasttrans = $transactionid;

		if($first>0 && $catid != 1)
		{
			$celldata['top'] = "NA";
			$days=GetElapsedDays($prevmeeting, $date);
			$prevmeeting = $date;
			$celldata['bottom'] = $days;
			array_push($cells, $celldata);
		}
		$first = 0;

		$ok = true;

		if($transactionid == 400)
		{
			$subid = $logint;
			$sub = FindSubMilestones($submilestones, $subid);
			$parent = $sub['parent'];
			$top = GetMilestoneString($submilestones, $parent);
		}
		else if($transactionid > 19)
		{
			$ind = $transactionid - 20;
//	There may be milestones in the data that have been unlinked!
			if($ind>(count($milestones)-1)) continue;

			$on = $milestones[$ind];

			$catid = -1;
			if($on == 0 && $logbit != 0)
				{
				$milestones[$ind]=1;
				$infocount++;
				}
			else if($on > 0 && $logbit == 0)
				{
				$milestones[$ind]=0;
				$infocount--;
				}
			if($lastcatid != 2)
				continue;
			//$top = "IP$infocount";
			$top = GetCatLbl("IP") . MakeMilestonesString($milestones);
		}
		else
		{
			$lastcatid = $catid;

			if($catid == 1) $top = GetCatLbl("FM");
			//else if($catid == 2) $top = GetCatLbl("IP") . $infocount";
			else if($catid == 2) $top = GetCatLbl("IP") . MakeMilestonesString($milestones);
			else if($catid == 3) $top = GetCatLbl("S") . GetCatLbl("IP") . MakeMilestonesString($milestones);
			else if($catid == 4) $top = GetCatLbl("DP");
			else if($catid == 5) $top = GetCatLbl("S") . GetCatLbl("DP");
			else if($catid == 6) $top = GetCatLbl("CL");
			else if($catid == 9) $top = GetCatLbl("RM");
			else if($catid == 10) $top = GetCatLbl("T");
			else $top = "UNKNOWN";

/*
			$lastcatid = $catid;
			if($catid == 1) $top = "FM";
			//else if($catid == 2) $top = "IP$infocount";
			else if($catid == 2) $top = "IP" . MakeMilestonesString($milestones);
			else if($catid == 3) $top = "SIP";
			else if($catid == 4) $top = "DP";
			else if($catid == 5) $top = "SDP";
			else if($catid == 6) $top = "CL";
			else if($catid == 9) $top = "RM";
			else $top = "UNKNOWN";
*/

		}

		$prevmeeting = $date;
	}

	$date=time();
	$days=GetElapsedDays($prevmeeting, $date);
	if($i>0)
	{
		$celldata['top'] = $top;
		$celldata['bottom'] = $days;
		array_push($cells, $celldata);

		CountDays($daycells, $top, $days);

	}


	return $cells;
}

function MakeDayCells($op)
{
	$categories = $op['categories'];
	$lastdate = $op['firstmeeting'];

	$count = count($categories);
	$cells = array();

	$FM=0;
	$IP=0;
	$SIP=0;
	$TSIP=0;
	$DP=0;
	$SDP=0;
	$TSDP=0;
	$RM=0;

	$catid = 1;
	$date = 0;

	for($i=0; $i<=$count; $i++)
	{
		if($i<$count)
		{
			$cat = $categories[$i];
			$date = $cat['date'];
		}
		else $date = time();

		$days=GetElapsedDays($lastdate, $date);
		$lastdate = $date;

		if($catid == 1) $FM += $days;
		else if($catid == 2) $IP += $days;
		else if($catid == 3) $DP += $days;
		else if($catid == 4)
		{
			$SIP += $days;
			$TSIP++;
		}
		else if($catid == 5)
		{
			$SDP += $days;
			$TSDP++;
		}
		else if($catid == 6) $RM += $days;

		$catid = $cat['logint'];
	}

	$cells['FM']=$FM;
	$cells['IP']=$IP;
	$cells['SIP']=$SIP;
	$cells['TSIP']=$TSIP;
	$cells['DP']=$DP;
	$cells['SDP']=$SDP;
	$cells['TSDP']=$TSDP;
	$cells['RM']=$RM;

	return $cells;
}

function GetMeetingDates($dealid, &$next, &$last)
{
	$next = "";
	$last = "";

	$sql = "select * from log where DealID=$dealid and TransactionID=6 order by WhenChanged desc";

	$result = mssql_query($sql);

	if($result)
	{
		if($row = mssql_fetch_assoc($result))
		{
			$n = $row['LogDate'];
			$next = MakeDate(strtotime($n));
		}

		if($row = mssql_fetch_assoc($result))
		{
			$l = $row['LogDate'];
			$last = MakeDate(strtotime($l));
		}
	}
}

function GetOpportunities($salesid, &$maxcells, $fulldetail, $submilestones, $closed, $CompanyID)
{
	$oparray = array();
	if($closed)
		$sql = "SELECT O.*, A." . $_SESSION['account_name'] . " as Company , (C." . $_SESSION['contact_first_name'] . " + ' ' + C." . $_SESSION['contact_last_name'] . ") AS Contact
			FROM opportunities O
			LEFT JOIN Account A on O.AccountID = A.accountid
			LEFT JOIN Contact C on O.ContactID = C.ContactID
			WHERE O.PersonID=$salesid AND O.category=6 ORDER BY A." . $_SESSION['account_name'];
	else
		$sql = "select O.*, A." . $_SESSION['account_name'] . " as Company, (C." . $_SESSION['contact_first_name'] . " + ' ' + C." . $_SESSION['contact_last_name'] . ") AS Contact 
			FROM opportunities O
			LEFT JOIN Account A on O.AccountID = A.accountid
			LEFT JOIN Contact C on O.ContactID = C.ContactID
			WHERE O.PersonID = $salesid and O.category < 6 ORDER BY A." . $_SESSION['account_name'];

	//echo $sql.'<br>';

	$result = mssql_query($sql);
	$maxcells = 0;
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
//			if(is_null($row['ProductID']) || $row['ProductID'] < 0)
//				continue;

			$temp = array();
			$dealid = $row['DealID'];
			$temp['dealid'] = $dealid;
			$temp['company'] = $row['Company'];
			$temp['estvalue'] = $row['EstimatedDollarAmount'];
			$temp['firstmeeting'] = strtotime($row['FirstMeeting']);
			$temp['productid'] = $row['ProductID'];
			$categories = GetCategoryChanges($dealid, $fulldetail,$CompanyID);
			$temp['categories'] = $categories;

			$daycells = false;
			$cells = MakeCategoryCells($temp, $daycells, $fulldetail, $submilestones, $CompanyID);
//			$daycells = MakeDayCells($temp);
			$cellcount = count($cells);
			$temp['cellcount'] = $cellcount;
			if($cellcount > $maxcells)
				$maxcells = $cellcount;
			$temp['cells'] = $cells;
			$temp['daycells'] = $daycells;

			array_push($oparray, $temp);
		}
	}

	return $oparray;
}

function MakeDate($date)
{
	if($date < 1)
		return "";
	if($date == NULL)
		return "";
	return date("m/d/y", $date);
}

function PrintTitleRow(&$report, $title, $begin, $end)
{
	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
	SetRowColorStandard($report, -1, SHADE_MED);
	AddColumnText($report, $title, "", 0, 0);

	for($i=$begin; $i<$end; $i++)
		AddColumnText($report, "", "", 0, 0);
}

function PrintSpacerRow(&$report, $title, $begin, $end)
{
	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
	//SetRowColorStandard($report, -1, SHADE_LIGHT);
	AddColumnText($report, $title, "", 0, 0);

	for($i=$begin; $i<$end; $i++)
		AddColumnText($report, "", "", 0, 0);
}

function PrintCycleDays(&$report, $oparray, $begin, $end)
{
	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
	AddColumnText($report, "  Cycle", "", 0, 0);
	for($i=$begin; $i<$end; $i++)
	{
		$op = $oparray[$i];
		$firstmeeting = $op['firstmeeting'];
		$cycledays = GetElapsedDays($firstmeeting, time());

		AddColumnText($report, $cycledays, "", 0, 0);
	}
}

function PrintLocationDays(&$report, $oparray, $begin, $end, $title, $loc, $bcolor)
{
	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
	if($bcolor)
		SetRowColorStandard($report, -1, SHADE_LIGHT);
	AddColumnText($report, "  $title", "", 0, 0);
	for($i=$begin; $i<$end; $i++)
	{
		$op = $oparray[$i];
		$daycells = $op['daycells'];
		$days = $daycells[$loc];
		if($days == 0) $days = "";

		AddColumnText($report, $days, "", 0, 0);
	}
}

function PrintOpportunityRow(&$report, $oparray, $title, $varname, $begin, $end, $type)
{
	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
	SetRowColorStandard($report, -1, SHADE_MED);
	AddColumnText($report, $title, "", 0, 0);

	for($i=$begin; $i<$end; $i++)
	{
		$op = $oparray[$i];
		$data = $op[$varname];
		if($type == 1) /*date*/
			$data = MakeDate($data);

		AddColumnText($report, $data, "", 0, 0);
	}
}

function PrintSalesCycle(&$report, $oparray, $begin, $end, $companyid, $salesid)
{
//$co=count($oparray);
//print("Opcount=$co endval=$end");
	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
	SetRowColorStandard($report, -1, SHADE_LIGHT);
	AddColumnText($report, "  Average", "", 0, 0);

	for($i=$begin; $i<$end; $i++)
	{
		$op = $oparray[$i];
		$productid = $op['productid'];
		$AvgCycleDays = GetSalesCycleDays($salesid, $productid);
		AddColumnText($report, $AvgCycleDays, "", 0, 0);
	}

	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
	AddColumnText($report, "  Days Under Avg.", "", 0, 0);
	for($i=$begin; $i<$end; $i++)
	{
		$op = $oparray[$i];
		$productid = $op['productid'];
		$firstmeeting = $op['firstmeeting'];
		$AvgCycleDays = GetSalesCycleDays($salesid, $productid);
		$cycledays = GetElapsedDays($firstmeeting, time());
		$daysunder=0;
		if($AvgCycleDays > 0 && $cycledays < $AvgCycleDays) $daysunder = $AvgCycleDays - $cycledays;
		else $daysunder = "";
		AddColumnText($report, $daysunder, "", 0, 0);
	}

	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
	SetRowColorStandard($report, -1, SHADE_LIGHT);
	AddColumnText($report, "  Days Over Avg.", "", 0, 0);
	for($i=$begin; $i<$end; $i++)
	{
		$op = $oparray[$i];
		$productid = $op['productid'];
		$firstmeeting = $op['firstmeeting'];
		$AvgCycleDays = GetSalesCycleDays($salesid, $productid);
		$cycledays = GetElapsedDays($firstmeeting, time());
		$daysover=0;
		if($AvgCycleDays > 0 && $cycledays > $AvgCycleDays) $daysover = $cycledays - $AvgCycleDays;
		else $daysover = "";
		AddColumnText($report, $daysover, "", 0, 0);
	}
}

function PrintMeetingDates(&$report, $oparray, $begin, $end)
{
	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
	SetRowColorStandard($report, -1, SHADE_LIGHT);
	AddColumnText($report, "   Next", "", 0, 0);

	for($i=$begin; $i<$end; $i++)
	{
		$lastmeeting=0;
		$nextmeeting=0;
		$op = $oparray[$i];
		$dealid = $op['dealid'];
		GetMeetingDates($dealid, $nextmeeting, $lastmeeting);
		AddColumnText($report, $nextmeeting, "", 0, 0);
	}

	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
	AddColumnText($report, "   Last", "", 0, 0);
	for($i=$begin; $i<$end; $i++)
	{
		$lastmeeting=0;
		$nextmeeting=0;
		$op = $oparray[$i];
		$dealid = $op['dealid'];
		GetMeetingDates($dealid, $nextmeeting, $lastmeeting);
		AddColumnText($report, $lastmeeting, "", 0, 0);
	}
}

function PrintLocationRow(&$report, $oparray, $begin, $end, $ind)
{
	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
	$text = "";
	if($ind == 0)
		$text = "Location,   Days";

	AddColumnText($report, $text, "", 0, 0);

	for($i=$begin; $i<$end; $i++)
	{
		$op = $oparray[$i];
		$cells = $op['cells'];
		$cellcount = count($cells);
		if($ind < $cellcount)
		{
			$celldata = $cells[$ind];
			$top=$celldata['top'];
			$bottom=$celldata['bottom'];
			$text = "$top,   $bottom";
		}
		else $text = "";
		AddColumnText($report, $text, "", 0, 0);
	}
}


function AddLocationRow(&$report, $text1, $text2, $left, $width1, $width2)
{
	BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0);
	AddFreeFormText($report, $text1, -1, "center", $left, $width1);
	AddFreeFormText($report, $text2, -1, "left", $left+$width1, $width2);
}

function GetMilestoneList($personid, &$MSList)
{
	$sql="select * from Company where CompanyID=(select companyid from people where personid=$personid)";
	$res=mssql_query($sql);
	$arr = array();
	if($res)
	{
		$row=mssql_fetch_assoc($res);
		if(!is_null($row['Requirement1']))
		{
			array_push($arr, $row['Requirement1']);
		}
		$person = strlen($row['MS2Label']) > 0 ? $row['MS2Label'] : "Person";
		array_push($arr, $person);
		$need = strlen($row['MS3Label']) > 0 ? $row['MS3Label'] : "Need";
		array_push($arr, $need);
		$money = strlen($row['MS4Label']) > 0 ? $row['MS4Label'] : "Money";
		array_push($arr, $money);
		$time = strlen($row['MS52Label']) > 0 ? $row['MS5Label'] : "Time";
		array_push($arr, $time);

		if(!is_null($row['Requirement2']))
		{
			array_push($arr, $row['Requirement2']);
		}
		for ($i = 0; $i < count($arr); $i++)
		{
			if(strlen($MSList)) $MSList .= ";";
			$ind = $i + 1;
			$MSList .= "$ind=$arr[$i]";
		}
	}

	return count($arr);
}


/***********************************************************************************************/

$report = CreateReport();
$report['left'] = 72/4;
$report['right'] = 8.25 * 72;

$p = PDF_new();
PDF_set_parameter($p, "license", PDFLIB_LICENSE_KEY);
PDF_open_file($p, "");

$salesid = $treeid;


StartPDFReport($report, $p, "AAAA", "BBBB", "CCCCC");
//SetLogo($report, "report_logo.jpg", 10, 40, 72, 0);
SetLogo($report, "report_logo.jpg", 14, 50, 110, 0);

$salesname = GetSalesmanData($salesid);

$salestree = make_tree_path($treeid);
$MSCount=GetMilestoneList($treeid, $MSList);


$MSLabel=GetCatLbl("Information Phase") . " ($MSList)";



if($closed)
	$title = "Path Report (" . GetCatLbl("Closed") . " Opportunities)";
else
	$title = "Path Report (Active Opportunities)";

$report['footertext'] = $title . " for $salesname";

BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, 0.1);
AddFreeFormText($report, $title, -1, "center", 1.5, 5.5);


BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0.25);
AddFreeFormText($report, $salestree, -1, "center", 1.5, 5.5);

$left = 1;
$width1 = 1;
$width2 = 6;
BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, .2);
AddFreeFormText($report, "Location Abbreviations:", -1, "left", $left, 2.5);

/*
AddLocationRow($report, "FM", "First Meeting", $left, $width1, $width2);
AddLocationRow($report, "IP(1-$MSCount)", $MSLabel, $left, $width1, $width2);
AddLocationRow($report, "SIP", "Stalled Information Phase", $left, $width1, $width2);
AddLocationRow($report, "DP", "Decision Point", $left, $width1, $width2);
AddLocationRow($report, "SDP", "Stalled Decision Point", $left, $width1, $width2);
AddLocationRow($report, "RM", "Removed", $left, $width1, $width2);
AddLocationRow($report, "NA", "Unknown Location", $left, $width1, $width2);
*/

AddLocationRow($report, GetCatLbl("FM"), GetCatLbl("First Meeting"), $left, $width1, $width2);
AddLocationRow($report, GetCatLbl("IP") . "(1-$MSCount)", $MSLabel, $left, $width1, $width2);
AddLocationRow($report, GetCatLbl("S") . GetCatLbl("IP"), GetCatLbl("Stalled") . " " . GetCatLbl("Information Phase"), $left, $width1, $width2);
AddLocationRow($report, GetCatLbl("DP"), GetCatLbl("Decision Point"), $left, $width1, $width2);
AddLocationRow($report, GetCatLbl("S") . GetCatLbl("DP"), GetCatLbl("Stalled") . " " . GetCatLbl("Decision Point"), $left, $width1, $width2);
AddLocationRow($report, GetCatLbl("RM"), GetCatLbl("Removed"), $left, $width1, $width2);
AddLocationRow($report, GetCatLbl("T"), GetCatLbl("Target"), $left, $width1, $width2);
AddLocationRow($report, "NA", "Unknown Location", $left, $width1, $width2);

if($fulldetail)
{
	$milestones = GetMilestoneNames($mpower_companyid);
	$submilestones = GetSubMilestones($mpower_companyid);
	PrintMilestones($report, $submilestones, &$milestones);
}

$maxcells=0;
$oparray = GetOpportunities($salesid, $maxcells, $fulldetail, $submilestones, $closed, $mpower_companyid);
$count = count($oparray);

//print("count = $count<br>");

$increment = 4;
for($ind_begin=0,$ind_end=$increment; $ind_begin < $count; $ind_begin+=$increment, $ind_end+=$increment)
{
	if($ind_end > $count)
		$ind_end = $count;


	BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0.2);
	SetRowColorStandard($report, -1, SHADE_MED);
	AddColumn($report, "", "left", 1.2, 0);
	SetColColorStandard($report, -1, -1, SHADE_MED);
	for($i=$ind_begin; $i<$ind_end; $i++)
	{
		$op = $oparray[$i];
		$company = $op['company'];
		$dealid = $op['dealid'];

		AddColumn($report, "$company", "center", 1.7, 0);
	}

	/*
	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
	SetRowColorStandard($report, -1, SHADE_MED);
	AddColumnText($report, "First Mtg.", "", 0, 0);
	*/

	$maxcells = 0;
	$first_meetings = array();
	for($i=$ind_begin; $i<$ind_end; $i++)
	{
		$op = $oparray[$i];
		$cellcount = $op['cellcount'];
		if($cellcount > $maxcells)
			$maxcells = $cellcount;
		$firstmeeting = MakeDate($op['firstmeeting']);
		//AddColumnText($report, $firstmeeting, "", 0, 0);
		array_push($first_meetings, $firstmeeting);
	}

	PrintOpportunityRow($report, $oparray, "Est. Value", "estvalue", $ind_begin, $ind_end, 0);

	for($i=0; $i<$maxcells; $i++)
	{
		PrintLocationRow($report, $oparray, $ind_begin, $ind_end, $i);
		if($i % 2)
			SetRowColorStandard($report, -1, SHADE_LIGHT);
	}


	PrintTitleRow($report, "Meeting Dates", $ind_begin, $ind_end);

	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
	SetRowColorStandard($report, -1, SHADE_MED);
	AddColumnText($report, "   First", "", 0, 0);

	for ($k = 0; $k < count($first_meetings); ++$k)
		AddColumnText($report, $first_meetings[$k], "", 0, 0);

	PrintMeetingDates($report, $oparray, $ind_begin, $ind_end);

	PrintTitleRow($report, "Sales Cycle", $ind_begin, $ind_end);
	PrintSalesCycle($report, $oparray, $ind_begin, $ind_end, $companyid, $salesid);

	PrintTitleRow($report, "Total Days", $ind_begin, $ind_end);
	PrintCycleDays($report, $oparray, $ind_begin, $ind_end);
	SetRowColorStandard($report, -1, SHADE_LIGHT);

	PrintLocationDays($report, $oparray, $ind_begin, $ind_end, GetCatLbl("FM"), "FM", 0);
	PrintLocationDays($report, $oparray, $ind_begin, $ind_end, GetCatLbl("IP"), "IP", 1);
//	PrintLocationDays($report, $oparray, $ind_begin, $ind_end, GetCatLbl("SIP"), "SIP", 0);
	$sip = GetCatLbl("S") . GetCatLbl("IP");
	PrintLocationDays($report, $oparray, $ind_begin, $ind_end, $sip, "SIP", 0);

	PrintLocationDays($report, $oparray, $ind_begin, $ind_end, "Times $sip", "TSIP", 1);
	PrintLocationDays($report, $oparray, $ind_begin, $ind_end, GetCatLbl("DP"), "DP", 0);
	$sdp = GetCatLbl("S") . GetCatLbl("DP");
	PrintLocationDays($report, $oparray, $ind_begin, $ind_end, $sdp, "SDP", 1);
	PrintLocationDays($report, $oparray, $ind_begin, $ind_end, "Times " . $sdp, "TSDP", 0);
	PrintLocationDays($report, $oparray, $ind_begin, $ind_end, GetCatLbl("RM"), "RM", 1);

	BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0);
	//SetRowColorStandard($report, -1, "BLANK");
	AddColumnText($report, "", "", 0, 0);

}

PrintPDFReport($report, $p);
PDF_delete($p);

close_db();

?>
