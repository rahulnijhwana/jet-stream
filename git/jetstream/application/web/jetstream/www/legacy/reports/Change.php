<?php
 /**
 * @package Reports
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();


include('_report_utils.php');
include('_date_params.php');

begin_report("Change Report");

if (!isset($treeid))
{
//	print_tree_prompt(ANYBODY);
	print_tree_prompt(SALESPEOPLE_AND_MGR);

	end_report();
	exit();
}

if (!isset($dateFrom)||!isset($dateTo))
	{
	$tomorrow = date("m/d/Y", strtotime("+1 day"));
	print_date_params($treeid,$sortcol,$ord, "datefr:<$tomorrow", "dateto:<$tomorrow");
	if(!isset($dateFrom)) $dateFrom="N/A";
	if(!isset($dateTo)) $dateTo="N/A";
	end_report();
	exit();
	}


print("<meta http-equiv=\"Refresh\" content=\"1; URL=Change_allrecs_pdf2.php?SN=".$_REQUEST['SN']."&treeid=$treeid&dateFrom=$dateFrom&dateTo=$dateTo\">");
print('<br><br><b>     <div align="center"><span>Please wait while your report is being prepared.</span><br /><span><img src="../images/indicator.gif" alt="Loading..." /></span></div>');
// print('<br><br><b>     Please wait while your report is being prepared.<br><br><iframe style="display:none;" src="/develop/reports/sample.pdf"></iframe>');
end_report();
?>
