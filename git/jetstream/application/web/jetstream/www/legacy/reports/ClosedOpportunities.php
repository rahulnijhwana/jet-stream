<?php
 /**
 * @package Reports
 * Closed Opportunities Report
 * Prompts for Salesrep and Date Range
 */

// Definitions --------------------------------------------------------------------------------
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_export_utils.php');
include('_report_utils.php');
include('_date_params.php');
include('TableUtils.php');
include('jim_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');

define(SHADE_MED, "MED_BLUE");
define(SHADE_LIGHT, "LIGHT_BLUE");

global $report_export_option;

$cellfontsize="16px";

// Functions --------------------------------------------------------------------------------
function array_csort($marray, $column, $sortord)
{
  $sortarr=array();
  foreach ($marray as $row) {
    $sortarr[] = strtoupper($row[$column]);
  }
  array_multisort($sortarr, $sortord, $marray);
  return $marray;
}

function GetCompanyName($id)
{
	$sql = "select * from company where CompanyID='$id'";
	$ok = mssql_query($sql);
	if(($row = mssql_fetch_assoc($ok)))
	{
		return $row['Name'];
	}
	return 'Unknown!';
}

function EnterTotals(&$arrTotals, $key, $Estimated, $Actual)
{
	for ($i=0; $i<count($arrTotals); $i++)
	{
		if($key == $arrTotals[$i][0])
		{
			$arrTotals[$i][1] += $Estimated;
			$arrTotals[$i][2] += $Actual;
			return;
		}
	}
	$tmp = array();
	$tmp[0]=$key;
	$tmp[1]=$Estimated;
	$tmp[2]=$Actual;
	array_push($arrTotals, $tmp);
}

function GetMonthQuarter($m)
{
	$m--;
	return floor($m/3) + 1;
}

function MakeTotals(&$arrTotals, $CloseDate, $Estimated, $Actual)
{
	$info = getdate(strtotime($CloseDate));
	$y = $info['year'];
	$m = $info['mon'];
	$q = GetMonthQuarter($m);
	$m--;
	EnterTotals($arrTotals, "$y", $Estimated, $Actual);
	EnterTotals($arrTotals, "$y|$q", $Estimated, $Actual);
	EnterTotals($arrTotals, "$y|X|$m", $Estimated, $Actual);
}

function GetTotals($key, &$Estimated, &$Actual)
{
global $arrTotals;
	$Estimated = 0;
	$Actual = 0;
	for ($i = 0; $i < count($arrTotals); $i++)
	{
		if($key == $arrTotals[$i][0])
		{
//			$Estimated = '$' . number_format($arrTotals[$i][1]);
//			$Actual = '$' . number_format($arrTotals[$i][2]);
			$Estimated = fm($arrTotals[$i][1]);
			$Actual = fm($arrTotals[$i][2]);

		}
	}
}

function PrintQtrRow($yr, $q)
{
	$arrMonths = array('Jan','Feb','Mar','Apr','May','June','July','Aug','Sept','Oct','Nov','Dec');
	$mInd = ($q-1) * 3;

	for ($i=0; $i<2; $i++)
	{
		BeginTableRowData(1);
		GetTotals("$yr|$q", $Estimated, $Actual);
		if (!$i)
		{
			PrintTableCellEx("Q$q","align='center' width='6%' rowspan='2' bgcolor='lightblue'");
			PrintTableCellEx("Est:","align='left' width='6%' bgcolor='lightblue'");
			PrintTableCellEx("$Estimated", "align='right' width='13%' bgcolor='lightblue'");
		}
		else
		{
			PrintTableCellEx("Act:","align='left' width='6%' bgcolor='lightblue'");
			PrintTableCellEx("$Actual", "align='right' width='13%' bgcolor='lightblue'");
		}
		for($j=$mInd; $j < $mInd + 3; $j++)
		{
			$key="$yr|X|$j";
			GetTotals($key, $Estimated, $Actual);
			if (!$i)
			{
				PrintTableCellEx("$arrMonths[$j]","align='center' rowspan='2' width='6%' ");
				PrintTableCellEx("Est:","align='left' width='6%' ");
				PrintTableCellEx("$Estimated", "align='right'width='13%'");
			}
			else
			{
				PrintTableCellEx("Act:","align='left' width='6%'");
				PrintTableCellEx("$Actual", "align='right' width='13%' ");
			}
		}
		EndTableRow();
	}
}

function PrintTotalsHeader($yr)
{
	GetTotals("$yr", $Estimated, $Actual);
	$str  = "<table border=0 cellspacing=0 cellpadding=3 bgcolor='ivory' width='70%'>";
	$str .= "<tr><td align='left' width='20%' border=0><b>$yr</td>";
	$str .= "<td align='right' width='10%' border=0>Estimated:</td><td align='right' width='20%' border=0>$Estimated</td>";
	$str .= "<td align='right' width='10%' border=0>Actual:</td><td align='right' width='20%' border=0>$Actual</td></tr></table>";
	print($str);
}

function PrintTotals($yr, $qStart, $qStop)
{
	global $arrTotals;
//	GetTotals("$yr", $Estimated, $Actual);
//	PrintTableSpan(12, "<b>$yr Est: $Estimated Act: $Actual");

	PrintTotalsHeader($yr);
//	for ($i=1; $i<5; $i++)
	print "<table border=2 cellspacing=0 cellpadding=3 bgcolor='ivory' width='100%'>";
	for ($i=4; $i>0; $i--)
	{
		if ($i > $qStop) continue;
		if ($i < $qStart) break;
		PrintQtrRow($yr, $i);
	}
EndTable();
}

function GetDateComponent($timestamp, $key)
{
	$info = getdate($timestamp);
	return $info[$key];
}

function PrintDetail($dFrom, $dTo, $sortind, $sortord)
{
	global $arrDeals, $treeid;

	$url = $_SERVER['PHP_SELF'] . "?SN={$_REQUEST['SN']}&treeid=$treeid";
	if($dFrom!=""&&$dFrom!="N/A")
	{
		$uFrom=urlencode($dFrom);
		$uTo=urlencode($dTo);
		$url.="&dateFrom=$uFrom&dateTo=$uTo";
	}

	$arrColHeads=array(
		"<b>".GetCatLbl("Closed")." Date<br>(<A HREF=\"$url&sortcol=0&ord=0\">A </A><A HREF=\"$url&sortcol=0&ord=1\">D</A>)",
		"<b>Company<br>(<A HREF=\"$url&sortcol=1&ord=0\">A </A><A HREF=\"$url&sortcol=1&ord=1\">D </A>)",
		"<b>Sales Name<br>(<A HREF=\"$url&sortcol=2&ord=0\">A </A><A HREF=\"$url&sortcol=2&ord=1\">D</A>)",
		"<b>Offering<br>(<A HREF=\"$url&sortcol=3&ord=0\">A </A><A HREF=\"$url&sortcol=3&ord=1\">D</A>)",
		"<b>Source<br>(<A HREF=\"$url&sortcol=4&ord=0\">A </A><A HREF=\"$url&sortcol=4&ord=1\">D</A>)",
		"<b>Estimated<br>(<A HREF=\"$url&sortcol=5&ord=0\">A </A><A HREF=\"$url&sortcol=5&ord=1\">D</A>)",
		"<b>Actual<br>(<A HREF=\"$url&sortcol=6&ord=0\">A </A><A HREF=\"$url&sortcol=6&ord=1\">D</A>)",
	);

	//	BeginTable();
	print("<table border=2 cellspacing=0 cellpadding=3 bgcolor='ivory' width='100%'>");
	
	BeginTableRowData(2);
	PrintTableSpan(8, "<br><b>Detail");
	EndTableRow();

	PrintColHeads($arrColHeads, 1, 0, count($arrColHeads),FALSE);

	$arrFieldNames= array('ActualCloseDate','Company','SalesName','ProductName','Abbr','EstimatedDollarAmount','ActualDollarAmount');

	$arrDeals=array_csort($arrDeals, $arrFieldNames[$sortind], $sortord);

	foreach ($arrDeals as $Deal) {
		$d = date("m/d/y", $Deal['ActualCloseDate']);
		$DealID = $Deal['DealID'];
		BeginTableRow();
		PrintTableCell($d);
		$tempcompany = $Deal['Company'];
		if ($Deal['Division'] != '')
			$tempcompany .= '&nbsp;-&nbsp;'.$Deal['Division'];
		PrintTableCell($tempcompany);
		PrintTableCell($Deal['SalesName']);
	
		//		$OppProducts = GetOppProducts($DealID);
		$OppProducts = $Deal['ProductName'];
	
		if(strlen($OppProducts) > 0) PrintTableCell($OppProducts);
		else PrintTableCell($Deal['ProductName']);
	
		PrintTableCell($Deal['Abbr']);
	
		//		PrintTableCellEx('$' . number_format($arrDeals[$i]['EstimatedDollarAmount']), "align='right'");
		PrintTableCellEx(fm($Deal['EstimatedDollarAmount']), "align='right'");
	
		//		PrintTableCellEx('$' . number_format($arrDeals[$i]['ActualDollarAmount']), "align='right'");
		PrintTableCellEx(fm($Deal['ActualDollarAmount']), "align='right'");

		EndTableRow();
	}
	EndTable();
}

function PrintDetailColumnHeaders()
{
	BeginTableRowData(1);
	PrintTableCell("<b>".GetCatLbl("Closed")." Date");
	PrintTableCell("<b>Company");
	PrintTableCell("<b>Sales Name");
	PrintTableCell("<b>Offering");
	PrintTableCell("<b>Source");
	PrintTableCell("<b>Estimated");
	PrintTableCell("<b>Actual");
	EndTableRow();
}

function SetExportArrayCO($arrDeals, $salesbranch)
{
	// Load a detail array that we will save in case they want an export file.
	// Input: $arrDeals - a detail array without the rep tree
	//        $salesbranch - the rep tree to add to the detail
	// Output: $arrExport - array of detail merged with the rep tree
	// 05/28/2013: Vern Gorman - Blue Star Technologies: (#11531) New function for data export

	$tmpExport = array();

	// Load the column headers as themselves
	$tmp = array();
	$tmp[0] = 'Level1';
	$tmp[1] = 'Level2';
	$tmp[2] = 'Level3';
	$tmp[3] = 'Level4';
	$tmp[4] = 'Level5';
	$tmp[5] = 'Level6';
	$tmp[6] = 'ActualCloseDate';
	$tmp[7] = 'Company';
	$tmp[8] = 'Offering';
	$tmp[9] = 'Source';
	$tmp[10] = 'EstimatedDollarAmount';
	$tmp[11] = 'ActualDollarAmount';
	array_push($tmpExport, $tmp);

	// Load all the report detail
	foreach ($arrDeals as $deal) {		
		$tmp = array();
		// Add the sales person branch to each detail line
		$tmp[0] = $salesbranch[0];
		$tmp[1] = $salesbranch[1];
		$tmp[2] = $salesbranch[2];
		$tmp[3] = $salesbranch[3];
		$tmp[4] = $salesbranch[4];
		$tmp[5] = $salesbranch[5];
		
		$d = date("m/d/y", $deal['ActualCloseDate']);
		$DealID = $deal['DealID'];
		$tmp[6] = $d;
		$tempcompany = $deal['Company'];
		if ($deal['Division'] != '')
			$tempcompany .= ' - '.$deal['Division'];
		$tmp[7] = $tempcompany;
		$tmp[8] = $deal['ProductName'];
		$tmp[9] = $deal['Abbr'];
		$tmp[10] = $deal['EstimatedDollarAmount'];
		$tmp[11] = $deal['ActualDollarAmount'];		
		array_push($tmpExport, $tmp);
	}	
	return $tmpExport;
}

// Inline Code --------------------------------------------------------------------------------

if(isset($_SESSION['ExportArray'])) {
// Check for export before loading anything else.  
// The export headers need to be first.
// If you put anything other then the export data in this condition, it will appear in the export file.
	if (isset($_GET['export'])) {
		//export_csv("closed_opp_rpt");
		export_xls("closed_opp_rpt");
		exit();
	}	
	
	// Reset variable holding report detail for Export file
	unset($_SESSION['ExportArray']);
}

//***** Salesrep Prompt
if (!isset($treeid)) {
	begin_report(GetCatLbl('Closed'). ' Opportunities Report');
	print_tree_prompt(ANYBODY);
	end_report();
	exit();
}

//***** Date Range Prompt
if (!isset($dateFrom)||!isset($dateTo)) {
	$sortcol=0;
	$ord=0;
	$tomorrow = date("m/d/Y", strtotime("+1 day"));
	begin_report(GetCatLbl('Closed'). ' Opportunities Report');
	print_date_params($treeid,$sortcol,$ord, "datefr:<$tomorrow", "dateto:<$tomorrow");
	if(!isset($dateFrom)) $dateFrom="N/A";
	if(!isset($dateTo)) $dateTo="N/A";
	end_report();
	exit();
}

$report_export_option = 1;
begin_report(GetCatLbl('Closed'). ' Opportunities Report');
$report_export_option = 0;

if(!isset($sortcol)) $sortcol=0;

if($ord==0) $sortord=SORT_ASC;
else $sortord=SORT_DESC;

$total=0;

$idList = GetSalespeopleTree($treeid);

$sql = "SELECT A.$account_name as Company, opportunities.Division, opportunities.DealID, opportunities.EstimatedDollarAmount,";
$sql .= " opportunities.ActualDollarAmount, Sources.Abbr,";
//$sql .= " opportunities.ActualCloseDate, products.Name AS ProductName, people.FirstName, people.LastName";
$sql .= " opportunities.ActualCloseDate, people.FirstName, people.LastName";
$sql .= " FROM opportunities INNER JOIN people ON opportunities.PersonID = people.PersonID";
$sql .= " LEFT JOIN Account A on opportunities.AccountID = A.accountid";
$sql .= " LEFT OUTER JOIN Sources ON opportunities.SourceID = Sources.SourceID";
//$sql .= " INNER JOIN products ON opportunities.ProductID = products.ProductID";
$sql .= " WHERE opportunities.Category = 6 AND people.PersonID IN ($idList)";
$sql .= " AND opportunities.ActualCloseDate >= '$dateFrom' AND opportunities.ActualCloseDate <= '$dateTo'";
$sql .= " ORDER BY opportunities.ActualCloseDate DESC, A.$account_name ASC, opportunities.Division ASC";
$ok = mssql_query($sql);

$arrDeals = array();
$arrTotals = array();
while(($row=mssql_fetch_assoc($ok)))
{
	$tmp = array();
	$tmp['ActualCloseDate'] = strtotime($row['ActualCloseDate']);
	$tmp['Company'] = $row['Company'];
	$tmp['Division'] = $row['Division'];
	$tmp['DealID'] = $row['DealID'];
//	$tmp['ProductName'] = $row['ProductName'];
	$tmp['ProductName'] = GetOppProducts($row['DealID']);
	$tmp['SalesName'] = $row['LastName'] . ', ' . $row['FirstName'];
	$tmp['EstimatedDollarAmount'] = $row['EstimatedDollarAmount'];
	$tmp['ActualDollarAmount'] = $row['ActualDollarAmount'];
    $tmp['Abbr'] = $row['Abbr'];

    MakeTotals($arrTotals, $row['ActualCloseDate'], $tmp['EstimatedDollarAmount'], $tmp['ActualDollarAmount']);
	array_push($arrDeals, $tmp);
}

//***** Display report to screen
print_tree_path($treeid);

$strdate = date("M d Y", time());

$title=GetCatLbl("Closed") . " Opportunities Report";

$salestree=make_tree_path($treeid);

BeginTable();

//print("<table width='100%'>");
print("<table border=2 cellspacing=0 cellpadding=3 bgcolor='ivory' width='100%'>");

//print("<tr ><td align='center'><b>$title</td></tr>");

$companyname = GetCompanyName($mpower_companyid);
BeginTableRow();
PrintTableSpan(4, "<b>$companyname");
EndTableRow();

$shortdateFrom = date('m/d/y', strtotime($dateFrom));
$shortdateTo = date('m/d/y', strtotime($dateTo));

$daterange = "Date Range: $shortdateFrom to $shortdateTo";

BeginTableRow();
PrintTableSpan(4, "<b>$daterange<br><br>");
EndTableRow();
EndTable();
//print('<table width="100%">');
print("<table border=2 cellspacing=0 cellpadding=3 bgcolor='ivory' width='100%'>");
BeginTableRowData(2);
PrintTableSpan(12, "<b>Totals");
EndTableRow();

$endyear = GetDateComponent(strtotime($dateTo),'year');
$startyear = GetDateComponent(strtotime($dateFrom),'year');
$qStop=GetMonthQuarter(GetDateComponent(strtotime($dateTo), 'mon'));
$qStart=GetMonthQuarter(GetDateComponent(strtotime($dateFrom), 'mon'));

$year=$endyear;
EndTable();
while($year >= $startyear)
{
	$q1 = 0; $q2 = 5;
	if ($year==$startyear) $q1 = $qStart;
	if ($year==$endyear) $q2 = $qStop;
	PrintTotals($year--, $q1, $q2);
}

//EndTable();

if(!isset($sortcol)) $sortcol=0;

if($ord==0) $sortord=SORT_ASC;
else $sortord=SORT_DESC;

//print detail
PrintDetail($dateFrom, $dateTo, $sortcol, $sortord);

// Get a branch from the selected rep level up
$salesbranch = GetSalesPersonBranch($treeid);

//load array and session variable just in case they decide to create an Export file
$arrExport = array();
$arrExport = SetExportArrayCO($arrDeals, $salesbranch);

// load to session
$_SESSION['ExportArray']=$arrExport;

// Enable for debugging the export file
//foreach ($_SESSION['ExportArray'] as $x) {
//	echo "<p>Level1: ".$x['Level1']."<br />";
//	echo "Level2: ".$x['Level2']."<br />";
//	echo "Level3: ".$x['Level3']."<br />";
//	echo "Level4: ".$x['Level4']."<br />";
//	echo "Level5: ".$x['Level5']."<br />";
//	echo "Level6: ".$x['Level6']."<br />";
//	echo "Company: ".$x['Company']."<br />";
//	echo "ProductName: ".$x['ProductName']."<br />";
//	echo "Abbr: ".$x['Abbr']."<br />";
//	echo "ActualCloseDate: ".$x['ActualCloseDate']."<br />";
//	echo "EstimatedDollarAmount: ".$x['EstimatedDollarAmount']."<br />";
//	echo "ActualDollarAmount: ".$x['ActualDollarAmount']."<br />";
//}

end_report();

close_db();

?>