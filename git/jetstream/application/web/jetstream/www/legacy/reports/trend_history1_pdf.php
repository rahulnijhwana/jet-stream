<?php
 /**
 * @package Reports
 */
include('_report_utils.php');
include('trends_pdf.php');
include('ci_pdf_utils.php');

define(SHADE_MED, "MED_BLUE");
define(SHADE_LIGHT, "LIGHT_BLUE");



$report = CreateReport();
$report['left'] = 72/4;
$report['right'] = 8.25 * 72;

$title = "Trend History Report";

$p = PDF_new();
PDF_set_parameter($p, "license", PDFLIB_LICENSE_KEY);
PDF_open_file($p, "");
StartPDFReport($report, $p, "AAAA", "BBBB", "CCCCC");
//SetLogo($report, "report_logo.jpg", 10, 40, 72, 0);
SetLogo($report, "report_logo.jpg", 14, 50, 110, 0);
$salestree=make_tree_path($treeid);
$report['footertext']=$title;

BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, 0.1);
AddFreeFormText($report, $title, -1, "center", 1.5, 5.5);

BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0.25);
AddFreeFormText($report, $salestree, -1, "center", 1.5, 5.5);

$left = 1;
$width1 = 1;
$width2 = 3;


//PrintLocationKey($report, 1, 2, 2, $treeid);

TrendHistoryReport($report, $treeid, $mpower_companyid);

PrintPDFReport($report, $p);
PDF_delete($p);


/******************************************************************************************/
function Past12($Values)
{
	$today=GetDate();
	$d=$today["mday"];
	$m=$today["mon"];
	$y=$today["year"]-1;
	$lastyr=mktime(0,0,0,$m,$d,$y);
	$sz=count($Values);
	$ct=0;
	for($i=0; $i<$sz; $i++)
	{
		if($Values[$i]['enddate']>$lastyr) $ct++;
	}
	return $ct;

}

function PrintTrendHistoryRow(&$report, $TrendName, $Values, $begin, $end)
{
	$ycount=Past12($Values);
	$rowhead = "$TrendName\n($ycount in 12 months)";
//	AddColumnText($report, $TrendName, "", 0, 0);
	AddColumnText($report, $rowhead, "", 0, 0);
	$count = count($Values);

	for($i=$begin; $i<$end; $i++)
	{
		if($i<$count)
		{
			$Inst = $Values[$i];
			$Start = $Inst['startdate'];
			$End = $Inst['enddate'];
			$Days = GetElapsedDays($Start, $End);

			$StartDate = date("m/d/y", $Start);
			$EndDate = date ("m/d/y", $End);

			AddColumnText($report, "$StartDate - $EndDate\r($Days Days)", "", 0, 0);
		}
	}

}

function TrendHistoryReport(&$report, $salesid, $companyid)
{

	$TrendNames = Array();
	$TrendIds = Array();

	GetTrends($TrendNames, $TrendIds, $companyid);
	$Count = count($TrendNames);

	$history = array();
	$maxcount = 0;
	$Name = GetSalesmanName($salesid);

	for($i=0; $i<$Count; $i++)
	{
		$values = array();
		GetTrendHistory($TrendIds[$i]+100, $salesid, $values);
		$ct = count($values);
		if($ct > $maxcount)
			$maxcount = $ct;

		array_push($history, $values);
	}

	$begin = 0;
	$columns = 4;
	$end = $columns;

	$rowcolor=TRUE;
	while(1)
	{
		if($end > $maxcount) $end = $maxcount;
		if($end <= $begin) break;


		BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0);
		if ($rowcolor) SetRowColorStandard($report, -1, SHADE_MED);
		$rowcolor=!$rowcolor;
		AddColumn($report, "TREND", "left", 1.2, 0);
		SetColColorStandard($report, -1, -1, SHADE_MED);

		for($i=$begin; $i<$end; $i++)
			AddColumn($report, $i+1, "center", 1.4, 0.2);

		for($i=0; $i<$Count; $i++)
		{
			BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0.2);
			if ($rowcolor) SetRowColorStandard($report, -1, SHADE_MED);
			$rowcolor=!$rowcolor;
			$values = $history[$i];
			PrintTrendHistoryRow($report, $TrendNames[$i], $values, $begin, $end);
		}

		$begin += $columns;
		$end += $columns;
	}


//	EndTable();
}
close_db();
?>
