<?php
 /**
 * @package Reports
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_report_utils.php');

begin_report("Ranking Report");

if (!isset($treeid))
{
	$href = 'Ranking2.php?SN='.$_GET['SN'].'&rollup=0&ctime='.strtotime("now");
	print_tree_prompt(MANAGEMENT, $href);
	end_report();
	exit();
}

print("<meta http-equiv=\"Refresh\" content=\"1; URL=Ranking2.php?SN=".$_REQUEST['SN']."&treeid=$treeid&rollup=0&ctime=".strtotime("now")."\">");
print('<br><br><b>     <div align="center"><span>Please wait while your report is being prepared.</span><br /><span><img src="../images/indicator.gif" alt="Loading..." /></span></div>');
// print('<br><br><b>     Please wait while your report is being prepared.<br><br><iframe style="display:none;" src="/develop/reports/sample.pdf"></iframe>');
end_report();
?>
