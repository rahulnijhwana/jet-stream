<?php
 /**
 * @package Reports
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_report_utils.php');
include('ci_pdf_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');

define(SHADE_MED, "MED_BLUE");
define(SHADE_LIGHT, "LIGHT_BLUE");

$sql = "select * from people where PersonID=$treeid";
$result = mssql_query($sql);
if($row = mssql_fetch_array($result))
{
	$level = $row['Level'];
	$salesname = $row['FirstName'] . " " . $row['LastName'];
}

$report = CreateReport();
$report['pagewidth'] = 11*72;
$report['pageheight'] = 8.5*72;
$report['left'] = 72/4;
$report['right'] = 10.75 * 72;

$report['bottom'] = 8.25 * 72;

$report['normalfontsize'] = 9;



$p = PDF_new();
PDF_set_parameter($p, "license", PDFLIB_LICENSE_KEY);
PDF_open_file($p, "");

StartPDFReport($report, $p, "AAAA", "BBBB", "CCCCC");

//SetLogo($report, "report_logo.jpg", 10, 40, 72, 0);
SetLogo($report, "report_logo.jpg", 14, 50, 110, 0);

$salestree = make_tree_path($treeid);
$title = "Ranking Report";


$report['footertext'] = $title . " for $salesname";

BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, 0.1);
AddFreeFormText($report, $title, -1, "center", 2.75, 5.5);


BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0.1);
AddFreeFormText($report, $salestree, -1, "center", 2.75, 5.5);

PrintRanking($report, $treeid, $rollup, $level);

PrintPDFReport($report, $p);
PDF_delete($p);

close_db();
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
function GetBrand($companyid)
{
	$sql  = "SELECT Branding.BrandName, Branding.VendorName";
	$sql .= " FROM company INNER JOIN Resellers";
	$sql .= " ON company.ResellerID = Resellers.ResellerID";
	$sql .= " INNER JOIN Branding ON Resellers.BrandID = Branding.BrandID";
	$sql .= " WHERE (company.CompanyID = $companyid)";
	$ok=mssql_query($sql);
	$brand='Unknown';
	if($ok)
	{
		if($row=mssql_fetch_assoc($ok))
		{
			$brand = $row['BrandName'] . " " . $row['VendorName'];
		}
	}
	return $brand;

}
function CalcCurrentRank($idlist)
{
	if(strlen($idlist) == 0) return 0;

	$enddate = time();

	$sql = "SELECT O.*, A.$account_name as Company, (C.$contact_first_name + ' ' + C.$contact_last_name) AS Contact
		FROM opportunities O
		LEFT JOIN Account A on O.AccountID = A.accountid
		LEFT JOIN Contact C on O.ContactID = C.ContactID
		WHERE O.PersonID IN($idlist)";
	$result = mssql_query($sql);

	$fmcount = 0;
	$ipcount = 0;
	$dpcount = 0;
	$ccount = 0;

	$category = -1;
	$when = "";
	while($row = mssql_fetch_array($result))
	{
		$dealid = $row['DealID'];
		$category = $row['Category'];

		if($category == 1) $fmcount++;
		else if($category == 2) $ipcount++;
		else if($category == 4) $dpcount++;
		else if($category == 6)
		{
			$changedate = strtotime($row['ActualCloseDate']);
			$days = GetElapsedDays($changedate, $enddate);
			if($days < 30) $ccount++;
		}

	}

	$rank = CalcRank($idlist, $fmcount, $ipcount, $dpcount, $ccount);
	return $rank;
}

function CalcPeriodRank($idlist, $dateStart, $dateEnd)
{
/*
SELECT      AVG(DISTINCT Rating) AS avgRating, AVG(DISTINCT FM) AS avgFM, AVG(DISTINCT IP) AS avgIP, AVG(DISTINCT DP) AS avgDP, AVG(DISTINCT Closed) AS avgClosed,
                      AVG(DISTINCT TotalSales) AS avgTotalSales, AVG(DISTINCT TotSched) AS avgTotalSched, AVG(DISTINCT RemovedAvgAge) AS avgRemovedAvgAge,
                      AVG(DISTINCT StalledAvgAge) AS avgStalledAvgAge, AVG(DISTINCT DPAvgAge) AS avgDPAvgAge
FROM         snapshot
WHERE     (SalesID in ($idlist)) AND (SnapDate > '$dateStart') AND (SnapDate < '$dateEnd')
GROUP BY CompanyID
*/





}

function CalcRank($idlist, $fmcount, $ipcount, $dpcount, $ccount)
{
print("start of calcrank");

	if(strlen($idlist) == 0) return -1;
//print "Input: $idlist, $fmcount, $ipcount, $dpcount, $ccount<br>";

	$fm = 0;
	$ip = 0;
	$dp = 0;
	$c = 0;

// Get total of thresholds
	$sql = "select * from people where PersonID IN($idlist)";
	$result = mssql_query($sql);
	while($row = mssql_fetch_array($result))
	{
		$fm += $row['FMThreshold'];
		$ip += $row['IPThreshold'];
		$dp += $row['DPThreshold'];
		$c += $row['CloseThreshold'];
		$count++;
	}

	$fm2 = $fm * 0.66;
	$fm3 = $fm * 1.5;

	$ip2 = $ip * 0.66;
	$ip3 = $ip * 1.5;

	$dp2 = $dp * 0.66;
	$dp3 = $dp * 1.5;

	$c2 = $c * 0.66;
	$c3 = $c * 1.5;

	if($fmcount == 0) $fmval = 0;
	else if($fmcount < $fm2) $fmval = 1;
	else if($fmcount < $fm3) $fmval = 2;
	else $fmval = 3;

	if($ipcount == 0) $ipval = 0;
	else if($ipcount < $ip2) $ipval = 1;
	else if($ipcount < $ip3) $ipval = 2;
	else $ipval = 3;

	if($dpcount == 0) $dpval = 0;
	else if($dpcount < $dp2) $dpval = 1;
	else if($dpcount < $dp3) $dpval = 2;
	else $dpval = 3;

	if($ccount == 0) $cval = 0;
	else if($ccount < $c2) $cval = 1;
	else if($ccount < $c3) $cval = 2;
	else $cval = 3;

	$sql = "select * from analysis where FMValue=$fmval AND IPValue=$ipval AND DPValue=$dpval AND CloseValue=$cval";
	$result = mssql_query($sql);
	if($row = mssql_fetch_array($result))
	{
		$rank = $row['RankExperienced'];
//print "RANK: $rank, based on vals=($fmval,$ipval,$dpval,$cval)<br>";

		return $rank;
	}
	//return "($fmval,$ipval,$dpval,$cval)";
//print "RANK NOT FOUND! based on vals=($fmval,$ipval,$dpval,$cval)<br>";
	return 0;
}


function GetElapsedDays($StartTime, $EndTime)
{
	if(!($EndTime>$StartTime))
		return 0;
	$Diff = $EndTime - $StartTime;
	return (int)($Diff / (60 * 60 * 24));
}

function PrevQuarterStart($mon, $year)
{
	if($mon > 1) $mon -= 3;
	else
	{
		$mon = 10;
		$year--;
	}

	$date = "$mon/1/$year";

	return $date;
}

function CurrentQuarterStart($mon)
{
	if($mon < 4) $m=1;
	else if($mon < 7) $m = 4;
	else if($mon < 10) $m = 7;
	else $m = 10;

	return $m;
}

function GetTotalsArray()
{
	$a = array();

	$a['analysis'] = 0;
	$a['fm'] = 0;
	$a['ip'] = 0;
	$a['dp'] = 0;
	$a['closecount'] = 0;
	$a['closeamount'] = 0;
	$a['avg'] = 0;
	$a['days'] = 0;

	return $a;
}

function PrintRanking(&$report, $personid, $rollup, $level)
{
	$today = getdate();
	$mon = $today['mon'];
	$day = $today['mday'];
	$year = $today['year'];

	$strCurrent = "$mon/$day/$year";

	$m = CurrentQuarterStart($mon);
	$startCurrent = "$m/1/$year";

	$startPrev = PrevQuarterStart($m, $year);
	$startYTD = "1/1/$year";

	$y = $year - 1;
	$start12Month = "$mon/$day/$y";

	$d_sc = strtotime($startCurrent);
	$d_sp = strtotime($startPrev);
	$d_sytd = strtotime($startYTD);
	$d_s12 = strtotime($start12Month);

	$totals = array();

	if($rollup && $level>2)
	{
		$sql = "select * from people where Deleted = 0 AND SupervisorID=$personid order by LastName";

		$result = mssql_query($sql);
		while($row = mssql_fetch_array($result))
		{
			$tmp = array();
			$id = $row['PersonID'];
			$idlist = GetSalespeopleTree($id);
			$tmp['idlist'] = "$idlist";

			$fullname = GetFullName($row, false, false);
			$tmp['name'] = $fullname;

			$tmp['CurrentQuarter'] = GetTotalsArray();
			$tmp['PrevQuarter'] = GetTotalsArray();
			$tmp['YTD'] = GetTotalsArray();
			$tmp['12Month'] = GetTotalsArray();
			array_push($totals, $tmp);
		}
	}
	else
	{
		$inclause = GetSalespeopleTree($personid);
		$sql = "select * from people where Deleted = 0 AND PersonID IN($inclause) order by LastName";

		$result = mssql_query($sql);
		while($row = mssql_fetch_array($result))
		{
			$tmp = array();
			$id = $row['PersonID'];
			$tmp['idlist'] = "$id";

			$fullname = GetFullName($row, false, false);
			$tmp['name'] = $fullname;

			$tmp['CurrentQuarter'] = GetTotalsArray();
			$tmp['PrevQuarter'] = GetTotalsArray();
			$tmp['YTD'] = GetTotalsArray();
			$tmp['12Month'] = GetTotalsArray();
			array_push($totals, $tmp);
		}
	}

	$count = count($totals);
	for($i=0; $i<$count; $i++)
	{

		GetTotals($totals, $i, $d_sc, $d_sp, $d_sytd, $start12Month);
	}
	SortTotals($totals);
	PrintRankingReport($report, $totals);
}

function SortTotals(&$totals)
{
	SortArray($totals, 'rank');
}

function SortArray(&$array, $col)
{
	$desc = 0;
	if(func_num_args() > 2)
		$desc = func_get_arg(2);

	$count = count($array);
	$continue = true;
	while($continue)
	{
		$continue = false;
		for($i=1; $i<$count; $i++)
		{
			if(!$desc)
			{
				$rank1 = $array[$i-1][$col];
				$rank2 = $array[$i][$col];
			}
			else
			{
				$rank1 = $array[$i][$col];
				$rank2 = $array[$i-1][$col];
			}
			if($rank2 > $rank1)
			{
				$continue = true;
				$tmp = $array[$i];
				$array[$i] = $array[$i-1];
				$array[$i-1] = $tmp;
			}
		}
	}
}

function SetColumnRanks(&$totals)
{
$arrSections = array("CurrentQuarter","PrevQuarter","YTD","12Month");

//	SetColumnRank($totals,'', 'rank',0);

	for ($i = 0; $i < 4; $i++)
	{
			SetColumnRank($totals, $arrSections[$i], 'Rating',0);
			SetColumnRank($totals, $arrSections[$i], 'fm',0);
			SetColumnRank($totals, $arrSections[$i], 'ip',0);
			SetColumnRank($totals, $arrSections[$i], 'dp',0);
			SetColumnRank($totals, $arrSections[$i], 'closedcount',0);
			SetColumnRank($totals, $arrSections[$i], 'closedamount',0);
			SetColumnRank($totals, $arrSections[$i], 'avg', 1);

	}
}

function SetColumnRank(&$totals, $section, $colname, $rev)
{
	$colnamerank = $colname . 'RANK';
	$arrRanks = array();
	for ($i = 0; $i < count($totals); $i++)
	{
		$tmp = array();
		$tmp['ind'] = $i;
		if($section != '')
			$v=$totals[$i][$section][$colname];
		else
			$v=$totals[$i][$colname];
		$tmp['val'] = $v;
		$tmp['rank'] = 0;
		array_push($arrRanks, $tmp);
	}
	SortArray($arrRanks, 'val', $rev);
	$nextrank = 0;
	$rank = 0;
	$last = -1;
	for ($i = 0; $i < count($arrRanks); $i++)
	{
		$nextrank++;
		if ($arrRanks[$i]['val'] != $last)
			$rank = $nextrank;
		$last = $arrRanks[$i]['val'];
		$arrRanks[$i]['rank'] = $rank;
	}
	for ($i=0; $i<count($arrRanks); $i++)
	{
		$indTotals=$arrRanks[$i]['ind'];
		if($section != '')
			$totals[$indTotals][$section][$colnamerank] = $arrRanks[$i]['rank'];
		else
			$totals[$indTotals][$colnamerank] = $arrRanks[$i]['rank'];

	}
}

function GetDisp($val, $dec)
{
	$k = 0;
	$argcount=func_num_args();
	if ($argcount > 2)
	{
		$args = func_get_args();
		$k = $args[2];
	}
	if ($k) $val /= 1000;
	$disp = '0';
	if($dec == -1)
		$disp = number_format($val);
	else
	{
		if ($val > 0) $disp = round($val, $dec);

		if($dec>0)
		{
			if(''==strstr($disp, '.'))
			{
				$disp .= '.';
				while($dec-- > 0)
				{
					$disp .= '0';
				}
			}

		}
	}
	if ($disp < 1) $disp = '0';
	return $disp;
}

function BoxRowItems(&$report, $startcol)
{
	$c_row = $report['currentrow'];
	for ($i=1; $i < 8; $i++)
	{
		$colind = $startcol + $i;
		SetItemValue($report, $c_row, $colind, 'border_right', 1);
		SetItemValue($report, $c_row, $colind, 'border_left', 1);
//		SetItemValue($report, $c_row, $colind, 'border_bottom', 1);
//		SetItemValue($report, $c_row, $colind, 'border_top', 1);
	}

}

function AddRowSection(&$report, &$totals, $tag, $ind)
{
	$fmcount = $totals[$ind][$tag]['fm'];
	$ipcount = $totals[$ind][$tag]['ip'];
	$dpcount = $totals[$ind][$tag]['dp'];
	$ccount = $totals[$ind][$tag]['closecount'];
	$idlist = $totals[$ind]['idlist'];
	switch($tag)
	{
		case 'CurrentQuarter':
			$colind = 0;
			break;
		case 'PrevQuarter':
			$colind = 8;
			break;
		case 'YTD':
			$colind = 16;
			break;
		case '12Month':
			$colind = 24;
	}
	$c_row = $report['currentrow'];
	AddColumnText($report, GetDisp($totals[$ind][$tag]['Rating'], 1), "right", 0, 0);

	AddColumnText($report, GetDisp($totals[$ind][$tag]['fm'], 0), "right", 0, 0);
//	AddColumnText($report, '66', "right", 0, 0);
//	SetItemValue($report, $c_row, $colind + 1, 'border_right', 1);

	AddColumnText($report, GetDisp($totals[$ind][$tag]['ip'], 0), "right", 0, 0);
//	AddColumnText($report, '66', "right", 0, 0);
//	SetItemValue($report, $c_row, $colind + 2, 'border_right', 1);

	AddColumnText($report, GetDisp($totals[$ind][$tag]['dp'], 0), "right", 0, 0);
//	AddColumnText($report, '66', "right", 0, 0);
//	SetItemValue($report, $c_row, $colind + 3, 'border_right', 1);

//	AddColumnText($report, GetDisp($totals[$ind][$tag]['closedcount'], 0), "right", 0, 0);

if ($tag == 'YTD' || $tag == '12Month')
//	AddColumnText($report, '666', "right", 0, 0);
	// RJP 7-29-05: we don't want thousands for this column, per Al Lencioni
	AddColumnText($report, GetDisp($totals[$ind][$tag]['closedcount'], 0, 0), "right", 0, 0);
else
//	AddColumnText($report, '6,666', "right", 0, 0);
	AddColumnText($report, GetDisp($totals[$ind][$tag]['closedcount'], 0), "right", 0, 0);

//	SetItemValue($report, $c_row, $colind + 4, 'border_right', 1);

//	AddColumnText($report, GetDisp($totals[$ind][$tag]['closedamount'], -1), "right", 0, 0);


if ($tag == 'YTD' || $tag == '12Month')
//	AddColumnText($report, '$66,666', "right", 0, 0);
//	AddColumnText($report, GetDisp($totals[$ind][$tag]['closedamount'], -1, 1), "right", 0, 0);
	{
		$val = $totals[$ind][$tag]['closedamount'] / 1000;
//print '<<<' . fm($val, 1) . ">>><br>";
		AddColumnText($report, fm($val, 1), "right", 0, 0);
	}

else
//	AddColumnText($report, '$6,666,666', "right", 0, 0);
//	AddColumnText($report, GetDisp($totals[$ind][$tag]['closedamount'], -1), "right", 0, 0);
	AddColumnText($report, fm($totals[$ind][$tag]['closedamount'], 1), "right", 0, 0);

//	SetItemValue($report, $c_row, $colind + 5, 'border_right', 1);


	$avg = GetDisp($totals[$ind][$tag]['avg'], 0);
	if ($avg == '0') $avg = 'NA';
	AddColumnText($report, $avg, "right", 0, 0);

//	SetItemValue($report, $c_row, $colind + 6, 'border_right', 1);

	BoxRowItems($report, $colind);

//If avg == 0, use 10000 to fool ranking program
	if (!$totals[$ind][$tag]['avg'])
		$totals[$ind][$tag]['avg'] = 10000;

}


function AddRowSection_0(&$report, &$totals, $tag, $ind)
{
	$fmcount = $totals[$ind][$tag]['fm'];
	$ipcount = $totals[$ind][$tag]['ip'];
	$dpcount = $totals[$ind][$tag]['dp'];
	$ccount = $totals[$ind][$tag]['closecount'];
	$idlist = $totals[$ind]['idlist'];

	AddColumnText($report, GetDisp($totals[$ind][$tag]['Rating'], 1), "right", 0, 0);
AddColumnText($report, "", "", 0, 0);

	AddColumnText($report, GetDisp($totals[$ind][$tag]['fm'], 0), "right", 0, 0);
//	AddColumnText($report, '66', "right", 0, 0);
AddColumnText($report, "", "", 0, 0);

	AddColumnText($report, GetDisp($totals[$ind][$tag]['ip'], 0), "right", 0, 0);
//	AddColumnText($report, '66', "right", 0, 0);
AddColumnText($report, "", "", 0, 0);

//	AddColumnText($report, GetDisp($totals[$ind][$tag]['dp'], 0), "right", 0, 0);
	AddColumnText($report, '66', "right", 0, 0);
AddColumnText($report, "", "", 0, 0);

//	AddColumnText($report, GetDisp($totals[$ind][$tag]['closedcount'], 0), "right", 0, 0);

if ($tag == 'YTD' || $tag == '12Month')
	AddColumnText($report, '666', "right", 0, 0);
//	AddColumnText($report, GetDisp($totals[$ind][$tag]['closedcount'], 0, 1), "right", 0, 0);
else
	AddColumnText($report, '6,666', "right", 0, 0);
//	AddColumnText($report, GetDisp($totals[$ind][$tag]['closedcount'], 0), "right", 0, 0);

AddColumnText($report, "", "", 0, 0);

//	AddColumnText($report, GetDisp($totals[$ind][$tag]['closedamount'], -1), "right", 0, 0);

if ($tag == 'YTD' || $tag == '12Month')
	AddColumnText($report, '66,666', "right", 0, 0);
//	AddColumnText($report, GetDisp($totals[$ind][$tag]['closedamount'], -1, 1), "right", 0, 0);
else
	AddColumnText($report, '6,666,666', "right", 0, 0);
//	AddColumnText($report, GetDisp($totals[$ind][$tag]['closedamount'], -1), "right", 0, 0);

AddColumnText($report, "", "", 0, 0);


	$avg = GetDisp($totals[$ind][$tag]['avg'], 0);
	if ($avg == '0') $avg = 'NA';
	AddColumnText($report, $avg, "right", 0, 0);

//If avg == 0, use 10000 to fool ranking program
	if (!$totals[$ind][$tag]['avg'])
		$totals[$ind][$tag]['avg'] = 10000;
}

function AddRowRankSection(&$report, &$totals, $tag, $ind)
{
	switch($tag)
	{
		case 'CurrentQuarter':
			$colind = 0;
			break;
		case 'PrevQuarter':
			$colind = 8;
			break;
		case 'YTD':
			$colind = 16;
			break;
		case '12Month':
			$colind = 24;
	}


	AddColumnText($report, $totals[$ind][$tag]['RatingRANK'], "right", 0, 0);
	AddColumnText($report, $totals[$ind][$tag]['fmRANK'], "right", 0, 0);
	AddColumnText($report, $totals[$ind][$tag]['ipRANK'], "right", 0, 0);
	AddColumnText($report, $totals[$ind][$tag]['dpRANK'], "right", 0, 0);
	AddColumnText($report, $totals[$ind][$tag]['closedcountRANK'], "right", 0, 0);
	$closedamountrank = $totals[$ind][$tag]['closedamountRANK'];
	AddColumnText($report, $closedamountrank, "right", 0, 0);
	AddColumnText($report, $totals[$ind][$tag]['avgRANK'], "right", 0, 0);

	BoxRowItems($report, $colind);

}


function AddRowValues(&$report, &$totals, $ind)
{

	AddRowSection($report, $totals, "CurrentQuarter", $ind);

	AddColumnText($report, "", "", 0, 0);
	AddRowSection($report, $totals, "PrevQuarter", $ind);
	AddColumnText($report, "", "", 0, 0);
	AddRowSection($report, $totals, "YTD", $ind);
	AddColumnText($report, "", "", 0, 0);
	AddRowSection($report, $totals, "12Month", $ind);

}

function AddRowRankValues(&$report, &$totals, $ind)
{
	AddRowRankSection($report, $totals, "CurrentQuarter", $ind);
	AddColumnText($report, "", "", 0, 0);
	AddRowRankSection($report, $totals, "PrevQuarter", $ind);
	AddColumnText($report, "", "", 0, 0);
	AddRowRankSection($report, $totals, "YTD", $ind);
	AddColumnText($report, "", "", 0, 0);
	AddRowRankSection($report, $totals, "12Month", $ind);
}

function PrintHeadings(&$report)
{
global $mpower_companyid;
	BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0);
	AddColumn($report, "", "center", 1.2, 0);
	AddColumn($report, "Current Quarter", "center", 2.3, 0);
	AddColumn($report, "Previous Quarter", "center", 2.3, 0);
	AddColumn($report, "Year To Date", "center", 2.3, 0);
	AddColumn($report, "Previous 12 Months", "center", 2.3, 0);

	BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0);
	SetRowValue($report, -1, "rotate", 1);
	SetRowColorStandard($report, -1, SHADE_MED);
	AddColumn($report, " ", "left", 1.1, 0);
	SetColColorStandard($report, -1, -1, SHADE_MED);
	$brand = GetBrand($mpower_companyid);

	for($i=0; $i<4; $i++)
	{
		AddColumn($report, "$brand Rating", "center", 			.26, 0);
		AddColumn($report, GetCatLbl("First Meeting") . "s", "center", 			.23, 0);  //.24
		AddColumn($report, GetCatLbl("Information Phase"), "center", 		.23, 0);
		AddColumn($report, GetCatLbl("Decision Point"), "center", 			.23, 0);

// RJP 7-29-05: Number Closed should NEVER be in thousands, per Al Lencioni
if($i > 1)
	AddColumn($report, "Number " . GetCatLbl("Closed"), "center", 			.5, 0);  //.5
else
	AddColumn($report, "Number " . GetCatLbl("Closed"), "center", 			.33, 0);
if($i > 1)
	AddColumn($report, "Dollar Volume " . GetCatLbl("Closed") . " (thousands)", "center", 	.5, 0); //.74
else
	AddColumn($report, "Dollar Volume ". GetCatLbl("Closed"), "center", 	.74, 0);

		AddColumn($report, "Avg Sales Cycle", "center", 			.29, 0);
		if($i<3)
		{
		//print separator
			AddColumn($report, "", "center", 0.075, 0);
			SetColColorStandard($report, -1, -1, SHADE_MED);
		}
	}
}

function PrintRankingReport(&$report, &$totals)
{
	global $mpower_companyid;
	PrintHeadings($report);
	$count = count($totals);

	$highlight = 1;
	for($i=0; $i<$count; $i++)
	{
		BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
		if($highlight) $highlight = 0;
		else $highlight = 1;

		if($highlight) 	SetRowColorStandard($report, -1, SHADE_LIGHT);
		AddColumnText($report, $totals[$i]['name'], "", 0, 0);
		AddRowValues($report, $totals, $i);
	}

	SetColumnRanks($totals);


	BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0.25, true);
	AddFreeFormText($report, 'RANKINGS', -1, "center", 2.75, 5.5);

	PrintHeadings($report);

//Now, print out the rankings for each column
	for($i=0; $i<$count; $i++)
	{
		BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
		if($highlight) $highlight = 0;
		else $highlight = 1;

		if($highlight) 	SetRowColorStandard($report, -1, SHADE_LIGHT);
		AddColumnText($report, $totals[$i]['name'], "", 0, 0);


		AddRowRankValues($report, $totals, $i);
	}
}

function GetCloseAmount($dealid)
{
	$amount = 0;
	$sql = "select ActualDollarAmount from opportunities where DealID=$dealid";
	$result = mssql_query($sql);
	if($row = mssql_fetch_array($result))
	{
		$amount = $row['ActualDollarAmount'];
	}

	return $amount;
}

function GetSalesCycle($dealid)
{
	$sql = "select * from opportunities where DealID=$dealid";

	$cycle = 0;
	$result = mssql_query($sql);
	if($row = mssql_fetch_array($result))
	{
		$first = $row['FirstMeeting'];
		$close = $row['ActualCloseDate'];

		$firstdate = strtotime($first);
		$closedate = strtotime($close);
		$cycle = GetElapsedDays($firstdate, $closedate);
	}

	return $cycle;
}

function InsertSectionVals(&$arrRow, $Section, $row)
{
//print $arrRow['name'] . ' ' . 'DP: ' . $arrRow[$Section]['dp'] . '<br>';

	$arrRow[$Section]['Rating'] += $row['Rating'];
	$arrRow[$Section]['fm'] += $row['FM'];
	$arrRow[$Section]['ip'] += $row['IP'];
	$arrRow[$Section]['dp'] += $row['DP'];
	$arrRow[$Section]['totalsched'] += $row['TotalSched'];
	$arrRow[$Section]['RemovedAvgAge'] += $row['RemovedAvgAge'];
	$arrRow[$Section]['StalledAvgAge'] += $row['StalledAvgAge'];
	$arrRow[$Section]['DPAvgAge'] += $row['DPAvgAge'];
}

function MakeSectionAvgs(&$arrRow, $Section, $count)
{

	if($count > 0)
	{
		$arrRow[$Section]['Rating'] /= $count;
		$arrRow[$Section]['fm'] /= $count;
		$arrRow[$Section]['ip'] /= $count;
		$arrRow[$Section]['dp'] /= $count;
//		$arrRow[$Section]['closedcount'] /= $count;
//		$arrRow[$Section]['closedamount'] /= $count;
//		$arrRow[$Section]['totalsched'] /= $count;
		$arrRow[$Section]['RemovedAvgAge'] /= $count;
		$arrRow[$Section]['StalledAvgAge'] /= $count;
		$arrRow[$Section]['DPAvgAge'] /= $count;
	}
	else
	{
		$arrRow[$Section]['Rating'] = '0';
		$arrRow[$Section]['fm'] = '0';
		$arrRow[$Section]['ip'] = '0';
		$arrRow[$Section]['dp'] = '0';
		$arrRow[$Section]['closedcount'] = '0';
		$arrRow[$Section]['closedamount'] = '0';
		$arrRow[$Section]['totalsched'] = '0';
		$arrRow[$Section]['RemovedAvgAge'] = '0';
		$arrRow[$Section]['StalledAvgAge'] = '0';
		$arrRow[$Section]['DPAvgAge'] = '0';
	}
}

function CalcAvgCycles(&$arrRow, $current, $prev, $ytd, $start12Month)
{

	$idlist = $arrRow['idlist'];
/*
	$sql = "select * from opportunities where DealID in";
	$sql .= " (select DealID, WhenChanged from log where TransactionID=1 and LogInt=6";
//	$sql .= " and whenchanged >= '$dateStart' and whenchanged <= '$dateEnd'";
	$sql .= " and whenchanged >= '$start12Month'";

	$sql .= " and PersonID in ($idlist))";
*/

	$sql  = "SELECT O.*, A." . $_SESSION['account_name'] . " as Company, (C." . $_SESSION['contact_first_name'] . " + ' ' + C." . $_SESSION['contact_last_name'] . ") AS Contact FROM opportunities O";
	$sql .= " LEFT JOIN Account A on O.AccountID = A.accountid";
	$sql .= " LEFT JOIN Contact C on O.ContactID = C.ContactID";
	$sql .= " WHERE (O.PersonID IN ($idlist)) ";
	$sql .= " AND (O.ActualCloseDate >= '$start12Month')";
	
	//echo $sql.'<br>';
	
	$result = mssql_query($sql);
	while($row = mssql_fetch_array($result))
	{
		// $date = strtotime($row['WhenChanged']);
		// RJP 8-4-05: Use closing date, not WhenChanged
		$date = strtotime($row['ActualCloseDate']);
		$avg=0;
		$cycledays = GetElapsedDays(strtotime($row['FirstMeeting']), strtotime($row['ActualCloseDate']));

		if($date >= $current)
		{
			$arrRow['CurrentQuarter']['closedcount'] += 1;
			$arrRow['CurrentQuarter']['days'] += $cycledays;
			$arrRow['CurrentQuarter']['closedamount'] += $row['ActualDollarAmount'];
		}
		else if($date >= $prev)
		{
			$arrRow['PrevQuarter']['closedcount'] += 1;
			$arrRow['PrevQuarter']['days'] += $cycledays;
			$arrRow['PrevQuarter']['closedamount'] += $row['ActualDollarAmount'];
		}
		if($date >= $ytd)
		{
			$arrRow['YTD']['closedcount'] += 1;
			$arrRow['YTD']['days'] += $cycledays;
			$arrRow['YTD']['closedamount'] += $row['ActualDollarAmount'];
		}
		$arrRow['12Month']['closedcount'] += 1;
		$arrRow['12Month']['days'] += $cycledays;
		$arrRow['12Month']['closedamount'] += $row['ActualDollarAmount'];
	}
	$arrSections = array("CurrentQuarter","PrevQuarter","YTD","12Month");

	for ($i=0; $i < count($arrSections); $i++)
	{
		$count=$arrRow[$arrSections[$i]]['closedcount'];
		if(!$count) continue;
		$totaldays = $arrRow[$arrSections[$i]]['days'];
		$arrRow[$arrSections[$i]]['avg'] = intval(($totaldays/$count) + 0.5);
	}
}

function GetTotals(&$totals, $i, $current, $prev, $ytd, $start12Month)
{
	$idlist = $totals[$i]['idlist'];
	if(strlen($idlist == 0)) return;
	$sql =  "SELECT SnapDate, Rating, FM, IP, DP, Closed, TotalSales, TotSched, RemovedAvgAge, StalledAvgAge, DPAvgAge";
	$sql .= " FROM snapshot";
	$sql .= " WHERE SalesID in ($idlist) AND SnapDate > '$start12Month'";

	$result = mssql_query($sql);
	$currentquarterCount=0;
	$prevquarterCount=0;
	$ytdCount=0;
	$twelvemonthCount=0;
$dCount=0;
	while($row = mssql_fetch_array($result))
	{

$dCount++;
		$date = strtotime($row['SnapDate']);
		if($date >= $current)
		{
			InsertSectionVals($totals[$i], 'CurrentQuarter',$row);
			$currentquarterCount++;
		}
		else if($date >= $prev)
		{
			InsertSectionVals($totals[$i], 'PrevQuarter', $row);
			$prevquarterCount++;
		}
		if($date >= $ytd)
		{
			InsertSectionVals($totals[$i], 'YTD', $row);
			$ytdCount++;
		}
		InsertSectionVals($totals[$i], '12Month', $row);
		$twelvemonthCount++;
	}
/*
print "cCt=$currentquarterCount<br>";
print "pCt=$prevquarterCount<br>";
print "yCt=$ytdCount<br>";
print "12Ct=$twelvemonthCount<br>";
*/


	MakeSectionAvgs($totals[$i], 'CurrentQuarter', $currentquarterCount);
	MakeSectionAvgs($totals[$i], 'PrevQuarter', $prevquarterCount);
	MakeSectionAvgs($totals[$i], 'YTD', $ytdCount);
	MakeSectionAvgs($totals[$i], '12Month', $twelvemonthCount);



//And now for the dreaded sales cycle calculations!!
	$today = time();
	CalcAvgCycles($totals[$i], $current, $prev, $ytd, $start12Month);

}
?>
