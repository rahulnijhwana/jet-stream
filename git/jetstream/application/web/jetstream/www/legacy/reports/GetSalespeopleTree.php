<?php

function GetDirectReporters($SupervisorID)
{
	$SalesIds = array();

	$sql = "select * from people where Deleted=0 and SupervisorID=$SupervisorID";
	$result = mssql_query($sql);
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$id=$row['PersonID'];
			array_push($SalesIds, $id);
		}
	}
	return $SalesIds;
}

function _GetSalespeopleTree(&$SalesIds, $SupervisorID, $FullTree)
{
	$sql = "select * from people where Deleted=0 and PersonID=$SupervisorID";
	$result = mssql_query($sql);
	if($result)
	{
		if($row = mssql_fetch_assoc($result))
		{
			$issalesperson = $row['IsSalesperson'];

			if($issalesperson)
			{
				array_push($SalesIds, $SupervisorID);
			}
		}
	}


	$sql = "select * from people where Deleted=0 and SupervisorID=$SupervisorID";
	$result = mssql_query($sql);
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$id=$row['PersonID'];

			$level = $row['Level'];
			$issalesperson = $row['IsSalesperson'];

			if($issalesperson || $FullTree)
			{
				array_push($SalesIds, $id);
			}

			if($level > 1)
			{
				_GetSalespeopleTree($SalesIds, $id, $FullTree);
			}
		}
	}
}

function GetSalespeopleTree($SupervisorID)
{
	$full = 0;
	$args = func_get_args();
	if(count($args) > 1)
		$full = $args[1];

	$SalesIds = array();
	$idlist = "";

	_GetSalespeopleTree($SalesIds, $SupervisorID, $full);

	$count = count($SalesIds);
	for($i=0; $i<$count; $i++)
	{
		if($i > 0) $idlist .= ",";
		$idlist .= $SalesIds[$i];
	}

//print("idlist=$idlist<br>");
	return $idlist;
}

function GetSalespeopleTreeArray($SupervisorID)
{
	$SalesIds = array();
	$idlist = "";

	_GetSalespeopleTree($SalesIds, $SupervisorID, 0);

	return $SalesIds;
}

function GetFullName(&$row, $reverse, $breakline)
{
	$lastname = $row['LastName'];
	$firstname = $row['FirstName'];
	$groupname = $row['GroupName'];

	$fullname = "";

	if($reverse)
	{
		$fullname = $lastname . ",";
		if($breakline) $fullname .= "\r";
		else $fullname .= " ";
		$fullname .= $firstname;
	}
	else
	{
		$fullname = $firstname;
		if($breakline) $fullname .= "\r";
		else $fullname .= " ";
		$fullname .= $lastname;
	}

	if(strlen($groupname))
	{
		if($breakline) $fullname .= "\r";
		else $fullname .= " ";
		$fullname .= "($groupname)";
	}

	return $fullname;
}

function GetSalesPersonBranch($id)
{
	// Load the current people record for the starting id and add supervisor records all the way up
	// Input: $id - starting rep id
	// Output: $arrsp - array of rep names all the way up to the top dog
	// 05/28/2013: Vern Gorman - Blue Star Technologies: (#11531) New function for CSV export
	global $mpower_companyid;

	$tmp = array();

	$a = 0;
	while ($id != '') {
		// Get the people record for the specified id
		$sql = "select FirstName, LastName, GroupName, SupervisorID from people where CompanyID = '$mpower_companyid' and PersonID = '$id'";
		$result = mssql_query($sql);
		$row = mssql_fetch_assoc($result);

		$tmp[$a] = $row['FirstName'] . ' ' . $row['LastName'];
		$gname = $row['GroupName'];
		if ($gname != null && strlen($gname)) {
			$tmp[$a] = $gname;
		}

		$a++;	// increment the level
		$id = (int) $row['SupervisorID']; // Load the supervisor id and try again
	}

	// Reverse the order of the array with the top supervisor as element zero
	$arrsp = array();

	$j=0;
	for ($i=(count($tmp)-1); $i>=0; $i--) {
		if ($tmp[$i] != null && trim($tmp[$i]) != '') {
			$arrsp[$j] = $tmp[$i];
			$j++;
		}
	}

	return $arrsp;
}

?>