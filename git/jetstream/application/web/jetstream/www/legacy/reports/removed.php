<?php
 /**
 * @package Reports
 * Removed Opportunities Report
 * Prompts for Salesrep and Date Range
 */

// Definitions --------------------------------------------------------------------------------
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_report_utils.php');
include('_export_utils.php');
include('_date_params.php');
include('TableUtils.php');
include('jim_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');

global $report_export_option;

define("TRUE",1);
define("FALSE",0);

$cellfontsize="16px";

// Inline Code -----------------------------------------------------------------------------
if(isset($_SESSION['ExportArray'])) {
	// Check for export before loading anything else.
	// The export headers need to be first.
	// If you put anything other then the export data in this condition, it will appear in the export file.
	if (isset($_GET['export'])) {
		//export_csv("removed_opp_rpt");
		export_xls("removed_opp_rpt");
		exit();
	}

	// Reset variable holding report detail for Export file
	unset($_SESSION['ExportArray']);
}

?>

<script language="JavaScript">

var old_print = window.print;

function myPrint()
{
	alert("Please make sure your printer is in landscape mode for this report.");
	old_print();
}

window.print = myPrint;

</script>

<?php

//***** Salesrep Prompt
if (!isset($treeid)) {
	begin_report(GetCatLbl("Removed") . " Opportunities Report");
	print_tree_prompt(ANYBODY);
	end_report();
	exit();
}

//***** Date Range Prompt
if (!isset($dateFrom)||!isset($dateTo)) {
	begin_report(GetCatLbl("Removed") . " Opportunities Report");
	print_date_params($treeid,$sortcol,$ord);
	if(!isset($dateFrom)) $dateFrom="N/A";
	if(!isset($dateTo)) $dateTo="N/A";
	end_report();
	exit();
}

$report_export_option = 1;
begin_report(GetCatLbl("Removed") . " Opportunities Report");
$report_export_option = 0;

	
//***** Create report
print('<table width=100% border=0><tr><td valign="top">');
print('<b>' . make_tree_path($treeid) . "</b><br>\n\n");
if ($dateFrom!=""&&$dateFrom!="N/A") print "<b>From: $dateFrom to: $dateTo</b><br>";

$result = mssql_query("select Level from people where PersonID='$treeid'");
$row = mssql_fetch_array($result);

$level = $row['Level'];

print('</td><td align="right">');

print("<table><tr><td width=20>&nbsp;</td><td><table cellspacing=2 cellpadding=5 bgcolor='ivory'>");
print("<tr bgcolor='lightgrey'><td colspan=2><b><font size=2>Last Location abbreviations:</td></tr>");
print("<tr bgcolor='lightgrey'><td><b><font size=2>" . GetCatLbl('FM') . "</td><td><font size=2>" . GetCatLbl('First Meeting') . "</td></tr>");
print("<tr bgcolor='lightgrey'><td><b><font size=2>" . GetCatLbl('IP') . "(1-6)</td><td><font size=2>" . GetCatLbl('Information Phase') . "(no. of milestones reached)</td></tr>");
print("<tr bgcolor='lightgrey'><td><b><font size=2>" . GetCatLbl('S') . GetCatLbl('IP') . "</td><td><font size=2>" . GetCatLbl('Stalled') . ' ' . GetCatLbl('Information Phase') . "</td></tr>");
print("<tr bgcolor='lightgrey'><td><b><font size=2>" . GetCatLbl('DP') . "</td><td><font size=2>" . GetCatLbl('Decision Point') . "</td></tr>");
print("<tr bgcolor='lightgrey'><td><b><font size=2>" . GetCatLbl('S') . GetCatLbl('DP') . "</td><td><font size=2>" . GetCatLbl('Stalled') . ' ' . GetCatLbl('Decision Point') . "</td></tr>");
print("<tr bgcolor='lightgrey'><td><b><font size=2>" . GetCatLbl('RM') . "</td><td><font size=2>" . GetCatLbl('Removed') . "</td></tr>");
print("</table></table><br>\n");

print('</td></tr></table>');

if(!isset($sortcol)) $sortcol=0;

if($ord==0) $sortord=SORT_ASC;
else $sortord=SORT_DESC;

$arrProducts=array();
$arrReasons=array();
$arrLog=array();

GetAllProducts($mpower_companyid, $arrProducts);
GetReasons($arrReasons,$mpower_companyid);
//GetLogData($arrLog, $mpower_companyid);

// Load column headers to the CSV file array
$arrCSV = array();
$tmpCSV = array();

// Load the column headers as themselves
$tmpCSV[0] = 'Level1';
$tmpCSV[1] = 'Level2';
$tmpCSV[2] = 'Level3';
$tmpCSV[3] = 'Level4';
$tmpCSV[4] = 'Level5';
$tmpCSV[5] = 'Level6';
$tmpCSV[6] = 'Company';
$tmpCSV[7] = 'Source';
$tmpCSV[8] = 'Offering';
$tmpCSV[9] = 'Value';
$tmpCSV[10] = 'DateRemoved';
$tmpCSV[11] = 'ReasonRemoved';
$tmpCSV[12] = 'LastLocation';
$tmpCSV[13] = 'FirstMeeting';
$tmpCSV[14] = 'DaysInCycle';
array_push($arrCSV, $tmpCSV);

if($level == 1)
{
	RemovedOps($mpower_companyid, $treeid, $sortcol, $sortord,$dateFrom, $dateTo, true, $arrProducts, $arrReasons, $arrLog);
}
else
{

	$IsSales = 0;
	$inclause = GetSalespeopleTree($treeid);
	if($inclause)
		$sql="select * from people where PersonID IN($inclause)";

//	$result = mssql_query("select * from people where PersonID='$treeid'");
	$result = mssql_query("$sql");
	if($result)
	{
		while($row = mssql_fetch_array($result))
		{
			$IsSales=$row['IsSalesperson'];
			if ($IsSales)
			{
				$id = $row['PersonID'];
				$name = $row['FirstName'] . " " . $row['LastName'];
				$group = $row['GroupName'];
				if(strlen($group) > 0) $name .= " ($group)";
				print("<br><b>$name</b><br><br>");

				RemovedOps($mpower_companyid, $id, $sortcol, $sortord,$dateFrom, $dateTo, false, $arrProducts, $arrReasons,$arrLog);
			}
		}
	}
	
	// load to session
	$_SESSION['ExportArray'] = $arrCSV;
	
}

// Functions ------------------------------------------------------------------------------------------------------------------------
function RemovedOps($idCompany, $idPerson, $sortind, $sortord, $dFrom, $dTo, $issales, $arrProducts, $arrReasons, $arrLog)
{
	global $treeid, $arrCSV;

	// Get a branch from the selected rep level up
	$salesbranch = GetSalesPersonBranch($idPerson);
	
	$arrRemoved=array();
	$count=GetRemoved($idCompany, $idPerson, $arrRemoved, $dFrom, $dTo, $issales, $arrProducts, $arrReasons, $arrLog);
	BeginTable();
//	$url = $_SERVER['PHP_SELF'] . "?treeid=$idPerson";
	$url = $_SERVER['PHP_SELF'] . "?SN=".$_GET['SN']."&treeid=$treeid";

	if($dFrom!=""&&$dFrom!="N/A")
	{
		$uFrom=urlencode($dFrom);
		$uTo=urlencode($dTo);
		$url.="&dateFrom=$uFrom&dateTo=$uTo";
	}

	//$arrColHeads=array("Line", "<A HREF=\"$url&sortcol=0\">Company</A>","<A HREF=\"$url&sortcol=1\">Opportunity</A>","<A HREF=\"$url&sortcol=2\">Value</A>","<A HREF=\"$url&sortcol=3\">Date Removed</A>", "<A HREF=\"$url&sortcol=4\">Reason Removed</A>","<A HREF=\"$url&sortcol=5\">Last Location</A>","<A HREF=\"$url&sortcol=6\">Date of " . GetCatLbl('First Meeting') . "</A>","<A HREF=\"$url&sortcol=7\">Days in Sales Cycle</A>");
	$arrColHeads=array(
		"Line",
		"Company<br>(<A HREF=\"$url&sortcol=0&ord=0\">A </A><A HREF=\"$url&sortcol=0&ord=1\">D</A>)",
		((trim($_SESSION['company_obj']['Source2Label']) != '') ?($_SESSION['company_obj']['Source2Label']) :('Source2'))."<br>(<A HREF=\"$url&sortcol=1&ord=0\">A </A><A HREF=\"$url&sortcol=1&ord=1\">D </A>)",
		"Offering<br>(<A HREF=\"$url&sortcol=2&ord=0\">A </A><A HREF=\"$url&sortcol=2&ord=1\">D </A>)",
		"Value<br>(<A HREF=\"$url&sortcol=3&ord=0\">A </A><A HREF=\"$url&sortcol=3&ord=1\">D</A>)",
		"Date Removed<br>(<A HREF=\"$url&sortcol=4&ord=0\">A </A><A HREF=\"$url&sortcol=4&ord=1\">D</A>)",
		"Reason Removed<br>(<A HREF=\"$url&sortcol=5&ord=0\">A </A><A HREF=\"$url&sortcol=5&ord=1\">D</A>)",
		"Last Location<br>(<A HREF=\"$url&sortcol=6&ord=0\">A </A><A HREF=\"$url&sortcol=6&ord=1\">D</A>)",
		"Date of " . GetCatLbl('First Meeting') . "<br>(<A HREF=\"$url&sortcol=7&ord=0\">A </A><A HREF=\"$url&sortcol=7&ord=1\">D</A>)",
		"Days in Sales Cycle<br>(<A HREF=\"$url&sortcol=8&ord=0\">A </A><A HREF=\"$url&sortcol=8&ord=1\">D</A>)"
		);


	$arrFieldNames=array("Company","Source2Name","Opportunity","Value","RemoveDate","RemoveReason","LastLoc","FirstMeeting","CycleDays");
	PrintColHeads($arrColHeads, 1, 0, count($arrColHeads),FALSE);


//	array_multisort($arrRemoved, SORT_ASC ,$arrRemoved[0]);
	$arrRemoved=array_csort($arrRemoved, $arrFieldNames[$sortind], $sortord);

	for($i=0; $i< $count; $i++)
	{
		BeginTableRowData(2);
		PrintTableCell($i+1);
		$tempcompany = $arrRemoved[$i]["Company"];
		if ($arrRemoved[$i]["Division"] != '')
			$tempcompany .= '&nbsp;-&nbsp;'.$arrRemoved[$i]["Division"];
		PrintTableCell($tempcompany);
		PrintTableCell($arrRemoved[$i]["Source2Name"]);
		PrintTableCell($arrRemoved[$i]["Opportunity"]);
		PrintTableCell(fm($arrRemoved[$i]["Value"], 1));
		PrintTableCell(date("m-d-y",$arrRemoved[$i]["RemoveDate"]));
		PrintTableCell($arrRemoved[$i]["RemoveReason"]);
		PrintTableCell($arrRemoved[$i]["LastLoc"]);
		if ($arrRemoved[$i]["FirstMeeting"]=='')
			PrintTableCell('&nbsp;');
		else PrintTableCell(date("m-d-y",$arrRemoved[$i]["FirstMeeting"]));
		PrintTableCell($arrRemoved[$i]["CycleDays"]);
		EndTableRow();

		//load array and session variable just in case they decide to create a CSV file
		SetCSVArray($arrRemoved[$i], $salesbranch, $arrCSV);
	}
	EndTable();

}

function SetCSVArray($arrDetail, $arrBranch, &$arrCSV)
{
	// Load a detail array that we will save in case they want a CSV export file.
	// Input: $arrDetail - a detail array without the rep tree
	//        $arrBranch - the rep tree to add to the detail
	// Output: $arrCSV - array of detail merged with the rep tree
	// 05/29/2013: Vern Gorman - Blue Star Technologies: (#11531) New function for CSV export

	$tmpCSV = array();

	// Load all the report detail

	// Add the sales person branch to each detail line
	$tmpCSV[0] = $arrBranch[0];
	$tmpCSV[1] = $arrBranch[1];
	$tmpCSV[2] = $arrBranch[2];
	$tmpCSV[3] = $arrBranch[3];
	$tmpCSV[4] = $arrBranch[4];
	$tmpCSV[5] = $arrBranch[5];
	$tempcompany = $arrDetail["Company"];
	if ($arrDetail["Division"] != '')
		$tempcompany .= ' - '.$arrDetail["Division"];
	$tmpCSV[6] = $tempcompany;
	$tmpCSV[7] = $arrDetail["Source2Name"];
	$tmpCSV[8] = $arrDetail["Opportunity"];
	$tmpCSV[9] = $arrDetail["Value"];
	$tmpCSV[10] = date("m-d-y",$arrDetail["RemoveDate"]);
	$tmpCSV[11] = $arrDetail["RemoveReason"];
	$tmpCSV[12] = $arrDetail["LastLoc"];
	if ($arrDetail["FirstMeeting"]=='')
	{
		$tmpCSV[13] = '';
	}
	else
	{
		$tmpCSV[13] = date("m-d-y",$arrDetail["FirstMeeting"]);
	}
	$tmpCSV[14] = $arrDetail["CycleDays"];

	array_push($arrCSV, $tmpCSV);
}

function GetAllProducts($idCompany, &$arrProducts)
{
	$sql="select * from products where deleted=0 and CompanyID = $idCompany";
	$res=mssql_query($sql);
	$arrProducts[-1]="Unknown";
	while(($row=mssql_fetch_assoc($res)))
	{
		$arrProducts[$row["ProductID"]]=$row["Name"];
	}
}

function GetReasons(&$arrReasons, $companyid)
{
	$sql="select * from removereasons where CompanyID=$companyid";
	$res=mssql_query($sql);
	while($row=mssql_fetch_assoc($res))
	{
		$arrReasons[$row["ReasonID"]]=$row["Text"];
	}
}

function MakeMilestonesString($arrMilestones)
{
	$strMS="";
	$count=count($arrMilestones);
	for($i=0;$i<$count;$i++)
	{
		if($arrMilestones[$i]>0)
			$strMS.=($i+1);
	}
	return $strMS;

}

function MakeLocationString($idLoc, $arrMilestones)
{

	switch($idLoc)
	{
		case 1:
			return GetCatLbl("FM");
		case 2:
			return GetCatLbl("IP") . MakeMilestonesString($arrMilestones);
		case 3:
			return GetCatLbl("S") . GetCatLbl("IP");
		case 4:
			return GetCatLbl("DP");
		case 5:
			return GetCatLbl("S") . GetCatLbl("DP");
		case 6:
			return GetCatLbl("C");
		case 9:
			return GetCatLbl("RM");
		case 10:
			return GetCatLbl("T");
	}

	return "$idLoc";

}

function GetLogData(&$arrLog, $clause)
{
// this is getting all deleted opps, not just the ones selected...
// not any more!

	$arrLog=array();
//	$sql="select * from log where (TransactionID=1 or (TransactionID>19 and TransactionID < 26)) and  DealID in (select dealid from opportunities where category = 9 and PersonID=$personid) order by dealid, whenchanged DESC";
	$sql="select * from log where (TransactionID=1 or (TransactionID>19 and TransactionID < 26)) and  DealID in ($clause) order by dealid, whenchanged DESC";
	$res=mssql_query($sql);
	$lastdeal=-1;
	while(($row=mssql_fetch_assoc($res)))
	{
		if($lastdeal!=$row['DealID'])
			{
				if($lastdeal > -1)
				{
					$LastCat=MakeLocationString($catLast, $milestones);
					$LastDays=$RemoveDate - $datePrev;
					$arrLog[$lastdeal]=array('LastCat' => $LastCat, 'LastDays' => $LastDays, 'RemoveDate' => $RemoveDate);
				}
				$ord=0;
				$milestones=array();
				$lastdeal = $row['DealID'];
			}
		if ($row['TransactionID']==1 && $ord<2)
		{
			if(!$ord)
				{
					$RemoveDate=$row['WhenChanged'];
				}
			if($ord==1)
				{
					$datePrev=$row['WhenChanged'];
					$catLast=$row['LogInt'];
				}
			$ord++;
		}
		if($row['TransactionID']>19 && $row['TransactionID']<26)
		{
			$ind=$row['TransactionID']-20;
			$milestones[$ind]= $row['LogBit']==1;
		}
	}
// save the last one
	if($lastdeal > -1)
	{
		$LastCat=MakeLocationString($catLast, $milestones);
		$LastDays=$RemoveDate - $datePrev;
		$arrLog[$lastdeal]=array('LastCat' => $LastCat, 'LastDays' => $LastDays, 'RemoveDate' => $RemoveDate);
	}

}


function GetOpData($DealID, &$LastCat,  //Last category before being removed
							&$LastDays, //Number of days in last category
							&$RemoveDate)
{
	$sql = "select * from log where DealID=$DealID and (TransactionID=1 or (TransactionID>19 and TransactionID < 26)) order by WhenChanged DESC";
	$res = mssql_query($sql);
	$ord=0;
	$milestones=array();
	while(($row=mssql_fetch_assoc($res)))
	{
		if(!$ord)
			{
			$RemoveDate=$row['WhenChanged'];
			}
		if($ord==1)
			{
			$datePrev=$row['WhenChanged'];
			$catLast=$row['LogInt'];
			}
		$ord++;
		if($row['TransactionID']>19 && $row['TransactionID']<26)
		{
			$ind=$row['TransactionID']-20;
			$milestones[$ind]= $row['LogBit']==1;
		}
	}
	$LastCat=MakeLocationString($catLast, $milestones);
	$LastDays=$RemoveDate - $datePrev;
}


function datediff($interval, $date1, $date2)
{
 $s = strtotime($date2)-strtotime($date1);
 $d = intval($s/86400);
 $s -= $d*86400;
 $h = intval($s/3600);
 $s -= $h*3600;
 $m = intval($s/60);
 $s -= $m*60;
 $arrRes=array("d"=>$d,"h"=>$h,"m"=>$m,"s"=>$s);
 return $arrRes[$interval];
}

// A general function for sorting a multi-dimensional array by a specific column and order
// (assuming that all rows of the array have the same indexes (columns)):


function array_csort($marray, $column, $sortord)
{

  $sortarr=array();
  foreach ($marray as $row) {
    $sortarr[] = $row[$column];
  }
  array_multisort($sortarr, $sortord, $marray);
  return $marray;
}


function GetRemoved($idCompany, $idPerson, &$arrResults, $dateFrom, $dateTo, $issales, $arrProducts, $arrReasons, $arrLog)
{
	$arrOps=array();

	$sql="from Opportunities
		left join Account A on Opportunities.AccountID = A.accountid
		left join Contact C on Opportunities.ContactID = C.ContactID
		left join Sources2 SC on Opportunities.Source2ID = SC.Source2ID
		where category=9 and PersonID=$idPerson";

	if ($dateFrom!=""&&$dateFrom!="N/A")
		$sql.=" and RemoveDate between '$dateFrom' and '$dateTo'";

	$clause='select dealid '.$sql;
	$sql="select Opportunities.*, SC.Name AS Source2Name, A." . $_SESSION['account_name'] . " as Company, (C." . $_SESSION['contact_first_name'] . " + ' ' + C." . $_SESSION['contact_last_name'] . ") AS Contact ".$sql;
	$res=mssql_query($sql);

	GetLogData(&$arrLog, $clause);

	$ind=0;
	while($row=mssql_fetch_assoc($res))
	{
//		GetOpData($row["DealID"], $LastCat, $LastDays, $RemoveDate);
		$DealID = $row['DealID'];
		$tmplog=$arrLog[$row['DealID']];

		if($tmplog['LastCat'] <0) continue;
		$RemoveDate=$tmplog['RemoveDate'];
		$tmp=array();

		$OppProducts = GetOppProducts($DealID);
		if(strlen($OppProducts) > 0) $tmp["Opportunity"]=$OppProducts;
		else $tmp["Opportunity"]=$arrProducts[$row["ProductID"]];

		$tmp["DealID"]=$row['DealID'];
		$tmp["LastLoc"]=$tmplog['LastCat'];
		$tmp["LastDays"]=$tmplog['LastDays'];
//		$tmp["RemoveDate"] = date("m-d-y",strtotime($RemoveDate));
		$tmp["RemoveDate"] = strtotime($RemoveDate);

		$tmp["Company"] = $row["Company"];
		$tmp["Division"] = $row["Division"];
		$tmp["Value"] = $row["EstimatedDollarAmount"];
		$tmp["RemoveReason"] = $arrReasons[$row["RemoveReason"]];
//		$tmp["FirstMeeting"] = date("m-d-y", strtotime($row["FirstMeeting"]));
		if ($row["FirstMeeting"])
		{
			$tmp["FirstMeeting"] = strtotime($row["FirstMeeting"]);
			$dif=DateDiff("d", $row["FirstMeeting"], $RemoveDate);
			if ($dif>0)
				$tmp["CycleDays"]=$dif;
			else $tmp["CycleDays"]='';

		}
		else
		{
			$tmp["FirstMeeting"] = '';
			$tmp["CycleDays"]='';
		}
		$tmp["Source2Name"] = $row["Source2Name"];
		$arrResults[$ind++]=$tmp;
	}
	return $ind;
}

// Closing -------------------------------------------------------------------------------------------------------
end_report();

close_db();

?>