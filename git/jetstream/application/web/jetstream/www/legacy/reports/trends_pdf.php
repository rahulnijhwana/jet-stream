<?php
 /**
 * @package Reports
 */
include('category_labels.php');

function IsTrendCurrent($SalesId, $TrendId)
{
	$sql = "select * from log where PersonID=$SalesId and TransactionID=$TrendId order by WhenChanged desc";
	$selection = mssql_query($sql);
	if($selection)
	{
		if($row = mssql_fetch_assoc($selection))
		{
			$Current = $row['LogBit'];
			$WhenChanged = strtotime($row['WhenChanged']);
			if($Current)
			{
				return $WhenChanged;
			}
		}
	}
	return null;
}

function GetRegionCurrentTrendSummary($SalesIds, $TrendId, &$CountInTrend, &$AvgDays)
{
	$CountInTrend=0;
	$totdays=0;
	$count = count($SalesIds);
	$timenow = time();
	for($i=0; $i<$count; $i++)
	{
		if($starttime=IsTrendCurrent($SalesIds[$i], $TrendId))
		{
			$totdays += GetElapsedDays($starttime, $timenow);
			$CountInTrend++;
		}
	}

	if($CountInTrend > 0)
		$AvgDays = (int)($totdays/$CountInTrend);
}

function GetSupervisors($companyid, $level, $supervisorid, &$resultarray)
{
	$resultarray = array();

	$sql="";
	if($supervisorid < 0) $sql = "select * from people where CompanyID=$companyid and Level=$level";
	else $sql = "select * from people where CompanyID=$companyid and Level=$level and SupervisorID=$supervisorid";
//print("SQL=$sql<br>");
	$result = mssql_query($sql);
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$temp = array();
			$temp['level'] = $row['Level'];
			$temp['groupname'] = $row['GroupName'];
			$temp['superid'] = $row['PersonID'];
			array_push($resultarray, $temp);
		}
	}
}

function GetSalespeople(&$SalesNames, &$SalesIds, $CompanyID, $SupervisorID)
{
	$sql = "select * from people where Deleted=0 and IsSalesperson=1 and SupervisorID";

	if(is_array($SupervisorID))
	{
		$clause = " in(";
		$count = count($SupervisorID);
		for($i=0; $i<$count; $i++)
		{
			$super = $SupervisorID[$i];
			$id = $super['superid'];
			if($i > 0) $clause .= ',';
			$clause .= $id;
		}
		$clause .= ')';
		$sql .= $clause;
	}
	else
	{
		$sql .= "=$SupervisorID";
	}

	$sql .= " order by LastName, FirstName";

	$result = mssql_query($sql);
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$Name = $row['LastName'];
			$Name .= ', ';
			$Name .= $row['FirstName'];

			$id=$row['PersonID'];
			array_push($SalesNames, $Name);
			array_push($SalesIds, $id);
		}
	}
}

function GetElapsedDays($StartTime, $EndTime)
{
	$Diff = $EndTime - $StartTime;
	return (int)($Diff / (60 * 60 * 24));
}

function GetTrends(&$TrendNames, &$TrendIds, $companyid)
{

	$req1 = 0;
	$req2 = 0;
	$name1 = "";
	$name2 = "";

	$sql = "select * from company where CompanyID=$companyid";
	$result = mssql_query($sql);
	if($result)
	{
		if($row = mssql_fetch_assoc($result))
		{
			$req1 = $row['Requirement1used'];
			$req2 = $row['Requirement2used'];
			$name1 = $row['Requirement1'];
			$name2 = $row['Requirement2'];
			$MS2Label = $row['MS2Label'];
			$MS3Label = $row['MS3Label'];
			$MS4Label = $row['MS4Label'];
			$MS5Label = $row['MS5Label'];
		}
	}


	$sql = "select * from trends_config_categories";
	$result = mssql_query($sql);
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$trendid = $row['TrendID'];
			$name = $row['Name'];
			if ($trendid < 9)
				$name = ScanForLabels($name);
			if($trendid == 9)
			{
				if(strlen($MS2Label) > 0)
					$name = "Missing \"$MS2Label\" Milestone";
			}
			if($trendid == 10)
			{
				if(strlen($MS3Label) > 0)
					$name = "Missing \"$MS3Label\" Milestone";
			}
			if($trendid == 11)
			{
				if(strlen($MS4Label) > 0)
					$name = "Missing \"$MS4Label\" Milestone";
			}
			if($trendid == 12)
			{
				if(strlen($MS5Label) > 0)
					$name = "Missing \"$MS5Label\" Milestone";
			}

			if($trendid == 13)
			{
				if($req1) $name = "Missing \"$name1\" Milestone";
				else $trendid = -1;
			}

			if($trendid == 14)
			{
				if($req2) $name = "Missing \"$name2\" Milestone";
				else $trendid = -1;
			}

			if($trendid > -1)
			{

				array_push($TrendNames, $name);
				array_push($TrendIds, $trendid);

			}
		}
	}
}

/*
function GetTrends(&$TrendNames, &$TrendIds, $companyid)
{
	$req1 = 0;
	$req2 = 0;
	$name1 = "";
	$name2 = "";

	$sql = "select * from company where CompanyID=$companyid";
	$result = mssql_query($sql);
	if($result)
	{
		if($row = mssql_fetch_assoc($result))
		{
			$req1 = $row['Requirement1used'];
			$req2 = $row['Requirement2used'];
			$name1 = $row['Requirement1'];
			$name2 = $row['Requirement2'];
		}
	}


	$sql = "select * from trends";

	$result = mssql_query($sql);
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$trendid = $row['TrendID'];
			$name = $row['Name'];

			if($trendid == 13)
			{
				if($req1) $name = "Missing \"$name1\" Milestone";
				else $trendid = -1;
			}

			if($trendid == 14)
			{
				if($req2) $name = "Missing \"$name2\" Milestone";
				else $trendid = -1;
			}

			if($trendid > -1)
			{
				array_push($TrendNames, $name);
				array_push($TrendIds, $trendid);
			}
		}
	}
}
*/

function GetSalesmanName($PersonID)
{
	$sql = "select * from people where PersonID=$PersonID";
	$selection = mssql_query($sql);
	if($selection)
	{
		if($row = mssql_fetch_assoc($selection))
		{
			$Name = $row['FirstName'];
			$Name .= " ";
			$Name .= $row['LastName'];
			return $Name;
		}
	}

	return "";
}

function GetTrendHistory($trendid, $salesid, &$resultarray)
{
	$resultarray = array();

	$sql = "select * from log where TransactionID=$trendid and PersonID=$salesid order by WhenChanged";
	$selection = mssql_query($sql);
	if($selection)
	{
		$startdate = 0;
		$enddate = 0;
		$current = 0;
		$count = 0;

		while($row = mssql_fetch_assoc($selection))
		{
			$changed = strtotime($row['WhenChanged']);
			$current = $row['LogBit'];

			if($current)
			{
				$uselast = 0;
				$startdate = $changed;
				if($enddate > 0 )
				{
					$days = GetElapsedDays($enddate, $startdate);
					if($days == 0) $uselast = 1;
				}
			}
			else
			{
				$enddate = $changed;
				$days = GetElapsedDays($startdate, $enddate);
				if($days > 0)
				{
					if($uselast)
					{
						$temp = $resultarray[$count-1];
						$temp['enddate'] = $enddate;
						$temp['days'] = $temp['days'] + $days;
						$resultarray[$count-1] = $temp;
					}
					else
					{
						$temp = array();
						$temp['startdate'] = $startdate;
						$temp['enddate'] = $enddate;
						$temp['days'] = $days;
						$temp['current'] = false;
						array_push($resultarray, $temp);
						$count++;
					}
				}
			}
		}

		if($current)
		{
			if($uselast)
			{
				$temp = $resultarray[$count-1];
				$temp['enddate'] = time();
				$temp['days'] = $temp['days'] + GetElapsedDays($startdate, time());
				$temp['current'] = true;
				$resultarray[$count-1] = $temp;
			}
			else
			{
				$temp = array();
				$temp['startdate'] = $startdate;
				$temp['enddate'] = time();
				$temp['days'] = GetElapsedDays($startdate, time());
				$temp['current'] = true;
				array_push($resultarray, $temp);
			}
		}
	}

}


function AddLocationRow(&$report, $text1, $text2, $left, $width1, $width2)
{
	BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0);
	AddFreeFormText($report, $text1, -1, "center", $left, $width1);
	AddFreeFormText($report, $text2, -1, "left", $left+$width1, $width2);
}

function GetMilestoneList($personid, &$MSList)
{
	$sql="select Requirement1,Requirement2 from Company where CompanyID=(select companyid from people where personid=$personid)";
	$res=mssql_query($sql);
	$MSList="1=Person; 2=Need; 3=Money; 4=Time";
	$count=4;
	if($res)
		{
		$row=mssql_fetch_assoc($res);
		if(!is_null($row['Requirement1']))
		{
			$r1=$row['Requirement1'];
			$MSList.="; 5=$r1";
			$count++;
		}
		if(!is_null($row['Requirement2']))
			{
			$r2=$row['Requirement2'];
			$MSList.="; 6=$r2";
			$count++;
			}
		}

	return $count;
}


function PrintLocationKey(&$report, $left, $width1, $width2, $treeid)
{
	$MSCount=GetMilestoneList($treeid, $MSList);
	$MSLabel="Information Phase ($MSList)";

	BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, .2);
	AddFreeFormText($report, "Location Abbreviations:", -1, "left", $left, 2.5);

	AddLocationRow($report, "FM", "First Meeting", $left, $width1, $width2);
//	AddLocationRow($report, "IP(1-6)", "Information Phase", $left, $width1, $width2);
	AddLocationRow($report, "IP(1-$MSCount)", "Information Phase ($MSList)", $left, $width1, $width2);

	AddLocationRow($report, "SIP", "Stalled Information Phase", $left, $width1, $width2);
	AddLocationRow($report, "DP", "Decision Point", $left, $width1, $width2);
	AddLocationRow($report, "SDP", "Stalled Decision Point", $left, $width1, $width2);
	AddLocationRow($report, "RM", "Removed", $left, $width1, $width2);
	AddLocationRow($report, "NA", "Unknown Location", $left, $width1, $width2);

}

/***************************************
function GetConsolidatedTrend($StartDate, $TrendId, &$Count, &$AvgDays, &$CurrentCount)
{
	$Count = 0;
	$AvgDays = 0;
	$CurrentCount = 0;

	$sql = "select * from log where TransactionID=$TrendId order by PersonID,WhenChanged";

	$selection = mssql_query($sql);
	if($selection)
	{
		$totdays = 0;
		$starttime;
		$LastPerson = -1;
		$Current = 0;

		while($row = mssql_fetch_assoc($selection))
		{
			$PersonID = $row['PersonID'];
			if($Current && ($PersonID != $LastPerson))
			{
				$totdays += GetElapsedDays($starttime, time());
				$Count++;
				$CurrentCount++;
			}

			$LastPerson = $PersonID;
			$Current = $row['LogBit'];
			if($Current)
			{
				$starttime = strtotime($row['WhenChanged']);
			}
			else
			{
				$endtime = strtotime($row['WhenChanged']);
				$totdays += GetElapsedDays($starttime, $endtime);
				$Count++;
			}
		}


		if($Current)
		{
			$totdays += GetElapsedDays($starttime, time());
			$Count++;
			$CurrentCount++;
		}

		if($Count > 0)
		{
			$AvgDays = $totdays / $Count;
		}
	}
}
******************************************/
?>