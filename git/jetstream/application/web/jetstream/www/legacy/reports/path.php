<?php
 /**
 * @package Reports
 */

function GetSalespeople(&$SalesNames, &$SalesIds, $CompanyID, $SupervisorID)
{
	$sql = "select * from people where IsSalesperson=1 and SupervisorID";

	if(is_array($SupervisorID))
	{
		$clause = " in(";
		$count = count($SupervisorID);
		for($i=0; $i<$count; $i++)
		{
			$super = $SupervisorID[$i];
			$id = $super['superid'];
			if($i > 0) $clause .= ',';
			$clause .= $id;
		}
		$clause .= ')';
		$sql .= $clause;
	}
	else
	{
		$sql .= "=$SupervisorID";
	}

	$sql .= " order by LastName, FirstName";

	$result = mssql_query($sql);
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$Name = $row['LastName'];
			$Name .= ', ';
			$Name .= $row['FirstName'];

			$id=$row['PersonID'];
			array_push($SalesNames, $Name);
			array_push($SalesIds, $id);
		}
	}
}

function GetElapsedDays($StartTime, $EndTime)
{
	if(!($EndTime>$StartTime))
		return 0;
	$Diff = $EndTime - $StartTime;
	return (int)($Diff / (60 * 60 * 24));
}

function GetElapsedSeconds($StartTime, $EndTime)
{
	if(!($EndTime>$StartTime))
		return 0;
	$Diff = $EndTime - $StartTime;
	return $Diff;
}

function GetSalesCycleDays($personid, $productid)
{
//print("Person=$personid Product=$productid<br>");
	if (!$productid) return "";
	if($productid < 0) return "";

	$sql = "select * from salescycles where PersonID=$personid and ProductID=$productid";
	$result = mssql_query($sql);
	if($result)
	{
		if($row = mssql_fetch_assoc($result))
		{
			return $row['SalesCycleLength'];
		}
	}

	return "";
}
?>