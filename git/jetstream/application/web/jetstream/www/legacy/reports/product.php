<?php
 /**
 * @package Reports
 * Product Report
 * Prompts for Salesrep and Date Range
 */

// Definitions --------------------------------------------------------------------------------
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_paul_utils.php');
include('_export_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');

global $report_export_option;

// Inline Code -----------------------------------------------------------------------------
if(isset($_SESSION['ExportArray'])) {
	// Check for export before loading anything else.
	// The export headers need to be first.
	// If you put anything other then the export data in this condition, it will appear in the export file.
	if (isset($_GET['export'])) {
		//export_csv("product_rpt");
		export_xls("product_rpt");
		exit();
	}

	// Reset variable holding report detail for Export file
	unset($_SESSION['ExportArray']);
}

//***** Salesrep Prompt
if (!isset($treeid))
{
	begin_report_orig('Product Report');
	print_tree_prompt(ANYBODY);
	end_report_orig();
	exit();
}

//***** Date Range Prompt
if (!isset($dateFrom) || !isset($dateTo))
{
	begin_report_orig('Product Report');
	?>
	<center style="font-family:Arial; font-size:12pt;">Please select the date span to be used for the inclusion of closed opportunities.</center>
	<?
	$sortcol=0;
	$ord=0;
	$tomorrow = date("m/d/Y", strtotime("+1 day"));
	print_date_params($treeid,$sortcol,$ord, "datefr:<$tomorrow", "dateto:<$tomorrow");
	if(!isset($dateFrom)) $dateFrom="N/A";
	if(!isset($dateTo)) $dateTo="N/A";
	end_report_orig();
	exit();
}

//***** Create report
$report_export_option = 1;
begin_report('Product Report');
$report_export_option = 0;

$idList = GetSalespeopleTree($treeid);

$productSql = "SELECT Products.ProductID, Products.Name, Products.Abbr, Opportunities.Category, COUNT(Opp_Product_XRef.ProductID) AS transCount, COUNT(Opportunities.DealID) AS TotalOpps, SUM(ROUND(val * qty, 0)) AS TotalVal 
	FROM Products 
	LEFT OUTER JOIN Opp_Product_XRef ON Products.ProductID = Opp_Product_XRef.ProductID 
	LEFT JOIN Opportunities ON Opp_Product_XRef.DealID = Opportunities.DealID 
	WHERE Opportunities.Category IS NOT null AND
	opportunities.PersonID IN ($idList) AND 
	products.companyid = $mpower_companyid AND (opportunities.category NOT IN (1,6,9,10) OR ActualCloseDate BETWEEN '$dateFrom' AND '$dateTo') 
	GROUP BY products.productid, Products.Name, Products.Abbr, Opportunities.Category 
	ORDER BY Products.Name, opportunities.category";

// echo $productSql . "<br>";

$productResult = mssql_query($productSql);

while ($row = mssql_fetch_assoc($productResult))
{
    $name_list[$row['ProductID']]['name'] = $row['Name'];
    $name_list[$row['ProductID']]['abbr'] = $row['Abbr'];
    $quantity[$row['Category']][$row['ProductID']] += $row['TotalOpps'];
    $value[$row['Category']][$row['ProductID']] += $row['TotalVal'];
}

for($phase = 2; $phase <= 6; $phase++) {
    $quantity[$phase]['total'] = (array_key_exists($phase, $quantity)) ? array_sum($quantity[$phase]) : 0;
    $value[$phase]['total'] = (array_key_exists($phase, $value)) ? array_sum($value[$phase]) : 0;
}

$activeTotalQty = $quantity[2]['total'] + $quantity[4]['total'];
$activeTotalVal = $value[2]['total'] + $value[4]['total'];
$stalledTotalQty = $quantity[3]['total'] + $quantity[5]['total'];
$stalledTotalVal = $value[3]['total'] + $value[5]['total'];

$shortdateFrom = date('m/d/y', strtotime($dateFrom));
$shortdateTo = date('m/d/y', strtotime($dateTo));
$sqlstr = "select FirstName, LastName from people where PersonID = $treeid";
$result = mssql_query($sqlstr);
$reportPerson = mssql_fetch_assoc($result);
?>

		<link rel="stylesheet" type="text/css" href="../css/report.css">
		<script src="../javascript/utils.js,sorttable2.js,filter2.js"></script>
		</td>
	</tr>
</table>
<br>
<center style="font-size:12pt; font-weight:bold;"><?=$reportPerson['FirstName']?> <?=$reportPerson['LastName']?></center>
<br>
		<center>
            <form>
			<table class="report">
				<colgroup>
					<col span=2 />
					<col span=4 style="background-color: #F2FFF2"/>
					<col span=4 style="background-color: #DBFFDB"/>
					<col span=4 style="background-color: #FFEAEA" />
					<col span=4 style="background-color: #FFBFBF" />
					<col span=4 style="background-color: #DDDDFF" />
				</colgroup>
				<tr>
                    <th rowspan=3>Filter</th>
					<th rowspan=3>Product</th>
					<th colspan=8 class="ThickLeft">Active</th>
					<th colspan=8 class="ThickLeft">Stalled</th>
					<th colspan=4 rowspan=2 class="ThickLeft">
						<?=GetCatLbl("Closed")?><br>
						<?=$shortdateFrom?> - <?=$shortdateTo?>
					</th>
				</tr>
				<tr>
					<th colspan=2 class="ThickLeft"><?php echo GetCatLbl("IP") ?></th>
					<th colspan=2 class="ThickLeft"><?php echo GetCatLbl("DP") ?></th>
					<th colspan=4 class="ThickLeft">Total</th>
					<th colspan=2 class="ThickLeft"><?php echo GetCatLbl("IPS") ?></th>
					<th colspan=2 class="ThickLeft"><?php echo GetCatLbl("DPS") ?></th>
					<th colspan=4 class="ThickLeft">Total</th>
				</tr>
				<tr>
					<th class="ThickLeft">#</th>
					<th>%</th>
					<th class="ThickLeft">#</th>
					<th>%</th>
					<th class="ThickLeft">#</th>
					<th>%</th>
					<th>$</th>
					<th>%</th>

					<th class="ThickLeft">#</th>
					<th>%</th>
					<th class="ThickLeft">#</th>
					<th>%</th>
					<th class="ThickLeft">#</th>
					<th>%</th>
					<th>$</th>
					<th>%</th>

					<th class="ThickLeft">#</th>
					<th>%</th>
					<th>$</th>
					<th>%</th>
				</tr>


				<?
                foreach ($name_list as $id => $name) {
                    $active_total_qty = $quantity[2][$id] + $quantity[4][$id];
                    $active_total_val = $value[2][$id] + $value[4][$id];
                    $stalled_total_qty = $quantity[3][$id] + $quantity[5][$id];
                    $stalled_total_val = $value[3][$id] + $value[5][$id];

					echo '<tr>';
                    echo '<td><input type="checkbox" name="filter" value="' . $name['abbr'] . '" checked onclick="FilterByCheckbox(this)"></td>';
					echo '<td class="jl">' . $name['name'] . '</td>';
					echo '<td class="ThickLeft jr">' . NullToZero($quantity[2][$id]) . '</td>';
					echo '<td class="jr">' . GetPercent($quantity[2][$id], $quantity[2]['total']) . '</td>';
					echo '<td class="ThickLeft jr">' . NullToZero($quantity[4][$id]) . '</td>';
					echo '<td class="jr">' . GetPercent($quantity[4][$id], $quantity[4]['total']) . '</td>';
					echo '<td class="ThickLeft jr">' . $active_total_qty . '</td>';
					echo '<td class="jr">' . GetPercent($active_total_qty, $activeTotalQty) . '</td>';
					echo '<td class="jr">' . CurrencyFormat($active_total_val, 0, true, true) . '</td>';
					echo '<td class="jr">' . GetPercent($active_total_val, $activeTotalVal) . '</td>';

					echo '<td class="ThickLeft jr">' . NullToZero($quantity[3][$id]) . '</td>';
					echo '<td class="jr">' . GetPercent($quantity[3][$id], $quantity[3]['total']) . '</td>';
					echo '<td class="ThickLeft jr">' . NullToZero($quantity[5][$id]) . '</td>';
					echo '<td class="jr">' . GetPercent($quantity[5][$id], $quantity[5]['total']) . '</td>';
					echo '<td class="ThickLeft jr">' . $stalled_total_qty . '</td>';
					echo '<td class="jr">' . GetPercent($stalled_total_qty, $stalledTotalQty) . '</td>';
					echo '<td class="jr">' . CurrencyFormat($stalled_total_val, 0, true, true) . '</td>';
					echo '<td class="jr">' . GetPercent($stalled_total_val, $stalledTotalVal) . '</td>';

					echo '<td class="ThickLeft jr">' . $quantity[6][$id] . '</td>';
					echo '<td class="jr">' . GetPercent($quantity[6][$id], $quantity[6]['total']) . '</td>';
					echo '<td class="jr">' . CurrencyFormat($value[6][$id] . '</td>', 0, true, true);
					echo '<td class="jr">' . GetPercent($value[6][$id], $value[6]['total']) . '</td>';
					echo '</tr>';
				}
				?>
				<tr>
					<th>&nbsp;</th>
					<th>Totals</th>
					<th class="ThickLeft jr"><?=$quantity[2]['total'] ?></th>
					<th class="jr">100.0</th>
					<th class="ThickLeft jr"><?=$quantity[4]['total'] ?></th>
					<th class="jr">100.0</th>
					<th class="ThickLeft jr"><?=$activeTotalQty ?></th>
					<th class="jr">100.0</th>
					<th class="jr"><?=CurrencyFormat($activeTotalVal, 0, true, true) ?></th>
					<th class="jr">100.0</th>

					<th class="ThickLeft jr"><?=$quantity[3]['total'] ?></th>
					<th class="jr">100.0</th>
					<th class="ThickLeft jr"><?=$quantity[5]['total'] ?></th>
					<th class="jr">100.0</th>
					<th class="ThickLeft jr"><?=$stalledTotalQty ?></th>
					<th class="jr">100.0</th>
					<th class="jr"><?=CurrencyFormat($stalledTotalVal, 0, true, true) ?></th>
					<th class="jr">100.0</th>

					<th class="ThickLeft jr"><?=$quantity[6]['total'] ?></th>
					<th class="jr">100.0</th>
					<th class="jr"><?=CurrencyFormat($value[6]['total'], 0, true, true) ?></th>
					<th class="jr">100.0</th>
				</tr>
			</table>
            </form>

			<br>

			<?

			/* $companyID = 33;

			$companySql = "select * from Company where CompanyID = $companyID";
			$companyResult = mssql_query($companySql);

			$company = mssql_fetch_assoc($companyResult);
			*/

			$oppSql = "SELECT opportunities.DealID
                	, opportunities.Division
                	, A.$account_name AS Company
                	, opportunities.Category
                	, opportunities.ActualDollarAmount
                	, opportunities.EstimatedDollarAmount
                	, opportunities.FirstMeeting
                	, people.FirstName
                	, people.LastName
                	, people.PersonID
                	, Sources2.Name AS SourceName
                	, Sources2.Abbr AS SourceAbbr
                	, Valuations.VLevel AS ValLevel
                	, Valuations.Label AS ValLabel 
                FROM opportunities 
                	LEFT JOIN Account A ON opportunities.AccountID = A.accountid
                	LEFT JOIN people ON opportunities.PersonID = people.PersonID 
                	LEFT OUTER JOIN Sources2 ON opportunities.Source2ID = Sources2.Source2ID 
                	LEFT OUTER JOIN Valuations ON Opportunities.CompanyID = Valuations.CompanyID 
                		AND Opportunities.VLevel = Valuations.VLevel 
                WHERE opportunities.PersonID IN ($idList) 
                	AND (opportunities.Category NOT IN (1,6,9,10) 
               		OR (opportunities.Category = 6 
               		AND ActualCloseDate BETWEEN '$dateFrom' AND '$dateTo')) 
                ORDER BY SourceAbbr";

				
			// echo $oppSql . '<br>';

			$oppResult = mssql_query($oppSql);

			?>
			<table class="sortable" id="tableMain">
				<thead>
					<tr>
						<th><?=$source2label?></th>
						<th>Salesperson</th>
						<th>Company</th>
						<th>Val</th>
						<th>Offerings</th>
						<th>Value</th>
						<th><?=GetCatLbl("FM")?> Date</th>
						<th>Loc</th>
					</tr>
				</thead>
				<tbody>
					<?
					$arrCSV = array();
					$tmpCSV = array();
					
					// Load the column headers as themselves
					$tmpCSV[0] = 'Level1';
					$tmpCSV[1] = 'Level2';
					$tmpCSV[2] = 'Level3';
					$tmpCSV[3] = 'Level4';
					$tmpCSV[4] = 'Level5';
					$tmpCSV[5] = 'Level6';
					$tmpCSV[6] = 'Source';
					$tmpCSV[7] = 'Company';
					$tmpCSV[8] = 'Val';
					$tmpCSV[9] = 'Offering';
					$tmpCSV[10] = 'Value';
					$tmpCSV[11] = 'FMDate';
					$tmpCSV[12] = 'Location';
					array_push($arrCSV, $tmpCSV);
					
					$locabbr = array("", "FM", "IP", "IPS", "DP", "DPS", "C", "", "", "RM", "T");
					while ($row = mssql_fetch_assoc($oppResult)) {

						$prodSQL = "SELECT Products.Abbr, Opp_Product_XRef.val, Opp_Product_XRef.qty 
							FROM Opp_Product_XRef, Products 
							WHERE Opp_Product_XRef.DealID = ".$row['DealID']." 
								AND Opp_Product_XRef.ProductID = Products.ProductID 
							ORDER BY Products.Name";
						$prodResult = mssql_query($prodSQL);
						$offerings = "";
                        $value = 0;
  						while ($prodRow = mssql_fetch_assoc($prodResult)) {
							$offerings .= $prodRow['Abbr'] . ' ';
                            $value += $prodRow['qty'] * $prodRow['val'];
						}
						$val = ($row["ValLevel"] == "" || $row["ValLevel"] < 1) ? "" : $row["ValLevel"];
						$companyText = ($row["Division"] != '') ? $row["Company"] . ' - ' . $row["Division"] : $row["Company"];

                        if ($row["Category"] == 6) {
                           $value = $row["ActualDollarAmount"];
                        }

						// $value = ($row["Category"] == 6) ? $row["ActualDollarAmount"] : $row["EstimatedDollarAmount"];

						if (mssql_num_rows($prodResult) > 0) {
							echo '<tr>';
							echo '<td title="' . $row["SourceName"] . '">' . $row["SourceAbbr"] . '</td>';
							echo '<td class="jl">' . $row["LastName"] . ', ' . $row["FirstName"] . '</td>';
							echo '<td class="jl">' . $companyText . '</td>';
							echo '<td title="' . $row["ValLabel"] . '">' . $val . '</td>';
							echo '<td>' . $offerings . '</td>';
							echo '<td class="jr">' . CurrencyFormat($value, 0, true, true) . '</td>';
							echo '<td class="jr">' . date("m/d/y", strtotime($row["FirstMeeting"])) . '</td>';
							echo '<td>' . GetCatLbl($locabbr[$row["Category"]]) . '</td>';
							echo '</tr>';

													// Get a branch from the selected rep level up
							$salesbranch = GetSalesPersonBranch($row["PersonID"]);
							
							// Add the sales person branch to each detail line
							$tmpCSV = array();
							$tmpCSV[0] = $salesbranch[0];
							$tmpCSV[1] = $salesbranch[1];
							$tmpCSV[2] = $salesbranch[2];
							$tmpCSV[3] = $salesbranch[3];
							$tmpCSV[4] = $salesbranch[4];
							$tmpCSV[5] = $salesbranch[5];
							$tmpCSV[6] = $row["SourceAbbr"];
							$tmpCSV[7] = $companyText;
							$tmpCSV[8] = $val;
							$tmpCSV[9] = $offerings;
							$tmpCSV[10] = CurrencyFormat($value, 0, true, true);
							$tmpCSV[11] = date("m/d/y", strtotime($row["FirstMeeting"]));
							$tmpCSV[12] = GetCatLbl($locabbr[$row["Category"]]);
							array_push($arrCSV, $tmpCSV);
						}
					}

					// load to session
					$_SESSION['ExportArray'] = $arrCSV;

					?>
				</tbody>
                <tfoot>
                    <tr>
                        <td colspan=5 class="jr">Active Total</td>
                        <td class="jr"></td>
                        <td colspan=2></td>
                    </tr>
                    <tr>
                        <td colspan=5 class="jr">Stalled Total</td>
                        <td class="jr"></td>
                        <td colspan=2></td>
                    </tr>
                    <tr>
                        <td colspan=5 class="jr">Closed Total</td>
                        <td class="jr"></td>
                        <td colspan=2></td>
                    </tr>
                    <tr>
                        <td colspan=5 class="jr">Grand Total</td>
                        <td class="jr"></td>
                        <td colspan=2></td>
                    </tr>
                </tfoot>
			</table>
		</center>
        <script>
        
            function GetPhaseType(phase_name) {
            	
				var ip_lbl = "<?php echo GetCatLbl('IP') ?>".replace(/^\s+|\s+$/g,"");
				var dp_lbl = "<?php echo GetCatLbl('DP') ?>".replace(/^\s+|\s+$/g,"");
				var ips_lbl = "<?php echo GetCatLbl('IPS') ?>".replace(/^\s+|\s+$/g,"");
				var dps_lbl = "<?php echo GetCatLbl('DPS') ?>".replace(/^\s+|\s+$/g,"");
				var c_lbl = "<?php echo GetCatLbl('C') ?>".replace(/^\s+|\s+$/g,"");
				phase_name = phase_name.replace(/^\s+|\s+$/g,"");
				
				switch (phase_name)
                {
                    case ip_lbl:
                    case dp_lbl:
                        return 'active';
                    case ips_lbl:
                    case dps_lbl:
                        return 'stalled';
                    case c_lbl:
                        return 'closed';
                }
            }

           SumTable('tableMain');
        </script>

	</body>
</html>

<?php

// Functions -----------------------------------------------------------------------------
function GetPercent($numerator, $denominator) {
    $output = ($denominator != 0) ? CurrencyFormat((round($numerator/$denominator * 1000)/10), 1) : '-';    
    return $output;
}

function NullToZero($value) {
    $output = (is_null($value)) ? 0 : $value;
    return $output;
}

?>

