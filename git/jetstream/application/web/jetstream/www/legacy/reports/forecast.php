<?php
 /**
 * @package Reports
 */

// Definitions -----------------------------------------------------------------------------
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_report_utils.php');
include('_export_utils.php');

// Inline Code -----------------------------------------------------------------------------

if(isset($_SESSION['ExportArray'])) {
	// Check for export before loading anything else.
	// The export headers need to be first.
	// If you put anything other then the export data in this condition, it will appear in the export file.
	if (isset($_GET['export'])) {
		//export_csv("forecast_rpt");
		export_xls("forecast_rpt");
		exit();
	}

	// Reset variable holding report detail for Export file
	unset($_SESSION['ExportArray']);
}

/*
//reenable when done
if (isset($summary) && $summary == 1)
{
	$summary = true;
	begin_report('Forecast Summary Report');
}
else
{
	$summary = false;
	begin_report('Forecast Detail Report');
}
*/

begin_report('Sales Performance Indicator');

if (!isset($treeid))
{
/*
	if (isset($summary) && $summary == 1)
		print_tree_prompt(SALESPEOPLE_MGR);
	else
		print_tree_prompt(SALESPEOPLE);
*/
	$href = 'forecast_pdf.php?SN='.$_GET['SN'].'&ctime='.strtotime("now");
	print_tree_prompt(ANYBODY, $href);
	end_report();
	exit();
}

print("<meta http-equiv=\"Refresh\" content=\"1; URL=forecast_pdf.php?SN=".$_REQUEST['SN']."&treeid=$treeid&ctime=".strtotime("now")."\">");


/*
if(isset($summary) && $summary==true)
	print("<meta http-equiv=\"Refresh\" content=\"10; URL=forecast_pdf.php?treeid=$treeid&summary=$summary\">");
else
	print("<meta http-equiv=\"Refresh\" content=\"10; URL=forecast_pdf.php?treeid=$treeid\">");
*/

print('<br><br><b>     <div align="center"><span>Please wait while your report is being prepared.</span><br /><span><img src="../images/indicator.gif" alt="Loading..." /></span></div>');
//print('<br><br><b>     Please wait while your report is being prepared.<br><br><iframe style="display:none;" src="/develop/reports/sample.pdf"></iframe>');
end_report();
?>
