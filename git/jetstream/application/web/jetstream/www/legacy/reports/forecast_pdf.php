<?php
 /*
 * @package Reports
 * Performance Indicator Report
 * Prompts for Salesrep 
 */

// Definitions -----------------------------------------------------------------------------
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_report_utils.php');
include('_export_utils.php');
include('jim_utils.php');
include('ci_pdf_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');

define(SHADE_MED, "MED_BLUE");
define(SHADE_LIGHT, "LIGHT_BLUE");

Global $report_export_option;

// Functions -----------------------------------------------------------------------------
function GetElapsedDays($StartTime, $EndTime)
{
	$Diff = $EndTime - $StartTime;
	return (int)($Diff / (60 * 60 * 24));
}

function GetLevel($personid)
{
	$sql = "SELECT level 
		FROM people 
		WHERE PersonID=$personid";

	$result = mssql_query($sql);
	if ($result) {
		if($row = mssql_fetch_object($result))
			return $row->level;
	}
	return -1;
}

function GetMonth($date)
{
	$str = date("m", $date);
	return (int)$str;
}

function GetYear($date)
{
	$str = date("Y", $date);
	return (int)$str;
}

function GetStartCurrentQuarter()
{
	$today = time();
	$month = GetMonth($today);
	$year = GetYear($today);
	$startmonth;
	if($month < 4) $startmonth = 1;
	else if ($month < 7) $startmonth = 4;
	else if ($month < 10) $startmonth = 7;
	else $startmonth = 10;
	$startdate = "$month-1-$year";

	return $startdate;
}

function GetClosedCurrentQuarter($salesid)
{
	$closed = array();
	$startdate = GetStartCurrentQuarter();
	$sql = "SELECT O.*, A." . $_SESSION['account_name'] . "  AS Company, (C." . $_SESSION['contact_first_name'] . " + ' ' + C." . $_SESSION['contact_last_name'] . ") AS Contact
		FROM opportunities O
		LEFT JOIN Account A ON O.AccountID = A.accountid
		LEFT JOIN Contact C ON O.ContactID = C.ContactID
		WHERE O.PersonID IN ($salesid) AND O.ActualCloseDate>='$startdate'";

	$result = mssql_query($sql);
	if ($result) {
		while($row = mssql_fetch_object($result)) {
			$product = $row->ProductID;
			$amount = $row->ActualDollarAmount;
			$baseamount = 0;
			$tag = "$product";
			if(isset($closed[$tag]))
				$baseamount = $closed[$tag];
			$closed[$tag] = $baseamount + $amount;
		}
	}

	return $closed;
}

function GetDefaultRatios($CompanyID, &$IPRatio, &$DPRatio)
{
	$IPRatio = 0;
	$DPRatio = 0;

	$sql = "SELECT * 
		FROM company 
		WHERE CompanyID=$CompanyID";
	$result = mssql_query($sql);
	if($result) 	{
		if($row = mssql_fetch_object($result)) {
			$IPRatio = $row->IPCloseRatioDefault / 100;
			$DPRatio = $row->DPCloseRatioDefault / 100;
		}
	}
}

function GetProducts(&$Products, $CompanyID)
{
	$Products = array();
	$sql = "SELECT * 
		FROM products 
		WHERE CompanyID=$CompanyID AND Deleted=0 
		ORDER BY Name";
	$result = mssql_query($sql);
	if($result) {
		while($row = mssql_fetch_assoc($result)) {
			$prod=array();
			$id=$row['ProductID'];
			$name=$row['Name'];
			$def = $row['DefaultSalesCycle'];
			$deleted = $row['Deleted'];

			$prod['ProductID'] = $id;
			$prod['Name'] = $name;
			$prod['DefCycle'] = $def;
			array_push($Products, $prod);
		}
	}
}

function GetSalesCycle($personid, $productid)
{
	if(FALSE==strstr($personid,',')) {
		$sql="SELECT * 
			FROM salescycles 
			WHERE PersonID=$personid AND productid=$productid";
	}
	else {
		$sql="SELECT * 
			FROM salescycles 
			WHERE PersonID IN ($personid) AND productid=$productid";
	}
	$result=mssql_query($sql);
	if($result) {
		$row=mssql_fetch_object($result);
		if($row) return $row->SalesCycleLength;
	}
}

function GetOpenOpportunities(&$Opportunities, $SalesmanID, &$Cycles, &$Errors, $issales)
{
	$Opportunities = array();
	$today = time();

	if($issales) {
		$sql = "SELECT O.*, A." . $_SESSION['account_name'] . "  AS Company, (C." . $_SESSION['contact_first_name'] . " + ' ' + C." . $_SESSION['contact_last_name'] . ") AS Contact
			FROM opportunities O
			LEFT JOIN Account A ON O.AccountID = A.accountid
			LEFT JOIN Contact C ON O.ContactID = C.ContactID
			WHERE O.PersonID=$SalesmanID AND (O.Category=2 OR O.Category=4) 
			ORDER BY O.ProductID";
	}
	else {
		$inclause = GetSalespeopleTree($SalesmanID);
		if(strlen($inclause == 0)) return;
		$sql = "SELECT O.*, A." . $_SESSION['account_name'] . "  AS Company, (C." . $_SESSION['contact_first_name'] . " + ' ' + C." . $_SESSION['contact_last_name'] . ") AS Contact
			FROM opportunities O
			LEFT JOIN Account A ON O.AccountID = A.accountid
			LEFT JOIN Contact C ON O.ContactID = C.ContactID
			WHERE O.PersonID IN ($inclause) AND (O.Category=2 OR O.Category=4) 
			ORDER BY O.ProductID";
	}

	$result = mssql_query($sql);
	if($result) {
		while($row = mssql_fetch_assoc($result)) {
			$firstmeeting = strtotime($row['FirstMeeting']);

			$value = $row['EstimatedDollarAmount'];
			$product = $row['ProductID'];
			$elapseddays = GetElapsedDays($firstmeeting, $today);
			$category = $row['Category'];
			$foreclosedate = $row['ForecastCloseDate'];
			$adjclosedate = $row['AdjustedCloseDate'];
			$company = $row['Company'];

			if($product < 0) continue;
			if($value == 0) continue;

			$closedate;
			if(isset($adjclosedate)) {
				$closedate = $adjclosedate;
			}
			else if(isset($foreclosedate)) {
				$closedate = $foreclosedate;
			}
			else {
				$personid=$row['PersonID'];
//				$code = "$SalesmanID:$product";
				$code = "$personid:$product";
				$defaultdays = $Cycles[$code];
				$closedate = $firstmeeting + ($defaultdays * 60 * 60 * 24);
			}

			$forecastdays = GetElapsedDays($firstmeeting, $closedate);
			$days = $forecastdays - $elapseddays;
//print("first mtg: $firstmeeting closedate: $closedate  forecastdays: $forecastdays elapseddays: $elapseddays days: $days<br>");

			if($days < 0) {
				$err = $company;
				array_push($Errors, $err);
				continue;
			}

			$op = array();
			$op['Value'] = $value;
			$op['ProductID'] = $product;
			$op['Days'] = $days;
			$op['Used'] = 0;
			$op['Category'] = $category;
			$op['Company'] = $company;
			array_push($Opportunities, $op);
		}
	}
}

function GetCloseRatios($SalesID, &$InfoCount, &$DecisionCount, &$CloseCount)
{
	$InfoCount=0;
	$DecisionCount=0;
	$CloseCount=0;
//	$sql = "select * from log where PersonID=$SalesID and TransactionID=1";
	$sql = "SELECT * 
		FROM log 
		WHERE PersonID IN ($SalesID) AND TransactionID=1";

	$sql .= " ORDER BY DealID, WhenChanged";

	$result = mssql_query($sql);
	if($result) {
		$lastdeal=-1;
		$info=0;
		$decision=0;
		$close=0;
		$lastcat = -1;

		while($row = mssql_fetch_object($result)) {
			$dealid = $row->DealID;
			$category = $row->LogInt;
			if($dealid != $lastdeal) {
				if($lastcat == 6) {
					$CloseCount++;
					$InfoCount++;
					$DecisionCount++;
				}

				else if($lastcat == 9) {
					if($info>0) $InfoCount++;
					if($decision>0) $DecisionCount++;
				}

				$lastdeal = $dealid;
				$info=0;
				$decision=0;
				$close=0;
			}

			if($category == 2)
				$info++;

			if($category == 4) {
				$info++;
				$decision++;
			}

			$lastcat = $category;

		}

		if($lastcat == 6 || $lastcat == 9) {
			if($info > 0) $InfoCount++;
			if($decision > 0) $DecisionCount++;
		}
	}
}

function ReadSalesmanRow(&$row, &$Errors, &$Cycles, $issales)
{
	$sales=array();
	$id=$row['PersonID'];
	$name=$row['FirstName'];
	$name .= ' ';
	$name .= $row['LastName'];
	$sales['PersonID'] = $id;
	if(!$issales) {
		$group = $row['GroupName'];
		if(strlen($group)) $name .= " ($group)";
	}
	//print "Name: $name<br>";
	$sales['Name'] = $name;

	$Opportunities = array();
	GetOpenOpportunities($Opportunities, $id, $Cycles, $Errors, $issales);
	$sales['Opportunities'] = $Opportunities;

	$InfoCount=0;
	$DecisionCount=0;
	$CloseCount=0;
	GetCloseRatios($id, $InfoCount, $DecisionCount, $CloseCount);
	$sales['InfoCount'] = $InfoCount;
	$sales['DecisionCount'] = $DecisionCount;
	$sales['CloseCount'] = $CloseCount;
	$closed = GetClosedCurrentQuarter($id);
	$sales['Closed'] = $closed;

	return $sales;
}


function GetSalesmen(&$Salesmen, $supervisorid, &$Cycles, &$Errors)
{
	$sql = "SELECT * 
		FROM people 
		WHERE PersonID=$supervisorid AND Deleted=0";
	$result = mssql_query($sql);
	if($result) {
		if($row = mssql_fetch_assoc($result)) {
			$sales = ReadSalesmanRow($row, $Errors, $Cycles, 0);
			array_push($Salesmen, $sales);
		}
	}

	$sql = "SELECT * 
		FROM people 
		WHERE SupervisorID=$supervisorid AND Level>1 AND Deleted=0";
	$sql .= " ORDER BY LastName";
	$result = mssql_query($sql);
	if($result) {
		while($row = mssql_fetch_assoc($result)) {
			$sales = ReadSalesmanRow($row, $Errors, $Cycles, 0);
			array_push($Salesmen, $sales);
		}
	}

	$sql = "SELECT * 
		FROM people 
		WHERE SupervisorID=$supervisorid AND IsSalesperson=1 AND Deleted=0";
	$sql .= " ORDER BY LastName";
	$result = mssql_query($sql);
	if($result) {
		while($row = mssql_fetch_assoc($result)) {
			$sales = ReadSalesmanRow($row, $Errors, $Cycles, 1);
			array_push($Salesmen, $sales);
		}
	}
}


function GetSingleSalesman(&$Salesmen, $personid, &$Cycles, &$Errors)
{
	$sql = "SELECT * 
		FROM people 
		WHERE PersonID=$personid";

	$result = mssql_query($sql);
	if($result) {
		if($row = mssql_fetch_assoc($result)) {
			$sales = ReadSalesmanRow($row, $Errors, $Cycles, true);
			array_push($Salesmen, $sales);
		}
	}
}

function CalcDays(&$Opportunities, $ProductID, $colcount, $IPRatio, $DPRatio, &$arrAnswers, $ind, $amtclosed, $amtopen, $amttotal)
{
	$tmp = array();
	for($i=0; $i<$colcount; $i++) {
		$totval = 0;

		$opcount = count($Opportunities);
		$maxdays = ($i+1) * 30;
		$mindays=$i*30;
		for($j=0; $j<$opcount; $j++) {
			$op = $Opportunities[$j];
			$prodid = $op['ProductID'];
			$value = $op['Value'];
			$days = $op['Days'];
			$category = $op['Category'];

			if($prodid != $ProductID) continue;
			if($days > $maxdays) continue;
			if($days < $mindays) continue;

			if($category == 2) {
				$value *= $IPRatio;
			}
			else if($category == 4) {
				$value *= $DPRatio;
			}
			else {
				$value = 0;
			}
			$totval += (int)$value;
			$Opportunities[$j] = $op;
		}
//		AddColumnText($report, number_format(round(k($totval), 1), 1), "right", 0, 0);
		$strInd = $maxdays . 'days';
		$tmp[$i] = $totval;

	}
	$tmp[$colcount] = $amtclosed;
	$tmp[$colcount+1] = $amtopen;
	$tmp[$colcount+2] = $amttotal;

	$arrAnswers[$ind] = $tmp;
}

function PrintDays(&$report, $arrRow, $colcount)
{
	for ($i=0; $i < count($arrRow); $i++) {
		if($i < $colcount) {
			AddColumnText($report, fm(number_format(round(k($arrRow[$i]), 1), 1), -1), "right", 0, 0);
		}
		else {
			AddColumnText($report, fm(round(k($arrRow[$i])), 1), "right", 0, 0);
		}
	}
}

/*
function PrintDays(&$report, &$Opportunities, $ProductID, $colcount, $IPRatio, $DPRatio)
{
	for($i=0; $i<$colcount; $i++) {
		$totval = 0;

		$opcount = count($Opportunities);
		$maxdays = ($i+1) * 30;
		$mindays=$i*30;
		for($j=0; $j<$opcount; $j++) {
			$op = $Opportunities[$j];
			$prodid = $op['ProductID'];
			$value = $op['Value'];
			$days = $op['Days'];
			$category = $op['Category'];

			if($prodid != $ProductID) continue;
			if($days > $maxdays) continue;
			if($days < $mindays) continue;

			if($category == 2) $value *= $IPRatio;
			else if($category == 4) $value *= $DPRatio;
			else $value = 0;
			$totval += (int)$value;
			$Opportunities[$j] = $op;
		}
		AddColumnText($report, number_format(round(k($totval), 1), 1), "right", 0, 0);
	}
}*/


function PrintDays_dummy(&$report, &$Opportunities, $ProductID, $colcount, $IPRatio, $DPRatio)
{
	for($i=0; $i<$colcount; $i++) {
		AddColumnText($report, '9,999.9', "right", 0, 0);
	}
}

function CalcRatio($total, $closed, $defaultratio)
{
	$makeup=0;
	if($total < 30) $makeup = 30 - $total;

	$total += $makeup;
	$closed += $makeup * $defaultratio;
	$ratio = $closed / $total;
	return $ratio;
}

function TotalOpenOpportunities($ProductID, &$Opportunities)
{
	$opcount = count($Opportunities);
	$total = 0;
	for($i=0; $i<$opcount; $i++) {
		$op = $Opportunities[$i];
		$prodid = $op['ProductID'];
		if($prodid == $ProductID) {
			$value = $op['Value'];
			$total += $value;
		}
	}
	return $total;
}

function MakeBreaks($name)
{
	$name = str_replace('/', ' / ', $name);
	$name = str_replace('-', ' - ', $name);
	$name = str_replace(':', ': ', $name);
	return $name;
}

function BoxRowItems(&$report, $startcol, $ccount, $top)
{
	$c_row = $report['currentrow'];
	for ($i=0; $i < $ccount; $i++) {
		$colind = $startcol + $i;
		SetItemValue($report, $c_row, $colind, 'border_right', 1);
		SetItemValue($report, $c_row, $colind, 'border_left', 1);
		if($top) {
			SetItemValue($report, $c_row, $colind, 'border_bottom', 1);
			SetItemValue($report, $c_row, $colind, 'border_top', 1);
		}
	}
}

function k($val)
{
	return $val / 1000;
}

function SumTotals($arrAnswers, &$arrTotals)
{
	for ($i=0; $i < count($arrAnswers); $i++) {
		$row = $arrAnswers[$i];
		for($j=0; $j < count($row); $j++) {
			$arrTotals[$j] += $row[$j];
		}
	}
	return $arrTotals;
}


function PrintSalesmanNumbers(&$report, $level, &$sales, $colcount, &$Products, $IPDefault, $DPDefault, &$arrCSV)
{
	$sname = $sales['Name'];
	$id = $sales['PersonID'];
	$InfoCount = $sales['InfoCount'];
	$DecisionCount = $sales['DecisionCount'];
	$CloseCount = $sales['CloseCount'];

	$IPRatio = CalcRatio($InfoCount, $CloseCount, $IPDefault);
	$DPRatio = CalcRatio($DecisionCount, $CloseCount, $DPDefault);

	$Opportunities = $sales['Opportunities'];
	$closed = $sales['Closed'];

	$prodcount = count($Products);

	$arrAnswers=array();

	for($i=0; $i<$prodcount; $i++) {
		$prod = $Products[$i];
		$pid = $prod['ProductID'];
		$tag = "$id";
		$closedamount = 0;
		if(isset($closed[$tag])) {
			$closedamount = $closed[$tag];
		}

		$openamount = TotalOpenOpportunities($pid, $Opportunities);

		CalcDays($Opportunities, $pid, $colcount, $IPRatio, $DPRatio, $arrAnswers, $i, $closedamount, $openamount, $closedamount+$openamount);
	}

	$totals=array(count($arrAnswers[0]));
	SumTotals($arrAnswers, $totals);
	BeginNewRow($report, RT_COL_TEXT_BOLD, NORMAL_FONTSIZE, 0);
	SetRowColorStandard($report, -1, SHADE_MED);
	$hilight=0;
	if ($level>1) {
		AddColumnText($report, $sname, "", 0, 0);
	}
	$hilight=1;
	AddColumnText($report, "Totals:", "center", 0,0);
	$tcount = count($totals);
	for ($i=0; $i < $tcount; $i++) {
		if ($i < $tcount - 3) {
			AddColumnText($report, number_format(round(k($totals[$i]),1),1), "right", 0, 0);
		}
		else {
			AddColumnText($report, number_format(round(k($totals[$i]))), "right", 0, 0);
		}
	}
	BoxRowItems($report, 0, $colcount+5, 1);

	for($i=0; $i<$prodcount; $i++) {
		$prod = $Products[$i];
		$name = $prod['Name'];
		BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
		if($hilight) SetRowColorStandard($report, -1, SHADE_LIGHT);
		if($hilight) $hilight=0;
		else $hilight=1;

		if($level>1) AddColumnText($report, " ", "", 0, 0);

		AddColumnText($report, MakeBreaks($name), "left", 0, 0);

		PrintDays($report, $arrAnswers[$i], $colcount);
		$startcol= $level > 1 ? 1 : 2;
		BoxRowItems($report, 0, $colcount+5, 0);

		// Get a branch from the selected rep level up for the CSV array
		$salesbranch = GetSalesPersonBranch($id); // from GetSalespeopleTree.php

		// Fill a temp variable and load the CSV array 
		$tmpCSV = Array();
		$tmpCSV[0] = $salesbranch[0];
		$tmpCSV[1] = $salesbranch[1];
		$tmpCSV[2] = $salesbranch[2];
		$tmpCSV[3] = $salesbranch[3];
		$tmpCSV[4] = $salesbranch[4];
		$tmpCSV[5] = $salesbranch[5];
		$tmpCSV[6] = $name;
		$a=0;
		for ($i=0; $i<count($arrAnswers); $i++) {
			if($i < $colcount) {
				$tmpCSV[$a+7] = $arrAnswers[$a];
				$a++;
			}
		}
		$tmpCSV[$a+7] = $arrAnswers[$a];
		$a++;
		$tmpCSV[$a+7] = $arrAnswers[$a];
		$a++;
		$tmpCSV[$a+7] = $arrAnswers[$a];
		array_push($arrCSV, $tmpCSV);
	}
}

function PrintColTitles(&$report, $firstcol, $secondcol, $maxdays)
{
	BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0);
	SetRowValue($report, -1, "rotate", 1);
	if($firstcol) {
		AddColumn($report, "$firstcol", "", 1.0, 0);
	}
	AddColumn($report, "$secondcol", "center", 1.0, 0);

	SetRowColorStandard($report, -1, SHADE_MED);
	SetColColorStandard($report, -1, -1, SHADE_MED);
	$colcount=0;
	for($i=30; $i<=$maxdays; $i+=30) {
		AddColumn($report, "$i Day", "center", .4, 0);
		$colcount++;
	}
	AddColumn($report, "This Quarter ".GetCatLbl("Closed"), "", .5, 0);
	AddColumn($report, "This Quarter Open", "", .5, 0);
	AddColumn($report, "This Quarter Total", "", .5, 0);
}

/*
function AddLocationRow(&$report, $text1, $text2, $left, $width1, $width2)
{
	BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0);
	AddFreeFormText($report, $text1, -1, "center", $left, $width1);
	AddFreeFormText($report, $text2, -1, "left", $left+$width1, $width2);
}
*/

function GetSalesCycles(&$Cycles, $CompanyID, &$maxdays)
{
	$maxdays = 0;
	$Cycles = array();
	$sql = "SELECT * 
		FROM salescycles 
		WHERE CompanyID = $CompanyID 
		ORDER BY PersonID, ProductID";
	$result = mssql_query($sql);
	if($result) {
		while($row = mssql_fetch_assoc($result)) {
			$person = $row['PersonID'];
			$product = $row['ProductID'];
			$days = $row['SalesCycleLength'];
			if($days > $maxdays) $maxdays = $days;
			$code = "$person:$product";

			$Cycles[$code] = $days;
		}
	}
}

function GetMaxDays($companyid)
{
	$sql="SELECT Max(SalesCycleLength) AS MaxDays 
		FROM SalesCycles 
		WHERE CompanyID=$companyid 
		GROUP BY CompanyID";
	$result=mssql_query($sql);
	if ($result) {
		$row=mssql_fetch_assoc($result);
		if($row) return $row['MaxDays'];
	}
}

// Inline Code -----------------------------------------------------------------------------

if(isset($_SESSION['ExportArray'])) {
	// Check for export before loading anything else.
	// The export headers need to be first.
	// If you put anything other then the export data in this condition, it will appear in the export file.
	if (isset($_GET['export'])) {
		//export_csv("forecast_rpt");
		export_xls("forecast_rpt");
		exit();
	}

	// Reset variable holding report detail for Export file
	unset($_SESSION['ExportArray']);
}

if (!isset($treeid)) {
	begin_report('Sales Performance Indicator');
	print_tree_prompt(ANYBODY);
	end_report();
	exit();
}

$report_export_option = 1;
begin_report('Sales Performance Indicator');
$report_export_option = 0;

//***** Create a report object and provide some property values.
$report = CreateReport();
$report['pagewidth'] = 11*72;
$report['pageheight'] = 8.5*72;
$report['left'] = 72/4;
$report['right'] = 10.75 * 72;
$report['bottom'] = 8.25 * 72;
$report['normalfontsize'] = 8;

//***** Create a PDF object
$p = PDF_new();
PDF_set_parameter($p, "license", PDFLIB_LICENSE_KEY);
PDF_open_file($p, "");

StartPDFReport($report, $p, "AAAA", "BBBB", "CCCCC"); // from ci_pdf_utils.php
SetLogo($report, "report_logo.jpg", 14, 50, 110, 0); // from ci_pdf_utils.php

//***** Build the report title
$sql="SELECT name 
	FROM company 
	WHERE companyid = $mpower_companyid";
$ok=mssql_query($sql);
if(($row=mssql_fetch_assoc($ok))) {
	$companyname=$row['name'];
}
$title="Sales Performance Indicator Report: $companyname";
$report['footertext']=$title;
BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, 0.1); // from ci_pdf_utils.php
AddFreeFormText($report, $title, -1, "center", 1.5, 8); // from ci_pdf_utils.php

//***** Load the sales people
$salestree=make_tree_path($treeid); // from GetSalespeopleTree.php
BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0.25); // from ci_pdf_utils.php
AddFreeFormText($report, $salestree, -1, "center", 2.75, 5.5); // from ci_pdf_utils.php

//***** Initialize variables for report
$left = 1;
$width1 = 1;
$width2 = 3;

$personid = $treeid;
$companyid = $mpower_companyid;

$cellfontsize=2;
$maxdays = 0;
$colcount = 0;
$IPRatio = 0;
$DPRatio = 0;

$Cycles = array();
GetSalesCycles($Cycles, $companyid, $maxdays);

$Products = array();
GetProducts($Products, $companyid);
GetDefaultRatios($companyid, $IPRatio, $DPRatio);

$level = GetLevel($personid);
$Salesmen=array();
$Errors = array();

if($level > 1) {
	GetSalesmen($Salesmen, $personid, $Cycles, $Errors);
}
else {
	GetSingleSalesman($Salesmen, $personid, $Cycles, $Errors);
}

//***** Print warning message before report
$ErrCount = count($Errors);
if($ErrCount > 0) {
	BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0); // from ci_pdf_utils.php
	AddColumn($report, "You must adjust Forecast ".GetCatLbl("Closed")." Date for the following opportunities before they can be included in this report:", "left", 4.0, 0);

	for($i=0; $i<$ErrCount; $i++) {
		$err = $Errors[$i];
		BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0); // from ci_pdf_utils.php
		AddColumnText($report, $err, "left", 2.75, 5.5); // from ci_pdf_utils.php
	}
}

//***** Print value info
BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0); // from ci_pdf_utils.php
AddColumn($report, "Values in Thousands", "right", 10.0, 0); // from ci_pdf_utils.php

//***** Print the guts of the report now
$smcount = count($Salesmen);

$DoPrint=TRUE;
if(!$smcount) {
	BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, .25); // from ci_pdf_utils.php
	AddFreeFormText($report, "No subordinates found!", -1, "center", 2.75, 5.5); // from ci_pdf_utils.php
}
else {
	$increment=16;
	$ccount=(int)($maxdays/30);

	if(0) {
		//DDDDDUmmy settings!
		$increment=25;
		$maxdays=540;
		$ccount=(int)($maxdays/30);
	}

	if($stop>$ccount) $stop=$ccount;
	if($level>1) {
		PrintColTitles($report, "Users", "Products", $maxdays);
	}
	else {
		PrintColTitles($report, false, "Products", $maxdays);
	}

	$arrCSV = array();
	$tmpCSV = array();
	
	// Load the CSV column headers as themselves
	$tmpCSV[0] = 'Level1';
	$tmpCSV[1] = 'Level2';
	$tmpCSV[2] = 'Level3';
	$tmpCSV[3] = 'Level4';
	$tmpCSV[4] = 'Level5';
	$tmpCSV[5] = 'Level6';
	$tmpCSV[6] = 'Products';
	$index = 7;
	for($i=30; $i<=$maxdays; $i+=30) {
		$tmpCSV[$index] = 'Days'.$i;
		$index++;
	}
	$tmpCSV[$index] = 'QtrClosed';
	$index++;
	$tmpCSV[$index] = 'QtrOpen';
	$index++;
	$tmpCSV[$index] = 'QtrTotal';
	array_push($arrCSV, $tmpCSV);
	
	for($i=0; $i<$smcount; $i++) {
			$sales = $Salesmen[$i];
			
			PrintSalesmanNumbers($report, $level, $sales, $ccount, $Products, $IPRatio, $DPRatio, $arrCSV);

			BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0); // from ci_pdf_utils.php
			SetRowColorStandard($report, -1, SHADE_MED); // from ci_pdf_utils.php
			AddColumnText($report, " ", "", 0, 0); // from ci_pdf_utils.php

			$Salesmen[$i] = $sales;
	}
}

PrintPDFReport($report, $p);
PDF_delete($p);

// load to session
$_SESSION['ExportArray'] = $arrCSV;

//close_db();
end_report();

?>
