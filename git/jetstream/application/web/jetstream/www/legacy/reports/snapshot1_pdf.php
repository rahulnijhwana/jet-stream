<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();


include('_report_utils.php');
//include('jim_utils.php');
include('ci_pdf_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');

$dateFrom = date('n/j/Y', strtotime($dateFrom));
$dateTo = date('n/j/Y', strtotime($dateTo));

define('SHADE_MED', 'MED_BLUE');
define('SHADE_LIGHT', 'LIGHT_BLUE');

define("TRUE",1);
define("FALSE",0);


$report = CreateReport();
$report['pagewidth'] = 11 * 72;
$report['pageheight'] = 8.5 * 72;
$report['left'] = 72 / 4;
$report['right'] = 10.75 * 72;
$report['bottom'] = 8.25 * 72;

if (isset($detail) && $detail == 1)
{
	$detail = true;
	$title='Board Snapshot Report';
}
else
{
	$detail = false;
	$title='Board Snapshot Report';
}

$p = PDF_new();
PDF_set_parameter($p, "license", PDFLIB_LICENSE_KEY);
PDF_open_file($p, "");
StartPDFReport($report, $p, "AAAA", "BBBB", "CCCCC");
SetLogo($report, "report_logo.jpg", 14, 50, 110, 0);
$salestree=make_tree_path($treeid);

if($showmult) $multlist=GetDirectReporters($treeid);
else $treelist=GetSalespeopleTree($treeid);

$bSales=0;
// rjp 5-21-04: include self in treelist if a salesperson
/*
if($treelist)
{
	$sql = "select IsSalesperson from people where PersonID = $treeid";
	$res = mssql_query($sql);
	$row = mssql_fetch_assoc($res);
	if($row['IsSalesperson'])
	{
		$bSales=1;
		$treelist = "$treeid,$treelist";
	}
}
*/

//set valuation flag
$ValuationUsed = GetValFlag($mpower_companyid);


$report['footertext']=$title;

BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, 0.1);
AddFreeFormText($report, $title, -1, "center", 2.75, 5.5);

BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0.25);
AddFreeFormText($report, $salestree, -1, "center", 2.75, 5.5);

$left = 1;
$width1 = 1;
$width2 = 3;

if($showmult)
{
	$count = count($multlist);
	for($i=0; $i<$count; $i++)
	{
		$id = $multlist[$i];
		$salestree=make_tree_path($id);
		$break = false;
		if($i>0) $break=true;
		BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0.25, $break);
		AddFreeFormText($report, $salestree, -1, "center", 2.75, 5.5);
		$list = GetSalespeopleTree($id);
		GetLogData($report, $id, $dateFrom, $dateTo, $list, $ValuationUsed);
	}
}
else
{
	GetLogData($report, $treeid, $dateFrom, $dateTo, $treelist, $ValuationUsed);
}
/*
echo '<pre>';
print_r($report);
echo '</pre>';
exit;
*/
PrintPDFReport($report, $p);
PDF_delete($p);

/*******************************************************************************************/

//function GetDaysFromTime($tm)
//{
//	$days = (int) $tm;
//	$days = (int)($tm/(24*60*60));
//	return $days;
//}

function GetCustomLabel()
{
global $mpower_companyid;
	$lbl='$';
	$sql = "select HasCustomAmount, CustomAmountName from company where CompanyID = $mpower_companyid";
	$res = mssql_query($sql);
	$row=mssql_fetch_assoc($res);
	if($row)
	{
		$hasCustom = $row['HasCustomAmount'];
		if(1==$hasCustom)
		{
			$lbl=$row['CustomAmountName'];
		}
	}
	return $lbl;
}

function GetValLabel()
{
	global $mpower_companyid;
	$sql = "select ValLabel from company where CompanyID = $mpower_companyid";
	$res = mssql_query($sql);
	$row = mssql_fetch_assoc($res);
	if($row)
		return $row['ValLabel'];
	return 'Valuation';
}

function CountDates($date, $date_array)
{
	$count = count($date_array);

	$result = 0;
	for($i=0; $i<$count; $i++)
	{
//print("dates = $date $date_array[$i]<br>");
		if($date == $date_array[$i]) $result++;
	}

	return $result;
}

// new theory about first meetings:
//  don't care when the first meeting is, only when it's put on the board
//  that way, the first meeting column should match up with what was recorded in the board snapshot table
function FirstMeetingsScheduled($id)
{
	$sql=	"select log.DealId, min(log.WhenChanged) LogDate FROM";
	$sql .=  " log INNER JOIN opportunities";
	$sql .=  " ON log.DealID = opportunities.DealID";
	$sql .=  " where TransactionID = 3 and log.PersonID IN($id)";
	$sql .=  " and ((not Category=9) or (FM_took_place = 1))";
	$sql .=  " group by log.DealID order by LogDate DESC";

//print "$sql<br>";

	$fm_array = array();

	$result = mssql_query($sql);
	if($result)
	{
//		$LastDeal=-1;
//		$ct=0;
//		$logint=-1;

		while($row = mssql_fetch_assoc($result))
		{
//Use the earliest of: date of the meeting / when it was scheduled

			$logdate = date("M j, Y", strtotime($row['LogDate']));
//			$logdate = preg_replace('/ \d+:\d+\w+$/', '', $row['LogDate']);
//			$changedate = preg_replace('/ \d+:\d+\w+$/', '', $row['WhenChanged']);

			$ldate = strtotime($logdate);
//			$wcdate = strtotime($changedate);

// see note above			$date = ($wcdate <= $ldate) ? $wcdate : $ldate;
			$date = $ldate;
//			if($LastDeal != $row['DealID'])
//				$LastDeal = $row['DealID'];
//			else
//				continue;

			array_push($fm_array, $date);
		}
	}

	return $fm_array;
}

function MakeMonthArray($fm_array, $startdate, $enddate)
{
	$month_array = array();
	$count = count($fm_array);
	for($i = 0; $i < $count; $i++) {
		$timestamp = $fm_array[$i];
		if($timestamp<$startdate || $timestamp>=$enddate) {
			continue;
		}
		$date = getdate($timestamp);
		$mday = $date['mday'];
		$month = $date['mon'];
		$year = $date['year'];
		$key = "$month$year";
		$val = (isset($month_array[$key])) ? $month_array[$key] : 0;
		// $val = $month_array[$key];
		$val += 1;
		$month_array[$key] = $val;
//print("$mday $month $year = $val<br>");
	}

	return $month_array;
}

function GetMonthCount($month, $year, $month_array)
{
	$key = "$month$year";
	return $month_array[$key];
}

function AddLocationRow(&$report, $text1, $text2, $left, $width1, $width2)
{
	BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0);
	AddFreeFormText($report, $text1, -1, "center", $left, $width1);
	AddFreeFormText($report, $text2, -1, "left", $left+$width1, $width2);
}

function PrintSnapshotRow(&$report, $date, $fm, $ip, $sip, $dp, $sdp, $cl, $sales, $avgsales, $rating, $schedmtgs, $stalled, $stalledavgage, $removed, $removedavgage, $dpavgage, $valavg, $valused, $hilight, $schedfirstmtgs)
{
	$date = substr($date, 0, strlen($date)-4).substr($date, strlen($date)-2);
	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
	if ($hilight) SetRowColorStandard($report, -1, SHADE_LIGHT);
	AddColumnText($report, $date, "left", 0, 0);
	if($valused)
		AddColumnText($report, $valavg, "", 0, 0);
	AddColumnText($report, $schedfirstmtgs, "right", 0, 0);
	AddColumnText($report, $fm, "center", 0, 0);
	AddColumnText($report, $ip, "center", 0, 0);
	AddColumnText($report, $sip, "center", 0, 0);
	AddColumnText($report, $dp, "center", 0, 0);
	AddColumnText($report, $sdp, "center", 0, 0);
	AddColumnText($report, $cl, "center", 0, 0);
	AddColumnText($report, fm($sales,1), "center", 0, 0);
	AddColumnText($report, fm($avgsales,1), "center", 0, 0);
	AddColumnText($report, $rating, "center", 0, 0);
	AddColumnText($report, $schedmtgs, "center", 0, 0);
	AddColumnText($report, $stalled, "center", 0, 0);
	AddColumnText($report, $stalledavgage, "center", 0, 0);
	AddColumnText($report, $removed, "center", 0, 0);
	AddColumnText($report, $removedavgage, "center", 0, 0);
	AddColumnText($report, $dpavgage, "center", 0, 0);

}

function summrec(&$report, $mo, $totrecs, $totdays, $totdprecs, $totfm, $totip, $totsip, $totdp, $totsdp, $totcl,
			$totsales, $totrating, $totsched, $totstalled,
			$totstalledavgage, $totremoved, $totremovedavgage, $totdpavgage,
			$stallcount, $remcount, $totavgval, $valcount, $valused, $hilight, $totschedfirst)
{
	$summfm = sprintf("%.01f", $totfm / $totdays);
	$summip = sprintf("%.01f", $totip / $totdays);
	$summsip = sprintf("%.01f", $totsip / $totdays);
	$summdp = sprintf("%.01f", $totdp / $totdays);
	$summsdp = sprintf("%.01f", $totsdp / $totdays);
	$summcl = sprintf(" %d ", $totcl);
//OK to round to nearest $  Allan Ellison 2/4/2005
//	$summsales = sprintf("%.01f", $totsales);
	$summsales = sprintf("%ld", round($totsales));

	if($totcl != 0)
	{
//		$summavgsales = sprintf("%.02f", $summsales / $totcl);
		$summavgsales = sprintf("%ld", round($summsales / $totcl));
	}
	else
	{
		$summavgsales = '0';
	}
	$summrating = sprintf("%.01f", $totrating / $totrecs);
	$summsched = sprintf("%.01f", $totsched / $totdays);
	$summschedfirst = sprintf("%d", $totschedfirst);
	$summstalled = sprintf("%.01f", $totstalled / $totdays);
	// rjp 5-18-2004: stalled and removed should only average non-zero items
	if($stallcount)
	{
		$summstalledavgage = sprintf("%.01f", $totstalledavgage / $stallcount);
	}
	else
	{
		$summstalledavgage = 'N/A';
	}
	//$summremoved = sprintf("%.01f", $totremoved / $totdays);
	$summremoved = $totremoved;
	if($remcount)
	{
		$summremovedavgage = sprintf("%.01f", $totremovedavgage / $remcount);
	}
	else
	{
		$summremovedavgage = 'N/A';
	}
	if ($totdprecs)
		$summdpavgage = sprintf("%.01f", $totdpavgage / $totdprecs);
	else $summdpavgage='N/A';

	if($valcount)
	{
		$summvalavg = sprintf("%.01f", $totavgval / $valcount);
	}
	else
	{
		$summvalavg = '0.0';
	}
	//Abbreviate month to 3 chars
		$sp = strpos($mo,' ');
		if($sp > -1)
		{
			$m = substr($mo, 0, 3);
			$y = substr($mo, $sp);
			$mo = "$m $y";
		}
	PrintSnapshotRow($report, $mo, $summfm, $summip, $summsip, $summdp,
		$summsdp, $summcl, $summsales, $summavgsales, $summrating,
		$summsched, $summstalled, $summstalledavgage, $summremoved,
		$summremovedavgage, $summdpavgage, $summvalavg, $valused, $hilight, $summschedfirst);
}

function GetClosedList($salesid, $startdate, $enddate, $treelist)
{
	$closed_arr = array();

	if($treelist != "")
	{
		$sql = "select ActualCloseDate, ActualDollarAmount from opportunities where PersonID in ($treelist) and ActualCloseDate>=CONVERT(DATETIME,'$startdate 00:00:00',102) and ActualCloseDate<=CONVERT(DATETIME,'$enddate 23:59:59',102) ORDER BY ActualCloseDate";
	}
	else
	{
		$sql = "select ActualCloseDate, ActualDollarAmount from opportunities where PersonID=$salesid and ActualCloseDate>=CONVERT(DATETIME,'$startdate 00:00:00',102) and ActualCloseDate<=CONVERT(DATETIME,'$enddate 23:59:59',102) ORDER BY ActualCloseDate";
	}

	$res=mssql_query($sql);
	while(($row=mssql_fetch_assoc($res)))
	{
		array_push($closed_arr, $row);
	}

	return $closed_arr;
}

function CompareDates($date1, $date2)
{
	if($date1['year'] < $date2['year']) return -1;
	if($date1['year'] > $date2['year']) return 1;
	if($date1['yday'] < $date2['yday']) return -1;
	if($date1['yday'] > $date2['yday']) return 1;
//print("Dates $date1,$date2");
	return 0;
}

$lastGetClosed;		// this handles closed items that occurred on the weekend, we pick them up monday
function GetClosedData($closed_arr, $sdate, &$cl_count, &$cl_amount)
{
	global $lastGetClosed;

	$testdate = getdate(strtotime($sdate));

	$cl_count=0;
	$cl_amount=0;
	$size = sizeof($closed_arr);

	for($i=0; $i<$size; $i++)
	{
		$row = $closed_arr[$i];
		$date = getdate(strtotime($row['ActualCloseDate']));

		// $closedate = strtotime($date);
		if ($lastGetClosed)
		{
			if(0>CompareDates($lastGetClosed,$date) && 0<=CompareDates($testdate, $date))
			{
				$cl_count++;
				$cl_amount += $row['ActualDollarAmount'];
			}
		}
		else
		{
			if(0==CompareDates($testdate, $date))
			{
				$cl_count++;
				$cl_amount += $row['ActualDollarAmount'];
			}
		}
	}
	$lastGetClosed=$testdate;
}

$lastdate='';	// array containing the last valid date of data

function GetClosedDataMonth($closed_arr, $month, $year, &$cl_count, &$cl_amount)
{
	global $lastdate;  // last date of data, don't go past this


	$start = sprintf("%d/01/%d", $month, $year);
	$end;
	$inclusive=false;
	if($month < 12) $end = sprintf("%d/01/%d", $month+1, $year);
	else $end = sprintf("01/01/%d", $year+1);
	$teststart = getdate(strtotime($start));
	$testend = getdate(strtotime($end));

	if (0<CompareDates($testend,$lastdate))
	{
		$testend=$lastdate;
		$inclusive=true;
	}

	$cl_count=0;
	$cl_amount=0;
	$size = sizeof($closed_arr);

	for($i=0; $i<$size; $i++)
	{
		$row = $closed_arr[$i];
		$date=getdate(strtotime($row['ActualCloseDate']));
		if(0>=CompareDates($teststart, $date) && ($inclusive ? (0<=CompareDates($testend, $date)) : (0<CompareDates($testend, $date))))
		{
			$cl_count++;
			$cl_amount += $row['ActualDollarAmount'];
		}
	}
}

function GetLogData(&$report, $salesid, $startdate, $enddate, $treelist, $valused)
{
	global $lastdate, $bSales;
	$reccount = 0;
	BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0);
	SetRowValue($report, -1, "rotate", 1);
	SetRowColorStandard($report, -1, SHADE_MED);
	AddColumn($report, "Date", "left", 1.0, 0);
	SetColColorStandard($report, -1, -1, SHADE_MED);

	$lblCustom = GetCustomLabel();

	$fm_array = FirstMeetingsScheduled($treelist);

	$month_array = MakeMonthArray($fm_array, strtotime($startdate), strtotime($enddate));

	if($valused)
		AddColumn($report, GetValLabel()." Average", "right", .4, 0);
	$strFM = "First Meetings (FM)";
	// RJP 7-25-05: Since the text is passed by reference,
	// strings should not be assigned from CatLbl.
	CatLbl('First Meeting', $strFM);
	CatLbl('FM', $strFM);
	$strDP = "Decision Point (DP)";
	CatLbl('Decision Point', $strDP);
	CatLbl('DP', $strDP);

//	AddColumn($report, "First Meetings (FM)", "right", .4, 0); // .8 in wide, 0 extra space
	AddColumn($report, "First Meetings Entered Today", "right", .5, 0);
	AddColumn($report, $strFM, "right", .4, 0); // .8 in wide, 0 extra space

	AddColumn($report, GetCatLbl("Information Phase"), "right", .4, 0);
//	AddColumn($report, "Stalled " . GetCatLbl("Information Phase"), "right", .4, 0);
	AddColumn($report, GetCatLbl('Stalled') . " " . GetCatLbl("Information Phase"), "right", .4, 0);

//	AddColumn($report, "Decision Point (DP)", "right", .4, 0);
	AddColumn($report, $strDP, "right", .4, 0);

//	AddColumn($report, "Stalled " . GetCatLbl("Decision Point"), "right", .4, 0);
	AddColumn($report, GetCatLbl('Stalled') ." " . GetCatLbl("Decision Point"), "right", .4, 0);


	AddColumn($report, GetCatLbl("Closed"), "right", .5, 0);
	AddColumn($report, "Total Sales $lblCustom", "right", 1.0, 0);
	AddColumn($report, "Avg Sales $lblCustom", "right", 1.0, 0);

	AddColumn($report, "Rating", "right", .6, 0);
	AddColumn($report, "Total Sched Meetings", "right", .6, 0);
//	AddColumn($report, "Total Stalled", "right", .5, 0);
	AddColumn($report, "Total " . GetCatLbl('Stalled'), "right", .5, 0);

//	AddColumn($report, "Average Age Stalled", "right", .5, 0);
	AddColumn($report, "Average Age " . GetCatLbl('Stalled'), "right", .5, 0);

	AddColumn($report, "Total Removed", "right", .5, 0);

	AddColumn($report, "Average Age Removed", "right", .5, 0);
	$strLbl = "Average Days FM to DP";
	CatLbl('FM', $strLbl);
	CatLbl('DP', $strLbl);

//	AddColumn($report, "Average Days FM to DP", "right", .5, 0);
	AddColumn($report, $strLbl, "right", .5, 0);


	if($startdate != 'N/A' && $enddate != 'N/A')
	{
		$period=datediff_php("d", $startdate, $enddate);
	}
	else
	{
		$period=30;
		if($enddate == 'N/A')
			$enddate == dateadd_php("d", $startdate, $period);
	}
//Treelist is never null!  The first query never runs.  God help us if it does!
	if($treelist != "")
	{
		$sql = "select max(dateadd(day, -1, SnapDate)) as PeriodEnd, sum(SchedFirstMeetings) as TotFirst, sum(FM) as FM, sum(IP) as IP, sum(SIP) as SIP, sum(DP) as DP, sum(SDP) as SDP, sum(Closed) as Closed, sum(TotalSales) as TotalSales, avg(RemovedAvgAge) as RemovedAvgAge, avg(StalledAvgAge) as StalledAvgAge, avg(DPAvgAge) as DPAvgAge, sum(TotSched) as TotSched, sum(TotStalled) as TotStalled, sum(Removed) as Removed, avg(cast(Rating as float)) as avgRating, avg(ValAvg) as ValAvg from snapshot where SalesID in ($treelist) and datediff(day, dateadd(day, -1, SnapDate), '$enddate') <= $period and dateadd(day, -1, SnapDate)<=CONVERT(DATETIME,'$enddate 23:59:59',102) GROUP BY SnapDate ORDER BY PeriodEnd";
	}
	else
	{
		$sql = "select dateadd(day, -1, SnapDate) as PeriodEnd, * from snapshot where SalesID=$salesid and datediff(day, dateadd(day, -1, SnapDate), '$enddate') <= $period and PeriodEnd<=CONVERT(DATETIME,'$enddate 23:59:59',102) ORDER BY SnapDate";
	}
	$res=mssql_query($sql);
	$thismonth = "";
	$hilight=1;
	while(($row=mssql_fetch_assoc($res)))
	{

		// extract month (%% WARNING: USA-centric)
		$date_arr = preg_split('/ /', $row['PeriodEnd']);
		$mo = $date_arr[0];
		if($mo != $thismonth)
		{
			if($thismonth)
			{
				BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0.17);
			}
			else
			{
				$dateinfo = strtotime($row['PeriodEnd']);
				$closed_arr = GetClosedList($salesid,strftime('%m/%d/%Y',$dateinfo) , $enddate, $treelist);
			}
			$thismonth = $mo;
		}

		$cl_count=0;
		$cl_amount=0;

		// chop off the time-of-day
		$periodend = date('m/j/Y', strtotime($row['PeriodEnd']));
		// $periodend = preg_replace('/ \d+:\d+\w+$/', '', $row['PeriodEnd']);
		GetClosedData($closed_arr, $periodend, $cl_count, $cl_amount);

		$avgval = sprintf("%.01f", $row['ValAvg']);

		$fm = sprintf(" %d ", $row['FM']);
		$ip = sprintf(" %d ", $row['IP']);
		$sip = sprintf(" %d ", $row['SIP']);
		$dp = sprintf(" %d ", $row['DP']);
		$sdp = sprintf(" %d ", $row['SDP']);

//		$cl = sprintf(" %d ", $row['Closed']);
//		$sales = sprintf("%d", round($row['TotalSales']));
		$cl = $cl_count;
		$sales = $cl_amount;

		$removedavgage = $row['RemovedAvgAge'];
		if($removedavgage)
		{
			$removedavgage = sprintf("%.01f", $removedavgage);
		}
		$stalledavgage = $row['StalledAvgAge'];
		if($stalledavgage)
		{
			$stalledavgage = sprintf("%.01f", $stalledavgage);
		}
		$dpavgage = sprintf("%.01f", $row['DPAvgAge']);
		if($cl != 0)
		{
//			$avgsales = sprintf("%.02f", $sales / $cl);
$avgsales = sprintf("%d", round($sales / $cl));

		}
		else
		{
			$avgsales = '0';
		}
		if(!$bSales)
		{
			// in management rollup, it's an average
			$rating = sprintf("%.01f", $row['avgRating']);
		}
		else
		{
//			$rating = sprintf(" %d ", $row['Rating']);
			$rating = sprintf(" %d ", $row['avgRating']);
		}
		$schedmtgs = sprintf(" %d ", $row['TotSched']);
		$stalled = sprintf(" %d ", $row['TotStalled']);
		$removed = sprintf(" %d ", $row['Removed']);
		if($hilight) $hilight = 0;
		else $hilight = 1;

		$lastdate=$row['PeriodEnd'];	// used so we don't overshoot the last data

		$schedfirstmtgs = CountDates(strtotime($periodend), $fm_array);
		
		PrintSnapshotRow($report, $periodend, $fm, $ip, $sip, $dp, $sdp, $cl,
			$sales, $avgsales, $rating, $schedmtgs, $stalled, $stalledavgage,
			$removed, $removedavgage, $dpavgage, $avgval, $valused, $hilight, $schedfirstmtgs);
	}

	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0.33);

	if($treelist != "")
	{
		$sql = "select *, datename(month, dateadd(day, -1, SnapDate)) + ' ' + cast(datepart(year, dateadd(day, -1, SnapDate)) as varchar) as Label,";
		$sql.= "datepart(month, dateadd(day, -1, SnapDate)) as Month, datepart(year, dateadd(day, -1, SnapDate)) as Year from snapshot where SalesID in ($treelist) and dateadd(day, -1, SnapDate)>=CONVERT(DATETIME,'$startdate 0:00:00',102) and dateadd(day, -1, SnapDate)<=CONVERT(DATETIME,'$enddate 23:59:59',102) ORDER BY SnapDate desc";
//		$sql = "select *, datename(month, dateadd(day, -1, SnapDate)) + ' ' + cast(datepart(year, dateadd(day, -1, SnapDate)) as varchar) as Month from snapshot where SalesID in ($treelist) and datediff(month, dateadd(day, -1, SnapDate), getdate()) <= 12 ORDER BY SnapDate desc";
//		$sql = "select *, datename(month, dateadd(day, -1, SnapDate)) + ' ' + cast(datepart(year, dateadd(day, -1, SnapDate)) as varchar) as Month from snapback where SalesID in ($treelist) and datediff(month, dateadd(day, -1, SnapDate), getdate()) <= 12 ORDER BY SnapDate desc";

	}
	else
	{
		$sql = "select *, datename(month, dateadd(day, -1, SnapDate)) + ' ' + cast(datepart(year, dateadd(day, -1, SnapDate)) as varchar) as Label,";
		$sql.= "datepart(month, dateadd(day, -1, SnapDate)) as Month, datepart(year, dateadd(day, -1, SnapDate)) as Year from snapshot where SalesID=$salesid and dateadd(day, -1, SnapDate)>=CONVERT(DATETIME,'$startdate 0:00:00',102) and dateadd(day, -1, SnapDate)<=CONVERT(DATETIME,'$enddate 23:59:59',102) ORDER BY SnapDate desc";
//		$sql = "select *, datename(month, dateadd(day, -1, SnapDate)) + ' ' + cast(datepart(year, dateadd(day, -1, SnapDate)) as varchar) as Month from snapshot where SalesID=$salesid and datediff(month, dateadd(day, -1, SnapDate), getdate()) <= 12 ORDER BY SnapDate desc";
//		$sql = "select *, datename(month, dateadd(day, -1, SnapDate)) + ' ' + cast(datepart(year, dateadd(day, -1, SnapDate)) as varchar) as Month from snapback where SalesID=$salesid and datediff(month, dateadd(day, -1, SnapDate), getdate()) <= 12 ORDER BY SnapDate desc";
	}
	$res=mssql_query($sql);
	$thismonth = 0;
	$thisyear = 0;
	$thislabel="";
	//$vertspace = 0.25;
	//$hilight=1;

	$lastdate=getdate(strtotime($lastdate));
	$SnapDate="";
	while(($row=mssql_fetch_assoc($res)))
	{
		$reccount++;
		$mo = $row['Month'];
		$yr = $row['Year'];
		$label = $row['Label'];
//print("Month $mo ThisMonth $thismonth<br>");
		if($mo != $thismonth || $yr !=$thisyear)
		{
			if($thismonth != 0)	// skip first time around
			{
				$cl_count = 0;
				$cl_amount = 0;
				GetClosedDataMonth($closed_arr, $thismonth, $thisyear, $cl_count, $cl_amount);

				$totcl = $cl_count;
				$totsales = $cl_amount;

				if($hilight) $hilight = 0;
				else $hilight = 1;

				$totschedfirst = GetMonthCount($thismonth, $thisyear, $month_array);
				summrec($report, $thislabel, $totrecs, $totdays, $totdprecs, $totfm, $totip, $totsip, $totdp, $totsdp,
					$totcl, $totsales, $totrating, $totsched, $totstalled,
					$totstalledavgage, $totremoved, $totremovedavgage, $totdpavgage,
					$stallcount, $remcount, $totavgval, $valcount, $valused, $hilight, $totschedfirst);
			}
			$totrecs = 0;
			$totdays = 0;
			$totdprecs=0;
			$totfm = 0;
			$totip = 0;
			$totsip = 0;
			$totdp = 0;
			$totsdp = 0;
			$totcl = 0;
			$totsales = 0;
			$totrating = 0;
			$totsched = 0;
			$totschedfirst = 0;
			$totstalled = 0;
			$totstalledavgage = 0;
			$totremoved = 0;
			$totremovedavgage = 0;
			$totdpavgage = 0;
			$thismonth = $mo;
			$thisyear = $yr;
			$thislabel= $label;
			$stallcount = 0;
			$remcount = 0;
			$totavgval = 0;
			$valcount = 0;
		}

		if ($SnapDate !=$row['SnapDate'])
		{
			$SnapDate=$row['SnapDate'];
			$totdays ++;
		}
		$totrecs++;
		$totfm += $row['FM'];
		$totip += $row['IP'];
		$totsip += $row['SIP'];
		$totdp += $row['DP'];
		$totsdp += $row['SDP'];

//		$totcl += $row['Closed'];
//		$totsales += $row['TotalSales'];

		$totrating += $row['Rating'];
		$totsched += $row['TotSched'];

//		$totschedfirst += $row['TotFirst'];
		// $sdate = preg_replace('/ \d+:\d+\w+$/', '', $row['SnapDate']);
		$sdate = date('M j, Y', strtotime($row['SnapDate']));

		$totstalled += $row['TotStalled'];
		if($row['StalledAvgAge'] != 0)
		{
			$stallcount++;
		}
		$totstalledavgage += $row['StalledAvgAge'];
		$totremoved += $row['Removed'];
		if($row['RemovedAvgAge'] != 0)
		{
			$remcount++;
		}
		$totremovedavgage += $row['RemovedAvgAge'];
		if ($row['DPAvgAge'])
		{
			$totdprecs++;
			$totdpavgage += $row['DPAvgAge'];
		}
		if ($row['ValAvg'] > 0)
		{
			$totavgval += $row['ValAvg'];
			$valcount++;
		}
	}

/*!!!!!
	if($reccount)
	{
		$cl_count = 0;
		$cl_amount = 0;

		$dateinfo = getdate(strtotime($SnapDate));
		GetClosedDataMonth($closed_arr, $mo, $yr, $cl_count, $cl_amount);

		$totcl = $cl_count;
		$totsales = $cl_amount;

		if($hilight) $hilight = 0;
		else $hilight = 1;

		$totschedfirst = GetMonthCount($mo, $yr, $month_array);

		summrec($report, $label, $totrecs, $totdays, $totdprecs, $totfm, $totip, $totsip, $totdp, $totsdp, $totcl,
			$totsales, $totrating, $totsched, $totstalled,
			$totstalledavgage, $totremoved, $totremovedavgage,
			$totdpavgage, $stallcount, $remcount, $totavgval, $valcount, $valused, $hilight, $totschedfirst);
	}
*/
}

function datediff_php($interval, $date1, $date2)
{
 $s = strtotime($date2)-strtotime($date1);
 $d = intval($s/86400);
 $s -= $d*86400;
 $h = intval($s/3600);
 $s -= $h*3600;
 $m = intval($s/60);
 $s -= $m*60;
 $arrRes=array("d"=>$d,"h"=>$h,"m"=>$m,"s"=>$s);
 return $arrRes[$interval];
}


function dateadd_php($interval, $date, $period)
{
	switch($interval)
	{
		case "d":
			$s = $period * 86400;
		case "h":
			$s = $period * 3600;
		case "m":
			$s = $period * 60;
	}
	$dt = strtotime($date);
	return date("m/d/Y", $dt + $s);
}

function GetValFlag($companyid)
{
	$sql = "select ValuationUsed from Company where companyid=$companyid";
	$res = mssql_query($sql);
	$row = mssql_fetch_assoc($res);
	return $row['ValuationUsed'];
}
close_db();

?>
