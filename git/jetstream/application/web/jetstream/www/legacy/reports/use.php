<?php
 /**
 * @package Reports
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">
<?php

include('_report_utils.php');
include ('GetSalespeopleTree.php');

$cellfontsize="16px";

begin_report('Use Report');
// treeid will be set to the ID of a person to base data on
if (!isset($treeid))
{
	print_tree_prompt(MANAGEMENT);	// can also be SALESPEOPLE or SALESPEOPLE_MGR
	// to control who can be selected in the tree
	end_report();
	exit();
}

print_tree_path($treeid);

include('_russ_utils.php');

$grace = get_grace_period($mpower_companyid);

$udata = get_user_data($mpower_userid, $mpower_companyid);
$level = $udata['Level'];
$person_id = $udata['PersonID'];

//print("<h3>mpower_userid: $mpower_userid mpower_companyid: $mpower_companyid person_id: $person_id level: $level</h3><br>\n");

// Set the time zone according to the user setting.
// PHP time functions take the TZ variable into account,
// so the current date will reflect the new time zone setting,
// e.g. if the time zone is America/Chicago and the current
// system time is 12:34 PM GMT, PHP functions will consider
// the time to be 07:34 AM CDT once the TZ environment variable
// is set.
$tz_name = get_timezone_name($udata['timezone']);
putenv("TZ=$tz_name");

if($level < 2)
{
	$levname = get_level_name($level, $mpower_companyid);
	print("<H3>$levname do not have permission to view this report.</H3>\n");
	end_report();
	return;
}

// NOTE: when a column name is unambiguous (i.e. not in two different tables),
// the table prefix will NOT make it into the rows fetched by mssql_fetch_assoc().
// This is true even in a join, if we're not selecting the same-named column from
// two or more tables.  In this query, log.PersonID must be retrieved as $row['PersonID']
// rather than as $row['log.PersonID'] in order to fetch the data.


// we can't select on TransactionID = 300, since people who have never logged in
// need to be counted.

// POSSIBLE CHANGE: Narrow the selection down so that we don't go through
// every entry in the log table.  3 mos. before current year?


function PrintUseReport($personid, $level, $companyid)
{

	$thisqtr = current_quarter();
	$thisyr = strftime("%Y"); // %Y = year with century
	$grace = get_grace_period($companyid);

	$query	= "SELECT log.PersonID, StartDate, LogDate, Level, LastName + ', ' + FirstName AS FullName, TransactionID  "
	. "FROM people LEFT JOIN log ON log.PersonID = people.PersonID "
	. "WHERE (SupervisorID=$personid OR (people.PersonID=$personid AND IsSalesperson=1)) and Deleted=0 "
	. "AND log.CompanyID = $companyid "
	. "ORDER BY FullName, LogDate";


	$result	= mssql_query($query);

	$rowcount = 0;
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$rowcount++;
			$person = $row['PersonID'];
			$logdate = timestamp_convert_tz($row['LogDate']);

			//Per ASA rqst 6/11/2004.  Jim (put back, temporarily)
			//			$logdate = date("m/d/Y", strtotime($row['LogDate']));

			$tid = $row['TransactionID'];
			// use the start date as the last use date, in case the salesperson never logged in
			if(!$usetbl[$person])
			{
				$usetbl[$person] = date("m/d/y", strtotime(timestamp_convert_tz($row['StartDate'])));
				//				$usetbl[$person] = date("m/d/y", strtotime($row['StartDate']));

				// initialize totals
				$alerts_total[$person] = 0;
				$alerts_qtr[$person] = 0;
				$alerts_ytd[$person] = 0;
			}

			// check to see if we have a previous entry
			if($tid == 300)
			{
				$last = $usetbl[$person];
				$interval = day_interval($last, $logdate);
				if($interval > $grace)
				{
					$alerts_total[$person]++;
					if(which_quarter($logdate) == $thisqtr && which_year($logdate) == $thisyr)
					{
						$alerts_qtr[$person]++;
					}
					if(which_year($logdate) == $thisyr)
					{
						$alerts_ytd[$person]++;
					}
				}
				$usetbl[$person] = date("m/d/y", strtotime($logdate));
			}
			$names[$person] = $row['FullName'];
		}
	}

	if($rowcount == 0) return;

	$ids = array_keys($names);

	foreach($ids as $person)
	{
		$interval = day_interval($usetbl[$person], 'now');
		if($interval > $grace)
		{
			$alertdays[$person] = $interval - $grace;
			$alerts_total[$person]++;
			if(which_quarter($usetbl[$person]) == $thisqtr && which_year($usetbl[$person]) == $thisyr)
			{
				$alerts_qtr[$person]++;
			}
			if(which_year($usetbl[$person]) == $thisyr)
			{
				$alerts_ytd[$person]++;
			}
		}
		else
		{
			$alertdays[$person] = 'N/A';
		}
	}

	print "<table border=2 cellspacing=0 cellpadding=10>\n";
	table_headings(array("&nbsp;", "Date of Last Activity", "Number of Days in Use Alert Mode",
	"Number of Alerts All-Time", "Number of Alerts This Quarter", "Number of Alerts Year-to-Date"));

	table_row(array(get_level_name($level - 1, $companyid), "", "",
	array_sum($alerts_total), array_sum($alerts_qtr), array_sum($alerts_ytd)));
	table_row(array("", "", "", "", "", ""));
	foreach($ids as $person)
	{
		table_row(array($names[$person], $usetbl[$person], $alertdays[$person],
		$alerts_total[$person], $alerts_qtr[$person], $alerts_ytd[$person]));
	}
	print "</table>\n";
}


function UseReport($salesid, $companyid)
{
	$strdate = date("m/d/y", time());
	print("<br><b>Date run:   $strdate</b><br><br>");

	$sql="select * from people where PersonID=$salesid";
	$res=mssql_query($sql);

	if($row=mssql_fetch_assoc($res))
	{
		$level = $row['Level'];
		//		if($level == 2)

		if($level > 1)
		{
			PrintUseReport($salesid, $level, $companyid);
		}
		else
		{
			PrintUseGroup($salesid, "", $companyid);
		}
	}

}

function PrintUseGroup($supervisorid, $nametree, $companyid)
{

	$sql = "select * from people where SupervisorID=$supervisorid and Deleted=0 and level>1";
	$res=mssql_query($sql);

	while($row=mssql_fetch_assoc($res))
	{
		$id = $row['PersonID'];
		$level = $row['Level'];
		$name = $row['FirstName'] . " " . $row['LastName'];
		$group = $row['GroupName'];
		if(strlen($group) > 0) $name .= " ($group)";
		$name = $nametree . "$name: ";

		print("<br><br><b>$name</b><br>");
		if($level == 2) PrintUseReport($id, $level, $companyid);
		else PrintUseGroup($id, $name, $companyid);
	}
}

////////////////////////////////////////////////////////////////////////////////////////


UseReport($treeid, $mpower_companyid);

end_report();
?>

