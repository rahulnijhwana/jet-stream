<?php
 /**
 * @package Reports
 */

define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('dbg.php');
include('_report_utils.php');
include('jim_utils.php');
include('ci_pdf_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');

define(SHADE_MED, "MED_BLUE");
define(SHADE_LIGHT, "LIGHT_BLUE");


define("TRUE",1);
define("FALSE",0);

//Updates Needed REport
//List all opportunities with an alert2 field true.  Most fields come from the opportunity.

$report = CreateReport();
$report['left'] = 72/4;
$report['right'] = 8.25 * 72;

if (false==IsManager($treeid))
{
	$issales = true;
	$title='Updates Needed Detail Report';
}
else
{
	$issales = false;
	$title='Updates Needed Report';
}

$p = PDF_new();
PDF_set_parameter($p, "license", PDFLIB_LICENSE_KEY);
PDF_open_file($p, "");
StartPDFReport($report, $p, "AAAA", "BBBB", "CCCCC");
//SetLogo($report, "report_logo.jpg", 10, 40, 72, 0);
SetLogo($report, "report_logo.jpg", 14, 50, 110, 0);
$salestree=make_tree_path($treeid);
$report['footertext']=$title;

BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, 0.1);
AddFreeFormText($report, $title, -1, "center", 1.5, 5.5);

BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0.25);
AddFreeFormText($report, $salestree, -1, "center", 1.5, 5.5);

$left = 1;
$width1 = 1;
$width2 = 5;

$sql="select * from company where CompanyID=$mpower_companyid";
$res=mssql_query($sql);

$row=mssql_fetch_assoc($res);
$UseReq1 = $row['Requirement1used'];

PrintLocAbbr($report, $left, $width1, $width2, $treeid);

UpdatesNeeded_OneLevel($report, $mpower_companyid, $treeid, $issales, $UseReq1);

PrintPDFReport($report, $p);
PDF_delete($p);


function PrintLocAbbr(&$report, $left, $width1, $width2, $treeid)
{
	$MSCount=GetMilestoneList($treeid, $MSList);
	$MSLabel="Information Phase ($MSList)";

	BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, .2);
	AddFreeFormText($report, "Location Abbreviations:", -1, "left", $left, 2.5);


	AddLocationRow($report, GetCatLbl("FM"), GetCatLbl("First Meeting"), $left, $width1, $width2);
	AddLocationRow($report, GetCatLbl("IP") . "(1-$MSCount)", GetCatLbl("Information Phase") . "($MSList)", $left, $width1, $width2);
	AddLocationRow($report, GetCatLbl("S"). GetCatLbl("IP"), GetCatLbl("Stalled") . ' ' . GetCatLbl("Information Phase"), $left, $width1, $width2);
	AddLocationRow($report, GetCatLbl("DP"), GetCatLbl("Decision Point"), $left, $width1, $width2);
	AddLocationRow($report, GetCatLbl("S") . GetCatLbl("DP"), GetCatLbl("Stalled") . " " . GetCatLbl("Decision Point"), $left, $width1, $width2);
	AddLocationRow($report, GetCatLbl("RM"), GetCatLbl("Removed"), $left, $width1, $width2);
	AddLocationRow($report, "NA", "Unknown Location", $left, $width1, $width2);
}



//UpdatesNeeded($mpower_companyid, $treeid);
function UpdatesNeeded_OneLevel(&$report, $CompanyID, $SupervisorID, $issales, $UseReq1)
{
	UpdatesNeeded($report, $CompanyID, $SupervisorID, TRUE, $issales, $UseReq1);
}

function UpdatesNeeded_Detail(&$report, $CompanyID, $SupervisorID, $UseReq1)
{
	UpdatesNeeded($report, $CompanyID, $SupervisorID, FALSE, $UseReq1);
}

function UpdatesNeeded(&$report, $idCompany, $idPerson, $bTopOnly, $issales, $UseReq1)
{
	$arrNeeded=array();
	if(!GetNeeded($report, $idPerson, &$arrNeeded, $bTopOnly, $issales, $UseReq1))
	{
		dbg("GetNeeded() returned false");
		return;
	}

	dbg("GetNeeded() returned true");
	BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0);

	SetRowColorStandard($report, -1, SHADE_MED);

	AddColumn($report, "Salesperson", "center", 1.2, 0);

	AddColumn($report, "Company", "center", 1.4, 0);
	AddColumn($report, "Offering", "center", 1.4, 0);
	AddColumn($report, "Meeting Date to be Updated", "center", 0.8, 0);
	AddColumn($report, "Loc", "center", 0.8, 0);
	AddColumn($report, "Date of Alert", "center", 0.8, 0);
	AddColumn($report, "Alerts This Quarter", "center", 0.8, 0);
	AddColumn($report, "Alerts YTD", "center", 0.8, 0);

	$last="";
	$QTotal = 0;
	$YTotal = 0;
	$rowcolor=FALSE;
	for($i=0; $i< count($arrNeeded); $i++)
	{
		//BeginTableRowData(2);
		dbg("Starting row");
		BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
		if($rowcolor) SetRowColorStandard($report, -1, SHADE_MED);
		if (!$bTopOnly==TRUE) $rowcolor=!$rowcolor;
		if($last!=$arrNeeded[$i]["Salesman"])
		{
			if($last != "")
			{
				AddColumnText($report, "   TOTAL:", "", 0, 0);
				AddColumnText($report, "", "", 0, 0);
				AddColumnText($report,"", "", 0, 0);
				AddColumnText($report,"", "", 0, 0);
				AddColumnText($report,"", "", 0, 0);
				AddColumnText($report,"", "", 0, 0);
				AddColumnText($report,$QTotal, "right", 0, 0);
				AddColumnText($report,$YTotal, "right", 0, 0);
				BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
				SetRowColorStandard($report, -1, SHADE_MED);
				$rowcolor=!$rowcolor;
				$QTotal = 0;
				$YTotal = 0;
			}
			AddColumnText($report, $arrNeeded[$i]["Salesman"], "", 0, 0);


			$last=$arrNeeded[$i]["Salesman"];
		}
		else AddColumnText($report, "", "", 0, 0);

		AddColumnText($report, $arrNeeded[$i]["Company"], "left", 0, 0);
		AddColumnText($report,$arrNeeded[$i]["Opportunity"], "left", 0, 0);
		AddColumnText($report,$arrNeeded[$i]["MDUpdate"], "center", 0, 0);
		AddColumnText($report,$arrNeeded[$i]["Location"], "center", 0, 0);
		AddColumnText($report,$arrNeeded[$i]["AlertDate"], "center", 0, 0);
		AddColumnText($report,$arrNeeded[$i]["AlertCountQ"], "right", 0, 0);
		AddColumnText($report,$arrNeeded[$i]["AlertCountYTD"], "right", 0, 0);

		$QTotal += (int)$arrNeeded[$i]["AlertCountQ"];
		$YTotal += (int)$arrNeeded[$i]["AlertCountYTD"];

	}


	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
//	$rowcolor=!$rowcolor;
	if($rowcolor) SetRowColorStandard($report, -1, SHADE_MED);
	AddColumnText($report, "   TOTAL:", "", 0, 0);
	AddColumnText($report, "", "", 0, 0);
	AddColumnText($report,"", "", 0, 0);
	AddColumnText($report,"", "", 0, 0);
	AddColumnText($report,"", "", 0, 0);
	AddColumnText($report,"", "", 0, 0);
	AddColumnText($report,$QTotal, "right", 0, 0);
	AddColumnText($report,$YTotal, "right", 0, 0);
	$QTotal = 0;
	$YTotal = 0;
}

function PrintTotals(&$QTotal, &$YTotal)
{
}

function IsManager($PersonID)
{
	$sql="select Level from People where Personid=$PersonID";
	$res=mssql_query($sql);
	if ($res)
	{
		$row=mssql_fetch_assoc($res);
		if($row)
		{
			return (1<$row["Level"]);
		}
	}
	return false;
}
function GetProducts(&$arrProducts)
{
	$ind=0;
	$sql="select * from products";
	$res=mssql_query($sql);

	while($row=mssql_fetch_assoc($res))
	{
		$arrProducts[$row["ProductID"]]=$row["Name"];
	}
	return 1;
}

/*
function GetOpData($DealID, &$LastCat, //Last category before being removed
							&$LastDays, //Number of days in last category
							&$RemoveDate)
{
	$sql = "select * from log where DealID=$DealID and TransactionID=1 order by WhenChanged DESC";
	$res = mssql_query($sql);
	$rowLast=mssql_fetch_assoc($res);
	$rowPrev=mssql_fetch_assoc($res);
	if(!rowPrev)
	{
		$LastCat=-1;
		return;
	}
	$LastCat=$rowPrev["LogInt"];
	$LastDays=$rowLast["WhenChanged"] - $rowPrev["WhenChanged"];
	$RemoveDate=$rowLast["WhenChanged"];
}
*/
function GetMonth($date)
{
	$str = date("m", $date);
	return (int)$str;
}

function GetYear($date)
{
	$str = date("Y", $date);
	return (int)$str;
}

function GetStartCurrentQuarter()
{
	$today = time();
	$month = GetMonth($today);
	$year = GetYear($today);
	$startmonth;
	if($month < 4) $startmonth = 1;
	else if ($month < 7) $startmonth = 4;
	else if ($month < 10) $startmonth = 7;
	else $startmonth = 10;
//$year=2000;
	$startdate = "$startmonth-1-$year";

	return $startdate;
}


function GetAlertData($DealIDs, &$arrQ, &$arrYTD, &$arrDate)
{
	if(!strlen($DealIDs)) return;

	$dateNow=date("m/d/Y");
	$m=date("m");
	$QStart = GetStartCurrentQuarter();
	$mtY=mktime(0,0,0,1,1,date("Y"));
	$YStart=date("m/d/Y", $mtY);

	$sqlYTD="select DealID, count(DealID) as YTDCount, Max(WhenChanged) as AlertDate from Log where DealID in ($DealIDs) and TransactionID=202 and LogBit=1 and WhenChanged between '$YStart' and '$dateNow' group by DealID";
//print("sql=$sqlYTD  <br>");
	$res=mssql_query($sqlYTD);
	while($row=mssql_fetch_assoc($res))
	{
$d = $row['DealID'];
$y = $row['YTDCount'];
//print("$d $y   <br>");
//$date = $row["AlertDate"];
//print("$date<br>");

		$arrYTD[$row["DealID"]]=$row["YTDCount"];
//		$arrDate[$row["DealID"]]=date("m/d/y",$row["AlertDate"]);
		$arrDate[$row["DealID"]]=$row["AlertDate"];
	}

	$sqlQ="select DealID, count(DealID) as QCount from Log where DealID in ($DealIDs) and TransactionID=202 and LogBit=1 and WhenChanged between '$QStart' and '$dateNow' group by DealID";
	$res=mssql_query($sqlQ);
//print "$sqlQ<br>";
	while($row=mssql_fetch_assoc($res))
	{
//print 'QCount: ' . $row["QCount"] . '<br>';
		$arrQ[$row["DealID"]]=$row["QCount"];
	}
}


function GetSingleSalesman($idPerson, &$arrResults)
{
	$sql="select * from people where personid=$idPerson";
	$res=mssql_query($sql);
	if($res)
		{
		if($row=mssql_fetch_assoc($res))
			{
			$strTst=trim($row["GroupName"]);
			if(strlen($strTst)>0)
				$strName=$strTst;
			else
				{
				$strName=$row['FirstName'];
				$strName.=" ";
				$strName.=$row['LastName'];
				}
			$arrResults[$idPerson]=$strName;
			}
		}

}

function GetMilestones($idPerson, $idDeal, $UseReq1)
{
	$milestones=array();
	for($i=0; $i<6; $i++) $milestones[$i]=0;

	$sql="select * from log where PersonID=$idPerson and DealID=$idDeal and (TransactionID>19 and TransactionID <26) order by WhenChanged";

	$res=mssql_query($sql);
	while($row=mssql_fetch_assoc($res))
	{
		$ind=$row['TransactionID']-20;
		$milestones[$ind]=($row['LogBit']==1);
	}
	$strMilestones="";
	for($i=0; $i<6; $i++)
		if($milestones[$i] > 0)
		{
			if($UseReq1) $strMilestones .= ($i+1);
			else $strMilestones .= $i;
		}
	return $strMilestones;
}

function _GetNeeded(&$report, $idPerson, &$arrResults, &$arrProducts, &$DealIDs, $name, $issales, $UseReq1)
{
	if($issales)
	{
		$sql="select Opportunities.*, A." . $_SESSION['account_name'] . " as Company, (C." . $_SESSION['contact_first_name'] . " + ' ' + C." . $_SESSION['contact_last_name'] . ") AS Contact from Opportunities 
			left join Account A on Opportunities.AccountID = A.accountid
			left join Contact C on Opportunities.ContactID = C.ContactID
			where Category<>9 and Alert3=1 and PersonID=$idPerson ORDER BY A." . $_SESSION['account_name'];
dbg("first SQL=$sql");
	}
	else
	{
		$inclause = GetSalesPeopleTree($idPerson);
dbg("inclause=$inclause");
		if(!strlen($inclause)) return;
		$sql="select Opportunities.*, A." . $_SESSION['account_name'] . " as Company, (C." . $_SESSION['contact_first_name'] . " + ' ' + C." . $_SESSION['contact_last_name'] . ") AS Contact from Opportunities 
			left join Account A on Opportunities.AccountID = A.accountid
			left join Contact C on Opportunities.ContactID = C.ContactID
			where Category<>9 and Alert3=1 and PersonID IN($inclause) ORDER BY PersonID, A." . $_SESSION['account_name'];
dbg("second SQL=$sql");
	}

	$res=mssql_query($sql);

	$ind=0;
	while($row=mssql_fetch_assoc($res))
	{
dbg("Fetched a row");
		$arrTmp=array();
		if(strlen($DealIDs) > 0) $DealIDs .= ",";
		$DealIDs .= $row['DealID'];
		$DealID = $row['DealID'];
		$arrTmp["Salesman"]=$name;
		$arrTmp["DealID"]=$row["DealID"];
		$arrTmp["Company"] =$row["Company"];

		$OppProducts = GetOppProducts($DealID);
		if(strlen($OppProducts) > 0) $arrTmp["Opportunity"] = $OppProducts;
		else $arrTmp["Opportunity"] = $arrProducts[$row["ProductID"]];

		if($row["NextMeeting"]==NULL)
			$arrTmp["MDUpdate"] = date("m/d/y", strtotime($row["FirstMeeting"]));
		else
			$arrTmp["MDUpdate"] = date("m/d/y", strtotime($row["NextMeeting"]));
		if($row['Category']==2)
			{
			$strMilestones=GetMilestones($row['PersonID'], $row['DealID'], $UseReq1);
			$strLoc=GetLoc($row["Category"]);
			$arrTmp["Location"]=GetCatLbl($strLoc) . $strMilestones;
			}
		else
			$arrTmp["Location"]=GetCatLbl(GetLoc($row["Category"]));
		$arrResults[]=$arrTmp;

		$ind++;
	}

	return $ind;
}

function GetNeeded(&$report, $idPerson, &$arrResults, $bTopOnly, $issales, $UseReq1)
{
	$arrProducts=array();
	$DealIDs = "";
	if(!GetProducts($arrProducts))
		{
			return FALSE;
		}
	if($issales)
	{
		$sql = "select * from people where PersonID=$idPerson";
		$result=mssql_query($sql);
		if($row=mssql_fetch_assoc($result))
		{
			$name = $row['FirstName'] . " " . $row['LastName'];
			$id = $row['PersonID'];
			_GetNeeded($report, $id, $arrResults, $arrProducts, $DealIDs, $name, true, $UseReq1);
		}
	}
	else
	{
		$sql = "select * from people where SupervisorID=$idPerson and Level>1 and Deleted=0";
		$result=mssql_query($sql);
		while($row=mssql_fetch_assoc($result))
		{
			$id = $row['PersonID'];
			$name = $row['FirstName'] . " " . $row['LastName'];
			$group = $row['GroupName'];
			if(strlen($group) > 0) $name .= $group;
			_GetNeeded(&$report, $id, $arrResults, $arrProducts, $DealIDs, $name, false, $UseReq1);
		}

		$sql = "select * from people where (SupervisorID=$idPerson OR PersonID=$idPerson) and IsSalesperson=1 and Deleted=0 ORDER BY LastName,FirstName";
		$result=mssql_query($sql);
		while($row=mssql_fetch_assoc($result))
		{
			$id = $row['PersonID'];
			$name = $row['FirstName'] . " " . $row['LastName'];
			$group = $row['GroupName'];
			if(strlen($group) > 0) $name .= $group;
			_GetNeeded(&$report, $id, $arrResults, $arrProducts, $DealIDs, $name, true, $UseReq1);
		}
	}
	$arrAlertsYTD=array();
	$arrAlertsQ=array();
	$arrAlertDates=array();
	GetAlertData($DealIDs, $arrAlertsQ, $arrAlertsYTD, $arrAlertDates);
	for($ind=0;$ind<count($arrResults);$ind++)
	{
		$idDeal=$arrResults[$ind]["DealID"];

		$arrResults[$ind]["AlertDate"]=date("m/d/y", strtotime($arrAlertDates[$idDeal]));

		$q=$arrAlertsQ[$idDeal];
		$y = $arrAlertsYTD[$idDeal];

		$arrResults[$ind]["AlertCountQ"]=(int)$q;
		$arrResults[$ind]["AlertCountYTD"]=(int)$y;
	}
	//sort($arrResults);

	return TRUE;
}

/*
function GetNeeded(&$report, $idPerson, &$arrResults, $bTopOnly)
{
	$arrProducts=array();
	if(!GetProducts($arrProducts))
		{
			return FALSE;
		}

	$arrOps=array();
	$arrPeople=array();
	$strIDs=GetSubordinateList($idPerson, $arrPeople, $bTopOnly);
	if(!count($arrPeople))
	{
		$strIDs=$idPerson;
		GetSingleSalesman($idPerson, $arrPeople);
	}
	if(!strlen($strIDs))
	{
		BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, .25);
		AddFreeFormText($report, "Personnel not found", -1, "center", 1.5, 5.5);


		return FALSE;
	}
//Need to weed out recs where meeting date > alert date ???
	$sql="select * from Opportunities where Category<>9 and Alert3=1 and PersonID in ($strIDs) order by PersonID, Company";
//print("sql=$sql<br>");
	$res=mssql_query($sql);

	$ind=0;
	$strDealIDs="";
	while($row=mssql_fetch_assoc($res))
	{
		if(strlen($strDealIDs)>1) $strDealIDs.=",";
		$strDealIDs.=$row["DealID"];
		$arrTmp=array();
		$arrTmp["Salesman"]=$arrPeople[$row["PersonID"]];
		$arrTmp["DealID"]=$row["DealID"];
		$arrTmp["Company"] =$row["Company"];
		$arrTmp["Opportunity"] = $arrProducts[$row["ProductID"]];
		if($row["NextMeeting"]==NULL)
			$arrTmp["MDUpdate"] = date("m/d/y", strtotime($row["FirstMeeting"]));
		else
			$arrTmp["MDUpdate"] = date("m/d/y", strtotime($row["NextMeeting"]));
		if($row['Category']==2)
			{
			$strMilestones=GetMilestones($row['PersonID'], $row['DealID']);
			$strLoc=GetLoc($row["Category"]);
			$arrTmp["Location"]=GetCatLbl($strLoc) . $strMilestones;
			}
		else
			$arrTmp["Location"]=GetCatLbl(GetLoc($row["Category"]));
		$arrResults[$ind]=$arrTmp;

		$ind++;
	}

	if(!strlen($strDealIDs))
	{
		BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, .25);
		AddFreeFormText($report, "No data!", -1, "center", 1.5, 5.5);
		return FALSE;
	}
	$arrAlertsYTD=array();
	$arrAlertsQ=array();
	$arrAlertDates=array();
	GetAlertData($strDealIDs, $arrAlertsQ, $arrAlertsYTD, $arrAlertDates);
	for($ind=0;$ind<count($arrResults);$ind++)
	{
		$idDeal=$arrResults[$ind]["DealID"];
		$arrResults[$ind]["AlertDate"]=date("m/d/y", strtotime($arrAlertDates[$idDeal]));

		$q=$arrAlertsQ[$idDeal];
		$y = $arrAlertsYTD[$idDeal];
//print("$idDeal   q=$q   y=$y <br>");

		$arrResults[$ind]["AlertCountQ"]=(int)$q;
		$arrResults[$ind]["AlertCountYTD"]=(int)$y;
	}
	sort($arrResults);

	return TRUE;
}
*/


close_db();

?>
