<?php
 /**
 * @package Reports
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_paul_utils.php');
include('GetSalespeopleTree.php');
include('category_labels.php');

$report_name = 'Dashboard Progression';
if (!isset($treeid))
{
	begin_report_orig($report_name);
	print_tree_prompt(ANYBODY);
	end_report_orig();
	exit();
}

if (!isset($dateFrom) || !isset($dateTo))
{
	begin_report_orig($Source2Label.' Report');
	?>
	<center style="font-family:Arial; font-size:12pt;">Please select the date span to be used for the inclusion of closed opportunities.</center>
	<?
	$sortcol=0;
	$ord=0;
	$tomorrow = date("m/d/Y", strtotime("+1 day"));
	print_date_params($treeid,$sortcol,$ord, "datefr:<$tomorrow", "dateto:<$tomorrow");
	if(!isset($dateFrom)) $dateFrom="N/A";
	if(!isset($dateTo)) $dateTo="N/A";
	end_report_orig();
	exit();
}

begin_report($report_name);

$id_list = GetSalespeopleTree($treeid);
$id_count = substr_count($id_list, ",") + 1;

$sqlstr = "select TAbbr, FMAbbr, IPAbbr, DPAbbr, CAbbr, RAbbr, MoveShowTarget from company where CompanyID = $mpower_companyid";
$result = mssql_query($sqlstr);
$reportLabels = mssql_fetch_assoc($result);
$t_label = ($reportLabels['TAbbr'] != NULL) ? $reportLabels['TAbbr'] : "T";
$fm_label = ($reportLabels['FMAbbr'] != NULL) ? $reportLabels['FMAbbr'] : "FM";
$ip_label = ($reportLabels['IPAbbr'] != NULL) ? $reportLabels['IPAbbr'] : "IP";
$dp_label = ($reportLabels['DPAbbr'] != NULL) ? $reportLabels['DPAbbr'] : "DP";
$c_label = ($reportLabels['CAbbr'] != NULL) ? $reportLabels['CAbbr'] : "C";
$r_label = ($reportLabels['RAbbr'] != NULL) ? $reportLabels['RAbbr'] : "R";


if ($reportLabels['MoveShowTarget'] == 1) {
    $include_targets = TRUE;
}

$movement_sql = <<<SQL_END

    SELECT mv, COUNT(mv)  AS mvCount FROM
    (
    	SELECT COALESCE(CONVERT(VARCHAR, SRC_CAT), '0') + '-' + CONVERT(VARCHAR, DEST_CAT) AS mv, * FROM
    	(
    		SELECT l1.DealID
    		, l1.CompanyID
    		, l1.PersonID
    		, l1.TransactionID
    		, l1.WhenChanged
    		, l1.LogInt AS DEST_CAT
    		, (
    			SELECT TOP 1 LogInt 
    			FROM Log l2 
    			WHERE L2.TransactionID = 1 
    				AND l2.DealID = l1.DealID 
    				AND l2.WhenChanged < l1.WhenChanged 
    			ORDER BY WhenChanged DESC
    		) SRC_CAT
    		FROM log l1
    		WHERE l1.TransactionID = 1
    		AND l1.PersonID IN ($id_list)
    		AND l1.WhenChanged BETWEEN '$dateFrom' AND '$dateTo'
    	) movement
    ) SRC
    GROUP BY mv
SQL_END;
//echo $movement_sql . "<br>";
$movement_result = mssql_query($movement_sql);

// Movement defs
define ("TFM",  1); // T to FM
define ("TIP",  2); // T to IP
define ("TDP",  3); // T to DP
define ("TC",   4); // T to C
define ("TR",   5); // T to R
define ("TT",   6); // T Total
define ("FMIP", 7); // FM to IP
define ("FMDP", 8); // FM to DP
define ("FMC",  9); // FM to C
define ("FMR", 10); // FM to R
define ("FMT", 11); // FM Total
define ("IPDP",12); // IP to DP
define ("IPC", 13); // IP to C
define ("IPR", 14); // IP to R
define ("IPT", 15); // IP Total
define ("DPC", 16); // DP to C
define ("DPR", 17); // DP to R
define ("DPT", 18); // DP Total

$movements = array();
while ($row = mssql_fetch_assoc($movement_result)) {
    switch($row['mv']) {
        case '10-1':
            $movements[TFM] += $row['mvCount'];
            $movements[TT] += $row['mvCount'];
            break;
        case '10-2':
        case '10-3':
            $movements[TIP] += $row['mvCount'];
            $movements[TT] += $row['mvCount'];
            break;
        case '10-4':
        case '10-5':
            $movements[TDP] += $row['mvCount'];
            $movements[TT] += $row['mvCount'];
            break;
        case '10-6':
            $movements[TC] += $row['mvCount'];
            $movements[TT] += $row['mvCount'];
            break;
        case '10-9':
            $movements[TR] += $row['mvCount'];
            $movements[TT] += $row['mvCount'];
            break;
        case '0-2':
        case '0-3':
        case '1-2':
        case '1-3':
            $movements[FMIP] += $row['mvCount'];
            $movements[FMT] += $row['mvCount'];
            break;

        case '0-4':
        case '0-5':
        case '1-4':
        case '1-5':
            $movements[FMDP] += $row['mvCount'];
            $movements[FMT] += $row['mvCount'];
            break;
        case '0-6':
        case '1-6':
            $movements[FMC] += $row['mvCount'];
            $movements[FMT] += $row['mvCount'];
            break;
        case '1-9':
            $movements[FMR] += $row['mvCount'];
            $movements[FMT] += $row['mvCount'];
            break;
        case '2-4':
        case '3-4':
        case '2-5':
        case '3-5':
            $movements[IPDP] += $row['mvCount'];
            $movements[IPT] += $row['mvCount'];
            break;
        case '2-6':
        case '3-6':
            $movements[IPC] += $row['mvCount'];
            $movements[IPT] += $row['mvCount'];
            break;
        case '2-9':
        case '3-9':
            $movements[IPR] += $row['mvCount'];
            $movements[IPT] += $row['mvCount'];
            break;
        case '4-6':
        case '5-6':
            $movements[DPC] += $row['mvCount'];
            $movements[DPT] += $row['mvCount'];
            break;
        case '4-9':
        case '5-9':
            $movements[DPR] += $row['mvCount'];
            $movements[DPT] += $row['mvCount'];
            break;
        default:
             //echo 'Ignoring case: ' . $row['mv'] . '<br>';
    }
    //echo $row['mv'] . " -> " . $row['mvCount'] . "<br>";
}

if (!$include_targets) {
    $movements[FMIP] += $movements[TIP];
    $movements[FMDP] += $movements[TDP];
    $movements[FMC] += $movements[TC];
    $movements[FMT] += $movements[TIP];
    $movements[FMT] += $movements[TDP];
    $movements[FMT] += $movements[TC];
}

$sqlstr = "select FirstName, LastName from people where PersonID = $treeid";
$result = mssql_query($sqlstr);
$reportPerson = mssql_fetch_assoc($result);

?>
		<link rel="stylesheet" type="text/css" href="../css/report.css">
		</td>
	</tr>
</table>
<br>

<center style="font-size:12pt; font-weight:bold;"><?php echo $reportPerson['FirstName'] . ' ' . $reportPerson['LastName']; ?></center>
<br>
<?php if ($id_count > 1) : ?>
<center style="font-size:10pt;">Rolled up from <?php echo $id_count; ?> salespeople</center>
<?php endif; ?>
<center style="font-size:10pt;">For date range: <?php echo $dateFrom . ' to ' . $dateTo; ?></center>
<br>
<center>
    <table class="report">
        <tr>
            <th>
        <?php if ($include_targets): ?>
            <?php echo $t_label ?> Moves</th>
            <th>#</th>
            <th>%</th>
            <th class="thickleft">
        <?php endif; ?>            
            <?php echo $fm_label ?> Moves</th>
            <th>#</th>
            <th>%</th>
            <th class="thickleft"><?php echo $ip_label ?> Moves</th>
            <th>#</th>
            <th>%</th>
            <th class="thickleft"><?php echo $dp_label ?> Moves</th>
            <th>#</th>
            <th>%</th>
        </tr>
        <tr>
            <td>
        <?php if ($include_targets): ?>
            <?php echo $t_label ?> > <?php echo $fm_label ?></td>
            <td class="jr"><?php echo $movements[TFM] ?></td>
            <td class="jr"><?php echo DispPercent($movements[TFM], $movements[TT]) ?></td>
            <td class="thickleft">
        <?php endif; ?>            
            <?php echo $fm_label ?> > <?php echo $ip_label ?></td>
            <td class="jr"><?php echo $movements[FMIP] ?></td>
            <td class="jr"><?php echo DispPercent($movements[FMIP], $movements[FMT]) ?></td>
            <td class="thickleft"><?php echo $ip_label ?> > <?php echo $dp_label ?></td>
            <td class="jr"><?php echo $movements[IPDP] ?></td>
            <td class="jr"><?php echo DispPercent($movements[IPDP], $movements[IPT]) ?></td>
            <td class="thickleft"><?php echo $dp_label ?> > <?php echo $c_label ?></td>
            <td><?php echo $movements[DPC] ?></td>
            <td><?php echo DispPercent($movements[DPC], $movements[DPT]) ?></td>
        </tr>
        <tr>
            <td>
        <?php if ($include_targets): ?>
            <?php echo $t_label ?> > <?php echo $ip_label ?></td>
            <td class="jr"><?php echo $movements[TIP] ?></td>
            <td class="jr"><?php echo DispPercent($movements[TIP], $movements[TT]) ?></td>
            <td class="thickleft">
        <?php endif; ?>
            <?php echo $fm_label ?>  > <?php echo $dp_label ?></td>
            <td class="jr"><?php echo $movements[FMDP] ?></td>
            <td class="jr"><?php echo DispPercent($movements[FMDP], $movements[FMT]) ?></td>
            <td class="thickleft"><?php echo $ip_label ?> > <?php echo $c_label ?></td>
            <td class="jr"><?php echo $movements[IPC] ?></td>
            <td class="jr"><?php echo DispPercent($movements[IPC], $movements[IPT]) ?></td>
            <td class="thickleft unused">&nbsp;</td>
            <td class="unused">&nbsp;</td>
            <td class="unused">&nbsp;</td>
        </tr>
        <tr>
            <td>
        <?php if ($include_targets): ?>
            <?php echo $t_label ?> > <?php echo $dp_label ?></td>
            <td class="jr"><?php echo $movements[TDP] ?></td>
            <td class="jr"><?php echo DispPercent($movements[TDP], $movements[TT]) ?></td>
            <td class="thickleft">
        <?php endif; ?>            
            <?php echo $fm_label ?> > <?php echo $c_label ?></td>
            <td class="jr"><?php echo $movements[FMC] ?></td>
            <td class="jr"><?php echo DispPercent($movements[FMC], $movements[FMT]) ?></td>
            <td class="thickleft unused">&nbsp;</td>
            <td class="unused">&nbsp;</td>
            <td class="unused">&nbsp;</td>
            <td class="thickleft unused">&nbsp;</td>
            <td class="unused">&nbsp;</td>
            <td class="unused">&nbsp;</td>
        </tr>
        <?php if ($include_targets): ?>
        <tr>
            <td><?php echo $t_label ?> > <?php echo $c_label ?></td>
            <td class="jr"><?php echo $movements[TC] ?></td>
            <td class="jr"><?php echo DispPercent($movements[TC], $movements[TT]) ?></td>
            <td class="thickleft unused">&nbsp;</td>
            <td class="unused">&nbsp;</td>
            <td class="unused">&nbsp;</td>
            <td class="thickleft unused">&nbsp;</td>
            <td class="unused">&nbsp;</td>
            <td class="unused">&nbsp;</td>
            <td class="thickleft unused">&nbsp;</td>
            <td class="unused">&nbsp;</td>
            <td class="unused">&nbsp;</td>
        </tr>
        <?php endif; ?>            
        <tr>
            <td>
        <?php if ($include_targets): ?>
            <?php echo $t_label ?> > <?php echo $r_label ?></td>
            <td class="jr"><?php echo $movements[TR] ?></td>
            <td class="jr"><?php echo DispPercent($movements[TR], $movements[TT]) ?></td>
            <td class="thickleft">
        <?php endif; ?>            
            <?php echo $fm_label ?> > <?php echo $r_label ?></td>
            <td class="jr"><?php echo $movements[FMR] ?></td>
            <td class="jr"><?php echo DispPercent($movements[FMR], $movements[FMT]) ?></td>
            <td class="thickleft"><?php echo $ip_label ?> > <?php echo $r_label ?></td>
            <td class="jr"><?php echo $movements[IPR] ?></td>
            <td class="jr"><?php echo DispPercent($movements[IPR], $movements[IPT]) ?></td>
            <td class="thickleft"><?php echo $dp_label ?> > <?php echo $r_label ?></td>
            <td class="jr"><?php echo $movements[DPR] ?></td>
            <td class="jr"><?php echo DispPercent($movements[DPR], $movements[DPT]) ?></td>
        </tr>
        <tr>
            <th>
        <?php if ($include_targets): ?>
            Total</th>
            <th class="jr"><?php echo $movements[TT] ?></th>
            <th>&nbsp;</th>
            <th class="thickleft">
        <?php endif; ?>            
            Total</th>
            <th class="jr"><?php echo $movements[FMT] ?></th>
            <th>&nbsp;</th>
            <th class="thickleft">Total</th>
            <th class="jr"><?php echo $movements[IPT] ?></th>
            <th>&nbsp;</th>
            <th class="thickleft">Total</th>
            <th class="jr"><?php echo $movements[DPT] ?></th>
            <th>&nbsp;</th>
        </tr>

    </table>
</center>
<?php
end_report();
?>

<?php

function DispPercent($num, $denom) {
    if ($denom != 0) {
        $percent = round($num / $denom * 1000) / 10;
        return ($percent == 0) ? "&nbsp;" : sprintf("%01.1f", $percent);
    }
    else {
        return "-";
    }

}

?>
