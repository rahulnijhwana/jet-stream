<?php
 /**
 * @package Reports
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_report_utils.php');
include('_date_params.php');

$showmult = false;

begin_report('Board Snapshot Report');

$please_get_out = false;

if (!isset($snapmonth))
{
	//$snapmonth
}

if (!isset($treeid))
{
	if($showmult) print_tree_prompt(MANAGEMENT);
	else print_tree_prompt(ANYBODY);
//	$please_get_out = true;
	end_report();
	exit();
}

if (!isset($dateFrom)||!isset($dateTo))
	{
	$sortcol=0;
	$ord=0;
	$today=date("m/d/Y");
	print_date_params($treeid,$sortcol,$ord, "datefr:<$today", "dateto:<$today");
	if(!isset($dateFrom)) $dateFrom="N/A";
	if(!isset($dateTo)) $dateTo="N/A";
	end_report();
	exit();
	}


if ($please_get_out)
{
	end_report();
	exit();
}

print("<meta http-equiv=\"Refresh\" content=\"1; URL=snapshot1_pdf.php?SN=".$_REQUEST['SN']."&treeid=$treeid&dateFrom=$dateFrom&dateTo=$dateTo&showmult=$showmult\">");
print('<br><br><b>     <div align="center"><span>Please wait while your report is being prepared.</span><br /><span><img src="../images/indicator.gif" alt="Loading..." /></span></div>');
// print('<br><br><b>     Please wait while your report is being prepared.<br><br><iframe style="display:none;" src="/develop/reports/sample.pdf"></iframe>');
end_report();
?>
