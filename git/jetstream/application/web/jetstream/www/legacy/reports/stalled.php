<?php
 /**
 * @package Reports
 * Stalled Opportunities Report
 */

// Definitions --------------------------------------------------------------------------------
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('_report_utils.php');
include('_export_utils.php');
include('TableUtils.php');
include('category_labels.php');

//include('firstmeeting_utils.php');
include('GetSalespeopleTree.php');

global $report_export_option;

// Functions -----------------------------------------------------------------------------
function GetProducts(&$Prods, $id)
{
	$sql = "select * from products where companyid = $id";
	$ok = mssql_query($sql);
	if($ok)
	{
		while($row = mssql_fetch_assoc($ok))
		{
			$id=$row['ProductID'];
			$Prods[$id] = $row['Name'];
		}
	}
}

function GetSources(&$Sources, $id)
{
	$sql = "select * from sources where companyid = $id";
	$ok = mssql_query($sql);
	if($ok)
	{
		while($row = mssql_fetch_assoc($ok))
		{
			$id=$row['SourceID'];
			$Sources[$id] = $row['Name'];
		}
	}
}


function GetStalledOpps(&$opps, $supervisorid, $prods, $sources)
{
	$inlist = GetSalespeopleTree($supervisorid);
	$sql = "SELECT opportunities.*, A." . $_SESSION['account_name'] . " as Company
		FROM  opportunities INNER JOIN people ON opportunities.PersonID = people.PersonID
		LEFT JOIN Account A on opportunities.AccountID = A.accountid";
	$sql .= " WHERE (people.PersonID in ($inlist)) AND (people.Deleted = 0)";
	$sql .= " AND (opportunities.Category = 3 OR opportunities.Category = 5) 
		order by people.LastName, people.FirstName, opportunities.Category, A." . $_SESSION['account_name'];

	$ok=mssql_query($sql);
	if($ok)
	{
		while($row = mssql_fetch_assoc($ok))
		{
			$temp = array();
			$DealID = $row['DealID'];

			$temp['PersonID'] = $row['PersonID'];
			$temp['name'] = $row['LastName'] . ", " . $row['FirstName'];
			$temp['category'] = $row['Category'] == 3 ? GetCatLbl('Stalled').' '.GetCatLbl('IP') : GetCatLbl('Stalled').' '.GetCatLbl('DP');
			$temp['lastmoved'] = Date("m/d/y", strtotime($row['LastMoved']));
			$temp['daysold'] = CalcDays($row['LastMoved']);
			$temp['firstmeeting'] = date("m/d/y", strtotime($row['FirstMeeting']));
			$temp['company'] = $row['Company'];
			$temp['division'] = $row['Division'];

			$OppProducts = GetOppProducts($DealID);
			if(strlen($OppProducts) > 0) $temp['prodname'] = $OppProducts;
			else $temp['prodname'] = $prods[$row['ProductID']];

			if(is_null($temp['prodname'])) $temp['prodname'] = 'Unk.';
			$temp['sourcename'] = $sources[$row['SourceID']];
			if(is_null($temp['sourcename'])) $temp['sourcename'] = 'Unk.';
			$temp['vlevel'] = $row['VLevel'] > 0 ? $row['VLevel'] : 'Unk';
			array_push($opps, $temp);
		}
	}
}

function datediff($interval, $date1, $date2)
{
 $s = strtotime($date2)-strtotime($date1);
 $d = intval($s/86400);
 $s -= $d*86400;
 $h = intval($s/3600);
 $s -= $h*3600;
 $m = intval($s/60);
 $s -= $m*60;
 $arrRes=array("d"=>$d,"h"=>$h,"m"=>$m,"s"=>$s);
 return $arrRes[$interval];
}

function CalcDays($dateLastMoved)
{
	if(!strlen($dateLastMoved)) return 0;
	$now = Date("m/d/y");
	$then = Date("m/d/y", strtotime($dateLastMoved));
	return datediff("d", $then, $now);
}


function PrintColumnHeaders()
{
	global $comprow;
	BeginTableRowData(1);
	PrintTableCell("<b>Company");
	PrintTableCell("<b>Offering");
	if($comprow['sOK']) PrintTableCell("<b>Source");
	PrintTableCell("<b>" . GetCatLbl('First Meeting'));
	if($comprow['vOK']) PrintTableCell("<b>Val Level");
	PrintTableCell("<b>Entered " . GetCatLbl('Stalled'));
	PrintTableCell("<b>Days Old");
	EndTableRow();
}

function PrintBlankRow()
{
	global $colcount;
	BeginTableRow();
	PrintTableSpan($colcount,"<br>");
	EndTableRow();
}

// Inline Code -----------------------------------------------------------------------------
if(isset($_SESSION['ExportArray'])) {
	// Check for export before loading anything else.
	// The export headers need to be first.
	// If you put anything other then the export data in this condition, it will appear in the export file.
	if (isset($_GET['export'])) {
		//export_csv("stalled_opp_rpt");
		export_xls("stalled_opp_rpt");
		exit();
	}

	// Reset variable holding report detail for Export file
	unset($_SESSION['ExportArray']);
}

//***** Salesrep Prompt
if (!isset($treeid))
{
	begin_report(GetCatLbl('Stalled') . ' Opportunities Report');
	print_tree_prompt(MANAGEMENT);
	end_report();
	exit();
}

$report_export_option = 1;
begin_report(GetCatLbl('Stalled') . ' Opportunities Report');
$report_export_option = 0;

$compsql = "select ValuationUsed as vOK, SourceUsed as sOK from Company where CompanyID=$mpower_companyid";
$ok=mssql_query($compsql);
$comprow = mssql_fetch_assoc($ok);

$colcount=5;
if($comprow['vOK']) $colcount++;
if($comprow['sOK']) $colcount++;

print_tree_path($treeid);

$strdate = date("m/d/y", time());
print("<br><b>Date run:   $strdate</b><br><br>");

$personid = $treeid;

$companyid = $mpower_companyid;

$cellfontsize="16px";

BeginTable();

// Print the report title
BeginTableRowData(1);
PrintTableSpan($colcount, "<b>" . GetCatLbl('Stalled') . " Report");
EndTableRow();

// Print the column headers
PrintColumnHeaders();
$ColHdrs=1;

// Access the database
$Prods = array();
GetProducts($Prods, $companyid);
$Sources = array();
GetSources($Sources, $companyid);
$Opps=array();
GetStalledOpps($Opps, $personid, $Prods, $Sources);
$count = count($Opps);

// Initialize
$totals=array();
$lastSP='';
$lastCat='';
$catcount=0;
$totaldays=0;

$arrCSV = array();
$tmpCSV = array();

// Load the column headers as themselves
$tmpCSV[0] = 'Level1';
$tmpCSV[1] = 'Level2';
$tmpCSV[2] = 'Level3';
$tmpCSV[3] = 'Level4';
$tmpCSV[4] = 'Level5';
$tmpCSV[5] = 'Level6';
$tmpCSV[6] = 'Company';
$tmpCSV[7] = 'Offering';
$tmpCSV[8] = 'Source';
$tmpCSV[9] = 'FirstMeeting';
$tmpCSV[10] = 'ValLevel';
$tmpCSV[11] = 'EnteredStalled';
$tmpCSV[12] = 'DaysOld';
array_push($arrCSV, $tmpCSV);

// Loop through the stalled opps to get some counts
$i=0;
while(1)
{
	if($i == $count) break;
	$rec=$Opps[$i++];

	if(strlen($lastSP) &&($i == $count || $lastSP != $rec['name'] || $lastCat != $rec['category']))
	{
		$tmp=array();
		$tmp['name']=$lastSP;
		$tmp['category'] = $lastCat;
		$tmp['totaldays'] = $totaldays;
		$tmp['catcount'] = $catcount;
		array_push($totals, $tmp);
		$totaldays=0;
		$catcount=0;
	}
	$lastSP = $rec['name'];
	$lastCat = $rec['category'];
	$totaldays += $rec['daysold'];
	$catcount++;
}

// Loop through the stalled opps again to print some rows
$indSP=0;
for($i=0; $i<$count; $i++)
{
	$rec = $Opps[$i];
	if($rec['name'] != $lastSP || $rec['category'] != $lastCat)
	{
		$name = $totals[$indSP]['name'];
		$cat = $totals[$indSP]['category'];
		$catcount = $totals[$indSP]['catcount'];
		$totaldays = $totals[$indSP]['totaldays'];
		$avg=0;
		if($catcount > 0) $avg = round($totaldays / $catcount);
		if($rec['name'] != $lastSP )
		{
			PrintBlankRow();
			BeginTableRowData(1);
			PrintTableSpan($colcount,"$name");
			EndTableRow();
			$ColHdrs=0;
		}
		BeginTableRowData(1);
		PrintTableSpan($colcount, "$cat: $catcount Opportunities Average $avg Days Old");
		EndTableRow();
		$ColHdrs=0;
		
		$indSP++;
		$lastSP=$rec['name'];
		$lastCat = $rec['category'];
		
		// Print column headers on a break if we didn't just print them
		If($ColHdrs==0) PrintColumnHeaders();
	}
	
	BeginTableRow();
	$tempcompany = $rec['company'];
	if ($rec['division'] != '')
		$tempcompany .= '&nbsp;-&nbsp;'.$rec['division'];
	PrintTableCell($tempcompany);
	PrintTableCell($rec['prodname']);
	if($comprow['sOK'])
		PrintTableCell($rec['sourcename']);
	PrintTableCell($rec['firstmeeting']);
	if($comprow['vOK'])
		PrintTableCell($rec['vlevel']);
	PrintTableCell($rec['lastmoved']);
	PrintTableCell($rec['daysold']);
	EndTableRow();
	$ColHdrs=0;
	
	// Get a branch from the selected rep level up
	//$salesbranch = GetSalesPersonBranch($personid);
	$salesbranch = GetSalesPersonBranch($rec['PersonID']);
	
	// Add to CSV array
	$tmpCSV = array();
	
	$tmpCSV[0] = $salesbranch[0];
	$tmpCSV[1] = $salesbranch[1];
	$tmpCSV[2] = $salesbranch[2];
	$tmpCSV[3] = $salesbranch[3];
	$tmpCSV[4] = $salesbranch[4];
	$tmpCSV[5] = $salesbranch[5];
	$tempcompany = $rec['company'];
	if ($rec['division'] != '')
		$tempcompany .= ' - '.$rec['division'];
	$tmpCSV[6] = $tempcompany;
	$tmpCSV[7] = $rec['prodname'];
	$tmpCSV[8] = $rec['sourcename'];
	$tmpCSV[9] = $rec['firstmeeting'];
	$tmpCSV[10] = $rec['vlevel'];
	$tmpCSV[11] = $rec['lastmoved'];
	$tmpCSV[12] = $rec['daysold'];
	array_push($arrCSV, $tmpCSV);
}

EndTable();

// load to session
$_SESSION['ExportArray'] = $arrCSV;

end_report();
?>
