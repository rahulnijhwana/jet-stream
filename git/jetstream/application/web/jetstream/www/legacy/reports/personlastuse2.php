<?php
 /**
 * @package Reports
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');

SessionManager::Init();
SessionManager::Validate();

include('../reports/_report_utils.php');
include('../reports/TableUtils.php');
include('../reports/path.php');
include('../reports/ci_pdf_utils.php');
include('../reports/GetSalespeopleTree.php');

define(SHADE_MED, "MED_BLUE");
define(SHADE_LIGHT, "LIGHT_BLUE");
$hilight;// boolean used to track color of report row (dark, light, dark ... etc) 

if(isset($CompanyID))
	$sql = "select Name, NonUseGracePeriod from Company where companyid=$CompanyID";
else
	$sql = "select Name, NonUseGracePeriod from Company where companyid=$mpower_companyid";

$ok = mssql_query($sql);
$row = mssql_fetch_assoc($ok);

$GracePeriod = $row['NonUseGracePeriod'];
$CompanyName = $row['Name'];

// Get PersonID of the user logged in
$sql = "SELECT PersonID FROM Logins WHERE ID = $currentuser_loginid";
$ok = mssql_query($sql);
$row = mssql_fetch_assoc($ok);
$LoggedInPersonID = $row["PersonID"];

$tsGraceDate = time() - ($GracePeriod * 24 * 60 * 60);

$title = "Last Login Report";

$report = CreateReport();
$report['footertext'] = $title;
$report['left'] = 72/4;
$report['right'] = 8.25 * 72;

$p = PDF_new();

PDF_set_parameter($p, "license", PDFLIB_LICENSE_KEY); // add this back in production

PDF_open_file($p, "");

StartPDFReport($report, $p, "A", "B", "C");

SetLogo($report, "report_logo.jpg", 10, 40, 72, 0);

// Get Data of everyone under top person
$arrLastLoginData = GetLastLoginData($treeid);

// Get Totals from everyone under top person
GetTotals($arrLastLoginData, $totPeople, $totGP, $tot30, $tot60, $totOver, $totNever);

BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, 0.1);
AddFreeFormText($report, $title, -1, "center", 1.5, 5.5);

BeginNewRow($report, RT_FREEFORM, NORMAL_FONTSIZE, 0.25);

if (isset($treeid))
	AddFreeFormText($report, make_tree_path($treeid), -1, "center", 1.5, 5.5);
else
	AddFreeFormText($report, $CompanyName, -1, "center", 1.5, 5.5);

//--------- Begin Print Totals

BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0.2);
AddColumn($report, "TOTALS", "right", 1.5, 0);
AddColumn($report, " ", "left", 0.5, 0);

BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
AddColumnText($report, "People:", "right", 0, 0);
AddColumnText($report, $totPeople, "right", 0, 0);

BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
AddColumnText($report, "Within $GracePeriod days:", "right", 0, 0);
AddColumnText($report, $totGP, "right", 0, 0);

BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
$GPPlus = $GracePeriod + 1;
AddColumnText($report, "$GPPlus to 30 days:", "right", 0, 0);
AddColumnText($report, $tot30, "right", 0, 0);

BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
AddColumnText($report, "31 to 60 days:", "right", 0, 0);
AddColumnText($report, $tot60, "right", 0, 0);

BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
AddColumnText($report, "Over 60 days:", "right", 0, 0);
AddColumnText($report, $totOver, "right", 0, 0);

BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
AddColumnText($report, "Never logged in:", "right", 0, 0);
AddColumnText($report, $totNever, "right", 0, 0);

BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
AddColumnText($report, ' ', "left", 0, 0);

BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
AddColumnText($report, ' ', "left", 0, 0);

//------------End Totals

//------------Begin Details

BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0.2);
SetColColorStandard($report, -1, -1, SHADE_MED);
AddColumn($report, "", "left", 0.1, 0);
AddColumn($report, "", "left", 0.1, 0);
AddColumn($report, "", "left", 0.1, 0);
AddColumn($report, "", "left", 0.1, 0);
AddColumn($report, "", "left", 0.1, 0);
AddColumn($report, "", "left", 0.1, 0);
AddColumn($report, "People", "left", 2.5, 0);
AddColumn($report, "Days Since Last Login", "center", 0.8, 0);
AddColumn($report, "Within Grace Period\n($GracePeriod Days)", "center", 0.8, 0);
$postgrace = $GracePeriod + 1;
AddColumn($report, "$postgrace to 30 Days", "center", 0.8, 0);
AddColumn($report, "31 to 60 Days", "center", 0.8, 0);
AddColumn($report, "Over 60 Days", "center", 0.80, 0);

	// using recursion, find and print people
	$indentlevel = 0;
	if ($LoggedInPersonID == $treeid)// Logged-in person chose self
	{
		// don't include Logged-in person in results
		PrintLastLoginDataTree($report, $arrLastLoginData, $treeid, $indentlevel);
	}
	else // Logged-in chose someone under them
	{	
		// include chosen person in results
		for ($i=0; $i < count($arrLastLoginData); $i++)
		{
			if($arrLastLoginData[$i]['PersonID'] == $treeid)
			{
				$indentlevel += 1;
				PrintPersonRec($report, $arrLastLoginData[$i], $indentlevel);
				PrintLastLoginDataTree($report, $arrLastLoginData, $arrLastLoginData[$i]['PersonID'], $indentlevel);
			}
		}
	}


//------------End Details

PrintPDFReport($report, $p);

PDF_delete($p);

close_db();

/******************************************************************************
	Functions ...
******************************************************************************/


// Called by main code
function GetLastLoginData($treeid)
{
	// Get list of people in the company
	$sql = " SELECT PersonID, SupervisorID FROM people";
	$sql .= " WHERE CompanyID = (SELECT companyID FROM people WHERE personID=$treeid)";
	$sql .= " AND (people.Deleted = 0) AND (people.SupervisorID NOT IN (-1,-3))";
	
	// put results in an array for processing
	$result = mssql_query($sql);
	$peopleArray = array();
	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$aperson = array();
			$aperson['PersonID'] = $row['PersonID'];
			$aperson['SupervisorID'] = $row['SupervisorID'];
			array_push($peopleArray, $aperson);
		}
	}
	
	// use array to create inclause of ids of everyone under top (treeid) person 
	$inclause = "0,"; //set initial id value to zero. The ensures query will work even if top person has no one under them
	GetPeopleTree($peopleArray, $inclause, $treeid);
	
	global $LoggedInPersonID;
	if ($LoggedInPersonID == $treeid)
		$inclause = trim($inclause,",");
	else
		$inclause = $inclause . $treeid;

	$sql = "";		
	$sql .= " SELECT people.PersonID, people.IsSalesperson, company.Name as Comp,";
	$sql .= " people.LastName + ', ' + people.FirstName AS Person, people.Level AS PersonLevel,";
	$sql .= " MAX([log].WhenChanged) AS LastLogin, SupervisorID";
	$sql .= " FROM Company INNER JOIN People ON company.CompanyID = People.CompanyID";
	$sql .= " LEFT JOIN Log ON log.PersonID = people.PersonID and log.transactionid = 300";
	$sql .= " WHERE people.PersonID IN ($inclause)";
	$sql .= " GROUP BY company.Name, people.LastName, people.FirstName, people.SupervisorID,";
	$sql .= " people.PersonID, people.IsSalesperson, people.Level";
	$sql .= " ORDER BY supervisorID, lastname;";

	return GetResultsArray($sql);
}

// Called by GetLastLoginData, then called by itself recursively
function GetPeopleTree($peopleArray, &$inclause, $id)
{
	for($i=0; $i < count($peopleArray); $i++)
	{		
		if($peopleArray[$i]['SupervisorID'] == $id)
		{
			$inclause .= $peopleArray[$i]['PersonID'] . ",";
			GetPeopleTree($peopleArray, $inclause, $peopleArray[$i]['PersonID']);
		}
	}
}

//Called by GetLastLoginData
function GetResultsArray($sql)
{
//	$aliases=false;
//	if (func_num_args() >1)
//	{
//		$args = func_get_args();
//		if ($args[1] == true)
//			$aliases = true;
//	}

	$result = mssql_query($sql);
	$pm = GetPrevMonth();
	$comparray = array();

	if($result)
	{
		while($row = mssql_fetch_assoc($result))
		{
			$cm = getdate();
			$t = strtotime($row['LastLogin']);
			$today = time();
			$DaysPastGrace = DaysPastGrace($t);
			$d = getdate($t);
			$strDate = $d["mon"] . "/" . $d["mday"] . "/" . $d["year"];
			$comp = array();
//			if($aliases == true)
//				$comp['Alias'] = $row['Alias'];
			$comp['Person'] = $row['Person'];
			$comp['LastLogin'] = $strDate;
			$comp['SupervisorID'] = $row['SupervisorID'];
			$comp['PersonID'] = $row['PersonID'];
			$comp['Level'] = $row['PersonLevel'];
			$comp['IsSalesperson'] = $row['IsSalesperson'];
			// $comp['DaysSinceLast'] = round(($today - $t) / (60 * 60 * 24));
			// RJP 9-15-05: strip out time-of-day in our calculation, so that
			// we don't get the situation of two people with the same last login date
			// getting different numbers of days because more than 24 hours passed.
			$comp['DaysSinceLast'] = ($DaysPastGrace == 'NEVER') ? -1 : daydiff($today, $t);
			array_push($comparray, $comp);
		}
	}
	return $comparray;
}

// Called by GetResultsArray
function DaysPastGrace($ldate)
{
	global $GracePeriod;
	if(!$ldate) return "NEVER";

	$y = date("Y");
	$m = date("m");
	$d = date("d");
	$d -= $GracePeriod;
	$gpd = mktime(0, 0, 0, $m, $d,  $y);
	return (($ldate - $gpd) / (60 * 60 * 24));

}

// Called by GetResultsArray
function GetPrevMonth()
{
	$m = date("m");

	$y = date("Y");
	if($m == 1)
	{
		$m=12;
		$y--;
	}

	else
		$m--;
	return getdate(mktime(0, 0, 0, $m, 1,  $y));

}

// Called by daydiff
function striptime($t)
{
    // Strip out the time-of-day in a UNIX timestamp, that is,
    // set the time to midnight of that timestamp's date.
    $tmp = getdate($t);
    return mktime(0, 0, 0, $tmp['mon'], $tmp['mday'], $tmp['year']);
}

// Called by GetResultsArray
function daydiff($new, $old)
{
	// Compute the difference in days between two timestamps,
	// ignoring the time-of-day.
	return round((striptime($new) - striptime($old)) / (60 * 60 * 24));
}

// Called by main code
function GetTotals($arr, &$totPeople, &$totGP, &$tot30, &$tot60, &$totOver, &$Never)
{
	global $GracePeriod;
	$totPeople=0;
	$totGP=0;
	$tot30=0;
	$tot60=0;
	$totOver=0;
	$Never=0;
	for($i = 0; $i < count($arr); $i++)
	{
		$totPeople++;
		$DaysOld = $arr[$i]['DaysSinceLast'];
		if($DaysOld == -1) $Never++;
		else if($DaysOld <= $GracePeriod) $totGP++;
		else if($DaysOld > $GracePeriod && $DaysOld <= 30)
			$tot30++;
		else if($DaysOld > 30 && $DaysOld <= 60)
			$tot60++;
		else if($DaysOld > 60)
			$totOver++;

	}
}

// Called by main code first, then called by itself recursively
function PrintLastLoginDataTree(&$report, $arrRecs, $id, $indentlevel)
{ 
	$indentlevel += 1;
	for ($i=0; $i < count($arrRecs); $i++)
	{		
		if($arrRecs[$i]['SupervisorID'] == $id)
		{
			PrintPersonRec($report, $arrRecs[$i], $indentlevel);//print to report
			PrintLastLoginDataTree($report, $arrRecs, $arrRecs[$i]['PersonID'], $indentlevel);
		}
	}
}

// Called by main code and PrintLastLoginDataTree
function PrintPersonRec(&$report, $comp, $indentlevel)
{
	$aliases=0;
	if (func_num_args() > 3)
	{
		$args = func_get_args();
		if ($args[3] == true)
			$aliases = true;
	}
	global $GracePeriod;
	global $hilight;
	if($hilight == 1) $hilight = 0;
	else $hilight = 1;

	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
	if($hilight)
		SetRowColorStandard($report, -1, SHADE_LIGHT);
	if(!$aliases)
	{
		$dispLevels= array();
		$dispLevels[6-$indentlevel] = ""; 
		for($i=5; $i > -1; $i--)
		{
			AddColumnText($report, $dispLevels[$i], "", 0, 0);
		}
		
		$l_prefix = '';
		for($i=0; $i < $indentlevel; $i++)
		{
			$l_prefix .= '     ';
		}
		$level = $comp['Level'];
		AddColumnText($report, $l_prefix . "($level) " . $comp['Person'], "", 0, 0);
	}
	else
	{
		AddColumnText($report, $comp['Person'], "", 0, 0);
		AddColumnText($report, $comp['Alias'], "", 0, 0);
	}
	$DaysOld = $comp['DaysSinceLast'];
	if($DaysOld == -1)
	{
		$DaysOld = "NEVER";
		$LastLogin = '';
	}
	else
		$LastLogin = date("m/d/y", strtotime($comp['LastLogin']));
	$pad = '        ';
	AddColumnText($report, $DaysOld . $pad, "right", 0, 0);

	if ($DaysOld <= $GracePeriod)
		AddColumnText($report, $LastLogin, "center", 0, 0);
	else
		AddColumnText($report, " ", "center", 0, 0);

	if ($DaysOld > $GracePeriod && $DaysOld <=30)
		AddColumnText($report, $LastLogin, "center", 0, 0);
	else
		AddColumnText($report, " ", "center", 0, 0);
	if ($DaysOld > 30 && $DaysOld <=60)
		AddColumnText($report, $LastLogin, "center", 0, 0);
	else
		AddColumnText($report, " ", "center", 0, 0);
	if ($DaysOld > 60)
		AddColumnText($report, $LastLogin, "center", 0, 0);
	else
		AddColumnText($report, " ", "center", 0, 0);
}

?>
