<?php
 /**
 * @package Reports
 */
require_once '_paul_utils.php';
// require_once '_report_utils.php';
require_once 'GetSalespeopleTree.php';
require_once 'TableUtils.php';
require_once 'jim_utils.php';

$report_name = 'Average Sales Cycle Summary Report';
$who = MANAGEMENT;

begin_report($report_name);

if (!isset($treeid))
{
	print_tree_prompt($who);
	end_report();
	exit();
}

$person_list = implode(", ", GetSalespeopleTreeArray($treeid));

print_tree_path($treeid);

$sales_cycle_sql = "select datediff(day, FirstMeeting,ActualCloseDate) as SalesCycle, 
    PersonID, ProductID
    from opportunities
    where opportunities.CompanyID = $mpower_companyid
    and ActualCloseDate is not NULL 
    and ActualCloseDate <> '1/1/1900'
    and PersonID in ($person_list)";
    
$sales_cycle_result = mssql_query($sales_cycle_sql);

$sales_cycle_grid = array();
while ($row = mssql_fetch_assoc($sales_cycle_result))
{
    $sales_cycle_grid[$row['PersonID']][$row['ProductID']][] = $row['SalesCycle'];
}

$product_sql = "select ProductID, Name, DefaultSalesCycle
    from products 
    where CompanyID = $mpower_companyid and deleted = 0 order by name";

$product_result = mssql_query($product_sql);

$people_sql = "select PersonID, FirstName, LastName from people where PersonID in ($person_list) order by LastName, FirstName";
$people_result = mssql_query($people_sql);


echo "<table border = 1><td>Products</td>";
while ($row = mssql_fetch_assoc($people_result)) {
    $people_array[] = $row['PersonID'];
    echo "<td>". $row['FirstName'] . " " . $row['LastName'] . "</td>";
}

$products = array();
while ($row = mssql_fetch_assoc($product_result)) {
    echo "<tr>";
    echo "<td>" . $row['Name'] . "</td>";
    $products[$row['ProductID']] = array();
    foreach($people_array as $person) {
        echo "<td align=right>";
        if (isset($sales_cycle_grid[$person][$row['ProductID']]) && is_array($sales_cycle_grid[$person][$row['ProductID']])) {
            $cycles = MpowerPad($sales_cycle_grid[$person][$row['ProductID']], $row['DefaultSalesCycle']);
            $products[$row['ProductID']] = array_merge($products[$row['ProductID']], $cycles);
            echo ArrayAverage($cycles);
        }
        else {
            echo "-";
        }
        echo "</td>";
    }
    echo "<td>" . ArrayAverage($products[$row['ProductID']]) . "</td>";

    echo "</tr>";
}
echo "</table>";


function MpowerAverage($cycles) {
    sort($cycles);
    $front = TRUE;
    while (sizeof($sorted_array) > 24) {
        if ($front) {
            $sorted_array = array_shift($sorted_array);
            $front = FALSE;
        }
        else {
            $sorted_array = array_pop($sorted_array);
            $front = TRUE;
        }
    }

    $sum = array_sum($cycles);
    $num = sizeof($cycles);

    return floor($sum/$num);
}

function MpowerPad($cycles, $pad) {
    $cycles = array_pad($cycles, 24, $pad);

    sort($cycles);

    $front = TRUE;
	$sorted_array = array();
    while (sizeof($sorted_array) > 24) {
        if ($front) {
            $sorted_array = array_shift($sorted_array);
            $front = FALSE;
        }
        else {
            $sorted_array = array_pop($sorted_array);
            $front = TRUE;
        }
    }
    return $cycles;
}

function ArrayAverage($array) {
    $sum = array_sum($array);
    $len = sizeof($array);
	if ($len == 0) {
		return '-';
	}
    return floor($sum/$len);
}


// echo "<pre>";
// print_r($sales_cycle_grid);
// echo "</pre>";


?>
