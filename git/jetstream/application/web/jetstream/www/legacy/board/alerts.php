<?php
/**
* @package Board
*/

//$start = microtime(true);

define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');
require_once(BASE_PATH . '/include/class.ReportingTree.php');

SessionManager::Init();
SessionManager::Validate();

$targetsalesperson = $_SESSION['tree_obj']->GetTarget(true);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>M-Power</title>
<link rel="stylesheet" type="text/css" href="../css/sharedstyles.css,boardstyles.css">
<script language="javascript" src="../javascript/utils.js,pageformat.js,windowing.js,saver2.js"></script>

<script>
//<!--
<?php
include_once('../data/get_login.php');
include_once('../data/get_company.php');
$peers = is_array($targetsalesperson) && count($targetsalesperson) ? implode(',',array_values($targetsalesperson)) : $mpower_effective_userid;

$sql="SELECT * FROM people 
		WHERE isSalesperson = 1 AND 
			  PersonID IN ($peers)
		ORDER BY FirstName";
dump_sql_as_array('g_people', $sql);

$sql = "SELECT PersonID,ProductID,SalesCycleLength,StandardDeviation FROM salescycles WHERE CompanyID = '$mpower_companyid' AND PersonID IN ($peers)";
dump_sql_as_array('g_salescycles', $sql);

$sql = "SELECT * FROM snoozealerts WHERE PersonID IN ($peers)";
dump_sql_as_array('g_snoozealerts', $sql);


$period = 'm';
$result = mssql_query("select ClosePeriod from company where CompanyID = '$mpower_companyid'");
if ($result && ($row = mssql_fetch_assoc($result))) $period = $row['ClosePeriod'];

$now = getdate();
$first = mktime(0, 0, 0, $now['mon'], 1, $now['year']);
if ($period == 'm') {
	$first = strtotime('-2 months', $first);
} else {
	$dif = (((int) $now['mon'] - 1) % 3) + 6;
	$first = strtotime("-$dif months", $first);
}

$first = date("Y/m/d", $first);

$sql = "SELECT opportunities.*
		, Account." . $_SESSION['account_name'] . " Company
		, (Contact.$contact_first_name + ' ' + Contact.$contact_last_name) AS Contact
		FROM opportunities 
		LEFT JOIN Account ON opportunities.AccountID = account.AccountID
		LEFT JOIN Contact ON opportunities.ContactID = Contact.ContactID
		WHERE opportunities.CompanyID = '$mpower_companyid' AND (opportunities.Category != '6' OR opportunities.ActualCloseDate >= cast('$first' AS datetime)) AND (opportunities.Category != '9')
			 AND opportunities.PersonID in ($peers)";
			 
dump_sql_as_array('g_opps', $sql);

$companyList="('$mpower_companyid')";

include_once('../data/get_opp_products_xrefs.php');
dump_sql_as_array('g_products', "select * from products where CompanyID IN ($companyList) and Deleted = '0' order by Name");

calc_access_type();
print("window.g_accessType = $currentuser_accesstype;\n");

include_once('../data/get_trends.php');

?>
//-->
</script>

<script language="javascript" src="../javascript/category_labels.js,offerings.js,main_alerts.js"></script>

</head>

<body bgcolor="white">

<script language="JavaScript">
<!--
function do_save()
{	
	g_saver = new Saver();
	g_saver.open();
	save_table(g_snoozealerts, g_snoozealerts.SnoozeAlertsID, true, '\n', saved_new_snoozealerts, '../data/insert_snoozealerts.php');
}

function save_table(table, idColumn, negativeIDs, delim, callback, url)
{
	var dirties = '';
	for (var i = 0; i < table.length; ++i)
	{
		if (!table[i].isDirty) continue;
		if (table[i].deleted) continue;

		if ((negativeIDs && table[i][idColumn] < 0) || (!negativeIDs && table[i][idColumn] > 0))
		{
			if (dirties.length) dirties += delim;
			dirties += table[i].toPipes();
		}
	}
	if (dirties.length)
	{
		g_saver.m_data = dirties;
		g_saver.m_callback = callback;
		g_saver.m_params['mpower_fieldlist'] = table['_fieldlist'];
		g_saver.m_url = url;
		g_saver.save();
	}
	else callback('ok');
}

function done_saving(errmsg)
{
	g_saver.close();
	if (errmsg)
	alert('Error Saving: ' + errmsg);
	var hr=window.location.href;
	var pos = hr.indexOf('?');
	if (pos>=0) hr=hr.substr(hr,0,pos);
	if ((navigator.appName.indexOf("Netscape") == -1))
	document.body.onbeforeunload=null;
	window.location.href=hr+"?SN=<?=$_GET['SN']?>&time="+(new Date()).getTime();
}

function saved_new_snoozealerts(result)
{	
	if (result != 'ok')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}

	save_table(g_snoozealerts, g_snoozealerts.SnoozeAlertsID, false, '|', saved_existing_snoozealerts, '../data/update_snoozealerts.php');	
}

function saved_existing_snoozealerts(result)
{
	if (result != 'ok')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	done_saving();
}

function do_edit(oppID)
{
	var id = oppID.substr(1);
	var opp = find_opportunity(id);
	var hash = '#' + id + '_' + opp[g_opps.PersonID] + '_' + opp[g_opps.VLevel];
	var sn = 'SN=<?=$_REQUEST['SN']?>';
	var search = '?' + sn + '&reqOpId=' + id + '&reqPersonId=' + opp[g_opps.PersonID];
	var url = '../shared/edit_opp2.php' + search + hash; 

	Windowing.openSizedPrompt(url, 630, 820);
}

function open_trend(trendID, personID) {
	Windowing.openSizedWindow('trend.php?SN=<?=$_REQUEST['SN']?>&mpower_trendid=' + trendID + '&trendid=' + trendID + '&salesid=' + personID + '&target=' + personID, document.body.clientHeight, 790, 'trenddetails');
}

function update_from_edit_window()
{
	redraw();
}

function redraw()
{
	document.getElementById('theAlerts').innerHTML = Alerts.makeHTML();
}

calcOppBorders();
calcAllAlerts();

Header.setText('Alerts');
Header.addButton(ButtonStore.getButton('Print'));
document.writeln(Header.makeHTML());

var ivoryBox = new IvoryBox('100%', null);
document.writeln(ivoryBox.makeTop());
document.writeln('<div id="theAlerts">');
document.writeln(Alerts.makeHTML());
document.writeln('</div>');
document.writeln(ivoryBox.makeBottom());
// -->
</script>

</body>
</html>
<?php //echo 'Time hack : '.(microtime(true) - $start)/1000000; ?>