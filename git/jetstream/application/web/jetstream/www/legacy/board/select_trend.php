<?php
/**
* @package Board
*/
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');
require_once(BASE_PATH . '/include/class.DbConnManager.php');
require_once(BASE_PATH . '/include/class.SqlBuilder.php');
require_once(BASE_PATH . '/include/class.ReportingTree.php');

// validate user
SessionManager::Init();
SessionManager::Validate();
$pid = $target;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>M-Power</title>
        <link rel="stylesheet" type="text/css" href="../css/sharedstyles.css,boardstyles.css">
        <script language="JavaScript" src="../javascript/pageformat.js,utils.js,main_trends.js,category_labels.js"></script>
		<script language="JavaScript">
		<?php
		include_once('../data/_base_utils.php');
		include_once('../data/_query_utils.php');		

		echo '<!--'."\n";
		include_once('../data/get_company.php');
		include_once('../data/get_trends.php');

        if ($pid > 0) {

			$sql="SELECT * FROM people WHERE PersonID = $pid";
			dump_sql_as_array('g_people', $sql);
			$peopledata = get_data($sql);
			$salespersonname = $peopledata[0]['FirstName'].' '.$peopledata[0]['LastName'];

			$first = date('Y-m-d h:i:s');
			$sql = "SELECT * FROM opportunities WHERE CompanyID = '$mpower_companyid'";
			$sql .= " AND (Category != '6' OR ActualCloseDate >= '$first')";
			$sql .= " AND (Category != '9')";
			$sql .= " AND PersonID =$pid";
			dump_sql_as_array('g_opps', $sql);

        }
        else {			
			$targetsalesperson = $_SESSION['tree_obj']->GetTarget(true);

			if(is_array($targetsalesperson) && count($targetsalesperson)) $peers = implode(',',array_values($targetsalesperson));
			$salespersonname = 'All Salespeople';

			$sql="SELECT * FROM people 
					WHERE isSalesperson = 1 AND 
						  PersonID IN ($peers)
					ORDER BY LastName";
			dump_sql_as_array('g_people', $sql);

			$period = 'm';
			$result = mssql_query("select ClosePeriod from company where CompanyID = '$mpower_companyid'");
			if ($result && ($row = mssql_fetch_assoc($result))) $period = $row['ClosePeriod'];
		
			$now = getdate();
			$first = mktime(0, 0, 0, $now['mon'], 1, $now['year']);
			if ($period == 'm') {
				$first = strtotime('-2 months', $first);
			} else {
				$dif = (((int) $now['mon'] - 1) % 3) + 6;
				$first = strtotime("-$dif months", $first);
			}
		
			$first = date("Y/m/d", $first);

			$sql = "SELECT * FROM opportunities 
					WHERE CompanyID = '$mpower_companyid' AND 
						  (Category != '6' OR ActualCloseDate >= cast('$first' AS datetime)) AND 
						  (Category != '9' OR AutoReviveDate IS NOT NULL) AND PersonID in ($peers) order by Company ";
			dump_sql_as_array('g_opps', $sql);
        }
        ?>
	
		function open_trend(trendID, personID) {
			window.location.href = '<?=SessionManager::CreateUrl("trend.php")?>&trendid=' + trendID + '&target=<?=$target?>&salesid=' + personID + '&mpower_trendid=' + trendID;
		}
		subTrendNames();
		// -->
		</script>
    </head>

    <body bgcolor="white">
        <script language="JavaScript">
            <!--
            function init() {
                var headerText = 'Trends';
                headerText += ' / ' + "<?=$salespersonname?>";

                Header.setText(headerText);
                Header.addButton(ButtonStore.getButton('Print'));
                document.writeln(Header.makeHTML());
            }

            function show_trends() {				

                var trendsPerLine = Math.floor(document.body.offsetWidth / 130);
				<?php
					if($pid == -1) echo 'document.writeln(Trends.listAll(trendsPerLine));';
					else echo 'document.writeln(Trends.list(\''.$pid.'\', trendsPerLine));';
				?>
            }

            init();
            var ivoryBox = new IvoryBox('100%', null);
            document.writeln(ivoryBox.makeTop());
            show_trends();
            document.writeln(ivoryBox.makeBottom());

            // -->
        </script>

    </body>
</html>
