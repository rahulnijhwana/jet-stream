<?php
/**
* @package Board
*/
//$start = microtime(true);
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/class.ReportingTree.php';

SessionManager::Init();
SessionManager::Validate();

$targetsalesperson = $_SESSION['tree_obj']->GetLimb($mpower_effective_userid);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>M-Power - Spreadsheet View</title>
<link rel="stylesheet" type="text/css" href="../css/sharedstyles.css,boardstyles.css,tooltipstyle.css,spread_sheet.css" media="screen">
<link rel="stylesheet" type="text/css" href="../css/sharedstyles.css,boardstyles.css,tooltipstyle.css,print_spread_sheet.css" media="print">
<script language="JavaScript" src="../javascript/windowing.js,utils.js,pageformat.js,category_labels.js,smartselect.js,reassign.js,ci_confirm.js,save_board.js,saver2.js,offerings.js"></script>

<script language="Javascript">
<?php
include_once('../data/_base_utils.php');
include_once('../data/_query_utils.php');

echo '<!--'."\n";

if ($currentuser_loginid==7687) print("g_showaffiliates=true;\n"); 
else print("g_showaffiliates=false;\n");

$g_category = -1;
$g_personID = -1;
$g_opplist = -1;

$companyList="('$mpower_companyid')";

$hasharray = split('_',$hash);
if (count($hasharray) == 3) {
	list($cat, $g_personID, $g_opplist) = $hasharray;
} else if (count($hasharray) == 2) {
	list($cat, $g_personID) = $hasharray;
} else if (count($hasharray) == 1) {
	list($cat) = $hasharray;
}

if ($cat > 6 && $cat < 10) $cat = 6;
$g_category = $cat;

if(is_array($targetsalesperson) && count($targetsalesperson)) {
	$peers = implode(',',array_values($targetsalesperson));
	$salespersonname = $_SESSION['tree_obj']->GetInfo($g_personID, 'FirstName') . ' ' . $_SESSION['tree_obj']->GetInfo($g_personID, 'LastName');
} else {
	$peers = $mpower_effective_userid;
	$salespersonname = $_SESSION['tree_obj']->GetInfo($mpower_effective_userid, 'FirstName') . ' ' . $_SESSION['tree_obj']->GetInfo($mpower_effective_userid, 'LastName');
}

$sql = "SELECT PersonID,ProductID,SalesCycleLength,StandardDeviation FROM salescycles WHERE CompanyID = '$mpower_companyid' AND PersonID IN ($peers)";
dump_sql_as_array('g_salescycles', $sql);

$sql="SELECT * FROM people 
		WHERE isSalesperson = 1 AND 
			  PersonID IN ($peers)
		ORDER BY LastName";
dump_sql_as_array('g_people', $sql);

if(isset($timestamp) && $g_category == 6) {
	
	$day = '01';
	$month = date('m',$timestamp);
	$year = date('Y',$timestamp);

	if ($month == 12) {
		$nmonth = '01';
		$nyear = $year + 1;  	
	}else {
		$nmonth = $month + 1;
		$nyear = $year;
	}

	$current = $month.'-'.$day.'-'.$year;
	$next =  $nmonth.'-'.$day.'-'.$nyear;

	$first = date('Y-m-d h:i:s');
	$where = "( ActualCloseDate >= cast('$current' AS datetime) and ActualCloseDate < cast('$next' AS datetime)) AND";
}
else  {

	$period = 'm';
	$result = mssql_query("select ClosePeriod from company where CompanyID = '$mpower_companyid'");
	if ($result && ($row = mssql_fetch_assoc($result))) $period = $row['ClosePeriod'];

	$now = getdate();
	$first = mktime(0, 0, 0, $now['mon'], 1, $now['year']);
	if ($period == 'm') {
		$first = strtotime('-2 months', $first);
	} else {
		$dif = (((int) $now['mon'] - 1) % 3) + 6;
		$first = strtotime("-$dif months", $first);
	}

 	$first = date("Y/m/d", $first);
	$where = "(O.Category != '6' OR O.ActualCloseDate >= cast('$first' AS datetime)) AND ";
}


/*
$sql = "SELECT O.*, A.$account_name as Company, (C.$contact_first_name + ' ' + C.$contact_last_name) AS Contact 
		FROM opportunities O
		LEFT JOIN Account A on O.AccountID = A.accountid
		LEFT JOIN Contact C on O.ContactID = C.ContactID 
		WHERE O.CompanyID = '$mpower_companyid' AND 
			  $where  
		(O.Category != '9' OR O.AutoReviveDate IS NOT NULL) AND O.PersonID in ($peers) order by A.$account_name";
		
*/


$sql = "SELECT O.DealID,O.CompanyID,O.Category,O.Company,O.Division,O.Contact,convert(varchar, O.FirstMeeting,0) as FirstMeeting,O.ProductID
,O.EstimatedDollarAmount,O.Requirement1,O.Person,O.Need,O.Money,O.Time,O.Requirement2,convert(varchar, O.NextMeeting,0) as NextMeeting
,O.ActualDollarAmount,convert(varchar, O.AdjustedCloseDate,0) as AdjustedCloseDate,convert(varchar, O.ForecastCloseDate,0) as ForecastCloseDate
,O.PersonID,convert(varchar, O.ActualCloseDate,0) as ActualCloseDate,O.NextMeetingTime,convert(varchar, O.LastMoved,0) as LastMoved
,O.Alert1,O.Alert2,O.Alert3,O.Alert4,O.Alert5,O.Alert6,O.RemoveReason,convert(varchar, O.RemoveDate,0) as RemoveDate,O.FirstMeetingTime
,O.ValID,O.VLevel,convert(varchar, O.TargetDate,0) as TargetDate,O.SourceID,convert(varchar, O.BillDate,0) as BillDate
,O.CMID,O.FM_took_place,O.NM_New,O.Source2ID,O.PrevDaysOld,O.ChangedByID,convert(varchar, O.AutoReviveDate,0) as AutoReviveDate
,O.AutoRepeatIncrement,O.Renewed,convert(varchar, O.CreateDate,0) as CreateDate,O.Transactional,O.IsPO,O.PoNumber,convert(varchar, O.PoStartDate,0) as PoStartDate
,convert(varchar, O.PoEndDate,0) as PoEndDate,O.PoMaxAmount,O.AccountID,O.ContactID,O.IsEventCancelled,O.LatestEventID
,O.FMEventID,O.NMEventID,O.CallIn, A.$account_name as Company, (C.$contact_first_name + ' ' + C.$contact_last_name) AS Contact 
		FROM opportunities O
		LEFT JOIN Account A on O.AccountID = A.accountid
		LEFT JOIN Contact C on O.ContactID = C.ContactID 
		WHERE O.CompanyID = '$mpower_companyid' AND 
			  $where  
		(O.Category != '9' OR O.AutoReviveDate IS NOT NULL) AND O.PersonID in ($peers) order by A.$account_name";

dump_sql_as_array('g_opps', $sql);

$newopplist = array();
$rst = get_data($sql);
foreach($rst as $opp) {
	if ($opp['PersonID'] != $g_personID) continue;
	if ($g_category == -2) { 
		if ($opp['AutoReviveDate'] == '' || ($opp['Category'] != 6 && $opp['Category'] != 9)) continue;
	} else {
		if ($opp['Category'] != $g_category && $g_category != -1) continue;
	}
	$newopplist[] = $opp['DealID'];
}

$opplist = implode(',',$newopplist);
$opplist = empty($opplist) ? -1 : $opplist;
$g_opplist = $opplist;

include_once('../data/get_spreadsheet_subanswers.php');
include_once('../data/get_company.php');
include_once('../data/get_user.php');

dump_sql_as_array('g_submilestones', "SELECT * FROM SubMilestones WHERE CompanyID IN ($companyList) ORDER BY Location, Seq");
dump_sql_as_array('g_snoozealerts', "SELECT * FROM snoozealerts WHERE PersonID IN ($peers)");
dump_sql_as_array('g_logins', "SELECT * FROM Logins WHERE CompanyID = '$mpower_companyid' AND PersonID in ($peers)");

include_once('../data/get_sources.php');
include_once('../data/get_sources2.php');
include_once('../data/get_login.php');

?>

g_category = '<?=$g_category?>';
g_personID = '<?=$g_personID?>';
g_opplist = '<?=$g_opplist?>';
var cat = <?=$cat?>;
g_mass_move=false;
g_auto_revive=false;

if (g_category == -1) g_mass_move = true;
else if (g_category == -2) g_auto_revive = true;

<?php

dump_sql_as_array('g_opp_products_xref', "SELECT * FROM Opp_Product_XRef where CompanyID IN ($companyList) order by DealID, Seq");
dump_sql_as_array('g_products', "select * from products where CompanyID IN ($companyList) and Deleted = '0' order by Name");

include('../data/get_login.php');

?>
// -->
</script>
<script language="javascript" src="../javascript/main_alerts.js,spreadsheet.js"></script>
</head>

<body bgcolor="white">
<form name="form1">

<script language="JavaScript">
<!--
g_catDescs = new Array(catLabel('First Meeting'), catLabel('Information Phase'), catLabel('Stalled') + ' in ' + catLabel('Information Phase'),
		catLabel('Decision Point'), catLabel('Stalled') + ' in ' + catLabel('Decision Point'), catLabel('Closed'),'','','',catLabel('Target'));


g_sortField = '';
g_sortDescending = false;
g_valuation_used = (g_company[0][g_company.ValuationUsed] == '1');
g_source_used = (g_company[0][g_company.SourceUsed] == '1');
g_source2_used = (g_company[0][g_company.Source2Used] == '1');
g_opportunities = g_opps;
var g_amount_name = (g_company[0][g_company.CustomAmountName]);
g_CMInterface=g_company[0][g_company.CMInterface]=='1';
m_milestones = new Array('Requirement1', 'Person', 'Need', 'Money', 'Time', 'Requirement2');
g_the_only_company=g_company[0];
Spreadsheet = new Object();
g_editedSubAnswers = new Array();

function init()
{

	var headerText = g_catDescs[cat - 1];
	if (g_mass_move) headerText='Reassign';
	if (g_auto_revive) headerText = 'Opportunities Scheduled to Auto-'+g_company[0][g_company.ReviveLabel]+'/'+g_company[0][g_company.RenewLabel];

	headerText += ' / ' + "<?=$salespersonname?>";

	Header.setText(headerText);
	if (cat == 1 && g_alias_fullrights) Header.addButton(ButtonStore.getButton('New Opp'));
	Header.addButton(new HeaderButton('Print', null, 'printSpreadsheet()'));

	if (g_mass_move) Header.addButton(new HeaderButton('Reassign', null, 'Reassign()'));
	document.writeln(Header.makeHTML());
}


function do_edit(oppID)
{
	var id = oppID.substr(1);
	var opp = find_opportunity(id);
	var hash = '#' + id + '_' + opp[g_opps.PersonID];
	hash += '_' + opp[g_opps.VLevel];

	var sn = 'SN=<?=$_REQUEST['SN']?>';
	var search = '?' + sn + '&isspreadsheet=1&reqOpId='+id+'&reqPersonId='+ '<?=$g_personID?>';
	var url = '../shared/edit_opp2.php' + search + hash; 

	Windowing.openSizedPrompt(url, 630, 820);
}


function do_new()
{
	if (g_CMInterface)
	{
		window.parent.location.href = 'https://na1.salesforce.com/006/e?retURL=%2Fservlet%2Fservlet.Integration%3Flid%3D01r300000000hUf';
	}
	else
	{
		document.body.style.cursor = 'wait';
		var hash = '#-1_' + '<?=$g_personID?>';
		var sn = 'SN=<?=$_REQUEST['SN']?>';

		var search = '?' + sn + '&isspreadsheet=1&reqOpId=-1&reqPersonId='+ '<?=$g_personID?>';
		Windowing.openSizedPrompt('../shared/edit_opp2.php' +search + hash, 630, 820);
		document.body.style.cursor = 'auto';
	}
}


init();
var ivoryBox = new IvoryBox('100%', null);
document.writeln(ivoryBox.makeTop());
document.writeln('<div id="theSpreadsheet">' +  '</div>');
document.writeln(ivoryBox.makeBottom());

document.getElementById('theSpreadsheet').innerHTML = make_spreadsheet();
if(g_mass_move) initSPSelect(g_personID);

// -->
</script>
</form>
</body>
</html>
<?php //echo 'Time hack : '.(microtime(true) - $start)/1000000; ?>