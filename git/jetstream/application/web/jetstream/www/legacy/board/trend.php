<?php
/**
* @package Board
*/
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');
require_once(BASE_PATH . '/include/class.DbConnManager.php');
require_once(BASE_PATH . '/include/class.SqlBuilder.php');

// validate user
SessionManager::Init();
SessionManager::Validate();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>M-Power</title>
<link rel="stylesheet" type="text/css" href="../css/sharedstyles.css,boardstyles.css">
<script language="JavaScript" src="../javascript/pageformat.js,category_labels.js,main_trends.js"></script>

</head>

<body bgcolor="white">

<script language="JavaScript">
<?php
include_once('../data/_base_utils.php');
include_once('../data/_query_utils.php');
include_once('../data/get_company.php');
include_once('../data/get_trendtext.php');

$pid = (isset($_REQUEST['salesid'])) ? $_REQUEST['salesid'] : null;
$trendid = (isset($_REQUEST['trendid'])) ? $_REQUEST['trendid'] : null;

$sql="SELECT FirstName, LastName, Color FROM people WHERE PersonID = $pid";
$peopledata = get_data($sql);
$salespersonname = $peopledata[0]['FirstName'].' '.$peopledata[0]['LastName'];
$color = $peopledata[0]['Color'];
?>

<!--
g_trendID = null;
g_color = null;

function init()
{
	g_trendID = <?=$trendid?>;
	name = "<?=$salespersonname?>";
	g_color = "<?=$color?>";

	var headerText = 'Trend Details';
	if (name) headerText += ' / ' + name;		
	if (!g_color) g_color = 'silver';

	Header.setText(headerText);
	Header.addButton(ButtonStore.getButton('Trends'));
	Header.addButton(ButtonStore.getButton('Print'));
	document.writeln(Header.makeHTML());
}

function do_trends() {	
	document.body.style.cursor = 'wait';
	window.location.href = 'select_trend.php?SN=<?=$SN?>&target=<?=$target?>';
	document.body.style.cursor = 'auto';
}

function write_trend_name()
{

	var trend = g_trendtext[0];
	if (trend == null) return;

	var picSuffix='';
	if (g_trendID>8)
	{
		var req1Used = (g_company[0][g_company.Requirement1used] == '1')
		var req2Used = (g_company[0][g_company.Requirement2used] == '1')
		if (!req1Used && !req2Used) picSuffix='_a';
		else if (req1Used && !req2Used) picSuffix='_b';
		else if (!req1Used && req2Used) picSuffix='_c';
	}

	document.write('<table border=0><tr valign="middle"><td>');
	document.write('<span style="background-color: ' + g_color + '"><img src="trendpics/trend' + g_trendID + '_on'+picSuffix+'.gif"></span>');
	document.write('</td><td width=30>&nbsp;</td><td class="title">');

	var trendName =	makeTrendName(g_trendID, trend[g_trendtext.Name]);	
	trendName = substituteCategoryLabels(trendName);

	document.writeln(trendName);

	makeTrendName(g_trendID, trend[g_trendtext.Name])
	document.writeln('</td></tr></table>');
}

function write_trend_content()
{
	var trend = g_trendtext[0];
	if (trend == null) return;
	trend[g_trendtext.Content] = substituteCategoryLabels(trend[g_trendtext.Content]);
	document.writeln(trend[g_trendtext.Content]);
}

init();
var ivoryBox = new IvoryBox('100%', null);
document.writeln(ivoryBox.makeTop());
write_trend_name();
write_trend_content();
document.writeln(ivoryBox.makeBottom());
// -->
</script>

</body>
</html>
