
// object that can make a header.  call addButton() a few times, then call write().
Header = new Object();

// add a button to the list that this header can show
Header.addButton = _Header_addButton;

// set the text of the header
Header.setText = _Header_setText;

// return the HTML, ready to be displayed
Header.makeHTML = _Header_makeHTML;

Header.setVarButton = _Header_setVarButton;
// show/hide one button by name
Header.showButton = _Header_showButton;
Header.hideButton = _Header_hideButton;

// disable / enable button by name
Header.enableButton = _Header_enableButton;
Header.disableButton = _Header_disableButton;

// enable/disable all buttons
Header.enableAllButtons = _Header_enableAllButtons;
Header.disableAllButtons = _Header_disableAllButtons;

// make a button blink (or make it stop)
Header.blinkOn = _Header_blinkOn;
Header.blinkOff = _Header_blinkOff;
Header.isBlinking = _Header_isBlinking;

// make a button highlighted or not
Header.highlightOn = _Header_highlightOn;
Header.highlightOff = _Header_highlightOff;
Header.isHighlighted = _Header_isHighlighted;

// "private" variables
Header.m_buttons = new Object();
Header.m_buttonsShown = new Object();
Header.m_blinkTimers = new Object();
Header.m_blinkedButtons = new Object();
Header.m_text = '';


// you can set the button's name/label, icon src, and the expression to eval when clicked
function HeaderButton(name, iconSrc, expr)
{
	this.m_name = name;
	this.m_label = name;
	this.m_iconSrc = iconSrc;
	this.m_expr = expr;
	if (arguments.length > 3)
		this.m_label = arguments[3];
	this.m_varWidth=false;
}

// a store of standard buttons to draw from
ButtonStore = new Object();

// get a standard button out of the store, to pass in to a Header's addButton()
ButtonStore.getButton = _ButtonStore_getButton;

// the buttons that can be shown by name
ButtonStore.allButtons = new Array(
//	new HeaderButton('New', null, 'do_new()'),
	new HeaderButton('New Opp', null, 'do_new()'),
	new HeaderButton('Edit', null, 'do_edit()'),
	new HeaderButton('View', null, 'do_view()'),
	new HeaderButton('Siblings', null, 'do_siblings()'),
	new HeaderButton('My Dashboard', null, 'do_my_board()'),
	new HeaderButton('Reports', null, 'do_reports()'),
	new HeaderButton('Coaching', null, 'do_analysis()'),
	new HeaderButton('Trends', null, 'do_trends()'),
	new HeaderButton('Alerts', null, 'do_alerts()'),
	new HeaderButton('Print', null, 'window.print()'),
	new HeaderButton('Reselect', null, 'do_reselect()'),
	new HeaderButton('Export', null, 'do_export()'), // added 6/18/2013 (VJG)
	new HeaderButton('Save', null, 'do_save()'),
	new HeaderButton('Dashboards...',null,'do_ChangeBoard()'),
//	new HeaderButton('New Target',null,'do_new_target()'),
	new HeaderButton('Toggle T', null, 'toggle_target()'),
	new HeaderButton('Calendar', null, 'do_Calendar()'),
//	new HeaderButton('Mass Move', null, 'do_MassMove()'),
	new HeaderButton('Reassign', null, 'do_MassMove()'),
	new HeaderButton('Dashboard', null, 'do_Back()'));
	
function _Header_addButton(button, hide)
{
	var name = button.m_name;
	Header.m_buttons[name] = button;

	if (hide) Header.m_buttonsShown[name] = false;
	else Header.m_buttonsShown[name] = true;
}

function _Header_setText(text)
{
	Header.m_text = text.toString();
	var tag = document.getElementById('theHeaderText');
	if (tag)
		tag.innerHTML = Header.m_text;
}

function _Header_showButton(buttonName)
{
	Header.m_buttonsShown[buttonName] = true;
	var tag = document.getElementById('headertab_' + buttonName);
	if (tag) {
		tag.style.display = '';
	}
	else
	{
		var tag = document.getElementById('headerbutton_' + buttonName);
		if (tag) tag.style.display = '';
	}	
}

function _Header_setButtonLabel(buttonName, buttonLabel)
{
	var tag = document.getElementById('headerbutton_' + buttonName);
	if (tag) {
		tag.innerHTML = buttonLabel;
	}
}

function _Header_hideButton(buttonName)
{
	Header.m_buttonsShown[buttonName] = false;
	var tag = document.getElementById('headertab_' + buttonName);
	if (tag) {
		tag.style.display = 'none';
	}
	else
	{
		var tag = document.getElementById('headerbutton_' + buttonName);
		if (tag) tag.style.display = 'none';
	}	
}

// sets the button's varWidth (true or false) must be called before buttons are drawn
function _Header_setVarButton(buttonName,varWidth)
{
	for (var k in Header.m_buttons)
	{
		var button = Header.m_buttons[k];
		if (buttonName!=button.m_name) continue;
		button.m_varWidth=varWidth;
	}
}


function _Header_enableButton(buttonName)
{
	var tag = document.getElementById('headertab_' + buttonName);
	if (tag) {
		tag.disabled=false;
	}
	else
	{
		var tag = document.getElementById('headerbutton_' + buttonName);
		if (tag) {
			tag.disabled = false;
		}
	}	
}

function _Header_disableButton(buttonName)
{
	var tag = document.getElementById('headertab_' + buttonName);
	if (tag) {
		tag.disabled = true;
	} else {
		var tag = document.getElementById('headerbutton_' + buttonName);
		if (tag) {
			tag.disabled = true;
		}
	}	
}

function _Header_enableAllButtons()
{
	for (var k in Header.m_buttons) Header.enableButton(k);
}

function _Header_disableAllButtons()
{
	for (var k in Header.m_buttons) Header.disableButton(k);
}

function _Header_makeHTML()
{
	var cmBranch = "";
	if (window.g_company)
	{
		if (g_company[0][g_company.CMPath] == 'sf') {
			cmBranch = '_sf';
		}
	}
// the header has been split into 2 different tables.  Why?  because otherwise, the 2nd cell in the 2nd row
// gets almost no room, while the 1st cell gets way more than asked for.  With the two rows split into separate tables, the 
// space assignment works like you'd expect it to.
	var PicPath='../';
	if (_Header_makeHTML.arguments.length>0) {
		PicPath=_Header_makeHTML.arguments[0];
	}

	var ret = '<table width="100%" border=0 cellspacing=0 cellpadding=0 style="background-image:url('+PicPath+'images/header_tble_bg.jpg); background-repeat:no-repeat;"><tr height=37>';
	ret += '<td align="left"  ><img src="'+PicPath+'images/spacer.gif" height=37 width=50></td>';
	ret += '<td width=100%><img src="'+PicPath+'images/spacer.gif" height=37 width=100%></td>';
	ret += '<td width=6 style="background-image:url('+PicPath+'images/header_top_mid.gif); background-repeat:no-repeat;"><img src="'+PicPath+'images/spacer.gif" width=6 height=37></td>';
	ret += '<td align="right" height=37 class="header_table" width="100%">';
	
	ret += '<table cellpadding=0 cellspacing=0 border=0 style="width:1px"><tr>';
	for (var k in Header.m_buttons)
	{
		var button = Header.m_buttons[k];
		if (Header.m_buttonsShown[button.m_name] == false) {
			var disp=' style="display: none"';
		} else {
			var disp='';
		}
		var varWidth=button.m_varWidth;
		if (button.m_name == 'Save' || button.m_name == 'Alerts') {
			varWidth=false;
		}
		if (varWidth)
		{
			ret+='<td id="headertab_' + button.m_name+'"' + disp+ ' width=1><table border=0 cellspacing=0 cellpadding=0 style="width:1px">';
			ret+='<tr><td class="menu_button_l" width=3 rowspan=2><img src='+PicPath+'images/button-grey-l.gif width=3 height=22 style="margin-left:1px;"></td>';
		}
		ret += '<td>';
		if (button.m_name == 'Save' || button.m_name == 'Alerts') {
			ret += '<div type="button" class="sized_command_nogradient_new"';
		} else {
			if (varWidth) {
				ret += '<div type="button" class="menu_button_m" style="white-space:nowrap"';
			}
			else { 
				ret += '<div type="button" class="sized_command_new"';
			}
//			if (varWidth) ret += '<div type="button" style="border-width:0px; padding:0px; margin:0px;" ';
					
		}
		if (!varWidth) {
			ret += disp;
		}
		ret += ' id="headerbutton_' + button.m_name + '"';
		ret += ' onclick="' + button.m_expr + '" onmouseenter="document.body.style.cursor=\'hand\';" onmouseleave="document.body.style.cursor=\'auto\';">';
		if (button.m_iconSrc) {
			ret += '<img src="' + button.m_iconSrc + '">';
		}
		//if (varWidth) ret += "<div style=' background-color:blue'><nobrk>"+button.m_label+"</nobrk></div>";
		//if (varWidth) ret += "<div style=' background-color:blue; border-width:0px!important; padding:0px!important; margin:0px!important;'>"+"this is just a test"+"</div>";
		//else 
		//ret += "<nobrk>"+button.m_label+"</nobrk>";
		ret += button.m_label;
		ret += '</div></td>';
		if (varWidth) {
			ret+='<td rowspan=2 class="menu_button_r"><img src='+PicPath+'images/button-grey-r.gif style="margin-right:1px;"></td></tr>';
			ret += '</table></td>';
			// ret += '<tr><td><img src="'+PicPath+'images/spacer.gif" width=54 height=1></td></tr></table></td>';
		}
	}
	ret +='</tr></table>';
	
	ret += '</td><td width=13 style="background-image:url('+PicPath+'images/header_top_right.gif); background-repeat:no-repeat;"><img src="'+PicPath+'images/spacer.gif"></td></tr><tr>';
	ret += '<td align="left" height=20 ><img src="'+PicPath+'images/spacer.gif" height=20 width=50></td>';
	ret += '<td width=100%><img src="'+PicPath+'images/spacer.gif" height=20 width=100%></td>';
	ret += '<td height=20 width=6 style="background-image:url('+PicPath+'images/header_mid_mid.gif); background-repeat:no-repeat;"><img src="'+PicPath+'images/spacer.gif" height=20 width=6></td>';
	ret += '<td id="theHeaderText" class="header_text" valign="middle" nowrap>';
	ret += Header.m_text;
	ret += '</td><td height=20 width=9 style="background-image:url('+PicPath+'images/header_mid_right.gif); background-repeat:no-repeat;"><img src="'+PicPath+'images/spacer.gif" height=20 width=9></td>';
	
	ret +='</tr><tr>';
	ret += '<td align="left" height=9 ><img src="'+PicPath+'images/spacer.gif" height=9 width=50></td>';
	ret += '<td width=100%><img src="'+PicPath+'images/spacer.gif" height=9 width=100%></td>';
	ret += '<td height=9 width=6 style="background-image:url('+PicPath+'images/header_bottom_mid.gif); background-repeat:no-repeat;"><img src="'+PicPath+'images/spacer.gif" height=9 width=6></td>';
	ret += '<td height=9 style="background-image:url('+PicPath+'images/header_bottom_under.gif); background-repeat:repeat-x;"><img src="'+PicPath+'images/spacer.gif" height=9 width=100%></td>';
	ret += '</td><td height=9 width=9 style="background-image:url('+PicPath+'images/header_bottom_right.gif); background-repeat:no-repeat;"><img src="'+PicPath+'images/spacer.gif" height=9 width=9></td>';
	ret+='</tr></table>';

	return ret;
}

function _Header_blinkOn(buttonName)
{
	if (Header.m_blinkTimers[buttonName] != null) return;
	_Header_doBlink(buttonName);
}

function _Header_blinkOff(buttonName)
{
	var restore = false;
	if (Header.m_blinkedButtons[buttonName]) restore = true;

	window.clearTimeout(Header.m_blinkTimers[buttonName]);
	Header.m_blinkTimers[buttonName] = null;

	if (restore) Header.doBlink(buttonName, true);
}

function _Header_isBlinking(buttonName)
{
	return (Header.m_blinkTimers[buttonName] != null);
}

function _Header_doBlink(buttonName, dontSetTimer)
{
	var tag = document.getElementById('headerbutton_' + buttonName);
	if (!tag) {
		return;
	}

	if (Header.m_blinkedButtons[buttonName])
	{
		Header.m_blinkedButtons[buttonName] = false;
		tag.className = 'sized_command_nogradient_new';
		if (dontSetTimer) {
			return;
		}
		Header.m_blinkTimers[buttonName] = window.setTimeout('Header.doBlink("' + buttonName + '")', 500);
	}
	else
	{
		Header.m_blinkedButtons[buttonName] = true;
		tag.className = 'sized_command_hl_new';
		if (dontSetTimer) {
			return;
		}
		Header.m_blinkTimers[buttonName] = window.setTimeout('Header.doBlink("' + buttonName + '")', 20000);
	}
}

Header.doBlink = _Header_doBlink;

function _Header_highlightOn(buttonName)
{
	var tag = document.getElementById('headerbutton_' + buttonName);
	if (!tag) {
		return;
	}
	tag.className = 'sized_command_hl';
}

function _Header_highlightOff(buttonName)
{
	var tag = document.getElementById('headerbutton_' + buttonName);
	if (!tag) {
		return;
	}
	tag.className = 'sized_command';
}

function _Header_isHighlighted(buttonName)
{
	var tag = document.getElementById('headerbutton_' + buttonName);
	if (!tag) {
		return false;
	}
	return (tag.className == 'sized_command_hl');
}

function _ButtonStore_getButton(buttonName)
{
	for (var i = 0; i < ButtonStore.allButtons.length; ++i)
	{
		if (ButtonStore.allButtons[i].m_name.toLowerCase() == buttonName.toLowerCase()) {
			return ButtonStore.allButtons[i];
		}
	}

	return null;
}


// ---------------------------------------------------------------------------------------

/* use it like this:
var box = new IvoryBox('100%', '50%');
box.setType('dgray','.gif','gray');	// picture file prefix, extension, and bgcolor for inner cell
document.writeln(box.makeTop());
document.writeln('this is content');
document.writeln(box.makeBottom());
*/

// set width and/or height to null to suppress the attribute(s) completely
function IvoryBox(width, height)
{
	this.m_width = width;
	this.m_height = height;
	this.m_loc = '../';

	this.makeTop = _IvoryBox_makeTop;
	this.makeBottom = _IvoryBox_makeBottom;
	this.setType = _IvoryBox_setType;
	this.setBorderSize = _IvoryBox_setBorderSize;
	this.m_type='ivory';
	this.m_bgcolor='ivory';
	this.m_ext='.jpg';
	this.m_widthl=8;
	this.m_widthr=11;
	this.m_heightt=10;
	this.m_heightb=11;
}

function _IvoryBox_setType(type,ext,bgcolor)
{
	this.m_type=type;
	this.m_bgcolor=bgcolor;
	this.m_ext=ext;
}

function _IvoryBox_setBorderSize(widthl,heightt,widthr,heightb)
{
	this.m_widthl=widthl;
	this.m_heightt=heightt;
	this.m_widthr=widthr;
	this.m_heightb=heightb;
}	


function _IvoryBox_makeTop()
{
	var ret = '<table';
	if (this.m_width) {
		ret += ' width="' + this.m_width + '"';
	}
	if (this.m_height) {
		ret += ' height="' + this.m_height + '"';
	}
	ret += ' border=0 cellspacing=0 cellpadding=0><tr>';
	ret += '<td width='+this.m_widthl+' height='+this.m_heightt+'><img width='+this.m_widthl+' height='+this.m_heightt+' src="' + this.m_loc + 'images/'+this.m_type+'box_tl'+this.m_ext+'"></td>';
	ret += '<td width="100%" height='+this.m_heightt+'><img width="100%" height='+this.m_heightt+' src="' + this.m_loc + 'images/'+this.m_type+'box_t'+this.m_ext+'"></td>';
	ret += '<td width='+this.m_widthr+' height='+this.m_heightt+'><img width='+this.m_widthr+' height='+this.m_heightt+' src="' + this.m_loc + 'images/'+this.m_type+'box_tr'+this.m_ext+'"></td></tr><tr>';
	ret += '<td width='+this.m_widthl+' style="background: url('+ this.m_loc + 'images/'+this.m_type+'box_l'+this.m_ext+');"></td>';
	ret += '<td width="100%" height="100%" class="info" bgcolor="'+this.m_bgcolor+'" valign="top">';
	return ret;
}

function _IvoryBox_makeBottom()
{
	var ret = '</td><td width='+this.m_widthr+' style="background: url('+ this.m_loc + 'images/'+this.m_type+'box_r'+this.m_ext+');"></td></tr><tr>';
	ret += '<td width='+this.m_widthl+' height='+this.m_heightb+'><img width='+this.m_widthl+' height='+this.m_heightb+' src="' + this.m_loc + 'images/'+this.m_type+'box_bl'+this.m_ext+'"></td>';
	ret += '<td width="100%" height='+this.m_heightb+'><img width="100%" height='+this.m_heightb+' src="' + this.m_loc + 'images/'+this.m_type+'box_b'+this.m_ext+'"></td>';
	ret += '<td width='+this.m_widthr+' height='+this.m_heightb+'><img width='+this.m_widthr+' height='+this.m_heightb+' src="' + this.m_loc + 'images/'+this.m_type+'box_br'+this.m_ext+'"></td></tr></table>';
	return ret;
}

function makeFloatingBoxElem(width, height, content_elem)
{
	var temptable = document.createElement("TABLE");
	if (width) {
		temptable.width = width;
	}
	if (height) {
		temptable.height = height;
	}
	temptable.style.width = width;
	temptable.style.height = height;
	temptable.border = "0";
	temptable.cellSpacing = "0";
	temptable.cellPadding = "0";

	var temptr = temptable.insertRow(temptable.rows.length);

	var temptd = temptr.insertCell(temptr.cells.length);
	temptd.width = "8";
	temptd.height = "10";
	var tempimg = document.createElement("IMG");
	tempimg.style.width = "8px";
	tempimg.style.height = "10px";
	tempimg.src = "../images/ivorybox_tl.jpg";
	temptd.appendChild(tempimg);

	temptd = temptr.insertCell(temptr.cells.length);
	temptd.width = "100%";
	temptd.height = "10";
	tempimg = document.createElement("IMG");
	tempimg.style.width = "100%";
	tempimg.style.height = "10px";
	tempimg.src = "../images/ivorybox_t.jpg";
	temptd.appendChild(tempimg);

	temptd = temptr.insertCell(temptr.cells.length);
	temptd.width = "11";
	temptd.height = "10";
	tempimg = document.createElement("IMG");
	tempimg.style.width = "11px";
	tempimg.style.height = "10px";
	tempimg.src = "../images/ivorybox_tr.jpg";
	temptd.appendChild(tempimg);

	temptr = temptable.insertRow(temptable.rows.length);

	temptd = temptr.insertCell(temptr.cells.length);
	temptd.width = "8";
	temptd.height = "100%";
	tempimg = document.createElement("IMG");
	tempimg.style.width = "8px";
	tempimg.style.height = "100%";
	tempimg.src = "../images/ivorybox_l.jpg";
	temptd.appendChild(tempimg);

	temptable.content_td = temptr.insertCell(temptr.cells.length);
	if (content_elem) {
		temptable.content_td.appendChild(content_elem);
	}

	temptd = temptr.insertCell(temptr.cells.length);
	temptd.width = "11";
	temptd.height = "100%";
	tempimg = document.createElement("IMG");
	tempimg.style.width = "11px";
	tempimg.style.height = "100%";
	tempimg.src = "../images/ivorybox_r.jpg";
	temptd.appendChild(tempimg);

	temptr = temptable.insertRow(temptable.rows.length);

	temptd = temptr.insertCell(temptr.cells.length);
	temptd.width = "8";
	temptd.height = "11";
	tempimg = document.createElement("IMG");
	tempimg.style.width = "8px";
	tempimg.style.height = "11px";
	tempimg.src = "../images/ivorybox_bl.jpg";
	temptd.appendChild(tempimg);

	temptd = temptr.insertCell(temptr.cells.length);
	temptd.width = "100%";
	temptd.height = "11";
	tempimg = document.createElement("IMG");
	tempimg.style.width = "100%";
	tempimg.style.height = "11px";
	tempimg.src = "../images/ivorybox_b.jpg";
	temptd.appendChild(tempimg);

	temptd = temptr.insertCell(temptr.cells.length);
	temptd.width = "11";
	temptd.height = "11";
	tempimg = document.createElement("IMG");
	tempimg.style.width = "11px";
	tempimg.style.height = "11px";
	tempimg.src = "../images/ivorybox_br.jpg";
	temptd.appendChild(tempimg);

	return temptable;
}
