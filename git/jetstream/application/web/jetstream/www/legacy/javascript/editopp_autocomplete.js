
$(document).ready(function() {	
	
	function findValueCallback(event, data, formatted) {
		//$("<li>").html( !data ? "No match!" : "Selected: " + formatted).appendTo("#result");
		//alert(formatted);
		//alert(data);		
		//$("#contact").autocomplete(data);
		}	

	$("#select_company").autocomplete("../data/company_search.php", {
		width: 240,
		selectFirst: false
	});	
	
	$("#select_company").result(findValueCallback).next().click();	
	
	/*var company = $("#select_company").val();
	if(company != '') {
		$("#select_company").focus();
	}*/
	
	$("#contact").autocomplete("../data/contact_search.php", {
		width: 240,
		selectFirst: false,
		extraParams: {company: function() { return $("#select_company").val(); }}
	});	
	
	$("#contact").result(findContactValueCallback).next().click();	
	
	
	function findContactValueCallback(event, data, formatted) {
		if(data[1] > 0) {
			$('#ContactID').val(data[1]);
			$('#CurrentContact').val(data[0]);
		}
	}		

});
