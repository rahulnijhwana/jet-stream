
g_has_custom_amount = false;
g_the_only_company=g_company[0];

if (g_the_only_company[g_company.HasCustomAmount] == '1')
{
	g_amount_name = g_the_only_company[g_company.CustomAmountName];
	g_has_custom_amount = true;
}
else
{
	if (g_the_only_company[g_company.MonetaryValue] == '1')
		g_amount_name = '$ Value';
	else g_amount_name="Value";

}

g_valuation_used = (g_the_only_company[g_company.ValuationUsed] == '1');
g_source_used = (g_the_only_company[g_company.SourceUsed] == '1');
g_source2_used = (g_the_only_company[g_company.Source2Used] == '1');
g_target_used = (g_the_only_company[g_company.TargetUsed] == '1');
//g_valuation_used = 0;

//Is user an alias?  if so, does he have full rights?
g_alias_fullrights = 1;
if (g_login[0][g_login.LoginID] != g_login[0][g_login.PersonID])
	g_alias_fullrights = (g_login[0][g_login.FullRights] == '1');

Spreadsheet = new Object();

Spreadsheet.makeBody = _Spreadsheet_makeBody;
Spreadsheet.makeRow = _Spreadsheet_makeRow;
Spreadsheet.compare = _Spreadsheet_compare;
Spreadsheet.getSrcAbbr = _Spreadsheet_getSrcAbbr;
Spreadsheet.getSrc2Abbr = _Spreadsheet_getSrc2Abbr;
//Spreadsheet.getOppList = _Spreadsheet_getOppList;
Spreadsheet.makeTitle = _Spreadsheet_makeTitle;

function _Spreadsheet_makeTitle(category)
{
	var catDescs = new Array('First Meeting', 'Information Phase', 'Stalled in Information Phase',
		'Decision Point', 'Stalled in Decision Point', 'Closed','','','','Target');

	return catLabel(catDescs[category-1]);
}



function _Spreadsheet_makeRow(data, rowcnt, companyid)
{
	var bVal = g_valuation_used;
	var bSrc = g_source_used;
	var bSrc2 = g_source2_used;
	var bTarg = (data[g_opps.Category] == '10');

	if (data[g_opps.Category] == '9' && !g_auto_revive) {
		return '';
	}
	var id = data[g_opps.DealID];
	var targetDate;
	var daysOld;
	targetDate = Dates.mssql2us(data[g_opps.TargetDate]);
    daysOld = getDaysOld(targetDate);
	var space = targetDate.indexOf(' ');
	if (space > 1) targetDate = targetDate.substr(0, space);
	tdRaw = mkShortDate(targetDate);
	targetDate = Dates.formatShortDate(tdRaw);
	var now = new Date();
//	daysOld = Math.round((now - tdRaw) / 86400000);
	var firstMeeting = Dates.mssql2us(data[g_opps.FirstMeeting]);
	var space = firstMeeting.indexOf(' ');
	if (space > 1) firstMeeting = firstMeeting.substr(0, space);
	firstMeeting = Dates.formatShortDate(Dates.makeDateObj(firstMeeting));

	var nextMeeting;
	if (data[g_opps.Category] == '6')
		nextMeeting = Dates.mssql2us(data[g_opps.ActualCloseDate]);
	else
	{
		var nextMeeting = Dates.mssql2us(data[g_opps.NextMeeting]);
		var space = nextMeeting.indexOf(' ');
		if (space > 1) nextMeeting = nextMeeting.substr(0, space);
	}
	nextMeeting = Dates.formatShortDate(Dates.makeDateObj(nextMeeting));

    if (data[g_opps.Category] == '6') {
        var daysSinceFM = getDaysDiff(nextMeeting, firstMeeting);
    }
    else {
        var daysSinceFM = getDaysOld(firstMeeting);
    }


	var oppID = data[g_opps.ProductID];
	var oppDesc = getOfferingsList(data[g_opps.DealID]);
	var dollarAmount = data[g_opps.ActualDollarAmount];
	if (dollarAmount == '0' || dollarAmount == 0 || dollarAmount == '')
		dollarAmount = data[g_opps.EstimatedDollarAmount];
	if (dollarAmount != '0' && dollarAmount != 0 && dollarAmount != '')
		Spreadsheet.dollarTotal += parseInt(dollarAmount);
	dollarAmount = format_money(dollarAmount);

	var statusColor = 'ivory';
	var statusChar = '&nbsp;';

	if (data[g_opps.Category] == '1')
	{
		var now = new Date();
		var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		var firstMtgDate = Dates.makeDateObj(Dates.mssql2us(data[g_opps.FirstMeeting]));
	}
	else if (data[g_opps.Category] != '6' && data.border)
	{
		statusColor = data.border;
		if (data.border=="red")
			statusChar='R';
		else statusChar = 'Y';
	}

	var output = '<tr class="spreadsheet" height=25 id="' + id + '">';
	var vLevel = data[g_opps.VLevel] > -1 ? data[g_opps.VLevel] : '';

	var srcAbbr = Spreadsheet.getSrcAbbr( data[g_opps.SourceID]);
	var src2Abbr = Spreadsheet.getSrc2Abbr( data[g_opps.Source2ID]);
	if (g_auto_revive)
	{
		output += '<td width="1" align="center"><nobr>';
		var templabel = ((data[g_opps.Category]==9)?g_company[0][g_company.ReviveLabel]:g_company[0][g_company.RenewLabel]);
		output += '<input type="button" style="width:90px;" value="'+templabel+' Now" onclick="doReviveNow('+data[g_opps.DealID]+')"></input>';
		output += '<input type="button" value="Cancel" onclick="cancelRevive('+data[g_opps.DealID]+')"></input>';
		output += '</nobr></td>';
	}
	if(g_alias_fullrights && !g_mass_move && !g_auto_revive)
	{
		output += '<td width=30 align="center"><a href="javascript:edit_one(\'i' + id + '\')">';
		output += '<img border=0 src="../images/go.gif" alt="Edit Opportunity"></a></td>';
	}
//CHECKBOXES
	if (g_mass_move)
	{
		output+= '<td width=20 align="center"><input type=checkbox id="check'+rowcnt+'" name="check'+id+'"';
		if (!(!Spreadsheet.checkedRows['check'+id]))
			output+=' checked';
		output+='></td>';
		output+= '<td width=20 align="center">' + targetDate + '</td>';
	}
	if((data[g_opps.Category] != '10' || g_mass_move) && !g_auto_revive)
	{
		output += '<td width=10 align="center" bgcolor="' + statusColor + '"><b>' + statusChar + '</b></td>';
	}
//CATEGORIES
	if (g_mass_move)
	{
		output+= '<td width=20>' + getCatAbbr(data[g_opps.Category]) + '</td>';
	}

	var strCompany =  data[g_opps.Company];
	if(data[g_opps.Division].length)
		strCompany += '<br>' + data[g_opps.Division];
	if(data[g_opps.Contact].length)
		strCompany += '<br>' + data[g_opps.Contact];
	output += '<td '+ ((bTarg || g_auto_revive)? '' : 'width="115"')+'>' + strCompany + '</td>';

	if (g_auto_revive)
	{
		output += '<td width=35 style="text-align: center">';
		output += getCatAbbr(data[g_opps.Category]);
		output += '</td>';
	}

	if(bVal)
	{
		output += '<td width=35 style="text-align: center">' + vLevel + '</td>';
	}
//	if(bSrc && bTarg) source should be visible all the time 2/23/2005
	if(bSrc2)
	{
		output += '<td width=35>' + src2Abbr + '</td>';

	}
	if(bSrc)
	{
		output += '<td width=35>' + srcAbbr + '</td>';

	}
	if(bTarg)
	{
		if(!g_mass_move)
		{
			output += '<td align="right">' + targetDate + '</td>';
			output += '<td align="right">' + daysOld + '</td>';
		}
		else
		{
			for (var i=0; i<8; i++)
				output += '<td></td>';
			output += '</tr>';
		}
	}
	else
	{
		if (!g_auto_revive)
		{
			var cat = data[g_opps.Category];
			if(cat == 1 && mtgPast(Dates.mssql2us(data[g_opps.FirstMeeting])))
				output += '<td width=55 bgcolor="black" style="color:white" align="right">' + firstMeeting + '</td>';
			else
				output += '<td width=55 align="right">' + firstMeeting + '</td>';
			if(!g_mass_move && cat > 1 && cat < 10)
				output += '<td width=55 align="center">' + daysSinceFM + '</td>';

			output += '<td>' + oppDesc + '</td>';
			output += '<td width=62 align="right">' + dollarAmount + '</td>';

			var msNames = new Array();
			var msCols = new Array();
			var msLocs = new Array();
			if (g_the_only_company[g_company.Requirement1used] == '1')
			{
				msNames[msNames.length] = g_company[0][g_company.Requirement1];
				msCols[msCols.length] = g_opps.Requirement1;
				msLocs[msLocs.length] = 0;
			}

			msNames[msNames.length] = (g_company[0][g_company.MS2Label] == '') ? 'Person' : g_company[0][g_company.MS2Label];
			msCols[msCols.length] = g_opps.Person;
			msLocs[msLocs.length] = 1;
			msNames[msNames.length] = (g_company[0][g_company.MS3Label] == '') ? 'Need' :  g_company[0][g_company.MS3Label];
			msCols[msCols.length] = g_opps.Need;
			msLocs[msLocs.length] = 2;
			msNames[msNames.length] =  (g_company[0][g_company.MS4Label] == '') ? 'Money' :  g_company[0][g_company.MS4Label];
			msCols[msCols.length] = g_opps.Money;
			msLocs[msLocs.length] = 3;
			msNames[msNames.length] =  (g_company[0][g_company.MS5Label] == '') ? 'Time' :  g_company[0][g_company.MS5Label];
			msCols[msCols.length] = g_opps.Time;
			msLocs[msLocs.length] = 4;

			if (g_company[0][g_company.Requirement2used] == '1')
			{
				msNames[msNames.length] = g_company[0][g_company.Requirement2];
				msCols[msCols.length] = g_opps.Requirement2;
				msLocs[msLocs.length] = 5;

			}

			for (var i = 0; i < msCols.length; ++i)
			{
				var col = msCols[i];
	//Show rollover even if milestone is completed because of new data stored
				if (data[col] == '1')
					output += '<td width=40 align="center" class="milestone_yes_small" style="border: thick solid black"';
				else
				{
					output += '<td width=45 align="center" class="milestone_no_small" ';
				}

				output += ' id="td_l' + msLocs[i] + 'd' + data[g_opps.DealID] + '" ';
				output += writeMouseoverCall(msLocs[i], data[g_opps.DealID], msNames[i], data[g_opps.Company], companyid);
				output += '> ';
				output += msNames[i] + '</td>';
			}
			if(cat !=1 && cat != 6 && mtgPast(Dates.mssql2us(data[g_opps.NextMeeting])))
				output += '<td width=55  bgcolor="black" style="color:white" align="right">' + nextMeeting + '</td>';
			else
				output += '<td width=55 align="right">' + nextMeeting + '</td>';
		}
		else
		{
			output += '<td width=80 align="right" nowrap>';
			if (data[g_opps.Category] == 6)
				output += Dates.formatShortDate(Dates.makeDateObj(data[g_opps.ActualCloseDate]))
			else
				output += Dates.formatShortDate(Dates.makeDateObj(data[g_opps.RemoveDate]))
			output += '</td>';
			
			output += '<td width=130 align="right" nowrap><nobr><input type="text" style="text-align:right; border:none; background-color:transparent; font-family:Arial; font-size:10pt;" size="10" readonly name="textAutoReviveDate'+data[g_opps.DealID]+'" value="'+Dates.formatShortDate(Dates.makeDateObj(data[g_opps.AutoReviveDate]))+'"></input>';
			if (data[g_opps.Category] == 6)
				output += '&nbsp;&nbsp;<input type="button" name="autoDateCal" class="etc-button" onclick="editAutoReviveClose('+data[g_opps.DealID]+');"></nobr>';
			else
				output += '&nbsp;&nbsp;<input type="button" name="autoDateCal" class="etc-button" onclick="Windowing.dropBox.editedDealID = '+data[g_opps.DealID]+'; show_calendar(\'textAutoReviveDate'+data[g_opps.DealID]+'\')"></nobr>';
			if (data[g_opps.AutoRepeatIncrement] != '')
				output += '<br>Repeats every '+translateAutoIncrement(data[g_opps.AutoRepeatIncrement]);
			output += '</td>';
		}
	}
	output += '</tr>';
	return output;
}

function translateAutoIncrement(str)
{
	str = str.replace(/\+/g, '');
	str = str.replace('1 ', '');
	if (str == '3 months')
		str = 'quarter';
	return str;
}


function writeMouseoverCall(locid, dealid, locname, company, companyid)
{
	var html = ' onmouseover="popMSWindow(' + locid + ', ' + dealid + ', \'' + escape(locname) + '\',\'' + escape(company) + '\',' + companyid + ')" ';

	html += ' onmouseout="zapMSWindow(' + locid + ', ' + dealid + ')" ';
	return html;

}

function _Spreadsheet_getSrcAbbr(idSource)
{

	for (var i=0; i<g_sources.length; i++)
	{
		if(idSource == g_sources[i][g_sources.SourceID])
			return g_sources[i][g_sources.Abbr];
	}
	return '';
}

function _Spreadsheet_getSrc2Abbr(idSource2)
{

	for (var i=0; i<g_sources2.length; i++)
	{
		if(idSource2 == g_sources2[i][g_sources2.Source2ID])
			return g_sources2[i][g_sources2.Abbr];
	}
	return '';
}


g_mass_move = false;
g_auto_revive = false;
function _Spreadsheet_makeBody(personID, category, sortField, sortDescending)
{
//For g_showaffiliates:  Need to filter submilestones by companyid of the person being displayed
	var companyid = g_the_only_company[g_company.CompanyID];
	if(g_showaffiliates)
	{
		for (var i=0; i< g_people.length; i++)
		{
			if(g_people[i][g_people.PersonID] == personID)
			{
				companyid = g_people[i][g_people.CompanyID];
				break;
			}
		}
	}
	if (category == -1)
		g_mass_move = true;
	else
		g_mass_move = false;
	if (category == -2)
		g_auto_revive = true;
	else
		g_auto_revive = false;

	var opps = new Array();
	for (var i = 0; i < g_opps.length; ++i)
	{
		var data = g_opps[i];
		if (data[g_opps.PersonID] != personID)
			continue;
		if (category == -2)
		{
			if (data[g_opps.AutoReviveDate] == '' || (data[g_opps.Category] != 6 && data[g_opps.Category] != 9))
				continue;
		}
		else
		{
			if (data[g_opps.Category] != category && category != -1)
				continue;
		}
		//alert(data[g_opps.Company]);
		opps[opps.length] = data;
	}

	if (sortField)
	{
		if (sortField == 'milestones' || sortField == 'SrcAbbr' || sortField == 'Src2Abbr' || sortField == 'DaysOld' || sortField == 'RemoveAndCloseDate')
			Spreadsheet.m_sortField = sortField;
		else
			Spreadsheet.m_sortField = g_opps[sortField];
		Spreadsheet.m_sortDescending = sortDescending;
		opps.sort(Spreadsheet.compare);
	}

	Spreadsheet.dollarTotal = 0;
	var output = '';
	var row_num = 0;
	for (var i = 0; i < opps.length; ++i)
	{
		var data = opps[i];
        var row_output = Spreadsheet.makeRow(data, row_num, companyid);
        if (row_output != '') {
    		output += '<div id="div' + data[g_opps.DealID] + '">';
    		output += row_output;
    		output += '</div>';
            row_num += 1;
        }
	}
	if (opps.length > 0 && category != 10 && category != 1 && !g_auto_revive)
	{
		var colbefore = 9;
		var colafter = 7;
		if (category == -1)
			colbefore = 9;
		if (!g_source2_used)
			colbefore--;
		if (!g_source_used)
			colbefore--;
		if (g_company[0][g_company.Requirement1used] != '1')
			colafter--;
		if (g_company[0][g_company.Requirement2used] != '1')
			colafter--;
		output += '<tr class="spreadsheet" style="background-color:#F1F1FF;">';
		for (var k = 0; k < colbefore; ++k)
			output += '<td style="border-top:2px double silver;">&nbsp;</td>';
		output += '<td style="border-top:2px double silver; font-weight:bold;" align="right">'+format_money(Spreadsheet.dollarTotal)+'</td>';
		for (var k = 0; k < colafter; ++k)
			output += '<td style="border-top:2px double silver;">&nbsp;</td>';
		output += '</tr>';
	}

	return output;
}

Spreadsheet.m_milestones = new Array('Requirement1', 'Person', 'Need', 'Money', 'Time', 'Requirement2');

function _Spreadsheet_compare(a, b)
{
	var aa = 0;
	var bb = 0;

	if (Spreadsheet.m_sortField == 'RemoveAndCloseDate')
	{
		if (a[g_opps.Category] == 6)
		{
			if (a[g_opps.ActualCloseDate] == '')
				aa = Number.NEGATIVE_INFINITY;
			else
				aa = Dates.makeDateObj(a[g_opps.ActualCloseDate]).getTime();
		}
		else if (a[g_opps.Category] == 9)
		{
			if (a[g_opps.RemoveDate] == '')
				aa = Number.NEGATIVE_INFINITY;
			else
				aa = Dates.makeDateObj(a[g_opps.RemoveDate]).getTime();
		}
		if (b[g_opps.Category] == 6)
		{
			if (b[g_opps.ActualCloseDate] == '')
				bb = Number.NEGATIVE_INFINITY;
			else
				bb = Dates.makeDateObj(b[g_opps.ActualCloseDate]).getTime();
		}
		else if (b[g_opps.Category] == 9)
		{
			if (b[g_opps.RemoveDate] == '')
				bb = Number.NEGATIVE_INFINITY;
			else
				bb = Dates.makeDateObj(b[g_opps.RemoveDate]).getTime();
		}
	}
	else if (Spreadsheet.m_sortField == 'milestones')
	{
		for (var i = 0; i < Spreadsheet.m_milestones.length; ++i)
		{
			var idx = Spreadsheet.m_milestones[i];
			var fld = g_opps[idx];
			if (a[fld] == '1') ++aa;
			if (b[fld] == '1') ++bb;
		}
	}
	else if (Spreadsheet.m_sortField == 'Src2Abbr')
	{
		aa = Spreadsheet.getSrc2Abbr( a[g_opps.Source2ID]);
		bb = Spreadsheet.getSrc2Abbr( b[g_opps.Source2ID]);

	}
	else if (Spreadsheet.m_sortField == 'SrcAbbr')
	{
		aa = Spreadsheet.getSrcAbbr( a[g_opps.SourceID]);
		bb = Spreadsheet.getSrcAbbr( b[g_opps.SourceID]);

	}
	else if (Spreadsheet.m_sortField == 'DaysOld')
	{
		var targetDate = Dates.mssql2us(a[g_opps.TargetDate]);
		var space = targetDate.indexOf(' ');
		if (space > 1) targetDate = targetDate.substr(0, space);
		var tdRaw = Dates.makeDateObj(targetDate);
		targetDate = Dates.formatShortDate(tdRaw);
		var now = new Date();
		aa = Math.round((now - tdRaw) / 86400000);

		targetDate = Dates.mssql2us(b[g_opps.TargetDate]);
		space = targetDate.indexOf(' ');
		if (space > 1) targetDate = targetDate.substr(0, space);
		tdRaw = Dates.makeDateObj(targetDate);
		targetDate = Dates.formatShortDate(tdRaw);
		bb = Math.round((now - tdRaw) / 86400000);

	}
	else if (Spreadsheet.m_sortField == g_opps.ProductID)
	{
		aa = find_product(a[Spreadsheet.m_sortField]);
		bb = find_product(b[Spreadsheet.m_sortField]);
		if (aa) aa = aa[g_products.Name];
		else aa = '';
		if (bb) bb = bb[g_products.Name];
		else bb = '';
	}
	else if (Spreadsheet.m_sortField == g_opps.EstimatedDollarAmount ||
		Spreadsheet.m_sortField == g_opps.ActualDollarAmount)
	{
		aa = parseInt(a[Spreadsheet.m_sortField], 10);
		bb = parseInt(b[Spreadsheet.m_sortField], 10);
		if (isNaN(aa)) aa = 0;
		if (isNaN(bb)) bb = 0;
	}
	else if (Spreadsheet.m_sortField == g_opps.FirstMeeting ||
		Spreadsheet.m_sortField == g_opps.NextMeeting ||
		Spreadsheet.m_sortField == g_opps.ActualCloseDate ||
		Spreadsheet.m_sortField == g_opps.AutoReviveDate)
	{
		if (a[Spreadsheet.m_sortField] == '')
			aa = Number.NEGATIVE_INFINITY;
		else
			aa = Dates.makeDateObj(a[Spreadsheet.m_sortField]).getTime();

		if (b[Spreadsheet.m_sortField] == '')
			bb = Number.NEGATIVE_INFINITY;
		else
			bb = Dates.makeDateObj(b[Spreadsheet.m_sortField]).getTime();

		if (isNaN(aa)) aa = Number.NEGATIVE_INFINITY;
		if (isNaN(bb)) bb = Number.NEGATIVE_INFINITY;
	}
	else if (Spreadsheet.m_sortField == g_opps.Company)
	{
		var str1 =  a[Spreadsheet.m_sortField];
		var str2 =  b[Spreadsheet.m_sortField];
		aa = str1.toUpperCase();
		bb = str2.toUpperCase();
	}
	else
	{
		aa = a[Spreadsheet.m_sortField];
		bb = b[Spreadsheet.m_sortField];
	}

	if (Spreadsheet.m_sortField==g_opps.Category)
	{
		if (aa==10) aa='0';
		if (bb==10) bb='0';
	}

	if (Spreadsheet.m_sortDescending)
	{
		if (aa < bb) return 1;
		if (aa > bb) return -1;
		return 0;
	}
	else
	{
		if (aa < bb) return -1;
		if (aa > bb) return 1;
		return 0;
	}
}

function list_people_options(who_is_selected)
{
	var ret = '';
	for (var k = 0; k < g_people.length; ++k)
	{
		var data = g_people[k];
		if (data[g_people.IsSalesperson] != '1')
			continue;
		ret += '<option value="' + data[g_people.PersonID] + '"';
		if (data[g_people.PersonID] == who_is_selected)
			ret += ' selected';
		ret += '>' + data[g_people.LastName] + ', ' + data[g_people.FirstName];
		if (data[g_people.Level] > 1 && data[g_people.GroupName] && data[g_people.GroupName] != '')
			ret += ' (' + data[g_people.GroupName] + ')';
		ret += '</option>';
	}
	return ret;
}

function mtgPast(strD)
{
	var now = new Date();
	var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
	var d = Dates.makeDateObj(strD);
	if (today.getTime() > d.getTime())
		return true;
	return false;

}



