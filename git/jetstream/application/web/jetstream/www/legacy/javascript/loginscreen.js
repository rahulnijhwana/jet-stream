LoginScreen = new Object();

LoginScreen.makeHTML = _LoginScreen_makeHTML;
LoginScreen.logIn = _LoginScreen_logIn;
LoginScreen.setCookie = _LoginScreen_setCookie;
LoginScreen.getCookie = _LoginScreen_getCookie;

function check_login(overrideSharedPath)
{
	if (window.loginResult == null || window.loginResult < 1) // invalid or incomplete login
	{
		Header.setText('Log In');
		document.writeln(Header.makeHTML('./'));

		var ivoryBox = new IvoryBox('100%', null);
		if (overrideSharedPath) ivoryBox.m_loc = overrideSharedPath;
		document.writeln(ivoryBox.makeTop());

		var coid = LoginScreen.getCookie('mpower_companyid');
		if (!coid || isNaN(parseInt(coid)))
			document.writeln('<br><p class="error">Invalid login information. Please return to the login page and try again.</p><br>');
		else
		{
			var uid = LoginScreen.getCookie('mpower_userid');
			var pwd = LoginScreen.getCookie('mpower_pwd');
			if (uid == null && pwd == null)
			{
				document.writeln('<p>Please enter your user ID and password to continue:</p>');
				document.writeln(LoginScreen.makeHTML());
			}
			else
				document.writeln(LoginScreen.makeHTML('Invalid login information. Please check your spelling and try again.'));
		}

		document.writeln(ivoryBox.makeBottom());
		return false;
	}

	return true;
}

function admin_check_login()
{	
	if (window.loginResult == null || (window.loginResult < 1 && window.loginResult!=3)) // invalid or incomplete login
	{
		Header.setText('Log In');
		var str = Header.makeHTML('./');

		var ivoryBox = new IvoryBox('100%', null);
		str += ivoryBox.makeTop();

		var coid = LoginScreen.getCookie('mpower_companyid');
		if (!coid || isNaN(parseInt(coid)))
			str += '<br><p class="error">Invalid login information. Please return to the login page and try again.</p><br>';
		else
		{
			var uid = LoginScreen.getCookie('mpower_userid');
			var pwd = LoginScreen.getCookie('mpower_pwd');
			if (uid == null && pwd == null)
			{
				str += '<p>Please enter your user ID and password to continue:</p>';
				str += LoginScreen.makeHTML();
			}
			else
				str += LoginScreen.makeHTML('Invalid login information. Please check your spelling and try again.');
		}

		str += ivoryBox.makeBottom();

		document.body.innerHTML = str;
		return false;
	}

	return true;
}

function _LoginScreen_makeHTML(errorMsg)
{
	var ret = '<form name="loginform" action="javascript:LoginScreen.logIn()">';
	if (errorMsg) ret += '<p class="error">' + errorMsg + '</p>';
	ret += '<table border=0 class="plabel" align="center">';
	ret += '<tr><td>User ID: </td><td><input type="text" name="mpower_userid"></td></tr>';
	ret += '<tr><td>Password: </td><td><input type="password" name="mpower_pwd"></td></tr>';
	ret += '<tr><td>&nbsp;</td><td><input class="command" type="submit" name="loginbutton" value="Log In"></td></tr>';
	ret += '</table></form>';
	return ret;
}

function _LoginScreen_logIn()
{
	var f = document.forms['loginform'];

	var required = new Object();
	required['mpower_userid'] = 'userID';
	required['mpower_pwd'] = 'password';

	for (var k in required)
	{
		if (f.elements[k].value == '')
		{
			alert('Please enter your ' + required[k] + ' to continue.');
			f.elements[k].focus();
			f.elements[k].select();
			return;
		}

		LoginScreen.setCookie(k, f.elements[k].value);
	}

	// this doesn't work in mozilla because it doesn't check the server again..
	// perhaps setting an expires header on the server will solve that?
	window.history.go(0);
}

function _LoginScreen_setCookie(cookieName, cookieValue)
{
	var cookie = cookieName += '=' + escape(cookieValue) + '; path=/';
	document.cookie = cookie;
}

function _LoginScreen_getCookie(cookieName)
{
	var name = cookieName + '=';
	var dc = document.cookie;

	if (dc.length > 0)
	{
		var begin = dc.indexOf(name);
		if (begin != -1)
		{
			begin += name.length;
			var end = dc.indexOf(';', begin);
			if (end == -1) end = dc.length;
			return unescape(dc.substring(begin, end));
		}
	}

	return null;
}

