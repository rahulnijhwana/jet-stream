function toggelViewButtons(btnType)
{
	if(btnType=='Company')
	{
		if($("#select_company").val()!='')
		{
			$('#imgViewCompany').css({visibility:"visible"});					
		}
		else
		{
			$('#imgViewCompany').css({visibility:"hidden"});
			$('#imgViewContact').css({visibility:"hidden"});
			$('#cellDetails').html('');
		}
						
	}
	else if(btnType=='Contact')
	{
		if($("#select_company").val()!='' && $("#Contact").val()!='')
		{
			$('#imgViewContact').css({visibility:"visible"});
		}
	}
}
function viewDetails(btnType)
{
	if(window.opener)
	{
		if(!g_somethingWasChanged && opp_id!=0)
		{		
			
			if(btnType=='Company' && g_opportunity[g_oppCols.AccountID]!='' && $("#select_company").val()!='')							
			{
				window.opener.location.href='../../slipstream.php?action=company&accountId='+g_opportunity[g_oppCols.AccountID];						
				window.close();
			}
			else if(btnType=='Company' && (g_opportunity[g_oppCols.AccountID]==''|| $("#select_company").val()==''))
			{
				alert('No Company found');
			}
			if(btnType=='Contact'!='' && g_opportunity[g_oppCols.ContactID]!='' && $("#contact").val()!='')
			{
				window.opener.location.href='../../slipstream.php?action=contact&contactId='+g_opportunity[g_oppCols.ContactID];			
				window.close();
			}			
			else if(btnType=='Contact'!='' && (g_opportunity[g_oppCols.ContactID]=='' || $("#contact").val()==''))
			{
				alert('No Contact found');
			}
		}
		else if(g_somethingWasChanged && opp_id!=0)
		{
			if(btnType=='Company' && $("#select_company").val()!='')							
			{
				if(confirm('Do you want to save changes & view details'))
				{
					redirect_to_jetstream=1;
					redirect_type=btnType;
					ok();
				}			
			}
			else if(btnType=='Company' && $("#select_company").val()=='')
			{
				alert('No Company found');
			}
			if(btnType=='Contact' && $("#contact").val()!='')							
			{
				if(confirm('Do you want to save changes & view details'))
				{
					redirect_to_jetstream=1;
					redirect_type=btnType;
					ok();
				}			
			}
			else if(btnType=='Contact' && $("#contact").val()=='')
			{
				alert('No Contact found');
			}			
		}
		else
		{
			if(g_somethingWasChanged)
			{
				if(btnType=='Company' && trim($("#select_company").val())=='')
				{
					alert('No Company found');
					return;
				}
				if(btnType=='Contact' && trim($("#contact").val())=='')
				{
					alert('No Contact found');
					return;
				}					
				if(confirm('Do you want to save changes & view details'))
				{
					redirect_to_jetstream=1;
					redirect_type=btnType;
					ok();
				}
			}
			else
			 alert('Create an opp to view details');
		}
	}
}