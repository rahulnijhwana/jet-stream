//----------------------- milestone labels ------------------------------------

//controlID, target, caption(with embedded target), fieldname, default
g_cat_map = new Array(
//Admin
	new Array("thFM", "{{FM}}", "{{FM}} Threshhold", 'FMLabel', "First Meeting"),
	new Array("thIP", "{{IP}}", "{{IP}} Threshhold", 'IPLabel', "Information Phase"),
	new Array("thDP", "{{DP}}", "{{DP}} Threshhold", 'DPLabel', "Decision Point"),
	new Array("thC", "{{C}}", "{{C}} Threshhold", 'CLabel', "Close"),
	new Array("thqFM", "{{FM}}", "{{FM}}", 'FMLabel', "First Meeting"),
	new Array("thqIP", "{{IP}}", "{{IP}}", 'IPLabel', "Information Phase"),
	new Array("thqDP", "{{DP}}", "{{DP}}", 'DPLabel', "Decision Point"),
	new Array("thqC", "{{C}}", "{{C}}", 'CLabel', "Close"),
	new Array("btnTarg","{{T}}","New {{T}}", 'TLabel', 'Target'),
	new Array("lblIPCloseRatio", "{{IP}}", "{{IP}}:", 'IPLabel', "Information Phase"),
	new Array("lblDPCloseRatio", "{{DP}}", "{{DP}}:", 'DPLabel', "Decision Point"),
	new Array("lblIPStalledLimit", "{{IP}}", "{{IP}}:", 'IPLabel', "Information Phase"),
	new Array("lblDPStalledLimit", "{{DP}}", "{{DP}}:", 'DPLabel', "Decision Point"),
	new Array("lblTargetToggle", "{{T}}", "{{T}} (5th Column)", 'TLabel', "Target"),

//edit opportunity window
	new Array("eo_fm_input_caption","{{FM}}","{{FM}}:&nbsp", 'FMLabel', 'First Meeting'),
	new Array("lblT", "{{T}}", "{{T}}", 'TLabel', "Target"),
	new Array("lblFM", "{{FM}}", "{{FM}}", 'FMLabel', "First Meeting"),
	new Array("lblIP", "{{IP}}", "{{IP}}", 'IPLabel', "Information Phase"),
	new Array("lblDP", "{{DP}}", "{{DP}}", 'DPLabel', "Decision Point"),
	new Array("lblC", "{{C}}", "{{C}}", 'CLabel', "Closed")
	);


function getCatLabel(strToken, strCaption, fieldName, strDefault)
{
//	var strLabel = g_the_only_company[g_company[fieldName]];
	var strLabel;
	if('undefined' != typeof(g_company))
		strLabel = g_company[0][g_company[fieldName]];
	else 	
		strLabel = g_the_only_company[g_company[fieldName]];
	if (!strLabel.length) strLabel = strDefault;
	return strCaption.replace(strToken,strLabel);
}

function setCatLabel(id, strToken, strCaption, fieldName, strDefault)
{
	var item = document.getElementById(id);
	if (item == null) return;
	item.innerHTML = getCatLabel(strToken, strCaption, fieldName, strDefault);
}


function setCategoryLabels()
{
	for (var i = 0; i < g_cat_map.length; i++)
	{
		var r = g_cat_map[i];
		setCatLabel(r[0], r[1], r[2], r[3], r[4]);
	}

}

g_cat_abbr=['','FMAbbr','IPAbbr','IPSAbbr','DPLabel','DPSLabel','CAbbr','CAbbr','CAbbr','RAbbr','TAbbr'];
g_cat_abbr_default=['','FM','IP','IPS','DP','DPS','C','C','C','R','Tgt'];

function getCatAbbr(cat)
{
	var strlabel=g_the_only_company[g_company[g_cat_abbr[cat]]];
	if (!strlabel.length) strlabel=g_cat_abbr_default[cat];
	return strlabel;
}

//used on the board side

function catLabel(label)
{
	switch (label)
	{
		case 'Target':
			return getCatLabel('{{T}}','{{T}}','TLabel', label);
		case 'T':
		case 'Tgt':
		case 'Tgts':
			return getCatLabel('{{T}}','{{T}}','TAbbr', label);		
		case 'First Meeting':
			return getCatLabel('{{FM}}','{{FM}}','FMLabel', label);
		case 'FM':
		case 'First Mtg':
			return getCatLabel('{{FM}}','{{FM}}','FMAbbr', label);
		case 'Information Phase':
			return getCatLabel('{{IP}}','{{IP}}','IPLabel', label);
		case 'Information Phase Stalled':
		case 'Stalled in Information Phase':
			return getCatLabel('{{IPS}}','{{IPS}}','IPSLabel', label);
		case 'IP':
			return getCatLabel('{{IP}}','{{IP}}','IPAbbr', label);
		case 'IPS':
			return getCatLabel('{{IPS}}','{{IPS}}','IPSAbbr', label);
		case 'Decision Point':
			return getCatLabel('{{DP}}','{{DP}}','DPLabel', label);
		case 'Decision Point Stalled':
		case 'Stalled in Decision Point':
			return getCatLabel('{{DPS}}','{{DPS}}','DPSLabel', label);
		case 'DP':
			return getCatLabel('{{DP}}','{{DP}}','DPAbbr', label);
		case 'DPS':
			return getCatLabel('{{DPS}}','{{DPS}}','DPSAbbr', label);
		case 'Closed':
			return getCatLabel('{{C}}','{{C}}','CLabel', label);
		case 'C':
			return getCatLabel('{{C}}','{{C}}','CAbbr', label);
		case 'Removed':
			return getCatLabel('{{R}}','{{R}}','RLabel', label);
		case 'R':
			return getCatLabel('{{R}}','{{R}}','RAbbr', label);
		case 'Stalled':
			return getCatLabel('{{Stalled}}','{{Stalled}}','SLabel', label);
		case 'S':
			return getCatLabel('{{S}}','{{S}}','SAbbr', label);
			
		
	}
	return label;

}


g_cat_targets = new Array(
			'Target','T',
			'First Meeting','FM','First Mtg',
			'Information Phase','IP',
			'Information Phase Stalled','Stalled in Information Phase','IPS',
			'Decision Point','DP',
			'Decision Point Stalled','Stalled in Decision Point','DPS',
			'Closed','C','Removed','R','Stalled','S'
			);

function substituteCategoryLabels(text)
{
	for (i=0; i < g_cat_targets.length; i++)
	{
//		text = text.replace('{{' + g_cat_targets[i] + '}}', catLabel(g_cat_targets[i]));
		text = replaceAll(text, '{{' + g_cat_targets[i] + '}}', catLabel(g_cat_targets[i]));

	}
	return text;
}

function subTrendNames()
{
	for (var i = 0; i < g_trends.length; i++)
	{
		g_trends[i][g_trends.Name] = substituteCategoryLabels(g_trends[i][g_trends.Name]);		
		
	}
}

function replaceAll(text, target, sub)
{
	while(1)
	{
		var pos = text.indexOf(target);
		if(pos < 0) break;
		text = text.replace(target, sub);
	}
	return text;
}


