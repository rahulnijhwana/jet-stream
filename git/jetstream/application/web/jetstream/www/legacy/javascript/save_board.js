function do_save()
{
	Header.disableAllButtons();
	if (typeof(Grid) != 'undefined') Grid.enable = false;
	g_saver = new Saver();
	g_saver.open();
	save_table(g_opps, g_opps.DealID, true, '\012', saved_new_opps, '../data/insert_opps.php');
}

function done_saving(errmsg)
{	
	Header.enableAllButtons();
	if (typeof(Grid) != 'undefined') Grid.enable = true;
	g_saver.close();
	if (errmsg)
		alert('Error Saving: ' + errmsg);
//	else history.go(0);
	if (typeof(Grid) != 'undefined') 
	{
		var hr=window.location.href;
		var pos = hr.indexOf('?');
		if (pos>=0) hr=hr.substr(hr,0,pos);
		window.location.href=hr+"?time="+(new Date()).getTime();
	}

	// Hide the light box.
	//if(window.location.href.indexOf('detailview') != -1) setTimeout("parent.display()", 1000);
	if(window.location.href.indexOf('show=1') != -1) {		
		window.location.reload();		
	} else {
		if(window.opener){	
			try {
				
				if(edit_opps_do_another) {
					alert('Opportunity Saved successfully.');
					//window.opener.location.reload();
					window.location.reload();
					
				} else {				
					if(redirect_to_jetstream == 1){
						acc=$("#select_company").val();
						contact=$("#contact").val(); 
						window.location.href='redirect_to_jetstream.php?SN='+SN + '&type='+redirect_type+'&acc='+acc+'&contact='+contact;
					} else {
						window.opener.location.reload();
						window.close();
					}
				}
			} catch(e) {
				window.opener.location.reload();
				window.close();
			}
		}
	}
}

function save_table(table, idColumn, negativeIDs, delim, callback, url, oppNote)
{
	var dirties = '';
	var cnt = 0;
	for (var i = 0; i < table.length; ++i)
	{
		if (!table[i].isDirty) continue;
		if (table[i].deleted) continue;
		
		if ((negativeIDs && table[i][idColumn] < 0) || (!negativeIDs && table[i][idColumn] > 0))
		{
			if (dirties.length) dirties += delim;
			dirties += table[i].toPipes();
						
			if(typeof(oppNote) != 'undefined') {		
				if((oppNote['NoteSub'][cnt] != '') || (oppNote['NoteText'][cnt] != '')) {
					dirties += '|NOTE|'+oppNote['NoteSub'][cnt]+'|'+oppNote['NoteText'][cnt]+'|'+oppNote['NotePrivate'][cnt];									
				}
				cnt++;
			}
		}
	}
	
	
	if (dirties.length)
	{
		g_saver.m_data = dirties;
		g_saver.m_callback = callback;
		g_saver.m_params['mpower_fieldlist'] = table['_fieldlist'];
		g_saver.m_url = url;
		g_saver.save();
	}
	else callback('ok');
}

function delete_from_table(table, idColumn, tableName, removeColumn, callback, url)
{
	var deleteds = '';
	for (var i = 0; i < table.length; ++i)
	{
		if (!table[i].deleted) continue;

		if (deleteds.length) deleteds += ',';
		deleteds += "'" + table[i][idColumn] + "'";
	}

	if (deleteds.length)
	{
		g_saver.reset();
		g_saver.m_params['mpower_remove_table'] = tableName;
		g_saver.m_params['mpower_remove_field'] = removeColumn;
		g_saver.m_params['mpower_remove_value'] = deleteds;
		g_saver.m_callback = callback;
		g_saver.m_url = url;
		g_saver.save();
	}
	else callback('ok');
}

function saved_new_opps(result)
{
	if (result.has_error)
	{
		done_saving(Saver.stripHTML(result));
		return;	
	}
	if (result == 'Insert opportunities failed.')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	
	if(result.indexOf("new Array") != -1)
	{	
		eval('result = ' + result);	
	}	

	for (var k = 0; k < g_opp_products_xref.length; ++k)
	{
		for (var n = 0; n < result.length; n += 2)
		{						
			if (g_opp_products_xref[k][g_opp_products_xref.DealID] == result[n])
				g_opp_products_xref[k][g_opp_products_xref.DealID] = result[n + 1];
				break;
		}
	}
	
	
	for (var k = 0; k < g_editedSubAnswers.length; ++k)
	{
		for (var n = 0; n < result.length; n += 2)
		{
			if (g_editedSubAnswers[k][0] == result[n])
			{
				g_editedSubAnswers[k][0] = result[n + 1];
				break;
			}
		}
	}
	
	save_table(g_opps, g_opps.DealID, false, '|', saved_existing_opps, '../data/update_opps.php');
}

function saved_existing_opps(result)
{
	if (result != 'ok')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	var dirties = '';
	for (var i = 0; i < g_editedSubAnswers.length; ++i)
	{
		if (dirties.length) dirties += '\012';
		dirties += g_editedSubAnswers[i].toPipes();
	}

	if (dirties.length > 0)
	{
		g_saver.reset();
		g_saver.m_data = dirties;
		g_saver.m_callback = saved_subanswers;
		g_saver.m_url = "../data/set_subanswer.php";
		g_saver.save();
	}
	else
		saved_subanswers('true');
}

function saved_subanswers(result)
{
	if (result != 'true')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	save_table(g_people, g_people.PersonID, false, '|', saved_existing_people,
		'../data/update_people_board.php');
}

function saved_existing_people(result)
{
	if (result != 'ok')
	{
		alert('saved_existing_people');
		done_saving(Saver.stripHTML(result));
		return;
	}
	save_table(g_snoozealerts, g_snoozealerts.SnoozeAlertsID, true, '\n', saved_new_snoozealerts,
		'../data/insert_snoozealerts.php');
}

function saved_new_snoozealerts(result)
{
	if (result != 'ok')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	save_table(g_snoozealerts, g_snoozealerts.SnoozeAlertsID, false, '|',
			saved_existing_snoozealerts, '../data/update_snoozealerts.php');
}

function saved_existing_snoozealerts(result)
{
	if (result != 'ok')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	delete_from_table(g_snoozealerts, g_snoozealerts.SnoozeAlertsID, 'snoozealerts', 'SnoozeAlertsID',
		deleted_snoozealerts, '../data/remove_rows.php');
}

function deleted_snoozealerts(result)
{
	if (result != 'ok')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	
	save_table(g_user, g_user.PersonID, false, '|', saved_user_settings, '../data/update_people_board.php');
}

//Added to save user view settings  10/19/2004
function saved_user_settings(result)
{
	//alert('About to save login.');
	if(result!='ok')
	{
		done_saving(Saver.stripHTML(result));
		return;		
	}
	save_table(g_logins, g_logins.PersonID, false, '|', saved_alias_settings, '../data/update_logins.php');
	
}



function saved_alias_settings(result)
{
	if (result != 'ok')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	for(var i=0; i<g_opp_products_xref.length; i++)
	{
		if(g_opp_products_xref[i].added==true)
		{	
			g_opp_products_xref[i].added=false;
			g_opp_products_xref[i].isDirty=true;
		}
	}
	
	
	
	save_table(g_opp_products_xref, g_opp_products_xref.ID, true, '\012', saved_new_prod_xrefs, '../data/insert_arr_opp_prod_xref.php');
}


function saved_new_prod_xrefs(result)
{
	//alert('About to save existing xrefs.');
	if (result != 'ok')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	for(var i=0; i<g_opp_products_xref.length; i++)
	{
		if(g_opp_products_xref[i].isDirty == true) g_opp_products_xref[i].isDirty=false;
		if(g_opp_products_xref[i].edited==true)
		{
			g_opp_products_xref[i].edited=false;
			g_opp_products_xref[i].isDirty=true;
		}
	}
	
	
	
	save_table(g_opp_products_xref, g_opp_products_xref.ID, false, '|', saved_existing_prod_xrefs, '../data/update_arr_opp_prod_xrefs.php');
}

function saved_existing_prod_xrefs(result)
{
	//alert('Saved existing xrefs. result=' + result);
	if (result != 'ok')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	delete_from_table(g_opp_products_xref, g_opp_products_xref.ID, 'Opp_Product_XRef', 'ID', deleted_opp_prod_xrefs, '../data/remove_rows.php');
}

function deleted_opp_prod_xrefs(result)
{
	//alert('deleted xrefs. result=' + result);
	if (result != 'ok')
	{
		done_saving(Saver.stripHTML(result));
		return;
	}
	done_saving();
}
