function trim(str) {
    return str.replace(/^\s*|\s*$/g,"");
}

function wr(text)
{
    document.write(text);
}

function wrln(text)
{
    document.writeln(text);
}


function makeBoxHeader(label)
{
    var ret = '<td height="40" colspan=2>';

    ret += '<table width=100% height=100% cellpadding=0 cellspacing=0 border=0><tr><td><img src="../images/edophead-left.gif" ></td>';
    ret += '<td width=100% height=100%';
    ret += ' class="edop_header" valign="top">';
    // here's an extra table to move the heading up a couple of pixels
    ret += '<table width=100% height=35 cellpadding=0 cellspacing=0 border=0><tr><td class="plabel_eo" style="text-align: left;" height=100% width=100% valign="center">';
    ret += label;
    ret += '</td></tr></table>';
    ret += '</td><td><img src="../images/edophead-right.gif"></td></tr></table></td>';
    return ret;
}

function makeBoxMiddleTop()
{

    var ret = '<table width=100% height=42 cellpadding=0 cellspacing=0 border=0><tr><td><img src="../images/edopmiddle-left.gif" ></td>';
    ret += '<td width=100% ';
    ret += ' class="edop_middle" style="text-align: left;" valign="center"';
    ret += '>';
    return ret;
}

function makeBoxMiddleBottom()
{
    return '</td><td><img src="../images/edopmiddle-right.gif"></td></tr></table>';
}

function makeBoxMileTop(Mile)
{

    var ret = '<table width=100% height=53 cellpadding=0 cellspacing=0 border=0><tr><td id="'+Mile+'-left"><img src="../images/edopmile-left.gif" ></td>';
    ret += '<td id="'+Mile+'-back" width=100% ';
    ret += ' class="edop_mile" ';
    ret += '>';
    return ret;
}

function makeBoxMileBottom(Mile)
{
    return '</td><td id="'+Mile+'-right"><img src="../images/edopmile-right.gif"></td></tr></table>';
}

function write_header_title(img, text)
{
    document.writeln('<table border=0><tr><td><img src="../images/' + img + '" align="left">');
    document.writeln('</td><td class="title" valign="middle" >' + text + '</td></tr></table>');
}

function write_header() {
		g_admin_mode = false;
    var header = '';
    if (g_addMode) {
        header = 'Add Note';
    }
    else {
        header = 'Opportunity Notes';
    }	

    Header.setText(header);
    wr(Header.makeHTML ());
}

function load_page(page_no) {
	var temp_url = window.location.href;
	temp_url = temp_url.replace('#', '');
	
	var find_page_indx = temp_url.indexOf('&page=')	
	if(find_page_indx >= 0) {
		temp_url = temp_url.substr(0,find_page_indx);
	}
	temp_url = temp_url.replace('&success=1', '');
		
	temp_url += '&page='+page_no;
	window.location.href = temp_url;
}

function add_opp_note(opp_id) {
	window.location.href = '../shared/add_note.php?oppid='+opp_id;
}

function cancel_note(opp_id){
	window.location.href = '../shared/opp_notes.php?oppid='+opp_id;
}

function add_note_go()
{
	document.addNewNote.submit();
}