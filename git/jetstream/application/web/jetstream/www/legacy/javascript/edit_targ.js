function cancel()
{
	if (g_somethingWasChanged) {
		if(confirm('Are you sure you want to close and lose the changes?')) window.close();
	} else {
		window.close();
	}
}



function write_header_title(img, text)
{
	document.writeln('<table border=0><tr><td><img src="../images/' + img + '" align="left">');
	document.writeln('</td><td class="plabel" valign="middle">' + text + '</td></tr></table>');
}

function makeBoxHeader(label)
{
	var ret = '<table height="40" width="100%" cellpadding=0 cellspacing=0 border=0><tr><td><img src="../images/edophead-left.gif" ></td>';
	ret += '<td width=100% ';
	ret += ' class="edop_header" style="text-align: left;vertical-align: middle" ';
	ret += '><span style="position:relative; top:-1px;">'+ label + '</span></td><td><img src="../images/edophead-right.gif"></td></tr></table>';
	return ret;
}

function is_valid_number(val)
{
	var test = parseInt(unformat_money(val), 10);
	if (isNaN(test)) return false;

	var validChars = '$0123456789,.';
	for (var i = 0; i < val.length; ++i)
	{
		var ch = val.charAt(i);
		if (validChars.indexOf(ch) == -1) return false;
	}

	return true;
}

function is_valid_date(val)
{
	var validChars = '0123456789/-';
	for (var i = 0; i < val.length; ++i)
	{
		var ch = val.charAt(i);
		if (validChars.indexOf(ch) == -1) return false;
	}

	var test = new Date(val);
	if (isNaN(test.getTime())) return false;
	return true;
}

function alert_error(msg)
{
	alert(msg);
	return false;
}


function find_matching_target(company, personID, dealID)
{
	for (var i=0; i < g_opps.length; i++)
	{
		if (g_opps[i][g_opps.DealID] == dealID) continue;
		if (g_opps[i][g_opps.Category] != '10') continue;
		if (g_opps[i][g_opps.Company].toUpperCase() != company.toUpperCase()) continue;
		if (g_opps[i][g_opps.PersonID] != personID) continue;
		return true;
	}
	return false;
}



// check to ensure that applicable conditions are met for the selected category
function check_rules()
{
	var els = document.forms['form1'].elements;

	if (ssSalesPerson && ssSalesPerson.getSelectedVal() < 0)
	{
		alert("You must assign this " + catLabel('Target') + " to a salesperson before you finish editing.");
		return false;
	}

	var spID = g_opportunity[g_oppCols.PersonID];
	if (g_admin_mode && ssSalesPerson != null)
		spID = ssSalesPerson.getSelectedVal();
	if (find_matching_target(els.company.value, spID, g_opportunity[g_oppCols.DealID]))
	{
		var ok = confirm('There is already a ' + catLabel('Target') + ' on the board with a matching prospect company and salesperson.\n\nContinue?');
		if (!ok) return false;
	}

	var now = new Date();
	var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

	if (els.targetDate.value == '')
		return alert_error('Please enter a ' + catLabel('Target') + ' date.');
	else
	{
		var temp_val = Dates.normalize(els.targetDate.value);
		if (!temp_val)
			return alert_error('Please enter a valid date in the first meeting field.');
		var test_date = new Date(temp_val);
		if (test_date.getTime() > now.getTime())
			return alert_error("Date Assigned cannot be in the future!");
		els.targetDate.value = temp_val;
	}
	els.company.value = els.company.value.trim();
	if (els.company.value == '')
		return alert_error("Please enter the prospect company's name.");
	els.division.value = els.division.value.trim();
	els.contact.value = els.contact.value.trim();
	return true;
}

function write_product_options()
{
	document.writeln('<option selected value="-1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>');
	document.writeln(list_product_options());
}

function list_product_options()
{
	var ret = '';
	for (var i = 0; i < g_products.length; ++i)
	{
		var data = g_products[i];
		ret += '<option value="' + data[g_products.ProductID] + '">' + data[g_products.Name] + '</option>';
	}

	return ret;
}


function catchchkAssigned()
{
	clearInterval(g_timer);
	check_date('targetDate', 'Date Assigned', 'nofuture');
}


function setchkAssigned()	
{
	window.focus();
	g_timer=setInterval("catchchkAssigned();", 1);
}

function show_calendar(targetName)
{
	document.body.style.cursor = 'wait';
	Windowing.dropBox.calendarTarget = document.forms['form1'].elements[targetName];
	if (targetName=='targetDate')
		Windowing.dropBox.onCalendarEdited = setchkAssigned;
	else
		Windowing.dropBox.onCalendarEdited = null;
	var win = Windowing.openSized2ndPrompt('../shared/calendar.html', 320, 600);
	win.focus();
	something_changed();
	document.body.style.cursor = 'auto';
}

var currentClockTarget = null;

function show_clock(targetName)
{
	currentClockTarget = targetName;
	document.body.style.cursor = 'wait';
	Windowing.dropBox.clockInitialTime = document.forms['form1'].elements[currentClockTarget].value;
	Windowing.dropBox.clockCallback = clock_callback;
	Windowing.openSized2ndPrompt('../shared/clock.html', 100, 250).focus();
	document.body.style.cursor = 'auto';
}

function clock_callback(time)
{
	document.forms['form1'].elements[currentClockTarget].value = time;
	something_changed();
}

function category_changed()
{
	check_category_closed();
	something_changed();
}

function check_category_closed(fromLoaded)
{
	if (fromLoaded) fromLoaded = true;
	else fromLoaded = false;
	var label = document.getElementById('closedlabel');
	var els = document.forms['form1'].elements;
	var cat = value_of_radio(document.forms['form1'].elements.category);

	if (cat == 6)
	{
		label.className = 'taglabel';

		if (g_opportunity[g_oppCols.ActualCloseDate] != '')
		{
			var actualCloseDate=Dates.mssql2us(g_opportunity[g_oppCols.ActualCloseDate]);
			var pos = actualCloseDate.indexOf(' ');
			if (pos != -1) actualCloseDate = actualCloseDate.substr(0, pos);
			els.actualCloseDate.value = actualCloseDate;
		}
		else
			els.actualCloseDate.value = Dates.formatDate(new Date());
		els.actualCloseDate.disabled = false;
		els.actualCloseCal.disabled = false;
		if (els.buttonQuestions) els.buttonQuestions.disabled = false;

		var actual = parseInt(g_opportunity[g_oppCols.ActualDollarAmount], 10);
		if (actual != '' && actual != '0' && !isNaN(actual))
		{
			if (g_has_custom_amount) els.actualDollarAmount.value = actual;
			else els.actualDollarAmount.value = format_money(actual);
		}
		els.actualDollarAmount.disabled = false;

		els.nextMeeting.value = '';
		els.nextMeetingTime.value = '';
		els.nextMeeting.disabled = true;
		els.nextMeetingTime.disabled = true;
		els.nextMeetingTimePopup.disabled = true;
		els.nextMeetingCal.disabled = true;

		if (!fromLoaded && allSubAnswersChecked() == false)
		{
			var doallcheck = confirm("Check the remaining milestones?");
			if (doallcheck)
			{
				if (g_subMSEnabled)
				{
					var tempid = null;
					for (var k = 0; k < g_submilestones.length; ++k)
					{
						tempid = g_submilestones[k][g_submilestones.SubID];
						g_editedSubAnswers[tempid] = '1';
					}
				}
				if (g_req1Used) setMilestone('Requirement1', 1);
				if (g_req2Used) setMilestone('Requirement2', 1);
				setMilestone('Person', 1);
				setMilestone('Need', 1);
				setMilestone('Money', 1);
				setMilestone('Time', 1);
			}
		}
	}
	else
	{
		label.className = 'taglabel_disabled';

		els.actualCloseDate.value = '';
		els.actualCloseDate.disabled = true;
		els.actualCloseCal.disabled = true;
		if (els.buttonQuestions)
			els.buttonQuestions.disabled = true;

		els.actualDollarAmount.value = '';
		els.actualDollarAmount.disabled = true;

		els.nextMeeting.disabled = false;
		els.nextMeetingTime.disabled = false;
		els.nextMeetingTimePopup.disabled = false;
		els.nextMeetingCal.disabled = false;
	}
	g_prev_cat = cat;
}

function value_of_select(selectObj)
{
	if (!value_of_select.arguments.length) return '';

	var sel = selectObj.selectedIndex;
	if (sel == -1) return '';

	return selectObj.options[sel].value;
}

function set_select_value(sel, val)
{
	for (var i = 0; i < sel.length; ++i)
	{
		if (sel.options[i].value != val) continue;
		sel.selectedIndex = i;
		return;
	}
}

function value_of_radio(radioObj)
{
	if (!value_of_radio.arguments.length) return '';

	for (var i = 0; i < radioObj.length; ++i)
		if (radioObj[i].checked) return radioObj[i].value;

	return '';
}
function onCompanyIndexChanged(row)
{
		var els = document.forms['form1'].elements;
		els.division.value=row[3];
		els.contact.value=row[4];
}

function setSSVals()
{
	if(g_source_used)
		ssSource.setVal(g_sourceID);
	if(g_source2_used)
		ssSource2.setVal(g_source2ID);
	if(g_valuation_used)
		ssValuation.setVal(g_valuationLevel);
	if(g_admin_mode)
	{
		ssSalesPerson.setVal(g_personID);
		onChangePerson();
	}
}

function positionSelects()
{
	if (ssSource)
		ssSource.positionControls();
	if (ssSource2)
		ssSource2.positionControls();
	if (ssValuation)
		ssValuation.positionControls();
	if (ssSalesPerson)
		ssSalesPerson.positionControls();
}

function format_date(val)
{
	if (val == '&nbsp;') return val;
	var ret = Dates.mssql2us(val);
	var pos = ret.indexOf(' ');
	if (pos != -1) ret = ret.substr(0, pos);
	return ret;
}

// these are degenerate versions of functions
function get_salesperson_info(id)       // only support salesmen editing their own opps
{
    var ret = new Object();
    ret.name = g_user[0][g_user.FirstName] + ' ' + g_user[0][g_user.LastName];
    return ret;
}

function something_changed()
{
	g_somethingWasChanged = true;
}

function find_opportunity(id)
{
	for (var i = 0; i < g_opps.length; ++i)
	if (g_opps[i][g_opps.DealID] == id) return g_opps[i];

	return null;
}

function init_edit_window()
{
	g_opportunity = find_opportunity(g_dealID);
	g_prev_cat = g_old_cat = g_opportunity[g_oppCols.Category];
	if (g_opportunity == null) return; 

	var cat = g_opportunity[g_oppCols.Category];
	g_valuationLevel = g_opportunity[g_oppCols.VLevel];
	g_sourceID = g_opportunity[g_oppCols.SourceID];
	g_source2ID = g_opportunity[g_oppCols.Source2ID];

	var targetDate = Dates.mssql2us(g_opportunity[g_oppCols.TargetDate]);
	var pos = targetDate.indexOf(' ');
	var els = document.forms['form1'].elements;
	if (pos != -1)
	{
		targetDate = targetDate.substr(0, pos);
		els.targetDate.value = targetDate;
	}
	els.company.value = g_opportunity[g_oppCols.Company].unescape_html();
	els.division.value = g_opportunity[g_oppCols.Division].unescape_html();
	els.contact.value = g_opportunity[g_oppCols.Contact].unescape_html();
}

function update_from_edit_window(opp, oldCategory, oldPersonID)
{	
	opp.isDirty = true;

	var dealID = opp[g_opps.DealID];
	if ((dealID < 0 && !opp.added) || oldCategory == 9)
	{	
		opp[g_opps.DealID] = dealID;
		g_opps[g_opps.length] = opp;
		opp.color = "";
		opp.added = true;
	}
}

function patchLastMoved() {
    if(g_opportunity[g_oppCols.DealID] > -1) return;
    if(g_opportunity[g_oppCols.Category] != '3' && g_opportunity[g_oppCols.Category] != '5') return;

    var lastMoved;

    if(g_opportunity[g_oppCols.NextMeeting] != null)
        lastMoved = Dates.mssql2us(g_opportunity[g_oppCols.NextMeeting]);
    else
        lastMoved = Dates.mssql2us(g_opportunity[g_oppCols.FirstMeeting]);

    g_opportunity[g_oppCols.LastMoved] = lastMoved;
}

function broadcast_changes(all_done)
{
	var el = document.forms['form1'].elements;

	if (g_admin_mode && ssSalesPerson != null)
		g_personID = g_opportunity[g_oppCols.PersonID] = ssSalesPerson.getSelectedVal();
	else
		g_opportunity[g_oppCols.PersonID] = g_personID;

	if(g_valuation_used) g_valuationLevel = g_opportunity[g_oppCols.VLevel] = ssValuation.getSelectedVal();

	if(g_source_used)
		g_sourceID = g_opportunity[g_oppCols.SourceID] = ssSource.getSelectedVal();
	if(g_source2_used)
		g_source2ID = g_opportunity[g_oppCols.Source2ID] = ssSource2.getSelectedVal();

	companyID = g_company[0][g_company.CompanyID];
	g_opportunity[g_oppCols.CompanyID] = companyID;

	if (!g_admin_mode) g_opportunity[g_oppCols.DealID] = g_dealID;

	g_opportunity[g_oppCols.Company] = el.company.value.escape_html();
	g_opportunity[g_oppCols.Division] = el.division.value.escape_html();
	g_opportunity[g_oppCols.Contact] = el.contact.value.escape_html();
	g_opportunity[g_oppCols.TargetDate] = el.targetDate.value;
	g_opportunity[g_oppCols.Requirement1] = 0;
	g_opportunity[g_oppCols.Person] = 0;
	g_opportunity[g_oppCols.Need] = 0;
	g_opportunity[g_oppCols.Money] = 0;
	g_opportunity[g_oppCols.Time] = 0;
	g_opportunity[g_oppCols.Requirement2] = 0;

	for (var k = 0; k < g_people.length; ++k)
	{
		if (g_people[k][g_people.PersonID] == g_opportunity[g_oppCols.PersonID])
		{
			g_opportunity[g_oppCols.FirstName] = g_people[k][g_people.FirstName];
			g_opportunity[g_oppCols.LastName] = g_people[k][g_people.LastName];
			g_opportunity[g_oppCols.Color] = g_people[k][g_people.Color];
			break;
		}
	}

	g_opportunity[g_oppCols.Category] = '10';
    patchLastMoved();

    if (g_standalone) g_opportunity.isDirty = true;
    else update_from_edit_window(g_opportunity, g_oldCategory);

    if (all_done)

    {
        do_save();
        if (g_standalone)
        {
            g_saver._realClose = g_saver.close;
            g_saver.close = function()
            {
                updateSFOppFromMPower();
                g_saver._realClose();
                redirectSalesforce();
            };
        }
    }
}
function check_styles()
{
	if (g_req1Used) setMilestone('Requirement1', g_opportunity[g_oppCols.Requirement1]);
	setMilestone('Person', g_opportunity[g_oppCols.Person]);
	setMilestone('Need', g_opportunity[g_oppCols.Need]);
	setMilestone('Money', g_opportunity[g_oppCols.Money]);
	setMilestone('Time', g_opportunity[g_oppCols.Time]);
	if (g_req2Used) setMilestone('Requirement2', g_opportunity[g_oppCols.Requirement2]);
}

function toggle_clicked(daobj)
{
	something_changed();

	if (daobj.className == "milestone_no") daobj.className = "milestone_yes";
	else daobj.className = "milestone_no";
}
function getMilestone(ms)
{
	return (document.getElementById('chk' + ms).className == 'milestone_yes') ? '1' : '0';
}
function onChangePerson()
{
	something_changed();
	if(ssSalesPerson != null)
	{
		var selectVal = ssSalesPerson.getSelectedVal();
		if (selectVal > -1)
		{
			for (var k = 0; k < g_people.length; ++k)
			{
				if (g_people[k][g_people.PersonID] == selectVal)
				{
					document.getElementById('tagtable').style.backgroundColor = g_people[k][g_people.Color];
					return;
				}
			}
		}
	}
}
function onChangeSource()
{
	something_changed();
}


function onChangeValuation()
{
	something_changed();
}

function setDropdown(d, val)
{
	for(var i = 0; i < d.options.length; i++)
	{
		if (val == d.options[i].value)
		{
			d.selectedIndex = i;
			break;
		}
	}
}

function init_column_ids()
{
	var colNames = get_opportunity_columns();
	for (var i = 0; i < colNames.length; ++i)
	{
		var name = colNames[i];
		g_oppCols[name] = g_opps[name];
	}
}

function set_tag_color()
{
	document.getElementById('tagtable').bgColor = g_color;
}


function get_opportunity_columns()
{
	var ret = new Array();
	for (var k in g_opps)
	{
		var n = parseInt(k, 10);
		if (!isNaN(n)) continue;
		if (k.charAt(0) == '_' || k == 'toPipes') continue;
		ret[ret.length] = k;
	}

	return ret;
}
function resetDropDown(idDrop)
{
	var drop = document.getElementById(idDrop);
	if (drop) drop.selectedIndex = -1;
}

function setMilestone(ms, zero_or_one)
{
	document.getElementById('chk' + ms).className = (zero_or_one == '1') ? 'milestone_yes' : 'milestone_no';
	if (!g_subMSEnabled) document.getElementById('checkbox' + ms).checked = (zero_or_one == '1');
}

function getValArray()
{
	var ret = Array();
	var vals = g_valuations;
	for (var k = 0; k < vals.length; ++k)
	{
		var data = vals[k];
		if (data[g_valuations.Activated] != true)
			continue;
		var tmp = Array();
		tmp[0] = data[g_valuations.VLevel];
		tmp[1] = '[' + data[g_valuations.VLevel] + ']     ' + data[g_valuations.Label];
		ret[ret.length] = tmp;
	}
	return ret;
}


function getPeopleArray()
{
	var ret = Array();
	for (var k = 0; k < g_people.length; k++)
	{
		var data = g_people[k];
		if (data[g_people.IsSalesperson] != '1')
			continue;
		var tmp = Array();
		tmp[0] = data[g_people.PersonID];
		tmp[1] = data[g_people.LastName] + ', ' + data[g_people.FirstName];
		ret[ret.length] = tmp;
	}
	return ret;
}

