function do_admin_revive()
{
    var buttonRemRev = document.getElementById('radioR');
    buttonRemRev.innerHTML = vt('Remove');
    buttonRemRev.onclick = remove;
    something_changed();
    did_revive = true;
}

function checkDiff(actualVal, closeVal)
{
    var difMax = g_the_only_company[g_company.MaxDiff];
    var disc = Math.abs(actualVal - closeVal);
    if (disc && disc > difMax)
    {
        if (difMax > 0)
            return alert_error('The difference between actual sales of offerings and the closed amount can be no greater than ' + difMax + '.');
        else
            return alert_error('The actual sales of offerings must match the closed amount.');

    }
    return true;

}

function after_rules() {
    if('undefined' == typeof(g_do_another)) {
        g_do_another = 0;
    }

    var els = document.forms['form1'].elements;
    var cat = value_of_radio(els.category);
    var boardLocation = g_boardLocations[cat];

    if (cat==6)
    {
    }
    if (g_somethingWasChanged) broadcast_changes(!g_do_another);
	else window.close();
    if (g_do_another)
    {
        document.forms.form1.reset();
        if (g_admin_mode) ssSalesPerson.setVal(g_personID);
        l_editedSubAnswers = new Object();
        init_new_window();
        var els = document.forms['form1'].elements;
        els.actualCloseDate.disabled = true;
        if (els.buttonQuestions) els.buttonQuestions.disabled = true;
        els.actualCloseCal.disabled = true;
        els.actualDollarAmount.disabled = true;
		showClosedAmount();
        if (g_Transactional) els.transactional.checked=false;
        document.getElementById('closedlabel').className = 'taglabel_disabled_eo';
        document.getElementById('closedlabel2').className = 'taglabel_disabled_eo';

        if (g_req1Used) setMilestone('Requirement1', '0');
        setMilestone('Person', '0');
        setMilestone('Need', '0');
        setMilestone('Money', '0');
        setMilestone('Time', '0');
        if (g_req2Used) setMilestone('Requirement2', '0');
        setRadioButtons(1);

    }
    else
    {
        //if (!g_standalone) window.close();
        if (!g_somethingWasChanged) redirectSalesforce();
    }

}

// this should only be called after the form is set up
function showClosedAmount()
{
	var disp="block";
	var label='Estimated ' + g_amount_name+':';
	if (g_Transactional && document.getElementById("transactional").checked)
	{
		label=g_amount_name+':';
		disp="none";
	}
	document.getElementById('estLabel').innerHTML=label;
	//document.getElementById("rowClosedAmount").style.display=disp;
}

function launchAll(callBack)
{
    clearInterval(g_timer);
    g_callBack();
}

function catchAll(callBack) {
    g_callBack=callBack;
    g_timer=setInterval("launchAll();", 1);
}

function ok(do_another) {

    var hasPerson = getMilestone('Person') == '1';
    var hasNeed = getMilestone('Need') == '1';
    var hasMoney = getMilestone('Money') == '1';
    var hasTime = getMilestone('Time') == '1';
    if (g_req1Used) var hasReq1 = getMilestone('Requirement1') == '1';
    if (g_req2Used) var hasReq2 = getMilestone('Requirement2') == '1';
    
    var firstMeeting = document.getElementById('firstMeeting'); 
    var secondMeeting = document.getElementById('nextMeeting');
    
	if ( firstMeeting.value != '' && secondMeeting.value != ''  ){
		var firstTemp = new Array();
		firstTemp = firstMeeting.value.split("/");
		var firstDate = new Date; 
		firstDate.setDate(firstTemp[1]);
		firstDate.setMonth(firstTemp[0]- 1 );
		firstDate.setFullYear(firstTemp[2]);
		var secondTemp = new Array();
		secondTemp = secondMeeting.value.split("/");
		var secondDate = new Date; 
		secondDate.setDate(secondTemp[1]);
		secondDate.setMonth(secondTemp[0] - 1 );
		secondDate.setFullYear(secondTemp[2]);
		
		if (firstDate > secondDate){
			alert('First meeting date cannot be after the next meeting date ! ');
			return false; 
		}
		
	}
    if (!do_another) do_another = false;
    document.body.style.cursor = 'wait';
    if (check_rules())
    {	
    		if(do_another) {    			
    			//var indexVal = oppNote['NoteSub'].length;
    			//oppNote['NoteSub'][indexVal] = '';
    			//oppNote['NoteText'][indexVal] = '';			
				
				edit_opps_do_another = do_another;
				do_another = false;
    		}
        g_do_another=do_another;
        var els = document.forms['form1'].elements;
        var cat = value_of_radio(els.category);
        after_rules();
    }
    document.body.style.cursor = 'auto';
}

function revive_event() {
    revive();
}



function make_dropdowns_equal(w)
{
    var selval;
    if(g_valuation_used)
        {
        selval = document.getElementById('select_valuation');
        if(selval.clientWidth < w)
            selval.style.width = w;
        }
    var selsrc;
    if(g_source_used)
    {
        selsrc = document.getElementById('select_source');
        if(selsrc.clientWidth < w)
            selsrc.style.width = w;
    }
    var selsrc2;
    if(g_source2_used)
    {
        selsrc2 = document.getElementById('select_source2');
        if(selsrc2.clientWidth < w)
            selsrc2.style.width = w;
    }
    var selsp;
    if(g_admin_mode)
    {
        selsp  = document.getElementById('select_salesperson');
        if(selsp.clientWidth < w)
            selsp.style.width = w;
    }
}

function setDropdown(d, val)
{
    for(var i = 0; i < d.options.length; i++)
    {
        if (val == d.options[i].value)
            {
            d.selectedIndex = i;
            break;
            }
    }
}

function setDropdowns()
{
}

function getWinWidth() {
    var wdth = 120;
    if(navigator.appName == "Netscape")
    {
        wdth = window.innerWidth;
    }
    else if(navigator.appName == "Microsoft Internet Explorer")
    {
        wdth = document.body.clientWidth;
    }
    return wdth;
}

function checkWindowSize() {
    return;
    var tagTable = document.getElementById('tagtable');
    var optimalWidth = tagTable.clientWidth + (2*20);
    if (optimalWidth > getWinWidth())
    {
        var offset = optimalWidth - getWinWidth();
        window.resizeBy(offset, 0);
    }
}

function loaded()
{
    init_column_ids();
    if (g_editMode)
        init_edit_window();
    else
        init_new_window();

    var els = document.forms['form1'].elements;
    if (g_opportunity && g_opportunity[g_oppCols.Category] == '9' && !Windowing.dropBox.revive_opp)
    {
        els.nextMeeting.value = '';
        els.nextMeetingTime.value = '';
        els.nextMeeting.disabled = true;
        els.nextMeetingTime.disabled = true;
        els.nextMeetingTimePopup.disabled = true;
        els.nextMeetingCal.disabled = true;

    }
	if ( g_opportunity && g_opportunity[g_opps.Agency] == 1 ) {
		els.agency.checked = true;
	}
	
    if(g_editMode == false)
    {
        var buttonRemRev = document.getElementById("radioR");
        if (g_adminCall)
        {
            buttonRemRev.onclick = do_admin_revive;
            buttonRemRev.myData = g_opportunity;
            buttonRemRev.myDataIndex = g_opportunity.myDataIndex;
        }
        else
            buttonRemRev.onclick = revive_event;


    }

    if (g_CMInterface) document.getElementById('company').readOnly=true;
	
    setDropdowns();

    make_dropdowns_equal(210);
    initSmartSelect();
    setOfferingsList();
    checkWindowSize();


    setTimeout('positionSelects()', 1);

    if (g_target_used)
    {
        if(g_admin_mode == true)
            els.targetDate.disabled = false;
        else
            els.targetDate.disabled = true;
    }
    showClosedAmount();
}

function positionSelects()
{
    if (ssSource)
        ssSource.positionControls();
    if (ssSource2)
        ssSource2.positionControls();
    if (ssValuation)
        ssValuation.positionControls();
    if (ssSalesPerson)
        ssSalesPerson.positionControls();
    if (ssCompany)
        ssCompany.positionControls();
}


function remove_end()
{
    clearInterval(g_timer);
    broadcast_changes(true);    
}

function initSmartSelect()
{
    if(g_source_used)
        ssSource = new SmartSelect(document.getElementById('select_source'), g_sources, g_sources.SourceID, g_sources.Name, g_sourceID);
    if(g_source2_used)
        ssSource2 = new SmartSelect(document.getElementById('select_source2'), g_sources2, g_sources2.Source2ID, g_sources2.Name, g_source2ID);
    if (g_valuation_used)
    {
        var arrVals = getValArray();
        arrVals.noSort = false;
        ssValuation = new SmartSelect(document.getElementById('select_valuation'), arrVals, 0, 1, g_valuationLevel);
        ssValuation.fnSort(sortVals);
    }
    if (g_admin_mode) {
        var arrPeople = getPeopleArray();
        ssSalesPerson = new SmartSelect(document.getElementById('select_salesperson'), arrPeople, 0, 1, g_personID);
    }

    if (g_admin_mode || g_opp_age < 4) {

		if (opp_account != '') {
			document.getElementById('select_company').value = opp_account;
		}		
		
		if (opp_contact != '') {
			document.getElementById('contact').value = opp_contact;
		}		
    }
}

function onCompanyIndexChanged(row)
{
    var els = document.forms['form1'].elements;
    els.division.value=row[3];
    if (g_showcontact) els.contact.value=row[4];
	
	var g_contact_list = new Array();
	g_contact_list.Contact = 0;
	
	var count = 0;
	for (var i in g_company_list) {
		if (g_company_list[i][g_company_list.Company] == row[0] && g_company_list[i][g_company_list.Contact] != '') {
			var temp = new Array(g_company_list[i][g_company_list.Contact]);			
			g_contact_list[count] = temp;			
			count++;
		}
	}

	//ssContact = new SmartSelect(document.getElementById('contact'), g_contact_list, g_contact_list.Contact, g_contact_list.Contact);
	//ssContact.exact=false;
	//ssContact.setSelectWidth(400);
}

function answer_report()
{
    var temp = '';
    for (var k = 1; k < 7; ++k)
        temp += k + ' = ' + g_question_checks[k] + '\n';
    alert(temp);
}

function tmp_sortNameCol(a, b)
{
      if(a[0] < b[0])
          return -1;
       if(a[0] > b[0])
          return 1;

   return 0;
}


function sortVals(a, b)
{
    var termOne = a[1];
    var termTwo = b[1];
    return termOne - termTwo;
}

// returns -2 if all required milestone questions are filled in
// returns -1 if the only missing ones are checkboxes and yes/no's
// returns the milestone index of the first milestone missing a text, combo, date
// bSetMilesontes is true, then the milestones will be set on or off according to the questions
// 	if set, the return won't be valid
function allSubAnswersChecked(bSetMilestones)
{
    var ret=-2;
    var bMilestonesOn=[true, true, true, true, true, true]
// a transactional opp's milestones are always true    
    if (g_Transactional && document.getElementById("transactional").checked) 
    {
    	if (bSetMilestones) {
		if (g_req1Used)
			setMilestone('Requirement1', true);
		setMilestone('Person', true);
		setMilestone('Need', true);
		setMilestone('Money', true);
		setMilestone('Time', true);
		if (g_req2Used)
			setMilestone('Requirement2', true);
	}	
    	return -2;
    }    	
    for (var k = 0; k < g_submilestones.length; ++k)
    {
        if (!g_req1Used && g_submilestones[k][g_submilestones.Location] == 0)
            continue;
        if (!g_req2Used && g_submilestones[k][g_submilestones.Location] == 5)
            continue;
        if (g_submilestones[k][g_submilestones.IsHeading] == '1')
            continue;
        if (g_submilestones[k][g_submilestones.Mandatory] != '1') continue;
        var tempid = g_submilestones[k][g_submilestones.SubID];
        var loc=g_submilestones[k][g_submilestones.Location];
        if (g_submilestones[k][g_submilestones.CheckBox] == '1')
        {
            if (getSubAnswer(tempid) == 0)
            {
            	bMilestonesOn[g_submilestones[k][g_submilestones.Location]]=false;
                ret=-1;
            }
        }
        else if (g_submilestones[k][g_submilestones.YesNo] == '1')
        {
            if (g_submilestones[k][g_submilestones.RightAnswer] != getSubAnswer(tempid))
            {
            	bMilestonesOn[g_submilestones[k][g_submilestones.Location]]=false;
                ret=-1;
            }
        }
        else if (g_submilestones[k][g_submilestones.DropDown] == '1')
        {
            var ans=getSubAnswer(tempid);
            if (ans=='' || ans=='-1') 
            {
            	if (!bSetMilestones) return loc;
            	bMilestonesOn[g_submilestones[k][g_submilestones.Location]]=false;
       	    }    	
            //Jim, 4/4/2006
            //They answered, but is their answer approved?  At this point, need to loop through answers in sms rec,
            //check index against OK bit fields.
            for (var q=0; q < 20; q++)
            {
                if(g_submilestones[k][g_submilestones['DropChoice'+q]] == ans)
                {
                    //alert('found ans ' + ans);
                    if(g_submilestones[k][g_submilestones['DChoiceOK'+q]] != '1')
                    {
	            	if (!bSetMilestones) return loc;
	            	bMilestonesOn[g_submilestones[k][g_submilestones.Location]]=false;
                    }		        
                    break;
                }
            }
        }
        else
        {
            if (getSubAnswer(tempid)=='') 
            {
            	if (!bSetMilestones) return loc;
            	bMilestonesOn[g_submilestones[k][g_submilestones.Location]]=false;
            }
        }
    }
    if (bSetMilestones)
    {
	    if (g_req1Used) {
		setMilestone('Requirement1', bMilestonesOn[0]);
	    }
	    setMilestone('Person', bMilestonesOn[1]);
	    setMilestone('Need', bMilestonesOn[2]);
	    setMilestone('Money', bMilestonesOn[3]);
	    setMilestone('Time', bMilestonesOn[4]);
	    if (g_req2Used)
		setMilestone('Requirement2', bMilestonesOn[5]);
    }
    return ret;
}

function getSubAnswer(SubID)
{
    for (var k in l_editedSubAnswers)
    {
        if (k == SubID)
            return l_editedSubAnswers[k];
    }
    for (var k = 0; k < g_editedSubAnswers.length; ++k)
    {
        if (g_editedSubAnswers[k][0] == g_dealID
            && g_editedSubAnswers[k][1] == SubID)
            return g_editedSubAnswers[k][2];
    }
    if (g_editMode)
    {
        for (var k = 0; k < g_subanswers.length; ++k)
        {
            if (g_subanswers[k][g_subanswers.SubID] == SubID
                && g_subanswers[k][g_subanswers.DealID] == g_dealID)
                return g_subanswers[k][g_subanswers.Answer];
        }
    }
    return '';
}

function setSubAnswer(SubID, answer)
{
    l_editedSubAnswers[SubID] = answer;
    something_changed();
}

function setGlobalSubAnswer(DealID, SubID, Answer, CompletedLocation)
{
//  No longer adequate!
//  g_editedSubAnswers[g_editedSubAnswers.length] = new Array(DealID, SubID, Answer);
    var bFound = 0;
    for(var i = 0; i< g_editedSubAnswers.length; ++i)
    {
        if (g_editedSubAnswers[i][0] == DealID && g_editedSubAnswers[i][1] == SubID)
        {
            bFound = 1;
            g_editedSubAnswers[i][2] = Answer;
            g_editedSubAnswers[i][3] = CompletedLocation;
        }
    }
    if(!bFound);
        g_editedSubAnswers[g_editedSubAnswers.length] = new Array(DealID, SubID, Answer, CompletedLocation);
}



function get_opportunity_columns()
{
    var ret = new Array();
    for (var k in g_opps)
    {
        var n = parseInt(k, 10);
        if (!isNaN(n)) continue;
        if (k.charAt(0) == '_' || k == 'toPipes') continue;
        ret[ret.length] = k;
    }

    return ret;
}

function list_product_options()
{
    var ret = '';
    for (var i = 0; i < g_products.length; ++i)
    {
        var data = g_products[i];
        ret += '<option value="' + data[g_products.ProductID] + '">' + data[g_products.Name] + '</option>';
    }

    return ret;
}

// these are degenerate versions of functions
/*function get_salesperson_info(id)       // only support salesmen editing their own opps
{
    var ret = new Object();
    ret.name = g_user[0][g_user.FirstName] + ' ' + g_user[0][g_user.LastName];
    return ret;
}*/

function get_salesperson_info(id)
{
	var ret = new Object();
	var monthsNew = g_company[0][g_company.MonthsConsideredNew];

	for (var i = 0; i < g_people.length; ++i)
	{
		if (g_people[i][g_people.PersonID] != id)
			continue;
		ret.rating = g_people[i][g_people.Rating];
		ret.color = g_people[i][g_people.Color];
		ret.name = g_people[i][g_people.FirstName] + ' ' + g_people[i][g_people.LastName];
		ret.rawdata = g_people[i];

		var expDate = new Date(Dates.mssql2us(g_people[i][g_people.StartDate]));

		for (; monthsNew>0; --monthsNew)
		{
			var m = expDate.getMonth();
			if (m == 11)
			{
				expDate.setMonth(0);
				expDate.setYear(expDate.getYear() + 1);
			}
			else expDate.setMonth(m + 1);
		}
		if (expDate.getTime() > new Date().getTime())
		{
			ret.tenure = 'New';
			if (g_user[0][g_user.Level] > 1)
				ret.name += '&nbsp;(n)';
		}
		else
			ret.tenure = 'Experienced';

		ret.analysisID = g_people[i][g_people.AnalysisID];
		ret.PersonID = g_people[i][g_people.PersonID];
		ret.CompanyID= g_people[i][g_people.CompanyID];
		break;
	}

	return ret;
}

function find_opportunity(DealID)
{
    return g_opps[0];   // this is the only one we have
}





function getXMLHttpRequest() {
    if(typeof ActiveXObject!="undefined") {
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    else if(typeof XMLHttpRequest!="undefined") {
        return new XMLHttpRequest();
    }
    else {
        return null;
    }
}

function trim(str) {
    return str.replace(/^\s*|\s*$/g,"");
}

function setMilestone(mile, zero_or_one) {
    var td_left=document.getElementById(mile+'-left');
    var td_right=document.getElementById(mile+'-right');
    var td_back=document.getElementById(mile+'-back');
    if (zero_or_one =='1')
    {
        td_left.innerHTML='<img src="../images/edopmileon-left.gif" >';
        td_right.innerHTML='<img src="../images/edopmileon-right.gif" >';
        td_back.className='edop_mileon';
    }
    else
    {
        td_left.innerHTML='<img src="../images/edopmile-left.gif" >';
        td_right.innerHTML='<img src="../images/edopmile-right.gif" >';
        td_back.className='edop_mile';
    }

    if (!g_subMSEnabled)
        document.getElementById('checkbox' + mile).checked = (zero_or_one == '1');
}

function getMilestone(mile)
{
    var td_back=document.getElementById(mile+'-back');
    return (td_back.className == 'edop_mileon') ? '1' : '0';
}

function check_styles()
{
    if (g_req1Used) {
        setMilestone('Requirement1', g_opportunity[g_oppCols.Requirement1]);
    }
    setMilestone('Person', g_opportunity[g_oppCols.Person]);
    setMilestone('Need', g_opportunity[g_oppCols.Need]);
    setMilestone('Money', g_opportunity[g_oppCols.Money]);
    setMilestone('Time', g_opportunity[g_oppCols.Time]);
    if (g_req2Used)
        setMilestone('Requirement2', g_opportunity[g_oppCols.Requirement2]);
}

function toggle_clicked(daobj)
{
    something_changed();

    if (daobj.className == "milestone_no")
    {
        daobj.className = "milestone_yes";
    }
    else
    {
        daobj.className = "milestone_no";
    }
}


function is_valid_number(val)
{
    var test = parseInt(unformat_money(val), 10);
    if (isNaN(test))
        return false;

    var validChars = '$0123456789,.';
    for (var i = 0; i < val.length; ++i)
    {
        var ch = val.charAt(i);
        if (validChars.indexOf(ch) == -1) return false;
    }

    return true;
}

function is_valid_date(val)
{
    var validChars = '0123456789/-';
    for (var i = 0; i < val.length; ++i)
    {
        var ch = val.charAt(i);
        if (validChars.indexOf(ch) == -1) return false;
    }

    var test = Dates.makeDateObj(val);
    if (isNaN(test.getTime())) return false;
    return true;
}

function alert_error(msg)
{
    alert(msg);
    return false;
}

function getBillDatePrompt()
{
    var BillDatePrompt=g_the_only_company[g_company.BillDatePrompt];
    if (BillDatePrompt=='') BillDatePrompt='Bill Date';
    var lastchar = BillDatePrompt.length - 1;
    if(BillDatePrompt.charAt(lastchar) == ':')
        BillDatePrompt = BillDatePrompt.substring(0, lastchar);
    return BillDatePrompt;
}

function find_matching_target(company, personID, dealID)
{
	for (var i=0; i < g_opps.length; i++)
	{
		if (g_opps[i][g_opps.DealID] == dealID) continue;
		if (g_opps[i][g_opps.Category] != '10') continue;
		if (g_opps[i][g_opps.Company].toUpperCase() != company.toUpperCase()) continue;
		if (g_opps[i][g_opps.PersonID] != personID) continue;
		return true;
	}
	return false;
}

// check to ensure that applicable conditions are met for the selected category
function check_rules()
{	
    var hasPerson = getMilestone('Person') == '1';
    var hasNeed = getMilestone('Need') == '1';
    var hasMoney = getMilestone('Money') == '1';
    var hasTime = getMilestone('Time') == '1';
    if (g_req1Used)
        var hasReq1 = getMilestone('Requirement1') == '1';
    if (g_req2Used)
        var hasReq2 = getMilestone('Requirement2') == '1';

    if (/*!window.opener ||*/ (g_opportunity[g_oppCols.Category] == '9' && !did_revive))
        return true;
    var els = document.forms['form1'].elements;

    if (g_Transactional && els.transactional.checked) els.actualDollarAmount.value=els.estimatedDollarAmount.value;

    // if target (either radio won't be set, or it will be == 10)
    var isTarget = (value_of_radio()==10 || value_of_radio()=='') ? true : false;

    if (g_admin_mode)

    {
        if(ssSalesPerson.getSelectedVal() < 0)
        {
            alert("You must assign this opportunity to a salesperson before you finish editing.");
            return false;
        }
    }

    var now = new Date();
    var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

    var tempfmdate = Dates.makeDateObj(els.firstMeeting.value);
    var tempnmdate = Dates.makeDateObj(els.nextMeeting.value);
    var tempActualCloseDate = Dates.makeDateObj(els.actualCloseDate.value);

    if (g_target_used)
    {
        var msAssigned = (Dates.makeDateObj(els.targetDate.value)).getTime();
        if (msAssigned > today.getTime())
        {
            return alert_error('Date Assigned cannot be in the future.');
        }
    }


    if (isTarget)
    {
        if (els.firstMeeting.value != '')
            return alert_error(catLabel('Target') + ' opportunities cannot have any ' + catLabel('First Meeting') + ' Date.');
        if (els.firstMeetingTime.value != '')
            return alert_error(catLabel('Target') + ' opportunities cannot have any' + catLabel('First Meeting') + ' Time.');
        if (els.nextMeeting.value != '')
            return alert_error(catLabel('Target') + ' opportunities cannot have a Next Meeting Date.');
        if (els.nextMeetingTime.value != '')
            return alert_error(catLabel('Target') + ' opportunities cannot have a Next Meeting Time.');
        if (els.actualCloseDate.value != '')
            return alert_error(catLabel('Target') + ' opportunities cannot have an Actual Close Date.');
        if (g_admin_mode && !(!els.adjustedCloseDate) && els.adjustedCloseDate.value != '')
            return alert_error(catLabel('Target') + ' opportunities cannot have an Adj. Est. '+catLabel('Closed')+' Date.');
        if (unformat_money(els.estimatedDollarAmount.value) != '')
            return alert_error(catLabel('Target') + ' opportunities cannot have an Estimated Dollar Amount.');
        if (unformat_money(els.actualDollarAmount.value) != '')
            return alert_error(catLabel('Target') + ' opportunities cannot have an Actual Dollar Amount.');

        var spID = g_opportunity[g_oppCols.PersonID];
        if (g_admin_mode && ssSalesPerson != null) spID = ssSalesPerson.getSelectedVal();
        if (find_matching_target(els.company.value, spID, g_opportunity[g_oppCols.DealID]))
        {
                var ok = confirm('There is already a ' + catLabel('Target') + ' on the board with a matching prospect company and salesperson.\n\nContinue?');
                if (!ok) return false;
        }

    }
    else
    {
        if (els.firstMeeting.value == '')
            return alert_error('Please enter a TEST ' + catLabel('First Meeting') + ' date.');
        else
        {
            var temp_val = Dates.normalize(els.firstMeeting.value);
            if (!temp_val)
                return alert_error('Please enter a valid date in the ' + catLabel('First Meeting') + ' field.');
            els.firstMeeting.value = temp_val;
        }

        if (!els.firstMeetingTime.disabled && els.firstMeetingTime.value == '')
            return alert_error('Please enter ' + catLabel('First Meeting') + ' time.');

        if (els.nextMeeting.value != '' && els.nextMeeting.value != 'NONE')
        {
            var temp_val = Dates.normalize(els.nextMeeting.value);
                if (!temp_val)
                    return alert_error('Please enter a valid date in the next meeting date field.');
                els.nextMeeting.value = temp_val;
        }

        if (els.actualCloseDate.value != '')
        {
            var temp_val = Dates.normalize(els.actualCloseDate.value);
            if (!temp_val)
                return alert_error('Please enter a valid date in the actual close date field.');
            els.actualCloseDate.value = temp_val;
        }

        if (g_admin_mode && !(!els.adjustedCloseDate) && els.adjustedCloseDate.value != '')
        {
            var temp_val = Dates.normalize(els.adjustedCloseDate.value);
            if (!temp_val)
                return alert_error('Please enter a valid date in the adjusted close date field.');
            els.adjustedCloseDate.value = temp_val;
        }

        if (unformat_money(els.estimatedDollarAmount.value) != '' && !is_valid_number(unformat_money(els.estimatedDollarAmount.value)))
            return alert_error('Please enter a number in the estimated ' + g_amount_name + ' field.');

        if (unformat_money(els.actualDollarAmount.value) != '' && !is_valid_number(unformat_money(els.actualDollarAmount.value)))
            return alert_error('Please enter a number in the actual ' + g_amount_name + ' field.');

        els.company.value = els.company.value.trim();

        if (els.company.value == '')
            return alert_error("Please enter the prospect company's name.");
			
		

        // check new opps for duplicates (company name and opportunity)
        if (g_editMode == false && !g_standalone)
        {
            //alert('Need to handle question of duplicate opportunities with multiple products!');
        }
    }
    if (g_enforce_rules == false)
        return true;

    var cat = value_of_radio(els.category);
    var boardLocation = g_boardLocations[cat];

    if (!isTarget)
    {
        if (hasPersonQuestions && hasPerson && !g_question_checks[1])
        {
            alert('Please completely answer the questions for "Person"');
            return false;
        }

        if (hasNeedQuestions && hasNeed && !g_question_checks[2])
        {
            alert('Please completely answer the questions for "Need"');
            return false;
        }

        if (hasMoneyQuestions && hasMoney && !g_question_checks[3])
        {
            alert('Please completely answer the questions for "Money"');
            return false;
        }

        if (hasTimeQuestions && hasTime && !g_question_checks[4])
        {
            alert('Please completely answer the questions for "Time"');
            return false;
        }

        if (g_req1Used && hasReq1Questions && hasReq1 && !g_question_checks[5])
        {
            alert('Please completely answer the questions for "' + g_the_only_company[g_company.Requirement1] + '"');
            return false;
        }

        if (g_req2Used && hasReq2Questions && hasReq2 && !g_question_checks[6])
        {
            alert('Please completely answer the questions for "' + g_the_only_company[g_company.Requirement2] + '"');
            return false;
        }
    }
    //if either estimated amount or bill date is nonzero, the other must also contain a value
    if(g_bill_used)
    {
        if (unformat_money(els.estimatedDollarAmount.value) != '' && els.BillDate.value == '')
            return alert_error('Opportunities with an estimated dollar amount must also have a ' + getBillDatePrompt());
        if (els.BillDate.value != '' && unformat_money(els.estimatedDollarAmount.value) == '')
            return alert_error('Opportunities with a ' + getBillDatePrompt() + ' must also have an estimated value amount.');

    }
	
	if (els.contact.value == '' && cat >= 1 && cat <=6)
		return alert_error("Please enter the contact.");	
	
	if (els.company.value.trim() == '')
		return alert_error("Please enter the prospect company's name.");
	
    if (!isTarget)
    {
		var temp_contact = els.contact.value.split(' '); 
		if ( temp_contact.length < 2 )
			return alert_error("Please first name and last name to continue.");
    }
    // closed opportunities must give closed date and actual dollar value
    if (cat == 6)
    {
        if (els.actualCloseDate.value == '')
            return alert_error('Please enter a closed date to continue.');

        if (tempActualCloseDate.getTime() > today.getTime())
            return alert_error(catLabel('Closed') + ' date must be in the past.');

        if (tempActualCloseDate.getTime() < tempfmdate.getTime())
            return alert_error(catLabel('Closed') + ' date cannot be before ' + catLabel('First Meeting') + '.');

        if (unformat_money(els.actualDollarAmount.value) == '')
            return alert_error('Please enter the actual ' + g_amount_name + ' to continue.');

        if (getOfferingsCount(g_dealID) == 0)
            return alert_error('You must specify at least one offering for opportunities in ' + boardLocation + '.');
    }

	

    if (cat == 4 || cat == 5 || cat == 6)
    {
        // must know product type
        if (getMainOffering(g_dealID) < 0)
            return alert_error('Please select one or more offerings in order to place this opportunity into ' + boardLocation + '.');

        // dollar amount must be specified
        if (unformat_money(els.estimatedDollarAmount.value) == '')
            return alert_error('Please indicate the estimated ' + g_amount_name + ' of this opportunity to place it into ' + boardLocation + '.');

        // all predefined milestones must be checked
        if (!hasPerson || !hasNeed || !hasMoney || !hasTime)
            return alert_error('All milestones must be filled in to place the opportunity into ' + boardLocation + '.');

        // all user-defined milestones checked that are in use
        if ((g_req1Used && !hasReq1) || (g_req2Used && !hasReq2))
            return alert_error('All milestones must be filled in to place the opportunity into ' + boardLocation + '.');
        //1/6/2006: Source must be filled in to place opp in DP or Closed
        if (g_source_used && 0 > ssSource.getSelectedVal())
            return alert_error('Please indicate a source to place this opportunity into  ' + boardLocation + '.');
        if (g_source2_used && 0 > ssSource2.getSelectedVal())
            return alert_error('Please indicate a '+g_the_only_company[g_company.Source2Label]+' to place this opportunity into  ' + boardLocation + '.');

        //Must have valuation assigned
        //VALUATION
        if (g_valuation_used && ssValuation.getSelectedVal() < 0)
            return alert_error(g_ValLabel+' must be assigned in order to place the opportunity into ' + boardLocation + '.');
        //BILLING DATE STUFF
        if (g_bill_used)
        {
            if(els.BillDate.value == '')
                return alert_error('Please enter the ' + getBillDatePrompt() + ' to place the opportunity into ' + boardLocation + '.');
            else
            {
                var temp_val = Dates.normalize(els.BillDate.value);
                if (!temp_val)
                    return alert_error('Please enter a valid date in the ' + getBillDatePrompt() + ' field.');
                els.BillDate.value = temp_val;
                if ((cat == 4 || cat == 5) && Dates.makeDateObj(els.BillDate.value).getTime() < today.getTime())
                    return alert_error('Please set a ' + getBillDatePrompt() + ' in the future to place the opportunity into ' + boardLocation + '.');

                if (cat == 6 && g_admin_mode && !(!els.adjustedCloseDate))  //Adjusted close date
                {
                    var adjClose = Dates.makeDateObj(els.adjustedCloseDate.value).getTime();
                    var billDate = Dates.makeDateObj(els.BillDate.value).getTime();

                    if (billDate < adjClose)
                        return alert_error('Opportunities in ' + boardLocation + ' must have a ' + getBillDatePrompt() + ' greater than or equal to the adjusted close date.');
                }
            }
        }
    }
	
    if (cat == 2 || cat == 4)
    {
        // must have next meeting date and time to be in non-stalled
        if (els.nextMeeting.value == '' || els.nextMeetingTime.value == '')
            return alert_error('Please enter the next meeting date and time to place the opportunity into ' + boardLocation + '.');

        // ensure that the next meeting date is in the future.
        if (tempnmdate.getTime() < today.getTime())
            return alert_error('Please set a next meeting date in the future to place the opportunity into ' + boardLocation + '.');

		if(isNextMeetingClosed()) {
            return alert_error('The next meeting is closed.  Please add a new next meeting to place the opportunity into ' + boardLocation + '.');
		}
			
    }
	
	if(cat == 9) {
		category_changed(cat);
	}	
	
	if (cat == 3 || cat == 5 || cat == 9 || cat == 6) {	
		var params = {reschedule: false, followup: false};
		if (cat == 9) {
			params.allow_fm_remove = true;
		}
		
		if(!isFirstMeetingClosed() || !isNextMeetingClosed()) {
			EditOppEvent(params);
			return;    
		}
	}


	
//    // All events must be cancelled or complete
//        if (cat == 3 || cat == 5)
//    {		
//		// if next meeting time is given, it cannot be in the future
//        /*if(els.nextMeeting.value != 'NONE') {
//            if (els.nextMeeting.value != '' && (Dates.makeDateObj(els.nextMeeting.value).getTime() > today.getTime())) {
//                //return alert_error('You have entered a next meeting date in the future; this opportunity does not belong in ' + boardLocation + '.');
//				return alert_error('You have entered a next meeting date in the future; Please cancel or complete meeting for opportunity to belong ' + boardLocation + '.');
//            }
//        }*/		
//		if(!isNextMeetingClosed()){			
//			displayEventDialog();			
//			return;
//		} else if (!isFirstMeetingClosed()) {
//			displayEventDialog();			
//			return;
//		}
//    }
//	
//	if (cat == 6 || cat == 9)
//    {	
//		if(!isNextMeetingClosed()){			
//			displayEventDialog();			
//			return;
//		} else if (!isFirstMeetingClosed()) {
//			displayEventDialog();			
//			return;
//		}
//    }	
	
    if (cat == 1)
    {
        // first meeting date must be in the future
        if (tempfmdate.getTime() < today.getTime()) {
            return alert_error('Please set a ' + catLabel('First Meeting') + ' date in the future to place the opportunity into ' + boardLocation + '.');
        }

        // should not have next meeting date or time in first meeting category
        if (els.nextMeeting.value != '' || els.nextMeetingTime.value != '') {
            return alert_error('If you have a next meeting date and time, the opportunity does not belong in ' + boardLocation + '.');
        }

        // can't fill in any milestones in first meeting
        if (hasPerson || hasNeed || hasMoney || hasTime || (g_req1Used && hasReq1) || (g_req2Used && hasReq2)) {
            return alert_error('If you have achieved one or more milestones, the opportunity does not belong in ' + boardLocation + '.');
        }

        if (getOfferingsCount(g_dealID) > 0) {
            return alert_error('You are not allowed to specify offerings for opportunities in ' + boardLocation + '.');
        }

        if (unformat_money(els.estimatedDollarAmount.value) != '') {
            return alert_error('You are not allowed to specify an estimated ' + g_amount_name + ' for opportunities in ' + boardLocation + '.');
        }
    }
    else
    {
        if (Dates.makeDateObj(els.firstMeeting.value).getTime() > today.getTime() && !isFirstMeetingClosed())
            return alert_error('An opportunity with a future ' + catLabel('First Meeting') + ' date must be placed in the ' + catLabel('First Meeting') + ' category');
    }

    if (g_Transactional && els.transactional.checked && cat<4)
    	return alert_error('If this is '+g_TransLabel+' please place it in '+catLabel('Decision Point')+', '+catLabel('Decision Point Stalled')+', or '+catLabel('Closed')+'.'); 
    
    if (!(g_Transactional && els.transactional.checked) && cat > 0 && cat < 4 && hasPerson && hasNeed && hasMoney && hasTime && (!g_req1Used || hasReq1) && (!g_req2Used || hasReq2))
        return alert_error("An opportunity cannot be placed in " + boardLocation + " with all milestones filled in. It belongs in " + catLabel('Decision Point') + ".");

    if (cat == 10 && (hasPerson || hasNeed || hasMoney || hasTime || hasReq1 || hasReq2))
    {
        return alert_error(catLabel('Target') + " opportunities cannot have any filled milestones!");
    }

    return true;
}

function onClickedEditOfferings( a )
{
    document.body.style.cursor = 'wait';
    Windowing.dropBox.editOppWin = window;
	var w = 700;
	var h = 600;

	w += 32;
	h += 96;
	wleft = (screen.width - w) / 2;
	wtop = (screen.height - h) / 2;
	if (wleft < 0) {
		w = screen.width;
		wleft = 0;
	}
	if (wtop < 0) {
		h = screen.height;
		wtop = 0;
	}
//	var win = window.open('../shared/offerings.html',
//	'offerings',
//	'width=' + w + ', height=' + h + ', ' +
//	'left=' + wleft + ', top=' + wtop + ', ' +
//	'location=no, menubar=no, ' +
//	'status=no, toolbar=no, scrollbars=no, resizable=no');
	if  ( a > 0 ) {
		var win = window.open('../shared/offerings2.html',
	'offerings',
	'width=' + w + ', height=' + h + ', ' +
	'left=' + wleft + ', top=' + wtop + ', ' +
	'location=no, menubar=no, ' +
	'status=no, toolbar=no, scrollbars=yes, resizable=yes');
	}
	else {
		var win = window.open('../shared/offerings.html',
	'offerings',
	'width=' + w + ', height=' + h + ', ' +
	'left=' + wleft + ', top=' + wtop + ', ' +
	'location=no, menubar=no, ' +
	'status=no, toolbar=no, scrollbars=no, resizable=no');
	}
	win.resizeTo(w, h);
	win.moveTo(wleft, wtop);
	win.focus();

    //var win = Windowing.openSized2ndPromptScroll('../shared/offerings.html', 700, 700);
	//var win = window.open('../shared/offerings.html', 'mywindow', 'width=700px, height=500px');
    document.body.style.cursor = 'auto';
}

function setOfferingsList()
{
    var oStr = getOfferingsList(g_dealID);
    var oList = document.getElementById('listOfferings');
    oList.value = oStr;
    var estVal = document.getElementById('estimatedDollarAmount');
    var offeringsVal = getOfferingsVal(g_dealID);
    estVal.value = format_money(offeringsVal);
}

function getProductAbbr(id)
{
    return window.opener.getProdAbbr(id);
}


function catchchkNextMeeting()
{
    clearInterval(g_timer);
    chkNextMeeting();
}

function chkNextMeeting()       //called from the exit of show_calendar and from onchange in html
{
    focus();
    var els = document.forms['form1'].elements;
    var cat = value_of_radio(els.category);
    if (cat == 3 || cat == 5)
        return;
    if (g_nextMeeting == '')
        g_meetingOccurred=true;
    else
    {
        if (ci_confirm("Is this a new meeting?","New","Reschedule")) g_meetingOccurred=true;
    }
}

function setchkNextMeeting()    // set a callback so that editopp gets a chance to check nextmeeting when window closes
{
    window.focus();
    g_timer=setInterval("catchchkNextMeeting();", 1);
}

function catchchkAssigned()
{
    clearInterval(g_timer);
    check_date('edit_dateid', 'Date Assigned', 'nofuture');
}


function setchkAssigned()   // set a callback so that editopp gets a chance to check nextmeeting when window closes
{
    window.focus();
    g_timer=setInterval("catchchkAssigned();", 1);
}

function processActCloseDate() {
    daysSinceFM();
}

function show_calendar(targetName)
{
    document.body.style.cursor = 'wait';
    Windowing.dropBox.calendarTarget = document.forms['form1'].elements[targetName];
    if (targetName=='nextMeeting')
        Windowing.dropBox.onCalendarEdited = setchkNextMeeting;
    else if (targetName=='targetDate')
    {
        Windowing.dropBox.onCalendarEdited = setchkAssigned;
    }
    else if (targetName=='actualCloseDate') {
        Windowing.dropBox.onCalendarEdited = processActCloseDate;

    }
    else
        Windowing.dropBox.onCalendarEdited = null;
    var win = Windowing.openSized2ndPrompt('../shared/calendar.html', 320, 600);
    win.focus();
    something_changed();
    document.body.style.cursor = 'auto';
}

function processFirstMeeting()
{
    daysSinceFM();
    checkDateAssigned();
}


function show_calendar_clock(calendarTargetName, clockTargetName)
{
    currentClockTarget = clockTargetName;
    document.body.style.cursor = 'wait';
    Windowing.dropBox.initialDate = document.forms['form1'].elements[calendarTargetName].value;
    Windowing.dropBox.calendarTarget = document.forms['form1'].elements[calendarTargetName];
    Windowing.dropBox.clockInitialTime = document.forms['form1'].elements[currentClockTarget].value;
    Windowing.dropBox.clockCallback = clock_callback;
    if (calendarTargetName=='nextMeeting')
        Windowing.dropBox.onCalendarEdited = setchkNextMeeting;
    else
        Windowing.dropBox.onCalendarEdited = processFirstMeeting;
    var win = Windowing.openSized2ndPrompt('../shared/calendar_clock.html', 320, 800);
    win.focus();
    something_changed();
    document.body.style.cursor = 'auto';
}

function show_clock(targetName)
{
    currentClockTarget = targetName;
    document.body.style.cursor = 'wait';
    Windowing.dropBox.clockInitialTime = document.forms['form1'].elements[currentClockTarget].value;
    Windowing.dropBox.clockCallback = clock_callback;
    Windowing.openSized2ndPrompt('../shared/clock.html', 100, 250).focus();
    document.body.style.cursor = 'auto';
}

function clock_callback(time)
{
    document.forms['form1'].elements[currentClockTarget].value = time;
    something_changed();
}

function setNextMeetingNone(cat)
{
    var nm = document.getElementById('nextMeeting');
    var nmt = document.getElementById('nextMeetingTime');
    if(cat == '3' ||cat == '5')
    {
        nm.style.fontFamily = 'ARIAL BLACK';
        nm.value='NONE';
        nmt.style.fontFamily = 'ARIAL BLACK';
        nmt.value='NONE';
    }
    else
    {
        nm.style.fontFamily = '';
        if(nm.value == 'NONE') nm.value='';
        nmt.style.fontFamily = '';
        if(nmt.value == 'NONE') nmt.value='';
    }
}

function setRadioButtons(cat) {

    // set all the category buttons on way or the other
   if ((g_target_used && g_editMode) || opp_account != '')
        document.getElementById('radioT').className = (cat==10) ? 'panelblue' : 'panel';
    document.getElementById('radioFM').className = (cat==1) ? 'panelon' : 'panel';
    document.getElementById('radioC').className = (cat==6) ? 'panelgold' : 'panel';
    document.getElementById('radioR').className = (cat==9) ? 'panelon' : 'panel';
    document.getElementById('radioI').className = (cat==2) ? 'panelmidon' : 'panelmid';
    document.getElementById('radioIS').className = (cat==3) ? 'panelsmallon' : 'panelsmall';
    document.getElementById('radioDP').className = (cat==4) ? 'panelmidon' : 'panelmid';
    document.getElementById('radioDPS').className = (cat==5) ? 'panelsmallon' : 'panelsmall';

    if (cat==3 || cat==5) {
        document.getElementById('panelnext').bgColor='#EE2329'; // red stalled
    }
    else if (cat==2 || cat==4) {
        document.getElementById('panelnext').bgColor='#06BA46'; // green ip & sp
    }
    else {
        document.getElementById('panelnext').bgColor='#949597';
    }

}

/*function checkCatRules(cat)
{
}
*/

function zeroSubMilestones() {
    if (g_subMSEnabled)
    {
        var tempid = null;
        for (var k = 0; k < g_submilestones.length; ++k)
        {
            tempid = g_submilestones[k][g_submilestones.SubID];
            l_editedSubAnswers[tempid] = '0';
        }
    }
    if (g_req1Used)
        setMilestone('Requirement1', 0);
    if (g_req2Used)
        setMilestone('Requirement2', 0);
    setMilestone('Person', 0);
    setMilestone('Need', 0);
    setMilestone('Money', 0);
    setMilestone('Time', 0);
}

function category_changed(cat)
{
    if(cat < 0) return;
    if(cat == 10 && ((g_opportunity[g_oppCols.NextMeeting] != '' || g_opportunity[g_oppCols.NextMeetingTime] != '') || (opp_account != '' && ($('#nextMeeting').val() != '' || $('#nextMeetingTime').val() != '' || $('#firstMeeting').val() != '' || $('#firstMeetingTime').val() != '' ))))
    {
        if(confirm(catLabel('Target') + ' opportunities cannot have dates or times set for meetings, milestones or offerings.  Clear values?'))
        {
            document.getElementById('firstMeeting').value = '';
            document.getElementById('firstMeetingTime').value = '';
            document.getElementById('sinceFirstMeetingTime').value = '';
            document.getElementById('nextMeeting').value = '';
            document.getElementById('nextMeetingTime').value = '';
			
            jet_events = new Array();
			// FMEventEditor = new Array();
			// NMEventEditor = new Array();			

            if (g_bill_used)
                document.getElementById('BillDate').value = '';
            document.getElementById('estimatedDollarAmount').value = '';

            zeroSubMilestones();
            zeroOfferings(g_dealID);
            setOfferingsList();
        } else return;
    }
	
	
    if (cat != 9 || g_editMode || (opp_account != '')) setRadioButtons(cat);
	// check if the category is closed or removed show the event completion dialog
	
	if (cat == 3 || cat == 5 || cat == 9 || cat == 6) {	
		var params = {reschedule: false, followup: false, do_after_func: function() { category_changed(cat) } };
		if (cat == 9) {
			params.allow_fm_remove = true;
		}
		
		if(!isFirstMeetingClosed() || !isNextMeetingClosed()) {
			EditOppEvent(params);
			return;
			// displayEventDialog();
			//return;
		// } else if (!isFirstMeetingClosed()) {
			//EditOppEvent({reschedule: false, followup: false});
			// displayEventDialog();
			//return;
		}
    }
	
	check_category_closed();
	
    if(did_revive) initAfterRevive();
	something_changed();
	
	if(cat == 9) {
		remove();
	}
}

function initAfterRevive()
{
    var label = document.getElementById('closedlabel');
    var label2 = document.getElementById('closedlabel2');

    var els = document.forms['form1'].elements;
    var cat = value_of_radio(document.forms['form1'].elements.category);
    var today = new Date();

    if(cat=='10'||cat=='1')
    {
        els.firstMeeting.value = '';
        els.firstMeetingTime.value = '';
        els.nextMeeting.value = '';
        els.nextMeetingTime.value = '';
        els.actualDollarAmount.value = '';
        els.estimatedDollarAmount.value = '';
    }
    if(cat == '10' && g_admin_mode && g_target_used)
    {
        els.targetDate.value = Dates.formatShortDate(today);
    }
    var buttonRemRev = document.getElementById("radioR");
    if (g_adminCall)
    {
        buttonRemRev.onclick = do_admin_revive;
        buttonRemRev.myData = g_opportunity;
        buttonRemRev.myDataIndex = g_opportunity.myDataIndex;
    }
    else {
        buttonRemRev.onclick = revive_event;
    }
}

function catchMilestoneCheck()
{
    clearInterval(g_timer);
    doOfferingActuals();
}

// if an optional callback function is passed as an argument
// the offerings_actual will call it on ok
function doOfferingActuals()        //called from the exit of show_calendar and from onchange in html
{
    var dollAmt=unformat_money(document.forms['form1'].elements.actualDollarAmount.value);
    if (dollAmt == '' || dollAmt==0) return;
    document.body.style.cursor = 'wait';
    Windowing.dropBox.editOppWin = window;
    if (arguments.length > 0)
    {
        Windowing.dropBox.callBack= catchAll;
        Windowing.dropBox.callFunc=arguments[0];
    }
    var win = Windowing.openSized2ndPromptScroll('../shared/offerings_actual.html',500, 700);

    win.focus();
    //something_changed();
    document.body.style.cursor = 'auto';
}

function milestoneCheckCallBack()
{
    g_timer=setInterval("catchMilestoneCheck();", 1);
}

// sets all the milestone questions it can to ok
function doAllMilestoneChecks()
{
    if (g_subMSEnabled)
    {
        var tempid = null;
        for (var k = 0; k < g_submilestones.length; ++k)
        {
            if (!g_req1Used && g_submilestones[k][g_submilestones.Location] == 0)
                continue;
            if (!g_req2Used && g_submilestones[k][g_submilestones.Location] == 5)
                continue;
            if (g_submilestones[k][g_submilestones.IsHeading] == '1')
                continue;
            if (g_submilestones[k][g_submilestones.Mandatory] != '1')
                continue;
            var tempid = g_submilestones[k][g_submilestones.SubID];
            if (g_submilestones[k][g_submilestones.CheckBox] == '1')
                l_editedSubAnswers[tempid] = '1';
            else if (g_submilestones[k][g_submilestones.YesNo] == '1')
                l_editedSubAnswers[tempid]=g_submilestones[k][g_submilestones.RightAnswer];
        }
    }
}

function openAutoRenew()
{
    Windowing.dropBox.g_opportunity = g_opportunity;
    Windowing.dropBox.g_oppCols = g_oppCols;
    Windowing.dropBox.editOppWin = window;
    Windowing.dropBox.hideFirstQuestion = false;

	var w = 600;
	var h = 300;

	w += 32;
	h += 96;
	wleft = (screen.width - w) / 2;
	wtop = (screen.height - h) / 2;
	if (wleft < 0) {
		w = screen.width;
		wleft = 0;
	}
	if (wtop < 0) {
		h = screen.height;
		wtop = 0;
	}
	var win = window.open('../shared/renew.html',
	'offerings',
	'width=' + w + ', height=' + h + ', ' +
	'left=' + wleft + ', top=' + wtop + ', ' +
	'location=no, menubar=no, ' +
	'status=no, toolbar=no, scrollbars=no, resizable=no');
	win.resizeTo(w, h);
	win.moveTo(wleft, wtop);
	win.focus();


   // Windowing.openSized2ndPrompt('../shared/renew.html', 300, 500);
}

// enable/disable inputs for actual close date, actual dollar amount
function check_category_closed(fromLoaded)
{
    if (fromLoaded)
        fromLoaded = true;
    else
        fromLoaded = false;
    var label = document.getElementById('closedlabel');
    var label2 = document.getElementById('closedlabel2');
    var els = document.forms['form1'].elements;
    var cat = value_of_radio(document.forms['form1'].elements.category);
    var today = new Date();

    if (cat == 6)
    {
        label.className = 'taglabel_eo';
        label2.className = 'taglabel_eo';

        if (g_opportunity[g_oppCols.ActualCloseDate] != '')
        {
            var actualCloseDate = format_date(Dates.mssql2us(g_opportunity[g_oppCols.ActualCloseDate]));
            var pos = actualCloseDate.indexOf(' ');
            if (pos != -1) actualCloseDate = actualCloseDate.substr(0, pos);
            els.actualCloseDate.value = actualCloseDate;
        }
        else
            els.actualCloseDate.value = Dates.formatShortDate(new Date());
        els.actualCloseDate.disabled = false;
        els.actualCloseCal.disabled = false;
        if (els.buttonQuestions)
            els.buttonQuestions.disabled = false;

        var actual = parseInt(unformat_money(g_opportunity[g_oppCols.ActualDollarAmount]), 10);
        if (actual != '' && actual != '0' && !isNaN(actual))
        {
            els.actualDollarAmount.value = format_money(actual);
        }
        els.actualDollarAmount.disabled = false;
        if (!(!els.adjustedCloseDate))
            els.adjustedCloseDate.disabled = true;
        var labelAdjEst = document.getElementById('labelAdjEst');
        if (labelAdjEst)
            labelAdjEst.className = 'taglabel_disabled_eo';
        //els.btnOfferingsA.disabled = false;

        //els.nextMeeting.value = '';
        //els.nextMeetingTime.value = '';
        els.nextMeeting.disabled = true;
        els.nextMeetingTime.disabled = true;
        els.nextMeetingTimePopup.disabled = true;
        els.nextMeetingCal.disabled = true;

        var docheck = false;
        if (!fromLoaded)
        {
            var AnsStatus=allSubAnswersChecked();
            if (AnsStatus==-1)  // only checks and yes/no's left
            {
                var doallcheck = confirm("There are required yes/no questions that haven't been answered.  Should they be answered automatically?");
                if (doallcheck)
                {
                    doAllMilestoneChecks();
                    if (g_req1Used)
                        setMilestone('Requirement1', 1);
                    if (g_req2Used)
                        setMilestone('Requirement2', 1);
                    setMilestone('Person', 1);
                    setMilestone('Need', 1);
                    setMilestone('Money', 1);
                    setMilestone('Time', 1);
                }
            }
            else if (AnsStatus>=0)
            {
                docheck = confirm("There are required questions that haven't been answered.  Do you want to answer them now?");
                if (docheck)

                {
                    doAllMilestoneChecks();
                    popupSubMilestones(g_mileStoneLocs[AnsStatus], true);
                }
            }

            if (docheck)
            {
                window.checkMSWinDone = function()
                {
                    try
                    {
                        if (window.criteriaWin.document)
                        {
                            void(0);
                        }
                    }
                    catch (e)
                    {
                        openAutoRenew();
                        return;
                    }
                    setTimeout('checkMSWinDone();', 500);
                }
                setTimeout('checkMSWinDone();', 500);
            }
            else
                openAutoRenew();
        }

        //Windowing.dropBox.editOppWin = window;
        //var win = Windowing.openSized2ndPrompt('../shared/close_wizard.php', 500, 700);
        //win.focus();
    }
    else
    {

        label.className = 'taglabel_disabled_eo';
        label2.className = 'taglabel_disabled_eo';

        els.actualCloseDate.value = '';
        els.actualCloseDate.disabled = true;
        if (!(!els.adjustedCloseDate))
            els.adjustedCloseDate.disabled = false;
        var labelAdjEst = document.getElementById('labelAdjEst');
        if (labelAdjEst)
            labelAdjEst.className = 'plabel_eo';
        els.actualCloseCal.disabled = true;
        if (els.buttonQuestions)
            els.buttonQuestions.disabled = true;

        els.actualDollarAmount.value = '';
        els.actualDollarAmount.disabled = true;
        //els.btnOfferingsA.disabled = true;

        els.nextMeeting.disabled = false;
        els.nextMeetingTime.disabled = false;
        els.nextMeetingTimePopup.disabled = false;
        els.nextMeetingCal.disabled = false;

    }

    g_prev_cat = cat;

}

function value_of_select(selectObj)
{
    if (!value_of_select.arguments.length) return '';

    var sel = selectObj.selectedIndex;
    if (sel == -1) return '';

    return selectObj.options[sel].value;
}

function value_of_radio(radioObj)
{
    // ok, here comes the brute force method, but it works
    if (((g_target_used && g_editMode == true) || opp_account != '') && document.getElementById("radioT").className=='panelblue')
        return '10';
    if (document.getElementById("radioFM").className=='panelon') return '1';
    if (document.getElementById("radioC").className=='panelgold') return '6';
    if (document.getElementById("radioR").className=='panelon') return '9';
    if (document.getElementById("radioI").className=='panelmidon') return '2';
    if (document.getElementById("radioDP").className=='panelmidon') return '4';
    if (document.getElementById("radioIS").className=='panelsmallon') return '3';
    if (document.getElementById("radioDPS").className=='panelsmallon') return '5';

    return '';
}

function onChangePerson()
{
    something_changed();
    var now = new Date();
    var today = Dates.formatShortDate(now);   
    document.getElementById('edit_dateid').value = today;
}

function do_edit_questions()
{
    Windowing.openSized2ndPrompt('../data/close_questions.php?oppid=' + g_opportunity[g_oppCols.DealID], 320, 600);
}

function wr(text)
{
    document.write(text);
}

function wrln(text)
{
    document.writeln(text);
}

function onFocusDollar()
{
    var len=document.selection.createRange().text.length;
    this.value=unformat_money(this.value);
    if (len) this.select();
//  document.selection.createRange().move('character',0);
}

function onBlurDollar()
{
    this.value=format_money(unformat_money(this.value));
}

function makeBoxHeader(label)
{
    var ret = '<td height="40" colspan=2>';

    ret += '<table width=100% height=100% cellpadding=0 cellspacing=0 border=0><tr><td><img src="../images/edophead-left.gif" ></td>';
    ret += '<td width=100% height=100%';
    ret += ' class="edop_header" valign="top">';
    // here's an extra table to move the heading up a couple of pixels
    ret += '<table width=100% height=35 cellpadding=0 cellspacing=0 border=0><tr><td class="plabel_eo" style="text-align: left;" height=100% width=100% valign="center">';
    ret += label;
    ret += '</td></tr></table>';
    ret += '</td><td><img src="../images/edophead-right.gif"></td></tr></table></td>';
    return ret;
}

function makeBoxMiddleTop()
{

    var ret = '<table width=100% height=42 cellpadding=0 cellspacing=0 border=0><tr><td><img src="../images/edopmiddle-left.gif" ></td>';
    ret += '<td width=100% ';
    ret += ' class="edop_middle" style="text-align: left;" valign="center"';
    ret += '>';
    return ret;
}

function makeBoxMiddleBottom()
{
    return '</td><td><img src="../images/edopmiddle-right.gif"></td></tr></table>';
}

function makeBoxMileTop(Mile)
{

    var ret = '<table width=100% height=53 cellpadding=0 cellspacing=0 border=0><tr><td id="'+Mile+'-left"><img src="../images/edopmile-left.gif" ></td>';
    ret += '<td id="'+Mile+'-back" width=100% ';
    ret += ' class="edop_mile" ';
    ret += '>';
    return ret;
}

function makeBoxMileBottom(Mile)
{
    return '</td><td id="'+Mile+'-right"><img src="../images/edopmile-right.gif"></td></tr></table>';
}

// Applies to vertical text on category pushbuttons.
// First, get any user-defined alias for the label
// Then, interpolated <br>'s
function vt(lbl)
{
    var al = catLabel(lbl);
    var v = '';
    for(var i = 0; i< al.length; i++)
    {
        if(i)
            v += '<br>';
        v += al.charAt(i);
    }
    return v;
}

function CantChange(prompt)
{
    if (g_CMInterface)
    {
        alert("The "+prompt+" cannot be changed from MPower.  Please edit the Account Name in Salesforce.com.");
        document.getElementById('division').focus();
    }
    return false;
}

function checkActual()
{
    var els = document.forms['form1'].elements;
        var cat = value_of_radio(els.category);
        if (cat == 6 && unformat_money(els.estimatedDollarAmount.value) != unformat_money(els.actualDollarAmount.value))
        {
            doOfferingActuals();
        }
}

function update_from_edit_window(opp, oldCategory, oldPersonID)
{

	//alert('edit window');

	var skip_calcs=false;
	if(arguments.length > 3)
	skip_calcs=('skip_calcs' == arguments[3]);
	//document.body.style.cursor = 'wait';

	opp.isDirty = true;

	var dealID = opp[g_opps.DealID];
	if ((dealID < 0 && !opp.added) || oldCategory == 9)
	{	//alert('1');
		// add a new row & get a new deal ID
		if (dealID == -1)
			dealID = --g_newDealID;
		opp[g_opps.DealID] = dealID;
		g_opps[g_opps.length] = opp;
		var info = get_salesperson_info(opp[g_opps.PersonID]);
		opp.color = info.color;
		//Grid.addOpportunity(opp);
		opp.added = true;
	}
	else if (oldCategory == opp[g_opps.Category] && oldPersonID == opp[g_opps.PersonID])
	{	//alert('2')
		// keep the cell in the same place, and avoid re-flowing
		//var cellID = Grid.removeOpportunity(opp, true);
		//Grid.addOpportunity(opp, cellID);
	}
	else
	{	//alert(3)
		if (oldPersonID != opp[g_opps.PersonID])
		opp.color = get_salesperson_info(opp[g_opps.PersonID]).color;
		var newCategory = opp[g_opps.Category];
		var newPersonID = opp[g_opps.PersonID];
		opp[g_opps.Category] = oldCategory;
		opp[g_opps.PersonID] = oldPersonID;
		//Grid.removeOpportunity(opp);
		opp[g_opps.Category] = newCategory;
		opp[g_opps.PersonID] = newPersonID;
		//Grid.addOpportunity(opp);
	}
	if(!skip_calcs)
	{

		//Analysis.calcFor(opp[g_opps.PersonID]); // recalculate analysis for that user
		//Trends.calc();
		//calcOppBorders();
		//calcAllAlerts(); // needs to be after trends and analysis
	}

	if(window.opener && g_isSpreadsheet && false) {
		var win = Windowing.getWindow(g_user[0][g_user.PersonID]+'spreadsheet');
		if (win)
		win.update_from_edit_window(opp, oldCategory, oldPersonID);
	
		win = Windowing.getWindow(g_user[0][g_user.PersonID]+'alerts');
		if (win) win.update_from_edit_window();
	
		//if (!Header.isBlinking('Save'))
		//Header.blinkOn('Save');
		//Header.enableButton('Save');
		window.opener.update_from_edit_window(g_opportunity, g_oldCategory, g_oldPersonID);
		window.close();
	}

	//document.body.style.cursor = 'auto';
}


function broadcast_changes(all_done)
{
    var el = document.forms['form1'].elements;

    if (g_admin_mode)
    {
        g_personID = g_opportunity[g_oppCols.PersonID] = ssSalesPerson.getSelectedVal();
    }
    else
    {
        g_opportunity[g_oppCols.PersonID] = g_personID;
    }
    //alert(g_opportunity[g_oppCols.AutoReviveDate]);

    if (g_valuation_used) g_valuationLevel = g_opportunity[g_oppCols.VLevel] = ssValuation.getSelectedVal();
    if (g_source_used) g_sourceID = g_opportunity[g_oppCols.SourceID] = ssSource.getSelectedVal();
    if (g_source2_used) g_source2ID = g_opportunity[g_oppCols.Source2ID] = ssSource2.getSelectedVal();


    companyID = g_company[0][g_company.CompanyID];	
   /* if (companyID)
    {
        var tempstr = "CompanyID and cookies are inconsistent. Please contact customer support with the following information:\n";
        tempstr += "CompanyID = " + companyID + "\n";
        tempstr += "mpower_companyid = " + companyID + "\n";
        alert(tempstr);
    }*/
    g_opportunity[g_oppCols.CompanyID] = companyID;

    if (!g_adminCall)
        g_opportunity[g_oppCols.DealID] = g_dealID;

    g_opportunity[g_oppCols.Company] = el.company.value.escape_html();
    g_opportunity[g_oppCols.Division] = el.division.value.escape_html();
    if (g_showcontact) g_opportunity[g_oppCols.Contact] = el.contact.value.escape_html();

    if (g_Transactional) g_opportunity[g_oppCols.Transactional]=el.transactional.checked ? '1' : '0';
    
    g_opportunity[g_oppCols.FirstMeeting] = el.firstMeeting.value;


//  g_opportunity[g_oppCols.ProductID] = ssOfferings.getSelectedVal();
    g_opportunity[g_oppCols.ProductID] = getMainOffering(g_dealID);


    g_opportunity[g_oppCols.EstimatedDollarAmount] = unformat_money(el.estimatedDollarAmount.value);
    g_opportunity[g_oppCols.ActualDollarAmount] = unformat_money(el.actualDollarAmount.value);
    g_opportunity[g_oppCols.ActualCloseDate] = el.actualCloseDate.value;

    g_opportunity[g_oppCols.NextMeeting] = Dates.normalize(el.nextMeeting.value);
    if(el.nextMeetingTime.value != 'NONE')
        g_opportunity[g_oppCols.NextMeetingTime] = el.nextMeetingTime.value;
    g_opportunity[g_oppCols.FirstMeetingTime] = el.firstMeetingTime.value;
    if (g_admin_mode)
        g_opportunity[g_oppCols.AdjustedCloseDate] = el.adjustedCloseDate.value;

    if (g_req1Used) {
        g_opportunity[g_oppCols.Requirement1] = getMilestone('Requirement1');
    }
    else {
        g_opportunity[g_oppCols.Requirement1] = 0;
    }

    g_opportunity[g_oppCols.Person] = getMilestone('Person');
    g_opportunity[g_oppCols.Need] = getMilestone('Need');
    g_opportunity[g_oppCols.Money] = getMilestone('Money');
    g_opportunity[g_oppCols.Time] = getMilestone('Time');

    if (g_req2Used) {
        g_opportunity[g_oppCols.Requirement2] = getMilestone('Requirement2');
    }
    else {
        g_opportunity[g_oppCols.Requirement2] = 0;
    }

    if (g_opportunity[g_oppCols.Category] != '9' || did_revive == true)
    {
        g_opportunity[g_oppCols.Category] = value_of_radio();
    }

    for (var k = 0; k < g_people.length; ++k)
    {
        if (g_people[k][g_people.PersonID] == g_opportunity[g_oppCols.PersonID])
        {
            g_opportunity[g_oppCols.FirstName] = g_people[k][g_people.FirstName];
            g_opportunity[g_oppCols.LastName] = g_people[k][g_people.LastName];
            g_opportunity[g_oppCols.Color] = g_people[k][g_people.Color];
            break;
        }
    }

    for (var k in l_editedSubAnswers)
    {
        setGlobalSubAnswer(g_dealID, k, l_editedSubAnswers[k], g_opportunity[g_oppCols.Category]);
    }


    if(g_target_used)
    {
        g_opportunity[g_oppCols.TargetDate] = el.targetDate.value;
    }

    if (g_bill_used)
    {
        g_opportunity[g_oppCols.BillDate] = el.BillDate.value;
    }


    //The opportunities table trigger doesn't update LastMoved if the opp is new
    //In order for stalled opps to display properly on the salesman's board,
    //this patch is necessary
    patchLastMoved();

    // tell the base window, which will tell the others (spreadsheet, alerts)
    if (g_standalone) g_opportunity.isDirty = true;
    else update_from_edit_window(g_opportunity, g_oldCategory, g_oldPersonID);

    if (all_done)

    {
        do_save();
        if (g_standalone)
        {
            g_saver._realClose = g_saver.close;
            g_saver.close = function()
            {
                updateSFOppFromMPower();
                g_saver._realClose();
                redirectSalesforce();
            };
        }
    }
}

function format_date(val)
{
    if (val == '&nbsp;') return val;
    var ret = Dates.mssql2us(val);
    var pos = ret.indexOf(' ');
    if (pos != -1) ret = ret.substr(0, pos);
    ret = Dates.formatShortDate(Dates.makeDateObj(ret));
    return ret;
}

function write_header_title(img, text)
{
    document.writeln('<table border=0><tr><td><img src="../images/' + img + '" align="left">');
    document.writeln('</td><td class="title" valign="middle" >' + text + '</td></tr></table>');
}

function write_header() {
    //If board user is a manager, then admin functionality
    if (g_admin_mode) {
        g_admin_mode = true;
        g_adminCall = true;
    }
    else {
        var level = g_user[0][g_user.Level];
        if(level >= 2 && !g_standalone)
        g_admin_mode = true;
    }

    var header = '';
    if (g_editMode) {
        header = 'Edit Opportunity';
    }
    else {
        header = 'New Opportunity';
    }
    if (g_personID != '-1') {		
        var info = get_salesperson_info(g_personID);
		//alert('g_personID : ' + g_personID + info.name);
        header += ' / ' + info.name;
    }

	

    Header.setText(header);
    wr(Header.makeHTML ());
}

function something_changed()
{
    g_somethingWasChanged = true;
	if(window.opener && g_isSpreadsheet) {
		try {
			window.opener.gbload();
		}catch(e) {}
	}
}


function change_contactID() {
	if ($('#contact').val() != $('#CurrentContact').val()) {
		$('#ContactID').val('');
	}
}


function daysSinceFM()
{
    var daysSince = '';
    var fm = document.getElementById('firstMeeting').value;
/*
    var fmt = '1/1/1970 ' + document.getElementById('firstMeetingTime').value;
    var msFM = Date.parse(fm) + Date.parse(fmt);
    var now = new Date();
    var msSince = now.getTime() - msFM;
    if(msSince > 0)
    {
        daysSince = Math.floor(msSince / 86400000);
    }
*/

    var els = document.forms['form1'].elements;
    var cat = value_of_radio(els.category);
    if (cat == 6) {
        var adc = document.getElementById('actualCloseDate').value;
        daysSince = getDaysDiff(adc, fm);
        //alert(adc);
        //alert(fm);
        //alert(daysSince);
       }
    else {
        daysSince = getDaysOld(fm);
    }
    var trDaysSinceFM = document.getElementById('trDaysSinceFM');
    trDaysSinceFM.style.visibility = (daysSince <= 0 || isNaN(daysSince)) ? 'hidden' : 'visible';

    document.getElementById('sinceFirstMeetingTime').value = daysSince;
}

function checkDateAssigned() {
    //if (g_target_used && g_editMode==false)
    if (g_target_used)
    {
        var targetDate = document.getElementById('targetDate');
        if (targetDate)
        {
            var fmval = document.getElementById('firstMeeting').value;
            if (fmval == '')
                return;
            var fm = (Dates.makeDateObj(Dates.normalize(fmval))).getTime();
            //alert(document.getElementById('targetDate'));
            var target = document.getElementById('targetDate').value;
            var targetd = (Dates.makeDateObj(Dates.normalize(target))).getTime();
            var now = new Date();
            var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            // if there's no target date, then use the least of fm and now
            //if (target!='' && fm < targetd)
            //  document.getElementById('targetDate').value = document.getElementById('firstMeeting').value;
        }
    }
}



function make_new_opportunity()
{
    var ret = new Array();
    for (var k in g_opps)
    {
        var n = parseInt(k, 10);
        if (!isNaN(n))
            continue;
        if (k.charAt(0) == '_' || k == 'toPipes' || k == 'quicksort' || k == 'radixsort')
            continue;
        ret[ret.length] = '';
    }
    ret[g_opps.DealID] = getTempID();
    return ret;
}

function init_new_window() {
    g_opportunity = make_new_opportunity();
    g_dealID = g_opportunity[g_oppCols.DealID];
    
    //It was unable to select the first meeting when adding a new opps if the terget is disabled.
    //if(g_target_used)
    //{
    //    category_changed(1);
    //}

    category_changed(1);
}

function patchLastMoved() {
    // Do nothing if this isn't a new opportunity
    if(g_opportunity[g_oppCols.DealID] > -1)
        return;
    // Don't bother with new opportunities other than stalled
    if(g_opportunity[g_oppCols.Category] != '3' && g_opportunity[g_oppCols.Category] != '5')
        return;

    // If there's a next meeting, use that.  Else use first meeting date.
    var lastMoved;
    if(g_opportunity[g_oppCols.NextMeeting] != null)
        lastMoved = Dates.mssql2us(g_opportunity[g_oppCols.NextMeeting]);
    else
        lastMoved = Dates.mssql2us(g_opportunity[g_oppCols.FirstMeeting]);
    g_opportunity[g_oppCols.LastMoved] = lastMoved;
}

function init_column_ids()
{
    var colNames = get_opportunity_columns();
    for (var i = 0; i < colNames.length; ++i)
    {
        var name = colNames[i];
        g_oppCols[name] = g_opps[name];
    }
}

function setMainOffering()
{
    g_opportunity[g_oppCols.ProductID] = getMainOffering(g_dealID);
}

function clearThis(elem, e)
{
    if (e.keyCode == 46 || e.keyCode == 8) {
        elem.value = '';
    }
}

function init_edit_window()
{	
	g_opportunity = find_opportunity(g_dealID);
    g_prev_cat = g_old_cat = g_opportunity[g_oppCols.Category];
    if (g_opportunity == null)
        return; // shouldn't happen

    var cat = g_opportunity[g_oppCols.Category];
    g_oldCategory = cat;
    g_oldPersonID = g_opportunity[g_oppCols.PersonID];
    var isTarget = (cat == 10);
    var els = document.forms['form1'].elements;
    
    if (g_Transactional) els.transactional.checked=g_opportunity[g_oppCols.Transactional]=='1' ? true : false;
    
    setRadioButtons(cat);

    var firstMeeting = format_date(g_opportunity[g_oppCols.FirstMeeting]);
    var pos = firstMeeting.indexOf(' ');
    if (pos != -1)
        firstMeeting = firstMeeting.substr(0, pos);

    var nextMeeting = format_date(g_opportunity[g_oppCols.NextMeeting]);
    var pos = nextMeeting.indexOf(' ');
    if (pos != -1)
        nextMeeting = nextMeeting.substr(0, pos);

    var nextMeetingTime = g_opportunity[g_oppCols.NextMeetingTime];

    els.nextMeeting.value = nextMeeting;
    els.nextMeetingTime.value = nextMeetingTime;
    g_nextMeeting=nextMeeting;  // save next meeting for later comparison

    var firstMeetingTime = g_opportunity[g_oppCols.FirstMeetingTime];
    els.firstMeetingTime.value = firstMeetingTime;

    els.firstMeeting.value = firstMeeting;
    daysSinceFM();
    if (g_target_used)
    {
        var targetDate = format_date(g_opportunity[g_oppCols.TargetDate]);
        var pos = targetDate.indexOf(' ');
        if (pos != -1)
            targetDate = targetDate.substr(0, pos);
        els.targetDate.value = targetDate;
    }

    if (g_bill_used)
    {
        var BillDate = format_date(g_opportunity[g_oppCols.BillDate]);
        var pos = BillDate.indexOf(' ');
        if (pos != -1)
            BillDate = BillDate.substr(0, pos);
        els.BillDate.value = BillDate;
    }

    if (cat != 1 && !g_admin_mode && !isTarget)
    {
        els.firstMeeting.disabled = true;
        els.firstMeetingCal.disabled = true;
        els.firstMeetingTime.disabled = true;
        els.firstMeetingTimePopup.disabled = true;
    }
    if (cat == 6)
    {
        var actualCloseDate = format_date(g_opportunity[g_oppCols.ActualCloseDate]);
        var pos = actualCloseDate.indexOf(' ');
        if (pos != -1)
            actualCloseDate = actualCloseDate.substr(0, pos);
        els.actualCloseDate.value = format_date(actualCloseDate);
        daysSinceFM();
        els.actualDollarAmount.value = format_money(g_opportunity[g_oppCols.ActualDollarAmount]);

		els.actualDollarAmount.disabled = false;
		els.actualCloseDate.disabled = false;

        if (els.actualDollarAmount.value == '$0')
            els.actualDollarAmount.value = '';
        if (!(!els.adjustedCloseDate))
            els.adjustedCloseDate.disabled = true;
        var labelAdjEst = document.getElementById('labelAdjEst');
        if (labelAdjEst)
            labelAdjEst.className = 'taglabel_disabled_eo';
    }
    else
    {
        if (!(!els.adjustedCloseDate))
            els.adjustedCloseDate.disabled = false;
        var labelAdjEst = document.getElementById('labelAdjEst');
        if (labelAdjEst)
            labelAdjEst.className = 'plabel_eo';
    }

    //registerAutoCommas(els.actualDollarAmount);
    els.company.value = g_opportunity[g_oppCols.Company].unescape_html();
    els.division.value = g_opportunity[g_oppCols.Division].unescape_html();
    if (g_showcontact) els.contact.value = g_opportunity[g_oppCols.Contact].unescape_html();

    els.estimatedDollarAmount.value = format_money(g_opportunity[g_oppCols.EstimatedDollarAmount]);

    if (g_admin_mode)
    {
        adjustedClose = format_date(g_opportunity[g_oppCols.AdjustedCloseDate]);
        pos = adjustedClose.indexOf(' ');
        if (pos != -1)
            adjustedClose = adjustedClose.substr(0, pos);
        if (!(!els.adjustedCloseDate))
            els.adjustedCloseDate.value = adjustedClose;
    }

    if (els.estimatedDollarAmount.value == '$0')
        els.estimatedDollarAmount.value = '';
        els.estimatedDollarAmount.disabled = true;

    createDate = g_opportunity[g_oppCols.CreateDate];
    g_opp_age = getDaysOld(Dates.mssql2us(createDate));

    if (!g_admin_mode && g_opp_age > 3) {
        els.company.readOnly = true;
    }

    check_styles();

    // if the opportunity is not in FM, do not allow it to be placed there
    var fmRadio = document.getElementById('radioFM');
    var tRadio = document.getElementById('radioT');

    g_valuationLevel = g_opportunity[g_oppCols.VLevel];
    g_sourceID = g_opportunity[g_oppCols.SourceID];
    g_source2ID = g_opportunity[g_oppCols.Source2ID];
    g_personID = g_opportunity[g_oppCols.PersonID];

}

function show_opp_notes(opp_id)
{
	Windowing.dropBox.editOppWin = window;
	
	if(opp_id > 0) {
    Windowing.openSized2ndPromptScroll('../shared/opp_notes.php?oppid='+opp_id , 630, 820);
  }
  else {
  	Windowing.openSized2ndPrompt('../shared/add_note.php?oppid='+opp_id , 630, 820);  	
  }
}

function setNoteVal(noteSub, noteText, notePrivate) {	
	var indx = oppNote['NoteSub'].length;
	if(indx != 0) {
		indx =indx-1;		
	}
	oppNote['NoteSub'][indx] = noteSub;
	oppNote['NoteText'][indx] = noteText;	
	oppNote['NotePrivate'][indx] = notePrivate;
}
function getNoteVal() {	
	var indx = oppNote['NoteSub'].length;
	
	if(indx > 0) {
		var returnArr = new Array();
		
		if(indx != 0) {
			indx =indx-1;		
		}
		returnArr['NoteSub'] = oppNote['NoteSub'][indx];
		returnArr['NoteText'] = oppNote['NoteText'][indx];	
		returnArr['NotePrivate'] = oppNote['NotePrivate'][indx];
		return returnArr;
	}
	return false;
}
