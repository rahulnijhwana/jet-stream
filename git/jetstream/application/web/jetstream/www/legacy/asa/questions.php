<?php

include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Questions', true);

if (isset($new_question_text) && isset($whichsection))
{
	$new_question_text = str_replace("\\\"", "\"", str_replace("\\'", "''", $new_question_text));
	mssql_query("insert into questions (QuestionText, IsHeader, WithHeader) values ('$new_question_text', 0, $whichsection)");
	print('<span style="color:red;font-style:italic;">Question Added</span><br><br>');
}

if (isset($deleteid))
{
	mssql_query("delete from questions where QuestionID = $deleteid");
	mssql_query("delete from analysisquestionxref where QuestionID = $deleteid");
	print('<span style="color:red;font-style:italic;">Question Deleted</span><br><br>');
}

if (isset($new_section_text))
{
	$header_check_result = mssql_query("select * from questions where IsHeader <> 0 order by IsHeader DESC");
	$header_check_row = mssql_fetch_assoc($header_check_result);
	$header_id = $header_check_row['IsHeader'] + 1;
	
	print("header_id = $header_id<br>");
	
	$new_section_text = str_replace("\\\"", "\"", str_replace("\\'", "''", $new_section_text));
	mssql_query("insert into questions (QuestionText, IsHeader, WithHeader) values ('$new_section_text', $header_id, $header_id)");
	print('<span style="color:red;font-style:italic;">Section Added</span><br><br>');
}

if (isset($deletesectionid))
{
	mssql_query("delete from analysisquestionxref where QuestionID in (select QuestionID from questions where WithHeader = $deletesectionid)");
	mssql_query("delete from questions where WithHeader = $deletesectionid");
	print('<span style="color:red;font-style:italic;">Section Deleted</span><br><br>');
}

?>

<script language="JavaScript">

function confirmDelete(id)
{
	if (confirm("Are you sure you want to completely delete this question as well as it's associations with curves?"))
		document.location.href = 'questions.php?deleteid=' + id;
}

function confirmDeleteSection(id)
{
	if (confirm("Are you sure you want to completely delete this section, it's questions, as well as it's associations with curves?"))
		document.location.href = 'questions.php?deletesectionid=' + id;
}

</script>

<form action="questions.php" method="POST">
<b>Add New Question</b><br>
Section: <select name="whichsection">
<?php

$section_result = mssql_query("select * from questions where IsHeader <> 0 order by IsHeader");
while ($section_row = mssql_fetch_assoc($section_result))
{
	print('<option value="' . $section_row['IsHeader'] . '">' . $section_row['QuestionText'] . '</option>');
}

?>
</select><br>
<textarea type="text" name="new_question_text" rows="5" cols="60"></textarea><br>
<input type="submit" value="Add Question">
</form>
<br>

<form action="questions.php" method="POST">
<b>Add New Section</b><br>
<textarea type="text" name="new_section_text" rows="5" cols="60"></textarea><br>
<input type="submit" value="Add Section">
</form>
<br>

<b>Available Questions</b>
<table>

<?php

$result = mssql_query("select * from questions order by WithHeader, IsHeader DESC, QuestionID");

while ($row = mssql_fetch_assoc($result))
{
	if ($row['IsHeader'] != 0)
		print('<tr><td>&nbsp;</td></tr><tr><td valign="top"><a href="javascript:confirmDeleteSection(' . $row['WithHeader'] . ');">Delete Section</a></td><td style="font-weight:bold;">' . $row['QuestionText'] . '</td></tr>');
	else
		print('<tr><td valign="top"><a href="javascript:confirmDelete(' . $row['QuestionID'] . ');">Delete</a></td><td>' . $row['QuestionText'] . '</td></tr>');
}

?>

</table>

<?php
print_footer();
?>
