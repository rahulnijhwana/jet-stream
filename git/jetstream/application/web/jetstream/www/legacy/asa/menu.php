<?php
include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Menu', false);

foreach ($menu as $file => $desc)
{
	if (!strcmp($file, 'menu.php')) continue;

	$pos = strpos($desc, '|');
	if ($pos !== false && $pos != strlen($desc) - 1)
	{
		$short = substr($desc, 0, $pos);
		$ext = substr($desc, ++$pos);
	}
	else 
	{
		$short = $desc;
		$ext = '';
	}

	print("<a href=\"$file\">$short</a>");
	if (strlen($ext)) print(" - $ext");
	print("<br>\n");
}

print_footer();
?>
