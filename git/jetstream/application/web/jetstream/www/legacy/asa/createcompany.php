<?php

include_once('_asa_utils.php');
include_once('mpsettings.php');

check_asa_login($PHP_SELF);
print_header('Create Company', true);

//print(`mkdir ../../thingy`);

if (isset($FullCompanyName) && isset($DirectoryName) && isset($ResellerSel))
{
	$isError = FALSE;
	$errStr = '';
	if($FullCompanyName == "") {
		exit("Error: Full Company Name is blank.");
	}

	if($DirectoryName == "") {
		exit("Error: Directory Name is blank.");
	}

	$query_str = "SELECT CompanyID from Company where DirectoryName = '".$DirectoryName."';";
	$result = mssql_query($query_str);
	if($result){
		$temp_assoc = mssql_fetch_assoc($result);
		if($temp_assoc['CompanyID'] != NULL){
			//exit("Error: Directory Name exists.");
			$errStr = 'Error: Directory Name exists.';
			$isError = TRUE;
		}
	}
	//exit("Error:".(!$isError));
	if(!$isError){

		$query_str = "SELECT ResellerLogo, BrandID from Resellers where ResellerID = $ResellerSel;";
		$result = mssql_query($query_str);
		if($result) {
			$temp_assoc = mssql_fetch_assoc($result);
			$ResellerLogo = $temp_assoc['ResellerLogo'];
			$BrandSel = $temp_assoc['BrandID'];
		} else {
			$ResellerLogo = "asa.jpg";
			$BrandSel = 1;
		}
	
		$query_str = "SELECT BrandName, DomainName, TrademarkText from Branding where BrandID = $BrandSel;";
		$result = mssql_query($query_str);
		if($result) {
			$temp_assoc = mssql_fetch_assoc($result);
			$BrandName = $temp_assoc['BrandName'];
			$DomainName = $temp_assoc['DomainName'];
			$TrademarkText = $temp_assoc['TrademarkText'];
		} else {
			// failed, assume ASA/M-Power
			$BrandName = "M-Power";
			$DomainName = "mpasa.com";
		}
	
		if(isset($Demo)) $Demo = 1;
		else $Demo=0;
		
		if(isset($NoteAlert)) $NoteAlert = 1;
		else $NoteAlert=0;
		
		if(!isset($class)) $class=0;
		
		$mpower = 0;
		$slipstream = 0;
		
		if(isset($system) && ($system == 1 || $system == 3)) $mpower = 1;
		if(isset($system) && ($system == 2 || $system == 3)) $slipstream = 1;	
		
	
		// 2-21-05: need ResellerID in company table so editlogo.php can know
		// which reseller logo to use.
		$query_str = "INSERT INTO company (Name, Address1, Address2, MonthsConsideredNew, ";
		$query_str .= "LevelCount, Requirement1, Requirement2, FMThreshold, IPThreshold, DPThreshold, CloseThreshold, ";
		$query_str .= "ClosePeriod, Requirement1used, Requirement2used, Requirement1trend, Requirement2trend, MissedMeetingGracePeriod, NonUseGracePeriod, IPCloseRatioDefault, DPCloseRatioDefault, IPStalledLimit, DPStalledLimit, SubMSEnabled, DirectoryName, Disabled, Snapshot, BrandID, Demo, ResellerID, Class, HasCustomAmount, CustomAmountName, Lang, ValLabel, ValAbbr, ReviveLabel, RevivedLabel, RenewLabel, RenewedLabel, DefaultBranch, LoginPageSuffix, Mpower, Slipstream, NoteAlert) VALUES (";
		$query_str .= "'$FullCompanyName', '', '', 6, 2, 'Req1', 'Req2', 8, 6, 4, 2, 'm', 0, 0, '', '', 3, 3, 25, 50, 14, 7, 1, '$DirectoryName', 0, 1, $BrandSel, $Demo, $ResellerSel, $class, 1, 'Dollars','en','Valuation','Val','Retarget','Retargeted','Renew','Renwed','v2','$loginpage', $mpower, $slipstream, $NoteAlert)"; 
	
		//echo $query_str;
	
	
		$result = mssql_query($query_str);
		if ($result == false)
			exit('Error inserting company data');
	
		$result = mssql_query("SELECT @@IDENTITY AS 'CompanyID'");
		if ($result == false)
			exit('Error getting CompanyID');
	
		$temp_assoc = mssql_fetch_assoc($result);
		$company_id = $temp_assoc['CompanyID'];
	
		$query_str = "INSERT INTO AccountMap (CompanyID,FieldName,LabelName,BasicSearch,IsCompanyName,IsRequired,ValidationType) VALUES ($company_id,'Text01','CompanyName',1,1,1,3)";
		$result = mssql_query($query_str);
		if ($result == false)
			exit('Error inserting account map data');
		
		$query_str = "INSERT INTO ContactMap (CompanyID,FieldName,LabelName,BasicSearch,IsFirstName,IsRequired,ValidationType) VALUES ($company_id,'Text01','First Name',1,1,1,3)";
		$result = mssql_query($query_str);
		if ($result == false)
			exit('Error inserting contact map data');
		
		$query_str = "INSERT INTO ContactMap (CompanyID,FieldName,LabelName,BasicSearch,IsLastName,IsRequired,ValidationType) VALUES ($company_id,'Text02','Last Name',1,1,1,3)";
		$result = mssql_query($query_str);
		if ($result == false)
			exit('Error inserting contact map data');
			
		for ($k = 0; $k < 6; ++$k)
		{
			$query_str = "INSERT INTO levels (CompanyID, Name, LevelNumber, ViewSiblings, Enabled, Import) VALUES ($company_id, ";
			$level_str = "'Level #" . ($k + 1) . "', ";
			if ($k == 0)
				$level_str = "'Salespeople', ";
			else if ($k == 1)
				$level_str = "'Sales Managers', ";
			$query_str .= $level_str;
			$query_str .= ($k + 1) . ", ";
			$query_str .= "1, ";
			if ($k < 2)
				$query_str .= "1, ";
			else
				$query_str .= "0, ";
			$query_str .= "0)";
			$result = mssql_query($query_str);
			if ($result == false)
				exit('Error inserting level data');
		}
	
	//	$default_reason_array = array('Prospect no longer in business', 'Stalled too long', 'Training');
		$default_reason_array = array('Training');
	
		$reason_count = count($default_reason_array);
		for ($k = 0; $k < $reason_count; ++$k)
		{
			$result = mssql_query("INSERT INTO removereasons (Text, CompanyID, Deleted) VALUES ('${default_reason_array[$k]}', $company_id, 0)");
				if ($result == false)
			exit('Error inserting removal reason data');
		}
	
	
		$default_sm_result = mssql_query("insert into SubMilestones (Prompt, Location, CompanyID, Seq, Enable, Name, AlphaNum, Length, Checkbox, YesNo, Mandatory, FillWholeLength,DropDown,DropChoice1,DropChoice2,DropChoice3,DropChoice4,DropChoice5,DropChoice6,DropChoice7,DropChoice8,DropChoice9,DropChoice10,RightAnswer) select Prompt, Location, $company_id, Seq, Enable, Name, AlphaNum, Length, Checkbox, YesNo, Mandatory, FillWholeLength,DropDown,DropChoice1,DropChoice2,DropChoice3,DropChoice4,DropChoice5,DropChoice6,DropChoice7,DropChoice8,DropChoice9,DropChoice10,RightAnswer from SubMilestoneDefaults");
	
		$has_img = (isset($_FILES['LogoImage']) && $_FILES['LogoImage']['size'] > 0);
		/*
		$templateStr = '';
		if ($has_img)
			$templateStr = file_get_contents("index_template.html");
		else
			$templateStr = file_get_contents("index_template_no_logo.html");
	
		$templateStr = str_replace("{COMPANYNAME}", $FullCompanyName, $templateStr);
		$templateStr = str_replace("{COMPANYID}", $company_id, $templateStr);
		$templateStr = str_replace("{BRANDNAME}", $BrandName, $templateStr);
		$templateStr = str_replace("{DOMAINNAME}", $DomainName, $templateStr);
	
		$templateStr = str_replace("{RESELLERLOGO}", $ResellerLogo, $templateStr);
		$templateStr = str_replace("{TRADEMARKTEXT}", $TrademarkText, $templateStr);
		*/
	
		mkdir(MP_PATH . "/$DirectoryName");
		if ($has_img)
		{
			$temp_image_name = $_FILES['LogoImage']['name'];
			$temp_image_name = strtolower($temp_image_name);
			if (strpos($temp_image_name, '.jpg') !== FALSE || strpos($temp_image_name, '.jpeg') !== FALSE)
				$temp_image_name = 'logo.jpg';
			if (strpos($temp_image_name, '.gif') !== FALSE)
				$temp_image_name = 'logo.gif';
			if (strpos($temp_image_name, '.bmp') !== FALSE)
				$temp_image_name = 'logo.bmp';
			if (strpos($temp_image_name, '.png') !== FALSE)
				$temp_image_name = 'logo.png';
			move_uploaded_file($_FILES['LogoImage']['tmp_name'], MP_PATH . "/$DirectoryName/" . $temp_image_name);
			//$templateStr = str_replace("{LOGO}", $temp_image_name, $templateStr);
	//Update DB
			$sql="UPDATE company SET LogoName='$temp_image_name' where CompanyID=$company_id";
			$result = mssql_query($sql);
			if ($result == false)
				exit('Error updating logo name');
	
		}
		/*
		$templateStr = file_get_contents("index_template_new.php");
		$fp = fopen("../../$DirectoryName/index.php", "w");
		if ($fp == FALSE)
			exit("Could not create index file");
		fwrite($fp, $templateStr);
		fclose($fp);
		*/
		shell_exec("cp -f index_template_new.php " . MP_PATH . "/$DirectoryName/index.php");
		shell_exec("chmod 775 " . MP_PATH . "/$DirectoryName");
		shell_exec("chmod 775 " . MP_PATH . "/$DirectoryName/*");
	
		exit("Success! New CompanyID is $company_id" . '<br><a href="createcompany.php">Add Another Company</a>');
	}
}

?>

<script language="JavaScript">

function GoForIt()
{
	var theform = document.forms.form1;
	// BUG FIX (rjp): must compare using theform.Foo.value, not theform.Foo
	if (theform.FullCompanyName.value == '')
	{
		alert('"Full Company Name" is blank');
		return;
	}
	if (theform.DirectoryName.value == '')
	{
		alert('"Directory Name" is blank');
		return;
	}
<?php
	// Build input validation for the reseller selection box
	$query_str = "SELECT ResellerID FROM Resellers WHERE UserLogoRequired = 1;";
	$result = mssql_query($query_str);
	if($result) {
		print "\tif (theform.LogoImage.value == '' && (";
		$temp_assoc = mssql_fetch_assoc($result);
		while($temp_assoc) {
			print "theform.ResellerSel.value == " . $temp_assoc['ResellerID'];
			$temp_assoc = mssql_fetch_assoc($result);
			if($temp_assoc) {
				print " || ";
			}
		}
		print "))\n";
		print "\t{\n";
		print "\t\talert('Logo Image required when using this reseller');\n";
		print "\t\treturn;\n";
		print "\t}\n";
	}
?>
	theform.submit();
}

</script>

<form enctype="multipart/form-data" name="form1" action="createcompany.php" method="post">

<table>
	<tr>
		<td>Full Company Name</td><td><input type="text" name="FullCompanyName" size="50"></td>
	</tr>
	<tr>
		<td>
			Directory Name<br>
			<i>No spaces</i>
		</td><td><input type="text" name="DirectoryName" size="50"></td>

	</tr>
	<tr><td colspan="2" style="text-align:center;color:#FF0033"><span><? echo $errStr;?></span></td></tr>
	<tr>
		<td>
			Demo
		</td><td><input type="checkbox" name="Demo"></td>
	</tr>
	
	<tr>
		<td>
			Note Alert
		</td><td><input type="checkbox" name="NoteAlert"></td>
	</tr>
	
	<tr>
		<td>Logo Image <i>(optional)</i></td><td><input type="hidden" name="MAX_FILE_SIZE" value="1000000" /><input type="file" name="LogoImage"/></td>
	</tr>
	<tr>
		<td>Domain and Reseller:</td>
		<td>
<?php
		$query_str = "SELECT BrandID, VendorName FROM Branding ORDER BY BrandID;";
		$result = mssql_query($query_str);
		if($result) {
			// use a single <SELECT> with <OPTGROUP>s instead of trying to use
			// radio buttons and multiple <SELECT>s.
			print "<SELECT NAME=\"ResellerSel\">";
			$temp_assoc = mssql_fetch_assoc($result);
			while($temp_assoc)
			{
				print "<OPTGROUP LABEL=\"" . $temp_assoc['VendorName'] . "\">";
				$rsquery_str = "SELECT ResellerID, ResellerName, UserLogoRequired from Resellers WHERE BrandID = " . $temp_assoc['BrandID'] . " ORDER BY ResellerName;";
				$rsresult = mssql_query($rsquery_str);
				if($rsresult)
				{
					$rsassoc = mssql_fetch_assoc($rsresult);
					while($rsassoc)
					{
						// We can derive the BrandID from the Resellers table.
						print "<OPTION VALUE=\"" . $rsassoc['ResellerID'] . "\"";
						if($rsassoc['UserLogoRequired'])
						{
							print " TITLE=\"Logo Image Required\"";
						}
						if($rsassoc['ResellerName'] == 'ASA')
						{
							print " selected";
						}
						print ">" . $rsassoc['ResellerName'] . "</OPTION>";
						$rsassoc = mssql_fetch_assoc($rsresult);
					}
				}
				print "</OPTGROUP>";
				$temp_assoc = mssql_fetch_assoc($result);
			}
			print "</SELECT>";
		}

?>
		</td>
	</tr>
	
	<tr>
		<td>Assign</td>
		<td>
			<input type="radio" name="system" value="1" checked>M-Power</input>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="radio" name="system" value="2">Slipstream</input>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="radio" name="system" value="3">Both</input>
		</td>
	</tr>
		
	<tr>
		<td>Login Page</td>
		<td>
			<input type="radio" name="loginpage" value="" checked>M-Power</input>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="radio" name="loginpage" value="am">Accountability Manager</input>
		</td>
	</tr>
<?php
function getClassRadioHTML($class, $classval)
{
	switch($class)
	{
		case 0:
			$label = "Customer";break;
		case 1:
			$label = "Trainer/Affiliate";break;
		case 2:
			$label = "Nontrainer Reseller";break;
		case 3:
			$label = "Other";break;
	}
	$strchk = ($class == $classval ? 'checked' : '');
	return "<tr><td><input type=\"radio\" name=\"class\" value=\"$class\" $strchk>$label</td></tr>";
}
	for($i = 0; $i< 4; $i++)
		print(getClassRadioHTML($i, 0));
?>
	<tr>
		<td align="center" colspan="2"><button type="button" onclick="GoForIt();">Submit</button></td>
	</tr>
</table>

</form>

<?php
print_footer();
?>
