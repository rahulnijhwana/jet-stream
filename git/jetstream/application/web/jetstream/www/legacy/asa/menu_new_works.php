<?php
include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">
<head>
<title>System Administration Utilities</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language="JavaScript" src="../javascript/smartselect.js"></script>

</head>
<body link="blue" vlink="blue" onload="onLoad();">
<script language="JavaScript">

var arrUtils=Array();
arrUtils[0] = Array('Create company', 'createcompany.php');
arrUtils[1] = Array('Edit company name', 'editcompanyname_dir.php');
arrUtils[2] = Array('Disable company', 'disable.php');
arrUtils[3] = Array('Edit logo', 'editlogo.php');
arrUtils[4] = Array('Curves', 'curves.php');
arrUtils[5] = Array('Analysis', 'analysis.php');
arrUtils[6] = Array('Questions', 'questions.php');
arrUtils[7] = Array('Actions', 'actions.php');
arrUtils[8] = Array('Purge removed', 'purge.php');
arrUtils[9] = Array('EULA', 'eula.php');
arrUtils[10] = Array('Delete company', 'deletecompany.php');
arrUtils[11] = Array('Labels: milestone', 'milestone_labels.php');
arrUtils[12] = Array('Superuser', 'editsuperuser.php');
arrUtils[13] = Array('Head count', 'personcount.php');
arrUtils[14] = Array('Last login', 'personlastuse.php');
arrUtils[15] = Array('Create reseller', 'createreseller.php');
arrUtils[16] = Array('Edit reseller', 'editreseller.php');
arrUtils[17] = Array('Delete reseller', 'deletereseller.php');
arrUtils[18] = Array('Labels: column', 'category_labels.php');

var SS = null;

function onLoad()
{
	SS = new SmartSelect(document.getElementById('runUtil'), arrUtils, 1, 0, 'editlogo.php');
}

function Go()
{

	var form = document.forms['form1'];
	var fname = SS.getSelectedVal();
	if (fname == '-1')
		alert('Please make a valid selection');
	else
	{
		form.action = fname;
		form.submit();
	}
}

function handleKeyUp()
{
	SS.autoFill(window.event);
}


</script>
<form enctype="multipart/form-data" name="form1" method="post">

<table>
<tr><td>Select utility:&nbsp</td><td ><input type="text" id="runUtil" onKeyUp="handleKeyUp();"></td></tr>
<tr><td colspan="2" align="center"><input type="button" onclick="Go();" value="Go"></td></tr>
</table>



</form>
</body></html>

