<?

include_once('_asa_utils.php');
check_asa_login($PHP_SELF);

if (isset($_REQUEST['updatedata']))
{
	$uda = explode(',', $_REQUEST['updatedata']);
	for ($k = 0; $k < count($uda); $k += 2)
	{
		$temp_companyid = $uda[$k];
		$temp_branch = $uda[$k+1];
		$sqlstr = "update company set DefaultBranch='$temp_branch' where CompanyID=$temp_companyid";
		mssql_query($sqlstr);
	}
}

print_header('Edit Company Default Branch', true);

$sqlstr = "select * from company order by Name";
$result = mssql_query($sqlstr);

$k = 0;

$validBranches = array('current', 'develop', 'interim', 'partner', 'v2');
?>

<script language="Javascript">

var editedCompanies = new Object();

function onChangeBranch(cid, newbranchelem)
{
	newbranchelem.style.backgroundColor = 'red';
	newbranchelem.style.color = 'white';
	newbranchelem.blur();
	editedCompanies[cid] = newbranchelem.value;
	document.getElementById('buttonSave').disabled = false;
}

function dosave()
{
	var daform = document.forms.form1;
	var str = ''
	for (var k in editedCompanies)
	{
		if (str != '')
			str += ',';
		str += k+','+editedCompanies[k];
	}
	daform.updatedata.value = str;
	daform.submit();
}

</script>

<form name="form1" action="defaultbranch.php" method="post">
	<input type="hidden" name="updatedata" value=""></input>
</form>

<input id="buttonSave" type="button" value="Save Changes" onclick="dosave();" disabled="true"></input>

<div style="width:500px; height:400px; overflow:auto; border:1px solid black;">
<table width="100%" cellspacing="0">
	<?while ($row = mssql_fetch_assoc($result)):?>
		<?if (($k%2)==0):?>
			<tr>
		<?else:?>
			<tr style="background-color:#EEEEEE;">
		<?endif;?>
			<td><?=$row['Name']?></td>
			<td>
				<select onchange="onChangeBranch(<?=$row['CompanyID']?>, this)">
					<?for ($n = 0; $n < count($validBranches); ++$n):?>
						<option value="<?=$validBranches[$n]?>" <?=($row['DefaultBranch']==$validBranches[$n])?'selected':''?> ><?=$validBranches[$n]?></option>
					<?endfor;?>
				</select>
			</td>
		</tr>
		<?$k++?>
	<?endwhile;?>
</table>
</div>

<? print_footer(); ?>