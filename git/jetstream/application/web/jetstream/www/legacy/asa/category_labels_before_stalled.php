<?php

include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Category Labels', true);

if (isset($new_label_text) && isset($new_abbr) && isset($whichcategory))
{
	$new_label_text = str_replace("\\\"", "\"", str_replace("\\'", "''", $new_label_text));
	$new_abbr = str_replace("\\\"", "\"", str_replace("\\'", "''", $new_abbr));

	mssql_query("insert into CategoryLabels (Label, Abbr, CategoryID) values ('$new_label_text', '$new_abbr', $whichcategory)");
	print('<span style="color:red;font-style:italic;">Category Label Added</span><br><br>');
}

if (isset($deleteid))
{
	mssql_query("delete from CategoryLabels where ID = $deleteid");
	print('<span style="color:red;font-style:italic;">Category Label Deleted</span><br><br>');
}

?>

<script language="JavaScript">

function confirmDelete(id)
{
	if (confirm("Are you sure you want to delete this category label?"))
		document.location.href = 'category_labels.php?deleteid=' + id;
}

function onclickGo()
{
 	var theform = document.form1;
 	if (theform.new_label_text.value == '')
 	{
 		alert('Please enter a value for the category label text!');
 		return;
 	}
 	if (theform.new_abbr.value == '')
 	{

 		alert('Please enter a value for the category label abbreviation!');
 		return;
 	}
 	theform.submit();
}


</script>

<form name="form1" action="category_labels.php" method="POST">
<b>Add New Category Label</b><br>
<table>
<tr><td>Milestone:</td>
<td> <select name="whichcategory">
<?php
	print('<option value="1">First Meeting / FM</option>');
	print('<option value="2">Information Phase / IP</option>');
	print('<option value="3">Stalled Information Phase / SIP</option>');
	print('<option value="4">Decision Point / DP</option>');
	print('<option value="5">Stalled Decision Point / SDP</option>');
	print('<option value="6">Closed / C</option>');
	print('<option value="9">Removed / R</option>');
	print('<option value="10">Target / T</option>');

?>
</select><br>
</td></tr>
<tr>
<td>Text:&nbsp</td><td><input type="text" maxlength="50" name="new_label_text" style="width:400px"></td>
</tr><tr>
<td>Abbr:&nbsp</td><td><input type="text" maxlength="5" name="new_abbr" style="width:40px"></td>
</tr>
</table>
<br>
<input type="button" name="btnGo" value="Go" onclick="onclickGo();">
</form>
<br>

<table>
<b>Available Category Labels</b>


<?php

$result = mssql_query("select * from CategoryLabels order by CategoryID, Label");
$lastID = -1;
while ($row = mssql_fetch_assoc($result))
{
	if($lastID != $row['CategoryID'])
	{
		$lastID = $row['CategoryID'];
		switch($lastID)
		{
			case 1:
				$Category='First Meeting / FM'; break;
			case 2:
				$Category='Information Phase / IP'; break;
			case 3:
				$Category='Stalled Information Phase / SIP'; break;
			case 4:
				$Category='Decision Point / DP'; break;
			case 5:
				$Category='Stalled Decision Point / SDP'; break;
			case 6:
				$Category='Closed / C'; break;
			case 9:
				$Category='Removed / R'; break;
			case 10:
				$Category='Target / T'; break;


		}
		print('<tr><td>&nbsp;</td></tr><tr><td valign="top">' . $Category . '</td></tr>');
	}
	print('<tr><td valign="top"><a href="javascript:confirmDelete(' . $row['ID'] . ');">Delete</a></td><td>' . $row['Label'] . ' / ' . $row['Abbr'] . '</td></tr>');
}

?>

</table>

<?php
print_footer();
?>
