<?php

include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Disable Company', true);

if (isset($disableid))
{
	mssql_query("update company set Disabled = 1 where CompanyID = $disableid");
}

if (isset($enableid))
{
	mssql_query("update company set Disabled = 0 where CompanyID = $enableid");
}

?>

<script language="JavaScript">

function onDisable(id)
{
	window.location.href = 'disable.php?disableid=' + id;
}

function onEnable(id)
{
	window.location.href = 'disable.php?enableid=' + id;
}

</script>

<input type="button" value="Finished" onclick="window.location.href = 'menu.php';">

<table>
<tr>
	<td><b>Disabled</b></td>
	<td><b>Name</b></td>
	<td><b>Directory</b></td>
</tr>
<?php

$result = mssql_query("select * from company order by Name");

while ($row = mssql_fetch_assoc($result))
{
	print('<tr><td align="center" valign="middle">');
	print('<input type="checkbox" ');
	if ($row['Disabled'] > 0)
		print('checked onclick="onEnable(' . $row['CompanyID'] . ')"');
	else
		print('onclick="onDisable(' . $row['CompanyID'] . ')"');
	print('>');
	print('</td><td ');
	if ($row['Disabled'] > 0)
		print('style="color:red; font-weight:bold;"');
	print('>');
	print($row['Name']);
	print('</td><td ');
	if ($row['Disabled'] > 0)
		print('style="color:red; font-weight:bold;"');
	print('>');
	print($row['DirectoryName']);
	print('</td><td ');
	if ($row['Disabled'] > 0)
		print('style="color:red; font-weight:bold;"');
	print('>');
	print($row['CompanyID']);
	print('</td></tr>');
}

?>
</table>


<?php
print_footer();
?>
