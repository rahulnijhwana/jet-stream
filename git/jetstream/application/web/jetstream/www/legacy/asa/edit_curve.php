<?php

include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Edit Curve Analysis', true);

if ($do_add == '1')
	$add_result = mssql_query("insert into analysis (FMValue, IPValue, DPValue, CloseValue, RankNew, RankExperienced) values ($fm_val, $ip_val, $dp_val, $c_val, 1, 1)");

if (isset($fm_val) || isset($ip_val) || isset($dp_val) || isset($c_val))
{
	if (isset($ranknew))
	{
		mssql_query("update analysis set RankNew = $ranknew where FMValue = '$fm_val' and IPValue = '$ip_val' and DPValue = '$dp_val' and CloseValue = '$c_val'");
		print('<span style="color:red;font-style:italic;">Rank New updated</span><br><br>');
	}
	
	if (isset($rankexp))
	{
		mssql_query("update analysis set RankExperienced = $rankexp where FMValue = '$fm_val' and IPValue = '$ip_val' and DPValue = '$dp_val' and CloseValue = '$c_val'");
		print('<span style="color:red;font-style:italic;">Rank Experienced updated</span><br><br>');
	}
	
	$result = mssql_query("select * from analysis where FMValue = '$fm_val' and IPValue = '$ip_val' and DPValue = '$dp_val' and CloseValue = '$c_val'");
	$analysis_row = mssql_fetch_assoc($result);
	$analysisid = $analysis_row['AnalysisID'];
}
else if (isset($analysisid))
{
	if (isset($ranknew))
	{
		mssql_query("update analysis set RankNew = $ranknew where AnalysisID = $analysisid");
		print('<span style="color:red;font-style:italic;">Rank New updated</span><br><br>');
	}

	if (isset($rankexp))
	{
		mssql_query("update analysis set RankExperienced = $rankexp where AnalysisID = $analysisid");
		print('<span style="color:red;font-style:italic;">Rank Experienced updated</span><br><br>');
	}
	
	$result = mssql_query("select * from analysis where AnalysisID = $analysisid");
	$analysis_row = mssql_fetch_assoc($result);
	$fm_val = $analysis_row['FMValue'];
	$ip_val = $analysis_row['IPValue'];
	$dp_val = $analysis_row['DPValue'];
	$c_val = $analysis_row['CloseValue'];
}
else
{
	header("Location: curves.php");
	exit();
}

if (isset($add_reasonid))
{
	$check_exist_result = mssql_query("select * from analysisreasonxref where AnalysisID = $analysisid and ReasonID = $add_reasonid");
	if (mssql_num_rows($check_exist_result) == 0)
	{
		$add_reason_result = mssql_query("insert into analysisreasonxref (AnalysisID, ReasonID) values ($analysisid, $add_reasonid)");
		if ($add_reason_result)
			print('<span style="color:red;font-style:italic;">Reason added</span><br><br>');
	}
}

if (isset($delete_reason))
{
	$delete_result = mssql_query("delete from analysisreasonxref where AnalysisID = $analysisid and ReasonID = $delete_reason");
	if ($delete_result)
		print('<span style="color:red;font-style:italic;">Reason deleted</span><br><br>');
}

if (isset($add_actionid))
{
	$check_exist_result = mssql_query("select * from analysisactionxref where AnalysisID = $analysisid and ActionID = $add_actionid");
	if (mssql_num_rows($check_exist_result) == 0)
	{
		$add_action_result = mssql_query("insert into analysisactionxref (AnalysisID, ActionID) values ($analysisid, $add_actionid)");
		if ($add_action_result)
			print('<span style="color:red;font-style:italic;">Action added</span><br><br>');
	}
}

if (isset($delete_question))
{
	$delete_result = mssql_query("delete from analysisquestionxref where AnalysisID = $analysisid and QuestionID = $delete_question");
	if ($delete_result)
		print('<span style="color:red;font-style:italic;">Question deleted</span><br><br>');
	
	$check_last_in_section_result = mssql_query("select * from questions where QuestionID in (select * from analysisquestionxref where $analysisid) and WithHeader = $delete_question_section");
	if (mssql_num_rows($check_exist_result) == 1)
	{
		$check_row = mssql_fetch_assoc($check_last_in_section_result);
		if ($check_row['IsHeader'] == $delete_question_section)
		{
			$delete_result = mssql_query("delete from analysisquestionxref where AnalysisID = $analysisid and QuestionID = " . $check_row['QuestionID']);
		}
	}
}

if (isset($add_questionid))
{
	$check_exist_result = mssql_query("select * from analysisquestionxref where AnalysisID = $analysisid and QuestionID = $add_questionid");
	if (mssql_num_rows($check_exist_result) == 0)
	{
		$section_check_result = mssql_query("select * from analysisquestionxref where AnalysisID = $analysisid and QuestionID = $sectionid");
		if (mssql_num_rows($section_check_result) == 0)
			mssql_query("insert into analysisquestionxref (AnalysisID, QuestionID) values ($analysisid, $sectionid)");
		
		$add_question_result = mssql_query("insert into analysisquestionxref (AnalysisID, QuestionID) values ($analysisid, $add_questionid)");
		if ($add_question_result)
			print('<span style="color:red;font-style:italic;">Question added</span><br><br>');
	}
}

if (isset($delete_action))
{
	$delete_result = mssql_query("delete from analysisactionxref where AnalysisID = $analysisid and ActionID = $delete_action");
	if ($delete_result)
		print('<span style="color:red;font-style:italic;">Action deleted</span><br><br>');
}

?>

<script language="JavaScript">

function onAddReason()
{
	window.location.href = 'xref_reason_add.php?analysisid=' + analysisid + '&reason_exclusion_list=' + reason_exclusion_list;
}

function onAddQuestion(section)
{
	window.location.href = 'xref_question_add.php?analysisid=' + analysisid + '&section=' + section + '&question_exclusion_list=' + question_exclusion_list;
}

function onAddAction()
{
	window.location.href = 'xref_action_add.php?analysisid=' + analysisid + '&action_exclusion_list=' + action_exclusion_list;
}

</script>

Selected curve: <span style="font-weight:bold; font-size:16pt;"><?= "$fm_val $ip_val $dp_val $c_val" ?></span><br>
<br>
<input type="button" onclick="window.location.href='curves.php';" value="Finished Editing">
<br>
<!-- AnalysisID = <?=$analysisid?><br> -->

<br>
Rank New:
<select onchange="window.location.href = 'edit_curve.php?analysisid=<?=$analysisid?>&ranknew=' + this.value;"?>
	<option value="1" <?=(($analysis_row['RankNew'] == 1) ? 'selected' : '')?> >1</option>
	<option value="2" <?=(($analysis_row['RankNew'] == 2) ? 'selected' : '')?> >2</option>
	<option value="3" <?=(($analysis_row['RankNew'] == 3) ? 'selected' : '')?> >3</option>
	<option value="4" <?=(($analysis_row['RankNew'] == 4) ? 'selected' : '')?> >4</option>
	<option value="5" <?=(($analysis_row['RankNew'] == 5) ? 'selected' : '')?> >5</option>
	<option value="6" <?=(($analysis_row['RankNew'] == 6) ? 'selected' : '')?> >6</option>
	<option value="7" <?=(($analysis_row['RankNew'] == 7) ? 'selected' : '')?> >7</option>
	<option value="8" <?=(($analysis_row['RankNew'] == 8) ? 'selected' : '')?> >8</option>
	<option value="9" <?=(($analysis_row['RankNew'] == 9) ? 'selected' : '')?> >9</option>
	<option value="10" <?=(($analysis_row['RankNew'] == 10) ? 'selected' : '')?> >10</option>
</select>

<br>
Rank Experienced:
<select onchange="window.location.href = 'edit_curve.php?analysisid=<?=$analysisid?>&rankexp=' + this.value;"?>
	<option value="1" <?=(($analysis_row['RankExperienced'] == 1) ? 'selected' : '')?> >1</option>
	<option value="2" <?=(($analysis_row['RankExperienced'] == 2) ? 'selected' : '')?> >2</option>
	<option value="3" <?=(($analysis_row['RankExperienced'] == 3) ? 'selected' : '')?> >3</option>
	<option value="4" <?=(($analysis_row['RankExperienced'] == 4) ? 'selected' : '')?> >4</option>
	<option value="5" <?=(($analysis_row['RankExperienced'] == 5) ? 'selected' : '')?> >5</option>
	<option value="6" <?=(($analysis_row['RankExperienced'] == 6) ? 'selected' : '')?> >6</option>
	<option value="7" <?=(($analysis_row['RankExperienced'] == 7) ? 'selected' : '')?> >7</option>
	<option value="8" <?=(($analysis_row['RankExperienced'] == 8) ? 'selected' : '')?> >8</option>
	<option value="9" <?=(($analysis_row['RankExperienced'] == 9) ? 'selected' : '')?> >9</option>
	<option value="10" <?=(($analysis_row['RankExperienced'] == 10) ? 'selected' : '')?> >10</option>
</select>
<br>

<br><b>Analysis (Reasons)</b><br>
<a href="javascript:onAddReason();">Add Analysis</a><br>
<table>

<?php

$sql = "select * from reasons where ReasonID in (select ReasonID from analysisreasonxref where AnalysisID = $analysisid) order by ReasonID";
$reason_result = mssql_query($sql);

$reason_exclusion_list = '';
while ($reason_row = mssql_fetch_assoc($reason_result))
{
	print('<tr>');
	print('<td><a href="edit_curve.php?analysisid=' . $analysisid . '&delete_reason=' . $reason_row['ReasonID'] . '">Delete</a></td>');
	print('<td>' . $reason_row['ReasonText'] . '</td>');
	print('</tr>');
	
	if ($reason_exclusion_list != '')
		$reason_exclusion_list .= ',';
	$reason_exclusion_list .= $reason_row['ReasonID'];
}

?>

</table><br>

<br><b>Questions</b><br>
<table>

<?php

$sql = "select * from questions where QuestionID in (select QuestionID from analysisquestionxref where AnalysisID = $analysisid) or IsHeader <> 0 order by WithHeader, IsHeader DESC, QuestionID";
$question_result = mssql_query($sql);

$question_exclusion_list = '';
while ($question_row = mssql_fetch_assoc($question_result))
{
	print('<tr>');
	if ($question_row['IsHeader'] != 0)
	{
		print('<td></td>');
		print('<td><b>' . $question_row['QuestionText'] . '</b> <a href="javascript:onAddQuestion(' . $question_row['IsHeader'] . ');">Add question to this section</a></td>');
		$current_section = $question_row['IsHeader'];
	}
	else
	{
		print('<td><a href="edit_curve.php?analysisid=' . $analysisid . '&delete_question=' . $question_row['QuestionID'] . '&delete_question_section=' . $current_section . '">Delete</a></td>');
		print('<td>' . $question_row['QuestionText'] . '</td>');
	}
	print('</tr>');
	
	if ($question_exclusion_list != '')
		$question_exclusion_list .= ',';
	$question_exclusion_list .= $question_row['QuestionID'];
}

?>

</table><br>

<br><b>Actions</b><br>
<a href="javascript:onAddAction();">Add Action</a><br>
<table border="0">

<?php

$sql = "select * from actions where ActionID in (select ActionID from analysisactionxref where AnalysisID = $analysisid) order by ActionID";
$action_result = mssql_query($sql);

$action_exclusion_list = '';
while ($action_row = mssql_fetch_assoc($action_result))
{
	print('<tr><td colspan="3" style="border-bottom-style:solid; border-bottom-width:1px; border-bottom-color:blacks;">&nbsp;</td></tr>');
	print('<tr>');
	print('<td valign="top" rowspan="2"><a href="edit_curve.php?analysisid=' . $analysisid . '&delete_action=' . $action_row['ActionID'] . '">Delete</a></td>');
	print('<td valign="top"><b>New:</b></td><td> ' . $action_row['ActionTextNew'] . '</td><tr><td valign="top"><b>Experienced:</b></td><td>' . $action_row['ActionTextExperienced'] . '</td>');
	print('</tr>');
	
	if ($action_exclusion_list != '')
		$action_exclusion_list .= ',';
	$action_exclusion_list .= $action_row['ActionID'];
}

print('<tr><td colspan="3" style="border-bottom-style:solid; border-bottom-width:1px; border-bottom-color:blacks;">&nbsp;</td></tr>');

?>

</table><br>
<br>
<input type="button" onclick="window.location.href='curves.php';" value="Finished Editing">
<br>
<script language="JavaScript">

reason_exclusion_list = '<?=$reason_exclusion_list?>';
question_exclusion_list = '<?=$question_exclusion_list?>';
action_exclusion_list = '<?=$action_exclusion_list?>';
analysisid = <?=$analysisid?>;

</script>

<?php
print_footer();
?>