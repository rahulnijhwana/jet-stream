<?php
include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Purge Removed', true);
?>

<form name="form1" action="purge.php" method="post">

<?php

function print_reason_select($co)
{
	print("<select name=\"reason\">\n");
	print("<option value=\"\">Please select a removal reason</option>\n");

	$sql = "select ReasonID, Text from removereasons where CompanyID='$co' and Deleted='0' order by Text";
	$result = mssql_query($sql);
	while ($row = mssql_fetch_assoc($result))
	{
		print('<option value="' . $row['ReasonID'] . '">' . $row['Text'] . "</option>\n");
	}

	print("</select>\n");
}

$allow = true;

if (isset($company) && strlen($company))
{
	print('<input type="hidden" name="company" value="' . $company . '">');
	$result = mssql_query("select Name from company where CompanyID = '$company'");
	$row = mssql_fetch_assoc($result);
	print('<p>Purging opportunities removed during training for ' . $row['Name'] . '</p>');

	if (isset($reason) && strlen($reason))
	{
		$sql = "delete from opportunities where CompanyID='$company' and Category='9' and RemoveReason='$reason'";
		mssql_query($sql);
		
		$result = mssql_query("SELECT @@ROWCOUNT");
		list($affected) = mssql_fetch_row($result);
		print('<p>' . $affected . ' opportunities were purged.</p>');

		$allow = false;
	}
	else print_reason_select($company);
}
else print_company_select();

if ($allow) print('<input type="submit" name="submit" value="Continue">');
?>

</form>

<?php
print_footer();
?>
