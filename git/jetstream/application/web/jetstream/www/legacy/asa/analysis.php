<?php

include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Analysis (Reasons)', true);

if (isset($new_reason_text))
{
	$new_reason_text = str_replace("\\\"", "\"", str_replace("\\'", "''", $new_reason_text));
	mssql_query("insert into reasons (ReasonText) values ('$new_reason_text')");
	print('<span style="color:red;font-style:italic;">Analysis Added</span><br><br>');
}

if (isset($deleteid))
{
	mssql_query("delete from reasons where ReasonID = $deleteid");
	mssql_query("delete from analysisreasonxref where ReasonID = $deleteid");
	print('<span style="color:red;font-style:italic;">Analysis Deleted</span><br><br>');
}

?>

<script language="JavaScript">

function confirmDelete(id)
{
	if (confirm("Are you sure you want to completely delete this analysis as well as all of it's associations with curves?"))
		document.location.href = 'analysis.php?deleteid=' + id;
}

</script>

<form action="analysis.php" method="POST">
<b>Add New Analysis</b><br>
<textarea type="text" name="new_reason_text" rows="5" cols="60"></textarea><br>
<input type="submit" value="Add Analysis">
</form>
<br>

<b>Available Analysis</b>
<table>

<?php

$result = mssql_query("select * from reasons order by ReasonID");

while ($row = mssql_fetch_assoc($result))
{
	print('<tr><td valign="top"><a href="javascript:confirmDelete(' . $row['ReasonID'] . ');">Delete</a></td><td>' . $row['ReasonText'] . '</td></tr>');
}

?>

</table>

<?php
print_footer();
?>
