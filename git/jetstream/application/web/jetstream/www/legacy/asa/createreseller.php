<?php

include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Create Reseller', true);


if (isset($ResellerName) && isset($BrandSel))
{
	if($ResellerName == "")
	{
		exit("Error: Reseller Name is blank.");
	}

	if (isset($LogoRequired)) $LogoRequired = 1;
	else $LogoRequired = 0;


	$has_img = (isset($_FILES['LogoImage']) && $_FILES['LogoImage']['size'] > 0);
	if (!$has_img)
	{
		$image_name = '';
		$logo_required = 0;
	}
	else
	{
		$image_name = $_FILES['LogoImage']['name'];
		$ok=move_uploaded_file($_FILES['LogoImage']['tmp_name'], "../../current/shared/images/resellers/" . $image_name);
		if(!$ok)
			exit("Failed to move uploaded file");
		$logo_required = 1;
	}
	$query_str = "INSERT INTO Resellers (BrandID, ResellerName, ResellerLogo, UserLogoRequired)";
	$query_str .= " VALUES ($BrandSel, '$ResellerName', '$image_name', $LogoRequired);";

	$result = mssql_query($query_str);
	if ($result == false)
		exit('Error inserting reseller data');


	exit("New reseller $ResellerName was successfully added." . '<br><a href="createreseller.php">Add Another Reseller</a>');
}

?>

<script language="JavaScript">

function GoForIt()
{
	var theform = document.forms.form1;
	if (theform.ResellerName.value == '')
	{
		alert('"Reseller Name" is blank');
		return;
	}
	theform.submit();
}

</script>

<form enctype="multipart/form-data" name="form1" action="createreseller.php" method="post">

<table>
	<tr>
		<td>Reseller Name</td><td><input type="text" name="ResellerName" size="50"></td>
	</tr>

	<tr>
		<td>
			Logo Image Required
		</td><td><input type="checkbox" name="LogoRequired"></td>
	</tr>

	<tr>
		<td>Logo Image <i>(optional)</i></td><td><input type="hidden" name="MAX_FILE_SIZE" value="1000000" /><input type="file" name="LogoImage"/></td>
	</tr>
	<tr>
		<td>Brand:</td>
		<td>
<?php
		$query_str = "SELECT BrandID, VendorName FROM Branding ORDER BY VendorName;";
		$result = mssql_query($query_str);
		if($result)
		{
			print "<SELECT NAME=\"BrandSel\">";
			while(($row = mssql_fetch_assoc($result)))
			{
				print "<OPTION VALUE=\"" . $row['BrandID'] . "\">";
				print $row['VendorName'] . "</OPTION>";
			}
			print "</SELECT>";
		}
?>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2"><button type="button" onclick="GoForIt();">Submit</button></td>
	</tr>
</table>

</form>

<?php
print_footer();
?>
