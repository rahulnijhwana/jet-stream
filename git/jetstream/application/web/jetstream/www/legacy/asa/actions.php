<?php

include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Actions', true);

if (isset($new_action_text_new) && isset($new_action_text_exp))
{
	$new_action_text_new = str_replace("\\\"", "\"", str_replace("\\'", "''", $new_action_text_new));
	$new_action_text_exp = str_replace("\\\"", "\"", str_replace("\\'", "''", $new_action_text_exp));
	mssql_query("insert into actions (ActionTextNew, ActionTextExperienced) values ('$new_action_text_new', '$new_action_text_exp')");
	print('<span style="color:red;font-style:italic;">Action Added</span><br><br>');
}

if (isset($deleteid))
{
	mssql_query("delete from actions where ActionID = $deleteid");
	mssql_query("delete from analysisactionxref where ActionID = $deleteid");
	print('<span style="color:red;font-style:italic;">Analysis Deleted</span><br><br>');
}

?>

<script language="JavaScript">

function confirmDelete(id)
{
	if (confirm("Are you sure you want to completely delete this action as well as all of it's associations with curves?"))
		document.location.href = 'actions.php?deleteid=' + id;
}

</script>

<form action="actions.php" method="POST">
<b>Add New Action</b><br>
New:<br>
<textarea type="text" name="new_action_text_new" rows="5" cols="60"></textarea><br>
Experienced:<br>
<textarea type="text" name="new_action_text_exp" rows="5" cols="60"></textarea><br>
<input type="submit" value="Add Action">
</form>
<br>

<b>Available Actions</b>
<table>

<?php

$result = mssql_query("select * from actions order by ActionID");

while ($row = mssql_fetch_assoc($result))
{
	print('<tr><td colspan="3" style="border-bottom-style:solid; border-bottom-width:1px; border-bottom-color:blacks;">&nbsp;</td></tr>');
	print('<tr>');
	print('<td valign="top" rowspan="2"><a href="javascript:confirmDelete(' . $row['ActionID'] . ');">Delete</a></td>');
	print('<td valign="top"><b>New:</b></td><td> ' . $row['ActionTextNew'] . '</td><tr><td valign="top"><b>Experienced:</b></td><td>' . $row['ActionTextExperienced'] . '</td>');
	print('</tr>');
}

?>

</table>

<?php
print_footer();
?>
