<?php
session_start();
include_once('../data/_db2.php');

function check_asa_login($callingpage)
{
	$asa_user = $_SESSION['asa_user'];
	$asa_pwd = $_SESSION['asa_pwd'];

	if (isset($asa_user) && isset($asa_pwd) &&
		(
			(!strcmp($asa_user, 'asa') && !strcmp($asa_pwd, 'Xu93y2hsqmrGxKt'))
		))
		return;

	$_SESSION['calling_page'] = $callingpage;
	header("Location: index.php");
	exit();
}

$menu = array("menu_new.php" => "Main Menu");

/*
//$menu = array("menu.php" => "Main Menu",
$menu = array("menu_new.php" => "Main Menu",
			"createcompany.php" => "Create Company|Create a new company in the system",
			"editcompanyname_dir.php" => "Edit Company Name|Change an existing company's displayed name and directory",
			"disable.php" => "Disable Company|Prevent users from logging in for a given company",
			"editlogo.php" => "Edit Logo|Add, change, or delete the logo image on a company's login page",
			"curves.php" => "Curves|Edit curves and associate analyses, questions and actions with curve definitions",
			"analysis.php" => "Analysis|Add or edit analysis items (sometimes called reasons)",
			"questions.php" => "Questions|Add or edit questions for the analysis screen",
			"actions.php" => "Actions|Add or edit actions for the analysis screen",
			//"trends.php" => "Trends|Edit trends",
			"purge.php" => "Purge Removed|Purge opportunities removed during training",
			//"nonpayment.php" => "Non-Payment|Add or remove a non-payment warning for a company",
			"eula.php" => "EULA|Edit the end user license agreement all new users see",
			"deletecompany.php" => "Delete Company|Permanently delete index, logo and all data for a company",
			"milestone_labels.php" => "Milestone Labels|Add or delete synonyms for milestones",
			"editsuperuser.php" => "Superuser|Edit Superuser UserID and Password",
			"personcount.php" => "Head Count|Personnel Count Report",
			"personlastuse.php" => "Last Login|Personnel Last Login Report",
			"mark_person_nocharge.php" => "No Charge|Mark Personnel No Charge",
			"createreseller.php" => "Create reseller|Create Reseller",
			"editreseller.php" => "Edit reseller|Edit Reseller",
			"deletereseller.php" => "Delete reseller|Delete Reseller",
			"category_labels.php" => "Category Labels|Add or delete synonyms for categories");
*/


function print_header($title, $showmenu)
{
	print('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">' . "\n");
	// "M-Power" removed per ASA request (for Systema)
	print("<html lang=\"en\">\n<head>\n<title>System Admin - $title</title>\n");
	print('<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">' . "\n");
	print("</head>\n<body link=\"blue\" vlink=\"blue\">\n\n");
	if ($showmenu) print_menu();
	print("<h3>$title</h3>\n\n");
}

function print_footer()
{
	print("</body>\n</html>\n");
}

function print_menu()
{
	global $menu;

	$first = true;
	foreach ($menu as $file => $desc)
	{
		if ($first)
		{
			print('<p>[ ');
			$first = false;
		}
		else print("\n | ");

		$pos = strpos($desc, '|');
		if ($pos !== false) $desc = substr($desc, 0, $pos);

		print("<a href=\"$file\">$desc</a>");
	}

	print(" ]</p>\n\n");
}

function print_company_select()
{
	print("<select name=\"company\">\n");
	print("<option value=\"\">Please select a company</option>\n");

	$result = mssql_query("select CompanyID, Name from company order by Name");
	while ($row = mssql_fetch_assoc($result))
	{
		print('<option value="' . $row['CompanyID'] . '">' . $row['Name'] . "</option>\n");
	}

	print("</select>\n");
}

?>
