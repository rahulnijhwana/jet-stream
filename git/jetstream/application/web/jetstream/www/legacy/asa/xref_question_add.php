<?php

include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Add Question to Curve Analysis', true);

if ($question_exclusion_list == '')
	$list_result = mssql_query("select * from questions order by WithHeader, IsHeader DESC, QuestionID");
else
	$list_result = mssql_query("select * from questions where QuestionID not in ($question_exclusion_list) or IsHeader <> 0 order by WithHeader, IsHeader DESC, QuestionID");

$result = mssql_query("select * from analysis where AnalysisID = $analysisid");
$analysis_row = mssql_fetch_assoc($result);
$fm_val = $analysis_row['FMValue'];
$ip_val = $analysis_row['IPValue'];
$dp_val = $analysis_row['DPValue'];
$c_val = $analysis_row['CloseValue'];

?>

<script language="JavaScript">

function checkTheForm()
{
	var daform = document.forms.form1;
	var selected_one = null;
	for (var k = 0; k < daform.add_questionid.length; ++k)
	{
		if (daform.add_questionid[k].checked)
			selected_one = daform.add_questionid[k];
	}
	if (!selected_one)
	{
		alert("Please choose a set of questions first");
		return;
	}
	daform.submit();
}

</script>

Selected curve: <span style="font-weight:bold; font-size:16pt;"><?= "$fm_val $ip_val $dp_val $c_val" ?></span><br>

Please choose a question below then click the Add button.<br><br>

<form name="form1" action="edit_curve.php" method="POST">
<input type="hidden" name="analysisid" value="<?=$analysisid?>">

<input type="button" onclick="checkTheForm();" value="Add">
<input type="button" onclick="window.location.href='edit_curve.php?analysisid=<?=$analysisid?>';" value="Cancel">
<table>

<?php

$current_section = 1;
while ($list_row = mssql_fetch_assoc($list_result))
{
	if ($list_row['WithHeader'] == $section)
	{
		if ($list_row['IsHeader'] != 0)
		{
			print('<tr><td colspan="2" style="font-weight:bold;">Section: ' . $list_row['QuestionText'] . '</td></tr>');
			$sectionid = $list_row['QuestionID'];
		}
		else
		{
			print('<tr><td valign="top"><input type="radio" name="add_questionid" value="' . $list_row['QuestionID'] . '"></td><td>');
			print($list_row['QuestionText'] . '</td></tr>');
		}
	}
}

?>

</table>
<input type="hidden" name="sectionid" value="<?=$sectionid?>">
<input type="button" onclick="checkTheForm();" value="Add">
<input type="button" onclick="" value="Cancel">

</form>

<?php
print_footer();
?>