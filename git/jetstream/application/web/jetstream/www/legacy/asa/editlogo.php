<?php

include_once('_asa_utils.php');
include_once('mpsettings.php');

check_asa_login($PHP_SELF);
print_header('Edit Company Logo', true);

if (isset($selectedcompany)) {
	$selected_result = mssql_query("select * from company where CompanyID = $selectedcompany");
	$selected_row = mssql_fetch_assoc($selected_result);
}

$has_img = (isset($_FILES['LogoImage']) && $_FILES['LogoImage']['size'] > 0);
if ($has_img || isset($delete_image)) {
	// must deal with reseller/branding issues (2-21-05)
	$BrandSel = $selected_row['BrandID'];
	$ResellerSel = $selected_row['ResellerID'];

	$query_str = "SELECT ResellerLogo from Resellers where ResellerID = $ResellerSel;";
	$result = mssql_query($query_str);
	if($result) {
		$temp_assoc = mssql_fetch_assoc($result);
		$ResellerLogo = $temp_assoc['ResellerLogo'];
	} else {
		$ResellerLogo = "asa.jpg";
	}

	$query_str = "SELECT BrandName, DomainName, TrademarkText from Branding where BrandID = $BrandSel;";
	$result = mssql_query($query_str);
	if($result) {
		$temp_assoc = mssql_fetch_assoc($result);
		$BrandName = $temp_assoc['BrandName'];
		$DomainName = $temp_assoc['DomainName'];
		$TrademarkText = $temp_assoc['TrademarkText'];
	} else {
		// failed, assume ASA/M-Power
		$BrandName = "M-Power";
		$DomainName = "mpasa.com";
	}
	$company_id = $selectedcompany;
	$DirectoryName = $selected_row['DirectoryName'];
	/*
	$templateStr = '';
	if ($has_img)
		$templateStr = file_get_contents("index_template.html");
	else
		$templateStr = file_get_contents("index_template_no_logo.html");

	$FullCompanyName = $selected_row['Name'];

	$templateStr = str_replace("{COMPANYNAME}", $FullCompanyName, $templateStr);
	$templateStr = str_replace("{COMPANYID}", $company_id, $templateStr);

	// 2-21-05 need to deal with reseller/branding issues
	$templateStr = str_replace("{BRANDNAME}", $BrandName, $templateStr);
	$templateStr = str_replace("{DOMAINNAME}", $DomainName, $templateStr);
	$templateStr = str_replace("{RESELLERLOGO}", $ResellerLogo, $templateStr);
	$templateStr = str_replace("{TRADEMARKTEXT}", $TrademarkText, $templateStr);
	*/
	//mkdir("../../$DirectoryName");
	if ($has_img) {
		$temp_image_name = $_FILES['LogoImage']['name'];
		$temp_image_name = strtolower($temp_image_name);
		if (strpos($temp_image_name, '.jpg') !== FALSE || strpos($temp_image_name, '.jpeg') !== FALSE)
			$temp_image_name = 'logo.jpg';
		if (strpos($temp_image_name, '.gif') !== FALSE)
			$temp_image_name = 'logo.gif';
		if (strpos($temp_image_name, '.bmp') !== FALSE)
			$temp_image_name = 'logo.bmp';
		if (strpos($temp_image_name, '.png') !== FALSE)
			$temp_image_name = 'logo.png';
		move_uploaded_file($_FILES['LogoImage']['tmp_name'], MP_PATH . "/$DirectoryName/" . $temp_image_name);
		//$templateStr = str_replace("{LOGO}", $temp_image_name, $templateStr);

	//Update DB
		$sql="UPDATE company SET LogoName='" . $temp_image_name . "' where CompanyID=$company_id";
		$result = mssql_query($sql);
		if ($result == false)
			exit('Error updating logo name');
	}
	/*
	$fp = fopen("../../$DirectoryName/index.html", "w");
	if ($fp == FALSE)
		exit("Could not create index file");
	fwrite($fp, $templateStr);
	fclose($fp);
	*/
	shell_exec("cp -f index_template_new.php " . MP_PATH . "/$DirectoryName/index.php");
	shell_exec("chmod 775 " . MP_PATH . "/$DirectoryName");
	shell_exec("chmod 775 " . MP_PATH . "/$DirectoryName/*");

	if (isset($delete_image)) {
		shell_exec("rm $delete_image");
		//print("UPDATE company SET LogoName = null where CompanyID = $company_id");
		mssql_query("UPDATE company SET LogoName = null where CompanyID = $company_id");
	}
}

?>

<script language="JavaScript">

function onChangeCompany(id)
{
	if (id != -1)
		window.location.href = 'editlogo.php?selectedcompany=' + id;
}

</script>

<select onchange="onChangeCompany(this.value)">
<option value="-1">--- Select a company ---</option>
<?php

$company_result = mssql_query("select * from company order by Name");

while ($row = mssql_fetch_assoc($company_result))
{
	print('<option value="' . $row['CompanyID'] . '" ');
	if (isset($selectedcompany) && $row['CompanyID'] == $selectedcompany)
		print('selected');
	print('>' . $row['Name'] . ' (' . $row['DirectoryName'] . ')</option>');
}

?>
</select><br><br>

<?php

if (isset($selectedcompany)):

	$dirname = $selected_row['DirectoryName'];

	$files = shell_exec("ls " . MP_PATH . "/$dirname/logo.*");
	$files = str_replace("\n", ',', $files);
	$files = explode(',', $files);

	if (count($files) > 1) {
		$current_image = $files[0];
		print('<b>Current Logo:</b><br><br>');
		print('<img src="' . str_replace(MP_PATH, '', $current_image) . '?randnum=' . rand() . '">');
		print('<br><br>');
		print('<a href="editlogo.php?selectedcompany=' . $selectedcompany . '&delete_image=' . $current_image . '" value="Delete Logo">Delete Logo</a>');

	}
	else
		print('This company does not have a logo');

?>
<br><br>

<b>Upload new Logo:</b><br><br>
<form enctype="multipart/form-data" name="form1" action="editlogo.php" method="post">
<input type="hidden" name="selectedcompany" value="<?=$selectedcompany?>">
<input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
<input type="file" name="LogoImage"></input><br><br>
<input type="submit" value="Submit New Logo">
</form>

<?php endif; ?>

<?php
print_footer();
?>
