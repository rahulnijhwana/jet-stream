<?php
include('../reports/_report_utils.php');
include_once('_asa_utils.php');
check_asa_login($PHP_SELF);

print_header('Personnel Last Login Report', true);

//begin_report("Personnel Last Login Report");


/*
if(isset($CompanyID))
{
	print_report_top("Personnel Last Login Report");
	print("<meta c=\"Refresh\" content=\"10; URL=personlastuse2.php?class=1\">");
	print("<br><br><b>     Please wait while your report is being prepared.<br><br>");
	end_report();
	exit();
}
*/
?>

<script language="JavaScript">

function onChangeCompany(id)
{
	var theform = document.forms.form1;
	theform.CompanyID.value = id;
}


function DoIt()
{
	var theform = document.forms.form1;

	if (theform.CompanyID.value == '-1')
	{
		alert('Please make a selection and try again.');
		return;
	}

	var strAction = "personlastuse2.php?CompanyID=" + theform.CompanyID.value;
	theform.action=strAction;
	theform.submit();

}
</script>
<select onchange="onChangeCompany(this.value)">
<option value="-1">--- Select a company ---</option>

<?php
$company_result = mssql_query("select * from company where (Demo=0 or Demo is null) order by Name ");
while ($row = mssql_fetch_assoc($company_result))
{
	print('<option value="' . $row['CompanyID'] . '" ');
	if (isset($selectedcompany) && $row['CompanyID'] == $selectedcompany)
		print('selected');
	print('>' . $row['Name'] . '</option>');
}
?>
</select><br><br>
<form  enctype="application/x-www-form-urlencoded" name="form1" action="personlastuse.php" method="post">
<table>
<tr><td><input type="hidden" name="CompanyID" value="-1"></td></tr>
<tr><td align="center" colspan="2"><button type="button" onclick="DoIt();">Submit</button></td></tr>
</table>
</form>

<?php
print_footer();
?>
