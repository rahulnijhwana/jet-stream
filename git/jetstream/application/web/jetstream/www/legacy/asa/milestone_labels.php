<?php

include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Milestone Labels', true);

if (isset($new_label_text) && isset($whichmilestone))
{
	$new_label_text = str_replace("\\\"", "\"", str_replace("\\'", "''", $new_label_text));
	mssql_query("insert into MilestoneLabels (Label, MilestoneID) values ('$new_label_text', $whichmilestone)");
	print('<span style="color:red;font-style:italic;">Milestone Label Added</span><br><br>');
}

if (isset($deleteid))
{
	mssql_query("delete from MilestoneLabels where ID = $deleteid");
	print('<span style="color:red;font-style:italic;">Milestone Label Deleted</span><br><br>');
}

?>

<script language="JavaScript">

function confirmDelete(id)
{
	if (confirm("Are you sure you want to delete this milestone label?"))
		document.location.href = 'milestone_labels.php?deleteid=' + id;
}


</script>

<form action="milestone_labels.php" method="POST">
<b>Add New Milestone Label</b><br>
Milestone: <select name="whichmilestone">
<?php
	print('<option value="2">Person</option>');
	print('<option value="3">Need</option>');
	print('<option value="4">Money</option>');
	print('<option value="5">Time</option>');
?>
</select><br>
<input type="text" name="new_label_text" style="width:400px"></textarea><br>
<input type="submit" value="Submit">
</form>
<br>

<b>Available Milestone Labels</b>
<table>

<?php

$result = mssql_query("select * from MilestoneLabels order by MilestoneID, Label");
$lastID = -1;
while ($row = mssql_fetch_assoc($result))
{
	if($lastID != $row['MilestoneID'])
	{
		$lastID = $row['MilestoneID'];
		switch($lastID)
		{
			case 2:
				$Milestone='Person';break;
			case 3:
				$Milestone='Need';break;
			case 4:
				$Milestone='Money';break;
			case 5:
				$Milestone='Time';break;
		}
		print('<tr><td>&nbsp;</td></tr><tr><td valign="top">' . $Milestone . '</td></tr>');
	}
	print('<tr><td valign="top"><a href="javascript:confirmDelete(' . $row['ID'] . ');">Delete</a></td><td>' . $row['Label'] . '</td></tr>');
}

?>

</table>

<?php
print_footer();
?>
