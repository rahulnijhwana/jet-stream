<?php
include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">
<head>
<title>System Administration Utilities</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language="JavaScript" src="../javascript/smartselect.js"></script>

</head>
<body link="blue" vlink="blue" onLoad="onLoad();">
<script language="JavaScript">

var arrUtils=Array();
<?if ($asa_user == 'asa'):?>
	arrUtils[arrUtils.length] = Array('Create Company', 'createcompany.php');
	arrUtils[arrUtils.length] = Array('Edit Company', 'editcompanyname_dir.php');
	arrUtils[arrUtils.length] = Array('Disable Company', 'disable.php');
	arrUtils[arrUtils.length] = Array('Edit Logo', 'editlogo.php');
	arrUtils[arrUtils.length] = Array('Curves', 'curves.php');
	arrUtils[arrUtils.length] = Array('Analysis', 'analysis.php');
	arrUtils[arrUtils.length] = Array('Questions', 'questions.php');
	arrUtils[arrUtils.length] = Array('Actions', 'actions.php');
	arrUtils[arrUtils.length] = Array('Purge Removed', 'purge.php');
	arrUtils[arrUtils.length] = Array('EULA', 'eula.php');
	arrUtils[arrUtils.length] = Array('Delete Company', 'deletecompany.php');
	arrUtils[arrUtils.length] = Array('Labels Milestone', 'milestone_labels.php');
	arrUtils[arrUtils.length] = Array('Superuser', 'editsuperuser.php');
	arrUtils[arrUtils.length] = Array('CreateOptionSet', 'createoptionset.php');
	arrUtils[arrUtils.length] = Array('EditOptionSet', 'editoptionset.php');
<?endif;?>
<?if ($asa_user == 'asa' || $asa_user == 'rahn'):?>
	arrUtils[arrUtils.length] = Array('Head Count', 'personcount.php');
	arrUtils[arrUtils.length] = Array('Last Login', 'personlastuse.php');
	arrUtils[arrUtils.length] = Array('Rahn Report', 'rahnreport.php');
<?endif;?>
<?if ($asa_user == 'asa'):?>
	arrUtils[arrUtils.length] = Array('Create Reseller', 'createreseller.php');
	arrUtils[arrUtils.length] = Array('Edit Reseller', 'editreseller.php');
	arrUtils[arrUtils.length] = Array('Delete Reseller', 'deletereseller.php');
	arrUtils[arrUtils.length] = Array('Labels Column', 'category_labels.php');
	arrUtils[arrUtils.length] = Array('Mark Personnel No Charge','mark_person_nocharge.php');
	arrUtils[arrUtils.length] = Array('Edit Trends',	'trends.php');
	arrUtils[arrUtils.length] = Array('Non-payment Warning','nonpayment.php');
	arrUtils[arrUtils.length] = Array('Mark Reports Viewable By Salespeople','report_permissions.php');
	arrUtils[arrUtils.length] = Array('Edit Company Default Branch', 'defaultbranch.php');
<?endif;?>


var SS;

function onLoad()
{
	SS = new SmartSelect(document.getElementById('runUtil'), arrUtils, 1, 0, -1, true);
	SS.setSubmitBtn(document.getElementById('btnGo'));

	SS.setEditFocus();

}

function Go()
{

	var form = document.forms['form1'];
	var fname = SS.getSelectedVal();
	if (fname == '-1')
		alert('Please make a valid selection');
	else
	{
		form.action = fname;
		form.submit();
	}
}


</script>
<form enctype="multipart/form-data" name="form1" method="post">

<table>
<tr><td>Select utility:&nbsp</td><td ><input style="width:250px" type="text" id="runUtil"></td>
<td align="center"><input id='btnGo' type="submit" onClick="Go();" value="Go"></td></tr>
</table>



</form>
</body></html>

