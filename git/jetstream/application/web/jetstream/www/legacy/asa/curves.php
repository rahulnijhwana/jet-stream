<?php

include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Curves', true);

?>

<script language="JavaScript">

function onEditCurve()
{
	var daform = document.forms.form1;
	var lookupstr = '_';
	lookupstr += daform.fm_val.value;
	lookupstr += daform.ip_val.value;
	lookupstr += daform.dp_val.value;
	lookupstr += daform.c_val.value;

	if (!lookup_table[lookupstr])
	{
		if (confirm('That curve does not exist. A salesperson with this curve would receive the following message for their analysis: "This analysis is not expected to occur during normal sales operations." Add this curve now?'))
			onAddCurve();
		return;
	}
	
	daform.do_add.value = '0';
	daform.submit();
}

function onAddCurve()
{
	var daform = document.forms.form1;
	var lookupstr = '_';
	lookupstr += daform.fm_val.value;
	lookupstr += daform.ip_val.value;
	lookupstr += daform.dp_val.value;
	lookupstr += daform.c_val.value;
	
	if (lookup_table[lookupstr])
	{
		if (confirm('That curve already exists. Edit it now?'))
			onEditCurve();
		return;
	}
	
	daform.do_add.value = '1';
	daform.submit();
}

var currently_highlited = null;

function onGoto()
{
	var daform = document.forms.form1;
	
	var tempstr = 'tr';
	tempstr += daform.fm_val.value;
	tempstr += daform.ip_val.value;
	tempstr += daform.dp_val.value;
	tempstr += daform.c_val.value;
	
	var target_tr = document.getElementById(tempstr);
	if (!target_tr)
	{
		if (confirm('That curve does not exist. Would you like to view all curves including the missing ones?'))
			window.location.href = 'curves.php?show_missing_curves=1';
		return;
	}
	
	if (currently_highlited)
	{
		var temptr = document.getElementById(currently_highlited);
		temptr.style.backgroundColor = 'transparent';
	}
	
	currently_highlited = tempstr;
	target_tr.style.backgroundColor = 'lightblue';
	
	var temphref = window.location.href;
	var poundpos = temphref.indexOf('#');
	if (poundpos >= 0)
		temphref = temphref.substring(0, poundpos);
	
	window.location.href = temphref + '#' + tempstr;
	
}

</script>

<form name="form1" action="edit_curve.php" method="POST">

<input type="hidden" name="do_add" value="0">

<table>
	<tr>
		<td align="center">FM</td>
		<td align="center">IP</td>
		<td align="center">DP</td>
		<td align="center">C</td>
		<td align="center" valign="middle" rowspan="2">
			<input type="button" onclick="onAddCurve();" value="Add">
			<input type="button" onclick="onEditCurve();" value="Edit">
			<input type="button" onclick="onGoto();" value="Go to">
		</td>
	</tr>
	<tr>
		<td>
			<select name="fm_val">
				<option value="0">0</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
			</select>
		</td>
		<td>
			<select name="ip_val">
				<option value="0">0</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
			</select>
		</td>
		<td>
			<select name="dp_val">
				<option value="0">0</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
			</select>
		</td>
		<td>
			<select name="c_val">
				<option value="0">0</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
			</select>
		</td>
	</tr>
</table>

</form>

<?php

if (!isset($sortby))
	$sortby = 'curve';

if ($show_missing_curves == '1'):
?>

<a href="curves.php?show_missing_curves=0">Hide Missing Curves</a>

<?php else: ?>

<form name="sort" action="curves.php">
Sorted by
<select name="sortby" onchange="document.forms.sort.submit();">
	<option value="curve" <?= (($sortby == 'curve') ? 'selected="true"' : '') ?> >Curve Values</option>
	<option value="rank_new" <?= (($sortby == 'rank_new') ? 'selected="true"' : '') ?> >Rank New</option>
	<option value="rank_exp" <?= (($sortby == 'rank_exp') ? 'selected="true"' : '') ?> >Rank Experienced</option>
</select>
</form>

<a href="curves.php?show_missing_curves=1">Show Missing Curves</a>

<?php endif; ?>

<table border="1">
	<tr>
		
		<td colspan="4" align="center" style="font-weight:bold;<?= (($sortby == 'curve') ? 'background-color:lightgreen;' : '') ?>">Curve</td>
		<td colspan="2" align="center" style="font-weight:bold;">Rank</td>
		<td rowspan="2" align="center" style="font-weight:bold;">Actions</td>
	</tr>
	<tr>
		<td align="center" width="40" style="font-weight:bold;<?= (($sortby == 'curve') ? 'background-color:lightgreen;' : '') ?>">FM</td>
		<td align="center" width="40" style="font-weight:bold;<?= (($sortby == 'curve') ? 'background-color:lightgreen;' : '') ?>">IP</td>
		<td align="center" width="40" style="font-weight:bold;<?= (($sortby == 'curve') ? 'background-color:lightgreen;' : '') ?>">DP</td>
		<td align="center" width="40" style="font-weight:bold;<?= (($sortby == 'curve') ? 'background-color:lightgreen;' : '') ?>">C</td>
		<td align="center" width="100" style="font-weight:bold;<?= (($sortby == 'rank_new') ? 'background-color:lightgreen;' : '') ?>">New</td>
		<td align="center" width="100" style="font-weight:bold;<?= (($sortby == 'rank_exp') ? 'background-color:lightgreen;' : '') ?>">Experienced</td>
	</tr>
<?php

$lookup_table = 'var lookup_table = new Array();';

if ($show_missing_curves == '1')
{
	for ($temp_fm = 0; $temp_fm < 4; ++$temp_fm)
	{
		for ($temp_ip = 0; $temp_ip < 4; ++$temp_ip)
		{
			for ($temp_dp = 0; $temp_dp < 4; ++$temp_dp)
			{
				for ($temp_c = 0; $temp_c < 4; ++$temp_c)
				{
					print('<tr id="tr' . $temp_fm . $temp_ip . $temp_dp . $temp_c . '">');
					print('<td align="center">' . $temp_fm . '</td>');
					print('<td align="center">' . $temp_ip . '</td>');
					print('<td align="center">' . $temp_dp . '</td>');
					print('<td align="center">' . $temp_c . '</td>');

					$temp_result = mssql_query("select * from analysis where FMValue = '$temp_fm' and IPValue = '$temp_ip' and DPValue = '$temp_dp' and CloseValue = '$temp_c'");
					if ($temp_row = mssql_fetch_assoc($temp_result))
					{
						print('<td align="center">' . $temp_row['RankNew'] . '</td>');
						print('<td align="center">' . $temp_row['RankExperienced'] . '</td>');
						print('<td align="center"><a href="edit_curve.php?analysisid=' . $temp_row['AnalysisID'] . '">Edit</a> <a href="curves.php?deletethis=' . $temp_row['AnalysisID'] . '">Delete</a></td>');
						
						$lookup_table .= 'lookup_table["_' . $temp_fm . $temp_ip . $temp_dp . $temp_c . '"] = true;';
					}
					else
					{
						print('<td align="center" colspan="2" style="background-color:pink;">Missing</td>');
						print('<td align="center"><a href="">Add</a></td>');
					}
				}
			}
		}
	}
}
else
{
	if ($sortby == 'curve')
		$all_analysis_result = mssql_query("select * from analysis where AnalysisID <> -1 order by FMValue, IPValue, DPValue, CloseValue");
	else if ($sortby == 'rank_new')
		$all_analysis_result = mssql_query("select * from analysis where AnalysisID <> -1 order by RankNew, RankExperienced");
	else if ($sortby == 'rank_exp')
		$all_analysis_result = mssql_query("select * from analysis where AnalysisID <> -1 order by RankExperienced, RankNew");
	while ($analysis_row = mssql_fetch_assoc($all_analysis_result))
	{
		print('<tr id="tr' . $analysis_row['FMValue'] . $analysis_row['IPValue'] . $analysis_row['DPValue'] . $analysis_row['CloseValue'] . '">');
		print('<td align="center">' . $analysis_row['FMValue'] . '</td>');
		print('<td align="center">' . $analysis_row['IPValue'] . '</td>');
		print('<td align="center">' . $analysis_row['DPValue'] . '</td>');
		print('<td align="center">' . $analysis_row['CloseValue'] . '</td>');
		print('<td align="center">' . $analysis_row['RankNew'] . '</td>');
		print('<td align="center">' . $analysis_row['RankExperienced'] . '</td>');
		print('<td align="center"><a href="edit_curve.php?analysisid=' . $analysis_row['AnalysisID'] . '">Edit</a> <a href="curves.php?deletethis=' . $analysis_row['AnalysisID'] . '">Delete</a></td>');
		print('</tr>');

		$lookup_table .= 'lookup_table["_' . $analysis_row['FMValue'] . $analysis_row['IPValue'] . $analysis_row['DPValue'] . $analysis_row['CloseValue'] . '"] = true;';
	}
}

?>


</table>

<script language="JavaScript">
<?= $lookup_table ?>
</script>

<?php
print_footer();
?>
