<?php
include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('End User License Agreement', true);

if (isset($eulatext))
{
	$eulatext = str_replace("\\'", "''", $eulatext);
	$eulatext = nl2br($eulatext);
	mssql_query("update systemsettings set EulaText='$eulatext' where SystemID='1'");
}
?>

<form name="form1" action="eula.php" method="post">

<?php
$eulatext = '';
$result = mssql_query("select EulaText from systemsettings where SystemID='1'");
if ($row = mssql_fetch_assoc($result)) 
{
	$eulatext = $row['EulaText'];
	$eulatext = stripslashes($eulatext);
	$eulatext = str_replace('<br />', '', $eulatext);
}
print("<textarea name=\"eulatext\" rows=30 cols=80>$eulatext</textarea>");
?>

<br>
<input type="submit" name="submit" value="Save">
</form>

<?php
print_footer();
?>
