<?php

include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Delete Reseller', true);



if (isset($selectedreseller))
{
	$selected_result = mssql_query("select * from Resellers where ResellerID = $selectedreseller");
	if($selected_result) $selected_row = mssql_fetch_assoc($selected_result);
	else exit("Unknown reseller No. $selectedreseller");
}

if (isset($selectedreseller) && isset($dodelete) && $dodelete==true)
{
	$name = $selected_row['ResellerName'];
	mssql_query("delete from Resellers where ResellerID = $selectedreseller");
	print("Reseller $name has been permanently deleted");
	exit();
}

?>

<script language="JavaScript">

function onChangeReseller(id)
{
	window.location.href = 'deletereseller.php?selectedreseller=' + id;
}

function DoIt()
{
	if (!confirm("WARNING!\n\nYou are about to delete\n\nALL DATA\n\nfor this reseller\n\nPERMANENTLY.\n\n Do you want to continue?"))
		exit();

	var theform = document.forms.form1;
	var strAction = "deletereseller.php?selectedreseller=" + theform.ResellerID.value + "&dodelete=true";
	theform.action=strAction;
	theform.submit();
}


</script>


<select onchange="onChangeReseller(this.value)">
<option value="-1">--- Select a reseller ---</option>
<?php

$company_result = mssql_query("select * from Resellers order by ResellerName");

while ($row = mssql_fetch_assoc($company_result))
{
	print('<option value="' . $row['ResellerID'] . '" ');
	if (isset($selectedreseller) && $row['ResellerID'] == $selectedreseller)
		print('selected');
	print('>' . $row['ResellerName'] . '</option>');

}
?>
</select><br><br>
<?php
if (isset($selectedreseller))
	{
	$ResellerName = $selected_row['ResellerName'];
	print('<form  name="form1" action="deletereseller.php" method="post">');
	print('<table><tr><td>Reseller to Delete:  <b>' . $ResellerName . '</b></td></tr>');
	print('<tr><td><input type="hidden" name="ResellerID" value="' . $selectedreseller . '"></td></tr>');
	print('<tr><td align="center" colspan="2"><button type="button" onclick="DoIt();">Delete</button></td></tr></table></form>');
	}
print_footer();
?>

