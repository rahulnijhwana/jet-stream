<?php

include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Add Question to Curve Analysis', true);

if ($action_exclusion_list == '')
	$list_result = mssql_query("select * from actions order by ActionID");
else
	$list_result = mssql_query("select * from actions where ActionID not in ($action_exclusion_list) order by ActionID");

$result = mssql_query("select * from analysis where AnalysisID = $analysisid");
$analysis_row = mssql_fetch_assoc($result);
$fm_val = $analysis_row['FMValue'];
$ip_val = $analysis_row['IPValue'];
$dp_val = $analysis_row['DPValue'];
$c_val = $analysis_row['CloseValue'];

?>

<script language="JavaScript">

function checkTheForm()
{
	var daform = document.forms.form1;
	var selected_one = null;
	for (var k = 0; k < daform.add_actionid.length; ++k)
	{
		if (daform.add_actionid[k].checked)
			selected_one = daform.add_actionid[k];
	}
	if (!selected_one)
	{
		alert("Please choose an action first");
		return;
	}
	daform.submit();
}

</script>

Selected curve: <span style="font-weight:bold; font-size:16pt;"><?= "$fm_val $ip_val $dp_val $c_val" ?></span><br>

Please choose an action below then click the Add button.<br><br>

<form name="form1" action="edit_curve.php" method="POST">
<input type="hidden" name="analysisid" value="<?=$analysisid?>">

<input type="button" onclick="checkTheForm();" value="Add">
<input type="button" onclick="window.location.href='edit_curve.php?analysisid=<?=$analysisid?>';" value="Cancel">
<table>

<?php

while ($list_row = mssql_fetch_assoc($list_result))
{
	print('<tr><td colspan="3" style="border-bottom-style:solid; border-bottom-width:1px; border-bottom-color:blacks;">&nbsp;</td></tr>');
	print('<tr><td valign="top" rowspan="2"><input type="radio" name="add_actionid" value="' . $list_row['ActionID'] . '"></td>');
	print('<td valign="top"><b>New:</b></td><td> ' . $list_row['ActionTextNew'] . '</td><tr><td valign="top"><b>Experienced:</b></td><td>' . $list_row['ActionTextExperienced'] . '</td>');
	print('</tr>');
}

?>

</table>
<input type="button" onclick="checkTheForm();" value="Add">
<input type="button" onclick="" value="Cancel">

</form>

<?php
print_footer();
?>