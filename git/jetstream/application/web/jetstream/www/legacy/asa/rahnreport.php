<?php
include('../reports/_report_utils.php');
include('../reports/path.php');

function GetPersonnelCounts()
{
	$comparray = array();
	$sql = "SELECT company.Name as Comp, company.Class, people.[Level] as Lv, people.SupervisorID as Sup, ";
	$sql .= "people.IsSalesperson as bSales, logins.LoginID as LoginID, logins.NoCharge as NC, ";
	$sql .= "people.PersonID AS PersonID FROM Logins INNER JOIN people ";
	$sql .= "ON Logins.PersonID = people.PersonID INNER JOIN company ";
	$sql .= "ON people.CompanyID = company.CompanyID ";
	$sql .= "WHERE ((company.Demo=0) or (company.Demo IS NULL)) and (company.Disabled=0) ";
	$sql .= "and ((people.Deleted=0) or (people.Deleted IS NULL)) ";
	$sql .= "and (NOT (people.SupervisorID = -1)) ";
	$sql .= "ORDER BY Company.Class, company.Name, people.[Level] DESC, people.IsSalesperson, people.LastName";

	$result = mssql_query($sql);

	$class='';
	$last = "";
	$total=0;
	$nctotal=0;
	$admin=0;
	$sales=0;
	$salesnc=0;
	$mgrs=0;
	$mgrsnc=0;
	$aliases=0;
	$aliasesnc=0;
	$bDone=0;
	if($result)
	{
		while(1)
		{
			$row = mssql_fetch_assoc($result);
			if(!$row) $bDone = 1;
			if(strlen($last) && $last != $row['Comp'])
			{
				$comp = array();
				$comp['class'] = $class;
				$comp['comp'] = $last;
				$comp['admin'] = $admin;
				$comp['sales'] = $sales;
				$comp['salesnc'] = $salesnc;
				$comp['mgrs'] = $mgrs;
				$comp['mgrsnc'] = $mgrsnc;
				$comp['aliases'] = $aliases;
				$comp['aliasesnc'] = $aliasesnc;
				$comp['total'] = $total;
				$comp['nctotal'] = $nctotal;
				array_push($comparray, $comp);
				$total=0;
				$nctotal=0;
				$admin=0;
				$sales=0;
				$salesnc=0;
				$mgrs=0;
				$mgrsnc=0;
				$aliases=0;
				$aliasesnc=0;
			}
			if($bDone == 1) break;
			if(is_null($row['Class'])) $class=0;
			else $class = $row['Class'];
			$last = $row['Comp'];
			$level = $row['Lv'];
			$sup = $row['Sup'];
			$nc = ($row['NC'] == '1');

//nc and administrators not included in total
			if (!$nc && $sup != -3)
				$total++;
			else
				$nctotal++;


			if($row['LoginID'] != $row['PersonID'])
				{
				$nc ? ++$aliasesnc : ++$aliases;
				continue;
				}
			if ($sup == -3)
				{
				++$admin;
				continue;
				}
			if ($level == 1)
				{
				$nc ? ++$salesnc : ++$sales;
				continue;
				}
			$nc ? ++$mgrsnc : ++$mgrs;
		}
	}

	return $comparray;
}

function GetClassTotals($arrCompanies)
{
	$arrTotals = array();
	$last=-1;
	$i=0;
	$bDone=0;
	$compcount=0;
	while(1)
	{
		if(count($arrCompanies) == $i) $bDone = 1;
		else $comp = $arrCompanies[$i++];
		if($last > -1 && ($bDone || $last != $comp['class']))
		{
			$tmp = array();
			$tmp['class'] = $last;
			$tmp['totAdmin'] = $totAdmin;
			$tmp['totMgrs'] = $totMgrs;
			$tmp['totMgrsNC'] = $totMgrsNC;
			$tmp['totSales'] = $totSales;
			$tmp['totSalesNC'] = $totSalesNC;
			$tmp['totAliases'] = $totAliases;
			$tmp['totAliasesNC'] = $totAliasesNC;
			$tmp['totCharged'] = $totCharged;
			$tmp['totNotCharged'] = $totNotCharged;
			$tmp['compcount'] = $compcount;
			array_push($arrTotals, $tmp);
		}
		if($bDone) break;
		if($last != $comp['class'])
			$totAdmin=$totMgrs=$totMgrsNC=$totSales=$totSalesNC=$totAliases=$totAliasesNC=$totCharged=$totNotCharged=$compcount=0;
		$last = $comp['class'];
		$totAdmin += $comp['admin'];
		$totMgrs += $comp['mgrs'];
		$totMgrsNC += $comp['mgrsnc'];
		$totSales += $comp['sales'];
		$totSalesNC += $comp['salesnc'];
		$totAliases += $comp['aliases'];
		$totAliasesNC += $comp['aliasesnc'];
		$totCharged += $comp['total'];
		$totNotCharged += $comp['nctotal'];
		$compcount++;
	}
	return $arrTotals;
}

$arrCompanies = GetPersonnelCounts();
$arrTotals = GetClassTotals($arrCompanies);

$count = count($arrCompanies);
$lastclass=-1;
$bFirst=1;
$indClass=0;
for($i=0; $i<$count; $i++)
{
	$comp = $arrCompanies[$i];
	if($lastclass != $comp['class'])
	{
		$lastclass = $comp['class'];
		WriteClassHeading($report, $comp['class'], !$bFirst, $arrTotals[$indClass]['compcount']);
		WriteClassTotals($report, $arrTotals[$indClass]);
		AddSpacerRow($report);
		$indClass++;
		$bFirst = 0;
	}

	BeginNewRow($report, RT_COL_TEXT, SMALL_FONTSIZE, 0);
	AddColumnText($report, $comp['comp'], "", 0, 0);
	AddColumnText($report, $comp['admin'], "right", 0, 0);
	AddColumnText($report, $comp['mgrs'], "right", 0, 0);
	AddColumnText($report, $comp['mgrsnc'], "right", 0, 0);
	AddColumnText($report, $comp['sales'], "right", 0, 0);
	AddColumnText($report, $comp['salesnc'], "right", 0, 0);
	AddColumnText($report, $comp['aliases'], "right", 0, 0);
	AddColumnText($report, $comp['aliasesnc'], "right", 0, 0);
	AddColumnText($report, $comp['total'], "right", 0, 0);
	AddColumnText($report, $comp['nctotal'], "right", 0, 0);
	AddColumnText($report, $comp['total']+$comp['nctotal'], "right", 0, 0);
	if(!($i % 2))
		SetRowColorStandard($report, -1, SHADE_LIGHT);

}

close_db();
?>
