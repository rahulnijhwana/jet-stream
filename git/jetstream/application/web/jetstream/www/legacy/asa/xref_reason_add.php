<?php

include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Add Reason to Curve Analysis', true);

if ($reason_exclusion_list == '')
	$list_result = mssql_query("select * from reasons order by ReasonID");
else
	$list_result = mssql_query("select * from reasons where ReasonID not in ($reason_exclusion_list) order by ReasonID");

$result = mssql_query("select * from analysis where AnalysisID = $analysisid");
$analysis_row = mssql_fetch_assoc($result);
$fm_val = $analysis_row['FMValue'];
$ip_val = $analysis_row['IPValue'];
$dp_val = $analysis_row['DPValue'];
$c_val = $analysis_row['CloseValue'];

?>

<script language="JavaScript">

function checkTheForm()
{
	var daform = document.forms.form1;
	var selected_one = null;
	for (var k = 0; k < daform.add_reasonid.length; ++k)
	{
		if (daform.add_reasonid[k].checked)
			selected_one = daform.add_reasonid[k];
	}
	if (!selected_one)
	{
		alert("Please choose a reason first");
		return;
	}
	daform.submit();
}

</script>

Selected curve: <span style="font-weight:bold; font-size:16pt;"><?= "$fm_val $ip_val $dp_val $c_val" ?></span><br>

Please choose a reason below then click the Add button.<br><br>

<form name="form1" action="edit_curve.php" method="POST">
<input type="hidden" name="analysisid" value="<?=$analysisid?>">

<input type="button" onclick="checkTheForm();" value="Add">
<input type="button" onclick="window.location.href='edit_curve.php?analysisid=<?=$analysisid?>';" value="Cancel">
<table>

<?php

while ($list_row = mssql_fetch_assoc($list_result))
{
	print('<tr><td valign="top"><input type="radio" name="add_reasonid" value="' . $list_row['ReasonID'] . '"></td><td>');
	print($list_row['ReasonText'] . '</td></tr>');
}

?>

</table>
<input type="button" onclick="checkTheForm();" value="Add">
<input type="button" onclick="" value="Cancel">

</form>

<?php
print_footer();
?>