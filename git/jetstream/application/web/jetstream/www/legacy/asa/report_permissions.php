<?php

include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Mark Reports Viewable By Salespeople', true);

$reptable = 'reports';
//$reptable = 'reports';

if (isset($allowid))
{
	mssql_query("update $reptable set Salespeople = 1 where ReportID = $allowid");
}

if (isset($blockid))
{
	mssql_query("update $reptable set Salespeople = 0 where ReportID = $blockid");
}

?>

<script language="JavaScript">

function onBlockID(id)
{
	window.location.href = 'report_permissions.php?blockid=' + id;
}

function onAllowID(id)
{
	window.location.href = 'report_permissions.php?allowid=' + id;
}

</script>


<?php


	print('<table>');
	print('<tr>');
	print('<td width="100"><b>Name</b></td>');
	print('<td width="100"><b>View By<br>Salespeople</b></td>');
	print('</tr>');
	$sql="SELECT Name, Salespeople, ReportID from $reptable";
	$result=mssql_query($sql);
	$arrResults = array();
	while(($rw=mssql_fetch_assoc($result)))
	{
		$rw['Name'] = stripBrackets($rw['Name']);
		array_push($arrResults, $rw);
	}

	sort($arrResults);

	for($i=0; $i < count($arrResults); $i++)
	{
		$row = $arrResults[$i];
		$tdcolored = '<td';
		if ($row['Salespeople'] > 0)
			$tdcolored .= ' style="color:red; font-weight:bold;" ';
		$tdcolored .= '>';
		print('<tr>');
		print($tdcolored . $row['Name'] . '</td>');
//		print($tdcolored . '" <input type="checkbox" ');
		print('<td align="center"><input type="checkbox" ');

		if ($row['Salespeople'] > 0)
			print(' checked onchange="onBlockID(' . $row['ReportID'] . ')"');
		else
			print(' onchange="onAllowID(' . $row['ReportID'] . ')"');

		print('></td></tr>');
	}

	print('</table>');

function stripBrackets($strIn)
{
	$strIn = str_replace('{{', '', $strIn);
	$strIn = str_replace('}}', '', $strIn);
	return $strIn;
}



?>


<?php
print_footer();
?>
