<?php
include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">
<head>
<title>System Administration Utilities</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body link="blue" vlink="blue" onload="onLoad();">
<script language="JavaScript">

var arrUtils=Array();
arrUtils[0] = Array('Create company', 'createcompany.php');
arrUtils[1] = Array('Edit company name', 'editcompanyname_dir.php');
arrUtils[2] = Array('Disable company', 'disable.php');
arrUtils[3] = Array('Edit logo', 'editlogo.php');
arrUtils[4] = Array('Curves', 'curves.php');
arrUtils[5] = Array('Analysis', 'analysis.php');
arrUtils[6] = Array('Questions', 'questions.php');
arrUtils[7] = Array('Actions', 'actions.php');
arrUtils[8] = Array('Purge removed', 'purge.php');
arrUtils[9] = Array('EULA', 'eula.php');
arrUtils[10] = Array('Delete company', 'deletecompany.php');
arrUtils[11] = Array('Milestone labels', 'milestone_labels.php');
arrUtils[12] = Array('Superuser', 'editsuperuser.php');
arrUtils[13] = Array('Head count', 'personcount.php');
arrUtils[14] = Array('Last login', 'personlastuse.php');
arrUtils[15] = Array('Create reseller', 'createreseller.php');
arrUtils[16] = Array('Edit reseller', 'editreseller.php');
arrUtils[17] = Array('Delete reseller', 'deletereseller.php');
arrUtils[18] = Array('Category labels', 'category_labels.php');

function onLoad()
{

	arrUtils.sort();
//	autoFill();

	var selUtils = document.getElementById('selUtil');
	for (var i=0; i<arrUtils.length; i++)
	{
		var op = new Option;
		op.value = arrUtils[i][1];
		op.text = arrUtils[i][0];
		op.arrInd = i;
		selUtils.options.add(op, i);

	}
}

function autoFill()
{
	var runUtil = document.getElementById('runUtil');
	var srch = runUtil.value;
	var sel = document.getElementById('selUtil');
	sel.selectedIndex = -1;
	var answers = Array();
	var ans_count = 0;

	for (var i=0; i< arrUtils.length; i++)
	{

		if (srch.length > 0 &&(srch.toUpperCase() != arrUtils[i][0].slice(0, srch.length).toUpperCase()))
		{
			continue;
		}

		if(srch.length > 0)
		{
			answers[ans_count] = i;
			ans_count++;
		}

	}

	if (answers.length == 0) return;
	var sel = document.getElementById('selUtil');
	sel.selectedIndex=answers[0];

	if (answers.length == 1 && !(window.event.keyCode==8))
	{
		var ind = answers[0];
		runUtil.value=arrUtils[ind][0];
	}

}

function Go()
{

	var form = document.forms['form1'];
	var sel = document.getElementById('selUtil');
	if (sel.selectedIndex < 0)
	{
		alert('Nothing selected!');
		return;
	}
	var fname = sel.options[sel.selectedIndex].value;
	form.action = fname;
	form.submit();

}

function onSelChange(sel)
{
	if (sel.selectedIndex < 0) return;
	var ip = document.getElementById('runUtil');
	ip.value = arrUtils[sel.selectedIndex][0];
}

</script>
<form name="form1" method="post">

<table>
<tr><td colspan="2" align="center">ASA Utilities</td></tr>
<tr><td>Selected:&nbsp</td><td><input type="text" id="runUtil" onKeyUp="autoFill();"></td></tr>
<tr><td valign="top">Utilities:&nbsp</td><td><select size="20" id="selUtil" onchange="onSelChange(this);"></td></tr>
<tr><td colspan="2" align="center"><input type="button" onclick="Go();" value="Run Utility"></td></tr>
</table>



</form>
</body></html>

