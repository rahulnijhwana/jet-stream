<?php

include_once('_asa_utils.php');
include_once('mpsettings.php');

check_asa_login($PHP_SELF);
print_header('Edit OptionSet', true);

if(isset($_POST['btnSubmit'])){
	$editOptionSetId = $_POST['editedOptionSet'];
	$optionsArray = array();
	$query_str = "SELECT OptionID,OptionName FROM [Option] where OptionSetID = ".$editOptionSetId.";";
	$optionsSel = mssql_query($query_str);
	while($oldOptions =  mssql_fetch_assoc($optionsSel)){
		$optionsArray[] = array("OptionID"=>$oldOptions['OptionID'],"OptionName"=>$oldOptions['OptionName']);
	}

	foreach($optionsArray as $opts){
		$optFieldId = 'option_'.$opts['OptionID'];
		if(isset($_POST[$optFieldId]) && strlen(trim($_POST[$optFieldId])) > 0){
			//update options
			$sql = "UPDATE [option] SET OptionName ='".$_POST[$optFieldId] ."' WHERE OptionID =".$opts['OptionID'];
			$ok = mssql_query($sql);
		}else{
			//delete options
			$sql = "DELETE from [option] WHERE OptionID =".$opts['OptionID'];
			$ok = mssql_query($sql);
		}
	}
	$optionList = $_POST['optionList'];
	if(count($optionList) >0){
		foreach($optionList as $list){
			$optionName = $list;
			if(strlen(trim($optionName)) > 0){
				$insert_options_str = "insert into [Option] (CompanyID,OptionName,OptionSetID) values(-1,'$optionName',$editOptionSetId)";
				$result = mssql_query($insert_options_str);
				if ($result == false){
					exit('Error inserting Option data');
				}
			}
		}
	}
}

$query_str = "SELECT OptionSetID,OptionSetName FROM OptionSet where CompanyID = -1 ;";
$optionSet = mssql_query($query_str);

$optionSetDetails = array();
while($existingOptionSet =  mssql_fetch_assoc($optionSet)){
	$optArray = array();
	$query_str = "SELECT OptionID,OptionName FROM [Option] where OptionSetID = ".$existingOptionSet['OptionSetID']." ;";
	$options = mssql_query($query_str);
	while($existingOption =  mssql_fetch_assoc($options)){
		$optArray[] = array("OptionID"=>$existingOption['OptionID'],"OptionName"=>$existingOption['OptionName']);
	}
	$optionSetDetails[] = array("OptionSetId"=>$existingOptionSet['OptionSetID'],"OptionSetName"=>$existingOptionSet['OptionSetName'],"Options"=>$optArray);
}
?>

<script>
	function getOptions(optSel){
		document.getElementById('optionsdiv').innerHTML = '';
		<?php echo "var AllOptionSet = " . json_encode($optionSetDetails) . " ;\n";?> 
		try
		{
			AllOptionSet = eval(AllOptionSet);
		}
		catch(e)
		{
			AllOptionSet = JSON.decode(AllOptionSet);		
		}
		if(optSel.value != -1){
			for(i=0;i<AllOptionSet.length;i++){
				if(AllOptionSet[i].OptionSetId == optSel.value){
					createOptions(AllOptionSet[i].Options);
					break;
				}
			}
		}	
	}
	
	function createOptions(options){
		for(i=0;i<options.length;i++){
			newDiv=document.createElement('div');
			newDiv.innerHTML ='<input type="text" name="option_'+ options[i]['OptionID']+'"  id="option_'+ options[i]['OptionID']+'" value="'+ options[i]['OptionName'] +'"/>';
			document.getElementById('optionsdiv').appendChild(newDiv);
		}	
	}
	
	function addOptionList()
	{
		inner='';
		for(i=0;i<5;i++){
			newDiv=document.createElement('div');
			newDiv.innerHTML ='<input type="text" name="optionList[]" id="optionList[]" value=""/>';
			document.getElementById('optionsdiv').appendChild(newDiv);
		}
	}

	function saveOptionList(){
		if(document.getElementById('editedOptionSet').value == -1)
			return false;
			
		editoptionform.submit();
	}
</script>

<form enctype="multipart/form-data" name="editoptionform" action="editoptionset.php" method="post" onsubmit="javascript: return saveOptionList();">

<table>
	<tr>
		<td colspan = "2"><span id="errorlist" style="color:#CC0000;" ></span></td>
	</tr>
	<tr>
		<td>Select OptionSet :</td>&nbsp;&nbsp;
		<td>
			<select name="editedOptionSet" id="editedOptionSet" onchange="javascript:getOptions(this);"><option value="-1"></option>
			<?php
			foreach($optionSetDetails as $optset){
				echo '<option value="'.$optset['OptionSetId'].'">'.$optset['OptionSetName'].'</option>';
			}
			?>
			</select>
		</td>
	</tr>
	<tr>
		<td valign="top">Options :</td>
		<td>
			<div id='optionsdiv' name='optionsdiv'>
			</div>
		</td>
	</tr>
	
	<tr>
		<td>
			<input type="button" value="Add Options" onclick="javascript: addOptionList();"/>
		</td>
		<td>
			<input type="submit" value="Save Options" name="btnSubmit" />
		</td>
	</tr>
</table>
</form>