<?php

include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Edit Reseller', true);


if (isset($selectedreseller) && isset($ResellerName) && isset($BrandSel))
{
	if($ResellerName == "")
	{
		exit("Error: Reseller Name is blank.");
	}

	if (isset($LogoRequired)) $LogoRequired = 1;
	else $LogoRequired = 0;


	$has_img = (isset($_FILES['LogoImage']) && $_FILES['LogoImage']['size'] > 0);
	if (!$has_img)
	{
		$image_name = '';
	}
	else
	{
		$image_name = $_FILES['LogoImage']['name'];
		$ok=move_uploaded_file($_FILES['LogoImage']['tmp_name'], "../../current/shared/images/resellers/" . $image_name);
		if(!$ok)
			exit("Failed to move uploaded file");
	}
	$query_str =  "UPDATE Resellers set BrandID=$BrandSel, ResellerName='$ResellerName',";
	$query_str .= " UserLogoRequired=$LogoRequired, ResellerLogo='$image_name'";
	$query_str .= " where ResellerID=$selectedreseller;";
//print "$query_str<br>";

	$result = mssql_query($query_str);
	if ($result == false)
		exit('Error updating reseller data<br>');

//	exit("Reseller $ResellerName was successfully edited." . '<br>');
}

if (isset($delete_image))
{
	$ok = mssql_query("UPDATE Resellers SET ResellerLogo = '' where ResellerID = $selectedreseller");
/*
	if($ok)
		exit("Logo successfully deactivated");
	else
		exit("Error deactivating logo");
*/
}


/*
if (isset($selectedreseller))
{
	$selected_result = mssql_query("select * from Resellers where ResellerID = $selectedreseller");
	if($selected_result) $selected_row = mssql_fetch_assoc($selected_result);
	else exit("Invalid Reseller ID $selectedreseller");
}
*/
?>

<script language="JavaScript">
function onChangeReseller(id)
{
	if (id != -1)
		window.location.href = 'editreseller.php?selectedreseller=' + id;
}



function GoForIt()
{
	var theform = document.forms.form1;
	if (theform.ResellerName.value == '')
	{
		alert('"Reseller Name" is blank');
		return;
	}
	theform.submit();
}

</script>

<form enctype="multipart/form-data" name="form1" action="editreseller.php" method="post">
<select onchange="onChangeReseller(this.value)">
<option value="-1">--- Select a company ---</option>
<?php

$company_result = mssql_query("select * from Resellers order by ResellerName");

while ($row = mssql_fetch_assoc($company_result))
{
	print('<option value="' . $row['ResellerID'] . '" ');
	if (isset($selectedreseller) && $row['ResellerID'] == $selectedreseller)
		print('selected');
	print('>' . $row['ResellerName'] . '</option>');
}

?>
</select><br><br>
<?php
if (isset($selectedreseller))
	{
	$selected_result = mssql_query("select * from Resellers where ResellerID = $selectedreseller");
	if($selected_result)
		$selected_row = mssql_fetch_assoc($selected_result);
	else
		exit("Invalid Reseller ID $selectedreseller");

	$ResellerName = $selected_row['ResellerName'];
	print('<form  enctype="application/x-www-form-urlencoded" name="form1" action="editreseller.php" method="post">');
	print('<table><tr><td>New Name</td><td><input type="text" value="' . $ResellerName . '" name="ResellerName" size="50"></td></tr>');

	print('<tr><td>Logo Image Required</td><td><input type="checkbox" name="LogoRequired"');
	if($selected_row['UserLogoRequired'] == 1)
		print(' checked');
	print('></td></tr>');
	if (0 < strlen($selected_row['ResellerLogo']))
	{
		$current_image = $selected_row['ResellerLogo'];
		$img_path = "../shared/images/resellers/$current_image";
		print('<tr><td><b>Current Logo:</b></td>');

		print('<td><img src="' . $img_path . '"></td></tr>');

		print('<tr><td><a href="editreseller.php?selectedreseller=' . $selectedreseller . '&delete_image=' . $img_path . '" value="Delete Logo">Delete Logo</a></td></tr>');

	}
	else
		print('<tr><td><b>This reseller does not have a logo</b></td></tr>');

	print('<tr><td><input type="hidden" name="MAX_FILE_SIZE" value="1000000" /></td></tr>');
	print('<tr><td>New logo:</td><td><input type="file" name="LogoImage"></input></td></tr>');
	print('<tr><td><input type="hidden" name="selectedreseller" value="' . $selectedreseller . '"></td></tr>');
	print('<tr><td>Brand:</td><td>');
	$query_str = "SELECT BrandID, VendorName FROM Branding ORDER BY VendorName;";
	$result = mssql_query($query_str);
	if($result)
	{
		print "<SELECT NAME=\"BrandSel\">";
		while(($row = mssql_fetch_assoc($result)))
		{
			print "<OPTION VALUE=\"" . $row['BrandID'] . "\"";
			if($selected_row['BrandID'] == $row['BrandID'])
				print " selected";
			print ">";
			print $row['VendorName'] . "</OPTION>";
		}
		print "</SELECT>";
	}

	print('<tr><td align="center" colspan="2"><button type="button" onclick="GoForIt();">Submit</button></td></tr></table></form>');
	}
print_footer();
?>

