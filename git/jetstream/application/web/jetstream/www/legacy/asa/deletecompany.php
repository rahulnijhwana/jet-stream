<?php

include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Delete Company', true);



if (isset($selectedcompany))
{
	$selected_result = mssql_query("select * from company where CompanyID = $selectedcompany");
	$selected_row = mssql_fetch_assoc($selected_result);
}

if (isset($selectedcompany) && isset($dodelete) && $dodelete==true)
{

	$directory = $selected_row['DirectoryName'];
	$name = $selected_row['Name'];
	shell_exec("rm ../../$directory/*.*");
	shell_exec("rmdir ../../$directory");
	mssql_query("delete from company where CompanyID = $selectedcompany");
	print("$name has been permanently deleted");
	exit();
}


?>

<script language="JavaScript">

function onChangeCompany(id)
{
	window.location.href = 'deletecompany.php?selectedcompany=' + id;
}

function DoIt()
{
	if (!confirm("WARNING!\n\nYou are about to delete\n\nALL DATA\n\nfor this company\n\nPERMANENTLY.\n\n Do you want to continue?"))
		exit();

	var theform = document.forms.form1;
	var strAction = "deletecompany.php?selectedcompany=" + theform.CompanyID.value + "&dodelete=true";
	theform.action=strAction;
	theform.submit();
}


</script>


<select onchange="onChangeCompany(this.value)">
<option value="-1">--- Select a company ---</option>
<?php

$company_result = mssql_query("select * from company order by Name");

while ($row = mssql_fetch_assoc($company_result))
{
	print('<option value="' . $row['CompanyID'] . '" ');
	if (isset($selectedcompany) && $row['CompanyID'] == $selectedcompany)
		print('selected');
	print('>' . $row['Name'] . ' (' . $row['DirectoryName'] . ')</option>');

}
?>
</select><br><br>
<?php
if (isset($selectedcompany))
	{
	$CompanyName = $selected_row['Name'];
	print('<form  name="form1" action="deletecompany.php" method="post">');
	print('<table><tr><td>Company to Delete:  <b>' . $CompanyName . '</b></td></tr>');
	print('<tr><td><input type="hidden" name="CompanyID" value="' . $selectedcompany . '"></td></tr>');
	print('<tr><td align="center" colspan="2"><button type="button" onclick="DoIt();">Delete</button></td></tr></table></form>');
	}
print_footer();
?>

