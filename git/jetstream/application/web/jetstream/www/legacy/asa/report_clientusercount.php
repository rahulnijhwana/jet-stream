<?php
include('../reports/_report_utils.php');
include('../reports/TableUtils.php');
include('../reports/jim_utils.php');
include('../reports/ci_pdf_utils.php');

define("TRUE",1);
define("FALSE",0);

define(SHADE_MED, "MED_BLUE");
define(SHADE_LIGHT, "LIGHT_BLUE");


$report = CreateReport();
$report['left'] = 72/4;
$report['right'] = 10.75 * 72;
$report['bottom'] = 8.25 * 72;

$report['pageheight'] = 8.5*72;
$report['pagewidth'] = 11*72;


$p = PDF_new();
PDF_set_parameter($p, "license", PDFLIB_LICENSE_KEY);
PDF_open_file($p, "");

StartPDFReport($report, $p, "AAAA", "BBBB", "CCCCC");

SetLogo($report, "../images/asalogo.jpg", 10, 40, 72, 0);

BeginNewRow($report, RT_FREEFORM, BIG_FONTSIZE, 0.1);
// "ASA" and "M-Power" removed (for Systema)
AddFreeFormText($report, "Admin Client User Count", -1, "center", 2.7, 5.5);

$num_months = 6;

$vert_width = .325;
$counter_width = .3;
$client_name_width = 1.4;
$client_status_width = 1.0;

$shade_r = 0.90;
$shade_g = 0.90;
$shade_b = 0.90;

BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0);
AddColumn($report, "", "center", $counter_width, 0);
AddColumn($report, "", "center", $client_name_width, 0);
AddColumn($report, "", "center", $client_status_width, 0);

$this_month = strtotime("now");

for ($k = 0; $k < $num_months; ++$k)
{
	AddColumn($report, strftime(" %B- %y", $this_month), "center", $vert_width * 4, 0);
	SetItemValue($report, -1, -1, "border_left", 1);
	SetItemValue($report, -1, -1, "border_top", 1);
	SetItemValue($report, -1, -1, "border_bottom", 1);
	$this_month = strtotime("-1 month", $this_month);
	if ($k % 2)
		SetColColor($report, -1, -1, $shade_r, $shade_g, $shade_b);
}

SetItemValue($report, -1, -1, "border_right", 1);

BeginNewRow($report, RT_COL_HEADING, NORMAL_FONTSIZE, 0);
AddColumn($report, "", "center", $counter_width, 0);
AddColumn($report, "Client Name", "left", $client_name_width, 0);
SetItemValue($report, -1, -1, "valign", "bottom");
AddColumn($report, "Client Status", "left", $client_status_width, 0);
SetItemValue($report, -1, -1, "valign", "bottom");

for ($k = 0; $k < $num_months; ++$k)
{
	AddColumn($report, "SP", "center", $vert_width, 0);
	SetItemValue($report, -1, -1, "rotate", 1);
	SetItemValue($report, -1, -1, "border_left", 1);
	if ($k % 2)
		SetColColor($report, -1, -1, $shade_r, $shade_g, $shade_b);
	AddColumn($report, "MGR", "center", $vert_width, 0);
	SetItemValue($report, -1, -1, "rotate", 1);
	SetItemValue($report, -1, -1, "border_left", 1);
	if ($k % 2)
		SetColColor($report, -1, -1, $shade_r, $shade_g, $shade_b);
	AddColumn($report, "ADMIN", "center", $vert_width, 0);
	SetItemValue($report, -1, -1, "rotate", 1);
	SetItemValue($report, -1, -1, "border_left", 1);
	if ($k % 2)
		SetColColor($report, -1, -1, $shade_r, $shade_g, $shade_b);
	AddColumn($report, "TOTAL", "center", $vert_width, 0);
	SetItemValue($report, -1, -1, "rotate", 1);
	SetItemValue($report, -1, -1, "border_left", 1);
	if ($k % 2)
		SetColColor($report, -1, -1, $shade_r, $shade_g, $shade_b);
	SetItemValue($report, -1, -1, "border_right", 1);
}

for ($k = 0; $k < $num_months; ++$k)
{
	AddColumn($report, "", "right", $vert_width, 0);
	SetItemValue($report, -1, -1, "rotate", 1);
	SetItemValue($report, -1, -1, "border_left", 1);
	if ($k % 2)
		SetColColor($report, -1, -1, $shade_r, $shade_g, $shade_b);
	AddColumn($report, "", "right", $vert_width, 0);
	SetItemValue($report, -1, -1, "rotate", 1);
	SetItemValue($report, -1, -1, "border_left", 1);
	if ($k % 2)
		SetColColor($report, -1, -1, $shade_r, $shade_g, $shade_b);
	AddColumn($report, "", "right", $vert_width, 0);
	SetItemValue($report, -1, -1, "rotate", 1);
	SetItemValue($report, -1, -1, "border_left", 1);
	if ($k % 2)
		SetColColor($report, -1, -1, $shade_r, $shade_g, $shade_b);
	AddColumn($report, "", "right", $vert_width, 0);
	SetItemValue($report, -1, -1, "rotate", 1);
	SetItemValue($report, -1, -1, "border_left", 1);
	if ($k % 2)
		SetColColor($report, -1, -1, $shade_r, $shade_g, $shade_b);
	SetItemValue($report, -1, -1, "border_right", 1);
}

$result = mssql_query("select * from company order by Name");

$this_month = strtotime("now");
$this_month = strtotime(strftime("%m/1/%y", $this_month));
$this_month = strtotime("+1 month", $this_month);

$month_dates = array();
for ($k = 0; $k < ($num_months + 1); ++$k)
{
	$month_dates[$k] = strftime("%D", $this_month);
	$this_month = strtotime("-1 month", $this_month);
}

$company_counter = 1;
while ($row = mssql_fetch_assoc($result))
{
	BeginNewRow($report, RT_COL_TEXT, NORMAL_FONTSIZE, 0);
	SetRowValue($report, -1, "border_top", 1);
	AddColumnText($report, $company_counter, "", 0, 0);
	AddColumnText($report, $row['Name'], "", 0, 0);
	if ($row['Disabled'] == 1)
		AddColumnText($report, "Disabled", "", 0, 0);
	else
		AddColumnText($report, "Active", "", 0, 0);
	
	$companyid = $row['CompanyID'];
	
	for ($k = 0; $k < $num_months; ++$k)
	{
		$start_date = $month_dates[$k + 1];
		$end_date = $month_dates[$k];
		$when_str = "DateCreated < '$end_date' and (DateDeleted > '$end_date' or DateDeleted is null)";
		
		$people_result = mssql_query("select Level, SupervisorID from people where CompanyID = $companyid and SupervisorID <> -1 and ($when_str)");
		
		$sp_count = 0;
		$mgr_count = 0;
		$admin_count = 0;
		
		while ($people_row = mssql_fetch_assoc($people_result))
		{
			if ($people_row['SupervisorID'] == -3)
				$admin_count++;
			else if ($people_row['Level'] == 1)
				$sp_count++;
			else if (1 < $people_row['Level'])
				$mgr_count++;
		}
		
		AddColumnText($report, $sp_count, "", 0, 0);
		AddColumnText($report, $mgr_count, "", 0, 0);
		AddColumnText($report, $admin_count, "", 0, 0);
		AddColumnText($report, $sp_count + $mgr_count + $admin_count, "", 0, 0);
	}
	
	$company_counter++;
}

SetRowValue($report, -1, "border_bottom", 1);

PrintPDFReport($report, $p);
PDF_delete($p);

?>
