<?php

include_once('_asa_utils.php');
include_once('mpsettings.php');

check_asa_login($PHP_SELF);
print_header('Create OptionSet', true);

if(isset($_POST['btnSubmit'])){
//print_r($_POST);exit;
	$recordCount = 0;
	$optionSetName = $_POST['optionSetName'];
	$optionList = $_POST['optionList'];
	$errorCount = 0;
	if(count($optionList) < 1)
		exit('Error : Cannot save blank optionset.');
		
	$query_str = "SELECT * FROM OptionSet where CompanyID < 0;";
	$result = mssql_query($query_str);
	while($existingOptionSet =  mssql_fetch_assoc($result)){
		$recordCount = $recordCount+1;
		if(strtolower($optionSetName) == strtolower($existingOptionSet['OptionSetName'])){
			$errorCount = $errorCount+1;
			exit('Error OptionSet with name '.$optionSetName.' exists.');
		}
	}
	if($errorCount == 0){
		$insert_optionset_str = "insert into OptionSet (CompanyID,OptionSetName) values(-1,'$optionSetName')";
		$result = mssql_query($insert_optionset_str);

		if ($result == false){
			exit('Error inserting OptionSet data');
		}else{
			$query_str = "SELECT OptionSetID FROM OptionSet where CompanyID = -1 AND OptionSetName = '".$optionSetName."';";
			$result = mssql_query($query_str);
			$value = mssql_fetch_assoc($result);
			if(count($value) < 0)
				exit('Error inserting Option data');
			
			$optionSetId = $value[OptionSetID];
			foreach($optionList as $list){
				$optionName = $list;
				if(strlen(trim($optionName)) > 0){
					$insert_options_str = "insert into [Option] (CompanyID,OptionName,OptionSetID) values(-1,'$optionName',$optionSetId)";
					$result = mssql_query($insert_options_str);
					if ($result == false){
						exit('Error inserting Option data');
					}
				}
			}
		}
	}
}

?>

<script>
	function addOptionList()
	{
		inner='';
		for(i=0;i<5;i++){
			newDiv=document.createElement('div');
			newDiv.innerHTML ='<input type="text" name="optionList[]" id="optionList[]" value=""/>';
			document.getElementById('optionsdiv').appendChild(newDiv);
		}
	}

	function saveOptionList(){
		if(document.getElementById('optionSetName').value == ''){
			document.getElementById('errorlist').innerHTML = 'Please specify a name for the option list.';
			return false;
		}
		if(!document.getElementById('optionList[]')){
			document.getElementById('errorlist').innerHTML = 'Cannot create a blank optionset.';
			return false;
		}
		createoptionform.submit();
	}
</script>

<form enctype="multipart/form-data" name="createoptionform" action="createoptionset.php" method="post" onsubmit="javascript: return saveOptionList();">

<table>
	<tr>
		<td colspan = "2"><span id="errorlist" style="color:#CC0000;" ></span></td>
	</tr>
	<tr>
		<td>OptionSet Name</td><td><input type="text" name="optionSetName" id="optionSetName" size="20"></td>
	</tr>
	<tr>
		<td>Options</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
			<div id='optionsdiv' name='optionsdiv'>
			</div>
		</td>
	</tr>
	<tr>
		<td>
			<input type="button" value="Add Options" onclick="javascript: addOptionList();"/>
		</td>
		<td>
			<input type="submit" value="Save Options" name="btnSubmit" />
		</td>
	</tr>
</table>
</form>