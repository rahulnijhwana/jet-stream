<?php

include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Edit Company', true);

if (isset($EditCompanyName) && isset($selectedcompany))
{
	$sql="select * from company where CompanyID=$selectedcompany";
	$result=mssql_query($sql);
	$rec=mssql_fetch_assoc($result);
	$directory=$rec['DirectoryName'];
	$logoname=$rec['LogoName'];
	$has_img=(0<strlen($logoname));
	$BrandSel = $rec['BrandID'];
	$ResellerSel = $rec['ResellerID'];
	/*
	$query_str = "SELECT ResellerLogo from Resellers where ResellerID = $ResellerSel;";
	$result = mssql_query($query_str);
	if($result) {
		$temp_assoc = mssql_fetch_assoc($result);
		$ResellerLogo = $temp_assoc['ResellerLogo'];
	} else {
		$ResellerLogo = "asa.jpg";
	}
	*/
	$query_str = "SELECT BrandName, DomainName, TrademarkText from Branding where BrandID = $BrandSel;";
	$result = mssql_query($query_str);
	if($result) {
		$temp_assoc = mssql_fetch_assoc($result);
		$BrandName = $temp_assoc['BrandName'];
		$DomainName = $temp_assoc['DomainName'];
		$TrademarkText = $temp_assoc['TrademarkText'];
	} else {
		// failed, assume ASA/M-Power
		$BrandName = "M-Power";
		$DomainName = "mpasa.com";
	}

	if (!$has_img) //check for logofile
	{
		$files = shell_exec("ls ../../$directory/logo.*");
		$files = str_replace("\n", ',', $files);
		$files = explode(',', $files);
		if (count($files) > 1)
		{
			$has_img = true;
			$logoname = $files[0];
		}
	}

//Now, re-create index.html
	/*
	$templateStr = '';
	if ($has_img)
		$templateStr = file_get_contents("index_template.html");
	else
		$templateStr = file_get_contents("index_template_no_logo.html");

	$FullCompanyName = $EditCompanyName;
	$company_id = $selectedcompany;

	$templateStr = str_replace("{COMPANYNAME}", $FullCompanyName, $templateStr);
	$templateStr = str_replace("{COMPANYID}", $company_id, $templateStr);

	// 2-21-05 need to deal with reseller/branding issues
	$templateStr = str_replace("{BRANDNAME}", $BrandName, $templateStr);
	$templateStr = str_replace("{DOMAINNAME}", $DomainName, $templateStr);
	$templateStr = str_replace("{RESELLERLOGO}", $ResellerLogo, $templateStr);
	$templateStr = str_replace("{TRADEMARKTEXT}", $TrademarkText, $templateStr);
	if ($has_img)
	{
		$temp_image_name = $logoname;
		$temp_image_name = strtolower($temp_image_name);
		if (strpos($temp_image_name, '.jpg') !== FALSE || strpos($temp_image_name, '.jpeg') !== FALSE)
			$temp_image_name = 'logo.jpg';
		if (strpos($temp_image_name, '.gif') !== FALSE)
			$temp_image_name = 'logo.gif';
		if (strpos($temp_image_name, '.bmp') !== FALSE)
			$temp_image_name = 'logo.bmp';
		if (strpos($temp_image_name, '.png') !== FALSE)
			$temp_image_name = 'logo.png';
		$templateStr = str_replace("{LOGO}", $temp_image_name, $templateStr);
		$logoname = $temp_image_name;
	}

	$fp = fopen("../../$directory/index.html", "w");
	if ($fp == FALSE)
		exit("Could not create index file");
	fwrite($fp, $templateStr);
	fclose($fp);
	*/

	shell_exec("mv ../../$directory/index.html ../../$directory/index.html.bak");
	shell_exec("cp -f index_template_new.php ../../$directory/index.php");
	shell_exec("chmod 775 ../../$directory");
	shell_exec("chmod 775 ../../$directory/*");

	if ($NewDir == $rec['DirectoryName']) $NewDir = null;
	if (isset($NewDir))
	{
//Check for existence of newdir
		$files = shell_exec("ls ../../$NewDir");
		$files = str_replace("\n", ',', $files);
		$files = explode(',', $files);
		if (count($files) > 1)
		{
			print('<span style="color:red;font-style:italic;"><b>' . $NewDir . ' </b>already exists.  Cannot rename directory<br><br></b></span><br><br>');
			$NewDir = null;
		}
		else
		{
			shell_exec("mv ../../$directory ../../$NewDir");
		}
	}
	$msg = "";
	$update_list="";
	if ($EditCompanyName != $rec['Name'])
	{
		$msg = "Company name has been updated to '$EditCompanyName'";
		$update_list = " Name='$EditCompanyName'";

	}
	if (isset($NewDir) && $NewDir != $rec['DirectoryName'])
	{
		if (strlen($msg)) $msg .="; ";
		$msg .= "Directory Name has been updated to '$NewDir'";
		if (strlen($update_list)) $update_list .= ",";
		$update_list .= " DirectoryName='$NewDir'";

	}
	
	if (isset($_REQUEST['ResellerSel']))
	{
		if (strlen($update_list))
			$update_list .= ",";
		$update_list .= " ResellerID=".$_REQUEST['ResellerSel'];
	}
	
	if(isset($Demo)) $Demo=1;
	else $Demo = 0;
	if (strlen($update_list)) $update_list .= ",";
	$update_list .= " Demo=$Demo ";
	
	if(isset($NoteAlert)) $NoteAlert=1;
	else $NoteAlert=0;
	if (strlen($update_list)) $update_list .= ",";
	$update_list .= " NoteAlert=$NoteAlert ";

	$mpower = 0;
	$slipstream = 0;
	
	if(isset($system) && ($system == 1 || $system == 3)) $mpower = 1;
	if(isset($system) && ($system == 2 || $system == 3)) $slipstream = 1;	

	$update_list .= " , Mpower=$mpower, ";
	$update_list .= " Slipstream=$slipstream ";
	
// new
	if(isset($DemoData)) $DemoData=1;
	else $DemoData = 0;
	if (strlen($update_list)) $update_list .= ",";
	$update_list .= " DemoData=$DemoData ";
	
// end new
	if(isset($class))
	{
		if (strlen($update_list)) $update_list .= ",";
		$update_list .= " Class=$class ";
	}

//We used to exit here if !strlen($msg)	Instead, we now have to update incase Demo has changed
	if(strlen($msg))
	{
		print('<span style="color:red;font-style:italic;">' . $msg . '</span><br><br>');
	}

/*
	$sql = "UPDATE company SET Name = '$EditCompanyName'";
	if($has_img) $sql .= ", LogoName='$logoname'";
	if(isset($NewDir)) $sql .= ", DirectoryName='$NewDir'";
 	$sql .= ", Demo=$Demo";
	$sql .= " WHERE CompanyID = $selectedcompany";
*/
	$sql = "UPDATE company SET $update_list WHERE CompanyID = $selectedcompany";
	//echo $sql;
	
	$ok = mssql_query($sql);
	if($ok)
		exit("DB Success");
	else
		exit("DB Error: sql=$sql");
}



if (isset($selectedcompany))
{
	$selected_result = mssql_query("select * from company where CompanyID = $selectedcompany");
	$selected_row = mssql_fetch_assoc($selected_result);
}
?>

<script language="JavaScript">

function onChangeCompany(id)
{
	if (id != -1)
		window.location.href = 'editcompanyname_dir.php?selectedcompany=' + id;
}

function getNewName()
{
	var newname = document.getElementByID("NewName");
	return newname.value;
}

function DoIt()
{

	var theform = document.forms.form1;
	if (theform.EditCompanyName == '')
	{
		alert('"Company Name" is blank');
		return;
	}
	theform.submit();
}
</script>

<select onchange="onChangeCompany(this.value)">
<option value="-1">--- Select a company ---</option>
<?php

$company_result = mssql_query("select * from company order by Name");

while ($row = mssql_fetch_assoc($company_result))
{
	print('<option value="' . $row['CompanyID'] . '" ');
	if (isset($selectedcompany) && $row['CompanyID'] == $selectedcompany)
		print('selected');
//	print('>' . $row['Name'] . '</option>');
	print('>' . $row['Name'] . ' (' . $row['DirectoryName'] . ')</option>');

}
?>
</select><br><br>
<?php
function getClassRadioHTML($class, $classval)
{
	switch($class)
	{
		case 0:
			$label = "Customer";break;
		case 1:
			$label = "Trainer/Affiliate";break;
		case 2:
			$label = "Nontrainer Reseller";break;
		case 3:
			$label = "Other";break;
	}
	$strchk = ($class == $classval ? 'checked' : '');
	return "<tr><td><input type=\"radio\" name=\"class\" value=\"$class\" $strchk>$label</td></tr>";
}

if (isset($selectedcompany))
{
	$CompanyName = $selected_row['Name'];
	$DirectoryName = $selected_row['DirectoryName'];
	print('<form  name="form1" enctype="multipart/form-data" action="editcompanyname_dir.php" method="post">');
	print('<table><tr><td>New Name</td><td><input type="text" value="' . $CompanyName . '" name="EditCompanyName" size="50"></td></tr>');

	print('<tr><td>New Directory</td><td><input type="text" name="NewDir" value="' . $DirectoryName . '"></td></tr>');

	print('<tr><td>Reseller</td><td>');
	$query_str = "SELECT BrandID, VendorName FROM Branding ORDER BY BrandID;";
	$result = mssql_query($query_str);
	if($result) {
		// use a single <SELECT> with <OPTGROUP>s instead of trying to use
		// radio buttons and multiple <SELECT>s.
		print('<SELECT NAME="ResellerSel">');
		$rsquery_str = "SELECT ResellerID, ResellerName, UserLogoRequired from Resellers ORDER BY ResellerName;";
		$rsresult = mssql_query($rsquery_str);
		if($rsresult)
		{
			$rsassoc = mssql_fetch_assoc($rsresult);
			while($rsassoc)
			{
				// We can derive the BrandID from the Resellers table.
				print "<OPTION VALUE=\"" . $rsassoc['ResellerID'] . "\"";
				if($rsassoc['UserLogoRequired'])
				{
					print " TITLE=\"Logo Image Required\"";
				}
				if($rsassoc['ResellerID'] == $selected_row['ResellerID'])
				{
					print " selected";
				}
				print ">" . $rsassoc['ResellerName'] . "</OPTION>";
				$rsassoc = mssql_fetch_assoc($rsresult);
			}
		}
		print('</SELECT>');
	}
	print('</td></tr>');

	print('<tr><td>Assign</td><td>');
	
	$mpower = ($selected_row['Mpower'] == 1) ? 1 : 0;
	$slipstream = ($selected_row['Slipstream'] == 1) ? 1 : 0;
	
	if ($mpower == 1 && $slipstream == 1) $both_checked = 'checked';
	else if ($mpower == 0 && $slipstream == 1) $slipstream_checked = 'checked';
	else $mpower_checked = 'checked';
		
	print('<input type="radio" name="system" value="1" ' . $mpower_checked . '>M-Power</input>');
	print('&nbsp;&nbsp;&nbsp;&nbsp;');
	print('<input type="radio" name="system" value="2" ' . $slipstream_checked . '>Slipstream</input>');
	print('&nbsp;&nbsp;&nbsp;&nbsp;');
	print('<input type="radio" name="system" value="3" ' . $both_checked . '>Both</input>');
	print('</td></tr>');

	print('<tr><td>Demo</td><td><input type="checkbox"');
	if ($selected_row['Demo'] > 0)
		print (' checked');
	print(' name="Demo"></td></tr>');
	
	//New demodata field
	print('<tr><td>DemoData</td><td><input type="checkbox"');
	if ($selected_row['demodata'] > 0)
		print (' checked');
	print(' name="DemoData"></td></tr>');

	print('<tr><td>Note Alert</td><td><input type="checkbox"');
	if ($selected_row['NoteAlert'] > 0)
		print (' checked'); 
	print(' name="NoteAlert"></td></tr>');

/*
	print('<tr><td><input type="radio" name="class" value="0" checked>Customer</td></tr>');
	print('<tr><td><input type="radio" name="class" value="1">Trainer/Affilliate</td></tr>');
	print('<tr><td><input type="radio" name="class" value="2">Nontrainer Reseller</td></tr>');
	print('<tr><td><input type="radio" name="class" value="3">Other</td></tr>');
*/
	for($i = 0; $i< 4; $i++)
		print(getClassRadioHTML($i, $selected_row['Class']));

	print('<tr><td><input type="hidden" name="selectedcompany" value="' . $selectedcompany . '"></td></tr>');

	print('<tr><td align="center" colspan="2"><button type="button" onclick="DoIt();">Submit</button></td></tr></table></form>');
}
print_footer();
?>
