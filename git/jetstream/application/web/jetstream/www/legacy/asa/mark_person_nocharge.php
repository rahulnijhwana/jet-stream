<?php

include_once('_asa_utils.php');
check_asa_login($PHP_SELF);
print_header('Mark People As No Charge', true);

if (isset($chargeid))
{
	mssql_query("update Logins set NoCharge = 0 where ID = $chargeid");
}

if (isset($nochargeid))
{
	mssql_query("update Logins set NoCharge = 1 where ID = $nochargeid");
}

?>

<script language="JavaScript">

function onChargeID(selcompany, id)
{
	window.location.href = 'mark_person_nocharge.php?selectedcompany=' + selcompany + '&chargeid=' + id;
}

function onNoChargeID(selcompany, id)
{
	window.location.href = 'mark_person_nocharge.php?selectedcompany=' + selcompany + '&nochargeid=' + id;
}

</script>

<span id="companyTotals" style="font-weight:bold"></span><br><br>

<select onchange="onChangeCompany(this.value)">
<option value="-1">--- Select a company ---</option>

<?php
$sql = "select * from company WHERE ";
$sql .= "((company.Demo=0) or (company.Demo IS NULL)) and (company.Disabled=0) ";
$sql .= "order by Name";
$company_result = mssql_query($sql);
while ($row = mssql_fetch_assoc($company_result))
{
	print('<option value="' . $row['CompanyID'] . '" ');
	if (isset($selectedcompany) && $row['CompanyID'] == $selectedcompany)
		print('selected');
	print('>' . $row['Name'] . '</option>');
}
?>
</select><br><br>

<?php
if (isset($selectedcompany))
{
	print('<input type="button" value="Finished" onclick="window.location.href = \'menu.php\';">');
	print('<table>');
	print('<tr><td></td><td align="center" colspan="2"><b>USERS</b></td><td></td></tr>');
	print('<tr>');
	print('<td width="100"><b>Level</b></td>');
	print('<td width="100"><b>Last<br>Name</b></td>');
	print('<td width="100"><b>First<br> Name</b></td>');
//	print('<td width="100"><b>Alias</b></td>');
	print('<td width="100"><b>No<br>Charge</b></td>');
	print('</tr>');
	$sql="SELECT Logins.ID as RecID, Logins.Name, people.LastName, people.FirstName, people.SupervisorID, Logins.PersonID, Logins.LoginID, ";
	$sql .= "Logins.CompanyID, people.[Level] as Lv, Logins.NoCharge ";
	$sql .= "FROM Logins INNER JOIN people ON Logins.PersonID = people.PersonID ";
	$sql .= "WHERE     (Logins.CompanyID = $selectedcompany) ";
	$sql .= "AND ((people.Deleted=0) or (people.Deleted IS NULL)) ";
	$sql .= "AND (NOT (people.SupervisorID = -3)) ";
	$sql .= "ORDER BY people.[Level] DESC, people.LastName, people.FirstName";
	$result=mssql_query($sql);
	$arrResults = array();
	$sales=0;
	$mgrs=0;
	$aliases=0;
	$unassigned=0;
	while(($rw=mssql_fetch_assoc($result)))
	{
		if($rw['NoCharge'] == 0)
		{
			if($rw['PersonID'] != $rw['LoginID'])
				$aliases++;
			elseif($rw['SupervisorID'] == -1)
				$unassigned++;
			elseif($rw['Lv'] > 1) $mgrs++;
			else $sales++;
		}
		array_push($arrResults, $rw);
	}
	$total = $sales + $mgrs + $aliases;
	$strTotals = "Total Charged: $total Managers: $mgrs  Salesmen: $sales  Aliases: $aliases  (Unassigned: $unassigned)";
	print('<script language="JavaScript">');
	print('var title=document.getElementById("companyTotals");');
	print("title.outerText=\"$strTotals\";");
	print('</script>');

	for($i=0; $i < count($arrResults); $i++)
	{
		$row = $arrResults[$i];
		if($row['SupervisorID'] == -1) continue;
		$tdcolored = '<td';
		if ($row['NoCharge'] > 0)
			$tdcolored .= ' style="color:red; font-weight:bold;" ';
		$tdcolored .= '>';
		$isAlias = ($row['PersonID'] != $row['LoginID']);
		if($isAlias) continue;
		print('<tr>');
		print($tdcolored . $row['Lv'] . '</td>');
		print($tdcolored . $row['LastName'] . '</td>');
		print($tdcolored . $row['FirstName'] . '</td>');
/*
		if ($isAlias)
			print($tdcolored . $row['Name'] . '</td>');
		else
			print('<td></td>');
*/
		print($tdcolored . '<input type="checkbox" ');
		if ($row['NoCharge'] > 0)
			print(' checked onchange="onChargeID(' . $selectedcompany . ',' . $row['RecID'] . ')"');
		else
			print(' onchange="onNoChargeID(' . $selectedcompany . ',' . $row['RecID'] . ')"');

		print('></td></tr>');
	}


	print('</table>');
	print('<table>');
	print('<tr><td></td><td align="center" colspan="2"><b>ALIASES</b></td><td></td></tr>');

	print('<tr>');
	print('<td width="100"><b>Level</b></td>');
	print('<td width="150"><b>Alias<br>Name</b></td>');
	print('<td width="150"><b>User<br>Name</b></td>');
	print('<td width="100"><b>No<br>Charge</b></td>');
	print('</tr>');

//Now, display the aliases
	for($i=0; $i < count($arrResults); $i++)
	{
		$row = $arrResults[$i];
		$tdcolored = '<td';
		if ($row['NoCharge'] > 0)
			$tdcolored .= ' style="color:red; font-weight:bold;" ';
		$tdcolored .= '>';
		$isAlias = ($row['PersonID'] != $row['LoginID']);
		if(!$isAlias) continue;
		print('<tr>');
		print($tdcolored . $row['Lv'] . '</td>');
		print($tdcolored . $row['Name'] . '</td>');
		print($tdcolored . $row['LastName'] . ', ' . $row['FirstName'] . '</td>');

		print($tdcolored . '<input type="checkbox" ');
		if ($row['NoCharge'] > 0)
			print(' checked onchange="onChargeID(' . $selectedcompany . ',' . $row['RecID'] . ')"');
		else
			print(' onchange="onNoChargeID(' . $selectedcompany . ',' . $row['RecID'] . ')"');

		print('></td></tr>');
	}
	print('</table>');


}
?>

<script language="JavaScript">

function onChangeCompany(id)
{
	if (id != -1)
		window.location.href = 'mark_person_nocharge.php?selectedcompany=' + id;
}

/*
function DoIt()
{

	var theform = document.forms.form1;
	var strAction = "mark_person_nocharge.php?selectedcompany=" + theform.CompanyID.value;

	theform.action=strAction;

	theform.submit();
}
*/

</script>

<?php
print_footer();
?>
