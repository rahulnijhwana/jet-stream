<?php
/**
 * @package Shared
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');
require_once(BASE_PATH . '/include/class.DbConnManager.php');
require_once(BASE_PATH . '/include/class.SqlBuilder.php');

SessionManager::Init();
SessionManager::Validate();

//print_r($_SESSION);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
<TITLE>Milestone Checklist</TITLE>

<link rel="stylesheet" type="text/css" href="../css/sharedstyles.css,tabbed_pane.css">
<script language="JavaScript" src="../javascript/windowing.js,utils.js,pageformat.js,tabbed_pane.js"></script>

</HEAD>

<script language="JavaScript">


g_highlightMissing = Windowing.dropBox.highlightMissing;
g_redchecks = new Array();

<?php
include_once('../data/_base_utils.php');
include_once('../data/_query_utils.php');
include_once('../data/get_company.php');
$companyList="('$mpower_companyid')";
include_once('../data/get_submilestones.php');

?>
//g_submilestones = window.opener.g_submilestones;
//g_company = window.opener.g_company;
g_the_only_company = g_company[0];

isIE = (navigator.appName.indexOf("Microsoft") != -1) ? true : false;

g_location = '<?php echo $location; ?>';
g_location_id = <?php echo $location_id; ?>;
g_companyid = <?php echo $companyid; ?>;
g_real_parent = Windowing.dropBox.editOppWin;

g_req1Used = (g_company[0][g_company.Requirement1used] == '1')
g_req2Used = (g_company[0][g_company.Requirement2used] == '1')

checkids = new Array();

function markDirty(ind)
{
	checkids[ind][1] = '1';
}

g_MileNames = new Array('Requirement1','Person','Need','Money','Time','Requirement2');



function onOK()
{
	var allset;
	var checkcnt=0;
	for (var m = 0; m < 6; ++m)	// check each milestone
	{
		if (!m && !g_req1Used) continue;
		if (m==5 && !g_req2Used) continue;
		allset = true;
		for (var k = 0; k < g_submilestones.length; ++k)  // look at all submilestones
		{
			var sub=g_submilestones[k];
			if (sub[g_submilestones.Enable] != '1') continue;
//Added to filter submilestone questions for g_showaffiliates
			if (sub[g_submilestones.CompanyID] != g_companyid) continue;
			if (sub[g_submilestones.Location] == m)	// to find ones that belong to this milestone
			{
				if(g_submilestones[k][g_submilestones.IsHeading] == '1') continue;

				var mand=sub[g_submilestones.Mandatory];
				var subid=checkids[checkcnt][0];
				var inpobj = document.getElementById('sub' + checkids[checkcnt][0]);
				var dirty = checkids[checkcnt][1] == '1';
				checkcnt++;
				if (g_submilestones[k][g_submilestones.CheckBox] == '1')
				{
					if (mand>0 && !inpobj.checked)
						allset = false;
					if(dirty) 		// only update dirty
						g_real_parent.setSubAnswer(subid, inpobj.checked ? 1 : 0);
				}
				else
				{
					if (mand>0)
					{
						if (g_submilestones[k][g_submilestones.YesNo] == '1')
						{
							//alert(m+" "+g_submilestones[k][g_submilestones.Prompt]+" "+g_submilestones[k][g_submilestones.RightAnswer]+" "+inpobj.value);
							if (g_submilestones[k][g_submilestones.RightAnswer] != inpobj.value)
							{
//								alert(m+": "+g_submilestones[k][g_submilestones.Prompt]+' '+g_submilestones[k][g_submilestones.RightAnswer]+' '+inpobj.value);
								allset=false;
							}
						}
						else if (g_submilestones[k][g_submilestones.DropDown] == '1')
						{
							if (inpobj.value=='' || inpobj.value=='-1') allset=false;
//Ok, they chose from the dropdown.  But did they choose right?
//							if(allset)
							else
							{
								for (var q=0; q < 20; q++)
								{
									if(g_submilestones[k][g_submilestones['DropChoice'+q]] == inpobj.value)
									{
										//alert('found ans ' + ans);
										if(g_submilestones[k][g_submilestones['DChoiceOK'+q]] != '1')
											allset=false;
										break;
									}
								}
							}
						}
						else if (inpobj.value=='')
						{
							allset=false;
						}
					}
					if (dirty)
					{
						if(g_submilestones[k][g_submilestones.AlphaNum] == '1')
						{
							var strVal = inpobj.value;
							var strOK = escape(strVal);
							g_real_parent.setSubAnswer(subid,strOK);
						}
						else
						{
							if (g_submilestones[k][g_submilestones.Calendar] == '1')
							{
								if(inpobj.value!='' && !Dates.normalize(inpobj.value))
								{
									alert('Please enter a valid date for ' + g_submilestones[k][g_submilestones.Prompt]);
									inpobj.focus();
									return;
								}
							}
							g_real_parent.setSubAnswer(subid,inpobj.value);
						}
					}
				}
			}
		}
		//alert(m+': '+allset);
		if (!g_real_parent.g_Transactional || !g_real_parent.document.getElementById("transactional").checked) g_real_parent.setMilestone(g_MileNames[m], allset ? 1 : 0);
	}
	if (Windowing.dropBox.callBack)
	{
		var callBack=Windowing.dropBox.callBack;
		Windowing.dropBox.callBack=null;
		callBack();
	}
	if (hInterval)
		clearInterval(hInterval);
	window.close();
}

function onCancel()
{
	window.close();
}

function wr(txt)
{
	document.write(txt);
}

</script>

<BODY style="background-color:white; font-family: tahoma, arial, sans-serif;font-size: 12px;" onload="loaded();">
<form name="form1">


<script language='javascript'>
//Netscape isn't smart enough to know whwere to put these buttons --
if(!isIE)
{
	wr('<table width="100%"><tr><td width="100%" align="right">');
	wr('<button class="command" style="width:80px;" onclick="onOK()">OK</button>&nbsp;&nbsp;&nbsp;<button class="command" style="width:80px;" onclick="onCancel()">Cancel</button>');
	wr('</td></tr></table>');
}

//var allElems = new Object();
g_divs = new Array();		// list of all the div suffixes

var hInterval = null;

function doRedCheck()
{
	var mscompleted = new Array(true, true, true, true, true, true);
	for (var k = 0; k < g_redchecks.length; ++k)
	{
		var tempredcheck = g_redchecks[k];
		var sub = tempredcheck.subms;
		var inputelem = document.getElementById(tempredcheck.inputid);
		var labelelem = document.getElementById(tempredcheck.labelid);
		if (sub[g_submilestones.CheckBox] == '1')
			labelelem.style.color = inputelem.checked ? "black" : "red";
		else if (sub[g_submilestones.YesNo] == '1')
			labelelem.style.color = (inputelem.value == sub[g_submilestones.RightAnswer]) ? "black" : "red";
		else if (sub[g_submilestones.DropDown] == '1')
		{
			if (sub[g_submilestones.Prompt] == 'Decision-Making Influence of Principal Detractor')
				status = inputelem.tagName;
			labelelem.style.color = (inputelem.value != -1) ? "black" : "red";
		}
		else
			labelelem.style.color = (inputelem.value.trim() != '') ? "black" : "red";

		if (mscompleted[sub[g_submilestones.Location]] && labelelem.style.color == "red")
			mscompleted[sub[g_submilestones.Location]] = false;
	}

	for (var k = 0; k < mscompleted.length; ++k)
	{
		var mslabel = document.getElementById("mslabel_"+k);
		if (mslabel)
			mslabel.style.color = mscompleted[k] ? "black" : "red";
	}
}

function loaded()
{
	window.focus();
	var tabbedPane = new JSTabbedPane('../images/');
	for (var k = 0; k < g_divs.length; ++k)
	{
		var strtitle = '';
		switch (g_divs[k])
		{
			case 0:
				strtitle = g_the_only_company[g_company.Requirement1];
				break;
			case 1:
				strtitle = g_the_only_company[g_company.MS2Label] == '' ? 'Person' : g_the_only_company[g_company.MS2Label];
				break;
			case 2:
				strtitle = g_the_only_company[g_company.MS3Label] == '' ? 'Need' : g_the_only_company[g_company.MS3Label];
				break;
			case 3:
				strtitle = g_the_only_company[g_company.MS4Label] == '' ? 'Money' : g_the_only_company[g_company.MS4Label];
				break;
			case 4:
				strtitle = g_the_only_company[g_company.MS5Label] == '' ? 'Time' : g_the_only_company[g_company.MS5Label];
				break;
			case 5:
				strtitle = g_the_only_company[g_company.Requirement2];
				break;
		}
		tabbedPane.addTabFromElem('<b><a href="javascript:void(0);" class="PointerElem" id="mslabel_'+g_divs[k]+'">' + strtitle + '</a></b>', document.getElementById('divQ' + g_divs[k]));
	}

	tabbedPane.addToElement(document.getElementById('divTab'));

	var t=g_location_id;	// get starting tab
	if (!g_req1Used) t--;

	tabbedPane.selectTab(t);
//	tabbedPane.onTabChanged = onTabChange;

	//var divEnd = document.getElementById('divEnd');
	//if (divEnd.offsetTop && window.resizeTo)
	//	window.resizeTo(600, divEnd.offsetTop + 50);

	if (g_highlightMissing)
	{
		doRedCheck();
		hInterval = setInterval("doRedCheck();", 500);
	}
}

</script>

<div id="divTab" style="height:200px;"></div>
<center>
<div class="TabbedPane_SourceDiv">

<script language="JavaScript">



var calCheck=-1;
function show_calendar(targetName, chk)
{
	document.body.style.cursor = 'wait';
	Windowing.dropBox.calendarTarget = document.getElementById(targetName);
	Windowing.dropBox.onCalendarEdited=setCalFieldDirty;
	calCheck = chk;
	var win = Windowing.open('../shared/calendar.html', 320, 600, 'calWindow', true);
	win.focus();
	document.body.style.cursor = 'auto';
}


function setCalFieldDirty()
{
	markDirty(calCheck);
}

for (m = 0; m < 6; ++m)
{
	if (!m && !g_req1Used)
		continue;
	if (m==5 && !g_req2Used)
		continue;
	g_divs[g_divs.length]=m;
	wr('<div id="divQ'+m+'"><table width=100%>');


	for (var k = 0; k < g_submilestones.length; ++k)
	{
		var sub=g_submilestones[k];
		if (sub[g_submilestones.Location] == m)
		{
			if (sub[g_submilestones.CompanyID] != g_companyid)
				continue;
			if (sub[g_submilestones.Enable] != '1')
				continue;
			var tempid = 'sub' + sub[g_submilestones.SubID];
			var mand=sub[g_submilestones.Mandatory];
			if(sub[g_submilestones.IsHeading] == '1')
			{
				wr('<tr><td colspan=2 align="left" style="font-style:italic;font-weight:bold;"><br>' + sub[g_submilestones.Prompt]+'</td>');
				continue;
			}

			wr('<tr><td align="right" id="label_'+tempid+'" style="font-size:14px;">'+sub[g_submilestones.Prompt]);
			if (mand == 0 || mand=='')
				wr(' (Optional)');
			else if (g_highlightMissing)
			{
				var tempredcheck = new Object();
				tempredcheck.labelid = "label_"+tempid;
				tempredcheck.inputid = tempid;
				tempredcheck.subms = sub;
				g_redchecks[g_redchecks.length] = tempredcheck;
			}
			wr('</td><td align="left">');
			var answer=g_real_parent.getSubAnswer(sub[g_submilestones.SubID]);
			if (sub[g_submilestones.CheckBox] == '1')
			{
				document.write('<input type="checkbox" id="' + tempid + '" onclick="markDirty(' + checkids.length + ')"');
				//alert(g_submilestones[k][g_submilestones.SubID]);

				if (answer == 1)
				{
					wr(' checked');
					//alert("checked");
				}
				wr('>');
			}
			else if (sub[g_submilestones.YesNo] == '1')
			{
				wr('<select id="' + tempid + '" onchange="markDirty(' + checkids.length + ')">');
				wr('<option value="-1"></option>');
				wr('<option value="1"');
				if (answer == "1")
					wr(' SELECTED');
				wr('>Yes</option>');
				wr('<option value="0"');
				if (answer == "0")
					wr(' SELECTED');
				wr('>No</option>');
				if (mand == "2")
				{
					wr('<option value="2"');
					if (answer == "2")
						wr(' SELECTED');
					wr('>N/A</option>');
				}
				wr('</select>');
			}
			else if (g_submilestones[k][g_submilestones.DropDown] == '1')
			{
				wr('<select id="' + tempid + '" onchange="markDirty(' + checkids.length + ')">');
				wr('<option value="-1"></option>');
				for (var g = 1; g < 21; ++g)
				{
					var temp_choice = g_submilestones[k][g_submilestones['DropChoice'+g]];
					if (temp_choice && temp_choice != '')
					{
						wr('<option value="'+temp_choice+'"');
						if (answer == temp_choice)
							wr(' SELECTED');
						wr('>'+temp_choice+'</option>');
					}
				}
				if (mand == 2)
				{
					wr('<option value="2"');
					if (answer == 2)
						wr(' SELECTED');
					wr('>N/A</option>');
				}
				wr('</select>');
			}
			else
			{
				wr('<input type="text" onchange="markDirty(' + checkids.length + ')" onkeypress="markDirty(' + checkids.length + ')" id="' + tempid + '"');
//				wr(' onchange="markDirty(' + checkids.length + ')"');
//				wr(' onchange="alert(\'what was that?\')"');

				var length=g_submilestones[k][g_submilestones.Length];
				if (g_submilestones[k][g_submilestones.Calendar] == 1) length = 10;
					
				if (length > 0 && length < 51)
					wr(' size="' +length+ '" maxlength="' +length+ '"');
				else
					wr(' size="50" maxlength="50"');
				//if ($anscount > 0)
				wr(' value="' + unescape(answer) + '"');
				wr('>');


				if (g_submilestones[k][g_submilestones.Calendar] == 1)
				{
					wr('<input type="button" id="targetDateCal" class="etc-button" ');
					wr('onclick="show_calendar(\'' + tempid + '\',' + checkids.length + ')">');
				}
				//else if (mand == 2)
				//{
				//	wr('&nbsp;<input name="button' . $qid . '" type="button" value="N/A" onClick="this.form.' . $qid . '.value=this.form.button' . $qid . '.value">');
				//}
			}
			wr('</td></tr>');
			var tmp = new Array();
			tmp[0] = g_submilestones[k][g_submilestones.SubID];
			tmp[1] = 0;
			checkids[checkids.length] = tmp;
		}
	}


	wr('</table></div>');
}

wr('</div>');

if(isIE)
{
	wr('<table width="100%"><tr><td width="100%" align="center">');
	wr('<button class="command" style="width:80px;" onclick="onOK()">OK</button>&nbsp;&nbsp;&nbsp;<button class="command" style="width:80px;" onclick="onCancel()">Cancel</button>');
	wr('</td></tr></table>');
}



</script>

</center>
</form>
</BODY>
</HTML>
