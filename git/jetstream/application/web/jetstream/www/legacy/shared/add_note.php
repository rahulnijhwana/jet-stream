<?php
/**
* @package Shared
*/

define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');
require_once(BASE_PATH . '/include/class.DbConnManager.php');
require_once(BASE_PATH . '/include/class.SqlBuilder.php');
require_once(BASE_PATH . '/include/class.ReportingTree.php');
require_once BASE_PATH . '/slipstream/class.Note.php';


SessionManager::Init();
SessionManager::Validate();

if(isset($_REQUEST['oppid']) && ($_REQUEST['oppid'] > 0)) {
	$opp_id = $_REQUEST['oppid'];
	
	$obj_note = new Note();
	
	if(isset($_REQUEST['action']) && ($_REQUEST['action'] == 'add')) {
		/* Set the Current timestamp. */
		$curTimeStamp = date('Y-m-d H:i:s');		
		$spn_NoteSubject = '';
		$spn_NoteText = '';
		
		$obj_note->noteCreator = $_SESSION['USER']['USERID'];
		$obj_note->noteCreationDate = $curTimeStamp;
		$obj_note->noteSubject = $_POST['NoteSubject'];
		$obj_note->noteText = nl2br($_POST['NoteText']);
		
		if(isset($_POST['CheckPrivate']) && ($_POST['CheckPrivate'])) {
			$obj_note->notePrivate = 1;
		}
		else {
			$obj_note->notePrivate = 0;
		}
		$obj_note->noteObjectType = NOTETYPE_OPPORTUNITY;
		$obj_note->noteObject = $opp_id;
		
		$result = $obj_note->createNote();		
			
		if($result['STATUS'] == "OK") {					
			header("Location:opp_notes.php?oppid=$opp_id&success=1");
			exit;
		}
		else {
			if(count($result['ERROR']) > 0) {
				foreach($result['ERROR'] as $error_key => $error_msg) {					
					if($error_key == 'NoteSubject') {
						$spn_NoteSubject = $error_msg;
					}
					if($error_key == 'NoteText') {
						$spn_NoteText = $error_msg;
					}
				}
			}
		}
	}
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Add Opportunity Note</title>
		<link rel="stylesheet" type="text/css" href="../css/sharedstyles.css">
		<script language="JavaScript" src="../javascript/windowing.js,pageformat.js,save_board.js,saver2.js,ci_confirm.js,offerings.js"></script>
		<style>
			<!--
			.clsRequire {
				color : #FFAA55;
			}
			.clsError {
				color : #FFAA55;
				font-size : 12px;		
				font-weight : bold;				
			}
			-->	
		</style>
		<script language="JavaScript" type="text/javascript" >
		<!--				
			var g_real_parent = Windowing.dropBox.editOppWin;
			
			<?php
			if(isset($_REQUEST['oppid']) && ($_REQUEST['oppid'] == 0)) {
				
				/* If someone wants to add a note for the new opportunity. */
				if(isset($_REQUEST['action']) && ($_REQUEST['action'] == 'add')) {					
					$show_error = false;
					if(trim($_POST['NoteSubject']) == '') {
						$spn_NoteSubject = 'The Subject of Note can\'t be left blank.';
						$show_error = true;
					}
					if(trim($_POST['NoteText']) == '') {
						$spn_NoteText = 'The Text of Note can\'t be left blank.';
						$show_error = true;
					}
					if(!$show_error) {
						$note_subject = str_replace("\r\n", '<br>', $_POST['NoteSubject']);
						$note_subject = str_replace('|', '&##124#;', $note_subject);
						$note_text = str_replace("\r\n", '<br>', $_POST['NoteText']);
						$note_text = str_replace('|', '&##124#;', $note_text);
						
						if(isset($_POST['CheckPrivate']) && ($_POST['CheckPrivate'])) {
							$note_private = 1;
						}
						else {
							$note_private = 0;
						}
					
						echo 'g_real_parent.setNoteVal(\''.$note_subject.'\', \''.$note_text.'\', '.$note_private.');'."\n";
						echo 'window.close();'."\n";
					}					
				}
			}
			?>
			function wr(text)
			{
			    document.write(text);
			}
			
			function makeBoxHeader(label)
			{
			    var ret = '<td height="40" colspan=2>';
			
			    ret += '<table width=100% height=100% cellpadding=0 cellspacing=0 border=0><tr><td><img src="../images/edophead-left.gif" ></td>';
			    ret += '<td width=100% height=100%';
			    ret += ' class="edop_header" valign="top">';
			    // here's an extra table to move the heading up a couple of pixels
			    ret += '<table width=100% height=35 cellpadding=0 cellspacing=0 border=0><tr><td class="plabel_eo" style="text-align: left;" height=100% width=100% valign="center">';
			    ret += label;
			    ret += '</td></tr></table>';
			    ret += '</td><td><img src="../images/edophead-right.gif"></td></tr></table></td>';
			    return ret;
			}
			
			function write_header_title(img, text)
			{
			    document.writeln('<table border=0><tr><td><img src="../images/' + img + '" align="left">');
			    document.writeln('</td><td class="title" valign="middle" >' + text + '</td></tr></table>');
			}
			
			function write_header() {
					g_admin_mode = false;
			    var header = '';
			    if (g_addMode) {
			        header = 'Add Note';
			    }
			    else {
			        header = 'Opportunity Notes';
			    }	
			
			    Header.setText(header);
			    wr(Header.makeHTML ());
			}
			
			function load_page(page_no) {
				var temp_url = window.location.href;
				temp_url = temp_url.replace('#', '');
				
				var find_page_indx = temp_url.indexOf('&page=')	
				if(find_page_indx >= 0) {
					temp_url = temp_url.substr(0,find_page_indx);
				}
				temp_url = temp_url.replace('&success=1', '');
					
				temp_url += '&page='+page_no;
				window.location.href = temp_url;
			}
			
			function add_opp_note(opp_id) {
				window.location.href = '../shared/add_note.php?oppid='+opp_id;
			}
			
			function cancel_note(opp_id){
				if(opp_id > 0) {
					window.location.href = '../shared/opp_notes.php?oppid='+opp_id;
				}
				else {
					window.close();
				}
			}
			
			function add_note_go()
			{
				document.addNewNote.submit();
			}
      g_addMode = false;  
      
      var ivoryBox = new IvoryBox('100%', null);
      ivoryBox.setType('grbl','.gif','#949597');
      ivoryBox.setBorderSize(8,9,8,9);

      var boxtop = ivoryBox.makeTop();
      var boxbottom = ivoryBox.makeBottom();

      var ivoryBox100 = new IvoryBox('100%', '100%');
      ivoryBox100.setType('grbl','.gif','#949597');
      ivoryBox100.setBorderSize(8,9,8,9);
      var boxtop100=ivoryBox100.makeTop();

      var ivoryBoxTall = new IvoryBox('100%', '100%');
      ivoryBoxTall.setType('grbl','.gif','#949597');
      ivoryBoxTall.setBorderSize(8,9,8,9);

      firstboxtop=ivoryBoxTall.makeTop();
      firstboxbottom=ivoryBoxTall.makeBottom();
     -->
		</script>
	</head>

	<body  style="background-color:black;" oldstyle="visibility: hidden" leftmargin=0 rightmargin=0 topmargin=0 bottommargin=0  >
		<script language="JavaScript" type="text/javascript">	
			<!--
			g_addMode = true;			
      
      var ivoryBox = new IvoryBox('100%', null);
      ivoryBox.setType('grbl','.gif','#949597');
      ivoryBox.setBorderSize(8,9,8,9);

      var boxtop = ivoryBox.makeTop();
      var boxbottom = ivoryBox.makeBottom();

      var ivoryBox100 = new IvoryBox('100%', '100%');
      ivoryBox100.setType('grbl','.gif','#949597');
      ivoryBox100.setBorderSize(8,9,8,9);
      var boxtop100=ivoryBox100.makeTop();

      var ivoryBoxTall = new IvoryBox('100%', '100%');
      ivoryBoxTall.setType('grbl','.gif','#949597');
      ivoryBoxTall.setBorderSize(8,9,8,9);

      firstboxtop=ivoryBoxTall.makeTop();
      firstboxbottom=ivoryBoxTall.makeBottom();     
      
      Header.addButton(new HeaderButton('Add', null, 'add_note_go();'));
      Header.addButton(new HeaderButton('Cancel', null, 'cancel_note(<?=$_REQUEST['oppid']?>);'));
      
			wr('<table cellspacing=0 cellpadding=5 style="border:0px solid green" bgcolor="white" width=100%><tr><td width=100%>');
			write_header();
			wr('</td></tr></table>');
			wr('<table width=100% cellpadding=5 cellspacing=0 style="border:0px solid cyan"><tr><td>');
			wr('<table width=100% cellpadding=3 cellspacing=0 style="border:0px solid cyan">');
			wr('<tr ><td rowspan=2 width=100% style="height:200px; border:0px solid green">');
			wr(firstboxtop);
			wr('<table width=100% style="border:0px solid coral">');
			wr(makeBoxHeader('Add Note'));
			wr('<tr><td nowrap align="left" class="plabel_eo" style="text-align:left;" >');		
			-->			     			
		</script>			
		<center>
			<form method='post' action='./add_note.php?action=add&oppid=<?=$_REQUEST['oppid']?>' action=add' id='addNewNote' name='addNewNote'>	
			<table border='0' cellpadding='5' cellspacing='0' align='center' width='90%' id='tblAddNote'>		
				<tr>
					<td width='15%' valign='top' align='left'></td>
					<td width='1%'></td>
					<td width='84%' valign='top' align='left' class="plabel_eo">Fields marked with <span class='clsRequire'>*</span> are mandatory</td>
				</tr>
				<tr>
					<td width='15%' valign='top' align='left' class="plabel_eo"><span class='clsRequire'>*</span>&nbsp;Subject</td>
					<td width='1%' class="plabel_eo">:</td>
					<td width='84%' valign='top' align='left'>
						<input type='text' name='NoteSubject' id='NoteSubject' size='50' maxlength='50'  value='<?=(isset($_REQUEST['NoteSubject'])) ? ($_REQUEST['NoteSubject']) : ''?>' />		
						<br/><span id='spn_NoteSubject' class='clsError'><?=(isset($spn_NoteSubject)) ? ($spn_NoteSubject) : '' ?></span>
					</td>
				</tr>
				<tr>
					<td width='15%' valign='top' align='left' valign="top" class="plabel_eo"><span class='clsRequire'>*</span>&nbsp;Text</td>
					<td width='1%' valign="top" class="plabel_eo">:</td>
					<td width='84%' valign='top'  align='left'  valign="top">
						<textarea name='NoteText' id='NoteText'  cols='70' rows="15"><?=(isset($_REQUEST['NoteText'])) ? ($_REQUEST['NoteText']) : ''?></textarea>
						<br/><span id='spn_NoteText' class='clsError'><?=(isset($spn_NoteText)) ? ($spn_NoteText) : '' ?></span>
					</td>
				</tr>
				<tr>
					<td width='15%' valign='top' align='left' valign="top" class="plabel_eo">Private</td>
					<td width='1%' valign="top" class="plabel_eo">:</td>
					<td width='84%' valign='top' align='left'  valign="top">	
							<input name="CheckPrivate" id="CheckPrivate" type="checkbox" value="true" <?= (isset($_REQUEST['NoteText']) && ($_REQUEST['NoteText'])) ? 'checked' : ''?> />
					</td>
				</tr>
				<tr>
					<td width="15%" valign="top"></td>
					<td width="1%" valign="top"></td>
					<td width="84%" valign="top">
						<!--<input type='submit' class='sized_command_new' id='btnAddNote' name='btnAddNote' value='Add'/>&nbsp;
						<input type='button' class='sized_command_new' value='Cancel' name='cancel' size='30' onclick="javascript:cancel_note(<?=$_REQUEST['oppid']?>)"/>-->
					</td>
				</tr>
			</table>
			</form>	
		</center>
		<script language="JavaScript" type="text/javascript">		
			<!--				
			wr('</td></tr></table>');
			wr(firstboxbottom);
			wr('</td></tr></table>');
			wr('</td></tr></table>');
			
			var parent_obj = Windowing.dropBox.editOppWin;
			var getOppNote = parent_obj.getNoteVal();
			if(getOppNote) {
				if(getOppNote['NoteSub'] != '') {
					document.getElementById('NoteSubject').value = getOppNote['NoteSub'];
				}
				
				if(getOppNote['NoteText'] != '') {
					document.getElementById('NoteText').value = getOppNote['NoteText'];
				}
			}
			-->	
		</script>	
	</body>
</html>