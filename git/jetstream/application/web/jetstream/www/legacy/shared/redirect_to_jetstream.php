<?php
	define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));	
	require_once(BASE_PATH . '/include/class.SessionManager.php');
	require_once(BASE_PATH . '/include/class.DbConnManager.php');
	require_once(BASE_PATH . '/include/class.SqlBuilder.php');		
	SessionManager::Init();
	SessionManager::Validate();	
	$sql='select AccountID from Account where ' . $_SESSION['account_name'] . '="' . $_REQUEST['acc'] . '" and CompanyID='.$_SESSION['mpower_companyid'];	
	$rs = DbConnManager::GetDb('mpower')->Exec($sql);	
	$accountId=0;
	$contactId=0;
	if(count($rs)>0)
		$accountId=$rs[0]['AccountID'];
		
	if($_REQUEST['type']=='Contact')	
		$contactId=getContactIdByName($_REQUEST['contact'],$accountId,$_SESSION['mpower_companyid']);
	
	function getContactIdByName($contact,$accountId,$companyId)
	{		
		
		$firstNameField=$_SESSION['contact_first_name'];
		$lastNameField=$_SESSION['contact_last_name'];
		$sql='select ContactID from Contact where AccountId=? and '.$firstNameField."+' '+".$lastNameField.'=?';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT,$accountId),array(DTYPE_STRING,$contact));			
		$rs = DbConnManager::GetDb('mpower')->Exec($sql);		
		if(count($rs)==1)
		{
			return $rs[0]['ContactID'];
		}
		else
			return '0';		
	}

?>
<script>
	<?php 
		
		echo 'var accountId="'.$accountId.'";';
		echo 'var contactId="'.$contactId.'";';		
		echo 'var rtype="'. $_REQUEST['type'] .'";';
	?>
	if(rtype=='Company' && accountId!='')
	{
		window.opener.location.href='../../slipstream.php?action=company&accountId='+accountId;						
		window.close();	
	}
	else if(rtype=='Contact' && contactId!='')
	{
		window.opener.location.href='../../slipstream.php?action=contact&contactId='+contactId;			
		window.close();
	}	
</script>