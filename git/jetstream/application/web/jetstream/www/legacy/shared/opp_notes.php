<?php
/**
* @package Shared
*/

define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');
require_once(BASE_PATH . '/include/class.DbConnManager.php');
require_once(BASE_PATH . '/include/class.SqlBuilder.php');
require_once(BASE_PATH . '/include/class.ReportingTree.php');
require_once BASE_PATH . '/slipstream/class.Note.php';

SessionManager::Init();
SessionManager::Validate();

$empty_error = 'No notes for this opportunity.';
$no_records = '';

if(isset($_REQUEST['oppid']) && ($_REQUEST['oppid'] != '')) {
	$opp_id = $_REQUEST['oppid'];
	
	if(isset($_REQUEST['page']) && ($_REQUEST['page'] > 0)) {
		$page_no = $_REQUEST['page'];
	}
	else {
		$page_no = 1;
	}
	
	if(isset($_REQUEST['success']) && ($_REQUEST['success'] == 1)) {
		$success_message = 'Note added successfully';
	}
	else {
		$success_message = '';
	}
	
	$obj_note = new Note();
	$obj_note->paginationFor = 'opp';
	$obj_note->pageNo = $page_no;
	$obj_note->noteObjectType = NOTETYPE_OPPORTUNITY;
	$obj_note->noteObject = $opp_id;
	$note_list = $obj_note->getNoteList();
	if (is_array($note_list) && count($note_list) > 0) {
		$note_list = $note_list;
		$page_links = str_replace("'", "\'", stripslashes($obj_note->getPages()));
		$page_links = str_replace("./images", "../../images", $page_links);
		$page_info = str_replace("'", "\'", stripslashes($obj_note->getPageInfo()));
		$no_records = '';
	} else {
		$no_records = $empty_error;
	}
}
else {
	$no_records = $empty_error;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">

    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Opportunity Notes</title>
		<link rel="stylesheet" type="text/css" href="../css/sharedstyles.css">
		<link rel="stylesheet" type="text/css" href="../../css/note.css">
    <!--<script language="JavaScript" src="../javascript/opp_notes.js,load_dropdowns.js,utils.js,windowing.js,category_labels.js,smartselect.js,pageformat.js,save_board.js,saver2.js,ci_confirm.js,offerings.js"></script>   -->
    <script language="JavaScript" src="../javascript/windowing.js,pageformat.js,save_board.js,saver2.js,ci_confirm.js,offerings.js"></script>
    

    <style>
			<!--
			#noteDiv table{
				font-family: Verdana, Helvetica, Tahoma, Arial;
			  font-size: 8pt;
			  color: rgb(0,0,0);
			  font-weight:normal;
			}
			.note {
				background-color: silver;
			}
			.note_alt {
				margin: 0 0px;
			}
			.note_alt p {
				margin: 0 0 5px 0;				
			}			
			.note p {
				padding-bottom:5px;				
			}
			.note_alt p.body {			
				margin: 0 0px;
			}
			.note p, .note p.body {
				margin: 0 20px;
			}
			.note b, .note_alt b {
				color: #017196;
			}
			#pageInfo b {
				color: #FFFFFF;
			}
			-->	
		</style>
		<script language="JavaScript" type="text/javascript" >
		<!--	
			function wr(text)
			{
			    document.write(text);
			}
			
			function makeBoxHeader(label)
			{
			    var ret = '<td height="40" colspan=2>';
			
			    ret += '<table width=100% height=100% cellpadding=0 cellspacing=0 border=0><tr><td><img src="../images/edophead-left.gif" ></td>';
			    ret += '<td width=100% height=100%';
			    ret += ' class="edop_header" valign="top">';
			    // here's an extra table to move the heading up a couple of pixels
			    ret += '<table width=100% height=35 cellpadding=0 cellspacing=0 border=0><tr><td class="plabel_eo" style="text-align: left;" height=100% width=100% valign="center">';
			    ret += label;
			    ret += '</td></tr></table>';
			    ret += '</td><td><img src="../images/edophead-right.gif"></td></tr></table></td>';
			    return ret;
			}
			
			function write_header_title(img, text)
			{
			    document.writeln('<table border=0><tr><td><img src="../images/' + img + '" align="left">');
			    document.writeln('</td><td class="title" valign="middle" >' + text + '</td></tr></table>');
			}
			
			function write_header() {
					g_admin_mode = false;
			    var header = '';
			    if (g_addMode) {
			        header = 'Add Note';
			    }
			    else {
			        header = 'Opportunity Notes';
			    }	
			
			    Header.setText(header);
			    wr(Header.makeHTML ());
			}
			
			function load_page(page_no) {
				var temp_url = window.location.href;
				temp_url = temp_url.replace('#', '');
				
				var find_page_indx = temp_url.indexOf('&page=')	
				if(find_page_indx >= 0) {
					temp_url = temp_url.substr(0,find_page_indx);
				}
				temp_url = temp_url.replace('&success=1', '');
					
				temp_url += '&page='+page_no;
				window.location.href = temp_url;
			}
			
			function add_opp_note(opp_id) {
				window.location.href = '../shared/add_note.php?oppid='+opp_id;
			}
			
			function cancel_note(opp_id){
				window.location.href = '../shared/opp_notes.php?oppid='+opp_id;
			}
			
			function add_note_go()
			{
				document.addNewNote.submit();
			}
      g_addMode = false;  
      
      var ivoryBox = new IvoryBox('100%', null);
      ivoryBox.setType('grbl','.gif','#949597');
      ivoryBox.setBorderSize(8,9,8,9);

      var boxtop = ivoryBox.makeTop();
      var boxbottom = ivoryBox.makeBottom();

      var ivoryBox100 = new IvoryBox('100%', '100%');
      ivoryBox100.setType('grbl','.gif','#949597');
      ivoryBox100.setBorderSize(8,9,8,9);
      var boxtop100=ivoryBox100.makeTop();

      var ivoryBoxTall = new IvoryBox('100%', '100%');
      ivoryBoxTall.setType('grbl','.gif','#949597');
      ivoryBoxTall.setBorderSize(8,9,8,9);

      firstboxtop=ivoryBoxTall.makeTop();
      firstboxbottom=ivoryBoxTall.makeBottom();
     -->
		</script>
    </head>

    <body  oldstyle="visibility: hidden" leftmargin=0 rightmargin=0 topmargin=0 bottommargin=0  style="background-color:black;"><form name="form1" style="height:100%" style="background-color:black"><!-- height attribute is for mozilla 1.2.1 -->
			<script language="JavaScript" type="text/javascript" >
				<!--
				Header.addButton(new HeaderButton('Add Note', null, 'add_opp_note(<?=$opp_id?>);'));
	      Header.addButton(new HeaderButton('Cancel', null, 'javascript:window.close();'));
	      
				wr('<table cellspacing=0 cellpadding=5 style="border:0px solid green" bgcolor="white" width=100%><tr><td width=100%>');
				write_header();
				wr('</td></tr></table>');
				wr('<table width=100% cellpadding=5 cellspacing=0 style="border:0px solid cyan">');// margin table
				wr('<tr><td style="color:#FFFFFF;font-weight:bold;" align="center"><?=$success_message ?></td></tr><tr><td>');			
				wr('<table width=100% cellpadding=3 cellspacing=0 style="border:0px solid cyan">');
				wr('<tr ><td rowspan=2 width=100% style="height:200px; border:0px solid green">');
				// ----------------------------------------------First column
				wr(firstboxtop);
				wr('<table width=100% style="border:0px solid coral">');
				wr(makeBoxHeader('Opportunity Notes'));
				wr('<tr><td nowrap align="left" class="plabel_eo" style="text-align:left;" >');				     
				wr('<div id="noteDiv"  width="95%">');
				wr('<table width="100%" border="0"><tr><td>');		
				wr('<span  style="float:right;" id="spanPageLinkTop"><? echo $page_links ?>')
				wr('</span>');
				wr('<span  style="float:right;" id="pageInfo"><? echo $page_info ?></span>');
				wr('</td></tr>');
				-->	
			</script>	
			<?
			$strNotes ='';
			if(count($note_list > 0)) {
				$strNotes .='<tr><td>';
				foreach ($note_list as $key => $note) { 				 
				 if($key % 2 ==0){$divclass = "note";} else {$divclass = "note_alt";}
				 	$strNotes .= '<div class="'.$divclass.'">';
					$strNotes .= '<p><b>Date:</b> '.$note['TimeStamp'].'</p>';
					$strNotes .= '<p><b>User:</b> '; 
					
					if (isset($note['AddressType']) && trim($note['AddressType']) != '') { 
						$strNotes .= '<b>'.$note['AddressType'].': </b>';
					}
					$strNotes .= $note['Creator'].'</p> ';
					
					if (isset($note['Company']) && trim($note['Company']) != '') {
						$strNotes .= '<p><b>Company:</b> '.$note['Company'].'</p>';
					} 
					if (isset($note['Contact']) && trim($note['Contact']) != '') {
						$strNotes .= '<p><b>Contact:</b> '.$note['Contact'].'</p>';
					}
					if(isset($note['Opp']) && trim($note['Opp']) != '') {
						$strNotes .= '<p><b>Opp:</b> '.$note['Opp'].'</p>';
					}
					if(isset($note['Type']) && trim($note['Type']) != '') {
						$strNotes .= '<p><b>Type:</b> '.str_replace("slipstream.php?", "../../slipstream.php?", $note['Type']).'</p>';
					}
					if(isset($note['Private']) && $note['Private'] ==  '1') {
						$strNotes .= '<p><b>Private:</b> <img src="../../images/checkbox_checked.png"></p>';
					}
					else if(isset($note['Private']) && $note['Private'] ==  '0') {
						$strNotes .= '<p><b>Private:</b> <img src="../../images/checkbox_unchecked.png"></p>';
					}
					$strNotes .= '<p><b>Subject:</b> '.$note['Subject'].'</p>';
					if (isset($note['Attachment']) && trim($note['Attachment']) != '') {
						$strNotes .= '<p><b>Attached:</b> '.$note['Attachment'].'</p>';
					}
					$strNotes .= '<p class="body">'.$note['Body'].'</p>';									
					$strNotes .= '</div>';					
			 	}
			 	$strNotes .='</td></tr>';
			} 
			echo $strNotes;
		?>
		<script language="JavaScript" type="text/javascript">	
			<!--
			wr('<tr><td style="color:#FFFFFF;">');			
			wr('<b><? if($no_records != '') { echo $no_records; } ?><b>');
			wr('</td></tr>');			
			wr('<tr><td>');			
			wr('<span style="float:right;" id="spanPageLinkBottom"><? echo $page_links ?>');	
			wr('</span>');
			wr('<input type="hidden" name="totRec" id="totRec" value="<? echo $tot_rec; ?>">');
			wr('</td></tr></table>');	
			wr('</div>');
			
			wr('</td></tr></table>');
			wr(firstboxbottom);
			wr('</td></tr></table>');
			wr('</td></tr></table>');
			-->	
		</script>	
   </body>
</html>