<?php
/**
* @package Shared
*/

define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');
require_once(BASE_PATH . '/include/class.DbConnManager.php');
require_once(BASE_PATH . '/include/class.SqlBuilder.php');

SessionManager::Init();
SessionManager::Validate();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- <title>M-Power</title> -->
<link rel="stylesheet" type="text/css" href="../css/sharedstyles.css">
<script language="JavaScript" src="../javascript/windowing.js,utils.js"></script>
</head>

<body style="background-color:lightgrey">

<script language="JavaScript">
<!--

<?php
	$companyList="('$mpower_companyid')";
	
	include_once('../data/_base_utils.php');
	include_once('../data/_query_utils.php');
	include_once('../data/get_company.php');
	include_once('../data/get_removedopps.php');


	$sql="SELECT * FROM people 
			WHERE Deleted = 0 AND 
				  isSalesperson = 1 AND 
				  (SupervisorID = $mpower_effective_userid or PersonID = $mpower_effective_userid)
			ORDER BY LastName";
	$getpeople = get_data($sql);
	$peers = $mpower_effective_userid;
	
	foreach($getpeople as $eachpeople) {
		if(empty($peers)) $peers .= $eachpeople['PersonID'];
		else $peers .= ', '.$eachpeople['PersonID'];
	}
	
	$first = date('Y-m-d h:i:s');
	$sql = "SELECT * FROM opportunities WHERE CompanyID = '$mpower_companyid'";
	$sql .= " AND (Category != '6' OR ActualCloseDate >= '$first')";
	$sql .= " AND (Category != '9')";
	$sql .= " AND PersonID in ($peers)";
	dump_sql_as_array('g_opps', $sql); 
	
	dump_sql_as_array('g_opp_products_xref', "SELECT * FROM Opp_Product_XRef where CompanyID IN ($companyList) order by DealID, Seq");
	dump_sql_as_array('g_products', "select * from products where CompanyID IN ($companyList) and Deleted = '0' order by Name");
?>

var g_sortField = '';
g_sortDescending = false;

function sort(fieldName)
{
	document.body.style.cursor = 'wait';
	if (g_sortField == fieldName) g_sortDescending = !g_sortDescending;
	else g_sortDescending = false;
	g_sortField = fieldName;
	var olist = document.getElementById('opplist');
	olist.contentWindow.location.reload();
	document.body.style.cursor = 'auto';
}

function find_removed_opp(id)
{
	for (var i = 0; i < opplist.g_removedopps.length; ++i)
	{
		if (opplist.g_removedopps[i][opplist.g_removedopps.DealID] != id)
			continue;
		return opplist.g_removedopps[i];
	}

	for (var i = 0; i < opplist.pp_opps.length; ++i)
	{
		if (opplist.pp_opps[i][opplist.pp_opps.DealID] != id)
			continue;
		return opplist.pp_opps[i];
	}

	return null;
}

function ok()
{
	if (opplist.g_selected)
	{
		var id = opplist.g_selected.id.substr(1);
		var opp = find_removed_opp(id);
		if (opp && Windowing.dropBox.reviveCallback)
			Windowing.dropBox.reviveCallback(opp, opplist.g_removedopps);
	}

	window.close();
}

function cancel()
{
	window.close();
}

// -->
</script>

<table border=0 height="100%" width="100%">
<tr valign="top">
	<td>
		<table border=0>
		<tr>
			<td><img src="../images/removed_opp.gif" align="left"></td>
			<td class="title" valign="middle">Revive A Removed Opportunity</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="command raised" width="45%" onmousedown="press_fake_button(this)" onmouseup="lift_fake_button(this); sort('Company')" style="cursor: default;">
		Company
	</td>
	<td class="command raised" width="9%" onmousedown="press_fake_button(this)" onmouseup="lift_fake_button(this); sort('FirstMeeting')" style="cursor: default;">
		First Mtg
	</td>
	<td class="command raised" width="24%" onmousedown="press_fake_button(this)" onmouseup="lift_fake_button(this); sort('Name')" style="cursor: default;">
		Offering
	</td>
	<td class="command raised" width="9%" onmousedown="press_fake_button(this)" onmouseup="lift_fake_button(this); sort('EstimatedDollarAmount')" style="cursor: default;">
		$ Value
	</td>
	<td class="command raised" width="13%" onmousedown="press_fake_button(this)" onmouseup="lift_fake_button(this); sort('RemoveDate')" style="cursor: default;">
		Date Removed
	</td>
</tr>
<tr valign="top">
	<td colspan="5">
		<iframe name="opplist" id="opplist" height="400" width="100%" src="revive_list.html" scrolling="yes"></iframe>
	</td>
</tr>
<tr valign="bottom">
	<td colspan="5" align="center">
		<button type="button" class="sized_command" onclick="ok()" disabled id="okbutton">OK</button>
		&nbsp;
		<button type="button" class="sized_command" onclick="cancel()">Cancel</button>
	</td>
</tr>
</table>

</body>
</html>
