<?php
/**
 * String functions
 * @package Shared
 */
/**
 * Get a string of random characters
 *
 * @param int $len   The length of the string to return
 * @param string $chars The set of characters to include in the string.  Defaults to capital letters and numbers.
 * @return string       A random string $len length made up of a random set of $chars
 */
function RandomString($len, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
{
    $string = '';
    for ($i = 0; $i < $len; $i++)
    {
        $pos = rand(0, strlen($chars)-1);
        $string .= $chars{$pos};
    }
    return $string;
}
?>
