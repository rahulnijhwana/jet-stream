<?php
/**
 * @package Data
 */

// output the results of a sql query to an array of arrays
// pass in a valid SQL statement
function dump_sql_as_array($arrayname, $sql, $append = false) {
	try {
		$result = mssql_query($sql);
	} catch (Exception $e) {
		error_log($e . ' - optimize SQL error: ' . $sql);
	}
	if ($result) {
		if ($append) {
			dump_resultset_as_array_append($arrayname, $result);
		} else {
			dump_resultset_as_array($arrayname, $result);
		}
	} else {
		error_log('optimize SQL failed: ' . $sql);
	}
}

//function dump_resultset_as_array_json($arrayname, $resultset, $func = NULL)
//{
//	$data_array = array();
//	while ($row = mssql_fetch_row($resultset)) {
//		$data_row = array();
//		foreach ($row as $value) {
//			$data_row[] = addslashes(trim($value));
//		}
//		$field_array[] = $data_row;
//		//$data_array[] = $data_row;
//	}
//	//$field_array[] = array($data_array);
//
//	$fieldlist = '';
//	$len = mssql_num_fields($resultset);
//	for ($i = 0; $i < $len; ++$i) {
//		$field = mssql_fetch_field($resultset, $i);
//		$field_array[$field->name] = $i;
//
//		$fieldlist .= $field->name;
//		if ($i != ($len - 1)) {
//			$fieldlist .= ',';
//		}
//	}
//	$field_array['_fieldlist'] = $fieldlist;
//
//	//print_r($field_array);
//
//	echo $arrayname . " = " . json_encode($field_array) . "\n";
//
//	print("\n\n");
//	//dump_resultset_as_array_orig($arrayname, $resultset, $func);
//}


// output the results of a query to an array of arrays
// pass in the result of a mssql_query() call
function dump_resultset_as_array($arrayname, $resultset, $func = NULL) {
	if (mssql_num_rows($resultset) > 0) mssql_data_seek($resultset, 0);
	print("$arrayname = [\n");
	$firstrow = true;
	while ($row = mssql_fetch_row($resultset)) {
		if ($firstrow) $firstrow = false;
		else print(",\n");
		if ($func) $func($resultset,$row);
		print('["');

		$first = true;
		foreach ($row as $value)
		{
			if ($first) $first = false;
			else print('","');
			print(addslashes(trim($value)));
		}
		print('"]');
	}

	print("];\n\n");

	$fieldlist = '';
	$len = mssql_num_fields($resultset);
	for ($i = 0; $i < $len; ++$i)
	{
		$field = mssql_fetch_field($resultset, $i);
		$fieldlist .= $field->name;
		if ($i != ($len - 1))
		$fieldlist .= ',';
		print("$arrayname." . $field->name . " = $i;\n");
	}

	print($arrayname . '._fieldlist = "' . $fieldlist . '";');
	print("\n\n");
}

function dump_resultset_as_array_append($arrayname, $resultset)
{
	$firstrow = true;
	while (($row = mssql_fetch_row($resultset)))
	{
		print($arrayname . '[' . $arrayname . '.length] = ["');

		$first = true;
		foreach ($row as $value)
		{
			if ($first)
			$first = false;
			else
			print('","');
			print(addslashes(trim($value)));
		}
		print('"];');
		print("\n");
	}

	print("\n");
}

// output the contents of an associate array to an array of arrays
// pass in the result of a mssql_fetch_assoc() call
function dump_assoc_as_array($arrayname, $assoc)
{
	print("$arrayname = [[\n\"");

	$first = true;
	foreach ($assoc as $value)
	{
		if ($first) $first = false;
		else print('","');
		print(addslashes(trim($value)));
	}
	print("\"]];\n\n");


	$fieldlist = '';
	$count = 0;
	foreach ($assoc as $fieldname => $value)
	{
		if ($count) $fieldlist .= ',';
		$fieldlist .= $fieldname;
		print("$arrayname.$fieldname = $count;\n");
		$count++;
	}

	print($arrayname . '._fieldlist = "' . $fieldlist . '";');
	print("\n\n");
}

// make an empty data array.  arrayname can also be an array of names.
function dump_nothing_as_array($arrayname)
{
	if (!is_array($arrayname))
	{
		print("$arrayname = [];\n");
		return;
	}

	$len = count($arrayname);
	for ($i = 0; $i < $len; ++$i)
	{
		$arr = $arrayname[$i];
		print("$arr = [];\n");
	}
}

// pass in a SQL query selecting one field from various records
// get back a comma-delimited list of the matches in that field
function make_in_clause($sql)
{
	$result = mssql_query($sql);
	if (!$result) return '';

	$firstrow = true;
	$values = '';
	while (($row = mssql_fetch_row($result)))
	{
		if ($firstrow) $firstrow = false;
		else $values .= ',';
		$values .= "'${row[0]}'";
	}

	return $values;
}

/**
 * \fn get_data(query)
 * \brief Returns the results of a query to an array of arrays
 * \param query MSSQL sql query
 * 
 */
function get_data($query) {
	$data = array();
	if ($result = mssql_query($query)) {
		while($row = mssql_fetch_array($result)) $data[] = $row;
		mssql_free_result($result);
	}
	return $data;
}

?>