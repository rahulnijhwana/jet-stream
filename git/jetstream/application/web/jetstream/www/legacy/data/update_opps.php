<?php
/**
 * @package Data
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');
require_once(BASE_PATH . '/include/mpconstants.php');
require_once(BASE_PATH . '/slipstream/class.Note.php');
require_once BASE_PATH . '/include/class.DashboardSearchCreatorNew.php';

require 'jetstream_events.php';

SessionManager::Init();
SessionManager::Validate();

require_once('_base_utils.php');
require_once('_update_utils.php');
include_once('log_obj.php');
include_once('autorevive.php');
require_once('_opps_utils.php');

function writeCMlog($message)
{
	//$handle=fopen("/usr/local/apache/htdocs/logs/CM.log","a+");
	//$logtime=date("Y M j D G:i:s");
	//fwrite($handle,"$logtime $message\n");
	//fclose($handle);
    LogFile::WriteLine($message);
}

print("<script language=\"JavaScript\">\n");

if (!check_user_login()) print_result('Invalid login');
if (!isset($mpower_data) || !strlen($mpower_data)) print_result('No data to save');
if (!isset($mpower_fieldlist) || !strlen($mpower_fieldlist)) print_result('Field list is missing');

print("</script>\n");

// determine if there is a CM, and set vars accordingly
// LF Optimize there is now a global array that holds the company info, this should be used instead of a query
$sql="SELECT CMInterface, CMPath, CMLogin, CMPass FROM company where companyid = $mpower_companyid";
$result=mssql_query($sql);
$CMInterface=false;
if ($row=mssql_fetch_assoc($result)) {
	if ($row['CMInterface']=='1') {
		$CMInterface=true;
		$CMPath=$row['CMPath'];
		$CMLogin=$row['CMLogin'];
		$CMPass=$row['CMPass'];
		$CMopps=array();
	}
}
else writeCMlog('Get company failed');

print('<div id="errmsg">');

$fields = explode(',', $mpower_fieldlist);
$values = explode('|', $mpower_data);
$ignored_list = array('Company', 'Contact');

$field_array = explode(',', $mpower_fieldlist);
$field_count = count($field_array);
$dealid_index = -1;
$category_index = -1;
$personid_index = -1;

for ($k = 0; $k < $field_count; ++$k) {
	if(strtolower($fields[$k]) == 'dealid')	{
		$dealid_index = $k;		
	} else if (strtolower($fields[$k]) == 'category') {
		$category_index = $k;
	} else if (strtolower($fields[$k]) == 'personid') {
		$personid_index = $k;
	}
}

$combined = array_chunk($values, count($fields));
$data = array();

$count = 1;
foreach($combined as $item) {
	$len = count($item);
	$contact = $item[$len -1];	
	$company = $item[$len - 2];	
	
	foreach($fields as $key=>$q) {		
		$data[] = $item[$key];		
	}	
	
	$contact = str_replace("'", "''", str_replace("\\\"", "\"", str_replace("\\'","'", $contact)));
	
	$contact_name = getContact($contact);	
	$fname = $contact_name['FirstName'];
	$lname = $contact_name['LastName'];
	
	$accountid = $contactid = 0;
	$company = trim($company);
	$company = str_replace("'", "''", str_replace("\\\"", "\"", str_replace("\\'","'", $company)));
	
	// account and opp
	$accountmap_records = mssql_query("SELECT AccountID, PrivateAccount, Inactive FROM Account WHERE " . $_SESSION['account_name'] . " = '$company' AND CompanyID = $mpower_companyid AND Deleted != 1");
	$acc_rs = mssql_fetch_array($accountmap_records);
	
	if (count($acc_rs) > 0 && isset($acc_rs['AccountID'])) {
		$accountid = $acc_rs['AccountID'];
		
		if($acc_rs['PrivateAccount'] == 1) {
			mssql_query("UPDATE Account SET PrivateAccount = 0 WHERE AccountID = '".$accountid."'");
		}
		if($acc_rs['Inactive'] == 1) {
			mssql_query("UPDATE Account SET Inactive = 0 WHERE AccountID = '".$accountid."'");
		}
	} else {			
		
		// create company
		$post = array();
		$post['rec_type'] = 'account';
		$post[$_SESSION['account_name']] = $company;		
		
		$accountid = create_company_contact($post);
		
		/*
		$result = mssql_query("INSERT INTO Account(CompanyID, " . $_SESSION['account_name'] . ") VALUES($mpower_companyid, '$company')");	
		$accountid = mssql_insert_id();
		
		if($result) {
			mssql_query("INSERT INTO PeopleAccount(AccountID, PersonID, CreatedBy, AssignedTo) VALUES('$accountid', '" . $_SESSION['login_id'] . "', 1, 1)");
		}	
		*/	
	}	
	
	if(isset($accountid) && ($accountid > 0)) {
		$obj_search = new DashboardSearchCreator();
		$obj_search->company_id = $mpower_companyid;
		$obj_search->record_id = $accountid;
		$obj_search->StoreAccountRecords();
	}
	
	// contact and opp
	$contactmap_records = mssql_query("SELECT ContactID, PrivateContact, Inactive FROM Contact WHERE " . $_SESSION['contact_first_name'] . " = '$fname' AND " . $_SESSION['contact_last_name'] . " = '$lname' AND AccountID = $accountid AND Deleted != 1");
	$contact_rs = mssql_fetch_array($contactmap_records);
	
	if (count($contact_rs) > 0 && isset($contact_rs['ContactID'])) {		
		$contactid = $contact_rs['ContactID'];		
		
		if($contact_rs['PrivateContact'] == 1) {
			mssql_query("UPDATE Contact SET PrivateContact = 0 WHERE ContactID = '".$contactid."'");
		}
		if($contact_rs['Inactive'] == 1) {
			mssql_query("UPDATE Contact SET Inactive = 0 WHERE ContactID = '".$contactid."'");
		}
	} else {		
		if(!empty($fname))	{
			
			/*
			$result = mssql_query("INSERT INTO Contact(AccountID, CompanyID, " . $_SESSION['contact_first_name'] . ", " . $_SESSION['contact_last_name'] . ") VALUES($accountid, $mpower_companyid, '$fname', '$lname')");
			$contactid = mssql_insert_id();

			if($result) {
				mssql_query("INSERT INTO PeopleContact(ContactID, PersonID, CreatedBy, AssignedTo) VALUES('$contactid', '" . $_SESSION['login_id'] . "', 1, 1)");
			}*/
			
			//create contact
			$post = array();
			$post['rec_type'] = 'contact';				
			$post['AccountID'] = $accountid;
			$post[$_SESSION['contact_first_name']] = $fname;	
			$post[$_SESSION['contact_last_name']] = $lname;
			
			$contactid = create_company_contact($post);	
		}
	}	
	
	if(isset($contactid) && ($contactid > 0)) {
		$obj_search = new DashboardSearchCreator();
		$obj_search->company_id = $mpower_companyid;
		$obj_search->record_id = $contactid;
		$obj_search->StoreContactRecords();
	}
	
	/* To set the values of last two columns(AccountID, ContactID) added to the opportunity later. */	
	$data[count($item) * $count - 8] = $contactid;
	$data[count($item) * $count - 9] = $accountid;
	
	$count++;	
}

$mpower_data = implode('|', $data);	

$ok = update_rows('opportunities', $mpower_fieldlist, $mpower_data, 'DealID', 'ChangedByID,Company,Contact,CallIn');

// file_put_contents('c:/test.txt', "Results: " . print_r($ok, TRUE) . PHP_EOL, FILE_APPEND);

CheckAutoRevive($mpower_companyid);

if ($ok) {
	SaveJetEvents($_POST['jet_events'], $data[$dealid_index], $contactid);
}

//include('calc_salescycles.php');
print('</div>');
print("<script language=\"JavaScript\">\n");

if ($CMInterface) {
	include_once("../$CMPath/cm_interface.php");
	$field_array = explode(',', $mpower_fieldlist);
	// RJP 5-22-03 - need two single quotes in str_replace
	// this explosion should match the one in _update_utils.php (update_rows)
	$value_array = explode('|', str_replace("\\\"", "\"", str_replace("\\'","''", $mpower_data)));
	$field_count = count($field_array);
	$value_count = count($value_array);
	$row_count = $value_count / $field_count;

	$CMOpps=array();
	$i=0;
	for ($n = 0; $n < $row_count; ++$n) {
		$CMOpp=array();
		for ($k = 0; $k < $field_count; ++$k) {
			$CMOpp[$field_array[$k]]=$value_array[$i];
			++$i;
		}
		if ($CMOpp['CMID']!='-1') {
			$sql = "select CMID from people where PersonID=".$CMOpp['PersonID'];
			$cmidResult = mssql_query($sql);
			if ($cmidResult && $cmidRow = mssql_fetch_assoc($cmidResult))
				$CMOpp['PersonCMID'] = $cmidRow['CMID'];
			array_push($CMOpps,$CMOpp);
		}
	}
	writeCMlog($CMLogin.' '.$CMPass);
	//writeCMlog(print_r($CMOpps,TRUE));
	CM_Connect($CMLogin, $CMPass);
	CM_UpdateOpps($CMOpps);
	writeCMlog("Done from CM_UpdateOpps test");
}

if ($ok) print_result('ok');
else if (strlen($UPDATE_ERROR_MSG)) print_result($UPDATE_ERROR_MSG);
else print_result('Update opportunities failed.');
close_db();









