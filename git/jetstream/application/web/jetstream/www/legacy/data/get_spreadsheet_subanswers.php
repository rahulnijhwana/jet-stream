<?php
/**
 * @package Data
 */
include_once('_base_utils.php');
include_once('_query_utils.php');


if (!check_user_login()) exit();

if (isset($opplist) && strlen(trim($opplist)) > 0) {
	dump_sql_as_array('g_subanswers_disp', "SELECT * FROM SubAnswers WHERE DealID in ($opplist) order by DealID, SubID");
}

?>