<?
/**
 * @package Data
 */

function CheckAutoRevive($CompanyID)
{
	// The first part does a 'Revive' - it moves a closed opp back to the target column
	$sql = "select DealID from opportunities where CompanyID = $CompanyID and Category=9 and AutoReviveDate is not null and AutoReviveDate <= '".date('n/j/Y')."'";
	$deals = make_in_clause($sql);
	if ($deals != '') {
		$sql = "update opportunities set ";
		$sql .= "Category=10, ";
		$sql .= "TargetDate=GETDATE(), ";
		$sql .= "FirstMeeting=null, ";
		$sql .= "FirstMeetingTime=null, ";
		$sql .= "NextMeeting=null, ";
		$sql .= "NextMeetingTime=null, ";
		$sql .= "Person=0, ";
		$sql .= "Need=0, ";
		$sql .= "Money=0, ";
		$sql .= "Time=0, ";
		$sql .= "Requirement1=0, ";
		$sql .= "EstimatedDollarAmount=0, ";
		$sql .= "RemoveReason=null, ";
		$sql .= "RemoveDate=null ";
		$sql .= "where DealID in ($deals)";
		mssql_query($sql);
		
		$sql = "delete from SubAnswers where DealID in ($deals)";
		mssql_query($sql);
		
		$sql = "delete from Opp_Product_XRef where DealID in ($deals)";
		mssql_query($sql);
	}
	
	// The second part does a 'Retarget' - it creates a new opp that matches the company, div, contact, etc of the original
	$sql = "select * from opportunities where CompanyID=$CompanyID and Category=6 and AutoReviveDate is not null and AutoReviveDate <= '".date('n/j/Y')."'";
	$result = mssql_query($sql);
	$rows = array();
	while ($row = mssql_fetch_assoc($result))
		array_push($rows, $row);
	for ($k = 0; $k < count($rows); ++$k)
	{
		$row = $rows[$k];
		$sql = "insert into opportunities (CompanyID,Category,AccountID,Division,ContactID,Requirement1,Requirement2,";
		$sql .= "Person,Need,Money,Time,PersonID,Alert1,Alert2,Alert3,Alert4,Alert5,Alert6,ValID,VLevel,TargetDate,";
		$sql .= "SourceID,Source2ID,Renewed) values (";
		$sql .= $CompanyID.',';
		$sql .= '10,';
		$sql .= "'".str_replace("'","''",$row['AccountID'])."',";
		$sql .= "'".str_replace("'","''",$row['Division'])."',";
		$sql .= "'".str_replace("'","''",$row['ContactID'])."',";
		$sql .= '0,0,0,0,0,0,';
		$sql .= $row['PersonID'].',';
		$sql .= '0,0,0,0,0,0,';
		if ($row['ValID'] == '')
			$sql .= 'null,';
		else
			$sql .= $row['ValID'].',';
		if ($row['VLevel'] == '')
			$sql .= 'null,';
		else
			$sql .= $row['VLevel'].',';
		$sql .= "GETDATE(),";
		if ($row['SourceID'] == '')
			$sql .= 'null,';
		else
			$sql .= $row['SourceID'].',';
		if ($row['Source2ID'] == '')
			$sql .= 'null,';
		else
			$sql .= $row['Source2ID'].',1)';
		
		//file_put_contents('D:/logsql.txt', "\n".'---------->'.$sql."\n", FILE_APPEND);
		mssql_query($sql);
		
		$sql = "update opportunities set AutoReviveDate=";
		if ($row['AutoRepeatIncrement'] != '')
		{
			$t = strtotime($row['AutoReviveDate']);
			$t = strtotime($row['AutoRepeatIncrement'], $t);
			$sql .= "'".date('n/j/Y', $t)."'";
		}
		else
			$sql .= 'NULL';
		$sql .= " where DealID=".$row['DealID'];
		//print($row['AutoRepeatIncrement'].'<br>');
		
		mssql_query($sql);
	}
}

?>