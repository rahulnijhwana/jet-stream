<?php

print('<script language="JavaScript">');

include_once('_base_utils.php');
include_once('_update_utils.php');

print('</script>');

if (!check_admin_login())
{
	close_db();
	exit();
}

print('<div id="errmsg">\n');
$result = update_rows('Logins', $mpower_fieldlist, $mpower_data, 'ID');
print('</div>');

print('<script language="JavaScript">window.result = ' . (($result != false) ? 'true' : 'false') . ';</script>');
if ($result == false)
	print('<div id="errmsg">' + $UPDATE_ERROR_MSG + '</div>');

close_db();

?>
