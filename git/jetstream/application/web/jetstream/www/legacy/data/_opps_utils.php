<?php
/**
 * @package data
 * functions for opps
 */
// create event
function create_event($event_contact_id, $user_id, $event_type, $event_subject, $today_date, $event_hour, $event_minute, $event_start_date, $event_start_time, $deal_id, $is_cancelled = 0, $is_closed = 0, $create_past_event = false) {
	$objEvent = new Event();
	$objEvent->allowCreatePastEvent = $create_past_event;	
	$objEvent->contactId = $event_contact_id;
	$objEvent->userId = $user_id;
	$objEvent->eventTypeId = $event_type;
	$objEvent->eventSubject = $event_subject;	
	$objEvent->eventDur = $event_hour.':'.$event_minute;
	$objEvent->saveType = 'EVENT';
	$objEvent->eventStartDate = $event_start_date;
	$objEvent->eventStartTime = $event_start_time;
	$objEvent->dealID = $deal_id;
	$objEvent->colseEventVal = $is_closed;
	$objEvent->cancelEventVal = $is_cancelled;	
	$objEvent->eventAction = 'add';			
	$resultEvent = $objEvent->SaveEvent();
	return ($resultEvent['STATUS'] == 'OK') ? true : false;
}




// handles creation of company and contact 
function create_company_contact($data) {
	$company_id = $_SESSION['company_obj']['CompanyID'];
	$user_id = $_SESSION['USER']['USERID'];
	
	require_once BASE_PATH . '/slipstream/class.ModuleCompanyContactNew.php';
	$rec = new ModuleCompanyContactNew($company_id, $user_id);		
	$rec->limit_validation = TRUE;	
	
	require_once BASE_PATH . '/slipstream/Inspekt.php';	
	$input = Inspekt_Cage::Factory($data);	
	$result = $rec->ValidateInput($input);
	
	$jet_id = $result['status']['RECID'];	
	return $jet_id;	
}


// create note
function create_note($created_by, $date, $note_subject, $note_text, $object_type, $object_referer, $private) {
	$objNote = new Note();
	$objNote->noteCreator = $created_by;
	$objNote->noteCreationDate = $date;
	$objNote->noteSubject = $note_subject;
	$objNote->noteText = $note_text;
	$objNote->noteObjectType = $object_type;
	$objNote->noteObject = $object_referer;
										
	$result = $objNote->createNote();	
}

// update event status
function update_event_status($event_id, $is_cancelled = 0, $is_closed = 0) {
	$event_id = (int) $event_id;
	$resultEvent = false;
	
	if($event_id > 0) {
		$resultEvent = mssql_query("UPDATE Events SET IsCancelled = '$is_cancelled', IsClosed = '$is_closed' WHERE EventID = '$event_id'");	
	}
	return ($resultEvent) ? true : false;
}

// Returns the last inserted id for the current db connection
function mssql_insert_id() {
	$sql = "SELECT SCOPE_IDENTITY() as idno;"; 
	$dbResult = mssql_query($sql);
	$res = mssql_fetch_array($dbResult); 
	return $res['idno'];
}

//update event
function update_event($event_id, $event_contact_id, $user_id, $event_type, $event_subject, $today_date, $event_hour, $event_minute, $event_start_date, $event_start_time, $deal_id) {
	$objEvent = new Event();
	$objEvent->contactId = $event_contact_id;
	$objEvent->userId = $user_id;
	$objEvent->eventTypeId = $event_type;
	$objEvent->eventSubject = $event_subject;	
	$objEvent->eventDur = $event_hour.':'.$event_minute;
	$objEvent->saveType = 'EVENT';
	$objEvent->eventStartDate = $event_start_date;
	$objEvent->eventStartTime = $event_start_time;
	$objEvent->dealID = $deal_id;
	$objEvent->eventId = $event_id;	
	$objEvent->eventAction = 'edit';
	$resultEvent = $objEvent->SaveEvent();

	return ($resultEvent['STATUS'] == 'OK') ? true : false;
}

function print_result($result) {
	print("window.result = '$result';\n");
	print("</script>\n");
	close_db();
	exit();
}

/*
function getContact($contact) {

	$fname = $lname = '';	
	$contact = trim($contact);
	if (str_word_count($contact) > 1) {
		preg_match('/[^ ]*$/', $contact, $results);
		$lname = count($results > 0) ? $results[0] : '';
	}
		
	$fname = str_replace($lname, '', $contact);	
	$fname = preg_replace('#\s{2,}#',' ',$fname);
		
	$lname = trim($lname);
	$fname = trim($fname);
	
	$name['FirstName'] = $fname;
	$name['LastName'] = $lname;
	
	return $name;	
}
*/

function getContact($contact) {

		$contact = trim($contact);
		$parts = explode(' ', $contact);

		$count = count($parts);
		
		if($count > 1){
			for($i=0;$i<$count-1;$i++){
				$name['FirstName'] .= $parts[$i] . ' ';
			}
			
			$name['FirstName'] = trim($name['FirstName']);
			$name['LastName'] = trim($parts[$count-1]);
		}
		else {
			$name['FirstName'] = trim($parts[0]);
			$name['LastName'] = '';
		}		
		return $name;		
}


// returns all the records from db in an array
function getRecord($sql) {	
	$record = array();
	$result = mssql_query($sql) or die("MS-Query Error in select-query");
	while ($row = mssql_fetch_assoc($result)) {
		$record[] = $row;
	}
	return $record;
}

?>