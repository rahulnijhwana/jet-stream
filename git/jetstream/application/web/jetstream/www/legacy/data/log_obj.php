<?php
/**
 * @package Shared
 */
include_once('settings.php');
include_once('string_func.php');
Logfile::Open($log_file);

/**
 * Logging Utility
 *
 * Call LogFile::WriteLine(<message>) to write the line to the log defined
 * in settings.php
 * 
 * Note:  Class methods require for PHP5 and up
 */ 
class LogFile
{
    /**
     * A constant file pointer to the logfile.
     * @var pointer $handle
     */
	static protected $handle;

    /**
     * A switch that indicates that the log can be successfully written to or not.
     * @var bool $writable
     */
	static protected $writable = FALSE;

    /**
     * A random 5 character string to identify this call from other calls in the log.
     * @var string $log_id
     */
	static protected $log_id;

    /**
     * Open the log and create an entry with the calling function.
     *
     * Note:  Fails silently
     *
     * @access public
     * @param string  $file_name       The full path of the log text file to open.
     */
	public static function Open($file_name)
	{
        if (!isset(self::$log_id)) {
    		self::$log_id = RandomString(5);
    	}
		if (is_writable($file_name)) {
			self::$handle = fopen($file_name, "a");
			self::$writable = TRUE;
			self::WriteLine("### Log Opened by " . $_SERVER['PHP_SELF'] . " ###");
		}
		else {
			// echo "Log is unwritable...<br>";
			self::$writable = FALSE;
		}
	}

    /**
     * Write a line to the log.  Prepends a time stamp and a unique ID.
     *
     * @access public
     * @param string  $message       The text to write to the logfile.  
     */
	public static function WriteLine($message)
	{
		if (self::$writable != FALSE) {
			$log_time = date("Ymd H:i:s ");
			$log_text = "$log_time " . self::$log_id . " $message\n";
			if (fwrite(self::$handle, $log_text) === FALSE) {
				self::$log_id = FALSE;
			}
		}
	}
}
?>