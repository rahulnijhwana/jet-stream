<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

$sql = "SELECT A.$account_name as Company, Opportunities.Division, (C.$contact_first_name + ' ' + C.$contact_last_name) AS Contact FROM Opportunities 
	LEFT JOIN Account A on Opportunities.AccountID = A.AccountID
	LEFT JOIN Contact C on Opportunities.ContactID = C.ContactID
	where (PersonID = $reqPersonId) and A.$account_name is not null group by A.$account_name, Division, C.$contact_first_name, C.$contact_last_name
	order by A.$account_name, Division, C.$contact_first_name, C.$contact_last_name";
dump_sql_as_array('g_company_list', $sql);
?>