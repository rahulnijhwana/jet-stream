<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

print("// ----- get_people -----\n\n");

dump_sql_as_array('g_people', "SELECT * FROM people WHERE CompanyID = '$mpower_companyid' AND Deleted = 0 ORDER BY LastName");
dump_sql_as_array('g_removed_people', "SELECT * FROM people WHERE CompanyID = '$mpower_companyid' AND Deleted = 1");

?>
