<?php
/**
 * @package Data
 */

define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');
require_once(BASE_PATH . '/include/mpconstants.php');
require_once(BASE_PATH . '/slipstream/class.Note.php');
require_once BASE_PATH . '/include/class.DashboardSearchCreatorNew.php';

SessionManager::Init();
SessionManager::Validate();

require_once('_base_utils.php');
require_once('_insert_utils.php');
require_once('_opps_utils.php');

require 'jetstream_events.php';

writeinslog("Inserting Opps");
   
print("<script language=\"JavaScript\">\n");

if (!check_user_login())
	print_result('Invalid login.');
if (!isset($mpower_data) || !strlen($mpower_data))
	print_result('No data to save.');
if (!isset($mpower_fieldlist) || !strlen($mpower_fieldlist))
	print_result('Field list is missing.');

$field_array = explode(',', $mpower_fieldlist);
$field_count = count($field_array);
$DealID_index = -1;
$category_index = -1;
$fm_date_index = -1;
$fm_time_index = -1;
$nm_date_index = -1;
$nm_time_index = -1;
$personid_index = -1;

for ($k = 0; $k < $field_count; ++$k)
{
	if ($field_array[$k] == 'DealID') {
		$DealID_index = $k;		
	} else if (strtolower($field_array[$k]) == 'category') {
		$category_index = $k;
	} else if (strtolower($field_array[$k]) == 'firstmeeting') {
		$fm_date_index = $k;
	} else if (strtolower($field_array[$k]) == 'firstmeetingtime') {
		$fm_time_index = $k;
	} else if (strtolower($field_array[$k]) == 'nextmeeting')	{
		$nm_date_index = $k;
	} else if (strtolower($field_array[$k]) == 'nextmeetingtime') {
		$nm_time_index = $k;
	} else if (strtolower($field_array[$k]) == 'personid') {
		$personid_index = $k;
	}
}

$rows = explode("\n", str_replace("\r\n","\n",$mpower_data));

$ok = true;
$len = count($rows);
$newids = 'new Array(';

print("</script>\n");
print('<div id="errmsg">');

for ($i = 0; $i < $len; ++$i) {
	
	$mpower_data_arr = explode("|NOTE|", $rows[$i]);
	$noteArr = array();
	if(strlen($mpower_data_arr[1]) > 0) {
		$noteArr = explode('|', $mpower_data_arr[1]);
	}
	$rows[$i] = $mpower_data_arr[0];
	writeinslog("Inserting One Opp");
	if ($i > 0) $newids .= ',';
	$temp_array = explode('|', $rows[$i]);
	$newids .= $temp_array[$DealID_index];

	$contact = array_pop($temp_array);
	$company = array_pop($temp_array);
	
	$contact = str_replace("'", "''", str_replace("\\\"", "\"", str_replace("\\'","'", $contact)));
	
	$contact_name = getContact($contact);	
	$fname = $contact_name['FirstName'];
	$lname = $contact_name['LastName'];
		
	$company = trim($company);
	$company = str_replace("'", "''", str_replace("\\\"", "\"", str_replace("\\'","'", $company)));
		
	// account and opp
	$accountmap_records = mssql_query("SELECT AccountID, PrivateAccount, Inactive FROM Account WHERE " . $_SESSION['account_name'] . " = '$company' AND CompanyID = $mpower_companyid AND Deleted != 1");	
	$acc_rs = mssql_fetch_array($accountmap_records);
	
	if (count($acc_rs) > 0 && isset($acc_rs['AccountID'])) {
		$accountid = $acc_rs['AccountID'];
		
		if($acc_rs['PrivateAccount'] == 1) {
			mssql_query("UPDATE Account SET PrivateAccount = 0 WHERE AccountID = '".$accountid."'");
		}
		if($acc_rs['Inactive'] == 1) {
			mssql_query("UPDATE Account SET Inactive = 0 WHERE AccountID = '".$accountid."'");
		}
	} else {			
		$post = array();
		$post['rec_type'] = 'account';
		$post[$_SESSION['account_name']] = $company;	
	
		//create account
		$accountid = create_company_contact($post);
		//$result = mssql_query("INSERT INTO Account(CompanyID, " . $_SESSION['account_name'] . ") VALUES($mpower_companyid, '$company')");	
		//$accountid = mssql_insert_id();
		
		//if($result) {
			//mssql_query("INSERT INTO PeopleAccount(AccountID, PersonID, CreatedBy, AssignedTo) VALUES('$accountid', '" . $_SESSION['login_id'] . "', 1, 1)");
		//}
	}
	
	if(isset($accountid) && ($accountid > 0)) {
		$obj_search = new DashboardSearchCreator();
		$obj_search->company_id = $mpower_companyid;
		$obj_search->record_id = $accountid;
		$obj_search->StoreAccountRecords();
	}
	
	$where = '';
	
	if (!empty($fname) && empty($lname)) {
		$where = $_SESSION['contact_first_name'] . " = '$fname'";
	}	
	else if(!empty($fname) && !empty($lname)) {
		$where = $_SESSION['contact_first_name'] . " = '$fname' AND " . $_SESSION['contact_last_name'] . " = '$lname'";
	}	
	
	if($where != '') {
		// contact and opp
		$contactmap_records = mssql_query("SELECT ContactID, PrivateContact, Inactive  FROM Contact WHERE $where AND AccountID = $accountid AND Deleted != 1");
		$contact_rs = mssql_fetch_array($contactmap_records);
		
		if (count($contact_rs) > 0 && isset($contact_rs['ContactID'])) {
			$contactid = $contact_rs['ContactID'];		
			
			if($contact_rs['PrivateContact'] == 1) {
				mssql_query("UPDATE Contact SET PrivateContact = 0 WHERE ContactID = '".$contactid."'");
			}
			if($contact_rs['Inactive'] == 1) {
				mssql_query("UPDATE Contact SET Inactive = 0 WHERE ContactID = '".$contactid."'");
			}			
		} else {		
			
			if(!empty($fname))	{
				
				/*
				$result = mssql_query("INSERT INTO Contact(AccountID, CompanyID, " . $_SESSION['contact_first_name'] . ", " . $_SESSION['contact_last_name'] . ") VALUES($accountid, $mpower_companyid, '$fname', '$lname')");
				$contactid = mssql_insert_id();
				if($result) {
					mssql_query("INSERT INTO PeopleContact(ContactID, PersonID, CreatedBy, AssignedTo) VALUES('$contactid', '" . $_SESSION['login_id'] . "', 1, 1)");
				}
				*/
			
				$post = array();
				$post['rec_type'] = 'contact';				
				$post['AccountID'] = $accountid;
				$post[$_SESSION['contact_first_name']] = $fname;	
				$post[$_SESSION['contact_last_name']] = $lname;			
			
				//create contact
				$contactid = create_company_contact($post);		
			}
		}
		if(isset($contactid) && ($contactid > 0)) {
			$obj_search = new DashboardSearchCreator();
			$obj_search->company_id = $mpower_companyid;
			$obj_search->record_id = $contactid;
			$obj_search->StoreContactRecords();
		}
	} else {
		$contactid = 'NULL';
	}

	$mpower_fieldlist = str_replace(',Company,Contact', '', $mpower_fieldlist);	
	
	/* To set the values of last four columns(AccountID, ContactID, IsFMCancelled, IsNMCancelled) added to the opportunity. */
	$temp_array[count($temp_array) - 7] = $accountid;	
	$temp_array[count($temp_array) - 6] = $contactid;	
	$temp_array[count($temp_array) - 5] = 0;	
	
	$values = implode('|', $temp_array);
	$result = insert_row('opportunities', $mpower_fieldlist, $values, 'DealID', 'ChangedByID,CallIn');
	
	if ($result != false) {
		//$id_result = mssql_query("SELECT @@IDENTITY AS 'DealID'");
		$last_inserted_deal_id = mssql_insert_id();
		$newids .= ',';
		$newids .= $last_inserted_deal_id;

		//$user_id = $_POST['reqPersonId'];
		
		$user_id = $temp_array[$personid_index];		
		$today_date = date('Y-m-d H:i:s.000');
		$category_val = $temp_array[$category_index];
		
		if($category_index != -1 && $contactid > 0) {
			SaveJetEvents($_POST['jet_events'], $last_inserted_deal_id, $contactid);
		}
		
		
//		// check if the contact is provided with a valid category		
//		if($category_index != -1 && $contactid > 0) {
//			//$category_val = $temp_array[$category_index];			
//			$fm_event_start_date = $_POST['fm_event_start_date'];
//			$nm_event_start_date = $_POST['nm_event_start_date'];
//			$event_contact_id = $contactid;
//			$fm_event_id = $nm_event_id = NULL;
//			
//			// if the first meeting event date is provided then create an event for this opportunity
//			if (!empty($fm_event_start_date)) {			
//				// create event for first meeting 
//				$event_subject = $_POST['fm_event_subject'];				
//				$event_type = $_POST['fm_event_type'];					
//				$event_duration = $_POST['fm_event_duration'];				
//				$event_duration = str_replace(array('AM', 'PM'), '', $event_duration);
//				$event_duration = trim($event_duration);
//				$event_hour = $event_minute = 0;
//				list($event_hour, $event_minute) = split(':', $event_duration, 2);				
//				$event_description = $_POST['fm_event_description'];	
//
//				// Check the meeting status and update the created event
//				$fmMeetingStatus = $_POST['fmMeetingStatus'];					
//				$fmMeetingDialogNote = $_POST['fmMeetingDialogNote'];	
//				$fm_event_start_time = $_POST['fm_event_start_time'];				
//				
//				$is_cancelled = $is_closed = 0;					
//				if(!empty($nm_event_start_date) || $fmMeetingStatus == 'CLOSED') $is_closed = 1;
//				else if ($fmMeetingStatus == 'CANCELLED' || $category_val == 10) $is_cancelled = 1;				
//				
//					
//				// create event
//				$ok = create_event($event_contact_id, $user_id, $event_type, $event_subject, $today_date, $event_hour, $event_minute, $fm_event_start_date, $fm_event_start_time, $last_inserted_deal_id, $is_cancelled, $is_closed, true);
//				
//			    if ($ok) {
//					// get the created event id
//					$fm_event_id = mssql_insert_id();
//					
//					// create a note for event	
//					create_note($user_id, $today_date, $event_subject, $event_description, NOTETYPE_EVENT, $fm_event_id, 0);	
//					
//					if (!empty($fmMeetingDialogNote)) {					
//						// create a note for event	
//						create_note($user_id, $today_date, $event_subject, $fmMeetingDialogNote, NOTETYPE_EVENT, $fm_event_id, 0);	
//					}				
//				}
//				
//				if ($is_cancelled == 1) $fm_event_id = NULL;
//				
//				/*
//				if($category_val == 6) {
//					// close the first meeting event
//					$ok = update_event_status($fm_event_id, 0, 1);				
//				} else if($category_val == 9) {
//					// cancel the first meeting event
//					$ok = update_event_status($fm_event_id, 1);
//				}*/	
//			}
//			
//			// create event for next meeting 
//			if (!empty($nm_event_start_date)) {
//			
//				// create event for next meeting 
//				$event_subject = $_POST['nm_event_subject'];			
//				$event_type = $_POST['nm_event_type'];		
//				$event_duration = $_POST['nm_event_duration'];				
//				$event_duration = str_replace(array('AM', 'PM'), '', $event_duration);
//				$event_duration = trim($event_duration);
//				$event_hour = $event_minute = 0;
//				list($event_hour, $event_minute) = split(':', $event_duration, 2);				
//				$event_description = $_POST['nm_event_description'];
//				$nmMeetingStatus = $_POST['nmMeetingStatus'];
//				$nmMeetingDialogNote = $_POST['nmMeetingDialogNote'];
//				
//				$is_closed = ($nmMeetingStatus == 'CLOSED') ? 1 : 0;
//				$is_cancelled = ($nmMeetingStatus == 'CANCELLED' || $category_val == 10) ? 1 : 0;				
//				$nm_event_start_time = $_POST['nm_event_start_time'];	
//				
//				
//				// create event
//				$ok = create_event($event_contact_id, $user_id, $event_type, $event_subject, $today_date, $event_hour, $event_minute, $nm_event_start_date, $nm_event_start_time, $last_inserted_deal_id, $is_cancelled, $is_closed);
//				
//			    if ($ok) {
//					$nm_event_id = mssql_insert_id();
//					// create a note for event	
//					create_note($user_id, $today_date, $event_subject, $event_description, NOTETYPE_EVENT, $nm_event_id, 0);	
//
//					if (!empty($nmMeetingDialogNote)) {					
//						// create a note for event	
//						create_note($user_id, $today_date, $event_subject, $fmMeetingDialogNote, NOTETYPE_EVENT, $nm_event_id, 0);	
//					}					
//				}				
//				
//				if ($is_cancelled == 1) $nm_event_id = NULL;
//			}
//			
//			$resultopp = mssql_query("UPDATE opportunities SET FMEventID = '$fm_event_id', NMEventID = '$nm_event_id' WHERE DealID = '$last_inserted_deal_id'");
//			if (!$resultopp) {
//				$ok = false;
//				writeinslog("Error Updating EventId in Opp table -- ".mssql_get_last_message());
//			}
//		}		
//		
		if(is_array($noteArr) && count($noteArr) > 0 && !empty($noteArr) && isset($noteArr)) {
			$note_subject = isset($noteArr['0']) ? $noteArr['0'] : '';
			$note_text = isset($noteArr['0']) ? $noteArr['1'] : '';			
			
			if(!empty($note_subject) && $note_subject != 'undefined') {
				$note_subject = str_replace('&##124#;', '|', $note_subject);
				$note_text = str_replace('&##124#;', '|', $note_text);
				$note_private = $noteArr['2'];
				
				if($note_private != '1') $note_private = 0;
				
				create_note($user_id, $today_date, $note_subject, $note_text, NOTETYPE_OPPORTUNITY, $last_inserted_deal_id, $note_private);
			}
		}
	}

	if (!$result) {
		$ok = false;
		writeinslog("Error Inserting Opp--failure to get new id");
	}
}

$newids .= ');';

//include('calc_salescycles.php');
print('</div>');
print("<script language=\"JavaScript\">\n");

if (!$ok) {
	writeinslog("Error Inserting Opp--bad return code");
	print_result('"Insert opportunities failed."');
} else {
	writeinslog("Inserting Opps--Success");
	print_result($newids);
}



close_db();
?>
