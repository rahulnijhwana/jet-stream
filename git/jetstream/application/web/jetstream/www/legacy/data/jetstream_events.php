<?php
require_once(BASE_PATH . '/slipstream/class.Event.php');
require_once BASE_PATH . '/slipstream/class.ModuleEventNew.php';


function SaveJetEvents($event_data, $deal_id, $contact_id) {
// #### Jetstream Event Handler ####

	// How events are stored in EditOpp:
	// array( event_id => array(field => value), ...), ...)

	// If this is a new event, the event_id will be -1 for NM and -2 for FM
	
	// Get Event Data
	$events = json_decode($event_data, TRUE);

	if (!is_array($events)) {
		error_log("JSON Decode Failed: $event_data");
		return;
	}
		
	if (!is_array($events)) {
		error_log("JSON Decode Failed: $event_data");
		return;
	}
	
	// Iterate through and create / update events
	foreach ($events as $event_id => $event) {
		$event['DealID'] = $deal_id;
		
		$event['ContactID'] = $contact_id;

//		$event_handler = new ModuleEventNew();
		
		// Determine what the ID is for this event (-1 for a new one)
		if ($event_id < 0) {
			$form_type = ADD;
		} else {
			$form_type = EDIT;
		}

		// Load the record from the database
		$sql = 'SELECT * FROM Event WHERE EventID = ?';
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $event_id));
		$recordset = DbConnManager::GetDb('mpower')->Execute($sql, 'RecEvent');
		
		if ($form_type == ADD) {
			$record = new RecEvent();
			$recordset[] = $record;
			$record->SetDatabase(DbConnManager::GetDb('mpower'));
		} else {
			$record = $recordset[0];
		}
		$record->Initialize();
		
		// Update object with the fields from the form
		foreach ($event as $field => $value) {
			if ($field != 'attendees') {
				$record->$field = $value;
			}
		}
		
		// file_put_contents('c:/test.txt', "Record: " . print_r($record, TRUE) . PHP_EOL, FILE_APPEND);
		
		// Save event object
		$record->Save();
		
		$save_id = $record->EventID;

		// Update opportunity record for each created event
		if ($save_id > 0 && $event_id < 0) {
			$save_field = ($event_id == -1) ? 'NMEventID' : 'FMEventID';
			$opp_event_sql = "UPDATE Opportunities Set $save_field = ? WHERE DealID = ?";
			$opp_event_sql = SqlBuilder()->LoadSql($opp_event_sql)->BuildSql(array(DTYPE_INT, array($save_id, $event['DealID'])));
			DbConnManager::GetDb('mpower')->Exec($opp_event_sql);
		}
		
		// file_put_contents('c:/test.txt', "ID: $save_id" . PHP_EOL . PHP_EOL, FILE_APPEND);
		
		// Save attendees
		if (!empty($event['attendees'])) {
			// Load all attendees of this record, index by PersonID
			$attendee_sql = 'SELECT * FROM EventAttendee WHERE EventID = ?';
			$attendee_sql = SqlBuilder()->LoadSql($attendee_sql)->BuildSql(array(DTYPE_INT, $record->EventID));
			$attendee_set = DbConnManager::GetDb('mpower')->Execute($attendee_sql, 'RecEventAttendee', 'PersonID');
			
			// Set all their delete flags (will be cleared as we process each rec from the web page)
			$attendee_set->SetAll('Deleted', true);
			
			// First attendee is creator
			$first = true;
			foreach($event['attendees'] as $attendee) {
				if (!$attendee_set->InRecordset($attendee)) {
					$attendee_rec = new RecEventAttendee();
					$attendee_set[$attendee] = $attendee_rec;
					$attendee_rec->SetDatabase(DbConnManager::GetDb('mpower'));
					//$attendee_set->Initialize();
					$attendee_rec->EventID = $record->EventID;
					$attendee_rec->PersonID = $attendee;
				}
				if ($first) {
					$attendee_set[$attendee]->Creator = 1;
					$first = false;
				} else {
					$attendee_set[$attendee]->Creator = 0;
				}
				$attendee_set[$attendee]->Deleted = false;
			}
			$attendee_set->Initialize()->Save();
		}
		
		// Save Notes
		if (!empty($event['Note'])) {
			// Handle note
			$note = new Note();
			$note->noteCreator = $_SESSION['USER']['USERID'];
			$note->noteCreationDate = date("Y-m-d H:i:s");
			$note->noteSubject = (isset($event['Closed']) && $event['Closed']) ? 'Event Completed in M-Power' : 'Event Created in M-Power';
			$note->noteText = nl2br($event['Note']);
			$note->noteObjectType = NOTETYPE_EVENT;
			$note->noteObject = $record->EventID;
			$result_note = $note->createNote();
		}
	}
}
