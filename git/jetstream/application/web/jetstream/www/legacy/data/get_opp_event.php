<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

echo "// ----- get_opp_evets -----\n\n";

$opp_id = filter_input(INPUT_GET, 'reqOpId', FILTER_SANITIZE_NUMBER_INT);

$event_sql = "SELECT EventID, Closed, Cancelled FROM Event WHERE
	eventid = (SELECT FMEventID FROM Opportunities WHERE DealID = '$opp_id')
	OR eventid = (SELECT NMEventID FROM Opportunities WHERE DealID = '$opp_id')";

$event_result = mssql_query($event_sql);

$events = array();
while($result = mssql_fetch_array($event_result)) {
	$events[$result['EventID']]	= array('Closed' => $result['Closed'], 'Cancelled' => $result['Cancelled']);
}

// Ensure it comes over as an object
$json_events = (count($events) > 0) ? json_encode($events) : '{}';

echo "var jet_events = $json_events;\n";

exit;

$fm_event_subject = $fm_event_start_date = $fm_event_start_time = $fm_event_duration = $fm_event_description = $fm_event_type = '';
$nm_event_subject = $nm_event_start_date = $nm_event_start_time = $nm_event_duration = $nm_event_description = $nm_event_type = '';	
$nm_event_id = $fm_event_id = $is_cancelled = $is_closed = $fm_is_cancelled = $fm_is_closed = 0;


if($opp_id > 0) {
	$sql = "SELECT FMEventID, NMEventID FROM opportunities WHERE DealID = '$opp_id'"; 
	$result = mssql_query($sql);
	$res = mssql_fetch_array($result); 
	
	$fm_event_id = (int) $res['FMEventID'];
	$nm_event_id = (int) $res['NMEventID'];	
	
	if($fm_event_id > 0) {
		$sql="SELECT CONVERT(varchar(20), StartDate) AS StartDate, Subject, EventTypeID, DurationHours, DurationMinutes, IsCancelled, IsClosed FROM Events 
				WHERE EventID = '$fm_event_id'";
		$rs = mssql_query($sql);
		$result = mssql_fetch_array($rs);
		
		if(count($result) > 0) {
			$ts = strtotime($result['StartDate']);	
			$fm_event_subject = addslashes($result['Subject']);				
			$fm_event_type = 'EVENT_'.$result['EventTypeID'];			
			$fm_event_start_date = date('m/d/Y', $ts);
			$fm_event_start_time = date('g:i A', $ts);
			$fm_event_duration = $result['DurationHours'] .':'.(!empty($result['DurationMinutes']) ? $result['DurationMinutes'] : '00');
			$fm_event_description = '';
			
			$fm_is_cancelled = $result['IsCancelled'];	
			$fm_is_closed = $result['IsClosed'];
		}
	}
	
	/*$nm_closed = false;
	
	if($nm_event_id > 0) {
			$sql = "SELECT EventID FROM events WHERE EventID = $nm_event_id AND isClosed = 1"; 
			$event_rs = mssql_query($sql);
			$event_res = mssql_fetch_array($event_rs); 
			if(is_array($event_res) && count($event_res) > 0) {				
				$nm_closed = true;
			}
	} */	
		
	if($nm_event_id > 0) {
		$sql="SELECT CONVERT(varchar(20), StartDate) AS StartDate, Subject, EventTypeID, DurationHours, DurationMinutes, IsCancelled, IsClosed FROM Events 
				WHERE EventID = '$nm_event_id'";
		$rs = mssql_query($sql);
		//echo '//'.$sql.';'."\n";
		$result = mssql_fetch_array($rs);
		
		if(is_array($result) && count($result) > 0) {
			$ts = strtotime($result['StartDate']);				
			$nm_event_subject = addslashes($result['Subject']);	
			
			$is_cancelled = $result['IsCancelled'];	
			$is_closed = $result['IsClosed'];
			
			$nm_event_type = 'EVENT_'.$result['EventTypeID'];
			$nm_event_start_date = date('m/d/Y', $ts);
			$nm_event_start_time = date('g:i A', $ts);
			$nm_event_duration = $result['DurationHours'] .':'.(!empty($result['DurationMinutes']) ? $result['DurationMinutes'] : '00');
			$nm_event_description = '';			
			echo 'modifyNextMeetingEvent = true;'."\n\n";			
		} else {
			$nm_event_id = 0;
		}
	}
}

echo 'FMEventEditor = new Array();'."\n\n";
echo "FMEventEditor['Subject'] = '$fm_event_subject';"."\n\n";
echo "FMEventEditor['EventType'] = '$fm_event_type';"."\n\n";
echo "FMEventEditor['StartDate'] = '$fm_event_start_date';"."\n\n";
echo "FMEventEditor['StartTime'] = '$fm_event_start_time';"."\n\n";
echo "FMEventEditor['Duration'] = '$fm_event_duration';"."\n\n";
echo "FMEventEditor['Description'] = '$fm_event_description';"."\n\n";
echo "FMEventEditor['FMEventID'] = '$fm_event_id';"."\n\n";
echo "FMEventEditor['FMEventIsCancelled'] = '$fm_is_cancelled';"."\n\n";
echo "FMEventEditor['FMEventIsClosed'] = '$fm_is_closed';"."\n\n";
echo "FMEventEditor['SaveEvent'] = false;"."\n\n";





echo 'NMEventEditor = new Array();'."\n\n";
echo "NMEventEditor['Subject'] = '$nm_event_subject';"."\n\n";
echo "NMEventEditor['EventType'] = '$nm_event_type';"."\n\n";
echo "NMEventEditor['StartDate'] = '$nm_event_start_date';"."\n\n";
echo "NMEventEditor['StartTime'] = '$nm_event_start_time';"."\n\n";
echo "NMEventEditor['Duration'] = '$nm_event_duration';"."\n\n";
echo "NMEventEditor['Description'] = '$nm_event_description';"."\n\n";
echo "NMEventEditor['NMEventID'] = '$nm_event_id';"."\n\n";
echo "NMEventEditor['NMEventIsCancelled'] = '$is_cancelled';"."\n\n";
echo "NMEventEditor['NMEventIsClosed'] = '$is_closed';"."\n\n";
echo "NMEventEditor['SaveEvent'] = false;"."\n\n";

?>