<?php
/**
 * @package Data
 */

if (!defined('BASE_PATH')) define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/lib.date.php';
SessionManager::Init();

/* Set the $check_eula to true.
if the $check_eula is false, then it will not check for any eula value, while loading the application.
This is needed to load the company autocomplete field through ajax.
Else it was not working in jetstream unless and until the mpower eula is accepted. */
SessionManager::$check_eula	= false;
SessionManager::Validate();

include_once('_base_utils.php');
include_once('_query_utils.php');

$company = str_replace("'", "''", str_replace("\\\"", "\"", str_replace("\\'","'", strtolower($_GET["term"]))));
if(!$company){
	return;
}

if(isset($_GET['fromcontact']) && ($_GET['fromcontact'])) {
	$con_mapped_fields = array();
	$acc_mapped_fields = array();
	$sel_fields = '';
	
	if(isset($_SESSION['USER']['CONTACTACCOUNTMAP']) && count($_SESSION['USER']['CONTACTACCOUNTMAP']) > 0) {
		foreach($_SESSION['USER']['CONTACTACCOUNTMAP'] as $val) {
			$con_mapped_fields[] = $val['ContactFieldName'];
			$acc_mapped_fields[] = $val['AccountFieldName'];
		}
		
		if(count($acc_mapped_fields > 0)) {
			foreach($acc_mapped_fields as $mfield) {
				if(stripos($mfield, 'date') !== false) {
					$sel_fields .= ', Convert(varchar, A.'.$mfield.', 20) AS'.$mfield;
				}
				else {
					$sel_fields .= ', A.'.$mfield;
				}
			}
		}		
		$has_mapped_fields = true;
		$has_company_fields = false;
	}
}
else if(isset($_GET['fromcompany']) && ($_GET['fromcompany'])) {
	$acc_mapped_fields = array();
	$sel_fields = '';
	
	if(isset($_SESSION['USER']['ACCOUNTMAP']) && count($_SESSION['USER']['ACCOUNTMAP']) > 0) {
		foreach($_SESSION['USER']['ACCOUNTMAP'] as $val) {
			$acc_mapped_fields[] = $val['FieldName'];
		}
		
		if(count($acc_mapped_fields > 0)) {
			foreach($acc_mapped_fields as $mfield) {
				if(stripos($mfield, 'date') !== false) {
					$sel_fields .= ', Convert(varchar, A.'.$mfield.', 20) AS '.$mfield;
				}
				else {
					$sel_fields .= ', A.'.$mfield;
				}
			}
		}		
		$has_company_fields = true;
		$has_mapped_fields = false;
	}
}
else {	
	$sel_fields = '';
	$has_mapped_fields = false;
	$has_company_fields = false;
}

$sql_check_assigned_user = '';
if(isset($_SESSION['company_obj']['LimitAccessToUnassignedUsers']) && ($_SESSION['company_obj']['LimitAccessToUnassignedUsers'] == 1)) {
	$sql_check_assigned_user .= ' INNER JOIN PeopleAccount AS PA ON A.AccountID = PA.AccountID AND PA.AssignedTo = 1 AND PA.PersonID = '.$_SESSION['USER']['USERID'];
}

$sql = "SELECT A.$account_name as Company, A.AccountID $sel_fields	
	From Account A 	
	$sql_check_assigned_user
	where (A.CompanyID = $mpower_companyid) and A.$account_name like '$company%'
	AND A.Inactive != 1 AND A.PrivateAccount != 1 AND A.Deleted != 1";
	
$data = array();

if ($result = mssql_query($sql)) {
	while($row = mssql_fetch_array($result)) {
		$indx = count($data);
		//$data[$indx][$row['AccountID']] = $row['Company'];
		$data[$indx]['AccountID'] = $row['AccountID'];
		$data[$indx]['Account'] = $row['Company'];
		
		if($has_mapped_fields) {	
			foreach($con_mapped_fields as $key => $val) {
				if(stripos($acc_mapped_fields[$key], 'date') !== false) {
					if(trim($row[$acc_mapped_fields[$key]]) != '') {
						$data[$indx][$val] = ReformatDate($row[$acc_mapped_fields[$key]]);
					}
					else {
						$data[$indx][$val] = '';
					}
				}
				else {
					$data[$indx][$val] = $row[$acc_mapped_fields[$key]];
				}
			}
		}
		
		if($has_company_fields) {	
			foreach($acc_mapped_fields as $key => $val) {
				if(stripos($val, 'date') !== false) {
					if(trim($row[$val]) != '') {
						$data[$indx][$val] = ReformatDate($row[$val]);
					}
					else {
						$data[$indx][$val] = '';
					}
				}		
				else {
					$data[$indx][$val] = $row[$val];
				}
			}
		}
	}		
}
if (!empty($data)) {
	echo json_encode($data);
}
/*foreach ($data as $key=>$value) {
	if (strpos(strtolower($key), $company) !== false) {
		echo "$key,$value\n";
	}
}*/

?>
