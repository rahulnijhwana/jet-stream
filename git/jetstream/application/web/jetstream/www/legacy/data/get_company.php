<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

print("// ----- get_company -----\n\n");

$query = "SELECT * FROM company WHERE CompanyID = ?";
$query = SqlBuilder()->LoadSql($query)->BuildSql(array(DTYPE_INT, $mpower_companyid));
$row = DbConnManager::GetDb('mpower')->Execute($query);

$currentCompany=$row;
if (isset($company_lean_and_mean) && $company_lean_and_mean)
{
	$row['Requirement1trend'] = '';
	$row['Requirement2trend'] = '';
}

$record = array();
foreach($row->GetFieldDef() as $key=>$comp) {
	$record[$key] = $row[0][$key];
}

dump_assoc_as_array('g_company', $record);

?>