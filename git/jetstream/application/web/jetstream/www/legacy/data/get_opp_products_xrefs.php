<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!check_user_login()) exit();

print("// ----- get_opp_products -----\n\n");
$sql = "SELECT * FROM Opp_Product_XRef where CompanyID IN ($companyList) order by DealID, Seq ";
dump_sql_as_array('g_opp_products_xref', $sql);
?>
