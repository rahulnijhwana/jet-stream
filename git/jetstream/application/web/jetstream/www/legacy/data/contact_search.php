<?php
/**
 * @package Data
 */

include_once('_base_utils.php');
include_once('_query_utils.php');

if (!defined('BASE_PATH')) define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/class.MapLookup.php';

SessionManager::Init();
/* Set the $check_eula to true.
if the $check_eula is false, then it will not check for any eula value, while loading the application.
This is needed to load the contact autocomplete field through ajax.
Else it was not working in jetstream unless and until the mpower eula is accepted. */
SessionManager::$check_eula	= FALSE;
SessionManager::Validate();

$company = '';
if(isset($_GET["company"])){
	$company = str_replace("'", "''", str_replace("\\\"", "\"", str_replace("\\'","'", strtolower($_GET["company"]))));
}

$contact = str_replace("'", "''", str_replace("\\\"", "\"", str_replace("\\'","'", strtolower($_GET["term"]))));

if(!$contact){
	return;
}

$sql_check_assigned_user = '';
$sql_assigned_user = '';
if(isset($_SESSION['company_obj']['LimitAccessToUnassignedUsers']) && ($_SESSION['company_obj']['LimitAccessToUnassignedUsers'] == 1)) {
	$sql_check_assigned_user .= ' INNER JOIN PeopleContact AS PC ON C.ContactID = PC.ContactID AND PC.AssignedTo = 1 AND PC.PersonID = '. $_SESSION['USER']['USERID'];
	$sql_assigned_user .= ' And PeopleContact.PersonID = ' . $_SESSION['USER']['USERID'];
}

$mpower_companyid = $_SESSION['company_id']; 

$contact_first_name = MapLookup::GetContactFirstNameField($mpower_companyid, TRUE);
$contact_last_name  = MapLookup::GetContactLastNameField($mpower_companyid, TRUE);
$account_name  = MapLookup::GetAccountNameField($mpower_companyid, TRUE);


$sql = "SELECT	DISTINCT C.ContactID, 
		(C.{$contact_first_name} + ' ' + C.{$contact_last_name}) AS Contact, 
		A.{$account_name} AS Account, 
		C.PrivateContact, 
		PeopleContact.PersonID
	FROM Contact C 
	{$sql_check_assigned_user}
	LEFT JOIN Account A on C.AccountID = A.AccountID 
	RIGHT OUTER JOIN PeopleContact on PeopleCOntact.ContactID = C.ContactID {$sql_assigned_user} 
	WHERE 
	C.CompanyID = {$mpower_companyid} AND (A.Inactive != 1 OR A.Inactive IS NULL) AND 
	(C.{$contact_first_name} like '{$contact}%' OR C.{$contact_last_name} like '{$contact}%') AND 
	C.Inactive != 1 AND  C.Deleted != 1";


if ( isset($_GET['email_hold']) ) $sql .= " AND 8448 IN (SELECT PersonID from PeopleContact WHERE ContactID = C.ContactID) " ;  

/*
$sql = "SELECT C.ContactID, (C.$contact_first_name + ' ' + C.$contact_last_name) AS Contact, A.$account_name AS Account, C.PrivateContact, PeopleContact.PersonID 
	FROM Contact C 
	$sql_check_assigned_user
	LEFT JOIN Account A on C.AccountID = A.AccountID
	RIGHT OUTER JOIN PeopleContact on PeopleCOntact.ContactID = C.ContactID And PeopleContact.PersonID = {$_SESSION['USER']['USERID']}
	WHERE (A.CompanyID = $mpower_companyid) AND A.Inactive != 1 AND (A.PrivateAccount != 1 OR " . $_SESSION['USER']['USERID'] . " IN (SELECT PersonID from PeopleAccount WHERE AccountID = A.AccountID))
	AND (C.$contact_first_name like '$contact%' OR C.$contact_last_name like '$contact%')   
	AND C.Inactive != 1 AND (C.PrivateContact != 1 OR " . $_SESSION['USER']['USERID'] . " IN (SELECT PersonID from PeopleContact WHERE ContactID = C.ContactID)) AND C.Deleted != 1";
*/	

if(trim($company) != '') {
	$sql .= " AND A.$account_name = '$company'";
}
$data = array();

if ($result = mssql_query($sql)) {
	while($row = mssql_fetch_array($result)) {	
		$indx = count($data);
		if(trim($company) != '') {	
			//$data[$indx][$row['ContactID']] = $row['Contact'];
			$data[$indx]['ContactID'] = $row['ContactID'];
			$data[$indx]['Contact'] = $row['Contact'];
			$data[$indx]['test'] = 'test_val';	
		}
		else {
			//$data[$indx][$row['ContactID']] = $row['Contact'].' ('.$row['Account'].')';			
			$data[$indx]['ContactID'] = $row['ContactID'];			
			$data[$indx]['Contact'] = $row['Contact'].' ('.$row['Account'].')';			
			$data[$indx]['Subject'] = 'test_val';
		}
		$data[$indx]['PrivateContact'] = $row['PrivateContact'];
	}		
}
$empty = array('0' => 'No Results Found');

if (!empty($data)) {
	echo json_encode($data);
}
else {
	echo json_encode($empty);
}
?>
