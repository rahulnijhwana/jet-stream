<?php
/**
 * @package Data
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../../..'));

require_once(BASE_PATH . '/include/class.SessionManager.php');
require_once(BASE_PATH . '/include/class.DbConnManager.php');
require_once(BASE_PATH . '/include/class.SqlBuilder.php');

SessionManager::Init();
SessionManager::Validate();

include_once('_base_utils.php');

function print_result($result)
{
	print("window.result = '$result';\n");
	print("</script>\n");
	close_db();
	exit();
}

print("<script language=\"JavaScript\">\n");

if (!check_user_login()) print_result('Invalid login');

$mpower_remove_value = stripslashes($mpower_remove_value);
$sql = "DELETE FROM $mpower_remove_table WHERE $mpower_remove_field in ($mpower_remove_value)";

print("</script>\n");
print('<div id="errmsg">');
$result = mssql_query($sql);
print('</div>');
print("<script language=\"JavaScript\">\n");

if ($result) print_result('ok');
else print_result('Error removing records');

close_db();

?>
