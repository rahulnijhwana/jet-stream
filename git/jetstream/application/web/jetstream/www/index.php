<?php

define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.Language.php';
require_once BASE_PATH . '/include/class.fieldsMap.php';
require_once BASE_PATH . '/include/class.Browser.php';
require_once BASE_PATH . '/include/class.Redirect.php';
require_once BASE_PATH . '/include/lib.timezone.php';
require_once BASE_PATH . '/slipstream/class.Util.php';
require_once BASE_PATH . '/slipstream/class.ImapSettings.php';

Redirect::toMobile();

SessionManager::Init()->ClearToken();

if (isset($_POST['company_dir']) || isset($_GET['company'])) {
	$company_sql = "SELECT * FROM company LEFT OUTER JOIN resellers ON company.ResellerID = Resellers.ResellerID";
	$company_sql .= ' WHERE DirectoryName = ?';
	$company_dir = (isset($_POST['company_dir'])) ? $_POST['company_dir'] : $_GET['company'];
	$company_sql = SqlBuilder()->LoadSql($company_sql)->BuildSql(array(DTYPE_STRING, $company_dir));
	if ($company_record = DbConnManager::GetDb('mpower')->GetOne($company_sql)) {
		$_SESSION['company_id'] = $company_record->CompanyID;
		Language::Init($company_record);
	}
}

if (isset($_POST['company'])) $_SESSION['company_id'] = $_POST['company'];

$login_error = false;
$error_message = '';
$company_sql = "SELECT * FROM company LEFT OUTER JOIN resellers ON company.ResellerID = Resellers.ResellerID";
$is_login = false;
$eula_status = false;

if (isset($_SESSION['company_id'])) {  // Request is from a post if company_id is set in the session
	// Cleanup necessary session and cookie info
	setcookie('Cal_Day', '', time() - 3600);
	unset($_SESSION['LastSearch']);
	
	$is_login = true;
	$company_id = $_SESSION['company_id'];	
	$username = GetIfExists('username', $_POST);
	$password = GetIfExists('password', $_POST);
	$quickpass = GetIfExists('quickpass', $_SESSION);
	
	if(isset($_POST['accept_btn']))	{
		$username = $_SESSION['slip_usename'];
		$password = $_SESSION['slip_password'];
	}
	if(isset($_SESSION['slip_usename'])) {
		$username = $_SESSION['slip_usename'];
	}
	if(isset($_SESSION['slip_password'])) {
		$password = $_SESSION['slip_password'];
	}
	if($quickpass) {	
		$username = GetIfExists('username', $_GET);
		$password = 'pass';		
	}

	if ($username == 'mpowerAdmin' && $password =='n0tt0d@y1') {
		setcookie('currentuser_loginid', '');
		setcookie('mpower_pwd', $password);
		setcookie('mpower_userid', $username);
		setcookie('mpower_companyid', $company_id);
		setcookie('mpower_effective_userid', '');
		setcookie('currentuser_isadmin', '1');
		setcookie("SN", $_GET['SN']);
		$location = 'administrator/admin/admin.html';
		header("Location: " . $location );
		exit;
	}
	
	/* Get the mpower and slipstream value from the db to know 
	if the user belongs to only mpower or slipstream or both.*/
	$comp_sql = "SELECT Mpower, Slipstream, DefaultDashBoard FROM company WHERE CompanyID = ?";
	$comp_sql = SqlBuilder()->LoadSql($comp_sql)->BuildSql(array(DTYPE_INT, $company_id));
	$comp = DbConnManager::GetDb('mpower')->GetOne($comp_sql);
	
	if($username == 'administrator' && $password == 'Xu93y2hsqmrGxKt') {
		$_SESSION['quickpass'] = true;
		$_SESSION['USER']['BROWSER_TIMEZONE'] = 'America/Chicago';
		
		header('Location:quickopen.php?SN='.$_GET['SN'].'&company_id='. $_SESSION['company_id'] );
	}
	/*$comp_mpower = $comp->Mpower;
	$comp_slipstream = $comp->Slipstream;
	$comp_default_dashboard = $comp->DefaultDashBoard;*/

	if ($username && $password) {		
		$login_sql = 'SELECT cast(logins.ID as real) as ID, logins.Password, logins.PasswordZ, logins.UserID, logins.PersonID, logins.AllowUploads,
						logins.AllowExport, people.SupervisorID, people.IsSalesPerson, people.Level, people.FirstName, people.LastName,
						people.SpecialPermissionsID, people.MasterAssignee, people.MailtoAction, people.UtcUpdate, timezones.zname, logins.NewEula
						FROM logins
						LEFT JOIN people ON logins.PersonID = people.PersonID
						LEFT JOIN timezones ON people.timezone = timezones.zid
					 	WHERE logins.CompanyID = ? AND logins.UserID = ?';
	
		$login_sql = SqlBuilder()->LoadSql($login_sql)->BuildSql(array(DTYPE_INT, $company_id), array(DTYPE_STRING, $username));
		
		$login = DbConnManager::GetDb('mpower')->GetOne($login_sql);

		/* when the eula authentication fails it redirects to the index page.
		There it needs the user name and password to sartisfy the above query.
		So the username and password is set in session. */
		$_SESSION['slip_usename'] = $username;
		$_SESSION['slip_password'] = $password;		
	
		if($quickpass) {
			$_SESSION['EULA_STATUS'] = true;
			$_SESSION['MPOWER_EULA'] = true;
			$_SESSION['SLIPSTREAM_EULA'] = true;
			$login->NewEula = 3;
		}		

		$crypted = crypt($password, $login->Password);
		if ((!$quickpass) && (!$login instanceof MpRecord || (($login->SupervisorID == -3 ) ? ($login->PasswordZ != $password) : !strstr($crypted, $login->Password) ))) {

			/*
			if ( count($_POST) > 0) {
				echo 'Debug 1<br>';
				echo '<pre>' . print_r($_POST, TRUE) . '</pre>';
				echo $login_sql . '<br>';

				exit;
			}
			*/

 			/* if the login authentication fails, then the username and password set in session is cleared. */
 			unset($_SESSION['slip_usename']);
 			unset($_SESSION['slip_password']);
			$login_error = true;
		} else if(($comp->Mpower == 1) && (($comp->DefaultDashBoard == 1) || (isset($_SESSION['MPOWER_EULA']) && !$_SESSION['MPOWER_EULA'])) && 
			(($login->NewEula != 1) && ($login->NewEula != 3)) && !isset($_POST['accept_btn'])) {
			/* This condition sartisfies,if the user eula is not 1(for Mpower) or 3(for both)
			and the user is a Mpower(Mpower = 1) user and the default dashboard is set as Mpower(1)
			or the mpower eula is set as false in session.
			When this condition sartisfies, it shows the Mpower Eula.
			*/
			$eula_status = true;
			
			if(($comp->Mpower == 1) && ($comp->Slipstream == 1)) {
				/* if the user belongs to both application then it checks
				if the Jetstream eula is already accepted, then it sets the eula to 3.
				else it sets the eula to 1. */
				if($login->NewEula == 2) {
					$_SESSION['EULA_VAL'] = '3';
				}	else {
					$_SESSION['EULA_VAL'] = '1';
				}				
			} else {
				/* if the user belongs to only Mpower application then it sets the eula to 1. */
				$_SESSION['EULA_VAL'] = '1';
			}
			
			include(BASE_PATH .'/mpower/mpower_eula.php');
		} else if(($comp->Slipstream == 1) && (($comp->DefaultDashBoard == 2) || (isset($_SESSION['SLIPSTREAM_EULA']) && !$_SESSION['SLIPSTREAM_EULA'])) && 
			(($login->NewEula != 2) && ($login->NewEula != 3)) && !isset($_POST['accept_btn'])) {
			/* This condition sartisfies,if the user eula is not 2(for Jetstream) or 3(for both)
			and the user is a Jetstream(Slipstream = 1) user and the default dashboard is set as Jetstream(2)
			or the Slipstream eula is set as false in session.
			When this condition sartisfies, it shows the Slipstream Eula.
			*/
			$eula_status = true;

			if(($comp->Mpower == 1) && ($comp->Slipstream == 1)) {
				/* if the user belongs to both application then it checks
				if the mpower eula is already accepted, then it sets the eula to 3.
				else it sets the eula to 2. */
				if($login->NewEula == 1) {
					$_SESSION['EULA_VAL'] = '3';
				}	else {
					$_SESSION['EULA_VAL'] = '2';
				}				
			} else {
				/* if the user belongs to only Mpower application then it sets the eula to 2. */
				$_SESSION['EULA_VAL'] = '2';
			}
			
			include(BASE_PATH .'/slipstream/slipstream_eula.php');
		}	else {
			if(isset($_POST['accept_btn']) && (!$quickpass))	{
				/* Updates the EULA field, when the accept button is clicked. */
				$update_sql = "UPDATE logins SET NewEula = ".$_SESSION['EULA_VAL']." WHERE ID = ?";				
				$update_sql = SqlBuilder()->LoadSql($update_sql)->BuildSql(array(DTYPE_INT, $login->ID));
				//echo $update_sql;
				$result = DbConnManager::GetDb('mpower')->Exec($update_sql);
				
				if($result) {
					$login->NewEula = $_SESSION['EULA_VAL'];
				}
			}
			/* checks and sets the mpower or eula status in session. */
			$_SESSION['EULA_STATUS'] = true;
			$_SESSION['MPOWER_EULA'] = true;
			$_SESSION['SLIPSTREAM_EULA'] = true;
			
			if(($comp->Mpower == 1) && ($comp->Slipstream == 1) && ($login->NewEula != 3)) {
				/* if the user belongs to only both applications and its eula is not 3
				, then it sets the mpower/slipstream(jetstream) eula status in session. */
				$_SESSION['EULA_STATUS'] = false;
				if($login->NewEula == 1) {
					$_SESSION['SLIPSTREAM_EULA'] = false;
				}
				else if($login->NewEula == 2) {
					$_SESSION['MPOWER_EULA'] = false;
				} else {
					$_SESSION['MPOWER_EULA'] = false;
					$_SESSION['SLIPSTREAM_EULA'] = false;
				}				
			}
			else if(($comp->Mpower == 1) && ($comp->Slipstream == 0) && (($login->NewEula != 1) && ($login->NewEula != 3))) {
				/* if the user belongs to only Mpower and its eula is not 1 or 3
				, then it sets the mpower eula to false. */
				$_SESSION['EULA_STATUS'] = false;
				$_SESSION['MPOWER_EULA'] = false;
			}
			else if(($comp->Slipstream == 1) && ($comp->Mpower == 0) && (($login->NewEula != 2) && ($login->NewEula != 3))) {
				/* if the user belongs to only Slipstream and its eula is not 2 or 3
				, then it sets the Slipstream eula to false. */
				$_SESSION['EULA_STATUS'] = false;
				$_SESSION['SLIPSTREAM_EULA'] = false;
			}
			// They have logged in successfully.  Now set up all the general session info
			require_once BASE_PATH . '/include/class.ReportingTree.php';

			// Update both the Last Used values in the People table and the Logins table
			if ((!isset($no_update_last_used) || $no_update_last_used == false) && (!$quickpass)) {
				$lastused = TimestampToMsSql(time());
				$lastused_ip = $_SERVER['REMOTE_ADDR'];

				$last_used_sql = "update people set LastUsed = ?, LastUsedIP = ?
					where PersonID = ?";
				$last_used_sql = SqlBuilder()->LoadSql($last_used_sql)->BuildSql(array(DTYPE_TIME, $lastused), array(DTYPE_STRING, $lastused_ip), array(DTYPE_INT, $login['PersonID']));
				DbConnManager::GetDb('mpower')->Exec($last_used_sql);

				$last_used_sql = "update logins set LastUsed = ?, LastUsedIP = ?
					where ID = ?";
				$last_used_sql = SqlBuilder()->LoadSql($last_used_sql)->BuildSql(array(DTYPE_TIME, $lastused), array(DTYPE_STRING, $lastused_ip), array(DTYPE_INT, $login['ID']));
				DbConnManager::GetDb('mpower')->Exec($last_used_sql);
			}

			SessionManager::CreateToken($login->PersonID, trim($company_id));

			$company_sql = 'SELECT * FROM company WHERE	companyid =	?';
			$company_sql = SqlBuilder()->LoadSql($company_sql)->BuildSql(array(DTYPE_INT, $company_id));
			$company = DbConnManager::GetDb('mpower')->GetOne($company_sql);

			$company_fields = array('Source2Label',
									'ClosePeriod',
									'Requirement1used',
									'Requirement2used',
									'CompanyID',
									'ValuationUsed',
									'Name',
									'MissedMeetingGracePeriod',
									'MonthsConsideredNew',
									'Mpower',
									'Slipstream',
									'LimitAccessToUnassignedUsers',
									'UseOppTracker',
									'HeaderText',
									'Jetfile',
									'MyGoalMine');

			foreach ($company_fields as $field) {
				$company_data[$field] = $company->$field;
			}
			$_SESSION['company_obj'] = $company_data;

			$tree = new ReportingTree();
			$_SESSION['tree_obj'] = $tree;

			Language::Init($company);

			// This makes it appear that the alias is exactly the same as the original
			// and doesn't account for other toggles.  ???
			$_SESSION['login_id'] = $login->PersonID;
			$_SESSION['pov'] = $login->PersonID;
		
// Exception for Al showing Array Services 4/2/09
			if($_SESSION['login_id'] == 11644) {
				$_SESSION['mpower'] = TRUE;
				$next_step = 'slipstream.php';
				$company->Slipstream = TRUE;
			}

			if ($company->Mpower == 1) {
				$next_step = 'choosepov.php';
				$_SESSION['combined'] = true;

				if (!$tree->IsSeniorMgr($login->PersonID)) {
					$next_step = 'dashboard.php';
				}
				if($login->Level == 1 && $login->IsSalesPerson == 0){
					$next_step = 'slipstream.php';
				}

				$_SESSION['mpower'] = true;
				$_SESSION['mpower_link'] = $next_step;
			} else {
				$_SESSION['mpower'] = false;
			}

			if (($company->DefaultDashBoard == 2 || $company->Mpower == 0) && $company->Slipstream == 1) {
				$next_step = 'slipstream.php';
			}

			// This setting helps a salesperson to view his report screen directly
			if(!$tree->IsManager($login->PersonID)) {
				$_SESSION['treeid'] = $login->PersonID;
			}

			// The remainder of these values are to support connections to the legacy code
			$_SESSION['currentuser_loginid'] = $login->ID;
			$_SESSION['mpower_effective_userid'] = $login->PersonID;
			$_SESSION['mpower_userid'] = $login->UserID;
			$_SESSION['mpower_pwd'] = $login->PasswordZ;
			$_SESSION['mpower_companyid'] = $company_id;

			setcookie('currentuser_loginid', $login->ID);
			setcookie('mpower_effective_userid', $login->PersonID);
			setcookie('mpower_userid', $login->UserID);
			setcookie('mpower_pwd', $login->PasswordZ);
			setcookie('mpower_companyid', $company_id);

			// Get the session values initialized if slipstream is enabled for the user's company
			if ($company->Slipstream) {
				// Slipstream session code
				$_SESSION['USER']['USERNAME'] = $username;
				$_SESSION['USER']['USERID'] =  $login->PersonID;
				$_SESSION['USER']['COMPANYID'] = $company_id;
				$_SESSION['USER']['SUPERVISORID'] = $login->SupervisorID;
				$_SESSION['USER']['ISSALESPERSON'] = $login->IsSalesPerson;
				$_SESSION['USER']['FIRSTNAME'] = $login->FirstName;
				$_SESSION['USER']['LASTNAME'] = $login->LastName;
				$_SESSION['USER']['COMPANY'] = $company->DirectoryName;
				$_SESSION['USER']['LEVEL'] = $login->Level;
				$_SESSION['USER']['UTC_UPDATE'] = $login->UtcUpdate;
				$_SESSION['USER']['ALLOW_UPLOADS'] = $login->AllowUploads;
				$_SESSION['USER']['ALLOW_EXPORT'] = $login->AllowExport;
				$_SESSION['USER']['MAILTO_ACTION'] = $login->MailtoAction;
				$_SESSION['USER']['IMAP_ENABLED'] = ImapSettings::isEnabled();
				$_SESSION['USER']['SPECIAL_PERMISSIONS_ID'] = $login->SpecialPermissionsID;
			}
			$_SESSION['USER']['TIMEZONE'] =  $login->zname;
			if (isset($_POST['browser_timezone'])) {
				$_SESSION['USER']['BROWSER_TIMEZONE'] = $_POST['browser_timezone'];
			}
			
			/*
			 * If a user has special permissions, override the limit access flag
			 */
			if($_SESSION['USER']['SPECIAL_PERMISSIONS_ID'] = $login->SpecialPermissionsID > 0){
			
				$_SESSION['company_obj']['LimitAccessToUnassignedUsers'] = false;
				$_SESSION['USER']['ALLOW_UPLOADS'] = true;
				$_SESSION['USER']['ALLOW_EXPORT'] = true;
				
			}
			
			/*
			 * Note Alert
			 
			if($company_id == 508 || $company_id == 731 || $company_id == 549 || $company_id == 726 || $company_id == 433 || $company_id == 430 || $company_id == 581 || $company_id == 717 || $company_id == 734 || $company_id == 737 || $company_id == 613 || $company_id == 762 || $company_id == 724 || $company_id == 753 || $company_id == 761) {
				$_SESSION['note_alert'] = true;
			}
			*/
			$_SESSION['note_alert'] = $company->NoteAlert;

			# Enable Call Report for all Companies
			$_SESSION['call_report'] = true;
			
			
			$field_map_obj = new fieldsMap();
			$field_map_obj->companyId = $company_id;
			$_SESSION['USER']['ACCOUNTMAP'] = $field_map_obj->getAccountMapFieldsFromDB();
			$_SESSION['USER']['CONTACTMAP'] = $field_map_obj->getContactMapFieldsFromDB();
			
			$_SESSION['account_name'] = $field_map_obj->getAccountNameField();
			$_SESSION['contact_first_name'] = $field_map_obj->getContactFirstNameField();
			$_SESSION['contact_last_name'] = $field_map_obj->getContactLastNameField();
			
			/* Set the account fields which can be accessed publicly in session. */
			$public_account_fields = array();
			foreach($_SESSION['USER']['ACCOUNTMAP'] as $accountmap) {
				if($accountmap['IsPrivate'] != 1) {
					$public_account_fields[] = $accountmap['FieldName'];
				}
			}
			$_SESSION['public_account_fields'] = $public_account_fields;
			
			/* Set the contact fields which can be accessed publicly in session. */
			$public_contact_fields = array();
			foreach($_SESSION['USER']['CONTACTMAP'] as $contactmap) {
				if($contactmap['IsPrivate'] != 1) {
					$public_contact_fields[] = $contactmap['FieldName'];
				}
			}
			$_SESSION['public_contact_fields'] = $public_contact_fields;
			
			/* Set the type of account/contact access in session. */
			if($_SESSION['company_obj']['LimitAccessToUnassignedUsers'] == 1) {
				if((count($_SESSION['public_account_fields']) > 0) && (in_array($_SESSION['account_name'], $_SESSION['public_account_fields']))) {
					$_SESSION['limit_account_access'] = ACCESS_UNASSIGNED_TRUE;
				} else {
					$_SESSION['limit_account_access'] = ACCESS_UNASSIGNED_FALSE;
				}
				
				if((count($_SESSION['public_contact_fields']) > 0) && 
					((in_array($_SESSION['contact_first_name'], $_SESSION['public_contact_fields'])) || (in_array($_SESSION['contact_last_name'], $_SESSION['public_contact_fields'])))
				) {
					$_SESSION['limit_contact_access'] = ACCESS_UNASSIGNED_TRUE;
				} else {
					$_SESSION['limit_contact_access'] = ACCESS_UNASSIGNED_FALSE;
				}
			} else {
				$_SESSION['limit_account_access'] = ACCESS_DEFAULT;
				$_SESSION['limit_contact_access'] = ACCESS_DEFAULT;
			}			

			$sql = 'SELECT CM.FieldName as ContactFieldName, AM.FieldName as AccountFieldName FROM ContactMap AS CM
				INNER JOIN AccountMap AS AM ON CM.AccountMapID = AM.AccountMapID AND CM.CompanyID = AM.CompanyID
				Where CM.CompanyID = ? AND CM.Enable=1';
			$con_acc_map_sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, (int)$company_id));
			$con_acc_map = DbConnManager::GetDb('mpower')->Exec($con_acc_map_sql);
			$_SESSION['USER']['CONTACTACCOUNTMAP'] = $con_acc_map;

			if (isset($_GET['SN'])) {
				setcookie("SN", $_GET['SN']);
			}

			$is_login = false;
	
			$location = SessionManager::CreateUrl($next_step);
			
			/* After the eula is accepted by the user, it redirects to the previous page, that the user has clciked. */
			if(isset($_SESSION['REDIRECT_PAGENAME']) && ($_SESSION['REDIRECT_PAGENAME'] != '')) {
				$location = $_SESSION['REDIRECT_PAGENAME'];
			}
	
			if ($login->SupervisorID == -3) {
				setcookie('currentuser_loginid', $login->ID);
				setcookie('mpower_pwd', $login->PasswordZ);
				setcookie('mpower_userid', $login->UserID);
				setcookie('mpower_companyid', $company_id);
				setcookie('mpower_effective_userid', $login->PersonID);
				setcookie('currentuser_isadmin', '1');
				// setcookie('account_name', $account->FieldName);
				// setcookie('contact_first_name', $contact_first_name->FieldName);
				// setcookie('contact_last_name', $contact_last_name->FieldName);

				//setcookie('flat_view_id', $login->PersonID);
				//setcookie('system_mode', 'legacy');
				//setcookie('LoginSessionID', '123642');

				$location = 'administrator/admin/admin.html';
			}

//			print_r($_SESSION);
			session_write_close();
			
			// Forward the browser to the dashboard with the appropriate SESSION info
			header("Location: " . $location );
			exit;
		}
	}
}

/*
else {  // We have no idea what company is trying to log in
	$is_login = false;
	//print_r($_SESSION);
	$error_message = 'Please use your assigned URL to access the M-Power system.';
}
*/

if($is_login) {
	// Run the SQL and get the matching record
	$company_sql .= ' WHERE CompanyID = ?';
	$company_sql = SqlBuilder()->LoadSql($company_sql)->BuildSql(array(DTYPE_INT, $company_id));
	if ($company_record = DbConnManager::GetDb('mpower')->GetOne($company_sql)) {
		$_SESSION['company_id'] = $company_record->CompanyID;
		Language::Init($company_record);
	}
}

//	} else {
//		//SessionDel('company');
//		$is_login = false;
//		//$error_message = 'Please provide a valide Company ID';
//		$error_message = 'Please use your assigned URL to access the M-Power system.';
//	}
//}

$logo_file = false;
$reseller_logo = false;

if (isset($company_record) && is_object($company_record)) {

	if ($company_record->InRecord('LogoName') && !is_null($company_record->LogoName)) {
		$logo_file = 'https://jetstream-images.s3.amazonaws.com/companylogos/' . str_replace('logo', $company_record->DirectoryName, $company_record->LogoName);
		$logo = "<img src=\"$logo_file\">";
	}

	if ($company_record->ResellerID > 1 && !is_null($company_record->ResellerLogo)) {
		$reseller_logo = $company_record->ResellerLogo;
	}
}

if(!$eula_status) {
?>

<html>
	<head>
		<title>
			<?php if (!isset($company_record) || $company_record->Mpower): ?>M-Power<?php endif; ?> <?php if (!isset($company_record) || $company_record->Slipstream): ?>Jetstream <?php endif; ?> Login
		</title>
		<link rel="shortcut icon" href="/favicon.ico" />
		<style type="text/css">
			body {position:relative;
				margin:0;
				padding:0;
				height:100%;
				font-family: "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
			}

			.header {
				padding:10px 0;
				text-align:center;
			}

			.footer {
				height:1px;
				font-size:.7em;
				padding-top:10px;
				padding-bottom:20px;
				text-align:center;
			}
			.login_form {
				float:left;
				border:1px solid green;
				width:350px;
				padding-left:50px;
			}

			fieldset {
				border: 3px double gray;
				padding:0 20px 0 20px;
				margin-bottom:10px;
			}

			fieldset p {
				margin:10px 0;
				padding:0;
			}

			legend {
				color: #FF0000;
				font-size:1.2em;
			}

			label {
				width: 5em;
				float: left;
				text-align: left;
				display: block
				padding:0;
				margin:3px;
				margin-right: 0.5em;
			}

			input {
				border: #000 1px solid;
				background: #f0f3f6;
				padding:2px;
				margin:0;
				font-size:.9em;
				font-family: "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
			}

			input.btn {
				font-size:.9em;
				font-weight:bold;
				padding:0 10px;
				margin:0;
				border:0px double #005EC6;
				background:#0077FF;
				color: white;
			}

			table {
				height:100%;
				width:100%;
			}
		</style>
		<script type="text/javascript" src="javascript/common_util.js"></script>
	</head>
	<body>
		<table style="position:relative;">
			<tr>
				<td class="header">
					<?php if (!isset($company_record) || $company_record->Mpower): ?><img src="images/login_mpower.png"><?php endif; ?> <?php if (!isset($company_record) || $company_record->Slipstream): ?><img style="margin:0 0 28px 10px;" src="images/login_jetstream.png"><?php endif; ?>
				</td>
			</tr>			
			<tr>
				<td style="text-align:center;vertical-align:top;">			
					<div style="font-size:.9em;width:300px;display:block;<?php if ($logo_file) : ?>margin-left:50;float:left;<?php else: ?>margin-left:auto;margin-right:auto;<?php endif ?>">
						<p style="display: inline;"><?php if ($login_error): ?><nobr>Username or password incorrect.<?php endif; ?>&nbsp;</nobr></p>
						<p style="display: inline;"><?php if (isset($_GET['smsg']) && (trim($_GET['smsg']) != '')): ?><nobr><?php echo trim($_GET['smsg']); endif; ?>&nbsp;</nobr></p>
						<form action="<?php echo SessionManager::CreateUrl("index.php")?>" method="POST">
							<fieldset>
								<legend><?php if (isset($company_record)) echo $company_record->Name . ' ' ?>Login</legend>
								<?php  if(!isset($_GET['company']) && !isset($_SESSION['company_id'])) : ?>
								<p><label for="company"><?php echo Language::__get('Company')?>:</label> <input type="text" name="company_dir"></p>
								<?php endif; ?>
								<p><label for="username"><?php echo Language::__get('Username')?>:</label> <input type="text" name="username"></p>
								<p><label for="password"><?php echo Language::__get('Password')?>:</label> <input type="password" name="password"></p>
								<p style="text-align:center;"><input type="submit" value="<?php echo Language::__get('Login')?>" class="btn"></p>
							</fieldset>
							<input type="hidden" id="browser_timezone" name="browser_timezone" value="">
						</form>
						<p style="text-align:center;clear:left;"><a href="forgetpassword.php"><?php echo Language::__get('ForgetPassword')?></a></p>
						<p style="text-align:center;"><a href="changepassword.php"><?php echo Language::__get('ChangePassword')?></a></p>
						<?php if (isset($company_record) && isset($_SESSION['company_id'])) : ?><p style="text-align:center;clear:left;"><a href="logout.php"><?php echo $company_record->Name?> is not my company</a></p><?php endif ?>
					</div>
					<?php if ($logo_file): ?>
						<img id="client_logo" src="<?php echo $logo_file ?>" alt="Client Logo">
					<?php endif ?>
				</td>
			</tr>
			<tr>
				<td class="footer">
				<div  style="position:relative;">
					<p>M-Power and Jetstream are trademarks of</p>
					<p>JetstreamCRM.com, LLC.</p> <p>Tel: (773) 714-8100</p>
					<p><a href="#" onclick="openCenteredWindow('privacy.html');">Privacy Policy</a></p>
					<?php if ($reseller_logo):?>
					<div style="position:absolute;bottom:0;right:0;"><img src="images/resellers/<?php echo $reseller_logo?>"></div>
					<?php endif;?>
				</div>
				</td>
			</tr>
		</table>
	</body>
<script type="text/javascript">
	var local_timezone = getTimezoneName();
	document.getElementById('browser_timezone').value = local_timezone;

	var myWindow;

	function openCenteredWindow(url) {
	    var width = 670;
	    var height = 500;
	    var left = parseInt((screen.availWidth/2) - (width/2));
	    var top = parseInt((screen.availHeight/2) - (height/2));
	    var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;
	    myWindow = window.open(url, "subWind", windowFeatures);
	}
	
</script>
</html>
<?php
}
?>
