<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));
define('ATTACHMENT_PATH', '/webtemp/attachments/');

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/email_service/class.EmailWebServices.php';
require_once BASE_PATH . '/email_service/class.EmailTemplate.php';
require_once BASE_PATH . '/include/class.Log.php';
require_once BASE_PATH . '/email_service/includes/nusoap.php';





function writeToLog($email_request, $type = '') {
	$timestamp = strtotime("now");	
	$filename = '/webtemp/logs/'.$timestamp.'_'.$type.'.xml';
	//file_put_contents($filename, $email_request);
}


function Authenticate($email_request) {
	//writeToLog($email_request, 'Authenticate');
	
	$email = new EmailWebServices();
	$email->AuthenticateRequest($email_request);
	
	//writeToLog($email->AuthenticateResponse(), 'AuthenticateResponse');
	
	return $email->AuthenticateResponse();
}



function AddEmail($email_request) {	
	//writeToLog($email_request, 'AddEmail');
	
	$email = new EmailWebServices();
	$email->AddEmailRequset($email_request);	
	
	if ($email->validRequest) {
		$email->saveEmail();
		$email->saveAttachments();
		$email->saveContacts();
		$email->createNote();
	}
	
	//writeToLog($email->AddEmailResponse(), 'AddEmailResponse');
	
	return $email->AddEmailResponse();
}

/**
 * fetch the user email template from database and retunrs it
 * @param email template request is a string
 * @return email template response is a string
 */
function GetEmailTemplate($email_request) {
	// get the request and pase it and validate 
	// check the database and return the response
	$template = new EmailTemplate();
	$template->Request($email_request);

	return $template->Response();
}



// Create a intsance of soap server
$server = new soap_server();
$server->configureWSDL('Jetstream Email Service', 'urn:soapserver.com');

/**
 * Authenticate method
 * Authenticates user 
 */
$server->register("Authenticate",
     			array('request' => 'xsd:string'),
                array('return' => 'xsd:string'),
                'urn:soapserver.com',
                'urn:soapserver.com#Authenticate');
/**
 * Add email into the system
 */
$server->register("AddEmail",
     			array('request' => 'xsd:string'),
                array('return' => 'xsd:string'),
                'urn:soapserver.com',
                'urn:soapserver.com#AddEmail');
				
/**
 * Get user email tempate from the system
 */
$server->register("GetEmailTemplate",
     			array('request' => 'xsd:string'),
                array('return' => 'xsd:string'),
                'urn:soapserver.com',
                'urn:soapserver.com#GetEmailTemplate');
				

				

$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
?>
