<?php

//require_once BASE_PATH . '/include/class.DbConnManager.php';
//require_once BASE_PATH . '/include/class.SqlBuilder.php';
//require_once BASE_PATH . '/include/mpconstants.php';

class EmailServiceHandler
{
	private $token;
	public $user;
	
	function EmailServiceHandler($user = array()) {
		$this->user = $user;
	}
	
	public function getToken() {		
		return $this->token;
	}

	public function Init($token = '') {		
		if (!empty($token)) {
			$this->token = $token;
		} else {
			if (!isset($this->user['person_id'])) $this->user['person_id'] = '';
			if (!isset($this->user['entry_id'])) $this->user['entry_id'] = '';
			$this->token = crypt(trim($this->user['person_id']) . trim($this->user['entry_id']). strtotime('now'));
		}
	}
	
	public function Store() {
		
		$timestamp = date('m/d/Y g:i:s A', strtotime('now'));
		$sql = "INSERT INTO EmailServiceLog(EmailServiceToken, CompanyID, PersonID, EmailEntryID, EmailUserRole, Timestamp, EmailAddress)
							VALUES (?, ?, ?, ?, ?, ?, ?)";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $this->token), array(DTYPE_INT, $this->user['company_id']), 
										array(DTYPE_INT, $this->user['person_id']), array(DTYPE_STRING, $this->user['entry_id']), 
										array(DTYPE_STRING, $this->user['role']), array(DTYPE_STRING, $timestamp), array(DTYPE_STRING, $this->user['email']));
		$data = DbConnManager::GetDb('mpower')->Exec($sql);
	}
	
	public function Load() {
		
		$status = false;
		
		$sql = "SELECT EmailServiceLogID, CompanyID, PersonID, EmailEntryID, EmailUserRole, Timestamp, EmailAddress FROM EmailServiceLog WHERE  EmailServiceToken = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $this->token));
		$rs = DbConnManager::GetDb('mpower')->getOne($sql);
				
		if ($rs instanceof MpRecord) {
			$status = true;
			$this->user['service_id'] = $rs->EmailServiceLogID;
			$this->user['company_id'] = $rs->CompanyID;
			$this->user['person_id'] = $rs->PersonID;
			$this->user['entry_id'] = trim($rs->EmailEntryID);
			$this->user['role'] = trim($rs->EmailUserRole);
			$this->user['email'] = trim($rs->EmailAddress);
			//$this->user['timestamp'] = $rs->EmailUserRole;
		} 	
		
		return $status;		
	}
	
	

	public function Destroy() {
		// either delete the row with the tokenid else store a bit to indicate the token has expired
	}

}