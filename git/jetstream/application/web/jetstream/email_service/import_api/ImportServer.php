<?php



define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.Authentication.php';
require_once BASE_PATH . '/email_service/includes/nusoap.php';
require_once BASE_PATH . '/email_service/import_api/class.ImportAPI.php';

require_once BASE_PATH . '/slipstream/class.ModuleCompanyContactNew.php';

require_once BASE_PATH . '/slipstream/class.Note.php';
require_once BASE_PATH . '/slipstream/class.Event.php';

require_once BASE_PATH . '/slipstream/Inspekt.php';

/**
 * Process the Import Request
 * @param $request {String}, xml string
 * @return $response {String}, xml string
 */
function Import($request) {	
	
	$import = new ImportAPI();
	
	// validate the request xml schema
	$response = $import->AuthenticateSchema($request);
	
	return $response;	
}

/**
 * Process the Authenticate Request
 * @param $request {String}, xml string
 * @return $response {String}, xml string
 */
function Authenticate($request) {	
	
	$import = new ImportAPI();
	
	// validate the request xml schema
	$response = $import->Authenticate($request);
	
	return $response;	
}


			


$response = Import(file_get_contents('client/AccountNote.xml'));

echo $response;

