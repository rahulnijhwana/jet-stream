<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

//error_reporting(E_ALL);
ini_set('display_errors', '0');
ob_start();

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.Authentication.php';
require_once BASE_PATH . '/email_service/includes/nusoap.php';
require_once BASE_PATH . '/email_service/import_api/class.ImportAPI.php';
require_once BASE_PATH . '/slipstream/class.ModuleCompanyContactNew.php';
require_once BASE_PATH . '/slipstream/class.Note.php';
require_once BASE_PATH . '/slipstream/class.Event.php';
require_once BASE_PATH . '/slipstream/Inspekt.php';

/**
 * Process the Import Request
 * @param $request {String}, xml string
 * @return $response {String}, xml string
 */
function Import($request) {	
	
	$import = new ImportAPI();
	
	// validate the request xml schema
	$response = $import->AuthenticateSchema($request);
	
	return $response;	
}

/**
 * Process the Authenticate Request
 * @param $request {String}, xml string
 * @return $response {String}, xml string
 */
function Authenticate($request) {	
	
	$import = new ImportAPI();
	
	// validate the request xml schema
	$response = $import->Authenticate($request);
	
	return $response;	
}

//ob_clean();

ob_end_clean();

// Create a intsance of soap server
$server = new soap_server();
$server->configureWSDL('Jetstream Import API', 'urn:soapserver.com');


/**
 * Import API Method
 */
$server->register("Import",
     			array('request' => 'xsd:string'),
                array('return' => 'xsd:string'),
                'urn:soapserver.com',
                'urn:soapserver.com#Import');
				
				
/**
 * Import API Authenticate Method
 */
$server->register("Authenticate",
     			array('request' => 'xsd:string'),
                array('return' => 'xsd:string'),
                'urn:soapserver.com',
                'urn:soapserver.com#Authenticate');				
				

$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
