<?PHP
/**
* The ImportAPI class. It contains the buissness logic for Import API web services
* @package WebServices
*/


class ImportAPI
{
	public $debug;
	private $log_file;

	function __construct() {
		$this->debug = false;
		$this->log_file = 'ImportAPILog';		
	}

	/**		
	 *	Get all the old record data 
	 */
	public function getOldData($id, $type, $companyid ){

		$mapsql = "select fieldname from {$type}map where companyid = ? ";
		$mapsql = SqlBuilder()->LoadSql($mapsql)->BuildSql(array(DTYPE_INT, $companyid));
		$mapdata = DbConnManager::GetDb('mpower')->Exec($mapsql);

		$fieldsArr = array () ;
		foreach( $mapdata as $map ){
			$fieldsArr[] = $map['fieldname'] ; 
		}
		$fieldsList = implode (' , ', $fieldsArr );
		
		$sql = "select $fieldsList from $type where {$type}id = ? ";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $id));
		$data = DbConnManager::GetDb('mpower')->Exec($sql);

		return $data[0] ; 
	}

	private function isNodeExist($nodes) {
		return (count($nodes) > 0) ? true : false;
	}

	
	/**
	 * Parse the xml to authenticate the request
	 */	 
	public function Authenticate($emailRequest) {
		// request object
		$objDOM = new DOMDocument();
		$objDOM->loadXML($emailRequest);
		
		// response object
		$doc = new DOMDocument();
		$doc->formatOutput = true;
		$response = $doc->createElement( "LoginResponse" );
		
		$status = 'failure';		
			
		// get the LoginRequest node
		$request_node = $objDOM->getElementsByTagName("LoginRequest")->item(0);
		
		// get the request node attributes
		$company_dir = $request_node->getAttribute('CompanyDirectoryName');
		$user_id = $request_node->getAttribute('UserID');
		$password = $request_node->getAttribute('Password');
		
		
		if (!empty($company_dir) && !empty($user_id) && !empty($password)) {					
			$company_record = Authentication::GetCompanyRecord($company_dir);
			if ($company_record) {			
				$company_id = $company_record->CompanyID;
				
				$login = Authentication::GetLoginRecord($company_id, $user_id, $password);
				if ($login) {					
					$status = 'success';
					
					// Company id
					$response_company_id = $doc->createElement( "CompanyID" );
					$response_company_id->appendChild( $doc->createTextNode( $company_id ) );  
					$response->appendChild( $response_company_id );
					
					// Person id
					$response_person_id = $doc->createElement( "PersonID" );
					$response_person_id->appendChild( $doc->createTextNode( $login->PersonID ) );  
					$response->appendChild( $response_person_id );					
					
					// First name
					$response_fname = $doc->createElement( "FirstName" );
					$response_fname->appendChild( $doc->createTextNode( $login->FirstName ) );  
					$response->appendChild( $response_fname );
					
					// Company id
					$response_lname = $doc->createElement( "LastName" );
					$response_lname->appendChild( $doc->createTextNode( $login->LastName ) );  
					$response->appendChild( $response_lname );		
				}
			}				
		}		
		
		
		// Set the Response status
		$response->setAttribute('Status', $status);				
		$doc->appendChild( $response );
		return $doc->saveXML();
	}	
	
	
	/**
	 * Parse the xml request
	 */
	public function AuthenticateSchema($emailRequest) {
		// request object
		$objDOM = new DOMDocument();
		$objDOM->loadXML($emailRequest);
		
		// response object
		$doc = new DOMDocument();
		$doc->formatOutput = true;
		$response = $doc->createElement( "Response" );
		$doc->appendChild( $response );
		
		$response_status = $doc->createElement( "Status" );
		$response->appendChild( $response_status );
		
		if (!$objDOM->schemaValidate('schema/Request.xsd')) {			
			$response_status->setAttribute('code', 'FAILURE');
			$response_error = $doc->createElement( "Error" );
			$response_error->appendChild( $doc->createTextNode( "Invalid XML Request." ) );  
			$response_status->appendChild( $response_error );
						
		} else {			
			// get the request node
			$request_node = $objDOM->getElementsByTagName("Request")->item(0);
			
			// get the request node attributes
			$request_compnay_id = $request_node->getAttribute('CompanyID');
			$request_user_id = $request_node->getAttribute('UserID');
			$request_timestamp = $request_node->getAttribute('Timestamp');
			$request_batch_id = $request_node->getAttribute('BatchID');	
			
			
			// set response attributes
			$response->setAttribute('CompanyID', $request_compnay_id);
			$response->setAttribute('UserID', $request_user_id);
			$response->setAttribute('Timestamp', $request_timestamp);
			$response->setAttribute('BatchID', $request_batch_id);		

			// set status attributes
			$response_status->setAttribute('code', 'OK');
			  
			// get the account element
			$request_accounts = $objDOM->getElementsByTagName("Account");
			if ($request_accounts->length > 0  ){
				$this->Parse($request_accounts, $doc, $response, 'account', $request_compnay_id, $request_user_id);
			} 
			// get the contact element
			$request_contacts = $objDOM->getElementsByTagName("Contact");
			if ( $request_contacts->length > 0  ){
			$this->Parse($request_contacts, $doc, $response, 'contact', $request_compnay_id, $request_user_id);
			}
			// get the event element
			$request_events = $objDOM->getElementsByTagName("Event");
			if ( $request_events->length > 0  ){
			$this->Parse($request_events, $doc, $response, 'event', $request_compnay_id, $request_user_id);
			}
			// get the note element
			$request_notes = $objDOM->getElementsByTagName("Note");
			if ( $request_notes->length >0 ) {
				$this->Parse($request_notes, $doc, $response, 'note', $request_compnay_id, $request_user_id);	
			} 
		}

		if (!$doc->schemaValidate('schema/Response.xsd')) {			
			$response_error = $doc->createElement( "Error" );
			$response_error->appendChild( $doc->createTextNode( "Invalid XML Response." ) );  
			$response_status->appendChild( $response_error );
			
		} 
		
		ob_end_clean();		
		return $doc->saveXML();	

			 
	}
	
	public function Parse($request_nodes, &$doc, &$response, $rec_type, $request_compnay_id, $request_user_id) {
//		file_put_contents( "c:\webtemp\importDebug.txt" , "$request_nodes, &$doc, , $rec_type, $request_compnay_id, $request_user_id \n", FILE_APPEND ); 

 		if ($rec_type == 'account' || $rec_type == 'contact') {
 			// The functionality for this has been moved below to handle the assignment properly
		} else if ($rec_type == 'event') {
			$objEvent = new Event($request_compnay_id, $request_user_id);
		}
		else if ($rec_type == 'note') {
			$objNote = new Note($request_compnay_id, $request_user_id);
		} else {
			// it should not be here
			return false;
		}
	
		
		if ($this->isNodeExist($request_nodes)) {

			foreach ($request_nodes as $id => $node) {
				$post = array();
				$post['rec_type'] = $rec_type;			

				$response_node = $doc->createElement( ucfirst($rec_type) );
				$response->appendChild( $response_node );
				// get the attributes
				$jet_id  = $node->getAttribute('JetID');
				$client_id  = $node->getAttribute('SourceID');
				$import_id = $node->getAttribute('ImportID');					
							
				
				// HEADER 					
				$node_header = $node->getElementsByTagName("Header")->item(0);					
				
				// get the header elements
				$private = $node_header->getElementsByTagName("Private")->item(0)->nodeValue;					
				$JettAccountID = $node_header->getElementsByTagName("JetAccountID")->item(0)->nodeValue;
				$JettContactID = $node_header->getElementsByTagName("JetContactID")->item(0)->nodeValue;
				$JettEventID = $node_header->getElementsByTagName("JetEventID")->item(0)->nodeValue;
				$assigned_to = $node_header->getElementsByTagName("AssignedTo")->item(0)->nodeValue;

				if ($rec_type == 'account' || $rec_type == 'contact') {	
					$assigned_to = (!empty($assigned_to)) ? $assigned_to : $request_user_id;
					// this is needed for account and contact
					$rec = new ModuleCompanyContactNew($request_compnay_id, $assigned_to);
					$select_field_array = array('Select01', 'Select02', 'Select03', 'Select04', 'Select05', 'Select06', 'Select07', 'Select08', 'Select09', 'Select10');
				}
				
				//var_dump($account_header_JettContactID);
				
				if ($rec_type == 'contact') {				
					$post['AccountID'] = $JettAccountID;
				} 
				
				$post['ID'] = (empty($jet_id)) ? 0 : $jet_id;
				$post['SourceID'] = $client_id;
				$post['ImportID'] = $import_id;			
					
				$post['private'] = ($private == 1) ? 'true' : 'false';
				
				// DATA 					
				$node_data = $node->getElementsByTagName("Data")->item(0);					
				$node_data_values = $node_data->getElementsByTagName("Value");
				
				
				// get the data elements					
				if ($this->isNodeExist($node_data_values)) {
					foreach ($node_data_values as $node_data_value) {
						$value = $node_data_value->nodeValue;
						$key = $node_data_value->getAttribute('FieldName');						
						if(($rec_type == 'account' || $rec_type == 'contact') && in_array($key, $select_field_array)) {
							// Get the option id of the drop down							
							$value = $this->GetOptionID($request_compnay_id, trim($value), $rec_type, trim($key));
						}					
						$post[$key] = $value;
					}
					if (($post['ID'] != 0 )&& ($rec_type == 'account' || $rec_type == 'contact') ){
						$oldData = $this->getOldData($post['ID'], $rec_type, $request_compnay_id);
						foreach( $oldData as $keyd => $valued ){
							if ( $valued != '' ) {
								if ( empty($post[$keyd]) )	$post[$keyd] = $valued ; 
							}
						}
					} 
				}
				
				if ($rec_type == 'account' || $rec_type == 'contact') {
					// for each node store the record
					$post_vars = $post;
					$input = Inspekt_Cage::Factory($post_vars);
					MapLookup::Load($request_compnay_id);
					$result = $rec->ValidateInput($input);

					//$rec->ClearAssignments($rec_type, $result['status']['RECID']); // Check what is missing ?  
					$rec->Assign($rec_type, $result['status']['RECID'], $assigned_to);
				}
				else if ($rec_type == 'event') {
					$objEvent->allowCreatePastEvent = true;					
					
					$objEvent->contactId = $JettContactID;
					$objEvent->userId = $request_user_id;
					
					
					// get the event type id
					
					$event_type_info = $this->GetEventType($request_compnay_id, trim($post['EventTypeName']));					
					$objEvent->eventTypeId = 'EVENT_'.$event_type_info['ID'];
					
					$objEvent->eventSubject = $post['Subject'];
					$objEvent->eventDur = $post['DurationHours'].':'.$post['DurationMinutes'];
					$objEvent->saveType = $event_type_info['TYPE'];					
					
					$start_date_time = trim($post['StartDateTime']);				
					//echo $start_date_time.'<br>';
					
					$ts = strtotime($start_date_time);				
					$objEvent->eventStartDate = date('m/d/y', $ts);
					$objEvent->eventStartTime = date('g:i A', $ts);
					
					//echo $objEvent->eventStartDate;					
					//echo '<br>';					
					//echo $objEvent->eventStartTime;					
					//$objEvent->colseEventVal = $post['IsClosed'];
					//$objEvent->cancelEventVal = $post['IsCancelled'];
					$objEvent->eventAction = 'add';			
					$result['status'] = $objEvent->SaveEvent();	
										
					
					//Create Note					
					$description = trim($post['Description']);						
					$event_id = (int)$result['status']['GETFORMATTEDVAR'];				
					
					if(!empty($description) && $event_id > 0) {
						
						$objNote = new Note($request_compnay_id, $request_user_id);							
						$objNote->noteCreator = $request_user_id;
						$objNote->noteCreationDate = date("Y-m-d H:i:s");
						$objNote->noteSubject = $post['Subject'];
						$objNote->noteText = $description;						
						
						$objNote->noteObjectType = 3;
						$objNote->noteObject = $event_id;
						$objNote->createNote();						
					}
					
									
				}
				else if ($rec_type == 'note') {
					$objNote->noteCreator = $request_user_id;
					$objNote->noteCreationDate = $post['CreationDate'];
					$objNote->noteSubject = $post['Subject'];
					$objNote->noteText = $post['NoteText'];
					
					//1 means company
					//2 means contact
					//3 means event
					//4 means opportunity
					//5 means task
					
					$object_type = 1;
					$object_referer = $JettAccountID;	
				
					if($JettAccountID > 0) {
						$object_type = 1;
						$object_referer = $JettAccountID;	
					} else if($JettContactID > 0) {
						$object_type = 2;
						$object_referer = $JettContactID;	
					} else if($JettEventID > 0) {
						$object_type = 3;
						$object_referer = $JettEventID;	
					} 					

					$objNote->noteObjectType = $object_type;
					$objNote->noteObject = $object_referer;
					
					//$objNote->noteObject = $post['ObjectReferer'];														
					$result['status'] = $objNote->createNote();
				}			
				
				$status = '';
				if ($result['status']['STATUS'] == 'OK') {
					$status = 'OK';
					//$jet_id = ($rec_type == 'event') ? $result['status']['GETFORMATTEDVAR'] : $result['status']['RECID'];
					
					// Get the JetID
					if ($rec_type == 'event') {
						$jet_id = $result['status']['GETFORMATTEDVAR'];
					} else if ($rec_type == 'note') {
						$jet_id = str_replace('INSERT_', '', $result['status']['GETFORMATTEDVAR']);
					} else {
						$jet_id = $result['status']['RECID'];
					}
										
					
					$response_info = $doc->createElement( "Info" );
					$response_info->appendChild( $doc->createTextNode( implode(',', $result['status']['MESSAGE']) ) );  
					$response_node->appendChild( $response_info );

				}
				else if ($result['status']['STATUS'] == 'FALSE') {
					$status = 'FAILURE';					
					
					if(!empty($result['status']['ERROR']) && is_array($result['status']['ERROR'])) {
						foreach($result['status']['ERROR'] as $field_key=>$reason) {						
							$response_error = $doc->createElement( "Error" );
							$response_error->setAttribute('field', $field_key);
							$response_error->setAttribute('reason', $reason);

							$response_error->appendChild( $doc->createTextNode( $post[$field_key] ) );  
							$response_node->appendChild( $response_error );							
						}					
					}					
				}
			
				// set account attributes
				$response_node->setAttribute('Status', $status);
				$response_node->setAttribute('JetID', $jet_id);
				$response_node->setAttribute('SourceID', $client_id);
				$response_node->setAttribute('ImportID', $import_id);
			}
		}		
	}
	
	
	/**
	* Functrion to find the option id of the drop down.
	* Hadles boith account and contact
	*/
	private function GetOptionID($company_id, $text, $rec_type, $key) {
		
		$map = ($rec_type == 'account') ? 'AccountMap' : 'ContactMap';
		$option_id = 0;
		
		$sql = "SELECT o.OptionID
				FROM ".$map." m
				LEFT JOIN OptionSet os ON m.OptionSetID=os.OptionSetID
				LEFT JOIN [Option] o ON os.OptionSetID=o.OptionSetID
				WHERE m.CompanyID=? AND o.OptionName=? AND (o.CompanyID = ? OR o.CompanyID = -1) AND  m.FieldName = ?";
				
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_id), array(DTYPE_STRING, $text), array(DTYPE_INT, $company_id), array(DTYPE_STRING, $key));
		$rs = DbConnManager::GetDb('mpower')->getOne($sql);
					
		if ($rs instanceof MpRecord) {			
			$option_id = $rs->OptionID;
		} 	
		
		return $option_id;	
	}	
	

	/**
	* Functrion to find the event id of the drop down.	
	*/
	private function GetEventType($company_id, $event_type_name) {
		
		$event_type = array();
		
		$sql = "Select EventTypeID, Type from EventType where CompanyID = ? and EventName = ? and (Deleted = 0 or Deleted IS NULL)";
				
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_id), array(DTYPE_STRING, $event_type_name));
		$rs = DbConnManager::GetDb('mpower')->getOne($sql);
					
		if ($rs instanceof MpRecord) {			
			$event_type['ID'] = $rs->EventTypeID;
			$event_type['TYPE'] = $rs->Type;
		} else {
			$event_type['ID'] = '-1';
			$event_type['TYPE'] = 'EVENT';
		} 	
		
		return $event_type;	
	}	
	
	/**
	 * writes to a log file
	 */
	private function WriteToLog($msg) {
		if ($this->debug) file_put_contents($this->log_file, $msg."\n\n", FILE_APPEND);
	}
}
