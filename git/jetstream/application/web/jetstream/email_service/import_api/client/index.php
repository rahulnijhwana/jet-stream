<?php

  $doc = new DOMDocument();
  $doc->formatOutput = true;

  $response = $doc->createElement( "Response" );
  $doc->appendChild( $response );

  $response_status = $doc->createElement( "Status" );
  $response->appendChild( $response_status );

  // set status attributes
  $response_status->setAttribute('code', 'OK');






  // set response attributes
  $response->setAttribute('CompanyID', '10');
  $response->setAttribute('UserID', '345');
  $response->setAttribute('Timestamp', '2009-03-03T11:13:30');
  $response->setAttribute('BatchID', '342234');


  $response_account = $doc->createElement( "Account" );
  $response->appendChild( $response_account );

  // set account attributes
  $response_account->setAttribute('Status', 'FAILURE');
  $response_account->setAttribute('JetID', '87848');
  $response_account->setAttribute('SourceID', 'CL125');
  $response_account->setAttribute('ImportID', '0E543973-6356-407f-8b0c-9fc40acfb0d6');



  $response_info = $doc->createElement( "Info" );
  $response_warning = $doc->createElement( "Warning" );
  $response_error = $doc->createElement( "Error" );

  $response_info->appendChild( $doc->createTextNode( ' This is good.' ) );
  $response_status->appendChild( $response_info );

  $response_info->appendChild( $doc->createTextNode( 'Hello how are you?' ) );
  $response_account->appendChild( $response_info );








  /*
  foreach( $books as $book )
  {
  $b = $doc->createElement( "book" );

  $author = $doc->createElement( "author" );
  $author->appendChild(
  $doc->createTextNode( $book['author'] )
  );
  $b->appendChild( $author );

  $title = $doc->createElement( "title" );
  $title->appendChild(
  $doc->createTextNode( $book['title'] )
  );
  $b->appendChild( $title );

  $publisher = $doc->createElement( "publisher" );
  $publisher->appendChild(
  $doc->createTextNode( $book['publisher'] )
  );
  $b->appendChild( $publisher );

  $r->appendChild( $b );
  }
  */
  echo $doc->saveXML();

