<?PHP
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.Log.php';
require_once BASE_PATH . '/include/class.Authentication.php';


/**
* The EmailWebServices class. It contains the buissness logic for Email web services
* @package WebServices
*/
class EmailTemplate
{
	private $valid_user;
	private $template;
	private $success_status;
	private $failure_message;

	function __construct() {
	}


	private function isNodeExist($nodes) {
		return (count($nodes) > 0) ? true : false;
	}

	
	/**
	 * Parse the xml request for authentication of the user
	 */
	public function Request($emailRequest) {

	 	$objDOM = new DOMDocument();
		$objDOM->loadXML($emailRequest);

		$user_node = $objDOM->getElementsByTagName("User")->item(0);

		if ($this->isNodeExist($user_node)) {

			// Get user attributes
			$company_dir = $user_node->getElementsByTagName("CompanyID")->item(0)->nodeValue;
			$username = $user_node->getElementsByTagName("UserID")->item(0)->nodeValue;
			$password = $user_node->getElementsByTagName("Password")->item(0)->nodeValue;
			
			// Validate user
			if (!empty($username) && !empty($password) && !empty($company_dir)) {
				$company_record = Authentication::GetCompanyRecord($company_dir);
				$company_id = $company_record->CompanyID;
				
				$login = Authentication::GetLoginRecord($company_id, $username, $password);				

				if (empty($login)) {					
					$this->valid_user = false;
					$this->failure_message = 'Invalid user';
					
				} else {
					$this->valid_user = true;					
					
					// get the template for the user
					$sql = "SELECT TemplateData FROM EmailTemplate WHERE PersonID = ?";
					$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $login->PersonID));
					$template = DbConnManager::GetDb('mpower')->GetOne($sql);
					if(!empty($template)) {
						$this->template = $template->TemplateData;
					}					
				}
			} else {
				$this->valid_user = false;
				$this->failure_message = 'Invalid user';
			}
		}
	}
	

	public function Response() {
	 	$objDOM = new DOMDocument();
		$objDOM->load(BASE_PATH.'/email_service/xmlTemplate/EmailTemplateResponse.xml');
		
		// success status boolean convert it to string to represent in xml
		$success_node = $objDOM->getElementsByTagName('SuccessStatus')->item(0);
		$success_text = $objDOM->createTextNode($this->valid_user ? 'True' : 'False');
		$success_node->appendChild($success_text);
		
		// template base64 encoded
		$template = $objDOM->getElementsByTagName('Template')->item(0);
		//$template_text = $objDOM->createTextNode(htmlspecialchars_decode('<div>test</div>'));
		$template_text = $objDOM->createCDATASection($this->template);
		$template->appendChild($template_text);
		
		// failure message if failed
		$failure_message_node = $objDOM->getElementsByTagName('FailureMessage')->item(0);
		$failure_text = $objDOM->createTextNode($this->failure_message);
		$failure_message_node->appendChild($failure_text);

		return $objDOM->saveXML();
	}
}
