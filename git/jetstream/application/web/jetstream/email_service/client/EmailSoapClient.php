<?php

require_once('../includes/nusoap.php');


//$loginRequest = file_get_contents('login.xml');


// Create instance of soap client
$soapClientObject = new soap_client("https://www.mpasa.com/email_service/EmailServer.php");
$authenticationRequest = getAuthenticationRequest();
$authenticationResponse = $soapClientObject->call('Authenticate', array($authenticationRequest));

echo '<br>--->AuthenticationRequest<br>';
echo '<div style="background-color:#FFFFFF"><code>'.htmlspecialchars($authenticationRequest).'</code></div>';
echo '<br>--->AuthenticationResponse<br>';
echo '<div style="background-color:#FFFFFF"><code>'.htmlspecialchars($authenticationResponse).'</code></div>';



$objDOM = new DOMDocument();
$objDOM->loadXML($authenticationResponse);
$user_node = $objDOM->getElementsByTagName("SuccessStatus")->item(0);		
$valid_user = $user_node->getElementsByTagName("ValidUser")->item(0)->nodeValue;	
$add_email = $user_node->getElementsByTagName("AddEmail")->item(0)->nodeValue;
$token = $user_node->getElementsByTagName("Token")->item(0)->nodeValue;
$failure_message = $objDOM->getElementsByTagName("FailureMessage")->item(0)->nodeValue;

$addEmailResponse = $addEmailRequest = '';
if ($valid_user == 'True' && $add_email == 'True') {
	$addEmailRequest = getAddEmail($token);
	//$ClientObject = new soap_client("http://localhost/email_service/EmailServer.php");
	$addEmailResponse = $soapClientObject->call('AddEmail', array($addEmailRequest));	
}

echo '<br>--->AddEmailRequest<br>';
echo '<div style="background-color:#99CC00"><code>'.htmlspecialchars($addEmailRequest).'</code></div>';
echo '<br>--->AddEmailResponse<br>';
echo '<div style="background-color:#99CC00"><code>'.htmlspecialchars($addEmailResponse).'</code></div>';



function getAuthenticationRequest() {
	 	$objDOM = new DOMDocument();
		$objDOM->load('AuthenticationRequest.xml');		
		
		$entry_id_node = $objDOM->getElementsByTagName('EntryID')->item(0);		
		$text = $objDOM->createTextNode($_POST['EntryID']);
		$entry_id_node->appendChild($text);
				
		$user_node = $objDOM->getElementsByTagName('User')->item(0);
		
		$company_node = $user_node->getElementsByTagName('CompanyID')->item(0);		
		$text = $objDOM->createTextNode($_POST['Company']);
		$company_node->appendChild($text);

		$user_id_node = $user_node->getElementsByTagName('UserID')->item(0);		
		$text = $objDOM->createTextNode($_POST['User']);
		$user_id_node->appendChild($text);

		$password_node = $user_node->getElementsByTagName('Password')->item(0);		
		$text = $objDOM->createTextNode($_POST['Password']);
		$password_node->appendChild($text);

		$email_node = $user_node->getElementsByTagName('EmailID')->item(0);		
		$text = $objDOM->createTextNode($_POST['Email']);
		$email_node->appendChild($text);

		$role_node = $user_node->getElementsByTagName('Role')->item(0);		
		$text = $objDOM->createTextNode($_POST['Role']);
		$role_node->appendChild($text);
		
		return $objDOM->saveXML();
}


function getAddEmail($token='') {

	 	$objDOM = new DOMDocument();
		$objDOM->load('AddEmailrequest.xml');		
		
		$token_node = $objDOM->getElementsByTagName('Token')->item(0);		
		$text = $objDOM->createTextNode($token);
		$token_node->appendChild($text);
				
		$from_node = $objDOM->getElementsByTagName('From')->item(0);
		
		$from_display_name_node = $from_node->getElementsByTagName('DisplayName')->item(0);		
		$text = $objDOM->createTextNode($_POST['FromDisplayName']);
		$from_display_name_node->appendChild($text);
		
		$from_email_node = $from_node->getElementsByTagName('EmailID')->item(0);		
		$text = $objDOM->createTextNode($_POST['FromEmailID']);
		$from_email_node->appendChild($text);
		
		$to_node = $objDOM->getElementsByTagName('To')->item(0);
		
		$to_display_name_node = $to_node->getElementsByTagName('DisplayName')->item(0);		
		$text = $objDOM->createTextNode($_POST['ToDisplayName']);
		$to_display_name_node->appendChild($text);
		
		$to_email_node = $to_node->getElementsByTagName('EmailID')->item(0);		
		$text = $objDOM->createTextNode($_POST['ToEmailID']);
		$to_email_node->appendChild($text);		


		$cc_node = $objDOM->getElementsByTagName('Cc')->item(0);
		
		$cc_display_name_node = $cc_node->getElementsByTagName('DisplayName')->item(0);		
		$text = $objDOM->createTextNode($_POST['CcDisplayName']);
		$cc_display_name_node->appendChild($text);
		
		$cc_email_node = $cc_node->getElementsByTagName('EmailID')->item(0);		
		$text = $objDOM->createTextNode($_POST['CcEmailID']);
		$cc_email_node->appendChild($text);	
		
		$received_node = $objDOM->getElementsByTagName('ReceivedDate')->item(0);		
		$text = $objDOM->createTextNode($_POST['ReceivedDate']);
		$received_node->appendChild($text);		
		
		$subject_node = $objDOM->getElementsByTagName('Subject')->item(0);		
		$text = $objDOM->createTextNode($_POST['Subject']);
		$subject_node->appendChild($text);		

		$body_node = $objDOM->getElementsByTagName('Body')->item(0);		
		$text = $objDOM->createTextNode(base64_encode($_POST['Body']));
		$body_node->appendChild($text);				
				
		return $objDOM->saveXML();

}


//print_r($_POST);
?>
