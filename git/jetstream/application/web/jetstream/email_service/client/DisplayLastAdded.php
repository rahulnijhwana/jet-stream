<?php

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.Language.php';
require_once BASE_PATH . '/include/lib.security.php';

$emailid = getEmail();

$emails = getEmailContent($emailid);
echo '<p>'. $emailid.'</p>';

echo '<p><u>Email</u></p>';
echo '<table border="1">';

echo '<tr><td>EmailContentID</td><td>PersonID</td><td>EntryID</td><td>FromName</td><td>FromEmail</td><td>RecipientNames</td><td>RecipientEmails</td><td>CcNames</td><td>CcEmails</td><td>ReceivedDate</td><td>Subject</td><td>Body</td></tr>';

foreach($emails as $email) {
	echo '<tr><td>'.implode("</td><td>",$email).'</td></tr>';
}
echo '</table>';

echo '<hr>';


$emailaddresses = getEmailAddress($emailid);
$notes = getNotes($emailid);

//echo '<p>'. $emailid.'</p>';

echo '<p><u>Email Address</u></p>';
echo '<table border="1">';

echo '<tr><td>EmailAddressID</td><td>EmailContentID</td><td>ContactID</td><td>EmailAddressType</td><td>EmailAddress</td><td>PersonID</td><td>DisplayName</td></tr>';

foreach($emailaddresses as $emailaddress) {
	echo '<tr><td>'.implode("</td><td>",$emailaddress).'</td></tr>';
}
echo '</table>';


echo '<hr>';
echo '<p><u>Notes</u></p>';
echo '<table border="1">';
echo '<tr><td>NoteID</td><td>CreatedBy</td><td>ObjectType</td><td>ObjectReferer</td><td>EmailContentID</td><td>NoteSpecialType</td><td>PrivateNote</td><td>CompanyID</td></tr>';
	  
foreach($notes as $note) {
	echo '<tr><td>'.implode("</td><td>",$note).'</td></tr>';
}

echo '</table>';


// generates token
function getEmailAddress($emailid) {
	$sql = "Select * from EmailAddresses WHERE EmailContentID = $emailid";
	$data = DbConnManager::GetDb('mpower')->Exec($sql);	
	return $data;
}


function getNotes($emailid) {
	$sql = "Select NoteID, CreatedBy, ObjectType, ObjectReferer, EmailContentID, NoteSpecialType, PrivateNote, CompanyID from Notes WHERE EmailContentID = $emailid";
	$data = DbConnManager::GetDb('mpower')->Exec($sql);	
	return $data;
}

function getEmailContent($emailid) {
	$sql = "Select EmailContentID, PersonID , EntryID, FromName, FromEmail, RecipientNames, RecipientEmails, CcNames, 
			CcEmails, ReceivedDate, Subject, Body  from EmailContent WHERE EmailContentID = $emailid";
	$data = DbConnManager::GetDb('mpower')->Exec($sql);	
	return $data;
}

// saves the token in database
function getEmail() {
	$sql = "Select TOP 1 EmailContentID FROM EmailContent Order by EmailContentID desc";
	$data = DbConnManager::GetDb('mpower')->GetOne($sql);	
	return $data->EmailContentID;
}



?>