<?PHP
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/mpconstants.php';
require_once BASE_PATH . '/include/class.Authentication.php';
require_once BASE_PATH . '/email_service/class.EmailServiceHandler.php';
require_once BASE_PATH . '/slipstream/class.Email.php';


/**
* The EmailWebServices class. It contains the buissness logic for Email web services
* @package WebServices
*/
class EmailWebServices
{
	private $subject;
	private $body;
	private $attachments;
	private $email_content_id;

	private $user;
	private $from_name;
	private $from_email;
	private $to_name;
	private $to_email;
	private $cc_name;
	private $cc_email;
	private $contact;
	private $received_date;

	private $failure_message;
	public $debug;
	private $log_file;
	private $contact_email_field_list;
	public $validRequest;
	private $valid_user;
	private $add_email;
	private $success_status;
	private $token;
	private $recipient;

	function __construct() {
		$this->debug = false;
		$this->log_file = '/webtemp/logs/EmailServiceLog';
		#$this->log_file = 'EmailServiceLog';
	}


	private function isNodeExist($nodes) {
		return (count($nodes) > 0) ? true : false;
	}

	/**
	 * Parse the xml request for authentication of the user
	 */
	public function AuthenticateRequest($emailRequest) {

	 	$objDOM = new DOMDocument();
		@$objDOM->loadXML($emailRequest);

		$user_node = $objDOM->getElementsByTagName("User")->item(0);
		$entry_id_node = $objDOM->getElementsByTagName("EntryID")->item(0);

		if ($this->isNodeExist($user_node) && $this->isNodeExist($entry_id_node)) {

			// Get user attributes
			$company_dir = $user_node->getElementsByTagName("CompanyID")->item(0)->nodeValue;
			$username = $user_node->getElementsByTagName("UserID")->item(0)->nodeValue;
			$password = $user_node->getElementsByTagName("Password")->item(0)->nodeValue;
			$user_email = $user_node->getElementsByTagName("EmailID")->item(0)->nodeValue;
			$user_role = trim($user_node->getElementsByTagName("Role")->item(0)->nodeValue);

			$entry_id = trim($entry_id_node->nodeValue);

			if (!empty($entry_id)) {
				// Validate user
				if (!empty($username) && !empty($password) && !empty($company_dir)) {
					//$company_id = $this->getCompany($company_dir);
					$company_record = Authentication::GetCompanyRecord($company_dir);
					if (!$company_record) {
						// error_log("Bad company: $company_dir $username $password");
						$this->failure_message = 'Invalid user';
						return;
					}
					$company_id = $company_record->CompanyID;
					
					$login = Authentication::GetLoginRecord($company_id, $username, $password);				

					if (empty($login)) {					
						$this->valid_user = false;
						$this->failure_message = 'Invalid user';
					} else {
						$this->valid_user = true;

						// check if the email exist
						if ($this->isEmailExists($entry_id, $company_id)) {
							$this->add_email = false;
							$this->failure_message = 'Email exists';
							//$this->createUserEmailRelation($user_role);
						} else {
							$user['company_id'] = $company_id;
							$user['person_id'] = $login->PersonID;
							$user['entry_id'] = $entry_id;
							$user['role'] = $user_role;
							$user['email'] = $user_email;
							$this->add_email = true;

							$objlog = new EmailServiceHandler($user);
							$objlog->Init();
							$objlog->Store();
							$this->token = $objlog->getToken();
							
							// store the email address in PeopleEmailAddr table if it is not present
							$this->storeUserEmail($user_email, $login->PersonID, $company_id);
						}
					}
				} else {
					$this->valid_user = false;
					$this->failure_message = 'Invalid user';
				}
			} else {
				$this->valid_user = false;
				$this->failure_message = 'Invalid email entryID';
			}
		}
	}
	
	function storeUserEmail($email, $person_Id, $company_id) {
		// check if the email exist in the db else create it.
		$email_sql = "Select count(*) Total from PeopleEmailAddr WHERE EmailAddr = ? AND PersonID = ?";
		$email_sql = SqlBuilder()->LoadSql($email_sql)->BuildSql(array(DTYPE_STRING, $email), array(DTYPE_INT, $person_Id));
		$email_rs = DbConnManager::GetDb('mpower')->GetOne($email_sql);
		//$this->writeToLog('Get Email Exist query: ' . $email_sql);
		if (!($email_rs instanceof MpRecord && $email_rs->Total > 0)) {
			// insert the record to People email address
			$sql = "INSERT INTO PeopleEmailAddr(PersonID, EmailAddr, isPrimaryEmailAddr, CompanyID)
					VALUES (?, ?, 0, ?)";
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(
					array(DTYPE_INT, $person_Id),
					array(DTYPE_STRING, $email),				
					array(DTYPE_INT, $company_id)
					);			
			$result = DbConnManager::GetDb('mpower')->Exec($sql);				
		}		
	}

	public function AuthenticateResponse() {

	 	$objDOM = new DOMDocument();
		$objDOM->load(BASE_PATH.'/email_service/xmlTemplate/AuthenticateResponse.xml');

		$success_node = $objDOM->getElementsByTagName('SuccessStatus')->item(0);

		$valid_user_node = $success_node->getElementsByTagName('ValidUser')->item(0);
		$text = $objDOM->createTextNode($this->valid_user ? 'True' : 'False');
		$valid_user_node->appendChild($text);

		if ($this->valid_user) {
			$add_email_node = $success_node->getElementsByTagName('AddEmail')->item(0);
			$text = $objDOM->createTextNode($this->add_email ? 'True' : 'False');
			$add_email_node->appendChild($text);

			if ($this->add_email) {
				$token_node = $success_node->getElementsByTagName('Token')->item(0);
				$text = $objDOM->createTextNode($this->token);
				$token_node->appendChild($text);
			}
		}

		$failure_message_node = $objDOM->getElementsByTagName('FailureMessage')->item(0);
		$text = $objDOM->createTextNode($this->failure_message);
		$failure_message_node->appendChild($text);

		return $objDOM->saveXML();
	}


	private function isEmailExists($entry_id, $company_id) {
		$email_sql = "SELECT count(*) AS Total FROM EmailContent WHERE EntryID = ? AND CompanyID = ?";
		$email_sql = SqlBuilder()->LoadSql($email_sql)->BuildSql(array(DTYPE_STRING, $entry_id), array(DTYPE_INT, $company_id));
		$email_rs = DbConnManager::GetDb('mpower')->GetOne($email_sql);
		$this->writeToLog('Get Email Exist query: ' . $email_sql);
		return ($email_rs instanceof MpRecord && $email_rs->Total > 0) ? true : false;
	}


	public function AddEmailRequset($emailRequest) {

	 	$objDOM = new DOMDocument();
		@$objDOM->loadXML($emailRequest);

		$valid = true;
		$token_node = $objDOM->getElementsByTagName("Token")->item(0);

		$token = '';
		if ($this->isNodeExist($token_node)) {
			$token = trim($token_node->nodeValue);
		} else $valid = false;

		// ERROR: No user is being declared with this service handler, so token
		// has no user values to build the token
		$objlog = new EmailServiceHandler();
		$objlog->Init($token);

		if ($objlog->Load()) {
			$this->user = $objlog->user;

			$from_node = $objDOM->getElementsByTagName("From")->item(0);
			if ($valid && $this->isNodeExist($from_node)) {
				$from_display_node = $from_node->getElementsByTagName("DisplayName")->item(0);
				if ($this->isNodeExist($from_display_node)) {
					$this->from_name = $from_display_node->nodeValue;
					$this->from_email = $from_node->getElementsByTagName("EmailID")->item(0)->nodeValue;
				}
			} else $valid = false;

			$to_node = $objDOM->getElementsByTagName("To")->item(0);
			if ($valid &&  $this->isNodeExist($to_node)) {
				$this->to_name = $to_node->getElementsByTagName("DisplayName")->item(0)->nodeValue;
				$this->to_email = $to_node->getElementsByTagName("EmailID")->item(0)->nodeValue;
			} else $valid = false;

			$cc_node = $objDOM->getElementsByTagName("Cc")->item(0);
			if ($valid && $this->isNodeExist($cc_node)) {

				$cc_display_node = $cc_node->getElementsByTagName("DisplayName")->item(0);
				if ($this->isNodeExist($cc_display_node)) {
					$this->cc_name = $cc_display_node->nodeValue;
					$this->cc_email = $cc_node->getElementsByTagName("EmailID")->item(0)->nodeValue;
				}
			} else $valid = false;

			$received_date_node = $objDOM->getElementsByTagName("ReceivedDate")->item(0);
			if ($valid && $this->isNodeExist($received_date_node)) {
				$this->received_date = $received_date_node->nodeValue;
			} else $valid = false;

			$email_subject = $email_body = '';

			$subject_node = $objDOM->getElementsByTagName("Subject")->item(0);
			if ($valid && $this->isNodeExist($subject_node)) $this->subject = $subject_node->nodeValue;
			else $valid = false;

			$email_body_node = $objDOM->getElementsByTagName("Body")->item(0);
			if ($valid && $this->isNodeExist($email_body_node)) $this->body = $this->decode($email_body_node->nodeValue);
			else $valid = false;

			$email_attachments = $objDOM->getElementsByTagName("Attachment");

			if ($valid && $this->isNodeExist($email_attachments)) {
				foreach ($email_attachments as $id => $attachment) {
					$name  = $this->decode($attachment->getAttribute('name'));
					$content = $attachment->nodeValue;

					$original_name = $name;
					$name = str_replace(' ', '_', $name);
					$stored_name = $this->user['entry_id'] . $name;
					$stored_name = str_replace('/', '', $stored_name);

					$this->attachments[$id]['original'] = $original_name;
					$this->attachments[$id]['store'] = $stored_name;
					$this->attachments[$id]['content'] 	= base64_decode($content);
				}
			} else $valid = false;

			if ($valid) {
				$this->validRequest = true;
				$this->success_status = true;
				$this->failure_message = '';
			} else {
				$this->validRequest = false;
				$this->success_status = false;
				$this->failure_message = 'Failed to parse xml';
			}

		} else {
			$this->validRequest = false;
			$this->success_status = false;
			$this->failure_message = 'Invalid token';
		}
	}


	/**
	* Stores the email in database.
	* @access public
	*/
	public function saveEmail() {
		$sql = "INSERT INTO EmailContent
			   		(PersonID, EntryID, FromName, FromEmail, RecipientNames, RecipientEmails,
			   		CcNames, CcEmails, ReceivedDate, Subject, Body, CompanyID, EmailServiceLogID)
		 			VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		$re1='(\\()';	# Any Single Character 1
		$re2='.*?';	# Non-greedy match on filler
		$re3='(\\))';	# Any Single Character 2

		$time = preg_replace( "/".$re1.$re2.$re3."/is", '', $this->received_date ); 

		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(
			array(DTYPE_INT, $this->user['person_id']),
			array(DTYPE_STRING, $this->user['entry_id']),
			array(DTYPE_STRING, $this->from_name),
			array(DTYPE_STRING, $this->from_email),
			array(DTYPE_STRING, $this->to_name),
			array(DTYPE_STRING, substr($this->to_email, 0, 499)),
			array(DTYPE_STRING, $this->cc_name),
			array(DTYPE_STRING, $this->cc_email),
			array(DTYPE_STRING, $time),
			array(DTYPE_STRING, $this->subject),
			array(DTYPE_STRING, $this->body),
			array(DTYPE_INT, $this->user['company_id']),
			array(DTYPE_INT, $this->user['service_id'])
		);
		
		$this->writeToLog('Saving EmailContent query: ' . $sql);
		$data = DbConnManager::GetDb('mpower')->Exec($sql);
		//$this->writeToLog('Email Saved OK');
		$identity_sql = 'select @@identity as EmailContentID';
		$identity = DbConnManager::GetDb('mpower')->getOne($identity_sql);
		$this->email_content_id = $identity->EmailContentID;
		//$this->writeToLog('Content id : ' . $identity->EmailContentID);
	}


	public function saveAttachments() {
		if (isset($this->attachments)) {
			foreach ($this->attachments as $attachment) {
				$path = ATTACHMENT_PATH . $attachment['store'];
				file_put_contents($path, $attachment['content']);

				$sql = "INSERT INTO EmailAttachments (EmailContentID, OriginalName, StoredName)	VALUES (?, ?, ?)";
				$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->email_content_id),
						array(DTYPE_STRING, $attachment['original']), array(DTYPE_STRING, $attachment['store']));

				$this->writeToLog('Save attachment query: ' . $sql);
				DbConnManager::GetDb('mpower')->Exec($sql);
			}
		}
	}


	public function AddEmailResponse() {
	 	$objDOM = new DOMDocument();
		$objDOM->load(BASE_PATH.'/email_service/xmlTemplate/AddEmailResponse.xml');

		$success_node = $objDOM->getElementsByTagName('SuccessStatus')->item(0);
		$text = $objDOM->createTextNode($this->success_status ? 'True' : 'False');
		$success_node->appendChild($text);

		$failure_message_node = $objDOM->getElementsByTagName('FailureMessage')->item(0);
		$text = $objDOM->createTextNode($this->failure_message);
		$failure_message_node->appendChild($text);

		return $objDOM->saveXML();
	}

	private function decode($encoded_str) {
		$decoded_str = base64_decode($encoded_str);
		$content = str_replace(chr(0), '', $decoded_str);
		return $content;
	}
	
	public function saveContacts() {

		$this->contact_email_field_list = Email::getContactEmailFields($this->user['company_id']);

		// check the from address and see if the contact is asa salesperson
		// save store to and cc

		$person_id = Email::getPersonID($this->user['company_id'], $this->from_email);
		$from_contact_id = Email::getContactID($this->user['company_id'], $this->from_email, $this->contact_email_field_list);		
		
		if ($person_id > 0) {
			$this->saveContact($this->to_email, $this->to_name, 'To');	
			$this->saveContact($this->cc_email, $this->cc_name, 'Cc');			
			$this->saveContact($this->from_email, $this->from_name, 'From');		
		} else if ($from_contact_id > 0) {
			// if the sender is a known contact
			$this->saveContact($this->from_email, $this->from_name, 'From');
		} else {
			//if the sender is outsider
			// for out sider we are keeping the to and cc email address for keeping track of email hold for user.
			$this->saveContact($this->to_email, $this->to_name, 'To');	
			$this->saveContact($this->cc_email, $this->cc_name, 'Cc');			
			$this->saveContact($this->from_email, $this->from_name, 'From');
		}
	}	

	private function saveContact($email_address, $email_names, $address_type) {
		$email_addresses = Email::getEmailAddressList($email_address, $email_names);

		if (!empty($email_addresses) && count($email_addresses) > 0) {
			foreach ($email_addresses as $display_name=>$email_addr) {
				if (!empty($email_addr)) {					
					$contact_id = Email::getContactID($this->user['company_id'], $email_addr, $this->contact_email_field_list);
					$person_id = Email::getPersonID($this->user['company_id'], $email_addr, $this->contact_email_field_list);				
					$this->recipient[$email_addr]['ContactID'] = $contact_id;
					$this->recipient[$email_addr]['PersonID'] =  $person_id;
					$this->recipient[$email_addr]['AddressType'] =  $address_type;

					// save the email address to email hold
					$sql = "INSERT INTO EmailAddresses(EmailContentID, PersonID, ContactID, EmailAddressType, EmailAddress, DisplayName) VALUES (?, ?, ?, ?, ?, ?)";
					$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $this->email_content_id), array(DTYPE_STRING, $person_id),
											array(DTYPE_INT, $contact_id), array(DTYPE_STRING, $address_type),
											array(DTYPE_STRING, $email_addr), array(DTYPE_STRING, $display_name));

					$this->writeToLog('Saving EmailAddresses query: ' . $sql);
					$data = DbConnManager::GetDb('mpower')->Exec($sql);				
				}
			}
		}
	}

	/*	
	public function isIgnoreList($email_addr) {	
		$sql = "SELECT count(*) Total FROM IgnoreEmailList WHERE EmailAddress = ? AND PersonID = ?";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $email_addr), array(DTYPE_INT, $person_id));
		$this->writeToLog('Get IgnoreEmailListID query: ' . $sql);
		$ignore_email_lists = DbConnManager::GetDb('mpower')->GetOne($sql);

		return (!empty($ignore_email_lists) && (count($ignore_email_lists->Total) > 0)) ? true : false;
	}
	*/


	public function saveNote($person_id, $private_note, $contact_id, $emailPrivacyEnabled) {
		$note = array();
		$note['CreatedBy'] = $person_id;
		$note['CompanyID'] = $this->user['company_id'];
		$note['CreationDate'] = $this->received_date;
		$note['ObjectReferer'] = $contact_id;
		$note['EmailContentID'] = $this->email_content_id;
		$note['PrivateNote'] = $private_note;
		$note['Incoming'] = $emailPrivacyEnabled;
		Email::createEmailNote($note);	
	}


	function createNote() {	
	
		$from_contact_id = Email::getContactID($this->user['company_id'], $this->from_email, $this->contact_email_field_list);
		$from_person_id = Email::getPersonID($this->user['company_id'], $this->from_email);	
		$emailPrivacyEnabled = Email::getEmailPrivacyEnabled($this->user['company_id']);
		
		// if sender is a user then create a note for each to and cc	
		if ($from_person_id > 0) {
			if (!empty($this->recipient) and count($this->recipient) > 0) {
			
				// check if all of the recipients are user make the note private.
				$is_all_salesperson = true;
				foreach ($this->recipient as $key => $emailaddress){
					if (!($emailaddress['PersonID'] > 0)) {
						$is_all_salesperson &= false;
					}
				}

				$private_note = ($is_all_salesperson) ? 1 : 0;
				
				foreach ($this->recipient as $emailaddress){
					if ($emailaddress['ContactID'] && ($emailaddress['AddressType'] != 'From')) {
						$this->saveNote($from_person_id, $private_note, $emailaddress['ContactID'], $emailPrivacyEnabled);
					}
				}
			}
		} else if ($from_contact_id > 0) {
			// if the sender is a known contact then save a note for that contact.
			$this->saveNote(0, 0, $from_contact_id, $emailPrivacyEnabled);
		}		
	}
	
	/**
	 * writes to a log file
	 */
	private function writeToLog($msg) {
		if ($this->debug) file_put_contents($this->log_file, $msg."\n\n", FILE_APPEND);
	}
}
