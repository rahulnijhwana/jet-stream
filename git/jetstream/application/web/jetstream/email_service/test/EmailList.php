<?php


define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
define('ATTACHMENT_PATH', BASE_PATH . '/email_service/attachments/');

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/email_service/class.EmailWebServices.php';
require_once BASE_PATH . '/email_service/includes/nusoap.php';


$sql = "SELECT EmailContentID, PersonID, EntryID, FromName, FromEmail, RecipientNames, RecipientEmails, CcNames, CcEmails, ReceivedDate, Subject, Body FROM EmailContent";
$sql_rs = DbConnManager::GetDb('mpower')->Exec($sql);

header ("content-type: text/xml");

echo '<Messages>';


foreach ($sql_rs as $email) {
	
	echo '<Email>';
	echo '<EmailContentID>' . htmlspecialchars($email['EmailContentID']) . '</EmailContentID>';
	echo '<PersonID>' . htmlspecialchars($email['PersonID']) . '</PersonID>';
	echo '<EntryID>'.$email['EntryID'].'</EntryID>';
	echo '<From>';
	echo '<FromName>' . htmlspecialchars($email['FromName']) . '</FromName>';
	echo '<FromEmail>'.$email['FromEmail'].'</FromEmail>';
	echo '</From>';
	echo '<To>';
	echo '<RecipientNames>' . htmlspecialchars($email['RecipientNames']) . '</RecipientNames>';
	echo '<RecipientEmails>'.$email['RecipientEmails'].'</RecipientEmails>';	
	echo '</To>';
	echo '<Cc>';	
	echo '<CcNames>'.$email['CcNames'].'</CcNames>';
	echo '<CcEmails>'.$email['CcEmails'].'</CcEmails>';
	echo '</Cc>';
	echo '<ReceivedDate>'.$email['ReceivedDate'].'</ReceivedDate>';
	echo '<Subject><![CDATA[' . htmlspecialchars($email['Subject']) . ']]></Subject>';
	//echo '<Body><![CDATA['. $email['Body'] . ']]></Body>';

	$attachment_sql = "SELECT EmailAttachmentID, OriginalName, StoredName FROM EmailAttachments WHERE EmailContentID = ".$email['EmailContentID'];
	$attachments = DbConnManager::GetDb('mpower')->Exec($attachment_sql);	
	
	echo '<Attachments>';
	foreach ($attachments as $attachment) {		
		echo '<Attachment>';
		echo '<OriginalName>'.htmlspecialchars($attachment['OriginalName']).'</OriginalName>';
		echo '<StoredName>'.htmlspecialchars($attachment['StoredName']).'</StoredName>';
		echo '</Attachment>';
	}
	echo '</Attachments>';	
	echo '</Email>';
}
echo '</Messages>';
?>
