<?php

define('BASE_PATH', realpath(dirname(__FILE__) . '/../..'));
define('ATTACHMENT_PATH', BASE_PATH . '/email_service/attachments/');

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/email_service/class.EmailWebServices.php';
require_once BASE_PATH . '/email_service/includes/nusoap.php';



function writeToLog($email_request, $type = '') {
	$timestamp = strtotime("now");	
	$filename = BASE_PATH.'/email_service/log/'.$timestamp.'_'.$type.'.xml';
	file_put_contents($filename, $email_request);
}


function Authenticate($email_request) {
	writeToLog($email_request, 'Authenticate');
	
	$email = new EmailWebServices();
	$email->AuthenticateRequest($email_request);
	
	writeToLog($email->AuthenticateResponse(), 'AuthenticateResponse');
	
	return $email->AuthenticateResponse();
}



function AddEmail($email_request) {	
	writeToLog($email_request, 'AddEmail');
	
	$email = new EmailWebServices();
	$email->AddEmailRequset($email_request);	
	
	if ($email->validRequest) {
		$email->saveEmail();
		$email->saveAttachments();
		$email->saveContacts();
		$email->creatNote();
	}
	
	writeToLog($email->AddEmailResponse(), 'AddEmailResponse');
	
	return $email->AddEmailResponse();
}




$authenticationResponse = Authenticate(file_get_contents('1228227874_Authenticate.xml'));

echo $authenticationResponse;

$objDOM = new DOMDocument();
$objDOM->loadXML($authenticationResponse);
$user_node = $objDOM->getElementsByTagName("SuccessStatus")->item(0);		
$valid_user = $user_node->getElementsByTagName("ValidUser")->item(0)->nodeValue;	
$add_email = $user_node->getElementsByTagName("AddEmail")->item(0)->nodeValue;
$token = $user_node->getElementsByTagName("Token")->item(0)->nodeValue;
$failure_message = $objDOM->getElementsByTagName("FailureMessage")->item(0)->nodeValue;

if($valid_user == 'True' && $add_email == 'True') {
	$res =  AddEmail(getAddEmail($token));
	echo $res;
}


function getAddEmail($token='') {

	 	$objDOM = new DOMDocument();
		$objDOM->load('1228227888_AddEmail.xml');		
		
		$token_node = $objDOM->getElementsByTagName('Token')->item(0);		
		$text = $objDOM->createTextNode($token);
		$token_node->appendChild($text);			
				
		return $objDOM->saveXML();
}
?>
