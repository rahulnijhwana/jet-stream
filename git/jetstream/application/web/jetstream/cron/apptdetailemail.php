<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.Language.php';
require_once BASE_PATH . '/include/class.ReplaceApptEmailTemplate.php';
require_once BASE_PATH . '/include/lib.date.php';


$max_note_added = 4;
$max_words_in_note = 100;

function statusOfMilestone($companyid,$dealid,$location){
        $stat_sql = "SELECT count(SubMilestones.SubID) as cnt 
			FROM SubMilestones LEFT OUTER JOIN
			SubAnswers ON SubMilestones.SubID = SubAnswers.SubID AND SubAnswers.DealID = ".$dealid."
			WHERE (SubMilestones.CompanyID = ".$companyid.") AND 
			(SubMilestones.Location = ".$location.") AND 
			(SubMilestones.Mandatory = 1) AND (SubAnswers.Answer IS NULL)
			AND (SubMilestones.IsHeading=0)";
                        $result = DbConnManager::GetDb('mpower')->getOne($stat_sql);
                        $count = $result->cnt;
        if($count == 0)
        $return = "(complete)";
        else 
        $return = "(incomplete)";
        
        return $return;
}


function WordTruncate($input, $numWords) 
{
        $input = ereg_replace('<br />','',$input);
        if(str_word_count($input,0)>$numWords) {
                $WordKey = str_word_count($input,1);
                $PosKey = str_word_count($input,2);
                reset($PosKey);
                foreach($WordKey as $key => &$value)
                {
                        $value=key($PosKey);
                        next($PosKey);
                }
                return substr($input,0,$WordKey[$numWords]);
        }
         else {return $input;}
}





$today_array = getdate();
$today_timestamp = mktime(00, 00, 00, $today_array['mon'], $today_array['mday'], $today_array['year']);
$starttime = date("Y-m-d H:i:s",mktime(00, 00, 00, $today_array['mon'], $today_array['mday'], $today_array['year']));
$endtime = date("Y-m-d H:i:s", mktime(23,59,59,$today_array['mon'], $today_array['mday'], $today_array['year']));

$lang_array = array(1=>'FMLabel','IPLabel','IPSLabel','DPLabel','DPSLabel','CLabel',9=>'RemoveLabel',10=>'TLabel');
//echo '<pre>';print_r($lang_array);echo '</pre>';exit;

$sql = "SELECT CompanyID FROM company where Disabled = 0 AND Slipstream = 1";	
	$company_rs = DbConnManager::GetDb('mpower')->Execute($sql);		
	$company_list = array();	 
	foreach($company_rs as $company) {
		$company_list[] = $company['CompanyID'];
	}
        
        //for($c=0;$c<count($company_list);$c++){ 

                $sql = "SELECT TemplateData FROM CompanyAccountContactTemp where RecType = 1 and CompanyID = ?";
                
                //$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_list[$c]));
                $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, 508));
                $result = DbConnManager::GetDb('mpower')->getOne($sql);
                $company_template = $result->TemplateData;
                
                
                $sql = "SELECT TemplateData FROM CompanyAccountContactTemp where RecType = 2 and CompanyID = ?";
                
                //$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_list[$c]));
                $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, 508));
                $result = DbConnManager::GetDb('mpower')->getOne($sql);
                $contact_template = $result->TemplateData;
                
                $sql = "SELECT FieldName FROM ContactMap where IsFirstName = 1 and CompanyID = ?";
                
                //$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_list[$c]));
                $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, 508));
                        $result = DbConnManager::GetDb('mpower')->getOne($sql);
                        $firstname_fieldname = $result->FieldName;
                        
                $sql = "SELECT FieldName FROM ContactMap where IsLastName = 1 and CompanyID = ?";
                
                //$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_list[$c]));
                $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, 508));
                        $result = DbConnManager::GetDb('mpower')->getOne($sql);
                        $lastname_fieldname = $result->FieldName;
                        
                $sql = "SELECT * FROM AccountMap where IsCompanyName = 1 and CompanyID = ?";
                
                //$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_list[$c]));
                $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, 508));
                $result = DbConnManager::GetDb('mpower')->getOne($sql);
                $companyname_fieldname = $result->FieldName;
                
                
                $sql = "SELECT PersonID, Email FROM people where CompanyID = ? and deleted = 0 and SupervisorID > -3";
               
                //$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_list[$c]));
                $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, 508));
                $people_rs = DbConnManager::GetDb('mpower')->Execute($sql);		
                $people_list = array();
                $people_email_list = array();
                foreach($people_rs as $people) {
                        $people_list[]          = $people['PersonID'];
                        $people_email_list[]    = $people['Email'];
                }
                
                
                for($i=0;$i<count($people_list);$i++){
                       if($people_list[$i] != ''&& $firstname_fieldname != '' && $lastname_fieldname != '' && $companyname_fieldname != '' && $company_template != '' && $contact_template != ''){
                       
                        $sql = "Select E.EventID, E.ContactID, E.DealID,  C.".$firstname_fieldname.", C.".$lastname_fieldname.
                                ", A.".$companyname_fieldname." AS companyName, convert(varchar, E.startdate, 108) as eventtime,
                                convert(varchar, E.startdate, 1) as eventdate, convert(varchar, E.startdate, 107) as eventdate2
                                from events E Left Join Contact C ON E.ContactID = C.ContactID
                                LEFT JOIN account A ON C.AccountID = A.AccountID
                                LEFT JOIN EventType ET ON E.EventTypeID = ET.EventTypeID
                                WHERE USERID = ?
                                and startdate >= '".$starttime ."'and startdate <='".$endtime."'
                                and IsCancelled = 0 and IsClosed = 0 and ET.Type = 'EVENT'
                                order by StartDate";
                        //$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, 3147));
                        $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $people_list[$i]));
                                
                               //echo $sql.'<br>';
                                
                                
                            $people_contact_rs = DbConnManager::GetDb('mpower')->Execute($sql);
                            $event_id = array();
                            $people_contact_fname = array();
                            $people_contact_lname = array();
                            $people_contact_companyname = array();
                            $people_event_startdate = array();
                            $people_event_starttime = array();
                            $people_event_startdate2 = array();
                            $event_dealid = array();
                            $people_contactid = array();
                            foreach($people_contact_rs as $people_contact) {
                                $people_contact_fname[] = $people_contact[$firstname_fieldname];
                                $people_contact_lname[] = $people_contact[$lastname_fieldname];
                                $people_contact_companyname[] = $people_contact['companyName'];
                                $people_event_startdate[] = $people_contact['eventdate'];
                                $people_event_starttime[] = $people_contact['eventtime'];
                                $people_event_startdate2[] = $people_contact['eventdate2'];
                                $event_id[] = $people_contact['EventID'];
                                $event_dealid[] = $people_contact['DealID'];
                                $people_contactid[] = $people_contact['ContactID'];
                            }
                           if(is_array($event_id) && count($event_id)>0){
                                for($j=0;$j<count($event_id);$j++){
                                if($people_contact_companyname[$j] != '' && ($people_contact_fname[$j] != '' || $people_contact_lname[$j] != '')){
                                    $event_timearr = array();
                                    $event_timearr = explode(':',$people_event_starttime[$j]);
                                    
                                    $msg_subject = 'Jetstream Mtg Notice - '.$people_contact_fname[$j].' '.$people_contact_lname[$j].' of '.$people_contact_companyname[$j].' '.$people_event_startdate[$j].' '.DrawTime(mktime($event_timearr[0],$event_timearr[1] , 00, $today_array['mon'], $today_array['mday'], $today_array['year']));
                                    $msg_body_header = 'You have an appointment scheduled with '.$people_contact_fname[$j].' '.$people_contact_lname[$j].' of '.$people_contact_companyname[$j].' on '.date('F j, Y', $today_timestamp).' at '.DrawTime(mktime($event_timearr[0],$event_timearr[1] , 00, $today_array['mon'], $today_array['mday'], $today_array['year']))."\n";
                                   
                                    $msg_body_contact = "\n".'Contact:'."\n".'----------------'."\n";
                                    
                                   
                                    //$msg_body_contact.= ReplaceEmailTemplate::ReplaceEmailTemplateContact($company_list[$c],$people_contactid[$j]);
                                    $msg_body_contact.= ReplaceEmailTemplate::ReplaceEmailTemplateContact(508,$people_contactid[$j],$contact_template);
                                    $msg_body_contact.= "\n";
                                    
                                    $msg_body_company = "\n".'Company:'."\n".'----------------'."\n";
                                    
                                    //$msg_body_company.=ReplaceEmailTemplate::ReplaceEmailTemplateCompany($company_list[$c],$people_contactid[$j]);
                                    $msg_body_company.=ReplaceEmailTemplate::ReplaceEmailTemplateCompany(508,$people_contactid[$j],$company_template);
                                    $msg_body_company.="\n";
                                    
                                   $msg_body_opp = '';
                                    if($event_dealid[$j] != ''){
                                           
                                                        
                                            $deal_sql = "select O.*, S.Name As source, S2.Name as category2, convert(varchar, O.FirstMeeting, 1) as firstoppdate,
                                                        convert(varchar, O.TargetDate, 1) as targetdate, V.Label from opportunities O
                                                        Left Join Sources S on O.SourceID = S.SourceID 
                                                        Left Join Sources2 S2 on O.Source2ID = S2.Source2ID
                                                        Left Join Valuations V on O.VLevel = V.VLevel and V.CompanyID = ?
                                                        where O.DealID = ? ";
                                            $deal_sql = SqlBuilder()->LoadSql($deal_sql)->BuildSql(array(DTYPE_INT, 508) ,array(DTYPE_INT, $event_dealid[$j]));
                                            //$deal_sql = SqlBuilder()->LoadSql($deal_sql)->BuildSql(array(DTYPE_INT, $event_dealid[$j]), array(DTYPE_INT, $company_list[$c]));
                                                //echo  $deal_sql.'<br>'           ;
                                            $event_deal_rs = DbConnManager::GetDb('mpower')->getOne($deal_sql);
                                            $oppor_division = $event_deal_rs->Division;
                                            $oppor_phase = $event_deal_rs->Category;
                                            $oppor_category = $event_deal_rs->category2;
                                            $oppor_source = $event_deal_rs->source;
                                            $oppor_createdate = $event_deal_rs->targetdate;
                                            $oppor_firstdate = $event_deal_rs->firstoppdate;
                                            $oppor_valuation_level = $event_deal_rs->VLevel;
                                            $oppor_valuation = $event_deal_rs->Label;
                                            $oppor_fmeeting_time = $event_deal_rs->FirstMeetingTime;
                                            $fmeeting_timearr = explode(':',$oppor_fmeeting_time);
                                               
                                                
                                            
                                            $msg_body_opp="\n".'Opportunity:'."\n".'-----------------'."\n";
                                            $msg_body_opp.= 'Company:  '.$people_contact_companyname[$j]."\n";
                                            $msg_body_opp.='Division:  '.$oppor_division."\n";
                                            $msg_body_opp.='Contact:  '.$people_contact_fname[$j].' '.$people_contact_lname[$j]."\n";
                                            $msg_body_opp.='Phase:  '.Language::Term($lang_array[$oppor_phase])."\n";
                                            if($oppor_category != '' || $oppor_category !=0)
                                            $msg_body_opp.='Category:  '.$oppor_category."\n";
                                            if($oppor_source != '' || $oppor_source !=0)
                                            $msg_body_opp.='Source:  '.$oppor_source."\n";
                                            if($oppor_valuation != '' || $oppor_valuation !=0)
                                            $msg_body_opp.='Valuation:  ['.$oppor_valuation_level.']  '.$oppor_valuation."\n";
                                            $msg_body_opp.='Assigned:  '.$oppor_createdate."\n";
                                            $msg_body_opp.='First Meeting:  '.$oppor_firstdate.', '.DrawTime(mktime($fmeeting_timearr[0],$fmeeting_timearr[1] , 00, $today_array['mon'], $today_array['mday'], $today_array['year']))."\n";
                                    }
                                    $msg_body_offer = '';
                                    if($event_dealid[$j] != ''){
                                            $offer_sql = "select PX.Qty, PX.Val, PX.ID, P.Name from Opp_Product_XRef PX left join products p 
                                                         on PX.ProductID = P.ProductID where DealId = ".$event_dealid[$j];
                                            
                                                            
                                            $offer_rs = DbConnManager::GetDb('mpower')->Execute($offer_sql);
                                            $offer_id = array();
                                            $offer_name = array();
                                            $offer_qty = array();
                                            $offer_val = array();
                                            foreach($offer_rs as $offer_list) {
                                                $offer_id[] = $offer_list['ID'];
                                                $offer_name[] = $offer_list['Name'];
                                                $offer_qty[] = $offer_list['Qty'];
                                                $offer_val[] = $offer_list['Val'];
                                            }
                                            if(count($offer_id)>0){
                                                    $msg_body_offer="\n".'Offerings: '."\n".'-----------------'."\n";
                                                    $gross = 0;
                                                    for($v=0;$v<count($offer_id);$v++){
                                                            $msg_body_offer.=$offer_name[$v].' - '.$offer_qty[$v].' @ '.'$'.$offer_val[$v].' : $'.$offer_qty[$v]*$offer_val[$v]."\n";
                                                            $gross = $gross + $offer_qty[$v]*$offer_val[$v];
                                                    }
                                                    $msg_body_offer.='========='."\n";
                                                    $msg_body_offer.='Est   $:     $'.$gross."\n";
                                            }
                                    }
                                    
                                    
                                    
                                    $msg_body_pm='';
                                    if($event_dealid[$j] != ''){
                                            
                                                    
                                             $pm_sql="SELECT SubMilestones.Prompt, SubAnswers.Answer
                                                        FROM SubMilestones LEFT OUTER JOIN
                                                        SubAnswers ON SubMilestones.SubID = SubAnswers.SubID AND
                                                        SubAnswers.DealID = ?
                                                        WHERE (SubMilestones.CompanyID = ?) AND 
                                                        (SubMilestones.Location = 1) AND 
                                                        (SubMilestones.Mandatory = 1) 
                                                        AND (SubMilestones.IsHeading=0)
                                                        order by Seq ASC";
                                                        
                                            $pm_sql = SqlBuilder()->LoadSql($pm_sql)->BuildSql(array(DTYPE_INT, $event_dealid[$j]), array(DTYPE_INT, 508));
                                            //$pm_sql = SqlBuilder()->LoadSql($pm_sql)->BuildSql(array(DTYPE_INT, $event_dealid[$j]), array(DTYPE_INT, $company_list[$c]));
                                            //echo $pm_sql.'<br>';
                                            $pm_rs = DbConnManager::GetDb('mpower')->Execute($pm_sql);
                                            $pm_prompt = array();
                                            $pm_answer = array();
                                            
                                            foreach($pm_rs as $pm_list) {
                                                $pm_prompt[] = $pm_list['Prompt'];
                                                $pm_answer[] = $pm_list['Answer'];
                                            }
                                            //echo '<pre>';print_r($pm_answer);echo '</pre>';
                                            $msg_body_pm="\n".'Person Milestone:   '.statusOfMilestone(508,$event_dealid[$j],1)."\n".'-----------------'."\n";
                                            //$msg_body_pm="\n".'Person Milestone:   '.statusOfMilestone($company_list[$c],$event_dealid[$j],1)."\n".'-----------------'."\n";
                                            for($w=0;$w<count($pm_prompt);$w++){
                                                 if($pm_answer[$w] != '')
                                                 {
                                                    if($pm_answer[$w] == 1)
                                                    $pm_answer[$w] = 'Yes';
                                                    else if ($pm_answer[$w] == 1)
                                                    $pm_answer[$w] = 'No';
                                                    $msg_body_pm.= $pm_prompt[$w].':   '.$pm_answer[$w]."\n";
                                                 }
                                                 else
                                                 $msg_body_pm.= $pm_prompt[$w].':   ________'."\n";
                                            }
                                    }
                                    
                                    
                                    $msg_body_nm='';
                                    if($event_dealid[$j] != ''){
                                         
                                                    
                                              $nm_sql="SELECT SubMilestones.Prompt, SubAnswers.Answer
                                                        FROM SubMilestones LEFT OUTER JOIN
                                                        SubAnswers ON SubMilestones.SubID = SubAnswers.SubID AND
                                                        SubAnswers.DealID = ?
                                                        WHERE (SubMilestones.CompanyID = ?) AND 
                                                        (SubMilestones.Location = 2) AND 
                                                        (SubMilestones.Mandatory = 1) 
                                                        AND (SubMilestones.IsHeading=0)
                                                        order by Seq ASC";
                                                        
                                              $nm_sql = SqlBuilder()->LoadSql($nm_sql)->BuildSql(array(DTYPE_INT, $event_dealid[$j]), array(DTYPE_INT, 508));
                                             //$nm_sql = SqlBuilder()->LoadSql($nm_sql)->BuildSql(array(DTYPE_INT, $event_dealid[$j]), array(DTYPE_INT, $company_list[$c]));
                                            
                                            $nm_rs = DbConnManager::GetDb('mpower')->Execute($nm_sql);
                                            $nm_prompt = array();
                                            $nm_answer = array();
                                            
                                            foreach($nm_rs as $nm_list) {
                                                $nm_prompt[] = $nm_list['Prompt'];
                                                $nm_answer[] = $nm_list['Answer'];
                                            }
                                            //echo '<pre>';print_r($nm_prompt);echo '</pre>';
                                            $msg_body_nm="\n".'Need Milestone:   '.statusOfMilestone(508,$event_dealid[$j],2)."\n".'-----------------'."\n";
                                            //$msg_body_nm="\n".'Need Milestone:   '.statusOfMilestone($company_list[$c],$event_dealid[$j],2)."\n".'-----------------'."\n";
                                            for($x=0;$x<count($nm_prompt);$x++){
                                                 if($nm_answer[$x] != ''){
                                                    if($nm_answer[$x] == 1)
                                                    $nm_answer[$x] = 'Yes';
                                                    else if ($nm_answer[$x] == 1)
                                                    $nm_answer[$x] = 'No';
                                                    $msg_body_nm.= $nm_prompt[$x].':   '.$nm_answer[$x]."\n";
                                                 }
                                                 
                                                 else
                                                 $msg_body_nm.= $nm_prompt[$x].':   ________'."\n";
                                            }
                                    }
                                    
                                    $msg_body_mm='';
                                    if($event_dealid[$j] != ''){
                                           
                                                    
                                            $mm_sql="SELECT SubMilestones.Prompt, SubAnswers.Answer
                                                        FROM SubMilestones LEFT OUTER JOIN
                                                        SubAnswers ON SubMilestones.SubID = SubAnswers.SubID AND
                                                        SubAnswers.DealID = ?
                                                        WHERE (SubMilestones.CompanyID = ?) AND 
                                                        (SubMilestones.Location = 3) AND 
                                                        (SubMilestones.Mandatory = 1) 
                                                        AND (SubMilestones.IsHeading=0)
                                                        order by Seq ASC";
                                                        
                                             $mm_sql = SqlBuilder()->LoadSql($mm_sql)->BuildSql(array(DTYPE_INT, $event_dealid[$j]), array(DTYPE_INT, 508));
                                             //$mm_sql = SqlBuilder()->LoadSql($mm_sql)->BuildSql(array(DTYPE_INT, $event_dealid[$j]), array(DTYPE_INT, $company_list[$c]));
                                            
                                            $mm_rs = DbConnManager::GetDb('mpower')->Execute($mm_sql);
                                            $mm_prompt = array();
                                            $mm_answer = array();
                                            
                                            foreach($mm_rs as $mm_list) {
                                                $mm_prompt[] = $mm_list['Prompt'];
                                                $mm_answer[] = $mm_list['Answer'];
                                            }
                                            //echo '<pre>';print_r($mm_prompt);echo '</pre>';
                                            $msg_body_mm="\n".'Money Milestone:   '.statusOfMilestone(508,$event_dealid[$j],3)."\n".'-----------------'."\n";
                                            //$msg_body_mm="\n".'Money Milestone:   '.statusOfMilestone($company_list[$c],$event_dealid[$j],3)."\n".'-----------------'."\n";
                                            for($y=0;$y<count($mm_prompt);$y++){
                                                 if($mm_answer[$y] != ''){
                                                    if($mm_answer[$y] == 1)
                                                    $mm_answer[$y] = 'Yes';
                                                    else if ($mm_answer[$y] == 1)
                                                    $mm_answer[$y] = 'No';
                                                    $msg_body_mm.= $mm_prompt[$y].':   '.$mm_answer[$y]."\n";
                                                 }
                                                 
                                                 else
                                                 $msg_body_mm.= $mm_prompt[$y].':   ________'."\n";
                                            }
                                    }
                                    
                                    $msg_body_tm='';
                                    if($event_dealid[$j] != ''){
                                          
                                                    
                                             $tm_sql="SELECT SubMilestones.Prompt, SubAnswers.Answer
                                                        FROM SubMilestones LEFT OUTER JOIN
                                                        SubAnswers ON SubMilestones.SubID = SubAnswers.SubID AND
                                                        SubAnswers.DealID = ?
                                                        WHERE (SubMilestones.CompanyID = ?) AND 
                                                        (SubMilestones.Location = 4) AND 
                                                        (SubMilestones.Mandatory = 1) 
                                                        AND (SubMilestones.IsHeading=0)
                                                        order by Seq ASC";
                                                        
                                             $tm_sql = SqlBuilder()->LoadSql($tm_sql)->BuildSql(array(DTYPE_INT, $event_dealid[$j]), array(DTYPE_INT, 508));
                                             //$tm_sql = SqlBuilder()->LoadSql($tm_sql)->BuildSql(array(DTYPE_INT, $event_dealid[$j]), array(DTYPE_INT, $company_list[$c]));
                                            
                                            $tm_rs = DbConnManager::GetDb('mpower')->Execute($tm_sql);
                                            $tm_prompt = array();
                                            $tm_answer = array();
                                            
                                            foreach($tm_rs as $tm_list) {
                                                $tm_prompt[] = $tm_list['Prompt'];
                                                $tm_answer[] = $tm_list['Answer'];
                                            }
                                            //echo '<pre>';print_r($tm_prompt);echo '</pre>';
                                            $msg_body_tm="\n".'Time Milestone:   '.statusOfMilestone(508,$event_dealid[$j],4)."\n".'-----------------'."\n";
                                            //$msg_body_tm="\n".'Time Milestone:   '.statusOfMilestone($company_list[$c],$event_dealid[$j],4)."\n".'-----------------'."\n";
                                            for($z=0;$z<count($tm_prompt);$z++){
                                                 if($tm_answer[$z] != '')
                                                 {
                                                    if($tm_answer[$z] == 1)
                                                    $tm_answer[$z] = 'Yes';
                                                    else if ($tm_answer[$z] == 1)
                                                    $tm_answer[$z] = 'No';
                                                    $msg_body_tm.= $tm_prompt[$z].':   '.$tm_answer[$z]."\n";
                                                 }
                                                 else
                                                 $msg_body_tm.= $tm_prompt[$z].':   ________'."\n";
                                            }
                                    }
                                    $msg_body_note='';
                                    if($event_dealid[$j] != ''){
                                            $note_sql = "select Top ".$max_note_added." N.NoteID, N.NoteText, N.Subject, convert(varchar, N.CreationDate, 1) as notedate,
                                                            convert(varchar, N.CreationDate, 108) as notetime, P.FirstName, P.LastName 
                                                            from Notes N left join People P on N.createdby = P.personid where
                                                            ( N.ObjectReferer in (select eventid from events where dealid = ?) and ObjectType = 3) or ( N.ObjectReferer = ? and ObjectType = 4) 
                                                            order by CreationDate desc";
                                             $note_sql = SqlBuilder()->LoadSql($note_sql)->BuildSql(array(DTYPE_INT, $event_dealid[$j]), array(DTYPE_INT, $event_dealid[$j]));
                                             
                                            $note_rs=DbConnManager::GetDb('mpower')->Execute($note_sql);
                                            $note_id = array();
                                            $note_text = array();
                                            $note_subject = array();
                                            $note_date = array();
                                            $note_time = array();
                                            $note_create_fname = array();
                                            $note_create_lname = array();
                                            foreach($note_rs as $note_list) {
                                                    $note_id[] = $note_list['NoteID'];
                                                    $note_text[] = $note_list['NoteText'];
                                                    $note_subject[] = $note_list['Subject'];
                                                    $note_date[] = $note_list['notedate'];
                                                    $note_time[] = $note_list['notetime'];
                                                    $note_create_fname[] = $note_list['FirstName'];
                                                    $note_create_lname[] = $note_list['LastName'];
                                            }
                                            //echo '<pre>';print_r($note_id);echo '</pre>';
                                            if(count($note_id)>0){
                                                    $msg_body_note="\n".'Notes:'."\n".'---------'."\n";
                                                    for($n=0;$n<count($note_id);$n++){
                                                        $note_timearr = explode(':',$note_time[$n]);
                                                        
                                                        $msg_body_note.='Date Stamp:  '.$note_date[$n].', '.DrawTime(mktime($note_timearr[0],$note_timearr[1] , 00, $today_array['mon'], $today_array['mday'], $today_array['year']))."\n";
                                                        $msg_body_note.='Salesperson:  '.$note_create_fname[$n].' '.$note_create_lname[$n]."\n";
                                                        if($note_subject[$n] != '')
                                                        $msg_body_note.='Subject:  '.$note_subject[$n]."\n";
                                                        $msg_body_note.=WordTruncate($note_text[$n], $max_words_in_note)."\n\n";
                                                    }
                                                    
                                            }
                                    }
                                    else {
                                            $note_sql = "select Top ".$max_note_added." N.NoteID, N.NoteText, N.Subject, convert(varchar, N.CreationDate, 1) as notedate,
                                                            convert(varchar, N.CreationDate, 108) as notetime, P.FirstName, P.LastName 
                                                            from Notes N left join People P on N.createdby = P.personid where
                                                            ( N.ObjectReferer = ? and ObjectType = 3) 
                                                            order by CreationDate desc";
                                             $note_sql = SqlBuilder()->LoadSql($note_sql)->BuildSql(array(DTYPE_INT, $event_id[$j]));
                                             
                                            $note_rs=DbConnManager::GetDb('mpower')->Execute($note_sql);
                                            $note_id = array();
                                            $note_text = array();
                                            $note_subject = array();
                                            $note_date = array();
                                            $note_time = array();
                                            $note_create_fname = array();
                                            $note_create_lname = array();
                                            foreach($note_rs as $note_list) {
                                                    $note_id[] = $note_list['NoteID'];
                                                    $note_text[] = $note_list['NoteText'];
                                                    $note_subject[] = $note_list['Subject'];
                                                    $note_date[] = $note_list['notedate'];
                                                    $note_time[] = $note_list['notetime'];
                                                    $note_create_fname[] = $note_list['FirstName'];
                                                    $note_create_lname[] = $note_list['LastName'];
                                            }
                                            //echo '<pre>';print_r($note_id);echo '</pre>';
                                            if(count($note_id)>0){
                                                    $msg_body_note="\n".'Notes:'."\n".'---------'."\n";
                                                    for($n=0;$n<count($note_id);$n++){
                                                        $note_timearr = explode(':',$note_time[$n]);
                                                        
                                                        $msg_body_note.='Date Stamp:  '.$note_date[$n].', '.DrawTime(mktime($note_timearr[0],$note_timearr[1] , 00, $today_array['mon'], $today_array['mday'], $today_array['year']))."\n";
                                                        $msg_body_note.='Salesperson:  '.$note_create_fname[$n].' '.$note_create_lname[$n]."\n";
                                                        if($note_subject[$n] != '')
                                                        $msg_body_note.='Subject:  '.$note_subject[$n]."\n";
                                                        $msg_body_note.=WordTruncate($note_text[$n], $max_words_in_note)."\n\n";
                                                    }
                                                    
                                            }
                                    }
                                    
                                    
                                    $msg_body_complete = $msg_body_header.$msg_body_company.$msg_body_contact.$msg_body_opp;
                                    $msg_body_complete.= $msg_body_offer.$msg_body_pm.$msg_body_nm.$msg_body_mm.$msg_body_tm;
                                    $msg_body_complete.=$msg_body_note;
                                    mail($people_email_list[$i],
                                    $msg_subject,
                                    $msg_body_complete, "From: jetstream@asasales.com");
                                    $msg_body_complete='';
                                }
                                }
                                
                                
                            }
                        }
                        
                }
        //}
?>
