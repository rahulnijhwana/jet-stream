<?php

$company_id = 370; //85;
$date_limit = '6/18/2005';

// db params
//$myServer = "ASA-VERIO";
//$myUser = "mpasac";
//$myPass = "qraal+zv";
//$myDb = "test";
$myServer = "192.168.55.131";
$myUser = "sa";
$myPass = "tech";
$myDb = "mpasac_test";

writeToLog('Connecting....');

$dbhandle = connect($myServer, $myUser, $myPass, $myDb);
if(!$dbhandle) {
	writeToLog('Connection Error.');
	die('Connection Error');
}


writeToLog('Connected.');

/* Function to connect to the db. */
function connect($myServer, $myUser, $myPass, $myDB) {

	// connection to the database
	$dbhandle = @mssql_connect($myServer, $myUser, $myPass);
	if($dbhandle) {
		// select a database to work with
		$selected = @mssql_select_db($myDB, $dbhandle);
		if($selected) {
			return $dbhandle;
		}
	}
	return false;
}

function writeToLog($msg) {
	file_put_contents('log.txt', $msg."\n", FILE_APPEND);
}


// returns all the records from db in an array
function getRecord($sql) {
	writeToLog($sql);
	$record = array();
	$result = mssql_query($sql) or die("MS-Query Error in select-query");
	while ($row = mssql_fetch_assoc($result)) {
		$record[] = $row;
	}
	return $record;
}

// Function to handle sql escape string
function escapeString($str) {
	return str_replace("'", "''", $str);
}


// Returns the last inserted id for the current db connection
function mssql_insert_id() {
	$sql = "SELECT SCOPE_IDENTITY() as idno;";
	$dbResult = mssql_query($sql);
	$res = mssql_fetch_array($dbResult);
	return $res['idno'];
}




$sql = "SELECT CompanyID, Name FROM Company where Companyid = $company_id";
$companies = getRecord($sql);


foreach ($companies as $company) {

	$companyid = $company['CompanyID'];

	writeToLog('Company: '. $company['Name'].'('.$companyid .')');


	$columns = array( array('FieldName' => 'Text01'), array('FieldName' => 'Text02'), array('FieldName' => 'Text03'), array('FieldName' => 'Text04'), array('FieldName' => 'Text05'));

	// Find the company column in AccountMap table, if not found create it.
	$accountmap = getRecord("SELECT FieldName FROM AccountMap WHERE IsCompanyName = 1 AND CompanyID = $companyid");
	if (count($accountmap) > 0) {
		$account_fields = implode(',', $accountmap[0]);
		writeToLog('AccountMap company name column exists: '.$account_fields);

	} else {

		$accountmap_columns = getRecord("SELECT FieldName FROM AccountMap WHERE CompanyID = $companyid");
		$available_columns = array_values(array_diff_assoc( $columns, $accountmap_columns));

		if (count($available_columns) > 0) {
			$account_fields = $available_columns[0]['FieldName'];
			mssql_query("INSERT INTO AccountMap (CompanyID, FieldName, LabelName, IsCompanyName, IsRequired) Values($companyid, '$account_fields', 'Company Name', 1, 1)");
			writeToLog('AccountMap Company name column created: '.$account_fields);

		} else {
			writeToLog('AccountMap column is not available for company name.');
			die('AccountMap column is not available for company name.');
		}
	}

	// Find the First Name column in ContactMap table, if not found create it
	$contactmap = getRecord("SELECT FieldName FROM ContactMap WHERE IsFirstName = 1 AND CompanyID = $companyid");
	if (count($contactmap) > 0) {
		$contact_fname = implode(',', $contactmap[0]);
		writeToLog('ContactMap first name Column exists: '.$contact_fname);
	} else {
		$contactmap_columns = getRecord("SELECT FieldName FROM ContactMap WHERE CompanyID = $companyid");
		$available_columns = array_values(array_diff_assoc( $columns, $contactmap_columns));

		if (count($available_columns) > 0) {
			$contact_fname = $available_columns[0]['FieldName'];
			mssql_query("INSERT INTO ContactMap (CompanyID, FieldName, LabelName, IsFirstName) Values($companyid, '$contact_fname', 'First Name', 1)");
			writeToLog('ContactMap first name column created: '.$contact_fname);
		} else {
			writeToLog('ContactMap column is not available for first name.');
			die('ContactMap Column is not available for first name.');
		}
	}

	// Find the Last Name column in ContactMap table, if not found create it
	$contactmap = getRecord("SELECT FieldName FROM ContactMap WHERE IsLastName = 1 AND CompanyID = $companyid");
	if (count($contactmap) > 0) {
		$contact_lname = implode(',', $contactmap[0]);
		writeToLog('ContactMap last name column exists: '.$contact_lname);
	} else {
		$contactmap_columns = getRecord("SELECT FieldName FROM ContactMap WHERE CompanyID = $companyid");
		$available_columns = array_values(array_diff_assoc( $columns, $contactmap_columns));

		if (count($available_columns) > 0) {
			$contact_lname = $available_columns[0]['FieldName'];
			mssql_query("INSERT INTO ContactMap (CompanyID, FieldName, LabelName, IsLastName) Values($companyid, '$contact_lname', 'Last Name', 1)");
			writeToLog('ContactMap last name column created: '.$contact_lname);
		} else {
			writeToLog('ContactMap column is not available for last name.');
			die('ContactMap Column is not available for last name.');
		}
	}

	writeToLog('Converting the opportunities ');

	/* Get the list of table names from the existing db. */
	$sql = "SELECT DealID, Company, Contact, AccountID, ContactID, PersonID, FirstMeeting, FirstMeetingTime, NextMeeting, NextMeetingTime, Category FROM opportunities WHERE CompanyID = $companyid AND AccountID IS NULL AND Company IS NOT NULL
	and (actualclosedate = null or actualclosedate > '$date_limit')";
	//$opps = getRecord($sql);
	$opps = mssql_query($sql);

	// For each opportunity, syncronise Company and Contact with the Account and Contact table
	while($opp = mssql_fetch_assoc($opps)) {
		//foreach ($opps as $opp) {
		$personid = $opp['PersonID'];
		$company = escapeString($opp['Company']);
		$fname = $lname = '';
		@list($fname, $lname) = explode(' ', $opp['Contact'], 2);
		$dealid = $opp['DealID'];
		$accountid = $contactid = 0;
		$fname = escapeString($fname);
		$lname = escapeString($lname);
		$category = $opp['Category'];

		// account and opp
		if(!empty($company)) {
			$accountmap_records = getRecord("SELECT AccountID, $account_fields FROM Account WHERE $account_fields = '$company' AND CompanyID = $companyid");
			if (count($accountmap_records) > 0) {
				$accountid = $accountmap_records[0]['AccountID'];
				$result = mssql_query("UPDATE opportunities SET AccountID = $accountid WHERE DealID = $dealid");
			}
			else {
				$result = mssql_query("INSERT INTO Account(CompanyID, $account_fields) VALUES($companyid, '$company')");
				$accountid = mssql_insert_id();
				$result = mssql_query("UPDATE opportunities SET AccountID = $accountid WHERE DealID = $dealid");
				$result = mssql_query("INSERT INTO PeopleAccount(AccountID, PersonID, CreatedBy, AssignedTo) VALUES('$accountid', $personid, 1, 1)");

			}

			// contact and opp
			if($accountid > 0)	{
				if(empty($fname) && empty($lname) && $category >= 1 && $category <= 6) {
					$fname =  'System Generated';
					$lname = '';
				}

				if(!empty($fname) || !empty($lname)) {
					$contactmap_records = getRecord("SELECT ContactID FROM Contact WHERE $contact_fname = '$fname' AND $contact_lname = '$lname' AND AccountID = $accountid");
					if (count($contactmap_records) > 0) {
						$contactid = $contactmap_records[0]['ContactID'];
						//$result = mssql_query("UPDATE opportunities SET ContactID = $contactid WHERE DealID = $dealid");
					} else {
						$result = mssql_query("INSERT INTO Contact(AccountID, $contact_fname, $contact_lname, CompanyID) VALUES($accountid, '$fname', '$lname', $companyid)");
						$contactid = mssql_insert_id();
						$result = mssql_query("INSERT INTO PeopleContact(ContactID, PersonID, CreatedBy, AssignedTo) VALUES('$contactid', $personid, 1, 1)");
					}
				}
				// Create Event for first meeting and next meeting
				// Maybe it is easier to say this - make FM open for all FM, NM open for all IP and DP, and all other FM and NM closed.

					
				if($contactid > 0) {
					$nm_event_id = $fm_event_id = $eventtype_id = 0;
						
					// Get the event type id

					$eventtype_records = getRecord("SELECT TOP 1 EventTypeID FROM EventType WHERE companyid = $companyid AND Type = 'EVENT' AND Deleted != 1");
					if (count($eventtype_records) > 0) {
						$eventtype_id = $eventtype_records[0]['EventTypeID'];
					}
					// Create FM
					if($category == 1) {  // In FM
						$first_meeting = date('Y-m-d', strtotime($opp['FirstMeeting']));
						$first_meeting .= $opp['FirstMeetingTime'];

						$start_date_utc = ToUtc($first_meeting);
						$end_date_utc = ToUtc($first_meeting, 1);

						// $start_date = date('Y-m-d H:i:s', strtotime($first_meeting));
						if(!empty($opp['FirstMeeting'])) {

								
							$result = mssql_query("INSERT INTO Event values($contactid, $companyid, $eventtype_id, $dealid, 'Opportunity Event', '', NULL, NULL, 0, 0, GetDate(), 0, 0, NULL, NULL, NULL, NULL, GetDate(), '$start_date_utc', '$end_date_utc', NULL)");
								
							//							$result = mssql_query("INSERT INTO Events values('$contactid', '$personid', $eventtype_id, 'Opportunity Event', '$start_date', NULL, 1, 0, '$start_date', 0, '$dealid', 0, 0, NULL)");
							$fm_event_id =  mssql_insert_id();
							$attendee = mssql_query ("INSERT INTO EventAttendee values ( {$fm_event_id}, {$personid}, 1 , 0 ) ") ;
						}
					}
					else if($category == 2 || $category == 4) { // IP or DP
						$first_meeting = date('Y-m-d', strtotime($opp['FirstMeeting']));
						$first_meeting .= $opp['FirstMeetingTime'];

						$start_date_utc = ToUtc($first_meeting);
						$end_date_utc = ToUtc($first_meeting, 1);

						// $start_date = date('Y-m-d H:i:s', strtotime($first_meeting));

						
						if(!empty($opp['FirstMeeting'])) {

							$result = mssql_query("INSERT INTO Event values($contactid, $companyid, $eventtype_id, $dealid, 'Opportunity Event', '', NULL, NULL, 0, 0, GetDate(), 0, 0, NULL, NULL, NULL, NULL, GetDate(), '$start_date_utc', '$end_date_utc', NULL)");
								
							// $result = mssql_query("INSERT INTO Events values('$contactid', '$personid', $eventtype_id, 'Opportunity Event', '$start_date', NULL, 1, 0, '$start_date', 0, '$dealid', 0, 1, NULL)");
							$fm_event_id =  mssql_insert_id();
							$attendee = mssql_query("INSERT INTO EventAttendee values ( {$fm_event_id}, {$personid}, 1 , 0 ) ");
						}

						$nm_meeting = date('Y-m-d', strtotime($opp['NextMeeting']));
						$nm_meeting .= $opp['NextMeetingTime'];

						$start_date_utc = ToUtc($nm_meeting);
						$end_date_utc = ToUtc($nm_meeting, 1);

						// $nm_start_date = date('Y-m-d H:i:s', strtotime($nm_meeting));

						if(!empty($opp['NextMeeting'])) {
							$result = mssql_query("INSERT INTO Event values($contactid, $companyid, $eventtype_id, $dealid, 'Opportunity Event', '', NULL, NULL, 0, 0, GetDate(), 0, 0, NULL, NULL, NULL, NULL, GetDate(), '$start_date_utc', '$end_date_utc', NULL)");
							// $result = mssql_query("INSERT INTO Events values('$contactid', '$personid', $eventtype_id, 'Opportunity Event', '$nm_start_date', NULL, 1, 0, '$nm_start_date', 0, '$dealid', 0, 0, NULL)");
							$nm_event_id =  mssql_insert_id();
							$attendee = mssql_query ("INSERT INTO EventAttendee values ( {$nm_event_id}, {$personid}, 1 , 0 ) ");
						}

					} else { // IPS, DPS, C
						//$today_date = date('Y-m-d H:i:s', strtotime('now'));
						$first_meeting = date('Y-m-d', strtotime($opp['FirstMeeting']));
						$first_meeting .= $opp['FirstMeetingTime'];

						$start_date_utc = ToUtc($first_meeting);
						$end_date_utc = ToUtc($first_meeting, 1);

						//$start_date = date('Y-m-d H:i:s', strtotime($first_meeting));

						if(!empty($opp['FirstMeeting'])) {
							$result = mssql_query("INSERT INTO Event values($contactid, $companyid, $eventtype_id, $dealid, 'Opportunity Event', '', NULL, NULL, 0, 0, GetDate(), 0, 0, NULL, NULL, NULL, NULL, GetDate(), '$start_date_utc', '$end_date_utc', NULL)");
							// $result = mssql_query("INSERT INTO Events values('$contactid', '$personid', $eventtype_id, 'Opportunity Event', '$start_date', NULL, 1, 0, '$start_date', 0, '$dealid', 0, 1, NULL)");
							$fm_event_id =  mssql_insert_id();
							$attendee = mssql_query ("INSERT INTO EventAttendee values ( {$fm_event_id}, {$personid}, 1 , 0 ) ");
								
						}

						$nm_meeting = date('Y-m-d', strtotime($opp['NextMeeting']));
						$nm_meeting .= $opp['NextMeetingTime'];

						$start_date_utc = ToUtc($nm_meeting);
						$end_date_utc = ToUtc($nm_meeting, 1);

						//$nm_start_date = date('Y-m-d H:i:s', strtotime($nm_meeting));

						if(!empty($opp['NextMeeting'])) {
							$result = mssql_query("INSERT INTO Event values($contactid, $companyid, $eventtype_id, $dealid, 'Opportunity Event', '', NULL, NULL, 0, 0, GetDate(), 0, 0, NULL, NULL, NULL, NULL, GetDate(), '$start_date_utc', '$end_date_utc', NULL)");
							// $result = mssql_query("INSERT INTO Events values('$contactid', '$personid', $eventtype_id, 'Opportunity Event', '$nm_start_date', NULL, 1, 0, '$nm_start_date', 0, '$dealid', 0, 1, NULL)");
							$nm_event_id =  mssql_insert_id();
							$attendee = mssql_query ("INSERT INTO EventAttendee values ( {$nm_event_id}, {$personid}, 1 , 0 ) ");
								
						}
					}


					$result = mssql_query("UPDATE opportunities SET ContactID = $contactid, FMeventID = $fm_event_id, NMEventID = $nm_event_id WHERE DealID = $dealid");
				}
			}
		}
	}

	writeToLog('Completed.');

}

function ToUtc($date_string, $interval = 0) {
	$utc_tz = new DateTimeZone('UTC');
	$opp_tz = new DateTimeZone('America/Chicago');

	$opp_date = new DateTime($date_string, $opp_tz);
	$opp_date->setTimezone($utc_tz);

	if ($interval > 0) {
		$opp_date->modify("+$interval hour");
	}

	return $opp_date->format('Y-m-d G:i:s.000');
}
