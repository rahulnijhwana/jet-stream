<? 

require '../include/class.DbConnManager.php';
require '../include/class.SqlBuilder.php';

$companyid = isset($_GET['companyid']) ? $_GET['companyid']: ''; 
$personid = isset($_GET['personid']) ? $_GET['personid'] : '' ; 


if (empty($companyid) ){
	echo "ERROR : Company ID is missing.....<br>"; 
	if (empty($personid)) echo "ERROR : Person ID is missing......."; 
	exit ; 
}  

if (isset($_GET['action']) and $_GET['action'] == 'contact'){
	
	echo "Running Jetstream Contact doubles fix.........................<br>";
	echo "Comparing FirstName, LastName, Primary Email, Address, Main Company Phone, Accountid, Address ";
	
	if ( $personid == 0 ){
		$sql = "SELECT text01, text02, text06, text09, text13, text12, text15, accountid, count(contactid)as count FROM contact WHERE companyid = ? AND contactid not in ( select contactid from peoplecontact ) AND deleted = 0 
group by text01, text02, text15, Accountid, text09, text13, text12, text06";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyid));

	}
	else {
		$sql = "SELECT text01, text02, text06, text09, text13, text12, text15, accountid, count(contactid)as count FROM contact WHERE companyid = ? AND contactid in ( select contactid from peoplecontact where personid = ? ) AND deleted = 0 
group by text01, text02, text15, Accountid, text09, text13, text12, text06";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyid), array(DTYPE_INT, $personid));

	}	

	$doubs = DbConnManager::GetDb('mpower')->Exec($sql);
	
	echo "<br> [FirstName] \t [LastNmae] \t :: \t [Count] <br>";
	
	$v = 1 ; 
	
	foreach ( $doubs as $doub ){
		if ($doub['count'] > 1 ){
			echo "{$doub['text01']} \t {$doub['text02']} \t :: \t {$doub['count']} <br>";
			$contacts = array () ; 
			$values = array () ; 
			$values['text01'] = $doub['text01'];
			$values['text02'] = $doub['text02']; 
			$values['text06'] = $doub['text06']; 
			$values['text09'] = $doub['text09'];
			$values['text13'] = $doub['text13'];
			$values['text12'] = $doub['text12'];
			$values['text15'] = $doub['text15'];
			$values['accountid'] = $doub['accountid'] ; 
			$contacts = GetIDs ( $values, 'Contact') ; 
			$primary = array_shift($contacts); 
			mergeContacts( $primary, $contacts ); 
			$v++ ;
		}
		if ($v > 10) break ; 
	}
	
	echo "<br><br> There are $v # contacts";
} 

else if (isset($_GET['action']) and $_GET['action'] == 'event'){

	echo "Running Jetstream Event doubles fix.........................<br>";

	$event = 1335 ; 
	
	$sql = "select Subject, startdateUTC, count(eventid) as count  from event  where companyid = ? and eventtypeid = $event and eventid in ( select eventid from eventattendee where Personid = ?  ) and cancelled = 0 
Group by subject, startdateUTC";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $companyid), array(DTYPE_INT, $personid));
	echo $sql ; 
	$doubs = DbConnManager::GetDb('mpower')->Exec($sql);
	
	echo "<br> [Subject] \t [Start UTC Date] \t :: \t [Count] <br>";
	
	$v = 1 ; 
	$officelive = array () ; 
	foreach ( $doubs as $doub ){
		if ($doub['count'] > 1 ){
			echo "{$doub['Subject']} \t {$doub['startdateUTC']} \t :: \t {$doub['count']} <br>";
			$contacts = array () ; 
			$values = array () ; 
//			if ( )  $officelive[] = 
			$values['Subject'] = $doub['Subject'];
			$values['startdateUTC'] = $doub['startdateUTC']; 
			$eventids = GetIDs ( $values, 'Event') ; 
			$primary = array_shift($eventids); 
			CancelEvents( $eventids ); 
			DeleteSync( $eventids );
			$v++ ;
		}
//		if ($v > 10 ) break ; 
	}
	echo "<br><br> There are $v # duplicate events";  
	
	echo "<br><br> Start fixing the Office Calendar ..................";

} 	

else if (isset($_GET['action']) and $_GET['action'] == 'task'){

} 	
	
function GetIDs( $params , $type){
	if ($_GET['personid'] !=0 ){
		$personsql = "contactID in (select contactid from peoplecontact where personid = {$_GET['personid']} )";
	}
	else {
		$personsql = "contactid not in ( select contactid from peoplecontact )";
	}

	$var = array () ; 
	if ($type == 'Contact')  
		$sql = "SELECT contactid FROM contact WHERE companyid = {$_GET['companyid']} AND $personsql AND deleted = 0 "; 
	else if ( $type == 'Event' )  
		$sql = "SELECT eventid FROM event WHERE companyid = {$_GET['companyid']} and eventtypeid = 1335 and eventid in ( select eventid from eventattendee where Personid = {$_GET['personid']}  ) " ; 
	else if ( $type == 'Task' )  
		$sql = "SELECT eventid FROM event WHERE companyid = {$_GET['companyid']} and eventtypeid = 1336 and eventid in ( select eventid from eventattendee where Personid = {$_GET['personid']}  ) " ; 
		
	foreach($params as $field => $value){
		if ($value == ''){
			$sql .= " AND $field is null"; 
		}
		else{
			$sql .= " AND $field = ? "; 
			$var[] = $value ; 
		}
	}
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $var) );
	echo $sql ; 
	$results = DbConnManager::GetDb('mpower')->Exec($sql);

	$list = array () ; 
	foreach ( $results as $result ){
		$list[] = ($type == 'Contact') ? $result['contactid'] : $result['eventid']; 
	}
	return $list ; 
	
}


function CancelEvents($list ){

	$sql = "update event set Cancelled = 1 where EventID in ( ". ArrayQm($list) ." )";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql( array (DTYPE_INT, $list ));
	DbConnManager::GetDb('mpower')->Execute($sql);

} 

function DeleteSync($list ){
	
	$sql = "update syncmlmapevent set Deleted = 1 where EventID in ( ". ArrayQm($list) ." )";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql( array (DTYPE_INT, $list ));
	DbConnManager::GetDb('mpower')->Execute($sql);

} 

function mergeContacts ( $primary_contact_id , $list ){

	$sql = "update opportunities set ContactID = ? where ContactID in ( ". ArrayQm($list) ." )";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array (DTYPE_INT, $primary_contact_id), array (DTYPE_INT, $list ));
	DbConnManager::GetDb('mpower')->Execute($sql);
	echo $sql . '<br>'; 
	
	$sql1 = "update Event set ContactID = ? where ContactID in ( ". ArrayQm($list) ." )";
	$sql1 = SqlBuilder()->LoadSql($sql1)->BuildSql(array (DTYPE_INT, $primary_contact_id), array (DTYPE_INT, $list));
	DbConnManager::GetDb('mpower')->Execute($sql1);
	echo $sql1 . '<br>'; 
	
	$sql2 = "update Notes set ObjectReferer = ? where ObjectType = ? and ObjectReferer in ( ". ArrayQm($list) ." ) ";
	$sql2 = SqlBuilder()->LoadSql($sql2)->BuildSql(array (DTYPE_INT, $primary_contact_id), array (DTYPE_INT, NOTETYPE_CONTACT), array (DTYPE_INT, $list));
	DbConnManager::GetDb('mpower')->Execute($sql2);
	echo $sql2 . '<br>'; 
	
	$sql3 = "update Contact set Deleted = 1 where ContactID in (". ArrayQm($list) .")";
	$sql3 = SqlBuilder()->LoadSql($sql3)->BuildSql(array (DTYPE_INT, $list));
	DbConnManager::GetDb('mpower')->Execute($sql3);
	echo $sql3 . '<br>'; 
	
} 