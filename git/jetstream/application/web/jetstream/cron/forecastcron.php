<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';

$today = (date('Y-m-d', strtotime('now')));

$debug = true;

// Foreach Company	
foreach (getCompanyList() as $company) {

	$debug_message = '';
	$debug_message .= "\n\n";
	$debug_message .= 'Company : '.$company->Name . "\n";
	$debug_message .= 'CompanyID : '.$company->CompanyID . "\n";
	$debug_message .= 'FMForecast : '.$company->FMForecast. "\n";	
	$debug_message .= 'Date : '.$today. "\n\n";	

	/* TODO
	 * return is not working for methods so place raw code, besso
	 */
	//$peopleList = getPersonList($company->CompanyID);
	$sql = "SELECT * FROM People WHERE CompanyID = ? AND IsSalesperson = 1";
	$params = array();
	$params[] = array(DTYPE_INT, $company->CompanyID);
	$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);	
	$oDB = DbConnManager::GetDb('mpower');
	$result = $oDB->CliExec($sql);

	if (!$result){
		error_log("Debug SQL: " . str_replace("\r", ' ', str_replace(array("\t", "\n"), '', $sql)));
		error_log("Back Trace SQL: ". print_r(debug_backtrace(), TRUE)."<br>");
	}

	$peopleList = array();
	while ($row = mssql_fetch_assoc($result)) {
		$oList = new stdClass(); 
		foreach( $row as $k => $v ){
			$oList->$k = $v;
		}
		$peopleList[] = $oList;
	}

	foreach ($peopleList as $people) {
	
		$forecast = $company->FMForecast;
		$debug_message .= 'People : ' . $people->FirstName . ' ' . $people->LastName  . "\n";
		$debug_message .= 'PeopleID : ' . $people->PersonID . "\n\n";	
		
		/** 
		 * Sales Cycle
		 *
		 * Salesperson's Overall Average Sales Cycle (in days)  
		 * This value should be calculated nightly for every salesperson and held in a unique field. 
		 */		
		$sales_cycle_sql = "SELECT sum(SalesCycleLength)/count(*) avg_sales_cycle FROM salescycles WHERE PersonID = $people->PersonID";		
		$sales_cycle_rs = DbConnManager::GetDb('mpower')->getOne($sales_cycle_sql);
		$avg_sales_cycle = round($sales_cycle_rs->avg_sales_cycle);
		//echo $sales_cycle_sql.'<br>';
		
		$debug_message .= 'Calculate Sales Cycle :'."\n";
		$debug_message .= 'Sales Cycle Sql Query : ' . $sales_cycle_sql . "\n";
		$debug_message .= 'Sales Cycle value : ' . $avg_sales_cycle . "\n\n";


		/**
		 * So, since the start_date = the greater of 12 months (372) || 4 x sales cycle (124 days), start_date = 372 days ago
		 * First meetings are the sum of meetings that occurred from 372 days ago to 31 days ago.
		 * Closes are the sum of closes that occurred from 341 days ago to today. Ratio should be closes / first meetings
		 */
		// Calculate date ranges
		$forecast_days = $forecast * $avg_sales_cycle;		
		if ($forecast_days < 372) {
			$forecast_days = 372;
			$forecast = 12;

		}
		$fm_span = $forecast - $avg_sales_cycle / 30;
		
		// Calculate the first meeting date range		
		$fm_start_day = (date('Y-m-d', strtotime("-$forecast_days days")));		
		$fm_close_day = (date('Y-m-d', strtotime("-$avg_sales_cycle days")));
		
		// Calculate the closing ratio date range		
		$temp_date_closing_ratio = $forecast_days - $avg_sales_cycle;		
		$closing_ratio_start_day = (date('Y-m-d', strtotime("-$temp_date_closing_ratio days")));
		$closing_ratio_close_day = $today;
		
		$start_year = date('Y', strtotime("-$temp_date_closing_ratio days"));
		$current_year = date('Y');
		
		$year_to_check = ($current_year - $start_year) > 0 ? $start_year : 0;


		/**
		 * First Meetings
		 * 
		 * There is some complexity on how to calculate that closing ratio.
		 * Basically, we need to offset the months you are looking at based on the length of the average sales cycle.
		 * Let me give you an example.
		 * If we count the number of first meetings in Jan to March, with a one month sales cycle, we would need to divide that by the number of closes in February to April.		
		 * It could get troublesome with a long sales cycle, though.  Think if their sales cycle was 18 months.  
		 * If you were measuring 6 months of data, working back from April, 2008, you would count closes in Nov 2007 - April 2008  
		 * and first meetings from April 2006 - Oct 2006
		 * Commented out AND FM_took_place = 1
		 */
		$fm_sql = "SELECT COUNT(*) AS FM_current FROM opportunities 
					WHERE PersonID = $people->PersonID AND ((Category IN(1,2,3,4,5,6)) OR (Category = 9)) AND 
						  FirstMeeting >= ? AND FirstMeeting <= ?";
		$fm_sql = SqlBuilder()->LoadSql($fm_sql)->BuildSql(array(DTYPE_TIME, $fm_start_day), array(DTYPE_TIME, $fm_close_day));
		$fm_rs = DbConnManager::GetDb('mpower')->getOne($fm_sql);
		$FM_current = ($fm_span == 0) ? 0 : round($fm_rs->FM_current / $fm_span);
		$FM_total = $fm_rs->FM_current;
		//echo $fm_sql.'<br>';
		
		$debug_message .= 'Calculate First Meetings :'."\n\n";
		$debug_message .= 'First Meetings Sql Query : ' . $fm_sql . "\n\n";
		$debug_message .= 'First Meetings value : ' . $FM_current . "\n\n";

		/**
		 * Closing Ratio
		 *
		 * Salesperson's Overall Closing Ratio - From First Meetings held to Closed. 
		 * This value should be calculated nightly for every salesperson and held in a unique field.
		 *		
		 * Average Sale
		 *
		 * The dollar average of the Closed transactions utilized to calculate closing ratio.  
		 * This value should be calculated nightly for every salesperson and held in a unique field.
		 *
		 * Sales Per Month
		 *
		 * The average number of sales per month of the same period as the closing ratio.  
		 * This value should be calculated nightly for every salesperson and held in a unique field. 
		 */
		$closing_ratio_sql = "SELECT count(*) as closing_ratio, sum(ActualDollarAmount) as total_revenue
								FROM opportunities 			
								WHERE PersonID = $people->PersonID AND Category = 6 AND ActualCloseDate >= ? AND ActualCloseDate <= ? AND 
									FirstMeeting >= ? AND FirstMeeting <= ?";	
		$closing_ratio_sql = SqlBuilder()->LoadSql($closing_ratio_sql)->BuildSql(array(DTYPE_TIME, $closing_ratio_start_day), array(DTYPE_TIME, $closing_ratio_close_day), array(DTYPE_TIME, $fm_start_day), array(DTYPE_TIME, $fm_close_day));
	
		$closing_ratio_rs = DbConnManager::GetDb('mpower')->getOne($closing_ratio_sql);
		$closing_ratio = ($FM_total == 0) ? 0 : round($closing_ratio_rs->closing_ratio / $FM_total, 2);
		$total_revenue = $closing_ratio_rs->total_revenue;
		$total_sales =  $closing_ratio_rs->closing_ratio;
		
		$debug_message .= 'Calculate Closing Ratio :'."\n\n";
		$debug_message .= 'Closing Ratio Sql Query : ' . $closing_ratio_sql . "\n";
		$debug_message .= 'Closing Ratio value : ' . $closing_ratio . "\n";
		$debug_message .= 'Total Revenue without unreported revenue : ' . $total_revenue . "\n";
		$debug_message .= 'Total Sales without unreported sales : ' . $total_sales . "\n\n";	
		

		/** 
		 * Calculate total upreported sales and revenue
		 */		
	 	if (!empty($year_to_check)) {
		
			$unreported_sql = "SELECT sum(Sales) Sales, sum(Revenue) Revenue FROM UnreportedSales WHERE Year >= $year_to_check AND YEAR < $current_year AND PersonID = $people->PersonID";
			$unreported_rs = DbConnManager::GetDb('mpower')->getOne($unreported_sql);
			$unreported_sales_per_month = ($fm_span == 0) ? 0 : round($unreported_rs->Sales / $fm_span);
			$unreported_avg_sale = ($unreported_rs->Sales == 0) ? 0 : round($unreported_rs->Revenue / $unreported_rs->Sales);
			
			//$total_sales += $unreported_sales;
			//$total_revenue += $unreported_revenue;
			
			$debug_message .= 'Calculate unreported sales and revenue :'."\n\n";
			$debug_message .= 'Unreported Sql Query : ' . $unreported_sql . "\n";
			$debug_message .= 'Total Revenue + unreported revenue : ' . $total_revenue . "\n";
			$debug_message .= 'Total Sales + unreported sales : ' . $total_sales . "\n";
		}
		
		
		/**
		 * Update the people table with calculated forecast values
		 */
		$update_sql = "UPDATE People SET FirstMeeting = ?, AverageSalesCycle = ?, ClosingRatio = ?, 
								TotalRevenues = ?, TotalSales = ?, UnreportedAvgSale = ?, UnreportedSalesPerMonth = ?
						WHERE PersonID = $people->PersonID";
		$update_sql = SqlBuilder()->LoadSql($update_sql)->BuildSql(array(DTYPE_INT, $FM_current), array(DTYPE_INT, $avg_sales_cycle), 
									array(DTYPE_FLOAT, $closing_ratio), array(DTYPE_INT, $total_revenue), array(DTYPE_INT, $total_sales), 
									array(DTYPE_INT, $unreported_avg_sale), array(DTYPE_INT, $unreported_sales_per_month));		
		$sales_rs = DbConnManager::GetDb('mpower')->Exec($update_sql);
		//echo $update_sql.'<br>';		
		
		$debug_message .= 'Update Person Data :'."\n\n";
		$debug_message .= $update_sql . "\n";
		$debug_message .= '-----------------------------'. "\n";		
	}
	$debug_message .= '================================'. "\n";
	
	if ($debug) {
		echo str_replace("\n", "<br>", $debug_message);
		file_put_contents('forecast.log', $debug_message, FILE_APPEND);
	}
}

function getCompanyList() {
	$sql = "SELECT * FROM company where disabled = 0";	
	$company_rs = DbConnManager::GetDb('mpower')->Execute($sql);		
	$company_list = array();
	foreach($company_rs as $company) {
		$company_list[] = $company;
	}
	
	return $company_list;	
}

function getPersonList($companyid) {
	$sql = "SELECT * FROM People WHERE CompanyID = ? AND IsSalesperson = 1";
	$params[] = array(DTYPE_INT, $companyid);
	$sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);	
	$oDB = DbConnManager::GetDb('mpower');
	$result = $oDB->CliExec($sql);

	if (!$result){
		error_log("Debug SQL: " . str_replace("\r", ' ', str_replace(array("\t", "\n"), '', $sql)));
		error_log("Back Trace SQL: ". print_r(debug_backtrace(), TRUE)."<br>");
	}

	$people_rs = array();
	while ($row = mssql_fetch_assoc($result)) {
		$people_rs[] = $row;
	}

	$people_list = array();
	 
	foreach($people_rs as $people) {
		$people_list[] = $people;
	}

	return $people_list;	
}
