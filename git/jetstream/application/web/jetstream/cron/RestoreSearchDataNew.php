<?php
/**
 * Start the session.
 * define BASE PATH
 * DB API functions file include
 * @package database
 * class file include
 */
ini_set('memory_limit','32M');
ignore_user_abort(1); // run script in background
set_time_limit(0); // run script forever 
define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));
require_once BASE_PATH . '/include/mpconstants.php';
require_once BASE_PATH . '/include/class.DashboardSearchCreatorNew.php';


/**
 * create DashboardSearchCreator() class object
 * usage : php RestoreSearchDataNew.php <option> <companyid>
 */
$obj_search = new DashboardSearchCreator();

if($argc == 3) {	
	if($argv[2] > 0 ) {
		$btn_val = $argv[1];
		$obj_search->company_id = $argv[2];
	}
	else {
		$btn_val = null;
	}
}
else {
	$btn_val = NULL;
}
//file_put_contents("D:/restore.txt", "\n".'---->'.BASE_PATH.'-->'.$btn_val.'-->'.$obj_search->company_id, FILE_APPEND);

if(isset($btn_val) && ($btn_val == 'restore_all')) {
	//file_put_contents("D:/restore.txt", "\n".date('m/d/Y h:i:s').'  Starts updating all.', FILE_APPEND);
	$ret_par = $obj_search->StoreAllRecords();
	
	/* if($ret_par) {
		file_put_contents("D:/restore.txt", "\n".date('m/d/Y h:i:s').'  Completes updating all.', FILE_APPEND);
	} else {
		file_put_contents("D:/restore.txt", "\n".date('m/d/Y h:i:s').'  Error in updating all.', FILE_APPEND);
	}	*/
}
else if(isset($btn_val) && ($btn_val == 'restore_companies')) {
	//file_put_contents("D:/restore.txt", "\n".date('m/d/Y h:i:s').'  Starts updating companies.', FILE_APPEND);
	$ret_par = $obj_search->StoreAccountRecords();
	
	/* if($ret_par) {
		file_put_contents("D:/restore.txt", "\n".date('m/d/Y h:i:s').'  Completes updating companies.', FILE_APPEND);
	} else {
		file_put_contents("D:/restore.txt", "\n".date('m/d/Y h:i:s').'  Error in updating companies.', FILE_APPEND);
	}*/
}
else if(isset($btn_val) && ($btn_val == 'restore_contacts')) {
	//file_put_contents("D:/restore.txt", "\n".date('m/d/Y h:i:s').'  Starts updating contacts.', FILE_APPEND);
	$ret_par = $obj_search->StoreContactRecords();
	
	/*if($ret_par) {
		file_put_contents("D:/restore.txt", "\n".date('m/d/Y h:i:s').'  Completes updating contacts.', FILE_APPEND);
	} else {
		file_put_contents("D:/restore.txt", "\n".date('m/d/Y h:i:s').'  Error in updating contacts.', FILE_APPEND);
	}*/
}
else {
	echo "\n usage : php RestoreSearchDataNew.php <option> <companyid> \n"; 
	//file_put_contents("D:/restore.txt", "\n".date('m/d/Y h:i:s').'  Wrong value is coming for the btn_val parameter.', FILE_APPEND);
}
?>