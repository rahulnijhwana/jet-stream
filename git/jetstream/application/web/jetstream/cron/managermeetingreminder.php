<?php

define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/lib.date.php';


$today_array = getdate();
$today_timestamp = mktime(00, 00, 00, $today_array['mon'], $today_array['mday'], $today_array['year']);
$tomorrow_timestamp = mktime(23, 59, 59, $today_array['mon'], $today_array['mday'] + 1, $today_array['year']);
$nextday_timestamp = mktime(23, 59, 59, $today_array['mon'], $today_array['mday'] + 2, $today_array['year']);


$sql = "SELECT CompanyID FROM company where Disabled = 0 AND Slipstream = 1";	
	$company_rs = DbConnManager::GetDb('mpower')->Execute($sql);		
	$company_list = array();	 
	foreach($company_rs as $company) {
		$company_list[] = $company['CompanyID'];
	}
        
//for($i=0;$i<count($company_list);$i++){   
   
    $sql = "select PersonID, Email from people where deleted = 0 and personid in (select supervisorid from people where level = 1 and deleted = 0 and companyid = ?)";
    
    //$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_list[$i]));
    $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, 508));
            $people_rs = DbConnManager::GetDb('mpower')->Execute($sql);		
            $manager_list = array();
            $manager_email_list = array();
            foreach($people_rs as $people) {
                    $manager_list[]          = $people['PersonID'];
                    $manager_email_list[]    = $people['Email'];
            }
    
    
    $sql = "SELECT FieldName FROM ContactMap where IsFirstName = 1 and CompanyID = ?";
    //$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_list[$i]));
    $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, 508));
            $result = DbConnManager::GetDb('mpower')->getOne($sql);
            $firstname_fieldname = $result->FieldName;
            
    
    $sql = "SELECT FieldName FROM ContactMap where IsLastName = 1 and CompanyID = ?";
    //$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_list[$i]));
    $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, 508));
            $result = DbConnManager::GetDb('mpower')->getOne($sql);
            $lastname_fieldname = $result->FieldName;
            
    
    $sql = "SELECT * FROM AccountMap where IsCompanyName = 1 and CompanyID = ?";
    //$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_list[$i]));
    $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, 508));
            $result = DbConnManager::GetDb('mpower')->getOne($sql);
            $companyname_fieldname = $result->FieldName;
            
           
    if(count($manager_list) > 0){
        for($l=0;$l<count($manager_list);$l++){ 
            $sql = "SELECT PersonID, FirstName, LastName FROM people where Supervisorid = ? AND Deleted = 0";
            $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $manager_list[$l]));
                    $people_rs = DbConnManager::GetDb('mpower')->Execute($sql);		
                    $salesperson_list = array();
                    $salesperson_firstname = array();
                    $salesperson_lastname = array();
                    foreach($people_rs as $people) {
                            $salesperson_list[]          = $people['PersonID'];
                            $salesperson_firstname[]    = $people['FirstName'];
                            $salesperson_lastname[]    = $people['LastName'];
                    }
            
            $msgbody_today = '---------'."\n";
	    $msgbody_today.= 'Today,  '. date('l, F jS', $today_timestamp)."\n";
	    $msgbody_today.= '---------'."\n";
	    
	    $msgbody_tomorrow = '---------'."\n";
	    $msgbody_tomorrow.= 'Tomorrow,  '. date('l, F jS', $tomorrow_timestamp)."\n";
	    $msgbody_tomorrow.= '---------'."\n";
	    
	    $msgbody_dayafter_tomorrow = '---------'."\n";
	    $msgbody_dayafter_tomorrow.=  date('l, F jS', $nextday_timestamp)."\n";
	    $msgbody_dayafter_tomorrow.= '---------'."\n";
            
            if(count($salesperson_list)>0){
                for($k=0;$k<count($salesperson_list);$k++){
                    if($salesperson_list[$k] != ''&& $firstname_fieldname != '' && $lastname_fieldname != '' && $companyname_fieldname != ''){
                        $sql = "Select E.EventID, E.DurationHours, E.DurationMinutes, C.".$firstname_fieldname.", C.".$lastname_fieldname.
                            ", A.".$companyname_fieldname." AS companyName, convert(varchar, E.startdate, 108) as eventtime,
                            convert(varchar, E.startdate, 110) as eventdate
                            from events E Left Join Contact C ON E.ContactID = C.ContactID
                            LEFT JOIN account A ON C.AccountID = A.AccountID
                            LEFT JOIN EventType ET ON E.EventTypeID = ET.EventTypeID
                            WHERE USERID = ?
                            and startdate >= '".date("Y-m-d H:i:s",$today_timestamp) ."'and startdate <='".date("Y-m-d H:i:s",$nextday_timestamp)."'
                            and IsCancelled = 0 and IsClosed = 0 and ET.Type = 'EVENT'
                            order by StartDate";
                        $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $salesperson_list[$k]));
                            //echo $sql.'<br>';
                            
                    
                        $people_contact_rs = DbConnManager::GetDb('mpower')->Execute($sql);		
                        $people_contact_list = array();
                        $people_contact_fname = array();
                        $people_contact_lname = array();
                        $people_contact_companyname = array();
                        $people_event_startdate = array();
                        $people_event_starttime = array();
                        $event_id = array();
                        $event_duration_hr = array();
                        $event_duration_min = array();
                        foreach($people_contact_rs as $people_contact) {
                                $people_contact_fname[] = $people_contact[$firstname_fieldname];
                                $people_contact_lname[] = $people_contact[$lastname_fieldname];
                                $people_contact_companyname[] = $people_contact['companyName'];
                                $people_event_startdate[] = $people_contact['eventdate'];
                                $people_event_starttime[] = $people_contact['eventtime'];
                                $event_id[]=$people_contact['EventID'];
                                $event_duration_hr[] = $people_contact['DurationHours'];
                                $event_duration_min[] = $people_contact['DurationMinutes'];
                        }
                        $today_count = 0;
                        $tomorrow_count = 0;
                        $dayafter_tommorow = 0;
                        $today_name_count = 0;
                        $tomorrow_name_count = 0;
                        $dayafter_name_tommorow = 0;
                        
                        for($j=0;$j<count($event_id);$j++){
                            $event_detailarr = explode('-',$people_event_startdate[$j]);
                            $event_timearr = explode(':',$people_event_starttime[$j]);
                            
                            if($event_detailarr[1] == date('j',$today_timestamp)){
                                $today_count = $today_count+1;
                                if($today_name_count == 0){
                                    $msgbody_today.='          '.$salesperson_firstname[$k].' '.$salesperson_lastname[$k]."\n";
                                    $today_name_count = $today_name_count+1;
                                }
                                $msgbody_today.='               '.'Company:  '.$people_contact_companyname[$j]."\n";
                                $msgbody_today.='               '.'Contact:  '.$people_contact_fname[$j].' '.$people_contact_lname[$j]."\n";
                                $msgbody_today.='               '.'Time:  '.DrawTime(mktime($event_timearr[0],$event_timearr[1] , 00, $today_array['mon'], $today_array['mday'], $today_array['year']));
                                $msgbody_today.=' - '.DrawTime(mktime($event_timearr[0]+$event_duration_hr[$j],$event_timearr[1]+$event_duration_min[$j] , 00, $today_array['mon'], $today_array['mday'], $today_array['year']));
                                if($event_duration_min[$j] == 0)
                                $msgbody_today.=' ('.$event_duration_hr[$j].' hrs)'."\n\n";
                                else if($event_duration_hr[$j] == 0)
                                $msgbody_today.=' ('.$event_duration_min[$j].' mins)'."\n\n";
                                else
                                $msgbody_today.=' ('.$event_duration_hr[$j].' hrs '.$event_duration_min[$j].' mins)'."\n\n";
                            }
                            
                            
                            if($event_detailarr[1] == date('j',$tomorrow_timestamp)){
                                $tomorrow_count = $tomorrow_count+1;
                                if($tomorrow_name_count == 0){
                                    $msgbody_tomorrow.='          '.$salesperson_firstname[$k].' '.$salesperson_lastname[$k]."\n";
                                    $tomorrow_name_count = $tomorrow_name_count+1;
                                }
                                $msgbody_tomorrow.='               '.'Company:  '.$people_contact_companyname[$j]."\n";
                                $msgbody_tomorrow.='               '.'Contact:  '.$people_contact_fname[$j].' '.$people_contact_lname[$j]."\n";
                                
                                $msgbody_tomorrow.='               '.'Time:  '.DrawTime(mktime($event_timearr[0],$event_timearr[1] , 00, $today_array['mon'], $today_array['mday']+1, $today_array['year']));
                                $msgbody_tomorrow.=' - '.DrawTime(mktime($event_timearr[0]+$event_duration_hr[$j],$event_timearr[1]+$event_duration_min[$j] , 00, $today_array['mon'], $today_array['mday']+1, $today_array['year']));
                                if($event_duration_min[$j] == 0)
                                $msgbody_tomorrow.=' ('.$event_duration_hr[$j].' hrs)'."\n\n";
                                else if($event_duration_hr[$j] == 0)
                                $msgbody_tomorrow.=' ('.$event_duration_min[$j].' mins)'."\n\n";
                                else
                                $msgbody_tomorrow.=' ('.$event_duration_hr[$j].' hrs '.$event_duration_min[$j].' mins)'."\n\n";
                        
                            }
                            
                            
                            if($event_detailarr[1] == date('j',$nextday_timestamp)){
                                $dayafter_tommorow = $dayafter_tommorow+1;
                                if($dayafter_name_tommorow == 0){
                                    $msgbody_dayafter_tomorrow.='          '.$salesperson_firstname[$k].' '.$salesperson_lastname[$k]."\n";
                                    $dayafter_name_tommorow = $dayafter_name_tommorow+1;
                                }
                                $msgbody_dayafter_tomorrow.='               '.'Company:  '.$people_contact_companyname[$j]."\n";
                                $msgbody_dayafter_tomorrow.='               '.'Contact:  '.$people_contact_fname[$j].' '.$people_contact_lname[$j]."\n";
                                $msgbody_dayafter_tomorrow.='               '.'Time:  '.DrawTime(mktime($event_timearr[0],$event_timearr[1] , 00, $today_array['mon'], $today_array['mday']+2, $today_array['year']));
                                $msgbody_dayafter_tomorrow.=' - '.DrawTime(mktime($event_timearr[0]+$event_duration_hr[$j],$event_timearr[1]+$event_duration_min[$j] , 00, $today_array['mon'], $today_array['mday']+2, $today_array['year']));
                                if($event_duration_min[$j] == 0)
                                $msgbody_dayafter_tomorrow.=' ('.$event_duration_hr[$j].' hrs)'."\n\n";
                                else if($event_duration_hr[$j] == 0)
                                $msgbody_dayafter_tomorrow.=' ('.$event_duration_min[$j].' mins)'."\n\n";
                                else
                                $msgbody_dayafter_tomorrow.=' ('.$event_duration_hr[$j].' hrs '.$event_duration_min[$j].' mins)'."\n\n";
                            }
                            
                            
                        }
                        if($today_count == 0){
                            $msgbody_today.='          '.$salesperson_firstname[$k].' '.$salesperson_lastname[$k]."\n";
                            $msgbody_today.='               '.'No Meetings Scheduled'."\n\n";
                        }
                        
                        if($tomorrow_count == 0){
                            $msgbody_tomorrow.='          '.$salesperson_firstname[$k].' '.$salesperson_lastname[$k]."\n";
                            $msgbody_tomorrow.='               '.'No Meetings Scheduled'."\n\n";
                        }
                        
                        if($dayafter_tommorow == 0){
                            $msgbody_dayafter_tomorrow.='          '.$salesperson_firstname[$k].' '.$salesperson_lastname[$k]."\n";
                            $msgbody_dayafter_tomorrow.='               '.'No Meetings Scheduled'."\n\n";
                        }
                        
                        
                        $today_name_count = 0;
                        $tomorrow_name_count = 0;
                        $dayafter_name_tommorow = 0;
                    }
                }
                $msg = $msgbody_today.$msgbody_tomorrow.$msgbody_dayafter_tomorrow;
               mail($manager_email_list[$l],
                        "Jetstream Appointment Reminder",
                        $msg, "From: jetstream@asasales.com");
                        $msg='';
            
            }
           
            
            
        }
    
    }
    
        
//}



?>