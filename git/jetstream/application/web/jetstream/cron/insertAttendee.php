<?PHP

ini_set('memory_limit','32M');
ignore_user_abort(1); // run script in background
set_time_limit(0); // run script forever
define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

require_once BASE_PATH . '/include/mpconstants.php';
require_once BASE_PATH . '/include/class.DashboardSearchCreatorNew.php';
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.DbConnMssql.php';



//$event_old = getEvent('event_old'); 
//$event_new = getEvent('event_new'); 
$map = getMap ('attendeeMap');
$eventAttendee = getEventAttendee('attendee', $map); 
echo "\n" ;

//echo print_r($event_old); 
//echo print_r($event_new); 
//echo print_r($eventAttendee); 

foreach ( $eventAttendee as $eventd){

//echo "Inserting old eventid : {$eventd['EventID']} \n" ;
$sqlcommand = "INSERT INTO EVENTATTENDEE (" ; 
$sqlcommand .=  implode(",", array_keys($eventd))  ; 
$sqlcommand .=  ") VALUES  ( ";
$sqlcommand .=  implode( ",", $eventd). ") "; 
echo $sqlcommand ; 
echo "\n"; 
//$eventAttendee = updateAttendee($eventAttendee, $eventn['eventid'] ) ; 

//echo $eventd['EventID']. " \n" ; 
//$newid[$eventd['EventID']] = checkNewID($event_old, $event_new, $eventd['EventID'] ); 

}

//print_r($newid ); 
echo "\n" ;

//echo print_r($eventAttendee ); 

function getMap($filename ){

$file_handle = fopen($filename, "rb");

$all = array() ;
$i = 0 ;

$event = array (
	   'OldID'
           ,'NewID') ; 

$temp = array () ; 

while (!feof($file_handle) ) {

$list = array () ;
$line_of_text = fgets($file_handle);
$list = explode("\t", $line_of_text)  ;

foreach ( $event as $key => $value ){
	$temp[trim($list[0])] = trim($list[1]);
}

$i++ ; 
//if ($i == 20 ) break ; 
}

fclose($file_handle);

return $temp ; 

}

function CheckNewID ($old, $new, $id ){
	$match = 0 ; 
	foreach ($old as $key => $data ){
		if ( trim($data['eventid']) == trim($id) ){
			$match = $key ; 
			break ; 	
		}
	}
	echo "\n \t $key \n";
	if ($match == 0 ) return 0 ; 
	$newid = 0 ; 
	foreach ($new as $key => $data ){
		echo " $key "; 
		if ( compare($old[$match]['data'],$data['data'])) {
			$newid = $data['EventID']; 	
		}
		echo "\n";
	}	
	return $newid ; 

}

function compare($old, $new  ){
//	if ( count(array_diff($old, $new )) ==0  )return true ; 
//	else return false  ; 
	echo " {$old['Subject']} <=> {$new["Subject"]} || {$old['ContactID']} <=> {$new["ContactID"]} || {$old['Location']} <=> {$new["Location"]} ";
//	if ( trim($old['Location']) == trim($new['Location']) &&  trim($old['Subject']) == trim($new['Subject']) && trim($old['DealID']) == trim($new['DealID']) && trim($old['ContactID']) == trim($new['ContactID']) ){
	if ( trim($old['Location']) == trim($new['Location']) &&  trim($old['Subject']) == trim($new['Subject']) && trim($old['DealID']) == trim($new['DealID']) && trim($old['ContactID']) == trim($new['ContactID']) ){
		echo " OK " ; 
		return true ; 
	}
	
	return false ; 

}

function updateAttendee($attendee, $eventid ){
	
	foreach ($attendee as $value){
		if ( $value["EventID"] == $eventid ) $value["EventID"] = $eventid ; 
		
	}

	return $attendee ; 
} 


function getEvent($filename){

$file_handle = fopen($filename, "rb");

$all = array() ;
$i = 0 ;

$event = array (
	   'ContactID'
           ,'CompanyID'
           ,'EventTypeID'
           ,'DealID'
           ,'Subject'
           ,'Location'
           ,'StartDate'
           ,'EndDate'
           ,'Cancelled'
           ,'Closed'
           ,'ModifyDate'
           ,'Private'
           ,'RepeatType'
           ,'RepeatInterval'
           ,'RepeatEndDate'
           ,'RepeatParent'
           ,'RepeatExceptions'
           ,'ModifyTime'
           ,'StartDateUTC'
           ,'EndDateUTC'
           ,'RepeatEndDateUTC' ) ; 

while (!feof($file_handle) ) {

$list = array () ;
$line_of_text = fgets($file_handle);
$list = explode("\t", $line_of_text)  ;
$list1 = array_shift($list); 
$temp = array () ; 

foreach ( $event as $key => $value ){
	if ( trim($list[$key]) != 'NULL' ) $temp[$value] = "'".trim($list[$key]) ."'";	
	else $temp[$value] = trim($list[$key]);	
}

$all[] = array ( 'data' => $temp , 'eventid' => $list1 )  ;
$i++ ; 
//if ($i == 20 ) break ; 
}

fclose($file_handle);

return $all ; 
}

function getEventAttendee($filename, $map){

$file_handle = fopen($filename, "rb");

$all = array() ;
$i = 0 ;

$event = array ('EventID'
           ,'PersonID'
           ,'Creator'
           ,'Deleted' ) ; 

while (!feof($file_handle) ) {

$list = array () ;
$line_of_text = fgets($file_handle);
$list = explode("\t", $line_of_text)  ;
$list1 = array_shift($list); 
$temp = array () ; 

foreach ( $event as $key => $value ){
	if ($value == 'EventID'){
		$temp[$value] = $map[trim($list[$key])];	
	}
	else 	$temp[$value] = trim($list[$key]);	

}

$all[] = $temp  ;
$i++ ; 
//if ($i == 3 ) break ; 
}

fclose($file_handle);

return $all ; 


} 

?>


