<?PHP
define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/slipstream/class.Email.php';
ini_set("memory_limit", "32M");

/*
// Email Content
UPDATE EC SET EC.CompanyID = P.CompanyID FROM EmailContent EC
LEFT JOIN People P ON EC.PersonID = P.PersonID
WHERE EC.CompanyID IS NULL

// People Email Address
UPDATE PE SET PE.CompanyID = P.CompanyID FROM PeopleEmailAddr PE
LEFT JOIN People P ON PE.PersonID = P.PersonID
WHERE PE.CompanyID IS NULL

*/


/**
 * loops through the rows of the emailcontent table, 
 * checks to see if emailaddress has an entry for every address in fromemail, recipientemails, and ccemails and no extra entries.  
 * If emailaddress does not have a matching entry, create it.  If there are any extra entries for an email, delete them.  
 * After creating and running this script, we may have to rerun the above query to ensure all the personids are set
 */
$sql = "SELECT * FROM EmailContent ORDER BY EmailContentID DESC";
$email_list = DbConnManager::GetDb('mpower')->GetSqlResult($sql);

if (!empty($email_list)) {	
	 while($email = mssql_fetch_assoc($email_list)) {

		//file_put_contents('email_cron.log', 'Stat : '."\n\n\n", FILE_APPEND);
		
		// get the email fields
		$from_email =  $email['FromEmail'];
		$from_name =  $email['FromName'];
		$recipient_emails =  $email['RecipientEmails'];
		$recipient_names =  $email['RecipientNames'];	
		$cc_emails =  $email['CcEmails'];
		$cc_names =  $email['CcNames'];		
		$email_content_id = $email['EmailContentID'];		
		$email_company_id = $email['CompanyID'];
		$creation_date = $email['ReceivedDate'];
		
		$email_address_list =array();
		$email_address_ids =array();
		
		$contact_email_field_list = getContactEmailFields($email_company_id);
		
		$email_address = storeEmail($email_content_id, $email_company_id, $from_email, $from_name, 'From', $contact_email_field_list);
		$email_address_list[] = $email_address;
		$email_address_ids[] = $email_address['EmailAddressID'];
		$from_person_id = $email_address['PersonID'];
		$from_contact_id = $email_address['ContactID'];
				
		// to email address			
		$to_email_addresses = Email::getEmailAddressList($recipient_emails, $recipient_names);
		if(!empty($to_email_addresses) && count($to_email_addresses) > 0) {
			foreach($to_email_addresses as $to_name => $to_email_address) {
				$email_address = storeEmail($email_content_id, $email_company_id, $to_email_address, $to_name, 'To', $contact_email_field_list);		
				$email_address_list[] = $email_address;
				$email_address_ids[] = $email_address['EmailAddressID'];				
			}
		}
		
		// cc email address
		if(!empty($cc_emails)) {		
			$cc_email_addresses = Email::getEmailAddressList($cc_emails, $cc_names);
			if(!empty($cc_email_addresses) && count($cc_email_addresses) > 0) {		
				foreach($cc_email_addresses as $cc_name => $cc_email_address) {
					$email_address = storeEmail($email_content_id, $email_company_id, $cc_email_address, $cc_name, 'Cc', $contact_email_field_list);		
					$email_address_list[] = $email_address;
					$email_address_ids[] = $email_address['EmailAddressID'];
				}
			}
		}
		
		// delete all the extra record from email address table
		if(!empty($email_address_ids) && count($email_address_ids) > 0) {
			$email_address_ids = implode(',',$email_address_ids);
			$sql = "DELETE  FROM EmailAddresses WHERE EmailContentID = ? AND EmailAddressID NOT IN ($email_address_ids)";
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(
					array(DTYPE_INT, $email_content_id)
					);			
			$result = DbConnManager::GetDb('mpower')->Exec($sql);
		}

		
		// from email address		
		/*
		1. When it is from a user:  Attach email note to all RECIPIENT contacts
			a. If all recipients are users, then make it private
		2. When it is not from a user:  Attach email note to the SENDER contact
		*/

		$note_list = array();
		
		// if sender is a salesperson then create a note for each to and cc	
		if ($from_person_id > 0) {
			if (!empty($email_address_list) and count($email_address_list) > 0) {
			
				// check if all of the recipients are salesperson.
				$is_all_salesperson = true;
				foreach ($email_address_list as $key=>$emailaddress){
					if (!($emailaddress['PersonID'] > 0)) {
						$is_all_salesperson &= false;
					}
				}

				$private_note = ($is_all_salesperson) ? 1 : 0;
				
				foreach ($email_address_list as $emailaddress){
					if ($emailaddress['ContactID'] && ($emailaddress['Type'] != 'From')) {
						$note = array();
						$note['CreatedBy'] = $from_person_id;
						$note['CompanyID'] = $email_company_id;
						$note['CreationDate'] = $creation_date;
						$note['ObjectReferer'] = $emailaddress['ContactID'];
						$note['EmailContentID'] = $email_content_id;
						$note['PrivateNote'] = $private_note;
						$note_list[] = createNote($note);
					}
				}
			}
		} else if ($from_contact_id > 0) {
			// if the sender is a known contact then save a note for that contact.
			$note = array();
			$note['CreatedBy'] = 0;
			$note['CompanyID'] = $email_company_id;
			$note['CreationDate'] = $creation_date;
			$note['ObjectReferer'] = $from_contact_id;
			$note['EmailContentID'] = $email_content_id;
			$note['PrivateNote'] = 0;
			$note_list[] = createNote($note);
		}
		
		
		// delete all the extra record from notes table
		if(!empty($note_list) && count($note_list) > 0) {
			$note_list = implode(',',$note_list);
			$sql = "DELETE FROM Notes WHERE EmailContentID = ? AND NoteID NOT IN ($note_list)";
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(
					array(DTYPE_INT, $email_content_id)
					);			
			$result = DbConnManager::GetDb('mpower')->Exec($sql);
		} else {
			// If it doesn't have any identified notes above, then delete all related
			// notes
			$sql = "DELETE FROM Notes WHERE EmailContentID = ?";
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(
					array(DTYPE_INT, $email_content_id)
					);			
			$result = DbConnManager::GetDb('mpower')->Exec($sql);
		}
		
		//file_put_contents('email_cron.log', 'End : '."\n\n\n", FILE_APPEND);
		
	}
}


// Run the script to populate the personid 
/*$sql = "update e set e.personid = p.personid from emailaddresses e
		left join emailcontent ec on e.emailcontentid = ec.emailcontentid
		left join people p on e.emailaddress = p.email and ec.companyid = 
		p.companyid and p.supervisorid != -3";
$result = DbConnManager::GetDb('mpower')->Exec($sql);
*/
/** 
 * Creates a Note
 */
function createNote($note) {
	$note_id = 0;
	// check if note exist
	$sql = "SELECT NoteID FROM Notes WHERE EmailContentID = ? AND ObjectReferer = ? AND CreatedBy = ?";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $note['EmailContentID']), array(DTYPE_STRING, $note['ObjectReferer']), array(DTYPE_INT, $note['CreatedBy']));
	$result = DbConnManager::GetDb('mpower')->Exec($sql);
	if(!empty($result) && count($result) > 0) {
		$note_id = $result[0]['NoteID'];
	} else {
		Email::createEmailNote($note);
		$identity_sql = 'select @@identity as NoteID';
		$identity = DbConnManager::GetDb('mpower')->getOne($identity_sql);
		$note_id = $identity->NoteID;		
	}		
		
	return $note_id;
}


/**
 * Stores email address in EmailAddresses table
 */
function storeEmail($email_content_id, $email_company_id, $email, $name, $type, $contact_email_field_list) {
	$email_address_id = 0;
	
	$sql = "SELECT * FROM EmailAddresses WHERE EmailContentID = ? AND EmailAddressType = ? AND EmailAddress = ?";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $email_content_id), array(DTYPE_STRING, $type), array(DTYPE_STRING, $email));
	$result = DbConnManager::GetDb('mpower')->Exec($sql);
	if(!empty($result) && count($result) > 0) {
		// add the email address id to the array
		$email_address_id = $result[0]['EmailAddressID'];	
		$contact_id = $result[0]['ContactID'];	
		$person_id = $result[0]['PersonID'];	

	} else {

		$person_id = Email::getPersonID($email_company_id, $email, $contact_email_field_list);
		$contact_id = Email::getContactID($email_company_id, $email, $contact_email_field_list);		
		
		// insert the record to email address
		$sql = "INSERT INTO EmailAddresses(EmailContentID, ContactID, EmailAddressType, EmailAddress, PersonID, DisplayName)
				VALUES (?, ?, ?, ?, ?, ?)";
		$sql = SqlBuilder()->LoadSql($sql)->BuildSql(
				array(DTYPE_INT, $email_content_id),
				array(DTYPE_STRING, $contact_id),				
				array(DTYPE_STRING, $type),				
				array(DTYPE_STRING, $email), 
				array(DTYPE_STRING, $person_id),				
				array(DTYPE_STRING, $name)
				);			
		$result = DbConnManager::GetDb('mpower')->Exec($sql);
		
		$identity_sql = 'select @@identity as EmailAddressID';
		$identity = DbConnManager::GetDb('mpower')->getOne($identity_sql);
		$email_address_id = $identity->EmailAddressID;
	}	
	
	$email_address['EmailAddressID'] = $email_address_id;
	$email_address['Email'] = $email;
	$email_address['Type'] = $type;
	$email_address['ContactID'] = $contact_id;
	$email_address['PersonID'] = $person_id;
	
	return $email_address;
}


/**
 * Get the email fields map for a company from ContactMap table
 */
function getContactEmailFields($company_id){
	$contact_email_field_list = array();

	// Get the email fields
	$sql = "SELECT FieldName FROM ContactMap WHERE CompanyID = ? AND Enable = 1 AND ValidationType = 5";
	$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_id));

	$contact_map = DbConnManager::GetDb('mpower')->Execute($sql);

	if (count($contact_map) > 0) {
		foreach ($contact_map as $key => $email_field) {
			$contact_email_field_list[] = $email_field->FieldName;
		}
	}
	return $contact_email_field_list;
}