#!/usr/bin/python

from sqlalchemy import create_engine, Column, ForeignKey #@UnresolvedImport
from sqlalchemy.orm import sessionmaker #@UnresolvedImport
from sqlalchemy.ext.declarative import declarative_base #@UnresolvedImport
from sqlalchemy.dialects.mssql import (BIT, DATETIME,#@UnresolvedImport
                                       INTEGER, SMALLINT, VARCHAR, TEXT)#@UnresolvedImport
import util #@UnresolvedImport
from constants import (VALIDATION_TYPE_EMAIL, EMAIL_TYPE_TO, #@UnresolvedImport
                       EMAIL_TYPE_FROM, EMAIL_TYPE_CC, NOTE_TYPE_CONTACT, #@UnresolvedImport
                       NOTE_TYPE_EMAIL) #@UnresolvedImport


engine = create_engine(util.getConnectionString(), echo='debug', echo_pool=True)
#engine = create_engine(util.getConnectionString(), echo=False)
Base = declarative_base(engine)

class Contact(Base):
    __tablename__ = 'Contact'

    id = Column("ContactID", INTEGER, primary_key=True)


class ImapCompanySettings(Base):
    __tablename__ = 'ImapCompanySettings'

    id = Column("ImapCompanySettingsID", INTEGER, primary_key=True)
    company_id = Column("CompanyID", INTEGER)
    enabled = Column("Enabled", BIT)

    def __init__(self, id, company_id, enabled):
        self.id = id
        self.company_id = company_id
        self.enabled = enabled

    def __repr__(self):
        return "<CompanySettings - '%d': - '%d'>" % (self.id, self.company_id)


class ImapUserSettings(Base):
    __tablename__ = 'ImapUserSettings'

    id = Column("ImapUserSettingsID", INTEGER, primary_key=True)
    server_id = Column("ImapServerID", INTEGER)
    enabled = Column("Enabled", BIT)
    person_id = Column("PersonID", INTEGER)
    username = Column("ImapUsername", VARCHAR)
    password = Column("ImapPassword", VARCHAR)
    last_sync = Column("LastSync", DATETIME)
    company_id = Column("CompanyID", INTEGER)

    def __init__(self, id, server_id, enabled, person_id, username, password, last_sync, company_id):
        self.id = id
        self.server_id = server_id
        self.enabled = enabled
        self.person_id = person_id
        self.username = username
        self.password = password
        self.last_sync = last_sync
        self.company_id = company_id

    def __repr__(self):
        return "<UserSettings - '%s': '%s' - '%s'>" % (self.id, self.username, self.last_sync)


class ImapServer(Base):
    __tablename__ = 'ImapServer'

    id = Column("ImapServerID", INTEGER, primary_key=True)
    address = Column("Address", VARCHAR)
    port = Column("Port", INTEGER)
    ssl = Column("SSL", BIT)
    private = Column("Private", BIT)

    def __init__(self, id, address, port, ssl, private):
        self.id = id
        self.address = address
        self.port = port
        self.ssl = ssl
        self.private = private


class EmailAddress(Base):
    __tablename__ = 'EmailAddresses'
    
    id = Column("EmailAddressID", INTEGER, primary_key=True)
    email_content_id = Column("EmailContentID", INTEGER)
    contact_id = Column("ContactID", INTEGER)
    email_address_type = Column("EmailAddressType", VARCHAR)
    email_address = Column("EmailAddress", VARCHAR)
    person_id = Column("PersonID", INTEGER)
    display_name = Column("DisplayName", VARCHAR)
    
    def __init__(self, email_content_id=None, contact_id=None,
                 email_address_type=None, email_address=None,
                 person_id=None, display_name=None):
        self.email_content_id = email_content_id
        self.contact_id = contact_id
        self.email_address_type = email_address_type
        self.email_address = email_address
        self.display_name = display_name


class EmailContent(Base):
    __tablename__ = 'EmailContent'

    id = Column("EmailContentID", INTEGER, primary_key=True)
    person_id = Column("PersonID", INTEGER)
    entry_id = Column("EntryID", VARCHAR)
    from_name = Column("FromName", VARCHAR)
    from_email = Column("FromEmail", VARCHAR)
    recipient_names = Column("RecipientNames", VARCHAR)
    recipient_emails = Column("RecipientEmails", VARCHAR)
    cc_names = Column("CcNames", VARCHAR)
    cc_emails = Column("CcEmails", VARCHAR)
    received_date = Column("ReceivedDate", DATETIME)
    subject = Column("Subject", VARCHAR)
    body = Column("Body", TEXT)
    company_id = Column("CompanyID", INTEGER)

    def __init__(self, person_id=None, entry_id=None, from_name=None,
                 from_email=None, recipient_names=None, recipient_emails=None,
                 cc_names=None, cc_emails=None, received_date=None,
                 subject=None, body=None, company_id=None):
        self.person_id = person_id
        self.entry_id = entry_id
        self.from_name = from_name
        self.from_email = from_email
        self.recipient_names = recipient_names
        self.recipient_emails = recipient_emails
        self.cc_names = cc_names
        self.cc_emails = cc_emails
        self.received_date = received_date
        self.subject = subject
        self.body = body
        self.company_id = company_id

    def __repr__(self):
        return "<EmailContent - '%d' - '%s'>" % (self.person_id, self.entry_id)


class EmailAttachment(Base):
    __tablename__ = 'EmailAttachments'

    id = Column("EmailAttachmentID", INTEGER, primary_key=True)
    email_content_id = Column("EmailContentID", INTEGER, ForeignKey("EmailContent.EmailContentID"), nullable=False)
    original_name = Column("OriginalName", VARCHAR)
    stored_name = Column("StoredName", VARCHAR)

    def __init__(self, original_name, email_content_id=None):
        self.original_name = original_name.replace(" ", "_")
        self.email_content_id = email_content_id

    def rename(self, entry_id):
        self.stored_name = entry_id + self.original_name


class ContactMap(Base):
    __tablename__ = 'ContactMap'
    
    id = Column("ContactMapID", INTEGER, primary_key=True)
    company_id = Column("CompanyID", INTEGER)
    field_name = Column("FieldName", VARCHAR)
    enabled = Column("Enable", BIT)
    validation_type = Column("ValidationType", INTEGER)


class People(Base):
    __tablename__ = 'People'
    
    id = Column("PersonID", INTEGER, primary_key=True)
    company_id = Column("CompanyID", INTEGER)
    email = Column("Email", VARCHAR)
    level = Column("Level", INTEGER)
    supervisor_id = Column("SupervisorID", INTEGER)
    first_name = Column("FirstName", VARCHAR)
    last_name = Column("LastName", VARCHAR)
    

class PeopleEmailAddr(Base):
    __tablename__ = 'PeopleEmailAddr'
    
    id = Column("PeopleEmailAddrID", INTEGER, primary_key=True)
    person_id = Column("PersonID", INTEGER)
    company_id = Column("CompanyID", INTEGER)
    email = Column("EmailAddr", VARCHAR)
    is_primary = Column("IsPrimaryEmailAddr", BIT)


class Note(Base):
    __tablename__ = 'Notes'

    id = Column("NoteID", INTEGER, primary_key=True)
    created_by = Column("CreatedBy", INTEGER)
    creation_date = Column("CreationDate", DATETIME)
    subject = Column("Subject", VARCHAR)
    note_text = Column("NoteText", TEXT)
    object_type = Column("ObjectType", SMALLINT)
    object_referrer = Column("ObjectReferer", INTEGER)
    email_content_id = Column("EmailContentID", INTEGER)
    email_address_type = Column("EmailAddressType", VARCHAR)
    note_special_type = Column("NoteSpecialType", INTEGER)
    private_note = Column("PrivateNote", BIT)
    company_id = Column("CompanyID", INTEGER)
    created_timezone = Column("CreatedTimezone", VARCHAR)
    
    def __init__(self, created_by=None, creation_date=None, subject=None,
                 note_text=None, object_type=None, object_referrer=None,
                 email_content_id=None, email_address_type=None,
                 note_special_type=None, private_note=None, company_id=None,
                 created_timezone=None):
        self.created_by = created_by
        self.creation_date = creation_date
        self.subject = subject
        self.note_text = note_text
        self.object_type = object_type
        self.object_referrer = object_referrer
        self.email_content_id = email_content_id
        self.email_address_type = email_address_type
        self.note_special_type = note_special_type
        self.private_note = private_note
        self.company_id = company_id
        self.created_timezone = created_timezone
        

def load_session():
    Session = sessionmaker(bind=engine)
    session = Session()
    return session


def get_companies():
    session = load_session()
    companies = session.query(ImapCompanySettings). \
        filter(ImapCompanySettings.enabled == True)
    session.close()
    return companies



def get_users(company_id):
    session = load_session()
    users = session.query(ImapUserSettings). \
        filter(ImapUserSettings.enabled == True). \
        filter(ImapUserSettings.company_id == company_id).all()
    session.close()
    return users


def get_server(server_id):
    session = load_session()
    server = session.query(ImapServer).filter(ImapServer.id == server_id).one()
    session.close()
    return server


def set_last_sync(user_settings):
    session = load_session()
    session.query(ImapUserSettings). \
        update({"LastSync" : user_settings.last_sync})
    session.commit()
    session.close()


def get_contact_email_fields(company_id):
    session = load_session()
    fields = session.query(ContactMap). \
        filter(ContactMap.company_id == company_id). \
        filter(ContactMap.enabled == True). \
        filter(ContactMap.validation_type == VALIDATION_TYPE_EMAIL).all()
    field_list = []
    for field in fields:
        field_list.append(field.field_name)
    session.close()
    return field_list


def get_person_id(company_id, email):
    session = load_session()
    person = session.query(People). \
        filter(People.company_id == company_id). \
        filter(People.email == email).first()
    if person is not None:
        session.close()
        return person.id
    people_email_addr = session.query(PeopleEmailAddr). \
        filter(PeopleEmailAddr.company_id == company_id). \
        filter(PeopleEmailAddr.email == email).first()
    if people_email_addr is not None:
        session.close()
        return people_email_addr.person_id
    return 0


def get_contact_id(company_id, email, contact_email_fields):
    if not email:
        return 0
    session = load_session()
    args = ({'company_id': company_id})
    for field in contact_email_fields:
        args[field] = email
        if contact_email_fields.index(field) == 0:
            sql = field + '=:' + field
        else:
            sql += ' OR ' + field + '=:' + field
    raw = "SELECT ContactID FROM Contact " \
        "WHERE CompanyID=:company_id AND (" + sql + ")"
    contact_id = session.query("ContactID").from_statement(raw).params(args).all()
    session.close()

    if(contact_id):
        t = contact_id[0]
        return t[0]
    else:
        return 0 


def get_email_address_list(display_names, email_addresses):
    names = display_names
    if(display_names == ' '):
        names = display_names.replace(" ", "")
    names = names.split(";")
    names = filter(None, names)
    addresses = email_addresses.replace(" ", "")
    addresses = addresses.split(";")
    addresses = filter(None, addresses)
    return dict(zip(names, addresses))


def get_email_type(email_content):
    type = ''
    from_address = get_email_address_list(email_content.from_name,
                                          email_content.from_email)
    if from_address:
        type = EMAIL_TYPE_FROM
    
    recipient_addresses = get_email_address_list(email_content.recipient_names,
                                                 email_content.recipient_emails)
    if recipient_addresses:
        type = EMAIL_TYPE_TO
    
    cc_addresses = get_email_address_list(email_content.cc_names,
                                          email_content.cc_emails)
    if cc_addresses:
        type = EMAIL_TYPE_CC

    return type


def save_contacts(email_content):
    from_address = get_email_address_list(email_content.from_name,
                                          email_content.from_email)
    if from_address:
        save_contact(email_content, from_address, EMAIL_TYPE_FROM)
    
    recipient_addresses = get_email_address_list(email_content.recipient_names,
                                                 email_content.recipient_emails)
    if recipient_addresses:
        save_contact(email_content, recipient_addresses, EMAIL_TYPE_TO)
    
    cc_addresses = get_email_address_list(email_content.cc_names,
                                          email_content.cc_emails)
    if cc_addresses:
        save_contact(email_content, cc_addresses, EMAIL_TYPE_CC)


def save_contact(email_content, address_list, type):
    session = load_session()
    fields = get_contact_email_fields(email_content.company_id)
    for name, address in address_list.items():
        print name + '|' + address + '|' + type
        email_address = EmailAddress()
        email_address.email_content_id = email_content.id
        email_address.contact_id = get_contact_id(email_content.company_id,
                                                  address, fields)
        email_address.email_address_type = type
        email_address.email_address = address
        email_address.person_id = get_person_id(email_content.company_id,
                                                address)
        email_address.display_name = name
        session.add(email_address)
        session.flush()
    
    session.commit()
    session.close()


def get_recipients(email_content):
    to_list = get_email_address_list(email_content.recipient_names,
                                     email_content.recipient_emails)
    cc_list = get_email_address_list(email_content.cc_names,
                                     email_content.cc_emails)
    recipient_list = dict(to_list.items() + cc_list.items())
    recipients = {}
    for recipient_address in recipient_list.values():
        recipients[recipient_address] = get_person_id(email_content.company_id,
                                     recipient_address)
    return recipients    


def create_note(email_content):
    session = load_session()
    fields = get_contact_email_fields(email_content.company_id)
    from_person_id = get_person_id(email_content.company_id,
                              email_content.from_email)
    
    from_contact_id = get_contact_id(email_content.company_id,
                                email_content.from_email, fields)
    
    if from_person_id is not 0:
        is_private = 1
        recipients = get_recipients(email_content)

        if 0 in recipients.itervalues():
            is_private = 0
        
        for recipient in recipients.iterkeys():
            recipient_contact_id = get_contact_id(email_content.company_id,
                                                  recipient, fields)
            if recipient_contact_id is not 0:
                note = Note(created_by=recipients[recipient],
                            creation_date=email_content.received_date,
                            note_text='Email',
                            subject='Email',
                            object_type=NOTE_TYPE_CONTACT,
                            note_special_type=NOTE_TYPE_EMAIL,
                            company_id=email_content.company_id,
                            object_referrer=recipient_contact_id,
                            email_content_id=email_content.id,
                            email_address_type=get_email_type(email_content),
                            private_note=is_private
                            )
                session.add(note)
            
    elif from_contact_id is not 0:
        note = Note(created_by=0,
                    creation_date=email_content.received_date,
                    note_text='Email',
                    subject='Email',
                    object_type=NOTE_TYPE_CONTACT,
                    note_special_type=NOTE_TYPE_EMAIL,
                    company_id=email_content.company_id,
                    object_referrer=from_contact_id,
                    email_content_id=email_content.id,
                    email_address_type=get_email_type(email_content),
                    private_note=0
                    )
        session.add(note)
     
    session.commit()
    session.close()
   

def save(email_content, attachments):
    session = load_session()
    session.add(email_content)
    session.flush()
    for attachment in attachments:
        attachment.email_content_id = email_content.id
        session.add(attachment)
    session.commit()
    save_contacts(email_content)
    create_note(email_content)
    session.close()
    print 'done'



if __name__ == "__main__":
    pass

