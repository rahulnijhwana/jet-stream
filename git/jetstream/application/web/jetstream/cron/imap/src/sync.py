#!/usr/bin/python
import email
import imaplib
import os
import re
from email.parser import HeaderParser
from email.utils import getaddresses
from datetime import datetime, timedelta
from time import localtime, strftime, mktime
import db


def parse_addresses(msg, type):
    """
    Builds 2 semi-colon separated strings for database insertion
    1 containing display names, 1 containing email addresses
    """
    names, emails = [], []
    raw = msg.get_all(type, [])
    addresses = getaddresses(raw)
    for address in addresses:
        name, addr = address
        if not name:
            name = addr
        names.append(name)
        emails.append(addr)
    return ";".join(names),";".join(emails)


def sync(user, server):
    ### directory where to save attachments (default: current)
    detach_dir = '/webtemp/attachments/'
    if server.ssl:
        m = imaplib.IMAP4_SSL(server.address, server.port)
    else:
        m = imaplib.IMAP4(server.address, server.port)
    m.login(user.username, user.password)
    m.select(readonly=True)
    local = datetime.today() - timedelta(hours=5)
    today = local.strftime("%d-%b-%Y")
    print 'today: ' + today
    resp, items = m.search(None, '(SINCE "' + today + '")') #@UnusedVariable
    items = items[0].split()
    
    # If this is the first run, just get messages from the last hour
    if not user.last_sync:
        user.last_sync = (datetime.fromtimestamp(mktime(localtime())) - timedelta(hours=5))
    for emailid in items:
        header = m.fetch(emailid, '(BODY[HEADER])')
        parser = HeaderParser()
        msg = parser.parsestr(header[1][0][1])
        received = datetime.fromtimestamp(email.utils.mktime_tz(email.utils.parsedate_tz(msg['Date']))) #@UndefinedVariable
        received -= timedelta(hours=5)
        print 'Last sync: ' + str(user.last_sync)
        print 'Received:' + str(received)
        if(received > user.last_sync):
            print 'new mail'
            email_content = db.EmailContent()
            email_content.recipient_names, email_content.recipient_emails = parse_addresses(msg, 'to')
            email_content.cc_names, email_content.cc_emails = parse_addresses(msg, 'cc')
            email_content.from_name, email_content.from_email = email.utils.parseaddr(msg['From']) #@UndefinedVariable
            received_date = email.utils.parsedate_tz(msg['Date'])[0:9] #@UndefinedVariable
            email_content.received_date = re.sub(r"\b0", "", strftime("%m/%d/%Y %H:%M:%S %p", received_date)) #@UndefinedVariable
            email_content.entry_id = email.utils.unquote(msg['Message-ID']) #@UndefinedVariable
            email_content.subject = msg['Subject']
            email_content.person_id = user.person_id
            email_content.company_id = user.company_id
            resp, data = m.fetch(emailid, "(RFC822)") #@UnusedVariable
            mail = email.message_from_string(data[0][1])
            if mail.get_content_maintype() != 'multipart':
                continue
            attachments = []
            for part in mail.walk():
                if part.get_content_maintype() == 'multipart':
                    continue
                if part.get('Content-Disposition') is None:
                    body = part.get_payload(decode=True)
                    continue
                filename = part.get_filename()
                counter = 1
                if not filename:
                    filename = 'part-%03d%s' % (counter, 'bin')
                    counter += 1
                else:
                    attachment = db.EmailAttachment(filename)
                    attachment.rename(email_content.entry_id)
                    filename = attachment.stored_name
                    attachments.append(attachment)
                att_path = os.path.join(detach_dir, filename)
                if not os.path.isfile(att_path):
                    fp = open(att_path, 'wb')
                    fp.write(part.get_payload(decode=True))
                    fp.close()
    
            email_content.body = body
            print 'body::::'
            print body
            db.save(email_content, attachments)
    
    # Server is set to UTC
    now = datetime.now()
    local = now - timedelta(hours=5) 
    user.last_sync = local.strftime("%m/%d/%Y %H:%M:%S %p")
    db.set_last_sync(user)


# Sync email!
if __name__ == "__main__":
    companies = db.get_companies()
    for company in companies:
        users = db.get_users(company.company_id)
        for user in users:
            server = db.get_server(user.server_id)
            sync(user, server)
