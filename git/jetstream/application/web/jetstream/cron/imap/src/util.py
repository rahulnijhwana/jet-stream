#!/usr/bin/python

from constants import DB_API, DB_USER, DB_PASS, DB_HOST

class ObjectUtil:
    def contents(self, obj):
        print '\n\n\nstart\n'
        for property, value in sorted(vars(obj).iteritems()):
            print property, ": ", value
        print '\nend\n\n\n'
    

def getConnectionString():
    return DB_API + '://' + DB_USER + ':' + DB_PASS + '@' + DB_HOST
