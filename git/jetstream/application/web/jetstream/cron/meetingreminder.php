<?php
define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));

require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/lib.date.php';

$today_array = getdate();
$today_timestamp = mktime(00, 00, 00, $today_array['mon'], $today_array['mday'], $today_array['year']);
$tomorrow_timestamp = mktime(23, 59, 59, $today_array['mon'], $today_array['mday'] + 1, $today_array['year']);
$nextday_timestamp = mktime(23, 59, 59, $today_array['mon'], $today_array['mday'] + 2, $today_array['year']);


$sql = "SELECT CompanyID FROM company where Disabled = 0 AND Slipstream = 1";	
	$company_rs = DbConnManager::GetDb('mpower')->Execute($sql);		
	$company_list = array();	 
	foreach($company_rs as $company) {
		$company_list[] = $company['CompanyID'];
	}
    
//for($k=0;$k<count($company_list);$k++){   
    $sql = "SELECT PersonID, Email FROM people where CompanyID = ? and deleted = 0 and SupervisorID > -3 ";
   
    //$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_list[$k]));
    $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, 508));
            
            $people_rs = DbConnManager::GetDb('mpower')->Execute($sql);		
            $people_list = array();
            $people_email_list = array();
            foreach($people_rs as $people) {
                    $people_list[]          = $people['PersonID'];
                    $people_email_list[]    = $people['Email'];
            } 
   
    $sql = "SELECT FieldName FROM ContactMap where IsFirstName = 1 and CompanyID = ?";
    //$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_list[$k]));
    $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, 508));
            $result = DbConnManager::GetDb('mpower')->getOne($sql);
            $firstname_fieldname = $result->FieldName;
            
   
    $sql = "SELECT FieldName FROM ContactMap where IsLastName = 1 and CompanyID = ?";
    //$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_list[$k]));
    $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, 508));
            $result = DbConnManager::GetDb('mpower')->getOne($sql);
            $lastname_fieldname = $result->FieldName;
            
    
    $sql = "SELECT * FROM AccountMap where IsCompanyName = 1 and CompanyID = ?";
    //$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $company_list[$k]));
    $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, 508));
            $result = DbConnManager::GetDb('mpower')->getOne($sql);
            $companyname_fieldname = $result->FieldName;
            
            
    
    for($i=0;$i<count($people_list);$i++){
	if($people_list[$i] != ''&& $firstname_fieldname != '' && $lastname_fieldname != '' && $companyname_fieldname != ''){
       
         $sql = "Select E.EventID, E.DurationHours, E.DurationMinutes, C.".$firstname_fieldname.", C.".$lastname_fieldname.
                ", A.".$companyname_fieldname." AS companyName, convert(varchar, E.startdate, 108) as eventtime,
                convert(varchar, E.startdate, 110) as eventdate
                from events E Left Join Contact C ON E.ContactID = C.ContactID
                LEFT JOIN account A ON C.AccountID = A.AccountID
                LEFT JOIN EventType ET ON E.EventTypeID = ET.EventTypeID
                WHERE USERID = ?
                 and startdate >= '".date("Y-m-d H:i:s",$today_timestamp)."'and startdate <='".date("Y-m-d H:i:s",$nextday_timestamp)."'
                 and IsCancelled = 0 and IsClosed = 0 and ET.Type = 'EVENT'
                order by StartDate";
                $sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_INT, $people_list[$i]));
        
                
                
                //echo $sql;exit;
                
                
            $people_contact_rs = DbConnManager::GetDb('mpower')->Execute($sql);		
            $people_contact_list = array();
	    $people_contact_fname = array();
            $people_contact_lname = array();
            $people_contact_companyname = array();
            $people_event_startdate = array();
            $people_event_starttime = array();
            $event_id = array();
            $event_duration_hr = array();
            $event_duration_min = array();
            foreach($people_contact_rs as $people_contact) {
                    $people_contact_fname[] = $people_contact[$firstname_fieldname];
                    $people_contact_lname[] = $people_contact[$lastname_fieldname];
                    $people_contact_companyname[] = $people_contact['companyName'];
                    $people_event_startdate[] = $people_contact['eventdate'];
                    $people_event_starttime[] = $people_contact['eventtime'];
                    $event_id[]=$people_contact['EventID'];
                    $event_duration_hr[] = $people_contact['DurationHours'];
                    $event_duration_min[] = $people_contact['DurationMinutes'];
            
	    }
            //echo '<pre>';print_r($event_duration_min);echo '</pre>';
	    $msgbody_today = '---------'."\n";
	    $msgbody_today.= 'Today,  '. date('l, F jS', $today_timestamp)."\n";
	    $msgbody_today.= '---------'."\n";
	    
	    $msgbody_tomorrow = '---------'."\n";
	    $msgbody_tomorrow.= 'Tomorrow,  '. date('l, F jS', $tomorrow_timestamp)."\n";
	    $msgbody_tomorrow.= '---------'."\n";
	    
	    $msgbody_dayafter_tomorrow = '---------'."\n";
	    $msgbody_dayafter_tomorrow.=  date('l, F jS', $nextday_timestamp)."\n";
	    $msgbody_dayafter_tomorrow.= '---------'."\n";
	    
            $msg='';
            
            $today_count = 0;
            $tomorrow_count = 0;
            $dayafter_tommorow_count = 0;
            if(count($event_id)>0){
                for($j=0;$j<count($event_id);$j++){
                    $event_detailarr = array();
                    $event_timearr = array();
                    $event_detailarr = explode('-',$people_event_startdate[$j]);
                    $event_timearr = explode(':',$people_event_starttime[$j]);
                    
                    if($event_detailarr[1] == date('j',$today_timestamp)){
                        $today_count = $today_count+1;
                        $msgbody_today.='Company:  '.$people_contact_companyname[$j]."\n";
                        $msgbody_today.='Contact:  '.$people_contact_fname[$j].' '.$people_contact_lname[$j]."\n";
                        
                        $msgbody_today.='Time:  '.DrawTime(mktime($event_timearr[0],$event_timearr[1] , 00, $today_array['mon'], $today_array['mday'], $today_array['year']));
                        $msgbody_today.=' - '.DrawTime(mktime($event_timearr[0]+$event_duration_hr[$j],$event_timearr[1]+$event_duration_min[$j] , 00, $today_array['mon'], $today_array['mday'], $today_array['year']));
                        if($event_duration_min[$j] == 0)
                        $msgbody_today.=' ('.$event_duration_hr[$j].' hrs)'."\n\n";
                        else if($event_duration_hr[$j] == 0)
                        $msgbody_today.=' ('.$event_duration_min[$j].' mins)'."\n\n";
                        else
                        $msgbody_today.=' ('.$event_duration_hr[$j].' hrs '.$event_duration_min[$j].' mins)'."\n\n";
                    }
                    
                    
                    if($event_detailarr[1] == date('j',$tomorrow_timestamp)){
                        $tomorrow_count = $tomorrow_count+1;
                        $msgbody_tomorrow.='Company:  '.$people_contact_companyname[$j]."\n";
                        $msgbody_tomorrow.='Contact:  '.$people_contact_fname[$j].' '.$people_contact_lname[$j]."\n";
                        
                       
                        $msgbody_tomorrow.='Time:  '.DrawTime(mktime($event_timearr[0],$event_timearr[1] , 00, $today_array['mon'], $today_array['mday']+1, $today_array['year']));
                        $msgbody_tomorrow.=' - '.DrawTime(mktime($event_timearr[0]+$event_duration_hr[$j],$event_timearr[1]+$event_duration_min[$j] , 00, $today_array['mon'], $today_array['mday']+1, $today_array['year']));
                        if($event_duration_min[$j] == 0)
                        $msgbody_tomorrow.=' ('.$event_duration_hr[$j].' hrs)'."\n\n";
                        else if($event_duration_hr[$j] == 0)
                        $msgbody_tomorrow.=' ('.$event_duration_min[$j].' mins)'."\n\n";
                        else
                        $msgbody_tomorrow.=' ('.$event_duration_hr[$j].' hrs '.$event_duration_min[$j].' mins)'."\n\n";
                        
                    }
                    
                    
                    if($event_detailarr[1] == date('j',$nextday_timestamp)){
                        $dayafter_tommorow_count = $dayafter_tommorow_count+1;
                        $msgbody_dayafter_tomorrow.='Company:  '.$people_contact_companyname[$j]."\n";
                        $msgbody_dayafter_tomorrow.='Contact:  '.$people_contact_fname[$j].' '.$people_contact_lname[$j]."\n";
                        
                        $msgbody_dayafter_tomorrow.='Time:  '.DrawTime(mktime($event_timearr[0],$event_timearr[1] , 00, $today_array['mon'], $today_array['mday']+2, $today_array['year']));
                        $msgbody_dayafter_tomorrow.=' - '.DrawTime(mktime($event_timearr[0]+$event_duration_hr[$j],$event_timearr[1]+$event_duration_min[$j] , 00, $today_array['mon'], $today_array['mday']+2, $today_array['year']));
                        if($event_duration_min[$j] == 0)
                        $msgbody_dayafter_tomorrow.=' ('.$event_duration_hr[$j].' hrs)'."\n\n";
                        else if($event_duration_hr[$j] == 0)
                        $msgbody_dayafter_tomorrow.=' ('.$event_duration_min[$j].' mins)'."\n\n";
                        else
                        $msgbody_dayafter_tomorrow.=' ('.$event_duration_hr[$j].' hrs '.$event_duration_min[$j].' mins)'."\n\n";
                        
                    }
                    
                    
                }
            }
            if($today_count == 0)
                $msgbody_today.='No Meetings Scheduled'."\n\n";
            
            if($tomorrow_count == 0)
                $msgbody_tomorrow.='No Meetings Scheduled'."\n\n";
            
            if($dayafter_tommorow_count == 0)
                $msgbody_dayafter_tomorrow.='No Meetings Scheduled'."\n\n";
                
                
            $msg = $msgbody_today.$msgbody_tomorrow.$msgbody_dayafter_tomorrow;
            //echo $msg;
           mail($people_email_list[$i],
            "Jetstream Appointment Reminder",
            $msg, "From: jetstream@asasales.com");
            $msg='';
           
	}


    }
    
//}


?>