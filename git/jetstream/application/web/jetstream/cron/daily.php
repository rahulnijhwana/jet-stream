<?php
// This script should be run once daily, for database housekeeping.
// It MUST be run using a PHP that has MSSQL support compiled in.

// Additional queries may be added to the $queries array as needed.

// Russ Price, 7-27-05

// RJP 2-2-06: do snapshot procedure before salescycle calc, so
// that it doesn't fall through the cracks if the salescycle
// calc crashes.

// rjp 2-22-06:	use mail() function instead of relying on cron,
// 		so we can indicate success

//		integrity check function per Larry

// rjp 4-19-06: log to logfile

// rjp 1-30-07: Limit first meeting integrity check to 25 months
//              per Paul Willarson, so that we don't worry about
//              ancient history.  This came about due to a database
//              space issue that required old log records to be cleared.
//		Also, make the integrity check ignore disabled companies.

// rjp 5-16-07: Break up salescycle calc to use a separate query for
//              each company instead of a single query for all companies.
//              Also, process only active companies.

// vjg 10-29-13:
//	I could not find the /var/log/httpd/ folder.  I found the dailyproc.log at /var/log/ so I changed the LogEntry function.

function LogEntry($text)
{
	//$logfile = "/var/log/httpd/dailyproc.log";
	$logfile = "/var/log/dailyproc.log";
	$file = fopen($logfile, "a+");
	$now = date("M j Y G:i:s");
	fwrite($file, "$now $text");
	fclose($file);
}

$now = date("M j Y G:i:s");
$txt = "Daily housekeeping run started at $now\r\n";
LogEntry($txt);
$msg = $txt;

//$dbalias = 'ASA-VERIO';
//$dbname = 'mpasac';
////$pw = 'fybna+ae';
//$pw = 'qraal+zv';

$dbalias = "192.168.55.131";
//$myUser = "sa";
$user = "sa";
$pw = "tech";
$dbname = "mpasac_test";

// Query descriptions - keep in sync with $queries array below!
$qdescs = array(
"Alert fix part 1",
"Alert fix part 2",
"Alert fix part 3",
"Snapshot",
);

// NOTE: Be sure to separate array elements with commas
$queries = array(

// Larry's alert fix, part 1
"update opportunities set alert3=0 "
. "where (Category =3 or Category >=5) and (alert3!=0 or alert3 is null);",

// Larry's alert fix, part 2
"update opportunities "
. "set alert3 = "
. "case when MissedMeetingGracePeriod < datediff(Day, FirstMeeting, GetDate()) "
. " then 1 "
. " else 0 "
. "end "
. "from company, opportunities where company.companyid=opportunities.companyid "
. " and category=1",

// Larry's alert fix, part 3
"update opportunities "
. "set alert3 = "
. "case when MissedMeetingGracePeriod < datediff(Day, NextMeeting, GetDate()) "
. " then 1 "
. " else 0 "
. "end "
. "from company, opportunities where company.companyid=opportunities.companyid "
. " and category in (2,4);",

// Snapshot
// added .dbo on 11/13/2013 (vjg)
"exec mpasac.dbo.dosnapshot;",

); // *** END OF ARRAY ***

// New function for integrity check

function integrity_check()
{
	global $msg;
	global $file;
	$rows = 0;
	$txt = "Checking First Meeting integrity: ";
	$query = "select DealID from opportunities inner join company "
		. "on opportunities.companyid = company.companyid "
		. "where (disabled <> 1) "
		. "and (firstmeeting is not null) "
		. "and (firstmeeting > dateadd(month, -25, getdate())) "
		. "and dealid not in "
		. "(select dealid from log where transactionid=3)";

	$result = mssql_query($query);
	while($row = mssql_fetch_array($result)) {
		if($rows < 50) {
			$failed[$rows] = $row['DealID'];
		}
		$rows++;
	}
	mssql_free_result($result);
	$txt .= $rows ? "FAILED! ($rows opportunities)\r\n" : "PASSED\r\n";
	if($rows > 50) {
		$txt .= "First 50 ";
	}
	if($rows) {
		$txt .= "Incorrect Opportunities:\r\n";
		for($i = 0; $i < $rows && $i < 50; $i++) {
			$txt .= "Deal ID $failed[$i]\r\n";
		}
	}
	LogEntry($txt);
	$msg .= $txt;
	return ($rows == 0);
}

function check_result($check, $text, $exit_code) {
	global $msg;
	if(!$check) {
		$logtime = date("M j Y G:i:s");
		$conn_err = mssql_get_last_message();
		LogEntry("$text: $conn_err\r\n");
		$msg .= "$logtime: $text: $conn_err\r\n";
		if($exit_code > 0) {
			mssql_close();
			mail("sysreports@asasales.com",
				"Daily processing - FAIL! - $logtime",
				$msg, "From: root@www.mpasa.com");
			exit($exit_code);
		}
	}
}

for($i = 1; $i <= 10 && !$dbconn; $i++) {
	$dbconn = mssql_connect($dbalias, $dbname, $pw);
	check_result($dbconn, "Try $i: Connection failed", 0);
}

if(!$dbconn) {
	global $msg;
	$txt = "Can't connect - giving up!\r\n";
	LogEntry($txt);
	$msg .= $txt;
	$logtime = date("M j Y G:i:s");
	mail("sysreports@asasales.com",
		"Daily processing - FAIL! - $logtime",
		$msg, "From: root@www.mpasa.com");
	exit(1);
}

if($result = mssql_select_db($dbname));
check_result($result, "Unable to select database", 2);

$integrity = integrity_check();

for($i = 0; $i < count($queries); $i++) {
	$now = date("M j Y G:i:s");
	$msg .= "Query $i (" . $qdescs[$i] . ") started at $now\r\n";
	LogEntry("Query $i (" . $qdescs[$i] . ") started\r\n");
	$result = mssql_query($queries[$i]);
	check_result($result, "Query $i failed", 3);
}

$indicator = $integrity ? "SUCCESS" : "FAIL!";

$line = "Starting salescycle calculations\r\n";
LogEntry($line);
$msg .= $line;

$cursor_co = mssql_query("select CompanyID, Name from Company where Disabled <> 1 order by CompanyID");
$co_fail = 0;
while($row = mssql_fetch_array($cursor_co)) {
	$now = date("M j Y G:i:s");
	$line = "Processing company " . $row[0] . " (" . $row[1] . ")\r\n";
	LogEntry($line);
	$msg .= $now . ": " . $line;
	$q = "exec company_salescycle_calc " . $row[0];
	$ex_result = mssql_query($q);
	if(!$ex_result) {
		$co_fail++;
		$line = "WARNING: Salescycle Calc for " . $row[0] . " (" . $row[1] . ") FAILED\r\n";
		LogEntry($line);
		$msg .= $line;
	}
}

if($indicator != "FAIL!" && $co_fail > 0) {
	$indicator = "WARNING";
}

mssql_close();
$now = date("M j Y G:i:s");
$msg .= "Daily housekeeping run finished at $now\r\n";
LogEntry("Daily housekeeping run finished\r\n");
mail("sysreports@asasales.com", "Daily processing - $indicator - $now",
	$msg, "From: root@www.mpasa.com");
exit(0);
?>
