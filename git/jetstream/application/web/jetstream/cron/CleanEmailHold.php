<?PHP
define('BASE_PATH', realpath(dirname(__FILE__) . '/..'));
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
//ini_set("memory_limit", "32M");

/**
 * Select all the duplicate email entries
 */
$sql = "select count(*) Total, EntryID from EmailContent group by EntryID having count(*) > 1";
$email_list = DbConnManager::GetDb('mpower')->GetSqlResult($sql);

if (!empty($email_list)) {	
	 while($email = mssql_fetch_assoc($email_list)) {
		file_put_contents('clean_email_cron.log', 'Start : '."\n\n\n", FILE_APPEND);
		$total =  $email['Total'];
		$entry_id =  $email['EntryID'];		
		$delete_count = $total - 1;		
		
		if($delete_count > 0) {
			$duplicate_ids = array();
			
			// select all the top most records leaving one latest record
			$sql = "Select TOP $delete_count * FROM EmailContent where EntryID = ?";
			$sql = SqlBuilder()->LoadSql($sql)->BuildSql(array(DTYPE_STRING, $entry_id));
			$duplicate_email_list = DbConnManager::GetDb('mpower')->GetSqlResult($sql);
			
			file_put_contents('clean_email_cron.log', 'Get the email ids: '.$sql."\n", FILE_APPEND);
			
			while($duplicate_email = mssql_fetch_assoc($duplicate_email_list)) {
				$duplicate_ids[] = $duplicate_email['EmailContentID'];
			}
			
			if(!empty($duplicate_ids) && count($duplicate_ids) > 0) {
			// Records to delete.
				$email_delete_ids = implode(',',$duplicate_ids);
				
				if(!empty($email_delete_ids)) {
					// Delete the duplicate email notes 
					// Delete the duplicate emailaddress 
					// Delete the duplicate emailcontent
					$sql = "DELETE from Notes where EmailContentID IN ($email_delete_ids);
							DELETE from EmailAddresses where EmailContentID IN ($email_delete_ids);
							DELETE from EmailContent where EmailContentID IN ($email_delete_ids);";
					$result = DbConnManager::GetDb('mpower')->Exec($sql);
					
					file_put_contents('clean_email_cron.log', 'Delete all the duplicate records : '.$sql."\n", FILE_APPEND);
				}
			}
		}
		file_put_contents('clean_email_cron.log', 'End : '."\n\n\n", FILE_APPEND);
	}
}
