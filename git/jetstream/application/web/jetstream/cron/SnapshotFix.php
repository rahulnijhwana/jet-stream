<?php

// Day list for running the snapshot fix
$day_list = array('05/22/2009', 
				
				'05/25/2009', '05/26/2009', '05/27/2009', '05/28/2009', '05/29/2009', 
				
				'06/01/2009', '06/02/2009', '06/03/2009', '06/04/2009', '06/05/2009',				
				'06/08/2009', '06/09/2009', '06/10/2009', '06/11/2009', '06/12/2009',
				'06/15/2009', '06/16/2009', '06/17/2009', '06/18/2009', '06/19/2009',
				'06/22/2009', '06/23/2009', '06/24/2009', '06/25/2009', '06/26/2009',
				'06/29/2009', '06/30/2009', '07/01/2009', '07/02/2009', '07/03/2009',
				
				'07/06/2009', '07/07/2009', '07/08/2009', '07/09/2009', '07/10/2009',
				'07/13/2009', '07/14/2009', '07/15/2009', '07/16/2009', '07/17/2009',
				'07/20/2009', '07/21/2009', '07/22/2009', '07/23/2009', '07/24/2009',
				'07/27/2009', '07/28/2009', '07/29/2009', '07/30/2009', '07/31/2009',				
				
				'08/03/2009', '08/04/2009', '08/05/2009', '08/06/2009', '08/07/2009',
				'08/10/2009', '08/11/2009', '08/12/2009', '08/13/2009', '08/14/2009',
				'08/17/2009', '08/18/2009');


$now = date("M j Y G:i:s");
writeLog("Snapshot Table Fix Process started at $now");

//live

//$dbalias = 'ASA-VERIO';
//$dbname = 'mpasac';
//$user = 'mpasac';
//$pw = 'qraal+zv';
$dbalias = "192.168.55.131";
$user = "sa";
$pw = "tech";
$dbname = "mpasac_test";


//local
/*$dbalias = 'localhost';
$dbname = 'mpower';
$user = 'sa';
$pw = 'sa99';
*/


//only process companies that are flagged for this operation
function getCompanyList() {
	$sql = "select CompanyID from company where snapshot = 1";		
	$record = array();
	
	$result = mssql_query($sql) or die("MS-Query Error in select-query");
	while ($row = mssql_fetch_assoc($result)) {
		$record[] = $row;
	}
	
	return $record;
}



$dbconn = mssql_connect($dbalias, $user, $pw);	


if(!$dbconn) {	
	$logtime = date("M j Y G:i:s");
	writeLog("Snapshot Table Fix - FAIL! - $logtime Can't connect - giving up!");
	exit(1);
}

if($result = mssql_select_db($dbname)) {
	writeLog('Database selected');
} else {
	writeLog('Unable to select database');
}


$companies = getCompanyList();

writeLog('---');

foreach($companies as $company) {
	$company_id = $company['CompanyID'];
	writeLog("Company ID: $company_id");
	
	foreach($day_list as $day) {	
		
		writeLog("Day: $day");
		writeLog("Run: exec fixsnap $company_id , '$day'");
		
		if($result = mssql_query("exec fixsnap $company_id , '$day'")) {
			writeLog('OK');
		} else {
			writeLog('Failed');
		}	
	
	}
	
	writeLog('Completed');	
	
}

function writeLog($msg) {
	echo $msg."\n";
	file_put_contents('Snapshot.log', $msg."\n", FILE_APPEND);
}

mssql_close();

$now = date("M j Y G:i:s");
writeLog("Snapshot Table Fix Process finished at $now");
exit(0);
