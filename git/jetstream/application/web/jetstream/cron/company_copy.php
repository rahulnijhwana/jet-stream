<?php

/*
 * This is a rough script for copying data from one M-Power company to another
 * It was specifically designed for making a demo version of LVRJ01 that utilizes
 * Jetstream
 * 
 * This could probably be somewhat simplified and greatly sped up by putting an original id
 * field in people and opportunities tables - then it wouldn't require going through
 * each person and opp 1 x 1
 * 
 * NY 1/21/2009
 */

//$myServer = "ASA-VERIO";
//$myUser = "mpasac";
//$myPass = "qraal+zv";
//$myDb = "mpasac";
$myServer = "192.168.55.131";
$myUser = "sa";
$myPass = "tech";
$myDb = "mpasac_test";

$dbhandle = connect($myServer, $myUser, $myPass, $myDb);

$source_company = 351;
$dest_company = 515;
$people = array(8439, 8215, 4735,10104,8449);


// **** Start copying Company settings ****

RunSql("Update c_dest
set c_dest.MonthsConsideredNew = c_source.MonthsConsideredNew
, c_dest.LevelCount = c_source.LevelCount
, c_dest.Requirement1 = c_source.Requirement1
, c_dest.Requirement2 = c_source.Requirement2
, c_dest.FMThreshold = c_source.FMThreshold
, c_dest.IPThreshold = c_source.IPThreshold
, c_dest.DPThreshold = c_source.DPThreshold
, c_dest.CloseThreshold = c_source.CloseThreshold
, c_dest.ClosePeriod = c_source.ClosePeriod
, c_dest.Requirement1used = c_source.Requirement1used
, c_dest.Requirement2used = c_source.Requirement2used
, c_dest.Requirement1trend = c_source.Requirement1trend
, c_dest.Requirement2trend = c_source.Requirement2trend
, c_dest.MissedMeetingGracePeriod = c_source.MissedMeetingGracePeriod
, c_dest.NonUseGracePeriod = c_source.NonUseGracePeriod
, c_dest.IPCloseRatioDefault = c_source.IPCloseRatioDefault
, c_dest.DPCloseRatioDefault = c_source.DPCloseRatioDefault
, c_dest.IPStalledLimit = c_source.IPStalledLimit
, c_dest.DPStalledLimit = c_source.DPStalledLimit
, c_dest.HasCustomAmount = c_source.HasCustomAmount
, c_dest.CustomAmountName = c_source.CustomAmountName
, c_dest.MonetaryValue = c_source.MonetaryValue
, c_dest.AskCloseQuestions = c_source.AskCloseQuestions
, c_dest.CustomSPGroupName = c_source.CustomSPGroupName
, c_dest.AskPersonQuestions = c_source.AskPersonQuestions
, c_dest.AskNeedQuestions = c_source.AskNeedQuestions
, c_dest.AskMoneyQuestions = c_source.AskMoneyQuestions
, c_dest.AskTimeQuestions = c_source.AskTimeQuestions
, c_dest.AskReq1Questions = c_source.AskReq1Questions
, c_dest.AskReq2Questions = c_source.AskReq2Questions
, c_dest.SubMSEnabled = c_source.SubMSEnabled
, c_dest.snapshot = c_source.snapshot
, c_dest.TargetUsed = c_source.TargetUsed
, c_dest.ValuationUsed = c_source.ValuationUsed
, c_dest.SourceUsed = c_source.SourceUsed
, c_dest.MS2Label = c_source.MS2Label
, c_dest.MS3Label = c_source.MS3Label
, c_dest.MS4Label = c_source.MS4Label
, c_dest.MS5Label = c_source.MS5Label
, c_dest.FMLabel = c_source.FMLabel
, c_dest.IPLabel = c_source.IPLabel
, c_dest.IPSLabel = c_source.IPSLabel
, c_dest.DPLabel = c_source.DPLabel
, c_dest.DPSLabel = c_source.DPSLabel
, c_dest.CLabel = c_source.CLabel
, c_dest.RLabel = c_source.RLabel
, c_dest.TLabel = c_source.TLabel
, c_dest.SLabel = c_source.SLabel
, c_dest.FMAbbr = c_source.FMAbbr
, c_dest.IPAbbr = c_source.IPAbbr
, c_dest.IPSAbbr = c_source.IPSAbbr
, c_dest.DPAbbr = c_source.DPAbbr
, c_dest.DPSAbbr = c_source.DPSAbbr
, c_dest.CAbbr = c_source.CAbbr
, c_dest.RAbbr = c_source.RAbbr
, c_dest.TAbbr = c_source.TAbbr
, c_dest.SAbbr = c_source.SAbbr
, c_dest.BillDateUsed = c_source.BillDateUsed
, c_dest.BillDatePrompt = c_source.BillDatePrompt
, c_dest.ViewSiblingsCalendar = c_source.ViewSiblingsCalendar
, c_dest.ViewExpectedBilling = c_source.ViewExpectedBilling
, c_dest.DeptLabel = c_source.DeptLabel
, c_dest.ContactLabel = c_source.ContactLabel
, c_dest.Source2Used = c_source.Source2Used
, c_dest.Source2Label = c_source.Source2Label
, c_dest.MaxDiff = c_source.MaxDiff
, c_dest.ValLabel = c_source.ValLabel
, c_dest.ValAbbr = c_source.ValAbbr
, c_dest.ReviveLabel = c_source.ReviveLabel
, c_dest.RevivedLabel = c_source.RevivedLabel
, c_dest.RenewLabel = c_source.RenewLabel
, c_dest.RenewedLabel = c_source.RenewedLabel
, c_dest.Source2Abbr = c_source.Source2Abbr
, c_dest.MoveShowTarget = c_source.MoveShowTarget
, c_dest.CompanyPopup = c_source.CompanyPopup
, c_dest.Transactional = c_source.Transactional
, c_dest.Translabel = c_source.Translabel
, c_dest.TransAbbr = c_source.TransAbbr
, c_dest.FMForecast = c_source.FMForecast
, c_dest.Seasonality = c_source.Seasonality
FROM
company c_dest
, (SELECT * FROM company WHERE companyid = $source_company) c_source 
WHERE c_dest.companyid = $dest_company");

// **** End copying Company settings ****

// **** Start copying Company lookup and setting tables ****

// Valuations
RunSql("DELETE FROM Valuations WHERE CompanyID = $dest_company");

RunSql("INSERT INTO Valuations 
	(CompanyID, VLevel, Label, Activated)
	SELECT $dest_company, VLevel, Label, Activated 
	FROM Valuations WHERE CompanyID = $source_company");

// Sources
RunSql("DELETE FROM Sources WHERE CompanyID = $dest_company");
RunSql("INSERT INTO Sources 
	(Name, CompanyID, Deleted, Abbr)
	SELECT Name, $dest_company, Deleted, Abbr 
	FROM Sources WHERE CompanyID = $source_company");
	
// Sources2
RunSql("DELETE FROM Sources2 WHERE CompanyID = $dest_company");

RunSql("INSERT INTO Sources2
	(Name, CompanyID, Deleted, Abbr)
	SELECT Name, $dest_company, Deleted, Abbr 
	FROM Sources2 WHERE CompanyID = $source_company");
	
// SubMilestones
RunSql("DELETE FROM SubMilestones WHERE CompanyID = $dest_company");

RunSql("INSERT INTO SubMilestones
	([Prompt]
		,[Location]
		,[CompanyID]
		,[Seq]
		,[IsHeading]
		,[Enable]
		,[Name]
		,[AlphaNum]
		,[Length]
		,[CheckBox]
		,[YesNo]
		,[Calendar]
		,[Mandatory]
		,[FillWholeLength]
		,[DropDown]
		,[DropChoice1]
		,[DropChoice2]
		,[DropChoice3]
		,[DropChoice4]
		,[DropChoice5]
		,[DropChoice6]
		,[DropChoice7]
		,[DropChoice8]
		,[DropChoice9]
		,[DropChoice10]
		,[RightAnswer]
		,[DChoiceOK1]
		,[DChoiceOK2]
		,[DChoiceOK3]
		,[DChoiceOK4]
		,[DChoiceOK5]
		,[DChoiceOK6]
		,[DChoiceOK7]
		,[DChoiceOK8]
		,[DChoiceOK9]
		,[DChoiceOK10]
		,[DependsOn]
		,[DropChoice11]
		,[DropChoice12]
		,[DropChoice13]
		,[DropChoice14]
		,[DropChoice15]
		,[DropChoice16]
		,[DropChoice17]
		,[DropChoice18]
		,[DropChoice19]
		,[DropChoice20]
		,[DChoiceOK11]
		,[DChoiceOK12]
		,[DChoiceOK13]
		,[DChoiceOK14]
		,[DChoiceOK15]
		,[DChoiceOK16]
		,[DChoiceOK17]
		,[DChoiceOK18]
		,[DChoiceOK19]
		,[DChoiceOK20]
	)
	SELECT [Prompt]
		,[Location]
		,515
		,[Seq]
		,[IsHeading]
		,[Enable]
		,[Name]
		,[AlphaNum]
		,[Length]
		,[CheckBox]
		,[YesNo]
		,[Calendar]
		,[Mandatory]
		,[FillWholeLength]
		,[DropDown]
		,[DropChoice1]
		,[DropChoice2]
		,[DropChoice3]
		,[DropChoice4]
		,[DropChoice5]
		,[DropChoice6]
		,[DropChoice7]
		,[DropChoice8]
		,[DropChoice9]
		,[DropChoice10]
		,[RightAnswer]
		,[DChoiceOK1]
		,[DChoiceOK2]
		,[DChoiceOK3]
		,[DChoiceOK4]
		,[DChoiceOK5]
		,[DChoiceOK6]
		,[DChoiceOK7]
		,[DChoiceOK8]
		,[DChoiceOK9]
		,[DChoiceOK10]
		,[DependsOn]
		,[DropChoice11]
		,[DropChoice12]
		,[DropChoice13]
		,[DropChoice14]
		,[DropChoice15]
		,[DropChoice16]
		,[DropChoice17]
		,[DropChoice18]
		,[DropChoice19]
		,[DropChoice20]
		,[DChoiceOK11]
		,[DChoiceOK12]
		,[DChoiceOK13]
		,[DChoiceOK14]
		,[DChoiceOK15]
		,[DChoiceOK16]
		,[DChoiceOK17]
		,[DChoiceOK18]
		,[DChoiceOK19]
		,[DChoiceOK20]
	FROM [SubMilestones]
	WHERE CompanyID = $source_company");

// Seasonality
RunSql("DELETE FROM Seasonality WHERE CompanyID = $dest_company");

RunSql("INSERT INTO Seasonality
	(CompanyID, PersonID, Year, Period, Seasonality, SeasonalityEnabled)
	SELECT $dest_company, PersonID, Year, Period, Seasonality, SeasonalityEnabled
	FROM Seasonality WHERE CompanyID = $source_company");
	
// removereasons
RunSql("DELETE FROM removereasons WHERE CompanyID = $dest_company");

RunSql("INSERT INTO removereasons
	(Text, CompanyID, Deleted)
	SELECT Text, $dest_company, Deleted
	FROM removereasons WHERE CompanyID = $source_company");
	
// products
RunSql("DELETE FROM products WHERE CompanyID = $dest_company");

RunSql("INSERT INTO products
	([Name],[Deleted],[DefaultSalesCycle],[CalculatedSalesCycle]
		,[ForecastSalesCycle],[CompanyID],[StandardDeviation],[YellowAlertPercentage]
		,[RedAlertPercentage],[DefaultValue],[DefaultQty],[Abbr],[UseDefault]
		,[AllowEdit],[CMID],[MfgCode],[MfgPrice])
	SELECT [Name],[Deleted],[DefaultSalesCycle],[CalculatedSalesCycle]
		,[ForecastSalesCycle],$dest_company,[StandardDeviation],[YellowAlertPercentage]
		,[RedAlertPercentage],[DefaultValue],[DefaultQty],[Abbr],[UseDefault]
		,[AllowEdit],[CMID],[MfgCode],[MfgPrice]
	FROM products  WHERE CompanyID = $source_company");
	
// MSAnalysisQuestions
RunSql("DELETE FROM MSAnalysisQuestions WHERE CompanyID = $dest_company");

RunSql("INSERT INTO MSAnalysisQuestions
	(Prompt, Location, CompanyID, Seq, IsHeading)
	SELECT Prompt, Location, $dest_company, Seq, IsHeading
	FROM MSAnalysisQuestions WHERE CompanyID = $source_company");
	
// Levels
RunSql("DELETE FROM levels WHERE CompanyID = $dest_company");

RunSql("INSERT INTO levels
	(CompanyID, Name, LevelNumber, ViewSiblings, Enabled, Import)
	SELECT $dest_company, Name, LevelNumber, ViewSiblings, Enabled, Import
	FROM levels WHERE CompanyID = $source_company");
	
// FiscalYear
RunSql("DELETE FROM FiscalYear WHERE CompanyID = $dest_company");

RunSql("INSERT INTO FiscalYear
	(CompanyID, Year, FiscalYearEndDate, Period, PeriodEndDate, PeriodLabel)
	SELECT $dest_company, Year, FiscalYearEndDate, Period, PeriodEndDate, PeriodLabel
	FROM FiscalYear WHERE CompanyID = $source_company");

// CompanyReportsXRef
RunSql("DELETE FROM CompanyReportsXRef WHERE CompanyID = $dest_company");

RunSql("INSERT INTO CompanyReportsXRef
	(CompanyID, ReportID, Salespeople)
	SELECT $dest_company, ReportID, Salespeople
	FROM CompanyReportsXRef WHERE CompanyID = $source_company");

// **** End copying Company lookup and setting tables ****
	
// **** Start copying People records ****
RunSql("DELETE FROM People WHERE CompanyID = $dest_company");

$people_trans = array();
$count = 0;
while ($count < count($people)) {
	$person = $people[$count];
	$people_trans[$person] = RunSqlGetId("INSERT INTO People
		(FirstName, LastName, CompanyID, StartDate, UserID, PasswordZ, IsSalesperson, Color, Level, RatingViewAllowed, SupervisorID, AverageSalesCycle, LimitSalesCycle, FMThreshold, IPThreshold, DPThreshold, AnalysisID, Rating, CloseThreshold, ViewSiblings, LastUsed, GroupName, TrendState, Deleted, Eula, LastChanged, FMValue, IPValue, DPValue, CloseValue, Email, timezone, DateCreated, DateDeleted, ShowRatingOnBoard, CMID, LastUsedIP, AppearsOnManager, FirstMeeting, AverageSale, ClosingRatio, MfgPrice, TotalRevenues, TotalSales, UnreportedAvgSale, UnreportedSalesPerMonth, PrivateEmail)
		SELECT FirstName, LastName, $dest_company, StartDate, UserID, PasswordZ, IsSalesperson, Color, Level, RatingViewAllowed, SupervisorID, AverageSalesCycle, LimitSalesCycle, FMThreshold, IPThreshold, DPThreshold, AnalysisID, Rating, CloseThreshold, ViewSiblings, LastUsed, GroupName, TrendState, Deleted, Eula, LastChanged, FMValue, IPValue, DPValue, CloseValue, Email, timezone, DateCreated, DateDeleted, ShowRatingOnBoard, CMID, LastUsedIP, AppearsOnManager, FirstMeeting, AverageSale, ClosingRatio, MfgPrice, TotalRevenues, TotalSales, UnreportedAvgSale, UnreportedSalesPerMonth, PrivateEmail
		FROM people WHERE CompanyID = $source_company AND PersonID = $person");

	$supervisor = GetRecord("select supervisorid from
		people where Personid = {$people_trans[$person]} and supervisorid > 0");

	if (count($supervisor) > 0) {
		$supervisor = $supervisor[0]['supervisorid'];
		if (!in_array($supervisor, $people)) {
			$people[] = $supervisor;
		}
	}
			
	RunSql("UPDATE people SET supervisorid = {$people_trans[$person]} WHERE CompanyID = $dest_company AND SupervisorID = $person");
	$count++;
}

RunSql("DELETE FROM Logins WHERE CompanyID = $dest_company");

foreach ($people_trans as $source_person => $dest_person) {
	RunSql("INSERT INTO Logins 
		(CompanyID, PersonID, UserID, PasswordZ, LoginID, Deleted, DateDeleted
		, Eula, Name, FullRights, Email, NoCharge, LastUsed, Import, LastUsedIP
		, Password, ForgetPasswordToken)
		SELECT 
		$dest_company, $dest_person, UserID, PasswordZ, $dest_person, Deleted, DateDeleted
		, Eula, Name, FullRights, Email, NoCharge, LastUsed, Import, LastUsedIP
		, Password, ForgetPasswordToken
		FROM Logins WHERE CompanyID = $source_company AND PersonID = $source_person");
}

// **** Finish copying People records ****

// **** Start copying Opp records ****
RunSql("DELETE FROM opportunities WHERE CompanyID = $dest_company");
RunSql("DELETE FROM Opp_Product_XRef WHERE CompanyID = $dest_company");

$opps_recordset = GetRecord("select dealid, personid
	FROM opportunities WHERE CompanyID = $source_company AND PersonID in (" . implode(", ", $people) . ")");

$opps_trans = array();
foreach ($opps_recordset as $opp) {
	$people_trans[$opp['dealid']] = RunSqlGetId("INSERT INTO opportunities
			(CompanyID, Category, Company, Division, Contact, FirstMeeting
			, ProductID, EstimatedDollarAmount, Requirement1, Person, Need
			, Money, Time, Requirement2, NextMeeting, ActualDollarAmount
			, AdjustedCloseDate, ForecastCloseDate, PersonID, ActualCloseDate
			, NextMeetingTime, LastMoved, Alert1, Alert2, Alert3, Alert4
			, Alert5, Alert6, RemoveReason, RemoveDate, FirstMeetingTime
			, ValID, VLevel, TargetDate, SourceID, BillDate, CMID
			, FM_took_place, NM_New, Source2ID, PrevDaysOld, ChangedByID
			, AutoReviveDate, AutoRepeatIncrement, Renewed, CreateDate
			, Transactional, IsPO, PoNumber, PoStartDate, PoEndDate
			, PoMaxAmount, AccountID, ContactID, IsEventCancelled
			, LatestEventID, FMEventID, NMEventID)
			SELECT $dest_company, Category, Company, Division, Contact, FirstMeeting
			, ProductID, EstimatedDollarAmount, Requirement1, Person, Need
			, Money, Time, Requirement2, NextMeeting, ActualDollarAmount
			, AdjustedCloseDate, ForecastCloseDate, {$people_trans[$opp['personid']]}, ActualCloseDate
			, NextMeetingTime, LastMoved, Alert1, Alert2, Alert3, Alert4
			, Alert5, Alert6, RemoveReason, RemoveDate, FirstMeetingTime
			, ValID, VLevel, TargetDate, SourceID, BillDate, CMID
			, FM_took_place, NM_New, Source2ID, PrevDaysOld, ChangedByID
			, AutoReviveDate, AutoRepeatIncrement, Renewed, CreateDate
			, Transactional, IsPO, PoNumber, PoStartDate, PoEndDate
			, PoMaxAmount, AccountID, ContactID, IsEventCancelled
			, LatestEventID, FMEventID, NMEventID
			FROM opportunities WHERE CompanyID = $source_company AND DealID = {$opp['dealid']}");

	RunSql("INSERT INTO Opp_Product_XRef (ProductID, DealID, Val, Qty, CompanyID
		, Seq, Deleted, ActVal, ActQty, QtyTest, AdjMfgPrice)
		SELECT ProductID, {$people_trans[$opp['dealid']]}, Val, Qty, $dest_company
		, Seq, Deleted, ActVal, ActQty, QtyTest, AdjMfgPrice FROM Opp_Product_XRef
		WHERE CompanyID = $source_company AND DealID = {$opp['dealid']}");
		
	RunSql("INSERT INTO SubAnswers (DealID, SubID, Answer, AnswerBackup
		, AnsweredPersonID, CompletedLocation)
		SELECT {$people_trans[$opp['dealid']]}, SubID, Answer, AnswerBackup
		, AnsweredPersonID, CompletedLocation 
		FROM SubAnswers
		WHERE DealID = {$opp['dealid']}");
}

RunSql("update sa
set subid = (select subid from submilestones where prompt like sm.prompt and companyid = $dest_company) 
from subanswers sa 
left join submilestones sm on sa.subid = sm.subid
where sa.dealid in (select dealid from opportunities where companyid = $dest_company)
and (select subid from submilestones where prompt like sm.prompt and companyid = $dest_company) is not null
");

// Update Productid on opp record
RunSql("update o set productid = (select top 1 productid from products where products.name = p.name and companyid = $dest_company)
from opportunities o
left join products p on o.productid = p.productid
where o.companyid = $dest_company and p.companyid != $dest_company");

// Update Productid on opp_product_xref
RunSql("update o set productid = (select top 1 productid from products where products.name = p.name and companyid = $dest_company)
from opp_product_xref o
left join products p on o.productid = p.productid
where o.companyid = $dest_company and p.companyid != $dest_company");

// Update Removereasons on opp record
RunSql("update o set removereason = (select top 1 reasonid from removereasons where removereasons.text = r.text and companyid = $dest_company)
from opportunities o
left join removereasons r on o.removereason = r.reasonid
where o.companyid = $dest_company and r.companyid != $dest_company");

// Update Sources on opp record
RunSql("update o set sourceid = (select top 1 sourceid from sources where sources.name = s.name and companyid = $dest_company)
from opportunities o
left join sources s on o.sourceid = s.sourceid
where o.companyid = $dest_company and s.companyid != $dest_company");

// Update Sources2 on opp record
RunSql("update o set source2id = (select top 1 source2id from sources2 where sources2.name = s.name and companyid = $dest_company)
from opportunities o
left join sources2 s on o.source2id = s.source2id
where o.companyid = $dest_company and s.companyid != $dest_company");






function getRecord($sql) {	
	// echo $sql . "\n\n";
	$record = array();
	$result = mssql_query($sql) or die("MS-Query Error in select-query 1");
	while ($row = mssql_fetch_assoc($result)) {
		$record[] = $row;
	}
	return $record;
}

/* Function to connect to the db. */
function Connect($myServer, $myUser, $myPass, $myDB) {
	// connection to the database
	$dbhandle = @mssql_connect($myServer, $myUser, $myPass);
	if($dbhandle) {
		// select a database to work with
		$selected = @mssql_select_db($myDB, $dbhandle);
		if($selected) {
			return $dbhandle;
		}
	}
	return false;
}

function RunSqlGetId($sql) {
	RunSql($sql);
	$id_result = mssql_query("select IdentityInsert = @@identity");
	$id = mssql_fetch_assoc($id_result);
	return $id['IdentityInsert''];
}

function RunSql($sql) {
	global $dbhandle;
	// echo $sql . "\n\n";
	$result = mssql_query($sql) or die("MS-Query Error in select-query 2");
	return mssql_rows_affected($dbhandle) . "\n";
}

