<!DOCTYPE html> 
<html>
<head>
    <title>Jetstream</title>
    <link rel="icon" href="images/JetstreamFavicon.png" />
    <link rel="shortcut icon" href="images/JetstreamFavicon.png" />
    <link rel="apple-touch-icon" href="images/JetstreamFavicon.png" />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="js/jquery.mobile/jquery.mobile-1.4.2.min.css" />
    <link rel="stylesheet" href="../jetstream/css/jquery-ui.css" />
    <link rel="stylesheet" href="style/style.css" />
    <script src="js/jquery.mobile/jquery-1.7.1.min.js"></script>
    <script src="js/jquery.mobile/jquery.mobile-1.4.2.min.js"></script>
    <!--script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script-->

    <!-- use Mobipicker than default datepicker -->
    <!--link rel="stylesheet" href="js/jquery.mobile/jquery.mobile.datepicker.css" />
    <script src="js/jquery.mobile/jquery.ui.datepicker.js"></script>
    <script id="mobile-datepicker" src="js/jquery.mobile/jquery.mobile.datepicker.js"></script-->

    <link rel="stylesheet" type="text/css" href="style/mobipick.css" />
    <script type="text/javascript" src="js/xdate.js"></script>
    <script type="text/javascript" src="js/xdate.i18n.js"></script>
    <script type="text/javascript" src="js/mobipick.js"></script>
</head>
<body>

<?php
if ( $pageid == 'common-login-login' ) $pageid = 'common-login';
?>

<div data-role="page" id="<?php echo $pageid; ?>">
    <?php
    $login_css = '';
    if ( $pageid == 'common-login' ) {
        $login_css = 'style="background-color:gray;"';
    ?>
    <div data-role="content" id="logo-wrap" style="background-color:white;">
        <img src='./images/logo.png' />
    </div>
    <?php
    }
    ?>

    <div data-role="navbar" data-iconpos="left" id="navbar">
        <ul>
            <li>
                <a href="#" <?php if ( $pageid == 'common-login' ) { ?> onclick="return false" <?php } else { ?> onclick="goContact()" <?php } ?>data-icon="user" class='lnk-contact-list' <?php echo $login_css; ?>>Contacts</a>
            </li>
            <li>
                <a href="#" <?php if ( $pageid == 'common-login' ) { ?> onclick="return false" <?php } else { ?> onclick="goEvent()" <?php } ?> data-icon="calendar" class='lnk-event-list' <?php echo $login_css; ?>>Events</a>
            </li>
            <li>
                <a href="#" <?php if ( $pageid == 'common-login' ) { ?> onclick="return false" <?php } else { ?> onclick="<?php echo ($login) ? 'logout()' : 'login()'; ?>" <?php } ?> data-icon="power"  class='lnk-common-login' <?php echo $login_css; ?>><?php echo ($login) ? 'Logout' : 'Login'; ?></a>
            </li>
        </ul>
    </div>
    <div id='error' class='ui-bar'></div>
    <?php
        $navlnk =  ( $pageid == 'event-new' ) ? 'event-list' : $pageid ;
        $navlnk =  ( $pageid == 'event-list-uncompleted' ) ? 'event-list' : $pageid ;
    ?>
        
    <script>
        <?php
        if ( $pageid != 'common-login' ) {
        ?>
        var navlnk = 'lnk-<?php echo $navlnk; ?>';
        $('#navbar a').removeClass('ui-btn-active').removeClass('ui-state-persist');
        $('.' + navlnk).addClass('ui-btn-active').addClass('ui-state-persist');
        <?php
        }
        ?>

        function showStatus(msg) {
            var oStatus = $('div#error');
            oStatus.text(msg).show('slow');
            setTimeout(function(){
                oStatus.hide('slow');
            },2000);
        }
    </script>
