<?php echo $header; ?>
<?php if($error != '') { ?> <script> showStatus('<?php echo $error; ?>'); </script><?php } ?>
	<form id="login-form" action="<?php echo $lnk_action; ?>" method="post" data-ajax="false">
	<div data-role="content"  data-theme="d">
		<li data-role="fieldcontain">
			<label for="company">Company :</label>
			<input type="text" name="company" value='<?php echo $company; ?>' />
			<input type="hidden" name="browser_timezone" value='' />
		</li>
		<li data-role="fieldcontain">
			<label for="username">Username :</label>
			<input type="text" name="username" value='' />
		</li>
		<li data-role="fieldcontain">
			<label for="password">Password :</label>
			<input type="password" name="password" value='' />
		</li>
	</div>
	<input type="submit" id='login-submit' value="Login">
	<br/>
	<button id='see-full-version' data-href='<?php echo $fullUrl; ?>'>See Full Version</button>
	</form>
<?php echo $footer; ?>
