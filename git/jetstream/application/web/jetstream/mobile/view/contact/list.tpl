<?php echo $header; ?>
<?php if($error != '') { ?><script> showStatus('<?php echo $error; ?>'); </script><?php } ?>

<?php
    $contactid = isset($contact['ContactID']) ? $contact['ContactID'] : NULL;
    $companyname = isset($contact['Account']) ? $contact['Account'] : NULL;
    $firstname = isset($contact['firstname']) ? $contact['firstname'] : NULL;
    $lastname = isset($contact['lastname']) ? $contact['lastname'] : NULL;
    $email = isset($contact['email']) ? $contact['email'] : NULL;
    $website = isset($contact['website']) ? $contact['website'] : NULL;
    
    $phone = NULL;
    $phone = isset($contact['direct_phone']) ? $contact['direct_phone'] : $phone;
    $phone = isset($contact['main_phone']) ? $contact['main_phone'] : $phone;
    
?>

    <link rel="stylesheet" href="style/contact.css" />
    <ul id="contact-atc" data-role="listview" data-inset="true" data-filter="true" data-theme='a' data-filter-placeholder="Find Contact or Company" data-filter-theme="a"></ul>

    <form id="contact-form" action='index.php?route=contact/list/addNote' method="post" data-ajax="true">
        <div data-role="content"  data-theme="d" id='content'>
            <li data-role="fieldcontain">
                <label for="company">Company:</label>
                <input type="text" name="companyname" placeholder="Company Name" value="<?php echo $companyname; ?>" readonly>
                <input type='hidden' name='contactid' value="<?php echo $contactid ?>"/>
                <input type='hidden' name='ContactID' value="<?php echo $contactid ?>"/>
                <input type='hidden' name='mode' value='' />
            </li>
            <li data-role="fieldcontain" id='li-name'>
                <label for="address">Name:</label>
                <fieldset class='ui-grid-a'>
                    <div class='ui-block-a no-margin-top'>
                        <input type="text" name="firstname" placeholder="First Name" value="<?php echo $firstname; ?>" readonly>
                    </div>
                    <div class='ui-block-b no-margin-top'>
                        <input type="text" name="lastname" placeholder="Last Name" value="<?php echo $lastname; ?>" readonly>
                    </div>
                </fieldset>
            </li>
            <li data-role="fieldcontain">
                <label for="phone">Phone:</label>
                <input type="text" name="phone" placeholder="Phone" value="<?php echo $phone; ?>" readonly>
            </li>
            <li data-role="fieldcontain" id='li-email'>
                <label for="email">Email:</label>
                <input type="text" name="email" placeholder="Email" value="<?php echo $email; ?>" readonly>
            </li>
            <li data-role="fieldcontain" id='li-website'>
                <label for="website">Website:</label>
                <input type="text" name="website" placeholder="Website" value="<?php echo $website; ?>" readonly>
            </li>
            <!--li data-role="fieldcontain">
                <label for="address">Address:</label>
                <fieldset class='ui-grid-a'>
                    <div class='ui-block-a no-margin-top'>
                        <input type="text" name="address1" placeholder="Address" value="">
                    </div>
                    <div class='ui-block-b no-margin-top'>
                        <input type="text" name="address2" placeholder="Address2" value="">
                    </div>
                </fieldset>
            </li>
            <li data-role="fieldcontain">
                <label for="address">City/State:</label>
                <fieldset class='ui-grid-b'>
                    <div class='ui-block-a no-margin-top'>
                        <input type="text" name="city" placeholder="City" value="">
                    </div>
                    <div class='ui-block-b no-margin-top'>
                        <input type="text" name="state" placeholder="State" value="">
                    </div>
                    <div class='ui-block-b no-margin-top'>
                       <input type="text" name="zip" placeholder="Zipcode" value="">
                    </div>
                </fieldset>
            </li-->
            <li data-role="fieldcontain">
                <label for="note">Note:</label>
                <fieldset class='ui-grid'>
                    <div class='ui-block no-margin-top'>
                        <input type="text" name="note_title" placeholder="Subject" value="" class='add-note-title'>
                    </div>
                    <div class='ui-block add-note-body'>
                        <textarea rows="10" name="note" id="note"></textarea>
                    </div>
                </fieldset>
                
            </li>
            <?php
            include DIR_VIEW. 'event/event_alert.tpl';
            ?>
            <li data-role="fieldcontain" class='submit-button-wrap'>
                <input type="button" id="contact-submit" value="Add Note">
            </li>
            <?php 
            if (isset($notes)) {
                $noteHtml = '<li data-role="fieldcontain" class="ui-field-contain note-wrap"></li>';
                foreach($notes as $note) {
                    $noteHtml .= '<li data-role="fieldcontain" class="ui-field-contain note note-wrap">';
                    $noteHtml .= '<table><tr><td class="head">' . $note['Subject'] . ' (' . $note['Creator'] . ')</td></tr>';
                    $noteHtml .= '<tr><td>' . $note['Body'] . '</td></tr>';
                    $noteHtml .= '<tr><td class="foot">' . $note['TimeStamp'] . '</td></tr></table></li>';
                }
                echo $noteHtml;
            }
            ?>
        </div>
    </form>

<script src="js/event.js"></script>
<?php echo $footer; ?>
