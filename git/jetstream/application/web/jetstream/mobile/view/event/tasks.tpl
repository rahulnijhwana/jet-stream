<div data-role="collapsible" data-iconpos="right" id="task-list-wrap">
    <h4>Tasks</h4>
    <ul data-role="listview" class="ui-listview-outer" data-inset="true" class='task-page'>
    <?php
    foreach($tasks as $task) {
        //echo '<pre>'; print_r($task); echo '</pre>';

        if ( $task['ContactName'] != '' ) {
            $contact = $task['ContactName'];
        } else {
            $contact = $task['AccountName'];
        }
    ?>
                    <li data-role="collapsible" data-iconpos="right" data-shadow="false" data-corners="false" class='row' data-collapsed='true'>
                    <h2><?php echo $task['Subject']; ?></h2>
                    <ul data-role="listview" data-shadow="false" data-inset="true" data-corners="false">
                    <li class="show-all"><?php echo $contact; ?></li>
                    <li class="show-all"><?php echo $task['when']; ?></li>
                    <!--li class="show-all">Attendees : <?php echo $task['attendees']; ?></li-->
                    <li class="show-all">Type : <?php echo $task['etype']; ?></li>
                    <li class="show-all">Location : <?php echo $task['Location']; ?></li>
                    <?php
                    if ( $task['first_note'] != '' ) {
                    ?>
                    <li class="show-all"><?php echo $task['first_note']; ?></li>
                    <?php
                    }
                    ?>
                    <li data-role="collapsible" data-iconpos="right" data-shadow="false" data-corners="false" class='note-wrap' data-collapsed='false'>
                    <h2>Add Note</h2>
                    <form class='event-note-form'>
                    <fieldset class='ui-grid'>
                    <div class='ui-block no-margin-top'>
                    <input type="text" name="note_title" placeholder="Subject" value="" class='add-note-title'>
                    <input type="hidden" name='eventid' value='<?php echo $task['EventID']; ?>' >
                    <input type="hidden" name='contactid' value='<?php echo $task['ContactID']; ?>' >
                    <input type="hidden" name='accountid' value='<?php echo $task['AccountID']; ?>' >
                    <input type="hidden" name='mode' value='task' >
                    </div>
                    <div class='ui-block add-note-body'>
                    <textarea cols="40" rows="10" name="note" id="note"></textarea>
                    </div>
                    </fieldset>
                    <fieldset data-role="controlgroup">
                    <input type="checkbox" name="CheckComplete" id="CheckComplete"
                    data-id="<?php echo $task['ContactID']; ?>" data-name="<?php echo $contact; ?>" >
                    <label for="CheckComplete">Complete Task</label>
                    </fieldset>

                    <?
                    if ( $task['ContactID'] != '' ) {
                    ?>
                    <fieldset data-role="controlgroup" id='check-complete-wrap'>
                    <input type="checkbox" name="CheckNewEvent" id="CheckNewEvent" 
                    data-id="<?php echo $task['ContactID']; ?>" data-name="<?php echo $task['ContactName']; ?>" data-type='contact' data-eventtype='task'>
                    <label for="CheckNewEvent">Add Next Event</label>
                    </fieldset>
                    <?php
                    } // check ContactID
                    ?>

                    <?
                    if ( $task['AccountID'] != '' ) {
                    ?>
                    <fieldset data-role="controlgroup" id='check-complete-wrap'>
                    <input type="checkbox" name="CheckNewEvent" id="CheckNewEvent" 
                    data-id="<?php echo $task['AccountID']; ?>" data-name="<?php echo $task['AccountName']; ?>" data-type='account' data-eventtype='task'>
                    <label for="CheckNewEvent">Add Next Event</label>
                    </fieldset>
                    <?php
                    } // check AccountID
                    ?>

                    <input type="submit" value="Submit" class='add-event-note' data-id="<?php echo $task['EventID']; ?>" />
                    </form>
                    </li>
                    </ul>
                    </li>
                    <?php
    }
    ?>
    </ul>
</div>
