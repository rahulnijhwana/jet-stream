<?php echo $header; ?>
<?php if($error != '') { ?><script> showStatus('<?php echo $error; ?>'); </script><?php } ?>

<script>
var people_list = [];
<?php
foreach($people_list as $id => $name) {
    if ( trim($name) != '' ) {
?>
    people_list['<?php echo $id; ?>'] = "<?php echo $name; ?>";
<?php
    }
}
?>
</script>

<?php
$recLabel = ucwords($rec_type); 
$eventLabel = ucwords($event_type);
?>

<div data-role="header">
    <!--h1>Add New <?php echo $eventLabel; ?></h1-->
    <h1>Add New Event</h1>
</div>

<form method="post" action="index.php?route=event/new/saveEvent" style="padding:0;margin:0;" data-ajax="false" id='event-form'>
<div data-role="content"  data-theme="d" id='content'>
    <li data-role="fieldcontain">
        <!-- TODO : form_required -->
        <label for="<?php echo $recLabel; ?>"><?php echo $recLabel; ?><span class="form_required">*</span></label>
        <!-- todo : ATC -->
        <input type="text" name="<?php echo $recLabel; ?>" id="<?php echo $recLabel; ?>" placeholder="<?php echo $recLabel; ?>" value="<?php echo $rec_name; ?>">
        <input type="hidden" name="EventID" value="">
        <input type="hidden" name="<?php echo $recLabel; ?>ID" value="<?php echo $rec_id; ?>">
        <input type="hidden" name="DealID" value="">
    </li>
    <li data-role="fieldcontain">
        <label for="Subject">Subject<span class="form_required">*</span></label>
        <input type="text" name="Subject" id="Subject" placeholder="Subject" value="">
    </li>
    <li data-role="fieldcontain">
        <label for="Location">Location</label>
        <input type="text" name="Location" id="Location" placeholder="Location" value="">
    </li>

    <li data-role="fieldcontain">
        <label for="address">Type<span class="form_required">*</span></label>
        <select name="EventType" id="EventType" class="clsSelect" onchange="DoChangeType(this.value, this);">
            <option value="" event_type="">Event Type</option>

            <optgroup label="Events (Date with Time)">
            <?php
            foreach($eventtypes as $etype ) {
                if ( 'EVENT' == $etype['Type'] ) {
            ?>
                <option style="background-color:<?php echo $etype['EventColor']; ?>" type='EVENT' value="<?php echo $etype['EventTypeID']; ?>"><?php echo $etype['EventName']; ?></option>                        
            <?php
                }
            }
            ?>
            </optgroup>

            <optgroup label="Tasks (Date only)">
            <?php
            foreach($eventtypes as $etype ) {
                if ( 'TASK' == $etype['Type'] ) {
            ?>
                <option style="background-color:<?php echo $etype['EventColor']; ?>" type='TASK' value="<?php echo $etype['EventTypeID']; ?>"><?php echo $etype['EventName']; ?></option>                        
            <?php
                }
            }
            ?>
            </optgroup>


        </select>                       
    </li>

    <li data-role="fieldcontain" id='priority_wrap'>
        <label for="address">Priority</label>
        <select name="priority" id="priority">
            <option value="">Priority</option>
            <option value="1">A - (Highest)</option>
            <option value="2">B - (High)</option>
            <option value="3">C - (Medium)</option>
            <option value="4">D - (Low)</option>
        </select>
    </li>

    <?php
    $defaultDate = date('m/d/Y');
    ?>
    <li data-role="fieldcontain" id="startdate_wrap">
        <label for="StartDate"><span id='label-prefix'>Start Date</span>
        <span class="form_required">*</span></label>
        <input type="text" name="StartDate" id="StartDate" class='mobipicker' value="">
    </li>

    <li data-role="fieldcontain" id='event_datetime'>
        <label for="address">Time<span class="form_required">*</span></label>
        <?php require_once DIR_VIEW . 'event/timeSelect.inc.tpl'; ?>
    </li>

    <li data-role="fieldcontain" id='enddate_wrap'>
        <label for="EndDate">End Date<span class="form_required">*</span></label>
        <input type="text" name="EndDate" id="EndDate" class='mobipicker' value="">
    </li>

    <li data-role="fieldcontain" id='event_repeat_wrap'>
        <label for="address">Repeat</label>
        <select name="Repeat" onchange="DoChangeRepeat(this.value);">
            <option label="Does not repeat" value="0">Does not repeat</option>
            <option label="Daily" value="1">Daily</option>
            <option label="Weekly" value="2">Weekly</option>
            <option label="Monthly" value="3">Monthly</option>
            <option label="Yearly" value="5">Yearly</option>
        </select>
    </li>

    <li data-role="fieldcontain" id="event_repeat" style="display:none;">
        <label></label>
        <div class="ui-corner-all custom-corners">
            <fieldset id="repeat_choices" class="ui-body" style="border: 1px solid rgb(230, 230, 230);">
                <legend>Repeat Frequency</legend>
                <div id="repeat_1" data-role='fieldcontain' class="ui-grid-b">
                    <div class="ui-block-a" style='line-height: 60px;text-align: right;padding-right: 10px;'>Every</div>
                    <div class="ui-block-b">
                        <select name="daily_freq" style='width:100px;'><option></option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option></select>
                    </div>
                    <div class="ui-block-c" style='line-height:60px; text-align:left; padding-left:10px;'>day(s)</div>
                    <!--div class="ui-block-d"></div>
                    <div class="ui-block-e"></div-->
                </div>
                <div id="repeat_2">
                    <div class="ui-grid-b">
                        <div class="ui-block-a" style='line-height: 60px;text-align: right;padding-right: 10px;'>Every</div>
                        <div class="ui-block-b">
                            <select name="weekly_freq"><option></option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option></select>
                        </div>
                        <div class="ui-block-c" style='line-height:60px; text-align:left; padding-left:10px;'>week(s) on:</div>
                    </div>
                    <div class="ui-grid-solo">
                        <div class="ui-block-a"><label><input type="checkbox" name="weekly_day[]" value="0">Sunday</label></div>
                        <div class="ui-block-a"><label><input type="checkbox" name="weekly_day[]" value="1">Monday</label></div>
                        <div class="ui-block-a"><label><input type="checkbox" name="weekly_day[]" value="2">Tuesday</label></div>
                        <div class="ui-block-a"><label><input type="checkbox" name="weekly_day[]" value="3">Wednesday</label></div>
                        <div class="ui-block-a"><label><input type="checkbox" name="weekly_day[]" value="4">Thursday</label></div>
                        <div class="ui-block-a"><label><input type="checkbox" name="weekly_day[]" value="5">Friday</label></div>
                        <div class="ui-block-a"><label><input type="checkbox" name="weekly_day[]" value="6">Saturday</label></div>
                    </div>
                </div>


                <div id="repeat_3">

                    <div class="ui-grid-b">
                        <div class="ui-block-a" style='line-height: 60px;text-align: right;padding-right: 10px;'>Every</div>
                        <div class="ui-block-b">
                            <select name="monthly_freq"><option></option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option></select>
                        </div>
                        <div class="ui-block-c" style='line-height:60px; text-align:left; padding-left:10px;'>week(s) on:</div>
                    </div>
                    
                    <div class="ui-grid-b">
                        <div class="ui-block-a">
                            <label for="monthly_type3">On the</label>
                            <input type="radio" name="monthly_type" id="monthly_type3" value="3" />
                        </div>
                        <div class="ui-block-b" style='line-height:60px; text-align:left; padding-left:10px;'>
                            <select name="monthly_daynum"><option></option><option value="1">1st</option><option value="2">2nd</option><option value="3">3rd</option><option value="4">4th</option><option value="5">5th</option><option value="6">6th</option><option value="7">7th</option><option value="8">8th</option><option value="9">9th</option><option value="10">10th</option><option value="11">11th</option><option value="12">12th</option><option value="13">13th</option><option value="14">14th</option><option value="15">15th</option><option value="16">16th</option><option value="17">17th</option><option value="18">18th</option><option value="19">19th</option><option value="20">20th</option><option value="21">21st</option><option value="22">22nd</option><option value="23">23rd</option><option value="24">24th</option><option value="25">25th</option><option value="26">26th</option><option value="27">27th</option><option value="28">28th</option><option value="29">29th</option><option value="30">30th</option><option value="31">31st</option></select>
                        </div>
                        <div class="ui-block-c"></div>
                    </div>

                    <div class="ui-grid-b">
                        <div class="ui-block-a">
                            <label for="monthly_type4">On the</label>
                            <input type="radio" name="monthly_type" id="monthly_type4" value="4" />
                        </div>
                        <div class="ui-block-b" style='line-height:60px; text-align:left; padding-left:10px;'>
                            <select name="monthly_pos">
                                <option></option>
                                <option label="First" value="1">First</option>
                                <option label="Second" value="2">Second</option>
                                <option label="Third" value="3">Third</option>
                                <option label="Fourth" value="4">Fourth</option>
                                <option label="Last" value="5">Last</option>
                            </select>
                        </div>
                        <div class="ui-block-c">
                            <select name="monthly_pos_type">
                                <option></option>
                                <option label="day" value="7">day</option>
                                <option label="weekday" value="8">weekday</option>
                                <option label="weekend day" value="9">weekend day</option>
                                <option label="Sunday" value="0">Sunday</option>
                                <option label="Monday" value="1">Monday</option>
                                <option label="Tuesday" value="2">Tuesday</option>
                                <option label="Wednesday" value="3">Wednesday</option>
                                <option label="Thursday" value="4">Thursday</option>
                                <option label="Friday" value="5">Friday</option>
                                <option label="Saturday" value="6">Saturday</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div id="repeat_5">


                    <div class="ui-grid-b">
                        <div class="ui-block-a" style='line-height: 60px;text-align: right;padding-right: 10px;'>Every</div>
                        <div class="ui-block-b">
                            <select name="yearly_freq"><option></option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select>
                        </div>
                        <div class="ui-block-c" style='line-height:60px; text-align:left; padding-left:10px;'>year(s) on:</div>
                    </div>
                    
                    <div class="ui-grid-c">
                        <div class="ui-block-a">
                            <label for="yearly_type5">On the</label>
                            <input type="radio" name="yearly_type" id="yearly_type5" value="5" />
                        </div>
                        <div class="ui-block-b" style='line-height:60px; text-align:left; padding-left:10px;'>
                            <select name="yearly_monthnum"><option value=""></option><option></option><option value="1">January</option><option value="2">February</option><option value="3">March</option><option value="4">April</option><option value="5">May</option><option value="6">June</option><option value="7">July</option><option value="8">August</option><option value="9">September</option><option value="10">October</option><option value="11">November</option><option value="12">December</option></select>
                        </div>
                        <div class="ui-block-c">
                            <select name="yearly_daynum"><option></option><option value="1">1st</option><option value="2">2nd</option><option value="3">3rd</option><option value="4">4th</option><option value="5">5th</option><option value="6">6th</option><option value="7">7th</option><option value="8">8th</option><option value="9">9th</option><option value="10">10th</option><option value="11">11th</option><option value="12">12th</option><option value="13">13th</option><option value="14">14th</option><option value="15">15th</option><option value="16">16th</option><option value="17">17th</option><option value="18">18th</option><option value="19">19th</option><option value="20">20th</option><option value="21">21st</option><option value="22">22nd</option><option value="23">23rd</option><option value="24">24th</option><option value="25">25th</option><option value="26">26th</option><option value="27">27th</option><option value="28">28th</option><option value="29">29th</option><option value="30">30th</option><option value="31">31st</option></select>
                        </div>
                        <div class="ui-block-d"></div>
                    </div>

                    <div class="ui-grid-c">
                        <div class="ui-block-a">
                            <label for="yearly_type6">On the</label>
                            <input type="radio" name="monthly_type" id="yearly_type6" value="6" />
                        </div>
                        <div class="ui-block-b" style='line-height:60px; text-align:left; padding-left:10px;'>
                            <select name="yearly_pos">
                                <option></option>
                                <option label="First" value="1">First</option>
                                <option label="Second" value="2">Second</option>
                                <option label="Third" value="3">Third</option>
                                <option label="Fourth" value="4">Fourth</option>
                                <option label="Last" value="5">Last</option>
                            </select>
                        </div>
                        <div class="ui-block-c">
                            <select name="yearly_pos_type">
                                <option></option>
                                <option label="day" value="7">day</option>
                                <option label="weekday" value="8">weekday</option>
                                <option label="weekend day" value="9">weekend day</option>
                                <option label="Sunday" value="0">Sunday</option>
                                <option label="Monday" value="1">Monday</option>
                                <option label="Tuesday" value="2">Tuesday</option>
                                <option label="Wednesday" value="3">Wednesday</option>
                                <option label="Thursday" value="4">Thursday</option>
                                <option label="Friday" value="5">Friday</option>
                                <option label="Saturday" value="6">Saturday</option>
                            </select>
                        </div>
                        <div class="ui-block-d">
                            <select name="yearly_pos_month"><option value=""></option><option></option><option value="1">January</option><option value="2">February</option><option value="3">March</option><option value="4">April</option><option value="5">May</option><option value="6">June</option><option value="7">July</option><option value="8">August</option><option value="9">September</option><option value="10">October</option><option value="11">November</option><option value="12">December</option></select>
                        </div>
                    </div>

                    
                </div>
                <div class="form_error" id="err_RepeatFreq"></div>
            </fieldset>

            <fieldset id="repeat_span" class="" style="border: 1px solid rgb(230, 230, 230);">
                <legend>Repeat Until</legend>
                
                <div class="ui-grid-solo">
                    <!--div class="ui-block-a"></div-->
                    <div class="ui-block-a">
                        <input type="radio" name="repeat_enddate_type" id='repeat_enddate_type1' value="0" checked="">
                        <label for="repeat_enddate_type1">No end date</label>
                    </div>
                    

                </div>
                <div class="ui-grid-a">
                    <!--div class="ui-block-a"></div-->
                    <div class="ui-block-a">
                        <input type="radio" name="repeat_enddate_type" id='repeat_enddate_type2' value="1">
                        <label for="repeat_enddate_type2">End by </label>
                        
                    </div>
                    <div class="ui-block-b">
                        <input type="text" name="repeat_enddate" class='mobipicker' size="6" value="" id="dp1399474426730">  
                    </div>
                    <!--div class="ui-block-d"></div-->

                    


                </div>
            </fieldset>    
        </div>
    </li>

    <?php
    $permitted_people = $people_list;
    ?>
    <li data-role="fieldcontain" id='primary-attendee-wrap'>
        <label for="">Primary Attendee</label>
        <fieldset class='ui-grid-a'>
        <div class='ui-block no-margin-top'>
            <select name="creator">
            <?php foreach($permitted_people as $pp_id => $pp_name) { ?>
                <option value="<?php echo $pp_id; ?>" <?php if ($pp_id == $creator) { ?>selected<?php } ?> ><?php echo $pp_name; ?></option>
            <?php } ?>
            </select>
        </div>
        </fieldset>
    </li>
 
    <li data-role="fieldcontain" id='attendee_section'>
        <label for="address">Other Attendees</label>
        <fieldset class='ui-grid'>
            <div class='ui-block no-margin-top'>
                <select name="add_attendee" id="add_attendee"></select>
            </div>
            <div class='ui-block no-margin-top'>
                <input type="button" value="Add" onclick="AddAttendee();">
                <input type="hidden" name="attendee_list" value="">
            </div>
            <div class='ui-block no-margin-top'>
                <div id="attendees" style="margin-left:10px;"></div>
            </div>
        </fieldset>
    </li>
    
    <!--li data-role="fieldcontain">
        <label for=""></label>
        <fieldset class='ui-grid'>
            <div id="attendees" style="margin-left:10px;"></div>
        </fieldset>
    </li-->

    <li data-role="fieldcontain" class='list-attendees'>
        <label for=""></label>
        <fieldset data-role="controlgroup">
            <input type="checkbox" name="CheckPrivate" id="CheckPrivate">
            <label for="CheckPrivate">Private</label>
        </fieldset>
    </li>

    <li data-role="fieldcontain">
        <label for="address">Note</label>
        <textarea name="Note" class="full" cols="35" rows="8" id="Notes"></textarea>                
    </li>

    <?php include DIR_VIEW. 'event/event_alert.tpl'; ?>

    <li data-role="fieldcontain">
        <input type="submit" value="Submit" id='save-event'>
    </li>

</div>
</form>

<script src="js/event.js"></script>

<?php
if ( $event_type == 'task' ) {
?>
<script>
/* Al decided to same with Event
    $('#event_datetime').hide();
    $('#event_repeat_wrap').hide();
    $('#primary-attendee-wrap').hide();
    $('#attendee_section').hide();
*/
</script>
<?php
}
?>
<?php echo $footer; ?>
