<?php
$timestamp = strtotime($alert['CreatedDate']);
$timestamp -= UTC_OFFSET;
$created = '@' . $timestamp;
$created = new DateTime($created, new DateTimeZone('America/Chicago'));
?>

<div class="show" data-role="dialog" id="note-alert">
    <h2>Note Alert!</h2>
    <table class="note_alert_table">
        <tr>
            <td class="subtitle">Subject</td>
            <td><?php echo $alert['Subject']; ?></td>
        </tr>
        <?php
        if ( $alert['Contact'] != '' ) {
        ?>
        <tr>
            <td class="subtitle">Contact</td>
            <td><?php echo $alert['Contact']; ?></td>
        </tr>
        <?php
        }
        ?>
        <tr>
            <td class="subtitle">Created</td>
            <td><?php echo $alert['FullName']; ?></td>
        <tr>
        <?php
        if ( $alert['Account'] != '' ) {
        ?>
        </tr>
            <td class="subtitle">Company</td>
            <td><?php echo $alert['Account']; ?></td>
        </tr>
        <?php
        }
        ?>
        <tr>
            <td class="subtitle">Date</td>
            <td>
            <?php 
                echo $created->format('F j Y, g:i a'); 
                //echo $created->format('Y m d, g:i a'); 
                //echo $created;
            ?>
            </td>
        </tr>
        <tr>
            <td class="subtitle">Type</td>
            <td><?php echo $alert['Type']; ?></td>
        </tr>
    </table>
    <div id="note_text">
        <?php echo nl2br($alert['NoteText']); ?>
    </div>
    <input type="hidden" name="alert_recipient_id" id="alert_recipient_id" value="<?php echo $alert['AlertRecipientID']; ?>" />
    <input type="hidden" name="alert_recipient_id" id="previous_alert_recipient_id" value="<?php echo $alert['previous']; ?>" />
    <input type="hidden" name="alert_recipient_id" id="next_alert_recipient_id" value="<?php echo $alert['next']; ?>" />

    <fieldset class='ui-grid'>
    <div class='ui-block'>
        <select id="snoozer" name="snoozer" style="margin: 12px 10px 0px 0px">
            <option value="5">5 &nbsp;Minutes</option>
            <option value="10">10 Minutes</option>
            <option value="15">15 Minutes</option>
            <option value="30">30 Minutes</option>
            <option value="60">1 Hour</option>
            <option value="120">2 Hours</option>
            <option value="240">4 Hours</option>
            <option value="480">8 Hours</option>
            <option value="720">12 Hours</option>
            <option value="1440">24 Hours</option>
        </select>
    </div>
    <input type='button' onclick='snoozeAlert();' class='ui-block' name='snooze' id='snooze' value='Snooze'/>
    <input type='button' onclick='dismissAlert();' class='ui-block' name='dismiss' id='dismiss' value='Dismiss'/>
    <a href='#' data-rel='back' name='close' id='close' style='display:none;' />
    </fieldset>

</div>

