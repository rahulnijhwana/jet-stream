<?php echo $header; ?>
<?php if($error != '') { ?><script> showStatus('<?php echo $error; ?>'); </script><?php } ?>

<link rel="stylesheet" href="style/event.css" />

<fieldset class='ui-grid-a'>
    <div class='ui-block-a'>
        <input type="button" value="Today's" onclick="goEvent();"
        <?php if ( $pageid == 'event-list' ) echo 'style="background-color:#9CAAc6; opacity:1; font-size:1em; text-indent:0;"'; ?> >
    </div>
    <div class='ui-block-b'>
        <input type="button" value="Uncompleted" onclick="goUncompletedEvent();"
        <?php if ( $pageid == 'event-list-uncompleted' ) echo 'style="background-color:#9CAAc6; opacity:1; font-size:1em; text-indent:0"'; ?> >
    </div>
</fieldset>

<?php
if ( $hasTask == true ) {
    //echo '<pre>'; print_r($tasks); echo '</pre>'; exit;
    require_once DIR_VIEW . 'event/tasks.tpl';
?>
<?php
}
?>

<?php
if (false == $hasEvent) {
?>
    <div data-role="content" id='content' class='event-no-result'>
        No Events
    </div>
<?php echo $footer; ?>
<?php
    exit;
}
?>

<div data-role="collapsible" data-iconpos="right" id="task-list-wrap" data-collapsed='false'>
<h4>Events</h4>
<ul data-role="listview" class="ui-listview-outer" data-inset="true" class='event-page'>
    <?php
    $isMpower = false;
    foreach($events as $event) {
        $aContact = $event['Contact'];
        $contact = $aContact['FirstName'] . ' ' . $aContact['LastName'];
        if ( isset($aContact['CompanyName']) && '' != $aContact['CompanyName'] ) {
            $contact .= ' (' . $aContact['CompanyName'] . ')';
        }

        if ( $event['DealID'] != '' ) {
            $isMpower = true;
        } else {
            $isMpower = false;
        }

        $module = $event['module'];

    ?>
    <li data-role="collapsible" data-iconpos="right" data-shadow="false" data-corners="false" class='row' data-collapsed='true'>
        <h2><?php echo $event['Subject']; ?></h2>
        <ul data-role="listview" data-shadow="false" data-inset="true" data-corners="false">
            <li class="show-all"><?php echo $contact; ?></li>
            <li class="show-all"><?php echo $event['when']; ?></li>
            <li class="show-all">Attendees : <?php echo $event['attendees']; ?></li>
            <li class="show-all">Type : <?php echo $event['etype']; ?></li>
            <li class="show-all">Location : <?php echo $event['Location']; ?></li>
            <li data-role="collapsible" data-iconpos="right" data-shadow="false" data-corners="false" class='note-wrap' data-collapsed='false'>
                <h2>Add Note</h2>
                <form class='event-note-form'>
                <fieldset class='ui-grid'>
                    <?php
                    if ( $isMpower ) {
                    ?>
                    <div class='ui-block no-margin-top' style='text-align:center'>
                        <h3 style="color:#808080;">M-Power Event</h3>
                        <div style="color:#808080;">Completion must be done on the main site</div>
                    </div>
                    <?php
                    } // mpower message
                    ?>
                    <div class='ui-block no-margin-top'>
                        <input type="text" name='note_title' placeholder="Subject" value="" class='add-note-title'>
                        <input type="hidden" name='eventid' value='<?php echo $event['EventID']; ?>' >
                        <input type="hidden" name='contactid' value='<?php echo $event['ContactID']; ?>' >
                        <input type="hidden" name='ContactID' value='<?php echo $event['ContactID']; ?>' >
                        <input type="hidden" name='mode' value='event' >
                    </div>
                    <div class='ui-block add-note-body'>
                        <textarea cols="40" rows="10" name="note" id="note"></textarea>
                    </div>
                    <!--div class='ui-block add-note-body'>
                        <input type="button" class="add-event-note" value="Submit">
                    </div-->
                </fieldset>
                <?php
                if ( !$isMpower ) {
                ?>
                <fieldset data-role="controlgroup">
                    <input type="checkbox" name="CheckComplete" id="CheckComplete"
                    data-id="<?php echo $event['ContactID']; ?>" data-name="<?php echo $contact; ?>" >
                    <label for="CheckComplete">Complete Event</label>
                </fieldset>
                <fieldset data-role="controlgroup" id='check-complete-wrap'>
                    <input type="checkbox" name="CheckNewEvent" id="CheckNewEvent"
                    data-id="<?php echo $event['ContactID']; ?>" data-name="<?php echo $contact; ?>" data-eventtype='event' data-type='contact'>
                    <label for="CheckNewEvent">Add Next Event</label>
                </fieldset>
                <?php
                }   // if not mpower
                ?>

                <?php include DIR_VIEW. 'event/event_alert.tpl'; ?>

                <input type="submit" value="Submit" class='add-event-note' data-id="<?php echo $event['EventID']; ?>" />
                </form>
            </li>
        </ul>
    </li>
    <?php
    }
    ?>
</ul>
</div>

<script src="js/event.js"></script>
<?php echo $footer; ?>
