		<div class="alert_options">
			<span class="subtitle" style="padding-left:20px;">1. How do you want to send the Alert?</span>
			<table class="user_tree">
				<tr>
					<td>
						<p class="delivery_methods">
							<label for="standard_event_alert">
								<input type="checkbox" name="standard_event_alert" id="standard_event_alert" checked="checked" />
								On Screen
							</label>
						</p>
					</td>
					<td>
						<p class="delivery_methods">
							<label for="email_event_alert">
								<input type="checkbox" name="email_event_alert" id="email_event_alert" />
								Email
							</label>
						</p>
					</td>
				</tr>
			</table>
			<div class="form_error" id="err_Delivery"></div>
		</div>
		<div class="alert_options">
			<span class="subtitle" style="padding-left:20px;">2. When should we send it?</span>
			<table class="user_tree" style="width:100%;">
				<tr>
					<td class="left_cell">
						<p class="timing">
							<label for="send_event_immediately_yes">
								<input type="radio" name="send_event_immediately" id="send_event_immediately_yes" checked="checked" value="1" />
								Now
							</label>
						</p>
					</td>
                </tr>
                <tr>
					<td class="right_cell">
						<p class="timing">
							<label for="send_event_immediately_no">
								<input type="radio" name="send_event_immediately" id="send_event_immediately_no" value="0" /> 
								Delay until
							</label>
						</p>
						<input type="text" name="delay_event_until" id="delay_event_until_day" class="mobipicker" size="12" value="" />
						<span class="time_select">
                            <?php
                            echo $module->hourSelect;
                            echo $module->minuteSelect;
                            echo $module->periodSelect;
                            ?>
                        </span> 
					</td>
				</tr>
			</table>
			<div class="form_error" id="err_Delay"></div>
		</div>
		<div class="alert_options">
			<span class="subtitle" style="padding-left:20px;">3. Who should we send it to?</span>
			<p>
				<span class="links">
					<a href="javascript:void(0);" id="select_event_all">Select All</a> |
					<a href="javascript:void(0);" id="select_event_assigned">Select Assigned</a>
					<!--a href="javascript:void(0);" id="select_event_level">Select Level</a-->
				</span>
			</p>
			<div class="form_error" id="err_Users"></div>
			<span id="level_event_msg">Select users by level.</span>
            <br/>
			<table class="user_tree user_select" style="border:1px #e2e2e2 solid;width:100%;">
                <?php foreach($module->tree as $level => $users) { ?>
                <tr>
					<td style="text-align:center;">
                        <!--a href="javascript:void(0);" id="level_<?php echo $level; ?>" class="level"><?php echo $level; ?></a-->
                        <?php echo $level; ?>
					</td>
				</tr>
                <tr>
                    <td class="user_level">
							<ul class="simple">
                                <?php 
                                foreach($users as $user) {
                                ?>
									<li style='list-style:none;' <?php if ( isset($user['AssignedTo']) && $user['AssignedTo']) { ?>class="heavy"<?php } ?> >
										<p class="timing">
											<label>
												<input type="checkbox" name="alert_user[]" value="<?php echo $user['PersonID']; ?>" class="all_users <?php if ( isset($user['AssignedTo']) && $user['AssignedTo']) { ?> assigned_users<?php } ?> event_level_<?php echo $user['Level']; ?>" />
												<?php
                                                echo $user['FirstName'];
                                                echo $user['LastName'];
                                                ?>
											</label>
										</p>
										<div class="clearing"></div>
									</li> 
                                <?php } ?>
							</ul>
				    </td>
                </tr>
                <?php } ?>
			</table>
		</div>
        <input type="hidden" id="all_event_checked" name="all_event_checked" />
        <input type="hidden" id="assigned_event_checked" name="assigned_event_checked" />
        <input type="hidden" id="level_event_checked" name="level_event_checked" />
        <input type="hidden" id="add_event_alert_hidden" name="add_event_alert_hidden" />
        <input type="hidden" id="eventNoteSplType" name="eventNoteSplType" value="" />
        <script>
            $('#delay_event_until_day').mobipick();
            $('#event_hours').prop('disabled', false);
            $('#event_minutes').prop('disabled', false);
            $('#event_periods').prop('disabled', false);
        </script>
