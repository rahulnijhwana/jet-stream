<?php
require_once '../slipstream/class.ModuleEventNew.php';

class ControllerEventNew extends Controller {

    public function index() {

		if (!$this->user->isLogin()) {
			$this->response->redirect('index.php?route=common/login');
		}

		$data['error'] = $this->error;

		$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');

        //echo '<pre>'; print_r($this->request->get); echo '</pre>'; exit;

    	$event_type = isset($this->request->get['eventtype']) ? $this->request->get['eventtype'] : 'event';
    	$rec_type = isset($this->request->get['rectype']) ? $this->request->get['rectype'] : 'contact';
    	$rec_id = isset($this->request->get['recid']) ? $this->request->get['recid'] : 0;
    	$rec_name = isset($this->request->get['recname']) ? $this->request->get['recname'] : '';

        if ( $rec_id == '' ) return false;
        if ( $rec_name == '' ) return false;

        /*
        echo '<pre>'; print_r($event_type); echo '</pre>';
        echo '<pre>'; print_r($rec_type); echo '</pre>';
        echo '<pre>'; print_r($rec_id); echo '</pre>';
        echo '<pre>'; print_r($rec_name); echo '</pre>'; exit;
        */

		$repeat_date = false;
        /*
		if (isset($this->request->get['eventid'])) {
			$rec_id = $this->request->get['eventid'];
			$rec_type = 'event';
			$action = 'edit';
			if ($rec_id < 0) {
				$action = $_GET['action'];
			}
		} elseif (isset($this->request->get['accountid'])) {
			$data['rec_id'] = $accountid;
			$data['rec_name'] = $accountname;
			$data['rec_type'] = 'account';
			$data['action'] = 'add';
		} elseif (isset($this->request->get['contactid'])) {
			$data['rec_id'] = $contactid;
			$data['rec_name'] = $contactname;
			$data['rec_type'] = 'contact';
			$data['action'] = 'add';
		} elseif (isset($this->request->get['oppid'])) {
			$rec_id = $this->request->get['oppid'];
			$rec_type = 'opp';
			$action = 'add';
		} else {
			$data['contactid'] = 0;
			$data['contactname'] = '';
			$data['rec_type'] = 'contact';
			$data['action'] = 'add';
		}
        */
		$data['rec_id'] = $rec_id;
		$data['rec_name'] = $rec_name;
		$data['rec_type'] = $rec_type;
		$data['event_type'] = $event_type;
		$data['action'] = 'add';

		/* TODO repeat later */
		/*
		if (isset($_GET['rep'])) {
			// Ref is passed when they select opening an instance of a recurring
			$rec_type = 'event';
			// echo base64_decode(filter_input(INPUT_GET, 'rep')) . '<br>';
			$repeating_instance = json_decode(base64_decode(filter_input(INPUT_GET, 'rep')), true);
			$rec_id = $repeating_instance['i'];
			$repeat_date = TrucatedDateToUnix($repeating_instance['d']);
			$action = 'edit';
		}
		*/
		
		/* form var not necessary yet */
		/*
		$form_vals = array();
		foreach(array_keys($_POST) as $post_var) {
			$form_vals[$post_var] = filter_input(INPUT_POST, $post_var);
			if ($form_vals[$post_var] == 'null') {
				unset ($form_vals[$post_var]);
			}
		}
		if (isset($_GET['StartDate'])) {
			$form_vals['StartDate'] = filter_input(INPUT_GET, 'StartDate');
		}
		if (isset($_GET['EndDate'])) {
			$form_vals['EndDate'] = filter_input(INPUT_GET, 'EndDate');
		}
		if (isset($_GET['BlankTime'])) {
			$form_vals['BlankTime'] = filter_input(INPUT_GET, 'BlankTime');
		}
		if (isset($_GET['Subject'])) {
			$form_vals['Subject'] = filter_input(INPUT_GET, 'Subject');
		}
		*/
		$userid = $this->user->userid;
		if ('' == $userid)	return false;

		$companyid = $this->user->companyid;
		if ('' == $companyid)	return false;

		$oEventNew = new ModuleEventNew($companyid, $userid);
		$oEventNew->Build($data['rec_type'], $data['rec_id'], $data['action'], true, $repeat_date);

		$data['eventtypes'] = $oEventNew->GetEventTypes();
		//echo '<pre>'; print_r($data['eventtypes']); echo '</pre>'; exit;
		$data['creator'] = $userid;
		//echo '<pre>'; print_r($oEventNew); echo '</pre>'; exit;
		
		$people_list = $oEventNew->GetPermittedPeople();
		//echo '<pre>'; print_r($people_list); echo '</pre>'; exit;

		$data['people_list'] = $people_list;
		//echo '<pre>'; print_r($people_list); echo '</pre>'; exit;

        $data['module'] = $oEventNew;
		$template = '/event/new.tpl';
		$this->response->setOutput($this->load->view($template, $data));
    }

    public function getAlertHtml() {
//echo '<pre>'; print_r($this->request->post); echo '</pre>'; exit;
        $userid = $this->user->userid;
        if ('' == $userid)  return false;

        $companyid = $this->user->companyid;
        if ('' == $companyid)   return false;

        $rec_type = $this->request->post['rec_type'];
        $rec_id = $this->request->post['rec_id'];
        $action = $this->request->post['action'];

        $oEventNew = new ModuleEventNew($companyid, $userid);
        $oEventNew->Build($rec_type, $rec_id, $action, true);

        $data['module'] = $oEventNew;
		$template = '/event/event_alert.inc.tpl';
		//$this->response->setOutput($this->load->view($template, $data));
        $html = $this->load->view($template, $data);

        echo json_encode($html);
    }


    public function saveEvent() {

		$userid = $this->user->userid;
		if ('' == $userid)	return false;

		$companyid = $this->user->companyid;
		if ('' == $companyid)	return false;

		$form_filters = array(
			'EventID' => FILTER_SANITIZE_NUMBER_INT,
			'AccountID' => FILTER_SANITIZE_NUMBER_INT,
			'ContactID' => FILTER_SANITIZE_NUMBER_INT,
			'DealID' => FILTER_SANITIZE_NUMBER_INT,
			'Contact' => FILTER_SANITIZE_STRING,
			'Subject' => FILTER_SANITIZE_STRING,
			'Location' => FILTER_SANITIZE_STRING,
			'EventType' => FILTER_SANITIZE_NUMBER_INT,
			'StartDate' => FILTER_SANITIZE_STRING,
			'StartTime' => FILTER_SANITIZE_STRING,
			'EndTime' => FILTER_SANITIZE_STRING,
			'EndDate' => FILTER_SANITIZE_STRING,
			'Repeat' => FILTER_SANITIZE_NUMBER_INT,
			'attendee_list' => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_NO_ENCODE_QUOTES),
			'CheckPrivate' => FILTER_SANITIZE_STRING,
			'RepeatParent' => FILTER_SANITIZE_NUMBER_INT,
			'OrigStartDate' => FILTER_SANITIZE_NUMBER_INT,
			'Note' => FILTER_SANITIZE_STRING,
			'priority' => FILTER_SANITIZE_NUMBER_INT,
			'add_event_alert_hidden' => FILTER_SANITIZE_NUMBER_INT,
			'email_event_alert' => FILTER_SANITIZE_STRING,
			'standard_event_alert' => FILTER_SANITIZE_STRING,
			'send_event_immediately' => FILTER_SANITIZE_STRING,
			'delay_event_until' => FILTER_SANITIZE_STRING,
			'event_hours' => FILTER_SANITIZE_STRING,
			'event_minutes' => FILTER_SANITIZE_STRING,
			'event_periods' => FILTER_SANITIZE_STRING,
            'creator' => FILTER_SANITIZE_NUMBER_INT
		);
	
		$form_values = filter_input_array(INPUT_POST, $form_filters);

        $form_values = $_POST;
        $form_values['alert_users'] = isset($_POST['alert_user']) ? $_POST['alert_user'] : '';
        /*
        if(isset($form_values['email_event_alert'])){
            $form_values['email_event_alert'] = 'on';
        } else {
            $form_values['email_event_alert'] = '';
        }
        */

		switch ($form_values['Repeat']) {
			case REPEAT_DAILY :
				$repeat_detail_filters = array(
					'daily_freq' => FILTER_SANITIZE_NUMBER_INT
				);
				break;
				
			case REPEAT_WEEKLY :
				$repeat_detail_filters = array(
					'weekly_freq' => FILTER_SANITIZE_NUMBER_INT,
					'weekly_day' => array('filter' => FILTER_VALIDATE_INT, 'flags' => FILTER_REQUIRE_ARRAY)
				);
				break;
				
			case REPEAT_MONTHLY :
				$repeat_detail_filters = array(
					'monthly_freq' => FILTER_SANITIZE_NUMBER_INT,
					'monthly_type' => FILTER_SANITIZE_NUMBER_INT,
					'monthly_daynum' => FILTER_SANITIZE_NUMBER_INT,
					'monthly_pos' => FILTER_SANITIZE_NUMBER_INT,
					'monthly_pos_type' => FILTER_SANITIZE_NUMBER_INT
				);
				break;
				
			case REPEAT_YEARLY :
				$repeat_detail_filters = array(
					'yearly_freq' => FILTER_SANITIZE_NUMBER_INT,
					'yearly_type' => FILTER_SANITIZE_NUMBER_INT,
					'yearly_monthnum' => FILTER_SANITIZE_NUMBER_INT,
					'yearly_daynum' => FILTER_SANITIZE_NUMBER_INT,
					'yearly_pos' => FILTER_SANITIZE_NUMBER_INT,
					'yearly_pos_type' => FILTER_SANITIZE_NUMBER_INT,
					'yearly_pos_month' => FILTER_SANITIZE_NUMBER_INT
				);
				break;
				
			default :
				$repeat_detail_filters = array();
		}

		if (count($repeat_detail_filters) > 0) {
			$form_values['repeat_enddate_type'] = filter_input(INPUT_POST, 'repeat_enddate_type', FILTER_SANITIZE_NUMBER_INT);
			$form_values['repeat_enddate'] = filter_input(INPUT_POST, 'repeat_enddate', FILTER_SANITIZE_STRING);
		}

		$form_values['repeat_detail'] = filter_input_array(INPUT_POST, $repeat_detail_filters);

		$form_values['CheckPrivate'] = 	(isset($form_values['CheckPrivate']) && 'on' == $form_values['CheckPrivate'] ) ? true : false;

		//$referer_url = parse_url(filter_input(INPUT_SERVER, 'HTTP_REFERER'));
		//$referer_url_query = ParseQuery($referer_url['query']);
		$source = '';

		/*
		if (isset($referer_url_query['action'])) {
			$source = $referer_url_query['action'];
		}
		else {
			$source = '';
		}
		*/
		
		$event = new ModuleEventNew($companyid, $userid);

		$no_save = ($form_values['DealID'] != 0) ? true : false;


        /* custome code to generate attendees_list */
        //echo '<pre>'; print_r($form_values); echo '</pre>';
        //$attendees = (!empty($form_values['ContactID'])) ? array($form_values['ContactID']) : array() ;
        $attendees = array($userid);
        if (!empty($form_values['creator'])) {
            array_push( $attendees, $form_values['creator'] );    
        }
        $otherAttendees =  array_filter(json_decode($form_values['attendee_list']));
        if ( count($otherAttendees) > 0 ) {
            foreach ( $otherAttendees as $o ) {
                array_push( $attendees, $o );
            }
        }

        $attendees = array_unique($attendees);
        $form_values['attendee_list'] = json_encode($attendees);
        //echo '<pre>'; print_r($form_values); echo '</pre>'; exit;

		$results = $event->SaveForm($form_values, $no_save, $source);

        $form_values['Creator'] = $userid;
		if ($results['status']['STATUS'] == 'OK') {
			$this->response->redirect('index.php?route=event/list');
		} else {

			$aMsg = $results['status']['ERROR'];
			foreach ( $aMsg as $key => $val ) {
				$msg = '[' . $key . '] ' . $val;
			}

            error_log('Error while inserting Event on Mobile');
            error_log(print_r($results, true));

			$this->error = $msg;
			$this->index();
				
		}
    }
}
?>
