<?php
require_once BASE_PATH . '/include/class.DbConnManager.php';
require_once BASE_PATH . '/include/class.SqlBuilder.php';
require_once BASE_PATH . '/include/class.SessionManager.php';
require_once BASE_PATH . '/include/class.AccountUtil.php';
require_once BASE_PATH . '/include/class.ContactUtil.php';
require_once BASE_PATH . '/include/class.EventUtil.php';
require_once BASE_PATH . '/include/class.UserUtil.php';
require_once BASE_PATH . '/include/class.JetDateTime.php';
require_once BASE_PATH . '/include/lib.date.php';
require_once BASE_PATH . '/slipstream/class.Note.php';

class ControllerEventAlert extends Controller {

    public function index() {

		if (!$this->user->isLogin()) {
			$this->response->redirect('index.php?route=common/login');
		}

		$data['error'] = $this->error;

        $time = time();
        $datetime = new DateTime('@' . $time, new DateTimeZone('UTC'));
        $now = $datetime->format('Y-m-d G:i:s.000');

        $sql = "
            SELECT * FROM Alert
            JOIN AlertRecipient ON Alert.AlertID = AlertRecipient.AlertID
            WHERE
            PersonID = ? AND
            Active = 1 AND
            (DelayUntil <= ? OR DelayUntil IS NULL) AND
            (SnoozeUntil <= ? OR SnoozeUntil IS NULL)";

        //$sql = "SELECT top 3 * FROM Alert JOIN AlertRecipient ON Alert.AlertID = AlertRecipient.AlertID WHERE PersonID = ? AND Active = 0";

        $params[] = array(DTYPE_INT, $_SESSION['USER']['USERID']);
        $params[] = array(DTYPE_STRING, $now);
        $params[] = array(DTYPE_STRING, $now);

        $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
        $results = DbConnManager::GetDb('mpower')->Exec($sql);

        //echo '<pre>'; print_r($results); echo '</pre>'; exit;

        $output = '';
        $count = count($results);

        $json = '';
        if($count > 0){

            $alerts = array();

            for($i=0;$i<$count;$i++){

                $date= $results[$i]['CreatedDate'];
                $date = str_replace(':000','',$date);
                $date = str_replace('.000','',$date);
                $results[$i]['CreatedDate'] = $date;

                $noteSql = "SELECT Subject, NoteText, ObjectType, ObjectReferer, NoteSpecialType FROM Notes WHERE NoteID = ?";

                $noteParams = array(DTYPE_INT, $results[$i]['RecordID']);
                $noteSql = SqlBuilder()->LoadSql($noteSql)->BuildSql($noteParams);
                $noteResult = DbConnManager::GetDb('mpower')->GetOne($noteSql);
                $fullName = UserUtil::getFullName($results[$i]['CreatedBy']);

                $results[$i]['NoteText'] = $noteResult['NoteText'];
                $results[$i]['Subject'] = $noteResult['Subject'];
                $results[$i]['FullName'] = $fullName;

                $note = new Note();
                $results[$i]['Type'] = $note->getNoteType($noteResult['ObjectType'], $noteResult['NoteSpecialType']);

                $referrer = $noteResult['ObjectReferer'];

                switch($noteResult['ObjectType']){

                    case NOTETYPE_COMPANY:
                        $results[$i]['Contact'] = '';
                        $results[$i]['Account'] = AccountUtil::getAccountName($referrer);
                        break;
                    case NOTETYPE_CONTACT:
                        $contact = ContactUtil::getContact($referrer);
                        $results[$i]['Contact'] = $contact['Text01'] . ' ' . $contact['Text02'];
                        $results[$i]['Account'] = AccountUtil::getAccountName($contact['AccountID']);
                        break;
                    case NOTETYPE_EVENT:
                        $contact = EventUtil::getEventContact($referrer);
                        $results[$i]['Contact'] = $contact['Text01'] . ' ' . $contact['Text02'];
                        $results[$i]['Account'] = AccountUtil::getAccountName($contact['AccountID']);
                        break;
                }
                $previous = array();
                if ( isset($results[$i-1]['AlertRecipientID']) ) {
                    $previous = $results[$i-1]['AlertRecipientID'];
                }

                $next = array();
                if ( isset($results[$i+1]['AlertRecipientID']) ) {
                    $next = $results[$i+1]['AlertRecipientID'];
                }

                $results[$i]['previous'] = $previous;
                $results[$i]['next'] = $next;

                $alerts[] = $results[$i];
            }

            $data['alert'] = $alerts[0];
        }
        if ( $count > 0 ) {
            $template = '/event/alert.tpl';
            $output = $this->load->view($template, $data);
        }
        echo $output;
        /*
        $json = json_encode(array('count' => $count, 'output' => $output));
        echo $json;
        */

    }

    public function snoozeOptions(){
        $template = '/event/snoozeOptions.tpl';
        $output = $this->load->view($template, array());
        echo $output;
    }

    public function dismissAlert() {
        $alertRecipientId = strip_tags($_POST['id']);

        if($alertRecipientId){
            $sql = "UPDATE AlertRecipient SET Active = 0 WHERE AlertRecipientID = ?";
            $params = array(DTYPE_INT, $alertRecipientId);
            $sql = SqlBuilder()->LoadSql($sql)->BuildSql($params);
            DbConnManager::GetDb('mpower')->Exec($sql);
        }

        echo 'ok';
    }

    public function snoozeAlert(){
        $snooze = $this->request->post['snooze'];
        $seconds = $snooze * 60;

        $time = time();
        $time += $seconds;

        $datetime = new DateTime('@' . $time, new DateTimeZone('UTC'));

        $snoozeUntil = $datetime->format('Y-m-d G:i:s.000');
        $alertRecipientId = strip_tags($this->request->post['id']);

        if($alertRecipientId){
            $sql = "UPDATE AlertRecipient SET SnoozeUntil = ? WHERE AlertRecipientID = ?";
            $params[] = array(DTYPE_STRING, $snoozeUntil);
            $params[] = array(DTYPE_INT, $alertRecipientId);
            $sql = SqlBuilder()->LoadSql($sql)->BuildSqlParam($params);
            DbConnManager::GetDb('mpower')->Exec($sql);
        }

        echo 'ok';


    }
}
?>
