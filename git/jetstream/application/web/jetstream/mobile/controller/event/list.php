<?php  
class ControllerEventList extends Controller {

    public function uncompleted() {
        $this->index('uncompleted');
    }

	public function index( $scope = '' ) {
		
		$data['error'] = $this->error;

		$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');
	
		if (!$this->user->isLogin()) {
			$this->response->redirect('index.php?route=common/login');
		}

		$this->load->model('event/base');
		$companyid = $this->session->data['USER']['COMPANYID'];

	    $req = array();
        if ( $scope == 'uncompleted' ) {
           $req['uncompleted'] = true; 
        }
		$aEvent = $this->model_event_base->getList($req);
		$data['events'] = $aEvent['event'];
		$data['tasks'] = $aEvent['task'];
        
        //echo '<pre>'; print_r($aEvent); echo '</pre>'; exit;
		$hasEvent = (count($aEvent['event']) > 0) ? true : false ;
		$data['hasEvent'] = $hasEvent;
		$hasTask = (count($aEvent['task']) > 0) ? true : false ;
		$data['hasTask'] = $hasTask;
	
		/* TODO : if eventid exist */
		/*
		if (isset($this->request->get['eventid'])) {
			$eventid = $this->request->get['eventid'];
			$aEvent = $this->model_event_base->getEventByid($eventid);
			$data['event'] = $aEvent['event'];
			$data['notes'] = $aEvent['notes'];
		}
		*/

        //echo '<pre>'; print_r($this->pageid); echo '</pre>'; exit;
        $data['pageid'] = $this->pageid;

		$template = '/event/list.tpl';
		$this->response->setOutput($this->load->view($template, $data));
	}

    /* TODO. Need to change to use core method */
	public function closeEvent($eventid) {
			
		$this->load->model('event/base');
		if ( $this->model_event_base->closeEvent($eventid) ) {
            return true;
		} else {
            return false;
		}
	}

    public function addNote(){

        $post = $this->request->post;
        $note_title = (!empty($post['note_title'])) ? $post['note_title'] : '' ;
        $eventid = (!empty($post['eventid'])) ? $post['eventid'] : '' ;
        $contactid = (!empty($post['contactid'])) ? $post['contactid'] : '' ;
        $accountid = (!empty($post['accountid'])) ? $post['accountid'] : '' ;
        $mode = (!empty($post['mode'])) ? $post['mode'] : 'event' ;
        $note = (!empty($post['note'])) ? $post['note'] : 'note' ;
        $CheckComplete = (!empty($post['CheckComplete'])) ? true : false ;
        $CheckNewEvent = (!empty($post['CheckNewEvent'])) ? true : false ;

        if ( $CheckComplete == true ) {
            if ( !$this->closeEvent($eventid) ) {
      	        echo json_encode(array('code'=>'500'));
                return;
            }
        }

    	$this->load->model('notes/base');
        if ( $note_title != '' && $note != '' ) {
      	    if($this->model_notes_base->addNote($post)){
           	    echo json_encode(array('code'=>'200'));
                return;
      	    }else{
      	    	echo json_encode(array('code'=>500));
      	    }
        }

        echo json_encode(array('code'=>'200'));

    }

	/* below from contact */

	public function getAccountEvent() {
		
		$query = isset($this->request->get['q']) ?  $this->request->get['q'] : '';
		if ('' == $query)	return false;

		$query = str_replace("'", "''", str_replace("\\\"", "\"", str_replace("\\'","'", strtolower($query))));

		/* TODO : limitAccess Control
		 * $sql_check_assigned_user = '';
			if(isset($_SESSION['company_obj']['LimitAccessToUnassignedUsers']) && ($_SESSION['company_obj']['LimitAccessToUnassignedUsers'] == 1)) {
				$sql_check_assigned_user .= ' INNER JOIN PeopleEvent AS PC ON C.EventID = PC.EventID AND PC.AssignedTo = 1 AND PC.PersonID = '.$_SESSION['USER']['USERID'];
			}
		 */
		$this->load->model('event/base');
		$aCompanyEvent = $this->model_event_base->getAccountEvent($query);

		echo json_encode($aCompanyEvent);
	}

	public function getEventById() {
		$eventid = isset($this->request->post['id']) ?  $this->request->post['id'] : '';
		$this->load->model('event/base');
		$aEvent = $this->model_event_base->getEventById($eventid);
		echo json_encode($aEvent);
	}

	public function getAccountById() {
		$accountid = isset($this->request->post['id']) ?  $this->request->post['id'] : '570670';

		$this->load->model('event/base');
		$aAccount = $this->model_event_base->getAccountById($accountid);
		echo json_encode($aAccount);
	}

}
