<?php  
class ControllerContactList extends Controller {

	public function index() {
		
		$data['error'] = $this->error;

		$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');
	
		if (!$this->user->isLogin()) {
			$this->response->redirect('index.php?route=common/login');
		}

		$this->load->model('contact/base');
		$companyid = $this->session->data['USER']['COMPANYID'];

		/* test code
		$request = array(
			'CompanyId' => $companyid
		);
		$aContact = $this->model_contact_base->getList($request);
		$data['contacts'] = $aContact;
		*/
	
		/* TODO : if contactid exist */
		if (isset($this->request->get['contactid'])) {
			$contactid = $this->request->get['contactid'];
			$aContact = $this->model_contact_base->getContactByid($contactid);
			$data['contact'] = $aContact['contact'];
			$data['notes'] = $aContact['notes'];
			$data['mode'] = 'contact';
		}

        /* not to used yet to retain selected account */
        /*
		if (isset($this->request->get['accountid'])) {
			$contactid = $this->request->get['accountid'];
			$aContact = $this->model_contact_base->getAccountByid($contactid);
			$data['contact'] = $aContact['account'];
			$data['notes'] = $aContact['notes'];
			$data['mode'] = 'account';
		}
        */

        //echo '<pre>'; print_r($aContact); echo '</pre>'; exit;

		$template = '/contact/list.tpl';
		$this->response->setOutput($this->load->view($template, $data));
	}

	public function getAccountContact() {
		
		$query = isset($this->request->get['q']) ?  $this->request->get['q'] : '';
		if ('' == $query)	return false;

		$query = str_replace("'", "''", str_replace("\\\"", "\"", str_replace("\\'","'", strtolower($query))));

		/* TODO : limitAccess Control
		 * $sql_check_assigned_user = '';
			if(isset($_SESSION['company_obj']['LimitAccessToUnassignedUsers']) && ($_SESSION['company_obj']['LimitAccessToUnassignedUsers'] == 1)) {
				$sql_check_assigned_user .= ' INNER JOIN PeopleContact AS PC ON C.ContactID = PC.ContactID AND PC.AssignedTo = 1 AND PC.PersonID = '.$_SESSION['USER']['USERID'];
			}
		 */
		$this->load->model('contact/base');
		$aCompanyContact = $this->model_contact_base->getAccountContact($query);

		echo json_encode($aCompanyContact);
	}

	public function getContactById() {
		$contactid = isset($this->request->post['id']) ?  $this->request->post['id'] : '';
		$this->load->model('contact/base');
		$aContact = $this->model_contact_base->getContactById($contactid);
		echo json_encode($aContact);
	}

	public function getAccountById() {
		$accountid = isset($this->request->post['id']) ?  $this->request->post['id'] : '570670';

		$this->load->model('contact/base');
		$aAccount = $this->model_contact_base->getAccountById($accountid);
		echo json_encode($aAccount);
	}

    public function addNote(){
    	$this->load->model('notes/base');
      	if($this->model_notes_base->addNote($this->request->post)){
      		echo json_encode(array('code'=>200));
      	}else{
      		echo json_encode(array('code'=>500));
      	}
    }
}
