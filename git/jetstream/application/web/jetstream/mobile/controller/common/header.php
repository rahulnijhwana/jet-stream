<?php   
class ControllerCommonHeader extends Controller {
	public function index() {
		$data['login'] = false;
		if ($this->user->isLogin()) {
			$data['login'] = true;
		}
		$data['pageid'] = $this->pageid;
		$data['pageid'] = $this->pageid;

		$template = '/common/header.tpl';
		return $this->load->view($template, $data);
	} 	
}