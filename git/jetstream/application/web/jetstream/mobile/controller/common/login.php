<?php  
class ControllerCommonLogin extends Controller { 

	public function index() {
		$data['error'] = $this->error;
		$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');
		$data['lnk_action'] = 'index.php?route=common/login/login';

		$company = isset($this->request->get['company']) ? $this->request->get['company'] : '';
        $data['company'] = $company;

        $directoryName = isset($_GET['company']) ? $_GET['company'] : ''; 

        if ( $directoryName != '' ) {
            $data['fullUrl'] = 'http://www.mpasa.com/' . $directoryName;
        } else {
            $data['fullUrl'] = 'http://www.mpasa.com/jetstream/';
        }
		$template = '/common/login.tpl';
	
		$this->response->setOutput($this->load->view($template, $data));
	}

	public function login() { 
	
		$company = isset($this->request->post['company']) ? $this->request->post['company'] : '';
		$username = isset($this->request->post['username']) ? $this->request->post['username'] : '';
		$password = isset($this->request->post['password']) ? $this->request->post['password'] : '';

		if ('' !== $company && '' !== $username && '' !== $password) {
			if ($this->user->login($company, $username, $password)) {
				$this->response->redirect('index.php?route=common/home');
			} else {
				$this->error = 'Wrong Login Information';
				$this->index();
				//$this->response->redirect('index.php?route=common/login');
			}
		} else {
			//$this->error = 'Empty Login Information';
			$this->index();
			//$this->response->redirect('index.php?route=common/login');
		}
		
	}	

	public function logout() {
		if($this->user->isLogin()) {
			$this->user->logout();
			$this->response->redirect('index.php?route=common/home');
		}
	}
}
