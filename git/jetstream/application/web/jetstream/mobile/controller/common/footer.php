<?php   
class ControllerCommonFooter extends Controller {
    public function index() {
        $data = array();
        $template = '/common/footer.tpl';
        return $this->load->view($template, $data);
    } 
}
