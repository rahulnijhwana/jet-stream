<?php  
class ControllerCommonHome extends Controller {
	public function index() {

		$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');
	
		if (!$this->user->isLogin()) {
			$this->response->redirect('index.php?route=common/login');
		} else {
			$this->response->redirect('index.php?route=contact/list');
		}

		$template = '/common/home.tpl';
		$this->response->setOutput($this->load->view($template, $data));
	}
}
