<?php

/* TODO conflict with web's class */
class Util {
	
	/* methods for user */
	function encode($originalStr) {
		$encodedStr = $originalStr;
		$num = mt_rand(1, 5);
		for($i = 1; $i <= $num; $i++) {
			$encodedStr = base64_encode($encodedStr);
		}
		$seed_array = array('M', 'P', 'O', 'W', 'E', 'R');
		$encodedStr = $encodedStr . "+" . $seed_array[$num];
		$encodedStr = base64_encode($encodedStr);
		return $encodedStr;
	}

	function decode($decodedStr) {
		$seed_array = array('M', 'P', 'O', 'W', 'E', 'R');
		$decoded = base64_decode($decodedStr);
		@list ($decoded, $letter) = split("\+", $decoded);
		for($i = 0; $i < count($seed_array); $i++) {
			if ($seed_array[$i] == $letter) break;
		}
		for($j = 1; $j <= $i; $j++) {
			$decoded = base64_decode($decoded);
		}
		return $decoded;
	}

	function encryptString($str) {
		$objUtil = new Util();
		$encryptedStr = sha1($str);
		$encodedStr = $objUtil->encode($encryptedStr);
		return $encodedStr;
	}

	function decryptString($encryptedStr) {
		$objUtil = new Util();
		$decryptedStr = $objUtil->decode($encryptedStr);
		return $decryptedStr;
	}
}
