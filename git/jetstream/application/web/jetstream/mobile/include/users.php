<?php
require_once '../include/class.fieldsMap.php';

class Users {

    public $userid;
    public $companyid;

    private $db;
    private $session;
    private $request;


    public function __construct($registry) {
        $this->db = $registry->get('db');
        $this->request = $registry->get('request');
        $this->session = $registry->get('session');
        if (isset($this->session->data['USER']['USERID'])) {
            $this->userid = $this->session->data['USER']['USERID'];
        }
        if (isset($this->session->data['USER']['COMPANYID'])) {
            $this->companyid = $this->session->data['USER']['COMPANYID'];
        }
    }

    /*
     * @param $directoryName : company directory name
     * @param $username
     * @param @password
     */
    public function login($directoryName, $username, $password) {

        $aCompany = $this->getCompanyInfo($directoryName);

        if (count($aCompany) == 0)  return false;
        $companyId = $aCompany[0]['CompanyID'];

        $sql = 'SELECT l.password, l.passwordz, p.supervisorid, l.personID, p.FirstName, p.LastName, p.Timezone
                  FROM logins l
                  JOIN people p ON l.PersonID = p.PersonID
                 WHERE l.CompanyID = ? AND l.UserID = ?';
        $params = array();
        $params[] = array(DTYPE_INT, $companyId);
        $params[] = array(DTYPE_STRING, $username);

        $aUser = $this->db->query($sql, $params);
        $aUser = $aUser[0];

        $crypted = crypt($password, $aUser['password']);
        if ( ($aUser['supervisorid'] == -3 ) ? ($aUser['passwordz'] != $password) : !strstr($crypted,$aUser['password'])) {
            /*
            echo '<pre>'; print_r('password in db :' . $aUser['password']); echo '</pre>';
            $crypt = crypt($password, $aUser['password']);
            echo '<pre>'; print_r('crypted : ' . $crypt); echo '</pre>';
            */
            return false;
        }

        $this->session->data['USER']['USERNAME'] = $username;
        $this->session->data['USER']['USERID'] =  $aUser['personID'];
        $this->session->data['USER']['FIRSTNAME'] = $aUser['FirstName'];
        $this->session->data['USER']['LASTNAME'] = $aUser['LastName'];
        $this->session->data['USER']['COMPANYID'] = $companyId;
        $this->session->data['USER']['COMPANYNAME'] = $directoryName;
        $this->session->data['USER']['TIMEZONE'] = $aUser['Timezone'];
        $this->session->data['USER']['BROWSER_TIMEZONE'] = isset($this->request->post['browser_timezone']) ? $this->request->post['browser_timezone'] : DEFAULT_TIMEZONE;

        $this->session->data['company_obj']['CompanyID'] = $companyId;
        $this->session->data['company_id'] = $companyId;

            $field_map_obj = new fieldsMap();
            $field_map_obj->companyId = $companyId;
            $this->session->data['USER']['ACCOUNTMAP'] = $field_map_obj->getAccountMapFieldsFromDB();
            $this->session->data['USER']['CONTACTMAP'] = $field_map_obj->getContactMapFieldsFromDB();
            
            /*
            $_SESSION['account_name'] = $field_map_obj->getAccountNameField();
            $_SESSION['contact_first_name'] = $field_map_obj->getContactFirstNameField();
            $_SESSION['contact_last_name'] = $field_map_obj->getContactLastNameField();
            */

        /*
        $_SESSION['USER']['SUPERVISORID'] = $login->SupervisorID;
        $_SESSION['USER']['ISSALESPERSON'] = $login->IsSalesPerson;
        $_SESSION['USER']['LEVEL'] = $login->Level;
        $_SESSION['USER']['UTC_UPDATE'] = $login->UtcUpdate;
        $_SESSION['USER']['ALLOW_UPLOADS'] = $login->AllowUploads;
        $_SESSION['USER']['ALLOW_EXPORT'] = $login->AllowExport;
        $_SESSION['USER']['MAILTO_ACTION'] = $login->MailtoAction;
        $_SESSION['USER']['IMAP_ENABLED'] = ImapSettings::isEnabled();
        $_SESSION['USER']['SPECIAL_PERMISSIONS_ID'] = $login->SpecialPermissionsID;
        */

        return true;

    }

    public function getCompanyInfo ($directoryName) {
        $sql = 'Select * From company Where directoryname = ? ';
        $params = array();
        $params[] = array(DTYPE_STRING, $directoryName);
        $aCompany = $this->db->query($sql, $params);
        return $aCompany;
    }

    public function isLogin() {
        return ('' != $this->userid) ? true : false;
    }

    public function logout() {
        $this->session->data = array();
        $this->userid = '';
        return true;
    }

    /*
    public function getCompanyId() {
        return ('' != $this->companyid) ? true : false;
    }
    */

}