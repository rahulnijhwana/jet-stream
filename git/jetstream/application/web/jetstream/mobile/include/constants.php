<?php
// HTTP
define('HTTP_SERVER', '');

// HTTPS
define('HTTPS_SERVER', '');

// DIR
define('DOC_ROOT', $_SERVER['DOCUMENT_ROOT']);
define('DIR_APPLICATION', DOC_ROOT . '/mobile/');
define('DIR_LIBRARY', DOC_ROOT . '/mobile/include/');
define('DIR_VIEW', DOC_ROOT . '/mobile/view/');

// for jetstream classes
define('BASE_PATH', '../');

define('DEFAULT_TIMEZONE', 'America/Chicago');

/*
# TYPE OF NOTE
define('NOTETYPE_COMPANY', 1);
define('NOTETYPE_CONTACT', 2);
define('NOTETYPE_EVENT', 3);
define('NOTETYPE_OPPORTUNITY', 4);
define('NOTETYPE_TASK', 5);
*/