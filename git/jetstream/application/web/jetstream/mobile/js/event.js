var people_list;
var type_list;
var webpath = '../../www';
var prev_start = '';

$(document).ready(function() {

	var sPath = window.location.pathname;
	var sPage = sPath.substring(sPath.lastIndexOf('/') + 1);

	if(sPage == 'edit_opp2.php'){
		$("#event_alert_wrapper").hide();
	}


	try {
		showNoteAlertHelp();
	}
	catch(err) {}

	$(".expand_event_alert").live('click', function(e){
        var that = $(e.target),
            wrapper = that.closest('#event_alert_wrapper'),
            form = that.closest('form');

        if ( form.find('input[name="ContactID"]').val() == '' ) {
            return false;
        }

		wrapper.find("#add_event_alert").slideToggle();

		if( wrapper.find("#CheckPrivate").attr('disabled') ){
			wrapper.find("#CheckPrivate").removeAttr('disabled');
			wrapper.find("#label_event_private").css('color', '#5C80A5');
    		$(this).attr('value', 'Add Alert');
    		$.scrollTo("#overlay", {duration : 2000});
    		wrapper.find("#add_event_alert_hidden").attr('value', '');
		}
        else {
        	$.scrollTo("#event_bottom", {duration : 1000});
        	wrapper.find("#CheckPrivate").attr('disabled', 'disabled');
			wrapper.find("#label_event_private").css('color', '#cacaca');
    		$(this).attr('value', 'Remove Alert');
    		wrapper.find("#add_event_alert_hidden").attr('value', '1');
        }
		
	});

	$("#show_event_detail").live('click', function(){
		$(".event_form").slideToggle();
	});
	
	$(".event_level").live('click', function(){
		
		id = ($(this).attr('id'));
		
		if($("#level_event_checked").attr('value') != 1){
			$('.'+id).attr('checked', true);
			$("#level_event_checked").attr('value', 1);
		}
		else {
			$('.'+id).removeAttr('checked');
			$("#level_event_checked").attr('value', 0);
		}
	});

	$("#select_event_level").live('click', function(){
		$("#level_event_msg").show();
		$(".header").fadeOut();
		$(".header").fadeIn();
		$(".header").fadeOut();
		$(".header").fadeIn();
	});

	$("#select_event_all").live('click', function(){
		toggleEventAllSelected();
	});

	$("#select_event_assigned").live('click', function(){
		toggleEventAssignedSelected();
	});

	$("#send_event_immediately_no").live('click', function(){
		$(".ui-datepicker-trigger").attr('title', 'Select a Date');
		$(".ui-datepicker-trigger").show();
		$("#delay_event_until_day" ).datepicker('enable');
		$("#delay_event_until_day" ).attr('value','');
		$("#event_hours").removeAttr('disabled');
		$("#event_minutes").removeAttr('disabled');
		$("#event_periods").removeAttr('disabled');
	});

	
	$("#send_event_immediately_yes").live('click', function(){
		$("#delay_event_until_day").attr('value', '');
		$("#delay_event_until_day").datepicker('disable');
		$("#event_hours").attr('disabled', 'disabled');
		$("#event_minutes").attr('disabled', 'disabled');
		$("#event_periods").attr('disabled', 'disabled');
		$("#spn_event_delay_until_error").text('');
		
	});

	$("#EventType").change(function(){
		$("#err_EventType").hide();
	});

	$("#standard_event_alert").change(function(){
		$("#err_Delivery").hide();
	});

	$("#email_event_alert").change(function(){
		$("#err_Delivery").hide();
	});

	$("#delay_event_until_day").live('change', function(){
		$("#err_Delay").hide();
	});


	
	var target = 0;
	
	var start_date = $("input[name='StartDate']").val();
	/*
	if (start_date != '') {
		prev_start = $.datepicker.parseDate('m/d/y', start_date);
	}
	*/

	try {
		// Get the selected user from edit-opportunity 
		target = ssSalesPerson.getSelectedVal();				
	}
	catch(err){}
			
	$("#eventMiniCalendar").load("{/literal}{$smarty.const.WEB_PATH}{literal}/ajax/ajax.EventMiniCalendar.php?target=" + target);

	//DoChangeRepeat($("select[name='Repeat']").val());
	RemoveAttendee($("select[name='creator']").val());
	UpdateAttendeeSelect();


	/*
	$("input[name='StartDate']").datepicker({
		showOn :"both",
		buttonImage : webpath + "/images/calendar.gif",
		buttonImageOnly :true,
		dateFormat: 'm/d/y',
		onSelect: function(dateText, inst) {
			var start_date = $(this);
			var start_parsed = $.datepicker.parseDate('m/d/y', start_date.val());

			
			var end_date = $("input[name='EndDate']");
			var repeat_end = $("input[name='RepeatEndDate']");
			if (end_date.val() == '') {
				end_date.val(start_date.val());
			} else if (prev_start != '') {
				var end_parsed = $.datepicker.parseDate('m/d/y', end_date.val());
				var new_end = new Date();
				new_end.setTime(end_parsed.getTime() + start_parsed.getTime() - prev_start.getTime() + 3600000);
				end_date.val($.datepicker.formatDate('m/d/y', new_end));
			} 
			if (repeat_end.val() != undefined && start_parsed > $.datepicker.parseDate('m/d/y', repeat_end.val())) {
				repeat_end.val(start_date.val());
			}
			prev_start = $.datepicker.parseDate('m/d/y', start_date.val());
			end_date.datepicker('option', 'minDate', start_parsed);
			repeat_end.datepicker('option', 'minDate', start_parsed);
		}

	});

	$("input[name='EndDate']").datepicker({
		showOn :"both",
		buttonImage : webpath + "/images/calendar.gif",
		buttonImageOnly :true,
		dateFormat: 'm/d/y'
	});

	$("input[name='repeat_enddate']").datepicker({
		showOn :"both",
		buttonImage : webpath + "/images/calendar.gif",
		buttonImageOnly :true,
		dateFormat: 'm/d/y'
	});
	*/

	$(".ui-datepicker-trigger").attr('title', 'Select a Date');
	/*
	$(".ui-datepicker-trigger").css( "width", "24px");
	$(".ui-datepicker-trigger").css( "height", "23px");
	*/
	if ($("select[name='EventType']").val() != '') {
		DoChangeType($("select[name='EventType']").val());
	}
	
	//$("#delay_event_until_day" ).datepicker('disable');
	$('#save-event').live('click', function(e){
		var form = $('#event-form'),
			oSubject = form.find('input[name="Subject"]'),
			oEventType = form.find('select[name="EventType"]'),
			oStartDate = form.find('input[name="StartDate"]'),
			oEndDate = form.find('input[name="EndDate"]'),
			oStartTime = form.find('select[name="StartTime"]'),
			oEndTime = form.find('select[name="EndTime"]');

		if ( oSubject.val() == '' ) {
			alert('Input Subject');
			return false;
		}

		if ( oEventType.val() == '' ) {
			alert('Choose EventType');
			return false;
		}

		var sDate = new Date(oStartDate.val());
		var eDate = new Date(oEndDate.val());
		if ( sDate.getTime() > eDate.getTime() ) {
			alert('"Start Date" should be earlier than "End Date"');
			return false;
		}

		var sTime = oStartDate.val() + ' ' + oStartTime.val();
		var eTime = oStartDate.val() + ' ' + oEndTime.val();
		if ( Date.parse(sTime) > Date.parse(eTime) ) {
			alert('"Start Time" should be earlier than "End Time"');
			return false;
		}

		form.submit();
		e.preventDefault();
	})

    $('#StartDate').on('change', function(e) {
        var choosed = $(this).val();
        console.log(choosed);
        $('#EndDate').val(choosed);
    });

});

function toggleEventAllSelected(){
	
	var current_state = $("#all_event_checked").attr('value');

	if(current_state != 1){
		$(".all_users").attr('checked', true);
		$("#all_event_checked").attr('value', 1);
	}
	else {
		resetEventSelected();
	}
}

function toggleEventAssignedSelected(){

	var current_state = $("#assigned_event_checked").attr('value');
	
	if(current_state != 1){
		$(".assigned_users").attr('checked', true);
		$("#assigned_event_checked").attr('value', 1);
	}
	else {
		resetEventSelected();	
	}
}



function resetEventSelected(){
	
	$(".all_users").removeAttr('checked');
	$("#all_event_checked").attr('value', 0);
	
	$(".assigned_users").removeAttr('checked');
	$("#assigned_event_checked").attr('value', 0);
}




function AddAttendee() {
	var to_add = $("select[name='add_attendee']").find(':selected').val(),
        oSelectedText = $('#add_attendee-button').find('span');
    oSelectedText.html('&nbsp;');
	if (to_add != "" && to_add != "All") {
		//$("#attendees").append('<div class="ui-body ui-corner-all" id="attendee_' + to_add + '">' + people_list[to_add] + '&nbsp;&nbsp;&nbsp;<a data-icon="delete" style="font-weight:bold;color:red;" onClick="RemoveAttendee(' + to_add + ')">X</a></div>');
		$("#attendees").append('<div class="" id="attendee_' + to_add + '">' + '<button class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-delete" onClick="RemoveAttendee(' + to_add + ')">' +  people_list[to_add] + '</button></div>');
		UpdateAttendeeSelect();
	}else if (to_add == "All") {
		$("#add_attendee option:not(option:first, option:last)").each(function(){
		//$("#attendees").append('<div class="ui-body ui-corner-all" id="attendee_' + $(this).val() + '">' + people_list[$(this).val()] + '&nbsp;&nbsp;&nbsp;<a style="font-weight:bold;color:red;" onClick="RemoveAttendee(' + $(this).val()+ ')">X</a></div>');		
		$("#attendees").append('<div class="" id="attendee_' + $(this).val() + '"><button class="ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-delete" onClick="RemoveAttendee(' + $(this).val()+ ')">' + people_list[$(this).val()] + '</button></div>');		
		});	
		UpdateAttendeeSelect();
	}
}

function RemoveAttendee(attendee_id) {
	$("#attendee_" + attendee_id).remove();
	UpdateAttendeeSelect();
}

function UpdateAttendeeSelect() {
	var attendee_select = $("select[name='add_attendee']");
	attendee_select.children().remove();
	attendee_select.append(new Option('', '', true));

	// Get the list of the current attendees
	current_array = new Array();
	current_count = 0;
	if ($("select[name='creator']")) {
		current_array[current_count] = $("input[name='creator']").val();
		current_count++;
	}
	$("div[id^='attendee_']").each(function() {
		current_array[current_count] = this.id.substring(9);
		current_count++;
	});
	$("input[name='attendee_list']").val(JSON.stringify(current_array));
	for (var person in people_list) {
		var found = false;
		for(var i = 0; i < current_array.length; i++) {
			if(current_array[i] == person) {
				var found = true;
			}
		}
		if (found) continue;
		if(person > 0)
		attendee_select.append("<option value='" + person + "'>" + people_list[person] + "</option>");
	}
	attendee_select.append("<option value='All'>" + "All" + "</option>");
	
}

function DoChangeRepeat(value) {
	var field = '',
		i = 1;
	$('#event_repeat').show();
	for( i ; i <= 5 ; i++ ) {
		field = 'repeat_' + i;
		if (i == value) {
			$('#' + field).show();
		} else {
			$('#' + field).hide();
		}
	}

    if ( value == 0 ) {
        $('#event_repeat').hide();
    }
}

function DoChangeType(value, that) {
    var oEventType = $('select#EventType'),
        element = $(that).find('option:selected'),
        type = element.attr('type'); 
	if (type == 'TASK') {
		$('#event_datetime').hide();
		$('#event_repeat_wrap').hide();
		$('#primary-attendee-wrap').hide();
		$('#attendee_section').hide();
		//$("div[id^='attendee_']").remove();
		//UpdateAttendeeSelect();
		$("#priority_wrap").show();
		$("#enddate_wrap").hide();
		$("#startdate_wrap span#label-prefix").text('Date');
	}
	else {
		$('#event_datetime').show();
		$('#event_repeat_wrap').show();
		$('#primary-attendee-wrap').show();
		$('#attendee_section').show();
		$("#priority_wrap").hide();
		$("#enddate_wrap").show();
		$("#startdate_wrap span#label-prefix").text('Start Date');
	}
}
