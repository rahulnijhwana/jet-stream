$(function(){
	
    var browser_timezone = getTimezoneName();

	$('input[name="browser_timezone"]').val(browser_timezone);

    var picker = $( ".mobipicker", this );
    picker.mobipick();
    //$('.mobipicker').mobipicker();

    $('#see-full-version').bind('click',function(e){
        e.preventDefault();
        var that = $(e.target),
            url = that.data('href');
        location.href = url;
    })

    $('#login-submit').bind('click',function(e){
		e.preventDefault();
		var form = $('#login-form'),
			oCompany = form.find('input[name="company"]'),
			oUsername = form.find('input[name="username"]'),
			oPassword = form.find('input[name="password"]');

		if (oCompany.val() == '') {
			showStatus('Input Username');
			//alert('Input Username');
			return false;
		}
		if (oUsername.val() == '') {
			showStatus('Input Username');
			//alert('Input Username');
			return false;
		}
		if (oPassword.val() == '') {
			showStatus('Input Password');
			//alert('Input Password');
			return false;
		}

		form.submit();
	})


	// contact autocomplete

	$.fn.drawNotes = function(notes) {
		var divContent = $('#contact-form div#content');
		var row = '<li data-role="fieldcontain" class="ui-field-contain note-wrap"></li>';
		$.each(notes, function(idx,note){
			// draw notes below note text box
			row += '<li data-role="fieldcontain" class="ui-field-contain note note-wrap">';
			row += '<table><tr><td class="head">' + note.Subject + ' (' + note.Creator + ')' + '</td></tr>';
			row += '<tr><td>' + note.Body + '</td></tr>';
			row += '<tr><td class="foot">' + note.TimeStamp +'</td></tr></table></li>';
		})
		divContent.append(row);
		return true;
	}
            
    $.fn.getAlertHtml = function(rec_type, rec_id, action) {
        var data = {
            'rec_type' : rec_type,
            'rec_id' : rec_id,
            'action' : action
        };
        $.ajax({
            type: 'POST',
            url: 'index.php?route=event/new/getAlertHtml',
            data: data,
            dataType: 'json',
            success: function(html){
                var oAlert = $('#add_event_alert'); 
                oAlert.html('').html(html);
            }

        });
    };

    $.fn.setContactDetailPanel = function(id) {
    	var phone = '';
        $.ajax({
            type: 'POST',
            url: 'index.php?route=contact/list/getContactById', 
            data: 'id=' + id,
            dataType: 'json',
            success: function(res){

            	var contact = res.contact;
            	var notes = res.notes;
                $('#contact-form').find('input[name="contactid"]').val(contact.ContactID);
                $('#contact-form').find('input[name="ContactID"]').val(contact.ContactID);
                $('#contact-form').find('input[name="companyname"]').val(contact.Account);

				$('#li-name').show();
                $('#contact-form').find('input[name="firstname"]').val(contact.firstname);
                $('#contact-form').find('input[name="lastname"]').val(contact.lastname);
                
				$('#li-website').hide();
                $('#li-email').show();
                $('#contact-form').find('input[name="email"]').val(contact.email);

				if (contact.direct_phone != '') {
					phone = contact.direct_phone;
                }

                if (contact.main_phone != '') {
					phone = contact.main_phone;
                }

                $('#contact-form').find('input[name="phone"]').val(phone);

				$('#contact-form').find('input[name="mode"]').val('contact');

                $('#contact-atc').val('');

                $('.note-wrap').remove();
                if (notes.length > 0) {
                	$.fn.drawNotes(notes);
                }

                $.fn.getAlertHtml('contact',id,'add');

            }
        });
    }

    $('#contact-form input[name="phone"]').on('click',function(e) {
        var that = $(this),
            phone = that.val();
        if ( phone != '' ) {
            var telUrl = 'tel:' + phone;
            window.location.href = telUrl;
        }
    });
	
    $('#contact-form input[name="website"]').on('click',function(e) {
        var that = $(this),
            website = that.val();
        if ( website != '' ) {
            window.location.href = website;
        }
    });
	
    $('#contact-form input[name="email"]').on('click',function(e) {
        var that = $(this),
            email = that.val();
        if ( email != '' ) {
            var url = 'mailto:' + email;
            window.location.href = url;
        }
    });
	 
	$.fn.setAccountDetailPanel = function(id) {
    	var phone = '';
        $.ajax({
            type: 'POST',
            url: 'index.php?route=contact/list/getAccountById', 
            data: 'id=' + id,
            dataType: 'json',
            success: function(res){

            	var account = res.account;
            	var notes = res.notes;
                $('#contact-form').find('input[name="contactid"]').val(account.AccountID);
                $('#contact-form').find('input[name="ContactID"]').val(account.AccountID);
                $('#contact-form').find('input[name="companyname"]').val(account.account_name);

                $('#li-name').hide();
                /*	
                $('#contact-form').find('input[name="firstname"]').val(account.firstname);
                $('#contact-form').find('input[name="lastname"]').val(account.lastname);
                $('#contact-form').find('input[name="email"]').val(account.email);
                */

				if (account.tollfree_phone != '') {
					phone = account.tollfree_phone;
                }

                if (account.main_phone != '') {
					phone = account.main_phone;
                }
                $('#contact-form').find('input[name="phone"]').val(phone);

                $('#li-email').hide();
                $('#li-website').show();
                $('#contact-form').find('input[name="website"]').val(account.website);

                $('#contact-form').find('input[name="mode"]').val('account');

                $('#contact-atc').val('');

                $('.note-wrap').remove();
                if (notes.length > 0) {
                	$.fn.drawNotes(notes);
                }

                $.fn.getAlertHtml('account',id,'add');

            }
        });
    }


    $("#contact-atc").on( "listviewbeforefilter", function ( e, data ) {

        var $ul = $( this ),
            $input = $( data.input ),
            value = $input.val(),
            html = "";
        $ul.html( "" );
        if ( value && value.length > 0 ) {
            $ul.html( "<li><div class='ui-loader'><span class='ui-icon ui-icon-loading'></span></div></li>" );
            $ul.listview( "refresh" );
            $.ajax({
                //url: "index.php?route=contact/list/getATCContacts",
                url: "index.php?route=contact/list/getAccountContact",
                dataType: "json",
                crossDomain: true,
                data: {
                    q: $input.val()
                }
            })
            .then( function ( response ) {

                $.each( response, function ( i, item ) {
                	
                	var isContact = false;
                	var isCompany = false;
                	//alert(item.ContactID);
                	if(item.ContactID != null) {
                		isContact = true;
                	}
					if(item.AccountID != null) {
                		isCompany = true;
                	}

                	if (isContact == true) {
                		if (isCompany == true) {
                    		html += "<li class='atc-list' data-id='"+ item.ContactID +"' data-icon='user' data-contact='true'><a href='#'>" + item.ContactName ;
                    		html += " (" + item.AccountName + ")</a></li>";
                    	} else {
                    		html += "<li class='atc-list' data-id='"+ item.ContactID +"' data-icon='user' data-contact='true'><a href='#'>" + item.ContactName + "</a></li>";
                    	}
                    } else {
                    	html += "<li class='atc-list' data-id='"+ item.AccountID +"' data-icon='grid' data-contact='false'><a href='#' style='color:#df6529'>" + item.AccountName + "</a></li>";
                    }
                });
                $ul.html( html );
                $ul.listview( "refresh" );
                $ul.trigger( "updatelayout");
                $('.atc-list').bind('click',function(){
                    var that = $(this);
                    var id = that.data('id');
                    var isContact = that.data('contact');
                    //$ul.html('');
                    //$input.val('');
                    $('.ui-input-clear').click();

                    if (isContact == true) {
                    	$.fn.setContactDetailPanel(id);
                    } else {
                    	$.fn.setAccountDetailPanel(id);
                    }

                })
            });
        }
    });

    // just add note for contacts
	$('#contact-submit').on('click', function(e){
        e.preventDefault();
        var contact_form = $('#contact-form');
        var oMode = $('input[name="mode"]');

        var oContactId = $('input[name="contactid"]');
        if (oContactId.val() == '') {
            showStatus('Choose Contact first');
            return false;
        }

		var oNoteTitle = $('input[name="note_title"]');
        if (oNoteTitle.val() == '') {
        	showStatus('Input Note subject');
            return false;
        }

		var oNoteBody = $('textarea[name="note"]');
        if (oNoteBody.val() == '') {
        	showStatus('Input Note Body');
            return false;
        }

        var action = contact_form.attr('action');
        $.ajax({
            type: 'POST',
            url: action, 
            data: contact_form.serialize(),
            dataType: 'json',
            success: function(res){
                if (res.code == '200') {
                    /*
                    if ( oMode.val() == 'account' ) {
                        location.href = 'index.php?route=contact/list&accountid=' + oContactId.val();
                    } else {
                        location.href = 'index.php?route=contact/list&contactid=' + oContactId.val();
                    }
                    */

                    alert('Note Added');
                    location.reload();
                }
            }
        });
    });


    /********* Event **********/

    // close event
    //$('button.close-event').on('click', function(e){
    $.fn.completeEvent = function(oCompleteEvent, done) {

        if (!confirm('Are you sure you would like to close this event?')) {
            return false;
        }
        var that = $(oCompleteEvent),
            eventid = that.data('id'),
            //oNewEvent = that.closest('ul').find('#CheckNewEvent'),
            data = {
                'eventid': eventid
            };
        $.ajax({
            type: 'POST',
            url: 'index.php?route=event/list/closeEvent', 
            data: data,
            dataType: 'json',
            success: function(res){
                if (res.code == '200') {
                    if ( done == false ) {
                        return true;
                    } else {
                        if (oNewEvent.is(':checked') ) {
                            var contactid = oNewEvent.data('id');
                            var contactName = oNewEvent.data('name');
                            var url = 'index.php?route=event/new&contactid=' + contactid + '&contactname=' + encodeURIComponent(contactName);
                            location.href = url;
                        } else {
                            location.reload();
                        }
                    }
                }
            }
        });

    }

    $('input[name="CheckComplete"]').on('change', function(e) {
        var that = $(this),
            mom = that.closest('ul');
        if ( that.is(':checked') ) {
            mom.find('#check-complete-wrap').show();
        } else {
            mom.find('#check-complete-wrap').hide();
        }
    });

    // add note for event
    $('input.add-event-note').on('click', function(e){
        e.preventDefault();
        var that = $(this),
            isCompleted = false,
            form = that.closest('form.event-note-form'),
            note_wrap = form.closest('li.note-wrap'),
            oNewEvent = that.closest('ul').find('#CheckNewEvent'),
            oCompleteEvent = that.closest('ul').find('#CheckComplete');

        var oNoteTitle = form.find('input[name="note_title"]');
        var oNoteBody = form.find('textarea[name="note"]');
        if ( oCompleteEvent.is(':checked') ) {

            
            var eventtype = oNewEvent.data('eventtype');
            if (!confirm('Are you sure you would like to complete this ' + eventtype + '?')) {
                return false;
            }
            isCompleted = true;

            /*
            if ( oNoteTitle.val() == '' && oNoteBody.val() == '' ) {
                done = true;
            }
            if ( false == $.fn.completeEvent(oCompleteEvent, done) ) {
                return false;
            }
            */
        } 

        if ( isCompleted == false ) {
            if (oNoteTitle.val() == '') {
                alert('Input Note subject');
                return false;
            }

            if (oNoteBody.val() == '') {
                alert('Input Note Body');
                return false;
            }
        } else {
            if (oNoteBody.val() != '') {
                if (oNoteTitle.val() == '') {
                    alert('Input Note Subject');
                    return false;
                }
            }
        }

        $.ajax({
            type: 'POST',
            url: 'index.php?route=event/list/addNote', 
            data: form.serialize(),
            dataType: 'json',
            success: function(res){
                if (res.code == '200') {
                    form.find('input[name="note_title"]').val('');
                    form.find('textarea[name="note"]').val('');
                    if (oNewEvent.is(':checked') ) {
                        var recid = oNewEvent.data('id');
                        var recname = oNewEvent.data('name');
                        var rectype = oNewEvent.data('type');
                        var url = 'index.php?route=event/new&eventtype='+eventtype+'&rectype=' + rectype + '&recid=' + recid + '&recname=' + encodeURIComponent(recname);
                    } else {
                        var url = 'index.php?route=event/list';
                    }
                    location.href = url;
                }
            }
        });
    })

    // add note for event
    $('button.add-event').on('click', function(e){
        var that = $(this),
            contactid = that.data('id'),
            contactName = that.data('name'),
            data = {
                'contactid': contactid,
                'contactName': contactName
            };
        var url = 'index.php?route=event/new&contactid=' + contactid + '&contactname=' + encodeURIComponent(contactName);
        location.href = url;
    })
    
})

function goHome() {
	location.href = 'index.php?route=common/home';
}

function goContact() {
	location.href = 'index.php?route=contact/list';
}

function goEvent() {
	location.href = 'index.php?route=event/list';
}

function goUncompletedEvent() {
	location.href = 'index.php?route=event/list/uncompleted';
}

function login() {
	location.href = 'index.php?route=common/home';
}

function logout() {
	location.href = 'index.php?route=common/login/logout';
}

function getTimezoneName() {
	tmSummer = new Date(Date.UTC(2005, 6, 30, 0, 0, 0, 0));
	so = -1 * tmSummer.getTimezoneOffset();
	tmWinter = new Date(Date.UTC(2005, 12, 30, 0, 0, 0, 0));
	wo = -1 * tmWinter.getTimezoneOffset();

	if (-660 == so && -660 == wo) return 'Pacific/Midway';
	if (-600 == so && -600 == wo) return 'Pacific/Tahiti';
	if (-570 == so && -570 == wo) return 'Pacific/Marquesas';
	if (-540 == so && -600 == wo) return 'America/Adak';
	if (-540 == so && -540 == wo) return 'Pacific/Gambier';
	if (-480 == so && -540 == wo) return 'US/Alaska';
	if (-480 == so && -480 == wo) return 'Pacific/Pitcairn';
	if (-420 == so && -480 == wo) return 'US/Pacific';
	if (-420 == so && -420 == wo) return 'US/Arizona';
	if (-360 == so && -420 == wo) return 'US/Mountain';
	if (-300 == so && -360 == wo) return 'US/Central';
	if (-360 == so && -360 == wo) return 'America/Guatemala';
	if (-360 == so && -300 == wo) return 'Pacific/Easter';
	if (-300 == so && -300 == wo) return 'America/Bogota';
	if (-240 == so && -300 == wo) return 'US/Eastern';
	if (-240 == so && -240 == wo) return 'America/Caracas';
	if (-240 == so && -180 == wo) return 'America/Santiago';
	if (-180 == so && -240 == wo) return 'Canada/Atlantic';
	if (-180 == so && -180 == wo) return 'America/Montevideo';
	if (-180 == so && -120 == wo) return 'America/Sao_Paulo';
	if (-150 == so && -210 == wo) return 'America/St_Johns';
	if (-120 == so && -180 == wo) return 'America/Godthab';
	if (-120 == so && -120 == wo) return 'America/Noronha';
	if (-60 == so && -60 == wo) return 'Atlantic/Cape_Verde';
	if (0 == so && -60 == wo) return 'Atlantic/Azores';
	if (0 == so && 0 == wo) return 'Africa/Casablanca';
	if (60 == so && 0 == wo) return 'Europe/London';
	if (60 == so && 60 == wo) return 'Africa/Algiers';
	if (60 == so && 120 == wo) return 'Africa/Windhoek';
	if (120 == so && 60 == wo) return 'Europe/Amsterdam';
	if (120 == so && 120 == wo) return 'Africa/Harare';
	if (180 == so && 120 == wo) return 'Europe/Athens';
	if (180 == so && 180 == wo) return 'Africa/Nairobi';
	if (240 == so && 180 == wo) return 'Europe/Moscow';
	if (240 == so && 240 == wo) return 'Asia/Dubai';
	if (270 == so && 210 == wo) return 'Asia/Tehran';
	if (270 == so && 270 == wo) return 'Asia/Kabul';
	if (300 == so && 240 == wo) return 'Asia/Baku';
	if (300 == so && 300 == wo) return 'Asia/Karachi';
	if (330 == so && 330 == wo) return 'Asia/Calcutta';
	if (345 == so && 345 == wo) return 'Asia/Katmandu';
	if (360 == so && 300 == wo) return 'Asia/Yekaterinburg';
	if (360 == so && 360 == wo) return 'Asia/Colombo';
	if (390 == so && 390 == wo) return 'Asia/Rangoon';
	if (420 == so && 360 == wo) return 'Asia/Almaty';
	if (420 == so && 420 == wo) return 'Asia/Bangkok';
	if (480 == so && 420 == wo) return 'Asia/Krasnoyarsk';
	if (480 == so && 480 == wo) return 'Australia/Perth';
	if (540 == so && 480 == wo) return 'Asia/Irkutsk';
	if (540 == so && 540 == wo) return 'Asia/Tokyo';
	if (570 == so && 570 == wo) return 'Australia/Darwin';
	if (570 == so && 630 == wo) return 'Australia/Adelaide';
	if (600 == so && 540 == wo) return 'Asia/Yakutsk';
	if (600 == so && 600 == wo) return 'Australia/Brisbane';
	if (600 == so && 660 == wo) return 'Australia/Sydney';
	if (630 == so && 660 == wo) return 'Australia/Lord_Howe';
	if (660 == so && 600 == wo) return 'Asia/Vladivostok';
	if (660 == so && 660 == wo) return 'Pacific/Guadalcanal';
	if (690 == so && 690 == wo) return 'Pacific/Norfolk';
	if (720 == so && 660 == wo) return 'Asia/Magadan';
	if (720 == so && 720 == wo) return 'Pacific/Fiji';
	if (720 == so && 780 == wo) return 'Pacific/Auckland';
	if (765 == so && 825 == wo) return 'Pacific/Chatham';
	if (780 == so && 780 == wo) return 'Pacific/Enderbury'
	if (840 == so && 840 == wo) return 'Pacific/Kiritimati';
	return 'America/Chicago                                                                 ';
}
