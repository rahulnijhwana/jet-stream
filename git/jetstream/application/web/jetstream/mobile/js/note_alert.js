var interval = 3000;

$(function(){
	setInterval('checkAlerts()', interval);
    $('select#event_hours').attr('disabled',false);
    $('event_hours-button').attr('disabled',false);
    $('select#event_minutes').attr('disabled',false);
    $('event_minutes-button').attr('disabled',false);
    $('select#event_periods').attr('disabled',false);
    $('event_periods-button').attr('disabled',false);
    $('select').selectmenu('enable');

    $("#select_event_level").live('click', function(){
        $("#level_event_msg").show();
        $(".header").fadeOut();
        $(".header").fadeIn();
        $(".header").fadeOut();
        $(".header").fadeIn();
    });

    $("#select_event_all").live('click', function(e){
        toggleEventAllSelected(e);
    });

    $("#select_event_assigned").live('click', function(e){
        toggleEventAssignedSelected(e);
    });

    $("#select_event_level").live('click', function(){
        $("#level_event_msg").show();
        $(".header").fadeOut();
        $(".header").fadeIn();
        $(".header").fadeOut();
        $(".header").fadeIn();
    });
});

function toggleEventAllSelected(e){
    var current_state = $("#all_event_checked").attr('value');

    if(current_state != 1){
        if ( $("#select_event_all").hasClass('ui-link') ) {
            $(".all_users").attr('checked', true).checkboxradio('refresh');
            $("#all_event_checked").attr('value', 1).textinput('refresh');
        } else {
            $(".all_users").attr('checked', true);
            $("#all_event_checked").attr('value', 1);
        }

    } else {
        resetEventSelected();
    }
}

function toggleEventAssignedSelected(){
    var current_state = $("#assigned_event_checked").val();
    //console.log(current_state);
    if(current_state != 1){
        if ( $("#select_event_all").hasClass('ui-link') ) {
            $(".assigned_users").attr('checked', true).checkboxradio('refresh');
            $("#assigned_event_checked").attr('value', 1).textinput('refresh');
        } else {
            $(".assigned_users").attr('checked', true);
            $("#assigned_event_checked").attr('value', 1);
        }
    }
    else {
        resetEventAssignedSelected();
    }
}

function resetEventSelected(){
    if ( $("#select_event_all").hasClass('ui-link') ) {
        $(".all_users").removeAttr('checked').checkboxradio('refresh');
        $("#all_event_checked").attr('value', 0).textinput('refresh');
    } else {
        $(".all_users").removeAttr('checked');
        $("#all_event_checked").attr('value', 0);
    }
}

function resetEventAssignedSelected(){
    $(".assigned_users").removeAttr('checked').checkboxradio('refresh');
    $("#assigned_event_checked").val('0').textinput('refresh');
}



function checkAlerts(){
	//if($("#note_alerts").dialog("isOpen") != true){
	if(!$("#note-alert").is(":visible")){
		$.ajax({
			   type: "POST",
			   //url: "ajax/ajax.checkAlerts.php",
			   url: "index.php?route=event/alert",
			   success: function(html){
				   if(html != ''){
						   $("#alert").click();
				   }
				   else {
					   $("#note_alerts").html('');
				   }
			   }
		});
	}
}

function dismissAlert(){
	var alert_recipient_id = $("#alert_recipient_id").attr('value');
	$.ajax({
		   type: "POST",
		   //url: "ajax/ajax.dismissAlert.php",
		   url: "index.php?route=event/alert/dismissAlert",
		   data: "id=" + alert_recipient_id,
		   success: function(msg){
               $('#note-alert').dialog('close');
           }
	});
}

function snoozeAlert(){
	var alert_recipient_id = $("#alert_recipient_id").attr('value');
	var snooze_option = $("#snoozer option:selected").val();
	$.ajax({
		   type: "POST",
		   //url: "ajax/ajax.snoozeAlert.php",
		   url: "index.php?route=event/alert/snoozeAlert",
		   data: "id=" + alert_recipient_id + '&snooze=' + snooze_option,
		   success: function(msg){
               $('#note-alert').dialog('close');
           }
	});
}

function showNext(){
	//console.log('getting next note alert');
	$(".show").hide();
	var next = $(".show").next('.hide');
	next.addClass('.show');
	
}

function showPrevious(){
	//console.log('getting previous note alert');
	$(".show").hide();
}


function showAlerts(){
	
	$('#note_alerts').dialog({
		title: 'Note Alert!',
		modal: true,
		width: 700,
		height: 500,
		show:{
			effect:"pulsate", 
			duration: 300, 
			easing:"easeOutExpo"
		},
		hide:{
			effect:"drop", 
			direction:"down", 
			distance:100, 
			duration:500, 
			easing:"easeOutExpo"
		},
		buttons: {
			"Snooze": function() { 
			snoozeAlert();
			$(this).dialog("close"); 
		    },
            /*
        	"Previous": function() { 
        		showPrevious();
            },
            "Next": function() { 
            	showNext();
            },
            */
	        "Dismiss": function() { 
	            dismissAlert();
	            $(this).dialog("close"); 
	        } 

        }
	});
	$.ajax({
		   type: "GET",
		   url: "ajax/ajax.snoozeOptions.php",
		   success: function(msg){
				$(".ui-dialog-buttonpane").append(msg);
		   }
	});
}

function showNoteAlertHelp(){
	try {
		$('.note_alert_help').cluetip({
			width: '300px',
			showTitle: false,
			cluetipClass: 'jtip',
			arrows: true,
			dropShadow: false,
			cursor: 'pointer',
			positionBy: 'fixed',
			leftOffset: '35px',
			topOffset: '15px'
		});
	}
	catch(err){
		//just for the pages that do not contain the #help div
	}
}


