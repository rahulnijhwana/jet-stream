<?php
require_once '../slipstream/class.Note.php';

class ModelNotesBase extends Model {

    public function addNote($req) {

    	/*
           [companyname] => Alterra
           [contactid] => 588356
           [mode] => account
           [firstname] => 
           [lastname] => 
           [phone] => (619) 466-2200
           [email] => 
           [website] => 
           [note_title] => note for contact
           [note] => hi
		*/
        //echo '<pre>'; print_r($req); echo '</pre>'; exit;
		$mode = isset($req['mode']) ? $req['mode'] : 'event';
		if ('' == $mode)	return false;

        /*
		if ( 'event' == $mode ) {
			$eventid = isset($req['eventid']) ? $req['eventid'] : '';
		}
		if ( 'task' == $mode ) {
			$eventid = isset($req['taskid']) ? $req['taskid'] : '';
		}
        */
		$eventid = isset($req['eventid']) ? $req['eventid'] : '';
        // Note from contact has no eventid
		//if ('' == $eventid)	return false;

		$contactid = isset($req['contactid']) ? $req['contactid'] : '';
		$accountid = isset($req['accountid']) ? $req['accountid'] : '';
		if ( '' == $contactid && '' == $accountid )	return false;

		$note_title = isset($req['note_title']) ? $req['note_title'] : '';
		if ('' == $note_title)	return false;

		$note = isset($req['note']) ? $req['note'] : '';
		if ('' == $note)	return false;
		
		$oNote = new Note();
		$curTimeStamp = date('Y-m-d H:i:s');

		$oNote->noteCreator = $this->user->userid;
		$oNote->noteCreationDate = $curTimeStamp;
		$oNote->noteSubject = $note_title;
		if ('contact' == $mode) {
			$oNote->noteObjectType = NOTETYPE_CONTACT;
		} else if ( 'event' == $mode || 'task' == $mode ) {
			$oNote->noteObjectType = NOTETYPE_EVENT;
		} else {
			$oNote->noteObjectType = NOTETYPE_COMPANY;
		}

		if ( 'event' == $mode || 'task' == $mode ) {
			$oNote->noteObject = $eventid;
		} else {
			$oNote->noteObject = $contactid;
		}

        /* Note Alert */
        $oNote->alertUsers = $req['alert_user'];
        $oNote->alertStandard = ( isset($req['standard_event_alert']) && $req['standard_event_alert'] == 'on' ) ? true : false;
        $oNote->alertEmail = ( isset($req['email_event_alert']) && $req['email_event_alert'] == 'on' ) ? true : false;

        if( $req['send_event_immediately'] != 1 ){
            $delay = $req['delay_event_until'] . ' ' . $req['event_hours'] . ':' . $req['event_minutes'] . ' ' . $req['event_periods'];
            $now = strtotime($delay);
            $now += UTC_OFFSET;
            $timestamp = '@' . $now;
            $datetime = new DateTime($timestamp, new DateTimeZone('America/Chicago'));
            $oNote->delayUntil = $datetime->format('Y-m-d G:i:s.000');
        }

		$oNote->noteText = nl2br($note);
		
		$res = $oNote->createNote();
		
    	//echo '<pre>'; print_r($res); echo '</pre>'; exit;

		if( 'OK' == $res['STATUS'] ) {
			return true;
		}
		return false;
    }
}
