<?php
require_once '../slipstream/class.Dashboard.php';
require_once '../include/class.MapLookup.php';
require_once '../slipstream/class.Note.php';


class ModelContactBase extends Model {

	public function getContactById ($contactid) {

		$userid = $this->user->userid;
		if ('' == $userid)	return false;

		$companyid = $this->user->companyid;
		if ('' == $companyid)	return false;

		$contact_first_name = MapLookup::GetContactFirstNameField($companyid, TRUE);
		$contact_last_name  = MapLookup::GetContactLastNameField($companyid, TRUE);
		$account_name  = MapLookup::GetAccountNameField($companyid, TRUE);
		$email_column  = MapLookup::GetPrimaryEmailField($companyid, TRUE);
		$mainPhone_column  = MapLookup::GetMainPhoneField($companyid, TRUE);
		$directPhone_column  = MapLookup::GetDirectPhoneField($companyid, TRUE);

		$sql = "SELECT	C.ContactID, 
					C.{$contact_first_name} AS firstname,
					C.{$contact_last_name} AS lastname, 
                    ";
        if ( '' != $email_column ) {
			$sql .= "C.{$email_column} AS email, ";
        } else {
            $sql .= "'' AS email, ";
        }

        if ( '' != $mainPhone_column ) {
		    $sql .= "C.{$mainPhone_column} AS main_phone, ";
        } else {
            $sql .= "'' AS main_phone, ";
        }

        if ( '' != $directPhone_column ) {
            $sql .= "C.{$directPhone_column} AS direct_phone, ";
        } else {
            $sql .= "'' AS direct_phone, ";
        }
        $sql .= "   A.{$account_name} AS Account,
					C.PrivateContact
				FROM Contact C 
				LEFT JOIN Account A on C.AccountID = A.AccountID 
				WHERE C.ContactID = ? AND (A.Inactive != 1 OR A.Inactive IS NULL) AND 
				C.Inactive != 1 AND  C.Deleted != 1";

		$params = array();
		$params[] = array(DTYPE_INT, $contactid);
		$aContact = $this->db->query($sql, $params);

		$oNote = new Note();
		$oNote->noteObjectType = NOTETYPE_CONTACT;
		$oNote->noteObject = $contactid;
		$noteList = $oNote->getNoteList();

        //echo '<pre>'; print_r($aContact); echo '</pre>'; exit;
	
		$response = array(
			'contact' => $aContact[0],
			'notes' => $noteList
		);

		return $response;
	}

	private function getAccountColumnName($companyid, $labelname) {
		$map = MapLookup::GetAccountMap($companyid);

        //echo '<pre>'; print_r($map); echo '</pre>'; exit;
	
		foreach($map['fields'] as $row) {

			if($row['LabelName'] == $labelname) {
				return $row['FieldName'];
				break;
			}
		}
		return false;
	}

    private function getAccountWithKeyword($companyid, $keyword) {
        $map = MapLookup::GetAccountMap($companyid);

        //echo '<pre>'; print_r($map); echo '</pre>'; exit;

        foreach($map['fields'] as $row) {

            if($row['Keyword'] == $keyword) {
                return $row['FieldName'];
                break;
            }
        }
        return false;
    }

	public function getAccountById ($accountid) {
	
		$userid = $this->user->userid;
		if ('' == $userid)	return false;

		$companyid = $this->user->companyid;
		if ('' == $companyid)	return false;

		$accountName_column = $this->getAccountColumnName($companyid, 'Company Name');
		$mainPhone_column = $this->getAccountColumnName($companyid, 'Phone - Main');
		$tollfreePhone_column = $this->getAccountColumnName($companyid, 'Phone - Toll Free');
		$website_column = $this->getAccountColumnName($companyid, 'Website');

        if ( '' == $accountName_column ) {
		    $accountName_column = $this->getAccountWithKeyword($companyid, 'Company');
        }

        if ( '' == $mainPhone_column ) {
		    $mainPhone_column = $this->getAccountWithKeyword($companyid, 'Phone - Business');
        }

        if ( '' == $tollfreePhone_column ) {
		    $tollfreePhone_column = $this->getAccountWithKeyword($companyid, 'Phone - Pager');
        }
        if ( '' == $website_column ) {
		    $website_column = $this->getAccountWithKeyword($companyid, 'URL');
        }
	
		$sql = "SELECT	A.AccountID, ";

        if ( '' != $accountName_column ) {
            $sql .= "A.{$accountName_column} AS account_name, ";
        } else {
            $sql .= "'' AS account_name, ";
        }

        if ( '' != $mainPhone_column ) {
            $sql .= "A.{$mainPhone_column} AS main_phone, ";
        } else {
            $sql .= "'' AS main_phone, ";
        }

        if ( '' != $tollfreePhone_column ) {
            $sql .= "A.{$tollfreePhone_column} AS tollfree_phone, ";
        } else {
            $sql .= "'' AS tollfree_phone, ";
        }

        if ( '' != $website_column ) {
            $sql .= "A.{$website_column} AS website ";
        } else {
            $sql .= "'' AS website ";
        }

        $sql .= " FROM Account A WHERE A.AccountID = ? and A.CompanyID = ?";

		$params = array();
		$params[] = array(DTYPE_INT, $accountid);
		$params[] = array(DTYPE_INT, $companyid);
		$Account = $this->db->query($sql, $params);

		$oNote = new Note();
		$oNote->noteObjectType = NOTETYPE_COMPANY;
		$oNote->noteObject = $accountid;
		$noteList = $oNote->getNoteList();
	
		$response = array(
			'account' => $Account[0],
			'notes' => $noteList
		);

		return $response;
	}

	public function getAccountContact($query) {
		$userid = $this->user->userid;
		if ('' == $userid)	return false;

		$companyid = $this->user->companyid;
		if ('' == $companyid)	return false;

		$search = array(
		    'page_no' => '1',
		    'records_per_page' => '50',
		    'assigned_search' => 0,
		    'query_type' => 3,
		    'search_string' => $query,
		    'inactive_type' => 0,
		    'assign' => '',
		    'sort_type' => '',
		    'Advance' => '',
		    'inclContacts' => '',
		    'sort_dir' => ''
		);

		$oDashboard = new Dashboard($companyid, $userid);
	    $oDashboard->Build($search);
		$aResult = $oDashboard->SearchResults();
	
	    return $aResult;

	}

	public function getATCContacts($query) {

		$userid = $this->user->userid;
		if ('' == $userid)	return false;

		$companyid = $this->user->companyid;
		if ('' == $companyid)	return false;

		$contact_first_name = MapLookup::GetContactFirstNameField($companyid, TRUE);
		$contact_last_name  = MapLookup::GetContactLastNameField($companyid, TRUE);
		$account_name  = MapLookup::GetAccountNameField($companyid, TRUE);


		/*
		echo '<pre>'; print_r($contact_first_name); echo '</pre>';
		echo '<pre>'; print_r($contact_last_name); echo '</pre>';
		echo '<pre>'; print_r($account_name); echo '</pre>';
		*/

		/*
		 * $companyid = 764;
		 * $userid = 10065;
		 * $query = 'c';
 		 */
		$limitAccessToUnassignedUsers = ($this->getLimitAccessToUnassignedUsers()) ? true : false ;
		$sql_check_assigned_user = '';
		if ($limitAccessToUnassignedUsers) {
			$sql_check_assigned_user .= ' INNER JOIN PeopleContact AS PC ON C.ContactID = PC.ContactID AND PC.AssignedTo = 1 ';
			$sql_check_assigned_user .= ' AND PC.PersonID = ' . $userid;
		}

		$sql = "SELECT Distinct C.ContactID, 
					(C.{$contact_first_name} + ' ' + C.{$contact_last_name}) AS Contact, 
					A.{$account_name} AS Account,
					C.PrivateContact,
					PeopleContact.PersonID
				FROM Contact C 
				{$sql_check_assigned_user}
				JOIN Account A on C.AccountID = A.AccountID 
				RIGHT OUTER JOIN PeopleContact on PeopleCOntact.ContactID = C.ContactID And PeopleContact.PersonID = ?
				WHERE C.CompanyID = ? AND (A.Inactive != 1 OR A.Inactive IS NULL) AND 
				(C.{$contact_first_name} like ? OR C.{$contact_last_name} like ? ) AND 
				C.Inactive != 1 AND  C.Deleted != 1";
		
		// TODO need to check later
		//if ( isset($_GET['email_hold']) ) $sql .= " AND 8448 IN (SELECT PersonID from PeopleContact WHERE ContactID = C.ContactID) " ;  
		/*
		if(trim($company) != '') {
			$sql .= " AND A.$account_name = '$company'";
		}
		*/

		$params = array();
		$params[] = array(DTYPE_INT, $userid);
		$params[] = array(DTYPE_INT, $companyid);
		$params[] = array(DTYPE_STRING, "%".$query."%");
		$params[] = array(DTYPE_STRING, "%".$query."%");

		$aContact = $this->db->query($sql, $params);
		
		$data = array();
		foreach ($aContact as $i => $row) {
			$data[$i]['ContactID'] = $row['ContactID'];			
			$data[$i]['Contact'] = $row['Contact'].' ('.$row['Account'].')';			
			//$data[$i]['Account'] = $row['Account'];
			//$data[$i]['PrivateContact'] = $row['PrivateContact'];
			//$data[$i]['PersonID'] = $row['PersonID'];
		}
		$empty = array();
	
		if (!empty($data)) {
			return $data;
		}
		else {
			return array();
		}
	}

	private function getLimitAccessToUnassignedUsers() {
		$sql = 'select LimitAccessToUnassignedUsers from company where companyid = ?';
		$params = array();
		//$params[] = array(DTYPE_INT, $this->user->companyid);
		$params[] = array(DTYPE_INT, 765);
		$aLimitAccessToUnassignedUsers = $this->db->query($sql, $params);
		if ( isset($aLimitAccessToUnassignedUsers[0]['LimitAccessToUnassignedUsers']) 
			&& 1 == $aLimitAccessToUnassignedUsers[0]['LimitAccessToUnassignedUsers'] ) {
			return true;
		}
		return false;
	}
}
