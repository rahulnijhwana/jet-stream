<?php
require_once '../slipstream/class.Event.php';
require_once '../slipstream/class.ModuleEventNew.php';
require_once '../include/class.MapLookup.php';
//require_once '../include/class.GoogleCalendar.php';
require_once '../slipstream/class.Note.php';

class ModelEventBase extends Model {

	public function getList($req = array()) {

        $uncompleted = (isset($req['uncompleted'])) ? true : false ;

		$oEvent = new Event();

		$start = $end = date("m/d/Y");
		$target = array(
			$this->user->userid
		);

        if ( $uncompleted ) {
            /*
		    $events = $oEvent->GetUncompletedEvents('EVENT');
		    $tasks = $oEvent->GetUncompletedEvents('TASK');
            */
            $events = $oEvent->GetPendingEvents();
		    $tasks = $oEvent->GetUncompletedEvents('TASK');
        } else {
		    $events = $oEvent->GetEventsByDateRange($start, $end, $target, 'EVENT');
		    $tasks = $oEvent->GetEventsByDateRange($start, $end, $target, 'TASK');
        }

		$userid = $this->user->userid;
		if ('' == $userid)	return false;

		$companyid = $this->user->companyid;
		if ('' == $companyid)	return false;

		$aEvent = array(
            'event' => array(),
            'task' => array()
        );

		foreach($events as $event) {
	
			if ( $event['Closed'] == '1' ) {
				continue;
			}

			$oModuleEventNew = new ModuleEventNew($companyid, $userid);
			$eventid = $event['EventID'];
			$oModuleEventNew->Build('event', $eventid, 'display', true);

            $event['module'] = $oModuleEventNew;
				
			$aData = $oModuleEventNew->event_info->GetDataset();
            //echo '<pre>'; print_r($aData); echo '</pre>'; exit;

            if ( !isset( $aData['Contact'] ) ) {
                continue;
            }

			$event['Contact'] = $aData['Contact'];
			if ('' != $event['RepeatParent']) {
				$event['when'] = $oModuleEventNew->GetRepeatReadable();
			} else {
				$event['when'] = $oModuleEventNew->event_str;
			}

			$attendees = array();
			foreach ($oModuleEventNew->attendees as $att) {
				$attendees[] = $att['name'];
			}
			$attendees = implode(', ',$attendees);
			$event['attendees'] = $attendees;
            //echo '<pre>'; print_r($events); echo '</pre>'; exit; 
            $event['etype'] = '';
            if ( isset($event['eventtype']['EventName']) ) {
			    $event['etype'] = $event['eventtype']['EventName'];
            }

            $oNote = new Note();
            $oNote->noteObjectType = NOTETYPE_EVENT;
            $oNote->noteObject = $eventid;
            $noteList = $oNote->getNoteList();
            $event['first_note'] = '';
            if ( !empty($noteList[0]['Body']) ) {
                $note_count = count($noteList);
                $event['first_note'] = $noteList[$note_count-1]['Body'];
            }

            //echo '<pre>'; print_r($event['Contact']); echo '</pre>'; exit;

            $aEvent['event'][] = $event;
		}

		$aTask = array();
		foreach($tasks as $task) {
	
			if ( $task['Closed'] == '1' ) {
				continue;
			}

			$oModuleEventNew = new ModuleEventNew($companyid, $userid);
			$eventid = $task['EventID'];
			$oModuleEventNew->Build('task', $eventid, 'display', true);

			$task['module'] = $oModuleEventNew;	

			$aData = $oModuleEventNew->event_info->GetDataset();
            /*
            echo '<pre>'; print_r($task); echo '</pre>';
            echo '<pre>'; print_r($aData); echo '</pre>'; exit;
            */

			//$task['Contact'] = $aData['Contact'];
			$task['when'] = $oModuleEventNew->event_str;

			$attendees = array();
			foreach ($oModuleEventNew->attendees as $att) {
				$attendees[] = $att['name'];
			}
			$attendees = implode(', ',$attendees);
			$task['attendees'] = $attendees;
            $task['etype'] = '';
            if ( isset($task['eventtype']['EventName']) ) {
			    $task['etype'] = $task['eventtype']['EventName'];
            }

            $oNote = new Note();
            $oNote->noteObjectType = NOTETYPE_EVENT;
            $oNote->noteObject = $eventid;
            $noteList = $oNote->getNoteList();
            $task['first_note'] = '';
            if ( !empty($noteList[0]['Body']) ) {
                $note_count = count($noteList);
                $task['first_note'] = $noteList[$note_count-1]['Body'];
            }

            $task['ContactName'] = '';
            if ( !empty($oModuleEventNew->event_info['Contact']['FirstName']) ) {
			    $firstname = $oModuleEventNew->event_info['Contact']['FirstName'];
			    $lastname = (!empty($oModuleEventNew->event_info['Contact']['LastName'])) ? $oModuleEventNew->event_info['Contact']['LastName'] : '';
                $task['ContactName'] = $firstname . ' ' . $lastname;
            }

			$aEvent['task'][] = $task;

		}

        //echo '<pre>'; print_r($aEvent); echo '</pre>'; exit;



		return $aEvent;
	}

    /* TODO sync with google */
	public function closeEvent($eventid) {

        /* TODO : mobile first
        // get google id from event table
        $sql = 'SELECT googleEventId FROM Event WHERE EventID = ?';
        $params = array();
        $params[] = array(DTYPE_INT, $eventid);
        $aEvent = $this->db->query($sql, $params);
        $googleid = $aEvent[0]['googleEventId'];
        */

        $sql = 'Update Event set Closed = 1 where eventId = ?';
        $params = array();
        $params[] = array(DTYPE_INT, $eventid);
        $status = $this->db->query($sql, $params);
        
        /*
        $googleEvent = new GoogleCalendar();
        $googleEvent->setDeletedGoogleEvent($googleid);
        */

        return true;
	}

	/* below from contact */
	public function getEventById ($eventid) {

		$userid = $this->user->userid;
		if ('' == $userid)	return false;

		$companyid = $this->user->companyid;
		if ('' == $companyid)	return false;

		$event_first_name = MapLookup::GetEventFirstNameField($companyid, TRUE);
		$event_last_name  = MapLookup::GetEventLastNameField($companyid, TRUE);
		$account_name  = MapLookup::GetAccountNameField($companyid, TRUE);
		$email_column  = MapLookup::GetPrimaryEmailField($companyid, TRUE);
		$mainPhone_column  = MapLookup::GetMainPhoneField($companyid, TRUE);
		$directPhone_column  = MapLookup::GetDirectPhoneField($companyid, TRUE);

		$sql = "SELECT	C.EventID, 
					C.{$event_first_name} AS firstname,
					C.{$event_last_name} AS lastname, 
					C.{$email_column} AS email, 
					C.{$mainPhone_column} AS main_phone, 
					C.{$directPhone_column} AS direct_phone, 
					A.{$account_name} AS Account,
					C.PrivateEvent
				FROM Event C 
				JOIN Account A on C.AccountID = A.AccountID 
				WHERE C.EventID = ? AND (A.Inactive != 1 OR A.Inactive IS NULL) AND 
				C.Inactive != 1 AND  C.Deleted != 1";

		$params = array();
		$params[] = array(DTYPE_INT, $eventid);
		$aEvent = $this->db->query($sql, $params);

		$oNote = new Note();
		$oNote->noteObjectType = NOTETYPE_CONTACT;
		$oNote->noteObject = $eventid;
		$noteList = $oNote->getNoteList();
	
		$response = array(
			'event' => $aEvent[0],
			'notes' => $noteList
		);

		return $response;
	}

	private function getAccountColumnName($companyid, $labelname) {
		$map = MapLookup::GetAccountMap($companyid);
	
		foreach($map['fields'] as $row) {

			if($row['LabelName'] == $labelname) {
				return $row['FieldName'];
				break;
			}
		}
		return false;
	}

	public function getAccountById ($accountid) {
	
		$userid = $this->user->userid;
		if ('' == $userid)	return false;

		$companyid = $this->user->companyid;
		if ('' == $companyid)	return false;

		$accountName_column = $this->getAccountColumnName($companyid, 'Company Name');
		$mainPhone_column = $this->getAccountColumnName($companyid, 'Phone - Main');
		$tollfreePhone_column = $this->getAccountColumnName($companyid, 'Phone - Toll Free');
		$website_column = $this->getAccountColumnName($companyid, 'Website');
	
		$sql = "SELECT	A.AccountID,
					A.{$accountName_column} AS account_name,
					A.{$mainPhone_column} AS main_phone, 
					A.{$tollfreePhone_column} AS tollfree_phone, 
					A.{$website_column} AS website
				FROM Account A
				WHERE A.AccountID = ? and A.CompanyID = ?";

		$params = array();
		$params[] = array(DTYPE_INT, $accountid);
		$params[] = array(DTYPE_INT, $companyid);
		$Account = $this->db->query($sql, $params);

		$oNote = new Note();
		$oNote->noteObjectType = NOTETYPE_COMPANY;
		$oNote->noteObject = $accountid;
		$noteList = $oNote->getNoteList();
	
		$response = array(
			'account' => $Account[0],
			'notes' => $noteList
		);

		return $response;
	}

	public function getAccountEvent($query) {
		$userid = $this->user->userid;
		if ('' == $userid)	return false;

		$companyid = $this->user->companyid;
		if ('' == $companyid)	return false;

		$search = array(
		    'page_no' => '1',
		    'records_per_page' => '50',
		    'assigned_search' => 0,
		    'query_type' => 3,
		    'search_string' => $query,
		    'inactive_type' => 0,
		    'assign' => '',
		    'sort_type' => '',
		    'Advance' => '',
		    'inclEvents' => '',
		    'sort_dir' => ''
		);

		$oDashboard = new Dashboard($companyid, $userid);
	    $oDashboard->Build($search);
		$aResult = $oDashboard->SearchResults();
	
	    return $aResult;

	}

	public function getATCEvents($query) {

		$userid = $this->user->userid;
		if ('' == $userid)	return false;

		$companyid = $this->user->companyid;
		if ('' == $companyid)	return false;

		$event_first_name = MapLookup::GetEventFirstNameField($companyid, TRUE);
		$event_last_name  = MapLookup::GetEventLastNameField($companyid, TRUE);
		$account_name  = MapLookup::GetAccountNameField($companyid, TRUE);


		/*
		echo '<pre>'; print_r($event_first_name); echo '</pre>';
		echo '<pre>'; print_r($event_last_name); echo '</pre>';
		echo '<pre>'; print_r($account_name); echo '</pre>';
		*/

		/*
		 * $companyid = 764;
		 * $userid = 10065;
		 * $query = 'c';
 		 */
		$limitAccessToUnassignedUsers = ($this->getLimitAccessToUnassignedUsers()) ? true : false ;
		$sql_check_assigned_user = '';
		if ($limitAccessToUnassignedUsers) {
			$sql_check_assigned_user .= ' INNER JOIN PeopleEvent AS PC ON C.EventID = PC.EventID AND PC.AssignedTo = 1 ';
			$sql_check_assigned_user .= ' AND PC.PersonID = ' . $userid;
		}

		$sql = "SELECT Distinct C.EventID, 
					(C.{$event_first_name} + ' ' + C.{$event_last_name}) AS Event, 
					A.{$account_name} AS Account,
					C.PrivateEvent,
					PeopleEvent.PersonID
				FROM Event C 
				{$sql_check_assigned_user}
				JOIN Account A on C.AccountID = A.AccountID 
				RIGHT OUTER JOIN PeopleEvent on PeopleCOntact.EventID = C.EventID And PeopleEvent.PersonID = ?
				WHERE C.CompanyID = ? AND (A.Inactive != 1 OR A.Inactive IS NULL) AND 
				(C.{$event_first_name} like ? OR C.{$event_last_name} like ? ) AND 
				C.Inactive != 1 AND  C.Deleted != 1";
		
		// TODO need to check later
		//if ( isset($_GET['email_hold']) ) $sql .= " AND 8448 IN (SELECT PersonID from PeopleEvent WHERE EventID = C.EventID) " ;  
		/*
		if(trim($company) != '') {
			$sql .= " AND A.$account_name = '$company'";
		}
		*/

		$params = array();
		$params[] = array(DTYPE_INT, $userid);
		$params[] = array(DTYPE_INT, $companyid);
		$params[] = array(DTYPE_STRING, "%".$query."%");
		$params[] = array(DTYPE_STRING, "%".$query."%");

		$aEvent = $this->db->query($sql, $params);
		
		$data = array();
		foreach ($aEvent as $i => $row) {
			$data[$i]['EventID'] = $row['EventID'];			
			$data[$i]['Event'] = $row['Event'].' ('.$row['Account'].')';			
			//$data[$i]['Account'] = $row['Account'];
			//$data[$i]['PrivateEvent'] = $row['PrivateEvent'];
			//$data[$i]['PersonID'] = $row['PersonID'];
		}
		$empty = array();
	
		if (!empty($data)) {
			return $data;
		}
		else {
			return array();
		}
	}

	private function getLimitAccessToUnassignedUsers() {
		$sql = 'select LimitAccessToUnassignedUsers from company where companyid = ?';
		$params = array();
		//$params[] = array(DTYPE_INT, $this->user->companyid);
		$params[] = array(DTYPE_INT, 765);
		$aLimitAccessToUnassignedUsers = $this->db->query($sql, $params);
		if ( isset($aLimitAccessToUnassignedUsers[0]['LimitAccessToUnassignedUsers']) 
			&& 1 == $aLimitAccessToUnassignedUsers[0]['LimitAccessToUnassignedUsers'] ) {
			return true;
		}
		return false;
	}

    public function snoozeAlert($req) {
        echo '<pre>'; print_r($req); echo '</pre>';
    }
}
