<?php
require_once './include/constants.php';
require_once './include/class.MVC.php';

// Jetstream configuration
require_once '../include/mpconstants.php';
	
$registry = new Registry();

$loader = new Loader($registry);
$registry->set('load', $loader);

$db = new DB('mssql');
$registry->set('db', $db);
	
// Request
$request = new Request();
$registry->set('request', $request);

$response = new Response();
$response->addHeader('Content-Type: text/html; charset=utf-8');
$registry->set('response', $response); 

$session = new Session();
$registry->set('session', $session); 
	
$user = new Users($registry);
$registry->set('user', $user);

/* TODO conflict with web's class */
/*
$util = new Util();
$registry->set('util', $util);
*/

$controller = new Front($registry);

// Router
if (isset($request->get['route'])) {
	$action = new Action($request->get['route']);
} else {
	$action = new Action('common/home');
}

// Dispatch
$controller->dispatch($action, new Action('error/not_found'));

// Output
$response->output();

?>
