#!/usr/bin/python
import email
import imaplib
import os
import re
import string
from pprint import pprint
from BeautifulSoup import BeautifulSoup
from email.mime.image import MIMEImage
from email.parser import HeaderParser
from email.utils import getaddresses
from datetime import datetime, timedelta
from time import localtime, strftime, mktime
from constants import IMAP_SERVER_TYPE_EXCHANGE, IMAP_SERVER_TYPE_GMAIL, IMAP_SERVER_TYPE_FUSEMAIL
import db


def parse_addresses(msg, type):
    """
    Builds 2 semi-colon separated strings for database insertion
    1 containing display names, 1 containing email addresses
    """
    names, emails = [], []
    raw = msg.get_all(type, [])
    addresses = getaddresses(raw)
    for address in addresses:
        name, addr = address
        if not name:
            name = addr
        names.append(name)
        emails.append(addr)
    return ";".join(names),";".join(emails)


def sync(user, server):

    local = datetime.today() - timedelta(hours=6)
    today = local.strftime("%d-%b-%Y")
    start_time = datetime.now() - timedelta(hours=6)

    print
    print 'IMAP Sync begins: ' + start_time.strftime("%m/%d/%Y %H:%M:%S %p")
    
    # If this is the first run, just get messages from the last hour
    if not user.last_sync:
        user.last_sync = (datetime.fromtimestamp(mktime(localtime())) - timedelta(hours=36))

    if server.ssl:
        print 'ssl'
        m = imaplib.IMAP4_SSL(server.address, server.port)
    else:
        m = imaplib.IMAP4(server.address, server.port)


    login_error = False
    try:
        m.login(user.username, user.password)
    except Exception, e:
        print 'Problem logging into server. Incorrect password?'
        print 'Account: ' + user.username
        login_error = True
    
    if not login_error:

        typ, data = m.list()
        print 'Response code:', typ
        print 'Response:'
        pprint(data)

        print 'Syncing user: ' + user.username
        print
        print 'Last sync: ' + str(user.last_sync)
        
        # Sent mail
        if server.imap_server_type_id == IMAP_SERVER_TYPE_GMAIL:
            m.select('[Gmail]/Sent Mail')
        elif server.imap_server_type_id == IMAP_SERVER_TYPE_EXCHANGE:
            m.select('Sent Items')
        elif server.imap_server_type_id == IMAP_SERVER_TYPE_FUSEMAIL:
            status, msgs = m.select('INBOX.Sent Items')
        else:
            m.select('Sent Items')
        try: 
            resp, sent = m.search(None, '(SINCE "' + today + '")')
            print resp
            print sent
            sent = sent[0].split()
            print 'Sent mail: '
            print sent
            traverse(m, user, sent)
        except Exception, e:
            print 'ERROR'
            raise e
        
        # Received mail
        m.select(readonly=True)
        resp, items = m.search(None, '(SINCE "' + today + '")') #@UnusedVariable
        items = items[0].split()
        print 'Received mail: '
        print items
        traverse(m, user, items)
 
        # Server is set to UTC
        local = datetime.now() - timedelta(hours=6) 
        db.set_last_sync(user, local)


def traverse(m, user, items):
    ### directory where to save attachments (default: current)
    detach_dir = '/webtemp/attachments/'

    # Translation table
    table = ''.join(' ' if n < 32 or n > 126 else chr(n) for n in xrange(256))

    for emailid in items:
        header = m.fetch(emailid, '(BODY.PEEK[HEADER])')
        parser = HeaderParser()
        msg = parser.parsestr(header[1][0][1])
        received = datetime.fromtimestamp(email.utils.mktime_tz(email.utils.parsedate_tz(msg['Date']))) #@UndefinedVariable
        received -= timedelta(hours=6)
        print 'Received: ' + str(received)
        if(received > user.last_sync):
            print 'new mail!'

            email_content = db.EmailContent()
            email_content.recipient_names, email_content.recipient_emails = parse_addresses(msg, 'to')
            email_content.cc_names, email_content.cc_emails = parse_addresses(msg, 'cc')
            email_content.from_name, email_content.from_email = parse_addresses(msg, 'from')
            received_date = email.utils.parsedate_tz(msg['Date'])[0:9]
            email_content.received_date = re.sub(r"\b0", "", strftime("%m/%d/%Y %H:%M:%S %p", received_date))
            if not msg['Message-ID']:
                continue
            email_content.entry_id = email.utils.unquote(msg['Message-ID']) #@UndefinedVariable
            email_content.subject = msg['Subject']
            email_content.person_id = user.person_id
            email_content.company_id = user.company_id
            #resp, data = m.fetch(emailid, "(RFC822)") #@UnusedVariable
            resp, data = m.fetch(emailid, "(BODY.PEEK[])")
            mail = email.message_from_string(data[0][1])

            valid_maintypes = ['multipart', 'text']
            if mail.get_content_maintype() not in valid_maintypes:
                continue
            
            attachments = []
            body = ''
            parts = 0
            for part in mail.walk():
                parts += 1
                if part.get_content_maintype() == 'multipart':
                    continue
                if part.get('Content-Disposition') is None:
                    accepted_types = ['text/plain', 'text/html']
                    if part.get_content_type() in accepted_types:
                        bod = ''
                        bod = part.get_payload(decode=True)
                        try:
                            clean = ''
                            clean = string.translate(bod, table)
                            body = clean

                        except Exception, e:
                            print 'ERROR Decoding body'
                            #raise e
                    
                    continue
                filename = part.get_filename()
                counter = 1
                if not filename:
                    filename = 'part-%03d%s' % (counter, 'bin')
                    counter += 1
                else:
                    attachment = db.EmailAttachment(filename)
                    attachment.rename(email_content.entry_id)
                    filename = attachment.stored_name
                    attachments.append(attachment)
                att_path = os.path.join(detach_dir, filename)
                if not os.path.isfile(att_path):
                    fp = open(att_path, 'wb')
                    fp.write(part.get_payload(decode=True))
                    fp.close()
    
            email_content.body = body
            db.save(email_content, attachments)
    

# Sync email!
if __name__ == "__main__":
    companies = db.get_companies()
    for company in companies:
        users = db.get_thinktin_user(company.company_id)
        for user in users:
            if user.person_id == 9290 and user.server_id == 2:
                print 'found'
                server = db.get_server(user.server_id)
                sync(user, server)

