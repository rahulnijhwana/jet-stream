#!/usr/bin/python
import datetime
from smtp import send_email
from phpserialize import unserialize
from BeautifulSoup import BeautifulSoup
from Cheetah.Template import Template
from sqlalchemy import create_engine, desc, and_, or_
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import between

from util import getConnectionString
from constants import (REPEAT_DAILY, REPEAT_MONTHLY, REPEAT_MONTHLY_POS,
                       REPEAT_NONE, REPEAT_WEEKLY, REPEAT_YEARLY,
                       REPEAT_YEARLY_POS)
from db import (Account, AccountMap, Company, Contact, ContactMap,
                EmailContent, Event, EventAttendee, EventType, Note,
                Option, Reminder, People, PeopleContact)


engine = create_engine(getConnectionString(), echo=False)
#engine = create_engine(getConnectionString(), echo='debug', echo_pool=True)
Base = declarative_base(engine)
today = datetime.date.today()


class CompanyUtil():
    
    def get_companies(self):
        session = load_session()
        companies = (session.query(Company).
            filter(Company.reminder == True).all())
        session.close()
        return companies


class PersonUtil():
    def get_people(self, company_id):
        session = load_session()
        people = (session.query(People).
            filter(People.company_id == company_id).
            filter(or_(People.deleted == 0, People.deleted == None)).
            filter(or_(People.supervisor_id > 0, People.supervisor_id == -2)).
            order_by(desc(People.level)))
        session.close()
        return people
    
    
    def get_children(self, person_id, people):
        children = []
        for person in people:
            if person.supervisor_id == person_id:
                children.append(person)
        for child in children:
            for person in people:
                if person.supervisor_id == child.id:
                    children.append(person)
        return children
  
    
    def get_salesperson_name(self, person_id):
        session = load_session()
        person = (session.query(People).
            filter(People.id == person_id).one())
        return person.first_name + ' ' + person.last_name


class EventUtil():

    def get_event_contact(self, event_id):
        contact_id = None
        session = load_session()

        try:
            event = session.query(Event).filter(Event.id == event_id).one()
            contact_id = event.contact_id
        except Exception, e:
            print 'ERROR! Contact not found:'
            print event_id
        session.close()
        return contact_id


    def get_events(self, person_id, day):
        
        repeating_events = []
       
        for repeating_event in self.get_repeating_events(person_id, day):
            repeating_events.append(repeating_event.id)

        session = load_session()
        if repeating_events:
            events = (session.query(Event).
                join(EventAttendee).
                join(EventType).
                filter(EventType.id == Event.event_type_id).
                filter(EventType.type == 'EVENT').
       	        filter(Event.contact_id != 0).
       	        filter(Event.contact_id != None).
                filter(or_(between(Event.start_date_utc, day,
                                   day + datetime.timedelta(days=1)),
                           Event.id.in_(repeating_events) )).
                filter(EventAttendee.person_id == person_id).
                order_by(Event.start_date_utc).all())
        else:
            events = (session.query(Event).
                join(EventAttendee).
                join(EventType).
                filter(EventType.id == Event.event_type_id).
                filter(EventType.type == 'EVENT').
       	        filter(Event.contact_id != 0).
       	        filter(Event.contact_id != None).
                filter(between(Event.start_date_utc, day,
                               day + datetime.timedelta(days=1))).
                filter(EventAttendee.person_id == person_id).
                order_by(Event.start_date_utc).all())
        session.close()
        
        return events


    def get_repeating_events(self, person_id, day=None):

        today = datetime.datetime.utcnow()
        
        session = load_session()
        events = (session.query(Event).
            join(EventAttendee).
            join(EventType).
            filter(EventType.id == Event.event_type_id).
            filter(EventType.type == 'EVENT').
            filter(or_(Event.closed == None, Event.closed == False)).
            filter(or_(Event.cancelled == None, Event.cancelled == False)).
            filter(EventAttendee.person_id == person_id).
            filter(Event.repeat_type > 0).
            filter(or_(Event.repeat_end_date_utc >= today,
                       Event.repeat_end_date_utc == None)).
            order_by(Event.start_date_utc).all())
        
        repeating_events = []
        for event in events:
            
            repeating_event = RepeatingEvent(event)
            repeat_dates = repeating_event.get_repeat_dates()
            
            for repeat_date in repeat_dates['repeat_dates']:
                
                if day == repeat_date:
                    repeating_events.append(event)

        session.close()
        return repeating_events


class RepeatingEvent():
    
    def __init__(self, event):
        self.event = event
        self.repeat_type = event.repeat_type
        self.repeat_exceptions = event.repeat_exceptions
        self.repeat_interval = event.repeat_interval
        self.start_date = event.start_date_utc
        self.end_date = event.repeat_end_date_utc
        self.utc_now = datetime.datetime.utcnow()
        self.default_end_date = self.utc_now + datetime.timedelta(days=365)
        self._setup()
        
    
    def _setup(self):
        '''Convert comma separated list of strings to datetimes'''
        _repeat_exceptions = []
        if self.repeat_exceptions is not None:
            exceptions = self.repeat_exceptions.split(',')

            for e in exceptions:
                converted = datetime.datetime.strptime(e, '%Y%m%d').date()
                _repeat_exceptions.append(converted)
            
        self.repeat_exceptions = _repeat_exceptions
        self.end_date = self.get_repeat_end_date()
        self.repeat_interval = unserialize(self.repeat_interval)
        

    def shift_days(self):
        '''Stored intervals come from PHP where the days of week are defined:
                0 = Sunday
                1 = Monday
                2 = Tuesday
                3 = Wednesday
                4 = Thursday
                5 = Friday
                6 = Saturday
        Shift the day by one to conform to the Python impl'''
        
        if self.repeat_interval.has_key(1):
            weekdays = self.repeat_interval[1]
            
            for k, v in weekdays.iteritems():
                weekdays[k] = v-1

            self.repeat_interval[1] = weekdays

        
    def get_repeat_end_date(self):
        repeat_end_date = self.default_end_date
        if self.end_date is not None:
            if self.end_date >= self.utc_now:
                repeat_end_date = self.end_date
            else:
                repeat_end_date = None
        return repeat_end_date
    
        
    def get_repeat_dates(self):

        if self.end_date is None:
            return {}
        
        repeat_dates = []
        
        if self.repeat_type == REPEAT_DAILY:

            self.shift_days()
            frequency = int(self.repeat_interval[0])

            day = self.start_date
            while day < self.end_date - datetime.timedelta(days=frequency):
                day = day + datetime.timedelta(days=frequency)
                if day >= self.utc_now:
                    if day.date() not in self.repeat_exceptions:
                        repeat_dates.append(day.date())

        elif self.repeat_type == REPEAT_WEEKLY:
            
            self.shift_days()
            frequency = int(self.repeat_interval[0])
            weekdays = self.repeat_interval[1]
            day = self.start_date

            while day < self.end_date - datetime.timedelta(days=frequency):
                
                if day.date() >= self.utc_now.date():
                    if day.date() not in self.repeat_exceptions:
                        if day.date().weekday() in weekdays.values():
                            repeat_dates.append(day.date())
                
                day = day + datetime.timedelta(days=frequency)
                
        elif self.repeat_type == REPEAT_MONTHLY:
            pass

        elif self.repeat_type == REPEAT_YEARLY:
            frequency = int(self.repeat_interval[0])
            month = int(self.repeat_interval[1])
            day_of_month = int(self.repeat_interval[2])
            day = self.start_date

        
        pair = {'event_id' : self.event.id, 'repeat_dates' : repeat_dates}

        return pair
   


class ContactUtil():
    
    def get_contact(self, contact_id):
        contact = None
        session = load_session()
        try:
            contact = session.query(Contact). \
                filter(Contact.id == contact_id).one()
        except Exception, e:
            print 'ERROR! Contact not found'
            print contact_id

        session.close()
        return contact
    
    
    def get_assigned_contacts(self, person_id):
        session = load_session()
        contacts = session.query(Contact, Account). \
            join(PeopleContact). \
            outerjoin(Account). \
            filter(or_(Contact.deleted == 0, Contact.deleted == None)). \
            filter(PeopleContact.person_id == person_id). \
            filter(PeopleContact.assigned_to == True).all()
        session.close()
        return contacts


    def get_option(self, id=None, field=None, label=None, company_id=None):
        invalid = [None, 0]
        if id not in invalid:
            session = load_session()
            option = session.query(Option).filter(Option.id == id).one()
            session.close()
            return option.name
        if company_id is not None:
            session = load_session()
            option = (session.query(Option).join(ContactMap).
                        filter(ContactMap.field_name == field).
                        filter(Option.name == label).
                        filter(ContactMap.company_id == company_id).all())
            return option[0].id
        
        
    # alias for get_option
    get_state = get_option
    
    
    def get_contact_name(self, company_id, contact_id):
        if contact_id is None:
            return

        session = load_session()
        first_name_field = session.query(ContactMap). \
            filter(ContactMap.company_id == company_id). \
            filter(ContactMap.is_first_name == True).one()

        last_name_field = session.query(ContactMap). \
            filter(ContactMap.company_id == company_id). \
            filter(ContactMap.is_last_name == True).one()
        
        args = ({'contact_id': contact_id})
        raw = "SELECT " +  first_name_field.field_name + ", " + \
            last_name_field.field_name + " FROM Contact " \
            "WHERE ContactID=:contact_id"
        
        first_name = (session.query(first_name_field.field_name).
                      from_statement(raw).params(args).one())
        last_name = (session.query(last_name_field.field_name).
                     from_statement(raw).params(args).one())
        
        session.close()
        return first_name[0] + " " + last_name[0]
    
    
    def get_contact_detail(self, company_id, contact_id):
        if contact_id is None:
            return
        
        session = load_session()
        contact_fields = session.query(Reminder). \
            filter(Reminder.company_id == company_id).one()
            
        args = ({'contact_id': contact_id})
        raw = "SELECT " +  \
            contact_fields.contact_address1_field + ", " + \
            contact_fields.contact_address2_field + ", " + \
            contact_fields.contact_city_field + ", " + \
            contact_fields.contact_state_field + ", " + \
            contact_fields.contact_zip_field + ", " + \
            contact_fields.contact_phone_field + \
            " FROM Contact WHERE ContactID=:contact_id"
        
        contact_detail = (session.query(contact_fields.contact_address1_field,
                                       contact_fields.contact_address2_field,
                                       contact_fields.contact_city_field,
                                       contact_fields.contact_state_field,
                                       contact_fields.contact_zip_field,
                                       contact_fields.contact_phone_field).
                                       from_statement(raw).params(args).one())               
        detail = {
                  'address1' : contact_detail[0],
                  'address2' : contact_detail[1],
                  'city' : contact_detail[2],
                  'state' : self.get_state(id=contact_detail[3]),
                  'zip' : contact_detail[4],
                  'phone' : contact_detail[5]
                  }
        
        session.close()
        
        str = ''
        if detail['address1'] is not None:
            str += detail['address1'] + '<br>'
        if detail['address2'] is not None:
            str += detail['address2'] + '<br>'
        
        addr = ''
        if detail['city'] is not None:
            addr = detail['city']
        
        if detail['state'] is not None:
            addr += ', ' + detail['state']
        
        if detail['zip'] is not None:
            addr += ' ' + detail['zip'] + '<br>'

        str += addr
        
        if detail['phone'] is not None:
            str += 'Phone: ' + detail['phone']
            
        return str
    
        
    def get_account_name(self, company_id, contact_id):
        
        if contact_id is None:
            return

        session = load_session()
        
        contact = session.query(Contact). \
            filter(Contact.id == contact_id).one()
        
        account_name_field = session.query(AccountMap). \
            filter(AccountMap.company_id == company_id). \
            filter(AccountMap.is_company_name == True).one()

        if contact.account_id is None:
            return
            
        args = ({'account_id': contact.account_id})

        raw = "SELECT " +  account_name_field.field_name + " FROM Account " \
            "WHERE AccountID=:account_id"
        
        account_name = (session.query(account_name_field.field_name).
                        from_statement(raw).params(args).one())
        session.close()
        return account_name[0]


    def get_account_detail(self, company_id, contact_id):
        if contact_id is None:
            return
        
        session = load_session()
        contact = session.query(Contact). \
            filter(Contact.id == contact_id).one()
        
        if contact.account_id is None:
            return
        
        account_fields = session.query(Reminder). \
            filter(Reminder.company_id == company_id).one()
            
        args = ({'account_id': contact.account_id})
        raw = "SELECT " +  \
            account_fields.account_address1_field + ", " + \
            account_fields.account_address2_field + ", " + \
            account_fields.account_city_field + ", " + \
            account_fields.account_state_field + ", " + \
            account_fields.account_zip_field + ", " + \
            account_fields.account_phone_field + ", " + \
            account_fields.account_fax_field + ", " + \
            account_fields.account_url_field + \
            " FROM Account WHERE AccountID=:account_id"
        
        account_detail = (session.query(account_fields.account_address1_field,
                                       account_fields.account_address2_field,
                                       account_fields.account_city_field,
                                       account_fields.account_state_field,
                                       account_fields.account_zip_field,
                                       account_fields.account_phone_field,
                                       account_fields.account_fax_field,
                                       account_fields.account_url_field).
                                       from_statement(raw).params(args).one())               
        detail = {
                  'address1' : account_detail[0],
                  'address2' : account_detail[1],
                  'city' : account_detail[2],
                  'state' : self.get_state(id=account_detail[3]),
                  'zip' : account_detail[4],
                  'phone' : account_detail[5],
                  'fax' : account_detail[6],
                  'url' : account_detail[7]
                  }
        
        session.close()
        str = ''
        if detail['address1'] is not None:
            str += detail['address1'] + '<br>'
        if detail['address2'] is not None:
            str += detail['address2'] + '<br>'
        
        addr = ''
        if detail['city'] is not None:
            addr = detail['city']
        
        if detail['state'] is not None:
            addr += ', ' + detail['state']
        
        if detail['zip'] is not None:
            addr += ' ' + detail['zip'] + '<br>'

        str += addr
        
        if detail['phone'] is not None:
            str += 'Bus: ' + detail['phone'] + '<br>'
        
        if detail['fax'] is not None:
            str += 'Fax: ' + detail['fax'] + '<br>'
        
        if detail['url'] is not None:
            str += 'Web: <a href="' +  detail['url'] + '">' + detail['url'] + '</a>'
            
        return str
        

def get_managers_summary(person, children):
    output = Template(file='/usr/local/templates/managers_report.tmpl', \
                   searchList=[{'person' : person, 'children' : children}])   
    return str(output)


def get_appointment_summary(person):
    output = Template(file='/usr/local/templates/appointment_summary.tmpl',
                      searchList=[{'person' : person}])
    return str(output)


def get_appointment_detail(person):
    output = Template(file='/usr/local/templates/appointment_detail.tmpl',
                      searchList=[{'person' : person}])
    return str(output)


def has_appointments_today(person):
    today = datetime.date.today()
    event_util = EventUtil()
    events = event_util.get_events(person.id, today)
    return events


def get_notes(person, event_id, contact_id):
    session = load_session()
    notes = session.query(Note). \
        filter(or_(  and_(Note.object_type == 3, Note.object_referrer == event_id), and_(Note.object_referrer == contact_id, Note.object_type == 2) )). \
            order_by(desc(Note.creation_date)).limit(5)
    
    valid_notes = []
    subject = ''
    note_text = ''
    
    for note in notes:
        if (note.created_by == person.id) or (note.created_by != person.id and note.private_note == 0):
            if note.subject == 'Email':
                try:
                    email_content = session.query(EmailContent). \
                        filter(note.email_content_id == EmailContent.id).one()
                    subject = email_content.subject
                
                    tags = ['img']
                    soup = strip_tags(email_content.body, tags)
                
                    if soup.html is not None:
                        note_text = soup.html.body
                    else :
                        note_text = email_content.body
                except Exception, e:
                    print 'ERROR! Problem finding email content: ' + str(note.email_content_id)
                    continue

            else :
                subject = note.subject
                note_text = note.note_text

            valid_notes.append({
                                'created_by' : note.created_by,
                                'creation_date' : note.creation_date,
                                'subject' : subject.decode("utf-8"),
                                'note_text' : note_text.decode("utf-8")
                                })
    return valid_notes

def strip_tags(html, invalid_tags):
    soup = BeautifulSoup(html)
    for tag in soup.findAll(True):
        if tag.name in invalid_tags:
            tag.replaceWith('')
    return soup


def load_session():
    Session = sessionmaker(bind=engine)
    session = Session()
    return session


appointment_detail_subject = "Jetstream Daily Appointment Reminder - Today's Appointments"
appointment_summary_subject = "Jetstream Daily Appointment Reminder - Upcoming 3-Day Summary Report"
managers_detail_subject = "Jetstream Daily Appointment Reminder - Upcoming 3-Day Manager's Summary Report"


if __name__ == "__main__":
    
    company_util = CompanyUtil()
    for company in company_util.get_companies():

	if company.id == 761:        
            print '\nDaily Appointment Reminder - ' + company.name + ' (' + str(company.id) + ') on ' + today.strftime('%b %d, %Y')
        
            person_util = PersonUtil()
            people = person_util.get_people(company.id)
        
            for person in people:
		if person.id != 9966:
	 	    children = person_util.get_children(person.id, people)
            	    if children:
                        args = {'person' : person, 'children' : children}
                    	managers_summary = get_managers_summary(**args)
                    	print 'sending managers summary for: ' + person.email
                    	#print managers_summary
                    	send_email(person.email, managers_detail_subject, managers_summary)

                    appointment_summary = get_appointment_summary(person)
                    print 'sending summary for: ' + person.email
                    #print appointment_summary
            	    send_email(person.email, appointment_summary_subject, appointment_summary)

            	    if has_appointments_today(person):
                    	appointment_detail = get_appointment_detail(person)
                    	print 'sending detail for: ' + person.email
                    	#print appointment_detail
                    	send_email(person.email, appointment_detail_subject, appointment_detail)

