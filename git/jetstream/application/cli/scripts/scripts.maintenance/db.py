#!/usr/bin/python

from sqlalchemy import create_engine, Column, ForeignKey, Unicode
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.mssql import (BIT, DATETIME,
                                       INTEGER, SMALLINT, VARCHAR, TEXT)
import util
from constants import (VALIDATION_TYPE_EMAIL, EMAIL_TYPE_TO,
                       EMAIL_TYPE_FROM, EMAIL_TYPE_CC, NOTE_TYPE_CONTACT,
                       NOTE_TYPE_EMAIL)

#engine = create_engine(util.getConnectionString(), echo='debug', echo_pool=True)
engine = create_engine(util.getConnectionString(), echo=False)
Base = declarative_base(engine)

class Account(Base):
    __tablename__ = 'Account'
    
    id = Column("AccountID", INTEGER, primary_key=True)
    company_id = Column("CompanyID", INTEGER)
    inactive = Column("Inactive", BIT)
    deleted = Column("Deleted", BIT)
    text01 = Column("Text01", VARCHAR)
    

class Alert(Base):
    __tablename__ = 'Alert'
    
    id = Column("AlertID", INTEGER, primary_key=True)
    alert_type_id = Column("AlertTypeID", INTEGER)
    record_id = Column("RecordID", INTEGER)
    record_type_id = Column("RecordTypeID", INTEGER)
    created_by = Column("CreatedBy", INTEGER)
    created_date = Column("CreatedDate", DATETIME)
    alert_standard = Column("AlertStandard", BIT)
    alert_email = Column("AlertEmail", BIT)
    delay_until = Column("DelayUntil", DATETIME)


class AlertRecipient(Base):
    __tablename__ = 'AlertRecipient'
    
    id = Column("AlertRecipientID", INTEGER, primary_key=True)
    alert_id = Column("AlertID", INTEGER, ForeignKey("Alert.AlertID"))
    person_id = Column("PersonID", INTEGER, ForeignKey("People.PersonID"))
    active = Column("Active", BIT)
    email_sent = Column("EmailSent", BIT)


class Company(Base):
    __tablename__ = 'Company'
    
    id = Column("CompanyID", INTEGER, primary_key=True)
    name = Column("Name",   VARCHAR)
    reminder = Column("Reminder", BIT)

    def __init__(self, id, name, reminder):
        self.id = id
        self.name = name
        self.reminder = reminder
        

class Contact(Base):
    __tablename__ = 'Contact'

    id = Column("ContactID", INTEGER, primary_key=True)
    account_id = Column("AccountID", INTEGER,
                        ForeignKey("Account.AccountID"),
                              nullable=True)
    private_contact = Column("PrivateContact", BIT)
    inactive = Column("Inactive", BIT)
    deleted = Column("Deleted", BIT)
    company_id = Column("CompanyID", INTEGER)
    modify_time = Column("ModifyTime", DATETIME)
    text01 = Column("Text01", VARCHAR)
    text02 = Column("Text02", VARCHAR)
    text03 = Column("Text03", VARCHAR)
    text04 = Column("Text04", VARCHAR)
    text05 = Column("Text05", VARCHAR)
    text06 = Column("Text06", VARCHAR)
    text07 = Column("Text07", VARCHAR)
    text08 = Column("Text08", VARCHAR)
    text09 = Column("Text09", VARCHAR)
    text10 = Column("Text10", VARCHAR)
    text11 = Column("Text11", VARCHAR)
    text12 = Column("Text12", VARCHAR)
    text13 = Column("Text13", VARCHAR)
    text14 = Column("Text14", VARCHAR)
    text15 = Column("Text15", VARCHAR)
    text16 = Column("Text16", VARCHAR)
    text17 = Column("Text17", VARCHAR)
    text18 = Column("Text18", VARCHAR)
    text19 = Column("Text19", VARCHAR)
    text20 = Column("Text20", VARCHAR)
    text21 = Column("Text21", VARCHAR)
    text22 = Column("Text22", VARCHAR)
    text23 = Column("Text23", VARCHAR)
    text24 = Column("Text24", VARCHAR)
    text25 = Column("Text25", VARCHAR)
    text26 = Column("Text26", VARCHAR)
    text27 = Column("Text27", VARCHAR)
    text28 = Column("Text28", VARCHAR)
    text29 = Column("Text29", VARCHAR)
    text30 = Column("Text30", VARCHAR)
    text31 = Column("Text31", VARCHAR)
    text32 = Column("Text32", VARCHAR)
    text33 = Column("Text33", VARCHAR)
    text34 = Column("Text34", VARCHAR)
    text35 = Column("Text35", VARCHAR)
    text36 = Column("Text36", VARCHAR)
    text37 = Column("Text37", VARCHAR)
    text38 = Column("Text38", VARCHAR)
    text39 = Column("Text39", VARCHAR)
    text40 = Column("Text40", VARCHAR)
    text41 = Column("Text41", VARCHAR)
    text42 = Column("Text42", VARCHAR)
    text43 = Column("Text43", VARCHAR)
    text44 = Column("Text44", VARCHAR)
    text45 = Column("Text45", VARCHAR)
    text46 = Column("Text46", VARCHAR)
    text47 = Column("Text47", VARCHAR)
    text48 = Column("Text48", VARCHAR)
    text49 = Column("Text49", VARCHAR)
    text50 = Column("Text50", VARCHAR)
    text51 = Column("Text51", VARCHAR)
    text52 = Column("Text52", VARCHAR)
    text53 = Column("Text53", VARCHAR)
    text54 = Column("Text54", VARCHAR)
    text55 = Column("Text55", VARCHAR)
    text56 = Column("Text56", VARCHAR)
    text57 = Column("Text57", VARCHAR)
    text58 = Column("Text58", VARCHAR)
    text59 = Column("Text59", VARCHAR)
    text60 = Column("Text60", VARCHAR)
    text61 = Column("Text61", VARCHAR)
    text62 = Column("Text62", VARCHAR)
    text63 = Column("Text63", VARCHAR)
    text64 = Column("Text64", VARCHAR)
    text65 = Column("Text65", VARCHAR)
    text66 = Column("Text66", VARCHAR)
    text67 = Column("Text67", VARCHAR)
    text68 = Column("Text68", VARCHAR)
    text69 = Column("Text69", VARCHAR)
    text70 = Column("Text70", VARCHAR)
    text71 = Column("Text71", VARCHAR)
    text72 = Column("Text72", VARCHAR)
    text73 = Column("Text73", VARCHAR)
    text74 = Column("Text74", VARCHAR)
    text75 = Column("Text75", VARCHAR)
    text76 = Column("Text76", VARCHAR)
    text77 = Column("Text77", VARCHAR)
    text78 = Column("Text78", VARCHAR)
    text79 = Column("Text79", VARCHAR)
    text80 = Column("Text80", VARCHAR)
    text81 = Column("Text81", VARCHAR)
    text82 = Column("Text82", VARCHAR)
    text83 = Column("Text83", VARCHAR)
    text84 = Column("Text84", VARCHAR)
    text85 = Column("Text85", VARCHAR)
    text86 = Column("Text86", VARCHAR)
    text87 = Column("Text87", VARCHAR)
    text88 = Column("Text88", VARCHAR)
    text89 = Column("Text89", VARCHAR)
    text90 = Column("Text90", VARCHAR)
    text91 = Column("Text91", VARCHAR)
    text92 = Column("Text92", VARCHAR)
    text93 = Column("Text93", VARCHAR)
    text94 = Column("Text94", VARCHAR)
    text95 = Column("Text95", VARCHAR)
    text96 = Column("Text96", VARCHAR)
    text97 = Column("Text97", VARCHAR)
    text98 = Column("Text98", VARCHAR)
    text99 = Column("Text99", VARCHAR)
    select01 = Column("Select01", INTEGER)
    select02 = Column("Select02", INTEGER)
    select03 = Column("Select03", INTEGER)
    select04 = Column("Select04", INTEGER)
    select05 = Column("Select05", INTEGER)
    select06 = Column("Select06", INTEGER)
    select07 = Column("Select07", INTEGER)
    select08 = Column("Select08", INTEGER)
    select09 = Column("Select09", INTEGER)
    select10 = Column("Select10", INTEGER)
    select11 = Column("Select11", INTEGER)
    select12 = Column("Select12", INTEGER)
    select13 = Column("Select13", INTEGER)
    select14 = Column("Select14", INTEGER)
    select15 = Column("Select15", INTEGER)
    select16 = Column("Select16", INTEGER)
    select17 = Column("Select17", INTEGER)
    select18 = Column("Select18", INTEGER)
    select19 = Column("Select19", INTEGER)
    select20 = Column("Select20", INTEGER)
    select21 = Column("Select21", INTEGER)
    select22 = Column("Select22", INTEGER)
    select23 = Column("Select23", INTEGER)
    select24 = Column("Select24", INTEGER)
    select25 = Column("Select25", INTEGER)
    select26 = Column("Select26", INTEGER)
    select27 = Column("Select27", INTEGER)
    select28 = Column("Select28", INTEGER)
    select29 = Column("Select29", INTEGER)
    select30 = Column("Select30", INTEGER)
    select31 = Column("Select31", INTEGER)
    select32 = Column("Select32", INTEGER)
    select33 = Column("Select33", INTEGER)
    select34 = Column("Select34", INTEGER)
    select35 = Column("Select35", INTEGER)
    select36 = Column("Select36", INTEGER)
    select37 = Column("Select37", INTEGER)
    select38 = Column("Select38", INTEGER)
    select39 = Column("Select39", INTEGER)
    select40 = Column("Select40", INTEGER)
    select41 = Column("Select41", INTEGER)
    select42 = Column("Select42", INTEGER)
    select43 = Column("Select43", INTEGER)
    select44 = Column("Select44", INTEGER)
    select45 = Column("Select45", INTEGER)
    select46 = Column("Select46", INTEGER)
    select47 = Column("Select47", INTEGER)
    select48 = Column("Select48", INTEGER)
    select49 = Column("Select49", INTEGER)
    select50 = Column("Select50", INTEGER)
    select51 = Column("Select51", INTEGER)
    select52 = Column("Select52", INTEGER)
    select53 = Column("Select53", INTEGER)
    select54 = Column("Select54", INTEGER)
    select55 = Column("Select55", INTEGER)
    select56 = Column("Select56", INTEGER)
    select57 = Column("Select57", INTEGER)
    select58 = Column("Select58", INTEGER)
    select59 = Column("Select59", INTEGER)
    select60 = Column("Select60", INTEGER)
    select61 = Column("Select61", INTEGER)
    select62 = Column("Select62", INTEGER)
    select63 = Column("Select63", INTEGER)
    select64 = Column("Select64", INTEGER)
    select65 = Column("Select65", INTEGER)
    select66 = Column("Select66", INTEGER)
    select67 = Column("Select67", INTEGER)
    select68 = Column("Select68", INTEGER)
    select69 = Column("Select69", INTEGER)
    select70 = Column("Select70", INTEGER)
    select71 = Column("Select71", INTEGER)
    select72 = Column("Select72", INTEGER)
    select73 = Column("Select73", INTEGER)
    select74 = Column("Select74", INTEGER)
    select75 = Column("Select75", INTEGER)
    select76 = Column("Select76", INTEGER)
    select77 = Column("Select77", INTEGER)
    select78 = Column("Select78", INTEGER)
    select79 = Column("Select79", INTEGER)
    select80 = Column("Select80", INTEGER)
    select81 = Column("Select81", INTEGER)
    select82 = Column("Select82", INTEGER)
    select83 = Column("Select83", INTEGER)
    select84 = Column("Select84", INTEGER)
    select85 = Column("Select85", INTEGER)
    select86 = Column("Select86", INTEGER)
    select87 = Column("Select87", INTEGER)
    select88 = Column("Select88", INTEGER)
    select89 = Column("Select89", INTEGER)
    select90 = Column("Select90", INTEGER)


class ContactMap(Base):
    __tablename__ = 'ContactMap'
    
    id = Column("ContactMapID", INTEGER, primary_key=True)
    company_id = Column("CompanyID", INTEGER)
    field_name = Column("FieldName", VARCHAR)
    is_first_name = Column("IsFirstName", BIT)
    is_last_name = Column("IsLastName", BIT)
    enabled = Column("Enable", BIT)
    option_set_id = Column("OptionSetID", INTEGER,
                           ForeignKey("Option.OptionSetID"),
                           nullable=True)
    validation_type = Column("ValidationType", INTEGER)


class AccountMap(Base):
    __tablename__ = 'AccountMap'
    
    id = Column("AccountMapID", INTEGER, primary_key=True)
    company_id = Column("CompanyID", INTEGER)
    field_name = Column("FieldName", VARCHAR)
    is_company_name = Column("IsCompanyName", BIT)


class DashboardSearch(Base):
    __tablename__ = 'DashboardSearch'
    
    id = Column("DashboardSearchID", INTEGER, primary_key=True)
    company_id = Column("CompanyID", INTEGER)
    rec_type = Column("RecType", INTEGER)
    rec_id = Column("RecID", INTEGER)
    search_text = Column("SearchText", VARCHAR)
    is_active = Column("IsActive", BIT)
    created = Column("CreatedTimestamp", DATETIME)
    modified = Column("ModifiedTimestamp", DATETIME)


class Reminder(Base):
    __tablename__ = 'Reminder'
    
    id = Column("ReminderID", INTEGER, primary_key=True)
    company_id = Column("CompanyID", INTEGER)
    enabled = Column("Enabled", BIT)
    account_address1_field = Column("AccountAddress1Field", VARCHAR)
    account_address2_field = Column("AccountAddress2Field", VARCHAR)
    account_city_field = Column("AccountCityField", VARCHAR)
    account_state_field = Column("AccountStateField", VARCHAR)
    account_zip_field = Column("AccountZipField", VARCHAR)
    account_phone_field = Column("AccountPhoneField", VARCHAR)
    account_fax_field = Column("AccountFaxField", VARCHAR)
    account_url_field = Column("AccountUrlField", VARCHAR)
    contact_address1_field = Column("ContactAddress1Field", VARCHAR)
    contact_address2_field = Column("ContactAddress2Field", VARCHAR)
    contact_city_field = Column("ContactCityField", VARCHAR)
    contact_state_field = Column("ContactStateField", VARCHAR)
    contact_zip_field = Column("ContactZipField", VARCHAR)
    contact_phone_field = Column("ContactPhoneField", VARCHAR)


class Option(Base):
    __tablename__ = 'Option'
    
    id = Column("OptionID", INTEGER, primary_key=True)
    name = Column("OptionName", VARCHAR)
    option_set_id = Column("OptionSetID", INTEGER,
                           ForeignKey("OptionSet.OptionSetID"),
                           nullable=False)

class OptionSet(Base):
    __tablename__ = 'OptionSet'
    
    id = Column("OptionSetID", INTEGER, primary_key=True)
    
    
class ExchangeCompanySettings(Base):
    __tablename__ = 'ExchangeCompanySettings'
    
    id = Column("ExchangeCompanySettingsID", INTEGER, primary_key=True)
    company_id = Column("CompanyID", INTEGER)
    enabled = Column("Enabled", BIT)
    domain = Column("Domain", VARCHAR)
    uri = Column("Uri", VARCHAR)
    

class ExchangeUserSettings(Base):
    __tablename__ = 'ExchangeUserSettings'
    
    id = Column("ExchangeUserSettingsID", INTEGER, primary_key=True)
    exchange_company_settings_id = Column("ExchangeCompanySettingsID", INTEGER)
    person_id = Column("PersonID", INTEGER)
    username = Column("Username", VARCHAR)
    password = Column("Password", VARCHAR)
    enabled = Column("Enabled", BIT)
    last_sync = Column("LastSync", DATETIME)
    account = Column ("Account", VARCHAR)
 

class ExchangeMap(Base):
    __tablename__ = 'ExchangeMap'
    
    id = Column("ExchangeMapID", INTEGER, primary_key=True)
    
    company_id = Column("CompanyID", INTEGER)
    given_name = Column("GivenName", VARCHAR)
    surname = Column("Surname", VARCHAR)
    email_address_1 = Column("EmailAddress1", VARCHAR)
    email_address_2 = Column("EmailAddress2", VARCHAR)
    email_address_3 = Column("EmailAddress3", VARCHAR)
    job_title = Column("JobTitle", VARCHAR)
    home_address = Column("HomeAddress", VARCHAR)
    home_city = Column("HomeCity", VARCHAR)
    home_state = Column("HomeState", VARCHAR)
    home_zip = Column("HomeZip", VARCHAR)
    home_country = Column("HomeCountry", VARCHAR)
    business_address = Column("BusinessAddress", VARCHAR)
    business_city = Column("BusinessCity", VARCHAR)
    business_state = Column("BusinessState", VARCHAR)
    business_zip = Column("BusinessZip", VARCHAR)
    business_country = Column("BusinessCountry", VARCHAR)
    other_address = Column("OtherAddress", VARCHAR)
    other_city = Column("OtherCity", VARCHAR)
    other_state = Column("OtherState", VARCHAR)
    other_zip = Column("OtherZip", VARCHAR)
    other_country = Column("OtherCountry", VARCHAR)
    assistant_phone = Column("AssistantPhone", VARCHAR)
    business_fax = Column("BusinessFax", VARCHAR)
    business_phone = Column("BusinessPhone", VARCHAR)
    business_phone_2 = Column("BusinessPhone2", VARCHAR)
    company_main_phone = Column("CompanyMainPhone", VARCHAR)
    home_fax = Column("HomeFax", VARCHAR)
    home_phone = Column("HomePhone", VARCHAR)
    home_phone_2 = Column("HomePhone2", VARCHAR)
    mobile_phone = Column("MobilePhone", VARCHAR)
    other_fax = Column("OtherFax", VARCHAR)
    other_telephone = Column("OtherTelephone", VARCHAR)
    pager = Column("Pager", VARCHAR)
    primary_phone = Column("PrimaryPhone", VARCHAR)


class ExchangeLog(Base):
    __tablename__ = 'ExchangeLog'
    
    id = Column("ExchangeLogID", INTEGER, primary_key=True)
    exchange_user_settings_id = Column("ExchangeUserSettingsID", INTEGER)
    record_type_id = Column("ExchangeRecordTypeID", INTEGER)
    record_id = Column("RecordID", INTEGER)
    item_id = Column("ItemID", VARCHAR)
    change_key = Column("ChangeKey", VARCHAR)
    modified = Column("Modified", DATETIME)


class ExchangeRecordType(Base):
    __tablename__ = 'ExchangeRecordType'
    
    id = Column("ExchangeRecordTypeID", INTEGER, primary_key=True)
    type = Column("Type", VARCHAR)
    

class ImapCompanySettings(Base):
    __tablename__ = 'ImapCompanySettings'

    id = Column("ImapCompanySettingsID", INTEGER, primary_key=True)
    company_id = Column("CompanyID", INTEGER)
    enabled = Column("Enabled", BIT)

    def __init__(self, id, company_id, enabled):
        self.id = id
        self.company_id = company_id
        self.enabled = enabled

    def __repr__(self):
        return "<CompanySettings - '%d': - '%d'>" % (self.id, self.company_id)


class ImapUserSettings(Base):
    __tablename__ = 'ImapUserSettings'

    id = Column("ImapUserSettingsID", INTEGER, primary_key=True)
    server_id = Column("ImapServerID", INTEGER)
    enabled = Column("Enabled", BIT)
    person_id = Column("PersonID", INTEGER)
    username = Column("ImapUsername", VARCHAR)
    password = Column("ImapPassword", VARCHAR)
    last_sync = Column("LastSync", DATETIME)
    company_id = Column("CompanyID", INTEGER)

    def __init__(self, id, server_id, enabled, person_id, username, password,
                 last_sync, company_id):
        self.id = id
        self.server_id = server_id
        self.enabled = enabled
        self.person_id = person_id
        self.username = username
        self.password = password
        self.last_sync = last_sync
        self.company_id = company_id

    def __repr__(self):
        return "<UserSettings - '%s': '%s' - '%s'>" % (self.id, self.username,
                                                       self.last_sync)


class ImapServer(Base):
    __tablename__ = 'ImapServer'

    id = Column("ImapServerID", INTEGER, primary_key=True)
    address = Column("Address", VARCHAR)
    port = Column("Port", INTEGER)
    ssl = Column("SSL", BIT)
    private = Column("Private", BIT)
    imap_server_type_id = Column("ImapServerTypeID", INTEGER)

    def __init__(self, id, address, port, ssl, private,
                 imap_server_type_id):
        self.id = id
        self.address = address
        self.port = port
        self.ssl = ssl
        self.private = private
        self.imap_server_type_id = imap_server_type_id


class EmailAddress(Base):
    __tablename__ = 'EmailAddresses'
    
    id = Column("EmailAddressID", INTEGER, primary_key=True)
    email_content_id = Column("EmailContentID", INTEGER)
    contact_id = Column("ContactID", INTEGER)
    email_address_type = Column("EmailAddressType", VARCHAR)
    email_address = Column("EmailAddress", VARCHAR)
    person_id = Column("PersonID", INTEGER)
    display_name = Column("DisplayName", VARCHAR)
    
    def __init__(self, email_content_id=None, contact_id=None,
                 email_address_type=None, email_address=None,
                 person_id=None, display_name=None):
        self.email_content_id = email_content_id
        self.contact_id = contact_id
        self.email_address_type = email_address_type
        self.email_address = email_address
        self.display_name = display_name


class EmailContent(Base):
    __tablename__ = 'EmailContent'

    id = Column("EmailContentID", INTEGER, primary_key=True)
    person_id = Column("PersonID", INTEGER)
    entry_id = Column("EntryID", VARCHAR)
    from_name = Column("FromName", VARCHAR)
    from_email = Column("FromEmail", VARCHAR)
    recipient_names = Column("RecipientNames", VARCHAR)
    recipient_emails = Column("RecipientEmails", VARCHAR)
    cc_names = Column("CcNames", VARCHAR)
    cc_emails = Column("CcEmails", VARCHAR)
    received_date = Column("ReceivedDate", DATETIME)
    subject = Column("Subject", Unicode)
    body = Column("Body", Unicode)
    company_id = Column("CompanyID", INTEGER)

    def __init__(self, person_id=None, entry_id=None, from_name=None,
                 from_email=None, recipient_names=None, recipient_emails=None,
                 cc_names=None, cc_emails=None, received_date=None,
                 subject=None, body=None, company_id=None):
        self.person_id = person_id
        self.entry_id = entry_id
        self.from_name = from_name
        self.from_email = from_email
        self.recipient_names = recipient_names
        self.recipient_emails = recipient_emails
        self.cc_names = cc_names
        self.cc_emails = cc_emails
        self.received_date = received_date
        self.subject = subject
        self.body = body
        self.company_id = company_id

    def __repr__(self):
        return "<EmailContent - '%d' - '%s'>" % (self.person_id, self.entry_id)


class EmailAttachment(Base):
    __tablename__ = 'EmailAttachments'

    id = Column("EmailAttachmentID", INTEGER, primary_key=True)
    email_content_id = Column("EmailContentID", INTEGER,
                              ForeignKey("EmailContent.EmailContentID"),
                              nullable=False)
    original_name = Column("OriginalName", Unicode)
    stored_name = Column("StoredName", Unicode)

    def __init__(self, original_name, email_content_id=None):
        self.original_name = original_name.replace(" ", "_")
        self.email_content_id = email_content_id

    def rename(self, entry_id):
        self.stored_name = entry_id + self.original_name


class People(Base):
    __tablename__ = 'People'
    
    id = Column("PersonID", INTEGER, primary_key=True)
    company_id = Column("CompanyID", INTEGER)
    deleted = Column("Deleted", BIT)
    email = Column("Email", VARCHAR)
    level = Column("Level", INTEGER)
    supervisor_id = Column("SupervisorID", INTEGER)
    first_name = Column("FirstName", VARCHAR)
    last_name = Column("LastName", VARCHAR)


class PeopleContact(Base):
    __tablename__ = 'PeopleContact'
    
    id = Column("PeopleContactID", INTEGER, primary_key=True)
    contact_id = Column("ContactID", INTEGER, ForeignKey('Contact.ContactID'))
    person_id = Column("PersonID", INTEGER)
    created_by = Column("CreatedBy", BIT)
    assigned_to = Column("AssignedTo", BIT)
    

class PeopleEmailAddr(Base):
    __tablename__ = 'PeopleEmailAddr'
    
    id = Column("PeopleEmailAddrID", INTEGER, primary_key=True)
    person_id = Column("PersonID", INTEGER)
    company_id = Column("CompanyID", INTEGER)
    email = Column("EmailAddr", VARCHAR)
    is_primary = Column("IsPrimaryEmailAddr", BIT)


class Note(Base):
    __tablename__ = 'Notes'

    id = Column("NoteID", INTEGER, primary_key=True)
    created_by = Column("CreatedBy", INTEGER)
    creation_date = Column("CreationDate", DATETIME)
    subject = Column("Subject", VARCHAR)
    note_text = Column("NoteText", TEXT)
    object_type = Column("ObjectType", SMALLINT)
    object_referrer = Column("ObjectReferer", INTEGER)
    email_content_id = Column("EmailContentID", INTEGER)
    email_address_type = Column("EmailAddressType", VARCHAR)
    note_special_type = Column("NoteSpecialType", INTEGER)
    private_note = Column("PrivateNote", BIT)
    company_id = Column("CompanyID", INTEGER)
    created_timezone = Column("CreatedTimezone", VARCHAR)
    
    def __init__(self, created_by=None, creation_date=None, subject=None,
                 note_text=None, object_type=None, object_referrer=None,
                 email_content_id=None, email_address_type=None,
                 note_special_type=None, private_note=None, company_id=None,
                 created_timezone=None):
        self.created_by = created_by
        self.creation_date = creation_date
        self.subject = subject
        self.note_text = note_text
        self.object_type = object_type
        self.object_referrer = object_referrer
        self.email_content_id = email_content_id
        self.email_address_type = email_address_type
        self.note_special_type = note_special_type
        self.private_note = private_note
        self.company_id = company_id
        self.created_timezone = created_timezone
        

class Event(Base):
    __tablename__ = 'Event'
  
    id = Column("EventID", INTEGER, primary_key=True)
    event_type_id = Column("EventTypeID", INTEGER,
                           ForeignKey('EventType.EventTypeID'))
    company_id = Column("CompanyID", INTEGER)
    contact_id = Column("ContactID", INTEGER)
    subject = Column("Subject", VARCHAR)
    location = Column("Location", VARCHAR)
    start_date_utc = Column("StartDateUTC", DATETIME)
    end_date_utc = Column("EndDateUTC", DATETIME)
    cancelled = Column("Cancelled", BIT)
    closed = Column("Closed", BIT)
    private = Column("Private", BIT)
    repeat_type = Column("RepeatType", INTEGER)
    repeat_interval = Column("RepeatInterval", VARCHAR)
    repeat_end_date_utc = Column("RepeatEndDateUTC", DATETIME)
    repeat_exceptions = Column("RepeatExceptions", TEXT)
    repeat_parent = Column("RepeatParent", INTEGER)


class EventAttendee(Base):
    __tablename__ = 'EventAttendee'
    
    id = Column("EventAttendeeID", INTEGER, primary_key=True)
    event_id = Column("EventID", INTEGER, ForeignKey('Event.EventID'))
    person_id = Column("PersonID", INTEGER)
    creator = Column("Creator", BIT)
    deleted = Column("Deleted", BIT)
 
   
class EventType(Base):
    __tablename__ = 'EventType'
    
    id = Column("EventTypeID", INTEGER, primary_key=True)
    event_type = relationship("Event", backref="event_type")
    company_id = Column("CompanyID", INTEGER)
    event_name = Column("EventName", VARCHAR)
    type = Column("Type", VARCHAR)


def load_session():
    Session = sessionmaker(bind=engine)
    session = Session()
    return session


def get_companies():
    session = load_session()
    companies = session.query(ImapCompanySettings). \
        filter(ImapCompanySettings.enabled == True).all()
    session.close()
    return companies


def get_asg():
    session = load_session()
    companies = session.query(ImapCompanySettings). \
        filter(ImapCompanySettings.company_id == 612).all()
    session.close()
    return companies


def get_users(company_id):
    session = load_session()
    users = session.query(ImapUserSettings). \
        filter(ImapUserSettings.enabled == True). \
        filter(ImapUserSettings.company_id == company_id).all()
    session.close()
    return users


def get_compliance_user(company_id):
    session = load_session()
    users = session.query(ImapUserSettings). \
        filter(ImapUserSettings.company_id == company_id).all()
    session.close()
    return users


def get_thinktin_user(company_id):
    session = load_session()
    users = session.query(ImapUserSettings). \
        filter(ImapUserSettings.company_id == company_id).all()
    session.close()
    return users


def get_server(server_id):
    session = load_session()
    server = session.query(ImapServer).filter(ImapServer.id == server_id).one()
    session.close()
    return server


def set_last_sync(user_settings, updated_datetime):
    session = load_session()
    session.add(user_settings)
    user_settings.last_sync = updated_datetime
    session.commit()
    session.close()


def get_contact_email_fields(company_id):
    session = load_session()
    fields = session.query(ContactMap). \
        filter(ContactMap.company_id == company_id). \
        filter(ContactMap.enabled == True). \
        filter(ContactMap.validation_type == VALIDATION_TYPE_EMAIL).all()
    field_list = []
    for field in fields:
        field_list.append(field.field_name)
    session.close()
    return field_list


def get_person_id(company_id, email):
    session = load_session()
    person = session.query(People). \
        filter(People.company_id == company_id). \
        filter(People.email == email).first()
    if person is not None:
        session.close()
        return person.id
    people_email_addr = session.query(PeopleEmailAddr). \
        filter(PeopleEmailAddr.company_id == company_id). \
        filter(PeopleEmailAddr.email == email).first()
    if people_email_addr is not None:
        session.close()
        return people_email_addr.person_id
    session.close()
    return 0


def get_contact_id(company_id, email, contact_email_fields, compliance=False):
    if not email:
        return 0
    if compliance:
        return 434510    
    session = load_session()

    args = ({'company_id': company_id})
    for field in contact_email_fields:
        args[field] = email
        if contact_email_fields.index(field) == 0:
            sql = field + '=:' + field
        else:
            sql += ' OR ' + field + '=:' + field
    raw = "SELECT ContactID FROM Contact " \
        "WHERE CompanyID=:company_id AND Deleted = 0 AND Inactive = 0 AND (" + sql + ")"
    contact_id = session.query("ContactID"). \
        from_statement(raw).params(args).all()
    session.close()

    if(contact_id):
        t = contact_id[0]
        return t[0]
    else:
        return 0 


def get_email_address_list(display_names, email_addresses):
    names = display_names
    if(display_names == ' '):
        names = display_names.replace(" ", "")
    names = names.split(";")
    names = filter(None, names)
    addresses = email_addresses.replace(" ", "")
    addresses = addresses.split(";")
    addresses = filter(None, addresses)
    return dict(zip(names, addresses))


def get_email_type(email_content):
    type = ''
    from_address = get_email_address_list(email_content.from_name,
                                          email_content.from_email)
    if from_address:
        type = EMAIL_TYPE_FROM
    
    recipient_addresses = get_email_address_list(email_content.recipient_names,
                                                 email_content.recipient_emails)
    if recipient_addresses:
        type = EMAIL_TYPE_TO
    
    cc_addresses = get_email_address_list(email_content.cc_names,
                                          email_content.cc_emails)
    if cc_addresses:
        type = EMAIL_TYPE_CC

    return type


def save_contacts(email_content, compliance=False):
    from_address = get_email_address_list(email_content.from_name,
                                          email_content.from_email)
    if from_address:
        save_contact(email_content, from_address, EMAIL_TYPE_FROM, compliance)
    
    recipient_addresses = get_email_address_list(email_content.recipient_names,
                                                 email_content.recipient_emails)
    if recipient_addresses:
        save_contact(email_content, recipient_addresses, EMAIL_TYPE_TO, compliance)
    
    cc_addresses = get_email_address_list(email_content.cc_names,
                                          email_content.cc_emails)
    if cc_addresses:
        save_contact(email_content, cc_addresses, EMAIL_TYPE_CC, compliance)


def save_contact(email_content, address_list, type, compliance):
    session = load_session()
    fields = get_contact_email_fields(email_content.company_id)
    for name, address in address_list.items():
        print name + '|' + address + '|' + type
        if compliance and address != 'compliance@asgib.com':
            continue
        email_address = EmailAddress()
        email_address.email_content_id = email_content.id
        email_address.contact_id = get_contact_id(email_content.company_id,
                                                  address, fields, compliance)
        email_address.email_address_type = type
        email_address.email_address = address
        email_address.person_id = get_person_id(email_content.company_id,
                                                address)
        email_address.display_name = name
        session.add(email_address)
        session.flush()
    
    session.commit()
    session.close()


def get_recipients(email_content):
    to_list = get_email_address_list(email_content.recipient_names,
                                     email_content.recipient_emails)
    cc_list = get_email_address_list(email_content.cc_names,
                                     email_content.cc_emails)
    recipient_list = dict(to_list.items() + cc_list.items())
    recipients = {}
    for recipient_address in recipient_list.values():
        recipients[recipient_address] = get_person_id(email_content.company_id,
                                     recipient_address)
    return recipients    


def create_note(email_content, compliance=False):
    session = load_session()
    fields = get_contact_email_fields(email_content.company_id)
    from_person_id = get_person_id(email_content.company_id,
                              email_content.from_email)
    from_contact_id = get_contact_id(email_content.company_id,
                                email_content.from_email, fields, compliance)
    if compliance:
        note = Note(created_by=9518,
                    creation_date=email_content.received_date,
                    note_text='Email',
                    subject='Email',
                    object_type=NOTE_TYPE_CONTACT,
                    note_special_type=NOTE_TYPE_EMAIL,
                    company_id=email_content.company_id,
                    object_referrer=434510,
                    email_content_id=email_content.id,
                    email_address_type=get_email_type(email_content),
                    private_note=1
                    )
        session.add(note)

    elif from_person_id is not 0 and not compliance:
        is_private = 1
        recipients = get_recipients(email_content)

        if 0 in recipients.itervalues():
            is_private = 0
        
        for recipient in recipients.iterkeys():
            recipient_contact_id = get_contact_id(email_content.company_id,
                                                  recipient, fields, compliance)
            if recipient_contact_id is not 0:
                note = Note(created_by=recipients[recipient],
                            creation_date=email_content.received_date,
                            note_text='Email',
                            subject='Email',
                            object_type=NOTE_TYPE_CONTACT,
                            note_special_type=NOTE_TYPE_EMAIL,
                            company_id=email_content.company_id,
                            object_referrer=recipient_contact_id,
                            email_content_id=email_content.id,
                            email_address_type=get_email_type(email_content),
                            private_note=is_private
                            )
                session.add(note)
            
    elif from_contact_id is not 0 and not compliance:
        note = Note(created_by=0,
                    creation_date=email_content.received_date,
                    note_text='Email',
                    subject='Email',
                    object_type=NOTE_TYPE_CONTACT,
                    note_special_type=NOTE_TYPE_EMAIL,
                    company_id=email_content.company_id,
                    object_referrer=from_contact_id,
                    email_content_id=email_content.id,
                    email_address_type=get_email_type(email_content),
                    private_note=0
                    )
        session.add(note)
     
    session.commit()
    session.close()


class EmailUtil():
    
    def get_existing_entry(self, entry_id, person_id):
        session = load_session()
        entry = session.query(EmailContent). \
            filter(EmailContent.entry_id == entry_id). \
            filter(EmailContent.person_id == person_id).all()
        return entry


def save(email_content, attachments, compliance=False):
    session = load_session()
    
    email_util = EmailUtil()
    existing = email_util.get_existing_entry(email_content.entry_id,
                                             email_content.person_id)
    if not existing:
        print 'email does not exist'
        session.add(email_content)
        session.flush()
        for attachment in attachments:
            attachment.email_content_id = email_content.id
            session.add(attachment)
        try:
            session.flush()
            session.commit()
            save_contacts(email_content, compliance)
            create_note(email_content, compliance)
        except Exception, e:
            print 'ERROR: Problem saving email:'
            print email_content.entry_id
            #raise e
            session.rollback()
        
    session.close()
    print 'done'

if __name__ == "__main__":
    pass


