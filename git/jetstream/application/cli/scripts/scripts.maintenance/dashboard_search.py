#!/usr/bin/python
from datetime import datetime, timedelta
from db import DashboardSearch, load_session

class DashboardSearchCreator:
    '''Add or update DashboardSearch records'''
    
    def __init__(self, company_id, rec_type, rec_id, search_text):
        
        self.company_id = company_id
        self.rec_type = rec_type
        self.rec_id = rec_id
        self.search_text = search_text


    def save(self):

        local = datetime.now() - timedelta(hours=5)
        
        session = load_session()
        search_records = (session.query(DashboardSearch).
                         filter(DashboardSearch.rec_id == self.rec_id).
                         filter(DashboardSearch.rec_type == self.rec_type).
                         filter(DashboardSearch.company_id == self.company_id).
                         all())
        if search_records:
            search_record = search_records[0]
            search_record.modified = local
        else:
            search_record = DashboardSearch()
            search_record.is_active = True
            search_record.rec_type = self.rec_type
            search_record.rec_id = self.rec_id
            search_record.company_id = self.company_id
            search_record.created = local
            session.add(search_record)
        search_record.search_text = self.search_text
        session.commit()
        session.close()

