#!/usr/bin/python
from constants import SEARCH_TYPE_ACCOUNT
from db import Account, load_session
from dashboard_search import DashboardSearchCreator

class AccountUtil:
    
    def __init__(self, company_id=None):
        self.company_id = company_id

    def get_account_id(self, account_name):
        account_id = self.find_account_id(account_name)
        
        if account_id:
            return account_id
        else:
            return self.create_account(account_name)
        
    
    def find_account_id(self, account_name):
        session = load_session()
        accounts = (session.query(Account).
                    filter(Account.company_id == self.company_id).
                    filter(Account.deleted == False).
                    filter(Account.inactive == False).
                    filter(Account.text01 == account_name).all())
        session.close()
        if accounts:
            if len(accounts) > 1:
                print ('WARN: More than one account matches criteria "' +
                       account_name + '" (' + str(len(accounts)) + ')')
            return accounts[0].id


    def create_account(self, account_name):
        session = load_session()
        account = Account()
        account.company_id = self.company_id
        account.deleted = False
        account.inactive = False
        account.text01 = account_name
        session.add(account)
        session.commit()
        account_id = account.id
        session.close()
        creator = DashboardSearchCreator(self.company_id, SEARCH_TYPE_ACCOUNT,
                                         account_id, account_name)
        creator.save()
        return account_id


    def get_account(self, account_id):
        print 'referrer'
        print account_id
        session = load_session()
        account = (session.query(Account).
                   filter(Account.id == account_id).one())
        
        session.close()
        return account.text01
        
        
        

