#!/usr/bin/python

import datetime
from Cheetah.Template import Template
from sqlalchemy import or_

from smtp import send_email
from db import Alert, AlertRecipient, People, Note, load_session
from reminder import PersonUtil, ContactUtil, EventUtil
from account import AccountUtil
from constants import (ALERT_SUBJECT_BASIC, NOTE_TYPE_ACCOUNT,
                       NOTE_TYPE_CONTACT, NOTE_TYPE_EVENT,
                       NOTE_TYPE_OPPORTUNITY, UTC_OFFSET)


class AlertUtil():
    """"""
    def __init__(self):
        self.email_alerts = []
        self.person_util = PersonUtil()


    def setup(self):
        """"""
        session = load_session()
        alerts = (session.query(Alert).
                   join(AlertRecipient).
                       filter(Alert.alert_email == True).
                       filter(or_(Alert.delay_until <= datetime.datetime.now(),
                                  Alert.delay_until == None)).
                       filter(AlertRecipient.email_sent == False).all())
        for alert in alerts:
            note = (session.query(Note).
                    filter(Note.id == alert.record_id).one())
            recipients = (session.query(AlertRecipient, People).
                          join(People).
                              filter(AlertRecipient.alert_id == alert.id).
                              filter(AlertRecipient.email_sent == False).all())
            email_alert = EmailAlert(alert=alert, note=note,
                                     recipients=recipients)
            self.email_alerts.append(email_alert)
        session.close()


    def build_reference(self, alert):
        company_id = alert.note.company_id
        account_util = AccountUtil()
        contact_util = ContactUtil()
        event_util = EventUtil()
        
        note_type = ''
        account_name = ''
        contact_name = ''
        
        if alert.note.object_type == NOTE_TYPE_ACCOUNT:
            note_type = 'Company'
            account_name = account_util.get_account(alert.note.object_referrer)
            contact_name = ''
        elif alert.note.object_type == NOTE_TYPE_CONTACT:
            note_type = 'Contact'
            contact = (contact_util.get_contact(alert.note.object_referrer))
            if contact:
                contact_name = contact.text01 + ' ' + contact.text02
                account_name = contact_util.get_account_name(company_id,
                                                             contact.id)
        elif alert.note.object_type == NOTE_TYPE_EVENT:
            note_type = 'Event'
            event_contact_id = (event_util.
                                get_event_contact(alert.note.object_referrer))
            if event_contact_id:
                contact = (contact_util.get_contact(event_contact_id))
                if contact:
                    contact_name = contact.text01 + ' ' + contact.text02
                    account_name = contact_util.get_account_name(company_id,
                                                                 contact.id)
        elif alert.note.object_type == NOTE_TYPE_OPPORTUNITY:
            note_type = 'Opportunity'
        if not note_type:
            raise NameError('Unknown Note Type')
        
        reference = {'note_type' : note_type,
                     'account_name' : account_name,
                     'contact_name' : contact_name}
        return reference


    def mark(self, recipient):
        print 'marking'
        session = load_session()
        session.add(recipient)
        recipient.email_sent = True
        session.commit()
        session.close()


    def format(self, sender, subject, body, reference):
        output = Template(file='/usr/local/templates/note_alert.tmpl',
                          searchList=[{'sender' : sender,
                                       'subject' : subject,
                                       'body' : body,
                                       'reference' : reference}])
        return str(output)
        

    def flush(self):
        start_time = (datetime.datetime.now() -
                      datetime.timedelta(hours=UTC_OFFSET))
        print ('Alert flush begins: ' + 
               start_time.strftime("%m/%d/%Y %H:%M:%S %p"))
        self.setup()
        for alert in self.email_alerts:
            sender = (self.person_util.
                      get_salesperson_name(alert.alert.created_by))
            alert_subject = alert.note.subject
            alert_body = alert.note.note_text
            reference = self.build_reference(alert)
            subject = ALERT_SUBJECT_BASIC
            body = self.format(sender, alert_subject, alert_body, reference)
            for recipient, person in alert.recipients:
                to = person.email
                print to
                print alert.alert.id
                send_email(to, subject, body, user='alert@asasales.com',
                           sender='Jetstream Note Alert! <alert@jetstreamcrm.com>',
                           priority='high')
                self.mark(recipient)


class EmailAlert():
    
    def __init__(self, alert, note, recipients):
        self.alert = alert
        self.note = note
        self.recipients = recipients


if __name__ == "__main__":
    
    alert_util = AlertUtil()
    alert_util.flush()

