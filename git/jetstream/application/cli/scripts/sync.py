#!/usr/bin/python
import email
import imaplib
import os
import re
import string
from pprint import pprint
from BeautifulSoup import BeautifulSoup
from email.mime.image import MIMEImage
from email.parser import HeaderParser
from email.utils import getaddresses
from datetime import datetime, timedelta
from time import localtime, strftime, mktime
from constants import IMAP_SERVER_TYPE_EXCHANGE, IMAP_SERVER_TYPE_GMAIL, IMAP_SERVER_TYPE_FUSEMAIL, OAUTH_KEY, OAUTH_SECRET
import db

import oauth2
#import oauth2.clients.imap as oauth_imaplib

import base64
import sys

def parse_addresses(msg, type):
    """
    Builds 2 semi-colon separated strings for database insertion
    1 containing display names, 1 containing email addresses
    """
    names, emails = [], []
    raw = msg.get_all(type, [])
    addresses = getaddresses(raw)
    for address in addresses:
        name, addr = address
        if not name:
            name = addr
        names.append(name)
        emails.append(addr)
    return ";".join(names),";".join(emails)

# https://code.google.com/p/google-mail-oauth2-tools/wiki/OAuth2DotPyRunThrough
def GenerateOAuth2String(username, access_token, base64_encode=True):
    auth_string = 'user=%s\1auth=Bearer %s\1\1' % (username, access_token)
    if base64_encode:
        auth_string = base64.b64encode(auth_string)
    return auth_string

def sync(user, server, google_sync):

    local = datetime.today() - timedelta(hours=6)
    today = local.strftime("%d-%b-%Y")
    start_time = datetime.now() - timedelta(hours=6)

    if google_sync:
        label = 'Google'
    else:
        label = 'IMAP'
        # If this is the first run, just get messages from the last hour
        if not user.last_sync:
            user.last_sync = (datetime.fromtimestamp(mktime(localtime())) - timedelta(hours=36))

    print
    print
    print label + ' Sync begins : ' + start_time.strftime("%m/%d/%Y %H:%M:%S %p")
    
    login_error = False
    google_error = False
    if google_sync:
        try:
            googletoken = db.get_google_token(gUser)
        except Exception, e:
            googletoken = False

        if False != googletoken:
            user.last_sync = googletoken.email_processed
            user.person_id = gUser

            print gUser
            #print 'Google with ' + (str)gUser

            # If this is the first run, just get messages from the last hour
            if not user.last_sync:
                user.last_sync = (datetime.fromtimestamp(mktime(localtime())) - timedelta(hours=36))

            refresh_token = googletoken.refresh_token
            google_email = googletoken.email

            if refresh_token != '':

                print 'OAUTH KEY : ' + OAUTH_KEY
                print 'OAUTH_SECRET : ' + OAUTH_SECRET
                print 'Refresh Token : ' + googletoken.refresh_token
                response = oauth2.RefreshToken(OAUTH_KEY, OAUTH_SECRET, refresh_token)

                #print response
                access_token = response['access_token']
                print 'Access Email : ' + googletoken.email

                if access_token != '':
                    print 'Get Access Token Success : ' + access_token
                    auth_str = GenerateOAuth2String(googletoken.email, access_token, base64_encode=False)
                    print 'AUTH STR : ' + auth_str
                    m = imaplib.IMAP4_SSL('imap.gmail.com')
                    #m.debug = 4
                    try:
                        m.authenticate('XOAUTH2', lambda x: auth_str)
                    except Exception, e:
                    #except imaplib.IMAP4.error, e:
                        print 'Authentication Error'
                        print e.args
                        print repr(e)
                        login_error = True
                        google_error = True
                        sys.exit(-1)
                 
                else:
                    print 'Fail to generate access_token with refresh_token'
                    print 'Account: ' + user.username
                    login_error = True
                    google_error = True

            else:
                print 'No refresh_token in DB. User should access Google oauth first'
                print 'Account: ' + user.username
                login_error = True
                google_error = True
        else:
            print 'No oauth_token in DB. User should access Google oauth first'
            print 'Account: ' + user.username
            login_error = True
            google_error = True


    # if oauth Fail, try with IMAP way
    #if  google_sync == False or google_error:
    if  google_sync == False:
        if server.ssl:
            print 'ssl'
            m = imaplib.IMAP4_SSL(server.address, server.port)
        else:
            m = imaplib.IMAP4(server.address, server.port)

        #login_error = False
        try:
            m.login(user.username, user.password)
        except Exception, e:
            print 'Problem logging into server. Incorrect password?'
            print 'Account: ' + user.username
            login_error = True
 
    if login_error :
        return False

    if not login_error:

        typ, data = m.list()
        print 'Response code:', typ
        print 'Response:'
        pprint(data)

        print 'Syncing user: ' + user.username
        print
        print 'Last sync: ' + str(user.last_sync)

        for line in data :
            matchObj = re.search( r'Sent', line, re.M|re.I)
            if matchObj :
                s = line.split()
                for box in s :
                    boxObj = re.search ( r'[Gmail]', box, re.M|re.I)
                    if boxObj :
                        print box

        # Sent mail
        if not server:
            m.select(box)
            #m.select('[Gmail]/Sent Mail')
            #m.select('[Gmail]/&vPSwuNO4ycDVaA-')
        else:
            if server.imap_server_type_id == IMAP_SERVER_TYPE_GMAIL:
                m.select('[Gmail]/Sent Mail')
            elif server.imap_server_type_id == IMAP_SERVER_TYPE_EXCHANGE:
                m.select('Sent Items')
            elif server.imap_server_type_id == IMAP_SERVER_TYPE_FUSEMAIL:
                status, msgs = m.select('INBOX.Sent Items')
            else:
                m.select('Sent Items')

        try: 
            resp, sent = m.search(None, '(SINCE "' + today + '")')
            print resp
            print sent
            sent = sent[0].split()
            print 'Sent mail: '
            print sent
            traverse(m, user, sent)
        except Exception, e:
            print 'ERROR'
            #raise e
            print e.args
        
        # Received mail
        m.select(readonly=True)
        resp, items = m.search(None, '(SINCE "' + today + '")') #@UnusedVariable
        items = items[0].split()
        print 'Received mail: '
        print items
        traverse(m, user, items)
 
        # Server is set to UTC
        local = datetime.now() - timedelta(hours=6) 
        if google_sync == True :
            db.set_google_last_sync(user, local)
        else:
            db.set_last_sync(user, local)


def traverse(m, user, items):
    ### directory where to save attachments (default: current)
    detach_dir = '/webtemp/attachments/'

    # Translation table
    table = ''.join(' ' if n < 32 or n > 126 else chr(n) for n in xrange(256))

    for emailid in items:
        header = m.fetch(emailid, '(BODY.PEEK[HEADER])')
        parser = HeaderParser()
        msg = parser.parsestr(header[1][0][1])
        received = datetime.fromtimestamp(email.utils.mktime_tz(email.utils.parsedate_tz(msg['Date']))) #@UndefinedVariable
        received -= timedelta(hours=6)
        print 'Received: ' + str(received)

        if ( received > user.last_sync ):
            print 'new mail!'
            email_content = db.EmailContent()
            email_content.recipient_names, email_content.recipient_emails = parse_addresses(msg, 'to')
            email_content.cc_names, email_content.cc_emails = parse_addresses(msg, 'cc')
            email_content.from_name, email_content.from_email = parse_addresses(msg, 'from')
            received_date = email.utils.parsedate_tz(msg['Date'])[0:9]
            email_content.received_date = re.sub(r"\b0", "", strftime("%m/%d/%Y %H:%M:%S %p", received_date))
            if not msg['Message-ID']:
                continue
            email_content.entry_id = email.utils.unquote(msg['Message-ID']) #@UndefinedVariable
            email_content.subject = msg['Subject']
            email_content.person_id = user.person_id
            email_content.company_id = user.company_id
            #resp, data = m.fetch(emailid, "(RFC822)") #@UnusedVariable
            resp, data = m.fetch(emailid, "(BODY.PEEK[])") #@UnusedVariable

            mail = email.message_from_string(data[0][1])

            valid_maintypes = ['multipart', 'text']
            if mail.get_content_maintype() not in valid_maintypes:
                continue
            
            attachments = []
            body = ''
            parts = 0
            for part in mail.walk():
                parts += 1
                if part.get_content_maintype() == 'multipart':
                    continue
                if part.get('Content-Disposition') is None:
                    accepted_types = ['text/plain', 'text/html']
                    if part.get_content_type() in accepted_types:
                        bod = ''
                        bod = part.get_payload(decode=True)
                        try:
                            clean = ''
                            clean = string.translate(bod, table)
                            body = clean

                        except Exception, e:
                            print 'ERROR Decoding body'
                            #raise e
                    
                    continue
                filename = part.get_filename()
                counter = 1
                if not filename:
                    filename = 'part-%03d%s' % (counter, 'bin')
                    counter += 1
                else:
                    attachment = db.EmailAttachment(filename)
                    attachment.rename(email_content.entry_id)
                    filename = attachment.stored_name
                    attachments.append(attachment)
                att_path = os.path.join(detach_dir, filename)
                if not os.path.isfile(att_path):
                    fp = open(att_path, 'wb')
                    fp.write(part.get_payload(decode=True))
                    fp.close()
    
            email_content.body = body
            #print body
            db.save(email_content, attachments)


# Sync email!
if __name__ == "__main__":
    # get companies who's useGoogleSync is true
    googleCompanies = db.get_google_companies()

    # get companies who has ImapUserSetting
    imapCompanies = db.get_companies()

    # Firstly Oauth way
    processedUser = []
    for gCompany in googleCompanies:
        gUsers = db.get_google_users(gCompany.id)
        if not gUsers:
            continue

        for gUser in gUsers:
            user = db.get_person(gUser)
            user.username = user.first_name
            sync(user, '', True)
            processedUser.append(gUser)

    for company in imapCompanies:
        #google_sync = db.get_google_sync(company.company_id)
        users = db.get_users(company.company_id)
        for user in users:
            if user.person_id in processedUser:
                continue

            print 'Run Below id for Imap'

            server = db.get_server(user.server_id)
            sync(user, server, False)
