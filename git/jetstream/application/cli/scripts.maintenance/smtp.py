#!/usr/bin/python
import smtplib
from email.mime.text import MIMEText


def send_email(to, subject, content, user='reminder@jetstreamcrm.com',
               sender='Jetstream Appointment Reminder <reminder@jetstreamcrm.com>',
               priority='normal'):
    port = 465
    server = 'smtp-relay.gmail.com'

    msg = MIMEText(content, 'html')
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = to
    if priority == 'high':
        msg['X-Priority'] = '1'
        msg['X-MSMail-Priority'] = 'High'
    mail_server = smtplib.SMTP(server, port)
    mail_server.ehlo()
    mail_server.starttls()
    mail_server.ehlo()
    mail_server.sendmail(user ,to, msg.as_string())
    mail_server.close()


def test_send_email(to):
    content = '''
                <html>
                    <head>
                        <title>Test</title>
                    </head>
                    <body>
                        <h1>Hello!</h1>
                    </body>
                </html>
              '''
    send_email(to, 'test', content)


if __name__ == "__main__":
    test_send_email('eric.beringer@gmail.com')
