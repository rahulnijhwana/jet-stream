#!/usr/bin/python
import email
import imaplib
import os
import re
import string
from BeautifulSoup import BeautifulSoup
from email.mime.image import MIMEImage
from email.parser import HeaderParser
from email.utils import getaddresses
from datetime import datetime, timedelta
from time import localtime, strftime, mktime
from constants import IMAP_SERVER_TYPE_EXCHANGE, IMAP_SERVER_TYPE_GMAIL
import db


def parse_addresses(msg, type):
    """
    Builds 2 semi-colon separated strings for database insertion
    1 containing display names, 1 containing email addresses
    """
    names, emails = [], []
    raw = msg.get_all(type, [])
    addresses = getaddresses(raw)
    for address in addresses:
        name, addr = address
        if not name:
            name = addr
        names.append(name)
        emails.append(addr)
    return ";".join(names),";".join(emails)


def sync(user, server):

    local = datetime.today() - timedelta(hours=6)
    today = local.strftime("%d-%b-%Y")

    start_time = datetime.now() - timedelta(hours=6)

    print
    print 'IMAP Sync begins: ' + start_time.strftime("%m/%d/%Y %H:%M:%S %p")
    
    user.last_sync = None

    # If this is the first run, just get messages from the last hour
    if not user.last_sync:
        user.last_sync = (datetime.fromtimestamp(mktime(localtime())) - timedelta(hours=16))

    if server.ssl:
        m = imaplib.IMAP4_SSL(server.address, server.port)
    else:
        m = imaplib.IMAP4(server.address, server.port)

    login_error = False
    try:
        m.login(user.username, user.password)
    except Exception, e:
        print 'Problem logging into server. Incorrect password?'
        print 'Account: ' + user.username
        login_error = True
    
    if not login_error:
        print 'Syncing user: ' + user.username
        print
        print 'Last sync: ' + str(user.last_sync)
        
        # Sent mail
        m.select('Sent Items')
            
        resp, sent = m.search(None, '(SINCE "' + today + '")') #@UnusedVariable
        sent = sent[0].split()
        print 'Sent mail: '
        print sent
        traverse(m, user, sent)
        
        # Server is set to UTC
        local = datetime.now() - timedelta(hours=6) 
        db.set_last_sync(user, local)


def traverse(m, user, items):
    ### directory where to save attachments (default: current)
    detach_dir = '/webtemp/attachments/'
    # Translation table
    table = ''.join(' ' if n < 32 or n > 126 else chr(n) for n in xrange(256))
    for emailid in items:
        header = m.fetch(emailid, '(BODY.PEEK[HEADER])')
        parser = HeaderParser()
        msg = parser.parsestr(header[1][0][1])
        if not msg['Date']:
            continue
        received = datetime.fromtimestamp(email.utils.mktime_tz(email.utils.parsedate_tz(msg['Date']))) #@UndefinedVariable
        received -= timedelta(hours=6)
        
        print 'Received: ' + str(received)
        if(received > user.last_sync):
            print 'new mail!'

            email_content = db.EmailContent()
            email_content.recipient_names, email_content.recipient_emails = parse_addresses(msg, 'to')
            email_content.cc_names, email_content.cc_emails = parse_addresses(msg, 'cc')
            if email_content.cc_names:
                email_content.cc_names += ';'
            email_content.cc_names += 'Compliance User'
            if email_content.cc_emails:
                email_content.cc_emails += ';'
            email_content.cc_emails += 'compliance@asgib.com'

            email_content.from_name, email_content.from_email = parse_addresses(msg, 'from')

            received_date = email.utils.parsedate_tz(msg['Date'])[0:9] #@UndefinedVariable
            email_content.received_date = re.sub(r"\b0", "", strftime("%m/%d/%Y %H:%M:%S %p", received_date)) #@UndefinedVariable
            if not msg['Message-ID']:
               continue
            email_content.entry_id = email.utils.unquote(msg['Message-ID']) #@UndefinedVariable
            email_content.subject = string.translate(msg['Subject'], table)
            email_content.person_id = user.person_id
            email_content.company_id = user.company_id
            resp, data = m.fetch(emailid, "(RFC822)") #@UnusedVariable
            mail = email.message_from_string(data[0][1])

            valid_maintypes = ['multipart', 'text']
            if mail.get_content_maintype() not in valid_maintypes:
                print mail.get_content_maintype()
                continue
            
            attachments = []
            body = ''
            parts = 0
            for part in mail.walk():
                parts += 1
                if part.get_content_maintype() == 'multipart':
                    continue
                if part.get('Content-Disposition') is None:
                    accepted_types = ['text/plain', 'text/html']
                    if part.get_content_type() in accepted_types:
                        bod = ''
                        bod = part.get_payload(decode=True)
                        try:
                            clean = ''
                            clean = string.translate(bod, table)
                            body = clean 

                        except Exception, e:
                            print 'ERROR Decoding body'
                            #raise e
                    
                    continue
                filename = part.get_filename()
                counter = 1
                if not filename:
                    filename = 'part-%03d%s' % (counter, 'bin')
                    counter += 1
                else:
                    attachment = db.EmailAttachment(filename)
                    attachment.rename(email_content.entry_id)
                    filename = attachment.stored_name
                    attachments.append(attachment)
                att_path = os.path.join(detach_dir, filename)
                if not os.path.isfile(att_path):
                    fp = open(att_path, 'wb')
                    fp.write(part.get_payload(decode=True))
                    fp.close()
    
            email_content.body = body
            print 'BODY::::::'
            print
            print email_content.body
            print
            print
            try:
                db.save(email_content, attachments, compliance=True)
            except Exception, e:
                raise e

    
# Sync email!
if __name__ == "__main__":
    companies = db.get_asg()
    for company in companies:
        users = db.get_users(company.company_id)
        for user in users:
            server = db.get_server(user.server_id)
            sync(user, server)

