# TODO
# set freetds memory
# set freetds check date off
# use sed -i or else.
# let me this later .. now so urgent with client.... :) besso

# Update Apt, but only after the new sources.list is in place and the 10gen file is in place
exec { 'apt-update':
  command => '/usr/bin/apt-get update'
}

# Packages that need to be installed
$webtools = [
  "apache2",
  "git-core",
  "curl",
  "python-boto",
  "memcached",
  "php5-fpm",
  "php5",
  "libapache2-mod-php5",
  "php5-dev",
  "php-pear",
  "php5-curl",
  "php5-imap",
  "php5-mcrypt",
  "php5-memcached",
  "php5-xdebug",
  "phpmyadmin"
]

# Ensure that all of the above packages are installed after updating Apt
package { $webtools:
  ensure => "installed",
  require => Exec['apt-update']
}

$odbc = [
  'unixodbc',
  'unixodbc-dev',
  'freetds-dev',
  'freetds-bin',
  'tdsodbc'
]

package { $odbc:
  ensure => "installed",
  require => Package[$webtool]
}

exec { 'set mssql datetime in php.ini' :
  command => '/bin/sed -i "s/;mssql.datetimeconvert = Off/mssql.datetimeconvert = Off/g" /etc/php5/apache2/php.ini',
  logoutput => true,
  require => Package[$webtool]
}

exec { 'set mssql textlimit in php.ini' :
  command => '/bin/sed -i "s/mssql.textlimit = 4096/mssql.textlimit = 99048576/g" /etc/php5/apache2/php.ini',
  logoutput => true,
  require => Package[$webtool]
}

exec { 'set mssql textsize in php.ini' :
  command => '/bin/sed -i "s/mssql.textsize = 4096/mssql.textsize = 99048576/g" /etc/php5/apache2/php.ini',
  logoutput => true,
  require => Package[$webtool]
}

exec { 'allow Apache to accept mod_rewrite' :
  command => '/bin/sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/sites-available/default',
  logoutput => true,
  require => Package[$webtool]
}

exec { 'enable mod_rewrite' :
  command => '/bin/ln -s /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/rewrite.load',
  logoutput => true,
  require => Package[$webtool]
}

package { 'php5-sybase':
  ensure => "installed",
  require => Package[$webtool]
}

file { 'odbc.ini':
  ensure => file,
  mode => 644,
  owner => 'root',
  group => 'root',
  path => '/etc/odbc.ini',
  source => '/vagrant/conf/odbc/odbc.ini',
  require => Package[$odbc]
}

file { 'freetds.conf':
  ensure => file,
  mode => 644,
  owner => 'root',
  group => 'root',
  path => '/etc/freetds/freetds.conf',
  source => '/vagrant/conf/odbc/freetds.conf',
  require => Package[$odbc]
}

file { 'bash_profile':
  ensure => file,
  mode => 0644,
  owner => 'vagrant',
  group => 'vagrant',
  path => '/home/vagrant/.bash_profile',
  source => '/vagrant/conf/.bash_profile'
}

# generate cache directory,  
file { ['/webtemp','/webtemp/newcache'] :
  ensure => 'directory',
  recurse => true,
  purge => true,
  force => true,
  mode => 0755,
  owner => 'www-data',
  group => 'www-data'
}

service { 'apache2':
  enable => true,
  ensure => "running"
}

# TODO. need to setup correct link for DocumentRoot
#exec { 'set document root' :
#  command => '/bin/rm -rf /var/www && /bin/ln -s /vagrant/application/web/jetstream /var/www && /bin/ln -s /vagrant/application/web/jetstream/www /var/www/jetstream',
#  logoutput => true
#}

Package[$webtools] ->
Package[$odbc] ->
File['bash_profile'] ->
File['odbc.ini'] ->
File['freetds.conf'] ->
Package['php5-sybase'] ->
Service['apache2']


# fi
