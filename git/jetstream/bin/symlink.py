#!/usr/bin/python

import os
import shutil

webroot = '/usr/share/nginx/www'
if not os.path.islink(webroot):
    shutil.rmtree(webroot)
    os.symlink('/vagrant/app/', '/usr/share/nginx/www')

docs = '/vagrant/app/docs'
if not os.path.islink(docs):
    os.symlink('/vagrant/docs', '/vagrant/app/docs')   